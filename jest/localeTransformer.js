// See lib/node/TranslationBuildTools/README.md for an overview of how locales are built

const buildNestedObject = require('./../lib/node/TranslationBuildTools/buildNestedObject');

module.exports = {
    process(src, filename) {
        const obj = buildNestedObject(filename);
        const locale = filename.match(/-(\w\w).json/)[1];
        const withLocale = {};
        withLocale[locale] = obj;
        return JSON.stringify(withLocale);
    }
};
