import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

global.RUNNING_IN_TEST_MODE = true;
global.bundledLocale = 'en';

configure({
    adapter: new Adapter()
});
