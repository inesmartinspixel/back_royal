## How to search and replace to transform the pixelmatters app.css conglomeration into something we can use

# First, use CSSFormat Sublime plugin to reformat the app.css from Pixelmatters using the CSS Expanded (Break Selectors) option

# Remove extra space between selectors
\}\n\n(.)
}\n$1

# Add back space after media queries
width:(\S)
width: $1

# Add back space after progid:
progid:
progid: 

# Add back space after media query ratio
ratio:(\S)
ratio: $1

# More media queries
ion:(\S)
ion: $1

# Fix hover issue
: hover
:hover

# Fix before issue
: before
:before

# Fix vector paths
\.\./img/(.*)\.svg
../vectors/\1.svg

# Fix image paths
\.\./img/(.*)\.png
../images/\1.png

# Change italic font name
proximanova-regularitalic
proximanova-regit

# Fix potentially busted "-of-type" selectors
: (.*?)-of-type
:$1-of-type

# Remove the first of the two animate.css files

# Remove font-face declarations for soft fonts (we already have our own via font-families.css that match up)

# Finally, use a diff tool to compare this transformed app.css to our current one and look for any anomalies