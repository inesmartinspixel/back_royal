##########################################################################################
## Deployment Includes
## You can modify these values if you want to deploy to an alternative environment
##########################################################################################

# change this to your branch name
deployment_branch: &deployment_branch master

# change "smartly-staging" to target env name
# NOTE: env name must be <= 40 characters
deployment_command: &deployment_command ./build/deploy.sh smartly-staging


##############################
## Includes
##############################

defaults: &defaults
  working_directory: /home/circleci/back_royal
  docker:
    - image: booleanbetrayal/circleci-trusty-ruby-node-python-awscli-psql-chrome:1.0.21

rails_defaults: &rails_defaults
  working_directory: /home/circleci/back_royal
  docker:
    - image: booleanbetrayal/circleci-trusty-ruby-node-python-awscli-psql-chrome:1.0.21
    - image: booleanbetrayal/postgisplv8:12-3.0
      environment:
          POSTGRES_HOST_AUTH_METHOD: trust
          POSTGRES_USER: circleci
          POSTGRES_DB: circleci
          PGDATA: /dev/shm/pgdata/data
  environment:
    - RAILS_ENV: test
    - PG_TEST_USERNAME: circleci
    - REDSHIFT_TEST_USERNAME: circleci


##############################
## Aliases
##############################

aliases:
  # Shallow Clone
  - &checkout-shallow
    name: Checkout
    command: |
      #!/bin/sh
      set -e

      # Workaround old docker images with incorrect $HOME
      # check https://github.com/docker/docker/issues/2968 for details

      if [ "${HOME}" = "/" ]
      then
        export HOME=$(getent passwd $(id -un) | cut -d: -f6)
      fi

      mkdir -p ~/.ssh

      echo 'github.com ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==
      bitbucket.org ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==
      ' >> ~/.ssh/known_hosts

      (umask 077; touch ~/.ssh/id_rsa)
      chmod 0600 ~/.ssh/id_rsa
      (cat <<EOF > ~/.ssh/id_rsa
      $CHECKOUT_KEY
      EOF
      )

      # use git+ssh instead of https
      git config --global url."ssh://git@github.com".insteadOf "https://github.com" || true

      if [ -e /home/circleci/back_royal/.git ]
      then
          cd /home/circleci/back_royal
          git remote set-url origin "$CIRCLE_REPOSITORY_URL" || true
      else
          mkdir -p /home/circleci/back_royal
          cd /home/circleci/back_royal

          # use deprecated lfs clone as it appears much more performant than standard + fetch
          git lfs clone --depth=1 "$CIRCLE_REPOSITORY_URL" .
      fi

      git lfs install

      if [ -n "$CIRCLE_TAG" ]
      then
         git fetch --depth=10 --force origin "refs/tags/${CIRCLE_TAG}"
      elif [[ "$CIRCLE_BRANCH" =~ ^pull\/* ]]
      then
         # For PR from Fork
          git fetch --depth=10 --force origin "$CIRCLE_BRANCH/head:remotes/origin/$CIRCLE_BRANCH"
      else
          git fetch --depth=10 --force origin "$CIRCLE_BRANCH:remotes/origin/$CIRCLE_BRANCH"
      fi

      if [ -n "$CIRCLE_TAG" ]
      then
          git reset --hard "$CIRCLE_SHA1"
          git checkout -q "$CIRCLE_TAG"
      elif [ -n "$CIRCLE_BRANCH" ]
      then
          git reset --hard "$CIRCLE_SHA1"
          git checkout -q -B "$CIRCLE_BRANCH"
      fi

      git reset --hard "$CIRCLE_SHA1"



# ' <- this is stupid, but this single-quote fixes VSCode YAML parsing

##############################
## Jobs Configuration
##############################

version: 2

jobs:

  ##############################
  ## Checkout and Cleanup
  ##############################


  checkout_and_cleanup:

    <<: *defaults
    working_directory: /home/circleci

    steps:

      - run: *checkout-shallow

      - run:
          name: Removing Unnecessary Files
          working_directory: /home/circleci/back_royal
          command: rm -rf *.md *.sublime* ./.git ./content ./hybrid ./scorm_packages ./zapier ./git_hooks ./doc ./r

      - persist_to_workspace:
          root: /home/circleci/back_royal
          paths:
            - "*"
            - .ebextensions
            - .elasticbeanstalk
            - app
            - bin
            - build
            - config
            - db
            - db_redshift
            - hybrid
            - karma
            - lib
            - log
            - public
            - script
            - spec
            - vendor
            - webpack
            - zapier



  ##############################
  ## DB Setup Job
  ##############################

  setup_db:

    <<: *rails_defaults

    steps:

      - attach_workspace:
          at: /home/circleci/back_royal

      - restore_cache:
          keys: # see: `save_cache` step below
            - bundler-{{ arch }}-{{ checksum "Gemfile.lock" }}
            - bundler-{{ arch }}-

      - run:
          name: Bundle Install
          command: bundle check --path=vendor/bundle || bundle install --path=vendor/bundle --jobs=4 --retry=3 --deployment --without development
          no_output_timeout: 5m

      - save_cache:
          key: bundler-{{ arch }}-{{ checksum "Gemfile.lock" }} # see: `restore_cache` step above
          paths:
            - ./vendor/bundle

      - run:
          name: Database Setup
          command: ./build/setup_db.sh

      - persist_to_workspace:
          root: /home/circleci/back_royal
          paths:
            - config/database.yml
            - config/database_redshift.yml
            - db/ci_artifacts
            - spec/fixtures



  ##############################
  ## Rails Tests Job
  ##############################

  test_rails:

    <<: *rails_defaults

    parallelism: 4

    steps:

      - attach_workspace:
          at: /home/circleci/back_royal

      - restore_cache:
          keys:
            - bundler-{{ arch }}-{{ checksum "Gemfile.lock" }}
            - bundler-{{ arch }}-

      - run:
          name: Bundle Install
          command: bundle check --path=vendor/bundle || bundle install --path=vendor/bundle --jobs=4 --retry=3 --deployment --without development
          no_output_timeout: 5m

      - run:
          name: Restoring DB Dumps ...
          command: ./build/restore_db.sh

      - run:
          name: Building Fixtures ...
          command: ENABLE_CLOUDWATCH_LOGGING=false bundle exec rake test:build_fixtures

      - run:
          name: Rails Tests
          command: |
            ENABLE_CLOUDWATCH_LOGGING=false bundle exec rspec --profile 10 \
                              -r rspec_junit_formatter \
                              --format progress \
                              --format RspecJunitFormatter \
                              --out test-results/rspec.xml \
                              $(circleci tests glob "spec/**/*_spec.rb" | circleci tests split --split-by=timings)

      - store_test_results:
          path: test-results



  ##############################
  ## Dependencies
  ##############################

  npm:

    resource_class: xlarge

    <<: *defaults

    steps:

      - attach_workspace:
          at: /home/circleci/back_royal

      - restore_cache:
          keys:
            - npm-cache-{{ arch }}-{{ checksum "package-lock.json" }}
            - npm-cache-{{ arch }}-

      - run:
          name: FortAwesome Registry Setup
          command: echo -e "@fortawesome:registry=https://npm.fontawesome.com/\n//npm.fontawesome.com/:_authToken=$FORTAWESOME_TOKEN" > .npmrc
          root: /home/circleci/

      - run:
          name: NPM CI Install
          command: npm ci
          no_output_timeout: 5m

      # `npm ci` works by clearing out existing dependencies prior to resolution to guarantee that files
      # have not been altered. This means we cannot cache node_modules, but we can still cache npm's cache!
      - save_cache:
            key: npm-cache-{{ arch }}-{{ checksum "package-lock.json" }}
            paths:
              - /home/circleci/.npm

      - persist_to_workspace:
          root: /home/circleci/back_royal
          paths:
            - .


  ##############################
  ## Front-End Build Job
  ##############################

  build:

    resource_class: xlarge

    <<: *defaults

    steps:

      - attach_workspace:
          at: /home/circleci/back_royal

      - run:
          name: Create temp directories
          command: mkdir -p tmp/

      - run:
          name: Build
          command: ./build/stamp.sh && npm run build
          no_output_timeout: 30m

      - store_artifacts:
          path: ./vendor/front-royal/modules/BuildConfig/BuildConfig.js # it's nice to have this for stamping validation / cross-referencing

      - store_artifacts:
          path: ./public/assets/scripts # useful for verification of cachebusting, etc

      - store_artifacts:
          path: ./config/webpack_manifest.json

      - store_artifacts:
          path: ./config/webpack_asset_manifest.json

      - store_artifacts:
          path: ./config/webpack_locale_manifest.json

      - persist_to_workspace:
          root: /home/circleci/back_royal
          paths:
            - .


  ##############################
  ## Front-End Tests Job
  ##############################

  test_js_modules:

    <<: *defaults

    parallelism: 15
    resource_class: large

    steps:
      - attach_workspace:
          at: /home/circleci/back_royal

      - run:
          name: JS Module Tests
          no_output_timeout: 60m
          command: NODE_OPTIONS=--max-old-space-size=4096 ./node_modules/.bin/jest -c jest/config.modules.json --runInBand --ci $(circleci tests glob "vendor/front-royal/modules/**/*spec.js" | circleci tests split --split-by=timings)

      - store_test_results:
          path: test-results

  ##############################
  ## Screenshots Job
  ##############################

  test_screenshots:

    <<: *rails_defaults

    parallelism: 4
    resource_class: large

    steps:

      - attach_workspace:
          at: /home/circleci/back_royal

      - restore_cache:
          keys:
            - bundler-{{ arch }}-{{ checksum "Gemfile.lock" }}
            - bundler-{{ arch }}-

      - run:
          name: Bundle Install
          command: bundle check --path=vendor/bundle || bundle install --path=vendor/bundle --jobs=4 --retry=3 --deployment --without development
          no_output_timeout: 5m

      - run:
          name: Restoring DB Dumps ...
          command: ./build/restore_db.sh
      - run:
          name: Building Fixtures ...
          command: ENABLE_CLOUDWATCH_LOGGING=false bundle exec rake test:puppet:build_fixtures

      - run:
          name: Starting Test Server
          command: npm run testserver
          background: true

      - run:
          name: Waiting for Test Server Startup
          command: sleep 20

      - run:
          name: Screenshot Specs
          no_output_timeout: 12m
          command: |
            set +e
            ./node_modules/.bin/jest -c jest/config.puppeteer.json --runInBand --ci $(circleci tests glob "spec/puppeteer/*_spec.js" | circleci tests split)
            last_err_code="${?}"
            set -e
            if [ "${last_err_code}" -ne 0 ]; then
              rm -rf test-results/*
              rm -rf spec/puppeteer/__errors__/*
              rm -rf spec/puppeteer/__diff_output__/*
              sleep 1
              ./node_modules/.bin/jest -c jest/config.puppeteer.json --detectOpenHandles --runInBand --ci $(circleci tests glob "spec/puppeteer/*_spec.js" | circleci tests split)
            fi


      - store_test_results:
          path: test-results

      - store_artifacts:
          path: spec/puppeteer/__errors__/
          destination: /screenshot_errors

      - store_artifacts:
          path: spec/puppeteer/__diff_output__/
          destination: /screenshot_failures


  ##############################
  ## Deploy Job
  ##############################

  deploy_to_environment:

    <<: *defaults

    steps:

      - attach_workspace:
          at: /home/circleci/back_royal

      - run:
          name: Setting up AWS
          command: mkdir -p /home/circleci/.aws && cp ./config/aws/roles/cli/config.aws.ci /home/circleci/.aws/config && echo -e "[default]\naws_access_key_id = ${AWS_ACCESS_KEY_ID}\naws_secret_access_key = ${AWS_SECRET_ACCESS_KEY}" > /home/circleci/.aws/credentials

      - deploy:
          name: Deploy
          command: *deployment_command


##############################
## Workflow Configuration
##############################

workflows:

  version: 2

  # This config should allow concurrent execution of Rails tests / alongside JS build + tests
  build_test_deploy:

    jobs:

      - checkout_and_cleanup

      - npm:
          requires:
            - checkout_and_cleanup

      - build:
          requires:
            - npm

      - setup_db:
          requires:
            - checkout_and_cleanup

      - test_rails:
          requires:
            - setup_db

      - test_js_modules:
          requires:
            - npm

      - test_screenshots:
          requires:
            - setup_db
            - build

      - deploy_to_environment:
          filters: # comment out this filter block if targeting an alternative environment
            branches:
              only: *deployment_branch
          requires:
            - test_rails
            - test_js_modules
            - test_screenshots
