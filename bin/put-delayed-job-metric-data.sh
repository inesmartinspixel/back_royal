#!/bin/sh

# Loading environment data
EB_SUPPORT_DIR=$(/opt/elasticbeanstalk/bin/get-config container -k support_dir)
. $EB_SUPPORT_DIR/envvars


echo "RUNNING QUERY AGAINST $RDS_HOSTNAME $RDS_DB_NAME"

queuedJobCount=`PGPASSWORD=$RDS_PASSWORD psql -h $RDS_HOSTNAME $RDS_DB_NAME -U $RDS_USERNAME -Atc "select count(*) from delayed_jobs where failed_at is null and run_at < now();"`

queued_job_cmd="aws cloudwatch put-metric-data --metric-name QueuedJobs --namespace  \"$APP_ENV_NAME DelayedJob\" --value \"$queuedJobCount\" --unit Count"

echo "COMMAND: $queued_job_cmd"

AWS_DEFAULT_REGION=us-east-1 sh -c "$queued_job_cmd"

###############################################

failedJobCount=`PGPASSWORD=$RDS_PASSWORD psql -h $RDS_HOSTNAME $RDS_DB_NAME -U $RDS_USERNAME -Atc "select count(*) from delayed_jobs where failed_at is not null;"`

failed_job_cmd="aws cloudwatch put-metric-data --metric-name FailedJobs --namespace  \"$APP_ENV_NAME DelayedJob\" --value \"$failedJobCount\" --unit Count"

echo "COMMAND: $failed_job_cmd"

AWS_DEFAULT_REGION=us-east-1 sh -c "$failed_job_cmd"

###############################################

erroredJobCount=`PGPASSWORD=$RDS_PASSWORD psql -h $RDS_HOSTNAME $RDS_DB_NAME -U $RDS_USERNAME -Atc "select count(*) from delayed_jobs where last_error is not null and ((queue = 'sync_with_airtable' AND attempts > 5) OR (queue = 'delete_from_segment' AND attempts > 9) OR (queue not in ('sync_with_airtable', 'delete_from_segment') and attempts > 3));"`

errored_job_cmd="aws cloudwatch put-metric-data --metric-name ErroredJobs --namespace  \"$APP_ENV_NAME DelayedJob\" --value \"$erroredJobCount\" --unit Count"

echo "COMMAND: $errored_job_cmd"

AWS_DEFAULT_REGION=us-east-1 sh -c "$errored_job_cmd"