#!/usr/bin/env bash

# see http://www.dannemanne.com/posts/post-deployment_script_on_elastic_beanstalk_restart_delayed_job

# Using similar syntax as the appdeploy pre hooks that is managed by AWS

# Loading environment data
EB_SCRIPT_DIR=$(/opt/elasticbeanstalk/bin/get-config container -k script_dir)
EB_SUPPORT_DIR=$(/opt/elasticbeanstalk/bin/get-config container -k support_dir)
EB_APP_USER=$(/opt/elasticbeanstalk/bin/get-config container -k app_user)
EB_APP_CURRENT_DIR=$(/opt/elasticbeanstalk/bin/get-config container -k app_deploy_dir)
EB_APP_PIDS_DIR=$(/opt/elasticbeanstalk/bin/get-config container -k app_pid_dir)

# Setting up correct environment and ruby version so that bundle can load all gems
. $EB_SUPPORT_DIR/envvars
. $EB_SCRIPT_DIR/use-app-ruby.sh

# Now we can do the actual restart of the worker. Make sure to have double quotes when using env vars in the command.
cd $EB_APP_CURRENT_DIR

# stopping first in case we're changing the list of queues
su -s /bin/bash -c "bundle exec bin/delayed_job --pid-dir=$EB_APP_PIDS_DIR stop" $EB_APP_USER

if [ -n "$ALTERNATIVE_STAGING_ENVIRONMENT" ]; then
    echo "Disabling delayed_job for $ALTERNATIVE_STAGING_ENVIRONMENT"
elif [ -z "$DELAYED_JOB_QUEUES" ]; then
    echo "No delayed job queues to process"
else
    echo "Starting delayed job: $DELAYED_JOB_QUEUES"
    su -s /bin/bash -c "bundle exec bin/delayed_job --pid-dir=$EB_APP_PIDS_DIR --queues=$DELAYED_JOB_QUEUES start" $EB_APP_USER
fi