#!/bin/sh

# Loading environment data
EB_SUPPORT_DIR=$(/opt/elasticbeanstalk/bin/get-config container -k support_dir)
. $EB_SUPPORT_DIR/envvars


echo "RUNNING QUERY AGAINST $RDS_HOSTNAME $RDS_DB_NAME"

# calculate the time between the last lesson:frame:unload event that was logged and the maximum
# last_lesson_activity_at in reports, which indicates how much delay there is between something happening
# and when it hits reports
reportsDelay=`PGPASSWORD=$RDS_PASSWORD psql -h $RDS_HOSTNAME $RDS_DB_NAME -U $RDS_USERNAME -Atc "select extract(epoch from (select max(created_at) from events where event_type='lesson:frame:unload') - (select max(last_lesson_activity_time) from user_progress_records where last_lesson_activity_time < now())) delay_seconds;"`

reports_delay_cmd="aws cloudwatch put-metric-data --metric-name ReportsDelaySeconds --namespace  \"$APP_ENV_NAME BackRoyal\" --value \"$reportsDelay\" --unit Seconds"

echo "COMMAND: $reports_delay_cmd"

AWS_DEFAULT_REGION=us-east-1 sh -c "$reports_delay_cmd"