class AddProjectScores < ActiveRecord::Migration[5.1]
    def up

        create_table :user_project_scores, id: :uuid do |t|
            t.uuid          :user_id,        null: false
            t.float         :score,          null: false
        end

        add_index :user_project_scores, :user_id, :unique => true


        # putting this in a function in case there are
        # strange exceptions at some point in the future
        execute %Q~
            CREATE FUNCTION is_paid_cert_program_type(program_type text) RETURNS BOOLEAN AS
            $func$
            BEGIN
                RETURN program_type like 'paid_cert%';
            END
            $func$ LANGUAGE plpgsql;
        ~

        ViewHelpers.migrate(self, 20180301211924)
    end

    def down
        ViewHelpers.rollback(self, 20180301211924)

        execute("DROP FUNCTION is_paid_cert_program_type(program_type text)")

        drop_table :user_project_scores
    end
end
