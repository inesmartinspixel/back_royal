class FinishIndustryRoleRefactor < ActiveRecord::Migration[5.1]
    def up
        remove_column :career_profiles, :industry, :text
        remove_column :career_profiles_versions, :industry, :text

        execute "UPDATE work_experiences SET role='other' WHERE role IS NULL"
        execute "UPDATE work_experiences SET industry='other' WHERE industry IS NULL"

        change_column_default(:work_experiences, :role, 'other')
        change_column_default(:work_experiences, :industry, 'other')
    end

    def down
        add_column :career_profiles, :industry, :text
        add_column :career_profiles_versions, :industry, :text

        change_column_default(:work_experiences, :role, nil)
        change_column_default(:work_experiences, :industry, nil)
    end
end
