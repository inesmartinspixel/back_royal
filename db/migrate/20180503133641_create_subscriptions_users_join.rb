class CreateSubscriptionsUsersJoin < ActiveRecord::Migration[5.1]
    def change
        create_table :subscriptions_users, id: :uuid do |t|
            t.uuid :user_id, null: false
            t.uuid :subscription_id, null: false
        end
        add_index :subscriptions_users, [:subscription_id], :unique => true # each subscription can only be joined with one user
        add_foreign_key :subscriptions_users, :users
        add_foreign_key :subscriptions_users, :subscriptions
    end
end
