class StudentNetworkOptimizations < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def change
        # this helps speed up student network queries, where we filter by accepted
        # and join against cohorts
        add_index :cohort_applications, [:status, :cohort_id], algorithm: :concurrently
    end
end
