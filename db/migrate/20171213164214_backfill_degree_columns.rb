class BackfillDegreeColumns < ActiveRecord::Migration[5.1]
  # we are disabling transactions so that we don't update every row in the
  # table in a single transaction.  In order for this to be ok, we need
  # to make sure everything in this migration is idempotent
  disable_ddl_transaction!

  def up
    # Backfill (idempotent)
    CareerProfile::EducationExperience.in_batches.update_all degree_program: true
    CareerProfile::EducationExperience.in_batches.update_all will_not_complete: false

    # Set nullable (idempotent)
    change_column_null :education_experiences, :degree_program, false
  end

  def down
    change_column_null :education_experiences, :degree_program, true
  end

end
