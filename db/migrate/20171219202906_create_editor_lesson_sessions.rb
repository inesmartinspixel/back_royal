class CreateEditorLessonSessions < ActiveRecord::Migration[5.1]
    def change
        create_table :editor_lesson_sessions, id: :uuid do |t|
            t.uuid          "user_id",              null: false
            t.uuid          "lesson_id",            null: false
            t.timestamp     "started_at",           null: false
            t.timestamp     "last_activity_time",   null: false
            t.float         "total_time",           null: false
            t.integer       "save_count",           null: false
        end

        add_index(:editor_lesson_sessions, [:user_id, :lesson_id])
        add_index(:editor_lesson_sessions, :started_at)
    end
end