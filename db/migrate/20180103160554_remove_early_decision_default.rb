class RemoveEarlyDecisionDefault < ActiveRecord::Migration[5.1]

  def up
    change_column_default :career_profiles, :consider_early_decision, nil
  end

  def down
    change_column_default :career_profiles, :consider_early_decision, false
  end

end
