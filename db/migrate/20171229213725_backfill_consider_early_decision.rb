class BackfillConsiderEarlyDecision < ActiveRecord::Migration[5.1]
    # we are disabling transactions so that we don't update every row in the
    # table in a single transaction.  In order for this to be ok, we need
    # to make sure everything in this migration is idempotent
    disable_ddl_transaction!

    def up
        # Backfill (idempotent)
        CareerProfile.in_batches.update_all consider_early_decision: false

        # Set nullable (idempotent)
        change_column_null :career_profiles, :consider_early_decision, false
    end

    def down
        change_column_null :career_profiles, :consider_early_decision, true
    end
end
