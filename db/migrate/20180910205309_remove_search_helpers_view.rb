class RemoveSearchHelpersView < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20180910205309)
    end

    def down
        ViewHelpers.rollback(self, 20180910205309)
    end
end
