class AddFieldsToOpenPosition < ActiveRecord::Migration[5.1]
    def change
        add_column :open_positions, :place_id, :text
        add_column :open_positions, :place_details, :json, :default => {}, :null => false
        add_column :open_positions, :role, :text
        add_column :open_positions, :position_type, :text
        add_column :open_positions, :job_post_url, :text
        add_column :open_positions, :description, :text
        add_column :open_positions, :perks, :text, :array => true, :default => []
        add_column :open_positions, :desired_years_experience, :json, :default => {}, :null => false
        add_column :open_positions, :featured, :boolean
        add_column :open_positions, :archived, :boolean

        add_column :open_positions_versions, :place_id, :text
        add_column :open_positions_versions, :place_details, :json
        add_column :open_positions_versions, :role, :text
        add_column :open_positions_versions, :position_type, :text
        add_column :open_positions_versions, :job_post_url, :text
        add_column :open_positions_versions, :description, :text
        add_column :open_positions_versions, :perks, :text, :array => true
        add_column :open_positions_versions, :desired_years_experience, :json
        add_column :open_positions_versions, :featured, :boolean
        add_column :open_positions_versions, :archived, :boolean
    end
end
