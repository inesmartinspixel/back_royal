class AddNewColumnsToCohortApplications < ActiveRecord::Migration[5.1]

    def up
        add_column :cohort_applications, :registered_early, :boolean
        add_column :cohort_applications, :allow_early_registration_pricing, :boolean
        add_column :cohort_applications_versions, :registered_early, :boolean
        add_column :cohort_applications_versions, :allow_early_registration_pricing, :boolean

        change_column_default(:cohort_applications, :registered_early, false)
        change_column_default(:cohort_applications, :allow_early_registration_pricing, false)
    end

    def down
        remove_column :cohort_applications, :registered_early
        remove_column :cohort_applications, :allow_early_registration_pricing
        remove_column :cohort_applications_versions, :registered_early
        remove_column :cohort_applications_versions, :allow_early_registration_pricing
    end

end
