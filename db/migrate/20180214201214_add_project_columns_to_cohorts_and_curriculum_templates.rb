class AddProjectColumnsToCohortsAndCurriculumTemplates < ActiveRecord::Migration[5.1]

  def up
    # cohorts, cohorts_versions
    add_column :cohorts, :project_submission_email, :string
    add_column :cohorts, :project_documents, :json, array: true
    add_column :cohorts_versions, :project_submission_email, :string
    add_column :cohorts_versions, :project_documents, :json, array: true

    # curriculum_templates, curriculum_templates_versions
    add_column :curriculum_templates, :project_submission_email, :string
    add_column :curriculum_templates, :project_documents, :json, array: true
    add_column :curriculum_templates_versions, :project_submission_email, :string
    add_column :curriculum_templates_versions, :project_documents, :json, array: true
  end

  def down
    # cohorts, cohorts_versions
    remove_column :cohorts, :project_submission_email
    remove_column :cohorts, :project_documents
    remove_column :cohorts_versions, :project_submission_email
    remove_column :cohorts_versions, :project_documents

    # curriculum_templates, curriculum_templates_versions
    remove_column :curriculum_templates, :project_submission_email
    remove_column :curriculum_templates, :project_documents
    remove_column :curriculum_templates_versions, :project_submission_email
    remove_column :curriculum_templates_versions, :project_documents
  end

end
