class AddNewColumnsToCareerProfileForAccreditationPurposes < ActiveRecord::Migration[5.1]

    def up
        add_column :career_profiles, :native_english_speaker, :boolean
        add_column :career_profiles, :earned_accredited_degree_in_english, :boolean
        add_column :career_profiles_versions, :native_english_speaker, :boolean
        add_column :career_profiles_versions, :earned_accredited_degree_in_english, :boolean
    end

    def down
        remove_column :career_profiles, :native_english_speaker
        remove_column :career_profiles, :earned_accredited_degree_in_english
        remove_column :career_profiles_versions, :native_english_speaker
        remove_column :career_profiles_versions, :earned_accredited_degree_in_english
    end
end
