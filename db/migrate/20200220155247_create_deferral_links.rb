class CreateDeferralLinks < ActiveRecord::Migration[6.0]
    def up
        create_table :deferral_links, id: :uuid do |t|
            t.timestamps
            t.uuid :user_id, null: false
            t.timestamp :expires_at, null: false
            t.boolean :used, default: false, null: false
            t.uuid :from_cohort_application_id, null: false
            t.uuid :to_cohort_application_id
        end

        add_foreign_key :deferral_links, :users
        add_foreign_key :deferral_links, :cohort_applications, column: :from_cohort_application_id
        add_foreign_key :deferral_links, :cohort_applications, column: :to_cohort_application_id
        add_index :deferral_links, [:user_id, :used] # for quickly verifying the only-one-active rule

        create_versions_table_and_trigger_v2(:deferral_links)
    end

    def down
        drop_table :deferral_links
        drop_table :deferral_links_versions
    end
end
