class BillingTransactionUpdates < ActiveRecord::Migration[5.2]
    def change
        #  id                      :uuid             not null, primary key
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  transaction_time        :datetime         not null
#  transaction_type        :text             not null
#  provider                :text             not null
#  provider_transaction_id :text             not null
#  amount                  :float            not null
#  amount_refunded         :float            default(0.0), not null
#  refunded                :boolean          default(FALSE), not null
#  currency                :text             not null
#  description             :text
#  metadata                :json
#  stripe_livemode         :boolean

        change_column_null :billing_transactions, :provider, :true
        change_column_null :billing_transactions, :provider_transaction_id, :true

        # these never should have been set on the versions table, but there was a bug in MigrateHelper
        change_column_null :billing_transactions_versions, :provider, :true
        change_column_null :billing_transactions_versions, :provider_transaction_id, :true

    end
end
