class BackfillRegisteredAtForCohortApplications < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        # This backfill is a little more verbose than usual because I am using the common table expression
        CohortApplication.where(registered: true).in_batches.each do |cohort_applications_relation|
            execute "
                WITH registered_ats AS MATERIALIZED (
                    SELECT
                        cav.id
                        , min(cav.updated_at) AS registered_at
                    FROM cohort_applications_versions cav
                        JOIN cohort_applications ca on cav.id = ca.id
                    WHERE cav.registered=true AND ca.registered = true AND cav.id IN (#{cohort_applications_relation.select(:id).to_sql})
                    GROUP BY cav.id
                )

                UPDATE cohort_applications SET registered_at = registered_ats.registered_at FROM registered_ats
                    WHERE registered_ats.id = cohort_applications.id
                    AND cohort_applications.id IN (#{cohort_applications_relation.select(:id).to_sql});
             "
        end
    end
end
