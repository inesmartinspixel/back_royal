class AddDiplomaGenDateToCohortsAndCurriculumTemplates < ActiveRecord::Migration[5.1]

  def up
    # cohorts, cohorts_versions
    add_column :cohorts, :diploma_generation_days_offset, :integer
    add_column :cohorts_versions, :diploma_generation_days_offset, :integer

    # curriculum_templates, curriculum_templates_versions
    add_column :curriculum_templates, :diploma_generation_days_offset, :integer
    add_column :curriculum_templates_versions, :diploma_generation_days_offset, :integer
  end

  def down
    # cohorts, cohorts_versions
    remove_column :cohorts, :diploma_generation_days_offset, :integer
    remove_column :cohorts_versions, :diploma_generation_days_offset, :integer

    # curriculum_templates, curriculum_templates_versions
    remove_column :curriculum_templates, :diploma_generation_days_offset, :integer
    remove_column :curriculum_templates_versions, :diploma_generation_days_offset, :integer
  end

end
