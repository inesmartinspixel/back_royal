class AddSkillsToCareerProfileSearches < ActiveRecord::Migration[5.1]
    # NOTE: following guidelines here: https://github.com/ankane/strong_migrations
    def up
      add_column :career_profile_searches, :skills, :text, array: true
      change_column_default :career_profile_searches, :skills, []
      commit_db_transaction
      CareerProfile::Search.in_batches.update_all skills: []
      change_column_null :career_profile_searches, :skills, false
    end

    def down
        remove_column :career_profile_searches, :skills
    end
end
