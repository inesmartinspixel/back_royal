class BackfillGraduationDates < ActiveRecord::Migration[5.1]
    def up

        # Since we're rewriting old versions, we don't NEED to edit_and_republish,
        # but it will prevent us from getting into a situation where all the cohorts
        # are showing that they have been updated since the last publish
        Cohort.where.not(graduation_date_days_offset: nil).edit_and_republish do |cohort|
            update_cohort(cohort)
        end

        # rewrite old versions to have reasonable values for the new column, since
        # the old column is going to eventually be removed
        Cohort::Version.where.not(graduation_date_days_offset: nil).each do |cohort_version|
            update_cohort(cohort_version)
            cohort_version.commit_changes
        end

        CurriculumTemplate.all.each do |template|
            next unless template.periods
            days = 0
            template.periods.each do |period|
                days = days + (period['days_offset'] || 0) + period['days']
            end
            update_dates(template, days)
            template.save!
        end
    end

    def update_cohort(cohort)
        # round to correct for DST
        days = ((cohort.end_date - cohort.start_date) / 1.day).round
        update_dates(cohort, days)
    end

    def update_dates(schedulable_item, cohort_days)
        if schedulable_item.graduation_date_days_offset
            schedulable_item.graduation_days_offset_from_end = schedulable_item.graduation_date_days_offset - cohort_days
        end

        if schedulable_item.diploma_generation_days_offset
            schedulable_item.diploma_generation_days_offset_from_end = schedulable_item.diploma_generation_days_offset - cohort_days
        end
    end
end
