class RemoveFinalScoreFromCohortUserProgressRecords < ActiveRecord::Migration[5.2]
    def up
        ViewHelpers.migrate(self, 20181211145713)
    end

    def down
        ViewHelpers.rollback(self, 20181211145713)
    end
end
