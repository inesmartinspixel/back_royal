class AddPaymentDetailsToApplications < ActiveRecord::Migration[5.1]
    def change

        add_column :cohort_applications, :num_charged_payments, :integer
        add_column :cohort_applications_versions, :num_charged_payments, :integer

        add_column :cohort_applications, :num_refunded_payments, :integer
        add_column :cohort_applications_versions, :num_refunded_payments, :integer

        remove_column :cohort_applications, :skip_auto_enrollment_expulsion, :boolean
        remove_column :cohort_applications_versions, :skip_auto_enrollment_expulsion, :boolean

    end
end
