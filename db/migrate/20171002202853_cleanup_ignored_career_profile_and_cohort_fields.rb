class CleanupIgnoredCareerProfileAndCohortFields < ActiveRecord::Migration[5.1]
    def change
        remove_column :career_profiles, :career_profile_active, :boolean
        remove_column :career_profiles_versions, :career_profile_active, :boolean

        [:cohorts, :curriculum_templates].each do |table|
            [:enrollment_warning_days_offset, :enrollment_expulsion_days_offset].each do |col|
                remove_column table, col, :integer
                remove_column :"#{table}_versions", col, :integer
            end
        end
    end
end
