class AddPrefAllowPushNotificationsToUsers < ActiveRecord::Migration[5.1]

    def up
        add_column :users, :pref_allow_push_notifications, :boolean
        add_column :users_versions, :pref_allow_push_notifications, :boolean

        change_column_default :users, :pref_allow_push_notifications, true

        recreate_users_trigger_v1
    end

    def down
        remove_column :users, :pref_allow_push_notifications
        remove_column :users_versions, :pref_allow_push_notifications

        recreate_users_trigger_v1
    end

end
