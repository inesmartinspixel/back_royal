class DropTempContentTables < ActiveRecord::Migration[6.0]
    def tables
        %w(
            published_cohort_periods
            published_cohort_periods_stream_locale_packs
            published_cohort_playlist_locale_packs
            published_cohort_stream_locale_packs
            published_cohort_lesson_locale_packs
        )
    end

    def up
        tables.each do |table|
            execute "DROP TABLE #{table}_temp"
        end
    end

    def down
        tables.each do |table|
            execute "CREATE TABLE #{table}_temp AS SELECT * FROM #{table}"
        end

        add_index :published_cohort_periods_temp, [:cohort_id, :index], :unique => true, :name => :coh_id_index_on_publ_coh_periods_temp

        add_index :published_cohort_periods_stream_locale_packs_temp, [:cohort_id, :index, :locale_pack_id], :unique => true, :name => :coh_id_index_on_publ_coh_per_str_loc_packs_temp

        add_index :published_cohort_playlist_locale_packs_temp, [:cohort_id, :playlist_locale_pack_id], :unique => true, :name => :cohort_and_play_lp_on_coh_play_lps_temp
        add_index :published_cohort_playlist_locale_packs_temp, :playlist_locale_pack_id, :name => :playlist_lp_on_cohort_playlist_packs_temp

        add_index :published_cohort_stream_locale_packs_temp, [:cohort_id, :stream_locale_pack_id], :name => :cohort_and_str_lp_on_cohort_str_lps_temp, :unique => true
        add_index :published_cohort_stream_locale_packs_temp, :stream_locale_pack_id, :name => :stream_lp_on_cohort_stream_packs_temp

        add_index :published_cohort_lesson_locale_packs_temp, [:cohort_id, :lesson_locale_pack_id], :unique => true, :name => :cohort_and_lesson_lp_on_coh_lesson_lps_temp
        add_index :published_cohort_lesson_locale_packs_temp, :lesson_locale_pack_id, :name => :lesson_lp_on_cohort_lesson_locale_packs_temp
    end
end
