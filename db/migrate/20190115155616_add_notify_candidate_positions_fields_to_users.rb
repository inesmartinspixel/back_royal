require File.expand_path("../migrate_helper", __FILE__)

class AddNotifyCandidatePositionsFieldsToUsers < ActiveRecord::Migration[5.2]
    include MigrateHelper

    def up
        add_column :users, :notify_candidate_positions, :boolean
        add_column :users_versions, :notify_candidate_positions, :boolean

        add_column :users, :notify_candidate_positions_recommended, :text
        add_column :users_versions, :notify_candidate_positions_recommended, :text

        recreate_users_trigger_v2
    end

    def down
        remove_column :users, :notify_candidate_positions
        remove_column :users_versions, :notify_candidate_positions

        remove_column :users, :notify_candidate_positions_recommended
        remove_column :users_versions, :notify_candidate_positions_recommended

        recreate_users_trigger_v2
    end
end
