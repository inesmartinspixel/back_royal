class AddExternalFlagToInstitutions < ActiveRecord::Migration[6.0]
    def up
        add_column :institutions, :external, :boolean, default: true, null: false
        add_column :institutions_versions, :external, :boolean
        recreate_trigger_v2(:institutions)
    end

    def down
        remove_column :institutions, :external, :boolean, default: true, null: false
        remove_column :institutions_versions, :external, :boolean
        recreate_trigger_v2(:institutions)
    end
end
