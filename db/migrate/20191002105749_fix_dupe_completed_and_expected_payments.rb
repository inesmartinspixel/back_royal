class FixDupeCompletedAndExpectedPayments < ActiveRecord::Migration[6.0]
  def up
    ViewHelpers.migrate(self, 20191002105749)
end

def down
    ViewHelpers.rollback(self, 20191002105749)
end
end
