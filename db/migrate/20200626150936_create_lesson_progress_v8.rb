class CreateLessonProgressV8 < ActiveRecord::Migration[6.0]
    def up
        execute "CREATE TYPE lesson_progress_json_v8 as (
            user_id uuid,
            updated_at int,
            created_at int,
            locale_pack_id uuid,
            frame_bookmark_id uuid,
            frame_history json,
            completed_frames json,
            challenge_scores json,
            frame_durations json,
            started_at int,
            completed_at int,
            complete boolean,
            last_progress_at int,
            id uuid,
            best_score float,
            for_assessment_lesson boolean,
            for_test_lesson boolean
        )"
    end

    def down
        execute "DROP TYPE lesson_progress_json_v8"
    end
end
