class AddUserHasSeenFirstPositionReviewModal < ActiveRecord::Migration[5.1]

    def up
        add_column :users, :has_seen_first_position_review_modal, :boolean, default: false
        add_column :users_versions, :has_seen_first_position_review_modal, :boolean
        recreate_users_trigger_v1
    end

    def down
        remove_column :users, :has_seen_first_position_review_modal
        remove_column :users_versions, :has_seen_first_position_review_modal
        recreate_users_trigger_v1
    end

end