class AddUpdatedAtIndexesToProgressTables < ActiveRecord::Migration[5.2]
    disable_ddl_transaction!

    def change

        if !Rails.env.production?
            # needed in set_progress_max_updated_at
            ActiveRecord::Base.logger = Logger.new(STDOUT)

            add_index(:lesson_progress, [:user_id, :updated_at], algorithm: :concurrently)
            add_index(:lesson_streams_progress, [:user_id, :updated_at], algorithm: :concurrently)
        end
    end
end