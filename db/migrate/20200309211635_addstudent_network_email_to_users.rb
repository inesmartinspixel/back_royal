class AddstudentNetworkEmailToUsers < ActiveRecord::Migration[6.0]
    
    def up

        add_column :users, :student_network_email, :string, limit: 255, null: true
        add_column :users_versions, :student_network_email, :string, limit: 255, null: true

        recreate_users_trigger_v4
    end

    def down
        remove_column :users, :student_network_email
        remove_column :users_versions, :student_network_email
        recreate_users_trigger_v4
    end

end
