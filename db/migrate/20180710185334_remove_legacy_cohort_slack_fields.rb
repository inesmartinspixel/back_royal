class RemoveLegacyCohortSlackFields < ActiveRecord::Migration[5.1]
    def change
        remove_column :cohorts, :slack_url, :text
        remove_column :cohorts, :slack_token, :text

        remove_column :cohorts_versions, :slack_url, :text
        remove_column :cohorts_versions, :slack_token, :text
    end
end
