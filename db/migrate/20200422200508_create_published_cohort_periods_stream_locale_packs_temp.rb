class CreatePublishedCohortPeriodsStreamLocalePacksTemp < ActiveRecord::Migration[6.0]
    def up
        create_table :published_cohort_periods_stream_locale_packs_temp, id: false do |t|
            t.datetime :created_at
            t.uuid :cohort_id
            t.integer :index
            t.uuid :locale_pack_id
            t.text :title
            t.boolean :required
            t.boolean :optional
        end

        add_index :published_cohort_periods_stream_locale_packs_temp, [:cohort_id, :index, :locale_pack_id], :unique => true, :name => :coh_id_index_on_publ_coh_per_str_loc_packs_temp

        # PublishedCohortPeriodsStreamLocalePack.rebuild
    end

    def down
        drop_table :published_cohort_periods_stream_locale_packs_temp
    end
end
