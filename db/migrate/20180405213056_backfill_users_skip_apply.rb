class BackfillUsersSkipApply < ActiveRecord::Migration[5.1]
  disable_ddl_transaction!

  def up
    # Backfill (idempotent)
    User.in_batches.update_all skip_apply: false

    # Set nullable (idempotent)
    change_column_null :users, :skip_apply, false
  end

  def down
    change_column_null :users, :skip_apply, true
  end

end
