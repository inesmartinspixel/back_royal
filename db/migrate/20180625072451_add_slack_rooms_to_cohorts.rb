class AddSlackRoomsToCohorts < ActiveRecord::Migration[5.1]

    def up

        # add join table for hiring teams
        create_table :cohort_slack_rooms, id: :uuid do |t|
            t.timestamps
            t.uuid :cohort_id, null: false
            t.text :title, null: false
            t.text :url, null: false
            t.text :admin_token, null: false
        end
        add_index :cohort_slack_rooms, [:cohort_id, :title], :unique => true
        add_index :cohort_slack_rooms, :url, :unique => true
        add_index :cohort_slack_rooms, :admin_token, :unique => true

        create_versions_table_and_trigger_v1("cohort_slack_rooms")

        add_column :cohort_applications, :cohort_slack_room_id, :uuid
        add_column :cohort_applications_versions, :cohort_slack_room_id, :uuid
        add_foreign_key :cohort_applications, :cohort_slack_rooms


        slack_rooms = execute(%Q~
            select
                id as cohort_id
                , name as cohort_name
                , slack_token
                , slack_url
            from cohorts
            where slack_token is not null~
        ).to_a

        slack_rooms.each do |slack_room|

            # some existing rooms have no urls, but it does not matter since we're
            # not planning on sending out any more links
            url = slack_room['slack_url'] || "no url for #{slack_room['cohort_id']}"
            result = execute(%Q~
                insert into cohort_slack_rooms (cohort_id, title, admin_token, url, created_at, updated_at) values (
                    '#{slack_room['cohort_id']}',
                    'Room 1',
                    '#{slack_room['slack_token']}',
                    '#{url}',
                    now(),
                    now()
                )
                returning id;
            ~).to_a

            slack_room_id = result[0]['id']

            result = execute(%Q~
                update cohort_applications
                    set cohort_slack_room_id='#{slack_room_id}'
                    where cohort_id='#{slack_room['cohort_id']}'
                        and status='accepted'
            ~)

            puts "Added #{result.cmd_tuples} accepted users to slack room for #{slack_room['cohort_name']}"
        end
    end

    def down
        remove_column :cohort_applications_versions, :cohort_slack_room_id
        remove_column :cohort_applications, :cohort_slack_room_id

        drop_table :cohort_slack_rooms_versions
        drop_table :cohort_slack_rooms
    end
end
