class BackfillAllowPasswordChange < ActiveRecord::Migration[5.2]
    disable_ddl_transaction!

    # Can't user User because we end up with a ColumnNotSerializableError error
    # see https://github.com/lynndylanhurley/devise_token_auth/issues/1079
    class BackfillableUser < ApplicationRecord
        self.table_name = 'users'
    end

    def up
        BackfillableUser.where(allow_password_change: nil).in_batches.update_all(allow_password_change: false)
    end

    def down
        # noop
    end
end
