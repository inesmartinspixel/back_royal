class FixCompletedAndExpectedPayments < ActiveRecord::Migration[6.0]
    def up
        ViewHelpers.migrate(self, 20191001184345)
    end

    def down
        ViewHelpers.rollback(self, 20191001184345)
    end
end
