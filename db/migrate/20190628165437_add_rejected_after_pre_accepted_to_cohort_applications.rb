class AddRejectedAfterPreAcceptedToCohortApplications < ActiveRecord::Migration[5.2]

    def up
        add_column :cohort_applications, :rejected_after_pre_accepted, :boolean, :default => false, :null => false
        add_column :cohort_applications_versions, :rejected_after_pre_accepted, :boolean

        recreate_trigger_v2(:cohort_applications)
    end

    def down
        remove_column :cohort_applications, :rejected_after_pre_accepted
        remove_column :cohort_applications_versions, :rejected_after_pre_accepted

        recreate_trigger_v2(:cohort_applications)
    end
end
