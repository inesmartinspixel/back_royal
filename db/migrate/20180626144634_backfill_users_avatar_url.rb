class BackfillUsersAvatarUrl < ActiveRecord::Migration[5.1]

    disable_ddl_transaction!

    def up

        while more_avatars_exist
            execute %Q~
                WITH avatars AS MATERIALIZED (
                    SELECT
                        users.id AS user_id
                        , (avatars.formats->>'original')::json->>'url' AS url
                    FROM users
                    JOIN s3_assets avatars
                        ON avatar_id = avatars.id
                    LIMIT 1000
                )
                UPDATE users
                SET avatar_url=subquery.url,
                    avatar_id=NULL
                FROM (
                    SELECT * FROM avatars
                ) AS subquery
                WHERE users.id=subquery.user_id;
            ~
        end

    end

    def more_avatars_exist
        !!(execute(%Q~
            SELECT users.id
            FROM users
            JOIN s3_assets
                ON s3_assets.id = users.avatar_id
            WHERE avatar_id IS NOT NULL
            LIMIT 1
        ~)).to_a[0]
    end

end
