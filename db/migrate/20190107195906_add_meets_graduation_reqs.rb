class AddMeetsGraduationReqs < ActiveRecord::Migration[5.2]
    def up
        add_column :cohort_applications, :meets_graduation_requirements, :boolean
        add_column :cohort_applications_versions, :meets_graduation_requirements, :boolean
        recreate_trigger_v2 "cohort_applications"
        ViewHelpers.migrate(self, 20190107195906)
    end

    def down
        ViewHelpers.rollback(self, 20190107195906)
        remove_column :cohort_applications, :meets_graduation_requirements
        remove_column :cohort_applications_versions, :meets_graduation_requirements
        recreate_trigger_v2 "cohort_applications"
end
end
