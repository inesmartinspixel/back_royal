class PullProjectScoreFromApplication < ActiveRecord::Migration[5.2]
    def up
        ViewHelpers.migrate(self, 20181212213605)
    end

    def down
        ViewHelpers.rollback(self, 20181212213605)
    end
end
