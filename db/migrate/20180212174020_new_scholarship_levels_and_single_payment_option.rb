class NewScholarshipLevelsAndSinglePaymentOption < ActiveRecord::Migration[5.1]


    def up
        migrate_relevant_data(true)
    end

    def down
        migrate_relevant_data(false)
    end



    def custom_coupon_level(stripe_coupon_id)
        amount = stripe_coupon_id.split('_').last.to_i * 100
        '\'{"name": "Custom Scholarship", "coupons": {"emba_800": {"id": "' + stripe_coupon_id + '", "amount_off": "' + amount.to_s + '", "percent_off": null} } }\'::json'
    end

    def migrate_relevant_data(up)

        if up

            # Coupon Scheme
            # `(*)` denotes new plan / coupon that needs to be added to Stripe
            # UP-FRONT (base of 9600) = emba_once_9600 {allow_premium_coupons: true, sort_key: 3, total_num_required_payments: 1}
            #
            # $8,000 = discount_1600 (*)
            # $6,500 = discount_3100 (*)
            # $5,500 = discount_4100 (*)
            # $4,500 = discount_5100 (*)
            # $3,500 = discount_6100 (*)
            # $2,400 = discount_7200 (*)

            # BI-ANNUAL (base of 4800) = emba_bi_4800
            #
            # $4,200 = discount_600
            # $3,400 = discount_1400
            # $2,900 = discount_1900 (*)
            # $2,400 = discount_2400 (*)
            # $1,850 = discount_2950 (*)
            # $1,300 = discount_3500 (*)

            # MONTHLY = No changes

            # new scholarship levels
            base_level_sql = '\'{"name": "No Scholarship", "coupons": {"emba_800": {"id": "none", "amount_off": 0, "percent_off": 0}, "emba_bi_4800": {"id": "discount_600", "amount_off": 60000, "percent_off": null}, "emba_once_9600": {"id": "discount_1600", "amount_off": 160000, "percent_off": null} } }\'::json'
            level_1_sql    = '\'{"name": "Level 1", "coupons": {"emba_800": {"id": "discount_150", "amount_off": 15000, "percent_off": null}, "emba_bi_4800": {"id": "discount_1400", "amount_off": 140000, "percent_off": null}, "emba_once_9600": {"id": "discount_3100", "amount_off": 310000, "percent_off": null} } }\'::json'
            level_2_sql    = '\'{"name": "Level 2", "coupons": {"emba_800": {"id": "discount_250", "amount_off": 25000, "percent_off": null}, "emba_bi_4800": {"id": "discount_1900", "amount_off": 190000, "percent_off": null}, "emba_once_9600": {"id": "discount_4100", "amount_off": 410000, "percent_off": null} } }\'::json'
            level_3_sql    = '\'{"name": "Level 3", "coupons": {"emba_800": {"id": "discount_350", "amount_off": 35000, "percent_off": null}, "emba_bi_4800": {"id": "discount_2400", "amount_off": 240000, "percent_off": null}, "emba_once_9600": {"id": "discount_5100", "amount_off": 510000, "percent_off": null} } }\'::json'
            level_4_sql    = '\'{"name": "Level 4", "coupons": {"emba_800": {"id": "discount_450", "amount_off": 45000, "percent_off": null}, "emba_bi_4800": {"id": "discount_2950", "amount_off": 295000, "percent_off": null}, "emba_once_9600": {"id": "discount_6100", "amount_off": 610000, "percent_off": null} } }\'::json'
            level_5_sql    = '\'{"name": "Level 5", "coupons": {"emba_800": {"id": "discount_550", "amount_off": 55000, "percent_off": null}, "emba_bi_4800": {"id": "discount_3500", "amount_off": 350000, "percent_off": null}, "emba_once_9600": {"id": "discount_7200", "amount_off": 720000, "percent_off": null} } }\'::json'
            full_level_sql = '\'{"name": "Full Scholarship", "coupons": {"emba_800": {"id": "discount_100_percent", "amount_off": null, "percent_off": 100}, "emba_bi_4800": {"id": "discount_100_percent", "amount_off": null, "percent_off": 100},  "emba_once_9600": {"id": "discount_100_percent", "amount_off": null, "percent_off": 100} } }\'::json'

            # new plans
            stripe_plans_assignment_sql =
                'stripe_plans = ARRAY[
                    \'{"id": "emba_800", "name": "Monthly Plan", "amount": "80000", "frequency": "monthly"}\'::json,
                    \'{"id": "emba_bi_4800", "name": "Bi-Annual Plan", "amount": "480000", "frequency": "bi_annual"}\'::json,
                    \'{"id": "emba_once_9600", "name": "Single Payment", "amount": "960000", "frequency": "once"}\'::json
                ]'

        else

            # old scholarship levels
            base_level_sql = '\'{"name": "No Scholarship", "coupons": {"emba_800": {"id": "none", "amount_off": 0, "percent_off": 0}, "emba_bi_4800": {"id": "discount_600", "amount_off": 60000, "percent_off": null} } }\'::json'
            level_1_sql    = '\'{"name": "Level 1", "coupons": {"emba_800": {"id": "discount_150", "amount_off": 15000, "percent_off": null}, "emba_bi_4800": {"id": "discount_1400", "amount_off": 140000, "percent_off": null} } }\'::json'
            level_2_sql    = '\'{"name": "Level 2", "coupons": {"emba_800": {"id": "discount_250", "amount_off": 25000, "percent_off": null}, "emba_bi_4800": {"id": "discount_2000", "amount_off": 200000, "percent_off": null} } }\'::json'
            level_3_sql    = '\'{"name": "Level 3", "coupons": {"emba_800": {"id": "discount_350", "amount_off": 35000, "percent_off": null}, "emba_bi_4800": {"id": "discount_2500", "amount_off": 250000, "percent_off": null} } }\'::json'
            level_4_sql    = '\'{"name": "Level 4", "coupons": {"emba_800": {"id": "discount_450", "amount_off": 45000, "percent_off": null}, "emba_bi_4800": {"id": "discount_3000", "amount_off": 300000, "percent_off": null} } }\'::json'
            level_5_sql    = '\'{"name": "Level 5", "coupons": {"emba_800": {"id": "discount_550", "amount_off": 55000, "percent_off": null}, "emba_bi_4800": {"id": "discount_3550", "amount_off": 355000, "percent_off": null} } }\'::json'
            full_level_sql = '\'{"name": "Full Scholarship", "coupons": {"emba_800": {"id": "discount_100_percent", "amount_off": null, "percent_off": 100}, "emba_bi_4800": {"id": "discount_100_percent", "amount_off": null, "percent_off": 100} } }\'::json'

            # old plans
            stripe_plans_assignment_sql =
                'stripe_plans = ARRAY[
                    \'{"id": "emba_800", "name": "Monthly Plan", "amount": "80000", "frequency": "monthly"}\'::json,
                    \'{"id": "emba_bi_4800", "name": "Bi-Annual Plan", "amount": "480000", "frequency": "bi_annual"}\'::json
                ]'

        end

        # set scholarship levels
        scholarship_levels_assignment_sql =
                "scholarship_levels = ARRAY[
                   #{base_level_sql},
                   #{level_1_sql},
                   #{level_2_sql},
                   #{level_3_sql},
                   #{level_4_sql},
                   #{level_5_sql},
                   #{full_level_sql}
                ]"

        # create plan and levels configuration for existing paid plans as necessary
        ["cohorts", "cohorts_versions", "curriculum_templates", "curriculum_templates_versions"].each do |table|
            additional_where = (up && ["cohorts", "cohorts_versions"].include?(table)) ? "AND name IN ('EMBA8', 'EMBA9')" : ""
            execute "UPDATE #{table} SET #{stripe_plans_assignment_sql}, #{scholarship_levels_assignment_sql} WHERE stripe_plans IS NOT NULL #{additional_where}"
        end

    end

end
