class AddSurveyColumnsToCareerProfiles < ActiveRecord::Migration[5.2]

    def change
        add_column :career_profiles, :survey_years_full_time_experience, :text
        add_column :career_profiles, :survey_most_recent_role_description, :text
        add_column :career_profiles, :survey_highest_level_completed_education_description, :text

        add_column :career_profiles_versions, :survey_years_full_time_experience, :text
        add_column :career_profiles_versions, :survey_most_recent_role_description, :text
        add_column :career_profiles_versions, :survey_highest_level_completed_education_description, :text
    end
end
