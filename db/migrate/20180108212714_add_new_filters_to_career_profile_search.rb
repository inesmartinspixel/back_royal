class AddNewFiltersToCareerProfileSearch < ActiveRecord::Migration[5.1]

    def change
        add_column :career_profile_searches, :employment_types_of_interest, :text, array: true, default: [], null: false
        add_column :career_profile_searches, :school_name, :text
        add_column :career_profile_searches, :in_school, :boolean
        add_column :career_profile_searches, :company_name, :text
    end
end
