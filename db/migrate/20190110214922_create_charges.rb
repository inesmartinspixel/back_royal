class CreateCharges < ActiveRecord::Migration[5.2]

    def up
        create_table :charges, :id => :uuid do |t|
            t.timestamps
            t.timestamp :charge_time, null: false
            t.text :provider, null: false
            t.text :provider_charge_id, null: false
            t.float :amount, null: false
            t.float :amount_refunded, null: false, default: 0
            t.boolean :refunded, null: false, default: false
            t.text :currency, null: false
            t.text :description
            t.json :metadata
            t.boolean :stripe_livemode
        end

        create_versions_table_and_trigger_v2(:charges)

        add_index :charges, [:provider, :provider_charge_id], unique: true

        create_table :charges_users do |t|
            t.timestamps
            t.references :charge, index: { name: :charges_users_join}, type: :uuid, foreign_key: true

            # no foreign key on user because it is allowed for a user to be
            # deleted while keeping around zir charges
            t.references :user, type: :uuid
        end

        create_table :charges_hiring_teams do |t|
            t.timestamps
            t.references :charge, index: { name: :charges_hiring_teams_join}, type: :uuid, foreign_key: true


            # no foreign key on hiring_team because it is allowed for a hiring_team to be
            # deleted while keeping around zir charges
            t.references :hiring_team, type: :uuid
        end
    end

    def down
        drop_table :charges_hiring_teams
        drop_table :charges_users
        drop_table :charges_versions
        drop_table :charges
    end
end
