
class AddEditorToPlaylistsVersions < ActiveRecord::Migration[5.2]

    def up
        add_editor_columns_to_versions_table("playlists")
    end

    def down
        rollback_editor_columns_from_versions_table("playlists")
    end
end
