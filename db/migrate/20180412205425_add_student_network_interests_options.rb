class AddStudentNetworkInterestsOptions < ActiveRecord::Migration[5.1]

  def up
    create_table "student_network_interests_options", id: :uuid do |t|
        t.timestamps
        t.text :text,       null: false
        t.text :locale,     null: false, default: "en"
        t.boolean :suggest, default: false
        t.index ["locale", "text"], unique: true, name: "student_network_interests_options_unique"
        t.index ["suggest", "locale"], name: "student_network_interests_options_auto_suggest"
    end

    create_versions_table_and_trigger("student_network_interests_options")

    create_table    "career_profiles_student_network_interests_options", id: :uuid do |t|
        t.uuid      :career_profile_id,                     null: false
        t.uuid      :student_network_interests_option_id,   null: false
        t.integer   :position,                              null: false
        t.index     ["career_profile_id", "student_network_interests_option_id"],  unique: true,  name: "career_profiles_student_network_interests_options_unique"
    end

    add_foreign_key "career_profiles_student_network_interests_options", "career_profiles", column: "career_profile_id", primary_key: "id"
    add_foreign_key "career_profiles_student_network_interests_options", "student_network_interests_options", column: "student_network_interests_option_id", primary_key: "id"
  end

  def down
    execute "DROP TABLE IF EXISTS career_profiles_student_network_interests_options"
    execute "DROP TABLE IF EXISTS student_network_interests_options"
    execute "DROP TABLE IF EXISTS student_network_interests_options_versions"
  end

  def create_versions_table_and_trigger(table_name, columns_to_watch = nil)

      klass = Class.new(ActiveRecord::Base) do
          self.table_name = table_name
      end

      klass.connection.schema_cache.clear!
      klass.reset_column_information

      # create the version table, including all columns from the source table
      # plus a few extras
      create_table "#{table_name}_versions", id: false, force: :cascade do |t|
          t.uuid     "version_id", null: false, default: "uuid_generate_v4()"
          t.string   "operation", limit: 1, null: false
          t.datetime "version_created_at", null: false

          puts "*** #{table_name}_versions"

          klass.column_names.each do |column_name|
              type = klass.columns_hash

              column_config = klass.columns_hash[column_name]
              meth = column_config.type.to_sym
              options = {}
              options[:limit] = column_config.limit unless column_config.limit.nil?
              options[:null] = false if column_config.null == false
              options[:array] = column_config.array
              # ignore defaults, since they should not be used in the audit table

              puts "t.#{meth}, #{column_name}, #{options.inspect}"
              t.send(meth, column_name, options)
          end
      end

      # for some reason the primary flag on create_table was not working
      execute "ALTER TABLE #{table_name}_versions ADD PRIMARY KEY (version_id);"

      # create indexes
      if klass.column_names.include?('id') && klass.column_names.include?('updated_at')
          add_index "#{table_name}_versions", [:id, :updated_at], name: "#{table_name}_versions_id_updated_at"
      elsif klass.column_names.include?('id')
          add_index "#{table_name}_versions", :id, name: "#{table_name}_versions_id"
      end

      insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
      if columns_to_watch.nil?
          update_command = insert_on_update
      else
          if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
          update_command = "
              IF #{if_string} THEN
              #{insert_on_update}
              END IF;
              "
      end

      # create the trigger
      trigger_sql = "
          CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
              BEGIN
                  --
                  -- Create a row in audit to reflect the operation performed on the table,
                  -- make use of the special variable TG_OP to work out the operation.
                  --
                  IF (TG_OP = 'DELETE') THEN
                      INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                      RETURN OLD;
                  ELSIF (TG_OP = 'UPDATE') THEN
                      #{update_command}
                      RETURN NEW;
                  ELSIF (TG_OP = 'INSERT') THEN
                      INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                      RETURN NEW;
                  END IF;
                  RETURN NULL; -- result is ignored since this is an AFTER trigger
              END;
          $#{table_name}_version$ LANGUAGE plpgsql;

          CREATE TRIGGER #{table_name}_versions
          AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
          FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
      "
      execute(trigger_sql)
  end

end
