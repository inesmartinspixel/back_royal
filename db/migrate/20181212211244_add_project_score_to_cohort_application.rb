class AddProjectScoreToCohortApplication < ActiveRecord::Migration[5.2]
    def change
        add_column :cohort_applications, :project_score, :float
        add_column :cohort_applications_versions, :project_score, :float
    end
end
