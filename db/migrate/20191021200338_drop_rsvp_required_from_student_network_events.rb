class DropRsvpRequiredFromStudentNetworkEvents < ActiveRecord::Migration[6.0]

    def up
        remove_column :student_network_events, :rsvp_required
        remove_column :student_network_events_versions, :rsvp_required
        recreate_trigger_v2(:student_network_events)
    end


    def down
        add_column :student_network_events, :rsvp_required, :boolean, :null => false, default: false
        add_column :student_network_events_versions, :rsvp_required, :boolean
        recreate_trigger_v2(:student_network_events)
    end

end
