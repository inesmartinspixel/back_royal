class AddAirtableDataFieldsToCohortApplications < ActiveRecord::Migration[5.2]
    def up
        add_column :cohort_applications, :rubric_round_1_tag, :string
        add_column :cohort_applications_versions, :rubric_round_1_tag, :string

        add_column :cohort_applications, :rubric_round_1_reason_mba_invite, :string, array: true
        add_column :cohort_applications_versions, :rubric_round_1_reason_mba_invite, :string, array: true

        add_column :cohort_applications, :rubric_round_1_reason_mba_reject, :string, array: true
        add_column :cohort_applications_versions, :rubric_round_1_reason_mba_reject, :string, array: true

        add_column :cohort_applications, :rubric_round_1_reason_emba_invite, :string, array: true
        add_column :cohort_applications_versions, :rubric_round_1_reason_emba_invite, :string, array: true

        add_column :cohort_applications, :rubric_round_1_reason_emba_reject, :string, array: true
        add_column :cohort_applications_versions, :rubric_round_1_reason_emba_reject, :string, array: true

        add_column :cohort_applications, :rubric_final_decision, :string
        add_column :cohort_applications_versions, :rubric_final_decision, :string

        add_column :cohort_applications, :rubric_final_decision_reason, :string
        add_column :cohort_applications_versions, :rubric_final_decision_reason, :string

        add_column :cohort_applications, :rubric_interviewer, :string
        add_column :cohort_applications_versions, :rubric_interviewer, :string

        add_column :cohort_applications, :rubric_interview, :string
        add_column :cohort_applications_versions, :rubric_interview, :string

        add_column :cohort_applications, :rubric_interview_scheduled, :boolean
        add_column :cohort_applications_versions, :rubric_interview_scheduled, :boolean

        add_column :cohort_applications, :rubric_interview_recommendation, :string
        add_column :cohort_applications_versions, :rubric_interview_recommendation, :string

        add_column :cohort_applications, :rubric_reason_for_declining, :string, array: true
        add_column :cohort_applications_versions, :rubric_reason_for_declining, :string, array: true

        add_column :cohort_applications, :rubric_likelihood_to_yield, :string
        add_column :cohort_applications_versions, :rubric_likelihood_to_yield, :string

        add_column :cohort_applications, :rubric_risks_to_yield, :string, array: true
        add_column :cohort_applications_versions, :rubric_risks_to_yield, :string, array: true

        recreate_trigger_v2 "cohort_applications"
    end

    def down
        remove_column :cohort_applications, :rubric_round_1_tag
        remove_column :cohort_applications_versions, :rubric_round_1_tag

        remove_column :cohort_applications, :rubric_round_1_reason_mba_invite
        remove_column :cohort_applications_versions, :rubric_round_1_reason_mba_invite

        remove_column :cohort_applications, :rubric_round_1_reason_mba_reject
        remove_column :cohort_applications_versions, :rubric_round_1_reason_mba_reject

        remove_column :cohort_applications, :rubric_round_1_reason_emba_invite
        remove_column :cohort_applications_versions, :rubric_round_1_reason_emba_invite

        remove_column :cohort_applications, :rubric_round_1_reason_emba_reject
        remove_column :cohort_applications_versions, :rubric_round_1_reason_emba_reject

        remove_column :cohort_applications, :rubric_final_decision
        remove_column :cohort_applications_versions, :rubric_final_decision

        remove_column :cohort_applications, :rubric_final_decision_reason
        remove_column :cohort_applications_versions, :rubric_final_decision_reason

        remove_column :cohort_applications, :rubric_interviewer
        remove_column :cohort_applications_versions, :rubric_interviewer

        remove_column :cohort_applications, :rubric_interview
        remove_column :cohort_applications_versions, :rubric_interview

        remove_column :cohort_applications, :rubric_interview_scheduled
        remove_column :cohort_applications_versions, :rubric_interview_scheduled

        remove_column :cohort_applications, :rubric_interview_recommendation
        remove_column :cohort_applications_versions, :rubric_interview_recommendation

        remove_column :cohort_applications, :rubric_reason_for_declining
        remove_column :cohort_applications_versions, :rubric_reason_for_declining

        remove_column :cohort_applications, :rubric_likelihood_to_yield
        remove_column :cohort_applications_versions, :rubric_likelihood_to_yield

        remove_column :cohort_applications, :rubric_risks_to_yield
        remove_column :cohort_applications_versions, :rubric_risks_to_yield

        recreate_trigger_v2 "cohort_applications"
    end
end
