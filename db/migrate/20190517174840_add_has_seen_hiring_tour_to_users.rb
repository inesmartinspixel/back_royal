class AddHasSeenHiringTourToUsers < ActiveRecord::Migration[5.2]
    def up
        add_column :users, :has_seen_hiring_tour, :boolean
        add_column :users_versions, :has_seen_hiring_tour, :boolean

        change_column_default :users, :has_seen_hiring_tour, false

        recreate_users_trigger_v2
    end

    def down
        remove_column :users, :has_seen_hiring_tour
        remove_column :users_versions, :has_seen_hiring_tour

        recreate_users_trigger_v2
    end
end
