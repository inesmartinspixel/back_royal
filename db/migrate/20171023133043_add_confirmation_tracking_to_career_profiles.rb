class AddConfirmationTrackingToCareerProfiles < ActiveRecord::Migration[5.1]
    def change
        add_column :career_profiles, :last_confirmed_at_by_student, :datetime
        add_column :career_profiles, :last_confirmed_at_by_internal, :datetime
        add_column :career_profiles, :last_updated_at_by_student, :datetime
        add_column :career_profiles, :last_confirmed_internally_by, :string
        add_column :career_profiles_versions, :last_confirmed_at_by_student, :datetime
        add_column :career_profiles_versions, :last_confirmed_at_by_internal, :datetime
        add_column :career_profiles_versions, :last_updated_at_by_student, :datetime
        add_column :career_profiles_versions, :last_confirmed_internally_by, :string
    end
end
