class ChangeDocumentIdType < ActiveRecord::Migration[5.2]
	def up
        change_column :signable_documents, :sign_now_document_id, :text
    end
    def down
        execute('ALTER TABLE signable_documents ALTER COLUMN sign_now_document_id TYPE UUID USING uuid_generate_v4()')
    end
end
