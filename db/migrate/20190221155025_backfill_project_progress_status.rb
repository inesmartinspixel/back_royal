class BackfillProjectProgressStatus < ActiveRecord::Migration[5.2]
    disable_ddl_transaction!

    def up
        # Backfill progress records with a score to 'submitted'
        ProjectProgress.where(status: nil).where("score is NOT NULL").in_batches.update_all status: 'submitted'
        # Backfill the rest to 'unsubmitted'
        ProjectProgress.where(status: nil).in_batches.update_all status: 'unsubmitted'

        change_column_null :project_progress, :status, false
    end

    def down
        change_column_null :project_progress, :status, true
    end

end
