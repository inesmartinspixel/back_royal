class CreateSignableDocuments < ActiveRecord::Migration[5.2]
    def change

        create_table :signable_documents, id: :uuid do |t|
            t.timestamps
            t.text :document_type, null: false
            t.uuid :sign_now_document_id, null: true # old documents are from docusign, so can be null
            t.text :sign_now_signing_link, null: true
            t.timestamp :sign_now_signing_link_expiry, null: true
            t.timestamp :signed_at, null: true
            t.uuid :s3_asset_id, null: true
            t.json :metadata, null: false, default: {}

            t.references :user, type: :uuid, null: false, foreign_key: true
        end

        add_index :signable_documents, :sign_now_document_id, unique: true

        create_table :s3_signable_document_assets, id: :uuid do |t|
            t.timestamps
            t.attachment :file
            t.string :file_fingerprint
        end
    end
end