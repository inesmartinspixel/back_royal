class AddCareerProfileSearchHelpersFulltextSupport < ActiveRecord::Migration[5.1]
    def up
        add_column :career_profile_search_helpers_2, :skills_vector, :tsvector
        add_column :career_profile_search_helpers_2, :student_network_interests_vector, :tsvector
    end

    def down
        remove_column :career_profile_search_helpers_2, :skills_vector
        remove_column :career_profile_search_helpers_2, :student_network_interests_vector
    end
end
