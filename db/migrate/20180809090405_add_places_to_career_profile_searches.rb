class AddPlacesToCareerProfileSearches < ActiveRecord::Migration[5.1]
    def up
        add_column :career_profile_searches, :places, :json, :array => true, :default => [], :null => false
    end

    def down
        remove_column :career_profile_searches, :places
    end
end
