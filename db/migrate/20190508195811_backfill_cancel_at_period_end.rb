class BackfillCancelAtPeriodEnd < ActiveRecord::Migration[5.2]
    disable_ddl_transaction!

    def up

        # confirmed that all existing subscriptions are `cancel_at_period_end=false`

        # ```
        # subs = []; has_more=true; while has_more; options = {limit: 100}; if subs.any?; options[:starting_after] = subs.last.id; end; list = Stripe::Subscription.list(options); subs += list.data; has_more = list.has_more; puts subs.size; end;

        # subs.map(&:cancel_at_period_end).uniq
        # ```

        ActiveRecord::Base.connection.execute(%Q~
           UPDATE subscriptions
           SET cancel_at_period_end=false
        ~)

        change_column_default :subscriptions, :cancel_at_period_end, false
        change_column_null :subscriptions, :cancel_at_period_end, false

    end

    def down
        change_column_default :subscriptions, :cancel_at_period_end, nil
        change_column_null :subscriptions, :cancel_at_period_end, true
    end
end
