class BackfillPreAcceptedAtForCohortApplications < ActiveRecord::Migration[5.1]
    def up
        execute("
            UPDATE cohort_applications
            SET pre_accepted_at = now()
            WHERE cohort_applications.pre_accepted_at IS NULL
                AND cohort_applications.status = 'pre_accepted'
        ")
    end

    def down
        # noop
    end
end
