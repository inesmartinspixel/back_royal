class MoveBonusAndEquityIntoPerks < ActiveRecord::Migration[5.1]
    def up

        execute(%Q~
            update open_positions
            set perks = array_remove(Array[
                            case when bonus then 'bonus' else null END
                            , case when equity then 'equity' else null END
                        ], null) || perks
        ~)

    end
end
