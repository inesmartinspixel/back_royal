class AddDraftedFromPositionsToPositions < ActiveRecord::Migration[5.1]
  def up
    # New Columns
    add_column :open_positions, :drafted_from_positions , :boolean
    add_column :open_positions_versions, :drafted_from_positions , :boolean

    # Change column default
    change_column_default(:open_positions, :drafted_from_positions , false)
  end

  def down
    # New columns
    remove_column :open_positions, :drafted_from_positions
    remove_column :open_positions_versions, :drafted_from_positions
  end
end
