class Projects < ActiveRecord::Migration[5.2]
    def change
        create_table :learner_projects, :id => :uuid do |t|
            t.timestamps
            t.text :title                   , null: false
            t.text :requirement_identifier  , null: false
            t.text :tag                     , null: true
            t.text :internal_notes          , null: true
            t.json :project_documents       , null: false, default: [], array: true
        end
    end
end
