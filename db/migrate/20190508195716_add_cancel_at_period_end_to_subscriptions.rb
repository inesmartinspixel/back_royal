class AddCancelAtPeriodEndToSubscriptions < ActiveRecord::Migration[5.2]
  def up
      add_column :subscriptions, :cancel_at_period_end, :boolean
      add_column :subscriptions_versions, :cancel_at_period_end, :boolean

      # eventually, we can make this not-null.  See https://trello.com/c/nQ7EKom1
      add_column :subscriptions, :current_period_end, :timestamp
      add_column :subscriptions_versions, :current_period_end, :timestamp

      recreate_trigger_v2(:subscriptions)
  end

  def down

      remove_column :subscriptions, :cancel_at_period_end, :boolean
      remove_column :subscriptions_versions, :cancel_at_period_end, :boolean

      remove_column :subscriptions, :current_period_end, :timestamp
      remove_column :subscriptions_versions, :current_period_end, :timestamp
      recreate_trigger_v2(:subscriptions)
  end
end
