class RemoveEtl < ActiveRecord::Migration[5.1]
    def up
        execute 'alter table etl.published_content_titles set schema public'
        execute 'alter table etl.internal_content_titles set schema public'
        execute 'alter table etl.old_assessment_scores set schema public'
        ViewHelpers.migrate(self, 20180130030914)
        execute 'drop schema etl cascade' # this cannot be rolled back, but we're gonna flatten migrations after this
    end

    def down

        execute 'alter table published_content_titles set schema etl'
        execute 'alter table internal_content_titles set schema etl'
        execute 'alter table old_assessment_scores set schema etl'
        ViewHelpers.rollback(self, 20180130030914)
    end
end
