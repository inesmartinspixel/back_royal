class AddRefundIssued < ActiveRecord::Migration[5.1]
    def up
        add_column :cohort_applications, :refund_issued_at, :timestamp
        add_column :cohort_applications_versions, :refund_issued_at, :timestamp

        add_column :cohort_applications, :refund_attempt_failed, :boolean
        add_column :cohort_applications_versions, :refund_attempt_failed, :boolean

        change_column_default :cohort_applications, :refund_attempt_failed, false
    end

    def down
        remove_column :cohort_applications, :refund_attempt_failed
        remove_column :cohort_applications_versions, :refund_attempt_failed

        remove_column :cohort_applications, :refund_issued_at
        remove_column :cohort_applications_versions, :refund_issued_at
    end
end
