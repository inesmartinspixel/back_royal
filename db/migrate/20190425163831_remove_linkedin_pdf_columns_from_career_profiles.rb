class RemoveLinkedinPdfColumnsFromCareerProfiles < ActiveRecord::Migration[5.2]

    def up
        remove_column :career_profiles, :linkedin_pdf_id
        remove_column :career_profiles, :linkedin_pdf_import_status

        remove_column :career_profiles_versions, :linkedin_pdf_id
        remove_column :career_profiles_versions, :linkedin_pdf_import_status

        recreate_trigger_v2(:career_profiles)
    end

    def down
        add_column :career_profiles, :linkedin_pdf_id, :uuid
        add_column :career_profiles, :linkedin_pdf_import_status, :text

        add_column :career_profiles_versions, :linkedin_pdf_id, :uuid
        add_column :career_profiles_versions, :linkedin_pdf_import_status, :text

        add_foreign_key :career_profiles, :s3_assets, column: :linkedin_pdf_id
        add_index :career_profiles, :linkedin_pdf_id

        recreate_trigger_v2(:career_profiles)
    end
end
