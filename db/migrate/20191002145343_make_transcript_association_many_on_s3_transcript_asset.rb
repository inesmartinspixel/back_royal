class MakeTranscriptAssociationManyOnS3TranscriptAsset < ActiveRecord::Migration[6.0]
    def up
        add_column :s3_transcript_assets, :education_experience_id, :uuid
        add_column :s3_transcript_assets, :transcript_type, :string
        # asset tables have no versions

        # Note: We'd normally do a migration outside of a transaction, but there are
        # only about 200 - 300 records that need to be migrated.
        ActiveRecord::Base.connection.execute %Q~
            -- Make CTE to gather up both sides' ids where the s3_transcript_asset_id has
            -- actually been used in the new admin transcript UI
            with both_ids AS MATERIALIZED (
                select id as education_experience_id, s3_transcript_asset_id
                from education_experiences
                where s3_transcript_asset_id is not null
            )

            -- Update the new column using the CTE
            update s3_transcript_assets
            set education_experience_id = both_ids.education_experience_id
            FROM both_ids
            WHERE id = both_ids.s3_transcript_asset_id;
        ~

        remove_foreign_key :education_experiences, :s3_transcript_assets

        # can't remove the education_experiences#s3_transcript_asset_id column entirely
        # yet as we are adding to ignored_columns in this changeset
        # FIXME: https://trello.com/c/UgXRCyV1
    end

    def down
        remove_column :s3_transcript_assets, :education_experience_id, :uuid
        remove_column :s3_transcript_assets, :transcript_type, :string
        add_foreign_key :education_experiences, :s3_transcript_assets
    end
end
