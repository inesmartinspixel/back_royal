class RemoveCohortContentDetailsDependencies < ActiveRecord::Migration[6.0]
    def up
        ViewHelpers.migrate(self, 20200529191133)
    end

    def down
        ViewHelpers.rollback(self, 20200529191133)
    end
end