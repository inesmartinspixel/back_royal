
class AddEditorToAccessGroupsCurriculumTemplatesVersions < ActiveRecord::Migration[5.2]

    def up
        add_editor_columns_to_versions_table("access_groups_curriculum_templates")
    end

    def down
        rollback_editor_columns_from_versions_table("access_groups_curriculum_templates")
    end
end
