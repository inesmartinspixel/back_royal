class ReCreatePersistedTimelineEventsUsers < ActiveRecord::Migration[5.2]
    def up
        drop_table :persisted_timeline_events_users

        create_table :persisted_timeline_events_users, :id => :uuid do |t|
            t.timestamps
            t.references :persisted_timeline_event, index: { name: :p_timeline_events_user_join}, type: :uuid
            t.references :user, type: :uuid
        end

        begin
            PersistedTimelineEventUserJoin.create!({
                persisted_timeline_event_id: 'd4e91020-067e-4039-a47c-bbdf4d6391eb',
                user_id: '528fe809-9658-4077-9be1-845592f70a09'
            })
        rescue => exception
            # no-op ... will only succeed on Production DB
        end
    end

    def down
        drop_table :persisted_timeline_events_users

        create_table :persisted_timeline_events_users do |t|
            t.timestamps
            t.references :persisted_timeline_event, index: { name: :p_timeline_events_user_join}, type: :uuid
            t.references :user, type: :uuid
        end

    end
end
