class BackfillS3Avatars < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up

        result = execute %Q~
            with step_1 AS MATERIALIZED (
                SELECT
                    users.id as user_id,
                    (regexp_split_to_array(avatar_url, '[^\/]\/[^\/]'))[1] as domain,
                    avatar_url,
                    avatar_provider
                FROM users
                WHERE avatar_url IS NOT NULL
            )
            , with_expected_provider AS MATERIALIZED (
                SELECT
                    step_1.*
                    , case
                        when domain like '%facebook%' then 'facebook'
                        when domain like '%fbsbx%' then 'facebook'
                        when domain like '%fbcdn%' then 'facebook'
                        when domain like '%smart%' then 's3'
                        when domain like '%google%' then 'google_oauth2'
                        when domain like '%licdn%' then 'linkedin'
                        when domain like '%gravatar%' then 'gravatar'
                        else domain END
                    as expected_provider
                from step_1
            )
            , in_s3 AS MATERIALIZED (
                select
                    with_expected_provider.user_id
                    , s3_assets.id as asset_id
                from with_expected_provider
                join s3_assets
                    on (s3_assets.formats->>'original')::json->>'url' = avatar_url
                where expected_provider='s3'
            )
            select * from in_s3
        ~

        result = result.to_a

        puts "updating #{result.size} users"
        ActiveRecord::Migration.verbose = false
        result.each_with_index do |row, i|

            execute %Q~
                UPDATE users
                SET avatar_id='#{row['asset_id']}',
                    avatar_url=null
                WHERE users.id='#{row['user_id']}'
            ~

            progress = i+1
            if progress % 500 == 0 || progress == result.size
                puts "#{progress} of #{result.size} users updated"
            end

        end
        ActiveRecord::Migration.verbose = true

    end
end
