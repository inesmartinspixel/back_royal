class AddExercisesAndActionsToCohortPeriodsView < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20180728183452)
    end

    def down
        ViewHelpers.rollback(self, 20180728183452)
    end
end
