class AddOfficialTestScoreToStreamProgress < ActiveRecord::Migration[5.1]
    def change

        add_column :lesson_streams_progress, :official_test_score, :float

    end
end
