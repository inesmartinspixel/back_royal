class AddRecommendedPositionsSeenToUsers < ActiveRecord::Migration[5.2]

    def up
          add_column :users, :recommended_positions_seen_ids, :uuid, array: true
          add_column :users_versions, :recommended_positions_seen_ids, :uuid, array: true

          change_column_default :users, :recommended_positions_seen_ids, []

          recreate_users_trigger_v2
      end

      def down
          remove_column :users, :recommended_positions_seen_ids
          remove_column :users_versions, :recommended_positions_seen_ids

          recreate_users_trigger_v2
      end
end
