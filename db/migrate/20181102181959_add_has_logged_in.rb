class AddHasLoggedIn < ActiveRecord::Migration[5.2]

    def up
        add_column :users, :has_logged_in, :boolean
        add_column :users_versions, :has_logged_in, :boolean

        change_column_default :users, :has_logged_in, false

        recreate_users_trigger_v1
    end

    def down
        remove_column :users, :has_logged_in, :boolean
        remove_column :users_versions, :has_logged_in, :boolean

        recreate_users_trigger_v1
    end
end
