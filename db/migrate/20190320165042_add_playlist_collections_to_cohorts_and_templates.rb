class AddPlaylistCollectionsToCohortsAndTemplates < ActiveRecord::Migration[5.2]
    def up
        add_column :cohorts, :playlist_collections, :json, array: true
        add_column :cohorts_versions, :playlist_collections, :json, array: true

        change_column_default :cohorts, :playlist_collections, []

        recreate_trigger_v2 "cohorts"

        add_column :curriculum_templates, :playlist_collections, :json, array: true
        add_column :curriculum_templates_versions, :playlist_collections, :json, array: true

        change_column_default :curriculum_templates, :playlist_collections, []

        recreate_trigger_v2 "curriculum_templates"
    end

    def down
        remove_column :cohorts, :playlist_collections
        remove_column :cohorts_versions, :playlist_collections

        recreate_trigger_v2 "cohorts"

        remove_column :curriculum_templates, :playlist_collections
        remove_column :curriculum_templates_versions, :playlist_collections

        recreate_trigger_v2 "curriculum_templates"
    end
end
