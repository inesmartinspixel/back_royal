class BackfillHiringManagerReviewerId < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        # Note: We define CandidatePositionInterest.reviewed_statuses, but we should avoid
        # referencing models in migrations because the code will eventually change and break
        # the migration.
        backfill_sql = %Q~ UPDATE candidate_position_interests AS cpi
            SET hiring_manager_reviewer_id = op.hiring_manager_id
            FROM open_positions AS op
            WHERE cpi.open_position_id = op.id
            AND cpi.hiring_manager_status IN ('saved_for_later', 'invited', 'accepted', 'rejected')
        ~
        CandidatePositionInterest.connection.execute(backfill_sql)
    end
end
