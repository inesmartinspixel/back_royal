class AddAccessGroupsIndex < ActiveRecord::Migration[5.1]
    def change

        add_index :access_groups_users, [:user_id, :access_group_id]

    end
end
