class StudentNetworkIndeterminateDates < ActiveRecord::Migration[6.0]

  def up
      add_column :student_network_events, :date_tbd, :boolean, null: false, default: false
      add_column :student_network_events, :date_tbd_description, :text

      add_column :student_network_events_versions, :date_tbd, :boolean
      add_column :student_network_events_versions, :date_tbd_description, :text

      change_column_null :student_network_events, :start_time, true
      change_column_null :student_network_events, :end_time, true

      execute "
        ALTER TABLE student_network_events ADD CONSTRAINT valid_dates_config CHECK
        ((date_tbd IS true) OR (start_time IS NOT NULL AND end_time IS NOT NULL))
      "

      recreate_trigger_v2(:student_network_events)
  end

  def down

      remove_column :student_network_events, :date_tbd
      remove_column :student_network_events, :date_tbd_description

      remove_column :student_network_events_versions, :date_tbd
      remove_column :student_network_events_versions, :date_tbd_description

      change_column_null :student_network_events, :start_time, false
      change_column_null :student_network_events, :end_time, false

      recreate_trigger_v2(:student_network_events)
  end

end
