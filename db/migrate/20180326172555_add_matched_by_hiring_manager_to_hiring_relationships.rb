class AddMatchedByHiringManagerToHiringRelationships < ActiveRecord::Migration[5.1]

  def up
    add_column :hiring_relationships, :matched_by_hiring_manager, :boolean
    add_column :hiring_relationships_versions, :matched_by_hiring_manager, :boolean
  end

  def down
    remove_column :hiring_relationships, :matched_by_hiring_manager, :boolean
    remove_column :hiring_relationships_versions, :matched_by_hiring_manager, :boolean
  end

end
