class AddUsersPrefStudentNetworkPrivacyIndex < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        # this is used for doing a query with limit and order in the student network
        execute "CREATE INDEX concurrently sn_privacy_on_users ON users ((pref_student_network_privacy <> 'full'))"
    end

    def down
        execute "DROP INDEX sn_privacy_on_users"
    end
end
