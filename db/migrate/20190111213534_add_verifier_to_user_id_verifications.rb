class AddVerifierToUserIdVerifications < ActiveRecord::Migration[5.2]
    def change
        add_column :user_id_verifications, :verifier_id, :uuid
        add_column :user_id_verifications, :verifier_name, :text
    end
end
