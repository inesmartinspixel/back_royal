class BackfillCandidateStatusForPositionsInvites < ActiveRecord::Migration[5.1]

  def change
    # Get all hiring relationships that were created from inviting a
    # candidate to interview from positions, have a candidate_status
    # of 'pending' and a hiring_manager_status of 'accepted', and set
    # candidate_status to 'accepted'
    HiringRelationship.joins("join candidate_position_interests cpi on cpi.open_position_id = hiring_relationships.open_position_id and cpi.candidate_id = hiring_relationships.candidate_id")
        .where("hiring_relationships.candidate_status = 'pending' and hiring_relationships.hiring_manager_status = 'accepted'")
        .in_batches.update_all candidate_status: 'accepted'
  end

end
