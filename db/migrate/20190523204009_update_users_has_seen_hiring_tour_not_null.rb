class UpdateUsersHasSeenHiringTourNotNull < ActiveRecord::Migration[5.2]

    def up
        change_column_null :users, :has_seen_hiring_tour, false
    end

    def down
        change_column_null :users, :has_seen_hiring_tour, true
    end
end
