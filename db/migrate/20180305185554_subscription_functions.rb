class SubscriptionFunctions < ActiveRecord::Migration[5.1]
    def up

        execute %Q~
            CREATE EXTENSION plv8;

            CREATE FUNCTION interval_for_plan(stripe_plans json[], stripe_plan_id text)
            RETURNS text AS $$

                function getIntervalForPlan(plan) {
                    switch (plan.frequency) {
                        case 'bi_annual':
                            return '6 months';
                        case 'monthly':
                            return '1 month';
                    }
                }

                var plan;
                for (var i = 0; i < stripe_plans.length; i++) {
                    var _plan = stripe_plans[i];
                    if (_plan.id === stripe_plan_id) {
                        plan = _plan;
                    }
                }

                return getIntervalForPlan(plan);

            $$ LANGUAGE plv8 IMMUTABLE STRICT;

            CREATE FUNCTION payment_amount_per_period(stripe_plans json[], stripe_plan_id text, scholarship_level json)
            RETURNS float AS $$

                function getPlanTuitionActualTotal(plan) {
                    var coupon = scholarship_level.coupons[plan.id];

                    var amountOff = coupon ? coupon.amount_off : 0,
                        percentOff = coupon ? coupon.percent_off : 0,
                        tuitionNumPaymentIntervals = getNumIntervalsForPlan(plan),
                        tuitionOriginalTotal = (plan.amount * tuitionNumPaymentIntervals / 100) || 0,
                        tuitionScholarshipTotal;


                    if (!!amountOff) {
                        tuitionScholarshipTotal = tuitionNumPaymentIntervals * amountOff / 100;
                    } else if (!!percentOff) {
                        tuitionScholarshipTotal = percentOff * tuitionOriginalTotal / 100;
                    } else {
                        tuitionScholarshipTotal = 0;
                    }
                    return tuitionOriginalTotal - tuitionScholarshipTotal;
                }

                function getNumIntervalsForPlan(plan) {
                    switch (plan.frequency) {
                        case 'bi_annual':
                            return 2;
                        case 'monthly':
                            return 12;
                        case 'once':
                            return 1;
                    }
                }

                var plan;
                for (var i = 0; i < stripe_plans.length; i++) {
                    var _plan = stripe_plans[i];
                    if (_plan.id === stripe_plan_id) {
                        plan = _plan;
                    }
                }

                var tuitionActualTotal = getPlanTuitionActualTotal(plan),
                    tuitionNumPaymentIntervals = getNumIntervalsForPlan(plan),
                    tuitionActualPaymentPerInterval = tuitionActualTotal / tuitionNumPaymentIntervals;

                return tuitionActualPaymentPerInterval;
            $$ LANGUAGE plv8 IMMUTABLE STRICT;
        ~
    end

    def down
        execute %Q~
            DROP FUNCTION payment_amount_per_period(json[], text, json);
            DROP FUNCTION interval_for_plan(stripe_plans json[], stripe_plan_id text);
            DROP EXTENSION plv8;
        ~
    end
end
