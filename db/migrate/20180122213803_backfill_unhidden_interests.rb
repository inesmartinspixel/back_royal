class BackfillUnhiddenInterests < ActiveRecord::Migration[5.1]

    def change

        # Find CandidatePositionInterests where the associated HiringRelationship
        # is either accepted or rejected by the hiring manager, but the interest
        # has an unexpected hiring_manager_status.
        hiring_relationships =  execute(
          "SELECT candidate_id, hiring_manager_id
          FROM hiring_relationships hr
          WHERE hiring_manager_status IN ('accepted', 'rejected')
              AND (
                  SELECT cpi.id
                  FROM candidate_position_interests cpi
                      JOIN open_positions op ON cpi.open_position_id = op.id
                  WHERE cpi.candidate_id = hr.candidate_id
                      AND op.hiring_manager_id = hr.hiring_manager_id
                      AND cpi.hiring_manager_status NOT IN ('saved_for_later', 'invited', 'accepted', 'rejected', 'hidden')
                      LIMIT 1
                  ) IS NOT NULL"
        ).to_a

        # Set them all to hiring_manager_status => 'hidden'
        hiring_relationships.each do |hr|
            CandidatePositionInterest
              .joins(:open_position)
              .where(
                  candidate_id: hr['candidate_id'],
                  open_positions: {
                      hiring_manager_id: hr['hiring_manager_id']
                  })
              .each do |interest|
                  interest.update!(hiring_manager_status: 'hidden')
              end
        end

    end

end
