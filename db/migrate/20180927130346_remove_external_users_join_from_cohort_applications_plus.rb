class RemoveExternalUsersJoinFromCohortApplicationsPlus < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20180927130346)
    end

    def down
        ViewHelpers.rollback(self, 20180927130346)
    end
end
