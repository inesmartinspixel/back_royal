class BackfillAdminStatusForOpenPositionsCuration < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        # Backfill (idempotent)
        CandidatePositionInterest.where.not(hiring_manager_status: 'hidden').update_all(admin_status: 'reviewed')
        CandidatePositionInterest.where(hiring_manager_status: 'hidden').update_all(admin_status: 'not_applicable')

        # Set nullable (idempotent)
        change_column_null :candidate_position_interests, :admin_status, false
    end

    def down
        change_column_null :candidate_position_interests, :admin_status, true
    end
end
