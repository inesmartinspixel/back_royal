class CreateNotifiedRecommendedOpenPosition < ActiveRecord::Migration[5.2]
    def change
        create_table :notified_recommended_open_positions, id: :uuid do |t|
            t.datetime :created_at
            t.uuid :user_id, null: false
            t.uuid :open_position_id, null: false
        end

        add_index :notified_recommended_open_positions, [:user_id, :open_position_id], unique: true, name: :uniq_on_user_and_open_position_id
    end
end
