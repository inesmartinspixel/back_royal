module MigrateHelper

    # Do not change this method (or any other one in this file) without
    # thinking about how it affects old migrations that use it.  In most cases
    # we probably want to copy the method and create a new version
    def recreate_users_trigger_v4
        recreate_trigger_v2('users',
            %w(
                created_at
                current_sign_in_at
                current_sign_in_ip
                failed_attempts
                id
                last_seen_at
                last_sign_in_at
                last_sign_in_ip
                locked_at
                remember_created_at
                sign_in_count
                unlock_token
                updated_at
                encrypted_password
                reset_password_sent_at
                reset_password_token
                tokens
                reset_password_redirect_url
            ), %w(
                encrypted_password
                reset_password_sent_at
                remember_created_at
                reset_password_token
                tokens
                reset_password_redirect_url
            )
        )
    end

    def recreate_users_trigger_v3
        recreate_trigger_v2('users',
            %w(
                created_at
                current_sign_in_at
                current_sign_in_ip
                failed_attempts
                id
                last_seen_at
                last_sign_in_at
                last_sign_in_ip
                locked_at
                remember_created_at
                sign_in_count
                unlock_token
                updated_at
                encrypted_password
                reset_password_sent_at
                reset_password_token
                tokens
                reset_password_redirect_url
                optimizely_segments
                optimizely_referer
                optimizely_buckets
            ), %w(
                encrypted_password
                reset_password_sent_at
                remember_created_at
                reset_password_token
                tokens
                reset_password_redirect_url
                optimizely_segments
                optimizely_referer
                optimizely_buckets
            )
        )
    end

    def recreate_users_trigger_v2
        recreate_trigger_v2('users', %w(
            created_at
            current_sign_in_at
            current_sign_in_ip
            failed_attempts
            id
            last_seen_at
            last_sign_in_at
            last_sign_in_ip
            locked_at
            remember_created_at
            sign_in_count
            unlock_token
            updated_at
        ))
    end

    # Do not change this method (or any other one in this file) without
    # thinking about how it affects old migrations that use it.  In most cases
    # we probably want to copy the method and create a new version
    #
    # Version 2 of recreate_trigger_v2 added the version_editor_id and version_editor_name support
    def recreate_trigger_v2(table_name, columns_to_not_watch = nil, unwritten_columns = nil)
        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        columns_to_watch = nil
        if columns_to_not_watch
            columns_to_watch = klass.columns
                                .reject { |col| col.type == :json }
                                .reject { |col| columns_to_not_watch.include?(col.name) }
                                .map(&:name)
        end

        insert_on_update = insert_version_command_v2(klass, 'U', unwritten_columns)
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} IS DISTINCT FROM OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                DECLARE
                    _editor_id text := current_setting('myvars.editor_id', true);
                    _editor_name text := current_setting('myvars.editor_name', true);
                BEGIN

                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        #{insert_version_command_v2(klass, 'D', unwritten_columns)}
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        #{insert_version_command_v2(klass, 'I', unwritten_columns)}
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger if exists #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end

    def insert_version_command_v2(klass, operation, unwritten_columns)
        table_name = klass.table_name
        source_record = operation == 'D' ? 'OLD' : 'NEW'

        unwritten_columns ||= []

        columns_to_write = klass.columns.map(&:name) - unwritten_columns

        # since version_editor_id and version_editor_name are not always at the beginning, we need to
        # explicitly list the columns in the correct order
        "
            INSERT INTO #{table_name}_versions
            (version_id, operation, version_created_at, version_editor_id, version_editor_name, #{columns_to_write.join(', ')})
            SELECT
                uuid_generate_v4(),
                '#{operation}',
                NOW(),
                case when _editor_id = '' then null else _editor_id::uuid end,
                case when _editor_name = '' then null else _editor_name end,
                #{columns_to_write.map { |col| "#{source_record}.#{col}" }.join(", ")};
        "
    end

    # Do not change this method (or any other one in this file) without
    # thinking about how it affects old migrations that use it.  In most cases
    # we probably want to copy the method and create a new version
    #
    # Version 2 of create_versions_table_and_trigger_v2 added the version_editor_id and version_editor_name support
    def create_versions_table_and_trigger_v2(table_name, columns_to_not_watch = nil)
        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        # create the version table, including all columns from the source table
        # plus a few extras
        create_table "#{table_name}_versions", id: false, force: :cascade do |t|
            t.uuid     "version_id", null: false, default: "uuid_generate_v4()"
            t.string   "operation", limit: 1, null: false
            t.datetime "version_created_at", null: false

            puts "*** #{table_name}_versions"

            klass.column_names.each do |column_name|
                type = klass.columns_hash

                column_config = klass.columns_hash[column_name]
                meth = column_config.type.to_sym
                options = {}
                options[:limit] = column_config.limit unless column_config.limit.nil?
                options[:array] = column_config.array
                # we used to accidentally set columns to null here in the versions table
                # ignore defaults, since they should not be used in the audit table

                puts "t.#{meth}, #{column_name}, #{options.inspect}"
                t.send(meth, column_name, options)
            end
        end

        # for some reason the primary flag on create_table was not working
        execute "ALTER TABLE #{table_name}_versions ADD PRIMARY KEY (version_id);"

        # create indexes
        if klass.column_names.include?('id') && klass.column_names.include?('updated_at')
            add_index "#{table_name}_versions", [:id, :updated_at], name: "#{table_name}_versions_id_updated_at"
        elsif klass.column_names.include?('id')
            add_index "#{table_name}_versions", :id, name: "#{table_name}_versions_id"
        end

        add_editor_columns_to_versions_table(table_name)
    end

    def add_editor_columns_to_versions_table(table_name)
        add_column("#{table_name}_versions", :version_editor_id, :uuid)
        add_column("#{table_name}_versions", :version_editor_name, :text)
        if table_name == 'users'
            recreate_users_trigger_v2
        else
            recreate_trigger_v2(table_name)
        end
    end

    def rollback_editor_columns_from_versions_table(table_name)
        remove_column("#{table_name}_versions", :version_editor_id)
        remove_column("#{table_name}_versions", :version_editor_name)

        if table_name == 'users'
            recreate_users_trigger_v1
        else
            create_trigger_v1(table_name)
        end

    end




    ############### Deprecated methods ################################

    # Do not change this method (or any other one in this file) without
    # thinking about how it affects old migrations that use it.  In most cases
    # we probably want to copy the method and create a v2
    def recreate_users_trigger_v1
        create_trigger_v1('users', %w(
            created_at
            current_sign_in_at
            current_sign_in_ip
            failed_attempts
            id
            last_seen_at
            last_sign_in_at
            last_sign_in_ip
            locked_at
            remember_created_at
            sign_in_count
            unlock_token
            updated_at
        ))
    end

    # Do not change this method (or any other one in this file) without
    # thinking about how it affects old migrations that use it.  In most cases
    # we probably want to copy the method and create a v2
    def create_trigger_v1(table_name, columns_to_not_watch = nil)
        if self.version > 20181231164301 && !self.reverting?
            raise "create_trigger_v1 is deprecated.  Use recreate_trigger_v2"
        end

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        columns_to_watch = nil
        if columns_to_not_watch
            columns_to_watch = klass.columns
                                .reject { |col| col.type == :json }
                                .reject { |col| columns_to_not_watch.include?(col.name) }
                                .map(&:name)
        end

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} IS DISTINCT FROM OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end

    # Do not change this method (or any other one in this file) without
    # thinking about how it affects old migrations that use it.  In most cases
    # we probably want to copy the method and create a v2
    def create_versions_table_and_trigger_v1(table_name, columns_to_watch = nil)
        if self.version > 20190103192037 && !self.reverting?
            raise "create_trigger_v1 is deprecated.  Use recreate_trigger_v2"
        end

            klass = Class.new(ActiveRecord::Base) do
                self.table_name = table_name
            end

            klass.connection.schema_cache.clear!
            klass.reset_column_information

            # create the version table, including all columns from the source table
            # plus a few extras
            create_table "#{table_name}_versions", id: false, force: :cascade do |t|
                t.uuid     "version_id", null: false, default: "uuid_generate_v4()"
                t.string   "operation", limit: 1, null: false
                t.datetime "version_created_at", null: false

                puts "*** #{table_name}_versions"

                klass.column_names.each do |column_name|
                    type = klass.columns_hash

                    column_config = klass.columns_hash[column_name]
                    meth = column_config.type.to_sym
                    options = {}
                    options[:limit] = column_config.limit unless column_config.limit.nil?
                    options[:null] = false if column_config.null == false
                    options[:array] = column_config.array
                    # ignore defaults, since they should not be used in the audit table

                    puts "t.#{meth}, #{column_name}, #{options.inspect}"
                    t.send(meth, column_name, options)
                end
            end

            # for some reason the primary flag on create_table was not working
            execute "ALTER TABLE #{table_name}_versions ADD PRIMARY KEY (version_id);"

            # create indexes
            if klass.column_names.include?('id') && klass.column_names.include?('updated_at')
                add_index "#{table_name}_versions", [:id, :updated_at], name: "#{table_name}_versions_id_updated_at"
            elsif klass.column_names.include?('id')
                add_index "#{table_name}_versions", :id, name: "#{table_name}_versions_id"
            end

            insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
            if columns_to_watch.nil?
                update_command = insert_on_update
            else
                if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
                update_command = "
                    IF #{if_string} THEN
                    #{insert_on_update}
                    END IF;
                    "
            end

            # create the trigger
            trigger_sql = "
                CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                    BEGIN
                        --
                        -- Create a row in audit to reflect the operation performed on the table,
                        -- make use of the special variable TG_OP to work out the operation.
                        --
                        IF (TG_OP = 'DELETE') THEN
                            INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                            RETURN OLD;
                        ELSIF (TG_OP = 'UPDATE') THEN
                            #{update_command}
                            RETURN NEW;
                        ELSIF (TG_OP = 'INSERT') THEN
                            INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                            RETURN NEW;
                        END IF;
                        RETURN NULL; -- result is ignored since this is an AFTER trigger
                    END;
                $#{table_name}_version$ LANGUAGE plpgsql;

                CREATE TRIGGER #{table_name}_versions
                AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
                FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
            "
            execute(trigger_sql)
    end

end