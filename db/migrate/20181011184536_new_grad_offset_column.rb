class NewGradOffsetColumn < ActiveRecord::Migration[5.1]
    def change

        add_column :cohorts, :graduation_days_offset_from_end, :integer
        add_column :cohorts_versions, :graduation_days_offset_from_end, :integer
        add_column :curriculum_templates, :graduation_days_offset_from_end, :integer
        add_column :curriculum_templates_versions, :graduation_days_offset_from_end, :integer

        add_column :cohorts, :diploma_generation_days_offset_from_end, :integer
        add_column :cohorts_versions, :diploma_generation_days_offset_from_end, :integer
        add_column :curriculum_templates, :diploma_generation_days_offset_from_end, :integer
        add_column :curriculum_templates_versions, :diploma_generation_days_offset_from_end, :integer

    end
end
