class RemoveTriggersForCandidatePositionInterests < ActiveRecord::Migration[5.1]
    # See 20171129193122_add_triggers_for_candidate_position_interests.rb

    def up
        execute("drop trigger validate_no_candidate_position_interest_conflict_on_candidate_position_interest_change on candidate_position_interests")
        execute("drop trigger validate_no_candidate_position_interest_conflict_on_hiring_relationship_change on hiring_relationships")
        execute("drop trigger validate_no_candidate_position_interest_conflict_on_user_change on users")

        execute("drop function if exists validate_no_candidate_position_interest_conflict_on_candidate_position_interest_change()")
        execute("drop function if exists validate_no_candidate_position_interest_conflict_on_hiring_relationship_change()")
        execute("drop function if exists validate_no_candidate_position_interest_conflict_on_user_change()")
    end

    def down
        create_trigger_on_candidate_position_interest_save
        create_trigger_on_hiring_relationship_save
        create_trigger_on_user_save
    end

    def create_trigger_on_candidate_position_interest_save
        # When a candidate_position_interest is created or updated and does not have a hiring_manager_status of reviewed then
        # we need to check that there are no hiring_relationships for the hiring_manager on the associated open_position
        # nor for any of their teammates that have a status of accepted or rejected. If the trigger detects this it errors and we
        # set the status of this candidate_position_interest to hidden in an around_save callback in candidate_position_interest.rb.

        _find_conflicts = find_conflicts("SELECT distinct hiring_manager_id from open_positions where open_positions.id = NEW.open_position_id", true)

        trigger_sql = %Q~
            CREATE OR REPLACE FUNCTION validate_no_candidate_position_interest_conflict_on_candidate_position_interest_change() RETURNS TRIGGER AS $candidate_position_interest$
                BEGIN
                    IF (
                        NEW.hiring_manager_status != 'reviewed'
                        AND
                        (
                            TG_OP = 'INSERT'
                            OR OLD.hiring_manager_status = 'reviewed'
                        )
                        AND EXISTS(#{_find_conflicts})
                    ) THEN
                        RAISE EXCEPTION 'Candidate position interests conflict with hiring relationships.  hiring_relationships record "%" blocked insert or update to candidate_position_interests', (select hiring_relationship_id from (#{_find_conflicts}) a);
                    END IF;

                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $candidate_position_interest$ LANGUAGE plpgsql;

            CREATE TRIGGER validate_no_candidate_position_interest_conflict_on_candidate_position_interest_change
            AFTER INSERT OR UPDATE ON candidate_position_interests
            FOR EACH ROW EXECUTE PROCEDURE validate_no_candidate_position_interest_conflict_on_candidate_position_interest_change();
        ~

        execute(trigger_sql)
    end

    def create_trigger_on_hiring_relationship_save
        # When a hiring_relationship is created or updated with a hiring_manager_status of accepted or rejected we want
        # to check that any candidate_position_interests for the hiring manager or hiring_manager's teammates
        # are either reviewed or hidden. This trigger sets up that check and raises if it detects a candidate_position_interest
        # that is not reviewed or hidden. We handle the error in an around_save callback in hiring_relationship.rb. We do this
        # at the database level with a trigger because of concurrency issues that can arise from a hiring_relationship and
        # candidate_position_interest being updated at the exact same time.

        _find_conflicts = find_conflicts("NEW.hiring_manager_id", true)

        trigger_sql = %Q~
            CREATE OR REPLACE FUNCTION validate_no_candidate_position_interest_conflict_on_hiring_relationship_change() RETURNS TRIGGER AS $hiring_relationship$
                BEGIN
                    IF (
                        NEW.hiring_manager_status IN ('accepted', 'rejected')
                        AND (
                            TG_OP = 'INSERT'
                            OR OLD.hiring_manager_status NOT IN ('accepted', 'rejected')
                        )
                        AND EXISTS(#{_find_conflicts})
                    ) THEN
                        RAISE EXCEPTION 'Candidate position interests conflict with hiring relationships.  candidate_position_interests record "%" blocked insert or update to hiring_relationships', (select candidate_position_interest_id from (#{_find_conflicts}) a);
                    END IF;

                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $hiring_relationship$ LANGUAGE plpgsql;

            CREATE TRIGGER validate_no_candidate_position_interest_conflict_on_hiring_relationship_change
            AFTER INSERT OR UPDATE ON hiring_relationships
            FOR EACH ROW EXECUTE PROCEDURE validate_no_candidate_position_interest_conflict_on_hiring_relationship_change();
        ~

        execute(trigger_sql)
    end

    def create_trigger_on_user_save
        # When a user is changing teams we need to check across the teammates for any candidate_position_interests
        # that are not reviewed or hidden and have a candidate that is in an accepted or rejected hiring_relationship. If the
        # trigger detects this then we hide the appropriate candidate_position_interests in an around_save callback in user.rb.

        trigger_sql = %Q~
            CREATE OR REPLACE FUNCTION validate_no_candidate_position_interest_conflict_on_user_change() RETURNS TRIGGER AS $user$
                BEGIN
                    IF (
                        NEW.hiring_team_id IS NOT NULL
                        AND (
                            TG_OP = 'INSERT'
                            OR OLD.hiring_team_id IS DISTINCT FROM NEW.hiring_team_id
                        )
                        AND EXISTS(#{find_conflicts("NEW.id", false)})
                    ) THEN
                        RAISE EXCEPTION 'Candidate position interests conflict with hiring relationships';
                    END IF;

                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $user$ LANGUAGE plpgsql;

            CREATE TRIGGER validate_no_candidate_position_interest_conflict_on_user_change
            AFTER INSERT OR UPDATE ON users
            FOR EACH ROW EXECUTE PROCEDURE validate_no_candidate_position_interest_conflict_on_user_change();
        ~

        execute(trigger_sql)
    end

    def find_conflicts(hiring_manager_id_matcher, restrict_candidate_id)
        candidate_id_clause = restrict_candidate_id ? "candidate_position_interests.candidate_id = NEW.candidate_id" : "TRUE"

        # look for a case where
        # a candidate_position_interest for a hiring manager on this team
        # joined with a hiring_relationship for the same candidate and a hiring manager on this team
        # is either
        #   - rejected and the open position matches the hiring relationship's hiring manager
        #   - OR is accepted

        "
            with hiring_managers AS MATERIALIZED (
                select
                    users.id
                from users
                where
                    id = (#{hiring_manager_id_matcher})
                    or hiring_team_id = (select hiring_team_id from users where id = (#{hiring_manager_id_matcher}))
            )
            select
                candidate_position_interests.id as candidate_position_interest_id,
                hiring_relationships.id as hiring_relationship_id
            from
                candidate_position_interests
                join open_positions on
                    candidate_position_interests.open_position_id = open_positions.id
                    and open_positions.hiring_manager_id in (select id from hiring_managers)
                join hiring_relationships on hiring_relationships.candidate_id = candidate_position_interests.candidate_id
                    and hiring_relationships.hiring_manager_id in (select id from hiring_managers)
                    and hiring_relationships.hiring_manager_status in ('accepted', 'rejected')
            where
                (
                    -- if the open_position on the candidate_position_interest is for the same
                    -- hiring manager as the relationship, then both rejected and accepted are
                    -- considered conflicts
                    hiring_relationships.hiring_manager_id = open_positions.hiring_manager_id
                    OR
                    -- if the open_position on the candidate_position_interest is for a
                    -- teammate of the hiring manager on the relationship, then only accepted
                    -- is considered a conflict
                    hiring_relationships.hiring_manager_status = 'accepted'
                )
                AND candidate_position_interests.hiring_manager_status not in ('hidden', 'reviewed')
                AND #{candidate_id_clause}

            -- we only return one conflict, even if there are multiples, to include
            -- in the error message.  The order is just here to make it consistently
            -- return the same one
            order by
                candidate_position_interests.id
                , hiring_relationships.id
            limit 1
        "
    end
end
