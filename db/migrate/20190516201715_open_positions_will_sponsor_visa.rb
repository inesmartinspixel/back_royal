class OpenPositionsWillSponsorVisa < ActiveRecord::Migration[5.2]

    def up
        add_column :open_positions, :will_sponsor_visa, :boolean
        add_column :open_positions_versions, :will_sponsor_visa, :boolean
        recreate_trigger_v2(:open_positions)
    end

    def down
        remove_column :open_positions, :will_sponsor_visa, :boolean
        remove_column :open_positions_versions, :will_sponsor_visa, :boolean
        recreate_trigger_v2(:open_positions)
    end
end
