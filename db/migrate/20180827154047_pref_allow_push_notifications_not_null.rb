class PrefAllowPushNotificationsNotNull < ActiveRecord::Migration[5.1]

    disable_ddl_transaction!

    # see also: https://github.com/lynndylanhurley/devise_token_auth/issues/1079#issuecomment-363155625
    # class User < ApplicationRecord
    #     def tokens_has_json_column_type?
    #         false
    #     end
    # end

    def up
        # User.where("pref_allow_push_notifications IS NULL").in_batches.update_all(pref_allow_push_notifications: true)
        change_column_null :users, :pref_allow_push_notifications, false
    end

    def down
        change_column_null :users, :pref_allow_push_notifications, true
    end

end
