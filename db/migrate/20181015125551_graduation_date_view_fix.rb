class GraduationDateViewFix < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20181015125551)
    end

    def down
        ViewHelpers.rollback(self, 20181015125551)
    end
end