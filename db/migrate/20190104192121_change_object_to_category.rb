class ChangeObjectToCategory < ActiveRecord::Migration[5.2]
    def change
        add_column :persisted_timeline_events, :category, :text

        unless reverting?
            execute("update persisted_timeline_events set category = 'note'")
            change_column_null :persisted_timeline_events, :category, false
        end

    end
end
