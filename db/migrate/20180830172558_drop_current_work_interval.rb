class DropCurrentWorkInterval < ActiveRecord::Migration[5.1]
    def change
        remove_column :career_profile_search_helpers_2, :current_work_interval, :interval
    end
end
