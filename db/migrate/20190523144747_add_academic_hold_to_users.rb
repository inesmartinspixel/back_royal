class AddAcademicHoldToUsers < ActiveRecord::Migration[5.2]
    def up
        add_column :users, :academic_hold, :boolean
        add_column :users_versions, :academic_hold, :boolean

        change_column_default :users, :academic_hold, false

        recreate_users_trigger_v2
    end

    def down
        remove_column :users, :academic_hold
        remove_column :users_versions, :academic_hold

        recreate_users_trigger_v2
    end
end
