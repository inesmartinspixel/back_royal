class BackfillStudentNetworkFields < ActiveRecord::Migration[5.1]
  disable_ddl_transaction!

  def up
    User.in_batches.update_all(pref_student_network_privacy: "anonymous", has_seen_student_network: false)

    CareerProfile.in_batches.update_all(student_network_looking_for: [])

    # Set nullable (idempotent)
    change_column_null :users, :pref_student_network_privacy, false
    change_column_null :users, :has_seen_student_network, false
    change_column_null :career_profiles, :student_network_looking_for, false
  end

  def down
    change_column_null :users, :pref_student_network_privacy, true
    change_column_null :users, :has_seen_student_network, true
    change_column_null :career_profiles, :student_network_looking_for, true
  end

end
