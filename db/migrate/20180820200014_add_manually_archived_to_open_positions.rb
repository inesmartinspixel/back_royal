class AddManuallyArchivedToOpenPositions < ActiveRecord::Migration[5.1]

    def up
        add_column :open_positions, :manually_archived, :boolean
        add_column :open_positions_versions, :manually_archived, :boolean

        change_column_default :open_positions, :manually_archived, false
    end

    def down
        remove_column :open_positions, :manually_archived
        remove_column :open_positions_versions, :manually_archived
    end
end
