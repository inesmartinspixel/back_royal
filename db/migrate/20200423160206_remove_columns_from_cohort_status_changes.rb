class RemoveColumnsFromCohortStatusChanges < ActiveRecord::Migration[6.0]
    def change

        reversible do |dir|
            dir.up do
                execute "truncate cohort_status_changes"
            end
        end

        # in order to be able to update rows from activerecord, we need a primary key
        add_column :cohort_status_changes, :id, :uuid, default: "uuid_generate_v4()", null: false
        reversible do |dir|
            dir.up do
                execute "ALTER TABLE cohort_status_changes ADD PRIMARY KEY (id);"
            end
        end
        remove_column :cohort_status_changes, :processed_up_to, :timestamp
        remove_column :cohort_status_changes, :foundations_complete, :boolean
        remove_column :cohort_status_changes, :showed_up, :boolean
        remove_column :cohort_status_changes, :past_enrollment_deadline, :boolean
        remove_column :cohort_status_changes, :accepted_at, :timestamp
        remove_column :cohort_status_changes, :enrolled, :boolean
        remove_column :cohort_status_changes, :enrolled_late, :boolean

        add_column :cohort_status_changes, :was_enrolled, :boolean, null: false

    end
end