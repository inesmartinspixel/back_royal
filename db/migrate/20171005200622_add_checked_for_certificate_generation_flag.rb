class AddCheckedForCertificateGenerationFlag < ActiveRecord::Migration[5.1]
    def change
        add_column :lesson_progress, :last_checked_for_cert_generation, :timestamp
    end
end
