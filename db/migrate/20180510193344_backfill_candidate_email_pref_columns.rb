class BackfillCandidateEmailPrefColumns < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        # Backfill (idempotent)
        User.where(:notify_email_newsletter => true).in_batches.update_all(notify_candidate_positions_technical: true, notify_candidate_positions_business: true)
        User.where(:notify_email_newsletter => false).in_batches.update_all(notify_candidate_positions_technical: false, notify_candidate_positions_business: false)

        # Set nullable (idempotent)
        change_column_null :users, :notify_candidate_positions_technical, false
        change_column_null :users, :notify_candidate_positions_business, false
    end

    def down
        change_column_null :users, :notify_candidate_positions_technical, true
        change_column_null :users, :notify_candidate_positions_business, true
    end
end
