class SupportAdvancedStudentNetworkFilters < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20180418143340)

        # used for the alumni filter in the student network
        add_index :cohort_applications, :graduation_status, :where => "status = 'accepted'", :name => "partial_grad_status_on_applications"
    end

    def down
        # used for the alumni filter in the student network
        remove_index :cohort_applications, name: "partial_grad_status_on_applications"

        ViewHelpers.rollback(self, 20180418143340)
    end
end
