class AddDegreeColumnsToEducationExperience < ActiveRecord::Migration[5.1]
  def up
    add_column :education_experiences, :degree_program, :boolean
    add_column :education_experiences, :will_not_complete, :boolean
    add_column :education_experiences_versions, :degree_program, :boolean
    add_column :education_experiences_versions, :will_not_complete, :boolean

    change_column_default :education_experiences, :degree_program, true
  end

  def down
    remove_column :education_experiences, :degree_program, :boolean
    remove_column :education_experiences, :will_not_complete, :boolean
    remove_column :education_experiences_versions, :degree_program, :boolean
    remove_column :education_experiences_versions, :will_not_complete, :boolean
  end
end
