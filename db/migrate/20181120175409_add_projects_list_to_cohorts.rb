class AddProjectsListToCohorts < ActiveRecord::Migration[5.2]
    def change

        add_column :cohorts, :learner_project_ids, :uuid, :array => true
        add_column :cohorts_versions, :learner_project_ids, :uuid, :array => true
        add_column :curriculum_templates, :learner_project_ids, :uuid, :array => true
        add_column :curriculum_templates_versions, :learner_project_ids, :uuid, :array => true

        change_column_default :cohorts, :learner_project_ids, []
        change_column_default :curriculum_templates, :learner_project_ids, []

    end
end