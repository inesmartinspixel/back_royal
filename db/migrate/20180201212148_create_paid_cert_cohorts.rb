class CreatePaidCertCohorts < ActiveRecord::Migration[5.1]
    def up

        [
            ['paid_cert_data_analytics', "Data Analytics", "Business decisions are increasingly driven by data analytics. Join one of today’s fastest growing and highest paid professions."],
            ['paid_cert_competitive_strategy',"Business Strategy: Achieving Competitive Advantage","Discover how to create and sustain long-term competitive advantage and pave the way for profitability and growth."],
            ['paid_cert_essentials_of_management',"Essentials of Management","Create lasting value in the marketplace by building and motivating a high-performing team."],
            ['paid_cert_financial_accounting',"Financial and Managerial Accounting","Prepare and analyze critical financial information for a range of business initiatives."],
            ['paid_cert_marketing_basics',"Marketing & Pricing Strategy","Learn to align marketing and business strategy to make better decisions about your brand, market, and competitors."],

            # not launching these yet
            ['paid_cert_entrepreneurial_strategy',"Entrepreneurial Strategy & Management","Entrepreneurship requires more than an idea. Learn to identify opportunities, develop a strategy, and lead your teams to success."],
            ['paid_cert_operations_management','Operations Management Certificate', '']
        ].each do |attrs|
            program_type, title, description = attrs

            exists = execute("select id from cohorts where program_type = '#{program_type}'").to_a.first

            unless exists

                id = execute("
                    insert into cohorts (title, description, program_type, created_at, updated_at, start_date, end_date,name) VALUES
                    (
                        '#{title}',
                        '#{description}',
                        '#{program_type}',
                        now(),
                        now(),
                        '2018-01-01 09:00:00',
                        '2018-01-01 09:00:00',
                        '#{program_type.upcase}'
                    ) returning id
                ").to_a[0]['id']

                next if program_type == 'paid_cert_entrepreneurial_strategy'
                next if program_type == 'paid_cert_operations_management'

                id = execute("
                    insert into cohort_promotions (cohort_id, updated_at, created_at) VALUES
                    (
                        '#{id}',
                        now(),
                        now()
                    )
                ")
            end
        end
    end
end
