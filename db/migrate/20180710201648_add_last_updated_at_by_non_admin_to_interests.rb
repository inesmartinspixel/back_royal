class AddLastUpdatedAtByNonAdminToInterests < ActiveRecord::Migration[5.1]

    def up
        add_column :candidate_position_interests, :last_updated_at_by_non_admin, :datetime
        add_column :candidate_position_interests_versions, :last_updated_at_by_non_admin, :datetime
    end

    def down
        remove_column :candidate_position_interests, :last_updated_at_by_non_admin
        remove_column :candidate_position_interests_versions, :last_updated_at_by_non_admin
    end

end
