class DropOptionsVersions < ActiveRecord::Migration[5.1]
    def up

        ["career_profiles_skills_options","career_profiles_awards_and_interests_options"].each do |table_name|
            execute "drop trigger #{table_name}_versions on #{table_name}"
            drop_table "#{table_name}_versions"
        end
    end

    def down
        # no-op
    end
end
