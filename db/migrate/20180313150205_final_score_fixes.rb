class FinalScoreFixes < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20180313150205)
    end

    def down
        ViewHelpers.rollback(self, 20180313150205)
    end
end