class MigrateViewsToUsePlaylistCollections < ActiveRecord::Migration[5.2]

    def up
        ViewHelpers.migrate(self, 20190409135750)
    end

    def down
        ViewHelpers.migrate(self, 20190409135750)
    end
end
