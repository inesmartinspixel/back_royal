class AddIndexToCohortApplications < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        execute "CREATE INDEX CONCURRENTLY user_id_and_applied_at_on_coh_applications ON cohort_applications (user_id, applied_at desc)"
    end

    def down
        execute "DROP INDEX user_id_and_applied_at_on_coh_applications"
    end
end