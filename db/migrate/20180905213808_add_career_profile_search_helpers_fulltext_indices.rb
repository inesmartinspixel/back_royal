class AddCareerProfileSearchHelpersFulltextIndices < ActiveRecord::Migration[5.1]

    disable_ddl_transaction!

    def change
        add_index :career_profile_search_helpers_2, :skills_vector, using: :gin, algorithm: :concurrently
        add_index :career_profile_search_helpers_2, :student_network_interests_vector, using: :gin, algorithm: :concurrently, name: 'idx_cp_search_helpers_2_on_student_network_interests_vector' # too long otherwise
    end
end
