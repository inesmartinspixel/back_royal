class BackfillEmploymentTypeForWorkExperiences < ActiveRecord::Migration[5.1]
    # we are disabling transactions so that we don't update every row in the
    # table in a single transaction.  In order for this to be ok, we need
    # to make sure everything in this migration is idempotent
    disable_ddl_transaction!

    def up
        CareerProfile::WorkExperience.in_batches.update_all employment_type: 'full_time'
        change_column_null :work_experiences, :employment_type, false
    end

    def down
        change_column_null :work_experiences, :employment_type, true
    end
end
