class CreatePlayerLessonSessions < ActiveRecord::Migration[5.1]
    def change
        create_table :player_lesson_sessions, id: :uuid do |t|
            t.uuid          "session_id",                      null: false
            t.uuid          "user_id",                      null: false
            t.uuid          "lesson_id",                    null: false
            t.uuid          "lesson_stream_id",            null: true
            t.timestamp     "started_at",                   null: false
            t.timestamp     "last_lesson_activity_time",   null: false
            t.float         "total_lesson_time",           null: false
            t.boolean       "started_in_this_session",      null: false
            t.boolean       "completed_in_this_session",      null: false
            t.uuid          "lesson_locale_pack_id",            null: false
            t.uuid          "lesson_stream_locale_pack_id",      null: true
            t.text          "lesson_locale",                    null: false
            t.text          "client_type",                   null: false
        end

        add_index(:player_lesson_sessions, [:user_id, :lesson_id, :lesson_stream_id, :session_id], unique: true, :name => "unique_on_player_lesson_sessions")
        add_index(:player_lesson_sessions, :started_at)
        add_index(:player_lesson_sessions, :last_lesson_activity_time, order: {last_lesson_activity_time: 'desc'}) # needed in write_new_records

        reversible do |dir|
            dir.up do
                ViewHelpers.migrate(self, 20180111202937)
                execute %Q~
                    CREATE FUNCTION client_type(client text, os_name text) RETURNS TEXT AS
                    $func$
                    BEGIN

                        RETURN CASE
                            WHEN client in ('ios', 'android') THEN 'mobile_app'
                            WHEN os_name ilike '%windows phone%'
                                    OR os_name ilike '%ios%'
                                    OR os_name ilike '%android%'
                                THEN
                                    'mobile_web'
                            WHEN os_name ilike '%windows%'
                                    OR os_name ilike '%mac os%'
                                    OR os_name ilike '%chrome os%'
                                THEN
                                    'desktop'
                            ELSE 'unknown'
                        END;

                    END
                    $func$ LANGUAGE plpgsql;
                ~
            end

            dir.down do
                ViewHelpers.rollback(self, 20180111202937)

                execute %Q~
                    DROP FUNCTION client_type(client text, os_name text);
                ~
            end
        end
    end
end



