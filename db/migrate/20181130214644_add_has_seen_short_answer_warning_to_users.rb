class AddHasSeenShortAnswerWarningToUsers < ActiveRecord::Migration[5.2]

  def up
        add_column :users, :has_seen_short_answer_warning, :boolean
        add_column :users_versions, :has_seen_short_answer_warning, :boolean

        change_column_default :users, :has_seen_short_answer_warning, false

        recreate_users_trigger_v1
    end

    def down
        remove_column :users, :has_seen_short_answer_warning
        remove_column :users_versions, :has_seen_short_answer_warning

        recreate_users_trigger_v1
    end
end
