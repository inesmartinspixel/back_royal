
class AddEditorToLessonStreamsVersions < ActiveRecord::Migration[5.2]

    def up
        add_editor_columns_to_versions_table("lesson_streams")
    end

    def down
        rollback_editor_columns_from_versions_table("lesson_streams")
    end
end
