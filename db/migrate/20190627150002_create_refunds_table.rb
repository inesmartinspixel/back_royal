class CreateRefundsTable < ActiveRecord::Migration[5.2]
    def up
        create_table :refunds, id: :uuid do |t|

            t.timestamps
            t.references :billing_transaction, type: :uuid, foreign_key: true, null: false
            t.text :provider, null: false
            t.text :provider_transaction_id, null: false
            t.timestamp :refund_time, null: false
            t.float :amount, null: false

        end

        add_index :refunds, [:provider, :provider_transaction_id], unique: true, :name => :uniq_on_refunds_provider_and_transaction_id
        create_versions_table_and_trigger_v2(:refunds)
    end

    def down
        drop_table :refunds_versions
        drop_table :refunds
    end
end
