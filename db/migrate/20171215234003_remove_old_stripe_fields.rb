class RemoveOldStripeFields < ActiveRecord::Migration[5.1]
    def change

        # Cohorts

        remove_column :cohorts, :stripe_plan_id, :text
        remove_column :cohorts, :stripe_plan_amount, :integer

        remove_column :cohorts_versions, :stripe_plan_id, :text
        remove_column :cohorts_versions, :stripe_plan_amount, :integer

        # Curriculum Templates

        remove_column :curriculum_templates, :stripe_plan_id, :text
        remove_column :curriculum_templates, :stripe_plan_amount, :integer

        remove_column :curriculum_templates_versions, :stripe_plan_id, :text
        remove_column :curriculum_templates_versions, :stripe_plan_amount, :integer

        # Cohort Applications

        remove_column :cohort_applications, :stripe_coupon_id, :text
        remove_column :cohort_applications, :stripe_coupon_amount_off, :integer
        remove_column :cohort_applications, :stripe_coupon_percent_off, :integer

        remove_column :cohort_applications_versions, :stripe_coupon_id, :text
        remove_column :cohort_applications_versions, :stripe_coupon_amount_off, :integer
        remove_column :cohort_applications_versions, :stripe_coupon_percent_off, :integer

    end
end
