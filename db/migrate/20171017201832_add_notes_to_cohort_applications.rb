class AddNotesToCohortApplications < ActiveRecord::Migration[5.1]
    def change
        add_column :cohort_applications, :notes, :text
        add_column :cohort_applications_versions, :notes, :text
    end
end
