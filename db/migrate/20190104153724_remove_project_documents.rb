class RemoveProjectDocuments < ActiveRecord::Migration[5.2]

    def up
        remove_column :cohorts, :project_documents
        remove_column :cohorts_versions, :project_documents
        remove_column :curriculum_templates, :project_documents
        remove_column :curriculum_templates_versions, :project_documents

        recreate_trigger_v2(:cohorts)
        recreate_trigger_v2(:curriculum_templates)
    end

    def down
        add_column :cohorts, :project_documents, :json, array: true
        add_column :cohorts_versions, :project_documents, :json, array: true
        add_column :curriculum_templates, :project_documents, :json, array: true
        add_column :curriculum_templates_versions, :project_documents, :json, array: true

        recreate_trigger_v2(:cohorts)
        recreate_trigger_v2(:curriculum_templates)
    end
end
