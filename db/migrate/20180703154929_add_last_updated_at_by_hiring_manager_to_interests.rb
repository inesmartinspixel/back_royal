class AddLastUpdatedAtByHiringManagerToInterests < ActiveRecord::Migration[5.1]
    def change
        add_column :candidate_position_interests, :last_updated_at_by_hiring_manager, :datetime
        add_column :candidate_position_interests_versions, :last_updated_at_by_hiring_manager, :datetime
    end
end
