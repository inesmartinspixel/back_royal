class AddUrlToPrivateDocumentsTables < ActiveRecord::Migration[6.0]
    def change
        [
            :s3_english_language_proficiency_documents,
            :s3_identification_assets,
            :s3_transcript_assets,
            :signable_documents
        ].each do |table|
            add_column table, :persisted_path, :string
        end
    end
end
