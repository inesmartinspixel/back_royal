class BackfillLocationForOpenPositions < ActiveRecord::Migration[5.2]
  disable_ddl_transaction!

    def up
        # batch update open positions outside of a transaction to prevent
        # large table locks
        OpenPosition.select('id')
            .where(
                location: nil
            )
            .where("place_details->>'lat' is not null")
            .where("place_details->>'lng' is not null")
            .find_in_batches do |batch|

                id_string = batch.map(&:id).map { |id| "'#{id}'" }.join(',')
                ActiveRecord::Base.connection.execute %Q~
                    update open_positions
                    set location = ST_GeographyFromText('POINT(' || (place_details->>'lng')::text || ' ' || (place_details->>'lat')::text || ')')
                    where id in (#{id_string})
                ~
        end
    end
end
