class CareerProfileSearchHelpers < ActiveRecord::Migration[5.1]
    def change

        # this index is used to help the hiring_relationships_subquery in
        # CareerProfilesController#default_prioritization
        add_index :hiring_relationships, [:hiring_manager_status, :candidate_id], :name => "hm_status_and_candidate_on_hiring_relationships"

        reversible do |dir|
            dir.up do
                ViewHelpers.migrate(self, 20180111202937)
                execute %Q~
                    CREATE FUNCTION count_estimate(query text) RETURNS INTEGER AS
                    $func$
                    DECLARE
                        rec   record;
                        ROWS  INTEGER;
                    BEGIN
                        FOR rec IN EXECUTE 'EXPLAIN ' || query LOOP
                            ROWS := SUBSTRING(rec."QUERY PLAN" FROM ' rows=([[:digit:]]+)');
                            EXIT WHEN ROWS IS NOT NULL;
                        END LOOP;

                        RETURN ROWS;
                    END
                    $func$ LANGUAGE plpgsql;
                ~
            end

            dir.down do
                ViewHelpers.rollback(self, 20180111202937)

                execute %Q~
                    DROP FUNCTION count_estimate(query text);
                ~
            end
        end
    end
end
