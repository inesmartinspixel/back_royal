class CreateUserDeletions < ActiveRecord::Migration[5.2]

    def up
        create_table :users_deletions, id: :uuid do |t|
            t.timestamps
        end
        add_column :users_deletions, :user_id, :uuid, :null => false
        add_column :users_deletions, :psuedo_anonymized_user_email, :text, :null => false
        add_column :users_deletions, :admin_id, :uuid
        add_column :users_deletions, :admin_name, :text

        execute (psuedo_anon_email_sql)
        execute (deletion_trigger_sql)
    end

    def down
        drop_table :users_deletions
        execute %Q~
            DROP FUNCTION psuedo_anonymized_user_email;
            DROP TRIGGER users_deletions on users;
        ~
    end

    def psuedo_anon_email_sql
        "
            CREATE OR REPLACE FUNCTION psuedo_anonymized_user_email(email text)
            RETURNS text AS $$

                function psuedo_anonymize_user_email(email) {
                    return email.replace(/^(.)(.*)(.@.)(.*)(\\..*$)/, function(match, $1, $2, $3, $4, $5) {
                        return $1 + $2.replace(/./g, '*') + $3 + $4.replace(/./g, '*') + $5;
                    });
                }
                return psuedo_anonymize_user_email(email)

            $$ LANGUAGE plv8 IMMUTABLE STRICT;
        "
    end

    def deletion_trigger_sql
        "
            CREATE OR REPLACE FUNCTION process_users_deletions() RETURNS TRIGGER AS $users_deletion$
            DECLARE
                _editor_id text := current_setting('myvars.editor_id', true);
                _editor_name text := current_setting('myvars.editor_name', true);
            BEGIN
                IF (TG_OP = 'DELETE') THEN
                    #{insert_deletion_sql}
                    RETURN OLD;
                END IF;
                RETURN NULL; -- result is ignored since this is an AFTER trigger
            END;
            $users_deletion$ LANGUAGE plpgsql;

            CREATE TRIGGER users_deletions
            AFTER DELETE ON users
            FOR EACH ROW EXECUTE PROCEDURE process_users_deletions();
        "
    end

    def insert_deletion_sql
        "
            INSERT INTO users_deletions
            (id, created_at, updated_at, user_id, psuedo_anonymized_user_email, admin_id, admin_name)
            SELECT
                uuid_generate_v4(),
                NOW(),
                NOW(),
                OLD.id,
                psuedo_anonymized_user_email(OLD.email),
                case when _editor_id = '' then null else _editor_id::uuid end,
                case when _editor_name = '' then null else _editor_name end;
        "
    end

end
