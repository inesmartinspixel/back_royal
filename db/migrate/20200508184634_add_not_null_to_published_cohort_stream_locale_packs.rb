class AddNotNullToPublishedCohortStreamLocalePacks < ActiveRecord::Migration[6.0]
    def change

        change_column_null :published_cohort_stream_locale_packs_temp, :created_at, false
        change_column_null :published_cohort_stream_locale_packs_temp, :cohort_id, false
        change_column_null :published_cohort_stream_locale_packs_temp, :cohort_name, false
        change_column_null :published_cohort_stream_locale_packs_temp, :foundations, false
        change_column_null :published_cohort_stream_locale_packs_temp, :required, false
        change_column_null :published_cohort_stream_locale_packs_temp, :specialization, false
        change_column_null :published_cohort_stream_locale_packs_temp, :elective, false
        change_column_null :published_cohort_stream_locale_packs_temp, :exam, false
        change_column_null :published_cohort_stream_locale_packs_temp, :stream_title, false
        change_column_null :published_cohort_stream_locale_packs_temp, :stream_locale_pack_id, false
        change_column_null :published_cohort_stream_locale_packs_temp, :stream_locales, false
        change_column_null :published_cohort_stream_locale_packs_temp, :playlist_count, false
    end
end
