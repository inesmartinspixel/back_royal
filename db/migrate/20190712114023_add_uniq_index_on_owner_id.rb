class AddUniqIndexOnOwnerId < ActiveRecord::Migration[5.2]
    def change
        add_index :hiring_teams, :owner_id, unique: true
    end
end
