class FixOpenPositionsTrigger < ActiveRecord::Migration[5.2]

    def up
        # this is only necessary because AddShortDescriptionToOpenPositions ran out of order on staging,
        # so recreate_trigger_v2 was not called after the column was added.  When migrations run in order,
        # this one is not necessary.  But, this one doesn't hurt anythign in any case.
        recreate_trigger_v2 "open_positions"
    end
end
