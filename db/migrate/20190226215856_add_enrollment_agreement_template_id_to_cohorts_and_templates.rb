
class AddEnrollmentAgreementTemplateIdToCohortsAndTemplates < ActiveRecord::Migration[5.2]
  def up

      add_column :cohorts, :enrollment_agreement_template_id, :text
      add_column :cohorts_versions, :enrollment_agreement_template_id, :text
      recreate_trigger_v2 "cohorts"

      add_column :curriculum_templates, :enrollment_agreement_template_id, :text
      add_column :curriculum_templates_versions, :enrollment_agreement_template_id, :text
      recreate_trigger_v2 "curriculum_templates"

  end

  def down

    remove_column :cohorts, :enrollment_agreement_template_id
    remove_column :cohorts_versions, :enrollment_agreement_template_id
    recreate_trigger_v2 "cohorts"

    remove_column :curriculum_templates, :enrollment_agreement_template_id
    remove_column :curriculum_templates_versions, :enrollment_agreement_template_id
    recreate_trigger_v2 "curriculum_templates"

  end
end
