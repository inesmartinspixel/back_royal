class AddEnglishLanguageProficiencyColumnsToUser < ActiveRecord::Migration[5.1]

    def up
        # Not all users are required to upload English language proficiency documents
        # e.g. a user that is a native_english_speaker is not required to upload such
        # documents and neither is a user that has earned an accredited degree from a
        # university taught in English (earned_accredited_degree_in_english) despite
        # not being a native_english_speaker. As such, we're not going to change this
        # column to have a default value.
        add_column :users, :english_language_proficiency_comments, :text
        add_column :users, :english_language_proficiency_documents_type, :text
        add_column :users, :english_language_proficiency_documents_approved, :boolean

        add_column :users_versions, :english_language_proficiency_comments, :text
        add_column :users_versions, :english_language_proficiency_documents_type, :text
        add_column :users_versions, :english_language_proficiency_documents_approved, :boolean

        recreate_users_trigger_v1
    end

    def down
        remove_column :users, :english_language_proficiency_comments
        remove_column :users, :english_language_proficiency_documents_type
        remove_column :users, :english_language_proficiency_documents_approved

        remove_column :users_versions, :english_language_proficiency_comments
        remove_column :users_versions, :english_language_proficiency_documents_type
        remove_column :users_versions, :english_language_proficiency_documents_approved

        recreate_users_trigger_v1
    end
end
