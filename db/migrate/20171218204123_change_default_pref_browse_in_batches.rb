class ChangeDefaultPrefBrowseInBatches < ActiveRecord::Migration[5.1]
  def up
    change_column_default :users, :pref_browse_in_batches, true
  end

  def down
    change_column_default :users, :pref_browse_in_batches, false
  end
end
