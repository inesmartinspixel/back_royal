class DropUnusedTables < ActiveRecord::Migration[5.1]

    def up
        # During anonymization testing, CDB dumps were produced with these changes already baked in
        # which is why we've added the IF EXISTS clause here.
        # These `*_versions` tables did not appear to have any corresponding primary tables.
        # The `applications` table was not readable by `ebroot` and no references could be found.
        execute "DROP TABLE IF EXISTS access_groups_lesson_streams_versions"
        execute "DROP TABLE IF EXISTS access_groups_playlists_versions"
        execute "DROP TABLE IF EXISTS lesson_streams_users_versions"
        execute "DROP TABLE IF EXISTS applications"
    end

    def down

        execute "CREATE TABLE public.access_groups_lesson_streams_versions (
                    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
                    operation character varying(1) NOT NULL,
                    version_created_at timestamp without time zone NOT NULL,
                    id uuid NOT NULL,
                    access_group_id uuid,
                    lesson_stream_id uuid
                );"

        execute "CREATE TABLE public.access_groups_playlists_versions (
                    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
                    operation character varying(1) NOT NULL,
                    version_created_at timestamp without time zone NOT NULL,
                    id uuid NOT NULL,
                    access_group_id uuid,
                    playlist_id uuid
                );"

        execute "CREATE TABLE public.lesson_streams_users_versions (
                    version_id uuid DEFAULT public.uuid_generate_v4(),
                    operation character varying(1) NOT NULL,
                    version_created_at timestamp without time zone NOT NULL,
                    user_id uuid NOT NULL,
                    lesson_stream_id uuid NOT NULL
                );"

        execute "CREATE TABLE public.applications (
                    cohort_id uuid,
                    status text
                );"

    end
end
