class AddSubscriptionsProductId < ActiveRecord::Migration[5.1]
    def up
        add_column :subscriptions, :stripe_product_id, :text
        add_column :subscriptions_versions, :stripe_product_id, :text
    end

    def down
        remove_column :subscriptions, :stripe_product_id
        remove_column :subscriptions_versions, :stripe_product_id
    end
end
