class CreatePersistedTimelineEvents < ActiveRecord::Migration[5.2]
    def change
        create_table :persisted_timeline_events, :id => :uuid do |t|
            t.timestamps
            t.timestamp :time       , null: false
            t.text :event           , null: false
            t.float :secondary_sort , null: true
            t.text :text            , null: true
            t.text :subtext         , null: true
            t.uuid :editor_id       , null: true
            t.text :editor_name     , null: true
            t.text :labels          , null: false, default: [], array: true
        end

        create_table :persisted_timeline_events_users do |t|
            t.timestamps
            t.references :persisted_timeline_event, index: { name: :p_timeline_events_user_join}, type: :uuid
            t.references :user, type: :uuid
        end

    end
end
