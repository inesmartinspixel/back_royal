# https://stackoverflow.com/questions/2913368/sorting-array-elements
class AddArraySortFunction < ActiveRecord::Migration[6.0]
    def up
        execute "
            CREATE OR REPLACE FUNCTION array_sort (ANYARRAY)
            RETURNS ANYARRAY LANGUAGE SQL
            AS $$
            SELECT ARRAY(SELECT unnest($1) ORDER BY 1)
            $$;
        "
    end

    def down
        execute "DROP FUNCTION array_sort"
    end
end
