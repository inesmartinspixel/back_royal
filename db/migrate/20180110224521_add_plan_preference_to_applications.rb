class AddPlanPreferenceToApplications < ActiveRecord::Migration[5.1]
    def up
        add_column :cohort_applications, :stripe_plan_id, :text
        add_column :cohort_applications_versions, :stripe_plan_id, :text

        # Update plans to emba_800 where there are charges already, or there are non-standard required payments
        execute "UPDATE cohort_applications SET stripe_plan_id = 'emba_800' where num_charged_payments IS NOT NULL OR total_num_required_stripe_payments < 12"

        # New standard, don't pre-populate since it's not reflective of the user's choice
        execute "UPDATE cohort_applications SET total_num_required_stripe_payments = NULL where num_charged_payments IS NULL AND stripe_plan_id IS NULL"
    end

    def down
        remove_column :cohort_applications, :stripe_plan_id, :text
        remove_column :cohort_applications_versions, :stripe_plan_id, :text
    end
end
