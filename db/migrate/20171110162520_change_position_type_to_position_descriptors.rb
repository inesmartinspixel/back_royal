class ChangePositionTypeToPositionDescriptors < ActiveRecord::Migration[5.1]
    def up
        remove_column :open_positions, :position_type
        add_column :open_positions, :position_descriptors, :text, :array => true, :default => []

        remove_column :open_positions_versions, :position_type
        add_column :open_positions_versions, :position_descriptors, :text, :array => true, :default => []

        change_column_default(:open_positions, :archived, false)
        change_column_default(:open_positions, :featured, false)

        execute(%Q~
            update open_positions set archived=false where archived is null;
            update open_positions set featured=false where featured is null;
        ~)
    end

    def down
        remove_column :open_positions, :position_descriptors
        add_column :open_positions, :position_type, :text

        remove_column :open_positions_versions, :position_descriptors
        add_column :open_positions_versions, :position_type, :text

        change_column_default(:open_positions, :archived, nil)
        change_column_default(:open_positions, :featured, nil)
    end
end
