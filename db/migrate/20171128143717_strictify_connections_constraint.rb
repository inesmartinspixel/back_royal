class StrictifyConnectionsConstraint < ActiveRecord::Migration[5.1]
    def up

        create_trigger_validate_hiring_teams_on_relationship_change(:up)
        create_trigger_validate_hiring_teams_on_team_change(:up)

        # FIXME: remove this once it has run on prod
        HiringRelationship.resolve_existing_team_conflicts
    end

    def down

        # these methods have a switch on reverting?, so they do the
        # right thing in the up and the down
        create_trigger_validate_hiring_teams_on_relationship_change(:down)
        create_trigger_validate_hiring_teams_on_team_change(:down)
    end

    def create_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end

    def find_dupes(dir, hiring_team_matcher)
        if dir == :down
            find_dupes_down(hiring_team_matcher)
        else
            find_dupes_up(hiring_team_matcher)
        end
    end

    def find_dupes_down(hiring_team_matcher)
        "
            WITH pairs AS MATERIALIZED (
                SELECT
                    candidate_id,
                    hiring_team_id
                FROM
                    hiring_relationships
                    JOIN users hiring_managers
                        ON hiring_managers.id = hiring_manager_id
                WHERE
                    hiring_manager_status = 'accepted'
                    AND hiring_managers.hiring_team_id = (#{hiring_team_matcher})
            )
            SELECT
                candidate_id
                , count(*)
            FROM pairs
            GROUP BY candidate_id
            HAVING COUNT(*) > 1
        "
    end

    def find_dupes_up(hiring_team_matcher)
        "
            WITH pairs AS MATERIALIZED (
                SELECT
                    candidate_id,
                    hiring_team_id,
                    hiring_manager_status
                FROM
                    hiring_relationships
                    JOIN users hiring_managers
                        ON hiring_managers.id = hiring_manager_id
                WHERE
                    hiring_manager_status != 'hidden'
                    AND hiring_managers.hiring_team_id = (#{hiring_team_matcher})
            )
            SELECT
                candidate_id
                , count(*)
            FROM pairs
            GROUP BY candidate_id
            HAVING
                COUNT(*) > 1
                AND COUNT(case when hiring_manager_status = 'accepted' then true else null end ) > 0
            LIMIT 1
        "
    end

    def create_trigger_validate_hiring_teams_on_relationship_change(dir)
        find_dupes = find_dupes(dir, "select hiring_team_id from users where id = NEW.hiring_manager_id")

        hiring_manager_status_clause = dir == :up ? "NEW.hiring_manager_status != 'hidden'" : "NEW.hiring_manager_status = 'accepted'"

        # Any time a relationship is changed to 'accepted' or it is already 'accepted' and the hiring manager changes,
        # check that there are no dupes
        trigger_sql = %Q~
            CREATE OR REPLACE FUNCTION validate_hiring_teams_on_relationship_change() RETURNS TRIGGER AS $hiring_relationship$
                BEGIN
                    IF (
                        #{hiring_manager_status_clause}
                        AND (
                            TG_OP = 'INSERT'
                            OR OLD.hiring_manager_status != NEW.hiring_manager_status
                            OR OLD.hiring_manager_id != NEW.hiring_manager_id
                        )
                        AND EXISTS(#{find_dupes})
                    ) THEN
                        RAISE EXCEPTION 'Multiple connections to candidate "%"', (select candidate_id from (#{find_dupes}) a);
                    END IF;

                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $hiring_relationship$ LANGUAGE plpgsql;

            DROP TRIGGER IF EXISTS validate_hiring_teams_on_relationship_change on hiring_relationships;
            CREATE TRIGGER validate_hiring_teams_on_relationship_change
            AFTER INSERT OR UPDATE ON hiring_relationships
            FOR EACH ROW EXECUTE PROCEDURE validate_hiring_teams_on_relationship_change();
        ~

        execute(trigger_sql)
    end

    def create_trigger_validate_hiring_teams_on_team_change(dir)
        find_dupes = find_dupes(dir, "NEW.hiring_team_id")

        # any time that a user's hiring_team changes, check that there are no dupes
        trigger_sql = %Q~
            CREATE OR REPLACE FUNCTION validate_hiring_teams_on_team_change() RETURNS TRIGGER AS $user$
                BEGIN
                    IF (
                        NEW.hiring_team_id IS NOT NULL
                        AND (
                            TG_OP = 'INSERT'
                            OR OLD.hiring_team_id IS NULL
                            OR OLD.hiring_team_id != NEW.hiring_team_id
                        )
                        AND EXISTS(#{find_dupes})
                    ) THEN
                        RAISE EXCEPTION 'Multiple connections to candidate "%"', (select candidate_id from (#{find_dupes}) a);
                    END IF;

                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $user$ LANGUAGE plpgsql;


            DROP TRIGGER IF EXISTS validate_hiring_teams_on_team_change on users;
            CREATE TRIGGER validate_hiring_teams_on_team_change
            AFTER INSERT OR UPDATE ON users
            FOR EACH ROW EXECUTE PROCEDURE validate_hiring_teams_on_team_change();
        ~

        execute(trigger_sql)
    end

    def drop_all_for_table(table_name)
        drop_versions_table_and_trigger table_name
        drop_table table_name
    end

    def drop_versions_table_and_trigger(table_name)
        drop_table "#{table_name}_versions"
        execute "DROP TRIGGER #{table_name}_versions ON #{table_name}"
    end

    def create_versions_table_and_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.connection.schema_cache.clear!
        klass.reset_column_information

        # create the version table, including all columns from the source table
        # plus a few extras
        create_table "#{table_name}_versions", id: false, force: :cascade do |t|
            t.uuid     "version_id", null: false, default: "uuid_generate_v4()"
            t.string   "operation", limit: 1, null: false
            t.datetime "version_created_at", null: false

            puts "*** #{table_name}_versions"

            klass.column_names.each do |column_name|
                type = klass.columns_hash

                column_config = klass.columns_hash[column_name]
                meth = column_config.type.to_sym
                options = {}
                options[:limit] = column_config.limit unless column_config.limit.nil?
                options[:null] = false if column_config.null == false
                options[:array] = column_config.array
                # ignore defaults, since they should not be used in the audit table

                puts "t.#{meth}, #{column_name}, #{options.inspect}"
                t.send(meth, column_name, options)
            end
        end

        # for some reason the primary flag on create_table was not working
        execute "ALTER TABLE #{table_name}_versions ADD PRIMARY KEY (version_id);"

        # create indexes
        if klass.column_names.include?('id') && klass.column_names.include?('updated_at')
            add_index "#{table_name}_versions", [:id, :updated_at]
        elsif klass.column_names.include?('id')
            add_index "#{table_name}_versions", :id
        end

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "
        execute(trigger_sql)
    end
end
