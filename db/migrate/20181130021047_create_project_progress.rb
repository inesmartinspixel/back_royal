class CreateProjectProgress < ActiveRecord::Migration[5.2]
    def change
        create_table :project_progress, :id => :uuid do |t|
            t.timestamps
            t.uuid  :user_id                    , null: false
            t.text  :requirement_identifier     , null: false
            t.float :score                      , null: true
            t.json  :score_history              , null: false, default: [], array: true
        end

        add_index :project_progress, [:user_id, :requirement_identifier], :unique => true
        add_foreign_key :project_progress, :users, column: :user_id, primary_key: :id
    end
end
