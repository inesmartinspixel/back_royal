class SubscriptionFunctionsPartDeux < ActiveRecord::Migration[5.1]
    def up

        execute %Q~
            DROP FUNCTION payment_amount_per_period(json[], text, json);
            DROP FUNCTION interval_for_plan(stripe_plans json[], stripe_plan_id text);

            CREATE FUNCTION fee_amount_per_interval(cohort_application cohort_applications)
            RETURNS float AS $$
                #{fee_amount_per_interval_fn}
                return getFeeAmountPerInterval(cohort_application);
            $$ LANGUAGE plv8 IMMUTABLE STRICT;



            CREATE FUNCTION total_fee_amount(cohort_application cohort_applications)
            RETURNS float AS $$
                #{num_intervals_for_plan_fn} // getNumIntervals
                #{current_stripe_plan_fn} // getCurrentStripePlan
                #{fee_amount_per_interval_fn} // feeAmountPerInterval

                if (!cohort_application.stripe_plans) {
                    return null;
                }

                if (getCurrentStripePlan(cohort_application)) {

                    var numIntervals = getNumIntervals(cohort_application);
                    var feeAmountPerInterval =  getFeeAmountPerInterval(cohort_application);

                    return numIntervals * feeAmountPerInterval;
                } else {
                    return 0;
                }

            $$ LANGUAGE plv8 IMMUTABLE STRICT;




            CREATE FUNCTION total_fees_plus_tuition(cohort_application cohort_applications)
            RETURNS float AS $$
                #{num_intervals_for_plan_fn} // getNumIntervals
                #{current_stripe_plan_fn} // getCurrentStripePlan
                #{total_base_tuition_fn} // totalBaseTuition

                var stripePlan = getCurrentStripePlan(cohort_application);
                if (stripePlan) {
                    return (stripePlan.amount / 100) * getNumIntervals(cohort_application);
                } else {
                    return totalBaseTuition(cohort_application); // full scholarship
                }

            $$ LANGUAGE plv8 IMMUTABLE STRICT;
            



            CREATE FUNCTION total_base_tuition(cohort_application cohort_applications)
            RETURNS float AS $$
                #{total_base_tuition_fn}
                return totalBaseTuition(cohort_application);

            $$ LANGUAGE plv8 IMMUTABLE STRICT;



            CREATE FUNCTION get_coupon_for_early_registration_situation(ca cohort_applications, early boolean)
            RETURNS json AS $$
            BEGIN

                -- There were a handful of "Full Scholarship" applications that had new stripe_plans 
                -- but a legacy scholarship_level, thus a coupon was not found. This bubbled up nil 
                -- instead of zero for net_required_payment. We saw that all of these applications were
                -- old and rejected / expelled, so we are not going to investigate further.

                RETURN ca.scholarship_level -> (case when early then 'early' else 'standard' end) -> (coalesce(ca.stripe_plan_id, default_stripe_plan(ca)->>'id'));
            END;
            $$ LANGUAGE plpgsql;



            CREATE FUNCTION get_coupon(ca cohort_applications)
            RETURNS json AS $$
            BEGIN
                RETURN get_coupon_for_early_registration_situation(ca, ca.registered_early);
            END;
            $$ LANGUAGE plpgsql;



            CREATE FUNCTION num_intervals(cohort_application cohort_applications)
            RETURNS float AS $$
                #{num_intervals_for_plan_fn} // getNumIntervals
                #{current_stripe_plan_fn} // getCurrentStripePlan

                return getNumIntervals(cohort_application);

            $$ LANGUAGE plv8 IMMUTABLE STRICT;




            CREATE FUNCTION total_scholarship(ca cohort_applications)
            RETURNS float AS $$
            BEGIN
                RETURN case
                    when (get_coupon(ca)->>'percent_off')::float = 100 then
                        total_fees_plus_tuition(ca) - total_discount(ca)
                    else
                        ((get_coupon(ca)->>'amount_off')::float / 100 - discount_per_interval(ca)) * num_intervals(ca)
                    end;
            END;
            $$ LANGUAGE plpgsql;



            CREATE FUNCTION discount_per_interval(ca cohort_applications)
            RETURNS float AS $$
            BEGIN
                return coalesce(
                    case
                        -- This only applies to people who have already registered
                        -- Before registering, you would need to check the clock, the
                        -- early_registration_deadline and allow_early_registration_pricing to
                        -- find out what discounts apply. See CohortApplication#applicable_registration_period
                        when ca.registered_early
                        then
                            (
                                (get_coupon_for_early_registration_situation(ca, true) ->> 'amount_off')::float -
                                (get_coupon_for_early_registration_situation(ca, false) ->> 'amount_off')::float
                            ) / 100
                        else
                        0
                    end
                , 0);
            END;
            $$ LANGUAGE plpgsql;


            CREATE FUNCTION total_discount(cohort_application cohort_applications)
            RETURNS float AS $$
            BEGIN
                RETURN case 
                    when num_intervals(cohort_application) is null then 0
                    else num_intervals(cohort_application) * discount_per_interval(cohort_application)
                    end;
            END;
            $$ LANGUAGE plpgsql;


            CREATE FUNCTION net_required_payment(ca cohort_applications)
            RETURNS float AS $$
            BEGIN
                RETURN total_fees_plus_tuition(ca) - total_discount(ca) - total_scholarship(ca);
            END;
            $$ LANGUAGE plpgsql;


            CREATE FUNCTION payment_amount_per_interval(ca cohort_applications)
            RETURNS float AS $$
            BEGIN
                RETURN case when num_intervals(ca) is null then 0
                    else
                        net_required_payment(ca) / num_intervals(ca)
                    end;
            END;
            $$ LANGUAGE plpgsql;


            CREATE FUNCTION default_stripe_plan(cohort_application cohort_applications)
            RETURNS json AS $$
                if (!cohort_application.stripe_plans) {
                    return null;
                }

                var oneTimePaymentPlan;
                for (var i = 0; i < cohort_application.stripe_plans.length; i++) {
                    var _plan = cohort_application.stripe_plans[i];
                    if (_plan.frequency === 'once') {
                        oneTimePaymentPlan = _plan;
                    }
                }

                if (oneTimePaymentPlan) {
                    return oneTimePaymentPlan;
                }


                var monthlyPlan;
                for (var i = 0; i < cohort_application.stripe_plans.length; i++) {
                    var _plan = cohort_application.stripe_plans[i];
                    if (_plan.frequency === 'monthly') {
                        monthlyPlan = _plan;
                    }
                }

                return monthlyPlan;

            $$ LANGUAGE plv8 IMMUTABLE STRICT;

            -- This intentionally leaves out the one-time situation, since those
            -- folks do not have an interval between payments.  This is used in 
            -- https://metabase.pedago-tools.com/question/70 to project out future payments
            CREATE FUNCTION stripe_plan_interval(cohort_application cohort_applications)
            RETURNS text AS $$
                #{current_stripe_plan_fn} // getCurrentStripePlan

                function getIntervalForPlan(plan) {
                    switch (plan.frequency) {
                        case 'bi_annual':
                            return '6 months';
                        case 'monthly':
                            return '1 month';
                    }
                }

                var plan = getCurrentStripePlan(cohort_application);

                return plan ? getIntervalForPlan(plan) : null;

            $$ LANGUAGE plv8 IMMUTABLE STRICT;

            ~
    end

    def down
        # see db/migrate/20180305185554_subscription_functions.rb
        execute %Q~

            DROP FUNCTION stripe_plan_interval(cohort_application cohort_applications);
            DROP FUNCTION default_stripe_plan(cohort_application cohort_applications);
            DROP FUNCTION payment_amount_per_interval(cohort_application cohort_applications);
            DROP FUNCTION net_required_payment(cohort_application cohort_applications);
            DROP FUNCTION total_discount(cohort_application cohort_applications);
            DROP FUNCTION discount_per_interval(cohort_application cohort_applications);
            DROP FUNCTION total_scholarship(ca cohort_applications);
            DROP FUNCTION num_intervals(cohort_application cohort_applications);
            DROP FUNCTION get_coupon(ca cohort_applications);
            DROP FUNCTION get_coupon_for_early_registration_situation(cohort_application cohort_applications, early boolean);
            DROP FUNCTION total_base_tuition(cohort_application cohort_applications);
            DROP FUNCTION total_fees_plus_tuition(cohort_application cohort_applications);
            DROP FUNCTION total_fee_amount(cohort_application cohort_applications);
            DROP FUNCTION fee_amount_per_interval(cohort_application cohort_applications);

            CREATE FUNCTION interval_for_plan(stripe_plans json[], stripe_plan_id text)
            RETURNS text AS $$

                function getIntervalForPlan(plan) {
                    switch (plan.frequency) {
                        case 'bi_annual':
                            return '6 months';
                        case 'monthly':
                            return '1 month';
                    }
                }

                var plan;
                for (var i = 0; i < stripe_plans.length; i++) {
                    var _plan = stripe_plans[i];
                    if (_plan.id === stripe_plan_id) {
                        plan = _plan;
                    }
                }

                return getIntervalForPlan(plan);

            $$ LANGUAGE plv8 IMMUTABLE STRICT;

            CREATE FUNCTION payment_amount_per_period(stripe_plans json[], stripe_plan_id text, scholarship_level json)
            RETURNS float AS $$

                function getPlanTuitionActualTotal(plan) {
                    var coupon = scholarship_level.coupons[plan.id];

                    var amountOff = coupon ? coupon.amount_off : 0,
                        percentOff = coupon ? coupon.percent_off : 0,
                        tuitionNumPaymentIntervals = getNumIntervalsForPlan(plan),
                        tuitionOriginalTotal = (plan.amount * tuitionNumPaymentIntervals / 100) || 0,
                        tuitionScholarshipTotal;


                    if (!!amountOff) {
                        tuitionScholarshipTotal = tuitionNumPaymentIntervals * amountOff / 100;
                    } else if (!!percentOff) {
                        tuitionScholarshipTotal = percentOff * tuitionOriginalTotal / 100;
                    } else {
                        tuitionScholarshipTotal = 0;
                    }
                    return tuitionOriginalTotal - tuitionScholarshipTotal;
                }

                function getNumIntervalsForPlan(plan) {
                    switch (plan.frequency) {
                        case 'bi_annual':
                            return 2;
                        case 'monthly':
                            return 12;
                        case 'once':
                            return 1;
                    }
                }

                var plan;
                for (var i = 0; i < stripe_plans.length; i++) {
                    var _plan = stripe_plans[i];
                    if (_plan.id === stripe_plan_id) {
                        plan = _plan;
                    }
                }

                var tuitionActualTotal = getPlanTuitionActualTotal(plan),
                    tuitionNumPaymentIntervals = getNumIntervalsForPlan(plan),
                    tuitionActualPaymentPerInterval = tuitionActualTotal / tuitionNumPaymentIntervals;

                return tuitionActualPaymentPerInterval;
            $$ LANGUAGE plv8 IMMUTABLE STRICT;
        ~
    end

    def fee_amount_per_interval_fn
        %Q~
            function getFeeAmountPerInterval(cohortApplication) {
                if (!cohortApplication.stripe_plans) {
                    return null;
                }

                if (cohort_application.stripe_plan_id === 'emba_875') {
                    return 75;
                }

                return 0;
            }
        ~
    end

    def num_intervals_for_plan_fn
        %Q~
        function getNumIntervals(cohortApplication) {
            var plan = getCurrentStripePlan(cohortApplication)
            if (!plan) {
                return null;
            }
            switch (plan.frequency) {
                case 'bi_annual':
                    return 2;
                case 'monthly':
                    return 12;
                case 'once':
                    return 1;
            }
        }
        ~
    end

    def current_stripe_plan_fn
        %Q~

        function getCurrentStripePlan(cohortApplication) {

            if (!cohortApplication.stripe_plans) {
                return null;
            }

            // Full scholarship users might have a stripe_plan_id, but they don't
            // actually have a stripe plan, since they don't pay.
            // Note that we have to be defensive here because paid_cert users DO have stripe_plans
            // but DO NOT have the notion of scholarships.
            if (cohortApplication.scholarship_level && cohortApplication.scholarship_level.name === 'Full Scholarship') {
                return null;
            }

            var plan;
            for (var i = 0; i < cohortApplication.stripe_plans.length; i++) {
                var _plan = cohortApplication.stripe_plans[i];
                if (_plan.id === cohortApplication.stripe_plan_id) {
                    plan = _plan;
                }
            }
            return plan;
        }
        ~
    end

    def total_base_tuition_fn
        %Q~
            function totalBaseTuition(cohortApplication) {
                var oneTimePaymentPlan;
                if (!cohortApplication.stripe_plans) {
                    return null;
                }

                for (var i = 0; i < cohortApplication.stripe_plans.length; i++) {
                    var _plan = cohortApplication.stripe_plans[i];
                    if (_plan.frequency === 'once') {
                        oneTimePaymentPlan = _plan;
                    }
                }

                // Some legacy users don't have a once plan, so just hardcode their total_base_tuition
                return oneTimePaymentPlan ? oneTimePaymentPlan.amount / 100 : 9600;
            }
        ~
    end
end
