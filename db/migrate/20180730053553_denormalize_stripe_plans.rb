class DenormalizeStripePlans < ActiveRecord::Migration[5.1]
    def change
        add_column :cohort_applications, :stripe_plans, :json, array: true
        add_column :cohort_applications_versions, :stripe_plans, :json, array: true

        add_column :cohort_applications, :scholarship_levels, :json, array: true
        add_column :cohort_applications_versions, :scholarship_levels, :json, array: true
    end
end
