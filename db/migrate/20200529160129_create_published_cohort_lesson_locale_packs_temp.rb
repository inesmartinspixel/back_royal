class CreatePublishedCohortLessonLocalePacksTemp < ActiveRecord::Migration[6.0]
    def up
        create_table :published_cohort_lesson_locale_packs_temp, id: false do |t|
            t.datetime :created_at
            t.uuid :cohort_id
            t.text :cohort_name
            t.boolean :foundations
            t.boolean :required
            t.boolean :specialization
            t.boolean :elective
            t.boolean :exam
            t.boolean :test
            t.boolean :assessment
            t.text :lesson_title
            t.uuid :lesson_locale_pack_id
            t.text :lesson_locales
        end

        add_index :published_cohort_lesson_locale_packs_temp, [:cohort_id, :lesson_locale_pack_id], :unique => true, :name => :cohort_and_lesson_lp_on_coh_lesson_lps_tbl
        add_index :published_cohort_lesson_locale_packs_temp, :lesson_locale_pack_id, :name => :lesson_lp_on_cohort_lesson_locale_packs_tbl

        # PublishedCohortLessonLocalePack.rebuild
    end

    def down
        drop_table :published_cohort_lesson_locale_packs_temp
    end
end
