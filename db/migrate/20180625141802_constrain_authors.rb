class ConstrainAuthors < ActiveRecord::Migration[5.1]
    def change

        add_foreign_key :lessons, :users, column: :author_id, primary_key: :id

    end
end
