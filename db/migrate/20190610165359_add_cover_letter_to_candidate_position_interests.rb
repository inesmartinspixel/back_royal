class AddCoverLetterToCandidatePositionInterests < ActiveRecord::Migration[5.2]

    def up
        add_column :candidate_position_interests, :cover_letter, :json, :default => {}, :null => false
        add_column :candidate_position_interests_versions, :cover_letter, :json

        recreate_trigger_v2(:candidate_position_interests)
    end

    def down
        remove_column :candidate_position_interests, :cover_letter
        remove_column :candidate_position_interests_versions, :cover_letter

        recreate_trigger_v2(:candidate_position_interests)
    end
end
