class BackfillNonDegreeProgramAttrsToNull < ActiveRecord::Migration[5.1]

  def change
    # See https://bitbucket.org/pedago-ondemand/back_royal/pull-requests/3841
    #
    # When the linked PR rolled, we removed degree, minor, gpa, and gpa_description
    # from the UI for non-degree programs.
    #
    # However, those fields were present from 12-19-2017 to 01-03-2018 (and beyond
    # for some legacy clients).
    execute "
        UPDATE education_experiences
        SET degree = NULL,
            minor = NULL,
            gpa = NULL,
            gpa_description = NULL
        WHERE degree_program = FALSE
            AND (degree IS NOT NULL
                OR minor IS NOT NULL
                OR gpa IS NOT NULL
                OR gpa_description IS NOT NULL
            );"
  end

end
