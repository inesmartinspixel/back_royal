class RemoveNonCriticalContentViews < ActiveRecord::Migration[6.0]
    def up
        ViewHelpers.migrate(self, 20200624011101)
    end

    def down
        ViewHelpers.rollback(self, 20200624011101)
    end
end
