class BackfillConsiderEarlyDecisionToNil < ActiveRecord::Migration[5.1]
  # we are disabling transactions so that we don't update every row in the
  # table in a single transaction.  In order for this to be ok, we need
  # to make sure everything in this migration is idempotent
  disable_ddl_transaction!

  def up
      # Set nullable (idempotent)
      change_column_null :career_profiles, :consider_early_decision, true

      subquery = CohortApplication.select('user_id').joins(:cohort).where("cohorts.program_type" => 'emba').where(status: ['accepted', 'pre_accepted', 'pending'])

      CareerProfile.where("user_id not in (#{subquery.to_sql})")
        .in_batches.update_all consider_early_decision: nil
  end

  def down
      change_column_null :career_profiles, :consider_early_decision, false
  end
end