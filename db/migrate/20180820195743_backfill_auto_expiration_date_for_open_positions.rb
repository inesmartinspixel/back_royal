class BackfillAutoExpirationDateForOpenPositions < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        sixty_days_from_today = (Date.today + 60.days).to_s
        OpenPosition.where(archived: false).in_batches.update_all auto_expiration_date: sixty_days_from_today

        # NOTE: if you're copying and pasting the contents of this migration file, this migration doesn't
        # change the column in question to be NOT NULL when migrating up, so be sure to include this step
        # if necessary after copying and pasting.
    end

    def down
        # noop

        # NOTE: if you're copying and pasting the contents of this migration file, this migration doesn't
        # change the column in question to be NULLABLE when migrating down, so be sure to include this step
        # if necessary after copying and pasting.
    end
end
