class CreateProjectProgressVersions < ActiveRecord::Migration[5.2]

    def up
        create_versions_table_and_trigger_v2(:project_progress)
    end

    def down
        drop_table :project_progress_versions
        execute "DROP TRIGGER project_progress_versions ON project_progress"
    end
end
