class AddIndexAndFkUsersAvatars < ActiveRecord::Migration[5.1]

    def up

        # Before we transitioned to avatar_assets, it was possible for users to have
        # duplicate avatar_ids that pointed to s3_assets records. If one of those users
        # then updated their avatar, the record would be deleted from s3_assets in back_royal
        # and the file would be deleted from S3. The other users sharing that same avatar_id
        # would then be pointed to a record that doesn't exist in either back_royal or S3.
        #
        # There are 35 such users records found on prod. We decided the best course of action
        # would be to unset their avatar_ids. I decided it would ideal to special case the
        # specific avatar_ids here to minimize risk.
        #
        # The records were found with the following query:
        #
        # WITH avatar_asset_ids AS MATERIALIZED (
        #   SELECT id
        #   FROM avatar_assets
        # ), s3_asset_ids AS MATERIALIZED (
        #   SELECT id
        #   FROM s3_assets
        # )
        # SELECT
        #   count(*) AS count
        #   , avatar_id
        # FROM users
        # WHERE avatar_id NOT IN (SELECT * FROM avatar_asset_ids)
        #   AND avatar_id NOT IN (SELECT * FROM s3_asset_ids)
        # GROUP BY avatar_id
        # ORDER BY count DESC;
        execute %Q~
            UPDATE users
            SET avatar_id = NULL
            WHERE avatar_id in (
                '05179984-478d-42f1-b20f-d4a0221dd42d',
                'db81ba4b-e1be-4077-85a5-067f6b37e365',
                '66ebd498-6aa6-4bb4-ae8c-df42808dac29'
            );
        ~

        add_index           :users, :avatar_id, :unique => true
        add_foreign_key     :users, :avatar_assets, column: :avatar_id, primary_key: :id
    end

    def down
        remove_foreign_key  :users, column: :avatar_id
        remove_index        :users, :avatar_id
    end

end
