class DropMultipleChoiceChallengeProgress < ActiveRecord::Migration[5.2]
    def up
        execute "DROP TABLE multiple_choice_challenge_progress"
    end

    def down

        execute "
            --
            -- Name: multiple_choice_challenge_progress; Type: TABLE; Schema: public;
            --

            CREATE TABLE public.multiple_choice_challenge_progress (
                id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
                user_id uuid,
                lesson_id uuid,
                frame_id uuid,
                editor_template character varying(255),
                challenge_id uuid,
                first_answer_id character varying(255),
                first_answer_estimated_time timestamp without time zone,
                first_answer_correct boolean,
                created_at timestamp without time zone,
                updated_at timestamp without time zone
            );

            --
            -- Name: multiple_choice_challenge_progress multiple_choice_challenge_progress_pkey; Type: CONSTRAINT; Schema: public;
            --

            ALTER TABLE ONLY public.multiple_choice_challenge_progress
                ADD CONSTRAINT multiple_choice_challenge_progress_pkey PRIMARY KEY (id);


            --
            -- Name: index_multiple_choice_challenge_progress_on_created_at; Type: INDEX; Schema: public;
            --

            CREATE INDEX index_multiple_choice_challenge_progress_on_created_at ON public.multiple_choice_challenge_progress USING btree (created_at);


            --
            -- Name: multiple_choice_challenge_progress_ed_template; Type: INDEX; Schema: public;
            --

            CREATE INDEX multiple_choice_challenge_progress_ed_template ON public.multiple_choice_challenge_progress USING btree (editor_template);


            --
            -- Name: multiple_choice_challenge_progress_unique; Type: INDEX; Schema: public;
            --

            CREATE UNIQUE INDEX multiple_choice_challenge_progress_unique ON public.multiple_choice_challenge_progress USING btree (user_id, lesson_id, frame_id, challenge_id);


            --
            -- Name: TABLE multiple_choice_challenge_progress; Type: ACL; Schema: public;
            --

            GRANT SELECT ON TABLE public.multiple_choice_challenge_progress TO read;
            GRANT SELECT ON TABLE public.multiple_choice_challenge_progress TO readonly;

        "

    end
end
