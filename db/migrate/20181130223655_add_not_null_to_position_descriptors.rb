class AddNotNullToPositionDescriptors < ActiveRecord::Migration[5.2]
    def change
        change_column_null :open_positions, :position_descriptors, false
    end
end
