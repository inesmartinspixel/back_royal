class DstAwareEnrollmentDeadline < ActiveRecord::Migration[5.1]
  def up

=begin
    The rules for adding intervals to times in postgres do not work
    the same as they do in ruby and javascript.  In ruby and javascript,
    we have TimeWithZone objects.  If you add days to one of those objects,
    it will respect the daylight savings rules for that particular timezone.

    In postgres, however, the database always follow the rules for the global
    time zone, regardless of the timezone on your particular time with zone object.
    That means that we cannot convert times to eastern, add days, and convert back
    to utc like we do in javascript and ruby.

    Instead, we do addition on a timestamp without zone.  See inline comments below
    for details.

    Practically this should just as well as the solutions in ruby and javascript,
    though it is theoretically worse.  For example, if there were a specific date
    at which Eastern time jumped ahead by 3 hours, that would be encoded in the
    rails and moment libraries, but we would not have a way of dealing with that
    here (this kind of thing happens in other time zones, but hopefully not
    eastern)
=end

    execute %Q~
        CREATE FUNCTION add_dst_aware_offset(reftime TIMESTAMP, val NUMERIC, unit TEXT)
          RETURNS TIMESTAMP AS $$
            SELECT (
                (
                    -- Start with a timestamp without zone, which is how we
                    -- normally store things in the database. For example, we
                    -- might have '2018/03/08 12:00:00' stored in the database. This
                    -- timestamp without time zone represents the time
                    -- '2018/03/08 12:00:00 UTC' == '2018/03/08 07:00 EST -0500', though
                    -- that is just something we know as developers, it is NOT encoded
                    -- in postgres in any way.
                    reftime

                    -- Tell postgres that the timestamp without zone that we have represents
                    -- a utc time, and convert it to a timestamp with zone
                    at time zone 'UTC'

                    -- Ask postgres what is the equivalent time in eastern. Postgres, surprisingly
                    -- returns a timestamp WITHOUT zone here, equal to '2018/03/08 07:00'.  So it
                    -- is the time in eastern that corresponds to the utc time we had saved in the
                    -- database, but there is no time zone information encoded in the object itself.
                    at time zone 'America/New_York'

                    -- Now we can add intervals to this time and it will act as though we lived
                    -- in a world with no timezones and no daylight savings time (e.g. the world
                    -- we should actually live in).  For example, we might add '10 days', ending
                    -- up with the timestamp without zone '2018/03/18 07:00'.
                    + (val || ' ' || unit)::INTERVAL
                )

                -- Tell postgres that this timestamp without timezone that we have represents
                -- an eastern time, and convert it to a timestamp with zone
                at time zone 'America/New_York'

                -- Ask postgres what is the equivalent time in UTC. Once again, postgres returns
                -- an timestamp without time zone, and that is what we want to put in the database.
                -- In our example, we are now on the other side of daylight savings time, so we
                -- get a utc time with a different hour than the one we started with, which is
                -- what we want: '2018/03/18 11:00'
                at time zone 'UTC'
            );
        $$ LANGUAGE SQL;
    ~

    # NOTE: this will impact the following:
    # ViewHelpers::PublishedCohorts
    # ViewHelpers::PublishedCohortPeriods
    # ViewHelpers::PublishedCohortAdmissionRounds
    ViewHelpers.migrate(self, 20180423140732)

    # Republish existing Cohorts to schedule jobs correctly.
    # NOTE: This helper should blow up if there are any unpublished
    # changes to cohorts, so we don't accidentally publish any
    # unwanted changes.
    Cohort.all_published
        .where(program_type: ['mba', 'emba'])
        .where("cohorts_versions.end_date > ?", Time.now)
        .edit_and_republish
  end

  def down
    ViewHelpers.rollback(self, 20180423140732)

    execute %Q~
        DROP FUNCTION add_dst_aware_offset(reftime TIMESTAMP, val NUMERIC, unit TEXT);
    ~
  end
end
