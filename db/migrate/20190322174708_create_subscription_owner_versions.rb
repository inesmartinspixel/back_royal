class CreateSubscriptionOwnerVersions < ActiveRecord::Migration[5.2]

    def up
        create_versions_table_and_trigger_v2(:subscriptions_users)
        create_versions_table_and_trigger_v2(:subscriptions_hiring_teams)
    end

    def down
        drop_table :subscriptions_users_versions
        drop_table :subscriptions_hiring_teams_versions
    end

end
