class AddTrgmIndicesToSuggestOptionsText < ActiveRecord::Migration[5.1]

    disable_ddl_transaction!

    def up

        return if Rails.env.production?

        ["professional", "educational"].each do |option_type|
            # see also: https://stackoverflow.com/a/13452528/824966
            execute "CREATE INDEX CONCURRENTLY #{option_type}_organization_options_text ON #{option_type}_organization_options USING gin (text gin_trgm_ops)"
        end
    end

    def down

        return if Rails.env.production?

        ["professional", "educational"].each do |option_type|
            execute "DROP INDEX #{option_type}_organization_options_text"
        end
    end

end
