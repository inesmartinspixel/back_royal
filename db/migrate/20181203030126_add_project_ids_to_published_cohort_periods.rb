class AddProjectIdsToPublishedCohortPeriods < ActiveRecord::Migration[5.2]
    def up
        ViewHelpers.migrate(self, 20181203030126)
    end

    def down
        ViewHelpers.rollback(self, 20181203030126)
    end
end
