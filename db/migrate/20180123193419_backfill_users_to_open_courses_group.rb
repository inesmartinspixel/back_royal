class BackfillUsersToOpenCoursesGroup < ActiveRecord::Migration[5.1]
    def up
        # backfill FREEMBA users to be in the OPEN COURSES access group
        execute("
            -- Create OPEN COURSES access group if it doesn't exist
            INSERT INTO access_groups (created_at, updated_at, name) values (now(), now(), 'OPEN COURSES'), (now(), now(), 'EXTRA COURSES') ON CONFLICT DO NOTHING;

            -- Backill users into the OPEN COURSES access group
            INSERT INTO access_groups_users (access_group_id, user_id) (
                SELECT
                    (SELECT id FROM access_groups WHERE access_groups.name = 'OPEN COURSES' LIMIT 1) as ag_id,
                    users.id
                FROM users
                WHERE sign_up_code IN ('FREEMBA', 'SMARTER', 'MBA', 'PRE_MBA', 'BLUEOCEAN')
            ) ON CONFLICT DO NOTHING;

            -- Remove SMARTER access group from users with specific sign up codes
            DELETE FROM
                access_groups_users
            WHERE
                -- user is in SMARTER access group
                access_groups_users.access_group_id = (SELECT id FROM access_groups WHERE access_groups.name = 'SMARTER' LIMIT 1)

                -- don't remove the user from the SMARTER access group if they're an institutional user
                AND access_groups_users.user_id IN (
                    SELECT
                        users.id
                    FROM users
                    WHERE sign_up_code IN ('FREEMBA', 'SMARTER', 'MBA', 'PRE_MBA', 'BLUEOCEAN')
                )
        ")
    end

    def down
        # noop
    end
end
