class ChangeOpenPositionsArchivedColumnToNotBeNullable < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        OpenPosition.where(archived: nil).in_batches.update_all archived: false
        change_column_null :open_positions, :archived, false
    end

    def down
        change_column_null :open_positions, :archived, true
    end
end
