class AddDefaultsToUserDocumentsBools < ActiveRecord::Migration[5.2]

    def up
        change_column_default :users, :english_language_proficiency_documents_approved, false
        change_column_default :users, :transcripts_verified, false
        
    end

    def down
        change_column_default :users, :english_language_proficiency_documents_approved, nil
        change_column_default :users, :transcripts_verified, nil
    end
end
