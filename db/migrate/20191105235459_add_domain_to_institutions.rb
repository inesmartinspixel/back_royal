class AddDomainToInstitutions < ActiveRecord::Migration[6.0]
    def up
        add_column :institutions, :domain, :text
        add_column :institutions_versions, :domain, :text
        recreate_trigger_v2(:institutions)
    end

    def down
        remove_column :institutions, :domain, :text
        remove_column :institutions_versions, :domain, :text
        recreate_trigger_v2(:institutions)
    end
end
