class RemoveCohortUserWeeks < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20180111150157)
    end

    def down
        ViewHelpers.rollback(self, 20180111150157)
    end
end
