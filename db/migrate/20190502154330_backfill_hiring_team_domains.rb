class BackfillHiringTeamDomains < ActiveRecord::Migration[5.2]
    disable_ddl_transaction!

    def up

        ActiveRecord::Base.connection.execute(%Q~
            -- These few teams have already been removed on production.  They're old
            -- and have no members
            delete from hiring_teams where id in (
                '658af495-3f17-4beb-8e1f-8b4f245fc091',
                'd05525e6-0721-4880-8718-aefa92ee603b',
                'ffdebd0b-9fed-4e62-b485-2edd68e1a998',
                'e00fd1ed-154c-488f-a5db-df60683f5a52'
            );

            -- We could've removed this one too, but I just set the one member
            -- as the ownere
            update hiring_teams set owner_id = '3dad5c21-605f-4951-a075-6231174a0e5c' where id='350b36af-2706-465a-bec3-d7a5633af19c';


            -- If there are any other teams without owners, this will blow up when adding the not null

            UPDATE hiring_teams
            SET domain=subquery.domain
            FROM (
                select
                    hiring_teams.id
                    , hiring_teams.owner_id
                    , split_part(users.email, '@', 2) as domain
                from hiring_teams
                    join users on hiring_teams.owner_id = users.id
                order by domain
            ) AS subquery
            WHERE hiring_teams.id=subquery.id;

        ~)

        change_column_null 'hiring_teams', "domain", false



    end
end
