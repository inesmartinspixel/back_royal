class MultipleStripePlanSupport < ActiveRecord::Migration[5.1]

    def up
        add_column :cohorts, :stripe_plans, :json, array: true
        add_column :cohorts_versions, :stripe_plans, :json, array: true
        add_column :curriculum_templates, :stripe_plans, :json, array: true
        add_column :curriculum_templates_versions, :stripe_plans, :json, array: true

        add_column :cohorts, :scholarship_levels, :json, array: true
        add_column :cohorts_versions, :scholarship_levels, :json, array: true
        add_column :curriculum_templates, :scholarship_levels, :json, array: true
        add_column :curriculum_templates_versions, :scholarship_levels, :json, array: true

        add_column :cohort_applications, :scholarship_level, :json
        add_column :cohort_applications_versions, :scholarship_level, :json

        if Rails.env == 'test'
            add_column :schedulable_items, :stripe_plans, :json, array: true
            add_column :schedulable_items, :scholarship_levels, :json, array: true
        end

        migrate_relevant_data

    end

    def down

        if Rails.env == 'test'
            remove_column :schedulable_items, :stripe_plans
            remove_column :schedulable_items, :scholarship_levels
        end

        remove_column :cohort_applications, :scholarship_level
        remove_column :cohort_applications_versions, :scholarship_level

        remove_column :cohorts, :scholarship_levels
        remove_column :cohorts_versions, :scholarship_levels
        remove_column :curriculum_templates, :scholarship_levels
        remove_column :curriculum_templates_versions, :scholarship_levels

        remove_column :cohorts, :stripe_plans
        remove_column :cohorts_versions, :stripe_plans
        remove_column :curriculum_templates, :stripe_plans
        remove_column :curriculum_templates_versions, :stripe_plans

    end



    def custom_coupon_level(stripe_coupon_id)
        amount = stripe_coupon_id.split('_').last.to_i * 100
        '\'{"name": "Custom Scholarship", "coupons": {"emba_800": {"id": "' + stripe_coupon_id + '", "amount_off": "' + amount.to_s + '", "percent_off": null} } }\'::json'
    end

    def migrate_relevant_data

        # pre-determined scholarship levels
        base_level_sql = '\'{"name": "No Scholarship", "coupons": {"emba_800": {"id": "none", "amount_off": 0, "percent_off": 0}, "emba_bi_4800": {"id": "discount_600", "amount_off": 60000, "percent_off": null} } }\'::json'
        level_1_sql    = '\'{"name": "Level 1", "coupons": {"emba_800": {"id": "discount_150", "amount_off": 15000, "percent_off": null}, "emba_bi_4800": {"id": "discount_1400", "amount_off": 140000, "percent_off": null} } }\'::json'
        level_2_sql    = '\'{"name": "Level 2", "coupons": {"emba_800": {"id": "discount_250", "amount_off": 25000, "percent_off": null}, "emba_bi_4800": {"id": "discount_2000", "amount_off": 200000, "percent_off": null} } }\'::json'
        level_3_sql    = '\'{"name": "Level 3", "coupons": {"emba_800": {"id": "discount_350", "amount_off": 35000, "percent_off": null}, "emba_bi_4800": {"id": "discount_2500", "amount_off": 250000, "percent_off": null} } }\'::json'
        level_4_sql    = '\'{"name": "Level 4", "coupons": {"emba_800": {"id": "discount_450", "amount_off": 45000, "percent_off": null}, "emba_bi_4800": {"id": "discount_3000", "amount_off": 300000, "percent_off": null} } }\'::json'
        level_5_sql    = '\'{"name": "Level 5", "coupons": {"emba_800": {"id": "discount_550", "amount_off": 55000, "percent_off": null}, "emba_bi_4800": {"id": "discount_3550", "amount_off": 355000, "percent_off": null} } }\'::json'
        full_level_sql = '\'{"name": "Full Scholarship", "coupons": {"emba_800": {"id": "discount_100_percent", "amount_off": null, "percent_off": 100}, "emba_bi_4800": {"id": "discount_100_percent", "amount_off": null, "percent_off": 100} } }\'::json'

        # create plan and levels configuration for existing `emba_800` plans
        ["cohorts", "cohorts_versions", "curriculum_templates", "curriculum_templates_versions"].each do |table|

            # Add new multi-plan configuration
            stripe_plans_assignment_sql =
                    'stripe_plans = ARRAY[
                        \'{"id": "emba_800", "name": "Monthly Plan", "amount": "80000", "frequency": "monthly"}\'::json,
                        \'{"id": "emba_bi_4800", "name": "Bi-Annual Plan", "amount": "480000", "frequency": "bi_annual"}\'::json
                    ]'

            # and scholarship levels
            scholarship_levels_assignment_sql =
                    "scholarship_levels = ARRAY[
                       #{base_level_sql},
                       #{level_1_sql},
                       #{level_2_sql},
                       #{level_3_sql},
                       #{level_4_sql},
                       #{level_5_sql},
                       #{full_level_sql}
                    ]"

            execute "UPDATE #{table} SET #{stripe_plans_assignment_sql}, #{scholarship_levels_assignment_sql} WHERE stripe_plan_id = 'emba_800'"

        end

        # back-populate scholarship_level on cohort_applications here, using all known coupon codes
        execute "UPDATE cohort_applications SET scholarship_level =
                    CASE
                        WHEN stripe_coupon_id IS NULL THEN #{base_level_sql}
                        WHEN stripe_coupon_id = 'none' THEN #{base_level_sql}
                        WHEN stripe_coupon_id = 'discount_150' THEN #{level_1_sql}
                        WHEN stripe_coupon_id = 'discount_250' THEN #{level_2_sql}
                        WHEN stripe_coupon_id = 'discount_350' THEN #{level_3_sql}
                        WHEN stripe_coupon_id = 'discount_450' THEN #{level_4_sql}
                        WHEN stripe_coupon_id = 'discount_550' THEN #{level_5_sql}
                        WHEN stripe_coupon_id = 'discount_100_percent' THEN #{full_level_sql}
                        WHEN stripe_coupon_id = 'discount_legacy_305' THEN " + custom_coupon_level("discount_legacy_305") + "
                        WHEN stripe_coupon_id = 'discount_legacy_405' THEN " + custom_coupon_level("discount_legacy_405") + "
                        WHEN stripe_coupon_id = 'discount_legacy_467' THEN " + custom_coupon_level("discount_legacy_467") + "
                        WHEN stripe_coupon_id = 'discount_legacy_605' THEN " + custom_coupon_level("discount_legacy_605") + "
                    END
                WHERE cohort_id IN (SELECT id FROM cohorts WHERE stripe_plans IS NOT NULL)"


    end

end
