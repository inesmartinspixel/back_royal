class CareerProfileIndexes < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!
    def change

        # these are used in the querying for recently_updated_career_profiles
        # in search_helpers
        add_index :career_profiles, :updated_at, algorithm: :concurrently
        add_index :hiring_relationships, :updated_at, algorithm: :concurrently
        add_index :work_experiences, :start_date, algorithm: :concurrently
        add_index :work_experiences, :end_date, algorithm: :concurrently
    end
end
