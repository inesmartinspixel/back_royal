class AddPreAcceptedToUsersRelevantCohortsView < ActiveRecord::Migration[5.1]
  def up
        ViewHelpers.migrate(self, 20171011175022)
    end

    def down
        ViewHelpers.rollback(self, 20171011175022)
    end
end
