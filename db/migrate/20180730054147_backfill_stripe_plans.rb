class BackfillStripePlans < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up

        while more_records_exist
            execute %Q~
                with applications_to_update AS MATERIALIZED (
                    SELECT
                        cohort_applications.id
                        , cohorts.stripe_plans
                        , cohorts.scholarship_levels
                    from cohort_applications
                        join cohorts on cohorts.id = cohort_applications.cohort_id
                    where
                        cohort_applications.stripe_plans is null
                        and cohorts.stripe_plans is not null
                    limit 1000
                )
                UPDATE cohort_applications
                SET
                    stripe_plans=subquery.stripe_plans,
                    scholarship_levels=subquery.scholarship_levels
                FROM (
                    SELECT * FROM applications_to_update
                ) AS subquery
                WHERE cohort_applications.id=subquery.id;
            ~
        end

    end

    def more_records_exist
        !!(execute(%Q~
            SELECT cohort_applications.id
            FROM cohort_applications
                JOIN cohorts on cohorts.id = cohort_id
            WHERE
                cohorts.stripe_plans IS NOT NULL
                AND cohort_applications.stripe_plans is null
            LIMIT 1
        ~)).to_a[0]
    end
end
