class RemoveCareerProfilesVersionsDefaults < ActiveRecord::Migration[5.1]
    def up
        change_column :career_profiles_versions, :short_answers, :json, :null => true
        change_column :career_profiles_versions, :willing_to_relocate, :boolean, :null => true
        change_column :career_profiles_versions, :short_answers, :json, :null => true
        change_column :career_profiles_versions, :open_to_remote_work, :boolean, :null => true
        change_column :career_profiles_versions, :top_mba_subjects, :text, :array => true, :null => true
        change_column :career_profiles_versions, :top_personal_descriptors, :text, :array => true, :null => true
        change_column :career_profiles_versions, :top_motivations, :text, :array => true, :null => true
        change_column :career_profiles_versions, :top_workplace_strengths, :text, :array => true, :null => true
        change_column :career_profiles_versions, :preferred_company_culture_descriptors, :text, :array => true, :null => true
        change_column :career_profiles_versions, :job_sectors_of_interest, :text, :array => true, :null => true, :default => nil
        change_column :career_profiles_versions, :employment_types_of_interest, :text, :array => true, :null => true, :default => nil
        change_column :career_profiles_versions, :company_sizes_of_interest, :text, :array => true, :null => true, :default => nil
        change_column :career_profiles_versions, :primary_areas_of_interest, :text, :array => true, :null => true, :default => nil
        change_column :career_profiles_versions, :place_details, :json, :null => true, :default => nil
    end

    def down
        # no-op
    end
end
