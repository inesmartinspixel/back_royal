class BackfillLearnerProjectsType < ActiveRecord::Migration[5.2]
    disable_ddl_transaction!

    def up
        # Backfill any capstone projects to "capstone"
        LearnerProject.where(project_type: nil).where("requirement_identifier ilike '\%capstone\%'").in_batches.update_all project_type: 'capstone'
        # Currently, all other learner projects are "standard"
        LearnerProject.where(project_type: nil).in_batches.update_all project_type: 'standard'
        change_column_null :learner_projects, :project_type, false
    end

    def down
        change_column_null :learner_projects, :project_type, true
    end

end
