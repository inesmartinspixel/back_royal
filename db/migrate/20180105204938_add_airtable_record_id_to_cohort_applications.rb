class AddAirtableRecordIdToCohortApplications < ActiveRecord::Migration[5.1]
    def change
        add_column :cohort_applications, :airtable_record_id, :string
        add_column :cohort_applications_versions, :airtable_record_id, :string
    end
end
