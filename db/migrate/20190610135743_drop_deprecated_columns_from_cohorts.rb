class DropDeprecatedColumnsFromCohorts < ActiveRecord::Migration[5.2]

    def up
        remove_column :cohorts, :playlist_pack_ids
        remove_column :cohorts_versions, :playlist_pack_ids

        remove_column :cohorts, :application_deadline
        remove_column :cohorts_versions, :application_deadline

        recreate_trigger_v2 "cohorts"
    end

    def down
        add_column :cohorts, :playlist_pack_ids, :uuid, array: true, default: '{}', null: false
        add_column :cohorts_versions, :playlist_pack_ids, :uuid, array: true

        add_column :cohorts, :application_deadline, :datetime
        add_column :cohorts_versions, :application_deadline, :datetime

        recreate_trigger_v2 "cohorts"
    end

end
