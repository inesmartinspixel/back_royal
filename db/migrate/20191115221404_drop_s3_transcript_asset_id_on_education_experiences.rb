class DropS3TranscriptAssetIdOnEducationExperiences < ActiveRecord::Migration[6.0]
    def up
        remove_column :education_experiences, :s3_transcript_asset_id, :uuid
        remove_column :education_experiences_versions, :s3_transcript_asset_id, :uuid
        recreate_trigger_v2(:education_experiences)
    end

    def down
        # See 20191115221404_drop_s3_transcript_asset_id_on_education_experiences.rb
        add_column :education_experiences, :s3_transcript_asset_id, :uuid
        add_index :education_experiences, :s3_transcript_asset_id, unique: true
        add_foreign_key :education_experiences, :s3_transcript_assets

        add_column :education_experiences_versions, :s3_transcript_asset_id, :uuid

        recreate_trigger_v2(:education_experiences)
    end
end
