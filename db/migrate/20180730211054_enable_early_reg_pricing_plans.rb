class EnableEarlyRegPricingPlans < ActiveRecord::Migration[5.1]

    def up

        # pre-determined scholarship levels
        base_level_sql = '\'{"name": "No Scholarship",      "coupons": {"emba_875": {"id": "none", "amount_off": 0, "percent_off": 0}, "emba_once_9600": {"id": "none", "amount_off": 0, "percent_off": 0} },                                              "standard": {"emba_875": {"id": "none", "amount_off": 0, "percent_off": 0}, "emba_once_9600": {"id": "none", "amount_off": 0, "percent_off": 0} },                                              "early": {"emba_875": {"id": "discount_early_75", "amount_off": 7500, "percent_off": 0}, "emba_once_9600": {"id": "discount_early_1600", "amount_off": 160000, "percent_off": 0} } }\'::json'
        level_1_sql    = '\'{"name": "Level 1",             "coupons": {"emba_875": {"id": "discount_150", "amount_off": 15000, "percent_off": null}, "emba_once_9600": {"id": "discount_1800", "amount_off": 180000, "percent_off": null} },              "standard": {"emba_875": {"id": "discount_150", "amount_off": 15000, "percent_off": null}, "emba_once_9600": {"id": "discount_1800", "amount_off": 180000, "percent_off": null} },              "early": {"emba_875": {"id": "discount_early_225", "amount_off": 22500, "percent_off": 0}, "emba_once_9600": {"id": "discount_early_3100", "amount_off": 310000, "percent_off": 0} } }\'::json'
        level_2_sql    = '\'{"name": "Level 2",             "coupons": {"emba_875": {"id": "discount_250", "amount_off": 25000, "percent_off": null}, "emba_once_9600": {"id": "discount_3000", "amount_off": 300000, "percent_off": null} },              "standard": {"emba_875": {"id": "discount_250", "amount_off": 25000, "percent_off": null}, "emba_once_9600": {"id": "discount_3000", "amount_off": 300000, "percent_off": null} },              "early": {"emba_875": {"id": "discount_early_325", "amount_off": 32500, "percent_off": 0}, "emba_once_9600": {"id": "discount_early_4100", "amount_off": 410000, "percent_off": 0} } }\'::json'
        level_3_sql    = '\'{"name": "Level 3",             "coupons": {"emba_875": {"id": "discount_350", "amount_off": 35000, "percent_off": null}, "emba_once_9600": {"id": "discount_4200", "amount_off": 420000, "percent_off": null} },              "standard": {"emba_875": {"id": "discount_350", "amount_off": 35000, "percent_off": null}, "emba_once_9600": {"id": "discount_4200", "amount_off": 420000, "percent_off": null} },              "early": {"emba_875": {"id": "discount_early_425", "amount_off": 42500, "percent_off": 0}, "emba_once_9600": {"id": "discount_early_5100", "amount_off": 510000, "percent_off": 0} } }\'::json'
        level_4_sql    = '\'{"name": "Level 4",             "coupons": {"emba_875": {"id": "discount_450", "amount_off": 45000, "percent_off": null}, "emba_once_9600": {"id": "discount_5400", "amount_off": 540000, "percent_off": null} },              "standard": {"emba_875": {"id": "discount_450", "amount_off": 45000, "percent_off": null}, "emba_once_9600": {"id": "discount_5400", "amount_off": 540000, "percent_off": null} },              "early": {"emba_875": {"id": "discount_early_525", "amount_off": 52500, "percent_off": 0}, "emba_once_9600": {"id": "discount_early_6100", "amount_off": 610000, "percent_off": 0} } }\'::json'
        level_5_sql    = '\'{"name": "Level 5",             "coupons": {"emba_875": {"id": "discount_550", "amount_off": 55000, "percent_off": null}, "emba_once_9600": {"id": "discount_6600", "amount_off": 660000, "percent_off": null} },              "standard": {"emba_875": {"id": "discount_550", "amount_off": 55000, "percent_off": null}, "emba_once_9600": {"id": "discount_6600", "amount_off": 660000, "percent_off": null} },              "early": {"emba_875": {"id": "discount_early_625", "amount_off": 62500, "percent_off": 0}, "emba_once_9600": {"id": "discount_early_7100", "amount_off": 710000, "percent_off": 0} } }\'::json'
        full_level_sql = '\'{"name": "Full Scholarship",    "coupons": {"emba_875": {"id": "discount_100_percent", "amount_off": null, "percent_off": 100}, "emba_once_9600": {"id": "discount_100_percent", "amount_off": null, "percent_off": 100} },    "standard": {"emba_875": {"id": "discount_100_percent", "amount_off": null, "percent_off": 100}, "emba_once_9600": {"id": "discount_100_percent", "amount_off": null, "percent_off": 100} },    "early": {"emba_875": {"id": "discount_early_100_percent", "amount_off": null, "percent_off": 100}, "emba_once_9600": {"id": "discount_early_100_percent", "amount_off": null, "percent_off": 100} }}\'::json'


        # Add new multi-plan configuration
        stripe_plans_assignment_sql =
                'stripe_plans = ARRAY[
                    \'{"id": "emba_875", "name": "Monthly Plan", "amount": "87500", "frequency": "monthly"}\'::json,
                    \'{"id": "emba_once_9600", "name": "Single Payment", "amount": "960000", "frequency": "once"}\'::json
                ]'

        # and scholarship levels
        scholarship_levels_assignment_sql =
                "scholarship_levels = ARRAY[
                   #{base_level_sql},
                   #{level_1_sql},
                   #{level_2_sql},
                   #{level_3_sql},
                   #{level_4_sql},
                   #{level_5_sql},
                   #{full_level_sql}
                ]"

        # update templates and versions
        ["curriculum_templates", "curriculum_templates_versions"].each do |table|
            execute "UPDATE #{table} SET #{stripe_plans_assignment_sql}, #{scholarship_levels_assignment_sql} WHERE program_type = 'emba'"
        end

        # update relevant cohorts and versions
        ["cohorts", "cohorts_versions"].each do |table|
            execute "UPDATE #{table} SET #{stripe_plans_assignment_sql}, #{scholarship_levels_assignment_sql} WHERE name IN ('EMBA11', 'EMBA12')"
        end

        # update relevant cohort_applications
        recent_emba_non_accepted_clause = "status NOT IN ('pre_accepted', 'accepted') AND cohort_id IN (SELECT id FROM cohorts WHERE name IN ('EMBA11', 'EMBA12'))"
        execute "UPDATE cohort_applications SET #{stripe_plans_assignment_sql}, #{scholarship_levels_assignment_sql} WHERE #{recent_emba_non_accepted_clause}"

        # iterate through and re-assign scholarship_level based on current level name
        execute "UPDATE cohort_applications SET scholarship_level = (CASE
                    WHEN scholarship_level::text ILIKE '%No Scholarship%' THEN #{base_level_sql}
                    WHEN scholarship_level::text ILIKE '%Level 1%' THEN #{level_1_sql}
                    WHEN scholarship_level::text ILIKE '%Level 2%' THEN #{level_2_sql}
                    WHEN scholarship_level::text ILIKE '%Level 3%' THEN #{level_3_sql}
                    WHEN scholarship_level::text ILIKE '%Level 4%' THEN #{level_4_sql}
                    WHEN scholarship_level::text ILIKE '%Level 5%' THEN #{level_5_sql}
                    WHEN scholarship_level::text ILIKE '%Full Scholarship%' THEN #{full_level_sql}
                END) WHERE #{recent_emba_non_accepted_clause}"



    end

    def down

        # pre-determined scholarship levels
        base_level_sql = '\'{ "name": "No Scholarship", "coupons": { "emba_800": { "id": "none", "amount_off": 0, "percent_off": 0 }, "emba_bi_4800": { "id": "discount_600", "amount_off": 60000, "percent_off": null }, "emba_once_9600": { "id": "discount_1600", "amount_off": 160000, "percent_off": null } } }\'::json'
        level_1_sql    = '\'{ "name": "Level 1", "coupons": { "emba_800": { "id": "discount_150", "amount_off": 15000, "percent_off": null }, "emba_bi_4800": { "id": "discount_1400", "amount_off": 140000, "percent_off": null }, "emba_once_9600": { "id": "discount_3100", "amount_off": 310000, "percent_off": null } } }\'::json'
        level_2_sql    = '\'{ "name": "Level 2", "coupons": { "emba_800": { "id": "discount_250", "amount_off": 25000, "percent_off": null }, "emba_bi_4800": { "id": "discount_1900", "amount_off": 190000, "percent_off": null }, "emba_once_9600": { "id": "discount_4100", "amount_off": 410000, "percent_off": null } } }\'::json'
        level_3_sql    = '\'{ "name": "Level 3", "coupons": { "emba_800": { "id": "discount_350", "amount_off": 35000, "percent_off": null }, "emba_bi_4800": { "id": "discount_2400", "amount_off": 240000, "percent_off": null }, "emba_once_9600": { "id": "discount_5100", "amount_off": 510000, "percent_off": null } } }\'::json'
        level_4_sql    = '\'{ "name": "Level 4", "coupons": { "emba_800": { "id": "discount_450", "amount_off": 45000, "percent_off": null }, "emba_bi_4800": { "id": "discount_2950", "amount_off": 295000, "percent_off": null }, "emba_once_9600": { "id": "discount_6100", "amount_off": 610000, "percent_off": null } } }\'::json'
        level_5_sql    = '\'{ "name": "Level 5", "coupons": { "emba_800": { "id": "discount_550", "amount_off": 55000, "percent_off": null }, "emba_bi_4800": { "id": "discount_3500", "amount_off": 350000, "percent_off": null }, "emba_once_9600": { "id": "discount_7200", "amount_off": 720000, "percent_off": null } } }\'::json'
        full_level_sql = '\'{ "name": "Full Scholarship", "coupons": { "emba_800": { "id": "discount_100_percent", "amount_off": null, "percent_off": 100 }, "emba_bi_4800": { "id": "discount_100_percent", "amount_off": null, "percent_off": 100 }, "emba_once_9600": { "id": "discount_100_percent", "amount_off": null, "percent_off": 100 } } }\'::json'


        # Add new multi-plan configuration
        stripe_plans_assignment_sql =
                'stripe_plans = ARRAY[
                    \'{ "id": "emba_800", "name": "Monthly Plan", "amount": "80000", "frequency": "monthly" }\'::json,
                    \'{ "id": "emba_bi_4800", "name": "Bi-Annual Plan", "amount": "4800", "frequency": "bi_annual" }\'::json,
                    \'{ "id": "emba_once_9600", "name": "Single Payment", "amount": "960000", "frequency": "once" }\'::json
                ]'

        # and scholarship levels
        scholarship_levels_assignment_sql =
                "scholarship_levels = ARRAY[
                   #{base_level_sql},
                   #{level_1_sql},
                   #{level_2_sql},
                   #{level_3_sql},
                   #{level_4_sql},
                   #{level_5_sql},
                   #{full_level_sql}
                ]"

        # update templates and versions
        ["curriculum_templates", "curriculum_templates_versions"].each do |table|
            execute "UPDATE #{table} SET #{stripe_plans_assignment_sql}, #{scholarship_levels_assignment_sql} WHERE program_type = 'emba'"
        end

        # update relevant cohorts and versions
        ["cohorts", "cohorts_versions"].each do |table|
            execute "UPDATE #{table} SET #{stripe_plans_assignment_sql}, #{scholarship_levels_assignment_sql} WHERE name IN ('EMBA11', 'EMBA12')"
        end

        # update relevant cohort_applications
        recent_emba_clause = "cohort_id IN (SELECT id FROM cohorts WHERE name IN ('EMBA11', 'EMBA12'))"
        execute "UPDATE cohort_applications SET #{stripe_plans_assignment_sql}, #{scholarship_levels_assignment_sql} WHERE #{recent_emba_clause}"

        # iterate through and re-assign scholarship_level based on current level name
        execute "UPDATE cohort_applications SET scholarship_level = (CASE
                    WHEN scholarship_level::text ILIKE '%No Scholarship%' THEN #{base_level_sql}
                    WHEN scholarship_level::text ILIKE '%Level 1%' THEN #{level_1_sql}
                    WHEN scholarship_level::text ILIKE '%Level 2%' THEN #{level_2_sql}
                    WHEN scholarship_level::text ILIKE '%Level 3%' THEN #{level_3_sql}
                    WHEN scholarship_level::text ILIKE '%Level 4%' THEN #{level_4_sql}
                    WHEN scholarship_level::text ILIKE '%Level 5%' THEN #{level_5_sql}
                    WHEN scholarship_level::text ILIKE '%Full Scholarship%' THEN #{full_level_sql}
                END) WHERE #{recent_emba_clause}"

    end

end
