class AddCompletedExpectedChargesView < ActiveRecord::Migration[5.2]
    def up
        ViewHelpers.migrate(self, 20190410151654)
    end
    def down
        ViewHelpers.rollback(self, 20190410151654)
    end
end
