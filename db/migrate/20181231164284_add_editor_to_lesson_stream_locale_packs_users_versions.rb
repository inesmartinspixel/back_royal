
class AddEditorToLessonStreamLocalePacksUsersVersions < ActiveRecord::Migration[5.2]

    def up
        add_editor_columns_to_versions_table("lesson_stream_locale_packs_users")
    end

    def down
        rollback_editor_columns_from_versions_table("lesson_stream_locale_packs_users")
    end
end
