class AddCandidatePositionIdToMailboxerConversations < ActiveRecord::Migration[5.2]

    def change
        add_column :mailboxer_conversations, :candidate_position_interest_id, :uuid
    end
end
