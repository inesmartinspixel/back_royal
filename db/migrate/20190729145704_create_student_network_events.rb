class CreateStudentNetworkEvents < ActiveRecord::Migration[5.2]

    def up

        create_table :student_network_events, id: :uuid do |t|
            t.timestamps
            t.text :title, :null => false
            t.text :description, :null => false
            t.text :event_type, :null => false
            t.datetime :start_time, :null => false
            t.datetime :end_time, :null => false
            t.uuid :image_id
            t.boolean :rsvp_required, :null => false, :default => false
            t.text :external_rsvp_url
            t.text :place_id
            t.text :place_id_anonymized
            t.json :place_details, :null => false, :default => {}
            t.json :place_details_anonymized, :null => false, :default => {}
            t.text :location_name
            t.boolean :published, :null => false, :default => false
            t.datetime :published_at
            t.text :internal_notes
            t.uuid :author_id, :null => false
            t.boolean :visible_to_current_degree_students, :null => false, :default => true
            t.boolean :visible_to_graduated_degree_students, :null => false, :default => true
            t.string :visible_to_accepted_degree_students_in_cohorts, array: true, :null => false, default: []
            t.boolean :visible_to_non_degree_users, :null => false, :default => false
        end

        create_versions_table_and_trigger_v2(:student_network_events)

        # PostGIS datatypes
        execute postgis_datatype_sql

        # Update location columns
        execute location_trigger_sql

        # Fulltext support
        create_table :student_network_event_fulltext, id: false, force: :cascade  do |t|
            t.timestamps
            t.uuid :student_network_event_id, :null => false
            t.tsvector :full_keyword_vector
            t.text :full_keyword_text
            t.tsvector :anonymous_keyword_vector
            t.text :anonymous_keyword_text
        end
        add_foreign_key :student_network_event_fulltext, :student_network_events
        add_index :student_network_event_fulltext, :student_network_event_id, name: "student_network_event_fulltext_on_student_network_event_id"
        add_index :student_network_event_fulltext, :full_keyword_vector, using: :gin, name: "student_network_event_fulltext_full_keyword_vector"
        add_index :student_network_event_fulltext, :anonymous_keyword_vector, using: :gin, name: "student_network_event_fulltext_anonymous_keyword_vector"
        execute "CREATE INDEX student_network_event_fulltext_full_keyword_text ON student_network_event_fulltext USING GIN (full_keyword_text gin_trgm_ops)"
        execute "CREATE INDEX student_network_event_fulltext_anonymous_keyword_text ON student_network_event_fulltext USING GIN (anonymous_keyword_text gin_trgm_ops)"

    end

    def down
        remove_foreign_key :student_network_event_fulltext, :student_network_events
        drop_table :student_network_event_fulltext
        drop_table :student_network_events_versions
        drop_table :student_network_events
    end

    def postgis_datatype_sql
        # We currently have to use sql to add datatypes exposed by the PostGIS adapter.
        # We could update our application's config in such a way that we don't have to do
        # this going forward, but the refactor looks extensive/scary.
        %Q{
            create extension if not exists postgis;

            ALTER TABLE student_network_events ADD COLUMN location geography(POINT,4326);
            ALTER TABLE student_network_events ADD COLUMN location_anonymized geography(POINT,4326);

            ALTER TABLE student_network_events_versions ADD COLUMN location geography(POINT,4326);
            ALTER TABLE student_network_events_versions ADD COLUMN location_anonymized geography(POINT,4326);

            CREATE INDEX student_network_events_on_location
                ON student_network_events
                USING GIST (location);
            CREATE INDEX student_network_events_on_location_anonymized
                ON student_network_events
                USING GIST (location_anonymized);
        }
    end

    def location_trigger_sql
        # DB triggers to update `location` and `location_anonymized` columns
        %Q{
            CREATE OR REPLACE FUNCTION update_student_network_event_location() RETURNS TRIGGER AS $update_student_network_event_location$
                BEGIN
                    IF (TG_OP = 'UPDATE' OR TG_OP = 'INSERT') THEN
                        NEW.location = CASE WHEN
                                (NEW.place_details->>'lng')::text IS NOT NULL AND (NEW.place_details->>'lat')::text IS NOT NULL
                            THEN
                                ST_GeographyFromText('POINT(' || (NEW.place_details->>'lng')::text || ' ' || (NEW.place_details->>'lat')::text || ')')
                            ELSE
                                NULL
                            END;
                    END IF;
                    RETURN NEW;
                END;
            $update_student_network_event_location$ LANGUAGE plpgsql;

            CREATE TRIGGER update_student_network_event_location
            BEFORE INSERT OR UPDATE ON student_network_events
            FOR EACH ROW EXECUTE PROCEDURE update_student_network_event_location();

            CREATE OR REPLACE FUNCTION update_student_network_event_location_anonymized() RETURNS TRIGGER AS $update_student_network_event_location_anonymized$
                BEGIN
                    IF (TG_OP = 'UPDATE' OR TG_OP = 'INSERT') THEN
                        NEW.location_anonymized = CASE WHEN
                                (NEW.place_details_anonymized->>'lng')::text IS NOT NULL AND (NEW.place_details_anonymized->>'lat')::text IS NOT NULL
                            THEN
                                ST_GeographyFromText('POINT(' || (NEW.place_details_anonymized->>'lng')::text || ' ' || (NEW.place_details_anonymized->>'lat')::text || ')')
                            ELSE
                                NULL
                            END;
                    END IF;
                    RETURN NEW;
                END;
            $update_student_network_event_location_anonymized$ LANGUAGE plpgsql;

            CREATE TRIGGER update_student_network_event_location_anonymized
            BEFORE INSERT OR UPDATE ON student_network_events
            FOR EACH ROW EXECUTE PROCEDURE update_student_network_event_location_anonymized();
        }
    end

end
