class AddDomainToTeams < ActiveRecord::Migration[5.2]

    def up
        add_column :hiring_teams, :domain, :text
        add_column :hiring_teams_versions, :domain, :text
        recreate_trigger_v2(:hiring_teams)
    end

    def down

        remove_column :hiring_teams, :domain, :text
        remove_column :hiring_teams_versions, :domain, :text
        recreate_trigger_v2(:hiring_teams)
    end
end
