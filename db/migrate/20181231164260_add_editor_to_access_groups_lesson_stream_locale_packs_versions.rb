
class AddEditorToAccessGroupsLessonStreamLocalePacksVersions < ActiveRecord::Migration[5.2]

    def up
        add_editor_columns_to_versions_table("access_groups_lesson_stream_locale_packs")
    end

    def down
        rollback_editor_columns_from_versions_table("access_groups_lesson_stream_locale_packs")
    end
end
