class AddAirtableBaseKeyToCohortApplications < ActiveRecord::Migration[5.1]
    def change
        add_column :cohort_applications, :airtable_base_key, :string
        add_column :cohort_applications_versions, :airtable_base_key, :string
    end
end
