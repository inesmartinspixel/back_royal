class AddCohortStatusToStudentNetworkInclusions < ActiveRecord::Migration[5.2]
    def change
        add_column :student_network_inclusions, :status, :text
    end
end
