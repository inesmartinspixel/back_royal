class AddSumRanges < ActiveRecord::Migration[5.1]
    def up
        execute %Q~

            -- This creates an aggregator that takes a list of
            -- timestamp ranges and determines the total amount of time
            -- included in all of them together, accounting for overlaps.
            --
            -- see sum_ranges_spec.rb for an example

            -- create a custom type for use inside the aggregator that
            -- holds the total value and the current range
            CREATE TYPE sum_tsrange_stype AS (
                total               interval,
                current_range       tsrange
            );

            -- create the function that the aggregator uses.  Each input range
            -- will be passed into this function one-by-one, and an instance
            -- of the custom type created above will be returned (just like inject
            -- in ruby or underscore)
            create function sum_tsrange_sfunc(state sum_tsrange_stype, next_range tsrange)
                RETURNS sum_tsrange_stype
                as $$
                    begin

                        -- The ranges that are passed in have to be ordered, otherwise
                        -- the logic does not work
                        IF lower(state.current_range) > lower(next_range) THEN
                            RAISE EXCEPTION 'ranges must be ordered with the lowest values for lower() first';

                        -- This block gets hit for the first range.  The total will stay
                        -- at 0 and we just set the range that was passed in as the current_range
                        elseif state.current_range is null then
                            return (state.total, next_range)::sum_tsrange_stype;

                        -- If the next range overlaps with the current range, then extend the current
                        -- range to include the next range
                        elseif state.current_range && next_range then
                            return (state.total, state.current_range + next_range)::sum_tsrange_stype;

                        -- If the next range does not overlap with the current range, then add
                        -- the size of the current range as the total and set current_range = next_range
                        else
                            return (state.total + (upper(state.current_range) - lower(state.current_range)), next_range)::sum_tsrange_stype;
                        END IF;
                    end
                $$ language plpgsql;

            -- this function is run once at the end.  We add the size of the last
            -- range to the total and then return the total
            create function sum_tsrange_ffunc(state sum_tsrange_stype)
                RETURNS interval
                    language sql
                as $FUNCTION$
                    select (sum_tsrange_sfunc(state, null)).total
                $FUNCTION$;

            create aggregate sum_ranges (tsrange)
            (
                sfunc = sum_tsrange_sfunc,
                stype = sum_tsrange_stype,
                finalfunc = sum_tsrange_ffunc,
                initcond = '(''0 day'',)'
            );
        ~
    end

    def down
        execute %Q~
            drop aggregate sum_ranges(tsrange);
            drop function sum_tsrange_ffunc(state sum_tsrange_stype);
            drop function sum_tsrange_sfunc(state sum_tsrange_stype, next_range tsrange);
            drop type sum_tsrange_stype;
        ~
    end
end
