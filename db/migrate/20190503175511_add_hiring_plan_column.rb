class AddHiringPlanColumn < ActiveRecord::Migration[5.2]
    def up
        add_column :hiring_teams, :hiring_plan, :text
        add_column :hiring_teams_versions, :hiring_plan, :text
        recreate_trigger_v2(:hiring_teams)
    end

    def down

        remove_column :hiring_teams, :hiring_plan, :text
        remove_column :hiring_teams_versions, :hiring_plan, :text
        recreate_trigger_v2(:hiring_teams)
    end
end
