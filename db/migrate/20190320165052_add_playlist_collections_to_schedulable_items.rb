class AddPlaylistCollectionsToSchedulableItems < ActiveRecord::Migration[5.2]

    # We don't have a dedicated model for the `schedulable_items` table,
    # so we just create one here on the fly since it's only needed for tests
    class SchedulableItem < ActiveRecord::Base; end

    def change
        if Rails.env.test?
            add_column :schedulable_items, :playlist_collections, :json, array: true
            change_column_default :schedulable_items, :playlist_collections, []


            SchedulableItem.update_all(playlist_collections: [])

            change_column_null :schedulable_items, :playlist_collections, false
        end
    end
end
