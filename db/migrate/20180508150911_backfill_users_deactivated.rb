class BackfillUsersDeactivated < ActiveRecord::Migration[5.1]
  disable_ddl_transaction!

  def up
  	User.in_batches.update_all(deactivated: false)
  	change_column_null :users, :deactivated, false
  end

  def down
  	change_column_null :users, :deactivated, true
  end

end
