class SplitEconomistHowDidYouHearValues < ActiveRecord::Migration[5.1]

    disable_ddl_transaction!

    def up
        # i'm assuming identifies are important here
        User.where(:how_did_you_hear_about_us => 'economist').each do |user|
            user.how_did_you_hear_about_us = 'economist_digital'
            user.save!
        end
    end

    def down
        User.where("how_did_you_hear_about_us ILIKE 'economist%'").each do |user|
            user.how_did_you_hear_about_us = 'economist'
            user.save!
        end
    end
end
