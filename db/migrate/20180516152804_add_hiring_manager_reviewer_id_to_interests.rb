class AddHiringManagerReviewerIdToInterests < ActiveRecord::Migration[5.1]
    def change
        add_column :candidate_position_interests, :hiring_manager_reviewer_id, :uuid
        add_column :candidate_position_interests_versions, :hiring_manager_reviewer_id, :uuid
    end
end
