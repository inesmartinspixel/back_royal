class MakeDegreeNotRequired < ActiveRecord::Migration[5.1]
    def change
        change_column_null "education_experiences", :degree, true
        change_column_null "education_experiences_versions", :degree, true
    end
end
