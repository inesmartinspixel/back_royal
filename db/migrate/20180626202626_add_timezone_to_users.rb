class AddTimezoneToUsers < ActiveRecord::Migration[5.1]

    def up
        add_column :users, :timezone, :text
        add_column :users_versions, :timezone, :text

        recreate_users_trigger_v1
    end

    def down
        remove_column :users, :timezone
        remove_column :users_versions, :timezone

        recreate_users_trigger_v1
    end
end
