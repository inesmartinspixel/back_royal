class ChangePlaylistCollectionsToBeNotNull < ActiveRecord::Migration[5.2]

    def up
        change_column_null :cohorts, :playlist_collections, false
        change_column_null :curriculum_templates, :playlist_collections, false
    end

    def down
        change_column_null :cohorts, :playlist_collections, true
        change_column_null :curriculum_templates, :playlist_collections, true
    end
end
