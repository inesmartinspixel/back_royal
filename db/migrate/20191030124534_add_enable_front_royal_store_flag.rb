class AddEnableFrontRoyalStoreFlag < ActiveRecord::Migration[6.0]
    def up
        add_column :users, :enable_front_royal_store, :boolean, :null => false, default: false
        add_column :users_versions, :enable_front_royal_store, :boolean

        recreate_users_trigger_v3
    end

    def down
        remove_column :users, :enable_front_royal_store
        remove_column :users_versions, :enable_front_royal_store
        recreate_users_trigger_v3
    end
end