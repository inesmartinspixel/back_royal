class AddMissingUserSearchIndices < ActiveRecord::Migration[5.2]

    disable_ddl_transaction!

    def up
        return if Rails.env.production?

        execute "CREATE INDEX CONCURRENTLY index_users_on_name_like ON users USING GIN (name gin_trgm_ops)"
        execute "CREATE INDEX CONCURRENTLY index_users_on_nickname_like ON users USING GIN (nickname gin_trgm_ops)"
        execute "CREATE INDEX CONCURRENTLY index_users_on_email_like ON users USING GIN (email gin_trgm_ops)"
        execute "CREATE INDEX CONCURRENTLY index_institutions_on_name_like ON institutions USING GIN (name gin_trgm_ops)"
        execute "CREATE INDEX CONCURRENTLY index_cohorts_on_name_like ON cohorts USING GIN (name gin_trgm_ops)"
        execute "CREATE INDEX CONCURRENTLY index_roles_on_name_like ON roles USING GIN (name gin_trgm_ops)"
        execute "CREATE INDEX CONCURRENTLY index_access_groups_on_name_like ON access_groups USING GIN (name gin_trgm_ops)"
    end

    def down
        return if Rails.env.production?

        execute "DROP INDEX index_users_on_name_like"
        execute "DROP INDEX index_users_on_nickname_like"
        execute "DROP INDEX index_users_on_email_like"
        execute "DROP INDEX index_institutions_on_name_like"
        execute "DROP INDEX index_cohorts_on_name_like"
        execute "DROP INDEX index_roles_on_name_like"
        execute "DROP INDEX index_access_groups_on_name_like"
    end

end