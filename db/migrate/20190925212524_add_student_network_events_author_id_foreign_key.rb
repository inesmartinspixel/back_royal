class AddStudentNetworkEventsAuthorIdForeignKey < ActiveRecord::Migration[6.0]
    def change
        add_foreign_key :student_network_events, :users, column: :author_id, primary_key: :id
    end
end
