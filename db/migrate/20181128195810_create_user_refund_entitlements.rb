class CreateUserRefundEntitlements < ActiveRecord::Migration[5.2]
    def change
        create_table :user_refund_entitlements, :id => :uuid do |t|
            t.timestamps
            t.uuid :user_id                                      , null: false
            t.timestamp :updated_to_last_lesson_activity_time    , null: true
            t.timestamp :updated_to_max_application_updated_at   , null: true
            t.float :refund_amount                               , null: true
        end

        add_index :user_refund_entitlements, :user_id, :unique => true, :name => :uniq_on_user_refund_entitlements
    end
end
