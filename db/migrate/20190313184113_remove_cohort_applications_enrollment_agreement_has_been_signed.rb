class RemoveCohortApplicationsEnrollmentAgreementHasBeenSigned < ActiveRecord::Migration[5.2]

    def up

        ViewHelpers.remove_view_temporarily_while(self, ViewHelpers::ApplicationsThatProvideStudentNetworkInclusion) do
            remove_column :cohort_applications, :enrollment_agreement_has_been_signed
            remove_column :cohort_applications_versions, :enrollment_agreement_has_been_signed
        end

        recreate_trigger_v2 "cohort_applications"
    end

    def down
        ViewHelpers.remove_view_temporarily_while(self, ViewHelpers::ApplicationsThatProvideStudentNetworkInclusion) do
            add_column :cohort_applications, :enrollment_agreement_has_been_signed, :boolean
            add_column :cohort_applications_versions, :enrollment_agreement_has_been_signed, :boolean
        end

        change_column_default :cohort_applications, :enrollment_agreement_has_been_signed, false

        recreate_trigger_v2 "cohort_applications"
    end
end
