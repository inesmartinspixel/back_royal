class AddOpenPositionsLocationIndex < ActiveRecord::Migration[5.2]
    disable_ddl_transaction!

    def up
        execute %Q{
            CREATE INDEX CONCURRENTLY open_positions_on_location
                ON open_positions
                USING GIST (location);
        }
    end

    def down
        execute %Q~
            DROP INDEX open_positions_on_location;
        ~
    end
end
