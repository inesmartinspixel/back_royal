class AddS3AssetFkIndices < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        add_index :s3_assets, :directory, algorithm: :concurrently

        add_index :lesson_streams_progress, :certificate_image_id, algorithm: :concurrently

        add_index :career_profiles, :linkedin_pdf_id, algorithm: :concurrently
        add_index :career_profiles, :resume_id, algorithm: :concurrently
    end

    def down
        remove_index :s3_assets, :directory

        remove_index :lesson_streams_progress, :certificate_image_id

        remove_index :career_profiles, :linkedin_pdf_id
        remove_index :career_profiles, :resume_id
    end

end
