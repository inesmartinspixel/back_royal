class RemoveStudentNetworkMembersView < ActiveRecord::Migration[5.2]
    def up
        ViewHelpers.migrate(self, 20190619181948)
    end

    def down
        ViewHelpers.rollback(self, 20190619181948)
    end
end
