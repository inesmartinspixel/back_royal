class AddAllowChangePassword < ActiveRecord::Migration[5.2]

    def up
        add_column :users, :allow_password_change, :boolean
        change_column_default :users, :allow_password_change, false

        add_column :users_versions, :allow_password_change, :boolean
        recreate_users_trigger_v2
    end

    def down
        remove_column :users_versions, :allow_password_change
        remove_column :users, :allow_password_change
        recreate_users_trigger_v2
    end
end
