class AddNotNullConstraintToAcademicHold < ActiveRecord::Migration[5.2]

    def up
        change_column_null :users, :academic_hold, false
    end

    def down
        change_column_null :users, :academic_hold, true
    end

end
