class AddAllowPasswordChangeDefault < ActiveRecord::Migration[5.2]
    def up
        change_column_null :users, :allow_password_change, false
    end
    def down
        change_column_null :users, :allow_password_change, true
    end
end