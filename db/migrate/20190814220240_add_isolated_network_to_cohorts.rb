class AddIsolatedNetworkToCohorts < ActiveRecord::Migration[5.2]

    def up
        add_column :cohorts, :isolated_network, :boolean, default: false, null: false
        add_column :cohorts_versions, :isolated_network, :boolean
        recreate_trigger_v2(:cohorts)
    end

    def down
        remove_column :cohorts, :isolated_network
        remove_column :cohorts_versions, :isolated_network
        recreate_trigger_v2(:cohorts)
    end

end
