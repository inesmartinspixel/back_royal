class AddNewProjectColumns < ActiveRecord::Migration[5.2]

    def up
        add_column :project_progress, :status, :text
        change_column_default :project_progress, :status, 'unsubmitted'
        add_column :project_progress, :id_verified, :boolean

        add_column :learner_projects, :project_type, :text
        change_column_default :learner_projects, :project_type, 'standard'
    end

    def down
        remove_column :project_progress, :status
        remove_column :project_progress, :id_verified

        remove_column :learner_projects, :project_type
    end

end
