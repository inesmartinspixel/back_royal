class AddTimezoneToStudentNetworkEvents < ActiveRecord::Migration[5.2]

    def up
        add_column :student_network_events, :timezone, :text
        add_column :student_network_events_versions, :timezone, :text
        recreate_trigger_v2(:student_network_events)
    end

    def down
        remove_column :student_network_events, :timezone
        remove_column :student_network_events_versions, :timezone
        recreate_trigger_v2(:student_network_events)
    end

end
