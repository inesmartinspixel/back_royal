class RemoveOldShortAnswerAndCareerGoalsColumnsFromCareerProfiles < ActiveRecord::Migration[5.2]

    def up
        remove_column :career_profiles, :short_answer_greatest_strength
        remove_column :career_profiles, :short_answer_professional_future
        remove_column :career_profiles, :short_answer_ideal_first_job
        remove_column :career_profiles, :short_answer_personal_passions
        remove_column :career_profiles, :career_goals

        remove_column :career_profiles_versions, :short_answer_greatest_strength
        remove_column :career_profiles_versions, :short_answer_professional_future
        remove_column :career_profiles_versions, :short_answer_ideal_first_job
        remove_column :career_profiles_versions, :short_answer_personal_passions
        remove_column :career_profiles_versions, :career_goals

        recreate_trigger_v2(:career_profiles)
    end

    def down
        add_column :career_profiles, :short_answer_greatest_strength, :text
        add_column :career_profiles, :short_answer_professional_future, :text
        add_column :career_profiles, :short_answer_ideal_first_job, :text
        add_column :career_profiles, :short_answer_personal_passions, :text
        add_column :career_profiles, :career_goals, :text

        add_column :career_profiles_versions, :short_answer_greatest_strength, :text
        add_column :career_profiles_versions, :short_answer_professional_future, :text
        add_column :career_profiles_versions, :short_answer_ideal_first_job, :text
        add_column :career_profiles_versions, :short_answer_personal_passions, :text
        add_column :career_profiles_versions, :career_goals, :text

        recreate_trigger_v2(:career_profiles)
    end
end
