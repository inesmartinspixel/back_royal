class BackfillPrefOneClickReachout < ActiveRecord::Migration[5.1]

    # we are disabling transactions so that we don't update ever row in the
    # users table in a single transaction.  In order for this to be
    # ok, we need to make sure everything in this migration is idempotent
    disable_ddl_transaction!

    def up

        # The default is false, because we want new hiring managers to not have this.  But
        # we want existing users to have it.
        time = User.minimum(:created_at)

        while time < Time.now + 1.month
            User.where("created_at >= ? and created_at <= ?", time, time + 1.month)
                .where(pref_one_click_reach_out: nil)
                .update_all(pref_one_click_reach_out: true)

            puts "updated all users created before #{time}"
            time += 1.month
        end

        # Set nullable. This is idempotent.  Even if somehow we run this and then
        # schema_migrations does not get updated, nothing bad will happen.
        change_column_null :users, :pref_one_click_reach_out, false
    end

    def down
        change_column_null :users, :pref_one_click_reach_out, true
    end
end