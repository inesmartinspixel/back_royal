class ChangeTypeOfVisibleColumn < ActiveRecord::Migration[5.2]
    def up
        remove_column :student_network_events, :visible_to_accepted_degree_students_in_cohorts
        remove_column :student_network_events_versions, :visible_to_accepted_degree_students_in_cohorts

        add_column :student_network_events, :visible_to_accepted_degree_students_in_cohorts, :uuid, array: true, default: [], null: false
        add_column :student_network_events_versions, :visible_to_accepted_degree_students_in_cohorts, :uuid, array: true, default: [], null: false
    end

    def down
        remove_column :student_network_events, :visible_to_accepted_degree_students_in_cohorts
        remove_column :student_network_events_versions, :visible_to_accepted_degree_students_in_cohorts

        add_column :student_network_events, :visible_to_accepted_degree_students_in_cohorts, :string, array: true, default: [], null: false
        add_column :student_network_events_versions, :visible_to_accepted_degree_students_in_cohorts, :string, array: true, default: [], null: false
    end
end
