class RemoveHiringApplicationAutoAssignCols < ActiveRecord::Migration[5.2]
    def up
        remove_column :hiring_applications, :auto_assign_candidates
        remove_column :hiring_applications, :targeted_career_profile_ids
        remove_column :hiring_applications, :processed_targeted_career_profile_ids
        remove_column :hiring_applications, :viewed_career_profile_list_ids

        remove_column :hiring_applications_versions, :auto_assign_candidates
        remove_column :hiring_applications_versions, :targeted_career_profile_ids
        remove_column :hiring_applications_versions, :processed_targeted_career_profile_ids
        remove_column :hiring_applications_versions, :viewed_career_profile_list_ids

        recreate_trigger_v2(:hiring_applications)
    end

    def down
        add_column :hiring_applications, :auto_assign_candidates, :boolean, default: true
        add_column :hiring_applications, :targeted_career_profile_ids, :uuid, array: true, null: false, default: '{}'
        add_column :hiring_applications, :processed_targeted_career_profile_ids, :boolean, default: false, null: false
        add_column :hiring_applications, :viewed_career_profile_list_ids, :uuid, array: true, null: false, default: '{}'

        add_column :hiring_applications_versions, :auto_assign_candidates, :boolean
        add_column :hiring_applications_versions, :targeted_career_profile_ids, :uuid, array: true
        add_column :hiring_applications_versions, :processed_targeted_career_profile_ids, :boolean
        add_column :hiring_applications_versions, :viewed_career_profile_list_ids, :uuid, array: true

        recreate_trigger_v2(:hiring_applications)

    end
end
