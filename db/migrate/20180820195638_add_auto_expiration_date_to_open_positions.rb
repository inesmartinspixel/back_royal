class AddAutoExpirationDateToOpenPositions < ActiveRecord::Migration[5.1]

    def up
        add_column :open_positions, :auto_expiration_date, :date
        add_column :open_positions_versions, :auto_expiration_date, :date
    end

    def down
        remove_column :open_positions, :auto_expiration_date
        remove_column :open_positions_versions, :auto_expiration_date
    end
end
