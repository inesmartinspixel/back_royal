class UpdateUsersHasSeenShortAnswerWarningNotNull < ActiveRecord::Migration[5.2]

    def up
        change_column_null :users, :has_seen_short_answer_warning, false
    end

    def down
        change_column_null :users, :has_seen_short_answer_warning, true
    end

end
