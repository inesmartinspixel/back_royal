class AddLongTermGoalToCareerProfiles < ActiveRecord::Migration[5.1]
    def change
        add_column :career_profiles, :long_term_goal, :text
        add_column :career_profiles_versions, :long_term_goal, :text
    end
end
