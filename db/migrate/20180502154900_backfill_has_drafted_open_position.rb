class BackfillHasDraftedOpenPosition < ActiveRecord::Migration[5.1]
  
  disable_ddl_transaction!

  def up
    # Backfill (idempotent)
    User.in_batches.update_all has_drafted_open_position: false
    # backfill this to true for hiring managers that have already created an open position
    User.joins(:open_positions).in_batches.update_all has_drafted_open_position: true

    # Set nullable (idempotent)
    change_column_null :users, :has_drafted_open_position, false
  end

  def down
    change_column_null :users, :has_drafted_open_position, true
  end

end
