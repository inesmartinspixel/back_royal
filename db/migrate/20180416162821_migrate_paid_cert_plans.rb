class MigratePaidCertPlans < ActiveRecord::Migration[5.1]

    def up
        # removing this because it was failing on ci, and it doesn't change the schema so it is unneeded

        # Cohort.where(program_type: paid_cert_program_types).edit_and_republish do |cohort|
        #     cert_plan_id = cohort.program_type_config.stripe_plan_config.keys.first.to_s
        #     cohort.stripe_plans = [{"id"=>cert_plan_id, "amount"=>150000, "frequency"=>"once", "name"=>"Single Payment"}]
        # end
    end

    def down
        # Cohort.where(program_type: paid_cert_program_types).edit_and_republish do |cohort|
        #     cohort.stripe_plans = [{"id"=>"cert_once_1500", "amount"=>150000, "frequency"=>"once", "name"=>"Single Payment"}]
        #     cohort.publish!
        # end
    end

    def paid_cert_program_types
        Cohort::ProgramTypeConfig.configs.to_a
            .select(&:is_paid_cert?)
            .map(&:program_type)
    end
end
