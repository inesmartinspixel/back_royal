class RemoveInteralColumnsFromCareerProfile < ActiveRecord::Migration[5.1]

    def change
        remove_column :career_profiles, :internal_quality_score, :text
        remove_column :career_profiles_versions, :internal_quality_score, :text

        remove_column :career_profiles, :internal_employment_notes, :text
        remove_column :career_profiles_versions, :internal_employment_notes, :text
    end
end
