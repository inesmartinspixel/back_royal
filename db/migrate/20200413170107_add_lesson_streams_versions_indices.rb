class AddLessonStreamsVersionsIndices < ActiveRecord::Migration[6.0]

    disable_ddl_transaction!

    def change
        add_index :lesson_streams_versions, :entity_metadata_id, algorithm: :concurrently
        add_index :lesson_streams_versions, :image_id, algorithm: :concurrently
        add_index :lesson_streams_versions, :author_id, algorithm: :concurrently
    end

end
