class AddEmailAndPhoneToIdologyVerification < ActiveRecord::Migration[5.1]
    def change

        add_column :idology_verifications, :email, :text
        add_column :idology_verifications, :phone, :text

    end
end
