class ConvertCohortStatusChangesToTable < ActiveRecord::Migration[6.0]
    def up
        ViewHelpers.migrate(self, 20191125221306) do
            create_table :cohort_status_changes, id: false do |t|
                t.uuid :user_id
                t.uuid :cohort_id
                t.timestamp :from_time
                t.timestamp :until_time
                t.timestamp :processed_up_to
                t.text :status
                t.text :cohort_application_status
                t.text :graduation_status
                t.boolean :foundations_complete
                t.boolean :showed_up
                t.boolean :past_enrollment_deadline
                t.timestamp :accepted_at
                t.boolean :enrolled
                t.boolean :enrolled_late
            end

            add_index :cohort_status_changes, [:user_id, :cohort_id, :from_time, :until_time], :name => :user_cohort_time_on_coh_status_changes, :unique => true
            add_index :cohort_status_changes, [:processed_up_to]
        end
    end

    def down
        ViewHelpers.rollback(self, 20191125221306) do
            drop_table :cohort_status_changes
        end
    end
end
