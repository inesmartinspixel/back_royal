class BackfillManuallyArchivedForOpenPositions < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        OpenPosition.where(archived: true).in_batches.update_all manually_archived: true
        OpenPosition.where(archived: false).in_batches.update_all manually_archived: false

        change_column_null :open_positions, :manually_archived, false
    end

    def down
        change_column_null :open_positions, :manually_archived, true
    end
end
