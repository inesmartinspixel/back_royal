class BackfillRefundAttemptFailed < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        CohortApplication.in_batches.update_all refund_attempt_failed: false
        change_column_null :cohort_applications, :refund_attempt_failed, false
    end

    def down
        change_column_null :cohort_applications, :refund_attempt_failed, true
    end
end
