class AddPaymentGracePeriodEndAtToCohortApplication < ActiveRecord::Migration[6.0]

    def change
        add_column :cohort_applications, :payment_grace_period_end_at, :datetime
        add_column :cohort_applications_versions, :payment_grace_period_end_at, :datetime
    end
end
