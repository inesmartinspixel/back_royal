class AddHiringTeamsInGoodStanding < ActiveRecord::Migration[5.1]
    def change
        add_column :hiring_teams, :subscription_required, :boolean, default: false, null: false
        add_column :hiring_teams_versions, :subscription_required, :boolean
    end

end
