class CreateCareerProfileSearchHelpers < ActiveRecord::Migration[5.1]
    def change
        create_table :career_profile_search_helpers_2, :id => :uuid do |t|
            t.uuid :career_profile_id                                        , null: false
            t.timestamp :current_up_to                                       , null: false
            t.text :work_experience_roles                                    , null: false, array: true
            t.text :work_experience_industries                               , null: false, array: true
            t.interval :past_work_interval                                   , null: true
            t.interval :current_work_interval                                , null: true
            t.text :work_experience_company_names                            , null: true
            t.text :education_experience_school_names                        , null: true
            t.text :current_work_experience_roles                            , null: false, array: true
            t.text :current_work_experience_industries                       , null: false, array: true
            t.boolean :has_current_work_experience                           , null: false
            t.boolean :in_school                                             , null: false
            t.text :skills_texts                                             , null: false, array: true
            t.text :student_network_interests_texts                          , null: false, array: true
            t.integer :accepted_by_hiring_managers_count                     , null: false
            t.float :default_prioritization                                  , null: false
            t.float :years_of_experience                                     , null: true
            t.timestamp :years_of_experience_reference_date                  , null: true
        end

        add_index :career_profile_search_helpers_2, :career_profile_id, :unique => true
        add_index :career_profile_search_helpers_2, :current_up_to
        add_index :career_profile_search_helpers_2, :work_experience_roles, using: :gin, :name => :wexp_roles_on_cp_search_helpers_2
        add_index :career_profile_search_helpers_2, :work_experience_industries, using: :gin, :name => :wexp_industries_on_cp_search_helpers_2
        add_index :career_profile_search_helpers_2, [:years_of_experience, :default_prioritization], order: {years_of_experience: :desc, default_prioritization: :desc}, :name => :years_exp
        add_index :career_profile_search_helpers_2, [:years_of_experience_reference_date, :default_prioritization], order: {years_of_experience_reference_date: :asc, default_prioritization: :desc}, :name => :years_exp_ref_date
        add_index :career_profile_search_helpers_2, :skills_texts, using: :gin, :name => :skills_texts_on_cp_search_helpers_2
        add_index :career_profile_search_helpers_2, :student_network_interests_texts, using: :gin, :name => :student_network_interests_texts_on_cp_search_helpers_2
        # sometimes we order by both in_school and default_prioritization, sometimes just by default_prioritization, so
        # it is good to have both indexes
        add_index :career_profile_search_helpers_2, [:in_school, :default_prioritization], order: {default_prioritization: :desc}, :name => :in_school_and_default_prioritization_on_cp_sear_2
        add_index :career_profile_search_helpers_2, :default_prioritization, :name => :default_prioritization_on_cp_search_helpers_2

        reversible do |dir|
            dir.up do
                execute "CREATE INDEX wexp_company_names_on_cp_search_helpers_2 ON career_profile_search_helpers_2 USING gin (work_experience_company_names gin_trgm_ops)"
                execute "CREATE INDEX edexp_school_names_on_cp_search_helpers_2 ON career_profile_search_helpers_2 USING gin (education_experience_school_names gin_trgm_ops)"
            end
        end
    end
end
