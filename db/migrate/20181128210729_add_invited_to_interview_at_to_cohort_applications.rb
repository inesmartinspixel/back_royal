class AddInvitedToInterviewAtToCohortApplications < ActiveRecord::Migration[5.2]

    def up
        add_column :cohort_applications, :invited_to_interview_at, :datetime
        add_column :cohort_applications_versions, :invited_to_interview_at, :datetime
    end

    def down
        remove_column :cohort_applications, :invited_to_interview_at
        remove_column :cohort_applications_versions, :invited_to_interview_at
    end
end
