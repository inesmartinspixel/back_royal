
class AddEditorToProfessionalOrganizationOptionsVersions < ActiveRecord::Migration[5.2]

    def up
        add_editor_columns_to_versions_table("professional_organization_options")
    end

    def down
        rollback_editor_columns_from_versions_table("professional_organization_options")
    end
end
