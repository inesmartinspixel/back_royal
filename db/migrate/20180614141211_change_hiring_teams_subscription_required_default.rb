class ChangeHiringTeamsSubscriptionRequiredDefault < ActiveRecord::Migration[5.1]
  def up
    change_column_default :hiring_teams, :subscription_required, true
  end

  def down
    change_column_default :hiring_teams, :subscription_required, false
  end
end
