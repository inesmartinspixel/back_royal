class UpdateGraduationRatesPart1 < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20180801123727)
    end

    def down
        ViewHelpers.rollback(self, 20180801123727)
    end
end
