class DropSubscriptionsUserId < ActiveRecord::Migration[5.1]
    def up
        remove_column :subscriptions, :user_id
        remove_column :subscriptions_versions, :user_id
    end

    def down
        add_column :subscriptions, :user_id, :uuid
        add_column :subscriptions_versions, :user_id, :uuid

        add_foreign_key :subscriptions, :users
    end
end
