class RemoveYearsOfExperience < ActiveRecord::Migration[5.1]
    def change
        reversible do |dir|
            dir.up do
                ViewHelpers.migrate(self, 20180117163345)
            end

            dir.down do
                ViewHelpers.rollback(self, 20180117163345)
            end
        end
    end
end