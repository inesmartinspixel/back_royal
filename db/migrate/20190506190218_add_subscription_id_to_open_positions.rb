class AddSubscriptionIdToOpenPositions < ActiveRecord::Migration[5.2]
    def up
        add_column :open_positions, :subscription_id, :uuid
        add_column :open_positions_versions, :subscription_id, :uuid

        add_index :open_positions, :subscription_id, unique: true
        add_foreign_key :open_positions, :subscriptions

        recreate_trigger_v2(:open_positions)
    end

    def down

        remove_column :open_positions, :subscription_id, :uuid
        remove_column :open_positions_versions, :subscription_id, :uuid
        recreate_trigger_v2(:open_positions)
    end
end
