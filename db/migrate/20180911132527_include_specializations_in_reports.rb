class IncludeSpecializationsInReports < ActiveRecord::Migration[5.1]

    def up
        execute %Q~
            CREATE FUNCTION specialization_progress(specializations json, num_required_specializations int, expected_specializations_complete float, previously_completed_lesson_locale_packs uuid[])
            RETURNS json AS $$

                var completedLessonLocalePackIds = {};
                previously_completed_lesson_locale_packs.forEach(function(localePackId){
                    completedLessonLocalePackIds[localePackId] = true;
                });

                specializations.forEach(function(specialization) {
                    specialization.lessons = specialization.lesson_locale_pack_ids.length;
                    specialization.completedLessons = 0;
                    specialization.lesson_locale_pack_ids.forEach(function(localePackId) {
                        if (completedLessonLocalePackIds[localePackId]) {
                            specialization.completedLessons ++;
                        }
                    });
                    specialization.percentComplete = specialization.completedLessons / specialization.lessons;
                });

                specializations = specializations.sort(function(a, b) {
                    if (a.percentComplete > b.percentComplete) {
                        return -1;
                    } else if (a.percentComplete < b.percentComplete) {
                        return 1;
                    } else if (a.lessons < b.lessons) {
                        return -1;
                    } else if (a.lessons > b.lessons) {
                        return 1;
                    } else if (a.playlist_locale_pack_id < b.playlist_locale_pack_id) {
                        return -1;
                    } else {
                        return 1;
                    }
                });

                var selectedSpecializations = specializations.slice(0, num_required_specializations);

                var completedSpecializations = 0;
                var expectedSpecializationLessonsComplete = 0;
                var completedSpecializationLessons = 0;
                selectedSpecializations.forEach(function(specialization, i) {
                    completedSpecializations = completedSpecializations + specialization.percentComplete;
                    completedSpecializationLessons = completedSpecializationLessons + specialization.completedLessons;
                    var expectedLessonsInThisSpecializationComplete = 0;
                    if (i < Math.floor(expected_specializations_complete)) {
                        expectedLessonsInThisSpecializationComplete = specialization.lessons;
                    } else if (i < expected_specializations_complete) {
                        var expectedPercentOfThisSpecializationComplete = expected_specializations_complete % 1;
                        expectedLessonsInThisSpecializationComplete = Math.floor(expectedPercentOfThisSpecializationComplete*specialization.lessons);
                    }
                    expectedSpecializationLessonsComplete = expectedSpecializationLessonsComplete + expectedLessonsInThisSpecializationComplete;
                });

                return {
                    completedSpecializations: completedSpecializations,
                    expectedSpecializationLessonsComplete: expectedSpecializationLessonsComplete,
                    completedSpecializationLessons: completedSpecializationLessons
                };
            $$ LANGUAGE plv8 IMMUTABLE STRICT;
        ~


        # changes to published_cohort_periods to add expected specialization info
        ViewHelpers.migrate(self, 20180911132527)

        create_table :cohort_user_periods, :id => :uuid do |t|
            t.uuid :user_id                                                 , null: false
            t.uuid :cohort_id                                               , null: false
            t.text :cohort_name                                             , null: false
            t.integer :index                                                , null: false
            t.integer :required_lessons_completed_in_period                 , null: false
            t.integer :specialization_lessons_completed_in_period           , null: false
            t.timestamp :period_start                                       , null: false
            t.timestamp :period_end                                         , null: false
            t.uuid :required_lesson_pack_ids_completed_in_period            , null: false, array: true
            t.uuid :specialization_lesson_pack_ids_completed_in_period      , null: false, array: true
            t.text :status_at_period_end                                    , null: false
            t.integer :cumulative_required_lessons_completed                , null: false
            t.integer :cumulative_required_up_to_now_lessons_completed      , null: false
            t.integer :cumulative_required_and_specialization_lessons_completed_in_last_4_periods   , null: false
            t.integer :cumulative_specializations_completed                 , null: false
            t.integer :expected_specialization_lessons_completed            , null: false
            t.integer :cumulative_specialization_lessons_completed          , null: false
            t.integer :cumulative_specialization_up_to_now_lessons_completed    , null: false
            t.integer :total_up_to_now_lessons_completed                    , null: false
            t.integer :total_lessons_expected                               , null: false
            t.timestamp :updated_at                                         , null: false
        end

        add_index :cohort_user_periods, [:user_id, :cohort_id, :index], :unique => true
        add_index :cohort_user_periods, [:period_end, :updated_at]

    end

    def down
        drop_table :cohort_user_periods
        ViewHelpers.rollback(self, 20180911132527)

        execute %Q~
            DROP FUNCTION specialization_progress(specializations json, num_required_specializations int, expected_specializations_complete float, previously_completed_lesson_locale_packs uuid[])
        ~
    end
end