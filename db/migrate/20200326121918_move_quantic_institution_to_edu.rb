class MoveQuanticInstitutionToEdu < ActiveRecord::Migration[6.0]
    def up
        institution&.update!(domain: 'quantic.edu')
    end

    def down
        institution&.update!(domain: 'quantic.mba')
    end

    # We do not use Institution.quantic here so that we do not 
    # end up caching an un-updated version of the institution
    # with the new cache key
    def institution
        Institution.find_by_id(InternalInstitutionMixin::QUANTIC_ID)
    end
end
