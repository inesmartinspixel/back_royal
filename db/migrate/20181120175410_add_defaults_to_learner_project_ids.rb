class AddDefaultsToLearnerProjectIds < ActiveRecord::Migration[5.2]
    disable_ddl_transaction!

    def change
        reversible do |dir|
            dir.up do
                ActiveRecord::Base.connection.execute(%Q~
                    update cohorts set learner_project_ids = Array[]::uuid[] where learner_project_ids is null;
                    update curriculum_templates set learner_project_ids = Array[]::uuid[] where learner_project_ids is null;
                ~)
            end
        end

        change_column_null :cohorts, :learner_project_ids, false
        change_column_null :curriculum_templates, :learner_project_ids, false
    end
end