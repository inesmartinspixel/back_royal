class AddLastUpdatedAtByHiringManagerToPositions < ActiveRecord::Migration[5.1]
    def change
        add_column :open_positions, :last_updated_at_by_hiring_manager, :datetime
        add_column :open_positions_versions, :last_updated_at_by_hiring_manager, :datetime
    end
end
