class AddTranscriptsVerifiedToUser < ActiveRecord::Migration[5.1]

    def up
        # Certain users aren't required to upload transcripts e.g. users that don't
        # have any formal education experience or users that have formal education
        # experience that they will_not_complete. As such, we're not going to change
        # this column to have a default value.
        add_column :users, :transcripts_verified, :boolean
        add_column :users_versions, :transcripts_verified, :boolean

        recreate_users_trigger_v1
    end

    def down
        remove_column :users, :transcripts_verified
        remove_column :users_versions, :transcripts_verified

        recreate_users_trigger_v1
    end
end
