class AddOfficialTranscriptRequiredOverrideToEducationExperiences < ActiveRecord::Migration[6.0]
    def up
        add_column :education_experiences, :official_transcript_required_override, :boolean, :default => false, :null => false
        add_column :education_experiences_versions, :official_transcript_required_override, :boolean
        recreate_trigger_v2(:education_experiences)
    end

    def down
        remove_column :education_experiences, :official_transcript_required_override
        remove_column :education_experiences_versions, :official_transcript_required_override
        recreate_trigger_v2(:education_experiences)
    end
end
