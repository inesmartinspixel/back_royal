class AddLevelsOfInterestToCareerProfileSearch < ActiveRecord::Migration[5.1]
    def change
        add_column :career_profile_searches, :levels_of_interest, :text, array: true
        change_column_default :career_profile_searches, :levels_of_interest, []
    end
end
