class CreatePublishedCohortPlaylistLocalePacksTemp < ActiveRecord::Migration[6.0]
    def up
        # For when we swap the temp tables into the mat view slots:
        #   ALTER TABLE [ IF EXISTS ] name RENAME TO new_name
        #   ALTER INDEX [ IF EXISTS ] name RENAME TO new_name

        create_table :published_cohort_playlist_locale_packs_temp, id: false do |t|
            t.datetime :created_at
            t.uuid :cohort_id
            t.text :cohort_name
            t.boolean :required
            t.boolean :specialization
            t.boolean :foundations
            t.text :playlist_title
            t.text :playlist_locales
            t.uuid :playlist_locale_pack_id
        end

        add_index :published_cohort_playlist_locale_packs_temp, [:cohort_id, :playlist_locale_pack_id], :unique => true, :name => :cohort_and_play_lp_on_coh_play_lps_temp
        add_index :published_cohort_playlist_locale_packs_temp, :playlist_locale_pack_id, :name => :playlist_lp_on_cohort_playlist_packs_temp

        # PublishedCohortPlaylistLocalePack.rebuild
    end

    def down
        drop_table :published_cohort_playlist_locale_packs_temp
    end
end
