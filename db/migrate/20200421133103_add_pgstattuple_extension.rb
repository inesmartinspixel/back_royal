class AddPgstattupleExtension < ActiveRecord::Migration[6.0]

    def up
        execute "CREATE EXTENSION IF NOT EXISTS pgstattuple"
    end

    def down
        execute "DROP EXTENSION pgstattuple"
    end

end
