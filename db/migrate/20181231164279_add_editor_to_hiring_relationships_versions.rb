
class AddEditorToHiringRelationshipsVersions < ActiveRecord::Migration[5.2]

    def up
        add_editor_columns_to_versions_table("hiring_relationships")
    end

    def down
        rollback_editor_columns_from_versions_table("hiring_relationships")
    end
end
