class AddHiringRelationshipsStripeUsageRecordInfo < ActiveRecord::Migration[5.1]
    def change
        add_column :hiring_relationships, :stripe_usage_record_info, :json
        add_column :hiring_relationships_versions, :stripe_usage_record_info, :json
    end
end
