class UpdateCareerProfilesHasNoFormalEducationNotNull < ActiveRecord::Migration[5.2]

    def up
        change_column_null :career_profiles, :has_no_formal_education, false
    end

    def down
        change_column_null :career_profiles, :has_no_formal_education, true
    end

end
