class RemoveConsiderMeritBasedFromCareerProfiles < ActiveRecord::Migration[5.2]
    def up
        remove_column :career_profiles, :consider_merit_based
        remove_column :career_profiles_versions, :consider_merit_based

        recreate_trigger_v2(:career_profiles)
    end

    def down
        add_column :career_profiles, :consider_merit_based, :boolean
        add_column :career_profiles_versions, :consider_merit_based, :boolean

        recreate_trigger_v2(:career_profiles)
    end
end
