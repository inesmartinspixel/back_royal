class AddCohortApplicationId < ActiveRecord::Migration[6.0]
    disable_ddl_transaction!

    def change
        add_column(:cohort_status_changes, :cohort_application_id, :uuid)
        add_index(:cohort_status_changes, :cohort_application_id, algorithm: :concurrently)
    end
end