class CreateStudentNetworkInclusions < ActiveRecord::Migration[5.2]
    def change
        create_table :student_network_inclusions, id: :uuid do |t|

            t.uuid :user_id, :null => false
            t.uuid :cohort_application_id, :null => false
            t.uuid :cohort_id, :null => false
            t.text :program_type, :null => false
            t.text :pref_student_network_privacy, :null => false
            t.text :graduation_status, :null => false

        end

        add_index :student_network_inclusions, :user_id, :unique => true
        add_index :student_network_inclusions, :cohort_id

        reversible do |dir|
            dir.up do
                # This is used for doing a query with limit and order in the student network.  Since we query by "!=",
                # it seems like we need to index that way
                execute "CREATE INDEX sn_full_privacy_on_student_network_inclusions ON student_network_inclusions ((pref_student_network_privacy <> 'full'))"
                execute "CREATE INDEX sn_hidden_privacy_on_student_network_inclusions ON student_network_inclusions ((pref_student_network_privacy <> 'hidden'))"
            end
        end
    end
end
