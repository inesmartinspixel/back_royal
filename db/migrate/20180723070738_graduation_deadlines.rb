class GraduationDeadlines < ActiveRecord::Migration[5.1]
    def up

        # This is a hack. It seems like some views are not materialized at this point,
        # event though they are created without the `with no data` flag. It's a mystery,
        # but this fixes it.
        RefreshMaterializedContentViews.refresh
        ViewHelpers.migrate(self, 20180723070738)
    end

    def down
        ViewHelpers.rollback(self, 20180723070738)
    end
end
