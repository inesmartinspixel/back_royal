class RemoveOpenPositionUniqueTitle < ActiveRecord::Migration[5.1]
    def up
        remove_index(:open_positions, [:hiring_manager_id, :title])
    end

    def down
        add_index(:open_positions, [:hiring_manager_id, :title], :unique => true)
    end
end
