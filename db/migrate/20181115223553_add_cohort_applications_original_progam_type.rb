class AddCohortApplicationsOriginalProgamType < ActiveRecord::Migration[5.2]
    def change
        add_column :cohort_applications, :original_program_type, :text
        add_column :cohort_applications_versions, :original_program_type, :text
    end
end
