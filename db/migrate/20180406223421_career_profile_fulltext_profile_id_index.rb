class CareerProfileFulltextProfileIdIndex < ActiveRecord::Migration[5.1]
    def change
        add_index :career_profile_fulltext, :career_profile_id
    end
end
