class CohortStatusChangesCohortApplicationIdNotNull < ActiveRecord::Migration[6.0]
    def change
        change_column_null :cohort_status_changes, :cohort_application_id, false
    end
end
