class SubscriptionFunctionsPartTrois < ActiveRecord::Migration[5.1]
    def up

        execute "

            DROP FUNCTION total_base_tuition(cohort_application cohort_applications);
            CREATE FUNCTION total_base_tuition(cohort_application cohort_applications)
            RETURNS float AS $$
                #{total_base_tuition_fn_new}
                return totalBaseTuition(cohort_application);

            $$ LANGUAGE plv8 IMMUTABLE STRICT;


            DROP FUNCTION fee_amount_per_interval(cohort_application cohort_applications);
            CREATE FUNCTION fee_amount_per_interval(cohort_application cohort_applications)
            RETURNS float AS $$
                #{fee_amount_per_interval_fn_new}
                return getFeeAmountPerInterval(cohort_application);
            $$ LANGUAGE plv8 IMMUTABLE STRICT;


            DROP FUNCTION total_fee_amount(cohort_application cohort_applications);
            CREATE FUNCTION total_fee_amount(cohort_application cohort_applications)
            RETURNS float AS $$
                #{num_intervals_for_plan_fn} // getNumIntervals
                #{current_stripe_plan_fn} // getCurrentStripePlan
                #{fee_amount_per_interval_fn_new} // feeAmountPerInterval

                if (!cohort_application.stripe_plans) {
                    return 0;
                }

                if (getCurrentStripePlan(cohort_application)) {

                    var numIntervals = getNumIntervals(cohort_application) || 0;
                    var feeAmountPerInterval =  getFeeAmountPerInterval(cohort_application);

                    return numIntervals * feeAmountPerInterval;
                } else {
                    return 0;
                }

            $$ LANGUAGE plv8 IMMUTABLE STRICT;


            DROP FUNCTION total_fees_plus_tuition(cohort_application cohort_applications);
            CREATE FUNCTION total_fees_plus_tuition(cohort_application cohort_applications)
            RETURNS float AS $$
                #{num_intervals_for_plan_fn} // getNumIntervals
                #{current_stripe_plan_fn} // getCurrentStripePlan
                #{total_base_tuition_fn_new} // totalBaseTuition

                var stripePlan = getCurrentStripePlan(cohort_application);
                if (stripePlan) {
                    return (stripePlan.amount / 100) * (getNumIntervals(cohort_application) || 0);
                } else {
                    return totalBaseTuition(cohort_application); // full scholarship
                }

            $$ LANGUAGE plv8 IMMUTABLE STRICT;


            DROP FUNCTION total_scholarship(ca cohort_applications);
            CREATE FUNCTION total_scholarship(ca cohort_applications)
            RETURNS float AS $$
            BEGIN
                RETURN case
                    when (get_coupon(ca)->>'percent_off')::float = 100 then
                        total_fees_plus_tuition(ca) - total_discount(ca)
                    when num_intervals(ca) is null then
                        0
                    else
                        ((get_coupon(ca)->>'amount_off')::float / 100 - discount_per_interval(ca)) * num_intervals(ca)
                    end;
            END;
            $$ LANGUAGE plpgsql;

            "


    end

    def down


        execute "

            DROP FUNCTION total_base_tuition(cohort_application cohort_applications);
            CREATE FUNCTION total_base_tuition(cohort_application cohort_applications)
            RETURNS float AS $$
                #{total_base_tuition_fn_old}
                return totalBaseTuition(cohort_application);

            $$ LANGUAGE plv8 IMMUTABLE STRICT;


            DROP FUNCTION fee_amount_per_interval(cohort_application cohort_applications);
            CREATE FUNCTION fee_amount_per_interval(cohort_application cohort_applications)
            RETURNS float AS $$
                #{fee_amount_per_interval_fn_old}
                return getFeeAmountPerInterval(cohort_application);
            $$ LANGUAGE plv8 IMMUTABLE STRICT;

            DROP FUNCTION total_fee_amount(cohort_application cohort_applications);
            CREATE FUNCTION total_fee_amount(cohort_application cohort_applications)
            RETURNS float AS $$
                #{num_intervals_for_plan_fn} // getNumIntervals
                #{current_stripe_plan_fn} // getCurrentStripePlan
                #{fee_amount_per_interval_fn_old} // feeAmountPerInterval

                if (!cohort_application.stripe_plans) {
                    return null;
                }

                if (getCurrentStripePlan(cohort_application)) {

                    var numIntervals = getNumIntervals(cohort_application);
                    var feeAmountPerInterval =  getFeeAmountPerInterval(cohort_application);

                    return numIntervals * feeAmountPerInterval;
                } else {
                    return 0;
                }

            $$ LANGUAGE plv8 IMMUTABLE STRICT;


            DROP FUNCTION total_fees_plus_tuition(cohort_application cohort_applications);
            CREATE FUNCTION total_fees_plus_tuition(cohort_application cohort_applications)
            RETURNS float AS $$
                #{num_intervals_for_plan_fn} // getNumIntervals
                #{current_stripe_plan_fn} // getCurrentStripePlan
                #{total_base_tuition_fn_old} // totalBaseTuition

                var stripePlan = getCurrentStripePlan(cohort_application);
                if (stripePlan) {
                    return (stripePlan.amount / 100) * (getNumIntervals(cohort_application) || 0);
                } else {
                    return totalBaseTuition(cohort_application); // full scholarship
                }

            $$ LANGUAGE plv8 IMMUTABLE STRICT;


            DROP FUNCTION total_scholarship(ca cohort_applications);
            CREATE FUNCTION total_scholarship(ca cohort_applications)
            RETURNS float AS $$
            BEGIN
                RETURN case
                    when (get_coupon(ca)->>'percent_off')::float = 100 then
                        total_fees_plus_tuition(ca) - total_discount(ca)
                    else
                        ((get_coupon(ca)->>'amount_off')::float / 100 - discount_per_interval(ca)) * num_intervals(ca)
                    end;
            END;
            $$ LANGUAGE plpgsql;
            "

    end


    # TROIS FUNCTIONS

    def total_base_tuition_fn_new
        %Q~
            function totalBaseTuition(cohortApplication) {
                var oneTimePaymentPlan;
                if (!cohortApplication.stripe_plans) {
                    return 0;
                }

                for (var i = 0; i < cohortApplication.stripe_plans.length; i++) {
                    var _plan = cohortApplication.stripe_plans[i];
                    if (_plan.frequency === 'once') {
                        oneTimePaymentPlan = _plan;
                    }
                }

                // Some legacy users don't have a once plan, so just hardcode their total_base_tuition
                return oneTimePaymentPlan ? oneTimePaymentPlan.amount / 100 : 9600;
            }
        ~
    end

    def fee_amount_per_interval_fn_new
        %Q~
            function getFeeAmountPerInterval(cohortApplication) {
                if (!cohortApplication.stripe_plans) {
                    return 0;
                }

                if (cohort_application.stripe_plan_id === 'emba_875') {
                    return 75;
                }

                return 0;
            }
        ~
    end



    # DEUX FUNCTIONS


    def total_base_tuition_fn_old
        %Q~
            function totalBaseTuition(cohortApplication) {
                var oneTimePaymentPlan;
                if (!cohortApplication.stripe_plans) {
                    return null;
                }

                for (var i = 0; i < cohortApplication.stripe_plans.length; i++) {
                    var _plan = cohortApplication.stripe_plans[i];
                    if (_plan.frequency === 'once') {
                        oneTimePaymentPlan = _plan;
                    }
                }

                // Some legacy users don't have a once plan, so just hardcode their total_base_tuition
                return oneTimePaymentPlan ? oneTimePaymentPlan.amount / 100 : 9600;
            }
        ~
    end

    def fee_amount_per_interval_fn_old
        %Q~
            function getFeeAmountPerInterval(cohortApplication) {
                if (!cohortApplication.stripe_plans) {
                    return null;
                }

                if (cohort_application.stripe_plan_id === 'emba_875') {
                    return 75;
                }

                return 0;
            }
        ~
    end




    # UNCHANGED FUNCTIONS

    def num_intervals_for_plan_fn
        %Q~
        function getNumIntervals(cohortApplication) {
            var plan = getCurrentStripePlan(cohortApplication)
            if (!plan) {
                return null;
            }
            switch (plan.frequency) {
                case 'bi_annual':
                    return 2;
                case 'monthly':
                    return 12;
                case 'once':
                    return 1;
            }
        }
        ~
    end

    def current_stripe_plan_fn
        %Q~

        function getCurrentStripePlan(cohortApplication) {

            if (!cohortApplication.stripe_plans) {
                return null;
            }

            // Full scholarship users might have a stripe_plan_id, but they don't
            // actually have a stripe plan, since they don't pay.
            // Note that we have to be defensive here because paid_cert users DO have stripe_plans
            // but DO NOT have the notion of scholarships.
            if (cohortApplication.scholarship_level && cohortApplication.scholarship_level.name === 'Full Scholarship') {
                return null;
            }

            var plan;
            for (var i = 0; i < cohortApplication.stripe_plans.length; i++) {
                var _plan = cohortApplication.stripe_plans[i];
                if (_plan.id === cohortApplication.stripe_plan_id) {
                    plan = _plan;
                }
            }
            return plan;
        }
        ~
    end


end
