class AddPreAcceptedAtToCohortApplications < ActiveRecord::Migration[5.1]
    def change
        add_column :cohort_applications, :pre_accepted_at, :datetime
        add_column :cohort_applications_versions, :pre_accepted_at, :datetime
    end
end
