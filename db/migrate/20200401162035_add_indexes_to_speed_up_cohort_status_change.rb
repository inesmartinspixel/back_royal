class AddIndexesToSpeedUpCohortStatusChange < ActiveRecord::Migration[6.0]
    disable_ddl_transaction!

    def change
        add_index :user_lesson_progress_records, :completed_at, algorithm: :concurrently
        add_index :cohort_applications_versions, :version_created_at, algorithm: :concurrently
    end
end
