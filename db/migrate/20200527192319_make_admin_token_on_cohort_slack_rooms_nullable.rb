class MakeAdminTokenOnCohortSlackRoomsNullable < ActiveRecord::Migration[6.0]
    def change
        change_column_null :cohort_slack_rooms, :admin_token, true
        change_column_null :cohort_slack_rooms_versions, :admin_token, true
    end
end
