class AddLockedDueToPastDuePaymentToCohortApplications < ActiveRecord::Migration[5.1]
    def up
        add_column :cohort_applications, :locked_due_to_past_due_payment, :boolean
        add_column :cohort_applications_versions, :locked_due_to_past_due_payment, :boolean

        change_column_default(:cohort_applications, :locked_due_to_past_due_payment, false)
    end

    def down
        remove_column :cohort_applications, :locked_due_to_past_due_payment
        remove_column :cohort_applications_versions, :locked_due_to_past_due_payment
    end
end
