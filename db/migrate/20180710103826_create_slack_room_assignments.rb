class CreateSlackRoomAssignments < ActiveRecord::Migration[5.1]
    def change
        create_table :cohort_slack_room_assignments, id: :uuid do |t|
            t.timestamps
            t.uuid :cohort_id, null: false
            t.json :slack_room_assignments, null: false
            t.json :location_assignments, null: false
        end
        add_index :cohort_slack_room_assignments, [:cohort_id], :unique => true
    end
end
