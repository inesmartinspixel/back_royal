class RemoveOptimizelyFields < ActiveRecord::Migration[6.0]
    def up
        remove_column :users, :optimizely_segments
        remove_column :users, :optimizely_referer
        remove_column :users, :optimizely_buckets

        drop_table :experiment_variations_users
        drop_table :experiment_variations
        drop_table :experiments

        recreate_users_trigger_v4
    end

    def down

        # experiments
        execute "
            CREATE TABLE public.experiments (
                id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
                created_at timestamp without time zone,
                updated_at timestamp without time zone,
                title character varying,
                optimizely_id text
            );
            ALTER TABLE ONLY public.experiments
                ADD CONSTRAINT experiments_pkey PRIMARY KEY (id);
            CREATE UNIQUE INDEX index_experiments_on_optimizely_id ON public.experiments USING btree (optimizely_id);
            CREATE UNIQUE INDEX index_experiments_on_title ON public.experiments USING btree (title);
        "

        # experiment_variations
        execute "
            CREATE TABLE public.experiment_variations (
                id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
                created_at timestamp without time zone,
                updated_at timestamp without time zone,
                title character varying NOT NULL,
                experiment_id uuid NOT NULL,
                optimizely_id text
            );
            ALTER TABLE ONLY public.experiment_variations
                ADD CONSTRAINT experiment_variations_pkey PRIMARY KEY (id);
            CREATE UNIQUE INDEX index_experiment_variations_on_experiment_id_and_title ON public.experiment_variations USING btree (experiment_id, title);
            CREATE UNIQUE INDEX index_experiment_variations_on_optimizely_id ON public.experiment_variations USING btree (optimizely_id);
            ALTER TABLE ONLY public.experiment_variations
                ADD CONSTRAINT fk_rails_06c3a97931 FOREIGN KEY (experiment_id) REFERENCES public.experiments(id);
        "

        # experiment_variations_users
        execute "
            CREATE TABLE public.experiment_variations_users (
                id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
                user_id uuid NOT NULL,
                experiment_id uuid NOT NULL,
                experiment_variation_id uuid NOT NULL
            );
            ALTER TABLE ONLY public.experiment_variations_users
                ADD CONSTRAINT experiment_variations_users_pkey PRIMARY KEY (id);
            CREATE UNIQUE INDEX index_experiment_variations_users_on_user_id_and_experiment_id ON public.experiment_variations_users USING btree (user_id, experiment_id);
            ALTER TABLE ONLY public.experiment_variations_users
                ADD CONSTRAINT fk_rails_502fd2e655 FOREIGN KEY (experiment_variation_id) REFERENCES public.experiment_variations(id);
        "

        add_column :users, :optimizely_segments, :json
        add_column :users, :optimizely_referer, :string
        add_column :users, :optimizely_buckets, :json
        recreate_users_trigger_v3
    end
end