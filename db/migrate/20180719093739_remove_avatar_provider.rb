require File.expand_path("../migrate_helper", __FILE__)
class RemoveAvatarProvider < ActiveRecord::Migration[5.1]

    def up
        remove_column :users, :avatar_provider
        remove_column :users_versions, :avatar_provider

        recreate_users_trigger_v1
    end

    def down
        add_column :users, :avatar_provider, :text
        add_column :users_versions, :avatar_provider, :text

        recreate_users_trigger_v1
    end
end
