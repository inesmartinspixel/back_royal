class AddStudentIdVerificationSupport < ActiveRecord::Migration[5.1]
    def change

        # idology_verifications

        create_table :idology_verifications, :id => :uuid do |t|
            t.timestamps
            t.uuid :user_id                                 , null: false
            t.text :idology_id_number                       , null: false
            t.text :scan_capture_url                        , null: false
            t.timestamp :verified_at                        , null: true
            t.text :response                                , null: true
        end

        add_foreign_key :idology_verifications, :users, column: :user_id
        add_index :idology_verifications, :idology_id_number
        add_index :idology_verifications, :user_id


        # user_id_verifications

        create_table :user_id_verifications, :id => :uuid do |t|
            t.timestamps
            t.uuid :cohort_id                               , null: false
            t.uuid :user_id                                 , null: false
            t.integer :id_verification_period_index         , null: false
            t.timestamp :verified_at                        , null: false
            t.text :verification_method                     , null: false
            t.uuid :idology_verification_id                 , null: true
        end

        add_foreign_key :user_id_verifications, :cohorts, column: :cohort_id
        add_foreign_key :user_id_verifications, :users, column: :user_id
        add_foreign_key :user_id_verifications, :idology_verifications, column: :idology_verification_id
        add_index :user_id_verifications, [:user_id, :cohort_id, :id_verification_period_index], unique: true, :name => "unique_on_user_id_verifications"
        add_index :user_id_verifications, :idology_verification_id


        # cohorts / templates

        add_column :cohorts, :id_verification_periods, :json, array: true
        add_column :cohorts_versions, :id_verification_periods, :json, array: true

        add_column :curriculum_templates, :id_verification_periods, :json, array: true
        add_column :curriculum_templates_versions, :id_verification_periods, :json, array: true

    end
end
