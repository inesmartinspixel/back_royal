class AddEnrollmentAgreementHasBeenSignedToCohortApplications < ActiveRecord::Migration[5.1]

    def up
        add_column :cohort_applications, :enrollment_agreement_has_been_signed, :boolean
        add_column :cohort_applications_versions, :enrollment_agreement_has_been_signed, :boolean

        change_column_default :cohort_applications, :enrollment_agreement_has_been_signed, false
    end

    def down
        remove_column :cohort_applications, :enrollment_agreement_has_been_signed
        remove_column :cohort_applications_versions, :enrollment_agreement_has_been_signed
    end
end
