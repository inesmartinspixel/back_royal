class AddIsolatedNetworkToViews < ActiveRecord::Migration[5.2]

    def up
        ViewHelpers.migrate(self, 20190815141309)
    end

    def down
        ViewHelpers.migrate(self, 20190815141309)
    end

end
