class AddPgAnalyzeSupport < ActiveRecord::Migration[5.1]

    def up
        return unless Rails.env.production?

        execute "
            CREATE SCHEMA pganalyze;

            CREATE EXTENSION IF NOT EXISTS pg_stat_statements;

            CREATE OR REPLACE FUNCTION pganalyze.get_stat_statements(showtext boolean = true) RETURNS SETOF pg_stat_statements AS
            $$
              /* pganalyze-collector */ SELECT * FROM public.pg_stat_statements(showtext);
            $$ LANGUAGE sql VOLATILE SECURITY DEFINER;

            CREATE OR REPLACE FUNCTION pganalyze.get_stat_activity() RETURNS SETOF pg_stat_activity AS
            $$
              /* pganalyze-collector */ SELECT * FROM pg_catalog.pg_stat_activity;
            $$ LANGUAGE sql VOLATILE SECURITY DEFINER;

            CREATE OR REPLACE FUNCTION pganalyze.get_column_stats() RETURNS SETOF pg_stats AS
            $$
              /* pganalyze-collector */ SELECT schemaname, tablename, attname, inherited, null_frac, avg_width,
              n_distinct, NULL::anyarray, most_common_freqs, NULL::anyarray, correlation, NULL::anyarray,
              most_common_elem_freqs, elem_count_histogram
              FROM pg_catalog.pg_stats;
            $$ LANGUAGE sql VOLATILE SECURITY DEFINER;

            CREATE OR REPLACE FUNCTION pganalyze.get_stat_replication() RETURNS SETOF pg_stat_replication AS
            $$
              /* pganalyze-collector */ SELECT * FROM pg_catalog.pg_stat_replication;
            $$ LANGUAGE sql VOLATILE SECURITY DEFINER;

            CREATE USER pganalyze WITH PASSWORD '#{ENV['PG_ANALYZE_PASSWORD']}' CONNECTION LIMIT 5;
            REVOKE ALL ON SCHEMA public FROM pganalyze;
            GRANT USAGE ON SCHEMA pganalyze TO pganalyze;
        "
    end

    def down
        return unless Rails.env.production?

        execute "
            DROP SCHEMA pganalyze CASCADE;

            DROP USER pganalyze;

            DROP EXTENSION pg_stat_statements;
        "
    end

end