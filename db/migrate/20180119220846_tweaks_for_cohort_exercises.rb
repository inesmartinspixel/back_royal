class TweaksForCohortExercises < ActiveRecord::Migration[5.1]
    def change
        add_column :cohorts, :slack_token, :string
        add_column :cohorts_versions, :slack_token, :string

        create_table :s3_exercise_documents, id: :uuid do |t|
            t.timestamps
            t.attachment :file
            t.string :file_fingerprint
        end
    end
end
