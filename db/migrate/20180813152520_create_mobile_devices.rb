class CreateMobileDevices < ActiveRecord::Migration[5.1]

    def up
        create_table :mobile_devices, id: :uuid do |t|
            t.timestamps
            t.uuid          :user_id,        null: false
            t.text          :device_token,   null: false
            t.text          :platform,       null: false
            t.datetime      :last_used_at,   null: false
        end

        add_index :mobile_devices, [:user_id, :device_token], :unique => true
    end

    def down
        drop_table :mobile_devices
    end

end
