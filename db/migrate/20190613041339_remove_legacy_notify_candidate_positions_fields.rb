class RemoveLegacyNotifyCandidatePositionsFields < ActiveRecord::Migration[5.2]
  def up
    remove_column :users, :notify_candidate_positions_technical
    remove_column :users_versions, :notify_candidate_positions_technical

    remove_column :users, :notify_candidate_positions_business
    remove_column :users_versions, :notify_candidate_positions_business

    recreate_users_trigger_v3
  end

  def down
    add_column :users, :notify_candidate_positions_technical, :boolean, :null => false, :default => true
    add_column :users_versions, :notify_candidate_positions_technical, :boolean

    add_column :users, :notify_candidate_positions_business, :boolean, :null => false, :default => true
    add_column :users_versions, :notify_candidate_positions_business, :boolean

    recreate_users_trigger_v3
  end
end
