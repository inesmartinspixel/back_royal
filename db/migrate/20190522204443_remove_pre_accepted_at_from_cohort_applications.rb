class RemovePreAcceptedAtFromCohortApplications < ActiveRecord::Migration[5.2]
    def up

        ViewHelpers.remove_view_temporarily_while(self, ViewHelpers::ApplicationsThatProvideStudentNetworkInclusion) do
            remove_column :cohort_applications, :pre_accepted_at
            remove_column :cohort_applications_versions, :pre_accepted_at
        end

        recreate_trigger_v2(:cohort_applications)
    end

    def down
        ViewHelpers.remove_view_temporarily_while(self, ViewHelpers::ApplicationsThatProvideStudentNetworkInclusion) do
            add_column :cohort_applications, :pre_accepted_at, :datetime
            add_column :cohort_applications_versions, :pre_accepted_at, :datetime
        end

        recreate_trigger_v2(:cohort_applications)
    end
end
