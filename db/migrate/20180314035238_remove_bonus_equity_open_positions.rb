class RemoveBonusEquityOpenPositions < ActiveRecord::Migration[5.1]
    def change
        remove_column :open_positions, :bonus, :boolean, default: false, null: false
        remove_column :open_positions, :equity, :boolean, default: false, null: false
        remove_column :open_positions_versions, :bonus, :boolean
        remove_column :open_positions_versions, :equity, :boolean
    end
end
