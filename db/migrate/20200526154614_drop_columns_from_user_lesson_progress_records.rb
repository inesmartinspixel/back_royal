class DropColumnsFromUserLessonProgressRecords < ActiveRecord::Migration[6.0]
    def change
        remove_column :user_lesson_progress_records, :test, :boolean
        remove_column :user_lesson_progress_records, :assessment, :boolean
    end
end
