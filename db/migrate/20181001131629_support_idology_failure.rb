class SupportIdologyFailure < ActiveRecord::Migration[5.1]
    def change
        rename_column :idology_verifications, :verified_at, :response_received_at
        add_column :idology_verifications, :verified, :boolean

        reversible do |dir|
            dir.up do
                # couldn't get this to cast existing columns, but we don't
                # expect there to be any records here yet anyhow
                remove_column :idology_verifications, :response
                add_column :idology_verifications, :response, :json
            end

            dir.down do
                remove_column :idology_verifications, :response
                add_column :idology_verifications, :response, :text
            end
        end
    end
end
