class ExplcitlyMaterializedViews < ActiveRecord::Migration[6.0]
    def up
        ViewHelpers.migrate(self, 20200625152000)
    end

    def down
        ViewHelpers.rollback(self, 20200625152000)
    end
end
