class SwapDerivedContentTables < ActiveRecord::Migration[6.0]

    def tables
        %w(
            published_cohort_periods
            published_cohort_periods_stream_locale_packs
            published_cohort_playlist_locale_packs
            published_cohort_stream_locale_packs
            published_cohort_lesson_locale_packs
        )
    end

    def up
        ViewHelpers.migrate(self, 20200609141113) do
            tables.each do |table|
                execute "CREATE TABLE #{table} AS SELECT * FROM #{table}_temp"
            end

            add_index :published_cohort_periods, [:cohort_id, :index], :unique => true, :name => :coh_id_index_on_publ_coh_periods

            add_index :published_cohort_periods_stream_locale_packs, [:cohort_id, :index, :locale_pack_id], :unique => true, :name => :coh_id_index_on_publ_coh_per_str_loc_packs

            add_index :published_cohort_playlist_locale_packs, [:cohort_id, :playlist_locale_pack_id], :unique => true, :name => :cohort_and_play_lp_on_coh_play_lps
            add_index :published_cohort_playlist_locale_packs, :playlist_locale_pack_id, :name => :playlist_lp_on_cohort_playlist_packs

            add_index :published_cohort_stream_locale_packs, [:cohort_id, :stream_locale_pack_id], :name => :cohort_and_str_lp_on_cohort_str_lps, :unique => true
            add_index :published_cohort_stream_locale_packs, :stream_locale_pack_id, :name => :stream_lp_on_cohort_stream_packs

            add_index :published_cohort_lesson_locale_packs, [:cohort_id, :lesson_locale_pack_id], :name => :cohort_and_lesson_lp_on_coh_lesson_lps, :unique => true
            add_index :published_cohort_lesson_locale_packs, :lesson_locale_pack_id, :name => :lesson_lp_on_cohort_lesson_locale_packs
        end
    end

    def down
        ViewHelpers.rollback(self, 20200609141113) do
            tables.each do |table|
                execute "DROP TABLE #{table}"
            end
        end
    end
end
