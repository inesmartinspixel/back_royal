class AddHasSeenMbaSubmitPopupToUsers < ActiveRecord::Migration[5.2]

  def up
      add_column :users, :has_seen_mba_submit_popup, :boolean
      add_column :users_versions, :has_seen_mba_submit_popup, :boolean

      change_column_default :users, :has_seen_mba_submit_popup, false

      recreate_users_trigger_v1
  end

  def down
      remove_column :users, :has_seen_mba_submit_popup, :boolean
      remove_column :users_versions, :has_seen_mba_submit_popup, :boolean

      recreate_users_trigger_v1
  end
end
