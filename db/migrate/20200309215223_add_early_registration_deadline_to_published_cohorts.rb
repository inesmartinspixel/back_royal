class AddEarlyRegistrationDeadlineToPublishedCohorts < ActiveRecord::Migration[6.0]
    def up
        ViewHelpers.migrate(self, 20200309215223)
    end

    def down
        ViewHelpers.rollback(self, 20200309215223)
    end
end
