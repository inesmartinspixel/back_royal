class TweaksForOpenPositionsCuration < ActiveRecord::Migration[5.1]
    def change
        add_index :candidate_position_interests, :open_position_id

        add_column :open_positions, :posted_at, :datetime
        add_column :open_positions_versions, :posted_at, :datetime

        add_column :open_positions, :last_curated_at, :datetime
        add_column :open_positions_versions, :last_curated_at, :datetime

        add_column :open_positions, :curation_email_last_triggered_at, :datetime
        add_column :open_positions_versions, :curation_email_last_triggered_at, :datetime

        add_column :candidate_position_interests, :hiring_manager_priority, :integer
        add_column :candidate_position_interests_versions, :hiring_manager_priority, :integer

        add_column :candidate_position_interests, :admin_status, :text
        add_column :candidate_position_interests_versions, :admin_status, :text

        change_column_default :candidate_position_interests, :admin_status, from: nil, to: 'unreviewed'
        change_column_default :candidate_position_interests, :hiring_manager_status, from: 'unseen', to: 'hidden'

        reversible do |dir|
            dir.up do
                # We need to backfill alongside the addition of the column since we are now are validating that posted_at is set
                # when featured.
                execute "
                    WITH posted_ats AS MATERIALIZED (
                        SELECT
                            id
                            , min(updated_at) AS posted_at
                            , count(*)
                        FROM open_positions_versions
                        WHERE featured=true
                        GROUP BY open_positions_versions.id

                    )

                    UPDATE open_positions SET posted_at = posted_ats.posted_at FROM posted_ats
                        WHERE posted_ats.id = open_positions.id;
                 "
            end
        end
    end
end
