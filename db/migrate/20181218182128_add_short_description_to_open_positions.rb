class AddShortDescriptionToOpenPositions < ActiveRecord::Migration[5.2]

    def up
        add_column :open_positions, :short_description, :text
        add_column :open_positions_versions, :short_description, :text
    end

    def down
        remove_column :open_positions, :short_description, :text
        remove_column :open_positions_versions, :short_description, :text
    end
end
