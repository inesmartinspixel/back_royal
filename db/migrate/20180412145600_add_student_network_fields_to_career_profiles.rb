class AddStudentNetworkFieldsToCareerProfiles < ActiveRecord::Migration[5.1]

  def up
      add_column :career_profiles, :student_network_looking_for, :text, array: true
      add_column :career_profiles_versions, :student_network_looking_for, :text, array: true
      change_column_default :career_profiles, :student_network_looking_for, []
  end

  def down
      remove_column :career_profiles, :student_network_looking_for
      remove_column :career_profiles_versions, :student_network_looking_for
  end

end
