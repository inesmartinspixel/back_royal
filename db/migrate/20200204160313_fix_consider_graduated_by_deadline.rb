class FixConsiderGraduatedByDeadline < ActiveRecord::Migration[6.0]
    def up
        ViewHelpers.migrate(self, 20200204160313)
    end

    def down
        ViewHelpers.rollback(self, 20200204160313)
    end
end
