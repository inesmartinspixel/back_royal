class AddHasNoFormalEducationToCareerProfile < ActiveRecord::Migration[5.2]

    def up
        add_column :career_profiles, :has_no_formal_education, :boolean
        add_column :career_profiles_versions, :has_no_formal_education, :boolean

        change_column_default :career_profiles, :has_no_formal_education, false

        recreate_trigger_v2 "career_profiles"
    end

    def down
        remove_column :career_profiles, :has_no_formal_education, :boolean
        remove_column :career_profiles_versions, :has_no_formal_education, :boolean

        recreate_trigger_v2 "career_profiles"
    end
end
