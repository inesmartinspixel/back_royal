class NormalizeUserBoolsNotNull < ActiveRecord::Migration[5.2]


    disable_ddl_transaction!

    def up
        change_column_null :users, :has_seen_welcome, false
        change_column_null :users, :free_trial_started, false
        change_column_null :users, :confirmed_profile_info, false
        change_column_null :users, :mba_content_lockable, false
        change_column_null :users, :has_seen_accepted, false
        change_column_null :users, :can_edit_career_profile, false
        change_column_null :users, :identity_verified, false
        change_column_null :users, :program_type_confirmed, false
        change_column_null :users, :notify_hiring_updates, false
        change_column_null :users, :notify_hiring_spotlights, false
        change_column_null :users, :notify_hiring_content, false
        change_column_null :users, :notify_hiring_candidate_activity, false
        change_column_null :users, :has_seen_featured_positions_page, false
        change_column_null :users, :has_seen_careers_welcome, false
        change_column_null :users, :can_purchase_paid_certs, false
        change_column_null :users, :has_seen_hide_candidate_confirm, false
        change_column_null :users, :has_seen_first_position_review_modal, false
        change_column_null :users, :english_language_proficiency_documents_approved, false
        change_column_null :users, :transcripts_verified, false
        change_column_null :users, :has_logged_in, false
    end

    def down
        change_column_null :users, :has_seen_welcome, true
        change_column_null :users, :free_trial_started, true
        change_column_null :users, :confirmed_profile_info, true
        change_column_null :users, :mba_content_lockable, true
        change_column_null :users, :has_seen_accepted, true
        change_column_null :users, :can_edit_career_profile, true
        change_column_null :users, :identity_verified, true
        change_column_null :users, :program_type_confirmed, true
        change_column_null :users, :notify_hiring_updates, true
        change_column_null :users, :notify_hiring_spotlights, true
        change_column_null :users, :notify_hiring_content, true
        change_column_null :users, :notify_hiring_candidate_activity, true
        change_column_null :users, :has_seen_featured_positions_page, true
        change_column_null :users, :has_seen_careers_welcome, true
        change_column_null :users, :can_purchase_paid_certs, true
        change_column_null :users, :has_seen_hide_candidate_confirm, true
        change_column_null :users, :has_seen_first_position_review_modal, true
        change_column_null :users, :english_language_proficiency_documents_approved, true
        change_column_null :users, :transcripts_verified, true
        change_column_null :users, :has_logged_in, true
    end
end
