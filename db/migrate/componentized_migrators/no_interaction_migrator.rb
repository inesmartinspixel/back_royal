require File.expand_path('../componentized_erb_migrator_base', __FILE__)

class NoInteractionMigrator < ComponentizedErbMigratorBase

	def template_path
		File.expand_path('../../templates/20140306204737_componentized_no_interaction.json.erb', __FILE__)
	end

	def frame_types
		['no_interaction']
	end

end