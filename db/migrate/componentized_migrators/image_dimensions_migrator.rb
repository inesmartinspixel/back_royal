require File.expand_path('../componentized_ruby_object_migrator_base', __FILE__)

class ImageDimensionsMigrator < ComponentizedRubyObjectMigratorBase

	def should_process_frame?(frame)
        raise_on_failures = true
		return false unless frame.is_a?(Lesson::Content::FrameList::Frame::Componentized)
        return frame.images.any?
    end

    def process_frame(frame)
        raise_on_failures = true
        
        frame.images.each do |image|
            geometry = Paperclip::Geometry.from_file(image.original_format_url) 
            image.image['dimensions'] = {
                width: geometry.width,
                height: geometry.height
            }
        end

    end

end
