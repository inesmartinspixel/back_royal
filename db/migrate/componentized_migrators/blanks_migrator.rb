require File.expand_path('../componentized_erb_migrator_base', __FILE__)

class BlanksMigrator < ComponentizedErbMigratorBase

	attr_accessor :layout_id, :answers, :shared_answer_list_id, :answer_lists, :challenges, :consumable, :sequential

	def template_path
		File.expand_path('../../templates/20140403144929_componentized_fill_in_the_blanks.json.erb', __FILE__)
	end

	def frame_types
		['blanks']
	end

	def prepare_frame(frame)
		@layout_id = SecureRandom.uuid
		@shared_answer_list_id = SecureRandom.uuid
		@answers = []
		@answer_lists = []
		@consumable = frame['data']['mode'] == 'consumable'
		@sequential = frame['data']['mode'] == 'sequential'

		if !@consumable && !@sequential
			raise "Must be either consumable or sequential"
		end

		if @consumable
			prepare_consumable_challenges(frame)
		end

		if @sequential
			prepare_sequential_challenges(frame)
		end

	end

	# consumable
		# 	{
		#   "data": {
		#     "mode": "consumable",
		#     "blanks": [
		#       "blank1",
		#       "blank2"
		#     ],
		#     "questions": [
		#       {
		#         "answers": [
		#           {
		#             "label": "blank1",
		#             "correct_index": 0
		#           },
		#           {
		#             "label": "blank2",
		#             "correct_index": 1
		#           },
		#           {
		#             "label": "confuser",
		#             "message": null
		#           }
		#         ]
		#       }
		#     ]
		#   }
		# }
	def prepare_consumable_challenges(frame)

		@challenges = []

		old_answers = frame['data']['questions'][0]['answers']

		@shared_answer_list_id = SecureRandom.uuid
		@answer_lists << {
			'id' => @shared_answer_list_id,
			'answers' => @answers
		}

		old_answers.each do |old_answer|

			# create the answer
			new_answer = {
		 		"id" => SecureRandom.uuid,
		 		"text" => escape_text(old_answer['label']),
		 		"text_id" => SecureRandom.uuid,
		 		"message" => old_answer['message']
		 	}

		 	# add it to the global list
		 	@answers.push new_answer

		 	# if this answer is not a confuser, then create the challenge
		 	# for which it is the correct answer
			if old_answer['correct_index']
				@challenges[old_answer['correct_index']] = {
					"id" => SecureRandom.uuid,
					"correct_answer_id" => new_answer['id'],
					"validator_id" => SecureRandom.uuid,
					"expected_answer_matcher_id" => SecureRandom.uuid,
					"answer_list_id" => @shared_answer_list_id,
					"messages" => [],
					"message_ids" => []
				}
			end

		end.compact

		# for consumable mode, any messages should be applied
		# across the board to all challenges, since that's how it
		# worked in the old "blanks" frame type
		@answers.each do |new_answer|
			if message = create_message(new_answer)
				@challenges.each do |challenge|
					challenge['messages'] << message
					challenge['message_ids'] << message['id']
				end
			end
		end

		
	end


	# sequential
		# 	 {
		#   "data": {
		#     "mode": "sequential",
		#     "blanks": [
		#       "blank",
		#       "blank2"
		#     ],
		#     "questions": [
		#       {
		#         "answers": [
		#           {
		#             "label": "blank",
		#             "correct": true
		#           },
		#           {
		#             "label": "confuser",
		#             "correct": false,
		#             "message": null
		#           }
		#         ]
		#       },
		#       {
		#         "answers": [
		#           {
		#             "label": "blank2",
		#             "correct": true
		#           },
		#           {
		#             "label": "blank2 confuser",
		#             "correct": false,
		#             "message": null
		#           }
		#         ]
		#       }
		#     ]
		#   }
		# }
	def prepare_sequential_challenges(frame)
		@challenges = frame['data']['questions'].map do |question|

			correct_answer = nil

			# get the list of answers for this challenge
			answers = question['answers'].map do |answer|
				new_answer = {
			 		"id" => SecureRandom.uuid,
			 		"text" => escape_text(answer['label']),
			 		"text_id" => SecureRandom.uuid,
			 		"message" => answer['message']
			 	}
			 	correct_answer = new_answer if answer['correct']
			 	new_answer
			end

			if correct_answer.nil?
				raise "No correct answer"
			end

			# add it to the global list
		 	@answers += answers

			answer_list = {
				'id' => SecureRandom.uuid,
				'answers' => answers
			} 

			@answer_lists << answer_list

			# create messages
			messages = answers.map do |answer|
				create_message(answer)
			end.compact

			# create the challenge
			{
				"id" => SecureRandom.uuid,
				"correct_answer_id" => correct_answer['id'],
				"validator_id" => SecureRandom.uuid,
				"expected_answer_matcher_id" => SecureRandom.uuid,
				"answer_list_id" => answer_list['id'],
				"message_ids" => messages.map { |m| m['id'] },
				"messages" => messages
			}

		end.compact
	end

	def create_message(answer)
		if answer['message']
			{
				"id" => SecureRandom.uuid,
				"answer_id" => answer['id'],
				"text" => escape_text(answer['message']),
				"text_id" => SecureRandom.uuid,
				"expected_answer_matcher_id" => SecureRandom.uuid
			}
		end
	end



end
