# this uses erb for migrating frames.  It has been used for converting
# non-componentized frames to componentized ones

class ComponentizedErbMigratorBase

	attr_accessor :author_comments_json, :main_ui_component_id, :continue_button_id,
                :content_for_text_id, :content_for_image_id, :frame_guid,
                :main_text_id, :main_text, :images, :modals, :lessons, :raise_on_failures,
                :puts_progress

    # always flush the buffer
    $stdout.sync = true

    def initialize
        self.puts_progress = true
    end

	def change(lessons = nil)

        log "-- Running #{self.class.name} --"

        # parse template text
        @template_text = File.open(template_path, 'rb') { |f| f.read }

        # process in batches if not supplied with concrete list
        if lessons
            log "----------- Processing All -----------"
            @lessons = lessons
            process_lesson_batch lessons.length
        else
            lesson_count = Lesson.count
            current_batch = 0
            Lesson.find_in_batches(batch_size: 10)  do |lessons|
                log "----------- Processing Batch #{current_batch+1} -----------"
                @lessons = lessons
                process_lesson_batch(current_batch * 10, lesson_count)
                current_batch += 1
            end
        end

		# IMPORTANT: ensure we update the cache since this only ever happens on stream / lesson saves and not on app init
		# Lesson::StreamCacheBuster.bust

	end

    def log(msg, newline = true)
        puts(msg) if self.puts_progress && newline
        print(msg) if self.puts_progress && !newline
        Rails.logger.info(msg)
    end

    def process_lesson_batch(offset, lesson_count)
        # papertrail paranoia ... this particular action should never create new versions
        Lesson.paper_trail_off!

        update_lessons_and_versions(offset, lesson_count)

        # re-enable paper_trail
        Lesson.paper_trail_on!
    end

	def template_path
		raise "Subclasses of ComponentizedMigratorBase should define template_path"
	end

	def frame_types
		raise "Subclasses of ComponentizedMigratorBase should define frame_types"
	end

    def prepare_frame(frame)
        # subclasses can add extra stuff here 
    end

    def escape_text(text)
        text.gsub('\\') { '\\\\' }.gsub('"', '\"')
    end

	def update_lessons_and_versions(offset, lesson_count)

        @lessons.each_with_index do |lesson, i|

            return unless lesson.lesson_type == 'frame_list'
            return unless lesson.content.frames

            log "\nProcessing Lesson #{offset+i+1} of #{lesson_count}: \"#{lesson.title}\" ..."

            begin
                
                if update_lesson(lesson)
                    lesson.update_columns(content_json: lesson['content_json'])
                    log " | Saved"

                    # IMPORTANT! ensure we're updating the optimized (de-normalized) published content as well
                    published_version = lesson.published_version
                    if published_version && update_lesson(published_version) 
                        lesson.publish!(published_version)
                        log " | Re-published"
                    else
                        log " | Remains un-published"
                    end


                else
                    log " | Skipped"
                end
                
                update_papertrail_versions(lesson.versions)
            rescue Exception => e
                raise e if raise_on_failures
                log "Failed to save #{lesson.title.inspect} / #{lesson.id}: #{e.message}"
            end

            # force a GC on each lesson + version batch for good measure
            GC.start
        end

    end

    def update_papertrail_versions(versions)
        versions.reverse.each do |version| 

            # instantiate a lesson based on the old version
            lesson = version.reify

            # proceed if lesson can be decoded, otherwise delete it
            if !lesson.nil?
                begin
                    if update_lesson(lesson, version.id)
                        # handle serialization of papertrail object (see also: has_paper_trail.rb)
                        serialized_lesson = PaperTrail.serializer.dump(lesson.attributes)
                        version.object = serialized_lesson
                        version.save!(:validate => false)
                        log " | Saved"
                    else
                        log " | Skipped"
                    end
                rescue Exception => e
                    log "Failed to save ARCHIVED #{lesson.title.inspect} / #{lesson.id} / #{version.id}: #{e.message}"
                end
            else
                if version.event != 'create'
                    log "Could not recover version #{version.id}. Deleting stale record."
                    version.delete
                end
            end

        end
    end

    def update_lesson(lesson, version_id = nil) 

        # iterate through all lessons
        processed_frames = []

        version_id = version_id || "LATEST"

        log "    Version: #{version_id} | Frames: ", false

        first_frame_done = false
        frames_require_update = false
        frames = lesson.content.frames

        frames.each_with_index do |frame, index|

            # keep updates in new collection
            processed_frames << frame

            # single out a appropriate frame_type(s)
            next unless frame_types.include? frame['frame_type']

            
            # keep track of whether we need to rebuild the frames content or not
            log (!frames_require_update ? "" : ", ") + "#{index}", false
            frames_require_update = true

            # generate IDs
            @main_ui_component_id = SecureRandom.uuid
            @content_for_text_id = SecureRandom.uuid
            @continue_button_id = SecureRandom.uuid
            @main_text_id = SecureRandom.uuid
            

            # set main text
            @main_text = escape_text(frame['text_content'])


            # set images and designate the main image if applicable
            @images = []
            @content_for_image_id = ''
            frame.images && frame.images.each do |image|
                new_image = { 'id' => image['id'], 'label' => image['label'], 'json' => image.to_json }
                
                # the current image was previously delegated as the main image
                if image['id'] == frame['main_image_id']
                    @content_for_image_id = new_image['id']
                end

                @images << new_image
            end
            
            # .. similar process for modals
            @modals = []
            frame['modals'] && frame['modals'].each do |modal|
                modal_text = modal.gsub('\\') { '\\\\' }.gsub('"', '\"')
                new_modal = { 'id' => SecureRandom.uuid, 'text' => modal_text }
                @modals << new_modal
            end


            # set author_comments
            @author_comments_json = frame['author_comments'] ? frame['author_comments'].to_json : '[]'

            # migrate frame guid
            @frame_guid = frame['frame_guid']

            prepare_frame(frame)

            # update frame with rendered data
            renderer = ERB.new(@template_text) # re-instantiate due to memleak?
            rendered_text = renderer.result(binding)

            decoded = nil
            begin
                decoded = ActiveSupport::JSON.decode(rendered_text)
            rescue Exception => e
                log "Failed to decode json: #{e.message}"
                log rendered_text
                raise e
            end
            new_frame = Lesson::Content::FrameList::Frame.load(decoded)
            processed_frames[index] = new_frame

        end

        # prevent unnecessary json operations if no frames are modified
        if frames_require_update 
            # set new frames
            lesson.content.frames = processed_frames

            # usually found on save! callbacks
            lesson.send :reconcile_content_json
        else
            log "NA", false
        end

        return frames_require_update

    end



end