require File.expand_path('../componentized_erb_migrator_base', __FILE__)

class VennDiagramMigrator < ComponentizedErbMigratorBase
    
    # keys: ["_id", "answers", "author_comments", "frame_guid", "frame_type", "images", "prompts", "text_content"]

    # answers
    #  - "label", "correct_index", "correct", "message"
    # - label can be an image like: "![Underexposed3]"
    # - pretty sure "correct" is meaningless?
    # - pretty sure we should ignore message as well, since it is not context-sensitive?

    # prompts
    # - an array of strings

    attr_accessor :start_button_id, :layout_id, :answers, :answer_list_id, :challenges

    def template_path
        File.expand_path('../../templates/20140409114447_componentized_venn_diagram.json.erb', __FILE__)
    end

    def frame_types
        ['venn_diagram']
    end

    def prepare_frame(frame)
        @start_button_id = SecureRandom.uuid
        @layout_id = SecureRandom.uuid
        @answer_list_id = SecureRandom.uuid
        @answers = []
        @answer_lists = []
        @challenges = []
        @venn_diagram_headers = frame['prompts'].map do |prompt|
            {
                "id" => SecureRandom.uuid,
                "text" => escape_text(prompt),
                "text_id" => SecureRandom.uuid,
            }
        end

        old_answers = frame['answers']

        @shared_answer_list_id = SecureRandom.uuid
        @answer_lists << {
            'id' => @shared_answer_list_id,
            'answers' => @answers
        }

        old_answers.each do |old_answer|
            # create the answer
            new_answer = {
                "id" => SecureRandom.uuid,
                "text" => escape_text(old_answer['label']),
                "text_id" => SecureRandom.uuid,
                "message" => old_answer['message']
            }

            # add it to the global list
            @answers.push new_answer

            # if this answer is not a confuser, then create the challenge
            # for which it is the correct answer
            if old_answer['correct']
                @challenges << {
                    "id" => SecureRandom.uuid,
                    "correct_answer_id" => new_answer['id'],
                    "validator_id" => SecureRandom.uuid,
                    "expected_answer_matcher_id" => SecureRandom.uuid,
                    "answer_list_id" => @shared_answer_list_id,
                    "messages" => [],
                    "message_ids" => []
                }
            end

        end.compact
        @answers.each do |new_answer|
            if message = create_message(new_answer)
                @challenges[0]['messages'] << message
                @challenges[0]['message_ids'] << message['id']
            end
        end

    end

    def create_message(answer)
        if answer['message']
            {
                "id" => SecureRandom.uuid,
                "answer_id" => answer['id'],
                "text" => escape_text(answer['message']),
                "text_id" => SecureRandom.uuid,
                "expected_answer_matcher_id" => SecureRandom.uuid
            }
        end
    end

end
