require File.expand_path('../componentized_ruby_object_migrator_base', __FILE__)

class ThisOrThatTilePromptsMigrator < ComponentizedRubyObjectMigratorBase

	def should_process_frame?(frame)
		return false unless frame.respond_to?(:main_ui_component)
        return frame.main_ui_component.editor_template == 'this_or_that'
    end

    def process_frame(frame)
        
        # delete the intro content and answerlist from challenges (main) component
        frame.components.delete(frame.main_ui_component.intro_content_for_interactive_image);
        frame.main_ui_component.intro_content_for_interactive_image_id = nil

        answer_list = frame.main_ui_component.shared_content_for_interactive

        frame.main_ui_component.shared_content_for_interactive_id = nil

        # create and add the tile prompt board
        tile_prompt_board = Lesson::Content::FrameList::Frame::Componentized::Component.load({
        	component_type: 'ComponentizedFrame.TilePromptBoard',
        	id: SecureRandom.uuid
        })
        frame.components << tile_prompt_board

        # set challenges and answer list references
		tile_prompt_board.challenges_component = frame.main_ui_component
        tile_prompt_board.answer_list = answer_list

        # create tracking collection for tiles
        tile_prompts = []

        # loop through existing MCC's 
        frame.main_ui_component.challenges.each do |challenge|

            # update the answer list
            challenge.answer_list = answer_list

            # grab the existing spinning prompt
            spinning_prompt = challenge.content_for_interactive_image

            # create a new tile prompt
            tile_prompt = Lesson::Content::FrameList::Frame::Componentized::Component.load({
                component_type: 'ComponentizedFrame.TilePrompt',
                id: SecureRandom.uuid
            })
            tile_prompt.challenge = challenge

            # assign the spinning prompt text or image
            if spinning_prompt.text
                tile_prompt.text = spinning_prompt.text
            else
                tile_prompt.image = spinning_prompt.image
            end

            # append the prompt to running list of components
            tile_prompts << tile_prompt
            frame.components << tile_prompt

            # remove the challenge's content_for_interactive_image (spinning prompt) reference
            frame.components.delete(spinning_prompt)
            challenge.content_for_interactive_image_id = nil

        end
            

        # add them to the components list and tile prompt board
        tile_prompt_board.tile_prompts = tile_prompts
        
        # set the main component shared content
        frame.main_ui_component.shared_content_for_interactive_image = tile_prompt_board

    end

end
