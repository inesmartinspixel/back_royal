# this uses the ruby classes that descend from Lesson::Content::FrameList::Frame::Componentized::Component
# for converting frames.

class ComponentizedRubyObjectMigratorBase

	attr_accessor :lessons, :raise_on_failures, :puts_progress, :update_old_versions, :create_new_version_on_update, :batch_size, :opts

    # always flush the buffer
    $stdout.sync = true

    def initialize
        self.puts_progress = true
        self.raise_on_failures = true
        self.update_old_versions = true
        self.create_new_version_on_update = false
        self.batch_size = 10
        self.opts = {}
    end

	def change(lessons = nil, opts = {})
        self.opts = opts
        if !opts[:batch_size].blank?
            self.batch_size = opts[:batch_size].to_i
        end
        if !opts[:create_new_version_on_update].blank?
            self.create_new_version_on_update = (opts[:create_new_version_on_update] == 'true')
        end

        log "-- Running #{self.class.name} --"

        # process in batches if not supplied with concrete list
        if lessons
            log "----------- Processing All -----------"
            @lessons = lessons
            process_lesson_batch 0, lessons.length
        else
            lesson_count = Lesson.count
            current_batch = 0
            Lesson.find_in_batches(batch_size: self.batch_size)  do |lessons|
                log "----------- Processing Batch #{current_batch+1} -----------"
                @lessons = lessons
                process_lesson_batch(current_batch * self.batch_size, lesson_count)
                current_batch += 1
            end
        end

	end

    def log(msg, newline = true)
        puts(msg) if self.puts_progress && newline
        print(msg) if self.puts_progress && !newline
        Rails.logger.info(msg)
    end

    def process_lesson_batch(offset, lesson_count)
        # disable papertrail- lesson updates wont create new versions
        if !self.create_new_version_on_update
            Lesson.paper_trail_off!
        end

        update_lessons_and_versions(offset, lesson_count)

        # re-enable paper_trail
        if !self.create_new_version_on_update
            Lesson.paper_trail_on!
        end

    end

    def should_process_frame?(frame, opts = {})
        raise "Subclasses of ComponentizedRubyObjectMigratorBase should define should_process_frame?"
    end

    def process_frame(frame, opts = {})
        raise "Subclasses of ComponentizedRubyObjectMigratorBase should define process_frame"
    end

	def update_lessons_and_versions(offset, lesson_count)

        @lessons.each_with_index do |lesson, i|

            return unless lesson.lesson_type == 'frame_list'
            return unless lesson.content.frames

            log "\nProcessing Lesson #{offset+i+1} of #{lesson_count}: \"#{lesson.title}\" ..."

            begin
                
                if update_lesson(lesson)
                    if !self.create_new_version_on_update
                        lesson.update_columns(content_json: lesson['content_json'])
                    else 
                        lesson.save!(:validate => false)
                    end
                    log " | Saved"

                    # IMPORTANT! ensure we're updating the optimized (de-normalized) published content as well
                    published_version = lesson.published_version
                    if published_version && update_lesson(published_version, "PUBLISHED") 
                        lesson.publish!(published_version, self.opts.merge(:validate => false))
                        log " | Re-published"
                    else
                        log " | Remains un-published"
                    end

                else
                    log " | Skipped"
                end
                
                update_papertrail_versions(lesson.versions) if update_old_versions
            rescue Exception => e
                raise e if raise_on_failures
                log "********************************************************"
                log "Failed to save #{lesson.title.inspect} / #{lesson.id}: #{e.message}"
                log "********************************************************"
            end

            # force a GC on each lesson + version batch for good measure
            GC.start
        end

    end

    def update_papertrail_versions(versions)
        versions.reverse.each do |version| 

            # instantiate a lesson based on the old version
            lesson = version.reify

            # proceed if lesson can be decoded, otherwise delete it
            if !lesson.nil?
                begin
                    if update_lesson(lesson, version.id)
                        # handle serialization of papertrail object (see also: has_paper_trail.rb)
                        serialized_lesson = PaperTrail.serializer.dump(lesson.attributes)
                        version.object = serialized_lesson
                        version.save!(:validate => false)
                        log " | Saved"
                    else
                        log " | Skipped"
                    end
                rescue Exception => e
                    raise e if raise_on_failures
                    log "********************************************************"
                    log "Failed to save ARCHIVED #{lesson.title.inspect} / #{lesson.id} / #{version.id}: #{e.message}"
                    log "********************************************************"
                end
            else
                if version.event != 'create'
                    log "Could not recover version #{version.id}. Deleting stale record."
                    version.delete
                end
            end

        end
    end

    def update_lesson(lesson, identifier = nil) 

        identifier = identifier || "LATEST"

        log "    Version: #{identifier} | Frames: ", false

        first_frame_done = false
        frames_require_update = false
        frames = lesson.content.frames

        frames.each_with_index do |frame, index|

            # single out a appropriate frame_type(s)
            next unless should_process_frame?(frame, self.opts)

            # keep track of whether we need to rebuild the frames content or not
            log (!frames_require_update ? "" : ", ") + "#{index}", false
            frames_require_update = true

            process_frame(frame, self.opts)

        end

        # prevent unnecessary json operations if no frames are modified
        if frames_require_update 
            # usually found on save! callbacks
            lesson.send :reconcile_content_json
        else
            log "NA", false
        end

        return frames_require_update

    end



end