require File.expand_path('../componentized_erb_migrator_base', __FILE__)

class ThisOrThatMigrator < ComponentizedErbMigratorBase
	
	# keys: ["_id", "answers", "author_comments", "frame_guid", "frame_type", "images", "prompts", "text_content"]

	# answers
	#  - "label", "correct_index", "correct", "message"
	# - label can be an image like: "![Underexposed3]"
	# - pretty sure "correct" is meaningless?
	# - pretty sure we should ignore message as well, since it is not context-sensitive?

	# prompts
	# - an array of strings

	attr_accessor :start_button_id, :layout_id, :answers, :answer_list_id, :challenges

	def template_path
		File.expand_path('../../templates/20140319202010_componentized_this_or_that.json.erb', __FILE__)
	end

	def frame_types
		['this_or_that']
	end

	def prepare_frame(frame)
		@start_button_id = SecureRandom.uuid
		@layout_id = SecureRandom.uuid
		@answer_list_id = SecureRandom.uuid
		@answers = frame['prompts'].map do |prompt|
			{
				"id" => SecureRandom.uuid,
				"text" => escape_text(prompt),
				"text_id" => SecureRandom.uuid,
			}
		end

		@challenges = frame['answers'].map do |answer|
			
			opts = {
				"id" => SecureRandom.uuid,
				"correct_answer_id" => @answers[answer['correct_index']]['id'],
				"validator_id" => SecureRandom.uuid,
				"expected_answer_matcher_id" => SecureRandom.uuid,
				"spinning_prompt_id" => SecureRandom.uuid
			}

			if answer['label'].starts_with?('!')
				image_label = answer['label'].match(/\!\[(\w+)\]/)[1]
				image_id = @images.detect { |image| image['label'] == image_label }['id']
				opts["spinning_prompt_image_id"] = image_id
			else
				opts["text"] = escape_text(answer['label'])
				opts["spinning_prompt_text_id"] = SecureRandom.uuid
			end

			opts
			
		end
	end

end
