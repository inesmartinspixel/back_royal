require File.expand_path('../componentized_ruby_object_migrator_base', __FILE__)

class RemoveMiniInstructionsMigrator < ComponentizedRubyObjectMigratorBase

    def should_process_frame?(frame)
        raise_on_failures = true
        return false unless frame.is_a?(Lesson::Content::FrameList::Frame::Componentized)
        return !!frame['miniInstructionsMap']
    end

    def process_frame(frame)
        raise_on_failures = true
        # you wouldn't think it, but this does in fact remove the key
        frame.miniInstructionsMap = nil
    end

end