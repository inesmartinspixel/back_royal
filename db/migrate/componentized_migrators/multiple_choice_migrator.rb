require File.expand_path('../componentized_erb_migrator_base', __FILE__)

class MultipleChoiceMigrator < ComponentizedErbMigratorBase

	attr_accessor :layout_id, :answers, :answer_list, :challenge, :check_many, :button_skin_type

	def template_path
		File.expand_path('../../templates/20140407175115_componentized_multiple_choice.json.erb', __FILE__)
	end

	def frame_types
		['multiple_choice', 'check_many', 'multiple_choice_image']
	end

	def prepare_frame(frame)
		@layout_id = SecureRandom.uuid
		@answers = []
		@answer_lists = []

		@check_many = frame['frame_type'] == 'check_many'
		@button_skin_type = @check_many ? 'checkboxes' : 'buttons'

		prepare_answer_components(frame);

	end


	def prepare_answer_components(frame)

		@answer_list = {
			'id' => SecureRandom.uuid,
			'answers' => @answers
		}

		@challenge = {
					"id" => SecureRandom.uuid,
					"validator_id" => SecureRandom.uuid,
					"answer_list_id" => @answer_list['id'],
					"expected_answer_matcher_ids" => [],
					"correct_answer_ids" => [],
					"messages" => [],
					"message_ids" => []
				}

		old_answers = frame['answers']


		old_answers.each do |old_answer|

			# create the answer
			new_answer = {
		 		"id" => SecureRandom.uuid,
		 		"message" => old_answer['message']
		 	}

		 	# handle possible multiple_choice_image
		 	if frame['frame_type'] == 'multiple_choice_image'
		 		new_answer['image_id'] = old_answer['label']
		 	else
		 		new_answer['text_id'] = SecureRandom.uuid
		 		new_answer['text'] = escape_text(old_answer['label'])
		 	end

		 	# add it to the global list
		 	@answers.push new_answer

		 	# if the old answer is marked as correct, add it to the list of correct answer ids,
		 	# generating a matcher that corresponds to it
			if old_answer['correct'] == true
				@challenge['correct_answer_ids'] << new_answer['id']
				@challenge['expected_answer_matcher_ids'] << SecureRandom.uuid
			end

			if message = create_message(new_answer)
				@challenge['messages'] << message
				@challenge['message_ids'] << message['id']
			end

		end
		
	end


	def create_message(answer)
		if !answer['message'].blank?
			{
				"id" => SecureRandom.uuid,
				"answer_id" => answer['id'],
				"text" => escape_text(answer['message']),
				"text_id" => SecureRandom.uuid,
				"expected_answer_matcher_id" => SecureRandom.uuid
			}
		end
	end



end
