class UpdateCohortUserPeriods < ActiveRecord::Migration[5.1]
    def up

        execute %Q~
            CREATE FUNCTION array_intersect(anyarray, anyarray)
              RETURNS anyarray
              language sql
            as $FUNCTION$
                SELECT ARRAY(
                    SELECT UNNEST($1)
                    INTERSECT
                    SELECT UNNEST($2)
                );
            $FUNCTION$;

            create aggregate array_cat(anyarray)
            (
                sfunc = array_cat,
                stype = anyarray
            );
        ~
        ViewHelpers.migrate(self, 20180202021428)

    end

    def down
        ViewHelpers.rollback(self, 20180202021428)

        execute '
            drop function array_intersect(anyarray, anyarray);
            drop aggregate array_cat(anyarray);
        '    end
end