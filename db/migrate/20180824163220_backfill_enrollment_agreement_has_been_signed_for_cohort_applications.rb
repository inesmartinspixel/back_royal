class BackfillEnrollmentAgreementHasBeenSignedForCohortApplications < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        CohortApplication.in_batches.update_all enrollment_agreement_has_been_signed: false
        change_column_null :cohort_applications, :enrollment_agreement_has_been_signed, false
    end

    def down
        change_column_null :cohort_applications, :enrollment_agreement_has_been_signed, true
    end
end
