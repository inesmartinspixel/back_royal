class CreateS3EnglishLanguageProficiencyDocuments < ActiveRecord::Migration[5.1]

    def change
        create_table :s3_english_language_proficiency_documents, id: :uuid do |t|
            t.timestamps
            t.attachment :file
            t.string :file_fingerprint
            t.uuid :user_id
        end

        add_index :s3_english_language_proficiency_documents, :user_id
    end
end
