class TurnOffEventsAutovacuum < ActiveRecord::Migration[6.0]
    def up
        execute("ALTER TABLE events SET (autovacuum_enabled = false, toast.autovacuum_enabled = false);")
    end

    def down
        execute("ALTER TABLE events SET (autovacuum_enabled = true, toast.autovacuum_enabled = true);")
    end
end
