class AddRubricInheritedToCohortApplications < ActiveRecord::Migration[5.2]
    def up
        add_column :cohort_applications, :rubric_inherited, :boolean, default: false, null: false
        add_column :cohort_applications_versions, :rubric_inherited, :boolean
        recreate_trigger_v2(:cohort_applications)
    end

    def down
        remove_column :cohort_applications, :rubric_inherited, :boolean, default: false, null: false
        remove_column :cohort_applications_versions, :rubric_inherited, :boolean
        recreate_trigger_v2(:cohort_applications)
    end
end
