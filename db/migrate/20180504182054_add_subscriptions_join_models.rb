class AddSubscriptionsJoinModels < ActiveRecord::Migration[5.1]
    def up

        # add join table for hiring teams
        create_table :subscriptions_hiring_teams, id: :uuid do |t|
            t.uuid :hiring_team_id, null: false
            t.uuid :subscription_id, null: false
        end
        add_index :subscriptions_hiring_teams, [:subscription_id], :unique => true # each subscription can only be joined with one hiring_team
        add_foreign_key :subscriptions_hiring_teams, :hiring_teams
        add_foreign_key :subscriptions_hiring_teams, :subscriptions


        # stop requiring a user_id on subscriptions
        change_column_null :subscriptions, :user_id, true
        change_column_null :subscriptions_versions, :user_id, true

        # convert user_id column to join tables (we'll delete the column later)
        execute %Q~

            insert into subscriptions_users (user_id, subscription_id)
                select subscriptions.user_id, subscriptions.id
                from subscriptions
                    left join subscriptions_users
                        on subscriptions_users.subscription_id = subscriptions.id
                where subscriptions_users.subscription_id is null
        ~


        # add owner_id to hiring teams and backfill initial values
        # Even after backfilling, allow nil, since we need to be able to create
        # teams initally in the admin ui with no users
        add_column :hiring_teams, :owner_id, :uuid
        add_column :hiring_teams_versions, :owner_id, :uuid
        add_foreign_key :hiring_teams, :users, column: "owner_id", primary_key: "id"

        # As a first pass, assign the oldest user as the owner
        # of each team.  The career network team will have to go through
        # and check everyone manuall
        execute %Q~

            UPDATE hiring_teams
                SET owner_id=subquery.user_id
                FROM (
                    SELECT
                        hiring_teams.id hiring_team_id,
                        (array_agg(users.id order by users.created_at))[1] user_id
                    from
                        hiring_teams
                        join users on hiring_teams.id = users.hiring_team_id
                    group by hiring_teams.id
                ) AS subquery
                WHERE hiring_teams.id=subquery.hiring_team_id
        ~

    end

    def down
        # remove the owner_id from hiring teams
        remove_column :hiring_teams_versions, :owner_id
        remove_column :hiring_teams, :owner_id

        # go back to requiring a user_id on subscriptions
        change_column_null :subscriptions, :user_id, true
        change_column_null :subscriptions_versions, :user_id, true

        # drop join tables
        drop_table :subscriptions_hiring_teams

    end
end
