class CreateAvatarAssets < ActiveRecord::Migration[5.1]
    def up
        create_table    "avatar_assets", id: :uuid do |t|
            t.timestamps
            t.string    "file_file_name"
            t.string    "file_content_type"
            t.integer   "file_file_size"
            t.datetime  "file_updated_at"
            t.string    "file_fingerprint"
            t.text      "source_url"
            t.text      "url"
        end
    end

    def down
        execute "DROP TABLE IF EXISTS avatar_assets"
    end

end
