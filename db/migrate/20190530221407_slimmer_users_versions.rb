class SlimmerUsersVersions < ActiveRecord::Migration[5.2]
    def up

        recreate_users_trigger_v3

        remove_column :users_versions, :encrypted_password
        remove_column :users_versions, :reset_password_token
        remove_column :users_versions, :reset_password_sent_at
        remove_column :users_versions, :remember_created_at
        remove_column :users_versions, :tokens
        remove_column :users_versions, :reset_password_redirect_url
        remove_column :users_versions, :optimizely_segments
        remove_column :users_versions, :optimizely_referer
        remove_column :users_versions, :optimizely_buckets


    end

    def down

        add_column :users_versions, :encrypted_password, :string
        add_column :users_versions, :reset_password_token, :string
        add_column :users_versions, :reset_password_sent_at, :datetime
        add_column :users_versions, :remember_created_at, :datetime
        add_column :users_versions, :tokens, :json
        add_column :users_versions, :reset_password_redirect_url, :string
        add_column :users_versions, :optimizely_segments, :json
        add_column :users_versions, :optimizely_referer, :string
        add_column :users_versions, :optimizely_buckets, :json

        recreate_users_trigger_v2
    end
end
