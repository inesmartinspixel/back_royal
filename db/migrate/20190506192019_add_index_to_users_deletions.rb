class AddIndexToUsersDeletions < ActiveRecord::Migration[5.2]
    disable_ddl_transaction!

    def change
        add_index :users_deletions, :user_id, algorithm: :concurrently
    end
end
