class BackfillCareerProfileSearchPlaces < ActiveRecord::Migration[5.1]
    def up

        # Code for generating the locations table:
        # puts Location.locations.map { |pair| "select '#{pair[0]}' as key, '#{pair[1].place_details.to_json}'::json as place_details\n" }.join("union all\n")

        execute %Q~

            -- create a table with location keys mapped to place_details
            -- this was generated code above in a rails console:
            with locations AS MATERIALIZED (
                select 'san_francisco' as key, '{"locality":{"short":"SF","long":"San Francisco"},"administrative_area_level_2":{"short":"San Francisco County","long":"San Francisco County"},"administrative_area_level_1":{"short":"CA","long":"California"},"country":{"short":"US","long":"United States"},"formatted_address":"San Francisco, CA, USA","utc_offset":-420,"lat":37.7749295,"lng":-122.4194155}'::json as place_details
                union all
                select 'new_york' as key, '{"locality":{"short":"New York","long":"New York"},"administrative_area_level_1":{"short":"NY","long":"New York"},"country":{"short":"US","long":"United States"},"formatted_address":"New York, NY, USA","utc_offset":-240,"lat":40.7127753,"lng":-74.0059728}'::json as place_details
                union all
                select 'seattle' as key, '{"locality":{"short":"Seattle","long":"Seattle"},"administrative_area_level_2":{"short":"King County","long":"King County"},"administrative_area_level_1":{"short":"WA","long":"Washington"},"country":{"short":"US","long":"United States"},"formatted_address":"Seattle, WA, USA","utc_offset":-420,"lat":47.6062095,"lng":-122.3320708}'::json as place_details
                union all
                select 'denver' as key, '{"locality":{"short":"Denver","long":"Denver"},"administrative_area_level_2":{"short":"Denver County","long":"Denver County"},"administrative_area_level_1":{"short":"CO","long":"Colorado"},"country":{"short":"US","long":"United States"},"formatted_address":"Denver, CO, USA","utc_offset":-360,"lat":39.7392358,"lng":-104.990251}'::json as place_details
                union all
                select 'boston' as key, '{"locality":{"short":"Boston","long":"Boston"},"administrative_area_level_2":{"short":"Suffolk County","long":"Suffolk County"},"administrative_area_level_1":{"short":"MA","long":"Massachusetts"},"country":{"short":"US","long":"United States"},"formatted_address":"Boston, MA, USA","utc_offset":-240,"lat":42.3600825,"lng":-71.0588801}'::json as place_details
                union all
                select 'los_angeles' as key, '{"locality":{"short":"Los Angeles","long":"Los Angeles"},"administrative_area_level_2":{"short":"Los Angeles County","long":"Los Angeles County"},"administrative_area_level_1":{"short":"CA","long":"California"},"country":{"short":"US","long":"United States"},"formatted_address":"Los Angeles, CA, USA","utc_offset":-420,"lat":34.0522342,"lng":-118.2436849}'::json as place_details
                union all
                select 'san_diego' as key, '{"locality":{"short":"San Diego","long":"San Diego"},"administrative_area_level_2":{"short":"San Diego County","long":"San Diego County"},"administrative_area_level_1":{"short":"CA","long":"California"},"country":{"short":"US","long":"United States"},"formatted_address":"San Diego, CA, USA","utc_offset":-420,"lat":32.715738,"lng":-117.1610838}'::json as place_details
                union all
                select 'washington_dc' as key, '{"locality":{"short":"Washington","long":"Washington"},"administrative_area_level_2":{"short":"District of Columbia","long":"District of Columbia"},"administrative_area_level_1":{"short":"DC","long":"District of Columbia"},"country":{"short":"US","long":"United States"},"formatted_address":"Washington, DC, USA","utc_offset":-240,"lat":38.9071923,"lng":-77.0368707}'::json as place_details
                union all
                select 'chicago' as key, '{"locality":{"short":"Chicago","long":"Chicago"},"administrative_area_level_2":{"short":"Cook County","long":"Cook County"},"administrative_area_level_1":{"short":"IL","long":"Illinois"},"country":{"short":"US","long":"United States"},"formatted_address":"Chicago, IL, USA","utc_offset":-300,"lat":41.8781136,"lng":-87.6297982}'::json as place_details
                union all
                select 'austin' as key, '{"locality":{"short":"Austin","long":"Austin"},"administrative_area_level_2":{"short":"Travis County","long":"Travis County"},"administrative_area_level_1":{"short":"TX","long":"Texas"},"country":{"short":"US","long":"United States"},"formatted_address":"Austin, TX, USA","utc_offset":-300,"lat":30.267153,"lng":-97.7430608}'::json as place_details
                union all
                select 'atlanta' as key, '{"locality":{"short":"Atlanta","long":"Atlanta"},"administrative_area_level_2":{"short":"Fulton County","long":"Fulton County"},"administrative_area_level_1":{"short":"GA","long":"Georgia"},"country":{"short":"US","long":"United States"},"formatted_address":"Atlanta, GA, USA","utc_offset":-240,"lat":33.7489954,"lng":-84.3879824}'::json as place_details
                union all
                select 'dallas' as key, '{"locality":{"short":"Dallas","long":"Dallas"},"administrative_area_level_2":{"short":"Dallas County","long":"Dallas County"},"administrative_area_level_1":{"short":"TX","long":"Texas"},"country":{"short":"US","long":"United States"},"formatted_address":"Dallas, TX, USA","utc_offset":-300,"lat":32.7766642,"lng":-96.79698789999999}'::json as place_details
                union all
                select 'london' as key, '{"locality":{"short":"London","long":"London"},"postal_town":{"short":"London","long":"London"},"administrative_area_level_2":{"short":"Greater London","long":"Greater London"},"administrative_area_level_1":{"short":"England","long":"England"},"country":{"short":"GB","long":"United Kingdom"},"formatted_address":"London, UK","utc_offset":60,"lat":51.5073509,"lng":-0.1277583}'::json as place_details
                union all
                select 'raleigh_durham' as key, '{"neighborhood":{"short":"Umstead","long":"Umstead"},"locality":{"short":"Raleigh","long":"Raleigh"},"administrative_area_level_2":{"short":"Wake County","long":"Wake County"},"administrative_area_level_1":{"short":"NC","long":"North Carolina"},"country":{"short":"US","long":"United States"},"formatted_address":"Umstead, Raleigh, NC, USA","utc_offset":-240,"lat":35.88022059999999,"lng":-78.7532884}'::json as place_details
                union all
                select 'miami' as key, '{"locality":{"short":"Miami","long":"Miami"},"administrative_area_level_2":{"short":"Miami-Dade County","long":"Miami-Dade County"},"administrative_area_level_1":{"short":"FL","long":"Florida"},"country":{"short":"US","long":"United States"},"formatted_address":"Miami, FL, USA","utc_offset":-240,"lat":25.7616798,"lng":-80.1917902}'::json as place_details
                union all
                select 'houston' as key, '{"locality":{"short":"Houston","long":"Houston"},"administrative_area_level_2":{"short":"Harris County","long":"Harris County"},"administrative_area_level_1":{"short":"TX","long":"Texas"},"country":{"short":"US","long":"United States"},"formatted_address":"Houston, TX, USA","utc_offset":-300,"lat":29.7604267,"lng":-95.3698028}'::json as place_details
                union all
                select 'seoul' as key, '{"locality":{"short":"Seoul","long":"Seoul"},"administrative_area_level_1":{"short":"Seoul","long":"Seoul"},"country":{"short":"KR","long":"South Korea"},"formatted_address":"Seoul, South Korea","utc_offset":540,"lat":37.566535,"lng":126.9779692}'::json as place_details
                union all
                select 'singapore' as key, '{"locality":{"short":"Singapore","long":"Singapore"},"country":{"short":"SG","long":"Singapore"},"formatted_address":"Singapore","utc_offset":480,"lat":1.3553794,"lng":103.8677444}'::json as place_details
                union all
                select 'dubai' as key, '{"locality":{"short":"Dubai","long":"Dubai"},"administrative_area_level_1":{"short":"Dubai","long":"Dubai"},"country":{"short":"AE","long":"United Arab Emirates"},"formatted_address":"Dubai - United Arab Emirates","utc_offset":240,"lat":25.2048493,"lng":55.2707828}'::json as place_details
                union all
                select 'paris' as key, '{"locality":{"short":"Paris","long":"Paris"},"administrative_area_level_2":{"short":"Paris","long":"Paris"},"administrative_area_level_1":{"short":"Île-de-France","long":"Île-de-France"},"country":{"short":"FR","long":"France"},"formatted_address":"Paris, France","utc_offset":120,"lat":48.85661400000001,"lng":2.3522219}'::json as place_details
                union all
                select 'berlin' as key, '{"locality":{"short":"Berlin","long":"Berlin"},"administrative_area_level_1":{"short":"Berlin","long":"Berlin"},"country":{"short":"DE","long":"Germany"},"formatted_address":"Berlin, Germany","utc_offset":120,"lat":52.52000659999999,"lng":13.404954}'::json as place_details
                union all
                select 'hong_kong' as key, '{"country":{"short":"HK","long":"Hong Kong"},"formatted_address":"Hong Kong","utc_offset":480,"lat":22.396428,"lng":114.109497}'::json as place_details
                union all
                select 'beijing' as key, '{"locality":{"short":"Beijing","long":"Beijing"},"administrative_area_level_1":{"short":"Beijing","long":"Beijing"},"country":{"short":"CN","long":"China"},"formatted_address":"Beijing, China","utc_offset":480,"lat":39.90419989999999,"lng":116.4073963}'::json as place_details
                union all
                select 'shanghai' as key, '{"locality":{"short":"Shanghai","long":"Shanghai"},"administrative_area_level_1":{"short":"Shanghai","long":"Shanghai"},"country":{"short":"CN","long":"China"},"formatted_address":"Shanghai, China","utc_offset":480,"lat":31.2303904,"lng":121.4737021}'::json as place_details
                union all
                select 'tokyo' as key, '{"colloquial_area":{"short":"Tokyo","long":"Tokyo"},"administrative_area_level_1":{"short":"Tokyo","long":"Tokyo"},"country":{"short":"JP","long":"Japan"},"formatted_address":"Tokyo, Japan","utc_offset":540,"lat":35.7090259,"lng":139.7319925}'::json as place_details
            )

            -- find all the distinct values of `locations` in career_profile_searches,
            -- unnest the locations
            -- and add an index.
            -- The result looks like:

            --      locations,location,i
            --      {atlanta},atlanta,1
            --      {austin},austin,1
            --      "{austin,san_francisco,chicago,boston,denver,los_angeles}",austin,1
            --      "{austin,san_francisco,chicago,boston,denver,los_angeles}",san_francisco,2
            --      "{austin,san_francisco,chicago,boston,denver,los_angeles}",chicago,3
            --      "{austin,san_francisco,chicago,boston,denver,los_angeles}",boston,4
            --      "{austin,san_francisco,chicago,boston,denver,los_angeles}",denver,5
            --      "{austin,san_francisco,chicago,boston,denver,los_angeles}",los_angeles,6

            , distinct_searches AS MATERIALIZED (
                SELECT
                    distinct
                        locations
                        , location.location
                        , location.i
                from career_profile_searches
                    , unnest(career_profile_searches.locations) WITH ORDINALITY location(location, i)
                where array_length(locations,1) > 0

                    -- oddly, array_length is null for empty arrays
                    and (array_length(places,1) is null or array_length(places,1) = 0 )
                order by
                    locations
                    , i
            )

            -- join the two tables above, aggregating by the full locations
            -- value and sorting the places by i so the places will be
            -- in the same order as the location keys
            , locations_to_place_details_map AS MATERIALIZED (
                SELECT
                    locations
                    , array_agg(locations.place_details order by i) as places
                FROM distinct_searches
                    join locations
                        on locations.key = distinct_searches.location
                group by locations
            )

            -- update
            UPDATE career_profile_searches
            SET places=subquery.places
            FROM (SELECT locations, places
                  FROM  locations_to_place_details_map) AS subquery
            WHERE career_profile_searches.locations=subquery.locations;
        ~
    end

    def down
        execute %q~
            update career_profile_searches set places = Array[]::json[]
        ~
    end
end
