class AddNotesToCohorts < ActiveRecord::Migration[5.1]
    def change

        add_column :cohorts, :internal_notes, :text
        add_column :cohorts_versions, :internal_notes, :text

    end
end
