class BackfillLevelsOfInterest < ActiveRecord::Migration[5.1]
    # we are disabling transactions so that we don't update every row in the
    # table in a single transaction.  In order for this to be ok, we need
    # to make sure everything in this migration is idempotent
    disable_ddl_transaction!

    def up
        # Backfill (idempotent)
        CareerProfile::Search.in_batches.update_all levels_of_interest: []

        # Set nullable (idempotent)
        change_column_null :career_profile_searches, :levels_of_interest, false
    end

    def down
        change_column_null :career_profile_searches, :levels_of_interest, true
    end
end
