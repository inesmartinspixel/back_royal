class AddFieldsForGranularTranscriptTracking < ActiveRecord::Migration[5.2]
    def up
        add_column :education_experiences, :s3_transcript_asset_id, :uuid
        add_column :education_experiences, :transcript_waiver, :text
        add_column :education_experiences, :transcript_approved, :boolean, default: false, null: false
        add_index :education_experiences, :s3_transcript_asset_id, unique: true
        add_foreign_key :education_experiences, :s3_transcript_assets

        add_column :education_experiences_versions, :s3_transcript_asset_id, :uuid
        add_column :education_experiences_versions, :transcript_waiver, :text
        add_column :education_experiences_versions, :transcript_approved, :boolean

        recreate_trigger_v2(:education_experiences)
    end

    def down
        remove_column :education_experiences, :s3_transcript_asset_id
        remove_column :education_experiences, :transcript_waiver
        remove_column :education_experiences, :transcript_approved

        remove_column :education_experiences_versions, :s3_transcript_asset_id
        remove_column :education_experiences_versions, :transcript_waiver
        remove_column :education_experiences_versions, :transcript_approved

        recreate_trigger_v2(:education_experiences)
    end
end
