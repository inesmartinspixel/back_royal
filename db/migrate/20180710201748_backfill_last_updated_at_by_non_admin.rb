class BackfillLastUpdatedAtByNonAdmin < ActiveRecord::Migration[5.1]

    disable_ddl_transaction!

    def change

        execute %Q~
            UPDATE candidate_position_interests
            SET last_updated_at_by_non_admin = last_updated_at_by_hiring_manager
            WHERE last_updated_at_by_hiring_manager IS NOT NULL
        ~

    end

end
