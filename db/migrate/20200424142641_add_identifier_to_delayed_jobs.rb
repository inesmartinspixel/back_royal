class AddIdentifierToDelayedJobs < ActiveRecord::Migration[6.0]
    def up
        add_column :delayed_jobs, :identifier, :text, null: true
        execute "CREATE INDEX identifier_on_delayed_jobs ON delayed_jobs (queue, identifier)"
    end

    def down
        execute "DROP INDEX identifier_on_delayed_jobs;"
        remove_column :delayed_jobs, :identifier
    end
end