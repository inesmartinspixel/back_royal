class RemoveArchivedWithoutConversation < ActiveRecord::Migration[5.1]
    def change
        remove_column :hiring_relationships, :hiring_manager_archived_without_conversation, :boolean, :default => false
        remove_column :hiring_relationships_versions, :hiring_manager_archived_without_conversation, :boolean
        remove_column :hiring_relationships, :candidate_archived_without_conversation, :boolean, :default => false
        remove_column :hiring_relationships_versions, :candidate_archived_without_conversation, :boolean
    end
end
