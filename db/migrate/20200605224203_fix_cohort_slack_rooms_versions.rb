class FixCohortSlackRoomsVersions < ActiveRecord::Migration[6.0]
    def up
        change_column_null :cohort_slack_rooms_versions, :admin_token, true
        change_column_null :cohort_slack_rooms_versions, :cohort_id, true
        change_column_null :cohort_slack_rooms_versions, :title, true
        change_column_null :cohort_slack_rooms_versions, :url, true
    end

    def down
        # no-op
    end
end
