class AddEarlyRegistrationDeadlineDaysOffsetToCohortsAndTemplates < ActiveRecord::Migration[5.1]

    def up
        add_column :cohorts, :early_registration_deadline_days_offset, :integer
        add_column :cohorts_versions, :early_registration_deadline_days_offset, :integer
        add_column :curriculum_templates, :early_registration_deadline_days_offset, :integer
        add_column :curriculum_templates_versions, :early_registration_deadline_days_offset, :integer

        change_column_default(:cohorts, :early_registration_deadline_days_offset, 0)
        change_column_default(:curriculum_templates, :early_registration_deadline_days_offset, 0)
    end

    def down
        remove_column :cohorts, :early_registration_deadline_days_offset
        remove_column :cohorts_versions, :early_registration_deadline_days_offset
        remove_column :curriculum_templates, :early_registration_deadline_days_offset
        remove_column :curriculum_templates_versions, :early_registration_deadline_days_offset
    end

end
