class RemoveDeprecatedOffsets < ActiveRecord::Migration[5.2]

    def up
        remove_column :cohorts, :graduation_date_days_offset
        remove_column :cohorts_versions, :graduation_date_days_offset
        remove_column :curriculum_templates, :graduation_date_days_offset
        remove_column :curriculum_templates_versions, :graduation_date_days_offset

        remove_column :cohorts, :diploma_generation_days_offset
        remove_column :cohorts_versions, :diploma_generation_days_offset
        remove_column :curriculum_templates, :diploma_generation_days_offset
        remove_column :curriculum_templates_versions, :diploma_generation_days_offset

        recreate_trigger_v2(:cohorts)
        recreate_trigger_v2(:curriculum_templates)
    end

    def down

        add_column :cohorts, :graduation_date_days_offset, :integer
        add_column :cohorts_versions, :graduation_date_days_offset, :integer
        add_column :curriculum_templates, :graduation_date_days_offset, :integer
        add_column :curriculum_templates_versions, :graduation_date_days_offset, :integer

        add_column :cohorts, :diploma_generation_days_offset, :integer
        add_column :cohorts_versions, :diploma_generation_days_offset, :integer
        add_column :curriculum_templates, :diploma_generation_days_offset, :integer
        add_column :curriculum_templates_versions, :diploma_generation_days_offset, :integer

        recreate_trigger_v2(:cohorts)
        recreate_trigger_v2(:curriculum_templates)
    end

end
