class AddBotUserAccessTokenToCohortSlackRooms < ActiveRecord::Migration[6.0]
    def up
        add_column :cohort_slack_rooms, :bot_user_access_token, :text
        add_column :cohort_slack_rooms_versions, :bot_user_access_token, :text

        recreate_trigger_v2(:cohort_slack_rooms)
    end

    def down
        remove_column :cohort_slack_rooms, :bot_user_access_token, :text
        remove_column :cohort_slack_rooms_versions, :bot_user_access_token, :text

        recreate_trigger_v2(:cohort_slack_rooms)
    end
end
