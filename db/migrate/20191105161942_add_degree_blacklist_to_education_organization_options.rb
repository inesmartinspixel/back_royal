class AddDegreeBlacklistToEducationOrganizationOptions < ActiveRecord::Migration[6.0]

    def up
        add_column :educational_organization_options, :degree_blacklist, :boolean, :default => false, :null => false
        add_column :educational_organization_options_versions, :degree_blacklist, :boolean

        recreate_trigger_v2(:educational_organization_options)
    end

    def down
        remove_column :educational_organization_options, :degree_blacklist
        remove_column :educational_organization_options_versions, :degree_blacklist

        recreate_trigger_v2(:educational_organization_options)
    end

end
