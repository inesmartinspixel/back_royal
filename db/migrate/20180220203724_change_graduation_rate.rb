class ChangeGraduationRate < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20180220203724)
    end

    def down
        ViewHelpers.rollback(self, 20180220203724)
    end
end
