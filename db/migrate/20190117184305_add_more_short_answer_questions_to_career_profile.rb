class AddMoreShortAnswerQuestionsToCareerProfile < ActiveRecord::Migration[5.2]

    def up
        add_column :career_profiles, :short_answer_why_pursuing, :text
        add_column :career_profiles, :short_answer_use_skills_to_advance, :text
        add_column :career_profiles, :short_answer_ask_professional_advice, :text

        add_column :career_profiles_versions, :short_answer_why_pursuing, :text
        add_column :career_profiles_versions, :short_answer_use_skills_to_advance, :text
        add_column :career_profiles_versions, :short_answer_ask_professional_advice, :text

        recreate_trigger_v2 "career_profiles"
    end

    def down
        remove_column :career_profiles, :short_answer_why_pursuing, :text
        remove_column :career_profiles, :short_answer_use_skills_to_advance, :text
        remove_column :career_profiles, :short_answer_ask_professional_advice, :text

        remove_column :career_profiles_versions, :short_answer_why_pursuing, :text
        remove_column :career_profiles_versions, :short_answer_use_skills_to_advance, :text
        remove_column :career_profiles_versions, :short_answer_ask_professional_advice, :text

        recreate_trigger_v2 "career_profiles"
    end
end
