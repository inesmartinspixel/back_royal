class AddActiveInstitution < ActiveRecord::Migration[6.0]  
    def up
        add_column :institutions_users, :active, :boolean, default: false, null: false
        add_column :institutions_users_versions, :active, :boolean
        add_index :institutions_users, [:user_id, :active], where: 'active = true', unique: true

        # We shouldn't be allowing duplicate join records
        # Note: It looks like the index needs to be rebuilt to become unique.
        #   See https://stackoverflow.com/a/46167539/1747491
        remove_index :institutions_users, [:institution_id, :user_id]
        add_index :institutions_users, [:institution_id, :user_id], unique: true

        # Backfill a primary key column so that we have access to ActiveRecord updates for the
        # newly defined associations in user.rb
        # Note: While technically a composite primary key feels like it could be more appropriate
        #   here, it'd probably more trouble than it's worth.
        #   See https://github.com/composite-primary-keys/composite_primary_keys
        #   and https://stackoverflow.com/a/41888654/1747491
        add_column :institutions_users, :id, :uuid, default: 'uuid_generate_v4()'
        add_column :institutions_users_versions, :id, :uuid
        execute "ALTER TABLE institutions_users ADD PRIMARY KEY (id);"

        recreate_trigger_v2(:institutions_users)
    end

    def down
        remove_column :institutions_users, :active
        remove_column :institutions_users_versions, :active
        # Note: When you remove the column it removes the unique index too

        remove_index :institutions_users, [:institution_id, :user_id]
        add_index :institutions_users, [:institution_id, :user_id]

        remove_column :institutions_users, :id
        remove_column :institutions_users_versions, :id

        recreate_trigger_v2(:institutions_users)
    end
end
