class AddMoreTranscriptColumnsToEducationExperiences < ActiveRecord::Migration[6.0]

    def up
        add_column :education_experiences, :official_transcript_required, :boolean, :default => false, :null => false
        add_column :education_experiences_versions, :official_transcript_required, :boolean

        recreate_trigger_v2(:education_experiences)
    end

    def down
        remove_column :education_experiences, :official_transcript_required
        remove_column :education_experiences_versions, :official_transcript_required

        recreate_trigger_v2(:education_experiences)
    end
end
