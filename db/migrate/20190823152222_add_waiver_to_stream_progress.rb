class AddWaiverToStreamProgress < ActiveRecord::Migration[5.2]
    def change
        add_column :lesson_streams_progress, :waiver, :text
    end
end
