class MoveS3AssetToMainModel < ActiveRecord::Migration[5.2]
    def up
        drop_table :s3_signable_document_assets

        change_table :signable_documents do |t|
            t.attachment :file
            t.string :file_fingerprint
        end

        remove_column :signable_documents, :s3_asset_id

        # I'm not sure why this isn't already null:false.  The original Migration
        # said it should be, but maybe the call to `references` messed that up
        change_column_null :signable_documents, :user_id, false
    end

    def down
        create_table :s3_signable_document_assets, id: :uuid do |t|
            t.timestamps
            t.attachment :file
            t.string :file_fingerprint
        end

        change_column_null :signable_documents, :user_id, true

        add_column :signable_documents, :s3_asset_id, :uuid
        remove_column :signable_documents, :file_file_name
        remove_column :signable_documents, :file_content_type
        remove_column :signable_documents, :file_file_size
        remove_column :signable_documents, :file_updated_at
        remove_column :signable_documents, :file_fingerprint

    end
end
