class CreateS3ProjectDocuments < ActiveRecord::Migration[5.1]
    def change
        create_table :s3_project_documents, id: :uuid do |t|
            t.timestamps
            t.attachment :file
            t.string :file_fingerprint
        end
    end
end
