class BackfillTranscriptsVerifiedForUsers < ActiveRecord::Migration[5.1]
    # disable_ddl_transaction!

    # see also: https://github.com/lynndylanhurley/devise_token_auth/issues/1079#issuecomment-363155625
    # class User < ApplicationRecord
    #     def tokens_has_json_column_type?
    #         false
    #     end
    # end

    def up
        # We used to verify the user's identification and transcript documents at the same time,
        # so for all users where identity_verified is already set to true, transcripts_verified
        # should also be true. It's also worth noting that we no longer require transcripts for
        # certain users (e.g. if the user has no formal education or has formal education that
        # they will_not_complete), so we are still allowing this column to be nullable.
        # User.where(identity_verified: true).in_batches.update_all(transcripts_verified: true)
    end

    def down
        # noop
    end
end
