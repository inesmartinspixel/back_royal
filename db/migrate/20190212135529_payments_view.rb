class PaymentsView < ActiveRecord::Migration[5.2]
    def up
        ViewHelpers.migrate(self, 20190212135529)
    end

    def down
        ViewHelpers.rollback(self, 20190212135529)
    end
end
