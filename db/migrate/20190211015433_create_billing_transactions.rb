class CreateBillingTransactions < ActiveRecord::Migration[5.2]

    def up
        # we are renaming the charges table to billing_transactions, but to do this
        # safely in production, we need to keep the old charges table around
        create_table :billing_transactions, :id => :uuid do |t|
            t.timestamps
            t.timestamp :transaction_time, null: false
            t.text :transaction_type, null: false
            t.text :provider, null: false
            t.text :provider_transaction_id, null: false
            t.float :amount, null: false
            t.float :amount_refunded, null: false, default: 0
            t.boolean :refunded, null: false, default: false
            t.text :currency, null: false
            t.text :description
            t.json :metadata
            t.boolean :stripe_livemode
        end

        create_versions_table_and_trigger_v2(:billing_transactions)

        add_index :billing_transactions, [:provider, :provider_transaction_id], unique: true, :name => :uniq_on_provider_and_transaction_id

        create_table :billing_transactions_users, :id => :uuid do |t|
            t.references :billing_transaction, index: { name: :billing_transactions_users_join}, type: :uuid, foreign_key: true

            # no foreign key on user because it is allowed for a user to be
            # deleted while keeping around zir billing_transactions
            t.references :user, type: :uuid
        end

        create_table :billing_transactions_hiring_teams, :id => :uuid  do |t|
            t.references :billing_transaction, index: { name: :billing_transactions_hiring_teams_join}, type: :uuid, foreign_key: true


            # no foreign key on hiring_team because it is allowed for a hiring_team to be
            # deleted while keeping around zir billing_transactions
            t.references :hiring_team, type: :uuid
        end

        # copy billing transactions
        execute %Q~
            insert into billing_transactions(
                id,
                created_at,
                updated_at,
                transaction_time,
                transaction_type,
                provider,
                provider_transaction_id,
                amount,
                amount_refunded,
                refunded,
                currency,
                description,
                metadata,
                stripe_livemode
            )
            select
                id,
                created_at,
                updated_at,
                charge_time,
                'payment',
                provider,
                provider_charge_id,
                amount,
                amount_refunded,
                refunded,
                currency,
                description,
                metadata,
                stripe_livemode
            from charges
        ~

        # copy joins
        execute %Q~
            insert into billing_transactions_users (user_id, billing_transaction_id)
                select user_id, charge_id from charges_users;

            insert into billing_transactions_hiring_teams (hiring_team_id, billing_transaction_id)
                select hiring_team_id, charge_id from charges_hiring_teams;
        ~
    end

    def down
        drop_table :billing_transactions_hiring_teams
        drop_table :billing_transactions_users
        drop_table :billing_transactions_versions
        drop_table :billing_transactions
    end
end