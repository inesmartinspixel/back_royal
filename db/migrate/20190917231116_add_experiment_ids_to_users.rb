class AddExperimentIdsToUsers < ActiveRecord::Migration[6.0]
    def up
        add_column :users, :experiment_ids, :string, array: true, default: [], null: false
        add_column :users_versions, :experiment_ids, :string, array: true
        recreate_users_trigger_v3
    end

    def down
        remove_column :users, :experiment_ids, :string, array: true, default: [], null: false
        remove_column :users_versions, :experiment_ids, :string, array: true
        recreate_users_trigger_v3
    end
end
