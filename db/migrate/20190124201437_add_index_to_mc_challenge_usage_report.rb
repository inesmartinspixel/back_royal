class AddIndexToMcChallengeUsageReport < ActiveRecord::Migration[5.2]
    disable_ddl_transaction!

    def change
        add_index :multiple_choice_challenge_usage_reports, :last_answer_at, algorithm: :concurrently
    end
end
