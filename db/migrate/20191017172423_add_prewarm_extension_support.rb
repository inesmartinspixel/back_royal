class AddPrewarmExtensionSupport < ActiveRecord::Migration[5.1]

  def up
      execute "CREATE EXTENSION IF NOT EXISTS pg_prewarm"
  end

  def down
      execute "DROP EXTENSION pg_prewarm"
  end

end
