class AddUserIdIndexToVersionsTables < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        add_index :career_profiles_versions, :user_id, algorithm: :concurrently

        add_index :cohort_applications_versions, :user_id, algorithm: :concurrently
    end

    def down
        remove_index :career_profiles_versions, :user_id

        remove_index :cohort_applications_versions, :user_id
    end
end
