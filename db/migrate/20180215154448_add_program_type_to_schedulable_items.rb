class AddProgramTypeToSchedulableItems < ActiveRecord::Migration[5.1]
    def change
        if Rails.env.test?
            add_column :schedulable_items, :program_type, :text
        end
    end
end
