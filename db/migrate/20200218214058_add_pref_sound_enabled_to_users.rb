class AddPrefSoundEnabledToUsers < ActiveRecord::Migration[6.0]

    def up
        add_column :users, :pref_sound_enabled, :boolean, :null => false, default: true
        add_column :users_versions, :pref_sound_enabled, :boolean

        recreate_users_trigger_v4
    end

    def down
        remove_column :users, :pref_sound_enabled
        remove_column :users_versions, :pref_sound_enabled
        recreate_users_trigger_v4
    end

end
