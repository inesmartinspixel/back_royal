class TweaksForScholarshipAndEarlyDecision < ActiveRecord::Migration[5.1]
    def up
        add_column :career_profiles, :short_answer_scholarship, :string
        add_column :career_profiles_versions, :short_answer_scholarship, :string

        change_column_default :career_profiles, :consider_merit_based, nil

        add_column :career_profiles, :consider_early_decision, :boolean
        add_column :career_profiles_versions, :consider_early_decision, :boolean
        change_column_default :career_profiles, :consider_early_decision, false
    end

    def down
        remove_column :career_profiles, :short_answer_scholarship
        remove_column :career_profiles_versions, :short_answer_scholarship

        change_column_default :career_profiles, :consider_merit_based, true

        remove_column :career_profiles, :consider_early_decision
        remove_column :career_profiles_versions, :consider_early_decision
    end
end
