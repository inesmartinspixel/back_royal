class AddMissingReadIndices< ActiveRecord::Migration[5.1]

    disable_ddl_transaction!

    def change

        # missing content indices
        add_index :playlists_versions, [:entity_metadata_id, :image_id, :author_id], :name => 'playlist_versions_entity_image_author', algorithm: :concurrently
        add_index :entity_metadata, :image_id, algorithm: :concurrently
        add_index :lessons_versions, [:author_id, :archived], algorithm: :concurrently
        add_index :published_cohort_stream_locale_packs, :required, algorithm: :concurrently
        add_index :lesson_streams_versions, :locale, algorithm: :concurrently

        # additional indices
        add_index :lesson_streams_progress, :completed_at
        add_index :users, :can_edit_career_profile, algorithm: :concurrently
        add_index :hiring_relationships, :hiring_manager_status, algorithm: :concurrently
        add_index :institutions_users, [:institution_id, :user_id], algorithm: :concurrently
        add_index :education_experiences, :career_profile_id, algorithm: :concurrently

    end
end
