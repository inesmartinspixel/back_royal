class RemoveLegacyCohortPromotions < ActiveRecord::Migration[5.1]
    def up
        # We use admission rounds to determine the promoted mba and emba cohorts, but we still have
        # legacy cohort promotions lingering around (MBA7 and EMBA3). This matters now that we are using
        # the cohorts_controller#index endpoint for paid cert changes.
        CohortPromotion.joins(:cohort).where(cohorts: {program_type: ['mba', 'emba']}).delete_all
    end
end
