class AddExtraPeriodsToPublishedCohortPeriods < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20181008182254)
    end

    def down
        ViewHelpers.rollback(self, 20181008182254)
    end
end