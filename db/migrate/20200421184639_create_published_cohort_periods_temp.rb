class CreatePublishedCohortPeriodsTemp < ActiveRecord::Migration[6.0]
    def up
        create_table :published_cohort_periods_temp, id: false do |t|
            t.datetime :created_at
            t.uuid :cohort_id
            t.text :name
            t.integer :index
            t.text :title
            t.text :style
            t.integer :days
            t.integer :days_offset
            t.integer :additional_specialization_playlists_to_complete
            t.json :stream_entries, array: true
            t.json :exercises, array: true
            t.json :actions, array: true
            t.uuid :learner_project_ids, array: true
            t.datetime :cohort_start_date
            t.datetime :period_start
            t.datetime :period_end
            t.uuid :required_stream_pack_ids, array: true
            t.uuid :optional_stream_pack_ids, array: true
            t.uuid :required_lesson_pack_ids, array: true
            t.uuid :cumulative_required_stream_pack_ids, array: true
            t.uuid :cumulative_required_lesson_pack_ids, array: true
            t.integer :cumulative_required_stream_pack_ids_count
            t.integer :cumulative_required_lesson_pack_ids_count
            t.float :expected_specializations_complete
        end

        add_index :published_cohort_periods_temp, [:cohort_id, :index], :unique => true, :name => :coh_id_index_on_publ_coh_periods_temp

        # PublishedCohortPeriod.rebuild
    end

    def down
        drop_table :published_cohort_periods_temp
    end
end
