class AddNewColumnsToCareerProfiles < ActiveRecord::Migration[5.2]

    def up
        add_column :career_profiles, :sufficient_english_work_experience, :boolean
        add_column :career_profiles, :english_work_experience_description, :text

        add_column :career_profiles_versions, :sufficient_english_work_experience, :boolean
        add_column :career_profiles_versions, :english_work_experience_description, :text

        recreate_trigger_v2 "career_profiles"
    end

    def down
        remove_column :career_profiles, :sufficient_english_work_experience
        remove_column :career_profiles, :english_work_experience_description

        remove_column :career_profiles_versions, :sufficient_english_work_experience
        remove_column :career_profiles_versions, :english_work_experience_description

        recreate_trigger_v2 "career_profiles"
    end

end
