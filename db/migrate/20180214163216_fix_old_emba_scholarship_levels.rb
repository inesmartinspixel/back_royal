class FixOldEmbaScholarshipLevels < ActiveRecord::Migration[5.1]

    def up
        migrate_relevant_data(true)
    end

    def down
        migrate_relevant_data(false)
    end


    def migrate_relevant_data(up)

        if up

            # old / proper scholarship levels
            base_level_sql = '\'{"name": "No Scholarship", "coupons": {"emba_800": {"id": "none", "amount_off": 0, "percent_off": 0}, "emba_bi_4800": {"id": "discount_600", "amount_off": 60000, "percent_off": null} } }\'::json'
            level_1_sql    = '\'{"name": "Level 1", "coupons": {"emba_800": {"id": "discount_150", "amount_off": 15000, "percent_off": null}, "emba_bi_4800": {"id": "discount_1400", "amount_off": 140000, "percent_off": null} } }\'::json'
            level_2_sql    = '\'{"name": "Level 2", "coupons": {"emba_800": {"id": "discount_250", "amount_off": 25000, "percent_off": null}, "emba_bi_4800": {"id": "discount_2000", "amount_off": 200000, "percent_off": null} } }\'::json'
            level_3_sql    = '\'{"name": "Level 3", "coupons": {"emba_800": {"id": "discount_350", "amount_off": 35000, "percent_off": null}, "emba_bi_4800": {"id": "discount_2500", "amount_off": 250000, "percent_off": null} } }\'::json'
            level_4_sql    = '\'{"name": "Level 4", "coupons": {"emba_800": {"id": "discount_450", "amount_off": 45000, "percent_off": null}, "emba_bi_4800": {"id": "discount_3000", "amount_off": 300000, "percent_off": null} } }\'::json'
            level_5_sql    = '\'{"name": "Level 5", "coupons": {"emba_800": {"id": "discount_550", "amount_off": 55000, "percent_off": null}, "emba_bi_4800": {"id": "discount_3550", "amount_off": 355000, "percent_off": null} } }\'::json'
            full_level_sql = '\'{"name": "Full Scholarship", "coupons": {"emba_800": {"id": "discount_100_percent", "amount_off": null, "percent_off": 100}, "emba_bi_4800": {"id": "discount_100_percent", "amount_off": null, "percent_off": 100} } }\'::json'

        else

            # new / incorrect scholarship levels
            base_level_sql = '\'{"name": "No Scholarship", "coupons": {"emba_800": {"id": "none", "amount_off": 0, "percent_off": 0}, "emba_bi_4800": {"id": "discount_600", "amount_off": 60000, "percent_off": null}, "emba_once_9600": {"id": "discount_1600", "amount_off": 160000, "percent_off": null} } }\'::json'
            level_1_sql    = '\'{"name": "Level 1", "coupons": {"emba_800": {"id": "discount_150", "amount_off": 15000, "percent_off": null}, "emba_bi_4800": {"id": "discount_1400", "amount_off": 140000, "percent_off": null}, "emba_once_9600": {"id": "discount_3100", "amount_off": 310000, "percent_off": null} } }\'::json'
            level_2_sql    = '\'{"name": "Level 2", "coupons": {"emba_800": {"id": "discount_250", "amount_off": 25000, "percent_off": null}, "emba_bi_4800": {"id": "discount_1900", "amount_off": 190000, "percent_off": null}, "emba_once_9600": {"id": "discount_4100", "amount_off": 410000, "percent_off": null} } }\'::json'
            level_3_sql    = '\'{"name": "Level 3", "coupons": {"emba_800": {"id": "discount_350", "amount_off": 35000, "percent_off": null}, "emba_bi_4800": {"id": "discount_2400", "amount_off": 240000, "percent_off": null}, "emba_once_9600": {"id": "discount_5100", "amount_off": 510000, "percent_off": null} } }\'::json'
            level_4_sql    = '\'{"name": "Level 4", "coupons": {"emba_800": {"id": "discount_450", "amount_off": 45000, "percent_off": null}, "emba_bi_4800": {"id": "discount_2950", "amount_off": 295000, "percent_off": null}, "emba_once_9600": {"id": "discount_6100", "amount_off": 610000, "percent_off": null} } }\'::json'
            level_5_sql    = '\'{"name": "Level 5", "coupons": {"emba_800": {"id": "discount_550", "amount_off": 55000, "percent_off": null}, "emba_bi_4800": {"id": "discount_3500", "amount_off": 350000, "percent_off": null}, "emba_once_9600": {"id": "discount_7200", "amount_off": 720000, "percent_off": null} } }\'::json'
            full_level_sql = '\'{"name": "Full Scholarship", "coupons": {"emba_800": {"id": "discount_100_percent", "amount_off": null, "percent_off": 100}, "emba_bi_4800": {"id": "discount_100_percent", "amount_off": null, "percent_off": 100},  "emba_once_9600": {"id": "discount_100_percent", "amount_off": null, "percent_off": 100} } }\'::json'

        end


        # back-populate scholarship_level on cohort_applications here, using all known coupon codes
        execute "UPDATE cohort_applications SET scholarship_level =
                    CASE
                        WHEN scholarship_level IS NULL THEN #{base_level_sql}
                        WHEN scholarship_level::text LIKE '%No Scholarship%' THEN #{base_level_sql}
                        WHEN scholarship_level::text LIKE '%Level 1%' THEN #{level_1_sql}
                        WHEN scholarship_level::text LIKE '%Level 2%' THEN #{level_2_sql}
                        WHEN scholarship_level::text LIKE '%Level 3%' THEN #{level_3_sql}
                        WHEN scholarship_level::text LIKE '%Level 4%' THEN #{level_4_sql}
                        WHEN scholarship_level::text LIKE '%Level 5%' THEN #{level_5_sql}
                        WHEN scholarship_level::text LIKE '%Full Scholarship%' THEN #{full_level_sql}
                        ELSE scholarship_level
                    END
                WHERE cohort_id IN (SELECT id FROM cohorts WHERE stripe_plans IS NOT NULL AND name NOT IN ('EMBA8', 'EMBA9'))"

    end

end
