class AddGraduationDateToCohorts < ActiveRecord::Migration[5.1]

    def change

        add_column :cohorts, :graduation_date_days_offset, :integer
        add_column :cohorts_versions, :graduation_date_days_offset, :integer
        add_column :curriculum_templates, :graduation_date_days_offset, :integer
        add_column :curriculum_templates_versions, :graduation_date_days_offset, :integer

    end
end
