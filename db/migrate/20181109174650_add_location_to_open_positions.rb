class AddLocationToOpenPositions < ActiveRecord::Migration[5.2]
    def up
        execute %Q{
            create extension if not exists postgis;
            ALTER TABLE open_positions ADD COLUMN location geography(POINT,4326);
            ALTER TABLE open_positions_versions ADD COLUMN location geography(POINT,4326);
        }
    end

    def down
        execute %Q~
            ALTER TABLE open_positions_versions DROP COLUMN location;
            ALTER TABLE open_positions DROP COLUMN location;
        ~
    end
end
