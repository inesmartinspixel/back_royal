class BackfillRegisteredCohortApplications < ActiveRecord::Migration[5.1]

  disable_ddl_transaction!

  def up
    change_column_default :cohort_applications, :registered, false

    CohortApplication.where(registered: nil).in_batches.update_all registered: false

    change_column_null :cohort_applications, :registered, false
  end

  def down
    change_column_default :cohort_applications, :registered, nil
    change_column_null :cohort_applications, :registered, true
  end

end
