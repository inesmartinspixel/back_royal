class CohortPredictionRecords < ActiveRecord::Migration[5.1]
    def change
        create_table :cohort_period_graduation_rate_predictions, id: :uuid do |t|
            t.timestamps                        null: false

            t.float :chance_of_success, null: false
            t.timestamp :cohort_end, null: false
            t.uuid :cohort_id, null: false
            t.text :cohort_name, null: false
            t.integer :cohorts_with_similar_periods_count
            t.text :current_position
            t.float :error
            t.float :final_result
            t.integer :index
            t.text :normalized_index, null: false
            t.integer :num_active, null: false
            t.integer :num_enrolled, null: false
            t.integer :num_failed, null: false
            t.integer :num_graduated, null: false
            t.timestamp :period_end
            t.json :similar_periods
            t.integer :similar_periods_count
        end

        add_index :cohort_period_graduation_rate_predictions, [:cohort_id, :index], :unique => true, :name => :unique_on_cp_graduation_rate_predictions

        create_table :user_chance_of_graduating_records, id: :uuid do |t|
            t.timestamps                        null: false
            t.uuid :user_id, null: false
            t.text :program_type, null: false
            t.float :chance_of_graduating, null: false
        end
        add_index :user_chance_of_graduating_records, [:user_id, :program_type], :unique => true, :name => :unique_on_user_chance_of_graduating_records


    end
end
