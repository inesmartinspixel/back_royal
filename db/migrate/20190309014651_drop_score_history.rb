class DropScoreHistory < ActiveRecord::Migration[5.2]
    def up
        remove_column :project_progress, :score_history
        remove_column :project_progress_versions, :score_history
        recreate_trigger_v2 "project_progress"
    end

    def down
        add_column :project_progress, :score_history, :json, array: true, default: []
        add_column :project_progress_versions, :score_history, :json, array: true, default: []
        recreate_trigger_v2 "project_progress"
    end
end
