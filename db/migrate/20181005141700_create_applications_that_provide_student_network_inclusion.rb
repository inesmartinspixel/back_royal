class CreateApplicationsThatProvideStudentNetworkInclusion < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20181005141700)
    end

    def down
        ViewHelpers.rollback(self, 20181005141700)
    end
end
