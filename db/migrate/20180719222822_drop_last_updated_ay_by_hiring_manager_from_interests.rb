class DropLastUpdatedAyByHiringManagerFromInterests < ActiveRecord::Migration[5.1]

    def change
        remove_column :candidate_position_interests, :last_updated_at_by_hiring_manager, :datetime
        remove_column :candidate_position_interests_versions, :last_updated_at_by_hiring_manager, :datetime
    end

end
