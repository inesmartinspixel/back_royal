class MeetsGradReqUpdate < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20180807190214)
    end

    def down
        ViewHelpers.rollback(self, 20180807190214)
    end
end
