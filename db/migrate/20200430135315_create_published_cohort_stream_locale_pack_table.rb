class CreatePublishedCohortStreamLocalePackTable < ActiveRecord::Migration[6.0]
    def up
        create_table :published_cohort_stream_locale_packs_temp, id: false do |t|
            t.datetime :created_at
            t.uuid :cohort_id
            t.text :cohort_name
            t.boolean :foundations
            t.boolean :required
            t.boolean :specialization
            t.boolean :elective
            t.boolean :exam
            t.text :stream_title
            t.uuid :stream_locale_pack_id
            t.text :stream_locales
            t.integer :playlist_count
        end

        add_index :published_cohort_stream_locale_packs_temp, [:cohort_id, :stream_locale_pack_id], :name => :cohort_and_str_lp_on_cohort_str_lps_tbl, :unique => true
        add_index :published_cohort_stream_locale_packs_temp, :stream_locale_pack_id, :name => :stream_lp_on_cohort_stream_packs_tbl


        # PublishedCohortStreamLocalePack.rebuild
    end

    def down
        drop_table :published_cohort_stream_locale_packs_temp
    end
end
