class AddAvatarIdToUsers < ActiveRecord::Migration[5.1]

    def up
        add_column :users, :avatar_id, :uuid
        add_column :users_versions, :avatar_id, :uuid
        recreate_users_trigger_v1

        add_column :s3_assets, :source_url, :text
    end

    def down
        remove_column :s3_assets, :source_url
        remove_column :users, :avatar_id
        remove_column :users_versions, :avatar_id
        recreate_users_trigger_v1
    end

end


