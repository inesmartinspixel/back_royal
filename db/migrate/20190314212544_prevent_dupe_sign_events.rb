class PreventDupeSignEvents < ActiveRecord::Migration[5.0]

    disable_ddl_transaction!

    def up

        # Remove duplicate enrollment agreements that will trip up the new constraint
        User.joins(:signable_documents).each do |user|
            enrollment_agreement_ids = []
            user.cohort_applications.each do |ca|
                next unless ca.enrollment_agreement
                enrollment_agreement_ids << ca.enrollment_agreement.id
            end
            user.signable_documents.where.not(id: enrollment_agreement_ids).destroy_all
        end

        execute "CREATE UNIQUE INDEX IF NOT EXISTS unique_signable_documents_user_id_program_type
                ON signable_documents( user_id, (metadata->>'program_type') )
                WHERE document_type = '#{SignableDocument::TYPE_ENROLLMENT_AGREEMENT}';"
    end

    def down
        remove_index :signable_documents, :name => "unique_signable_documents_user_id_program_type"
    end
end