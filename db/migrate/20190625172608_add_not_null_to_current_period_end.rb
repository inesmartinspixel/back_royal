class AddNotNullToCurrentPeriodEnd < ActiveRecord::Migration[5.2]
    def change
        change_column_null :subscriptions, :current_period_end, false
    end
end
