class MoreCareerProfileKeywordColumns < ActiveRecord::Migration[5.1]
    def up

        add_column :career_profile_fulltext, :student_network_full_keyword_vector, :tsvector
        add_column :career_profile_fulltext, :student_network_full_keyword_text, :text
        add_column :career_profile_fulltext, :student_network_anonymous_keyword_vector, :tsvector
        add_column :career_profile_fulltext, :student_network_anonymous_keyword_text, :text

        add_index :career_profile_fulltext, :student_network_full_keyword_vector, using: :gin, :name => 'career_profile_ft_sn_full_keyword_vec'
        execute "CREATE INDEX career_profile_fulltext_sn_full_keyword_text ON career_profile_fulltext USING GIN (student_network_full_keyword_text gin_trgm_ops)"
        add_index :career_profile_fulltext, :student_network_anonymous_keyword_vector, using: :gin, :name => 'career_profile_ft_sn_anon_keyword_vec'
        execute "CREATE INDEX career_profile_fulltext_sn_anon_keyword_text ON career_profile_fulltext USING GIN (student_network_anonymous_keyword_text gin_trgm_ops)"
    end

    def down
        remove_column :career_profile_fulltext, :student_network_full_keyword_vector
        remove_column :career_profile_fulltext, :student_network_full_keyword_text
        remove_column :career_profile_fulltext, :student_network_anonymous_keyword_vector
        remove_column :career_profile_fulltext, :student_network_anonymous_keyword_text
    end
end
