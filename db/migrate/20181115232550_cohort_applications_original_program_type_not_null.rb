class CohortApplicationsOriginalProgramTypeNotNull < ActiveRecord::Migration[5.2]

  disable_ddl_transaction!

  def up
      change_column_null :cohort_applications, :original_program_type, false
  end

  def down
      change_column_null :cohort_applications, :original_program_type, true
  end

end
