class AddRsvpStatusToStudentNetworkEvents < ActiveRecord::Migration[6.0]

    def up
        add_column :student_network_events, :rsvp_status, :text, :null => false, default: 'not_required'
        add_column :student_network_events_versions, :rsvp_status, :text

        StudentNetworkEvent.where(rsvp_required: true).update_all(rsvp_status: 'required')

        recreate_trigger_v2(:student_network_events)
    end

    def down
        remove_column :student_network_events, :rsvp_status
        remove_column :student_network_events_versions, :rsvp_status
        recreate_trigger_v2(:student_network_events)
    end

end
