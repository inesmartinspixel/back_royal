class AddEmploymentTypeToWorkExperiences < ActiveRecord::Migration[5.1]
    def up
        add_column :work_experiences, :employment_type, :text
        add_column :work_experiences_versions, :employment_type, :text

        change_column_default :work_experiences, :employment_type, 'full_time'
    end

    def down
        remove_column :work_experiences, :employment_type
        remove_column :work_experiences_versions, :employment_type
    end
end
