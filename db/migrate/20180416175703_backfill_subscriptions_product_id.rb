class BackfillSubscriptionsProductId < ActiveRecord::Migration[5.1]

  disable_ddl_transaction!

  def up
    # Backfill (idempotent)
    Subscription.in_batches.update_all stripe_product_id: 'emba'

    # Set nullable (idempotent)
    change_column_null :subscriptions, :stripe_product_id, false
  end

  def down
    change_column_null :subscriptions, :stripe_product_id, true
  end

end
