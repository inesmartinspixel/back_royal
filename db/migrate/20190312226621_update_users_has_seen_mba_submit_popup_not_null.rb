class UpdateUsersHasSeenMbaSubmitPopupNotNull < ActiveRecord::Migration[5.2]

    def up
        change_column_null :users, :has_seen_mba_submit_popup, false
    end

    def down
        change_column_null :users, :has_seen_mba_submit_popup, true
    end

end
