class AddUniqueIndexToContentPublishers < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def change
        add_index :content_publishers, :lesson_version_id, :unique => true, algorithm: :concurrently
        add_index :content_publishers, :lesson_stream_version_id, :unique => true, algorithm: :concurrently
        add_index :content_publishers, :playlist_version_id, :unique => true, algorithm: :concurrently
        add_index :content_publishers, :cohort_version_id, :unique => true, algorithm: :concurrently
    end
end
