class AddSlackUrlToCohorts < ActiveRecord::Migration[5.1]
    def change
        add_column :cohorts, :slack_url, :string
        add_column :cohorts_versions, :slack_url, :string
    end
end
