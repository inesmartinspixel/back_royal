class TweakYearsOfExperienceViewHelperLogic < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20171013223600)
    end

    def down
        ViewHelpers.rollback(self, 20171013223600)
    end
end
