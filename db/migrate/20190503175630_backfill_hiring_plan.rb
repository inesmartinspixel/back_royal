class BackfillHiringPlan < ActiveRecord::Migration[5.2]
    disable_ddl_transaction!

     def up

         ActiveRecord::Base.connection.execute(%Q~
            UPDATE hiring_teams
            SET hiring_plan='legacy'
         ~)

     end
end
