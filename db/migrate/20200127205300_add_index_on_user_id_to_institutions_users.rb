class AddIndexOnUserIdToInstitutionsUsers < ActiveRecord::Migration[6.0]
    disable_ddl_transaction!

    def up
        ActiveRecord::Base.connection.execute('CREATE INDEX CONCURRENTLY IF NOT EXISTS index_institutions_users_on_user_id ON institutions_users (user_id)')
    end

    def down
        ActiveRecord::Base.connection.execute('DROP INDEX index_institutions_users_on_user_id')
    end
end
