class ModifyPlaylistProgressViewToRestrictLocale < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20180629200606)
    end

    def down
        ViewHelpers.rollback(self, 20180629200606)
    end
end
