class AddWaivedToProjectProgress < ActiveRecord::Migration[5.2]
    def change
        add_column :project_progress, :waived, :boolean, :default => false
        add_column :project_progress, :marked_as_passed, :boolean, :default => false
    end
end
