class RemoveCurriculumTemplatesProgramGuideUrl < ActiveRecord::Migration[5.2]
    def up
        remove_column :curriculum_templates, :program_guide_url
        remove_column :curriculum_templates_versions, :program_guide_url
        recreate_trigger_v2 "curriculum_templates"
    end

    def down
        add_column :curriculum_templates, :program_guide_url, :text
        add_column :curriculum_templates_versions, :program_guide_url, :text
        recreate_trigger_v2 "curriculum_templates"
    end
end
