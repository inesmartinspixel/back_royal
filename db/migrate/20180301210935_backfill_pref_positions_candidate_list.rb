class BackfillPrefPositionsCandidateList < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        User.in_batches.update_all(pref_positions_candidate_list: true)
        change_column_null :users, :pref_positions_candidate_list, false
    end

    def down
        change_column_null :users, :pref_positions_candidate_list, true
    end
end
