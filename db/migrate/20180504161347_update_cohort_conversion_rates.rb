class UpdateCohortConversionRates < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20180504161347)
    end

    def down
        ViewHelpers.rollback(self, 20180504161347)
    end
end
