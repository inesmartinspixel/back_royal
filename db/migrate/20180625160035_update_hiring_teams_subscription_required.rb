class UpdateHiringTeamsSubscriptionRequired < ActiveRecord::Migration[5.1]

    def up

        # These teams will remain "unlocked"
        # ["Emerson Collective", "Earnest", "Mosaic", "ExecOnline", "PatientPing", "[TEST] Pedago internal", "Stedi", "FIXD Automotive", "Thanx", "Industrious Office", "CareDox", "Makespace Product"]
        excluded_ids = %w(
            '103754bd-5c76-4130-a7d6-05ea8b0233c0'
            '130f87ee-4f8d-4e7a-838c-2137135ac09a'
            '19ac5f41-fa04-40cc-8639-f912e3cfe24e'
            '19fc3acc-39ed-436e-8e6d-39fd55bed64e'
            '3ec452e1-4802-4718-bbb7-c85aab817c8c'
            '4a956f21-4d9f-4ac4-8046-d6f0ebf09820'
            'b1c1749f-0533-4741-b800-414556ae8fe3'
            'b4eef732-6674-4e36-9fac-98514668b750'
            'b7f379b4-f151-4828-bef2-7723f0eaa243'
            'd45503ae-03ca-4b3e-a9ce-16a40a5616d8'
            'f02d09b8-4710-45c9-acab-554fed9f9064'
            'f64b80e7-8b0a-4f10-9d0e-927c37971442'
            'ff444457-60b4-40c1-8e43-3364a160b8f3'
        )

        execute "UPDATE hiring_teams SET subscription_required = true WHERE id NOT IN (#{excluded_ids.join(',')})"

    end

    def down
        execute "UPDATE hiring_teams SET subscription_required = false"
    end

end
