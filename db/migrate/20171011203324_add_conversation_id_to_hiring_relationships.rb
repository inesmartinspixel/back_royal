class AddConversationIdToHiringRelationships < ActiveRecord::Migration[5.1]
    def change

        add_column :hiring_relationships, :conversation_id, :int
        add_column :hiring_relationships_versions, :conversation_id, :int

        add_foreign_key :hiring_relationships, :mailboxer_conversations, column: :conversation_id
    end
end
