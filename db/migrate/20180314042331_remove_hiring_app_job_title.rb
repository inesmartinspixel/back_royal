class RemoveHiringAppJobTitle < ActiveRecord::Migration[5.1]
    def change
        remove_column :hiring_applications, :job_title, :text
        remove_column :hiring_applications_versions, :job_title, :text
    end
end
