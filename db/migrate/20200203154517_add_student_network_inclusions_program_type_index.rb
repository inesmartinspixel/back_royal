class AddStudentNetworkInclusionsProgramTypeIndex < ActiveRecord::Migration[6.0]
    disable_ddl_transaction!

    def change
        add_index(:student_network_inclusions, [:program_type], algorithm: :concurrently)
    end
end