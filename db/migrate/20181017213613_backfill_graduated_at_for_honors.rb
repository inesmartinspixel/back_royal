class BackfillGraduatedAtForHonors < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!
    
    # Borrowed and modified from db/migrate_old/20170615160854_add_cohort_application_completed_at_graduated_at.rb
    def up
        ActiveRecord::Base.connection.execute %Q~
            UPDATE cohort_applications
            SET graduated_at = (SELECT version_created_at
                                FROM cohort_applications_versions
                                WHERE graduation_status = 'honors' AND user_id = cohort_applications.user_id
                                ORDER BY version_created_at ASC
                                LIMIT 1)
            WHERE graduation_status = 'honors';
        ~
    end

    def down
        # noop
    end
end
