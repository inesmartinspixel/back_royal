class AddMoreAirtableDataFieldsToCohortApplications < ActiveRecord::Migration[6.0]
    def up
        add_column :cohort_applications, :rubric_interview_date, :datetime
        add_column :cohort_applications_versions, :rubric_interview_date, :datetime

        add_column :cohort_applications, :rubric_motivation_score, :text
        add_column :cohort_applications_versions, :rubric_motivation_score, :text

        add_column :cohort_applications, :rubric_contribution_score, :text
        add_column :cohort_applications_versions, :rubric_contribution_score, :text

        add_column :cohort_applications, :rubric_insightfulness_score, :text
        add_column :cohort_applications_versions, :rubric_insightfulness_score, :text

        recreate_trigger_v2(:cohort_applications)
    end

    def down
        remove_column :cohort_applications, :rubric_interview_date, :datetime
        remove_column :cohort_applications_versions, :rubric_interview_date, :datetime

        remove_column :cohort_applications, :rubric_motivation_score, :text
        remove_column :cohort_applications_versions, :rubric_motivation_score, :text

        remove_column :cohort_applications, :rubric_contribution_score, :text
        remove_column :cohort_applications_versions, :rubric_contribution_score, :text

        remove_column :cohort_applications, :rubric_insightfulness_score, :text
        remove_column :cohort_applications_versions, :rubric_insightfulness_score, :text

        recreate_trigger_v2(:cohort_applications)
    end
end
