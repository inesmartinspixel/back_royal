class FixContentItemUpdatedAtIndices < ActiveRecord::Migration[5.2]
    disable_ddl_transaction!

    def up
        # see db/migrate_old/20150323182218_add_missing_lesson_versions.rb
        # In that migration, we added unique indexes on id/updated_at.  It appears
        # that back then we used to set up the versions trigger to not put any information
        # into versions with a 'D' operation.  The recent change that caused all the triggers
        # to be recreated, however, meant that we are now copying all of the columns into the
        # 'D' version.  This leads to a duplicate key error, since the last 'U' version has the
        # same updated_at as the 'D' version
        #
        # The fix here is just to remove the unique index and structure these tables like the
        # rest of our versions table, with a non-unique index on id, updated_at.  The extra security
        # we were trying to get with the constraint seems unnecessary at this point.
        execute %Q~
            ALTER TABLE lessons DROP CONSTRAINT lessons_id_fkey;
            ALTER TABLE lesson_streams DROP CONSTRAINT lesson_streams_id_fkey;
        ~

        remove_index :lessons_versions, :name => 'index_lessons_versions_on_id_and_updated_at'
        remove_index :lesson_streams_versions, :name => 'index_lesson_streams_versions_on_id_and_updated_at'

        add_index :lessons_versions, [:id, :updated_at], :algorithm => :concurrently, :name => 'index_lessons_versions_on_id_and_updated_at'
        add_index :lesson_streams_versions, [:id, :updated_at], :algorithm => :concurrently, :name => 'index_lesson_streams_versions_on_id_and_updated_at'
    end

    def down
        remove_index :lessons_versions, :name => 'index_lessons_versions_on_id_and_updated_at'
        add_index :lessons_versions, [:id, :updated_at], :unique => true

        remove_index :lesson_streams_versions, :name => 'index_lesson_streams_versions_on_id_and_updated_at'
        add_index :lesson_streams_versions, [:id, :updated_at], :unique => true

        execute("ALTER TABLE lessons
            ADD FOREIGN KEY (id, updated_at)
            REFERENCES lessons_versions (id, updated_at)
            INITIALLY DEFERRED");

        execute("ALTER TABLE lesson_streams
            ADD FOREIGN KEY (id, updated_at)
            REFERENCES lesson_streams_versions (id, updated_at)
            INITIALLY DEFERRED");
    end
end
