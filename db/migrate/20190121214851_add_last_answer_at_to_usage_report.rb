class AddLastAnswerAtToUsageReport < ActiveRecord::Migration[5.2]
    def change
        add_column :multiple_choice_challenge_usage_reports, :last_answer_at, :timestamp

        reversible do |dir|
            dir.up do
                # We artificially mark one last_answer_at to now so that we will start
                # counting from now.  Since the old servers will keep counting new answers
                # for the next few minutes of the rollout, we will end up double counting a
                # few.  Not a big deal though.  We don't need precision with this stuff.
                execute('
                    update multiple_choice_challenge_usage_reports set last_answer_at=now()
                    where
                    id = (select id from multiple_choice_challenge_usage_reports limit 1)
                ')
            end
        end
    end
end
