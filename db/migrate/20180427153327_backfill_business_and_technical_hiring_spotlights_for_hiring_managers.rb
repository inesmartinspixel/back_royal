class BackfillBusinessAndTechnicalHiringSpotlightsForHiringManagers < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        # Backfill (idempotent)
        User.where(:notify_hiring_spotlights => true).in_batches.update_all(notify_hiring_spotlights_business: true, notify_hiring_spotlights_technical: true)
        User.where(:notify_hiring_spotlights => false).in_batches.update_all(notify_hiring_spotlights_business: false, notify_hiring_spotlights_technical: false)

        # Set nullable (idempotent)
        change_column_null :users, :notify_hiring_spotlights_business, false
        change_column_null :users, :notify_hiring_spotlights_technical, false
    end

    def down
        change_column_null :users, :notify_hiring_spotlights_business, true
        change_column_null :users, :notify_hiring_spotlights_technical, true
    end
end
