class BackfillLockedDueToPastDuePayment < ActiveRecord::Migration[5.1]
    # we are disabling transactions so that we don't update every row in the
    # table in a single transaction.  In order for this to be ok, we need
    # to make sure everything in this migration is idempotent
    disable_ddl_transaction!

    def up
        CohortApplication.in_batches.update_all locked_due_to_past_due_payment: false
        change_column_null :cohort_applications, :locked_due_to_past_due_payment, false
    end

    def down
        change_column_null :cohort_applications, :locked_due_to_past_due_payment, true
    end
end
