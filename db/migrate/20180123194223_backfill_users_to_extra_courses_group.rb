class BackfillUsersToExtraCoursesGroup < ActiveRecord::Migration[5.1]
    def up
        # backfill users to be in the EXTRA COURSES access group if they were
        # rejected from either the MBA or EMBA at any point
        execute("
            -- Create EXTRA COURSES access group if it doesn't exist
            INSERT INTO access_groups (created_at, updated_at, name) values (now(), now(), 'EXTRA COURSES') ON CONFLICT DO NOTHING;

            -- Backfill users into the EXTRA COURSES access group
            INSERT INTO access_groups_users (access_group_id, user_id) (
                SELECT
                    (SELECT id FROM access_groups WHERE access_groups.name = 'EXTRA COURSES' LIMIT 1) as ag_id,
                    users.id
                FROM users
                    JOIN cohort_applications_versions ON cohort_applications_versions.user_id = users.id
                WHERE cohort_applications_versions.status = 'rejected'
                    AND cohort_applications_versions.cohort_id IN (
                    SELECT
                        cohorts.id
                    FROM cohorts
                    WHERE cohorts.program_type IN ('mba', 'emba')
                )
                GROUP BY ag_id, users.id
            ) ON CONFLICT DO NOTHING
        ")
    end

    def down
        # noop
    end
end
