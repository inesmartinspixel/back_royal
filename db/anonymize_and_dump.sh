#!/bin/bash

############################################################################################################
# BEGIN CONFIG BLOCK
############################################################################################################

# temporary CDB instance details
RDS_INSTANCE_ID=smartly-cdb
DB_HOST="${RDS_INSTANCE_ID}.cgmrwq5afwke.us-east-1.rds.amazonaws.com"
DB_NAME=back_royal
DB_SRC_ENV_TYPE=production
DB_INSTANCE_TYPE=db.m4.large

# source instance credentials
DB_USER_READONLY=readonly
DB_USER_READONLY_PASSWORD_NEW=${RDS_READONLY_PASSWORD_NEW}
DB_USER_READONLY_PASSWORD_ORIG=${RDS_READONLY_PASSWORD_ORIG}

# target instance credentials
DB_USER_WRITE=ebroot
DB_USER_WRITE_PASSWORD_NEW=${RDS_WRITE_PASSWORD_NEW}
DB_USER_WRITE_PASSWORD_ORIG=${RDS_WRITE_PASSWORD_ORIG}

# profile names
TOOLS_PROFILE_NAME=tools
SOURCE_PROFILE_NAME=production
TARGET_PROFILE_NAME=development

# account ids
STAGING_ACCOUNT_ID=933376372560
TOOLS_ACCOUNT_ID=751953004425

# snapshot encryption
KMS_TRANSIT_KEY_ARN="arn:aws:kms:us-east-1:${TOOLS_ACCOUNT_ID}:key/1ac17b76-5fab-432f-abf1-ffd28715cc2f"
TRANSIT_SNAPSHOT_ID=smartly-cdb-transit

# target s3 bucket
S3_DUMP_BUCKET=smartly-anonymized-developer-dumps

# anonymization rules
PGANTOMIZER_CONFIG=back_royal_pgantomizer.yml

############################################################################################################


# TESTING NOTES
#
# EC2 ENV: smartly-sqlpad (t2.micro)
# db.m4.large + t2.micro = 126m
# db.m4.xlarge + t2.micro = 123m
# db.m3.2xlarge + t2.micro = 120m
#
# EC2 ENV: smartly-sqlpad-beefier (m4.xlarge) - gp2 ssd - high connectivity - ebs
# db.m3.2xlarge + m4.xlarge = 94m
# db.m4.large + m4.xlarge = 95m # Does RDS mem / etc really impact?
#
#
# Definitely seeing AWS Network availability ("Low - Med" vs "High") affect dump performance, but not
# a very big concern if this is run infrequently. I was a bit surprised not to see DB instance class
# dramatically affect performance. This led me to believe that our General Purpose SSD (gp2) burstable
# IOPS (100 / 3000) were being overrun, but I didn't see this on the EBS volume, although it might
# warrant a bit more investigation. I did see performance improvements during the UPDATE phase with
# the use of the `bulk-import-postgres12` which tuned WAL write threhsholds and memory allocations.
#
# It'd also be interesting to test a c5d.large which is on a (Enhanced Networking + NVMe SSD).

# CD to script directory
cd "$(dirname "$0")"


# checks return value from running and exits if non-zero, loggint to Sentry
# see also: https://blog.sentry.io/2017/11/28/sentry-bash
eval "$(sentry-cli bash-hook)"


# begin timing
start_time="$(date +%s)"


# existing git repo clone of https://github.com/booleanbetrayal/pgantomizer/commits/misc-customizations
cd pgantomizer


# get latest version of pgantomizer's customizations, accounting for rebases
echo "Grabbing latest version of pgantomizer ..."
git reset --hard; git checkout master; git branch -D misc-customizations; git pull; git checkout misc-customizations;


# re-install pgantomizer
echo "Reinstalling pgantomizer ..."
cat files.txt | xargs sudo rm -f; sudo rm -rf .egg build dist; sudo python3 setup.py install --record files.txt


# Back home
cd ..


# terminate any lingering instance pro-actively
EXISTINGINSTANCE=$(aws rds describe-db-instances \
    --profile ${TOOLS_PROFILE_NAME} \
    --query 'DBInstances[*].[DBInstanceIdentifier]' \
    --filters Name=db-instance-id,Values=${RDS_INSTANCE_ID} \
    --output text \
    )
if [ -z $EXISTINGINSTANCE ]
then
    echo "No lingering ${RDS_INSTANCE_ID} instance found."
else
    echo "Lingering ${RDS_INSTANCE_ID} found. Terminating ..."
    aws rds delete-db-instance --profile ${TOOLS_PROFILE_NAME} --db-instance-identifier ${RDS_INSTANCE_ID} --skip-final-snapshot
    aws rds wait db-instance-deleted --profile ${TOOLS_PROFILE_NAME} --db-instance-identifier ${RDS_INSTANCE_ID}
fi


# find any lingering transit snapshot
echo "Deleting any lingering ${TRANSIT_SNAPSHOT_ID} snapshots ..."
aws rds delete-db-snapshot --profile ${SOURCE_PROFILE_NAME} --db-snapshot-identifier ${TRANSIT_SNAPSHOT_ID} 2> /dev/null || true
aws rds delete-db-snapshot --profile ${TOOLS_PROFILE_NAME} --db-snapshot-identifier ${TRANSIT_SNAPSHOT_ID} 2> /dev/null || true


# find the latest production snapshot for manipulation
SOURCE_DB_IDENTIFIER="smartly-${DB_SRC_ENV_TYPE}"
echo "Looking for latest automated ${SOURCE_DB_IDENTIFIER} snapshot ..."
SNAPSHOT_PROVIDER="$(aws rds describe-db-snapshots \
    --profile ${SOURCE_PROFILE_NAME} \
    --db-instance-identifier=${SOURCE_DB_IDENTIFIER} \
    --snapshot-type automated \
    --query="reverse(sort_by(DBSnapshots, &SnapshotCreateTime))[0]|DBSnapshotArn" \
    --output text)"


# copy the existing, production snapshot to a new snapshot, so that it can be shared with the tools account
echo "Copying snapshot $SNAPSHOT_PROVIDER to $TRANSIT_SNAPSHOT_ID"
aws rds copy-db-snapshot \
    --profile ${SOURCE_PROFILE_NAME} \
    --source-region=us-east-1 \
    --source-db-snapshot-identifier ${SNAPSHOT_PROVIDER} \
    --target-db-snapshot-identifier ${TRANSIT_SNAPSHOT_ID} \
    --kms-key-id "${KMS_TRANSIT_KEY_ARN}" \
    --region us-east-1


# artificial sleep to account for our DB snapshotting taking so long it exceeds the db-snapshot-completed max-count
sleep 1500 && aws rds wait db-snapshot-completed --profile ${SOURCE_PROFILE_NAME} --db-snapshot-identifier $TRANSIT_SNAPSHOT_ID


# share it with the tools account
aws rds modify-db-snapshot-attribute \
    --profile ${SOURCE_PROFILE_NAME} \
    --db-snapshot-identifier ${TRANSIT_SNAPSHOT_ID} \
    --attribute-name restore \
    --values-to-add "[\"${TOOLS_ACCOUNT_ID}\"]"


# get the fully qualified arn of the remote transit snapshot
SNAPSHOT_PROVIDER="$(aws rds describe-db-snapshots \
    --profile ${SOURCE_PROFILE_NAME} \
    --db-snapshot-identifier ${TRANSIT_SNAPSHOT_ID} \
    --query="reverse(sort_by(DBSnapshots, &SnapshotCreateTime))[0]|DBSnapshotArn" \
    --output text)"


# copy the shared snapshot to a local copy. attempting to restore directly yields:
# "Restoring db instance from cross account storage encrypted snapshot is not supported."
echo "Copying snapshot $SNAPSHOT_PROVIDER to $TRANSIT_SNAPSHOT_ID"
aws rds copy-db-snapshot \
    --profile ${TOOLS_PROFILE_NAME} \
    --source-region=us-east-1 \
    --source-db-snapshot-identifier ${SNAPSHOT_PROVIDER} \
    --target-db-snapshot-identifier ${TRANSIT_SNAPSHOT_ID} \
    --kms-key-id "${KMS_TRANSIT_KEY_ARN}" \
    --region us-east-1


# artificial sleep to account for our DB snapshotting taking so long it exceeds the db-snapshot-completed max-count
sleep 1500 && aws rds wait db-snapshot-completed --profile ${SOURCE_PROFILE_NAME} --db-snapshot-identifier $TRANSIT_SNAPSHOT_ID


# delete the remote transit snapshot since we no longer need it
aws rds delete-db-snapshot \
    --profile ${SOURCE_PROFILE_NAME} \
    --db-snapshot-identifier ${TRANSIT_SNAPSHOT_ID}


# get the fully qualified arn of the local transit snapshot
SNAPSHOT_PROVIDER="$(aws rds describe-db-snapshots \
    --profile ${TOOLS_PROFILE_NAME} \
    --db-snapshot-identifier ${TRANSIT_SNAPSHOT_ID} \
    --query="reverse(sort_by(DBSnapshots, &SnapshotCreateTime))[0]|DBSnapshotArn" \
    --output text)"


# restore the snapshot in the tools account
echo "Restoring snapshot $SNAPSHOT_PROVIDER as $RDS_INSTANCE_ID ..."
aws rds restore-db-instance-from-db-snapshot \
    --profile ${TOOLS_PROFILE_NAME} \
    --db-instance-identifier ${RDS_INSTANCE_ID} \
    --db-snapshot-identifier ${SNAPSHOT_PROVIDER} \
    --db-instance-class ${DB_INSTANCE_TYPE} \
    --no-multi-az \
    --publicly-accessible \
    --db-subnet-group-name anon-and-dump
echo "Restore queued. Awaiting available status ..."
aws rds wait db-instance-available --profile ${TOOLS_PROFILE_NAME} --db-instance-identifier ${RDS_INSTANCE_ID}


# delete the transit snapshot since we no longer need it
aws rds delete-db-snapshot \
    --profile ${TOOLS_PROFILE_NAME} \
    --db-snapshot-identifier ${TRANSIT_SNAPSHOT_ID}


# modify things you can't tweak during an initial spool
echo "Modifying ${RDS_INSTANCE_ID} ..."
aws rds modify-db-instance \
    --profile ${TOOLS_PROFILE_NAME} \
    --db-instance-identifier ${RDS_INSTANCE_ID} \
    --vpc-security-group-ids sg-011c759c2c91f3bfa \
    --backup-retention-period 0 \
    --db-parameter-group-name bulk-import-postgres12 \
    --apply-immediately
echo "Modifications queued. Awaiting available status ..."


# artificial sleep to account for status latency with `--apply-immediately` actually scheduling behind the scenes
sleep 180 && aws rds wait db-instance-available --profile ${TOOLS_PROFILE_NAME} --db-instance-identifier ${RDS_INSTANCE_ID}


# reboot to ensure parameter group is applied, etc
echo "Rebooting ${RDS_INSTANCE_ID} ..."
aws rds reboot-db-instance \
    --profile ${TOOLS_PROFILE_NAME} \
    --db-instance-identifier ${RDS_INSTANCE_ID}
aws rds wait db-instance-available --profile ${TOOLS_PROFILE_NAME} --db-instance-identifier ${RDS_INSTANCE_ID}


# determine the current version of the DB and grab latest (see also: `setup_db.sh`)
DB_VERSION=2019_$(PGPASSWORD=${RDS_READONLY_PASSWORD_ORIG} psql -h ${DB_HOST} -U ${DB_USER_READONLY} -c "SELECT count(*) FROM schema_migrations WHERE version > '20190000000000';" -qtAX ${DB_NAME})
echo "Retrieving pgantomizer configs for $DB_VERSION ..."
SS_CONFIG_DIR=s3://${S3_DUMP_BUCKET}/dump_configs/${DB_VERSION}
aws s3 cp $SS_CONFIG_DIR . --profile ${TARGET_PROFILE_NAME} --recursive
chmod +x ./anonymize_and_dump.sh


# handle any customizations that need to be baked into both the snapshot and dev dumps
echo "Removing select constraints to support anonymization ..."
PGPASSWORD=$RDS_WRITE_PASSWORD_ORIG psql -h $DB_HOST -U $DB_USER_WRITE -c "DROP INDEX index_users_on_phone;" -qtAX $DB_NAME


# update credentials for Staging / Local Development use to prevent any unnecessary Production credential exposure
PGPASSWORD=$RDS_WRITE_PASSWORD_ORIG psql -h $DB_HOST -U $DB_USER_WRITE -c "ALTER USER readonly WITH PASSWORD '${RDS_READONLY_PASSWORD_NEW}';" -qtAX $DB_NAME
PGPASSWORD=$RDS_WRITE_PASSWORD_ORIG psql -h $DB_HOST -U $DB_USER_WRITE -c "ALTER USER ebroot WITH PASSWORD '${RDS_WRITE_PASSWORD_NEW}';" -qtAX $DB_NAME


# anonymize all the data, truncating data we don't care about for either staging or local development
echo "Anonymizing $DB_NAME ..."
time pgantomizer --schema $PGANTOMIZER_CONFIG \
    --host ${DB_HOST} \
    --user ${DB_USER_WRITE} \
    --password ${RDS_WRITE_PASSWORD_NEW} \
    --dbname ${DB_NAME} \
    --skip-restore \
    --disable-schema-changes \
    --verbose


# determine dump naming convention
DUMP_TIME="$(date +%Y%m%d)"
DUMP_NAME_BASE=${DB_NAME}_${DB_SRC_ENV_TYPE}
DUMP_NAME_WITH_TIME=${DUMP_NAME_BASE}_${DUMP_TIME}
SNAPSHOT_NAME="$(echo $DUMP_NAME_WITH_TIME | sed -e 's/_/-/g')" # cannot have hypens, which may also exist in DB_NAME


# create snapshot of instance to share with staging use
echo "Snapshotting $RDS_INSTANCE_ID ..."
aws rds create-db-snapshot \
    --profile ${TOOLS_PROFILE_NAME} \
    --db-instance-identifier $RDS_INSTANCE_ID \
    --db-snapshot-identifier ${SNAPSHOT_NAME}-ANON


# artificial sleep to account for our DB snapshotting taking so long it exceeds the db-snapshot-completed max-count
sleep 1500 && aws rds wait db-snapshot-completed --profile ${TOOLS_PROFILE_NAME} --db-snapshot-identifier ${SNAPSHOT_NAME}-ANON


# share it with the staging account
aws rds modify-db-snapshot-attribute \
    --profile ${TOOLS_PROFILE_NAME} \
    --db-snapshot-identifier ${SNAPSHOT_NAME}-ANON \
    --attribute-name restore \
    --values-to-add "[\"${STAGING_ACCOUNT_ID}\"]"


# NOTE: CUSTOM DEV DUMP SLIMMING SUPPORT COULD GO HERE
#
# We might want to have do things like selectively delete from certain tables to reduce dev payload size, etc.
# We could do so here since we've aleady successfully snapshotted the RDS instance for staging use, keeping the
# intended schema / populated size intact.
#
# eg `psql -h $DB_HOST -U $DB_USER_WRITE -d $DB_NAME -c "DELETE FROM lesson_progress WHERE user_id NOT IN (SELECT id FROM users WHERE email LIKE '%@pedago.com');"`
#
# Initial suggestions from Nate:
#
#  1. Before dropping anything, make 3 separate dumps that each have the full content of one of these 3 tables.
#  2. Drop all lesson_progress that is not from @pedago.com users, and maybe leave users from one emba and one mba cohort or so?
#  3. Drop all lessons_versions except for working versions and currently published versions. (Gets rid of 92% of versions).
#     If we want to be a little less aggressive, we could keep all was_published versions as well (Gets rid of 87% of versions).
#     We could also keep all pinned versions (Gets rid of 48% of versions). I think I would just go with the most aggressive strategy
#     though. You only need non-working and non-published versions to work on the version management stuff in the editor,
#     and we very rarely work on that stuff.


# create dump
echo "Creating dump ..."
DUMP_DIR_FILENAME=${DUMP_NAME_BASE}.dir
TMP_DIR=$HOME/tmp/
DUMP_DIR_PATH=${TMP_DIR}${DUMP_DIR_FILENAME}
PGOPTIONS="-c statement_timeout=0" PGPASSWORD=$RDS_READONLY_PASSWORD_NEW pg_dump -Fd -j 8 -f $DUMP_DIR_PATH -h $DB_HOST -U $DB_USER_READONLY \
    --exclude-table-data=delayed_jobs \
    -v $DB_NAME


# archive up directory dump
DUMP_FILE_PATH=$HOME/${DUMP_NAME_WITH_TIME}.tar
touch $DUMP_FILE_PATH
tar cvf $DUMP_FILE_PATH -C $TMP_DIR $DUMP_DIR_FILENAME


# upload to S3
echo "Copying dump file to s3"
aws s3 cp $DUMP_FILE_PATH s3://${S3_DUMP_BUCKET}/ --profile ${TARGET_PROFILE_NAME}


# remove backup
echo "Removing dump files"
rm -rf $DUMP_DIR_PATH $DUMP_FILE_PATH


# terminate instance
echo "Terminating $RDS_INSTANCE_ID ..."
aws rds delete-db-instance --profile ${TOOLS_PROFILE_NAME} --db-instance-identifier $RDS_INSTANCE_ID --skip-final-snapshot
aws rds wait db-instance-deleted --profile ${TOOLS_PROFILE_NAME} --db-instance-identifier $RDS_INSTANCE_ID


echo "Process complete - Duration: $((($(date +%s)-$start_time)/60)) minutes"