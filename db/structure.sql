SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

-- BD: is this causing issues in CI? if so, replace with immediate line below
-- SELECT pg_catalog.set_config('search_path', '', false);
-- Previous search path assignment:
SET search_path = public, pg_catalog;


SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: plv8; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plv8 WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plv8; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plv8 IS 'PL/JavaScript (v8) trusted procedural language';


--
-- Name: dblink; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS dblink WITH SCHEMA public;


--
-- Name: EXTENSION dblink; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION dblink IS 'connect to other PostgreSQL databases from within a database';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: postgres_fdw; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgres_fdw WITH SCHEMA public;


--
-- Name: EXTENSION postgres_fdw; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION postgres_fdw IS 'foreign-data wrapper for remote PostgreSQL servers';


--
-- Name: tablefunc; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA public;


--
-- Name: EXTENSION tablefunc; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION tablefunc IS 'functions that manipulate whole tables, including crosstab';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- Name: client_requirements_json_v1; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.client_requirements_json_v1 AS (
	min_allowed_version integer,
	supported_in_latest_available_version boolean
);


--
-- Name: collection_progress_json_v1; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.collection_progress_json_v1 AS (
	started_at integer,
	updated_at integer
);


--
-- Name: concept_progress_json_v1; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.concept_progress_json_v1 AS (
	collected_at integer,
	collected_in json
);


--
-- Name: entity_metadata_json_v1; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.entity_metadata_json_v1 AS (
	id uuid,
	title character varying(256),
	description character varying(256),
	canonical_url text,
	tweet_template text,
	image json
);


--
-- Name: entity_metadata_json_v2; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.entity_metadata_json_v2 AS (
	id uuid,
	title character varying,
	description character varying,
	canonical_url text,
	tweet_template text,
	image json
);


--
-- Name: image_json_v1; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.image_json_v1 AS (
	id uuid,
	formats json,
	dimensions json
);


--
-- Name: lesson_progress_json_v1; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.lesson_progress_json_v1 AS (
	lesson_id uuid,
	frame_bookmark_id uuid,
	frame_history json,
	started_at integer,
	completed_at integer,
	complete boolean,
	last_progress_at integer,
	id uuid
);


--
-- Name: lesson_progress_json_v2; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.lesson_progress_json_v2 AS (
	lesson_id uuid,
	frame_bookmark_id uuid,
	frame_history json,
	completed_frames json,
	challenge_scores json,
	started_at integer,
	completed_at integer,
	complete boolean,
	last_progress_at integer,
	id uuid
);


--
-- Name: lesson_progress_json_v3; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.lesson_progress_json_v3 AS (
	lesson_id uuid,
	frame_bookmark_id uuid,
	frame_history json,
	completed_frames json,
	challenge_scores json,
	frame_durations json,
	started_at integer,
	completed_at integer,
	complete boolean,
	last_progress_at integer,
	id uuid
);


--
-- Name: lesson_progress_json_v4; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.lesson_progress_json_v4 AS (
	locale_pack_id uuid,
	frame_bookmark_id uuid,
	frame_history json,
	completed_frames json,
	challenge_scores json,
	frame_durations json,
	started_at integer,
	completed_at integer,
	complete boolean,
	last_progress_at integer,
	id uuid
);


--
-- Name: lesson_progress_json_v5; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.lesson_progress_json_v5 AS (
	locale_pack_id uuid,
	frame_bookmark_id uuid,
	frame_history json,
	completed_frames json,
	challenge_scores json,
	frame_durations json,
	started_at integer,
	completed_at integer,
	complete boolean,
	last_progress_at integer,
	id uuid,
	best_score double precision
);


--
-- Name: locale_pack_json_v1; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.locale_pack_json_v1 AS (
	id uuid,
	content_items json
);


--
-- Name: locale_pack_json_v2; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.locale_pack_json_v2 AS (
	id uuid,
	content_items json,
	groups json
);


--
-- Name: locale_pack_json_v3; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.locale_pack_json_v3 AS (
	id uuid,
	content_items json,
	groups json,
	content_topics json
);


--
-- Name: locale_pack_json_v4; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.locale_pack_json_v4 AS (
	id uuid,
	content_items json,
	groups json,
	content_topics json,
	practice_locale_pack_id uuid,
	is_practice_for_locale_pack_id uuid,
	practice_content_items json
);


--
-- Name: user_json_v1; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.user_json_v1 AS (
	id uuid,
	name character varying(256)
);


--
-- Name: add_dst_aware_offset(timestamp without time zone, numeric, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.add_dst_aware_offset(reftime timestamp without time zone, val numeric, unit text) RETURNS timestamp without time zone
    LANGUAGE sql
    AS $$
            SELECT (
                (
                    -- Start with a timestamp without zone, which is how we
                    -- normally store things in the database. For example, we
                    -- might have '2018/03/08 12:00:00' stored in the database. This
                    -- timestamp without time zone represents the time
                    -- '2018/03/08 12:00:00 UTC' == '2018/03/08 07:00 EST -0500', though
                    -- that is just something we know as developers, it is NOT encoded
                    -- in postgres in any way.
                    reftime

                    -- Tell postgres that the timestamp without zone that we have represents
                    -- a utc time, and convert it to a timestamp with zone
                    at time zone 'UTC'

                    -- Ask postgres what is the equivalent time in eastern. Postgres, surprisingly
                    -- returns a timestamp WITHOUT zone here, equal to '2018/03/08 07:00'.  So it
                    -- is the time in eastern that corresponds to the utc time we had saved in the
                    -- database, but there is no time zone information encoded in the object itself.
                    at time zone 'America/New_York'

                    -- Now we can add intervals to this time and it will act as though we lived
                    -- in a world with no timezones and no daylight savings time (e.g. the world
                    -- we should actually live in).  For example, we might add '10 days', ending
                    -- up with the timestamp without zone '2018/03/18 07:00'.
                    + (val || ' ' || unit)::INTERVAL
                )

                -- Tell postgres that this timestamp without timezone that we have represents
                -- an eastern time, and convert it to a timestamp with zone
                at time zone 'America/New_York'

                -- Ask postgres what is the equivalent time in UTC. Once again, postgres returns
                -- an timestamp without time zone, and that is what we want to put in the database.
                -- In our example, we are now on the other side of daylight savings time, so we
                -- get a utc time with a different hour than the one we started with, which is
                -- what we want: '2018/03/18 11:00'
                at time zone 'UTC'
            );
        $$;


--
-- Name: array_intersect(anyarray, anyarray); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.array_intersect(anyarray, anyarray) RETURNS anyarray
    LANGUAGE sql
    AS $_$
                SELECT ARRAY(
                    SELECT UNNEST($1)
                    INTERSECT
                    SELECT UNNEST($2)
                );
            $_$;


--
-- Name: array_sort_unique(anyarray); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.array_sort_unique(anyarray) RETURNS anyarray
    LANGUAGE sql
    AS $_$
              SELECT ARRAY(
                SELECT DISTINCT $1[s.i]
                FROM generate_series(array_lower($1,1), array_upper($1,1)) AS s(i)
                ORDER BY 1
              );
              $_$;


--
-- Name: client_type(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.client_type(client text, os_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
                    BEGIN

                        RETURN CASE
                            WHEN client in ('ios', 'android') THEN 'mobile_app'
                            WHEN os_name ilike '%windows phone%'
                                    OR os_name ilike '%ios%'
                                    OR os_name ilike '%android%'
                                THEN
                                    'mobile_web'
                            WHEN os_name ilike '%windows%'
                                    OR os_name ilike '%mac os%'
                                    OR os_name ilike '%chrome os%'
                                THEN
                                    'desktop'
                            ELSE 'unknown'
                        END;

                    END
                    $$;


--
-- Name: count_estimate(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.count_estimate(query text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
                    DECLARE
                        rec   record;
                        ROWS  INTEGER;
                    BEGIN
                        FOR rec IN EXECUTE 'EXPLAIN ' || query LOOP
                            ROWS := SUBSTRING(rec."QUERY PLAN" FROM ' rows=([[:digit:]]+)');
                            EXIT WHEN ROWS IS NOT NULL;
                        END LOOP;

                        RETURN ROWS;
                    END
                    $$;


--
-- Name: interval_for_plan(json[], text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.interval_for_plan(stripe_plans json[], stripe_plan_id text) RETURNS text
    LANGUAGE plv8 IMMUTABLE STRICT
    AS $$

                function getIntervalForPlan(plan) {
                    switch (plan.frequency) {
                        case 'bi_annual':
                            return '6 months';
                        case 'monthly':
                            return '1 month';
                    }
                }

                var plan;
                for (var i = 0; i < stripe_plans.length; i++) {
                    var _plan = stripe_plans[i];
                    if (_plan.id === stripe_plan_id) {
                        plan = _plan;
                    }
                }

                return getIntervalForPlan(plan);

            $$;


--
-- Name: is_paid_cert_program_type(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.is_paid_cert_program_type(program_type text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
            BEGIN
                RETURN program_type like 'paid_cert%';
            END
            $$;


--
-- Name: payment_amount_per_period(json[], text, json); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.payment_amount_per_period(stripe_plans json[], stripe_plan_id text, scholarship_level json) RETURNS double precision
    LANGUAGE plv8 IMMUTABLE STRICT
    AS $$

                function getPlanTuitionActualTotal(plan) {
                    var coupon = scholarship_level.coupons[plan.id];

                    var amountOff = coupon ? coupon.amount_off : 0,
                        percentOff = coupon ? coupon.percent_off : 0,
                        tuitionNumPaymentIntervals = getNumIntervalsForPlan(plan),
                        tuitionOriginalTotal = (plan.amount * tuitionNumPaymentIntervals / 100) || 0,
                        tuitionScholarshipTotal;


                    if (!!amountOff) {
                        tuitionScholarshipTotal = tuitionNumPaymentIntervals * amountOff / 100;
                    } else if (!!percentOff) {
                        tuitionScholarshipTotal = percentOff * tuitionOriginalTotal / 100;
                    } else {
                        tuitionScholarshipTotal = 0;
                    }
                    return tuitionOriginalTotal - tuitionScholarshipTotal;
                }

                function getNumIntervalsForPlan(plan) {
                    switch (plan.frequency) {
                        case 'bi_annual':
                            return 2;
                        case 'monthly':
                            return 12;
                        case 'once':
                            return 1;
                    }
                }

                var plan;
                for (var i = 0; i < stripe_plans.length; i++) {
                    var _plan = stripe_plans[i];
                    if (_plan.id === stripe_plan_id) {
                        plan = _plan;
                    }
                }

                var tuitionActualTotal = getPlanTuitionActualTotal(plan),
                    tuitionNumPaymentIntervals = getNumIntervalsForPlan(plan),
                    tuitionActualPaymentPerInterval = tuitionActualTotal / tuitionNumPaymentIntervals;

                return tuitionActualPaymentPerInterval;
            $$;


--
-- Name: process_access_groups_cohorts_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_access_groups_cohorts_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO access_groups_cohorts_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO access_groups_cohorts_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO access_groups_cohorts_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_access_groups_curriculum_templates_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_access_groups_curriculum_templates_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO access_groups_curriculum_templates_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO access_groups_curriculum_templates_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO access_groups_curriculum_templates_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_access_groups_institutions_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_access_groups_institutions_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO access_groups_institutions_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO access_groups_institutions_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO access_groups_institutions_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_access_groups_lesson_stream_locale_packs_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_access_groups_lesson_stream_locale_packs_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO access_groups_lesson_stream_locale_packs_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO access_groups_lesson_stream_locale_packs_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO access_groups_lesson_stream_locale_packs_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_access_groups_lesson_streams_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_access_groups_lesson_streams_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO access_groups_lesson_streams_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO access_groups_lesson_streams_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO access_groups_lesson_streams_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_access_groups_playlist_locale_packs_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_access_groups_playlist_locale_packs_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO access_groups_playlist_locale_packs_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO access_groups_playlist_locale_packs_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO access_groups_playlist_locale_packs_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_access_groups_playlists_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_access_groups_playlists_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO access_groups_playlists_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO access_groups_playlists_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO access_groups_playlists_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_access_groups_users_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_access_groups_users_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO access_groups_users_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO access_groups_users_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO access_groups_users_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_access_groups_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_access_groups_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO access_groups_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO access_groups_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO access_groups_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_awards_and_interests_options_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_awards_and_interests_options_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO awards_and_interests_options_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO awards_and_interests_options_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO awards_and_interests_options_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_candidate_position_interests_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_candidate_position_interests_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO candidate_position_interests_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO candidate_position_interests_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO candidate_position_interests_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_career_profile_lists_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_career_profile_lists_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO career_profile_lists_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO career_profile_lists_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO career_profile_lists_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_career_profiles_awards_and_interests_options_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_career_profiles_awards_and_interests_options_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO career_profiles_awards_and_interests_options_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO career_profiles_awards_and_interests_options_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO career_profiles_awards_and_interests_options_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_career_profiles_skills_options_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_career_profiles_skills_options_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO career_profiles_skills_options_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO career_profiles_skills_options_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO career_profiles_skills_options_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_career_profiles_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_career_profiles_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO career_profiles_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO career_profiles_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO career_profiles_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_cohort_applications_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_cohort_applications_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO cohort_applications_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO cohort_applications_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO cohort_applications_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_cohort_promotions_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_cohort_promotions_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO cohort_promotions_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO cohort_promotions_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO cohort_promotions_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_cohorts_users_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_cohorts_users_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO cohorts_users_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO cohorts_users_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO cohorts_users_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_cohorts_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_cohorts_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO cohorts_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO cohorts_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO cohorts_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_collection_concepts_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_collection_concepts_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO collection_concepts_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO collection_concepts_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO collection_concepts_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_collections_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_collections_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO collections_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO collections_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO collections_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_concepts_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_concepts_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO concepts_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO concepts_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO concepts_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_content_publishers_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_content_publishers_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO content_publishers_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO content_publishers_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO content_publishers_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_content_topics_daily_lesson_configs_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_content_topics_daily_lesson_configs_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO content_topics_daily_lesson_configs_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO content_topics_daily_lesson_configs_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO content_topics_daily_lesson_configs_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_content_topics_lesson_stream_locale_packs_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_content_topics_lesson_stream_locale_packs_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO content_topics_lesson_stream_locale_packs_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO content_topics_lesson_stream_locale_packs_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO content_topics_lesson_stream_locale_packs_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_content_topics_lesson_streams_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_content_topics_lesson_streams_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO content_topics_lesson_streams_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO content_topics_lesson_streams_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO content_topics_lesson_streams_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_content_topics_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_content_topics_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO content_topics_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO content_topics_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO content_topics_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_curriculum_templates_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_curriculum_templates_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO curriculum_templates_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO curriculum_templates_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO curriculum_templates_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_daily_lesson_configs_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_daily_lesson_configs_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO daily_lesson_configs_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO daily_lesson_configs_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO daily_lesson_configs_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_education_experiences_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_education_experiences_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO education_experiences_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO education_experiences_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO education_experiences_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_educational_organization_options_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_educational_organization_options_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO educational_organization_options_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO educational_organization_options_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO educational_organization_options_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_hiring_applications_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_hiring_applications_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO hiring_applications_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO hiring_applications_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO hiring_applications_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_hiring_relationships_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_hiring_relationships_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO hiring_relationships_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO hiring_relationships_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO hiring_relationships_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_hiring_teams_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_hiring_teams_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO hiring_teams_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO hiring_teams_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO hiring_teams_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_institutions_groups_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_institutions_groups_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO institutions_groups_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO institutions_groups_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO institutions_groups_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_institutions_users_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_institutions_users_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO institutions_users_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO institutions_users_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO institutions_users_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_institutions_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_institutions_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO institutions_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO institutions_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO institutions_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_lesson_locale_packs_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_lesson_locale_packs_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO lesson_locale_packs_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO lesson_locale_packs_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO lesson_locale_packs_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_lesson_stream_locale_packs_users_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_lesson_stream_locale_packs_users_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO lesson_stream_locale_packs_users_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO lesson_stream_locale_packs_users_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO lesson_stream_locale_packs_users_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_lesson_stream_locale_packs_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_lesson_stream_locale_packs_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO lesson_stream_locale_packs_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO lesson_stream_locale_packs_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO lesson_stream_locale_packs_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_lesson_streams_collections_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_lesson_streams_collections_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO lesson_streams_collections_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO lesson_streams_collections_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO lesson_streams_collections_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_lesson_streams_onboarding_goals_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_lesson_streams_onboarding_goals_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO lesson_streams_onboarding_goals_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO lesson_streams_onboarding_goals_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO lesson_streams_onboarding_goals_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_lesson_streams_users_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_lesson_streams_users_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO lesson_streams_users_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO lesson_streams_users_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO lesson_streams_users_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_lesson_streams_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_lesson_streams_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                        BEGIN
                            --
                            -- Create a row in audit to reflect the operation performed on the table,
                            -- make use of the special variable TG_OP to work out the operation.
                            --
                            IF (TG_OP = 'DELETE') THEN
                                INSERT INTO lesson_streams_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.id;
                                RETURN OLD;
                            ELSIF (TG_OP = 'UPDATE') THEN
                                INSERT INTO lesson_streams_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                                RETURN NEW;
                            ELSIF (TG_OP = 'INSERT') THEN
                                INSERT INTO lesson_streams_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                                RETURN NEW;
                            END IF;
                            RETURN NULL; -- result is ignored since this is an AFTER trigger
                        END;
                    $$;


--
-- Name: process_lesson_to_stream_joins_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_lesson_to_stream_joins_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO lesson_to_stream_joins_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO lesson_to_stream_joins_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO lesson_to_stream_joins_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_lessons_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_lessons_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                        BEGIN
                            --
                            -- Create a row in audit to reflect the operation performed on the table,
                            -- make use of the special variable TG_OP to work out the operation.
                            --
                            IF (TG_OP = 'DELETE') THEN
                                INSERT INTO lessons_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.id;
                                RETURN OLD;
                            ELSIF (TG_OP = 'UPDATE') THEN
                                INSERT INTO lessons_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                                RETURN NEW;
                            ELSIF (TG_OP = 'INSERT') THEN
                                INSERT INTO lessons_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                                RETURN NEW;
                            END IF;
                            RETURN NULL; -- result is ignored since this is an AFTER trigger
                        END;
                    $$;


--
-- Name: process_locations_of_interest_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_locations_of_interest_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO locations_of_interest_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO locations_of_interest_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO locations_of_interest_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_onboarding_goals_users_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_onboarding_goals_users_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO onboarding_goals_users_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO onboarding_goals_users_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO onboarding_goals_users_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_onboarding_goals_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_onboarding_goals_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO onboarding_goals_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO onboarding_goals_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO onboarding_goals_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_onboarding_motivations_users_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_onboarding_motivations_users_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO onboarding_motivations_users_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO onboarding_motivations_users_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO onboarding_motivations_users_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_onboarding_motivations_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_onboarding_motivations_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO onboarding_motivations_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO onboarding_motivations_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO onboarding_motivations_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_open_positions_skills_options_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_open_positions_skills_options_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO open_positions_skills_options_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO open_positions_skills_options_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO open_positions_skills_options_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_open_positions_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_open_positions_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO open_positions_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO open_positions_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO open_positions_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_peer_recommendations_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_peer_recommendations_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO peer_recommendations_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO peer_recommendations_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO peer_recommendations_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_playlist_locale_packs_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_playlist_locale_packs_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO playlist_locale_packs_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO playlist_locale_packs_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO playlist_locale_packs_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_playlists_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_playlists_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO playlists_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO playlists_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO playlists_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_professional_organization_options_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_professional_organization_options_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO professional_organization_options_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO professional_organization_options_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO professional_organization_options_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_roles_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_roles_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO roles_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO roles_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO roles_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_skills_options_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_skills_options_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO skills_options_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO skills_options_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO skills_options_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_student_network_interests_options_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_student_network_interests_options_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
              BEGIN
                  --
                  -- Create a row in audit to reflect the operation performed on the table,
                  -- make use of the special variable TG_OP to work out the operation.
                  --
                  IF (TG_OP = 'DELETE') THEN
                      INSERT INTO student_network_interests_options_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                      RETURN OLD;
                  ELSIF (TG_OP = 'UPDATE') THEN
                      INSERT INTO student_network_interests_options_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                      RETURN NEW;
                  ELSIF (TG_OP = 'INSERT') THEN
                      INSERT INTO student_network_interests_options_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                      RETURN NEW;
                  END IF;
                  RETURN NULL; -- result is ignored since this is an AFTER trigger
              END;
          $$;


--
-- Name: process_subscriptions_users_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_subscriptions_users_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO subscriptions_users_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO subscriptions_users_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO subscriptions_users_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_subscriptions_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_subscriptions_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO subscriptions_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO subscriptions_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO subscriptions_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_tags_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_tags_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO tags_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO tags_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO tags_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_users_roles_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_users_roles_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO users_roles_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO users_roles_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO users_roles_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_users_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_users_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO users_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN

                IF NEW.email IS DISTINCT FROM OLD.email OR NEW.encrypted_password IS DISTINCT FROM OLD.encrypted_password OR NEW.reset_password_token IS DISTINCT FROM OLD.reset_password_token OR NEW.reset_password_sent_at IS DISTINCT FROM OLD.reset_password_sent_at OR NEW.sign_up_code IS DISTINCT FROM OLD.sign_up_code OR NEW.reset_password_redirect_url IS DISTINCT FROM OLD.reset_password_redirect_url OR NEW.provider IS DISTINCT FROM OLD.provider OR NEW.uid IS DISTINCT FROM OLD.uid OR NEW.has_seen_welcome IS DISTINCT FROM OLD.has_seen_welcome OR NEW.notify_email_newsletter IS DISTINCT FROM OLD.notify_email_newsletter OR NEW.school IS DISTINCT FROM OLD.school OR NEW.free_trial_started IS DISTINCT FROM OLD.free_trial_started OR NEW.confirmed_profile_info IS DISTINCT FROM OLD.confirmed_profile_info OR NEW.pref_decimal_delim IS DISTINCT FROM OLD.pref_decimal_delim OR NEW.optimizely_referer IS DISTINCT FROM OLD.optimizely_referer OR NEW.pref_locale IS DISTINCT FROM OLD.pref_locale OR NEW.active_playlist_locale_pack_id IS DISTINCT FROM OLD.active_playlist_locale_pack_id OR NEW.avatar_url IS DISTINCT FROM OLD.avatar_url OR NEW.avatar_provider IS DISTINCT FROM OLD.avatar_provider OR NEW.mba_content_lockable IS DISTINCT FROM OLD.mba_content_lockable OR NEW.job_title IS DISTINCT FROM OLD.job_title OR NEW.phone IS DISTINCT FROM OLD.phone OR NEW.country IS DISTINCT FROM OLD.country OR NEW.has_seen_accepted IS DISTINCT FROM OLD.has_seen_accepted OR NEW.name IS DISTINCT FROM OLD.name OR NEW.nickname IS DISTINCT FROM OLD.nickname OR NEW.phone_extension IS DISTINCT FROM OLD.phone_extension OR NEW.can_edit_career_profile IS DISTINCT FROM OLD.can_edit_career_profile OR NEW.professional_organization_option_id IS DISTINCT FROM OLD.professional_organization_option_id OR NEW.pref_show_photos_names IS DISTINCT FROM OLD.pref_show_photos_names OR NEW.identity_verified IS DISTINCT FROM OLD.identity_verified OR NEW.sex IS DISTINCT FROM OLD.sex OR NEW.ethnicity IS DISTINCT FROM OLD.ethnicity OR NEW.race IS DISTINCT FROM OLD.race OR NEW.how_did_you_hear_about_us IS DISTINCT FROM OLD.how_did_you_hear_about_us OR NEW.anything_else_to_tell_us IS DISTINCT FROM OLD.anything_else_to_tell_us OR NEW.birthdate IS DISTINCT FROM OLD.birthdate OR NEW.address_line_1 IS DISTINCT FROM OLD.address_line_1 OR NEW.address_line_2 IS DISTINCT FROM OLD.address_line_2 OR NEW.city IS DISTINCT FROM OLD.city OR NEW.state IS DISTINCT FROM OLD.state OR NEW.zip IS DISTINCT FROM OLD.zip OR NEW.pref_keyboard_shortcuts IS DISTINCT FROM OLD.pref_keyboard_shortcuts OR NEW.fallback_program_type IS DISTINCT FROM OLD.fallback_program_type OR NEW.program_type_confirmed IS DISTINCT FROM OLD.program_type_confirmed OR NEW.invite_code IS DISTINCT FROM OLD.invite_code OR NEW.notify_hiring_updates IS DISTINCT FROM OLD.notify_hiring_updates OR NEW.notify_hiring_spotlights IS DISTINCT FROM OLD.notify_hiring_spotlights OR NEW.notify_hiring_content IS DISTINCT FROM OLD.notify_hiring_content OR NEW.notify_hiring_candidate_activity IS DISTINCT FROM OLD.notify_hiring_candidate_activity OR NEW.hiring_team_id IS DISTINCT FROM OLD.hiring_team_id OR NEW.referred_by_id IS DISTINCT FROM OLD.referred_by_id OR NEW.has_seen_featured_positions_page IS DISTINCT FROM OLD.has_seen_featured_positions_page OR NEW.pref_one_click_reach_out IS DISTINCT FROM OLD.pref_one_click_reach_out OR NEW.has_seen_careers_welcome IS DISTINCT FROM OLD.has_seen_careers_welcome OR NEW.can_purchase_paid_certs IS DISTINCT FROM OLD.can_purchase_paid_certs OR NEW.has_seen_hide_candidate_confirm IS DISTINCT FROM OLD.has_seen_hide_candidate_confirm OR NEW.pref_positions_candidate_list IS DISTINCT FROM OLD.pref_positions_candidate_list OR NEW.skip_apply IS DISTINCT FROM OLD.skip_apply OR NEW.has_seen_student_network IS DISTINCT FROM OLD.has_seen_student_network OR NEW.pref_student_network_privacy IS DISTINCT FROM OLD.pref_student_network_privacy OR NEW.notify_hiring_spotlights_business IS DISTINCT FROM OLD.notify_hiring_spotlights_business OR NEW.notify_hiring_spotlights_technical IS DISTINCT FROM OLD.notify_hiring_spotlights_technical OR NEW.has_drafted_open_position IS DISTINCT FROM OLD.has_drafted_open_position OR NEW.deactivated IS DISTINCT FROM OLD.deactivated OR NEW.notify_candidate_positions_technical IS DISTINCT FROM OLD.notify_candidate_positions_technical OR NEW.notify_candidate_positions_business IS DISTINCT FROM OLD.notify_candidate_positions_business OR NEW.avatar_id IS DISTINCT FROM OLD.avatar_id OR NEW.has_seen_first_position_review_modal IS DISTINCT FROM OLD.has_seen_first_position_review_modal THEN
                INSERT INTO users_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                END IF;

                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO users_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: process_work_experiences_version(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.process_work_experiences_version() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO work_experiences_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO work_experiences_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO work_experiences_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: update_career_profile_location(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_career_profile_location() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    IF (TG_OP = 'UPDATE' OR TG_OP = 'INSERT') THEN
                        NEW.location = CASE WHEN
                                (NEW.place_details->>'lng')::text IS NOT NULL AND (NEW.place_details->>'lat')::text IS NOT NULL
                            THEN
                                ST_GeographyFromText('POINT(' || (NEW.place_details->>'lng')::text || ' ' || (NEW.place_details->>'lat')::text || ')')
                            ELSE
                                NULL
                            END;
                    END IF;
                    RETURN NEW;
                END;
            $$;


--
-- Name: validate_hiring_teams_on_relationship_change(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.validate_hiring_teams_on_relationship_change() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    IF (
                        NEW.hiring_manager_status != 'hidden'
                        AND (
                            TG_OP = 'INSERT'
                            OR OLD.hiring_manager_status != NEW.hiring_manager_status
                            OR OLD.hiring_manager_id != NEW.hiring_manager_id
                        )
                        AND EXISTS(
            WITH pairs AS (
                SELECT
                    candidate_id,
                    hiring_team_id,
                    hiring_manager_status
                FROM
                    hiring_relationships
                    JOIN users hiring_managers
                        ON hiring_managers.id = hiring_manager_id
                WHERE
                    hiring_manager_status != 'hidden'
                    AND hiring_managers.hiring_team_id = (select hiring_team_id from users where id = NEW.hiring_manager_id)
            )
            SELECT
                candidate_id
                , count(*)
            FROM pairs
            GROUP BY candidate_id
            HAVING
                COUNT(*) > 1
                AND COUNT(case when hiring_manager_status = 'accepted' then true else null end ) > 0
            LIMIT 1
        )
                    ) THEN
                        RAISE EXCEPTION 'Multiple connections to candidate "%"', (select candidate_id from (
            WITH pairs AS (
                SELECT
                    candidate_id,
                    hiring_team_id,
                    hiring_manager_status
                FROM
                    hiring_relationships
                    JOIN users hiring_managers
                        ON hiring_managers.id = hiring_manager_id
                WHERE
                    hiring_manager_status != 'hidden'
                    AND hiring_managers.hiring_team_id = (select hiring_team_id from users where id = NEW.hiring_manager_id)
            )
            SELECT
                candidate_id
                , count(*)
            FROM pairs
            GROUP BY candidate_id
            HAVING
                COUNT(*) > 1
                AND COUNT(case when hiring_manager_status = 'accepted' then true else null end ) > 0
            LIMIT 1
        ) a);
                    END IF;

                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: validate_hiring_teams_on_team_change(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.validate_hiring_teams_on_team_change() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    IF (
                        NEW.hiring_team_id IS NOT NULL
                        AND (
                            TG_OP = 'INSERT'
                            OR OLD.hiring_team_id IS NULL
                            OR OLD.hiring_team_id != NEW.hiring_team_id
                        )
                        AND EXISTS(
            WITH pairs AS (
                SELECT
                    candidate_id,
                    hiring_team_id,
                    hiring_manager_status
                FROM
                    hiring_relationships
                    JOIN users hiring_managers
                        ON hiring_managers.id = hiring_manager_id
                WHERE
                    hiring_manager_status != 'hidden'
                    AND hiring_managers.hiring_team_id = (NEW.hiring_team_id)
            )
            SELECT
                candidate_id
                , count(*)
            FROM pairs
            GROUP BY candidate_id
            HAVING
                COUNT(*) > 1
                AND COUNT(case when hiring_manager_status = 'accepted' then true else null end ) > 0
            LIMIT 1
        )
                    ) THEN
                        RAISE EXCEPTION 'Multiple connections to candidate "%"', (select candidate_id from (
            WITH pairs AS (
                SELECT
                    candidate_id,
                    hiring_team_id,
                    hiring_manager_status
                FROM
                    hiring_relationships
                    JOIN users hiring_managers
                        ON hiring_managers.id = hiring_manager_id
                WHERE
                    hiring_manager_status != 'hidden'
                    AND hiring_managers.hiring_team_id = (NEW.hiring_team_id)
            )
            SELECT
                candidate_id
                , count(*)
            FROM pairs
            GROUP BY candidate_id
            HAVING
                COUNT(*) > 1
                AND COUNT(case when hiring_manager_status = 'accepted' then true else null end ) > 0
            LIMIT 1
        ) a);
                    END IF;

                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $$;


--
-- Name: array_cat(anyarray); Type: AGGREGATE; Schema: public; Owner: -
--

CREATE AGGREGATE public.array_cat(anyarray) (
    SFUNC = array_cat,
    STYPE = anyarray
);


--
-- Name: array_cat_agg(anyarray); Type: AGGREGATE; Schema: public; Owner: -
--

CREATE AGGREGATE public.array_cat_agg(anyarray) (
    SFUNC = array_cat,
    STYPE = anyarray
);


--
-- Name: red_royal; Type: SERVER; Schema: -; Owner: -
--

CREATE SERVER red_royal FOREIGN DATA WRAPPER postgres_fdw OPTIONS (
    dbname 'red_royal_test',
    host 'localhost',
    port ''
);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: access_groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.access_groups (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: access_groups_cohorts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.access_groups_cohorts (
    cohort_id uuid NOT NULL,
    access_group_id uuid NOT NULL
);


--
-- Name: access_groups_cohorts_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.access_groups_cohorts_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    cohort_id uuid NOT NULL,
    access_group_id uuid NOT NULL
);


--
-- Name: access_groups_curriculum_templates; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.access_groups_curriculum_templates (
    curriculum_template_id uuid NOT NULL,
    access_group_id uuid NOT NULL
);


--
-- Name: access_groups_curriculum_templates_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.access_groups_curriculum_templates_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    curriculum_template_id uuid NOT NULL,
    access_group_id uuid NOT NULL
);


--
-- Name: access_groups_institutions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.access_groups_institutions (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    access_group_id uuid NOT NULL,
    institution_id uuid NOT NULL
);


--
-- Name: access_groups_institutions_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.access_groups_institutions_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    access_group_id uuid,
    institution_id uuid
);


--
-- Name: access_groups_lesson_stream_locale_packs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.access_groups_lesson_stream_locale_packs (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    access_group_id uuid,
    locale_pack_id uuid
);


--
-- Name: access_groups_lesson_stream_locale_packs_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.access_groups_lesson_stream_locale_packs_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    access_group_id uuid,
    locale_pack_id uuid
);


--
-- Name: access_groups_playlist_locale_packs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.access_groups_playlist_locale_packs (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    access_group_id uuid,
    locale_pack_id uuid
);


--
-- Name: access_groups_playlist_locale_packs_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.access_groups_playlist_locale_packs_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    access_group_id uuid,
    locale_pack_id uuid
);


--
-- Name: access_groups_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.access_groups_users (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    access_group_id uuid NOT NULL,
    user_id uuid NOT NULL
);


--
-- Name: access_groups_users_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.access_groups_users_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    access_group_id uuid,
    user_id uuid
);


--
-- Name: access_groups_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.access_groups_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    name text NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: acquisitions; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.acquisitions AS
 SELECT acquisitions.user_id,
    acquisitions.acquired_at,
    acquisitions.acquired_on_cordova,
    acquisitions.registered,
    acquisitions.did_not_bounce
   FROM public.dblink('red_royal'::text, '

                        with ip_addresses_with_many_logins_in_one_day as (
                            select
                                distinct ip_address
                            from
                                events
                            where
                                event_type=''page_load:load''
                                and ip_address is not null
                            group by
                                ip_address
                                , date_trunc(''day'', estimated_time)
                            having
                               count(distinct user_id) > 5
                            order by ip_address
                        )
                        , ip_addresses_from_which_someone_registered as (
                            select
                                distinct ip_addresses_with_many_logins_in_one_day.ip_address
                            from
                                ip_addresses_with_many_logins_in_one_day
                                join events page_load_events
                                    on page_load_events.ip_address = ip_addresses_with_many_logins_in_one_day.ip_address
                                    and page_load_events.event_type = ''page_load:load''
                                join events user_created_events
                                    on user_created_events.user_id = page_load_events.user_id
                                    and user_created_events.event_type = ''user:created''
                        )
                        , bot_ip_addresses as (
                            select
                                ip_address
                            from
                                ip_addresses_with_many_logins_in_one_day
                            where
                                ip_address not in (select ip_address from ip_addresses_from_which_someone_registered)
                        )
                        , bot_users as (
                            select
                                distinct user_id
                            from events
                                join bot_ip_addresses
                                    on bot_ip_addresses.ip_address = events.ip_address
                            where event_type=''page_load:load''
                        )
                        , alias_users as (
                            select
                                distinct user_id
                            from
                                events
                            where event_type  = ''anonymous_user:login''
                                and user_id != logged_in_user_id
                                and user_id not in (select distinct user_id from events where event_type=''user:created'')
                        )
                        , first_page_loads as (
                            select
                                events.user_id
                                , min(estimated_time) acquired_at
                            from events
                            where event_type = ''page_load:load''
                                and user_id not in (select user_id from alias_users)
                                and user_id not in (select user_id from bot_users)
                            group by
                                events.user_id
                        )
                        , first_page_loads_with_cordova_flag as (
                            select
                                first_page_loads.user_id
                                , acquired_at

                                -- in the wild, there will only be one event with this
                                -- estimated_time, but in tests there are multiple events with the
                                -- same estimated_time, so we need to guarantee uniqueness
                                , count(case when cordova then true else null end) > 0 acquired_on_cordova
                            from first_page_loads
                                join events
                                    on events.user_id = first_page_loads.user_id
                                    and events.event_type=''page_load:load''
                                    and events.estimated_time = acquired_at
                            group by
                                first_page_loads.user_id
                                , acquired_at
                        )
                        , acquisitions as (
                            select
                              first_page_loads_with_cordova_flag.user_id
                              , acquired_at
                              , acquired_on_cordova
                              , sum(case when event_type=''user:created'' then 1 else 0 end) > 0 as registered
                              , count(*) > 1 as did_not_bounce
                            from first_page_loads_with_cordova_flag
                                join events
                                    on first_page_loads_with_cordova_flag.user_id = events.user_id
                                    and events.event_type in (''user:created'', ''page_load:load'', ''click'')
                            group by
                                first_page_loads_with_cordova_flag.user_id
                              , acquired_at
                              , acquired_on_cordova
                        )
                        select * from acquisitions

                    '::text) acquisitions(user_id uuid, acquired_at timestamp without time zone, acquired_on_cordova boolean, registered boolean, did_not_bounce boolean)
  WITH NO DATA;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: archived_events; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.archived_events (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    user_id uuid,
    event_type character varying(255),
    payload json,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    estimated_time timestamp without time zone,
    client_reported_time timestamp without time zone,
    hit_server_at timestamp without time zone,
    total_buffered_seconds double precision
);


--
-- Name: awards_and_interests_options; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.awards_and_interests_options (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    text text NOT NULL,
    locale text DEFAULT 'en'::text NOT NULL,
    suggest boolean DEFAULT false
);


--
-- Name: awards_and_interests_options_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.awards_and_interests_options_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    text text NOT NULL,
    locale text NOT NULL,
    suggest boolean
);


--
-- Name: candidate_position_interests; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.candidate_position_interests (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    candidate_id uuid NOT NULL,
    open_position_id uuid NOT NULL,
    candidate_status text NOT NULL,
    hiring_manager_status text DEFAULT 'hidden'::text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    hiring_manager_priority integer,
    admin_status text DEFAULT 'unreviewed'::text NOT NULL,
    hiring_manager_reviewer_id uuid
);


--
-- Name: candidate_position_interests_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.candidate_position_interests_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    candidate_id uuid NOT NULL,
    open_position_id uuid NOT NULL,
    candidate_status text NOT NULL,
    hiring_manager_status text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    hiring_manager_priority integer,
    admin_status text,
    hiring_manager_reviewer_id uuid
);


--
-- Name: career_profile_fulltext; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.career_profile_fulltext (
    career_profile_id uuid NOT NULL,
    keyword_vector tsvector,
    keyword_text text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    student_network_full_keyword_vector tsvector,
    student_network_full_keyword_text text,
    student_network_anonymous_keyword_vector tsvector,
    student_network_anonymous_keyword_text text
);


--
-- Name: career_profile_lists; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.career_profile_lists (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    name text NOT NULL,
    career_profile_ids uuid[] DEFAULT '{}'::uuid[] NOT NULL,
    role_descriptors text[] DEFAULT '{}'::text[] NOT NULL
);


--
-- Name: career_profile_lists_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.career_profile_lists_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    name text NOT NULL,
    career_profile_ids uuid[] NOT NULL,
    role_descriptors text[]
);


--
-- Name: career_profiles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.career_profiles (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    user_id uuid,
    score_on_gmat text,
    score_on_sat text,
    score_on_act text,
    short_answers json DEFAULT '{}'::json NOT NULL,
    willing_to_relocate boolean DEFAULT true NOT NULL,
    open_to_remote_work boolean DEFAULT true NOT NULL,
    authorized_to_work_in_us text,
    top_mba_subjects text[] DEFAULT '{}'::text[] NOT NULL,
    top_personal_descriptors text[] DEFAULT '{}'::text[] NOT NULL,
    top_motivations text[] DEFAULT '{}'::text[] NOT NULL,
    top_workplace_strengths text[] DEFAULT '{}'::text[] NOT NULL,
    bio text,
    career_goals text,
    personal_fact text,
    preferred_company_culture_descriptors text[] DEFAULT '{}'::text[] NOT NULL,
    li_profile_url text,
    resume_id uuid,
    personal_website_url text,
    blog_url text,
    tw_profile_url text,
    fb_profile_url text,
    job_sectors_of_interest text[] DEFAULT '{}'::text[] NOT NULL,
    employment_types_of_interest text[] DEFAULT '{}'::text[] NOT NULL,
    company_sizes_of_interest text[] DEFAULT '{}'::text[] NOT NULL,
    primary_areas_of_interest text[] DEFAULT '{}'::text[] NOT NULL,
    place_id text,
    place_details json DEFAULT '{}'::json NOT NULL,
    linkedin_pdf_id uuid,
    linkedin_pdf_import_status text,
    last_calculated_complete_percentage double precision,
    sat_max_score text DEFAULT '1600'::text NOT NULL,
    score_on_gre_verbal text,
    score_on_gre_quantitative text,
    score_on_gre_analytical text,
    short_answer_greatest_achievement text,
    short_answer_greatest_strength text,
    short_answer_professional_future text,
    short_answer_ideal_first_job text,
    primary_reason_for_applying text,
    do_not_create_relationships boolean DEFAULT false NOT NULL,
    internal_quality_score text,
    internal_employment_notes text,
    salary_range text,
    interested_in_joining_new_company text,
    locations_of_interest text[] DEFAULT '{none}'::text[] NOT NULL,
    short_answer_leadership_challenge text,
    short_answer_personal_passions text,
    consider_merit_based boolean,
    profile_feedback text,
    feedback_last_sent_at timestamp without time zone,
    location public.geography(Point,4326),
    github_profile_url text,
    long_term_goal text,
    last_confirmed_at_by_student timestamp without time zone,
    last_confirmed_at_by_internal timestamp without time zone,
    last_updated_at_by_student timestamp without time zone,
    last_confirmed_internally_by character varying,
    short_answer_scholarship character varying,
    consider_early_decision boolean,
    student_network_looking_for text[] DEFAULT '{}'::text[] NOT NULL
);


--
-- Name: career_profiles_skills_options; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.career_profiles_skills_options (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    career_profile_id uuid NOT NULL,
    skills_option_id uuid NOT NULL,
    "position" integer NOT NULL
);


--
-- Name: career_profiles_student_network_interests_options; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.career_profiles_student_network_interests_options (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    career_profile_id uuid NOT NULL,
    student_network_interests_option_id uuid NOT NULL,
    "position" integer NOT NULL
);


--
-- Name: education_experiences; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.education_experiences (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    graduation_year integer NOT NULL,
    degree text,
    major text NOT NULL,
    minor text,
    gpa text,
    career_profile_id uuid NOT NULL,
    educational_organization_option_id uuid NOT NULL,
    gpa_description text,
    degree_program boolean DEFAULT true NOT NULL,
    will_not_complete boolean
);


--
-- Name: educational_organization_options; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.educational_organization_options (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    text text NOT NULL,
    locale text DEFAULT 'en'::text NOT NULL,
    suggest boolean DEFAULT false,
    source_id text,
    image_url text
);


--
-- Name: hiring_relationships; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.hiring_relationships (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    hiring_manager_id uuid NOT NULL,
    candidate_id uuid NOT NULL,
    hiring_manager_status text DEFAULT 'pending'::text NOT NULL,
    candidate_status text DEFAULT 'hidden'::text NOT NULL,
    archived_after_accepted boolean DEFAULT false,
    hiring_manager_priority double precision DEFAULT 100 NOT NULL,
    hiring_manager_closed boolean,
    candidate_closed boolean,
    open_position_id uuid,
    hiring_manager_closed_info json DEFAULT '{}'::json NOT NULL,
    matched_at timestamp without time zone,
    conversation_id integer,
    matched_by_hiring_manager boolean,
    stripe_usage_record_info json
);


--
-- Name: professional_organization_options; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.professional_organization_options (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    text text NOT NULL,
    locale text DEFAULT 'en'::text NOT NULL,
    suggest boolean DEFAULT false,
    source_id text,
    image_url text
);


--
-- Name: skills_options; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.skills_options (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    text text NOT NULL,
    locale text DEFAULT 'en'::text NOT NULL,
    suggest boolean DEFAULT false
);


--
-- Name: student_network_interests_options; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.student_network_interests_options (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    text text NOT NULL,
    locale text DEFAULT 'en'::text NOT NULL,
    suggest boolean DEFAULT false
);


--
-- Name: work_experiences; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.work_experiences (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    job_title text NOT NULL,
    start_date date NOT NULL,
    end_date date,
    responsibilities text[] DEFAULT '{}'::text[] NOT NULL,
    career_profile_id uuid NOT NULL,
    professional_organization_option_id uuid NOT NULL,
    featured boolean DEFAULT false NOT NULL,
    role text DEFAULT 'other'::text,
    industry text DEFAULT 'other'::text,
    employment_type text DEFAULT 'full_time'::text NOT NULL
);


--
-- Name: career_profile_search_helpers; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.career_profile_search_helpers AS
 WITH aggregated_education_experience_data AS (
         SELECT career_profiles_1.id AS career_profile_id,
            string_agg(DISTINCT educational_organization_options.text, ' '::text) AS education_experience_school_names,
            ((min(education_experiences.graduation_year) || '/01/01'::text))::timestamp without time zone AS first_graduation_date,
            array_remove(array_agg(
                CASE
                    WHEN education_experiences.degree_program THEN ((education_experiences.graduation_year || '/01/01'::text))::timestamp without time zone
                    ELSE NULL::timestamp without time zone
                END ORDER BY education_experiences.graduation_year), NULL::timestamp without time zone) AS graduation_dates,
            (count(
                CASE
                    WHEN (education_experiences.degree_program AND (((date_part('month'::text, now()) < (6)::double precision) AND ((education_experiences.graduation_year)::double precision = date_part('year'::text, now()))) OR ((education_experiences.graduation_year)::double precision > date_part('year'::text, now())))) THEN true
                    ELSE NULL::boolean
                END) > 0) AS in_school
           FROM ((public.career_profiles career_profiles_1
             LEFT JOIN public.education_experiences ON ((career_profiles_1.id = education_experiences.career_profile_id)))
             LEFT JOIN public.educational_organization_options ON ((education_experiences.educational_organization_option_id = educational_organization_options.id)))
          WHERE (career_profiles_1.updated_at > career_profiles_1.created_at)
          GROUP BY career_profiles_1.id
        ), aggregated_hiring_relationship_data AS (
         SELECT career_profiles_1.id AS career_profile_id,
            count(accepted_hiring_relationships.id) AS accepted_by_hiring_managers_count
           FROM (public.career_profiles career_profiles_1
             LEFT JOIN public.hiring_relationships accepted_hiring_relationships ON (((accepted_hiring_relationships.candidate_id = career_profiles_1.user_id) AND (accepted_hiring_relationships.hiring_manager_status = 'accepted'::text))))
          WHERE (career_profiles_1.updated_at > career_profiles_1.created_at)
          GROUP BY career_profiles_1.id
        ), aggregated_work_experience_data AS (
         SELECT career_profiles_1.id AS career_profile_id,
            array_agg(DISTINCT work_experiences.role) AS work_experience_roles,
            array_remove(array_agg(
                CASE
                    WHEN (work_experiences.end_date IS NULL) THEN work_experiences.role
                    ELSE NULL::text
                END), NULL::text) AS current_work_experience_roles,
            array_agg(DISTINCT work_experiences.industry) AS work_experience_industries,
            array_remove(array_agg(
                CASE
                    WHEN (work_experiences.end_date IS NULL) THEN work_experiences.industry
                    ELSE NULL::text
                END), NULL::text) AS current_work_experience_industries,
            string_agg(DISTINCT professional_organization_options.text, ' '::text) AS work_experience_company_names,
            min(
                CASE
                    WHEN ((aggregated_education_experience_data.first_graduation_date IS NULL) OR (work_experiences.start_date > aggregated_education_experience_data.first_graduation_date)) THEN work_experiences.start_date
                    ELSE NULL::date
                END) AS earliest_work_start_date_after_first_graduation,
            ( SELECT max(_graduation_dates._graduation_dates) AS max
                   FROM unnest(aggregated_education_experience_data.graduation_dates) _graduation_dates(_graduation_dates)
                  WHERE (_graduation_dates._graduation_dates < min(
                        CASE
                            WHEN (work_experiences.start_date > aggregated_education_experience_data.graduation_dates[1]) THEN work_experiences.start_date
                            ELSE NULL::date
                        END))) AS last_graduation_before_first_work_experience,
            max(work_experiences.end_date) AS latest_work_end_date,
            (count(
                CASE
                    WHEN ((work_experiences.id IS NOT NULL) AND (work_experiences.end_date IS NULL)) THEN true
                    ELSE NULL::boolean
                END) > 0) AS has_current_work_experience
           FROM (((public.career_profiles career_profiles_1
             JOIN aggregated_education_experience_data ON ((career_profiles_1.id = aggregated_education_experience_data.career_profile_id)))
             LEFT JOIN public.work_experiences ON ((career_profiles_1.id = work_experiences.career_profile_id)))
             LEFT JOIN public.professional_organization_options ON ((work_experiences.professional_organization_option_id = professional_organization_options.id)))
          WHERE (career_profiles_1.updated_at > career_profiles_1.created_at)
          GROUP BY career_profiles_1.id, aggregated_education_experience_data.graduation_dates
        ), aggregated_skills_data AS (
         SELECT career_profiles_1.id AS career_profile_id,
            array_agg(DISTINCT skills_options.text) AS skills_texts
           FROM ((public.career_profiles career_profiles_1
             LEFT JOIN public.career_profiles_skills_options ON ((career_profiles_1.id = career_profiles_skills_options.career_profile_id)))
             LEFT JOIN public.skills_options skills_options ON ((career_profiles_skills_options.skills_option_id = skills_options.id)))
          WHERE (career_profiles_1.updated_at > career_profiles_1.created_at)
          GROUP BY career_profiles_1.id
        ), aggregated_student_network_interests_data AS (
         SELECT career_profiles_1.id AS career_profile_id,
            array_agg(DISTINCT student_network_interests_options.text) AS student_network_interests_texts
           FROM ((public.career_profiles career_profiles_1
             LEFT JOIN public.career_profiles_student_network_interests_options ON ((career_profiles_1.id = career_profiles_student_network_interests_options.career_profile_id)))
             LEFT JOIN public.student_network_interests_options student_network_interests_options ON ((career_profiles_student_network_interests_options.student_network_interests_option_id = student_network_interests_options.id)))
          WHERE (career_profiles_1.updated_at > career_profiles_1.created_at)
          GROUP BY career_profiles_1.id
        ), career_profiles_with_aggregated_data AS (
         SELECT aggregated_work_experience_data.career_profile_id,
            aggregated_work_experience_data.work_experience_roles,
            aggregated_work_experience_data.work_experience_industries,
            aggregated_work_experience_data.work_experience_company_names,
            aggregated_education_experience_data.education_experience_school_names,
            aggregated_work_experience_data.current_work_experience_roles,
            aggregated_work_experience_data.current_work_experience_industries,
            aggregated_work_experience_data.earliest_work_start_date_after_first_graduation,
            aggregated_work_experience_data.latest_work_end_date,
            aggregated_work_experience_data.has_current_work_experience,
            aggregated_education_experience_data.first_graduation_date,
            aggregated_work_experience_data.last_graduation_before_first_work_experience,
            aggregated_education_experience_data.in_school,
            aggregated_skills_data.skills_texts,
            aggregated_student_network_interests_data.student_network_interests_texts,
            aggregated_hiring_relationship_data.accepted_by_hiring_managers_count
           FROM ((((aggregated_work_experience_data
             JOIN aggregated_education_experience_data ON ((aggregated_education_experience_data.career_profile_id = aggregated_work_experience_data.career_profile_id)))
             JOIN aggregated_skills_data ON ((aggregated_skills_data.career_profile_id = aggregated_work_experience_data.career_profile_id)))
             JOIN aggregated_student_network_interests_data ON ((aggregated_student_network_interests_data.career_profile_id = aggregated_work_experience_data.career_profile_id)))
             JOIN aggregated_hiring_relationship_data ON ((aggregated_hiring_relationship_data.career_profile_id = aggregated_work_experience_data.career_profile_id)))
        ), with_experience_start_and_end_dates AS (
         SELECT career_profiles_with_aggregated_data.career_profile_id,
            career_profiles_with_aggregated_data.work_experience_roles,
            career_profiles_with_aggregated_data.work_experience_industries,
            career_profiles_with_aggregated_data.work_experience_company_names,
            career_profiles_with_aggregated_data.education_experience_school_names,
            career_profiles_with_aggregated_data.current_work_experience_roles,
            career_profiles_with_aggregated_data.current_work_experience_industries,
            career_profiles_with_aggregated_data.earliest_work_start_date_after_first_graduation,
            career_profiles_with_aggregated_data.latest_work_end_date,
            career_profiles_with_aggregated_data.has_current_work_experience,
            career_profiles_with_aggregated_data.first_graduation_date,
            career_profiles_with_aggregated_data.last_graduation_before_first_work_experience,
            career_profiles_with_aggregated_data.in_school,
            career_profiles_with_aggregated_data.skills_texts,
            career_profiles_with_aggregated_data.student_network_interests_texts,
            career_profiles_with_aggregated_data.accepted_by_hiring_managers_count,
            LEAST((career_profiles_with_aggregated_data.earliest_work_start_date_after_first_graduation)::timestamp without time zone, career_profiles_with_aggregated_data.last_graduation_before_first_work_experience) AS earliest_experience_start,
                CASE
                    WHEN career_profiles_with_aggregated_data.has_current_work_experience THEN now()
                    ELSE (career_profiles_with_aggregated_data.latest_work_end_date)::timestamp with time zone
                END AS latest_experience_end
           FROM career_profiles_with_aggregated_data
        )
 SELECT with_experience_start_and_end_dates.career_profile_id,
    with_experience_start_and_end_dates.work_experience_roles,
    with_experience_start_and_end_dates.work_experience_industries,
    with_experience_start_and_end_dates.work_experience_company_names,
    with_experience_start_and_end_dates.education_experience_school_names,
    with_experience_start_and_end_dates.current_work_experience_roles,
    with_experience_start_and_end_dates.current_work_experience_industries,
    with_experience_start_and_end_dates.earliest_work_start_date_after_first_graduation,
    with_experience_start_and_end_dates.latest_work_end_date,
    with_experience_start_and_end_dates.has_current_work_experience,
    with_experience_start_and_end_dates.first_graduation_date,
    with_experience_start_and_end_dates.last_graduation_before_first_work_experience,
    with_experience_start_and_end_dates.in_school,
    with_experience_start_and_end_dates.skills_texts,
    with_experience_start_and_end_dates.student_network_interests_texts,
    with_experience_start_and_end_dates.accepted_by_hiring_managers_count,
    with_experience_start_and_end_dates.earliest_experience_start,
    with_experience_start_and_end_dates.latest_experience_end,
    ((((((1000000 *
        CASE
            WHEN ((career_profiles.place_details #>> '{country,short}'::text[]) = ANY (ARRAY['US'::text, 'CA'::text])) THEN 1
            ELSE 0
        END) + (100000 *
        CASE
            WHEN (career_profiles.interested_in_joining_new_company = 'very_interested'::text) THEN 9
            WHEN (career_profiles.interested_in_joining_new_company = 'interested'::text) THEN 8
            WHEN (career_profiles.interested_in_joining_new_company = 'neutral'::text) THEN 7
            ELSE 6
        END)) + (10000 *
        CASE
            WHEN (career_profiles.do_not_create_relationships = false) THEN 1
            ELSE 0
        END)) +
        CASE
            WHEN (with_experience_start_and_end_dates.accepted_by_hiring_managers_count IS NOT NULL) THEN with_experience_start_and_end_dates.accepted_by_hiring_managers_count
            ELSE (0)::bigint
        END))::double precision + (date_part('epoch'::text, career_profiles.created_at) / ('100000000000'::bigint)::double precision)) AS default_prioritization,
        CASE
            WHEN ((with_experience_start_and_end_dates.earliest_experience_start IS NULL) OR (with_experience_start_and_end_dates.latest_experience_end IS NULL) OR (with_experience_start_and_end_dates.latest_experience_end < with_experience_start_and_end_dates.earliest_experience_start)) THEN (0)::double precision
            ELSE (date_part('epoch'::text, (with_experience_start_and_end_dates.latest_experience_end - (with_experience_start_and_end_dates.earliest_experience_start)::timestamp with time zone)) / date_part('epoch'::text, '1 year'::interval))
        END AS years_of_experience
   FROM (with_experience_start_and_end_dates
     JOIN public.career_profiles ON ((career_profiles.id = with_experience_start_and_end_dates.career_profile_id)))
  WITH NO DATA;


--
-- Name: career_profile_searches; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.career_profile_searches (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_id uuid NOT NULL,
    locations text[] DEFAULT '{}'::text[] NOT NULL,
    only_local boolean DEFAULT false NOT NULL,
    primary_areas_of_interest text[] DEFAULT '{}'::text[] NOT NULL,
    years_experience text[] DEFAULT '{}'::text[] NOT NULL,
    last_search_at timestamp without time zone,
    search_count integer,
    keyword_search text,
    preferred_primary_areas_of_interest text[] DEFAULT '{}'::text[] NOT NULL,
    industries text[] DEFAULT '{}'::text[] NOT NULL,
    skills text[] DEFAULT '{}'::text[] NOT NULL,
    employment_types_of_interest text[] DEFAULT '{}'::text[] NOT NULL,
    school_name text,
    in_school boolean,
    company_name text,
    levels_of_interest text[] DEFAULT '{}'::text[] NOT NULL
);


--
-- Name: career_profiles_awards_and_interests_options; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.career_profiles_awards_and_interests_options (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    career_profile_id uuid NOT NULL,
    awards_and_interests_option_id uuid NOT NULL,
    "position" integer NOT NULL
);


--
-- Name: career_profiles_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.career_profiles_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    user_id uuid,
    score_on_gmat text,
    score_on_sat text,
    score_on_act text,
    short_answers json,
    willing_to_relocate boolean,
    open_to_remote_work boolean,
    authorized_to_work_in_us text,
    top_mba_subjects text[],
    top_personal_descriptors text[],
    top_motivations text[],
    top_workplace_strengths text[],
    bio text,
    career_goals text,
    personal_fact text,
    preferred_company_culture_descriptors text[],
    li_profile_url text,
    resume_id uuid,
    personal_website_url text,
    blog_url text,
    tw_profile_url text,
    fb_profile_url text,
    job_sectors_of_interest text[],
    employment_types_of_interest text[],
    company_sizes_of_interest text[],
    primary_areas_of_interest text[],
    place_id text,
    place_details json,
    linkedin_pdf_id uuid,
    linkedin_pdf_import_status text,
    last_calculated_complete_percentage double precision,
    sat_max_score text,
    score_on_gre_verbal text,
    score_on_gre_quantitative text,
    score_on_gre_analytical text,
    short_answer_greatest_achievement text,
    short_answer_greatest_strength text,
    short_answer_professional_future text,
    short_answer_ideal_first_job text,
    primary_reason_for_applying text,
    do_not_create_relationships boolean,
    internal_quality_score text,
    internal_employment_notes text,
    salary_range text,
    interested_in_joining_new_company text,
    locations_of_interest text[],
    short_answer_leadership_challenge text,
    short_answer_personal_passions text,
    consider_merit_based boolean,
    profile_feedback text,
    feedback_last_sent_at timestamp without time zone,
    location public.geography(Point,4326),
    github_profile_url text,
    long_term_goal text,
    last_confirmed_at_by_student timestamp without time zone,
    last_confirmed_at_by_internal timestamp without time zone,
    last_updated_at_by_student timestamp without time zone,
    last_confirmed_internally_by character varying,
    short_answer_scholarship character varying,
    consider_early_decision boolean,
    student_network_looking_for text[]
);


--
-- Name: cohort_applications; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cohort_applications (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    applied_at timestamp without time zone NOT NULL,
    cohort_id uuid NOT NULL,
    user_id uuid NOT NULL,
    status text DEFAULT 'pending'::text NOT NULL,
    accepted_at timestamp without time zone,
    graduation_status text DEFAULT 'pending'::text,
    expelled_at timestamp without time zone,
    disable_exam_locking boolean DEFAULT false,
    skip_period_expulsion boolean DEFAULT false,
    completed_at timestamp without time zone,
    graduated_at timestamp without time zone,
    final_score double precision,
    total_num_required_stripe_payments integer,
    retargeted_from_program_type text,
    registered boolean,
    admissions_decision text,
    should_invite_to_reapply boolean,
    shareable_with_classmates boolean,
    notes text,
    scholarship_level json,
    num_charged_payments integer,
    num_refunded_payments integer,
    airtable_base_key character varying,
    airtable_record_id character varying,
    stripe_plan_id text,
    pre_accepted_at timestamp without time zone,
    locked_due_to_past_due_payment boolean DEFAULT false NOT NULL
);


--
-- Name: cohort_applications_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cohort_applications_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    applied_at timestamp without time zone NOT NULL,
    cohort_id uuid NOT NULL,
    user_id uuid NOT NULL,
    status text NOT NULL,
    accepted_at timestamp without time zone,
    graduation_status text,
    expelled_at timestamp without time zone,
    disable_exam_locking boolean,
    skip_period_expulsion boolean,
    completed_at timestamp without time zone,
    graduated_at timestamp without time zone,
    final_score double precision,
    total_num_required_stripe_payments integer,
    retargeted_from_program_type text,
    registered boolean,
    admissions_decision text,
    should_invite_to_reapply boolean,
    shareable_with_classmates boolean,
    notes text,
    scholarship_level json,
    num_charged_payments integer,
    num_refunded_payments integer,
    airtable_base_key character varying,
    airtable_record_id character varying,
    stripe_plan_id text,
    pre_accepted_at timestamp without time zone,
    locked_due_to_past_due_payment boolean
);


--
-- Name: cohorts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cohorts (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    start_date timestamp without time zone NOT NULL,
    playlist_pack_ids uuid[] DEFAULT '{}'::uuid[] NOT NULL,
    end_date timestamp without time zone NOT NULL,
    application_deadline timestamp without time zone,
    was_published boolean DEFAULT false,
    name text NOT NULL,
    required_playlist_pack_ids uuid[] DEFAULT '{}'::uuid[],
    specialization_playlist_pack_ids uuid[] DEFAULT '{}'::uuid[],
    periods json[] DEFAULT '{}'::json[] NOT NULL,
    program_type text DEFAULT 'mba'::text NOT NULL,
    num_required_specializations integer DEFAULT 0 NOT NULL,
    title text NOT NULL,
    description text,
    admission_rounds json[] DEFAULT '{}'::json[],
    registration_deadline_days_offset integer DEFAULT 0,
    external_schedule_url text,
    program_guide_url text,
    enrollment_deadline_days_offset integer DEFAULT 0 NOT NULL,
    stripe_plans json[],
    scholarship_levels json[],
    graduation_date_days_offset integer,
    internal_notes text,
    slack_token character varying,
    slack_url character varying,
    diploma_generation_days_offset integer,
    project_submission_email character varying,
    project_documents json[]
);


--
-- Name: roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.roles (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name character varying,
    resource_type character varying,
    resource_id uuid,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    email character varying DEFAULT ''::character varying,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying,
    last_sign_in_ip character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    failed_attempts integer DEFAULT 0,
    unlock_token character varying,
    locked_at timestamp without time zone,
    sign_up_code character varying,
    reset_password_redirect_url character varying,
    provider character varying,
    uid character varying DEFAULT ''::character varying NOT NULL,
    tokens json,
    has_seen_welcome boolean DEFAULT false,
    notify_email_newsletter boolean DEFAULT true NOT NULL,
    school text,
    provider_user_info json,
    free_trial_started boolean DEFAULT false,
    confirmed_profile_info boolean DEFAULT true,
    pref_decimal_delim character varying DEFAULT '.'::character varying NOT NULL,
    optimizely_segments json DEFAULT '{}'::json,
    optimizely_referer text,
    optimizely_buckets json DEFAULT '{}'::json,
    pref_locale character varying DEFAULT 'en'::character varying NOT NULL,
    active_playlist_locale_pack_id uuid,
    avatar_url text,
    avatar_provider text,
    mba_content_lockable boolean DEFAULT false,
    additional_details json,
    job_title text,
    phone text,
    country text,
    has_seen_accepted boolean DEFAULT false,
    name text,
    nickname text,
    phone_extension text,
    can_edit_career_profile boolean DEFAULT false,
    professional_organization_option_id uuid,
    pref_show_photos_names boolean DEFAULT true NOT NULL,
    identity_verified boolean DEFAULT false,
    sex text,
    ethnicity text,
    race text[] DEFAULT '{}'::text[] NOT NULL,
    how_did_you_hear_about_us text,
    anything_else_to_tell_us text,
    birthdate date,
    address_line_1 text,
    address_line_2 text,
    city text,
    state text,
    zip text,
    pref_keyboard_shortcuts boolean DEFAULT false NOT NULL,
    last_seen_at timestamp without time zone,
    fallback_program_type character varying,
    program_type_confirmed boolean DEFAULT false,
    invite_code character varying,
    notify_hiring_updates boolean DEFAULT true,
    notify_hiring_spotlights boolean DEFAULT true,
    notify_hiring_content boolean DEFAULT true,
    notify_hiring_candidate_activity boolean DEFAULT true,
    hiring_team_id uuid,
    referred_by_id uuid,
    has_seen_featured_positions_page boolean DEFAULT false,
    pref_one_click_reach_out boolean DEFAULT false NOT NULL,
    has_seen_careers_welcome boolean DEFAULT false,
    can_purchase_paid_certs boolean DEFAULT false,
    has_seen_hide_candidate_confirm boolean DEFAULT false,
    pref_positions_candidate_list boolean DEFAULT true NOT NULL,
    skip_apply boolean DEFAULT false NOT NULL,
    has_seen_student_network boolean DEFAULT false NOT NULL,
    pref_student_network_privacy text DEFAULT 'full'::text NOT NULL,
    notify_hiring_spotlights_business boolean DEFAULT true NOT NULL,
    notify_hiring_spotlights_technical boolean DEFAULT true NOT NULL,
    has_drafted_open_position boolean DEFAULT false NOT NULL,
    deactivated boolean DEFAULT false NOT NULL,
    notify_candidate_positions_technical boolean DEFAULT true NOT NULL,
    notify_candidate_positions_business boolean DEFAULT true NOT NULL,
    avatar_id uuid,
    has_seen_first_position_review_modal boolean DEFAULT false
);


--
-- Name: users_roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users_roles (
    user_id uuid,
    role_id uuid
);


--
-- Name: external_users; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.external_users AS
 SELECT users.id
   FROM public.users
  WHERE (EXISTS ( SELECT 1
           FROM public.users_roles
          WHERE ((users_roles.user_id = users.id) AND (EXISTS ( SELECT 1
                   FROM public.roles
                  WHERE ((roles.id = users_roles.role_id) AND ((roles.name)::text = 'learner'::text) AND ((users.email)::text !~~ '%pedago.com%'::text) AND ((users.email)::text !~~ '%smart.ly%'::text) AND ((users.email)::text !~~ '%workaround.com%'::text)))))));


--
-- Name: cohort_applications_plus; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.cohort_applications_plus AS
 WITH cohort_states AS (
         SELECT cohort_applications.id,
            cohort_applications.user_id,
            cohort_applications.cohort_id,
            cohort_applications.status AS current_status,
            cohort_applications.graduation_status,
            cohort_applications.final_score,
            cohorts.name AS cohort_name,
            cohort_applications.applied_at,
            (max(
                CASE
                    WHEN (aps.status = 'accepted'::text) THEN 1
                    ELSE 0
                END) = 1) AS was_accepted,
            min(
                CASE
                    WHEN (aps.status = 'accepted'::text) THEN aps.version_created_at
                    ELSE NULL::timestamp without time zone
                END) AS accepted_at,
            cohorts.start_date AS cohort_start_date
           FROM (((public.cohort_applications
             JOIN public.cohorts ON ((cohort_applications.cohort_id = cohorts.id)))
             JOIN public.cohort_applications_versions aps ON ((cohort_applications.id = aps.id)))
             JOIN public.external_users ON ((external_users.id = aps.user_id)))
          GROUP BY cohort_applications.id, cohort_applications.user_id, cohort_applications.cohort_id, cohorts.name, cohorts.start_date, cohort_applications.status, cohort_applications.graduation_status, cohort_applications.applied_at, cohort_applications.final_score
        ), cohort_states_2 AS (
         SELECT cohort_states.id,
            cohort_states.user_id,
            cohort_states.cohort_id,
            cohort_states.cohort_name,
            cohort_states.current_status,
            cohort_states.graduation_status,
            cohort_states.applied_at,
            cohort_states.accepted_at,
            cohort_states.was_accepted,
            cohort_states.final_score,
            string_agg(
                CASE
                    WHEN (later_cohort_states.cohort_name IS NOT NULL) THEN ((later_cohort_states.cohort_name || ':'::text) || later_cohort_states.current_status)
                    ELSE NULL::text
                END, ','::text) AS applied_to_later,
            string_agg(
                CASE
                    WHEN (previous_cohort_states.cohort_name IS NOT NULL) THEN ((previous_cohort_states.cohort_name || ':'::text) || previous_cohort_states.current_status)
                    ELSE NULL::text
                END, ','::text) AS applied_to_previous
           FROM ((cohort_states
             LEFT JOIN cohort_states later_cohort_states ON (((cohort_states.user_id = later_cohort_states.user_id) AND (later_cohort_states.cohort_start_date > cohort_states.cohort_start_date))))
             LEFT JOIN cohort_states previous_cohort_states ON (((cohort_states.user_id = previous_cohort_states.user_id) AND (previous_cohort_states.cohort_start_date < cohort_states.cohort_start_date))))
          GROUP BY cohort_states.id, cohort_states.user_id, cohort_states.cohort_id, cohort_states.cohort_name, cohort_states.current_status, cohort_states.applied_at, cohort_states.accepted_at, cohort_states.final_score, cohort_states.was_accepted, cohort_states.graduation_status
        )
 SELECT cohort_states_2.id,
    cohort_states_2.user_id,
    cohort_states_2.cohort_id,
    cohort_states_2.cohort_name,
    cohort_states_2.current_status,
    cohort_states_2.graduation_status,
    cohort_states_2.applied_at,
    cohort_states_2.accepted_at,
    cohort_states_2.was_accepted,
    cohort_states_2.final_score,
    cohort_states_2.applied_to_later,
    cohort_states_2.applied_to_previous
   FROM cohort_states_2
  WITH NO DATA;


--
-- Name: cohort_promotions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cohort_promotions (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    cohort_id uuid NOT NULL
);


--
-- Name: cohorts_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cohorts_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    start_date timestamp without time zone NOT NULL,
    playlist_pack_ids uuid[] NOT NULL,
    end_date timestamp without time zone,
    application_deadline timestamp without time zone,
    was_published boolean DEFAULT false,
    name text,
    required_playlist_pack_ids uuid[] DEFAULT '{}'::uuid[],
    specialization_playlist_pack_ids uuid[] DEFAULT '{}'::uuid[],
    periods json[],
    program_type text,
    num_required_specializations integer DEFAULT 0 NOT NULL,
    title text,
    description text,
    admission_rounds json[],
    registration_deadline_days_offset integer,
    external_schedule_url text,
    program_guide_url text,
    enrollment_deadline_days_offset integer,
    stripe_plans json[],
    scholarship_levels json[],
    graduation_date_days_offset integer,
    internal_notes text,
    slack_token character varying,
    slack_url character varying,
    diploma_generation_days_offset integer,
    project_submission_email character varying,
    project_documents json[]
);


--
-- Name: content_publishers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.content_publishers (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    published_at timestamp without time zone,
    object_updated_at timestamp without time zone,
    lesson_version_id uuid,
    lesson_stream_version_id uuid,
    playlist_version_id uuid,
    cohort_version_id uuid
);


--
-- Name: lesson_streams; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lesson_streams (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    title character varying,
    description text,
    author_id uuid,
    image_id uuid,
    chapters json DEFAULT '[]'::json,
    modified_at timestamp without time zone,
    pinned boolean DEFAULT false NOT NULL,
    pinned_title character varying,
    pinned_description text,
    was_published boolean DEFAULT false NOT NULL,
    last_editor_id uuid,
    entity_metadata_id uuid,
    credits text,
    what_you_will_learn character varying[] DEFAULT '{}'::character varying[],
    resource_downloads json,
    resource_links json,
    recommended_stream_ids uuid[] DEFAULT '{}'::uuid[],
    related_stream_ids uuid[] DEFAULT '{}'::uuid[],
    coming_soon boolean DEFAULT false NOT NULL,
    just_added boolean DEFAULT false NOT NULL,
    summaries json,
    beta boolean DEFAULT false NOT NULL,
    just_updated boolean DEFAULT false NOT NULL,
    locale_pack_id uuid,
    locale text DEFAULT 'en'::text NOT NULL,
    duplicated_from_id uuid,
    duplicated_to_id uuid,
    unlocked_for_mba_users boolean DEFAULT false,
    exam boolean DEFAULT false NOT NULL,
    time_limit_hours double precision,
    exam_open_time timestamp without time zone,
    exam_close_time timestamp without time zone,
    tag text
);


--
-- Name: lesson_streams_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lesson_streams_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    title character varying,
    description text,
    author_id uuid,
    image_id uuid,
    chapters json,
    modified_at timestamp without time zone,
    pinned boolean,
    pinned_title character varying,
    pinned_description text,
    was_published boolean,
    last_editor_id uuid,
    entity_metadata_id uuid,
    credits text,
    what_you_will_learn character varying[],
    resource_downloads json,
    resource_links json,
    recommended_stream_ids uuid[] DEFAULT '{}'::uuid[],
    related_stream_ids uuid[] DEFAULT '{}'::uuid[],
    coming_soon boolean,
    just_added boolean,
    summaries json,
    beta boolean,
    just_updated boolean,
    locale_pack_id uuid,
    locale text,
    duplicated_from_id uuid,
    duplicated_to_id uuid,
    unlocked_for_mba_users boolean DEFAULT false,
    exam boolean,
    time_limit_hours double precision,
    exam_open_time timestamp without time zone,
    exam_close_time timestamp without time zone,
    tag text
);


--
-- Name: lessons; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lessons (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    content_json json,
    author_id uuid,
    title character varying,
    description text[] DEFAULT '{}'::text[],
    modified_at timestamp without time zone,
    archived boolean DEFAULT false NOT NULL,
    pinned boolean DEFAULT false NOT NULL,
    pinned_title character varying,
    pinned_description text,
    was_published boolean DEFAULT false NOT NULL,
    last_editor_id uuid,
    seo_title character varying,
    seo_description character varying,
    seo_canonical_url text,
    seo_image_id uuid,
    lesson_type character varying,
    frame_count integer,
    entity_metadata_id uuid,
    key_terms character varying[] DEFAULT '{}'::character varying[] NOT NULL,
    assessment boolean DEFAULT false,
    unrestricted boolean DEFAULT false,
    tag text,
    locale_pack_id uuid,
    locale text DEFAULT 'en'::text NOT NULL,
    duplicated_from_id uuid,
    duplicated_to_id uuid,
    test boolean DEFAULT false NOT NULL
);


--
-- Name: lessons_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lessons_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    content_json json,
    author_id uuid,
    title character varying(255),
    description text[],
    modified_at timestamp without time zone,
    archived boolean,
    pinned boolean,
    pinned_title character varying(255),
    pinned_description text,
    was_published boolean,
    last_editor_id uuid,
    seo_title character varying,
    seo_description character varying,
    seo_canonical_url text,
    seo_image_id uuid,
    lesson_type character varying,
    frame_count integer,
    entity_metadata_id uuid,
    key_terms character varying[],
    assessment boolean,
    unrestricted boolean DEFAULT false,
    tag text,
    locale_pack_id uuid,
    locale text,
    duplicated_from_id uuid,
    duplicated_to_id uuid,
    test boolean
);


--
-- Name: playlists; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.playlists (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    title text NOT NULL,
    description text,
    stream_entries json[] DEFAULT '{}'::json[],
    image_id uuid,
    entity_metadata_id uuid,
    author_id uuid,
    last_editor_id uuid,
    modified_at timestamp without time zone,
    was_published boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    locale_pack_id uuid,
    locale text DEFAULT 'en'::text NOT NULL,
    duplicated_from_id uuid,
    duplicated_to_id uuid,
    tag text
);


--
-- Name: playlists_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.playlists_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    title text NOT NULL,
    description text,
    stream_entries json[],
    image_id uuid,
    entity_metadata_id uuid,
    author_id uuid,
    last_editor_id uuid,
    modified_at timestamp without time zone,
    was_published boolean NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    locale_pack_id uuid,
    locale text,
    duplicated_from_id uuid,
    duplicated_to_id uuid,
    tag text
);


--
-- Name: published_cohort_admission_rounds; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.published_cohort_admission_rounds AS
 WITH admission_round_definitions AS (
         SELECT published_cohorts.id AS cohort_id,
            published_cohorts.name,
            published_cohorts.program_type,
            cohorts_versions.start_date AS cohort_start_date,
            admission_rounds.definition,
            admission_rounds.index
           FROM ((public.cohorts_versions published_cohorts
             JOIN public.content_publishers ON ((content_publishers.cohort_version_id = published_cohorts.version_id)))
             JOIN public.cohorts_versions ON ((cohorts_versions.version_id = published_cohorts.version_id))),
            LATERAL unnest(cohorts_versions.admission_rounds) WITH ORDINALITY admission_rounds(definition, index)
        ), with_admission_round_details AS (
         SELECT admission_round_definitions.cohort_id,
            admission_round_definitions.name,
            admission_round_definitions.index,
            admission_round_definitions.program_type,
            public.add_dst_aware_offset(admission_round_definitions.cohort_start_date, (((admission_round_definitions.definition ->> 'application_deadline_days_offset'::text))::integer)::numeric, 'days'::text) AS application_deadline,
            public.add_dst_aware_offset(admission_round_definitions.cohort_start_date, (((admission_round_definitions.definition ->> 'decision_date_days_offset'::text))::integer)::numeric, 'days'::text) AS decision_date,
            admission_round_definitions.cohort_start_date
           FROM admission_round_definitions
        ), program_types AS (
         SELECT DISTINCT with_admission_round_details.program_type
           FROM with_admission_round_details
        ), promoted_admission_rounds AS (
         SELECT program_types.program_type,
            ( SELECT row_to_json(prepared_groups.*) AS row_to_json
                   FROM ( SELECT with_admission_round_details.cohort_id,
                            with_admission_round_details.index
                           FROM with_admission_round_details
                          WHERE ((with_admission_round_details.program_type = program_types.program_type) AND (with_admission_round_details.cohort_start_date > now()) AND (with_admission_round_details.application_deadline > now()))
                          ORDER BY with_admission_round_details.cohort_start_date, with_admission_round_details.application_deadline
                         LIMIT 1) prepared_groups) AS admission_round
           FROM program_types
        ), with_promoted AS (
         SELECT with_admission_round_details.cohort_id,
            with_admission_round_details.name,
            with_admission_round_details.index,
            with_admission_round_details.program_type,
            with_admission_round_details.application_deadline,
            with_admission_round_details.decision_date,
            with_admission_round_details.cohort_start_date,
            (promoted_admission_rounds.program_type IS NOT NULL) AS promoted
           FROM (with_admission_round_details
             LEFT JOIN promoted_admission_rounds ON (((((promoted_admission_rounds.admission_round ->> 'cohort_id'::text))::uuid = with_admission_round_details.cohort_id) AND (((promoted_admission_rounds.admission_round ->> 'index'::text))::integer = with_admission_round_details.index))))
        )
 SELECT with_promoted.cohort_id,
    with_promoted.name,
    with_promoted.index,
    with_promoted.program_type,
    with_promoted.application_deadline,
    with_promoted.decision_date,
    with_promoted.cohort_start_date,
    with_promoted.promoted
   FROM with_promoted
  ORDER BY with_promoted.program_type, with_promoted.cohort_start_date, with_promoted.application_deadline;


--
-- Name: published_cohorts; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.published_cohorts AS
 SELECT cohorts_versions.id,
        CASE
            WHEN (cohorts_versions.program_type = ANY (ARRAY['emba'::text, 'mba'::text])) THEN (promoted_admission_rounds.cohort_id IS NOT NULL)
            ELSE (cohort_promotions.cohort_id IS NOT NULL)
        END AS promoted,
    cohorts_versions.name,
    cohorts_versions.program_type,
    cohorts_versions.required_playlist_pack_ids,
    cohorts_versions.specialization_playlist_pack_ids,
    cohorts_versions.start_date,
    cohorts_versions.end_date,
        CASE
            WHEN (cohorts_versions.enrollment_deadline_days_offset IS NULL) THEN cohorts_versions.start_date
            ELSE public.add_dst_aware_offset(cohorts_versions.start_date, (cohorts_versions.enrollment_deadline_days_offset)::numeric, 'days'::text)
        END AS enrollment_deadline,
    cohorts_versions.version_id
   FROM (((public.cohorts_versions
     JOIN public.content_publishers ON ((content_publishers.cohort_version_id = cohorts_versions.version_id)))
     LEFT JOIN public.cohort_promotions ON ((cohort_promotions.cohort_id = cohorts_versions.id)))
     LEFT JOIN public.published_cohort_admission_rounds promoted_admission_rounds ON (((promoted_admission_rounds.cohort_id = cohorts_versions.id) AND (promoted_admission_rounds.promoted = true))));


--
-- Name: published_lessons; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.published_lessons AS
 SELECT lessons.id,
    lessons.locale_pack_id,
    lessons_versions.title,
    lessons_versions.version_id,
    lessons_versions.locale,
    lessons_versions.assessment,
    lessons_versions.test
   FROM ((public.lessons
     JOIN public.lessons_versions ON ((lessons.id = lessons_versions.id)))
     JOIN public.content_publishers ON ((content_publishers.lesson_version_id = lessons_versions.version_id)))
  WITH NO DATA;


--
-- Name: published_lesson_locale_packs; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.published_lesson_locale_packs AS
 SELECT published_lessons.locale_pack_id,
    lessons.title,
    string_agg(published_lessons.locale, ','::text) AS locales,
    (max(
        CASE
            WHEN published_lessons.test THEN 1
            ELSE 0
        END) = 1) AS test,
    (max(
        CASE
            WHEN published_lessons.assessment THEN 1
            ELSE 0
        END) = 1) AS assessment
   FROM (public.published_lessons
     JOIN public.lessons ON (((lessons.locale_pack_id = published_lessons.locale_pack_id) AND (lessons.locale = 'en'::text))))
  GROUP BY published_lessons.locale_pack_id, lessons.title
  WITH NO DATA;


--
-- Name: published_streams; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.published_streams AS
 SELECT lesson_streams.id,
    lesson_streams.locale_pack_id,
    lesson_streams_versions.title,
    lesson_streams_versions.version_id,
    lesson_streams_versions.chapters,
    lesson_streams_versions.locale,
    lesson_streams_versions.exam
   FROM ((public.lesson_streams
     JOIN public.lesson_streams_versions ON ((lesson_streams.id = lesson_streams_versions.id)))
     JOIN public.content_publishers ON ((content_publishers.lesson_stream_version_id = lesson_streams_versions.version_id)))
  WITH NO DATA;


--
-- Name: published_stream_lessons; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.published_stream_lessons AS
 WITH stream_lessons_1 AS (
         SELECT published_streams.id,
            published_streams.locale_pack_id,
            published_streams.title,
            published_streams.version_id,
            published_streams.chapters,
            published_streams.locale,
            published_streams.exam,
            (btrim((json_array_elements(((json_array_elements(published_streams.chapters) ->> 'lesson_ids'::text))::json))::text, '"'::text))::uuid AS lesson_id
           FROM public.published_streams
        ), stream_lessons_2 AS (
         SELECT (lessons_versions.id || (stream_lessons_1.id)::text) AS id,
            COALESCE(lessons_versions.test, false) AS test,
            COALESCE(lessons_versions.assessment, false) AS assessment,
            stream_lessons_1.title AS stream_title,
            stream_lessons_1.locale AS stream_locale,
            lessons_versions.title AS lesson_title,
            stream_lessons_1.locale_pack_id AS stream_locale_pack_id,
            stream_lessons_1.version_id AS stream_version_id,
            stream_lessons_1.id AS stream_id,
            published_lessons.locale_pack_id AS lesson_locale_pack_id,
            lessons_versions.version_id AS lesson_version_id,
            lessons_versions.locale AS lesson_locale,
            lessons_versions.id AS lesson_id
           FROM ((stream_lessons_1
             JOIN public.published_lessons ON ((stream_lessons_1.lesson_id = published_lessons.id)))
             JOIN public.lessons_versions ON ((published_lessons.version_id = lessons_versions.version_id)))
        )
 SELECT stream_lessons_2.id,
    stream_lessons_2.test,
    stream_lessons_2.assessment,
    stream_lessons_2.stream_title,
    stream_lessons_2.stream_locale,
    stream_lessons_2.lesson_title,
    stream_lessons_2.stream_locale_pack_id,
    stream_lessons_2.stream_version_id,
    stream_lessons_2.stream_id,
    stream_lessons_2.lesson_locale_pack_id,
    stream_lessons_2.lesson_version_id,
    stream_lessons_2.lesson_locale,
    stream_lessons_2.lesson_id
   FROM stream_lessons_2
  WITH NO DATA;


--
-- Name: published_stream_locale_packs; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.published_stream_locale_packs AS
 SELECT published_streams.locale_pack_id,
    lesson_streams.title,
    string_agg(published_streams.locale, ','::text) AS locales,
    (max(
        CASE
            WHEN published_streams.exam THEN 1
            ELSE 0
        END) = 1) AS exam
   FROM (public.published_streams
     JOIN public.lesson_streams ON (((lesson_streams.locale_pack_id = published_streams.locale_pack_id) AND (lesson_streams.locale = 'en'::text))))
  GROUP BY published_streams.locale_pack_id, lesson_streams.title
  WITH NO DATA;


--
-- Name: published_stream_lesson_locale_packs; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.published_stream_lesson_locale_packs AS
 SELECT DISTINCT published_stream_locale_packs.title AS stream_title,
    published_lesson_locale_packs.title AS lesson_title,
    published_lesson_locale_packs.test,
    published_lesson_locale_packs.assessment,
    published_stream_locale_packs.locale_pack_id AS stream_locale_pack_id,
    published_lesson_locale_packs.locale_pack_id AS lesson_locale_pack_id,
    published_stream_locale_packs.locales AS stream_locales,
    published_lesson_locale_packs.locales AS lesson_locales
   FROM ((public.published_stream_lessons
     JOIN public.published_stream_locale_packs ON ((published_stream_locale_packs.locale_pack_id = published_stream_lessons.stream_locale_pack_id)))
     JOIN public.published_lesson_locale_packs ON ((published_lesson_locale_packs.locale_pack_id = published_stream_lessons.lesson_locale_pack_id)))
  WITH NO DATA;


--
-- Name: published_cohort_periods; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.published_cohort_periods AS
 WITH period_definitions AS (
         SELECT published_cohorts.id AS cohort_id,
            published_cohorts.name,
            cohorts_versions.start_date AS cohort_start_date,
            periods.definition,
            periods.index
           FROM (public.published_cohorts
             JOIN public.cohorts_versions ON ((cohorts_versions.version_id = published_cohorts.version_id))),
            LATERAL unnest(cohorts_versions.periods) WITH ORDINALITY periods(definition, index)
        ), with_period_details AS (
         SELECT period_definitions.cohort_id,
            period_definitions.name,
            period_definitions.index,
            (period_definitions.definition ->> 'title'::text) AS title,
            (period_definitions.definition ->> 'style'::text) AS style,
            ((period_definitions.definition ->> 'days'::text))::integer AS days,
            ((period_definitions.definition ->> 'days_offset'::text))::integer AS days_offset,
            (period_definitions.definition ->> 'additional_specialization_playlists_to_complete'::text) AS additional_specialization_playlists_to_complete,
            ARRAY( SELECT elem.value AS elem
                   FROM json_array_elements(((period_definitions.definition ->> 'stream_entries'::text))::json) elem(value)) AS stream_entries,
            period_definitions.cohort_start_date
           FROM period_definitions
        UNION ALL
         SELECT published_cohorts.id AS cohort_id,
            published_cohorts.name,
            0 AS index,
            'Period 0'::text AS title,
            NULL::text AS style,
            NULL::integer AS days,
            NULL::integer AS days_offset,
            NULL::text AS additional_specialization_playlists_to_complete,
            NULL::json[] AS stream_entries,
            published_cohorts.start_date AS cohort_start_date
           FROM public.published_cohorts
        ), with_stream_entries AS (
         SELECT with_period_details.cohort_id,
            with_period_details.index,
            stream_entries.stream_entry,
            stream_entries.stream_entry_index
           FROM with_period_details,
            LATERAL unnest(with_period_details.stream_entries) WITH ORDINALITY stream_entries(stream_entry, stream_entry_index)
        ), stream_pack_ids AS (
         SELECT with_stream_entries.cohort_id,
            with_stream_entries.index,
            array_remove(array_agg(
                CASE
                    WHEN (((with_stream_entries.stream_entry ->> 'required'::text))::boolean = true) THEN ((with_stream_entries.stream_entry ->> 'locale_pack_id'::text))::uuid
                    ELSE NULL::uuid
                END), NULL::uuid) AS required_stream_pack_ids,
            array_remove(array_agg(
                CASE
                    WHEN (((with_stream_entries.stream_entry ->> 'required'::text))::boolean = false) THEN ((with_stream_entries.stream_entry ->> 'locale_pack_id'::text))::uuid
                    ELSE NULL::uuid
                END), NULL::uuid) AS optional_stream_pack_ids
           FROM with_stream_entries
          GROUP BY with_stream_entries.cohort_id, with_stream_entries.index
        ), with_pack_ids AS (
         SELECT with_period_details.cohort_id,
            with_period_details.name,
            with_period_details.index,
            with_period_details.title,
            with_period_details.style,
            with_period_details.days,
            with_period_details.days_offset,
            with_period_details.additional_specialization_playlists_to_complete,
            with_period_details.stream_entries,
            with_period_details.cohort_start_date,
            stream_pack_ids.required_stream_pack_ids,
            stream_pack_ids.optional_stream_pack_ids,
            ARRAY( SELECT published_stream_lesson_locale_packs.lesson_locale_pack_id
                   FROM public.published_stream_lesson_locale_packs
                  WHERE (published_stream_lesson_locale_packs.stream_locale_pack_id = ANY (stream_pack_ids.required_stream_pack_ids))) AS required_lesson_pack_ids
           FROM (with_period_details
             LEFT JOIN stream_pack_ids ON (((with_period_details.cohort_id = stream_pack_ids.cohort_id) AND (with_period_details.index = stream_pack_ids.index))))
        ), expected_counts AS (
         SELECT periods.cohort_id,
            periods.index,
            public.array_sort_unique(public.array_cat_agg(previous_periods.required_stream_pack_ids)) AS cumulative_required_stream_pack_ids,
            public.array_sort_unique(public.array_cat_agg(previous_periods.required_lesson_pack_ids)) AS cumulative_required_lesson_pack_ids
           FROM (with_pack_ids periods
             JOIN with_pack_ids previous_periods ON (((periods.cohort_id = previous_periods.cohort_id) AND (periods.index >= previous_periods.index))))
          GROUP BY periods.cohort_id, periods.index
        ), time_info AS (
         SELECT this_periods.cohort_id,
            this_periods.index,
            (COALESCE(sum((previous_periods.days + previous_periods.days_offset)), (0)::bigint) + this_periods.days_offset) AS offset_from_cohort_start
           FROM (with_period_details this_periods
             LEFT JOIN with_period_details previous_periods ON (((this_periods.cohort_id = previous_periods.cohort_id) AND (this_periods.index > previous_periods.index))))
          GROUP BY this_periods.cohort_id, this_periods.index, this_periods.days_offset
        ), with_time_info AS (
         SELECT with_pack_ids.cohort_id,
            with_pack_ids.name,
            with_pack_ids.index,
            with_pack_ids.title,
            with_pack_ids.style,
            with_pack_ids.days,
            with_pack_ids.days_offset,
            with_pack_ids.additional_specialization_playlists_to_complete,
            with_pack_ids.stream_entries,
            with_pack_ids.cohort_start_date,
            with_pack_ids.required_stream_pack_ids,
            with_pack_ids.optional_stream_pack_ids,
            with_pack_ids.required_lesson_pack_ids,
                CASE
                    WHEN (with_pack_ids.index = 0) THEN '2000-01-01 00:00:00'::timestamp without time zone
                    ELSE public.add_dst_aware_offset(with_pack_ids.cohort_start_date, (time_info.offset_from_cohort_start)::numeric, 'days'::text)
                END AS period_start,
                CASE
                    WHEN (with_pack_ids.index = 0) THEN ( SELECT public.add_dst_aware_offset(with_pack_ids.cohort_start_date, (first_periods.offset_from_cohort_start)::numeric, 'days'::text) AS add_dst_aware_offset
                       FROM time_info first_periods
                      WHERE ((first_periods.cohort_id = with_pack_ids.cohort_id) AND (first_periods.index = 1)))
                    ELSE public.add_dst_aware_offset(with_pack_ids.cohort_start_date, ((time_info.offset_from_cohort_start + with_pack_ids.days))::numeric, 'days'::text)
                END AS period_end
           FROM (with_pack_ids
             JOIN time_info ON (((with_pack_ids.cohort_id = time_info.cohort_id) AND (with_pack_ids.index = time_info.index))))
        ), with_expected_counts_and_time_info AS (
         SELECT with_time_info.cohort_id,
            with_time_info.name,
            with_time_info.index,
            with_time_info.title,
            with_time_info.style,
            with_time_info.days,
            with_time_info.days_offset,
            with_time_info.additional_specialization_playlists_to_complete,
            with_time_info.stream_entries,
            with_time_info.cohort_start_date,
            with_time_info.required_stream_pack_ids,
            with_time_info.optional_stream_pack_ids,
            with_time_info.required_lesson_pack_ids,
            with_time_info.period_start,
            with_time_info.period_end,
            expected_counts.cumulative_required_stream_pack_ids,
            expected_counts.cumulative_required_lesson_pack_ids,
            array_length(expected_counts.cumulative_required_stream_pack_ids, 1) AS cumulative_required_stream_pack_ids_count,
            array_length(expected_counts.cumulative_required_lesson_pack_ids, 1) AS cumulative_required_lesson_pack_ids_count
           FROM ((with_time_info
             JOIN expected_counts ON (((expected_counts.index = with_time_info.index) AND (expected_counts.cohort_id = with_time_info.cohort_id))))
             JOIN time_info ON (((time_info.index = with_time_info.index) AND (time_info.cohort_id = with_time_info.cohort_id))))
        )
 SELECT with_expected_counts_and_time_info.cohort_id,
    with_expected_counts_and_time_info.name,
    with_expected_counts_and_time_info.index,
    with_expected_counts_and_time_info.title,
    with_expected_counts_and_time_info.style,
    with_expected_counts_and_time_info.days,
    with_expected_counts_and_time_info.days_offset,
    with_expected_counts_and_time_info.additional_specialization_playlists_to_complete,
    with_expected_counts_and_time_info.stream_entries,
    with_expected_counts_and_time_info.cohort_start_date,
    with_expected_counts_and_time_info.required_stream_pack_ids,
    with_expected_counts_and_time_info.optional_stream_pack_ids,
    with_expected_counts_and_time_info.required_lesson_pack_ids,
    with_expected_counts_and_time_info.period_start,
    with_expected_counts_and_time_info.period_end,
    with_expected_counts_and_time_info.cumulative_required_stream_pack_ids,
    with_expected_counts_and_time_info.cumulative_required_lesson_pack_ids,
    with_expected_counts_and_time_info.cumulative_required_stream_pack_ids_count,
    with_expected_counts_and_time_info.cumulative_required_lesson_pack_ids_count
   FROM with_expected_counts_and_time_info
  ORDER BY with_expected_counts_and_time_info.cohort_id, with_expected_counts_and_time_info.index
  WITH NO DATA;


--
-- Name: published_playlists; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.published_playlists AS
 SELECT playlists.id,
    playlists.locale_pack_id,
    playlists_versions.title,
    playlists_versions.version_id,
    playlists_versions.locale,
    playlists_versions.stream_entries,
    array_length(playlists_versions.stream_entries, 1) AS stream_count
   FROM ((public.playlists
     JOIN public.playlists_versions ON ((playlists.id = playlists_versions.id)))
     JOIN public.content_publishers ON ((content_publishers.playlist_version_id = playlists_versions.version_id)))
  WITH NO DATA;


--
-- Name: published_playlist_locale_packs; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.published_playlist_locale_packs AS
 SELECT published_playlists.locale_pack_id,
    playlists.title,
    string_agg(published_playlists.locale, ','::text) AS locales
   FROM (public.published_playlists
     JOIN public.playlists ON (((playlists.locale_pack_id = published_playlists.locale_pack_id) AND (playlists.locale = 'en'::text))))
  GROUP BY published_playlists.locale_pack_id, playlists.title
  WITH NO DATA;


--
-- Name: published_cohort_playlist_locale_packs; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.published_cohort_playlist_locale_packs AS
 WITH required_cohort_playlist_locale_packs AS (
         SELECT cohorts.id AS cohort_id,
            cohorts.name AS cohort_name,
            true AS required,
            false AS specialization,
            unnest(cohorts.required_playlist_pack_ids) AS playlist_locale_pack_id
           FROM public.published_cohorts cohorts
        ), specialization_cohort_playlist_locale_packs AS (
         SELECT cohorts.id AS cohort_id,
            cohorts.name AS cohort_name,
            false AS required,
            true AS specialization,
            unnest(cohorts.specialization_playlist_pack_ids) AS playlist_locale_pack_id
           FROM public.published_cohorts cohorts
        ), cohort_playlist_locale_packs_1 AS (
         SELECT required_cohort_playlist_locale_packs.cohort_id,
            required_cohort_playlist_locale_packs.cohort_name,
            required_cohort_playlist_locale_packs.required,
            required_cohort_playlist_locale_packs.specialization,
            required_cohort_playlist_locale_packs.playlist_locale_pack_id
           FROM required_cohort_playlist_locale_packs
        UNION
         SELECT specialization_cohort_playlist_locale_packs.cohort_id,
            specialization_cohort_playlist_locale_packs.cohort_name,
            specialization_cohort_playlist_locale_packs.required,
            specialization_cohort_playlist_locale_packs.specialization,
            specialization_cohort_playlist_locale_packs.playlist_locale_pack_id
           FROM specialization_cohort_playlist_locale_packs
        ), cohort_playlist_locale_packs_2 AS (
         SELECT cohort_playlist_locale_packs_1.cohort_id,
            cohort_playlist_locale_packs_1.cohort_name,
            cohort_playlist_locale_packs_1.required,
            cohort_playlist_locale_packs_1.specialization,
            (published_playlist_locale_packs.locale_pack_id = ( SELECT cohorts.required_playlist_pack_ids[1] AS required_playlist_pack_ids
                   FROM public.cohorts
                  WHERE (cohorts.id = cohort_playlist_locale_packs_1.cohort_id))) AS foundations,
            published_playlist_locale_packs.title AS playlist_title,
            published_playlist_locale_packs.locales AS playlist_locales,
            published_playlist_locale_packs.locale_pack_id AS playlist_locale_pack_id
           FROM (cohort_playlist_locale_packs_1
             JOIN public.published_playlist_locale_packs ON ((published_playlist_locale_packs.locale_pack_id = cohort_playlist_locale_packs_1.playlist_locale_pack_id)))
        )
 SELECT cohort_playlist_locale_packs_2.cohort_id,
    cohort_playlist_locale_packs_2.cohort_name,
    cohort_playlist_locale_packs_2.required,
    cohort_playlist_locale_packs_2.specialization,
    cohort_playlist_locale_packs_2.foundations,
    cohort_playlist_locale_packs_2.playlist_title,
    cohort_playlist_locale_packs_2.playlist_locales,
    cohort_playlist_locale_packs_2.playlist_locale_pack_id
   FROM cohort_playlist_locale_packs_2
  WITH NO DATA;


--
-- Name: published_playlist_streams; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.published_playlist_streams AS
 WITH stream_locale_packs AS (
         SELECT published_playlists.id AS playlist_id,
            published_playlists.locale_pack_id AS playlist_locale_pack_id,
            published_playlists.title AS playlist_title,
            published_playlists.version_id AS playlist_version_id,
            published_playlists.locale AS playlist_locale,
            ((unnest(published_playlists.stream_entries) ->> 'locale_pack_id'::text))::uuid AS stream_locale_pack_id
           FROM public.published_playlists
        )
 SELECT stream_locale_packs.playlist_id,
    stream_locale_packs.playlist_locale_pack_id,
    stream_locale_packs.playlist_title,
    stream_locale_packs.playlist_version_id,
    stream_locale_packs.playlist_locale,
    stream_locale_packs.stream_locale_pack_id,
        CASE
            WHEN (locale_streams.title IS NOT NULL) THEN locale_streams.title
            ELSE en_streams.title
        END AS stream_title,
        CASE
            WHEN (locale_streams.id IS NOT NULL) THEN locale_streams.id
            ELSE en_streams.id
        END AS stream_id,
        CASE
            WHEN (locale_streams.version_id IS NOT NULL) THEN locale_streams.version_id
            ELSE en_streams.version_id
        END AS stream_version_id,
        CASE
            WHEN (locale_streams.locale IS NOT NULL) THEN locale_streams.locale
            ELSE en_streams.locale
        END AS stream_locale
   FROM ((stream_locale_packs
     LEFT JOIN public.published_streams en_streams ON (((stream_locale_packs.stream_locale_pack_id = en_streams.locale_pack_id) AND (en_streams.locale = 'en'::text))))
     LEFT JOIN public.published_streams locale_streams ON (((stream_locale_packs.stream_locale_pack_id = locale_streams.locale_pack_id) AND (locale_streams.locale = stream_locale_packs.playlist_locale))))
  WITH NO DATA;


--
-- Name: published_cohort_stream_locale_packs; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.published_cohort_stream_locale_packs AS
 WITH streams_from_groups AS (
         SELECT DISTINCT cohorts.id AS cohort_id,
            access_groups_lesson_stream_locale_packs.locale_pack_id AS stream_locale_pack_id,
            0 AS required,
            0 AS specialization,
            0 AS foundations
           FROM ((public.published_cohorts cohorts
             LEFT JOIN public.access_groups_cohorts ON ((access_groups_cohorts.cohort_id = cohorts.id)))
             LEFT JOIN public.access_groups_lesson_stream_locale_packs ON ((access_groups_lesson_stream_locale_packs.access_group_id = access_groups_cohorts.access_group_id)))
        ), streams_from_playlists AS (
         SELECT DISTINCT cohorts.id AS cohort_id,
            published_playlist_streams.stream_locale_pack_id,
                CASE
                    WHEN published_cohort_playlist_locale_packs.required THEN 1
                    ELSE 0
                END AS required,
                CASE
                    WHEN published_cohort_playlist_locale_packs.specialization THEN 1
                    ELSE 0
                END AS specialization,
                CASE
                    WHEN published_cohort_playlist_locale_packs.foundations THEN 1
                    ELSE 0
                END AS foundations
           FROM ((public.published_cohorts cohorts
             JOIN public.published_cohort_playlist_locale_packs ON ((cohorts.id = published_cohort_playlist_locale_packs.cohort_id)))
             JOIN public.published_playlist_streams ON ((published_playlist_streams.playlist_locale_pack_id = published_cohort_playlist_locale_packs.playlist_locale_pack_id)))
        ), required_streams_from_schedule AS (
         SELECT DISTINCT published_cohort_periods.cohort_id,
            unnest(published_cohort_periods.required_stream_pack_ids) AS stream_locale_pack_id,
            1 AS required,
            0 AS specialization,
            0 AS foundations
           FROM public.published_cohort_periods
        ), optional_streams_from_schedule AS (
         SELECT DISTINCT published_cohort_periods.cohort_id,
            unnest(published_cohort_periods.optional_stream_pack_ids) AS stream_locale_pack_id,
            0 AS required,
            0 AS specialization,
            0 AS foundations
           FROM public.published_cohort_periods
        ), all_stream_locale_pack_ids AS (
         SELECT streams_from_groups.cohort_id,
            streams_from_groups.stream_locale_pack_id,
            streams_from_groups.required,
            streams_from_groups.specialization,
            streams_from_groups.foundations
           FROM streams_from_groups
        UNION
         SELECT streams_from_playlists.cohort_id,
            streams_from_playlists.stream_locale_pack_id,
            streams_from_playlists.required,
            streams_from_playlists.specialization,
            streams_from_playlists.foundations
           FROM streams_from_playlists
        UNION
         SELECT required_streams_from_schedule.cohort_id,
            required_streams_from_schedule.stream_locale_pack_id,
            required_streams_from_schedule.required,
            required_streams_from_schedule.specialization,
            required_streams_from_schedule.foundations
           FROM required_streams_from_schedule
        UNION
         SELECT optional_streams_from_schedule.cohort_id,
            optional_streams_from_schedule.stream_locale_pack_id,
            optional_streams_from_schedule.required,
            optional_streams_from_schedule.specialization,
            optional_streams_from_schedule.foundations
           FROM optional_streams_from_schedule
        ), published_cohort_stream_locale_packs AS (
         SELECT cohorts.id AS cohort_id,
            cohorts.name AS cohort_name,
                CASE
                    WHEN (sum(all_stream_locale_pack_ids.foundations) > 0) THEN true
                    ELSE false
                END AS foundations,
                CASE
                    WHEN (sum(all_stream_locale_pack_ids.required) > 0) THEN true
                    ELSE false
                END AS required,
                CASE
                    WHEN (sum(all_stream_locale_pack_ids.specialization) > 0) THEN true
                    ELSE false
                END AS specialization,
                CASE
                    WHEN ((sum(all_stream_locale_pack_ids.required) + sum(all_stream_locale_pack_ids.specialization)) = 0) THEN true
                    ELSE false
                END AS elective,
            published_stream_locale_packs.exam,
            published_stream_locale_packs.title AS stream_title,
            published_stream_locale_packs.locale_pack_id AS stream_locale_pack_id,
            published_stream_locale_packs.locales AS stream_locales,
            count(DISTINCT published_cohort_playlist_locale_packs.playlist_locale_pack_id) AS playlist_count
           FROM ((((public.published_cohorts cohorts
             JOIN all_stream_locale_pack_ids ON ((cohorts.id = all_stream_locale_pack_ids.cohort_id)))
             JOIN public.published_stream_locale_packs ON ((published_stream_locale_packs.locale_pack_id = all_stream_locale_pack_ids.stream_locale_pack_id)))
             LEFT JOIN public.published_playlist_streams ON ((published_playlist_streams.stream_locale_pack_id = all_stream_locale_pack_ids.stream_locale_pack_id)))
             LEFT JOIN public.published_cohort_playlist_locale_packs ON (((cohorts.id = published_cohort_playlist_locale_packs.cohort_id) AND (published_playlist_streams.playlist_locale_pack_id = published_cohort_playlist_locale_packs.playlist_locale_pack_id))))
          GROUP BY cohorts.id, cohorts.name, published_stream_locale_packs.title, published_stream_locale_packs.locale_pack_id, published_stream_locale_packs.locales, published_stream_locale_packs.exam
        )
 SELECT published_cohort_stream_locale_packs.cohort_id,
    published_cohort_stream_locale_packs.cohort_name,
    published_cohort_stream_locale_packs.foundations,
    published_cohort_stream_locale_packs.required,
    published_cohort_stream_locale_packs.specialization,
    published_cohort_stream_locale_packs.elective,
    published_cohort_stream_locale_packs.exam,
    published_cohort_stream_locale_packs.stream_title,
    published_cohort_stream_locale_packs.stream_locale_pack_id,
    published_cohort_stream_locale_packs.stream_locales,
    published_cohort_stream_locale_packs.playlist_count
   FROM published_cohort_stream_locale_packs
  WITH NO DATA;


--
-- Name: published_cohort_lesson_locale_packs; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.published_cohort_lesson_locale_packs AS
 SELECT published_cohort_stream_locale_packs.cohort_id,
    published_cohort_stream_locale_packs.cohort_name,
    (sum(
        CASE
            WHEN published_cohort_stream_locale_packs.foundations THEN 1
            ELSE 0
        END) > 0) AS foundations,
    (sum(
        CASE
            WHEN published_cohort_stream_locale_packs.required THEN 1
            ELSE 0
        END) > 0) AS required,
    (sum(
        CASE
            WHEN published_cohort_stream_locale_packs.specialization THEN 1
            ELSE 0
        END) > 0) AS specialization,
    (sum(
        CASE
            WHEN published_cohort_stream_locale_packs.required THEN 0
            ELSE 1
        END) > 0) AS elective,
    (sum(
        CASE
            WHEN published_cohort_stream_locale_packs.exam THEN 1
            ELSE 0
        END) > 0) AS exam,
    published_stream_lesson_locale_packs.test,
    published_stream_lesson_locale_packs.assessment,
    published_stream_lesson_locale_packs.lesson_title,
    published_stream_lesson_locale_packs.lesson_locale_pack_id,
    published_stream_lesson_locale_packs.lesson_locales
   FROM (public.published_cohort_stream_locale_packs
     JOIN public.published_stream_lesson_locale_packs ON ((published_stream_lesson_locale_packs.stream_locale_pack_id = published_cohort_stream_locale_packs.stream_locale_pack_id)))
  GROUP BY published_stream_lesson_locale_packs.test, published_stream_lesson_locale_packs.assessment, published_stream_lesson_locale_packs.lesson_title, published_stream_lesson_locale_packs.lesson_locale_pack_id, published_stream_lesson_locale_packs.lesson_locales, published_cohort_stream_locale_packs.cohort_id, published_cohort_stream_locale_packs.cohort_name
  WITH NO DATA;


--
-- Name: user_lesson_progress_records; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_lesson_progress_records (
    user_id uuid NOT NULL,
    locale_pack_id uuid NOT NULL,
    started_at timestamp without time zone NOT NULL,
    completed_at timestamp without time zone,
    best_score_from_lesson_progress double precision,
    total_lesson_time double precision,
    last_lesson_activity_time timestamp without time zone NOT NULL,
    total_lesson_time_on_desktop double precision,
    total_lesson_time_on_mobile_app double precision,
    total_lesson_time_on_mobile_web double precision,
    total_lesson_time_on_unknown double precision,
    completed_on_client_type text,
    last_lesson_reset_at timestamp without time zone,
    lesson_reset_count integer,
    test boolean,
    assessment boolean,
    lesson_finish_count integer,
    average_assessment_score_first double precision,
    average_assessment_score_best double precision,
    official_test_score double precision
);


--
-- Name: cohort_user_lesson_progress_records; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.cohort_user_lesson_progress_records AS
 SELECT cohort_applications_plus.cohort_id,
    cohort_applications_plus.cohort_name,
    cohort_applications_plus.user_id,
    cohort_lesson_locale_packs.required,
    cohort_lesson_locale_packs.assessment,
    cohort_lesson_locale_packs.test,
    cohort_lesson_locale_packs.elective,
    cohort_lesson_locale_packs.foundations,
    cohort_lesson_locale_packs.lesson_title,
    cohort_lesson_locale_packs.lesson_locale_pack_id,
    lesson_progress.started_at,
    lesson_progress.completed_at,
    lesson_progress.total_lesson_time,
    lesson_progress.last_lesson_activity_time,
    lesson_progress.lesson_finish_count,
    lesson_progress.average_assessment_score_first,
    lesson_progress.average_assessment_score_best,
    lesson_progress.official_test_score,
    lesson_progress.total_lesson_time_on_desktop,
    lesson_progress.total_lesson_time_on_mobile_app,
    lesson_progress.total_lesson_time_on_mobile_web,
    lesson_progress.total_lesson_time_on_unknown,
    lesson_progress.completed_on_client_type,
    lesson_progress.lesson_reset_count,
    lesson_progress.last_lesson_reset_at
   FROM ((public.cohort_applications_plus
     JOIN public.published_cohort_lesson_locale_packs cohort_lesson_locale_packs ON ((cohort_applications_plus.cohort_id = cohort_lesson_locale_packs.cohort_id)))
     LEFT JOIN public.user_lesson_progress_records lesson_progress ON (((lesson_progress.locale_pack_id = cohort_lesson_locale_packs.lesson_locale_pack_id) AND (lesson_progress.user_id = cohort_applications_plus.user_id))))
  WHERE (cohort_applications_plus.was_accepted = true);


--
-- Name: published_cohort_content_details; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.published_cohort_content_details AS
 WITH lesson_info AS (
         SELECT cohort_lesson_locale_packs.cohort_id,
            cohort_lesson_locale_packs.cohort_name,
            array_length(cohorts.required_playlist_pack_ids, 1) AS required_playlist_count,
            array_length(cohorts.specialization_playlist_pack_ids, 1) AS specialization_playlist_count,
            count(DISTINCT
                CASE
                    WHEN cohort_lesson_locale_packs.foundations THEN cohort_lesson_locale_packs.lesson_locale_pack_id
                    ELSE NULL::uuid
                END) AS foundations_lesson_count,
            count(DISTINCT
                CASE
                    WHEN cohort_lesson_locale_packs.required THEN cohort_lesson_locale_packs.lesson_locale_pack_id
                    ELSE NULL::uuid
                END) AS required_lesson_count,
            count(DISTINCT
                CASE
                    WHEN cohort_lesson_locale_packs.test THEN cohort_lesson_locale_packs.lesson_locale_pack_id
                    ELSE NULL::uuid
                END) AS test_lesson_count,
            count(DISTINCT
                CASE
                    WHEN cohort_lesson_locale_packs.elective THEN cohort_lesson_locale_packs.lesson_locale_pack_id
                    ELSE NULL::uuid
                END) AS elective_lesson_count,
            (floor((date_part('epoch'::text, (cohorts.end_date - cohorts.start_date)) / date_part('epoch'::text, '7 days'::interval))) - (1)::double precision) AS duration_weeks,
            cohorts.enrollment_deadline
           FROM (public.published_cohort_lesson_locale_packs cohort_lesson_locale_packs
             JOIN public.published_cohorts cohorts ON ((cohorts.id = cohort_lesson_locale_packs.cohort_id)))
          GROUP BY cohort_lesson_locale_packs.cohort_id, cohort_lesson_locale_packs.cohort_name, cohorts.required_playlist_pack_ids, cohorts.specialization_playlist_pack_ids, cohorts.end_date, cohorts.start_date, cohorts.id, cohorts.enrollment_deadline
        ), stream_info AS (
         SELECT cohort_stream_locale_packs.cohort_id,
            count(DISTINCT
                CASE
                    WHEN cohort_stream_locale_packs.foundations THEN cohort_stream_locale_packs.stream_locale_pack_id
                    ELSE NULL::uuid
                END) AS foundations_stream_count,
            count(DISTINCT
                CASE
                    WHEN cohort_stream_locale_packs.required THEN cohort_stream_locale_packs.stream_locale_pack_id
                    ELSE NULL::uuid
                END) AS required_stream_count,
            count(DISTINCT
                CASE
                    WHEN cohort_stream_locale_packs.exam THEN cohort_stream_locale_packs.stream_locale_pack_id
                    ELSE NULL::uuid
                END) AS exam_stream_count,
            count(DISTINCT
                CASE
                    WHEN cohort_stream_locale_packs.elective THEN cohort_stream_locale_packs.stream_locale_pack_id
                    ELSE NULL::uuid
                END) AS elective_stream_count
           FROM (public.published_cohort_stream_locale_packs cohort_stream_locale_packs
             JOIN public.published_cohorts cohorts ON ((cohorts.id = cohort_stream_locale_packs.cohort_id)))
          GROUP BY cohort_stream_locale_packs.cohort_id
        )
 SELECT lesson_info.cohort_id,
    lesson_info.cohort_name,
    lesson_info.required_playlist_count,
    lesson_info.specialization_playlist_count,
    lesson_info.foundations_lesson_count,
    lesson_info.required_lesson_count,
    lesson_info.test_lesson_count,
    lesson_info.elective_lesson_count,
    lesson_info.duration_weeks,
    lesson_info.enrollment_deadline,
    stream_info.foundations_stream_count,
    stream_info.required_stream_count,
    stream_info.exam_stream_count,
    stream_info.elective_stream_count,
        CASE
            WHEN (lesson_info.duration_weeks > (0)::double precision) THEN ((lesson_info.required_lesson_count)::double precision / lesson_info.duration_weeks)
            ELSE NULL::double precision
        END AS expected_pace,
        CASE
            WHEN (lesson_info.duration_weeks > (0)::double precision) THEN (((lesson_info.required_lesson_count - lesson_info.foundations_lesson_count))::double precision / lesson_info.duration_weeks)
            ELSE NULL::double precision
        END AS expected_pace_fp
   FROM (lesson_info
     JOIN stream_info ON ((lesson_info.cohort_id = stream_info.cohort_id)))
  WITH NO DATA;


--
-- Name: cohort_status_changes; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.cohort_status_changes AS
 WITH indexed_application_versions AS (
         SELECT cohort_applications.user_id,
            cohort_applications.cohort_id,
            cohort_applications_versions.status,
            cohort_applications_versions.graduation_status,
            cohort_applications_versions.version_created_at,
            row_number() OVER (PARTITION BY cohort_applications_versions.user_id, cohort_applications_versions.id ORDER BY cohort_applications_versions.version_created_at) AS version_index
           FROM (public.cohort_applications
             JOIN public.cohort_applications_versions ON ((cohort_applications_versions.id = cohort_applications.id)))
          ORDER BY cohort_applications_versions.user_id, cohort_applications_versions.version_created_at
        ), application_changes AS (
         SELECT old_versions.user_id,
            old_versions.cohort_id,
            old_versions.version_created_at AS from_time,
            COALESCE(new_versions.version_created_at, '2099-01-01 00:00:00'::timestamp without time zone) AS until_time,
            old_versions.status AS cohort_application_status,
            COALESCE(old_versions.graduation_status, 'pending'::text) AS graduation_status,
            (count(previous_late_enrollments.user_id) > 0) AS enrolled_late
           FROM (((indexed_application_versions old_versions
             JOIN public.published_cohort_content_details cohort_content_details ON ((cohort_content_details.cohort_id = old_versions.cohort_id)))
             LEFT JOIN indexed_application_versions new_versions ON (((old_versions.user_id = new_versions.user_id) AND (old_versions.cohort_id = new_versions.cohort_id) AND (old_versions.version_index = (new_versions.version_index - 1)))))
             LEFT JOIN indexed_application_versions previous_late_enrollments ON (((old_versions.user_id = previous_late_enrollments.user_id) AND (old_versions.cohort_id = previous_late_enrollments.cohort_id) AND (old_versions.version_index >= previous_late_enrollments.version_index) AND (previous_late_enrollments.status = 'accepted'::text) AND (previous_late_enrollments.version_created_at >= cohort_content_details.enrollment_deadline))))
          GROUP BY old_versions.user_id, old_versions.cohort_id, old_versions.version_created_at, COALESCE(new_versions.version_created_at, '2099-01-01 00:00:00'::timestamp without time zone), old_versions.status, COALESCE(old_versions.graduation_status, 'pending'::text)
        ), past_enrollment_deadline_changes AS (
         SELECT cohort_applications.user_id,
            cohort_applications.cohort_id,
            cohort_content_details.enrollment_deadline AS from_time,
            '2099-01-01 00:00:00'::timestamp without time zone AS until_time,
            true AS past_enrollment_deadline,
            (application_changes.cohort_application_status = 'accepted'::text) AS enrolled_at_deadline,
            cohorts.name,
            cohorts.start_date
           FROM (((public.cohort_applications
             JOIN public.published_cohort_content_details cohort_content_details ON ((cohort_content_details.cohort_id = cohort_applications.cohort_id)))
             JOIN public.published_cohorts cohorts ON ((cohorts.id = cohort_applications.cohort_id)))
             JOIN application_changes ON (((application_changes.user_id = cohort_applications.user_id) AND (application_changes.cohort_id = cohort_applications.cohort_id) AND (application_changes.from_time <= cohort_content_details.enrollment_deadline) AND (application_changes.until_time > cohort_content_details.enrollment_deadline) AND (cohort_content_details.enrollment_deadline < now()))))
        ), foundations_complete_changes AS (
         SELECT cohort_user_lesson_progress_records.user_id,
            cohort_user_lesson_progress_records.cohort_id,
            true AS foundations_complete,
            max(cohort_user_lesson_progress_records.completed_at) AS from_time,
            '2099-01-01 00:00:00'::timestamp without time zone AS until_time
           FROM (public.cohort_user_lesson_progress_records
             JOIN public.published_cohort_content_details cohort_content_details ON ((cohort_user_lesson_progress_records.cohort_id = cohort_content_details.cohort_id)))
          WHERE (true AND (cohort_user_lesson_progress_records.foundations = true) AND (cohort_user_lesson_progress_records.completed_at IS NOT NULL))
          GROUP BY cohort_user_lesson_progress_records.user_id, cohort_user_lesson_progress_records.cohort_id, cohort_content_details.foundations_lesson_count
         HAVING (count(*) >= cohort_content_details.foundations_lesson_count)
        ), showed_up_changes AS (
         SELECT cohort_user_lesson_progress_records.user_id,
            cohort_user_lesson_progress_records.cohort_id,
            true AS showed_up,
            min(cohort_user_lesson_progress_records.completed_at) AS from_time,
            '2099-01-01 00:00:00'::timestamp without time zone AS until_time
           FROM (public.cohort_user_lesson_progress_records
             JOIN public.cohort_applications_plus ON (((cohort_user_lesson_progress_records.cohort_id = cohort_applications_plus.cohort_id) AND (cohort_user_lesson_progress_records.user_id = cohort_applications_plus.user_id))))
          WHERE (cohort_user_lesson_progress_records.completed_at > cohort_applications_plus.accepted_at)
          GROUP BY cohort_user_lesson_progress_records.user_id, cohort_user_lesson_progress_records.cohort_id
        ), change_times AS (
         SELECT application_changes.user_id,
            application_changes.cohort_id,
            application_changes.from_time AS change_time
           FROM application_changes
        UNION ALL
         SELECT foundations_complete_changes.user_id,
            foundations_complete_changes.cohort_id,
            foundations_complete_changes.from_time AS change_time
           FROM foundations_complete_changes
        UNION ALL
         SELECT showed_up_changes.user_id,
            showed_up_changes.cohort_id,
            showed_up_changes.from_time AS change_time
           FROM showed_up_changes
        UNION ALL
         SELECT past_enrollment_deadline_changes.user_id,
            past_enrollment_deadline_changes.cohort_id,
            past_enrollment_deadline_changes.from_time AS change_time
           FROM past_enrollment_deadline_changes
  ORDER BY 3
        ), change_times_2 AS (
         SELECT change_times.user_id,
            change_times.cohort_id,
            change_times.change_time,
            row_number() OVER (PARTITION BY change_times.user_id, change_times.cohort_id ORDER BY change_times.change_time) AS version_index
           FROM change_times
        ), versions AS (
         SELECT change_times.user_id,
            change_times.cohort_id,
            change_times.change_time AS from_time,
            COALESCE(next_change_times.change_time, '2099-01-01 00:00:00'::timestamp without time zone) AS until_time,
            application_changes.cohort_application_status,
            application_changes.graduation_status,
            COALESCE(foundations_complete_changes.foundations_complete, false) AS foundations_complete,
            COALESCE(showed_up_changes.showed_up, false) AS showed_up,
            COALESCE(past_enrollment_deadline_changes.past_enrollment_deadline, false) AS past_enrollment_deadline,
            COALESCE((application_changes.enrolled_late OR past_enrollment_deadline_changes.enrolled_at_deadline), false) AS enrolled,
            row_number() OVER (PARTITION BY change_times.user_id ORDER BY change_times.change_time) AS version_index,
            cohort_applications_plus.accepted_at,
            application_changes.enrolled_late
           FROM ((((((change_times_2 change_times
             JOIN public.cohort_applications_plus ON (((cohort_applications_plus.user_id = change_times.user_id) AND (cohort_applications_plus.cohort_id = change_times.cohort_id))))
             LEFT JOIN application_changes ON (((application_changes.from_time <= change_times.change_time) AND (application_changes.until_time > change_times.change_time) AND (application_changes.user_id = change_times.user_id) AND (application_changes.cohort_id = change_times.cohort_id))))
             LEFT JOIN foundations_complete_changes ON (((foundations_complete_changes.from_time <= change_times.change_time) AND (foundations_complete_changes.until_time > change_times.change_time) AND (foundations_complete_changes.user_id = change_times.user_id) AND (foundations_complete_changes.cohort_id = change_times.cohort_id))))
             LEFT JOIN showed_up_changes ON (((showed_up_changes.from_time <= change_times.change_time) AND (showed_up_changes.until_time > change_times.change_time) AND (showed_up_changes.user_id = change_times.user_id) AND (showed_up_changes.cohort_id = change_times.cohort_id))))
             LEFT JOIN past_enrollment_deadline_changes ON (((past_enrollment_deadline_changes.from_time <= change_times.change_time) AND (past_enrollment_deadline_changes.until_time > change_times.change_time) AND (past_enrollment_deadline_changes.user_id = change_times.user_id) AND (past_enrollment_deadline_changes.cohort_id = change_times.cohort_id))))
             LEFT JOIN change_times_2 next_change_times ON (((next_change_times.user_id = change_times.user_id) AND (next_change_times.cohort_id = change_times.cohort_id) AND (next_change_times.version_index = (change_times.version_index + 1)))))
          WHERE (application_changes.cohort_application_status IS NOT NULL)
        ), versions_2 AS (
         SELECT versions.user_id,
            versions.cohort_id,
            versions.from_time,
            versions.until_time,
                CASE
                    WHEN (versions.cohort_application_status = ANY (ARRAY['rejected'::text, 'deferred'::text])) THEN versions.cohort_application_status
                    WHEN (versions.cohort_application_status = 'pending'::text) THEN 'pending_acceptance'::text
                    WHEN ((versions.enrolled = true) AND (versions.cohort_application_status = 'expelled'::text)) THEN 'expelled'::text
                    WHEN ((versions.enrolled = false) AND (versions.cohort_application_status = 'expelled'::text)) THEN 'did_not_enroll'::text
                    WHEN (versions.graduation_status = ANY (ARRAY['graduated'::text, 'honors'::text])) THEN 'graduated'::text
                    WHEN (versions.graduation_status = 'failed'::text) THEN 'failed'::text
                    WHEN (versions.enrolled = true) THEN 'enrolled'::text
                    ELSE 'pending_enrollment'::text
                END AS status,
            versions.cohort_application_status,
            versions.graduation_status,
            versions.foundations_complete,
            versions.showed_up,
            versions.past_enrollment_deadline,
            versions.accepted_at,
            versions.enrolled,
            versions.enrolled_late
           FROM versions
        )
 SELECT versions_2.user_id,
    versions_2.cohort_id,
    versions_2.from_time,
    versions_2.until_time,
    versions_2.status,
    versions_2.cohort_application_status,
    versions_2.graduation_status,
    versions_2.foundations_complete,
    versions_2.showed_up,
    versions_2.past_enrollment_deadline,
    versions_2.accepted_at,
    versions_2.enrolled,
    versions_2.enrolled_late
   FROM versions_2
  ORDER BY versions_2.cohort_id, versions_2.user_id, versions_2.from_time, versions_2.until_time
  WITH NO DATA;


--
-- Name: institutions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.institutions (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    name character varying,
    sign_up_code character varying,
    scorm_token uuid,
    playlist_pack_ids uuid[] DEFAULT '{}'::uuid[]
);


--
-- Name: institutions_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.institutions_users (
    institution_id uuid,
    user_id uuid
);


--
-- Name: lesson_streams_progress; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lesson_streams_progress (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    lesson_bookmark_id uuid,
    started_at timestamp without time zone,
    completed_at timestamp without time zone,
    user_id uuid,
    certificate_image_id uuid,
    locale_pack_id uuid NOT NULL,
    time_runs_out_at timestamp without time zone,
    official_test_score double precision
);


--
-- Name: users_relevant_cohorts; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.users_relevant_cohorts AS
 SELECT DISTINCT users.id AS user_id,
    COALESCE(pending_or_accepted_cohorts.id, promoted_cohorts.id) AS cohort_id
   FROM (((public.users
     LEFT JOIN public.cohort_applications ON (((cohort_applications.user_id = users.id) AND (cohort_applications.status = ANY (ARRAY['pre_accepted'::text, 'accepted'::text, 'pending'::text])))))
     LEFT JOIN public.published_cohorts pending_or_accepted_cohorts ON ((pending_or_accepted_cohorts.id = cohort_applications.cohort_id)))
     LEFT JOIN public.published_cohorts promoted_cohorts ON (((pending_or_accepted_cohorts.id IS NULL) AND (promoted_cohorts.promoted = true) AND ((users.fallback_program_type)::text = promoted_cohorts.program_type))));


--
-- Name: user_can_see_playlist_locale_packs; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.user_can_see_playlist_locale_packs AS
 SELECT DISTINCT users.id AS user_id,
    unnest((relevant_cohorts.required_playlist_pack_ids || relevant_cohorts.specialization_playlist_pack_ids)) AS locale_pack_id
   FROM ((public.users
     JOIN public.users_relevant_cohorts ON ((users_relevant_cohorts.user_id = users.id)))
     JOIN public.published_cohorts relevant_cohorts ON ((relevant_cohorts.id = users_relevant_cohorts.cohort_id)))
UNION
 SELECT DISTINCT institutions_users.user_id,
    unnest(institutions.playlist_pack_ids) AS locale_pack_id
   FROM (public.institutions_users
     JOIN public.institutions ON ((institutions.id = institutions_users.institution_id)));


--
-- Name: playlist_progress; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.playlist_progress AS
 SELECT lesson_streams_progress.user_id,
    published_playlist_streams.playlist_locale_pack_id AS locale_pack_id,
    true AS started,
    (count(DISTINCT
        CASE
            WHEN (lesson_streams_progress.completed_at IS NOT NULL) THEN lesson_streams_progress.locale_pack_id
            ELSE NULL::uuid
        END) = published_playlists.stream_count) AS completed
   FROM (((public.lesson_streams_progress
     JOIN public.published_playlist_streams ON ((published_playlist_streams.stream_locale_pack_id = lesson_streams_progress.locale_pack_id)))
     JOIN public.published_playlists ON ((published_playlists.locale_pack_id = published_playlist_streams.playlist_locale_pack_id)))
     JOIN public.user_can_see_playlist_locale_packs ON (((published_playlists.locale_pack_id = user_can_see_playlist_locale_packs.locale_pack_id) AND (lesson_streams_progress.user_id = user_can_see_playlist_locale_packs.user_id))))
  GROUP BY lesson_streams_progress.user_id, published_playlist_streams.playlist_locale_pack_id, published_playlists.stream_count
  WITH NO DATA;


--
-- Name: user_participation_scores; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_participation_scores (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    user_id uuid NOT NULL,
    score double precision NOT NULL
);


--
-- Name: user_project_scores; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_project_scores (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    user_id uuid NOT NULL,
    score double precision NOT NULL
);


--
-- Name: cohort_user_progress_records; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.cohort_user_progress_records AS
 WITH current_status AS (
         SELECT cohort_applications_plus.cohort_id,
            cohort_applications_plus.user_id,
            cohort_applications_plus.cohort_name,
            published_cohorts.program_type,
            published_cohorts.start_date,
            cohort_status_changes.status
           FROM ((public.cohort_applications_plus
             JOIN public.published_cohorts ON ((published_cohorts.id = cohort_applications_plus.cohort_id)))
             JOIN public.cohort_status_changes ON (((cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id) AND (cohort_status_changes.user_id = cohort_applications_plus.user_id) AND (cohort_status_changes.until_time = '2099-01-01 00:00:00'::timestamp without time zone))))
          WHERE ((cohort_applications_plus.was_accepted = true) AND (published_cohorts.program_type <> 'career_network_only'::text))
        ), specialization_test_scores AS (
         SELECT current_status.user_id,
            current_status.cohort_id,
            playlists.playlist_title,
            lessons.stream_title,
            lessons.stream_locale_pack_id,
            cohorts_versions.num_required_specializations,
            array_agg(lessons.lesson_title) AS lessons,
            array_agg(lessons.lesson_locale_pack_id) AS lesson_locale_pack_ids,
            avg(lesson_progress.official_test_score) AS avg_test_score
           FROM (((((((current_status
             JOIN public.published_cohorts ON ((published_cohorts.id = current_status.cohort_id)))
             JOIN public.cohorts_versions ON (((published_cohorts.version_id = cohorts_versions.version_id) AND (cohorts_versions.num_required_specializations > 0))))
             JOIN public.published_cohort_playlist_locale_packs playlists ON (((playlists.cohort_id = current_status.cohort_id) AND (playlists.specialization = true))))
             JOIN public.published_playlist_streams ON ((published_playlist_streams.playlist_locale_pack_id = playlists.playlist_locale_pack_id)))
             JOIN public.published_stream_lesson_locale_packs lessons ON (((lessons.stream_locale_pack_id = published_playlist_streams.stream_locale_pack_id) AND (lessons.test = true))))
             JOIN public.lesson_streams_progress ON (((lesson_streams_progress.locale_pack_id = lessons.stream_locale_pack_id) AND (lesson_streams_progress.user_id = current_status.user_id) AND (lesson_streams_progress.completed_at IS NOT NULL))))
             JOIN public.user_lesson_progress_records lesson_progress ON (((lesson_progress.locale_pack_id = lessons.lesson_locale_pack_id) AND (lesson_progress.user_id = current_status.user_id))))
          GROUP BY current_status.user_id, current_status.cohort_id, playlists.playlist_title, lessons.stream_title, lessons.stream_locale_pack_id, cohorts_versions.num_required_specializations
        ), ranked_specialization_test_scores AS (
         SELECT specialization_test_scores.user_id,
            specialization_test_scores.cohort_id,
            specialization_test_scores.playlist_title,
            specialization_test_scores.stream_title,
            specialization_test_scores.stream_locale_pack_id,
            specialization_test_scores.num_required_specializations,
            specialization_test_scores.lessons,
            specialization_test_scores.lesson_locale_pack_ids,
            specialization_test_scores.avg_test_score,
            rank() OVER (PARTITION BY specialization_test_scores.cohort_id, specialization_test_scores.user_id ORDER BY specialization_test_scores.avg_test_score DESC) AS rank
           FROM specialization_test_scores
          ORDER BY specialization_test_scores.cohort_id, specialization_test_scores.user_id, (rank() OVER (PARTITION BY specialization_test_scores.cohort_id, specialization_test_scores.user_id ORDER BY specialization_test_scores.avg_test_score DESC))
        ), test_scores_to_count AS (
         SELECT ranked_specialization_test_scores.cohort_id,
            ranked_specialization_test_scores.user_id,
            ranked_specialization_test_scores.avg_test_score AS official_test_score,
            unnest(ranked_specialization_test_scores.lesson_locale_pack_ids) AS lesson_locale_pack_id
           FROM ranked_specialization_test_scores
          WHERE (ranked_specialization_test_scores.rank <= ranked_specialization_test_scores.num_required_specializations)
        UNION
         SELECT lesson_progress.cohort_id,
            lesson_progress.user_id,
            lesson_progress.official_test_score,
            lesson_progress.lesson_locale_pack_id
           FROM (public.cohort_applications_plus
             LEFT JOIN public.cohort_user_lesson_progress_records lesson_progress ON (((cohort_applications_plus.cohort_id = lesson_progress.cohort_id) AND (cohort_applications_plus.user_id = lesson_progress.user_id))))
          WHERE (lesson_progress.test AND lesson_progress.required)
        ), test_scores AS (
         SELECT test_scores_to_count.cohort_id,
            test_scores_to_count.user_id,
            avg(test_scores_to_count.official_test_score) AS avg_test_score
           FROM test_scores_to_count
          GROUP BY test_scores_to_count.cohort_id, test_scores_to_count.user_id
        ), lesson_counts AS (
         SELECT lesson_progress.cohort_id,
            lesson_progress.user_id,
            sum(
                CASE
                    WHEN ((lesson_progress.foundations = true) AND (lesson_progress.completed_at IS NOT NULL)) THEN 1
                    ELSE 0
                END) AS foundations_lessons_complete,
            sum(
                CASE
                    WHEN ((lesson_progress.required = true) AND (lesson_progress.completed_at IS NOT NULL)) THEN 1
                    ELSE 0
                END) AS required_lessons_complete,
            sum(
                CASE
                    WHEN ((lesson_progress.test = true) AND (lesson_progress.completed_at IS NOT NULL)) THEN 1
                    ELSE 0
                END) AS test_lessons_complete,
            sum(
                CASE
                    WHEN ((lesson_progress.elective = true) AND (lesson_progress.completed_at IS NOT NULL)) THEN 1
                    ELSE 0
                END) AS elective_lessons_complete,
            avg(
                CASE
                    WHEN lesson_progress.assessment THEN lesson_progress.average_assessment_score_first
                    ELSE NULL::double precision
                END) AS average_assessment_score_first,
            avg(
                CASE
                    WHEN lesson_progress.assessment THEN lesson_progress.average_assessment_score_best
                    ELSE NULL::double precision
                END) AS average_assessment_score_best,
            min(
                CASE
                    WHEN lesson_progress.assessment THEN lesson_progress.average_assessment_score_best
                    ELSE NULL::double precision
                END) AS min_assessment_score_best
           FROM (public.cohort_applications_plus
             LEFT JOIN public.cohort_user_lesson_progress_records lesson_progress ON (((cohort_applications_plus.cohort_id = lesson_progress.cohort_id) AND (cohort_applications_plus.user_id = lesson_progress.user_id))))
          WHERE (cohort_applications_plus.was_accepted = true)
          GROUP BY lesson_progress.cohort_id, lesson_progress.user_id
        ), stream_counts AS (
         SELECT cohort_applications_plus.cohort_id,
            cohort_applications_plus.user_id,
            sum(
                CASE
                    WHEN ((published_cohort_stream_locale_packs.foundations = true) AND (lesson_streams_progress.completed_at IS NOT NULL)) THEN 1
                    ELSE 0
                END) AS foundations_streams_complete,
            sum(
                CASE
                    WHEN ((published_cohort_stream_locale_packs.required = true) AND (lesson_streams_progress.completed_at IS NOT NULL)) THEN 1
                    ELSE 0
                END) AS required_streams_complete,
            sum(
                CASE
                    WHEN ((published_cohort_stream_locale_packs.exam = true) AND (lesson_streams_progress.completed_at IS NOT NULL)) THEN 1
                    ELSE 0
                END) AS exam_streams_complete,
            sum(
                CASE
                    WHEN ((published_cohort_stream_locale_packs.elective = true) AND (lesson_streams_progress.completed_at IS NOT NULL)) THEN 1
                    ELSE 0
                END) AS elective_streams_complete
           FROM ((public.cohort_applications_plus
             JOIN public.published_cohort_stream_locale_packs ON ((published_cohort_stream_locale_packs.cohort_id = cohort_applications_plus.cohort_id)))
             LEFT JOIN public.lesson_streams_progress ON (((published_cohort_stream_locale_packs.stream_locale_pack_id = lesson_streams_progress.locale_pack_id) AND (cohort_applications_plus.user_id = lesson_streams_progress.user_id))))
          WHERE (cohort_applications_plus.was_accepted = true)
          GROUP BY cohort_applications_plus.cohort_id, cohort_applications_plus.user_id
        ), playlist_counts AS (
         SELECT cohort_applications_plus.cohort_id,
            cohort_applications_plus.user_id,
            sum(
                CASE
                    WHEN ((published_cohort_playlist_locale_packs.required = true) AND (playlist_progress.completed = true)) THEN 1
                    ELSE 0
                END) AS required_playlists_complete,
            sum(
                CASE
                    WHEN ((published_cohort_playlist_locale_packs.specialization = true) AND (playlist_progress.completed = true)) THEN 1
                    ELSE 0
                END) AS specialization_playlists_complete
           FROM ((public.cohort_applications_plus
             JOIN public.published_cohort_playlist_locale_packs ON ((published_cohort_playlist_locale_packs.cohort_id = cohort_applications_plus.cohort_id)))
             LEFT JOIN public.playlist_progress ON (((published_cohort_playlist_locale_packs.playlist_locale_pack_id = playlist_progress.locale_pack_id) AND (cohort_applications_plus.user_id = playlist_progress.user_id))))
          WHERE (cohort_applications_plus.was_accepted = true)
          GROUP BY cohort_applications_plus.cohort_id, cohort_applications_plus.user_id
        ), with_counts AS (
         SELECT current_status.cohort_name,
            current_status.cohort_id,
            current_status.program_type,
            current_status.start_date,
            current_status.user_id,
            current_status.status,
            stream_counts.required_streams_complete,
            stream_counts.foundations_streams_complete,
            stream_counts.exam_streams_complete,
            stream_counts.elective_streams_complete,
            playlist_counts.required_playlists_complete,
            playlist_counts.specialization_playlists_complete,
            lesson_counts.required_lessons_complete,
            lesson_counts.foundations_lessons_complete,
            lesson_counts.test_lessons_complete,
            lesson_counts.elective_lessons_complete,
            lesson_counts.average_assessment_score_first,
            lesson_counts.average_assessment_score_best,
            lesson_counts.min_assessment_score_best,
            test_scores.avg_test_score
           FROM ((((current_status
             JOIN lesson_counts ON (((current_status.cohort_id = lesson_counts.cohort_id) AND (current_status.user_id = lesson_counts.user_id))))
             JOIN stream_counts ON (((current_status.cohort_id = stream_counts.cohort_id) AND (current_status.user_id = stream_counts.user_id))))
             JOIN playlist_counts ON (((current_status.cohort_id = playlist_counts.cohort_id) AND (current_status.user_id = playlist_counts.user_id))))
             JOIN test_scores ON (((current_status.cohort_id = test_scores.cohort_id) AND (current_status.user_id = test_scores.user_id))))
        ), with_averages AS (
         SELECT with_counts.cohort_name,
            with_counts.cohort_id,
            with_counts.program_type,
            with_counts.start_date,
            with_counts.user_id,
            with_counts.status,
            with_counts.required_streams_complete,
            with_counts.foundations_streams_complete,
            with_counts.exam_streams_complete,
            with_counts.elective_streams_complete,
            with_counts.required_playlists_complete,
            with_counts.specialization_playlists_complete,
            with_counts.required_lessons_complete,
            with_counts.foundations_lessons_complete,
            with_counts.test_lessons_complete,
            with_counts.elective_lessons_complete,
            with_counts.average_assessment_score_first,
            with_counts.average_assessment_score_best,
            with_counts.min_assessment_score_best,
            with_counts.avg_test_score,
            COALESCE(user_participation_scores.score, (0)::double precision) AS participation_score,
            COALESCE(user_project_scores.score, (0)::double precision) AS project_score,
            COALESCE(cohort_applications_plus.final_score,
                CASE
                    WHEN (with_counts.cohort_name = 'MBA1'::text) THEN (((0.7)::double precision *
                    CASE
                        WHEN (with_counts.avg_test_score IS NULL) THEN (0)::double precision
                        ELSE with_counts.avg_test_score
                    END) + ((0.3)::double precision *
                    CASE
                        WHEN (with_counts.average_assessment_score_best IS NULL) THEN (0)::double precision
                        ELSE with_counts.average_assessment_score_best
                    END))
                    WHEN ((with_counts.program_type = 'mba'::text) AND (with_counts.start_date < ( SELECT published_cohorts.start_date
                       FROM public.published_cohorts
                      WHERE (published_cohorts.name = 'MBA8'::text)))) THEN ((((0.7)::double precision *
                    CASE
                        WHEN (with_counts.avg_test_score IS NULL) THEN (0)::double precision
                        ELSE with_counts.avg_test_score
                    END) + ((0.2)::double precision *
                    CASE
                        WHEN (with_counts.average_assessment_score_best IS NULL) THEN (0)::double precision
                        ELSE with_counts.average_assessment_score_best
                    END)) + ((0.1)::double precision * COALESCE(user_participation_scores.score, (0)::double precision)))
                    WHEN (with_counts.program_type = 'mba'::text) THEN ((((0.7)::double precision *
                    CASE
                        WHEN (with_counts.avg_test_score IS NULL) THEN (0)::double precision
                        ELSE with_counts.avg_test_score
                    END) + ((0.2)::double precision *
                    CASE
                        WHEN (with_counts.average_assessment_score_best IS NULL) THEN (0)::double precision
                        ELSE with_counts.average_assessment_score_best
                    END)) + ((0.1)::double precision * COALESCE(user_project_scores.score, (0)::double precision)))
                    WHEN ((with_counts.program_type = 'emba'::text) AND (with_counts.start_date < ( SELECT published_cohorts.start_date
                       FROM public.published_cohorts
                      WHERE (published_cohorts.name = 'EMBA4'::text)))) THEN (((((0.6)::double precision *
                    CASE
                        WHEN (with_counts.avg_test_score IS NULL) THEN (0)::double precision
                        ELSE with_counts.avg_test_score
                    END) + ((0.2)::double precision *
                    CASE
                        WHEN (with_counts.average_assessment_score_best IS NULL) THEN (0)::double precision
                        ELSE with_counts.average_assessment_score_best
                    END)) + ((0.1)::double precision * COALESCE(user_project_scores.score, (0)::double precision))) + ((0.1)::double precision * COALESCE(user_participation_scores.score, (0)::double precision)))
                    WHEN (with_counts.program_type = 'emba'::text) THEN ((((0.6)::double precision *
                    CASE
                        WHEN (with_counts.avg_test_score IS NULL) THEN (0)::double precision
                        ELSE with_counts.avg_test_score
                    END) + ((0.2)::double precision *
                    CASE
                        WHEN (with_counts.average_assessment_score_best IS NULL) THEN (0)::double precision
                        ELSE with_counts.average_assessment_score_best
                    END)) + ((0.2)::double precision * COALESCE(user_project_scores.score, (0)::double precision)))
                    WHEN public.is_paid_cert_program_type(with_counts.program_type) THEN NULL::double precision
                    ELSE NULL::double precision
                END) AS final_score,
            ((with_counts.required_streams_complete)::double precision / (cohort_content_details.required_stream_count)::double precision) AS required_streams_perc_complete,
            ((with_counts.required_lessons_complete)::double precision / (cohort_content_details.required_lesson_count)::double precision) AS required_lessons_perc_complete,
                CASE
                    WHEN (cohort_content_details.elective_lesson_count > 0) THEN ((with_counts.elective_lessons_complete)::double precision / (cohort_content_details.elective_lesson_count)::double precision)
                    ELSE NULL::double precision
                END AS elective_lessons_perc_complete,
                CASE
                    WHEN (cohort_content_details.elective_stream_count > 0) THEN ((with_counts.elective_streams_complete)::double precision / (cohort_content_details.elective_stream_count)::double precision)
                    ELSE NULL::double precision
                END AS elective_streams_perc_complete
           FROM ((((with_counts
             JOIN public.published_cohort_content_details cohort_content_details ON ((with_counts.cohort_id = cohort_content_details.cohort_id)))
             LEFT JOIN public.user_participation_scores ON ((user_participation_scores.user_id = with_counts.user_id)))
             LEFT JOIN public.user_project_scores ON ((user_project_scores.user_id = with_counts.user_id)))
             LEFT JOIN public.cohort_applications_plus ON (((cohort_applications_plus.cohort_id = with_counts.cohort_id) AND (cohort_applications_plus.user_id = with_counts.user_id))))
        ), with_grad_requirements AS (
         SELECT with_averages.cohort_name,
            with_averages.cohort_id,
            with_averages.program_type,
            with_averages.start_date,
            with_averages.user_id,
            with_averages.status,
            with_averages.required_streams_complete,
            with_averages.foundations_streams_complete,
            with_averages.exam_streams_complete,
            with_averages.elective_streams_complete,
            with_averages.required_playlists_complete,
            with_averages.specialization_playlists_complete,
            with_averages.required_lessons_complete,
            with_averages.foundations_lessons_complete,
            with_averages.test_lessons_complete,
            with_averages.elective_lessons_complete,
            with_averages.average_assessment_score_first,
            with_averages.average_assessment_score_best,
            with_averages.min_assessment_score_best,
            with_averages.avg_test_score,
            with_averages.participation_score,
            with_averages.project_score,
            with_averages.final_score,
            with_averages.required_streams_perc_complete,
            with_averages.required_lessons_perc_complete,
            with_averages.elective_lessons_perc_complete,
            with_averages.elective_streams_perc_complete,
                CASE
                    WHEN (with_averages.program_type = ANY (ARRAY['mba'::text, 'emba'::text, 'the_business_certificate'::text])) THEN ((with_averages.final_score > (0.7)::double precision) AND (with_averages.required_streams_perc_complete = (1)::double precision) AND (with_averages.specialization_playlists_complete >= cohorts_versions.num_required_specializations))
                    WHEN public.is_paid_cert_program_type(with_averages.program_type) THEN ((with_averages.min_assessment_score_best > (0.8)::double precision) AND (with_averages.required_streams_perc_complete = (1)::double precision) AND (with_averages.project_score >= (3)::double precision))
                    ELSE NULL::boolean
                END AS meets_graduation_requirements
           FROM ((with_averages
             JOIN public.published_cohorts ON ((with_averages.cohort_id = published_cohorts.id)))
             JOIN public.cohorts_versions ON ((published_cohorts.version_id = cohorts_versions.version_id)))
        )
 SELECT with_grad_requirements.cohort_name,
    with_grad_requirements.cohort_id,
    with_grad_requirements.program_type,
    with_grad_requirements.start_date,
    with_grad_requirements.user_id,
    with_grad_requirements.status,
    with_grad_requirements.required_streams_complete,
    with_grad_requirements.foundations_streams_complete,
    with_grad_requirements.exam_streams_complete,
    with_grad_requirements.elective_streams_complete,
    with_grad_requirements.required_playlists_complete,
    with_grad_requirements.specialization_playlists_complete,
    with_grad_requirements.required_lessons_complete,
    with_grad_requirements.foundations_lessons_complete,
    with_grad_requirements.test_lessons_complete,
    with_grad_requirements.elective_lessons_complete,
    with_grad_requirements.average_assessment_score_first,
    with_grad_requirements.average_assessment_score_best,
    with_grad_requirements.min_assessment_score_best,
    with_grad_requirements.avg_test_score,
    with_grad_requirements.participation_score,
    with_grad_requirements.project_score,
    with_grad_requirements.final_score,
    with_grad_requirements.required_streams_perc_complete,
    with_grad_requirements.required_lessons_perc_complete,
    with_grad_requirements.elective_lessons_perc_complete,
    with_grad_requirements.elective_streams_perc_complete,
    with_grad_requirements.meets_graduation_requirements
   FROM with_grad_requirements
  WITH NO DATA;


--
-- Name: program_enrollment_progress_records; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.program_enrollment_progress_records AS
 WITH original_enrollments_1 AS (
         SELECT cohort_status_changes.user_id,
            published_cohorts.program_type,
            min(cohort_status_changes.from_time) AS enrolled_at,
            (array_agg(cohort_status_changes.cohort_id ORDER BY cohort_status_changes.from_time))[1] AS original_cohort_id
           FROM (public.cohort_status_changes
             JOIN public.published_cohorts ON ((published_cohorts.id = cohort_status_changes.cohort_id)))
          WHERE (cohort_status_changes.status = 'enrolled'::text)
          GROUP BY cohort_status_changes.user_id, published_cohorts.program_type
        ), original_enrollments_2 AS (
         SELECT first_enrollment.user_id,
            first_enrollment.program_type,
            first_enrollment.enrolled_at,
            first_enrollment.original_cohort_id,
            later_enrollment.program_type AS pt2,
            later_enrollment.enrolled_at AS e2
           FROM (original_enrollments_1 first_enrollment
             LEFT JOIN original_enrollments_1 later_enrollment ON (((first_enrollment.user_id = later_enrollment.user_id) AND (first_enrollment.enrolled_at < later_enrollment.enrolled_at))))
          WHERE (later_enrollment.program_type IS NULL)
        ), final_statuses_1 AS (
         SELECT original_enrollments.user_id,
            original_enrollments.original_cohort_id,
            original_enrollments.program_type,
            count(*) AS cohort_count,
            (array_agg(cohort_user_progress_records.cohort_id ORDER BY published_cohorts.start_date DESC))[1] AS final_cohort_id,
            (array_agg(cohort_user_progress_records.status ORDER BY published_cohorts.start_date DESC))[1] AS final_status
           FROM ((original_enrollments_2 original_enrollments
             JOIN public.cohort_user_progress_records ON ((cohort_user_progress_records.user_id = original_enrollments.user_id)))
             JOIN public.published_cohorts ON ((cohort_user_progress_records.cohort_id = published_cohorts.id)))
          WHERE (published_cohorts.program_type = original_enrollments.program_type)
          GROUP BY original_enrollments.user_id, original_enrollments.original_cohort_id, original_enrollments.program_type
        ), final_statuses_2 AS (
         SELECT final_statuses_1.user_id,
            final_statuses_1.program_type,
            final_statuses_1.original_cohort_id,
            final_statuses_1.final_cohort_id,
            ((final_statuses_1.original_cohort_id <> final_statuses_1.final_cohort_id) OR (final_statuses_1.final_status = 'deferred'::text)) AS deferred_out_of_original_cohort,
                CASE
                    WHEN ((final_statuses_1.final_status = 'deferred'::text) AND (original_cohorts.start_date < (now() - '2 years'::interval))) THEN 'did_not_graduate'::text
                    WHEN (final_statuses_1.final_status = ANY (ARRAY['expelled'::text, 'failed'::text, 'did_not_enroll'::text, 'rejected'::text])) THEN 'did_not_graduate'::text
                    ELSE final_statuses_1.final_status
                END AS final_status
           FROM (final_statuses_1
             JOIN public.published_cohorts original_cohorts ON ((original_cohorts.id = final_statuses_1.original_cohort_id)))
        )
 SELECT final_statuses_2.user_id,
    final_statuses_2.program_type,
    final_statuses_2.original_cohort_id,
    final_statuses_2.final_cohort_id,
    final_statuses_2.deferred_out_of_original_cohort,
    final_statuses_2.final_status
   FROM final_statuses_2
  WITH NO DATA;


--
-- Name: user_chance_of_graduating_records; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_chance_of_graduating_records (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_id uuid NOT NULL,
    program_type text NOT NULL,
    chance_of_graduating double precision NOT NULL
);


--
-- Name: cohort_conversion_rates; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.cohort_conversion_rates AS
 WITH counts_from_this_cohort AS (
         SELECT aps.cohort_id,
            count(DISTINCT aps.id) AS num_applied,
            count(
                CASE
                    WHEN aps.was_accepted THEN true
                    ELSE NULL::boolean
                END) AS num_accepted,
            count(
                CASE
                    WHEN (cohort_user_progress_records.status = ANY (ARRAY['enrolled'::text, 'graduated'::text, 'failed'::text, 'expelled'::text])) THEN true
                    ELSE NULL::boolean
                END) AS num_enrolled,
            count(
                CASE
                    WHEN (cohort_user_progress_records.status = ANY (ARRAY['enrolled'::text, 'pending_enrollment'::text])) THEN true
                    ELSE NULL::boolean
                END) AS num_active,
            count(
                CASE
                    WHEN (cohort_user_progress_records.status = 'deferred'::text) THEN true
                    ELSE NULL::boolean
                END) AS num_deferred,
            count(
                CASE
                    WHEN cohort_user_progress_records.meets_graduation_requirements THEN true
                    ELSE NULL::boolean
                END) AS num_meets_grad_req
           FROM (public.cohort_applications_plus aps
             LEFT JOIN public.cohort_user_progress_records ON (((aps.user_id = cohort_user_progress_records.user_id) AND (aps.cohort_id = cohort_user_progress_records.cohort_id))))
          GROUP BY aps.cohort_id
        ), enrollment_records_with_chance_of_graduating AS (
         SELECT enrollment_records.user_id,
            enrollment_records.program_type,
            enrollment_records.original_cohort_id,
            enrollment_records.final_cohort_id,
            enrollment_records.deferred_out_of_original_cohort,
            enrollment_records.final_status,
                CASE
                    WHEN (user_chance_of_graduating_records.chance_of_graduating IS NOT NULL) THEN user_chance_of_graduating_records.chance_of_graduating
                    WHEN (enrollment_records.final_status = 'graduated'::text) THEN (1)::double precision
                    WHEN (enrollment_records.final_status = 'did_not_graduate'::text) THEN (0)::double precision
                    ELSE NULL::double precision
                END AS chance_of_graduating
           FROM (public.program_enrollment_progress_records enrollment_records
             LEFT JOIN public.user_chance_of_graduating_records ON (((user_chance_of_graduating_records.user_id = enrollment_records.user_id) AND (user_chance_of_graduating_records.program_type = enrollment_records.program_type))))
        ), counts_from_users_who_first_enrolled_in_this_cohort AS (
         SELECT enrollment_records.original_cohort_id,
            count(*) AS num_enrolled,
            count(
                CASE
                    WHEN (enrollment_records.final_status = 'graduated'::text) THEN true
                    ELSE NULL::boolean
                END) AS num_graduated,
            count(
                CASE
                    WHEN (enrollment_records.final_status = 'did_not_graduate'::text) THEN true
                    ELSE NULL::boolean
                END) AS num_did_not_graduate,
            count(
                CASE
                    WHEN (enrollment_records.final_status = 'deferred'::text) THEN true
                    ELSE NULL::boolean
                END) AS num_currently_deferred,
            count(
                CASE
                    WHEN ((enrollment_records.original_cohort_id = enrollment_records.final_cohort_id) AND (enrollment_records.final_status = ANY (ARRAY['enrolled'::text, 'pending_enrollment'::text]))) THEN true
                    ELSE NULL::boolean
                END) AS num_currently_enrolled_in_orig_cohort,
            count(
                CASE
                    WHEN ((enrollment_records.original_cohort_id <> enrollment_records.final_cohort_id) AND (enrollment_records.final_status = ANY (ARRAY['enrolled'::text, 'pending_enrollment'::text]))) THEN true
                    ELSE NULL::boolean
                END) AS num_currently_enrolled_in_later_cohort,
            avg(enrollment_records.chance_of_graduating) AS expected_grad_rate,
            avg(
                CASE
                    WHEN (enrollment_records.chance_of_graduating IS NULL) THEN 1
                    WHEN (enrollment_records.chance_of_graduating > (0)::double precision) THEN 1
                    ELSE 0
                END) AS max_grad_rate,
            avg(
                CASE
                    WHEN (enrollment_records.chance_of_graduating IS NULL) THEN 0
                    WHEN (enrollment_records.chance_of_graduating < (1)::double precision) THEN 0
                    ELSE 1
                END) AS min_grad_rate
           FROM enrollment_records_with_chance_of_graduating enrollment_records
          GROUP BY enrollment_records.original_cohort_id
        ), step_1 AS (
         SELECT counts_from_this_cohort.cohort_id,
            published_cohorts.program_type,
            published_cohorts.name AS cohort_name,
            counts_from_this_cohort.num_applied,
            counts_from_this_cohort.num_accepted,
            counts_from_this_cohort.num_enrolled,
            counts_from_this_cohort.num_active,
            counts_from_this_cohort.num_meets_grad_req,
            counts_from_this_cohort.num_deferred,
            counts_from_users_who_first_enrolled_in_this_cohort.num_enrolled AS num_first_enrolled,
            counts_from_users_who_first_enrolled_in_this_cohort.num_graduated,
            counts_from_users_who_first_enrolled_in_this_cohort.num_did_not_graduate,
            counts_from_users_who_first_enrolled_in_this_cohort.num_currently_deferred,
            counts_from_users_who_first_enrolled_in_this_cohort.num_currently_enrolled_in_orig_cohort,
            counts_from_users_who_first_enrolled_in_this_cohort.num_currently_enrolled_in_later_cohort,
            counts_from_users_who_first_enrolled_in_this_cohort.expected_grad_rate,
            counts_from_users_who_first_enrolled_in_this_cohort.min_grad_rate,
            counts_from_users_who_first_enrolled_in_this_cohort.max_grad_rate
           FROM ((counts_from_this_cohort
             JOIN counts_from_users_who_first_enrolled_in_this_cohort ON ((counts_from_users_who_first_enrolled_in_this_cohort.original_cohort_id = counts_from_this_cohort.cohort_id)))
             JOIN public.published_cohorts ON ((published_cohorts.id = counts_from_this_cohort.cohort_id)))
          WHERE (published_cohorts.program_type = ANY (ARRAY['mba'::text, 'emba'::text]))
          ORDER BY published_cohorts.program_type, published_cohorts.start_date
        ), step_2 AS (
         SELECT step_1.cohort_id,
            step_1.program_type,
            step_1.cohort_name,
                CASE
                    WHEN (step_1.num_applied = 0) THEN NULL::double precision
                    ELSE ((step_1.num_accepted)::double precision / (step_1.num_applied)::double precision)
                END AS acceptance_rate,
                CASE
                    WHEN ((step_1.num_accepted - step_1.num_deferred) = 0) THEN NULL::double precision
                    ELSE ((step_1.num_enrolled)::double precision / ((step_1.num_accepted - step_1.num_deferred))::double precision)
                END AS yield,
            step_1.expected_grad_rate,
            step_1.min_grad_rate,
            step_1.max_grad_rate,
                CASE
                    WHEN (step_1.num_enrolled = 0) THEN NULL::double precision
                    ELSE ((step_1.num_meets_grad_req)::double precision / (step_1.num_enrolled)::double precision)
                END AS meets_grad_req_rate,
            step_1.num_applied,
            step_1.num_accepted,
            step_1.num_enrolled,
            step_1.num_active,
            step_1.num_deferred,
            step_1.num_meets_grad_req,
            step_1.num_first_enrolled,
            step_1.num_graduated,
            step_1.num_did_not_graduate,
            step_1.num_currently_deferred,
            step_1.num_currently_enrolled_in_orig_cohort,
            step_1.num_currently_enrolled_in_later_cohort
           FROM step_1
        )
 SELECT step_2.cohort_id,
    step_2.program_type,
    step_2.cohort_name,
    step_2.acceptance_rate,
    step_2.yield,
    step_2.expected_grad_rate,
    step_2.min_grad_rate,
    step_2.max_grad_rate,
    step_2.meets_grad_req_rate,
    step_2.num_applied,
    step_2.num_accepted,
    step_2.num_enrolled,
    step_2.num_active,
    step_2.num_deferred,
    step_2.num_meets_grad_req,
    step_2.num_first_enrolled,
    step_2.num_graduated,
    step_2.num_did_not_graduate,
    step_2.num_currently_deferred,
    step_2.num_currently_enrolled_in_orig_cohort,
    step_2.num_currently_enrolled_in_later_cohort
   FROM step_2
  WITH NO DATA;


--
-- Name: cohort_period_graduation_rate_predictions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cohort_period_graduation_rate_predictions (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    chance_of_success double precision NOT NULL,
    cohort_end timestamp without time zone NOT NULL,
    cohort_id uuid NOT NULL,
    cohort_name text NOT NULL,
    cohorts_with_similar_periods_count integer,
    current_position text,
    error double precision,
    final_result double precision,
    index integer,
    normalized_index text NOT NULL,
    num_active integer NOT NULL,
    num_enrolled integer NOT NULL,
    num_failed integer NOT NULL,
    num_graduated integer NOT NULL,
    period_end timestamp without time zone,
    similar_periods json,
    similar_periods_count integer
);


--
-- Name: cohort_promotions_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cohort_promotions_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    cohort_id uuid NOT NULL
);


--
-- Name: cohort_user_periods; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.cohort_user_periods AS
 WITH lesson_progress_records AS (
         SELECT cohort_user_lesson_progress_records.user_id,
            cohort_user_lesson_progress_records.cohort_id,
            cohort_user_lesson_progress_records.cohort_name,
            cohort_user_lesson_progress_records.completed_at,
            completed_in_periods.index AS completed_in_period_index,
            cohort_user_lesson_progress_records.lesson_locale_pack_id
           FROM (public.cohort_user_lesson_progress_records
             JOIN public.published_cohort_periods completed_in_periods ON (((cohort_user_lesson_progress_records.cohort_id = completed_in_periods.cohort_id) AND ((cohort_user_lesson_progress_records.completed_at >= completed_in_periods.period_start) AND (cohort_user_lesson_progress_records.completed_at <= completed_in_periods.period_end)))))
          WHERE ((cohort_user_lesson_progress_records.completed_at IS NOT NULL) AND (cohort_user_lesson_progress_records.required = true))
        ), with_lesson_info AS (
         SELECT cohort_applications_plus.user_id,
            cohort_applications_plus.cohort_id,
            cohort_applications_plus.cohort_name,
            published_cohort_periods.index,
            count(DISTINCT lesson_progress_records.lesson_locale_pack_id) AS required_lessons_completed_in_period,
            published_cohort_periods.period_start,
            published_cohort_periods.period_end,
            array_agg(DISTINCT lesson_progress_records.lesson_locale_pack_id) AS required_lesson_pack_ids_completed_in_period
           FROM ((public.cohort_applications_plus
             JOIN public.published_cohort_periods ON ((cohort_applications_plus.cohort_id = published_cohort_periods.cohort_id)))
             LEFT JOIN lesson_progress_records ON (((published_cohort_periods.cohort_id = lesson_progress_records.cohort_id) AND (cohort_applications_plus.user_id = lesson_progress_records.user_id) AND (published_cohort_periods.index = lesson_progress_records.completed_in_period_index))))
          WHERE ((cohort_applications_plus.was_accepted = true) AND (published_cohort_periods.period_end < now()))
          GROUP BY cohort_applications_plus.user_id, cohort_applications_plus.cohort_id, cohort_applications_plus.cohort_name, published_cohort_periods.index, published_cohort_periods.period_start, published_cohort_periods.period_end
        ), with_status_info AS (
         SELECT with_lesson_info.user_id,
            with_lesson_info.cohort_id,
            with_lesson_info.cohort_name,
            with_lesson_info.index,
            with_lesson_info.required_lessons_completed_in_period,
            with_lesson_info.period_start,
            with_lesson_info.period_end,
            with_lesson_info.required_lesson_pack_ids_completed_in_period,
            COALESCE(cohort_status_changes.status, 'not_yet_applied'::text) AS status_at_period_end
           FROM (with_lesson_info
             LEFT JOIN public.cohort_status_changes ON (((cohort_status_changes.cohort_id = with_lesson_info.cohort_id) AND (cohort_status_changes.user_id = with_lesson_info.user_id) AND (cohort_status_changes.from_time <= with_lesson_info.period_end) AND (cohort_status_changes.until_time > with_lesson_info.period_end))))
        ), last_4_standard_periods AS (
         SELECT this_period.cohort_id,
            this_period.index,
            this_period.style,
            ( SELECT min(a.index) AS min
                   FROM ( SELECT prev_standard_periods.index
                           FROM public.published_cohort_periods prev_standard_periods
                          WHERE ((prev_standard_periods.cohort_id = this_period.cohort_id) AND (prev_standard_periods.index <= this_period.index) AND (prev_standard_periods.style = 'standard'::text))
                          ORDER BY prev_standard_periods.index DESC
                         LIMIT 4) a) AS four_standard_periods_ago_index
           FROM public.published_cohort_periods this_period
        ), cumululative_data AS (
         SELECT user_periods.user_id,
            user_periods.cohort_id,
            user_periods.index,
            sum(prev_periods.required_lessons_completed_in_period) AS cumulative_required_lessons_completed,
            sum(
                CASE
                    WHEN (prev_periods.index >= last_4_standard_periods.four_standard_periods_ago_index) THEN prev_periods.required_lessons_completed_in_period
                    ELSE (0)::bigint
                END) AS cumulative_required_lessons_completed_in_last_4_periods,
            COALESCE(array_length(public.array_intersect(published_cohort_periods.cumulative_required_lesson_pack_ids, public.array_cat(DISTINCT prev_periods.required_lesson_pack_ids_completed_in_period)), 1), 0) AS cumulative_required_up_to_now_lessons_completed
           FROM (((with_status_info user_periods
             JOIN public.published_cohort_periods ON (((published_cohort_periods.cohort_id = user_periods.cohort_id) AND (published_cohort_periods.index = user_periods.index))))
             JOIN last_4_standard_periods ON (((last_4_standard_periods.cohort_id = user_periods.cohort_id) AND (last_4_standard_periods.index = user_periods.index))))
             LEFT JOIN with_status_info prev_periods ON (((user_periods.user_id = prev_periods.user_id) AND (user_periods.cohort_id = prev_periods.cohort_id) AND (user_periods.index >= prev_periods.index))))
          GROUP BY user_periods.user_id, user_periods.cohort_id, user_periods.index, published_cohort_periods.cumulative_required_lesson_pack_ids
        ), with_cumulative_data AS (
         SELECT with_status_info.user_id,
            with_status_info.cohort_id,
            with_status_info.cohort_name,
            with_status_info.index,
            with_status_info.required_lessons_completed_in_period,
            with_status_info.period_start,
            with_status_info.period_end,
            with_status_info.required_lesson_pack_ids_completed_in_period,
            with_status_info.status_at_period_end,
            cumululative_data.cumulative_required_lessons_completed,
            cumululative_data.cumulative_required_up_to_now_lessons_completed,
            cumululative_data.cumulative_required_lessons_completed_in_last_4_periods
           FROM (with_status_info
             JOIN cumululative_data ON (((with_status_info.user_id = cumululative_data.user_id) AND (with_status_info.cohort_id = cumululative_data.cohort_id) AND (with_status_info.index = cumululative_data.index))))
        )
 SELECT with_cumulative_data.user_id,
    with_cumulative_data.cohort_id,
    with_cumulative_data.cohort_name,
    with_cumulative_data.index,
    with_cumulative_data.required_lessons_completed_in_period,
    with_cumulative_data.period_start,
    with_cumulative_data.period_end,
    with_cumulative_data.required_lesson_pack_ids_completed_in_period,
    with_cumulative_data.status_at_period_end,
    with_cumulative_data.cumulative_required_lessons_completed,
    with_cumulative_data.cumulative_required_up_to_now_lessons_completed,
    with_cumulative_data.cumulative_required_lessons_completed_in_last_4_periods
   FROM with_cumulative_data
  ORDER BY with_cumulative_data.cohort_id, with_cumulative_data.user_id, with_cumulative_data.index
  WITH NO DATA;


--
-- Name: content_publishers_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.content_publishers_versions (
    version_id uuid DEFAULT public.uuid_generate_v4(),
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    published_at timestamp without time zone,
    object_updated_at timestamp without time zone,
    lesson_version_id uuid,
    lesson_stream_version_id uuid,
    playlist_version_id uuid,
    cohort_version_id uuid
);


--
-- Name: content_topics; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.content_topics (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    locales json DEFAULT '{}'::json
);


--
-- Name: content_topics_lesson_stream_locale_packs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.content_topics_lesson_stream_locale_packs (
    id integer NOT NULL,
    content_topic_id uuid NOT NULL,
    locale_pack_id uuid NOT NULL
);


--
-- Name: content_topics_lesson_stream_locale_packs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.content_topics_lesson_stream_locale_packs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: content_topics_lesson_stream_locale_packs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.content_topics_lesson_stream_locale_packs_id_seq OWNED BY public.content_topics_lesson_stream_locale_packs.id;


--
-- Name: content_topics_lesson_stream_locale_packs_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.content_topics_lesson_stream_locale_packs_versions (
    version_id uuid DEFAULT public.uuid_generate_v4(),
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id integer NOT NULL,
    content_topic_id uuid NOT NULL,
    locale_pack_id uuid NOT NULL
);


--
-- Name: content_topics_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.content_topics_versions (
    version_id uuid DEFAULT public.uuid_generate_v4(),
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    locales json
);


--
-- Name: content_views_refresh; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.content_views_refresh (
    updated_at timestamp without time zone
);


--
-- Name: curriculum_templates; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.curriculum_templates (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    name text NOT NULL,
    description text,
    required_playlist_pack_ids uuid[] DEFAULT '{}'::uuid[] NOT NULL,
    specialization_playlist_pack_ids uuid[] DEFAULT '{}'::uuid[] NOT NULL,
    periods json[] DEFAULT '{}'::json[] NOT NULL,
    num_required_specializations integer DEFAULT 0 NOT NULL,
    program_type text NOT NULL,
    admission_rounds json[] DEFAULT '{}'::json[],
    registration_deadline_days_offset integer DEFAULT 0,
    program_guide_url text,
    enrollment_deadline_days_offset integer,
    stripe_plans json[],
    scholarship_levels json[],
    graduation_date_days_offset integer,
    diploma_generation_days_offset integer,
    project_submission_email character varying,
    project_documents json[]
);


--
-- Name: curriculum_templates_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.curriculum_templates_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    name text NOT NULL,
    description text,
    required_playlist_pack_ids uuid[] NOT NULL,
    specialization_playlist_pack_ids uuid[] NOT NULL,
    periods json[] NOT NULL,
    num_required_specializations integer DEFAULT 0 NOT NULL,
    program_type text,
    admission_rounds json[],
    registration_deadline_days_offset integer,
    program_guide_url text,
    enrollment_deadline_days_offset integer,
    stripe_plans json[],
    scholarship_levels json[],
    graduation_date_days_offset integer,
    diploma_generation_days_offset integer,
    project_submission_email character varying,
    project_documents json[]
);


--
-- Name: delayed_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.delayed_jobs (
    id integer NOT NULL,
    priority integer DEFAULT 0 NOT NULL,
    attempts integer DEFAULT 0 NOT NULL,
    handler text NOT NULL,
    last_error text,
    run_at timestamp without time zone,
    locked_at timestamp without time zone,
    failed_at timestamp without time zone,
    locked_by character varying,
    queue character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.delayed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.delayed_jobs_id_seq OWNED BY public.delayed_jobs.id;


--
-- Name: distinct_user_ids; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.distinct_user_ids (
    user_id uuid NOT NULL
);


--
-- Name: editor_lesson_sessions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.editor_lesson_sessions (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    user_id uuid NOT NULL,
    lesson_id uuid NOT NULL,
    started_at timestamp without time zone NOT NULL,
    last_activity_time timestamp without time zone NOT NULL,
    total_time double precision NOT NULL,
    save_count integer NOT NULL
);


--
-- Name: education_experiences_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.education_experiences_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    graduation_year integer NOT NULL,
    degree text,
    major text NOT NULL,
    minor text,
    gpa text,
    career_profile_id uuid,
    educational_organization_option_id uuid,
    gpa_description text,
    degree_program boolean,
    will_not_complete boolean
);


--
-- Name: educational_organization_options_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.educational_organization_options_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    text text NOT NULL,
    locale text NOT NULL,
    suggest boolean,
    source_id text,
    image_url text
);


--
-- Name: entity_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.entity_metadata (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    title character varying,
    description character varying,
    canonical_url text,
    image_id uuid,
    tweet_template text
);


--
-- Name: events; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.events (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    user_id uuid,
    event_type character varying(255),
    payload json,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    estimated_time timestamp without time zone,
    client_reported_time timestamp without time zone,
    hit_server_at timestamp without time zone,
    total_buffered_seconds double precision
);


--
-- Name: experiment_variations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.experiment_variations (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    title character varying NOT NULL,
    experiment_id uuid NOT NULL,
    optimizely_id text
);


--
-- Name: experiment_variations_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.experiment_variations_users (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    user_id uuid NOT NULL,
    experiment_id uuid NOT NULL,
    experiment_variation_id uuid NOT NULL
);


--
-- Name: experiments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.experiments (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    title character varying,
    optimizely_id text
);


--
-- Name: fulltext_items; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.fulltext_items (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL
);


--
-- Name: global_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.global_metadata (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    site_name character varying,
    default_title character varying,
    default_description character varying,
    default_image_id uuid,
    default_canonical_url character varying
);


--
-- Name: hiring_applications; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.hiring_applications (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    applied_at timestamp without time zone NOT NULL,
    accepted_at timestamp without time zone,
    user_id uuid NOT NULL,
    status text DEFAULT 'pending'::text NOT NULL,
    job_role text,
    company_year integer,
    company_employee_count text,
    company_annual_revenue text,
    company_sells_recruiting_services boolean,
    logo_url text,
    website_url text,
    hiring_for text,
    team_name text,
    team_mission text,
    role_descriptors text[] DEFAULT '{}'::text[],
    fun_fact text,
    where_descriptors text[] DEFAULT '{}'::text[],
    position_descriptors text[] DEFAULT '{}'::text[],
    place_id text,
    place_details json DEFAULT '{}'::json NOT NULL,
    funding text,
    industry text,
    last_calculated_complete_percentage double precision,
    viewed_career_profile_list_ids uuid[] DEFAULT '{}'::uuid[] NOT NULL,
    targeted_career_profile_ids uuid[] DEFAULT '{}'::uuid[] NOT NULL,
    has_seen_welcome boolean DEFAULT false NOT NULL,
    auto_assign_candidates boolean DEFAULT true,
    processed_targeted_career_profile_ids boolean DEFAULT false NOT NULL,
    salary_ranges text[] DEFAULT '{}'::text[]
);


--
-- Name: hiring_applications_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.hiring_applications_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    applied_at timestamp without time zone NOT NULL,
    accepted_at timestamp without time zone,
    user_id uuid NOT NULL,
    status text NOT NULL,
    job_role text,
    company_year integer,
    company_employee_count text,
    company_annual_revenue text,
    company_sells_recruiting_services boolean,
    logo_url text,
    website_url text,
    hiring_for text,
    team_name text,
    team_mission text,
    role_descriptors text[],
    fun_fact text,
    where_descriptors text[],
    position_descriptors text[],
    place_id text,
    place_details json,
    funding text,
    industry text,
    last_calculated_complete_percentage double precision,
    viewed_career_profile_list_ids uuid[],
    targeted_career_profile_ids uuid[],
    has_seen_welcome boolean,
    auto_assign_candidates boolean,
    processed_targeted_career_profile_ids boolean,
    salary_ranges text[]
);


--
-- Name: hiring_relationships_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.hiring_relationships_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    hiring_manager_id uuid NOT NULL,
    candidate_id uuid NOT NULL,
    hiring_manager_status text NOT NULL,
    candidate_status text NOT NULL,
    archived_after_accepted boolean,
    hiring_manager_priority integer,
    hiring_manager_closed boolean,
    candidate_closed boolean,
    open_position_id uuid,
    hiring_manager_closed_info json,
    matched_at timestamp without time zone,
    conversation_id integer,
    matched_by_hiring_manager boolean,
    stripe_usage_record_info json
);


--
-- Name: hiring_teams; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.hiring_teams (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    title text NOT NULL,
    owner_id uuid,
    subscription_required boolean DEFAULT true NOT NULL
);


--
-- Name: hiring_teams_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.hiring_teams_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    title text NOT NULL,
    owner_id uuid,
    subscription_required boolean
);


--
-- Name: institution_playlist_locale_packs; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.institution_playlist_locale_packs AS
 WITH step_1 AS (
         SELECT institutions.id AS institution_id,
            institutions.name AS institution_name,
            unnest(institutions.playlist_pack_ids) AS playlist_locale_pack_id
           FROM public.institutions
        )
 SELECT step_1.institution_id,
    step_1.institution_name,
    published_playlist_locale_packs.title AS playlist_title,
    published_playlist_locale_packs.locales AS playlist_locales,
    published_playlist_locale_packs.locale_pack_id AS playlist_locale_pack_id
   FROM (step_1
     JOIN public.published_playlist_locale_packs ON ((published_playlist_locale_packs.locale_pack_id = step_1.playlist_locale_pack_id)))
  GROUP BY step_1.institution_id, step_1.institution_name, published_playlist_locale_packs.title, published_playlist_locale_packs.locales, published_playlist_locale_packs.locale_pack_id
  ORDER BY step_1.institution_name, published_playlist_locale_packs.title
  WITH NO DATA;


--
-- Name: institution_stream_locale_packs; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.institution_stream_locale_packs AS
 WITH streams_from_groups AS (
         SELECT DISTINCT institutions_1.id AS institution_id,
            access_groups_lesson_stream_locale_packs.locale_pack_id AS stream_locale_pack_id
           FROM ((public.institutions institutions_1
             LEFT JOIN public.access_groups_institutions ON ((access_groups_institutions.institution_id = institutions_1.id)))
             LEFT JOIN public.access_groups_lesson_stream_locale_packs ON ((access_groups_lesson_stream_locale_packs.access_group_id = access_groups_institutions.access_group_id)))
        ), streams_from_playlists AS (
         SELECT DISTINCT institutions_1.id AS institution_id,
            published_playlist_streams_1.stream_locale_pack_id
           FROM ((public.institutions institutions_1
             JOIN public.institution_playlist_locale_packs ON ((institutions_1.id = institution_playlist_locale_packs.institution_id)))
             JOIN public.published_playlist_streams published_playlist_streams_1 ON ((published_playlist_streams_1.playlist_locale_pack_id = institution_playlist_locale_packs.playlist_locale_pack_id)))
        ), all_stream_locale_pack_ids AS (
         SELECT streams_from_groups.institution_id,
            streams_from_groups.stream_locale_pack_id
           FROM streams_from_groups
        UNION
         SELECT streams_from_playlists_1.institution_id,
            streams_from_playlists_1.stream_locale_pack_id
           FROM streams_from_playlists streams_from_playlists_1
        )
 SELECT institutions.id AS institution_id,
    institutions.name AS institution_name,
    (streams_from_playlists.* IS NOT NULL) AS in_playlist,
    published_stream_locale_packs.exam,
    published_stream_locale_packs.title AS stream_title,
    published_stream_locale_packs.locale_pack_id AS stream_locale_pack_id,
    published_stream_locale_packs.locales AS stream_locales
   FROM ((((all_stream_locale_pack_ids
     JOIN public.institutions ON ((institutions.id = all_stream_locale_pack_ids.institution_id)))
     JOIN public.published_stream_locale_packs ON ((published_stream_locale_packs.locale_pack_id = all_stream_locale_pack_ids.stream_locale_pack_id)))
     LEFT JOIN public.published_playlist_streams ON ((published_playlist_streams.stream_locale_pack_id = published_stream_locale_packs.locale_pack_id)))
     LEFT JOIN streams_from_playlists ON (((institutions.id = streams_from_playlists.institution_id) AND (streams_from_playlists.stream_locale_pack_id = all_stream_locale_pack_ids.stream_locale_pack_id))))
  GROUP BY institutions.id, institutions.name, published_stream_locale_packs.title, published_stream_locale_packs.locale_pack_id, published_stream_locale_packs.locales, published_stream_locale_packs.exam, streams_from_playlists.*
  WITH NO DATA;


--
-- Name: institution_lesson_locale_packs; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.institution_lesson_locale_packs AS
 SELECT institution_stream_locale_packs.institution_id,
    institution_stream_locale_packs.institution_name,
    institution_stream_locale_packs.in_playlist,
    institution_stream_locale_packs.exam,
    institution_stream_locale_packs.stream_title,
    institution_stream_locale_packs.stream_locale_pack_id,
    institution_stream_locale_packs.stream_locales,
    published_stream_lesson_locale_packs.test,
    published_stream_lesson_locale_packs.assessment,
    published_stream_lesson_locale_packs.lesson_title,
    published_stream_lesson_locale_packs.lesson_locale_pack_id,
    published_stream_lesson_locale_packs.lesson_locales
   FROM (public.institution_stream_locale_packs
     JOIN public.published_stream_lesson_locale_packs ON ((published_stream_lesson_locale_packs.stream_locale_pack_id = institution_stream_locale_packs.stream_locale_pack_id)))
  WITH NO DATA;


--
-- Name: institutions_reports_viewers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.institutions_reports_viewers (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    user_id uuid NOT NULL,
    institution_id uuid NOT NULL
);


--
-- Name: institutions_users_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.institutions_users_versions (
    version_id uuid DEFAULT public.uuid_generate_v4(),
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    institution_id uuid,
    user_id uuid
);


--
-- Name: institutions_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.institutions_versions (
    version_id uuid DEFAULT public.uuid_generate_v4(),
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    name character varying,
    sign_up_code character varying,
    scorm_token uuid,
    playlist_pack_ids uuid[]
);


--
-- Name: internal_content_titles; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.internal_content_titles AS
( WITH step_1 AS (
         SELECT 'Lesson'::text AS type,
            content_items.id,
            content_items.locale,
            english.tag,
                CASE
                    WHEN (english.title IS NOT NULL) THEN english.title
                    ELSE content_items.title
                END AS internal_title
           FROM (public.lessons content_items
             LEFT JOIN public.lessons english ON (((content_items.locale_pack_id = english.locale_pack_id) AND (english.locale = 'en'::text))))
        ), add_tag_to_title AS (
         SELECT step_1.type,
            step_1.id,
            step_1.locale,
                CASE
                    WHEN (step_1.tag IS NOT NULL) THEN (((('('::text || step_1.tag) || ') '::text) || (step_1.internal_title)::text))::character varying
                    ELSE step_1.internal_title
                END AS internal_title
           FROM step_1
        ), set_title_type AS (
         SELECT add_tag_to_title.type,
            add_tag_to_title.id,
            add_tag_to_title.locale,
            (add_tag_to_title.internal_title)::text AS internal_title
           FROM add_tag_to_title
        )
 SELECT set_title_type.type,
    set_title_type.id,
    set_title_type.locale,
    set_title_type.internal_title
   FROM set_title_type)
UNION
( WITH step_1 AS (
         SELECT 'Lesson::Stream'::text AS type,
            content_items.id,
            content_items.locale,
            NULL::text AS tag,
                CASE
                    WHEN (english.title IS NOT NULL) THEN english.title
                    ELSE content_items.title
                END AS internal_title
           FROM (public.lesson_streams content_items
             LEFT JOIN public.lesson_streams english ON (((content_items.locale_pack_id = english.locale_pack_id) AND (english.locale = 'en'::text))))
        ), add_tag_to_title AS (
         SELECT step_1.type,
            step_1.id,
            step_1.locale,
                CASE
                    WHEN (step_1.tag IS NOT NULL) THEN (((('('::text || step_1.tag) || ') '::text) || (step_1.internal_title)::text))::character varying
                    ELSE step_1.internal_title
                END AS internal_title
           FROM step_1
        ), set_title_type AS (
         SELECT add_tag_to_title.type,
            add_tag_to_title.id,
            add_tag_to_title.locale,
            (add_tag_to_title.internal_title)::text AS internal_title
           FROM add_tag_to_title
        )
 SELECT set_title_type.type,
    set_title_type.id,
    set_title_type.locale,
    set_title_type.internal_title
   FROM set_title_type)
UNION
( WITH step_1 AS (
         SELECT 'Playlist'::text AS type,
            content_items.id,
            content_items.locale,
            NULL::text AS tag,
                CASE
                    WHEN (english.title IS NOT NULL) THEN english.title
                    ELSE content_items.title
                END AS internal_title
           FROM (public.playlists content_items
             LEFT JOIN public.playlists english ON (((content_items.locale_pack_id = english.locale_pack_id) AND (english.locale = 'en'::text))))
        ), add_tag_to_title AS (
         SELECT step_1.type,
            step_1.id,
            step_1.locale,
                CASE
                    WHEN (step_1.tag IS NOT NULL) THEN ((('('::text || step_1.tag) || ') '::text) || step_1.internal_title)
                    ELSE step_1.internal_title
                END AS internal_title
           FROM step_1
        ), set_title_type AS (
         SELECT add_tag_to_title.type,
            add_tag_to_title.id,
            add_tag_to_title.locale,
            add_tag_to_title.internal_title
           FROM add_tag_to_title
        )
 SELECT set_title_type.type,
    set_title_type.id,
    set_title_type.locale,
    set_title_type.internal_title
   FROM set_title_type)
  ORDER BY 1, 3, 4
  WITH NO DATA;


--
-- Name: lesson_activity_by_calendar_date_records; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.lesson_activity_by_calendar_date_records AS
 SELECT dblink.user_id,
    dblink.date,
    dblink.total_lesson_seconds
   FROM public.dblink('red_royal'::text, '
                        with unload_events as (
                            select

                            user_id

                            --  https://trello.com/c/RgurwSyg/1875-bug-fix-last-seen-column-showing-dates-in-the-future
                            , case
                                when events.estimated_time > getdate() then events.created_at
                                else events.estimated_time
                                end as estimated_time
                            , case when events.duration_total > 120 then 120 else events.duration_total end as duration_total
                            from
                                events
                            where events.event_type in (''lesson:frame:unload'')
                        )
                        select
                            user_id
                            , date_trunc(''day'', estimated_time) as date
                            , sum(duration_total) as total_lesson_seconds
                        from unload_events
                        group by
                            user_id
                            , date_trunc(''day'', estimated_time)
                    '::text) dblink(user_id uuid, date timestamp without time zone, total_lesson_seconds double precision)
  WITH NO DATA;


--
-- Name: lesson_fulltext; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lesson_fulltext (
    lesson_id uuid NOT NULL,
    content_vector tsvector,
    comments_vector tsvector,
    content_text text,
    comments_text text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: lesson_locale_packs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lesson_locale_packs (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    practice_locale_pack_id uuid
);


--
-- Name: lesson_locale_packs_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lesson_locale_packs_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    practice_locale_pack_id uuid
);


--
-- Name: lesson_progress; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lesson_progress (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    frame_bookmark_id uuid,
    started_at timestamp without time zone,
    completed_at timestamp without time zone,
    user_id uuid,
    frame_history json,
    completed_frames json DEFAULT '{}'::json,
    challenge_scores json DEFAULT '{}'::json,
    frame_durations json DEFAULT '{}'::json,
    locale_pack_id uuid NOT NULL,
    best_score double precision,
    last_checked_for_cert_generation timestamp without time zone
);


--
-- Name: lesson_stream_locale_packs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lesson_stream_locale_packs (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: lesson_stream_locale_packs_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lesson_stream_locale_packs_users (
    id integer NOT NULL,
    user_id uuid NOT NULL,
    locale_pack_id uuid NOT NULL
);


--
-- Name: lesson_stream_locale_packs_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.lesson_stream_locale_packs_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lesson_stream_locale_packs_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.lesson_stream_locale_packs_users_id_seq OWNED BY public.lesson_stream_locale_packs_users.id;


--
-- Name: lesson_stream_locale_packs_users_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lesson_stream_locale_packs_users_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id integer NOT NULL,
    user_id uuid NOT NULL,
    locale_pack_id uuid NOT NULL
);


--
-- Name: lesson_stream_locale_packs_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lesson_stream_locale_packs_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: lesson_to_stream_joins; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lesson_to_stream_joins (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    stream_id uuid,
    lesson_id uuid,
    "position" integer
);


--
-- Name: lesson_to_stream_joins_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lesson_to_stream_joins_versions (
    version_id uuid DEFAULT public.uuid_generate_v4(),
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    stream_id uuid,
    lesson_id uuid,
    "position" integer
);


--
-- Name: mailboxer_conversation_opt_outs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mailboxer_conversation_opt_outs (
    id integer NOT NULL,
    unsubscriber_type character varying,
    unsubscriber_id integer,
    conversation_id integer
);


--
-- Name: mailboxer_conversation_opt_outs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mailboxer_conversation_opt_outs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mailboxer_conversation_opt_outs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.mailboxer_conversation_opt_outs_id_seq OWNED BY public.mailboxer_conversation_opt_outs.id;


--
-- Name: mailboxer_conversations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mailboxer_conversations (
    id integer NOT NULL,
    subject character varying DEFAULT ''::character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: mailboxer_conversations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mailboxer_conversations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mailboxer_conversations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.mailboxer_conversations_id_seq OWNED BY public.mailboxer_conversations.id;


--
-- Name: mailboxer_notifications; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mailboxer_notifications (
    id integer NOT NULL,
    type character varying,
    body text,
    subject character varying DEFAULT ''::character varying,
    sender_type character varying,
    sender_id uuid,
    conversation_id integer,
    draft boolean DEFAULT false,
    notification_code character varying,
    notified_object_type character varying,
    notified_object_id uuid,
    attachment character varying,
    updated_at timestamp without time zone NOT NULL,
    created_at timestamp without time zone NOT NULL,
    global boolean DEFAULT false,
    expires timestamp without time zone,
    metadata json
);


--
-- Name: mailboxer_notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mailboxer_notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mailboxer_notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.mailboxer_notifications_id_seq OWNED BY public.mailboxer_notifications.id;


--
-- Name: mailboxer_receipts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mailboxer_receipts (
    id integer NOT NULL,
    receiver_type character varying,
    receiver_id uuid,
    notification_id integer NOT NULL,
    is_read boolean DEFAULT false,
    trashed boolean DEFAULT false,
    deleted boolean DEFAULT false,
    mailbox_type character varying(25),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    is_delivered boolean DEFAULT false,
    delivery_method character varying,
    message_id character varying
);


--
-- Name: mailboxer_receipts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mailboxer_receipts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mailboxer_receipts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.mailboxer_receipts_id_seq OWNED BY public.mailboxer_receipts.id;


--
-- Name: multiple_choice_challenge_progress; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.multiple_choice_challenge_progress (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    user_id uuid,
    lesson_id uuid,
    frame_id uuid,
    editor_template character varying,
    challenge_id uuid,
    first_answer_id character varying,
    first_answer_estimated_time timestamp without time zone,
    first_answer_correct boolean,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: multiple_choice_challenge_usage_reports; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.multiple_choice_challenge_usage_reports (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    lesson_id uuid,
    frame_id uuid,
    editor_template character varying,
    challenge_id uuid,
    answer_id uuid,
    count integer DEFAULT 0,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: old_assessment_scores; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.old_assessment_scores (
    user_id uuid NOT NULL,
    locale_pack_id uuid NOT NULL,
    average_assessment_score_first double precision NOT NULL,
    average_assessment_score_last double precision NOT NULL
);


--
-- Name: open_positions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.open_positions (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    hiring_manager_id uuid NOT NULL,
    title text NOT NULL,
    salary double precision,
    available_interview_times text,
    last_invitation_at timestamp without time zone,
    place_id text,
    place_details json DEFAULT '{}'::json NOT NULL,
    role text,
    job_post_url text,
    description text,
    perks text[] DEFAULT '{}'::text[],
    desired_years_experience json DEFAULT '{}'::json NOT NULL,
    featured boolean DEFAULT false,
    archived boolean DEFAULT false,
    position_descriptors text[] DEFAULT '{}'::text[],
    drafted_from_positions boolean DEFAULT false,
    posted_at timestamp without time zone,
    last_curated_at timestamp without time zone,
    curation_email_last_triggered_at timestamp without time zone,
    last_updated_at_by_hiring_manager timestamp without time zone
);


--
-- Name: open_positions_skills_options; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.open_positions_skills_options (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    open_position_id uuid NOT NULL,
    skills_option_id uuid NOT NULL,
    "position" integer NOT NULL
);


--
-- Name: open_positions_skills_options_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.open_positions_skills_options_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    open_position_id uuid NOT NULL,
    skills_option_id uuid NOT NULL,
    "position" integer NOT NULL
);


--
-- Name: open_positions_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.open_positions_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    hiring_manager_id uuid NOT NULL,
    title text NOT NULL,
    salary double precision,
    available_interview_times text,
    last_invitation_at timestamp without time zone,
    place_id text,
    place_details json,
    role text,
    job_post_url text,
    description text,
    perks text[],
    desired_years_experience json,
    featured boolean,
    archived boolean,
    position_descriptors text[] DEFAULT '{}'::text[],
    drafted_from_positions boolean,
    posted_at timestamp without time zone,
    last_curated_at timestamp without time zone,
    curation_email_last_triggered_at timestamp without time zone,
    last_updated_at_by_hiring_manager timestamp without time zone
);


--
-- Name: peer_recommendations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.peer_recommendations (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    career_profile_id uuid NOT NULL,
    airtable_record_id text,
    email text NOT NULL,
    contacted boolean DEFAULT false NOT NULL
);


--
-- Name: peer_recommendations_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.peer_recommendations_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    career_profile_id uuid NOT NULL,
    airtable_record_id text,
    email text NOT NULL,
    contacted boolean NOT NULL
);


--
-- Name: player_lesson_sessions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.player_lesson_sessions (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    session_id uuid NOT NULL,
    user_id uuid NOT NULL,
    lesson_id uuid NOT NULL,
    lesson_stream_id uuid,
    started_at timestamp without time zone NOT NULL,
    last_lesson_activity_time timestamp without time zone NOT NULL,
    total_lesson_time double precision NOT NULL,
    started_in_this_session boolean NOT NULL,
    completed_in_this_session boolean NOT NULL,
    lesson_locale_pack_id uuid NOT NULL,
    lesson_stream_locale_pack_id uuid,
    lesson_locale text NOT NULL,
    client_type text NOT NULL
);


--
-- Name: playlist_locale_packs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.playlist_locale_packs (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: playlist_locale_packs_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.playlist_locale_packs_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: professional_organization_options_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.professional_organization_options_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    text text NOT NULL,
    locale text NOT NULL,
    suggest boolean,
    source_id text,
    image_url text
);


--
-- Name: published_cohort_periods_stream_locale_packs; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.published_cohort_periods_stream_locale_packs AS
 SELECT published_cohort_periods.cohort_id,
    published_cohort_periods.index,
    published_stream_locale_packs.locale_pack_id,
    published_stream_locale_packs.title,
    (published_stream_locale_packs.locale_pack_id = ANY (published_cohort_periods.required_stream_pack_ids)) AS required,
    (published_stream_locale_packs.locale_pack_id = ANY (published_cohort_periods.optional_stream_pack_ids)) AS optional
   FROM (public.published_cohort_periods
     JOIN public.published_stream_locale_packs ON ((published_stream_locale_packs.locale_pack_id = ANY ((published_cohort_periods.required_stream_pack_ids || published_cohort_periods.optional_stream_pack_ids)))))
  ORDER BY published_cohort_periods.cohort_id, published_cohort_periods.index, (published_stream_locale_packs.locale_pack_id = ANY (published_cohort_periods.required_stream_pack_ids)) DESC
  WITH NO DATA;


--
-- Name: published_content_titles; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.published_content_titles AS
( WITH published_versions AS (
         SELECT lessons_versions.locale_pack_id,
            lessons_versions.locale,
            lessons_versions.title
           FROM (public.lessons_versions
             JOIN public.content_publishers ON ((content_publishers.lesson_version_id = lessons_versions.version_id)))
        )
 SELECT DISTINCT 'Lesson'::text AS type,
    working.locale_pack_id,
    working.locale AS target_locale,
    COALESCE(published_in_locale.locale, published_in_english.locale) AS published_locale,
    COALESCE(published_in_locale.title, published_in_english.title) AS published_title
   FROM ((public.lessons working
     LEFT JOIN published_versions published_in_locale ON (((working.locale_pack_id = published_in_locale.locale_pack_id) AND (working.locale = published_in_locale.locale))))
     LEFT JOIN published_versions published_in_english ON (((working.locale_pack_id = published_in_english.locale_pack_id) AND ('en'::text = published_in_english.locale))))
  WHERE ((published_in_locale.title IS NOT NULL) OR (published_in_english.title IS NOT NULL)))
UNION
( WITH published_versions AS (
         SELECT lesson_streams_versions.locale_pack_id,
            lesson_streams_versions.locale,
            lesson_streams_versions.title
           FROM (public.lesson_streams_versions
             JOIN public.content_publishers ON ((content_publishers.lesson_stream_version_id = lesson_streams_versions.version_id)))
        )
 SELECT DISTINCT 'Lesson::Stream'::text AS type,
    working.locale_pack_id,
    working.locale AS target_locale,
    COALESCE(published_in_locale.locale, published_in_english.locale) AS published_locale,
    COALESCE(published_in_locale.title, published_in_english.title) AS published_title
   FROM ((public.lesson_streams working
     LEFT JOIN published_versions published_in_locale ON (((working.locale_pack_id = published_in_locale.locale_pack_id) AND (working.locale = published_in_locale.locale))))
     LEFT JOIN published_versions published_in_english ON (((working.locale_pack_id = published_in_english.locale_pack_id) AND ('en'::text = published_in_english.locale))))
  WHERE ((published_in_locale.title IS NOT NULL) OR (published_in_english.title IS NOT NULL)))
UNION
( WITH published_versions AS (
         SELECT playlists_versions.locale_pack_id,
            playlists_versions.locale,
            playlists_versions.title
           FROM (public.playlists_versions
             JOIN public.content_publishers ON ((content_publishers.playlist_version_id = playlists_versions.version_id)))
        )
 SELECT DISTINCT 'Playlist'::text AS type,
    working.locale_pack_id,
    working.locale AS target_locale,
    COALESCE(published_in_locale.locale, published_in_english.locale) AS published_locale,
    COALESCE(published_in_locale.title, published_in_english.title) AS published_title
   FROM ((public.playlists working
     LEFT JOIN published_versions published_in_locale ON (((working.locale_pack_id = published_in_locale.locale_pack_id) AND (working.locale = published_in_locale.locale))))
     LEFT JOIN published_versions published_in_english ON (((working.locale_pack_id = published_in_english.locale_pack_id) AND ('en'::text = published_in_english.locale))))
  WHERE ((published_in_locale.title IS NOT NULL) OR (published_in_english.title IS NOT NULL)))
  ORDER BY 1, 3, 4
  WITH NO DATA;


--
-- Name: published_playlist_lessons; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.published_playlist_lessons AS
 SELECT published_playlist_streams.playlist_id,
    published_playlist_streams.playlist_locale_pack_id,
    published_playlist_streams.playlist_title,
    published_playlist_streams.playlist_version_id,
    published_playlist_streams.playlist_locale,
    published_playlist_streams.stream_locale_pack_id,
    published_playlist_streams.stream_title,
    published_playlist_streams.stream_id,
    published_playlist_streams.stream_version_id,
    published_playlist_streams.stream_locale,
    published_stream_lessons.lesson_title,
    published_stream_lessons.lesson_locale_pack_id,
    published_stream_lessons.lesson_version_id,
    published_stream_lessons.lesson_id,
    published_stream_lessons.lesson_locale
   FROM (public.published_playlist_streams
     JOIN public.published_stream_lessons ON ((published_stream_lessons.stream_id = published_playlist_streams.stream_id)))
  WITH NO DATA;


--
-- Name: roles_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.roles_versions (
    version_id uuid DEFAULT public.uuid_generate_v4(),
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    name character varying,
    resource_type character varying,
    resource_id uuid,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: s3_assets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.s3_assets (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    file_file_name character varying,
    file_content_type character varying,
    file_file_size integer,
    file_updated_at timestamp without time zone,
    directory character varying,
    styles text,
    file_fingerprint character varying,
    dimensions json,
    formats json,
    source_url text
);


--
-- Name: s3_exercise_documents; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.s3_exercise_documents (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    file_file_name character varying,
    file_content_type character varying,
    file_file_size integer,
    file_updated_at timestamp without time zone,
    file_fingerprint character varying
);


--
-- Name: s3_identification_assets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.s3_identification_assets (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    file_file_name character varying,
    file_content_type character varying,
    file_file_size integer,
    file_updated_at timestamp without time zone,
    file_fingerprint character varying,
    user_id uuid
);


--
-- Name: s3_project_documents; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.s3_project_documents (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    file_file_name character varying,
    file_content_type character varying,
    file_file_size integer,
    file_updated_at timestamp without time zone,
    file_fingerprint character varying
);


--
-- Name: s3_transcript_assets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.s3_transcript_assets (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    file_file_name character varying,
    file_content_type character varying,
    file_file_size integer,
    file_updated_at timestamp without time zone,
    file_fingerprint character varying,
    user_id uuid
);


--
-- Name: schedulable_items; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schedulable_items (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    required_playlist_pack_ids uuid[] DEFAULT '{}'::uuid[],
    specialization_playlist_pack_ids uuid[] DEFAULT '{}'::uuid[],
    periods json[] DEFAULT '{}'::json[] NOT NULL,
    num_required_specializations integer,
    stripe_plans json[],
    scholarship_levels json[],
    program_type text
);


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: skills_options_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.skills_options_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    text text NOT NULL,
    locale text NOT NULL,
    suggest boolean
);


--
-- Name: student_network_interests_options_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.student_network_interests_options_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    text text NOT NULL,
    locale text NOT NULL,
    suggest boolean
);


--
-- Name: subscriptions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subscriptions (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    stripe_plan_id text NOT NULL,
    stripe_subscription_id text NOT NULL,
    past_due boolean,
    stripe_product_id text NOT NULL
);


--
-- Name: subscriptions_hiring_teams; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subscriptions_hiring_teams (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    hiring_team_id uuid NOT NULL,
    subscription_id uuid NOT NULL
);


--
-- Name: subscriptions_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subscriptions_users (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    user_id uuid NOT NULL,
    subscription_id uuid NOT NULL
);


--
-- Name: subscriptions_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subscriptions_versions (
    version_id uuid DEFAULT public.uuid_generate_v4(),
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    stripe_plan_id text NOT NULL,
    stripe_subscription_id text NOT NULL,
    past_due boolean,
    stripe_product_id text
);


--
-- Name: tabular_things; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tabular_things (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    color text,
    shape text,
    user_id uuid
);


--
-- Name: time_series_things; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.time_series_things (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    user_id uuid NOT NULL,
    "time" timestamp without time zone NOT NULL
);


--
-- Name: user_progress_records; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.user_progress_records AS
 WITH lesson_info AS (
         SELECT user_lesson_progress_records.user_id,
            count(*) AS started_lesson_count,
            count(
                CASE
                    WHEN (user_lesson_progress_records.completed_at IS NOT NULL) THEN true
                    ELSE NULL::boolean
                END) AS completed_lesson_count,
            array_agg(user_lesson_progress_records.locale_pack_id) AS started_lesson_locale_pack_ids,
            array_remove(array_agg(
                CASE
                    WHEN (user_lesson_progress_records.completed_at IS NOT NULL) THEN user_lesson_progress_records.locale_pack_id
                    ELSE NULL::uuid
                END), NULL::uuid) AS completed_lesson_locale_pack_ids,
            sum(user_lesson_progress_records.total_lesson_time) AS total_lesson_time,
            max(user_lesson_progress_records.last_lesson_activity_time) AS last_lesson_activity_time,
            avg(user_lesson_progress_records.average_assessment_score_first) AS average_assessment_score_first,
            avg(user_lesson_progress_records.average_assessment_score_best) AS average_assessment_score_best,
            sum(user_lesson_progress_records.total_lesson_time_on_desktop) AS total_lesson_time_on_desktop,
            sum(user_lesson_progress_records.total_lesson_time_on_mobile_app) AS total_lesson_time_on_mobile_app,
            sum(user_lesson_progress_records.total_lesson_time_on_mobile_web) AS total_lesson_time_on_mobile_web,
            sum(user_lesson_progress_records.total_lesson_time_on_unknown) AS total_lesson_time_on_unknown
           FROM public.user_lesson_progress_records
          GROUP BY user_lesson_progress_records.user_id
        ), stream_info AS (
         SELECT lesson_streams_progress.user_id,
            count(*) AS started_stream_count,
            count(
                CASE
                    WHEN (lesson_streams_progress.completed_at IS NOT NULL) THEN true
                    ELSE NULL::boolean
                END) AS completed_stream_count,
            array_agg(lesson_streams_progress.locale_pack_id) AS started_stream_locale_pack_ids,
            array_remove(array_agg(
                CASE
                    WHEN (lesson_streams_progress.completed_at IS NOT NULL) THEN lesson_streams_progress.locale_pack_id
                    ELSE NULL::uuid
                END), NULL::uuid) AS completed_stream_locale_pack_ids
           FROM public.lesson_streams_progress
          GROUP BY lesson_streams_progress.user_id
        ), playlist_info AS (
         SELECT playlist_progress.user_id,
            count(*) AS started_playlist_count,
            count(
                CASE
                    WHEN playlist_progress.completed THEN true
                    ELSE NULL::boolean
                END) AS completed_playlist_count,
            array_agg(playlist_progress.locale_pack_id) AS started_playlist_locale_pack_ids,
            array_remove(array_agg(
                CASE
                    WHEN playlist_progress.completed THEN playlist_progress.locale_pack_id
                    ELSE NULL::uuid
                END), NULL::uuid) AS completed_playlist_locale_pack_ids
           FROM public.playlist_progress
          GROUP BY playlist_progress.user_id
        ), user_info AS (
         SELECT lesson_info.user_id,
            lesson_info.started_lesson_count,
            lesson_info.completed_lesson_count,
            lesson_info.started_lesson_locale_pack_ids,
            lesson_info.completed_lesson_locale_pack_ids,
            lesson_info.total_lesson_time,
            lesson_info.last_lesson_activity_time,
            lesson_info.average_assessment_score_first,
            lesson_info.average_assessment_score_best,
            lesson_info.total_lesson_time_on_desktop,
            lesson_info.total_lesson_time_on_mobile_app,
            lesson_info.total_lesson_time_on_mobile_web,
            lesson_info.total_lesson_time_on_unknown,
            stream_info.started_stream_count,
            stream_info.completed_stream_count,
            stream_info.started_stream_locale_pack_ids,
            stream_info.completed_stream_locale_pack_ids,
            playlist_info.started_playlist_count,
            playlist_info.completed_playlist_count,
            playlist_info.started_playlist_locale_pack_ids,
            playlist_info.completed_playlist_locale_pack_ids
           FROM ((lesson_info
             LEFT JOIN stream_info ON ((stream_info.user_id = lesson_info.user_id)))
             LEFT JOIN playlist_info ON ((playlist_info.user_id = lesson_info.user_id)))
        )
 SELECT user_info.user_id,
    user_info.started_lesson_count,
    user_info.completed_lesson_count,
    user_info.started_lesson_locale_pack_ids,
    user_info.completed_lesson_locale_pack_ids,
    user_info.total_lesson_time,
    user_info.last_lesson_activity_time,
    user_info.average_assessment_score_first,
    user_info.average_assessment_score_best,
    user_info.total_lesson_time_on_desktop,
    user_info.total_lesson_time_on_mobile_app,
    user_info.total_lesson_time_on_mobile_web,
    user_info.total_lesson_time_on_unknown,
    user_info.started_stream_count,
    user_info.completed_stream_count,
    user_info.started_stream_locale_pack_ids,
    user_info.completed_stream_locale_pack_ids,
    user_info.started_playlist_count,
    user_info.completed_playlist_count,
    user_info.started_playlist_locale_pack_ids,
    user_info.completed_playlist_locale_pack_ids
   FROM user_info
  WITH NO DATA;


--
-- Name: users_roles_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users_roles_versions (
    version_id uuid DEFAULT public.uuid_generate_v4(),
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    user_id uuid,
    role_id uuid
);


--
-- Name: users_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users_versions (
    version_id uuid DEFAULT public.uuid_generate_v4(),
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    email character varying,
    encrypted_password character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying,
    last_sign_in_ip character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    failed_attempts integer,
    unlock_token character varying,
    locked_at timestamp without time zone,
    sign_up_code character varying,
    reset_password_redirect_url character varying,
    provider character varying,
    uid character varying NOT NULL,
    tokens json,
    has_seen_welcome boolean DEFAULT false,
    notify_email_newsletter boolean,
    school text,
    provider_user_info json,
    free_trial_started boolean,
    confirmed_profile_info boolean,
    pref_decimal_delim character varying,
    optimizely_segments json,
    optimizely_referer text,
    optimizely_buckets json,
    pref_locale character varying,
    active_playlist_locale_pack_id uuid,
    avatar_url text,
    avatar_provider text,
    mba_content_lockable boolean DEFAULT false,
    additional_details json,
    job_title text,
    phone text,
    country text,
    has_seen_accepted boolean DEFAULT false,
    name text,
    nickname text,
    phone_extension text,
    can_edit_career_profile boolean,
    professional_organization_option_id uuid,
    pref_show_photos_names boolean,
    identity_verified boolean,
    sex text,
    ethnicity text,
    race text[],
    how_did_you_hear_about_us text,
    anything_else_to_tell_us text,
    birthdate date,
    address_line_1 text,
    address_line_2 text,
    city text,
    state text,
    zip text,
    pref_keyboard_shortcuts boolean,
    last_seen_at timestamp without time zone,
    fallback_program_type character varying,
    program_type_confirmed boolean,
    invite_code character varying,
    notify_hiring_updates boolean,
    notify_hiring_spotlights boolean,
    notify_hiring_content boolean,
    notify_hiring_candidate_activity boolean,
    hiring_team_id uuid,
    referred_by_id uuid,
    has_seen_featured_positions_page boolean,
    pref_one_click_reach_out boolean,
    has_seen_careers_welcome boolean,
    can_purchase_paid_certs boolean,
    has_seen_hide_candidate_confirm boolean,
    pref_positions_candidate_list boolean,
    skip_apply boolean,
    has_seen_student_network boolean,
    pref_student_network_privacy text,
    notify_hiring_spotlights_business boolean,
    notify_hiring_spotlights_technical boolean,
    has_drafted_open_position boolean,
    deactivated boolean,
    notify_candidate_positions_technical boolean,
    notify_candidate_positions_business boolean,
    avatar_id uuid,
    has_seen_first_position_review_modal boolean
);


--
-- Name: work_experiences_versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.work_experiences_versions (
    version_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    operation character varying(1) NOT NULL,
    version_created_at timestamp without time zone NOT NULL,
    id uuid NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    job_title text NOT NULL,
    start_date date NOT NULL,
    end_date date,
    responsibilities text[] NOT NULL,
    career_profile_id uuid,
    professional_organization_option_id uuid,
    featured boolean,
    role text,
    industry text,
    employment_type text
);


--
-- Name: content_topics_lesson_stream_locale_packs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_topics_lesson_stream_locale_packs ALTER COLUMN id SET DEFAULT nextval('public.content_topics_lesson_stream_locale_packs_id_seq'::regclass);


--
-- Name: delayed_jobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.delayed_jobs ALTER COLUMN id SET DEFAULT nextval('public.delayed_jobs_id_seq'::regclass);


--
-- Name: lesson_stream_locale_packs_users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_stream_locale_packs_users ALTER COLUMN id SET DEFAULT nextval('public.lesson_stream_locale_packs_users_id_seq'::regclass);


--
-- Name: mailboxer_conversation_opt_outs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailboxer_conversation_opt_outs ALTER COLUMN id SET DEFAULT nextval('public.mailboxer_conversation_opt_outs_id_seq'::regclass);


--
-- Name: mailboxer_conversations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailboxer_conversations ALTER COLUMN id SET DEFAULT nextval('public.mailboxer_conversations_id_seq'::regclass);


--
-- Name: mailboxer_notifications id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailboxer_notifications ALTER COLUMN id SET DEFAULT nextval('public.mailboxer_notifications_id_seq'::regclass);


--
-- Name: mailboxer_receipts id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailboxer_receipts ALTER COLUMN id SET DEFAULT nextval('public.mailboxer_receipts_id_seq'::regclass);


--
-- Name: access_groups_cohorts_versions access_groups_cohorts_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_cohorts_versions
    ADD CONSTRAINT access_groups_cohorts_versions_pkey PRIMARY KEY (version_id);


--
-- Name: access_groups_curriculum_templates_versions access_groups_curriculum_templates_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_curriculum_templates_versions
    ADD CONSTRAINT access_groups_curriculum_templates_versions_pkey PRIMARY KEY (version_id);


--
-- Name: access_groups_institutions access_groups_institutions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_institutions
    ADD CONSTRAINT access_groups_institutions_pkey PRIMARY KEY (id);


--
-- Name: access_groups_institutions_versions access_groups_institutions_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_institutions_versions
    ADD CONSTRAINT access_groups_institutions_versions_pkey PRIMARY KEY (version_id);


--
-- Name: access_groups_lesson_stream_locale_packs access_groups_lesson_stream_locale_packs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_lesson_stream_locale_packs
    ADD CONSTRAINT access_groups_lesson_stream_locale_packs_pkey PRIMARY KEY (id);


--
-- Name: access_groups_lesson_stream_locale_packs_versions access_groups_lesson_stream_locale_packs_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_lesson_stream_locale_packs_versions
    ADD CONSTRAINT access_groups_lesson_stream_locale_packs_versions_pkey PRIMARY KEY (version_id);


--
-- Name: access_groups access_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups
    ADD CONSTRAINT access_groups_pkey PRIMARY KEY (id);


--
-- Name: access_groups_playlist_locale_packs access_groups_playlist_locale_packs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_playlist_locale_packs
    ADD CONSTRAINT access_groups_playlist_locale_packs_pkey PRIMARY KEY (id);


--
-- Name: access_groups_playlist_locale_packs_versions access_groups_playlist_locale_packs_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_playlist_locale_packs_versions
    ADD CONSTRAINT access_groups_playlist_locale_packs_versions_pkey PRIMARY KEY (version_id);


--
-- Name: access_groups_users access_groups_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_users
    ADD CONSTRAINT access_groups_users_pkey PRIMARY KEY (id);


--
-- Name: access_groups_users_versions access_groups_users_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_users_versions
    ADD CONSTRAINT access_groups_users_versions_pkey PRIMARY KEY (version_id);


--
-- Name: access_groups_versions access_groups_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_versions
    ADD CONSTRAINT access_groups_versions_pkey PRIMARY KEY (version_id);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: awards_and_interests_options awards_and_interests_options_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.awards_and_interests_options
    ADD CONSTRAINT awards_and_interests_options_pkey PRIMARY KEY (id);


--
-- Name: awards_and_interests_options_versions awards_and_interests_options_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.awards_and_interests_options_versions
    ADD CONSTRAINT awards_and_interests_options_versions_pkey PRIMARY KEY (version_id);


--
-- Name: candidate_position_interests candidate_position_interests_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.candidate_position_interests
    ADD CONSTRAINT candidate_position_interests_pkey PRIMARY KEY (id);


--
-- Name: candidate_position_interests_versions candidate_position_interests_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.candidate_position_interests_versions
    ADD CONSTRAINT candidate_position_interests_versions_pkey PRIMARY KEY (version_id);


--
-- Name: career_profile_lists career_profile_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profile_lists
    ADD CONSTRAINT career_profile_lists_pkey PRIMARY KEY (id);


--
-- Name: career_profile_lists_versions career_profile_lists_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profile_lists_versions
    ADD CONSTRAINT career_profile_lists_versions_pkey PRIMARY KEY (version_id);


--
-- Name: career_profile_searches career_profile_searches_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profile_searches
    ADD CONSTRAINT career_profile_searches_pkey PRIMARY KEY (id);


--
-- Name: career_profiles_awards_and_interests_options career_profiles_awards_and_interests_options_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profiles_awards_and_interests_options
    ADD CONSTRAINT career_profiles_awards_and_interests_options_pkey PRIMARY KEY (id);


--
-- Name: career_profiles career_profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profiles
    ADD CONSTRAINT career_profiles_pkey PRIMARY KEY (id);


--
-- Name: career_profiles_skills_options career_profiles_skills_options_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profiles_skills_options
    ADD CONSTRAINT career_profiles_skills_options_pkey PRIMARY KEY (id);


--
-- Name: career_profiles_student_network_interests_options career_profiles_student_network_interests_options_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profiles_student_network_interests_options
    ADD CONSTRAINT career_profiles_student_network_interests_options_pkey PRIMARY KEY (id);


--
-- Name: career_profiles_versions career_profiles_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profiles_versions
    ADD CONSTRAINT career_profiles_versions_pkey PRIMARY KEY (version_id);


--
-- Name: cohort_applications cohort_applications_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cohort_applications
    ADD CONSTRAINT cohort_applications_pkey PRIMARY KEY (id);


--
-- Name: cohort_applications_versions cohort_applications_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cohort_applications_versions
    ADD CONSTRAINT cohort_applications_versions_pkey PRIMARY KEY (version_id);


--
-- Name: cohort_period_graduation_rate_predictions cohort_period_graduation_rate_predictions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cohort_period_graduation_rate_predictions
    ADD CONSTRAINT cohort_period_graduation_rate_predictions_pkey PRIMARY KEY (id);


--
-- Name: cohort_promotions cohort_promotions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cohort_promotions
    ADD CONSTRAINT cohort_promotions_pkey PRIMARY KEY (id);


--
-- Name: cohort_promotions_versions cohort_promotions_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cohort_promotions_versions
    ADD CONSTRAINT cohort_promotions_versions_pkey PRIMARY KEY (version_id);


--
-- Name: cohorts cohorts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cohorts
    ADD CONSTRAINT cohorts_pkey PRIMARY KEY (id);


--
-- Name: cohorts_versions cohorts_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cohorts_versions
    ADD CONSTRAINT cohorts_versions_pkey PRIMARY KEY (version_id);


--
-- Name: content_publishers content_publishers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_publishers
    ADD CONSTRAINT content_publishers_pkey PRIMARY KEY (id);


--
-- Name: content_topics_lesson_stream_locale_packs content_topics_lesson_stream_locale_packs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_topics_lesson_stream_locale_packs
    ADD CONSTRAINT content_topics_lesson_stream_locale_packs_pkey PRIMARY KEY (id);


--
-- Name: content_topics content_topics_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_topics
    ADD CONSTRAINT content_topics_pkey PRIMARY KEY (id);


--
-- Name: curriculum_templates curriculum_templates_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.curriculum_templates
    ADD CONSTRAINT curriculum_templates_pkey PRIMARY KEY (id);


--
-- Name: curriculum_templates_versions curriculum_templates_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.curriculum_templates_versions
    ADD CONSTRAINT curriculum_templates_versions_pkey PRIMARY KEY (version_id);


--
-- Name: delayed_jobs delayed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.delayed_jobs
    ADD CONSTRAINT delayed_jobs_pkey PRIMARY KEY (id);


--
-- Name: editor_lesson_sessions editor_lesson_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.editor_lesson_sessions
    ADD CONSTRAINT editor_lesson_sessions_pkey PRIMARY KEY (id);


--
-- Name: education_experiences education_experiences_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.education_experiences
    ADD CONSTRAINT education_experiences_pkey PRIMARY KEY (id);


--
-- Name: education_experiences_versions education_experiences_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.education_experiences_versions
    ADD CONSTRAINT education_experiences_versions_pkey PRIMARY KEY (version_id);


--
-- Name: educational_organization_options educational_organization_options_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.educational_organization_options
    ADD CONSTRAINT educational_organization_options_pkey PRIMARY KEY (id);


--
-- Name: educational_organization_options_versions educational_organization_options_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.educational_organization_options_versions
    ADD CONSTRAINT educational_organization_options_versions_pkey PRIMARY KEY (version_id);


--
-- Name: entity_metadata entity_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entity_metadata
    ADD CONSTRAINT entity_metadata_pkey PRIMARY KEY (id);


--
-- Name: archived_events events_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.archived_events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- Name: events events_pkey_new; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_pkey_new PRIMARY KEY (id);


--
-- Name: experiment_variations experiment_variations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.experiment_variations
    ADD CONSTRAINT experiment_variations_pkey PRIMARY KEY (id);


--
-- Name: experiment_variations_users experiment_variations_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.experiment_variations_users
    ADD CONSTRAINT experiment_variations_users_pkey PRIMARY KEY (id);


--
-- Name: experiments experiments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.experiments
    ADD CONSTRAINT experiments_pkey PRIMARY KEY (id);


--
-- Name: fulltext_items fulltext_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fulltext_items
    ADD CONSTRAINT fulltext_items_pkey PRIMARY KEY (id);


--
-- Name: global_metadata global_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.global_metadata
    ADD CONSTRAINT global_metadata_pkey PRIMARY KEY (id);


--
-- Name: hiring_applications hiring_applications_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hiring_applications
    ADD CONSTRAINT hiring_applications_pkey PRIMARY KEY (id);


--
-- Name: hiring_applications_versions hiring_applications_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hiring_applications_versions
    ADD CONSTRAINT hiring_applications_versions_pkey PRIMARY KEY (version_id);


--
-- Name: hiring_relationships hiring_relationships_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hiring_relationships
    ADD CONSTRAINT hiring_relationships_pkey PRIMARY KEY (id);


--
-- Name: hiring_relationships_versions hiring_relationships_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hiring_relationships_versions
    ADD CONSTRAINT hiring_relationships_versions_pkey PRIMARY KEY (version_id);


--
-- Name: hiring_teams hiring_teams_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hiring_teams
    ADD CONSTRAINT hiring_teams_pkey PRIMARY KEY (id);


--
-- Name: hiring_teams_versions hiring_teams_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hiring_teams_versions
    ADD CONSTRAINT hiring_teams_versions_pkey PRIMARY KEY (version_id);


--
-- Name: institutions institutions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.institutions
    ADD CONSTRAINT institutions_pkey PRIMARY KEY (id);


--
-- Name: institutions_reports_viewers institutions_reports_viewers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.institutions_reports_viewers
    ADD CONSTRAINT institutions_reports_viewers_pkey PRIMARY KEY (id);


--
-- Name: lesson_locale_packs lesson_locale_packs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_locale_packs
    ADD CONSTRAINT lesson_locale_packs_pkey PRIMARY KEY (id);


--
-- Name: lesson_locale_packs_versions lesson_locale_packs_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_locale_packs_versions
    ADD CONSTRAINT lesson_locale_packs_versions_pkey PRIMARY KEY (version_id);


--
-- Name: lesson_progress lesson_progress_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_progress
    ADD CONSTRAINT lesson_progress_pkey PRIMARY KEY (id);


--
-- Name: lesson_stream_locale_packs lesson_stream_locale_packs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_stream_locale_packs
    ADD CONSTRAINT lesson_stream_locale_packs_pkey PRIMARY KEY (id);


--
-- Name: lesson_stream_locale_packs_users lesson_stream_locale_packs_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_stream_locale_packs_users
    ADD CONSTRAINT lesson_stream_locale_packs_users_pkey PRIMARY KEY (id);


--
-- Name: lesson_stream_locale_packs_users_versions lesson_stream_locale_packs_users_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_stream_locale_packs_users_versions
    ADD CONSTRAINT lesson_stream_locale_packs_users_versions_pkey PRIMARY KEY (version_id);


--
-- Name: lesson_stream_locale_packs_versions lesson_stream_locale_packs_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_stream_locale_packs_versions
    ADD CONSTRAINT lesson_stream_locale_packs_versions_pkey PRIMARY KEY (version_id);


--
-- Name: lesson_streams lesson_streams_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_streams
    ADD CONSTRAINT lesson_streams_pkey PRIMARY KEY (id);


--
-- Name: lesson_streams_progress lesson_streams_progress_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_streams_progress
    ADD CONSTRAINT lesson_streams_progress_pkey PRIMARY KEY (id);


--
-- Name: lesson_streams_versions lesson_streams_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_streams_versions
    ADD CONSTRAINT lesson_streams_versions_pkey PRIMARY KEY (version_id);


--
-- Name: lesson_to_stream_joins lesson_to_stream_joins_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_to_stream_joins
    ADD CONSTRAINT lesson_to_stream_joins_pkey PRIMARY KEY (id);


--
-- Name: lessons lessons_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lessons
    ADD CONSTRAINT lessons_pkey PRIMARY KEY (id);


--
-- Name: lessons_versions lessons_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lessons_versions
    ADD CONSTRAINT lessons_versions_pkey PRIMARY KEY (version_id);


--
-- Name: mailboxer_conversation_opt_outs mailboxer_conversation_opt_outs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailboxer_conversation_opt_outs
    ADD CONSTRAINT mailboxer_conversation_opt_outs_pkey PRIMARY KEY (id);


--
-- Name: mailboxer_conversations mailboxer_conversations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailboxer_conversations
    ADD CONSTRAINT mailboxer_conversations_pkey PRIMARY KEY (id);


--
-- Name: mailboxer_notifications mailboxer_notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailboxer_notifications
    ADD CONSTRAINT mailboxer_notifications_pkey PRIMARY KEY (id);


--
-- Name: mailboxer_receipts mailboxer_receipts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailboxer_receipts
    ADD CONSTRAINT mailboxer_receipts_pkey PRIMARY KEY (id);


--
-- Name: multiple_choice_challenge_progress multiple_choice_challenge_progress_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.multiple_choice_challenge_progress
    ADD CONSTRAINT multiple_choice_challenge_progress_pkey PRIMARY KEY (id);


--
-- Name: multiple_choice_challenge_usage_reports multiple_choice_challenge_usage_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.multiple_choice_challenge_usage_reports
    ADD CONSTRAINT multiple_choice_challenge_usage_reports_pkey PRIMARY KEY (id);


--
-- Name: open_positions open_positions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.open_positions
    ADD CONSTRAINT open_positions_pkey PRIMARY KEY (id);


--
-- Name: open_positions_skills_options open_positions_skills_options_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.open_positions_skills_options
    ADD CONSTRAINT open_positions_skills_options_pkey PRIMARY KEY (id);


--
-- Name: open_positions_skills_options_versions open_positions_skills_options_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.open_positions_skills_options_versions
    ADD CONSTRAINT open_positions_skills_options_versions_pkey PRIMARY KEY (version_id);


--
-- Name: open_positions_versions open_positions_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.open_positions_versions
    ADD CONSTRAINT open_positions_versions_pkey PRIMARY KEY (version_id);


--
-- Name: peer_recommendations peer_recommendations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.peer_recommendations
    ADD CONSTRAINT peer_recommendations_pkey PRIMARY KEY (id);


--
-- Name: peer_recommendations_versions peer_recommendations_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.peer_recommendations_versions
    ADD CONSTRAINT peer_recommendations_versions_pkey PRIMARY KEY (version_id);


--
-- Name: player_lesson_sessions player_lesson_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.player_lesson_sessions
    ADD CONSTRAINT player_lesson_sessions_pkey PRIMARY KEY (id);


--
-- Name: playlist_locale_packs playlist_locale_packs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.playlist_locale_packs
    ADD CONSTRAINT playlist_locale_packs_pkey PRIMARY KEY (id);


--
-- Name: playlist_locale_packs_versions playlist_locale_packs_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.playlist_locale_packs_versions
    ADD CONSTRAINT playlist_locale_packs_versions_pkey PRIMARY KEY (version_id);


--
-- Name: playlists playlists_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.playlists
    ADD CONSTRAINT playlists_pkey PRIMARY KEY (id);


--
-- Name: playlists_versions playlists_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.playlists_versions
    ADD CONSTRAINT playlists_versions_pkey PRIMARY KEY (version_id);


--
-- Name: professional_organization_options professional_organization_options_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.professional_organization_options
    ADD CONSTRAINT professional_organization_options_pkey PRIMARY KEY (id);


--
-- Name: professional_organization_options_versions professional_organization_options_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.professional_organization_options_versions
    ADD CONSTRAINT professional_organization_options_versions_pkey PRIMARY KEY (version_id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: s3_assets s3_assets_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.s3_assets
    ADD CONSTRAINT s3_assets_pkey PRIMARY KEY (id);


--
-- Name: s3_exercise_documents s3_exercise_documents_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.s3_exercise_documents
    ADD CONSTRAINT s3_exercise_documents_pkey PRIMARY KEY (id);


--
-- Name: s3_identification_assets s3_identification_assets_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.s3_identification_assets
    ADD CONSTRAINT s3_identification_assets_pkey PRIMARY KEY (id);


--
-- Name: s3_project_documents s3_project_documents_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.s3_project_documents
    ADD CONSTRAINT s3_project_documents_pkey PRIMARY KEY (id);


--
-- Name: s3_transcript_assets s3_transcript_assets_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.s3_transcript_assets
    ADD CONSTRAINT s3_transcript_assets_pkey PRIMARY KEY (id);


--
-- Name: schedulable_items schedulable_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schedulable_items
    ADD CONSTRAINT schedulable_items_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: skills_options skills_options_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.skills_options
    ADD CONSTRAINT skills_options_pkey PRIMARY KEY (id);


--
-- Name: skills_options_versions skills_options_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.skills_options_versions
    ADD CONSTRAINT skills_options_versions_pkey PRIMARY KEY (version_id);


--
-- Name: student_network_interests_options student_network_interests_options_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.student_network_interests_options
    ADD CONSTRAINT student_network_interests_options_pkey PRIMARY KEY (id);


--
-- Name: student_network_interests_options_versions student_network_interests_options_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.student_network_interests_options_versions
    ADD CONSTRAINT student_network_interests_options_versions_pkey PRIMARY KEY (version_id);


--
-- Name: subscriptions_hiring_teams subscriptions_hiring_teams_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriptions_hiring_teams
    ADD CONSTRAINT subscriptions_hiring_teams_pkey PRIMARY KEY (id);


--
-- Name: subscriptions subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriptions
    ADD CONSTRAINT subscriptions_pkey PRIMARY KEY (id);


--
-- Name: subscriptions_users subscriptions_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriptions_users
    ADD CONSTRAINT subscriptions_users_pkey PRIMARY KEY (id);


--
-- Name: tabular_things tabular_things_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tabular_things
    ADD CONSTRAINT tabular_things_pkey PRIMARY KEY (id);


--
-- Name: time_series_things time_series_things_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.time_series_things
    ADD CONSTRAINT time_series_things_pkey PRIMARY KEY (id);


--
-- Name: user_chance_of_graduating_records user_chance_of_graduating_records_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_chance_of_graduating_records
    ADD CONSTRAINT user_chance_of_graduating_records_pkey PRIMARY KEY (id);


--
-- Name: user_participation_scores user_participation_scores_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_participation_scores
    ADD CONSTRAINT user_participation_scores_pkey PRIMARY KEY (id);


--
-- Name: user_project_scores user_project_scores_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_project_scores
    ADD CONSTRAINT user_project_scores_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: work_experiences work_experiences_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.work_experiences
    ADD CONSTRAINT work_experiences_pkey PRIMARY KEY (id);


--
-- Name: work_experiences_versions work_experiences_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.work_experiences_versions
    ADD CONSTRAINT work_experiences_versions_pkey PRIMARY KEY (version_id);


--
-- Name: awards_and_interests_options_auto_suggest; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX awards_and_interests_options_auto_suggest ON public.awards_and_interests_options USING btree (suggest, locale);


--
-- Name: awards_and_interests_options_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX awards_and_interests_options_unique ON public.awards_and_interests_options USING btree (locale, text);


--
-- Name: awards_and_interests_options_versions_id_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX awards_and_interests_options_versions_id_updated_at ON public.awards_and_interests_options_versions USING btree (id, updated_at);


--
-- Name: candidate_position_interests_versions_id_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX candidate_position_interests_versions_id_updated_at ON public.candidate_position_interests_versions USING btree (id, updated_at);


--
-- Name: career_profile_ft_sn_anon_keyword_vec; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX career_profile_ft_sn_anon_keyword_vec ON public.career_profile_fulltext USING gin (student_network_anonymous_keyword_vector);


--
-- Name: career_profile_ft_sn_full_keyword_vec; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX career_profile_ft_sn_full_keyword_vec ON public.career_profile_fulltext USING gin (student_network_full_keyword_vector);


--
-- Name: career_profile_fulltext_keyword_text; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX career_profile_fulltext_keyword_text ON public.career_profile_fulltext USING gin (keyword_text public.gin_trgm_ops);


--
-- Name: career_profile_fulltext_sn_anon_keyword_text; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX career_profile_fulltext_sn_anon_keyword_text ON public.career_profile_fulltext USING gin (student_network_anonymous_keyword_text public.gin_trgm_ops);


--
-- Name: career_profile_fulltext_sn_full_keyword_text; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX career_profile_fulltext_sn_full_keyword_text ON public.career_profile_fulltext USING gin (student_network_full_keyword_text public.gin_trgm_ops);


--
-- Name: career_profiles_awards_and_interests_options_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX career_profiles_awards_and_interests_options_unique ON public.career_profiles_awards_and_interests_options USING btree (career_profile_id, awards_and_interests_option_id);


--
-- Name: career_profiles_on_location; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX career_profiles_on_location ON public.career_profiles USING gist (location);


--
-- Name: career_profiles_skills_options_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX career_profiles_skills_options_unique ON public.career_profiles_skills_options USING btree (career_profile_id, skills_option_id);


--
-- Name: career_profiles_student_network_interests_options_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX career_profiles_student_network_interests_options_unique ON public.career_profiles_student_network_interests_options USING btree (career_profile_id, student_network_interests_option_id);


--
-- Name: coh_and_user_on_coh_app_plus; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX coh_and_user_on_coh_app_plus ON public.cohort_applications_plus USING btree (cohort_id, user_id);


--
-- Name: coh_id_index_on_publ_coh_per_str_loc_packs; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX coh_id_index_on_publ_coh_per_str_loc_packs ON public.published_cohort_periods_stream_locale_packs USING btree (cohort_id, index, locale_pack_id);


--
-- Name: coh_id_index_on_publ_coh_periods; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX coh_id_index_on_publ_coh_periods ON public.published_cohort_periods USING btree (cohort_id, index);


--
-- Name: coh_user__index_on_coh_user_periods; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX coh_user__index_on_coh_user_periods ON public.cohort_user_periods USING btree (cohort_id, user_id, index);


--
-- Name: cohort_and_lesson_lp_on_coh_lesson_lps; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX cohort_and_lesson_lp_on_coh_lesson_lps ON public.published_cohort_lesson_locale_packs USING btree (cohort_id, lesson_locale_pack_id);


--
-- Name: cohort_and_play_lp_on_coh_play_lps; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX cohort_and_play_lp_on_coh_play_lps ON public.published_cohort_playlist_locale_packs USING btree (cohort_id, playlist_locale_pack_id);


--
-- Name: cohort_and_str_lp_on_cohort_str_lps; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX cohort_and_str_lp_on_cohort_str_lps ON public.published_cohort_stream_locale_packs USING btree (cohort_id, stream_locale_pack_id);


--
-- Name: cohort_applications_cohorts_users; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX cohort_applications_cohorts_users ON public.cohort_applications USING btree (cohort_id, user_id);


--
-- Name: default_prioritization_on_cp_search_helpers; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX default_prioritization_on_cp_search_helpers ON public.career_profile_search_helpers USING btree (default_prioritization);


--
-- Name: delayed_jobs_priority; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX delayed_jobs_priority ON public.delayed_jobs USING btree (priority, run_at);


--
-- Name: delayed_jobs_queue; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX delayed_jobs_queue ON public.delayed_jobs USING btree (queue);


--
-- Name: edexp_school_names_on_cp_search_helpers; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX edexp_school_names_on_cp_search_helpers ON public.career_profile_search_helpers USING gin (education_experience_school_names public.gin_trgm_ops);


--
-- Name: educational_organization_options_auto_suggest; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX educational_organization_options_auto_suggest ON public.educational_organization_options USING btree (suggest, locale);


--
-- Name: educational_organization_options_text; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX educational_organization_options_text ON public.educational_organization_options USING gin (text public.gin_trgm_ops);


--
-- Name: educational_organization_options_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX educational_organization_options_unique ON public.educational_organization_options USING btree (locale, text);


--
-- Name: educational_organization_options_versions_id_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX educational_organization_options_versions_id_updated_at ON public.educational_organization_options_versions USING btree (id, updated_at);


--
-- Name: hir_rel_index_manager_candidate; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX hir_rel_index_manager_candidate ON public.hiring_relationships USING btree (hiring_manager_id, candidate_id);


--
-- Name: hm_status_and_candidate_on_hiring_relationships; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX hm_status_and_candidate_on_hiring_relationships ON public.hiring_relationships USING btree (hiring_manager_status, candidate_id);


--
-- Name: id_on_internal_content_titles; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX id_on_internal_content_titles ON public.internal_content_titles USING btree (id);


--
-- Name: idx_career_profiles_versions_id_updt; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_career_profiles_versions_id_updt ON public.career_profiles_versions USING btree (id, updated_at);


--
-- Name: idx_education_experiences_versions_id_updt; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_education_experiences_versions_id_updt ON public.education_experiences_versions USING btree (id, updated_at);


--
-- Name: idx_work_experiences_versions_id_updt; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_work_experiences_versions_id_updt ON public.work_experiences_versions USING btree (id, updated_at);


--
-- Name: in_school_and_default_prioritization_on_cp_search_helpers; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX in_school_and_default_prioritization_on_cp_search_helpers ON public.career_profile_search_helpers USING btree (in_school, default_prioritization DESC);


--
-- Name: index_access_groups_and_institutions; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_access_groups_and_institutions ON public.access_groups_institutions USING btree (access_group_id, institution_id);


--
-- Name: index_access_groups_and_users; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_access_groups_and_users ON public.access_groups_users USING btree (access_group_id, user_id);


--
-- Name: index_access_groups_institutions_versions_on_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_access_groups_institutions_versions_on_id ON public.access_groups_institutions_versions USING btree (id);


--
-- Name: index_access_groups_lesson_stream_locale_packs; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_access_groups_lesson_stream_locale_packs ON public.access_groups_lesson_stream_locale_packs USING btree (access_group_id, locale_pack_id);


--
-- Name: index_access_groups_lesson_stream_locale_packs_versions_on_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_access_groups_lesson_stream_locale_packs_versions_on_id ON public.access_groups_lesson_stream_locale_packs_versions USING btree (id);


--
-- Name: index_access_groups_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_access_groups_on_name ON public.access_groups USING btree (name);


--
-- Name: index_access_groups_playlist_locale_packs; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_access_groups_playlist_locale_packs ON public.access_groups_playlist_locale_packs USING btree (access_group_id, locale_pack_id);


--
-- Name: index_access_groups_playlist_locale_packs_versions_on_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_access_groups_playlist_locale_packs_versions_on_id ON public.access_groups_playlist_locale_packs_versions USING btree (id);


--
-- Name: index_access_groups_users_on_user_id_and_access_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_access_groups_users_on_user_id_and_access_group_id ON public.access_groups_users USING btree (user_id, access_group_id);


--
-- Name: index_access_groups_users_versions_on_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_access_groups_users_versions_on_id ON public.access_groups_users_versions USING btree (id);


--
-- Name: index_access_groups_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_access_groups_versions_on_id_and_updated_at ON public.access_groups_versions USING btree (id, updated_at);


--
-- Name: index_acquisitions_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_acquisitions_on_user_id ON public.acquisitions USING btree (user_id);


--
-- Name: index_candidate_position_interests_on_open_position_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_candidate_position_interests_on_open_position_id ON public.candidate_position_interests USING btree (open_position_id);


--
-- Name: index_career_profile_fulltext_on_career_profile_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_career_profile_fulltext_on_career_profile_id ON public.career_profile_fulltext USING btree (career_profile_id);


--
-- Name: index_career_profile_fulltext_on_keyword_vector; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_career_profile_fulltext_on_keyword_vector ON public.career_profile_fulltext USING gin (keyword_vector);


--
-- Name: index_career_profile_lists_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_career_profile_lists_versions_on_id_and_updated_at ON public.career_profile_lists_versions USING btree (id, updated_at);


--
-- Name: index_career_profile_search_helpers_on_career_profile_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_career_profile_search_helpers_on_career_profile_id ON public.career_profile_search_helpers USING btree (career_profile_id);


--
-- Name: index_career_profile_searches_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_career_profile_searches_on_user_id ON public.career_profile_searches USING btree (user_id);


--
-- Name: index_career_profiles_on_do_not_create_relationships; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_career_profiles_on_do_not_create_relationships ON public.career_profiles USING btree (do_not_create_relationships);


--
-- Name: index_career_profiles_on_locations_of_interest; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_career_profiles_on_locations_of_interest ON public.career_profiles USING gin (locations_of_interest);


--
-- Name: index_career_profiles_on_percent_complete_and_interest_level; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_career_profiles_on_percent_complete_and_interest_level ON public.career_profiles USING btree (last_calculated_complete_percentage, interested_in_joining_new_company);


--
-- Name: index_career_profiles_on_primary_areas_of_interest; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_career_profiles_on_primary_areas_of_interest ON public.career_profiles USING gin (primary_areas_of_interest);


--
-- Name: index_career_profiles_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_career_profiles_on_user_id ON public.career_profiles USING btree (user_id);


--
-- Name: index_cohort_applications_on_should_invite_to_reapply; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cohort_applications_on_should_invite_to_reapply ON public.cohort_applications USING btree (should_invite_to_reapply);


--
-- Name: index_cohort_applications_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_cohort_applications_on_user_id ON public.cohort_applications USING btree (user_id) WHERE (status = ANY (ARRAY['accepted'::text, 'pending'::text, 'pre_accepted'::text, 'emba_targeted'::text]));


--
-- Name: index_cohort_applications_plus_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cohort_applications_plus_on_user_id ON public.cohort_applications_plus USING btree (user_id);


--
-- Name: index_cohort_applications_plus_on_was_accepted; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cohort_applications_plus_on_was_accepted ON public.cohort_applications_plus USING btree (was_accepted);


--
-- Name: index_cohort_applications_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cohort_applications_versions_on_id_and_updated_at ON public.cohort_applications_versions USING btree (id, updated_at);


--
-- Name: index_cohort_conversion_rates_on_cohort_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_cohort_conversion_rates_on_cohort_id ON public.cohort_conversion_rates USING btree (cohort_id);


--
-- Name: index_cohort_promotions_on_cohort_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_cohort_promotions_on_cohort_id ON public.cohort_promotions USING btree (cohort_id);


--
-- Name: index_cohort_promotions_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cohort_promotions_versions_on_id_and_updated_at ON public.cohort_promotions_versions USING btree (id, updated_at);


--
-- Name: index_cohort_user_progress_records_on_cohort_id_and_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_cohort_user_progress_records_on_cohort_id_and_user_id ON public.cohort_user_progress_records USING btree (cohort_id, user_id);


--
-- Name: index_cohorts_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cohorts_versions_on_id_and_updated_at ON public.cohorts_versions USING btree (id, updated_at);


--
-- Name: index_content_publishers_on_cohort_version_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_content_publishers_on_cohort_version_id ON public.content_publishers USING btree (cohort_version_id);


--
-- Name: index_content_publishers_on_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_content_publishers_on_created_at ON public.content_publishers USING btree (created_at);


--
-- Name: index_content_publishers_on_lesson_stream_version_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_content_publishers_on_lesson_stream_version_id ON public.content_publishers USING btree (lesson_stream_version_id);


--
-- Name: index_content_publishers_on_lesson_version_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_content_publishers_on_lesson_version_id ON public.content_publishers USING btree (lesson_version_id);


--
-- Name: index_content_publishers_on_playlist_version_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_content_publishers_on_playlist_version_id ON public.content_publishers USING btree (playlist_version_id);


--
-- Name: index_content_publishers_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_content_publishers_versions_on_id_and_updated_at ON public.content_publishers_versions USING btree (id, updated_at);


--
-- Name: index_content_topics_lesson_stream_locale_packs_versions_on_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_content_topics_lesson_stream_locale_packs_versions_on_id ON public.content_topics_lesson_stream_locale_packs_versions USING btree (id);


--
-- Name: index_content_topics_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_content_topics_versions_on_id_and_updated_at ON public.content_topics_versions USING btree (id, updated_at);


--
-- Name: index_curriculum_templates_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_curriculum_templates_versions_on_id_and_updated_at ON public.curriculum_templates_versions USING btree (id, updated_at);


--
-- Name: index_distinct_user_ids_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_distinct_user_ids_on_user_id ON public.distinct_user_ids USING btree (user_id);


--
-- Name: index_editor_lesson_sessions_on_started_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_editor_lesson_sessions_on_started_at ON public.editor_lesson_sessions USING btree (started_at);


--
-- Name: index_editor_lesson_sessions_on_user_id_and_lesson_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_editor_lesson_sessions_on_user_id_and_lesson_id ON public.editor_lesson_sessions USING btree (user_id, lesson_id);


--
-- Name: index_education_experiences_on_career_profile_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_education_experiences_on_career_profile_id ON public.education_experiences USING btree (career_profile_id);


--
-- Name: index_entity_metadata_on_image_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_entity_metadata_on_image_id ON public.entity_metadata USING btree (image_id);


--
-- Name: index_events_on_created_at_new; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_on_created_at_new ON public.events USING btree (created_at);


--
-- Name: index_events_on_event_type_and_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_on_event_type_and_created_at ON public.archived_events USING btree (event_type, created_at);


--
-- Name: index_events_on_event_type_and_created_at_new; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_on_event_type_and_created_at_new ON public.events USING btree (event_type, created_at);


--
-- Name: index_events_on_event_type_and_estimated_time; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_on_event_type_and_estimated_time ON public.archived_events USING btree (event_type, estimated_time);


--
-- Name: index_events_on_event_type_and_estimated_time_new; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_on_event_type_and_estimated_time_new ON public.events USING btree (event_type, estimated_time);


--
-- Name: index_events_on_user_id_and_estimated_time; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_on_user_id_and_estimated_time ON public.archived_events USING btree (user_id, estimated_time);


--
-- Name: index_events_on_user_id_and_estimated_time_new; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_on_user_id_and_estimated_time_new ON public.events USING btree (user_id, estimated_time);


--
-- Name: index_events_on_user_id_and_event_type_and_created_at_new; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_on_user_id_and_event_type_and_created_at_new ON public.events USING btree (user_id, event_type, created_at);


--
-- Name: index_events_on_user_id_and_event_type_and_estimated_time; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_on_user_id_and_event_type_and_estimated_time ON public.archived_events USING btree (user_id, event_type, estimated_time);


--
-- Name: index_events_on_user_id_and_event_type_and_estimated_time_new; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_on_user_id_and_event_type_and_estimated_time_new ON public.events USING btree (user_id, event_type, estimated_time);


--
-- Name: index_experiment_variations_on_experiment_id_and_title; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_experiment_variations_on_experiment_id_and_title ON public.experiment_variations USING btree (experiment_id, title);


--
-- Name: index_experiment_variations_on_optimizely_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_experiment_variations_on_optimizely_id ON public.experiment_variations USING btree (optimizely_id);


--
-- Name: index_experiment_variations_users_on_user_id_and_experiment_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_experiment_variations_users_on_user_id_and_experiment_id ON public.experiment_variations_users USING btree (user_id, experiment_id);


--
-- Name: index_experiments_on_optimizely_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_experiments_on_optimizely_id ON public.experiments USING btree (optimizely_id);


--
-- Name: index_experiments_on_title; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_experiments_on_title ON public.experiments USING btree (title);


--
-- Name: index_global_metadata_on_site_name; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_global_metadata_on_site_name ON public.global_metadata USING btree (site_name);


--
-- Name: index_hiring_applications_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_hiring_applications_on_user_id ON public.hiring_applications USING btree (user_id);


--
-- Name: index_hiring_applications_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_hiring_applications_versions_on_id_and_updated_at ON public.hiring_applications_versions USING btree (id, updated_at);


--
-- Name: index_hiring_relationships_on_candidate_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_hiring_relationships_on_candidate_id ON public.hiring_relationships USING btree (candidate_id);


--
-- Name: index_hiring_relationships_on_hiring_manager_status; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_hiring_relationships_on_hiring_manager_status ON public.hiring_relationships USING btree (hiring_manager_status);


--
-- Name: index_hiring_relationships_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_hiring_relationships_versions_on_id_and_updated_at ON public.hiring_relationships_versions USING btree (id, updated_at);


--
-- Name: index_hiring_teams_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_hiring_teams_versions_on_id_and_updated_at ON public.hiring_teams_versions USING btree (id, updated_at);


--
-- Name: index_institution_lesson_locale_packs_on_lesson_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_institution_lesson_locale_packs_on_lesson_locale_pack_id ON public.institution_lesson_locale_packs USING btree (lesson_locale_pack_id);


--
-- Name: index_institutions_on_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_institutions_on_created_at ON public.institutions USING btree (created_at);


--
-- Name: index_institutions_on_sign_up_code; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_institutions_on_sign_up_code ON public.institutions USING btree (sign_up_code);


--
-- Name: index_institutions_users_on_institution_id_and_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_institutions_users_on_institution_id_and_user_id ON public.institutions_users USING btree (institution_id, user_id);


--
-- Name: index_institutions_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_institutions_versions_on_id_and_updated_at ON public.institutions_versions USING btree (id, updated_at);


--
-- Name: index_interests_candidate__id_and_open_position_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_interests_candidate__id_and_open_position_id ON public.candidate_position_interests USING btree (candidate_id, open_position_id);


--
-- Name: index_lesson_fulltext_on_comments_vector; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lesson_fulltext_on_comments_vector ON public.lesson_fulltext USING gin (comments_vector);


--
-- Name: index_lesson_fulltext_on_content_vector; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lesson_fulltext_on_content_vector ON public.lesson_fulltext USING gin (content_vector);


--
-- Name: index_lesson_fulltext_on_lesson_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_lesson_fulltext_on_lesson_id ON public.lesson_fulltext USING btree (lesson_id);


--
-- Name: index_lesson_locale_packs_on_practice_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_lesson_locale_packs_on_practice_locale_pack_id ON public.lesson_locale_packs USING btree (practice_locale_pack_id);


--
-- Name: index_lesson_locale_packs_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lesson_locale_packs_versions_on_id_and_updated_at ON public.lesson_locale_packs_versions USING btree (id, updated_at);


--
-- Name: index_lesson_progress_on_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lesson_progress_on_created_at ON public.lesson_progress USING btree (created_at);


--
-- Name: index_lesson_progress_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lesson_progress_on_user_id ON public.lesson_progress USING btree (user_id);


--
-- Name: index_lesson_progress_on_user_id_and_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_lesson_progress_on_user_id_and_locale_pack_id ON public.lesson_progress USING btree (user_id, locale_pack_id);


--
-- Name: index_lesson_stream_locale_packs_users_versions_on_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lesson_stream_locale_packs_users_versions_on_id ON public.lesson_stream_locale_packs_users_versions USING btree (id);


--
-- Name: index_lesson_stream_locale_packs_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lesson_stream_locale_packs_versions_on_id_and_updated_at ON public.lesson_stream_locale_packs_versions USING btree (id, updated_at);


--
-- Name: index_lesson_streams_on_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lesson_streams_on_created_at ON public.lesson_streams USING btree (created_at);


--
-- Name: index_lesson_streams_on_locale_pack_id_and_locale; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_lesson_streams_on_locale_pack_id_and_locale ON public.lesson_streams USING btree (locale_pack_id, locale);


--
-- Name: index_lesson_streams_progress_on_completed_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lesson_streams_progress_on_completed_at ON public.lesson_streams_progress USING btree (completed_at);


--
-- Name: index_lesson_streams_progress_on_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lesson_streams_progress_on_created_at ON public.lesson_streams_progress USING btree (created_at);


--
-- Name: index_lesson_streams_progress_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lesson_streams_progress_on_user_id ON public.lesson_streams_progress USING btree (user_id);


--
-- Name: index_lesson_streams_progress_on_user_id_and_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_lesson_streams_progress_on_user_id_and_locale_pack_id ON public.lesson_streams_progress USING btree (user_id, locale_pack_id);


--
-- Name: index_lesson_streams_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_lesson_streams_versions_on_id_and_updated_at ON public.lesson_streams_versions USING btree (id, updated_at);


--
-- Name: index_lesson_streams_versions_on_locale; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lesson_streams_versions_on_locale ON public.lesson_streams_versions USING btree (locale);


--
-- Name: index_lesson_streams_versions_on_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lesson_streams_versions_on_locale_pack_id ON public.lesson_streams_versions USING btree (locale_pack_id);


--
-- Name: index_lesson_to_stream_joins_on_lesson_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lesson_to_stream_joins_on_lesson_id ON public.lesson_to_stream_joins USING btree (lesson_id);


--
-- Name: index_lesson_to_stream_joins_on_stream_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lesson_to_stream_joins_on_stream_id ON public.lesson_to_stream_joins USING btree (stream_id);


--
-- Name: index_lesson_to_stream_joins_versions_on_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lesson_to_stream_joins_versions_on_id ON public.lesson_to_stream_joins_versions USING btree (id);


--
-- Name: index_lessons_on_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lessons_on_created_at ON public.lessons USING btree (created_at);


--
-- Name: index_lessons_on_locale_pack_id_and_locale; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_lessons_on_locale_pack_id_and_locale ON public.lessons USING btree (locale_pack_id, locale);


--
-- Name: index_lessons_on_title; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lessons_on_title ON public.lessons USING btree (title);


--
-- Name: index_lessons_versions_on_author_id_and_archived; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lessons_versions_on_author_id_and_archived ON public.lessons_versions USING btree (author_id, archived);


--
-- Name: index_lessons_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_lessons_versions_on_id_and_updated_at ON public.lessons_versions USING btree (id, updated_at);


--
-- Name: index_lessons_versions_on_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_lessons_versions_on_locale_pack_id ON public.lessons_versions USING btree (locale_pack_id);


--
-- Name: index_mailboxer_conversation_opt_outs_on_conversation_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailboxer_conversation_opt_outs_on_conversation_id ON public.mailboxer_conversation_opt_outs USING btree (conversation_id);


--
-- Name: index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type ON public.mailboxer_conversation_opt_outs USING btree (unsubscriber_id, unsubscriber_type);


--
-- Name: index_mailboxer_notifications_on_conversation_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailboxer_notifications_on_conversation_id ON public.mailboxer_notifications USING btree (conversation_id);


--
-- Name: index_mailboxer_notifications_on_notified_object_id_and_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailboxer_notifications_on_notified_object_id_and_type ON public.mailboxer_notifications USING btree (notified_object_id, notified_object_type);


--
-- Name: index_mailboxer_notifications_on_sender_id_and_sender_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailboxer_notifications_on_sender_id_and_sender_type ON public.mailboxer_notifications USING btree (sender_id, sender_type);


--
-- Name: index_mailboxer_notifications_on_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailboxer_notifications_on_type ON public.mailboxer_notifications USING btree (type);


--
-- Name: index_mailboxer_receipts_on_notification_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailboxer_receipts_on_notification_id ON public.mailboxer_receipts USING btree (notification_id);


--
-- Name: index_mailboxer_receipts_on_receiver_id_and_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailboxer_receipts_on_receiver_id_and_created_at ON public.mailboxer_receipts USING btree (receiver_id, created_at);


--
-- Name: index_mailboxer_receipts_on_receiver_id_and_receiver_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_mailboxer_receipts_on_receiver_id_and_receiver_type ON public.mailboxer_receipts USING btree (receiver_id, receiver_type);


--
-- Name: index_multiple_choice_challenge_progress_on_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_multiple_choice_challenge_progress_on_created_at ON public.multiple_choice_challenge_progress USING btree (created_at);


--
-- Name: index_multiple_choice_challenge_usage_reports_on_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_multiple_choice_challenge_usage_reports_on_created_at ON public.multiple_choice_challenge_usage_reports USING btree (created_at);


--
-- Name: index_open_positions_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_open_positions_versions_on_id_and_updated_at ON public.open_positions_versions USING btree (id, updated_at);


--
-- Name: index_player_lesson_sessions_on_last_lesson_activity_time; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_player_lesson_sessions_on_last_lesson_activity_time ON public.player_lesson_sessions USING btree (last_lesson_activity_time DESC);


--
-- Name: index_player_lesson_sessions_on_started_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_player_lesson_sessions_on_started_at ON public.player_lesson_sessions USING btree (started_at);


--
-- Name: index_playlist_locale_packs_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_playlist_locale_packs_versions_on_id_and_updated_at ON public.playlist_locale_packs_versions USING btree (id, updated_at);


--
-- Name: index_playlist_progress_on_user_id_and_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_playlist_progress_on_user_id_and_locale_pack_id ON public.playlist_progress USING btree (user_id, locale_pack_id);


--
-- Name: index_playlists_on_locale_pack_id_and_locale; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_playlists_on_locale_pack_id_and_locale ON public.playlists USING btree (locale_pack_id, locale);


--
-- Name: index_playlists_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_playlists_versions_on_id_and_updated_at ON public.playlists_versions USING btree (id, updated_at);


--
-- Name: index_playlists_versions_on_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_playlists_versions_on_locale_pack_id ON public.playlists_versions USING btree (locale_pack_id);


--
-- Name: index_published_cohort_content_details_on_cohort_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_published_cohort_content_details_on_cohort_id ON public.published_cohort_content_details USING btree (cohort_id);


--
-- Name: index_published_lesson_locale_packs_on_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_published_lesson_locale_packs_on_locale_pack_id ON public.published_lesson_locale_packs USING btree (locale_pack_id);


--
-- Name: index_published_lessons_on_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_published_lessons_on_id ON public.published_lessons USING btree (id);


--
-- Name: index_published_lessons_on_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_published_lessons_on_locale_pack_id ON public.published_lessons USING btree (locale_pack_id);


--
-- Name: index_published_playlist_lessons_on_lesson_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_published_playlist_lessons_on_lesson_locale_pack_id ON public.published_playlist_lessons USING btree (lesson_locale_pack_id);


--
-- Name: index_published_playlist_lessons_on_playlist_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_published_playlist_lessons_on_playlist_locale_pack_id ON public.published_playlist_lessons USING btree (playlist_locale_pack_id);


--
-- Name: index_published_playlist_lessons_on_stream_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_published_playlist_lessons_on_stream_id ON public.published_playlist_lessons USING btree (stream_id);


--
-- Name: index_published_playlist_lessons_on_stream_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_published_playlist_lessons_on_stream_locale_pack_id ON public.published_playlist_lessons USING btree (stream_locale_pack_id);


--
-- Name: index_published_playlist_locale_packs_on_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_published_playlist_locale_packs_on_locale_pack_id ON public.published_playlist_locale_packs USING btree (locale_pack_id);


--
-- Name: index_published_playlist_streams_on_playlist_id_and_stream_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_published_playlist_streams_on_playlist_id_and_stream_id ON public.published_playlist_streams USING btree (playlist_id, stream_id);


--
-- Name: index_published_playlist_streams_on_playlist_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_published_playlist_streams_on_playlist_locale_pack_id ON public.published_playlist_streams USING btree (playlist_locale_pack_id);


--
-- Name: index_published_playlist_streams_on_stream_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_published_playlist_streams_on_stream_id ON public.published_playlist_streams USING btree (stream_id);


--
-- Name: index_published_playlist_streams_on_stream_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_published_playlist_streams_on_stream_locale_pack_id ON public.published_playlist_streams USING btree (stream_locale_pack_id);


--
-- Name: index_published_playlists_on_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_published_playlists_on_id ON public.published_playlists USING btree (id);


--
-- Name: index_published_playlists_on_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_published_playlists_on_locale_pack_id ON public.published_playlists USING btree (locale_pack_id);


--
-- Name: index_published_stream_lessons_on_lesson_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_published_stream_lessons_on_lesson_locale_pack_id ON public.published_stream_lessons USING btree (lesson_locale_pack_id);


--
-- Name: index_published_stream_lessons_on_stream_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_published_stream_lessons_on_stream_id ON public.published_stream_lessons USING btree (stream_id);


--
-- Name: index_published_stream_lessons_on_stream_id_and_lesson_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_published_stream_lessons_on_stream_id_and_lesson_id ON public.published_stream_lessons USING btree (stream_id, lesson_id);


--
-- Name: index_published_stream_lessons_on_stream_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_published_stream_lessons_on_stream_locale_pack_id ON public.published_stream_lessons USING btree (stream_locale_pack_id);


--
-- Name: index_published_stream_locale_packs_on_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_published_stream_locale_packs_on_locale_pack_id ON public.published_stream_locale_packs USING btree (locale_pack_id);


--
-- Name: index_published_streams_on_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_published_streams_on_id ON public.published_streams USING btree (id);


--
-- Name: index_published_streams_on_locale_pack_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_published_streams_on_locale_pack_id ON public.published_streams USING btree (locale_pack_id);


--
-- Name: index_roles_on_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_roles_on_created_at ON public.roles USING btree (created_at);


--
-- Name: index_roles_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_roles_on_name ON public.roles USING btree (name);


--
-- Name: index_roles_on_name_and_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_roles_on_name_and_resource_type_and_resource_id ON public.roles USING btree (name, resource_type, resource_id);


--
-- Name: index_roles_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_roles_versions_on_id_and_updated_at ON public.roles_versions USING btree (id, updated_at);


--
-- Name: index_s3_assets_on_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_s3_assets_on_created_at ON public.s3_assets USING btree (created_at);


--
-- Name: index_s3_identification_assets_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_s3_identification_assets_on_user_id ON public.s3_identification_assets USING btree (user_id);


--
-- Name: index_s3_transcript_assets_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_s3_transcript_assets_on_user_id ON public.s3_transcript_assets USING btree (user_id);


--
-- Name: index_subscriptions_hiring_teams_on_subscription_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_subscriptions_hiring_teams_on_subscription_id ON public.subscriptions_hiring_teams USING btree (subscription_id);


--
-- Name: index_subscriptions_on_stripe_subscription_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_subscriptions_on_stripe_subscription_id ON public.subscriptions USING btree (stripe_subscription_id);


--
-- Name: index_subscriptions_users_on_subscription_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_subscriptions_users_on_subscription_id ON public.subscriptions_users USING btree (subscription_id);


--
-- Name: index_subscriptions_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscriptions_versions_on_id_and_updated_at ON public.subscriptions_versions USING btree (id, updated_at);


--
-- Name: index_unique_topics_packs; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_unique_topics_packs ON public.content_topics_lesson_stream_locale_packs USING btree (locale_pack_id, content_topic_id);


--
-- Name: index_user_lesson_progress_records_on_last_lesson_activity_time; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_lesson_progress_records_on_last_lesson_activity_time ON public.user_lesson_progress_records USING btree (last_lesson_activity_time);


--
-- Name: index_user_participation_scores_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_user_participation_scores_on_user_id ON public.user_participation_scores USING btree (user_id);


--
-- Name: index_user_progress_records_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_user_progress_records_on_user_id ON public.user_progress_records USING btree (user_id);


--
-- Name: index_user_project_scores_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_user_project_scores_on_user_id ON public.user_project_scores USING btree (user_id);


--
-- Name: index_users_on_can_edit_career_profile; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_can_edit_career_profile ON public.users USING btree (can_edit_career_profile);


--
-- Name: index_users_on_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_created_at ON public.users USING btree (created_at);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_email ON public.users USING btree (email);


--
-- Name: index_users_on_hiring_team_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_hiring_team_id ON public.users USING btree (hiring_team_id);


--
-- Name: index_users_on_last_seen_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_last_seen_at ON public.users USING btree (last_seen_at);


--
-- Name: index_users_on_phone; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_phone ON public.users USING btree (phone) WHERE ((provider)::text = 'phone_no_password'::text);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON public.users USING btree (reset_password_token);


--
-- Name: index_users_on_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_uid ON public.users USING btree (uid);


--
-- Name: index_users_on_unlock_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_unlock_token ON public.users USING btree (unlock_token);


--
-- Name: index_users_roles_on_user_id_and_role_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_roles_on_user_id_and_role_id ON public.users_roles USING btree (user_id, role_id);


--
-- Name: index_users_versions_on_id_and_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_versions_on_id_and_updated_at ON public.users_versions USING btree (id, updated_at);


--
-- Name: index_work_experiences_on_career_profile_id_and_end_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_work_experiences_on_career_profile_id_and_end_date ON public.work_experiences USING btree (career_profile_id, end_date);


--
-- Name: index_work_experiences_on_industry; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_work_experiences_on_industry ON public.work_experiences USING btree (industry);


--
-- Name: index_work_experiences_on_professional_organization_option_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_work_experiences_on_professional_organization_option_id ON public.work_experiences USING btree (professional_organization_option_id);


--
-- Name: index_work_experiences_on_role; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_work_experiences_on_role ON public.work_experiences USING btree (role);


--
-- Name: inst_and_lesson_lp_on_inst_lesson_lps; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX inst_and_lesson_lp_on_inst_lesson_lps ON public.institution_lesson_locale_packs USING btree (institution_id, stream_locale_pack_id, lesson_locale_pack_id);


--
-- Name: inst_and_play_lp_on_inst_play_lps; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX inst_and_play_lp_on_inst_play_lps ON public.institution_playlist_locale_packs USING btree (institution_id, playlist_locale_pack_id);


--
-- Name: inst_and_str_lp_on_inst_str_lps; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX inst_and_str_lp_on_inst_str_lps ON public.institution_stream_locale_packs USING btree (institution_id, stream_locale_pack_id);


--
-- Name: last_candidate_relationship_accepted_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX last_candidate_relationship_accepted_at ON public.hiring_relationships USING btree (candidate_id, hiring_manager_status, updated_at);


--
-- Name: last_hiring_manager_relationship_accepted_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX last_hiring_manager_relationship_accepted_at ON public.hiring_relationships USING btree (hiring_manager_id, candidate_status, updated_at);


--
-- Name: lesson_fulltext_comments_text; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX lesson_fulltext_comments_text ON public.lesson_fulltext USING gin (comments_text public.gin_trgm_ops);


--
-- Name: lesson_fulltext_content_text; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX lesson_fulltext_content_text ON public.lesson_fulltext USING gin (content_text public.gin_trgm_ops);


--
-- Name: lesson_lp_on_cohort_lesson_locale_packs; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX lesson_lp_on_cohort_lesson_locale_packs ON public.published_cohort_lesson_locale_packs USING btree (lesson_locale_pack_id);


--
-- Name: lesson_lp_on_pub_str_lesson_lp; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX lesson_lp_on_pub_str_lesson_lp ON public.published_stream_lesson_locale_packs USING btree (lesson_locale_pack_id);


--
-- Name: locale_and_lp_id_on_pub_content_titles; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX locale_and_lp_id_on_pub_content_titles ON public.published_content_titles USING btree (locale_pack_id, target_locale);


--
-- Name: multiple_choice_challenge_progress_ed_template; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX multiple_choice_challenge_progress_ed_template ON public.multiple_choice_challenge_progress USING btree (editor_template);


--
-- Name: multiple_choice_challenge_progress_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX multiple_choice_challenge_progress_unique ON public.multiple_choice_challenge_progress USING btree (user_id, lesson_id, frame_id, challenge_id);


--
-- Name: multiple_choice_challenge_usage_reports_ed_template; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX multiple_choice_challenge_usage_reports_ed_template ON public.multiple_choice_challenge_usage_reports USING btree (editor_template);


--
-- Name: multiple_choice_challenge_usage_reports_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX multiple_choice_challenge_usage_reports_unique ON public.multiple_choice_challenge_usage_reports USING btree (lesson_id, frame_id, challenge_id, answer_id);


--
-- Name: open_positions_skills_options_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX open_positions_skills_options_unique ON public.open_positions_skills_options USING btree (open_position_id, skills_option_id);


--
-- Name: open_positions_skills_options_versions_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX open_positions_skills_options_versions_id ON public.open_positions_skills_options_versions USING btree (id);


--
-- Name: page_load_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX page_load_id ON public.archived_events USING btree ((((payload ->> 'page_load_id'::text))::character varying), estimated_time, id);


--
-- Name: page_load_id_new; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX page_load_id_new ON public.events USING btree ((((payload ->> 'page_load_id'::text))::character varying), estimated_time, id);


--
-- Name: partial_grad_status_on_applications; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX partial_grad_status_on_applications ON public.cohort_applications USING btree (graduation_status) WHERE (status = 'accepted'::text);


--
-- Name: peer_recommendations_versions_id_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX peer_recommendations_versions_id_updated_at ON public.peer_recommendations_versions USING btree (id, updated_at);


--
-- Name: play_lps_on_inst_play_lps; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX play_lps_on_inst_play_lps ON public.institution_playlist_locale_packs USING btree (playlist_locale_pack_id);


--
-- Name: playlist_lp_on_cohort_playlist_packs; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX playlist_lp_on_cohort_playlist_packs ON public.published_cohort_playlist_locale_packs USING btree (playlist_locale_pack_id);


--
-- Name: playlist_versions_entity_image_author; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX playlist_versions_entity_image_author ON public.playlists_versions USING btree (entity_metadata_id, image_id, author_id);


--
-- Name: professional_organization_options_auto_suggest; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX professional_organization_options_auto_suggest ON public.professional_organization_options USING btree (suggest, locale);


--
-- Name: professional_organization_options_text; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX professional_organization_options_text ON public.professional_organization_options USING gin (text public.gin_trgm_ops);


--
-- Name: professional_organization_options_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX professional_organization_options_unique ON public.professional_organization_options USING btree (locale, text);


--
-- Name: professional_organization_options_versions_id_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX professional_organization_options_versions_id_updated_at ON public.professional_organization_options_versions USING btree (id, updated_at);


--
-- Name: published_content_titles_published_locale; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX published_content_titles_published_locale ON public.published_content_titles USING btree (published_locale);


--
-- Name: skills_options_auto_suggest; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX skills_options_auto_suggest ON public.skills_options USING btree (suggest, locale);


--
-- Name: skills_options_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX skills_options_unique ON public.skills_options USING btree (locale, text);


--
-- Name: skills_options_versions_id_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX skills_options_versions_id_updated_at ON public.skills_options_versions USING btree (id, updated_at);


--
-- Name: skills_texts_on_cp_search_helpers; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX skills_texts_on_cp_search_helpers ON public.career_profile_search_helpers USING gin (skills_texts);


--
-- Name: str_lp_on_inst_stream_lps; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX str_lp_on_inst_stream_lps ON public.institution_stream_locale_packs USING btree (stream_locale_pack_id);


--
-- Name: stream_lp_on_cohort_stream_packs; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX stream_lp_on_cohort_stream_packs ON public.published_cohort_stream_locale_packs USING btree (stream_locale_pack_id);


--
-- Name: stream_lp_on_pub_str_lesson_lp; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX stream_lp_on_pub_str_lesson_lp ON public.published_stream_lesson_locale_packs USING btree (stream_locale_pack_id, lesson_locale_pack_id);


--
-- Name: student_network_interests_options_auto_suggest; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX student_network_interests_options_auto_suggest ON public.student_network_interests_options USING btree (suggest, locale);


--
-- Name: student_network_interests_options_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX student_network_interests_options_unique ON public.student_network_interests_options USING btree (locale, text);


--
-- Name: student_network_interests_options_versions_id_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX student_network_interests_options_versions_id_updated_at ON public.student_network_interests_options_versions USING btree (id, updated_at);


--
-- Name: student_network_interests_texts_on_cp_search_helpers; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX student_network_interests_texts_on_cp_search_helpers ON public.career_profile_search_helpers USING gin (student_network_interests_texts);


--
-- Name: uniq_user_and_lesson_on_ulpr; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX uniq_user_and_lesson_on_ulpr ON public.user_lesson_progress_records USING btree (user_id, locale_pack_id);


--
-- Name: unique_dump_events_to_redshift_job; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_dump_events_to_redshift_job ON public.delayed_jobs USING btree (queue) WHERE ((queue)::text = 'dump_events_to_redshift'::text);


--
-- Name: unique_on_access_groups_cohorts; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_on_access_groups_cohorts ON public.access_groups_cohorts USING btree (access_group_id, cohort_id);


--
-- Name: unique_on_access_groups_curriculum_templates; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_on_access_groups_curriculum_templates ON public.access_groups_curriculum_templates USING btree (access_group_id, curriculum_template_id);


--
-- Name: unique_on_cp_graduation_rate_predictions; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_on_cp_graduation_rate_predictions ON public.cohort_period_graduation_rate_predictions USING btree (cohort_id, index);


--
-- Name: unique_on_lesson_stream_locale_packs_users; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_on_lesson_stream_locale_packs_users ON public.lesson_stream_locale_packs_users USING btree (user_id, locale_pack_id);


--
-- Name: unique_on_player_lesson_sessions; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_on_player_lesson_sessions ON public.player_lesson_sessions USING btree (user_id, lesson_id, lesson_stream_id, session_id);


--
-- Name: unique_on_published_playlist_lessons; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_on_published_playlist_lessons ON public.published_playlist_lessons USING btree (playlist_id, stream_id, lesson_id);


--
-- Name: unique_on_user_chance_of_graduating_records; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_on_user_chance_of_graduating_records ON public.user_chance_of_graduating_records USING btree (user_id, program_type);


--
-- Name: user_and_date_on_act_by_cal_date_rec; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX user_and_date_on_act_by_cal_date_rec ON public.lesson_activity_by_calendar_date_records USING btree (date, user_id);


--
-- Name: user_cohort_time_on_coh_status_changes; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX user_cohort_time_on_coh_status_changes ON public.cohort_status_changes USING btree (user_id, cohort_id, from_time, until_time);


--
-- Name: user_id_and_applied_at_on_coh_applications; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_id_and_applied_at_on_coh_applications ON public.cohort_applications USING btree (user_id, applied_at DESC);


--
-- Name: user_id_on_enrollment_records; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX user_id_on_enrollment_records ON public.program_enrollment_progress_records USING btree (user_id);


--
-- Name: wexp_company_names_on_cp_search_helpers; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX wexp_company_names_on_cp_search_helpers ON public.career_profile_search_helpers USING gin (work_experience_company_names public.gin_trgm_ops);


--
-- Name: wexp_industries_on_cp_search_helpers; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX wexp_industries_on_cp_search_helpers ON public.career_profile_search_helpers USING gin (work_experience_industries);


--
-- Name: wexp_roles_on_cp_search_helpers; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX wexp_roles_on_cp_search_helpers ON public.career_profile_search_helpers USING gin (work_experience_roles);


--
-- Name: years_exp_on_cp_search_helpers; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX years_exp_on_cp_search_helpers ON public.career_profile_search_helpers USING btree (years_of_experience DESC, default_prioritization DESC);


--
-- Name: access_groups_cohorts access_groups_cohorts_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER access_groups_cohorts_versions AFTER INSERT OR DELETE OR UPDATE ON public.access_groups_cohorts FOR EACH ROW EXECUTE PROCEDURE public.process_access_groups_cohorts_version();


--
-- Name: access_groups_curriculum_templates access_groups_curriculum_templates_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER access_groups_curriculum_templates_versions AFTER INSERT OR DELETE OR UPDATE ON public.access_groups_curriculum_templates FOR EACH ROW EXECUTE PROCEDURE public.process_access_groups_curriculum_templates_version();


--
-- Name: access_groups_institutions access_groups_institutions_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER access_groups_institutions_versions AFTER INSERT OR DELETE OR UPDATE ON public.access_groups_institutions FOR EACH ROW EXECUTE PROCEDURE public.process_access_groups_institutions_version();


--
-- Name: access_groups_lesson_stream_locale_packs access_groups_lesson_stream_locale_packs_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER access_groups_lesson_stream_locale_packs_versions AFTER INSERT OR DELETE OR UPDATE ON public.access_groups_lesson_stream_locale_packs FOR EACH ROW EXECUTE PROCEDURE public.process_access_groups_lesson_stream_locale_packs_version();


--
-- Name: access_groups_playlist_locale_packs access_groups_playlist_locale_packs_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER access_groups_playlist_locale_packs_versions AFTER INSERT OR DELETE OR UPDATE ON public.access_groups_playlist_locale_packs FOR EACH ROW EXECUTE PROCEDURE public.process_access_groups_playlist_locale_packs_version();


--
-- Name: access_groups_users access_groups_users_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER access_groups_users_versions AFTER INSERT OR DELETE OR UPDATE ON public.access_groups_users FOR EACH ROW EXECUTE PROCEDURE public.process_access_groups_users_version();


--
-- Name: access_groups access_groups_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER access_groups_versions AFTER INSERT OR DELETE OR UPDATE ON public.access_groups FOR EACH ROW EXECUTE PROCEDURE public.process_access_groups_version();


--
-- Name: awards_and_interests_options awards_and_interests_options_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER awards_and_interests_options_versions AFTER INSERT OR DELETE OR UPDATE ON public.awards_and_interests_options FOR EACH ROW EXECUTE PROCEDURE public.process_awards_and_interests_options_version();


--
-- Name: candidate_position_interests candidate_position_interests_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER candidate_position_interests_versions AFTER INSERT OR DELETE OR UPDATE ON public.candidate_position_interests FOR EACH ROW EXECUTE PROCEDURE public.process_candidate_position_interests_version();


--
-- Name: career_profile_lists career_profile_lists_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER career_profile_lists_versions AFTER INSERT OR DELETE OR UPDATE ON public.career_profile_lists FOR EACH ROW EXECUTE PROCEDURE public.process_career_profile_lists_version();


--
-- Name: career_profiles career_profiles_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER career_profiles_versions AFTER INSERT OR DELETE OR UPDATE ON public.career_profiles FOR EACH ROW EXECUTE PROCEDURE public.process_career_profiles_version();


--
-- Name: cohort_applications cohort_applications_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER cohort_applications_versions AFTER INSERT OR DELETE OR UPDATE ON public.cohort_applications FOR EACH ROW EXECUTE PROCEDURE public.process_cohort_applications_version();


--
-- Name: cohort_promotions cohort_promotions_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER cohort_promotions_versions AFTER INSERT OR DELETE OR UPDATE ON public.cohort_promotions FOR EACH ROW EXECUTE PROCEDURE public.process_cohort_promotions_version();


--
-- Name: cohorts cohorts_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER cohorts_versions AFTER INSERT OR DELETE OR UPDATE ON public.cohorts FOR EACH ROW EXECUTE PROCEDURE public.process_cohorts_version();


--
-- Name: content_publishers content_publishers_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER content_publishers_versions AFTER INSERT OR DELETE OR UPDATE ON public.content_publishers FOR EACH ROW EXECUTE PROCEDURE public.process_content_publishers_version();


--
-- Name: content_topics_lesson_stream_locale_packs content_topics_lesson_stream_locale_packs_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER content_topics_lesson_stream_locale_packs_versions AFTER INSERT OR DELETE OR UPDATE ON public.content_topics_lesson_stream_locale_packs FOR EACH ROW EXECUTE PROCEDURE public.process_content_topics_lesson_stream_locale_packs_version();


--
-- Name: content_topics content_topics_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER content_topics_versions AFTER INSERT OR DELETE OR UPDATE ON public.content_topics FOR EACH ROW EXECUTE PROCEDURE public.process_content_topics_version();


--
-- Name: curriculum_templates curriculum_templates_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER curriculum_templates_versions AFTER INSERT OR DELETE OR UPDATE ON public.curriculum_templates FOR EACH ROW EXECUTE PROCEDURE public.process_curriculum_templates_version();


--
-- Name: education_experiences education_experiences_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER education_experiences_versions AFTER INSERT OR DELETE OR UPDATE ON public.education_experiences FOR EACH ROW EXECUTE PROCEDURE public.process_education_experiences_version();


--
-- Name: educational_organization_options educational_organization_options_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER educational_organization_options_versions AFTER INSERT OR DELETE OR UPDATE ON public.educational_organization_options FOR EACH ROW EXECUTE PROCEDURE public.process_educational_organization_options_version();


--
-- Name: hiring_applications hiring_applications_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER hiring_applications_versions AFTER INSERT OR DELETE OR UPDATE ON public.hiring_applications FOR EACH ROW EXECUTE PROCEDURE public.process_hiring_applications_version();


--
-- Name: hiring_relationships hiring_relationships_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER hiring_relationships_versions AFTER INSERT OR DELETE OR UPDATE ON public.hiring_relationships FOR EACH ROW EXECUTE PROCEDURE public.process_hiring_relationships_version();


--
-- Name: hiring_teams hiring_teams_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER hiring_teams_versions AFTER INSERT OR DELETE OR UPDATE ON public.hiring_teams FOR EACH ROW EXECUTE PROCEDURE public.process_hiring_teams_version();


--
-- Name: institutions_users institutions_users_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER institutions_users_versions AFTER INSERT OR DELETE OR UPDATE ON public.institutions_users FOR EACH ROW EXECUTE PROCEDURE public.process_institutions_users_version();


--
-- Name: institutions institutions_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER institutions_versions AFTER INSERT OR DELETE OR UPDATE ON public.institutions FOR EACH ROW EXECUTE PROCEDURE public.process_institutions_version();


--
-- Name: lesson_locale_packs lesson_locale_packs_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER lesson_locale_packs_versions AFTER INSERT OR DELETE OR UPDATE ON public.lesson_locale_packs FOR EACH ROW EXECUTE PROCEDURE public.process_lesson_locale_packs_version();


--
-- Name: lesson_stream_locale_packs_users lesson_stream_locale_packs_users_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER lesson_stream_locale_packs_users_versions AFTER INSERT OR DELETE OR UPDATE ON public.lesson_stream_locale_packs_users FOR EACH ROW EXECUTE PROCEDURE public.process_lesson_stream_locale_packs_users_version();


--
-- Name: lesson_stream_locale_packs lesson_stream_locale_packs_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER lesson_stream_locale_packs_versions AFTER INSERT OR DELETE OR UPDATE ON public.lesson_stream_locale_packs FOR EACH ROW EXECUTE PROCEDURE public.process_lesson_stream_locale_packs_version();


--
-- Name: lesson_streams lesson_streams_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER lesson_streams_versions AFTER INSERT OR DELETE OR UPDATE ON public.lesson_streams FOR EACH ROW EXECUTE PROCEDURE public.process_lesson_streams_version();


--
-- Name: lesson_to_stream_joins lesson_to_stream_joins_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER lesson_to_stream_joins_versions AFTER INSERT OR DELETE OR UPDATE ON public.lesson_to_stream_joins FOR EACH ROW EXECUTE PROCEDURE public.process_lesson_to_stream_joins_version();


--
-- Name: lessons lessons_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER lessons_versions AFTER INSERT OR DELETE OR UPDATE ON public.lessons FOR EACH ROW EXECUTE PROCEDURE public.process_lessons_version();


--
-- Name: open_positions_skills_options open_positions_skills_options_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER open_positions_skills_options_versions AFTER INSERT OR DELETE OR UPDATE ON public.open_positions_skills_options FOR EACH ROW EXECUTE PROCEDURE public.process_open_positions_skills_options_version();


--
-- Name: open_positions open_positions_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER open_positions_versions AFTER INSERT OR DELETE OR UPDATE ON public.open_positions FOR EACH ROW EXECUTE PROCEDURE public.process_open_positions_version();


--
-- Name: peer_recommendations peer_recommendations_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER peer_recommendations_versions AFTER INSERT OR DELETE OR UPDATE ON public.peer_recommendations FOR EACH ROW EXECUTE PROCEDURE public.process_peer_recommendations_version();


--
-- Name: playlist_locale_packs playlist_locale_packs_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER playlist_locale_packs_versions AFTER INSERT OR DELETE OR UPDATE ON public.playlist_locale_packs FOR EACH ROW EXECUTE PROCEDURE public.process_playlist_locale_packs_version();


--
-- Name: playlists playlists_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER playlists_versions AFTER INSERT OR DELETE OR UPDATE ON public.playlists FOR EACH ROW EXECUTE PROCEDURE public.process_playlists_version();


--
-- Name: professional_organization_options professional_organization_options_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER professional_organization_options_versions AFTER INSERT OR DELETE OR UPDATE ON public.professional_organization_options FOR EACH ROW EXECUTE PROCEDURE public.process_professional_organization_options_version();


--
-- Name: roles roles_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER roles_versions AFTER INSERT OR DELETE OR UPDATE ON public.roles FOR EACH ROW EXECUTE PROCEDURE public.process_roles_version();


--
-- Name: skills_options skills_options_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER skills_options_versions AFTER INSERT OR DELETE OR UPDATE ON public.skills_options FOR EACH ROW EXECUTE PROCEDURE public.process_skills_options_version();


--
-- Name: student_network_interests_options student_network_interests_options_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER student_network_interests_options_versions AFTER INSERT OR DELETE OR UPDATE ON public.student_network_interests_options FOR EACH ROW EXECUTE PROCEDURE public.process_student_network_interests_options_version();


--
-- Name: subscriptions subscriptions_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER subscriptions_versions AFTER INSERT OR DELETE OR UPDATE ON public.subscriptions FOR EACH ROW EXECUTE PROCEDURE public.process_subscriptions_version();


--
-- Name: career_profiles update_career_profile_location; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER update_career_profile_location BEFORE INSERT OR UPDATE ON public.career_profiles FOR EACH ROW EXECUTE PROCEDURE public.update_career_profile_location();


--
-- Name: users_roles users_roles_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER users_roles_versions AFTER INSERT OR DELETE OR UPDATE ON public.users_roles FOR EACH ROW EXECUTE PROCEDURE public.process_users_roles_version();


--
-- Name: users users_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER users_versions AFTER INSERT OR DELETE OR UPDATE ON public.users FOR EACH ROW EXECUTE PROCEDURE public.process_users_version();


--
-- Name: hiring_relationships validate_hiring_teams_on_relationship_change; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER validate_hiring_teams_on_relationship_change AFTER INSERT OR UPDATE ON public.hiring_relationships FOR EACH ROW EXECUTE PROCEDURE public.validate_hiring_teams_on_relationship_change();


--
-- Name: users validate_hiring_teams_on_team_change; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER validate_hiring_teams_on_team_change AFTER INSERT OR UPDATE ON public.users FOR EACH ROW EXECUTE PROCEDURE public.validate_hiring_teams_on_team_change();


--
-- Name: work_experiences work_experiences_versions; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER work_experiences_versions AFTER INSERT OR DELETE OR UPDATE ON public.work_experiences FOR EACH ROW EXECUTE PROCEDURE public.process_work_experiences_version();


--
-- Name: education_experiences education_experiences_career_profiles_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.education_experiences
    ADD CONSTRAINT education_experiences_career_profiles_fk FOREIGN KEY (career_profile_id) REFERENCES public.career_profiles(id);


--
-- Name: education_experiences education_experiences_educational_organization_option_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.education_experiences
    ADD CONSTRAINT education_experiences_educational_organization_option_id_fk FOREIGN KEY (educational_organization_option_id) REFERENCES public.educational_organization_options(id);


--
-- Name: access_groups_curriculum_templates fk_rails_023125c1a0; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_curriculum_templates
    ADD CONSTRAINT fk_rails_023125c1a0 FOREIGN KEY (access_group_id) REFERENCES public.access_groups(id);


--
-- Name: hiring_relationships fk_rails_043ee6dcd0; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hiring_relationships
    ADD CONSTRAINT fk_rails_043ee6dcd0 FOREIGN KEY (candidate_id) REFERENCES public.users(id);


--
-- Name: experiment_variations fk_rails_06c3a97931; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.experiment_variations
    ADD CONSTRAINT fk_rails_06c3a97931 FOREIGN KEY (experiment_id) REFERENCES public.experiments(id);


--
-- Name: subscriptions_users fk_rails_07d1a68c30; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriptions_users
    ADD CONSTRAINT fk_rails_07d1a68c30 FOREIGN KEY (subscription_id) REFERENCES public.subscriptions(id);


--
-- Name: content_publishers fk_rails_10d4b1a059; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_publishers
    ADD CONSTRAINT fk_rails_10d4b1a059 FOREIGN KEY (lesson_stream_version_id) REFERENCES public.lesson_streams_versions(version_id);


--
-- Name: career_profiles fk_rails_1818e5a7b2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profiles
    ADD CONSTRAINT fk_rails_1818e5a7b2 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: open_positions_skills_options fk_rails_19092e6dd3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.open_positions_skills_options
    ADD CONSTRAINT fk_rails_19092e6dd3 FOREIGN KEY (open_position_id) REFERENCES public.open_positions(id);


--
-- Name: candidate_position_interests fk_rails_1a764cf0d7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.candidate_position_interests
    ADD CONSTRAINT fk_rails_1a764cf0d7 FOREIGN KEY (open_position_id) REFERENCES public.open_positions(id);


--
-- Name: open_positions fk_rails_1c0a3cf1d1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.open_positions
    ADD CONSTRAINT fk_rails_1c0a3cf1d1 FOREIGN KEY (hiring_manager_id) REFERENCES public.users(id);


--
-- Name: playlists fk_rails_1d16190cb2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.playlists
    ADD CONSTRAINT fk_rails_1d16190cb2 FOREIGN KEY (locale_pack_id) REFERENCES public.playlist_locale_packs(id);


--
-- Name: access_groups_curriculum_templates fk_rails_22fcb97933; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_curriculum_templates
    ADD CONSTRAINT fk_rails_22fcb97933 FOREIGN KEY (curriculum_template_id) REFERENCES public.curriculum_templates(id);


--
-- Name: lesson_fulltext fk_rails_2b5875f40f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_fulltext
    ADD CONSTRAINT fk_rails_2b5875f40f FOREIGN KEY (lesson_id) REFERENCES public.lessons(id);


--
-- Name: lesson_streams fk_rails_360a7536ec; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_streams
    ADD CONSTRAINT fk_rails_360a7536ec FOREIGN KEY (locale_pack_id) REFERENCES public.lesson_stream_locale_packs(id);


--
-- Name: lesson_streams_progress fk_rails_369b62c16f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_streams_progress
    ADD CONSTRAINT fk_rails_369b62c16f FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: access_groups_users fk_rails_3a696f9d7a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_users
    ADD CONSTRAINT fk_rails_3a696f9d7a FOREIGN KEY (access_group_id) REFERENCES public.access_groups(id);


--
-- Name: lesson_streams fk_rails_3d35167329; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_streams
    ADD CONSTRAINT fk_rails_3d35167329 FOREIGN KEY (entity_metadata_id) REFERENCES public.entity_metadata(id);


--
-- Name: subscriptions_hiring_teams fk_rails_3d9555054f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriptions_hiring_teams
    ADD CONSTRAINT fk_rails_3d9555054f FOREIGN KEY (subscription_id) REFERENCES public.subscriptions(id);


--
-- Name: institutions_users fk_rails_41eb873bc2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.institutions_users
    ADD CONSTRAINT fk_rails_41eb873bc2 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: career_profile_fulltext fk_rails_43426d9b05; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profile_fulltext
    ADD CONSTRAINT fk_rails_43426d9b05 FOREIGN KEY (career_profile_id) REFERENCES public.career_profiles(id);


--
-- Name: users_roles fk_rails_4a41696df6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users_roles
    ADD CONSTRAINT fk_rails_4a41696df6 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: lesson_streams_progress fk_rails_4d4c14b8f7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_streams_progress
    ADD CONSTRAINT fk_rails_4d4c14b8f7 FOREIGN KEY (certificate_image_id) REFERENCES public.s3_assets(id);


--
-- Name: experiment_variations_users fk_rails_502fd2e655; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.experiment_variations_users
    ADD CONSTRAINT fk_rails_502fd2e655 FOREIGN KEY (experiment_variation_id) REFERENCES public.experiment_variations(id);


--
-- Name: access_groups_cohorts fk_rails_51c6800e4b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_cohorts
    ADD CONSTRAINT fk_rails_51c6800e4b FOREIGN KEY (access_group_id) REFERENCES public.access_groups(id);


--
-- Name: lessons fk_rails_51fad6fce6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lessons
    ADD CONSTRAINT fk_rails_51fad6fce6 FOREIGN KEY (entity_metadata_id) REFERENCES public.entity_metadata(id);


--
-- Name: roles fk_rails_5296513585; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT fk_rails_5296513585 FOREIGN KEY (resource_id) REFERENCES public.lessons(id);


--
-- Name: access_groups_institutions fk_rails_56c1ce5700; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_institutions
    ADD CONSTRAINT fk_rails_56c1ce5700 FOREIGN KEY (access_group_id) REFERENCES public.access_groups(id);


--
-- Name: access_groups_lesson_stream_locale_packs fk_rails_5e2cde5448; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_lesson_stream_locale_packs
    ADD CONSTRAINT fk_rails_5e2cde5448 FOREIGN KEY (locale_pack_id) REFERENCES public.lesson_stream_locale_packs(id);


--
-- Name: access_groups_playlist_locale_packs fk_rails_5fd4628401; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_playlist_locale_packs
    ADD CONSTRAINT fk_rails_5fd4628401 FOREIGN KEY (locale_pack_id) REFERENCES public.playlist_locale_packs(id);


--
-- Name: access_groups_users fk_rails_60c3e3bb5f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_users
    ADD CONSTRAINT fk_rails_60c3e3bb5f FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: career_profiles_awards_and_interests_options fk_rails_626c0fe389; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profiles_awards_and_interests_options
    ADD CONSTRAINT fk_rails_626c0fe389 FOREIGN KEY (awards_and_interests_option_id) REFERENCES public.awards_and_interests_options(id);


--
-- Name: lesson_stream_locale_packs_users fk_rails_6666f3486c; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_stream_locale_packs_users
    ADD CONSTRAINT fk_rails_6666f3486c FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: access_groups_cohorts fk_rails_6c99c8b7ca; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_cohorts
    ADD CONSTRAINT fk_rails_6c99c8b7ca FOREIGN KEY (cohort_id) REFERENCES public.cohorts(id);


--
-- Name: candidate_position_interests fk_rails_6d213f9e27; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.candidate_position_interests
    ADD CONSTRAINT fk_rails_6d213f9e27 FOREIGN KEY (candidate_id) REFERENCES public.users(id);


--
-- Name: content_topics_lesson_stream_locale_packs fk_rails_73764924ae; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_topics_lesson_stream_locale_packs
    ADD CONSTRAINT fk_rails_73764924ae FOREIGN KEY (locale_pack_id) REFERENCES public.lesson_stream_locale_packs(id);


--
-- Name: lesson_to_stream_joins fk_rails_7a72508afb; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_to_stream_joins
    ADD CONSTRAINT fk_rails_7a72508afb FOREIGN KEY (lesson_id) REFERENCES public.lessons(id);


--
-- Name: access_groups_institutions fk_rails_7e8e950f0d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_institutions
    ADD CONSTRAINT fk_rails_7e8e950f0d FOREIGN KEY (institution_id) REFERENCES public.institutions(id);


--
-- Name: institutions_users fk_rails_838127df1b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.institutions_users
    ADD CONSTRAINT fk_rails_838127df1b FOREIGN KEY (institution_id) REFERENCES public.institutions(id);


--
-- Name: subscriptions_users fk_rails_89c5d24b8a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriptions_users
    ADD CONSTRAINT fk_rails_89c5d24b8a FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: career_profiles_skills_options fk_rails_8b94866679; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profiles_skills_options
    ADD CONSTRAINT fk_rails_8b94866679 FOREIGN KEY (skills_option_id) REFERENCES public.skills_options(id);


--
-- Name: hiring_applications fk_rails_8febee3291; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hiring_applications
    ADD CONSTRAINT fk_rails_8febee3291 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: users fk_rails_93f7158f2b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_rails_93f7158f2b FOREIGN KEY (hiring_team_id) REFERENCES public.hiring_teams(id);


--
-- Name: access_groups_playlist_locale_packs fk_rails_98229816bf; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_playlist_locale_packs
    ADD CONSTRAINT fk_rails_98229816bf FOREIGN KEY (access_group_id) REFERENCES public.access_groups(id);


--
-- Name: content_topics_lesson_stream_locale_packs fk_rails_9a9fa92d04; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_topics_lesson_stream_locale_packs
    ADD CONSTRAINT fk_rails_9a9fa92d04 FOREIGN KEY (content_topic_id) REFERENCES public.content_topics(id);


--
-- Name: lessons fk_rails_9b8f9ce145; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lessons
    ADD CONSTRAINT fk_rails_9b8f9ce145 FOREIGN KEY (author_id) REFERENCES public.users(id);


--
-- Name: open_positions_skills_options fk_rails_a13c4861d2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.open_positions_skills_options
    ADD CONSTRAINT fk_rails_a13c4861d2 FOREIGN KEY (skills_option_id) REFERENCES public.skills_options(id);


--
-- Name: career_profiles_awards_and_interests_options fk_rails_a1afc34b31; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profiles_awards_and_interests_options
    ADD CONSTRAINT fk_rails_a1afc34b31 FOREIGN KEY (career_profile_id) REFERENCES public.career_profiles(id);


--
-- Name: career_profiles_student_network_interests_options fk_rails_a65a4e9515; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profiles_student_network_interests_options
    ADD CONSTRAINT fk_rails_a65a4e9515 FOREIGN KEY (career_profile_id) REFERENCES public.career_profiles(id);


--
-- Name: career_profiles fk_rails_a70ce5103f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profiles
    ADD CONSTRAINT fk_rails_a70ce5103f FOREIGN KEY (linkedin_pdf_id) REFERENCES public.s3_assets(id);


--
-- Name: institutions_reports_viewers fk_rails_af9691a90e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.institutions_reports_viewers
    ADD CONSTRAINT fk_rails_af9691a90e FOREIGN KEY (institution_id) REFERENCES public.institutions(id);


--
-- Name: hiring_relationships fk_rails_b13d8b8116; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hiring_relationships
    ADD CONSTRAINT fk_rails_b13d8b8116 FOREIGN KEY (open_position_id) REFERENCES public.open_positions(id);


--
-- Name: hiring_teams fk_rails_b1b0e58459; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hiring_teams
    ADD CONSTRAINT fk_rails_b1b0e58459 FOREIGN KEY (owner_id) REFERENCES public.users(id);


--
-- Name: content_publishers fk_rails_b312c3cb3e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_publishers
    ADD CONSTRAINT fk_rails_b312c3cb3e FOREIGN KEY (cohort_version_id) REFERENCES public.cohorts_versions(version_id);


--
-- Name: career_profiles_skills_options fk_rails_bb8e114fd8; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profiles_skills_options
    ADD CONSTRAINT fk_rails_bb8e114fd8 FOREIGN KEY (career_profile_id) REFERENCES public.career_profiles(id);


--
-- Name: content_publishers fk_rails_c43de391d2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_publishers
    ADD CONSTRAINT fk_rails_c43de391d2 FOREIGN KEY (playlist_version_id) REFERENCES public.playlists_versions(version_id);


--
-- Name: access_groups_lesson_stream_locale_packs fk_rails_cc50f6155e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.access_groups_lesson_stream_locale_packs
    ADD CONSTRAINT fk_rails_cc50f6155e FOREIGN KEY (access_group_id) REFERENCES public.access_groups(id);


--
-- Name: hiring_relationships fk_rails_d09dd7374c; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hiring_relationships
    ADD CONSTRAINT fk_rails_d09dd7374c FOREIGN KEY (conversation_id) REFERENCES public.mailboxer_conversations(id);


--
-- Name: lesson_progress fk_rails_d135fefb97; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_progress
    ADD CONSTRAINT fk_rails_d135fefb97 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: peer_recommendations fk_rails_d4f3ebad4b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.peer_recommendations
    ADD CONSTRAINT fk_rails_d4f3ebad4b FOREIGN KEY (career_profile_id) REFERENCES public.career_profiles(id);


--
-- Name: career_profiles fk_rails_d5db044404; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profiles
    ADD CONSTRAINT fk_rails_d5db044404 FOREIGN KEY (resume_id) REFERENCES public.s3_assets(id);


--
-- Name: institutions_reports_viewers fk_rails_d9dc6bced3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.institutions_reports_viewers
    ADD CONSTRAINT fk_rails_d9dc6bced3 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: lessons fk_rails_d9fc3a4980; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lessons
    ADD CONSTRAINT fk_rails_d9fc3a4980 FOREIGN KEY (locale_pack_id) REFERENCES public.lesson_locale_packs(id);


--
-- Name: hiring_relationships fk_rails_dab1496889; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hiring_relationships
    ADD CONSTRAINT fk_rails_dab1496889 FOREIGN KEY (hiring_manager_id) REFERENCES public.users(id);


--
-- Name: content_publishers fk_rails_dd4ced1afd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_publishers
    ADD CONSTRAINT fk_rails_dd4ced1afd FOREIGN KEY (lesson_version_id) REFERENCES public.lessons_versions(version_id);


--
-- Name: users fk_rails_dee503ce6e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_rails_dee503ce6e FOREIGN KEY (active_playlist_locale_pack_id) REFERENCES public.playlist_locale_packs(id);


--
-- Name: career_profiles_student_network_interests_options fk_rails_dfb43fb436; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_profiles_student_network_interests_options
    ADD CONSTRAINT fk_rails_dfb43fb436 FOREIGN KEY (student_network_interests_option_id) REFERENCES public.student_network_interests_options(id);


--
-- Name: lesson_to_stream_joins fk_rails_e45ae1535e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_to_stream_joins
    ADD CONSTRAINT fk_rails_e45ae1535e FOREIGN KEY (stream_id) REFERENCES public.lesson_streams(id);


--
-- Name: lesson_stream_locale_packs_users fk_rails_e687f44d42; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_stream_locale_packs_users
    ADD CONSTRAINT fk_rails_e687f44d42 FOREIGN KEY (locale_pack_id) REFERENCES public.lesson_stream_locale_packs(id);


--
-- Name: cohort_applications fk_rails_eb7907dce7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cohort_applications
    ADD CONSTRAINT fk_rails_eb7907dce7 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: users_roles fk_rails_eb7b4658f8; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users_roles
    ADD CONSTRAINT fk_rails_eb7b4658f8 FOREIGN KEY (role_id) REFERENCES public.roles(id);


--
-- Name: cohort_applications fk_rails_ee6f31054b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cohort_applications
    ADD CONSTRAINT fk_rails_ee6f31054b FOREIGN KEY (cohort_id) REFERENCES public.cohorts(id);


--
-- Name: subscriptions_hiring_teams fk_rails_f09a084549; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriptions_hiring_teams
    ADD CONSTRAINT fk_rails_f09a084549 FOREIGN KEY (hiring_team_id) REFERENCES public.hiring_teams(id);


--
-- Name: lesson_streams fk_rails_f1f950c984; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_streams
    ADD CONSTRAINT fk_rails_f1f950c984 FOREIGN KEY (image_id) REFERENCES public.s3_assets(id);


--
-- Name: lesson_streams lesson_streams_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lesson_streams
    ADD CONSTRAINT lesson_streams_id_fkey FOREIGN KEY (id, updated_at) REFERENCES public.lesson_streams_versions(id, updated_at) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: lessons lessons_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lessons
    ADD CONSTRAINT lessons_id_fkey FOREIGN KEY (id, updated_at) REFERENCES public.lessons_versions(id, updated_at) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: mailboxer_conversation_opt_outs mb_opt_outs_on_conversations_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailboxer_conversation_opt_outs
    ADD CONSTRAINT mb_opt_outs_on_conversations_id FOREIGN KEY (conversation_id) REFERENCES public.mailboxer_conversations(id);


--
-- Name: mailboxer_notifications notifications_on_conversation_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailboxer_notifications
    ADD CONSTRAINT notifications_on_conversation_id FOREIGN KEY (conversation_id) REFERENCES public.mailboxer_conversations(id);


--
-- Name: mailboxer_receipts receipts_on_notification_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailboxer_receipts
    ADD CONSTRAINT receipts_on_notification_id FOREIGN KEY (notification_id) REFERENCES public.mailboxer_notifications(id);


--
-- Name: users users_professional_organization_option_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_professional_organization_option_id_fk FOREIGN KEY (professional_organization_option_id) REFERENCES public.professional_organization_options(id);


--
-- Name: work_experiences work_experiences_career_profiles_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.work_experiences
    ADD CONSTRAINT work_experiences_career_profiles_fk FOREIGN KEY (career_profile_id) REFERENCES public.career_profiles(id);


--
-- Name: work_experiences work_experiences_professional_organization_option_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.work_experiences
    ADD CONSTRAINT work_experiences_professional_organization_option_id_fk FOREIGN KEY (professional_organization_option_id) REFERENCES public.professional_organization_options(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20120418171104'),
('20120418171110'),
('20120418171112'),
('20130930195549'),
('20131007185018'),
('20131007202439'),
('20131007205041'),
('20131230160443'),
('20131231135702'),
('20140102191204'),
('20140103141732'),
('20140103182229'),
('20140107171514'),
('20140113180032'),
('20140114153928'),
('20140210180858'),
('20140307153934'),
('20140307153943'),
('20140318132400'),
('20140319202010'),
('20140403144929'),
('20140407175115'),
('20140409114447'),
('20140416133452'),
('20140416133924'),
('20140416142625'),
('20140416154528'),
('20140416154943'),
('20140417134736'),
('20140418144102'),
('20140422214525'),
('20140424143133'),
('20140428192817'),
('20140508031730'),
('20140508032340'),
('20140508214512'),
('20140513200319'),
('20140529200658'),
('20140529203616'),
('20140603164032'),
('20140603215028'),
('20140605235718'),
('20140610195147'),
('20140612164045'),
('20140617095126'),
('20140617213648'),
('20140619162156'),
('20140623143302'),
('20140623154409'),
('20140623195146'),
('20140703140825'),
('20140709174636'),
('20140710153343'),
('20140710173918'),
('20140715155537'),
('20140716141525'),
('20140716145616'),
('20140716203143'),
('20140717122050'),
('20140723123057'),
('20140724155447'),
('20140724191912'),
('20140728201537'),
('20140729161704'),
('20140731151613'),
('20140813171742'),
('20140814182533'),
('20140821211542'),
('20140822234423'),
('20140905122911'),
('20140912132330'),
('20140918141801'),
('20140925120230'),
('20141006145514'),
('20141027161547'),
('20141027185713'),
('20141111174125'),
('20141125145425'),
('20141203181646'),
('20141204155434'),
('20141208142911'),
('20141210191126'),
('20141211181852'),
('20141211211312'),
('20141217030832'),
('20141217195525'),
('20141219142943'),
('20141219172234'),
('20150105165629'),
('20150105184119'),
('20150106203422'),
('20150107185459'),
('20150112142254'),
('20150113161819'),
('20150115201402'),
('20150120141528'),
('20150120142010'),
('20150120232626'),
('20150121191240'),
('20150122210515'),
('20150127163456'),
('20150129203419'),
('20150209211327'),
('20150210025010'),
('20150211191041'),
('20150212025953'),
('20150218225938'),
('20150220173158'),
('20150220174530'),
('20150220200109'),
('20150224155417'),
('20150225154129'),
('20150302202007'),
('20150303162404'),
('20150303215749'),
('20150309150056'),
('20150309163512'),
('20150309164538'),
('20150309171245'),
('20150310153348'),
('20150310211039'),
('20150311190035'),
('20150311191249'),
('20150313192428'),
('20150316204541'),
('20150318133054'),
('20150320184411'),
('20150323182218'),
('20150324163748'),
('20150324165151'),
('20150327084818'),
('20150407202417'),
('20150408190201'),
('20150409195117'),
('20150417141257'),
('20150421181045'),
('20150422153751'),
('20150428122125'),
('20150429190554'),
('20150429204722'),
('20150503141727'),
('20150503142225'),
('20150504135232'),
('20150505090215'),
('20150508074624'),
('20150508132432'),
('20150519192532'),
('20150525200748'),
('20150527133644'),
('20150530190653'),
('20150602145909'),
('20150602174253'),
('20150604084346'),
('20150608084359'),
('20150608125654'),
('20150611141955'),
('20150615203244'),
('20150618160338'),
('20150630104003'),
('20150709183255'),
('20150710184957'),
('20150717180433'),
('20150720161512'),
('20150805160219'),
('20150805171027'),
('20150805204627'),
('20150806033112'),
('20150811184612'),
('20150812025805'),
('20150813152015'),
('20150827143639'),
('20150902153520'),
('20150902161854'),
('20150911103144'),
('20150929200719'),
('20150930192733'),
('20151007141602'),
('20151009174622'),
('20151014175029'),
('20151015172333'),
('20151020184345'),
('20151022152955'),
('20151102141007'),
('20151106182451'),
('20151109213153'),
('20151110032349'),
('20151209181450'),
('20151221160938'),
('20151221173424'),
('20151221230739'),
('20151222162238'),
('20151222211018'),
('20151222234631'),
('20151230205655'),
('20160104210312'),
('20160118150735'),
('20160118203028'),
('20160121232652'),
('20160208205004'),
('20160209212430'),
('20160210210706'),
('20160211184955'),
('20160211203029'),
('20160215173144'),
('20160217180957'),
('20160217192308'),
('20160218184555'),
('20160218192554'),
('20160222141646'),
('20160223035745'),
('20160223143547'),
('20160229191457'),
('20160304155843'),
('20160308213140'),
('20160311162148'),
('20160321160830'),
('20160322210307'),
('20160322214655'),
('20160329201454'),
('20160414160231'),
('20160415153122'),
('20160415155649'),
('20160419133633'),
('20160421195410'),
('20160422220007'),
('20160429173503'),
('20160502161128'),
('20160509184950'),
('20160517174910'),
('20160518155613'),
('20160524200513'),
('20160615190746'),
('20160621223331'),
('20160623151957'),
('20160624200951'),
('20160628212227'),
('20160629090052'),
('20160629130241'),
('20160630212626'),
('20160705165513'),
('20160706204835'),
('20160707191031'),
('20160708201016'),
('20160713044557'),
('20160713143619'),
('20160714152010'),
('20160714152051'),
('20160725175114'),
('20160726153811'),
('20160802092732'),
('20160803095716'),
('20160803195710'),
('20160804061128'),
('20160804092740'),
('20160804092741'),
('20160804092742'),
('20160804092743'),
('20160804094545'),
('20160809063258'),
('20160822200321'),
('20160831192952'),
('20160831195523'),
('20160831204108'),
('20160831211335'),
('20160901165159'),
('20160906195248'),
('20160912182753'),
('20160914152800'),
('20160919164857'),
('20160920154905'),
('20160921152327'),
('20160927161502'),
('20160928153909'),
('20160929153231'),
('20160929180151'),
('20161007162858'),
('20161012180438'),
('20161012182305'),
('20161012183741'),
('20161013203408'),
('20161017211236'),
('20161018155226'),
('20161018182443'),
('20161019155930'),
('20161021152120'),
('20161021174516'),
('20161021183217'),
('20161025142839'),
('20161026150207'),
('20161026192330'),
('20161101174312'),
('20161103142042'),
('20161108212326'),
('20161110023743'),
('20161110171958'),
('20161116201437'),
('20161122183326'),
('20161130211101'),
('20161130215514'),
('20161205211607'),
('20161206193803'),
('20161214151209'),
('20161214182507'),
('20170106214542'),
('20170106222320'),
('20170109152254'),
('20170110205135'),
('20170117171713'),
('20170118215040'),
('20170120020951'),
('20170126171643'),
('20170126193511'),
('20170203165124'),
('20170204200620'),
('20170206185113'),
('20170207195021'),
('20170207202940'),
('20170209151536'),
('20170213195120'),
('20170213220315'),
('20170216181847'),
('20170217163920'),
('20170218205448'),
('20170220153921'),
('20170220200244'),
('20170221140311'),
('20170221140312'),
('20170221140313'),
('20170223032757'),
('20170227200154'),
('20170228160118'),
('20170301175644'),
('20170301211232'),
('20170302155419'),
('20170302180457'),
('20170302202710'),
('20170303185136'),
('20170303223104'),
('20170306001915'),
('20170306001955'),
('20170306215939'),
('20170307183842'),
('20170307201319'),
('20170308223624'),
('20170309183025'),
('20170310204902'),
('20170313193458'),
('20170314144608'),
('20170317180549'),
('20170321170823'),
('20170321170916'),
('20170323141852'),
('20170323201802'),
('20170324205906'),
('20170327184151'),
('20170327195041'),
('20170329132726'),
('20170330133007'),
('20170331200942'),
('20170403171258'),
('20170404153806'),
('20170405152005'),
('20170405194031'),
('20170406155422'),
('20170410224834'),
('20170411154109'),
('20170411201741'),
('20170412171516'),
('20170412202904'),
('20170413155108'),
('20170414174516'),
('20170417184344'),
('20170419221736'),
('20170420204849'),
('20170421201751'),
('20170424161351'),
('20170424164640'),
('20170425135156'),
('20170425144201'),
('20170425151339'),
('20170425161452'),
('20170425185252'),
('20170425205644'),
('20170426142452'),
('20170427022710'),
('20170427135354'),
('20170427153620'),
('20170427182300'),
('20170509214453'),
('20170515203652'),
('20170516194653'),
('20170522233203'),
('20170523163929'),
('20170523210105'),
('20170525165348'),
('20170526143117'),
('20170530141902'),
('20170530182037'),
('20170531164523'),
('20170601135311'),
('20170601204044'),
('20170606142312'),
('20170608162605'),
('20170608193242'),
('20170608194624'),
('20170609141947'),
('20170612155937'),
('20170614011215'),
('20170614150547'),
('20170615160854'),
('20170616111955'),
('20170616132159'),
('20170621173718'),
('20170626073944'),
('20170629074157'),
('20170703170913'),
('20170712223238'),
('20170720133418'),
('20170720161042'),
('20170720224114'),
('20170721063545'),
('20170724055738'),
('20170725153358'),
('20170725182333'),
('20170726074016'),
('20170727183320'),
('20170728155738'),
('20170731221735'),
('20170802165134'),
('20170802193225'),
('20170804205746'),
('20170807020928'),
('20170807080915'),
('20170807200654'),
('20170810100840'),
('20170811061933'),
('20170814074448'),
('20170823104316'),
('20170823104740'),
('20170828175744'),
('20170829135100'),
('20170829140024'),
('20170829160405'),
('20170829163504'),
('20170829191835'),
('20170830195719'),
('20170831141533'),
('20170908153230'),
('20170911180116'),
('20170914203617'),
('20170915171428'),
('20170915173327'),
('20170918143723'),
('20170918153357'),
('20170918194659'),
('20170925215947'),
('20170929173337'),
('20171002154553'),
('20171002202853'),
('20171005200622'),
('20171006180220'),
('20171011143225'),
('20171011175022'),
('20171011203324'),
('20171013152956'),
('20171013203613'),
('20171013223600'),
('20171017044717'),
('20171017143253'),
('20171017201832'),
('20171018181822'),
('20171020195626'),
('20171023133043'),
('20171103203350'),
('20171103205034'),
('20171106210406'),
('20171110162520'),
('20171110182223'),
('20171110205958'),
('20171120202136'),
('20171128143717'),
('20171129154850'),
('20171129161118'),
('20171129193122'),
('20171130141931'),
('20171211021121'),
('20171211163113'),
('20171212022833'),
('20171213160349'),
('20171213164214'),
('20171215234003'),
('20171218204123'),
('20171219202906'),
('20171219232244'),
('20171220163237'),
('20171220201641'),
('20171221210151'),
('20171226194002'),
('20171229213725'),
('20180102230921'),
('20180103160554'),
('20180103182938'),
('20180103213808'),
('20180105204938'),
('20180108212714'),
('20180110224521'),
('20180111150157'),
('20180111202937'),
('20180111210553'),
('20180117163345'),
('20180117194442'),
('20180118232124'),
('20180119220846'),
('20180122213803'),
('20180123193419'),
('20180123194223'),
('20180124214525'),
('20180129200724'),
('20180130030914'),
('20180201190425'),
('20180201212148'),
('20180202021428'),
('20180202161440'),
('20180202193824'),
('20180205152616'),
('20180206214234'),
('20180206222608'),
('20180207200102'),
('20180212174020'),
('20180212234337'),
('20180212234900'),
('20180213172523'),
('20180213201105'),
('20180214163216'),
('20180214201214'),
('20180215154448'),
('20180220203724'),
('20180223223941'),
('20180225174507'),
('20180301002205'),
('20180301210911'),
('20180301210935'),
('20180301211924'),
('20180305185554'),
('20180312164332'),
('20180313012445'),
('20180313150205'),
('20180313155253'),
('20180314035238'),
('20180314042331'),
('20180315220831'),
('20180319195508'),
('20180320204354'),
('20180320204421'),
('20180326172555'),
('20180326174228'),
('20180402202737'),
('20180404233803'),
('20180405144247'),
('20180405213056'),
('20180406223421'),
('20180409142231'),
('20180412143854'),
('20180412145600'),
('20180412150535'),
('20180412205425'),
('20180416162821'),
('20180416175249'),
('20180416175703'),
('20180418143340'),
('20180418161139'),
('20180419200709'),
('20180423140732'),
('20180427152104'),
('20180427153327'),
('20180502153620'),
('20180502154900'),
('20180503133641'),
('20180504161347'),
('20180504182054'),
('20180508150848'),
('20180508150911'),
('20180509154715'),
('20180509204708'),
('20180510193344'),
('20180514165738'),
('20180515203344'),
('20180516152804'),
('20180516153100'),
('20180517202636'),
('20180525120707'),
('20180606104832'),
('20180607141057'),
('20180614141211'),
('20180621214726'),
('20180625141802'),
('20180625160035'),
('20180625163956'),
('20180626023039');


