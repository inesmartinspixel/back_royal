class AddImageAuthorToStreams < ActiveRecord::Migration[4.2]
    def change
        add_column :lesson_streams, :author_id, :integer
        add_column :lesson_streams, :image_id, :integer
    end
end