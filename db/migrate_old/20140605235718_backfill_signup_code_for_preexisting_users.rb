class BackfillSignupCodeForPreexistingUsers < ActiveRecord::Migration[4.2]
    def change
        puts '-- Updating preexisting users to have a default signup code of PEDAGOERS --'
        update_preexisting_user_signup_code
    end


    def update_preexisting_user_signup_code

        batch = 0
        User.find_in_batches(batch_size: 10) do |users|
            puts " ************ Processing user batch ##{batch}"

            users.each_with_index do |user, i|

                puts " ************ Processing user: \"#{user.email}\" ID: #{user.id} : #{i+1} of #{users.size}"

                if user.sign_up_code.blank?
                    user.sign_up_code = "PEDAGOER"
                    user.save!
                end

            end

            batch += 1
        end

    end
end
