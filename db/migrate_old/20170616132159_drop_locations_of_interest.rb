require File.expand_path('../20170523163929_create_locations_of_interest', __FILE__)

class DropLocationsOfInterest < ActiveRecord::Migration[5.1]
    def up
        CreateLocationsOfInterest.new.down
    end

    def down
        CreateLocationsOfInterest.new.up
    end
end
