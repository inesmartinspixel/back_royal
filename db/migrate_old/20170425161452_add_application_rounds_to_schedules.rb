class AddApplicationRoundsToSchedules < ActiveRecord::Migration[5.0]
    def change

        add_column :cohorts, :admission_rounds, :json, :array => true, :default => []
        add_column :cohorts_versions, :admission_rounds, :json, :array => true
        add_column :curriculum_templates, :admission_rounds, :json, :array => true, :default => []
        add_column :curriculum_templates_versions, :admission_rounds, :json, :array => true

    end
end
