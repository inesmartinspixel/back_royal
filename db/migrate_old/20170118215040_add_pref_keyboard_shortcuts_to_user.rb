class AddPrefKeyboardShortcutsToUser < ActiveRecord::Migration[5.0]
    def change
        add_column :users, :pref_keyboard_shortcuts, :boolean, default: true, :null => false
        add_column :users_versions, :pref_keyboard_shortcuts, :boolean
    end
end
