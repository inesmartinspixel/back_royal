class MoveSignUpCodesIntoTagGroups < ActiveRecord::Migration[4.2]
    def change
        puts '-- Migrating sign_up codes to tags --'
        migrate_sign_up_codes_to_group_tags
        add_insead_courses_to_insead_group
        create_superviewer_group
        add_existing_courses_to_superviewer
        add_admins_plus_to_superviewer
    end


    def migrate_sign_up_codes_to_group_tags

        batch = 0
        User.find_in_batches(batch_size: 10) do |users|
            puts " ************ Processing user batch ##{batch}"

            users.each_with_index do |user, i|

                puts " ************ Processing user: \"#{user.email}\" ID: #{user.id} : #{i+1} of #{users.size}"
                group_tag = user.sign_up_code

                # Rename PEDAGOER group to WORKAROUND
                if group_tag == "PEDAGOER" || group_tag == "PEDAGOERS"
                    group_tag = "WORKAROUND"
                end

                # if there's no group tag for this code already, create one
                # Tag.find_or_create_by(:entity_type => "Group", :text => group_tag)

                # if this user isnt already in this group, add them
                # Tag.find_or_create_by(:entity_type => "User", :text => group_tag, :entity_id => user.id.to_s)


            end

            batch += 1
        end

    end

    def add_insead_courses_to_insead_group
        insead_course_titles = ["Accounting (Pre-MBA)", "Statistics (Pre-MBA)"]
        insead_course_titles.each do |course_title|
            course = Lesson::Stream.where(title: course_title).first
            if !course.nil?
                # Tag.find_or_create_by(:entity_type => "Lesson::Stream", :text => "INSEAD2014", :entity_id => course.id.to_s)
            end
        end
    end

    def create_superviewer_group
        # Tag.find_or_create_by(:entity_type => "Group", :text => "SUPERVIEWER")
    end

    def add_existing_courses_to_superviewer
        batch = 0
        Lesson::Stream.find_in_batches(batch_size: 10) do |streams|
            puts " ************ Processing course batch ##{batch}"

            streams.each_with_index do |stream, i|

                puts " ************ Processing course: \"#{stream.title}\" ID: #{stream.id} : #{i+1} of #{streams.size}"

                # if this course isnt already in the superviewer group, add them
                # Tag.find_or_create_by(:entity_type => "Lesson::Stream", :text => "SUPERVIEWER", :entity_id => stream.id.to_s)


            end
        end
    end

    # add all users with role admin to the SUPERVIEWER group, plus daniel, caroline, james
    def add_admins_plus_to_superviewer
        batch = 0
        User.with_role(:admin).find_in_batches(batch_size: 10) do |users|
            puts " ************ Processing user batch ##{batch}"

            users.each_with_index do |user, i|

                puts " ************ Processing user: \"#{user.email}\" ID: #{user.id} : #{i+1} of #{users.size}"

                # if this course isnt already in the superviewer group, add them
                # Tag.find_or_create_by(:entity_type => "User", :text => "SUPERVIEWER", :entity_id => user.id.to_s)


            end
        end
        ["mintz.daniel@gmail.com", "jamespkelly@gmail.com", "dagati.caroline@gmail.com"].each do |email|
            user = User.find_by(:email => email)
            if !user.nil?
                puts " ************ Processing special case user: \"#{user.email}\" ID: #{user.id}"
                # Tag.find_or_create_by(:entity_type => "User", :text => "SUPERVIEWER", :entity_id => user.id.to_s)
            end
        end
    end
end
