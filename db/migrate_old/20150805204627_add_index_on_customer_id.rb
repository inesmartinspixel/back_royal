class AddIndexOnCustomerId < ActiveRecord::Migration[4.2]
    def change

        add_index :users, :stripe_customer_id, :unique => true
    end
end
