class AddNewEmbaQuestionsFields < ActiveRecord::Migration[5.0]
    def change
        add_column :career_profiles, :short_answer_leadership_challenge, :text
        add_column :career_profiles_versions, :short_answer_leadership_challenge, :text

        add_column :career_profiles, :short_answer_personal_passions, :text
        add_column :career_profiles_versions, :short_answer_personal_passions, :text
    end
end
