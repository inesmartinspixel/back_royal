class UpdateCareerProfileLocation < ActiveRecord::Migration[5.1]
    def up
        # Every time a career profile is saved, we populate the
        # locations column, creating a point of type geography out
        # of the latitude and longitude we find in the place_details
        trigger_sql = "
            CREATE OR REPLACE FUNCTION update_career_profile_location() RETURNS TRIGGER AS $update_career_profile_location$
                BEGIN
                    IF (TG_OP = 'UPDATE' OR TG_OP = 'INSERT') THEN
                        NEW.location = CASE WHEN
                                (NEW.place_details->>'lng')::text IS NOT NULL AND (NEW.place_details->>'lat')::text IS NOT NULL
                            THEN
                                ST_GeographyFromText('POINT(' || (NEW.place_details->>'lng')::text || ' ' || (NEW.place_details->>'lat')::text || ')')
                            ELSE
                                NULL
                            END;
                    END IF;
                    RETURN NEW;
                END;
            $update_career_profile_location$ LANGUAGE plpgsql;

            CREATE TRIGGER update_career_profile_location
            BEFORE INSERT OR UPDATE ON career_profiles
            FOR EACH ROW EXECUTE PROCEDURE update_career_profile_location();
        "
        execute(trigger_sql)
    end

    def down
        execute "DROP TRIGGER update_career_profile_location ON career_profiles"
    end
end
