class AddMetadataToMessages < ActiveRecord::Migration[5.1]
    def change

        add_column :mailboxer_notifications, :metadata, :json

    end
end
