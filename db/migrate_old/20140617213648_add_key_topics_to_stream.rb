class AddKeyTopicsToStream < ActiveRecord::Migration[4.2]
    def change
        add_column :lesson_streams, :key_topics, :json
    end
end
