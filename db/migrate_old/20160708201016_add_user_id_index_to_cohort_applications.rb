class AddUserIdIndexToCohortApplications < ActiveRecord::Migration[4.2]
    def change
        # Only allow one accepted/pending application per user
        add_index :cohort_applications, :user_id, where: "status in ('accepted', 'pending')", unique: true
    end
end
