class ConvertUserLessonProgressRecordsToTable < ActiveRecord::Migration[5.1]
    def up

        create_table :user_lesson_progress_records, id: false do |t|
            t.uuid :user_id, null: false
            t.uuid :locale_pack_id, null: false
            t.timestamp :started_at, null: false
            t.timestamp :completed_at
            t.float :best_score_from_lesson_progress
            t.float :total_lesson_time
            t.timestamp :last_lesson_activity_time, null: false
            t.float :total_lesson_time_on_desktop
            t.float :total_lesson_time_on_mobile_app
            t.float :total_lesson_time_on_mobile_web
            t.float :total_lesson_time_on_unknown
            t.text :completed_on_client_type
            t.timestamp :last_lesson_reset_at
            t.integer :lesson_reset_count
            t.boolean :test
            t.boolean :assessment
            t.integer :lesson_finish_count
            t.float :average_assessment_score_first
            t.float :average_assessment_score_best
            t.float :official_test_score
        end

        add_index :user_lesson_progress_records, [:user_id, :locale_pack_id], :unique => true, :name => :uniq_user_and_lesson_on_ulpr
        add_index :user_lesson_progress_records, [:last_lesson_activity_time]

        ViewHelpers.migrate(self, 20170918153357)

    end

    def down

        ViewHelpers.rollback(self, 20170918153357)

        drop_table :user_lesson_progress_records
    end
end
