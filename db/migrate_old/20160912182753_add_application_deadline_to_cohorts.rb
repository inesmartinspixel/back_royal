class AddApplicationDeadlineToCohorts < ActiveRecord::Migration[4.2]
    def up
        add_column :cohorts, :application_deadline, :datetime
        add_column :cohorts_versions, :application_deadline, :datetime
        execute "UPDATE cohorts SET application_deadline = (start_date - INTERVAL '1 month')"

        # 9/23 UTC
        execute "UPDATE cohorts SET application_deadline = date '2016-09-23' WHERE id='1789b5f0-0436-4e65-b521-3c19f63a71db'"

        # if instead, we want to go with EST ...
        # execute "UPDATE cohorts SET application_deadline = TIMESTAMP WITH TIME ZONE '2016-09-23 00:00:00-04' WHERE id='1789b5f0-0436-4e65-b521-3c19f63a71db'"

        change_column :cohorts, :application_deadline, :datetime, null: false

        remove_index :cohort_applications, name: :cohort_applications_cohorts_users
        add_index :cohort_applications, [:cohort_id, :user_id], unique: true, name: :cohort_applications_cohorts_users

    end

    def down
        remove_index :cohort_applications, name: :cohort_applications_cohorts_users
        add_index :cohort_applications, [:cohort_id, :user_id], name: :cohort_applications_cohorts_users

        remove_column :cohorts, :application_deadline
        remove_column :cohorts_versions, :application_deadline
    end
end
