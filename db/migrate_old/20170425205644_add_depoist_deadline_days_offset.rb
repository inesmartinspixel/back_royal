class AddDepoistDeadlineDaysOffset < ActiveRecord::Migration[5.0]
    def change

        add_column :cohorts, :deposit_deadline_days_offset, :integer, :default => 0
        add_column :cohorts_versions, :deposit_deadline_days_offset, :integer
        add_column :curriculum_templates, :deposit_deadline_days_offset, :integer, :default => 0
        add_column :curriculum_templates_versions, :deposit_deadline_days_offset, :integer

    end
end
