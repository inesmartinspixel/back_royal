require File.expand_path('../componentized_migrators/image_dimensions_migrator', __FILE__)

class AddDimensionsToExistingImageButtons < ActiveRecord::Migration[4.2]
    def up

        # insead_streams = Lesson::Stream.where("title LIKE  ? ", "%MBA%")
        # insead_lessons = insead_streams.map(&:lessons).flatten.uniq

        migrator = ImageDimensionsMigrator.new
        migrator.update_old_versions = false
        # migrator.change(insead_lessons)
        migrator.change

    end
end
