class CleanUpCollectionsAndConcepts < ActiveRecord::Migration[4.2]
    def change
        # only collection_progress is specific to a user, not the collection itself
        remove_column :collections, :user_id

        # dont need this now that we have a proper join table
        remove_column :concepts, :collection_guid

        # instead of icon_path we'll have a fk to s3assets instead
        remove_column :concepts, :icon_path
        add_reference :concepts, :image, references: :s3asset

    end
end
