class IndexEventsOnCreatedAt < ActiveRecord::Migration[5.0]
    disable_ddl_transaction!

    def up

        # this is too slow to run during an eb rollout.  In that case, we run the following
        # commands manually in psql:
        #     CREATE INDEX CONCURRENTLY "index_events_on_event_type_and_created_at" ON "events"  ("event_type", "created_at")
        return if Rails.env.production?
        add_index(:events, [ :event_type, :created_at ], algorithm: :concurrently)

    end

    def down

        return if Rails.env.production?
        remove_index(:events, [ :event_type, :created_at ])

    end
end
