class AddReportViewersJoin < ActiveRecord::Migration[4.2]
    def change

        create_table :institutions_reports_viewers, id: :uuid do |t|

            t.uuid :user_id, :null => false
            t.uuid :institution_id, :null => false

        end

        add_foreign_key "institutions_reports_viewers", "users", column: "user_id", primary_key: "id"
        add_foreign_key "institutions_reports_viewers", "institutions", column: "institution_id", primary_key: "id"

    end
end
