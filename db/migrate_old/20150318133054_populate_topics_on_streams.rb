class PopulateTopicsOnStreams < ActiveRecord::Migration[4.2]
    def up
        {
            '1deeff68-ed22-4399-a967-e6689c0aed3f' => ['Data Analysis', 'Marketing & Sales'],
            '50eeda2d-1b44-4a1d-b425-dead8982526f' => ['Accounting'],
            '87b85177-fdf6-4cf8-a29a-c44b0ef77530' => ['Strategy'],
            '35cf54b6-4522-44c3-b762-531bf25436f0' => ['Marketing & Sales'],
            '222d17ff-718d-4bbc-b4df-444e71e7aec9' => ['Finance'],
            '964fe746-9fb7-46c8-883c-4b653579733d' => ['Finance'],
            'f6296448-533f-456f-bdc3-1b0c83cf039f' => ['Economics'],
            'abd47e21-4b59-4ccf-910d-fb1c18b2e9df' => ['Marketing & Sales'],
            '95437d2a-0211-4899-825d-e2107c6729e6' => ['Economics'],
            '5b46c407-6061-45a9-aa3e-5dcf7a6f76d6' => ['Design'],
            '1d93f964-1086-4096-a9a7-26ac283c626a' => ['Marketing & Sales'],
            '8d720fb5-0320-4bf4-8c40-50808267a1be' => ['Data Analysis'],
            '48b273ee-d9ee-4c12-8482-3cfd37e34542' => ['Data Analysis'],
            'd79316c4-a419-41ff-ae42-be14fe70af44' => ['Data Analysis'],
            '5c4ea530-21ae-46ec-bfcd-ccfbf6f9c49a' => ['Data Analysis'],
            '9b680532-54e0-4ade-a048-1f1881e667ea' => ['Strategy'],
            '2d98a1c6-7a19-4fb9-a6a1-fb662ba9ce82' => ['Design'],
            'c36a0ccd-fc05-45fe-aead-9935703756b8' => ['Math'],
            'f52d38cb-f14c-404c-a91d-d63632108c28' => ['Math'],
            'a5a53cd3-9cbd-491c-9092-685a84e9604d' => ['Hot Topics'],
            'a15151fd-9f71-460b-9e13-a9bab3afd0e6' => ['Training'],
            'bf436c07-1c99-454b-b845-1c35c43f9035' => ['Training'],
            '64a3ede2-bf62-43fe-b68a-a4e671481143' => ['Demo']
        }.each do |stream_id, topic_names|

            begin
                stream = Lesson::Stream.find(stream_id)
                stream.topics = topic_names.map do |topic_name|
                    ContentTopic.find_or_create_by!(name: topic_name)
                end
                stream.save!
            rescue ActiveRecord::RecordNotFound
                puts "No record found for #{stream_id}"
            end

        end

    end

    def down
        Lesson::Stream.all do |stream|
            stream.topics = []
            stream.save!
        end
    end
end
