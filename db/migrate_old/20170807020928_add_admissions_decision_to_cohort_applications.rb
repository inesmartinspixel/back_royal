class AddAdmissionsDecisionToCohortApplications < ActiveRecord::Migration[5.1]
    def change
        add_column :cohort_applications, :admissions_decision, :text
        add_column :cohort_applications_versions, :admissions_decision, :text
    end
end
