class AddRegisteredToApplications < ActiveRecord::Migration[5.1]
    def change
        add_column :cohort_applications, :registered, :boolean
        add_column :cohort_applications_versions, :registered, :boolean
    end
end
