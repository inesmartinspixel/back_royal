class CreateInstitutions < ActiveRecord::Migration[4.2]
    def change
        create_table :institutions do |t|
            t.timestamps
            t.string :name
            t.string :sign_up_code
        end

        add_index :institutions, :sign_up_code, :unique => true

        create_table :institutions_groups do |t|
            t.timestamps
            t.references :institution
            t.references :tag
        end

    end
end
