class AddColumnsForAssigningInitialCandidates < ActiveRecord::Migration[5.0]
    def change

        add_column :hiring_applications, :viewed_career_profile_list_ids, :uuid, :array => true, :null => false, :default => '{}'
        add_column :hiring_applications_versions, :viewed_career_profile_list_ids, :uuid, :array => true

        add_column :hiring_applications, :targeted_career_profile_ids, :uuid, :array => true, :null => false, :default => '{}'
        add_column :hiring_applications_versions, :targeted_career_profile_ids, :uuid, :array => true

        add_column :hiring_relationships, :hiring_manager_priority, :integer, :null => false, :default => 100
        add_column :hiring_relationships_versions, :hiring_manager_priority, :integer
    end
end
