class RemoveForeignKeyConstraintFromMultipleChoiceChallengeProgress < ActiveRecord::Migration[4.2]
    def up
        remove_foreign_key :multiple_choice_challenge_progress, column: :user_id
    end
    def down
        add_foreign_key :multiple_choice_challenge_progress, :users
    end
end
