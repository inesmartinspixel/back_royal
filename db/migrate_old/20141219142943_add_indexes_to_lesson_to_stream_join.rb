class AddIndexesToLessonToStreamJoin < ActiveRecord::Migration[4.2]
    def change

        add_index(:lesson_to_stream_joins, :lesson_id)
        add_index(:lesson_to_stream_joins, :stream_id)

    end
end
