class CreateS3TranscriptAssets < ActiveRecord::Migration[4.2]
    def change
        create_table :s3_transcript_assets, id: :uuid do |t|
            t.timestamps
            t.attachment :file
            t.string :file_fingerprint
            t.uuid :user_id
        end

        add_index :s3_transcript_assets, :user_id
    end
end
