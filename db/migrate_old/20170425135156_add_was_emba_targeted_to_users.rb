class AddWasEmbaTargetedToUsers < ActiveRecord::Migration[5.0]
    def up
        add_column :users, :was_emba_targeted, :boolean, :default => false, :null => false
        add_column :users_versions, :was_emba_targeted, :boolean

        create_trigger('users', [
            'email',
            'encrypted_password',
            'reset_password_token',
            'reset_password_sent_at',
            'name',
            'nickname',
            'sign_up_code',
            'reset_password_redirect_url',
            'provider',
            'uid',
            'subscriptions_enabled',
            'notify_email_daily',
            'notify_email_content',
            'notify_email_features',
            'notify_email_reminders',
            'notify_email_newsletter',
            'free_trial_started',
            'confirmed_profile_info',
            'optimizely_referer',
            'active_playlist_locale_pack_id',
            'has_seen_welcome',
            'pref_decimal_delim',
            'pref_locale',
            'school',
            'avatar_url',
            'avatar_provider',
            'mba_content_lockable',
            'job_title',
            'phone',
            'phone_extension',
            'country',
            'has_seen_accepted',
            'can_edit_career_profile',
            'professional_organization_option_id',
            'pref_show_photos_names',
            'identity_verified',
            'sex',
            'ethnicity',
            'race',
            'how_did_you_hear_about_us',
            'address_line_1',
            'address_line_2',
            'city',
            'state',
            'zip',
            'birthdate',
            'anything_else_to_tell_us',
            'pref_keyboard_shortcuts',
            'mba_enabled',
            'fallback_program_type',
            'program_type_confirmed',
            'invite_code',
            'was_emba_targeted'
            # the json columns must be left out of here, since they can't be compared
        ])
    end

    def down
        remove_column :users, :was_emba_targeted, :boolean
        remove_column :users_versions, :was_emba_targeted, :boolean

        create_trigger('users', [
            'email',
            'encrypted_password',
            'reset_password_token',
            'reset_password_sent_at',
            'name',
            'nickname',
            'sign_up_code',
            'reset_password_redirect_url',
            'provider',
            'uid',
            'subscriptions_enabled',
            'notify_email_daily',
            'notify_email_content',
            'notify_email_features',
            'notify_email_reminders',
            'notify_email_newsletter',
            'free_trial_started',
            'confirmed_profile_info',
            'optimizely_referer',
            'active_playlist_locale_pack_id',
            'has_seen_welcome',
            'pref_decimal_delim',
            'pref_locale',
            'school',
            'avatar_url',
            'avatar_provider',
            'mba_content_lockable',
            'job_title',
            'phone',
            'phone_extension',
            'country',
            'has_seen_accepted',
            'can_edit_career_profile',
            'professional_organization_option_id',
            'pref_show_photos_names',
            'identity_verified',
            'sex',
            'ethnicity',
            'race',
            'how_did_you_hear_about_us',
            'address_line_1',
            'address_line_2',
            'city',
            'state',
            'zip',
            'birthdate',
            'anything_else_to_tell_us',
            'pref_keyboard_shortcuts',
            'mba_enabled',
            'program_type',
            'program_type_confirmed',
            'invite_code'
            # the json columns must be left out of here, since they can't be compared
        ])
    end

    def create_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end
end
