class ModifyExternalUsersViewImplementation < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20170918143723)
    end

    def down
        ViewHelpers.rollback(self, 20170918143723)
    end
end
