class AddConvenienceIndexesForUsersController < ActiveRecord::Migration[5.1]
    def change
        commit_db_transaction
        add_index :career_profiles, [:last_calculated_complete_percentage, :interested_in_joining_new_company], name: "index_career_profiles_on_percent_complete_and_interest_level", algorithm: :concurrently
        add_index :career_profiles, [:do_not_create_relationships], algorithm: :concurrently
        add_index :work_experiences, [:career_profile_id, :end_date], algorithm: :concurrently
        add_index :work_experiences, [:professional_organization_option_id], algorithm: :concurrently
    end
end
