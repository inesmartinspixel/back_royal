require File.expand_path('../componentized_migrators/this_or_that_tile_prompts_migrator', __FILE__)

class ThisOrThatTilePromptRedesign < ActiveRecord::Migration[4.2]

    def up
        migrator = ThisOrThatTilePromptsMigrator.new
        migrator.change
    end

end