class AddCohortApplicationCompletedAtGraduatedAt < ActiveRecord::Migration[5.1]
    def change
        add_column :cohort_applications, :completed_at, :datetime
        add_column :cohort_applications_versions, :completed_at, :datetime

        add_column :cohort_applications, :graduated_at, :datetime
        add_column :cohort_applications_versions, :graduated_at, :datetime

        unless reverting?
            # took 2.2s locally
            execute "UPDATE cohort_applications SET graduated_at = (SELECT version_created_at FROM cohort_applications_versions WHERE graduation_status='graduated' AND user_id=cohort_applications.user_id ORDER BY version_created_at ASC LIMIT 1) WHERE graduation_status='graduated';"
        end

    end
end
