class AddTestToLessons < ActiveRecord::Migration[4.2]
    def up

        add_column :lessons, :test, :boolean, :null => false, :default => false
        add_column :lessons_versions, :test, :boolean

        execute "UPDATE lessons_versions SET test = false"

    end

    def down
        remove_column :lessons, :test, :boolean
        remove_column :lessons_versions, :test
    end
end
