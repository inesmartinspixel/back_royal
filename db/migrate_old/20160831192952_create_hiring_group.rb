class CreateHiringGroup < ActiveRecord::Migration[4.2]
    def up
        unless AccessGroup.find_by_name('HIRING')
            AccessGroup.create!(name: 'HIRING')
        end
    end

    def down
        group = AccessGroup.find_by_name('HIRING')
        group.destroy if group
    end
end
