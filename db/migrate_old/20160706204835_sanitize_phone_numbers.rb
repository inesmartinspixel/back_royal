class SanitizePhoneNumbers < ActiveRecord::Migration[4.2]

    def up
        execute "SELECT regexp_replace(phone, '[^0-9]+', '', 'g') FROM users WHERE phone IS NOT NULL"
    end

    def down
        # no-op
    end

end
