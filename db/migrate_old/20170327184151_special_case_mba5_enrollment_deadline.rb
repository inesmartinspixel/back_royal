class SpecialCaseMba5EnrollmentDeadline < ActiveRecord::Migration[5.0]
    def up
        # changes to published_cohort_content_details
        ViewHelpers.migrate(self, 20170327184151)
    end

    def down
        ViewHelpers.rollback(self, 20170327184151)
    end
end
