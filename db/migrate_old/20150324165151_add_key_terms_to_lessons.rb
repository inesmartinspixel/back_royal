class AddKeyTermsToLessons < ActiveRecord::Migration[4.2]
    def up
        add_column :lessons,            :key_terms, :string, array: true, default: [], null: false
        add_column :lessons_versions,   :key_terms, :string, array: true
    end

    def down
        remove_column :lessons, :key_terms
        remove_column :lessons_versions, :key_terms
    end
end
