class AddEnrollmentExpulsionAutomationFields < ActiveRecord::Migration[5.0]
    def up

        # Set the default warning date to be the day before the two week period ends
        add_column :cohorts, :enrollment_warning_days_offset, :integer
        add_column :cohorts_versions, :enrollment_warning_days_offset, :integer
        add_column :curriculum_templates, :enrollment_warning_days_offset, :integer
        add_column :curriculum_templates_versions, :enrollment_warning_days_offset, :integer

        # Set the default expulsion date to be the day before the three week period ends
        add_column :cohorts, :enrollment_expulsion_days_offset, :integer
        add_column :cohorts_versions, :enrollment_expulsion_days_offset, :integer
        add_column :curriculum_templates, :enrollment_expulsion_days_offset, :integer
        add_column :curriculum_templates_versions, :enrollment_expulsion_days_offset, :integer

        add_column :cohort_applications, :skip_auto_enrollment_expulsion, :boolean, :default => false
        add_column :cohort_applications_versions, :skip_auto_enrollment_expulsion, :boolean

        add_column :cohort_applications, :expelled_at, :datetime
        add_column :cohort_applications_versions, :expelled_at, :datetime

        # Backfill expulsion_days_offset based on what has manually occurred up until this point.
        # Also backfill cohort_application's new expelled_at date based on the expulsion_days_offset.
        # See Backfill section at https://trello.com/c/QrKQHd4f
        if !Rails.env.test?
            cohort = Cohort.find_by_name('MBA1')
            cohort.update(enrollment_expulsion_days_offset: 35)
            cohort.publish!
            CohortApplication.where(cohort_id: cohort.id, status: 'expelled').update_all(expelled_at: cohort.enrollment_expulsion_date)

            cohort = Cohort.find_by_name('MBA2')
            cohort.update(enrollment_expulsion_days_offset: 35)
            cohort.publish!
            CohortApplication.where(cohort_id: cohort.id, status: 'expelled').update_all(expelled_at: cohort.enrollment_expulsion_date)

            cohort = Cohort.find_by_name('MBA3')
            cohort.update(enrollment_expulsion_days_offset: 21)
            cohort.publish!
            CohortApplication.where(cohort_id: cohort.id, status: 'expelled').update_all(expelled_at: cohort.enrollment_expulsion_date)

            cohort = Cohort.find_by_name('MBA4')
            cohort.update(enrollment_expulsion_days_offset: 21)
            cohort.publish!
            CohortApplication.where(cohort_id: cohort.id, status: 'expelled').update_all(expelled_at: cohort.enrollment_expulsion_date)

            cohort = Cohort.find_by_name('MBA5')
            cohort.update(enrollment_expulsion_days_offset: 22)
            cohort.publish!
            CohortApplication.where(cohort_id: cohort.id, status: 'expelled').update_all(expelled_at: cohort.enrollment_expulsion_date)

            cohort = Cohort.find_by_name('MBA6')
            cohort.update({
                enrollment_warning_days_offset: 13,
                enrollment_expulsion_days_offset: 20
            })
            cohort.publish!
            CohortApplication.where(cohort_id: cohort.id, status: 'expelled').update_all(expelled_at: cohort.enrollment_expulsion_date)

            cohort = Cohort.find_by_name('MBA7')
             cohort.update({
                enrollment_warning_days_offset: 13,
                enrollment_expulsion_days_offset: 20
            })
            cohort.publish!
            CohortApplication.where(cohort_id: cohort.id, status: 'expelled').update_all(expelled_at: cohort.enrollment_expulsion_date)
        end

        # DO NOT DO THIS IN THE FUTURE. Setting a default value in a transaction is already bad on large tables due to
        # locking every row, but when done with other migrations that are related to the table a deadlock occurs.
        add_column :users, :should_receive_enrollment_warning, :boolean, :default => false
        add_column :users_versions, :should_receive_enrollment_warning, :boolean

        create_trigger('users', [
            'email',
            'encrypted_password',
            'reset_password_token',
            'reset_password_sent_at',
            'name',
            'nickname',
            'sign_up_code',
            'reset_password_redirect_url',
            'provider',
            'uid',
            'subscriptions_enabled',
            'notify_email_daily',
            'notify_email_content',
            'notify_email_features',
            'notify_email_reminders',
            'notify_email_newsletter',
            'free_trial_started',
            'confirmed_profile_info',
            'optimizely_referer',
            'active_playlist_locale_pack_id',
            'has_seen_welcome',
            'pref_decimal_delim',
            'pref_locale',
            'school',
            'avatar_url',
            'avatar_provider',
            'mba_content_lockable',
            'job_title',
            'phone',
            'phone_extension',
            'country',
            'has_seen_accepted',
            'can_edit_career_profile',
            'professional_organization_option_id',
            'pref_show_photos_names',
            'identity_verified',
            'sex',
            'ethnicity',
            'race',
            'how_did_you_hear_about_us',
            'address_line_1',
            'address_line_2',
            'city',
            'state',
            'zip',
            'birthdate',
            'anything_else_to_tell_us',
            'pref_keyboard_shortcuts',
            'mba_enabled',
            'fallback_program_type',
            'program_type_confirmed',
            'invite_code',
            'should_receive_enrollment_warning'
            # the json columns must be left out of here, since they can't be compared
        ])

        ViewHelpers.migrate(self, 20170515203652)
    end

    def down
        ViewHelpers.rollback(self, 20170515203652)

        remove_column :cohorts, :enrollment_warning_days_offset, :string
        remove_column :cohorts_versions, :enrollment_warning_days_offset, :string
        remove_column :curriculum_templates, :enrollment_warning_days_offset, :string
        remove_column :curriculum_templates_versions, :enrollment_warning_days_offset, :string

        remove_column :cohorts, :enrollment_expulsion_days_offset, :string
        remove_column :cohorts_versions, :enrollment_expulsion_days_offset, :string
        remove_column :curriculum_templates, :enrollment_expulsion_days_offset, :string
        remove_column :curriculum_templates_versions, :enrollment_expulsion_days_offset, :string

        remove_column :cohort_applications, :skip_auto_enrollment_expulsion, :boolean
        remove_column :cohort_applications_versions, :skip_auto_enrollment_expulsion, :boolean

        remove_column :cohort_applications, :expelled_at, :datetime
        remove_column :cohort_applications_versions, :expelled_at, :datetime

        remove_column :users, :should_receive_enrollment_warning, :boolean
        remove_column :users_versions, :should_receive_enrollment_warning, :boolean

        create_trigger('users', [
            'email',
            'encrypted_password',
            'reset_password_token',
            'reset_password_sent_at',
            'name',
            'nickname',
            'sign_up_code',
            'reset_password_redirect_url',
            'provider',
            'uid',
            'subscriptions_enabled',
            'notify_email_daily',
            'notify_email_content',
            'notify_email_features',
            'notify_email_reminders',
            'notify_email_newsletter',
            'free_trial_started',
            'confirmed_profile_info',
            'optimizely_referer',
            'active_playlist_locale_pack_id',
            'has_seen_welcome',
            'pref_decimal_delim',
            'pref_locale',
            'school',
            'avatar_url',
            'avatar_provider',
            'mba_content_lockable',
            'job_title',
            'phone',
            'phone_extension',
            'country',
            'has_seen_accepted',
            'can_edit_career_profile',
            'professional_organization_option_id',
            'pref_show_photos_names',
            'identity_verified',
            'sex',
            'ethnicity',
            'race',
            'how_did_you_hear_about_us',
            'address_line_1',
            'address_line_2',
            'city',
            'state',
            'zip',
            'birthdate',
            'anything_else_to_tell_us',
            'pref_keyboard_shortcuts',
            'mba_enabled',
            'fallback_program_type',
            'program_type_confirmed',
            'invite_code'
            # the json columns must be left out of here, since they can't be compared
        ])
    end

    def create_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end
end
