class LateEnrollmentCohortStatusChangeFix < ActiveRecord::Migration[5.0]
    def up
        ViewHelpers.migrate(self, 20170531164523)
    end

    def down
        ViewHelpers.rollback(self, 20170531164523)
    end
end
