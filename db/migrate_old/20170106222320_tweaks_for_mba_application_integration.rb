class TweaksForMbaApplicationIntegration < ActiveRecord::Migration[5.0]
    def change
        remove_column :cohort_applications, :phone_number, :text
        remove_column :cohort_applications, :score_on_gmat, :text
        remove_column :cohort_applications, :score_on_sat, :text
        remove_column :cohort_applications, :score_on_act, :text
        remove_column :cohort_applications, :short_answers, :json
        remove_column :cohort_applications, :place_id, :text
        remove_column :cohort_applications, :place_details, :json

        remove_column :cohort_applications_versions, :phone_number, :text
        remove_column :cohort_applications_versions, :score_on_gmat, :text
        remove_column :cohort_applications_versions, :score_on_sat, :text
        remove_column :cohort_applications_versions, :score_on_act, :text
        remove_column :cohort_applications_versions, :short_answers, :json
        remove_column :cohort_applications_versions, :place_id, :text
        remove_column :cohort_applications_versions, :place_details, :json

        add_column :education_experiences, :gpa_description, :text
        add_column :education_experiences_versions, :gpa_description, :text

        add_column :career_profiles, :industry, :text
        add_column :career_profiles_versions, :industry, :text

        add_column :career_profiles, :sat_max_score, :text
        add_column :career_profiles_versions, :sat_max_score, :text

        add_column :career_profiles, :score_on_gre_verbal, :text
        add_column :career_profiles_versions, :score_on_gre_verbal, :text

        add_column :career_profiles, :score_on_gre_quantitative, :text
        add_column :career_profiles_versions, :score_on_gre_quantitative, :text

        add_column :career_profiles, :score_on_gre_analytical, :text
        add_column :career_profiles_versions, :score_on_gre_analytical, :text

        add_column :career_profiles, :short_answer_greatest_achievement, :text
        add_column :career_profiles_versions, :short_answer_greatest_achievement, :text

        add_column :career_profiles, :short_answer_greatest_strength, :text
        add_column :career_profiles_versions, :short_answer_greatest_strength, :text

        add_column :career_profiles, :short_answer_professional_future, :text
        add_column :career_profiles_versions, :short_answer_professional_future, :text

        add_column :career_profiles, :short_answer_ideal_first_job, :text
        add_column :career_profiles_versions, :short_answer_ideal_first_job, :text

        add_column :career_profiles, :primary_reason_for_applying, :text
        add_column :career_profiles_versions, :primary_reason_for_applying, :text
    end
end
