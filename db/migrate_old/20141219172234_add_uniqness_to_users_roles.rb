class AddUniqnessToUsersRoles < ActiveRecord::Migration[4.2]
    def up
        remove_duplicate_user_roles
        remove_index :users_roles, [:user_id, :role_id]
        add_index :users_roles, [:user_id, :role_id], :unique => true
    end

    def down
        add_index :users_roles, [:user_id, :role_id]
    end

    def remove_duplicate_user_roles
        batch = 0
        User.find_in_batches(batch_size: 10) do |users|
            puts " ************ Processing user batch ##{batch}"

            users.each_with_index do |user, i|
                puts " ************ Processing user #{user.email}"

                if user.roles.count > user.roles.uniq.count
                    # get all the users_roles join table rows
                    connection = ActiveRecord::Base.connection
                    results = connection.execute("select user_id, role_id from users_roles where user_id=#{user.id}")
                    user_id_role_id_hash = {}

                    # build a hash of user_id + role_id, looking for duplicates
                    results.each do |result|
                        if user_id_role_id_hash["#{result['user_id']}-#{result['role_id']}"].blank?
                            user_id_role_id_hash["#{result['user_id']}-#{result['role_id']}"] = result
                        else
                            # delete all the duplicates (cant delete all but one b/c users_roles doesnt have an id col)
                            connection.execute("delete from users_roles where user_id=#{result['user_id']} and role_id=#{result['role_id']}")

                            # restore the single original role
                            user.reload
                            user.add_role(Role.find(result['role_id'].to_i).name)
                        end
                    end
                end
            end

            batch += 1
        end
    end

end