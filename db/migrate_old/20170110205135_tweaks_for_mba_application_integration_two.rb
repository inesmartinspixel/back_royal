require File.expand_path('../20161116201437_views_for_mba_score_query', __FILE__)

class TweaksForMbaApplicationIntegrationTwo < ActiveRecord::Migration[5.0]
    def up
        change_column :career_profiles, :job_search_stage, :text, :default => "ready_to_start"
        change_column :career_profiles, :ideal_job_start_time, :text, :default => "soon_as_possible"

        temporarily_delete_user_views do
            add_column :users, :sex, :text
            add_column :users_versions, :sex, :text

            add_column :users, :ethnicity, :text
            add_column :users_versions, :ethnicity, :text

            add_column :users, :race, :text, :array => true, :default => [], :null => false
            add_column :users_versions, :race, :text, :array => true

            add_column :users, :how_did_you_hear_about_us, :text
            add_column :users_versions, :how_did_you_hear_about_us, :text

            add_column :users, :anything_else_to_tell_us, :text
            add_column :users_versions, :anything_else_to_tell_us, :text

            add_column :users, :birthdate, :date
            add_column :users_versions, :birthdate, :date

            add_column :users, :address_line_1, :text
            add_column :users, :address_line_2, :text
            add_column :users, :city, :text
            add_column :users, :state, :text
            add_column :users, :zip, :text
            add_column :users_versions, :address_line_1, :text
            add_column :users_versions, :address_line_2, :text
            add_column :users_versions, :city, :text
            add_column :users_versions, :state, :text
            add_column :users_versions, :zip, :text

            create_trigger('users', [
                'email',
                'encrypted_password',
                'reset_password_token',
                'reset_password_sent_at',
                'name',
                'nickname',
                'sign_up_code',
                'reset_password_redirect_url',
                'provider',
                'uid',
                'subscriptions_enabled',
                'notify_email_daily',
                'notify_email_content',
                'notify_email_features',
                'notify_email_reminders',
                'notify_email_newsletter',
                'free_trial_started',
                'confirmed_profile_info',
                'optimizely_referer',
                'active_playlist_locale_pack_id',
                'has_seen_welcome',
                'pref_decimal_delim',
                'pref_locale',
                'school',
                'avatar_url',
                'avatar_provider',
                'mba_content_lockable',
                'company',
                'job_title',
                'phone',
                'phone_extension',
                'country',
                'has_seen_accepted',
                'can_edit_career_profile',
                'professional_organization_option_id',
                'pref_show_photos_names',
                'identity_verified',
                'sex',
                'ethnicity',
                'race',
                'how_did_you_hear_about_us',
                'address_line_1',
                'address_line_2',
                'city',
                'state',
                'zip',
                'birthdate',
                'anything_else_to_tell_us'
                # the json columns must be left out of here, since they can't be compared
            ])
        end
        User.reset_column_information
    end

    def down
        change_column :career_profiles, :job_search_stage, :text
        change_column :career_profiles, :ideal_job_start_time, :text

        temporarily_delete_user_views do
            remove_column :users, :sex, :text
            remove_column :users_versions, :sex, :text

            remove_column :users, :ethnicity, :boolean
            remove_column :users_versions, :ethnicity, :boolean

            remove_column :users, :race, :text, :array => true, :default => [], :null => false
            remove_column :users_versions, :race, :text, :array => true

            remove_column :users, :how_did_you_hear_about_us, :text
            remove_column :users_versions, :how_did_you_hear_about_us, :text

            remove_column :users, :address_line_1, :text
            remove_column :users, :address_line_2, :text
            remove_column :users, :city, :text
            remove_column :users, :state, :text
            remove_column :users, :zip, :text
            remove_column :users_versions, :address_line_1, :text
            remove_column :users_versions, :address_line_2, :text
            remove_column :users_versions, :city, :text
            remove_column :users_versions, :state, :text
            remove_column :users_versions, :zip, :text

            create_trigger('users', [
                'email',
                'encrypted_password',
                'reset_password_token',
                'reset_password_sent_at',
                'name',
                'nickname',
                'sign_up_code',
                'reset_password_redirect_url',
                'provider',
                'uid',
                'subscriptions_enabled',
                'notify_email_daily',
                'notify_email_content',
                'notify_email_features',
                'notify_email_reminders',
                'notify_email_newsletter',
                'free_trial_started',
                'confirmed_profile_info',
                'optimizely_referer',
                'active_playlist_locale_pack_id',
                'has_seen_welcome',
                'pref_decimal_delim',
                'pref_locale',
                'school',
                'avatar_url',
                'avatar_provider',
                'mba_content_lockable',
                'company',
                'job_title',
                'phone',
                'phone_extension',
                'country',
                'has_seen_accepted',
                'can_edit_career_profile',
                'professional_organization_option_id',
                'pref_show_photos_names',
                'identity_verified'
                # the json columns must be left out of here, since they can't be compared
            ])
        end
    end

    def create_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end

    def temporarily_delete_user_views(&block)
        ViewsForMbaScoreQuery.new.down

        # this used to also rebuild external_users, but no more
        yield

        ViewsForMbaScoreQuery.new.up
    end
end
