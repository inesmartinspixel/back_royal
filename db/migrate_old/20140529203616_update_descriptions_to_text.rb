class UpdateDescriptionsToText < ActiveRecord::Migration[4.2]

    def up
        change_column :lessons, :description, :text
        change_column :lesson_streams, :description, :text
    end
    def down
        # This might cause trouble if you have strings longer
        # than 255 characters.
        change_column :lessons, :description, :string
        change_column :lesson_streams, :description, :string
    end

end
