class RemoveOldProgramGuideField < ActiveRecord::Migration[5.1]
    def change

        # Cohorts + Templates - remove old program_guide
        remove_column :cohorts, :program_guide, :text
        remove_column :cohorts_versions, :program_guide, :text
        remove_column :curriculum_templates, :program_guide, :text
        remove_column :curriculum_templates_versions, :program_guide, :text

    end
end
