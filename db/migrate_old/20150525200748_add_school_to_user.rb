class AddSchoolToUser < ActiveRecord::Migration[4.2]
    def change
        add_column :users, :school, :text
        add_column :users_versions, :school, :text
    end
end
