class FixCohortStatusStuff < ActiveRecord::Migration[5.1]

    def up
        ViewHelpers.migrate(self, 20170720133418)
    end

    def down
        ViewHelpers.rollback(self, 20170720133418)
    end
end
