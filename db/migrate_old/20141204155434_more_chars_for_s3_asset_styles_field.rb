class MoreCharsForS3AssetStylesField < ActiveRecord::Migration[4.2]
    def up
        change_column :s3_assets, :styles, :text
    end
    def down
        # This might cause trouble if you have strings longer
        # than 255 characters.
        change_column :s3_assets, :styles, :string
    end
end
