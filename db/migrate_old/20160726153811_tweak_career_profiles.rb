class TweakCareerProfiles < ActiveRecord::Migration[4.2]
    def up

        drop_trigger(:career_profiles)

        # Third-Party / Ad-Block Paranoid URL field normalization
        rename_column :career_profiles, :linked_in_url, :li_profile_url
        rename_column :career_profiles_versions, :linked_in_url, :li_profile_url

        rename_column :career_profiles, :facebook_profile_url, :fb_profile_url
        rename_column :career_profiles_versions, :facebook_profile_url, :fb_profile_url

        rename_column :career_profiles, :twitter_profile_url, :tw_profile_url
        rename_column :career_profiles_versions, :twitter_profile_url, :tw_profile_url

        # Normalization of choose-multiple values
        remove_column :career_profiles, :job_sectors_of_interest
        remove_column :career_profiles_versions, :job_sectors_of_interest
        add_column :career_profiles, :job_sectors_of_interest, :text, :default => [], :array => true, :null => false
        add_column :career_profiles_versions, :job_sectors_of_interest, :text, :default => [], :array => true, :null => false

        remove_column :career_profiles, :employment_types_of_interest
        remove_column :career_profiles_versions, :employment_types_of_interest
        add_column :career_profiles, :employment_types_of_interest, :text, :default => [], :array => true, :null => false
        add_column :career_profiles_versions, :employment_types_of_interest, :text, :default => [], :array => true, :null => false

        remove_column :career_profiles, :company_sizes_of_interest
        remove_column :career_profiles_versions, :company_sizes_of_interest
        add_column :career_profiles, :company_sizes_of_interest, :text, :default => [], :array => true, :null => false
        add_column :career_profiles_versions, :company_sizes_of_interest, :text, :default => [], :array => true, :null => false

        # primary_area_of_interest to primary_areas_of_interest (multiple)
        remove_column :career_profiles, :primary_area_of_interest
        remove_column :career_profiles_versions, :primary_area_of_interest
        add_column :career_profiles, :primary_areas_of_interest, :text, :array => true, :default => [], :null => false
        add_column :career_profiles_versions, :primary_areas_of_interest, :text, :array => true, :default => [], :null => false

        # remove favorite_mba_subject
        remove_column :career_profiles, :favorite_mba_subject
        remove_column :career_profiles_versions, :favorite_mba_subject

        # remove old place fields
        remove_column :career_profiles, :street_address
        remove_column :career_profiles_versions, :street_address
        remove_column :career_profiles, :city_state
        remove_column :career_profiles_versions, :city_state
        remove_column :career_profiles, :country_identifier
        remove_column :career_profiles_versions, :country_identifier
        remove_column :career_profiles, :time_zone_identiifier
        remove_column :career_profiles_versions, :time_zone_identiifier

        # add new place fields
        add_column :career_profiles, :place_id, :text
        add_column :career_profiles_versions, :place_id, :text
        add_column :career_profiles, :place_details, :json, :default => {}, :null => false
        add_column :career_profiles_versions, :place_details, :json, :default => {}, :null => false

        # update to numeric type
        remove_column :career_profiles, :desired_base_salary
        remove_column :career_profiles_versions, :desired_base_salary
        add_column :career_profiles, :desired_base_salary, :integer
        add_column :career_profiles_versions, :desired_base_salary, :integer

        # remove phone in favor of users.phone
        remove_column :career_profiles, :phone_number
        remove_column :career_profiles_versions, :phone_number

        create_trigger(:career_profiles)




        # add field to work_experience
        drop_trigger(:work_experiences)

        add_column :work_experiences, :organization, :text
        add_column :work_experiences_versions, :organization, :text

        create_trigger(:work_experiences)
    end

    def down

        drop_trigger(:career_profiles)


        # (Revert) Third-Party / Ad-Block Paranoid URL field normalization
        rename_column :career_profiles, :li_profile_url, :linked_in_url
        rename_column :career_profiles_versions, :li_profile_url, :linked_in_url

        rename_column :career_profiles, :fb_profile_url, :facebook_profile_url
        rename_column :career_profiles_versions, :fb_profile_url, :facebook_profile_url

        rename_column :career_profiles, :tw_profile_url, :twitter_profile_url
        rename_column :career_profiles_versions, :tw_profile_url, :twitter_profile_url

        # (Revert) Normalization of choose-multiple values
        remove_column :career_profiles, :job_sectors_of_interest
        remove_column :career_profiles_versions, :job_sectors_of_interest
        add_column :career_profiles, :job_sectors_of_interest, :json, :default => {}, :null => false
        add_column :career_profiles_versions, :job_sectors_of_interest, :json, :default => {}, :null => false

        remove_column :career_profiles, :employment_types_of_interest
        remove_column :career_profiles_versions, :employment_types_of_interest
        add_column :career_profiles, :employment_types_of_interest, :json, :default => {}, :null => false
        add_column :career_profiles_versions, :employment_types_of_interest, :json, :default => {}, :null => false

        remove_column :career_profiles, :company_sizes_of_interest
        remove_column :career_profiles_versions, :company_sizes_of_interest
        add_column :career_profiles, :company_sizes_of_interest, :json, :default => {}, :null => false
        add_column :career_profiles_versions, :company_sizes_of_interest, :json, :default => {}, :null => false

        # (Revert) primary_area_of_interest to primary_areas_of_interest (multiple)
        remove_column :career_profiles, :primary_areas_of_interest
        remove_column :career_profiles_versions, :primary_areas_of_interest
        add_column :career_profiles, :primary_area_of_interest, :text
        add_column :career_profiles_versions, :primary_area_of_interest, :text

        # (Revert) remove favorite_mba_subject
        add_column :career_profiles, :favorite_mba_subject, :text
        add_column :career_profiles_versions, :favorite_mba_subject, :text

        # (Revert) remove old place fields
        add_column :career_profiles, :street_address, :text
        add_column :career_profiles_versions, :street_address, :text
        add_column :career_profiles, :city_state, :text
        add_column :career_profiles_versions, :city_state, :text
        add_column :career_profiles, :country_identifier, :text
        add_column :career_profiles_versions, :country_identifier, :text
        add_column :career_profiles, :time_zone_identiifier, :text # intentionally misspelled (see create_career_profiles.rb)
        add_column :career_profiles_versions, :time_zone_identiifier, :text # intentionally misspelled (see create_career_profiles.rb)

        # (Revert) add new place fields
        remove_column :career_profiles, :place_id
        remove_column :career_profiles_versions, :place_id
        remove_column :career_profiles, :place_details
        remove_column :career_profiles_versions, :place_details

        # (Revert) update to numeric type
        remove_column :career_profiles, :desired_base_salary
        remove_column :career_profiles_versions, :desired_base_salary
        add_column :career_profiles, :desired_base_salary, :text
        add_column :career_profiles_versions, :desired_base_salary, :text

        # (Revert) remove phone in favor of users.phone
        add_column :career_profiles, :phone_number, :text
        add_column :career_profiles_versions, :phone_number, :text

        create_trigger(:career_profiles)




        # (Revert) add field to work_experience
        drop_trigger(:work_experiences)

        remove_column :work_experiences, :organization
        remove_column :work_experiences_versions, :organization

        create_trigger(:work_experiences)

    end


    # Helper Methods

    def drop_trigger(table_name)
        execute "DROP TRIGGER #{table_name}_versions ON #{table_name}"
    end

    def create_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("DROP TRIGGER IF EXISTS #{table_name}_versions ON #{table_name}")
        execute(trigger_sql)
    end
end
