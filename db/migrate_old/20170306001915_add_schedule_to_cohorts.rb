class AddScheduleToCohorts < ActiveRecord::Migration[5.0]
    # Add schedule as well as some other tweaks

    def up
        add_column :cohorts, :name, :text
        add_column :cohorts_versions, :name, :text

        ActiveRecord::Base.connection.execute("UPDATE cohorts SET name = name_locales->>'en'")
        ActiveRecord::Base.connection.execute("UPDATE cohorts_versions SET name = name_locales->>'en'") # need to make sure published versions have it

        # So we can migrate first
        change_column :cohorts, :name, :text, :null => false

        ViewHelpers.set_versions(self, 20170306001915)

        remove_column :cohorts, :name_locales, :json, :null => false, :default => {}
        remove_column :cohorts_versions, :name_locales, :json


        add_column :cohorts, :required_playlist_pack_ids, :uuid, :array => true, :default => '{}'
        add_column :cohorts_versions, :required_playlist_pack_ids, :uuid, :array => true, :default => '{}'
        add_column :cohorts, :specialization_playlist_pack_ids, :uuid, :array => true, :default => '{}'
        add_column :cohorts_versions, :specialization_playlist_pack_ids, :uuid, :array => true, :default => '{}'

        ActiveRecord::Base.connection.execute("UPDATE cohorts SET required_playlist_pack_ids = playlist_pack_ids")
        ActiveRecord::Base.connection.execute("UPDATE cohorts_versions SET required_playlist_pack_ids = playlist_pack_ids") # need to make sure published versions have it

        # we wil drop playlist_pack_ids in https://trello.com/c/SeMhrioj/1787-schedule-post-rollout-cleanup

        # We will drop this in https://trello.com/c/U1Tw3uyo
        # remove_column :cohorts, :end_date, :datetime
        # remove_column :cohorts_versions, :end_date, :datetime

        remove_column :cohorts, :status, :text, null: false, default: 'closed'
        remove_column :cohorts_versions, :status, :text

        remove_column :cohorts, :notes, :text, null: false, default: ''
        remove_column :cohorts_versions, :notes, :text

        remove_column :cohorts, :required_electives, :integer, null: false, default: 0
        remove_column :cohorts_versions, :required_electives, :integer

        add_column :cohorts, :periods, :json, null: false, default: [], array: true
        add_column :cohorts_versions, :periods, :json, array: true

        # Note: Could not call it just "type" due to usage of that property by Rails
        add_column :cohorts, :program_type, :text, null: false, default: 'mba'
        add_column :cohorts_versions, :program_type, :text

        if Rails.env == 'test'
            create_table :schedulable_items, id: :uuid do |t|
            end

            add_column :schedulable_items, :required_playlist_pack_ids, :uuid, :array => true, :default => '{}'
            add_column :schedulable_items, :specialization_playlist_pack_ids, :uuid, :array => true, :default => '{}'
            add_column :schedulable_items, :periods, :json, null: false, default: '{}', array: true
            add_column :schedulable_items, :num_required_specializations, :integer
        end
    end

    def down
        remove_column :cohorts, :program_type, :text, null: false, default: 'mba'
        remove_column :cohorts_versions, :program_type, :text

        add_column :cohorts, :name_locales, :json, :null => false, :default => {}
        add_column :cohorts_versions, :name_locales, :json

        # not fixing published versions here. eh
        Cohort.all.each do |cohort|
            cohort.name_locales['en'] = cohort[:name]
            cohort.save!
        end

        Cohort::Version::Updatable.all.each do |cohort_version|
            cohort_version.name_locales = {'en' => cohort_version[:name]}
            cohort_version.save!
        end

        ViewHelpers.set_versions(self, 20170306001915 - 1)

        remove_column :cohorts, :name, :text, :null => false
        remove_column :cohorts_versions, :name, :text

        ActiveRecord::Base.connection.execute("UPDATE cohorts SET playlist_pack_ids = required_playlist_pack_ids")
        ActiveRecord::Base.connection.execute("UPDATE cohorts_versions SET playlist_pack_ids = required_playlist_pack_ids")

        remove_column :cohorts, :required_playlist_pack_ids, :uuid, :array => true, :default => '{}'
        remove_column :cohorts_versions, :required_playlist_pack_ids, :uuid, :array => true, :default => '{}'
        remove_column :cohorts, :specialization_playlist_pack_ids, :uuid, :array => true, :default => '{}'
        remove_column :cohorts_versions, :specialization_playlist_pack_ids, :uuid, :array => true, :default => '{}'


        remove_column :cohorts, :periods, :json, null: false, default: {}
        remove_column :cohorts_versions, :periods, :json

        add_column :cohorts, :status, :text, null: false, default: 'closed'
        add_column :cohorts_versions, :status, :text

        add_column :cohorts, :notes, :text, null: false, default: ''
        add_column :cohorts_versions, :notes, :text

        add_column :cohorts, :required_electives, :integer, null: false, default: 0
        add_column :cohorts_versions, :required_electives, :integer

        if Rails.env == 'test'
            drop_table :schedulable_items
        end

    end
end
