class BackfillAcceptedOrganizations < ActiveRecord::Migration[4.2]
    def up
        ProfessionalOrganizationOption.where("created_at > '2016-09-27' AND suggest = true").each do |organization|
            # ensure we have copies for all locales
            organization.promote_suggested_for_all_locales # only one -- 'WotNow by Alltivity' at time of authoring
        end
    end

    def down
        # noop
    end
end
