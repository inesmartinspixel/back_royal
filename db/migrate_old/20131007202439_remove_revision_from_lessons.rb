class RemoveRevisionFromLessons < ActiveRecord::Migration[4.2]
    def up
        change_table :lessons do |t|
            t.remove :revision_guid
            t.remove :current
            t.rename :content, :content_json
        end

        remove_index(:lessons, :lesson_guid)
        add_index(:lessons, :lesson_guid, :unique => true)
    end

    def down
        change_table :lessons do |t|
            t.string :revision_guid
            t.boolean :current
            t.rename :content_json, :content
        end

        remove_index(:lessons, :lesson_guid) # remove the unique index
        add_index(:lessons, :lesson_guid)
        add_index(:lessons, :revision_guid, :unique => true)
        add_index(:lessons, :current, :unique => true) # there can be many records with current=nil, but only one with current=true (This should have been lesson_guid, current. But it is removed in following migration anyhow)
    end
end
