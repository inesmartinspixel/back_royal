class RemoveCareerProfileActiveLastUpdatedAt < ActiveRecord::Migration[5.1]
    def change
        remove_column :career_profiles, :career_profile_active_last_updated_at, :datetime
        remove_column :career_profiles_versions, :career_profile_active_last_updated_at, :datetime
    end
end
