class AlterLocaleColumnInLessonStreams < ActiveRecord::Migration[4.2]
    def up
        # add the default after the migration so that two updates
        # do not lead to a duplicate key error in the versions table/
        # Do it in a separate migration because you cannot alter a column with
        # pedning changes in migration
        change_column :lesson_streams, :locale, :text, :default => 'en', :null => false
    end

    def down
        change_column :lesson_streams, :locale, :text, :default => nil, :null => true
    end
end
