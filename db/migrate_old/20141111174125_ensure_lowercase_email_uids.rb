class EnsureLowercaseEmailUids < ActiveRecord::Migration[4.2]
    def change
        execute "UPDATE users SET email = LOWER(email), uid = LOWER(email)"
    end
end
