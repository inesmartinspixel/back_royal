class RePublishPublishedVersions < ActiveRecord::Migration[4.2]

    # disable_ddl_transaction!

    def up

        query = 'published_version_object IS NOT NULL'
        total = ContentPublisher.where(query).count
        i = 0;

        ContentPublisher.where(query).find_in_batches(batch_size: 50) do |content_publishers|
            content_publishers.each do |content_publisher|
                    content_publisher.publish!(content_publisher.published_version)
                    i = i + 1
            end
            puts "#{100*i/total.to_f}% complete"
        end

    end
end
