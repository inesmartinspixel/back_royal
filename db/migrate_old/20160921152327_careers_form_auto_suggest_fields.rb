class CareersFormAutoSuggestFields < ActiveRecord::Migration[4.2]

    def up


        ###############################
        # Misc Cleanup
        ###############################

        truncate_tables

        ###############################
        # Relational Consistency
        ###############################


        # replace work_experiences user_id with career_profile_id
        remove_column :work_experiences, :user_id
        remove_column :work_experiences_versions, :user_id
        add_column :work_experiences, :career_profile_id, :uuid, null: false
        add_column :work_experiences_versions, :career_profile_id, :uuid
        add_foreign_key :work_experiences, :career_profiles, column: :career_profile_id, primary_key: :id, name: 'work_experiences_career_profiles_fk'

        # replace work_experiences user_id with career_profile_id
        remove_column :education_experiences, :user_id
        remove_column :education_experiences_versions, :user_id
        add_column :education_experiences, :career_profile_id, :uuid, null: false
        add_column :education_experiences_versions, :career_profile_id, :uuid
        add_foreign_key :education_experiences, :career_profiles, column: :career_profile_id, primary_key: :id, name: 'education_experiences_career_profiles_fk'


        ###############################
        # More About You Fields
        ###############################

        # add new schema for More About You fields
        create_auto_suggest_table("skills_options", "career_profiles")
        create_auto_suggest_table("awards_and_interests_options", "career_profiles")

        # remove old Skills column
        remove_column :career_profiles, :skills
        remove_column :career_profiles_versions, :skills

        # remove old Awards and Interests section
        remove_column :career_profiles, :awards_and_interests
        remove_column :career_profiles_versions, :awards_and_interests


        ###############################
        # Work Experience Fields
        ###############################

        # add new schema for Organization and remove old column
        create_auto_suggest_table("professional_organization_options", "work_experiences", true)
        remove_column :work_experiences, :organization
        remove_column :work_experiences_versions, :organization

        # add new column
        add_column :work_experiences, :professional_organization_option_id, :uuid, null: false
        add_column :work_experiences_versions, :professional_organization_option_id, :uuid
        add_foreign_key :work_experiences, :professional_organization_options, column: :professional_organization_option_id, primary_key: :id, name: 'work_experiences_professional_organization_option_id_fk'


        ###############################
        # Education Experience Fields
        ###############################

        # add new schema for Organization and remove old column
        create_auto_suggest_table("educational_organization_options", "education_experiences", true)
        remove_column :education_experiences, :institution_name
        remove_column :education_experiences_versions, :institution_name

        # add new column
        add_column :education_experiences, :educational_organization_option_id, :uuid, null: false
        add_column :education_experiences_versions, :educational_organization_option_id, :uuid
        add_foreign_key :education_experiences, :educational_organization_options, column: :educational_organization_option_id, primary_key: :id, name: 'education_experiences_educational_organization_option_id_fk'


    end

    def down

        ###############################
        # Misc Cleanup
        ###############################

        truncate_tables


        ###############################
        # More About You Fields
        ###############################

        # (Revert) add new schema for More About You fields
        drop_auto_suggest_table("skills_options", "career_profiles")
        drop_auto_suggest_table("awards_and_interests_options", "career_profiles")


        # (Revert) remove old Skills column
        add_column :career_profiles, :skills, :text, default: [], array: true, null: false
        add_column :career_profiles_versions, :skills, :text, array: true

        # (Revert) remove old Awards and Interests section
        add_column :career_profiles, :awards_and_interests, :text, default: [], array: true, null: false
        add_column :career_profiles_versions, :awards_and_interests, :text, array: true


        ###############################
        # Work Experience Fields
        ###############################

        # (Revert) add new column
        remove_column :work_experiences, :professional_organization_option_id
        remove_column :work_experiences_versions, :professional_organization_option_id

        # (Revert) add new schema for Organization and remove old column
        drop_auto_suggest_table("professional_organization_options", "work_experiences")
        add_column :work_experiences, :organization, :text
        add_column :work_experiences_versions, :organization, :text


        ###############################
        # Education Experience Fields
        ###############################

        # (Revert) add new column
        remove_column :education_experiences, :educational_organization_option_id
        remove_column :education_experiences_versions, :educational_organization_option_id

        # (Revert) add new schema for Organization and remove old column
        drop_auto_suggest_table("educational_organization_options", "education_experiences")
        add_column :education_experiences, :institution_name, :text
        add_column :education_experiences_versions, :institution_name, :text



        ###############################
        # Relational Consistency
        ###############################


        # (Revert) replace work_experiences user_id with career_profile_id
        remove_column :work_experiences, :career_profile_id
        remove_column :work_experiences_versions, :career_profile_id
        add_column :work_experiences, :user_id, :uuid, null: false
        add_column :work_experiences_versions, :user_id, :uuid
        add_foreign_key :work_experiences, :users, column: :user_id, primary_key: :id, name: 'work_experiences_users_fk'

        # (Revert) replace work_experiences user_id with career_profile_id
        remove_column :education_experiences, :career_profile_id
        remove_column :education_experiences_versions, :career_profile_id
        add_column :education_experiences, :user_id, :uuid, null: false
        add_column :education_experiences_versions, :user_id, :uuid
        add_foreign_key :education_experiences, :users, column: :user_id, primary_key: :id, name: 'education_experiences_users_fk'


    end


    # Table Helper Methods

    def truncate_tables
        execute "TRUNCATE TABLE work_experiences CASCADE"
        execute "TRUNCATE TABLE work_experiences_versions CASCADE"
        execute "TRUNCATE TABLE education_experiences CASCADE"
        execute "TRUNCATE TABLE education_experiences_versions CASCADE"
    end

    def create_auto_suggest_table(table_name, mapped_table, organization_field = false)

        # create the option table

        create_table    "#{table_name}", id: :uuid do |t|
            t.timestamps                                                        null: false
            t.text          :text,                                              null: false
            t.text          :locale,                        default: 'en',      null: false
            t.boolean       :suggest,                       default: false
            t.index         [:locale, :text],               unique: true,       name: "#{table_name}_unique"
            t.index         [:suggest, :locale],                                name: "#{table_name}_auto_suggest"
        end
        if organization_field
            add_column "#{table_name}", :source_id, :text
            add_column "#{table_name}", :image_url, :text
        end

        create_versions_table_and_trigger(table_name)

        if !organization_field # these fields don't need join-tables

            # create the reference table
            join_table = "#{mapped_table}_#{table_name}"

            create_table    "#{join_table}", id: :uuid do |t|
                t.uuid      "#{mapped_table.singularize}_id",       null: false
                t.uuid      "#{table_name.singularize}_id",         null: false
                t.integer   :position,                              null: false  # needs to be orderable
                t.index     [ "#{mapped_table.singularize}_id", "#{table_name.singularize}_id"],  unique: true,  name: "#{join_table}_unique"
            end

            add_foreign_key "#{join_table}", "#{table_name}",   column: "#{table_name.singularize}_id",     primary_key: "id"
            add_foreign_key "#{join_table}", "#{mapped_table}", column: "#{mapped_table.singularize}_id",   primary_key: "id"

            create_versions_table_and_trigger(join_table)

        end

    end


    def drop_auto_suggest_table(table_name, mapped_table)
        join_table = "#{mapped_table}_#{table_name}"

        execute "DROP TABLE IF EXISTS #{join_table}"
        execute "DROP TABLE IF EXISTS #{join_table}_versions"

        execute "DROP TABLE #{table_name}"
        execute "DROP TABLE #{table_name}_versions"
    end


    # Trigger Helper Methods

    def create_versions_table_and_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.connection.schema_cache.clear!
        klass.reset_column_information

        # create the version table, including all columns from the source table
        # plus a few extras
        create_table "#{table_name}_versions", id: false, force: :cascade do |t|
            t.uuid     "version_id", null: false, default: "uuid_generate_v4()"
            t.string   "operation", limit: 1, null: false
            t.datetime "version_created_at", null: false

            puts "*** #{table_name}_versions"

            klass.column_names.each do |column_name|
                type = klass.columns_hash

                column_config = klass.columns_hash[column_name]
                meth = column_config.type.to_sym
                options = {}
                options[:limit] = column_config.limit unless column_config.limit.nil?
                options[:null] = false if column_config.null == false
                options[:array] = column_config.array
                # ignore defaults, since they should not be used in the audit table

                puts "t.#{meth}, #{column_name}, #{options.inspect}"
                t.send(meth, column_name, options)
            end
        end

        # for some reason the primary flag on create_table was not working
        execute "ALTER TABLE #{table_name}_versions ADD PRIMARY KEY (version_id);"

        # create indexes
        if klass.column_names.include?('id') && klass.column_names.include?('updated_at')
            add_index "#{table_name}_versions", [:id, :updated_at], name: "#{table_name}_versions_id_updated_at"
        elsif klass.column_names.include?('id')
            add_index "#{table_name}_versions", :id, name: "#{table_name}_versions_id"
        end

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "
        execute(trigger_sql)
    end

end
