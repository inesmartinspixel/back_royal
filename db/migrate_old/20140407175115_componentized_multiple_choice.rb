require File.expand_path('../componentized_migrators/multiple_choice_migrator', __FILE__)

class ComponentizedMultipleChoice < ActiveRecord::Migration[4.2]

    disable_ddl_transaction!

    def up
        migrator = MultipleChoiceMigrator.new
        migrator.change
    end
end
