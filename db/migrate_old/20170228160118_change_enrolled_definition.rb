class ChangeEnrolledDefinition < ActiveRecord::Migration[5.0]
    def up

        #update CohortContentDetails and CohortStatusChanges
        ViewHelpers.set_versions(self, 20170228160118)

    end

    def down

        ViewHelpers.set_versions(self, 20170228160118 - 1)

    end

end