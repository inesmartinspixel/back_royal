class AddPlaylistPackIdsToInstitutions < ActiveRecord::Migration[5.0]
    def up
        add_column :institutions, :playlist_pack_ids, :uuid, array: true, default: '{}'
        add_column :institutions_versions, :playlist_pack_ids, :uuid, array: true

        # Redacted to remove the reference in code
        # Institution.all.each do |institution|
        #     playlist_locale_pack_ids = institution.playlist_locale_packs.pluck('id') || []

        #     institution.playlist_pack_ids = playlist_locale_pack_ids
        #     institution.skip_refresh_materialized_views = true
        #     institution.save!

        #     puts "Migrated #{playlist_locale_pack_ids} to institution #{institution.name}"
        # end
    end

    def down
        remove_column :institutions, :playlist_pack_ids
        remove_column :institutions_versions, :playlist_pack_ids
    end
end
