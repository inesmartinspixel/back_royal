class CreateUsersRelevantCohorts < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20170601135311)
    end

    def down
        ViewHelpers.rollback(self, 20170601135311)
    end
end