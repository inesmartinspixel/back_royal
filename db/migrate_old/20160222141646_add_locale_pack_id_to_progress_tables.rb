class AddLocalePackIdToProgressTables < ActiveRecord::Migration[4.2]
    def up

        add_column :lesson_progress, :locale_pack_id, :uuid
        add_column :lesson_streams_progress, :locale_pack_id, :uuid

        add_index :lesson_progress, [:user_id, :locale_pack_id], :unique => true
        add_index :lesson_streams_progress, [:user_id, :locale_pack_id], :unique => true

    end

    def down
        remove_column :lesson_progress, :locale_pack_id, :uuid
        remove_column :lesson_streams_progress, :locale_pack_id, :uuid
    end
end