class CreateDistinctUserIds < ActiveRecord::Migration[4.2]
    def change
        create_table :distinct_user_ids, id: false do |t|

            t.uuid :user_id, null: false

        end

        add_index :distinct_user_ids, :user_id, :unique => true
    end
end