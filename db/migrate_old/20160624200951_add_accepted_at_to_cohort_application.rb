class AddAcceptedAtToCohortApplication < ActiveRecord::Migration[4.2]
    def change
        add_column :cohort_applications, :accepted_at, :datetime
        add_column :cohort_applications_versions, :accepted_at, :datetime
    end
end
