class AddPracticeFlagsToLessons < ActiveRecord::Migration[4.2]
    def change

        add_column :lesson_locale_packs, :practice_locale_pack_id, :uuid
        add_column :lesson_locale_packs_versions, :practice_locale_pack_id, :uuid
        add_index :lesson_locale_packs, :practice_locale_pack_id, :unique => true

    end
end
