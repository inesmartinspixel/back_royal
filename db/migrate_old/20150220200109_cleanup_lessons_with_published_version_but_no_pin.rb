class CleanupLessonsWithPublishedVersionButNoPin < ActiveRecord::Migration[4.2]
    def up

        execute('alter table lessons disable trigger lessons_versions')
        Lesson::Version.
            joins(:content_publisher).
            where(pinned: false).each do |version|

            execute("
                UPDATE lessons_versions
                SET pinned=TRUE, was_published=TRUE
                WHERE version_id='#{version.version_id}'
            ")

            associated_lesson = version.lesson
            if associated_lesson.updated_at == version.updated_at
                associated_lesson.update_columns({
                    pinned: true,
                    was_published: true
                })
            end
        end

        execute('alter table lessons enable trigger lessons_versions')

    end
end
