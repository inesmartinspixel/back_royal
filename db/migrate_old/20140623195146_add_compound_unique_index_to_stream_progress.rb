class AddCompoundUniqueIndexToStreamProgress < ActiveRecord::Migration[4.2]
    def change
        add_index(:lesson_streams_progress, [:lesson_stream_id, :user_id], :unique => true)
    end
end
