class ConvertOverviewReportsToViews < ActiveRecord::Migration[5.0]
    def up

        # clean up old stuff
        execute('drop table if exists user_lesson_progress_records_bak')
        execute('drop table if exists user_progress_records_bak')

        execute 'alter table etl.activity_by_calendar_date_records rename to activity_by_calendar_date_records_bak'
        ViewHelpers.migrate(self, 20170403171258)
    end

    def down
        ViewHelpers.rollback(self, 20170403171258)

        execute 'alter table etl.activity_by_calendar_date_records_bak rename to activity_by_calendar_date_records'
    end
end