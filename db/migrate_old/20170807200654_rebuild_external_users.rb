class RebuildExternalUsers < ActiveRecord::Migration[5.1]
    def up

        execute('drop view if exists external_users_events')
        execute('drop view if exists consumer_users_events')
        execute('drop view if exists institutional_users_events')
        execute('drop view if exists consumer_users')
        execute('drop view if exists institutional_users')

        ViewHelpers.migrate(self, 20170807200654)
    end

    def down
        ViewHelpers.rollback(self, 20170807200654)

        # we do not bother rebuilding those views.
    end
end
