class UpdateUserShortcutDisabled < ActiveRecord::Migration[5.0]
    def up
        change_column_default :users, :pref_keyboard_shortcuts, false
    end
    def down
        change_column_default :users, :pref_keyboard_shortcuts, true
    end
end
