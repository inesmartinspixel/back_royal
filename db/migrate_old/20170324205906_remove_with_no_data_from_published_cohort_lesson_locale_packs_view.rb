class RemoveWithNoDataFromPublishedCohortLessonLocalePacksView < ActiveRecord::Migration[5.0]
    def up
        ViewHelpers.migrate(self, 20170324205906)
    end

    def down
        ViewHelpers.rollback(self, 20170324205906)
    end
end
