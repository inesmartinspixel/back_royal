class AdditionalUserDetails < ActiveRecord::Migration[4.2]

    # No need to drop user views for these processes, because the trigger doesn't
    # compare on JSON values (yet). So no need to rebuild it.

    def up
        add_column :users, :additional_details, :json
        add_column :users_versions, :additional_details, :json
    end

    def down
        remove_column :users, :additional_details, :json
        remove_column :users_versions, :additional_details, :json
    end

end
