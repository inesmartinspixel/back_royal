class AddDuplicatedFromAndDuplicatedToToContentItems < ActiveRecord::Migration[4.2]
    def change

        [:lessons, :lessons_versions, :lesson_streams, :lesson_streams_versions].each do |table|
            add_column table, :duplicated_from_id, :uuid
            add_column table, :duplicated_to_id, :uuid
        end

    end
end
