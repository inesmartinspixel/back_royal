class RemoveLessonProgressDuplicates < ActiveRecord::Migration[4.2]
    def change
        puts '-- Updating lesson_progress to remove any duplicates --'
        remove_dups_from_lesson_progress
    end


    def remove_dups_from_lesson_progress
        # find the (user_id, lesson_guid) pairs where we have duplicates
        find_dups_query = "select user_id, lesson_guid from (select user_id, lesson_guid, count(*) num_rows from lesson_progress group by user_id, lesson_guid) as count_summaries where count_summaries.num_rows >= 2"
        dups_array = ActiveRecord::Base.connection.execute(find_dups_query)

        # iterate over the dups
        dups_array.each do |dup|
            user_id = dup["user_id"].to_i
            lesson_guid = dup["lesson_guid"]
            # order first by completed_at asc (which puts nils last) then by created_at desc to pick the
            # either the completed one, or if that doesnt exist, the most recently updated one
            set_of_dups = LessonProgress.where(:user_id => user_id, :lesson_guid => lesson_guid).order(:completed_at => :asc, :updated_at => :desc)

            # delete all except the first one
            (1..set_of_dups.length - 1).each do |i|
                set_of_dups[i].destroy!
            end

        end

    end

end
