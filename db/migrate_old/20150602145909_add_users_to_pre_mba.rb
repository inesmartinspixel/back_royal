class AddUsersToPreMba < ActiveRecord::Migration[4.2]
    def up


        ##################################
        ## CLEAR OUT DEMO INSTITUTION
        ##################################

        # find all users that belong to DEMO institution ...
        demo_users = User.
            joins("LEFT OUTER JOIN tags AS groups ON groups.entity_id = users.id").includes(:groups).
            joins("LEFT OUTER JOIN institutions_users ON users.id = institutions_users.user_id LEFT OUTER JOIN institutions ON institutions_users.institution_id = institutions.id").includes(:institutions).
            where("institutions.name = 'DEMO'")


        # add them to the DEMOUSER group
        demo_users.each do |user|
            user.add_to_group('DEMOUSER')
            user.save!
        end

        # remove DEMO institution
        dead_institution = Institution.find_by_name('DEMO')
        if dead_institution
            dead_institution.destroy!
        end



        ##################################
        ## CLEAR OUT BETA INSTITUTION
        ##################################

        # create the BETA group
        # Tag.create_group('BETA')

        # find all users that belong to BET institution ...
        old_beta_users = User.
            joins("LEFT OUTER JOIN tags AS groups ON groups.entity_id = users.id").includes(:groups).
            joins("LEFT OUTER JOIN institutions_users ON users.id = institutions_users.user_id LEFT OUTER JOIN institutions ON institutions_users.institution_id = institutions.id").includes(:institutions).
            where("institutions.name = 'BETA'")


        # add them to the BETA group
        old_beta_users.each do |user|
            user.add_to_group('BETA')
            user.save!
        end

        # remove DEMO institution
        dead_institution = Institution.find_by_name('BETA')
        if dead_institution
            dead_institution.destroy!
        end



        ##################################
        ## CLEAR OUT BLUEOCEAN INSTITUTION
        ##################################


        # find all users that belong to BLUEOCEAN institution ...
        blue_ocean_users = User.
            joins("LEFT OUTER JOIN tags AS groups ON groups.entity_id = users.id").includes(:groups).
            joins("LEFT OUTER JOIN institutions_users ON users.id = institutions_users.user_id LEFT OUTER JOIN institutions ON institutions_users.institution_id = institutions.id").includes(:institutions).
            where("institutions.name = 'BLUEOCEAN'")


        # add them to the BLUEOCEAN group
        blue_ocean_users.each do |user|
            user.add_to_group('BLUEOCEAN')
            user.save!
        end

        # remove BLUEOCEAN institution
        dead_institution = Institution.find_by_name('BLUEOCEAN')
        if dead_institution
            dead_institution.destroy!
        end


        ##################################
        ## MIGRATE BETA USERS TO PRE_MBA
        ##################################

        # find all users that belong in target groups ('BETA', 'BETA2', 'CORNELL', 'INSEAD2014') ...
        beta_users = User.
            joins("LEFT OUTER JOIN tags AS groups ON groups.entity_id = users.id").includes(:groups).
            where("groups.text IN ('BETA', 'BETA2', 'CORNELL', 'INSEAD2014') AND groups.entity_type = 'User'")

        # add these users to PRE_MBA group, and clear out their institutions
        beta_users.each do |user|
            user.institutions = []
            user.add_to_group('PRE_MBA')
            user.save!
        end


        ##################################
        ## REMOVE PEDAGO INSTITUTION
        ##################################

        # remove Pedago institution
        dead_institution = Institution.find_by_name('Pedago')
        if dead_institution
            dead_institution.destroy!
        end

    end

    def down
        # hard to migrate this away  =]
    end
end
