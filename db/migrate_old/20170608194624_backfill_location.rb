class BackfillLocation < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        # batch update cohort applications outside of a transaction to prevent
        # large table locks
        CareerProfile.select('id')
            .where(
                career_profile_active: true,
                location: nil
            )
            .where("place_details->>'lat' is not null")
            .where("place_details->>'lng' is not null")
            .find_in_batches do |batch|

                id_string = batch.map(&:id).map { |id| "'#{id}'" }.join(',')
                ActiveRecord::Base.connection.execute %Q~
                    update career_profiles
                    set location = ST_GeographyFromText('POINT(' || (place_details->>'lng')::text || ' ' || (place_details->>'lat')::text || ')')
                    where id in (#{id_string})
                ~
        end
    end
end
