class AddKeywordSearchToCareerProfileSearches < ActiveRecord::Migration[5.1]
    def change
        add_column :career_profile_searches, :keyword_search, :text
    end
end
