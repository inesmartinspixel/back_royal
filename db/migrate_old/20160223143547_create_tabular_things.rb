class CreateTabularThings < ActiveRecord::Migration[4.2]
    def change
        return unless Rails.env == 'test'
        create_table :tabular_things, id: :uuid do |t|
            t.text :color
            t.text :shape
            t.uuid :user_id
        end
    end
end
