class AddCollectedInToConceptProgress < ActiveRecord::Migration[4.2]
    def change
        # this could be an integer reference id to the lessons table, but using the json field
        # lets us add additional references in the future (like frame or stream) without data model changes
        # and cache metadata like lesson_title so we dont have to do a separate lookup for it on retrieval
        add_column :concept_progress, :collected_in, :json
    end
end
