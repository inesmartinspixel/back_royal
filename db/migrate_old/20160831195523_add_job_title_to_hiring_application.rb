class AddJobTitleToHiringApplication < ActiveRecord::Migration[4.2]
    def up

        add_column :hiring_applications, :job_title, :text
        add_column :hiring_applications_versions, :job_title, :text
    end

    def down
        remove_column :hiring_applications, :job_title
        remove_column :hiring_applications_versions, :job_title
    end
end
