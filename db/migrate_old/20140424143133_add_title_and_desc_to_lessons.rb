class AddTitleAndDescToLessons < ActiveRecord::Migration[4.2]
    def change
        add_column :lessons, :title, :string
        add_index :lessons, :title
        add_column :lessons, :description, :string
    end
end
