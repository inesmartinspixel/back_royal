require File.expand_path('../20161116201437_views_for_mba_score_query.rb', __FILE__)

class CreateUserCanSeeViews < ActiveRecord::Migration[5.0]
    def up

        ViewHelpers.set_versions(self, 20170221140312)

        # since the query above relies on published cohorts, we need
        # to go ahead and publish existing ones now (Removed this later
        # after this had already rolled to prod because it conflicts with
        # code added later)
        # Cohort.all.map(&:publish!)



    end

    def down

        views = %w(
            cohort_applications_plus
            cohort_user_lesson_progress_records
            cohort_user_weeks
            cohort_user_progress_records
            cohort_user_progress_records_with_weeks
            cohort_conversion_rates
        )

        # we need to remove the existing non-materialized content views
        # while we replace the content views
        views.reverse.each do |name|
            execute "drop view #{name}"
        end

        ViewHelpers.set_versions(self, 20170221140312 - 1)

        views.each do |name|
            old_migration = ViewsForMbaScoreQuery.new
            old_migration.send(name.to_sym) if old_migration.respond_to?(name.to_sym)
        end
    end
end
