class AddCompoundUniqueIndexToLessonProgress < ActiveRecord::Migration[4.2]
    def change
        add_index(:lesson_progress, [:lesson_guid, :user_id], :unique => true)
    end
end
