class AddContentViewsRefresh < ActiveRecord::Migration[5.0]
    def up
        create_table :content_views_refresh, id: false do |t|
            t.datetime :updated_at
        end

        # insert and assume updates from here on out
        execute "INSERT INTO content_views_refresh (updated_at) VALUES (NOW())"
    end

    def down
        drop_table :content_views_refresh
    end
end
