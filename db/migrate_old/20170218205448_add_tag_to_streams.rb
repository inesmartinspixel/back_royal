class AddTagToStreams < ActiveRecord::Migration[5.0]
    def change
        add_column :lesson_streams, :tag, :text
        add_column :lesson_streams_versions, :tag, :text
    end
end
