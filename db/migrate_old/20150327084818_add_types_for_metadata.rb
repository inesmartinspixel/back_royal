class AddTypesForMetadata < ActiveRecord::Migration[4.2]
    def up
        execute "CREATE TYPE entity_metadata_json_v1 AS (id uuid, title varchar(256), description varchar(256), canonical_url text, image json)"
    end

    def down
        execute "DROP TYPE entity_metadata_json_v1"
    end
end
