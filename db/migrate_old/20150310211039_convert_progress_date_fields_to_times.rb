class ConvertProgressDateFieldsToTimes < ActiveRecord::Migration[4.2]
    def up
        change_column :collection_progress, :started_at, :datetime
        change_column :collection_progress, :completed_at, :datetime
        change_column :concept_progress, :collected_at, :datetime
    end

    def down
        change_column :collection_progress, :started_at, :date
        change_column :collection_progress, :completed_at, :date
        change_column :concept_progress, :collected_at, :date
    end
end
