class AddFingerprintToPaperclipAssets < ActiveRecord::Migration[4.2]
    def change
        add_column :s3_assets, :file_fingerprint, :string
    end
end
