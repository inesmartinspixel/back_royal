class CreateS3Assets < ActiveRecord::Migration[4.2]
    def up
        drop_table :lesson_images

        create_table :s3_assets do |t|
            t.timestamps
            t.attachment :file
            t.string :directory
            t.string :styles
        end
    end

    def down
        drop_table :s3_assets

        create_table :lesson_images do |t|
            t.timestamps
            t.integer :lesson_id
            t.attachment :file
        end
    end
end
