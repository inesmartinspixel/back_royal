class AddIndexOnCreatedAtForMailboxerReceipts < ActiveRecord::Migration[5.0]
    def change
        add_index :mailboxer_receipts, [:receiver_id, :created_at]
    end
end
