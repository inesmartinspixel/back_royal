class UpdatesForInstConsumerSubscriptions < ActiveRecord::Migration[4.2]
    def change
        add_index(:subscriptions, :user_id)

        remove_column :subscriptions, :free_trial, :boolean
        remove_column :subscriptions, :free_trial_start_date, :date
        remove_column :subscriptions, :free_trial_days_valid, :integer
        add_column :subscriptions, :free_trial_start_date, :datetime
        add_column :subscriptions, :free_trial_end_date, :datetime

        create_table :institutions_users do |t|
            t.timestamps
            t.references :institution
            t.references :user
        end
    end
end
