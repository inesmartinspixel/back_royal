class CreateLessonProgress < ActiveRecord::Migration[4.2]
    def change
        create_table :lesson_progress do |t|
            t.timestamps
            t.references :lesson
            t.string :frame_bookmark_guid
            t.datetime :started_at
            t.datetime :completed_at
            t.references :user
        end

        add_index(:lesson_progress, :lesson_id)
        add_index(:lesson_progress, :user_id)

    end
end
