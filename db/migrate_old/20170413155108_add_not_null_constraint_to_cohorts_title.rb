class AddNotNullConstraintToCohortsTitle < ActiveRecord::Migration[5.0]
    def change
        change_column_null :cohorts, :title, false, ""
    end
end
