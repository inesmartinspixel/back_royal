class AddComingSoonCourseFlag < ActiveRecord::Migration[4.2]
    def change
        add_column :lesson_streams, :coming_soon, :boolean, default: false, null: false
        add_column :lesson_streams_versions, :coming_soon, :boolean
    end
end
