class PopulateLessonFulltext < ActiveRecord::Migration[4.2]

    disable_ddl_transaction!

    def up

        # process all lessons prior to adding the index
        total_count = Lesson.count
        puts "#{total_count} Lessons found."
        batch_size = 50

        Lesson.find_in_batches(batch_size: batch_size).with_index do |lessons, batch|
            puts "Processing batch ##{batch} of #{(total_count / batch_size).ceil}"
            lessons.each do |lesson|
                lesson.update_fulltext
            end
        end

        puts "Processed #{total_count} lessons\n\nGenerating indices ...\n"

        # generate the indices in one swoop
        add_index :lesson_fulltext, :content_vector, using: :gin
        add_index :lesson_fulltext, :comments_vector, using: :gin
        execute "CREATE INDEX lesson_fulltext_content_text ON lesson_fulltext USING GIN (content_text gin_trgm_ops)"
        execute "CREATE INDEX lesson_fulltext_comments_text ON lesson_fulltext USING GIN (comments_text gin_trgm_ops)"


    end

    def down
        remove_index :lesson_fulltext, :content_vector
        remove_index :lesson_fulltext, :comments_vector
        execute "DROP INDEX lesson_fulltext_content_text"
        execute "DROP INDEX lesson_fulltext_comments_text"
        execute "TRUNCATE lesson_fulltext"
    end

end
