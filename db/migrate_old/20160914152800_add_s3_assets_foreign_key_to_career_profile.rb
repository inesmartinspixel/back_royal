class AddS3AssetsForeignKeyToCareerProfile < ActiveRecord::Migration[4.2]
    def change
        add_column :career_profiles, :linkedin_pdf_id, :uuid
        add_foreign_key :career_profiles, :s3_assets, column: :linkedin_pdf_id
        add_column :career_profiles_versions, :linkedin_pdf_id, :uuid
    end
end
