class AddIndexToInviteToReapply < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def change
        add_index(:cohort_applications, :should_invite_to_reapply, algorithm: :concurrently)
    end
end
