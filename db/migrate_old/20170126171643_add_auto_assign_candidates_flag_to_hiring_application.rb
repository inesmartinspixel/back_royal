class AddAutoAssignCandidatesFlagToHiringApplication < ActiveRecord::Migration[5.0]
    def change
        add_column :hiring_applications, :auto_assign_candidates, :boolean, :default => true
        add_column :hiring_applications_versions, :auto_assign_candidates, :boolean
    end
end
