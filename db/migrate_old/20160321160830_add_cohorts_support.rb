class AddCohortsSupport < ActiveRecord::Migration[4.2]

    def up

        # cohorts

        create_table    :cohorts, id: :uuid do |t|
            t.timestamps                            null: false
            t.datetime      :start_date,            null: false
            t.json          :name_locales,          null: false,    default: {}
            t.text          :status,                null: false,    default: 'closed'
            t.uuid          :playlist_pack_ids,     null: false,    default: '{}',        array: true
            t.integer       :required_electives,    null: false,    default: 0
            t.text          :notes,                 null: false,    default: ''
        end

        create_versions_table_and_trigger('cohorts')

        # cohort_applications

        create_table :cohort_applications, id: :uuid do |t|
            t.timestamps                    null: false
            t.datetime      :applied_at,    null: false
            t.uuid          :cohort_id,     null: false
            t.uuid          :user_id,       null: false
            t.text          :status,        null: false,    default: 'pending'
        end

        add_index(:cohort_applications, [:cohort_id, :user_id], :unique => false, :name => :cohort_applications_cohorts_users)
        add_foreign_key "cohort_applications", "cohorts", column: "cohort_id", primary_key: "id"
        add_foreign_key "cohort_applications", "users", column: "user_id", primary_key: "id"

        create_versions_table_and_trigger('cohort_applications')

        # cohorts_users

        create_table :cohorts_users, id: :uuid do |t|
            t.uuid      :cohort_id,                 null: false
            t.uuid      :user_id,                   null: false
            t.uuid      :cohort_application_id,     null: false
            t.text      :status,                    null: false,    default: 'active'
        end

        add_index(:cohorts_users, [:cohort_id, :user_id], :unique => true, :name => :unique_on_cohorts_users)
        add_foreign_key "cohorts_users", "cohorts", column: "cohort_id", primary_key: "id"
        add_foreign_key "cohorts_users", "users", column: "user_id", primary_key: "id"
        add_foreign_key "cohorts_users", "cohort_applications", column: "cohort_application_id", primary_key: "id"

        create_versions_table_and_trigger('cohorts_users')

        # access_groups_cohorts

        create_table :access_groups_cohorts, id: false do |t|
            t.uuid      :cohort_id,         null: false
            t.uuid      :access_group_id,   null: false
        end

        add_index(:access_groups_cohorts, [:access_group_id, :cohort_id], :unique => true, :name => :unique_on_access_groups_cohorts)
        add_foreign_key "access_groups_cohorts", "access_groups", column: "access_group_id", primary_key: "id"
        add_foreign_key "access_groups_cohorts", "cohorts", column: "cohort_id", primary_key: "id"

        create_versions_table_and_trigger('access_groups_cohorts')
    end

    def down
        drop_all_for_table('access_groups_cohorts')
        drop_all_for_table('cohorts_users')
        drop_all_for_table('cohort_applications')
        drop_all_for_table('cohorts')
    end


    ## TABLE BOILERPLATE HELPER METHODS


    def drop_all_for_table(table_name)
        drop_versions_table_and_trigger table_name
        drop_table table_name
    end

    def create_versions_table_and_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.connection.schema_cache.clear!
        klass.reset_column_information

        # create the version table, including all columns from the source table
        # plus a few extras
        create_table "#{table_name}_versions", id: false, force: :cascade do |t|
            t.uuid     "version_id", null: false, default: "uuid_generate_v4()"
            t.string   "operation", limit: 1, null: false
            t.datetime "version_created_at", null: false

            puts "*** #{table_name}_versions"

            klass.column_names.each do |column_name|
                type = klass.columns_hash

                column_config = klass.columns_hash[column_name]
                meth = column_config.type.to_sym
                options = {}
                options[:limit] = column_config.limit unless column_config.limit.nil?
                options[:null] = false if column_config.null == false
                options[:array] = column_config.array
                # ignore defaults, since they should not be used in the audit table

                puts "t.#{meth}, #{column_name}, #{options.inspect}"
                t.send(meth, column_name, options)
            end
        end

        # for some reason the primary flag on create_table was not working
        execute "ALTER TABLE #{table_name}_versions ADD PRIMARY KEY (version_id);"

        # create indexes
        if klass.column_names.include?('id') && klass.column_names.include?('updated_at')
            add_index "#{table_name}_versions", [:id, :updated_at]
        elsif klass.column_names.include?('id')
            add_index "#{table_name}_versions", :id
        end

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "
        execute(trigger_sql)
    end

    def drop_versions_table_and_trigger(table_name)
        drop_table "#{table_name}_versions"
        execute "DROP TRIGGER #{table_name}_versions ON #{table_name}"
    end

    def create_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end

end