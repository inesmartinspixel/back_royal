class DropReportFrameEvents < ActiveRecord::Migration[4.2]

    def up
        execute "DROP TABLE report_frame_events"
    end

    def down

        # pg_dump back_royal_development -st report_frame_events
        execute "CREATE TABLE report_frame_events (
                    id uuid DEFAULT uuid_generate_v4() NOT NULL,
                    source_event_id uuid,
                    stream_id uuid,
                    lesson_id uuid,
                    frame_id uuid,
                    user_id uuid,
                    stream_title character varying(255),
                    lesson_title character varying(255),
                    frame_index integer,
                    frame_text text,
                    event_type character varying(255),
                    editor_template character varying(255),
                    lesson_play_id character varying(255),
                    created_at timestamp without time zone,
                    updated_at timestamp without time zone,
                    duration_active double precision,
                    duration_idle double precision,
                    duration_total double precision,
                    estimated_time timestamp without time zone,
                    client_reported_time timestamp without time zone,
                    frame_interaction_duration double precision,
                    lesson_complete boolean
                )"
        execute "ALTER TABLE ONLY report_frame_events ADD CONSTRAINT report_frame_events_pkey PRIMARY KEY (id)"
        execute "CREATE INDEX index_report_frame_events_on_created_at ON report_frame_events USING btree (created_at)"
        execute "CREATE INDEX index_report_frame_events_on_estimated_time ON report_frame_events USING btree (estimated_time)"
        execute "CREATE UNIQUE INDEX index_report_frame_events_on_source_event_id ON report_frame_events USING btree (source_event_id)"
        execute "CREATE INDEX index_report_frame_events_on_user_id ON report_frame_events USING btree (user_id)"
        execute "GRANT SELECT ON TABLE report_frame_events TO etl"

    end
end