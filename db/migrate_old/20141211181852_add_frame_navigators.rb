class AddFrameNavigators < ActiveRecord::Migration[4.2]

    disable_ddl_transaction!

    class Migrator < ComponentizedRubyObjectMigratorBase

        def should_process_frame?(frame, opts)
            return frame.is_a?(Lesson::Content::FrameList::Frame::Componentized) && frame.frame_navigator.nil?
        end

        def process_frame(frame, opts)
            frame.get_or_create_frame_navigator
        end

    end

    def up
        migrator = Migrator.new
        migrator.change
    end
end
