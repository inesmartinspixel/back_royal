class SetDefaultDisableExamLockingValue < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        # batch update cohort applications outside of a transaction to prevent
        # large table locks
        CohortApplication.select('id')
            .where(disable_exam_locking: nil)
            .find_in_batches do |batch|

                CohortApplication.where(disable_exam_locking: nil)
                            .where(id: batch.map(&:id))
                            .update_all(disable_exam_locking: false)
        end
    end
end