class AddParticipationScores < ActiveRecord::Migration[5.0]
    def up

        create_table :user_participation_scores, id: :uuid do |t|
            t.uuid          :user_id,        null: false
            t.float         :score,          null: false
        end

        execute "
            insert into user_participation_scores (user_id, score)
            values
            #{scores}
        "

        add_index :user_participation_scores, :user_id, :unique => true

        # update etl.cohort_user_progress_records
        ViewHelpers.set_versions(self, 20170227200154)

    end

    def down

        ViewHelpers.set_versions(self, 20170227200154 - 1)

        drop_table :user_participation_scores
    end

    def scores
        csv = %q(
            ('ec3a223b-7a2c-416e-8115-79436452410a',1.0),
            ('60fc7673-3149-4c80-b0a8-c7c5b5ef7f3e',0.9),
            ('dd8722c0-4aff-4145-9a73-f380b72eef2d',0.9),
            ('cbee858c-f676-439d-b490-3d0ddf76495c',0.9),
            ('62733a60-5d19-40fb-a60d-d4ee39d08f04',0.9),
            ('1c831ee0-d487-45ff-9b85-7d2948f3a976',0.9),
            ('d66edd78-4a99-4e08-9663-a2bf63b2dde3',0.8),
            ('65adfc47-f02e-4657-ad50-7fadfec5f6c2',0.7),
            ('90313601-3b1d-44f6-97df-a7d72f487964',0.7),
            ('063a8449-861b-4f14-846a-370cb479be39',0.6),
            ('bd43d5cf-2b14-4e26-b306-ab0f250d8fba',0.5),
            ('4324311e-f12b-43da-b7cb-c3ad81c2e0b2',0.4),
            ('0727db91-3951-4906-ae93-b15cbd126b85',0.4),
            ('8799ce92-0123-4d20-95b9-56c93949c775',0.4),
            ('11c8f39c-462c-46ca-a8a7-ac0bb4cc312c',0.3),
            ('c7d8bef0-6150-4f06-b4a2-a03f36cad4c7',0.3),
            ('6b8e5972-7b19-44c9-8bd7-cf5287308393',0.3),
            ('801f7160-5a81-4ee9-9db6-d2f9ced70ceb',0.3),
            ('4ce9bd6b-695e-4548-85f4-0d91185be2cb',0.3),
            ('b720925c-71d2-4266-d9c2-6073c674a29e',0.3),
            ('a467cc9e-34c6-485b-8c13-660741a317a4',0.3),
            ('fc3db8eb-2f76-4cab-9c73-4e33f00a2bf7',0.3),
            ('cbf1f910-7235-4099-a49d-4a006432f4de',0.3),
            ('fe917047-a064-4b4b-95be-721e489e4a9c',0.2),
            ('73f4069d-9ab5-4be8-a1bd-2a5fbc499b5a',0.2),
            ('b37f8ee4-4b2e-4b3f-88fb-b31166688257',0.2),
            ('3a0cd1e3-f435-48d4-9647-0aee9ae39905',0.2),
            ('75fe4dad-64e3-4f83-8e1c-7ca97da55fcb',0.2),
            ('9ac415a0-1257-428d-b39d-51a99c3ea913',0.2),
            ('afdd65cd-f71f-4abb-9a2e-8383b23a5d6b',0.2),
            ('c88982a1-c3f8-4753-892a-5fed02235d2a',0.2),
            ('032cc632-b4a9-431e-ac85-e573cf79eb78',0.2),
            ('1b484044-9905-42fc-8071-1540a4808178',0.2),
            ('98d0ac90-2891-42b8-96ce-193879d5128f',0.2),
            ('715757fc-17f5-4a48-86f9-648d472b7d17',0.2),
            ('d51f9cdc-a73d-44a8-a687-a3d654bd6ab0',0.2),
            ('b7c36d00-8f24-4e25-8a72-8500eee19527',0.2),
            ('a4ddbb3b-ff53-460c-bd48-102a287939f3',0.2),
            ('a132929f-f8fe-4113-98c1-4f9e30336b70',0.2),
            ('8d94a2b7-abbd-4f6b-9454-e7fd80d169e1',0.2),
            ('9fc10326-5e2f-45ec-99e7-612d5764687f',0.2),
            ('991ade87-304f-4753-9867-9a5888a8b73a',0.2),
            ('7a994124-3284-4c22-be8a-fc87b3476c49',0.2),
            ('7c7bafcb-88d2-42a9-974a-16310c3799b4',0.1),
            ('011f007a-a897-44dc-bfa9-1230658b7c5d',0.1),
            ('a66d3967-45f9-4b21-82b0-dee32f017889',0.1),
            ('e73ac926-4926-42b7-89d7-0eed9c9a4821',0.1),
            ('3f100f5f-3f9a-48cb-804a-cdd0a941f9b1',0.1),
            ('5a296db6-0e5c-433e-bead-96c6cbd0daeb',0.1),
            ('7178a647-5549-480b-9a1d-fb54accbd4dc',0.1),
            ('77332883-2e8b-4f7f-b0e6-af447b444ae1',0.1),
            ('957fab7f-2c35-421d-b995-aff628f1f9db',0.1),
            ('1b1fe309-bb8e-4a79-8fb8-7ec183e086ce',0.1),
            ('7f9d189a-9037-4a83-8697-5bdaaabb9d82',0.1),
            ('f0d170f1-d2fa-4bf5-9236-5c60e63f3d16',0.1),
            ('06289fac-0819-4c06-a379-c3448ae61189',0.1),
            ('b5c8ebab-28ed-4af8-836b-2b784a78ca69',0.1),
            ('7c8c20d6-0ca4-4084-9971-87923d01f67d',0.1),
            ('785e760c-e45b-47c2-9881-dfb039d5577f',0.1),
            ('27f5a3b1-859e-4329-97e2-3f0c3a6e6c53',0.1),
            ('a5637b64-07f6-4f75-a723-0063c4bee675',0.1),
            ('efe2f819-85cd-42fb-b89a-5c0d7100beda',0.1),
            ('653bfee2-632e-4975-8d8c-74c0b7846242',0.1),
            ('d31a548c-c925-4eb8-9511-aa7a8e3b2d96',0.1),
            ('66963aad-2642-4acf-980f-e20df5c00e33',0.1),
            ('f78cd06c-90e1-4b9c-9925-c6b66536caaa',0.1),
            ('2fe0e65b-8c92-46b8-a0a9-5be4667248ab',0.1),
            ('1462ac03-a3cd-401c-b6f9-203d90e9e0a5',0.1),
            ('d1943537-2c44-4c87-9f7e-e47adb5b1961',0.1),
            ('872f9ddd-7036-45c0-bf63-9868e60fcf98',0.1),
            ('0f4c8b0b-116b-4962-8594-58c94f4fdef8',0.1),
            ('cd897364-aa3b-433c-bdf0-a6e70010a203',0.1),
            ('98d561bb-43b3-41d6-b09e-1ad22de4ae65',0.1),
            ('6acf89f7-3e05-4939-b1c9-f1cf7c956e75',0.1),
            ('8ded8474-e6e9-40e3-8ce8-6f68779a711e',0.1),
            ('06a53dfe-c2c7-492b-aa9c-9743c6f5878f',0.1),
            ('d0f80f25-522d-464c-a7ba-d685a72408c3',0.1),
            ('3b4259a0-451b-42df-a144-40cba8571057',0.1),
            ('0df1ffbe-fc37-4e77-b90c-a086cf16228a',0.1),
            ('841a2ba6-4d7b-4e1a-af13-1e7db372afab',0.1),
            ('ccef7fea-c5dd-4a65-8cb5-925d9e341670',0.1),
            ('fab689e7-ba62-4443-ad21-c646b73458be',0.1),
            ('29b11d04-36f1-46c2-834a-bb3b03936613',0.1),
            ('58984bd9-df39-4f8e-9cef-e1e8fff6766d',0.1),
            ('d0eef2b6-9b30-4d3e-a0c0-fbd92d292961',0.1),
            ('e6b1e491-032e-4763-b363-f43ad9ded428',0.1),
            ('65912f98-c19e-4949-9b38-3f7466245244',0.1),
            ('caa5e729-8d90-4c58-9f34-4a7c9c2f62b8',0.1),
            ('3fee3db0-9259-40c0-b58a-6013ebd8357d',0.1),
            ('a0a6bbbc-ad9b-4737-b9d2-a0e5183873a8',0.1),
            ('ada87d75-4a24-4076-93aa-94b8187cb266',0.1),
            ('371ac21b-d3b0-4740-b71f-230756af9f04',0.1),
            ('0f679164-25ec-44a3-9a00-2dab4ef4e12c',0.1),
            ('cc8f7d89-61d5-4bfb-ba26-c3ff61002c87',0.1),
            ('ad45a999-a840-4a26-9c03-f4c3a8710f4d',0.1),
            ('26e9f79f-dd25-4caa-b5c6-df0427ac0c7d',0.1),
            ('d6f76200-99f5-4ef1-a7b7-e834d322736a',0.1),
            ('32340e28-3ef8-4f4e-90c1-6d805df93016',0.1),
            ('dde94307-6316-4c6b-be87-65941f704d6c',0.1),
            ('338b7550-116c-4d56-8320-629331166a89',0.1),
            ('8b5999c0-bd5e-42bc-8658-15740bbdf847',0.1),
            ('e200396a-ca68-4b6f-95c1-a519899a14c7',0.1),
            ('136da4d5-2784-4544-813a-b65a1c9343fd',0.1),
            ('d8a6709f-6fdb-4b07-acf2-e786fd62b55c',0.1),
            ('b8ab9054-a26c-4b78-997a-f8708a7d4409',0.1),
            ('e8caf0ad-8988-44d0-9c36-f1eac8358408',0.1),
            ('f7778f99-416d-4502-b1ec-fdb03178a4eb',0.1),
            ('50f5b504-7a53-4a10-be9a-718a7a53f365',0.1)
        )
    end


end
