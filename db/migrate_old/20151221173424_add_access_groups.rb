class AddAccessGroups < ActiveRecord::Migration[4.2]

    def up

        create_table    :access_groups, id: :uuid do |t|
            t.text      :name, null: false
            t.timestamps
        end
        add_index(:access_groups, :name)

        # Create version table and triggers
        create_versions_table_and_trigger('access_groups')

        ["users", "institutions", "playlists", "lesson_streams"].each do |table_name|

            item_name = table_name.singularize
            join_table = "access_groups_#{table_name}"

            # Create primary table
            create_table    join_table, id: :uuid do |t|
                t.uuid      "access_group_id"
                t.uuid      "#{item_name}_id"
            end

            add_foreign_key join_table, "access_groups", column: "access_group_id", primary_key: "id"
            add_foreign_key join_table, "#{table_name}", column: "#{item_name}_id", primary_key: "id"

            add_index(join_table, [ "access_group_id", "#{item_name}_id" ], unique: true, name: "index_access_groups_and_#{table_name}")

            # Create version table and triggers
            create_versions_table_and_trigger(join_table)

        end

        add_column :lessons, :tag, :text
        add_column :lessons_versions, :tag, :text

    end


    def down

        remove_column :lessons_versions, :tag
        remove_column :lessons, :tag

        ["users", "institutions", "playlists", "lesson_streams"].each do |table_name|

            join_table = "access_groups_#{table_name}"

            # Drop version table and triggers
            drop_versions_table_and_trigger(join_table)

            # Drop primary table
            drop_table join_table

        end

        drop_versions_table_and_trigger('access_groups')
        drop_table :access_groups

    end


    ################################
    ## Helper Methods
    ################################



    def create_versions_table_and_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.connection.schema_cache.clear!
        klass.reset_column_information

        # create the version table, including all columns from the source table
        # plus a few extras
        create_table "#{table_name}_versions", id: false, force: :cascade do |t|
            t.uuid     "version_id", null: false, default: "uuid_generate_v4()"
            t.string   "operation", limit: 1, null: false
            t.datetime "version_created_at", null: false

            puts "*** #{table_name}_versions"

            klass.column_names.each do |column_name|
                type = klass.columns_hash

                column_config = klass.columns_hash[column_name]
                meth = column_config.type.to_sym
                options = {}
                options[:limit] = column_config.limit unless column_config.limit.nil?
                options[:null] = false if column_config.null == false
                options[:array] = column_config.array
                # ignore defaults, since they should not be used in the audit table

                puts "t.#{meth}, #{column_name}, #{options.inspect}"
                t.send(meth, column_name, options)
            end
        end

        # for some reason the primary flag on create_table was not working
        execute "ALTER TABLE #{table_name}_versions ADD PRIMARY KEY (version_id);"

        # create indexes
        if klass.column_names.include?('id') && klass.column_names.include?('updated_at')
            add_index "#{table_name}_versions", [:id, :updated_at]
        elsif klass.column_names.include?('id')
            add_index "#{table_name}_versions", :id
        end

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "
        execute(trigger_sql)
    end

    def drop_versions_table_and_trigger(table_name)
        drop_table "#{table_name}_versions"
        execute "DROP TRIGGER #{table_name}_versions ON #{table_name}"
    end

    def create_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end

    def temporarily_delete_user_views(&block)
        # this used to do something, but no more
        yield
    end


end
