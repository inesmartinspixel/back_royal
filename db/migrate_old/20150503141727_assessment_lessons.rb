class AssessmentLessons < ActiveRecord::Migration[4.2]

    def change
        add_column :lessons, :assessment, :boolean, :default => false
        add_column :lessons_versions, :assessment, :boolean
    end

end
