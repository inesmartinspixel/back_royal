class EnrollmentExpulsionChanges < ActiveRecord::Migration[5.1]
    def up

        add_column :cohorts, :enrollment_deadline_days_offset, :integer, :null => false, :default => 0
        add_column :cohorts_versions, :enrollment_deadline_days_offset, :integer
        add_column :curriculum_templates, :enrollment_deadline_days_offset, :integer
        add_column :curriculum_templates_versions, :enrollment_deadline_days_offset, :integer

        execute(%Q~
            update cohorts set enrollment_deadline_days_offset =
                case when enrollment_expulsion_days_offset is null then 0 else enrollment_expulsion_days_offset + 1 end;

            update cohorts_versions set enrollment_deadline_days_offset =
                case when enrollment_expulsion_days_offset is null then 0 else enrollment_expulsion_days_offset + 1 end;

            update curriculum_templates set enrollment_deadline_days_offset =
                case when enrollment_expulsion_days_offset is null then 0 else enrollment_expulsion_days_offset + 1 end;

            update curriculum_templates_versions set enrollment_deadline_days_offset =
                case when enrollment_expulsion_days_offset is null then 0 else enrollment_expulsion_days_offset + 1 end;

            delete from delayed_jobs where queue='cohort__enrollment_warning';
            delete from delayed_jobs where queue='cohort__enrollment_expulsion';

        ~)

        ViewHelpers.migrate(self, 20170908153230)

    end

    def down

        execute(%Q~
            update cohorts set enrollment_expulsion_days_offset =
                case when enrollment_deadline_days_offset > 0 then enrollment_deadline_days_offset - 1 else null end;
            update cohorts_versions set enrollment_expulsion_days_offset =
                case when enrollment_deadline_days_offset > 0 then enrollment_deadline_days_offset - 1 else null end;
            update curriculum_templates set enrollment_expulsion_days_offset =
                case when enrollment_deadline_days_offset > 0 then enrollment_deadline_days_offset - 1 else null end;
            update curriculum_templates_versions set enrollment_expulsion_days_offset =
                case when enrollment_deadline_days_offset > 0 then enrollment_deadline_days_offset - 1 else null end;
        ~)
        # note: we are not recreating the 2 enrollment jobs in the down. IF you need then you can republish cohorts (see edit_and_republish)

        ViewHelpers.rollback(self, 20170908153230)

        remove_column :cohorts, :enrollment_deadline_days_offset, :integer
        remove_column :cohorts_versions, :enrollment_deadline_days_offset, :integer
        remove_column :curriculum_templates, :enrollment_deadline_days_offset, :integer
        remove_column :curriculum_templates_versions, :enrollment_deadline_days_offset, :integer
    end
end
