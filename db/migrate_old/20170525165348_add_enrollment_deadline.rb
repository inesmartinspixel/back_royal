class AddEnrollmentDeadline < ActiveRecord::Migration[5.0]
    def up
        ViewHelpers.migrate(self, 20170525165348)
    end

    def down
        ViewHelpers.rollback(self, 20170525165348)
    end
end
