# NOTE: This is running fine from a prod dump taken at 10a MST 7/14/2016
class NormalizePhoneNumbers < ActiveRecord::Migration[4.2]

    def up

        # pre-approved list of users to destroy
        users_to_destroy = [
            '5c1d8047-690b-49d3-ba9d-1e30c48dfb5a', # shrmtest@pedago.com                    | 1111111111   | email
            '639e681b-d9bf-4fdf-afac-656ae28810ff', # shrmtestmobile@pedago.com              | 1111111111   | email
            '08e50ca8-68da-4faa-a1b7-c204c1f87515', # scott+test3@pedago.com                 | 1234567890   | email
            '7a0b6bb7-3767-494b-bb57-9e62f0f54baa', # scott+test4@pedago.com                 | 1234567890   | email
            '1de76c72-583e-4f11-948e-376e137c78be', # d@d.com                                | 5555555555   | email
            'bbfe3de6-5724-4eec-bbf9-3365890f7bb9', # scott+testing@pedago.com               | 5599708578   | email
            '7bf8d530-c680-4829-8c29-390af6afa410', # karina+testing@pedago.com              | 123456       | email
            '0eda6da2-8732-42cd-9230-e0568bbc3429', # scott+test5@pedago.com                 | 1234567890   | email
            'f3a31feb-21e9-4e4c-89de-9294db40eec8', # [NULL] (alexie test)                   | 1123456789   | phone_no_password
        ]

        puts "Destroying known test accounts ..."

        User.where('id in (?)', users_to_destroy).destroy_all


        # pre-approved list of users to modify phone numbers for
        users_to_modify = {
            'c92db8e3-bbb5-4d77-9ea3-3e3d1591d546' => '(901) 495-6500',  # guy.carter@autozone.com                | 30812449     | email
            '0ca8c00e-7129-4d6c-9dd4-bc89ebc61232' => '+264618786878',   # [NULL]                                 | 618786878    | nokutula Masilo    | phone_no_password
            'e1179abe-13a4-4a65-bd3d-10da74b52d15' => '+810368358981',   # ishikawa_ruchia@r.recruit.co.jp        | 0368358981   | Lucia              | email
            '9ae5c897-2d38-49de-a679-4f31ab29bf83' => '+270822611220',   # [NULL]                                 | 0822611220   | Taryn              | phone_no_password
            '44359df9-eb06-4424-ab15-9c0028bed405' => '+270795346095',   # [NULL]                                 | 0795346095   | sibusiso E Mchunu  | phone_no_password`
            'ebeb0f56-5061-453e-9b59-b916282bb1d3' => '+270749933362',   # [NULL]                                 | 0749933362   | Sheldon Reimers    | phone_no_password
            '39e361db-5fec-4914-a936-04a4a32aeadb' => '+270763471220',   # [NULL]                                 | 0763471220   | Innocent Ukhurebor | phone_no_password
            '5ba35720-08c1-4a3c-8037-52b119f03158' => '+270611190283',   # [NULL]                                 | 0611190283   | Thabani            | phone_no_password
            '4c00348b-aaf0-4ed6-b123-49ed1253698b' => '+270728428522',   # [NULL]                                 | 0728428522   | sifiso             | phone_no_password
        }


        # Process all remaining users and handle known resets
        User.where('phone IS NOT NULL').each do |user|

            puts "Processing user #{user.name} <#{user.email}> :: #{user.phone} ..."

            if GlobalPhone.validate(user.phone)
                user.phone = user.phone # should implicitly convert
            elsif users_to_modify[user.id]
                user.phone = users_to_modify[user.id]
            else
                raise "Unknown invalid user: #{user.id}"
            end
            user.save!
        end
    end

    def down
        # no-op
    end
end
