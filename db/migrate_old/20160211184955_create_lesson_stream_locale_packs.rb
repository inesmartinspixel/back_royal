class CreateLessonStreamLocalePacks < ActiveRecord::Migration[4.2]
    def up

        create_table :lesson_stream_locale_packs, id: :uuid do |t|
            t.timestamps
        end
        create_versions_table_and_trigger('lesson_stream_locale_packs')

        add_column :lesson_streams,            :locale_pack_id, :uuid
        add_column :lesson_streams,            :locale, :text # we set this to null and add a default in the next migration, after current records are updated
        add_column :lesson_streams_versions,   :locale_pack_id, :uuid
        add_column :lesson_streams_versions,   :locale, :text
        add_index  :lesson_streams, [:locale_pack_id, :locale], :unique => true

        add_foreign_key :lesson_streams, :lesson_stream_locale_packs, :column => :locale_pack_id

        spanish_stream_ids = AccessGroup.where(name: ['Spanish Demo', 'UNIMINUTO ACCOUNTING']).map(&:lesson_streams).flatten.map(&:id).uniq
        spanish_stream_ids << SecureRandom.uuid # this is a hack, but it handles the case where spanish_stream_ids is empty
        id_string = spanish_stream_ids.map { |id| "'#{id}'" }.join(',')

        # Could not get this User insert to work using ActiveRecord.
        # Tried clearing schema_cache, reset_column_information, and eager_load!'ing to no avail
        user_id = SecureRandom.uuid
        encrypted_password = SecureRandom.urlsafe_base64(nil, false)
        execute "INSERT INTO users
                    (id, uid, first_name, email, encrypted_password, provider, created_at, updated_at)
                 VALUES ('#{user_id}', '#{user_id}', 'Stream Locale Migrator', 'stream-locale-migrator@pedago.com', '#{encrypted_password}', 'email', current_timestamp, current_timestamp)"
        execute "INSERT INTO users_roles (user_id, role_id) VALUES ('#{user_id}', (SELECT id FROM roles WHERE name='admin'))"


        Lesson::Stream.connection.execute("update lesson_streams_versions set locale = 'en' where id not in (#{id_string})")
        Lesson::Stream.connection.execute("update lesson_streams_versions set locale = 'es', last_editor_id='#{user_id}', was_published=false  where id in (#{id_string})")

        streams = Lesson::Stream.includes(:access_groups).all
        superviewer_group = AccessGroup.where(name: 'SUPERVIEWER').first
        streams.includes(:access_groups).each_with_index do |stream, i|

            stream.locale = spanish_stream_ids.include?(stream.id) ? 'es' : 'en'

            # driveby backfill of lesson_hashes and removal of test_out_lesson_id
            stream.chapters = stream.chapters.map { |chapter|
                chapter['lesson_hashes'] ||= chapter['lesson_ids'].map { |lesson_id|
                    {
                        'lesson_id' => lesson_id,
                        'coming_soon' => false
                    }
                }
                chapter.delete('test_out_lesson_id')
                chapter
            }

            # driveby backfill of superviewer group
            stream.access_groups << superviewer_group unless stream.access_groups.include?(superviewer_group)
            stream.save!

            # Lesson::Stream.update_from_hash!(user, attrs)
            puts "Updated #{i+1} if #{streams.size} streams" if i % 10 == 9
        end

    end

    def down
        remove_column :lesson_streams, :locale_pack_id
        remove_column :lesson_streams, :locale
        remove_column :lesson_streams_versions, :locale_pack_id
        remove_column :lesson_streams_versions, :locale
        drop_table :lesson_stream_locale_packs
    end

    def create_versions_table_and_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.connection.schema_cache.clear!
        klass.reset_column_information

        # create the version table, including all columns from the source table
        # plus a few extras
        create_table "#{table_name}_versions", id: false, force: :cascade do |t|
            t.uuid     "version_id", null: false, default: "uuid_generate_v4()"
            t.string   "operation", limit: 1, null: false
            t.datetime "version_created_at", null: false

            puts "*** #{table_name}_versions"

            klass.column_names.each do |column_name|
                type = klass.columns_hash

                column_config = klass.columns_hash[column_name]
                meth = column_config.type.to_sym
                options = {}
                options[:limit] = column_config.limit unless column_config.limit.nil?
                options[:null] = false if column_config.null == false
                options[:array] = column_config.array
                # ignore defaults, since they should not be used in the audit table

                puts "t.#{meth}, #{column_name}, #{options.inspect}"
                t.send(meth, column_name, options)
            end
        end

        # for some reason the primary flag on create_table was not working
        execute "ALTER TABLE #{table_name}_versions ADD PRIMARY KEY (version_id);"

        # create indexes
        if klass.column_names.include?('id') && klass.column_names.include?('updated_at')
            add_index "#{table_name}_versions", [:id, :updated_at]
        elsif klass.column_names.include?('id')
            add_index "#{table_name}_versions", :id
        end

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "
        execute(trigger_sql)
    end

    def drop_versions_table_and_trigger(table_name)
        drop_table "#{table_name}_versions"
        execute "DROP TRIGGER #{table_name}_versions ON #{table_name}"
    end

    def create_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end
end
