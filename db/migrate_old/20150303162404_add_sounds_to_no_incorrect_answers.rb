class AddSoundsToNoIncorrectAnswers < ActiveRecord::Migration[4.2]
    def up

        VersionMigrator.new(Lesson).where("content_json ILIKE '%\"no_incorrect_answers\":true%'").each do |lesson|
            lesson.content.frames.
                map { |f| f.components }.flatten.compact.
                select { |c| c.component_type == 'ComponentizedFrame.MultipleChoiceChallenge' }.
                select { |c| c.no_incorrect_answers == true }.
                each do |challenge|

                    challenge['behaviors']['PlayScalingSoundOnSelected'] = {}
                end
        end

    end
end
