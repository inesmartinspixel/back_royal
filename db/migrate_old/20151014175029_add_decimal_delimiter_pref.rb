class AddDecimalDelimiterPref < ActiveRecord::Migration[4.2]
    def change
        add_column :users, :pref_decimal_delim, :string, :null => false, :default => '.'
        add_column :users_versions, :pref_decimal_delim, :string
    end
end