class CreateNewProgressTypes < ActiveRecord::Migration[4.2]
    def up
        execute "CREATE TYPE lesson_progress_json_v4 AS (
            locale_pack_id uuid,
            frame_bookmark_id uuid,
            frame_history json,
            completed_frames json,
            challenge_scores json,
            frame_durations json,
            started_at int,
            completed_at int,
            complete boolean,
            last_progress_at int,
            id uuid
        )"
    end

    def down
        execute "DROP TYPE lesson_progress_json_v4"
    end
end
