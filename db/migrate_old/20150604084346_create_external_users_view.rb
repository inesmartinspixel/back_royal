class CreateExternalUsersView < ActiveRecord::Migration[4.2]
    def up
        ViewHelpers.migrate(self, 20150604084346)
    end

    def down
        ViewHelpers.rollback(self, 20150604084346)
    end

end
