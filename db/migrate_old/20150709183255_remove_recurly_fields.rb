class RemoveRecurlyFields < ActiveRecord::Migration[4.2]

    def up
        remove_column :subscriptions, :recurly_uuid
        remove_column :subscriptions, :recurly_plan_code
    end

    def down
        add_column :subscriptions, :recurly_uuid, :string
        add_column :subscriptions, :recurly_plan_code, :string
    end

end
