class CleanupAfterVersionsMigration < ActiveRecord::Migration[4.2]

    def up
        drop_table :versions
        remove_column :content_publishers, :published_version_object
        remove_column :content_publishers, :published_version_index
        remove_column :content_publishers, :lesson_stream_id
        remove_column :content_publishers, :lesson_id
        remove_column :content_publishers, :version_id

        remove_column :lessons, :version_id
        remove_column :lesson_streams, :version_id
        update_triggers
    end

    def update_triggers
        tables_to_update = ['lessons', 'lesson_streams']
        tables_to_update.each do |table_name|

            recreate_trigger_sql = "
                    CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                        BEGIN
                            --
                            -- Create a row in audit to reflect the operation performed on the table,
                            -- make use of the special variable TG_OP to work out the operation.
                            --
                            IF (TG_OP = 'DELETE') THEN
                                INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                                RETURN OLD;
                            ELSIF (TG_OP = 'UPDATE') THEN
                                INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                                RETURN NEW;
                            ELSIF (TG_OP = 'INSERT') THEN
                                INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                                RETURN NEW;
                            END IF;
                            RETURN NULL; -- result is ignored since this is an AFTER trigger
                        END;
                    $#{table_name}_version$ LANGUAGE plpgsql;

                    DROP TRIGGER #{table_name}_versions on #{table_name};
                    CREATE TRIGGER #{table_name}_versions
                    AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
                    FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();"
             execute(recreate_trigger_sql)
         end
    end
end
