class AddIndexToPublishedVersionObject < ActiveRecord::Migration[4.2]
    def up
        # add_index :content_publishers, :published_version_object, where: "published_version_object is not null"
        execute <<-SQL
            CREATE INDEX content_publishers_published_version_object_not_null
            ON content_publishers(id) where (published_version_object IS NOT NULL);
        SQL
    end

    def down
        execute <<-SQL
            DROP INDEX content_publishers_published_version_object_not_null;
        SQL
    end
end
