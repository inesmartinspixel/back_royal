class AddTagToPlaylists < ActiveRecord::Migration[4.2]
    def change

        add_column :playlists, :tag, :text
        add_column :playlists_versions, :tag, :text

    end
end
