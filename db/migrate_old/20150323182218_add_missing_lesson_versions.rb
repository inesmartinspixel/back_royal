class AddMissingLessonVersions < ActiveRecord::Migration[4.2]
    def up
        # db/migrate/20150220174530_backfill_seo_fields.rb
        # turned off the lessons_versions trigger and then saved
        # lessons.  So we ended up with records in the lessons table
        # that have no matching version in the lessons_versions table.
        # This migration should fix it and make sure it never happens
        # again
        delete_2_bad_lesson_versions
        delete_5_bad_stream_versions
        create_missing_lesson_versions
        add_unique_index_on_id_updated_at
        add_foreign_keys

        # We have to recreate the trigger so that we add no
        # data into the delete record int he versions table.  This
        # allows us to have a unique index on id/updated_at
        update_triggers
    end

    def down
        execute 'DROP index index_lessons_versions_on_id_and_updated_at CASCADE'
        execute 'DROP index index_lesson_streams_versions_on_id_and_updated_at CASCADE'
        add_index :lessons_versions, [:id, :updated_at]
        add_index :lesson_streams_versions, [:id, :updated_at]
    end

    def delete_2_bad_lesson_versions
        # there is one lesson version for a deleted lesson that has
        # two versions with the same updated at.  Delete them so
        # we can set up constraints
        bad_id = '7ff6167a-04c9-4842-88ef-ac9fbed79273'
        if (Lesson.where(id: bad_id).any?)
            raise "Did not expect this lesson to exist"
        end
        execute("DELETE FROM lessons_versions WHERE id='#{bad_id}'")
    end

    def delete_5_bad_stream_versions
        version_ids = [
            ['91caf2df-b250-49d5-9acd-ce67a85d3f9d', 'c0cb7c0b-6589-4800-b21b-6397e06929bf'], #Accounting (Pre‑MBA)                       | 2014-12-05 01:40:28.769778
            ['d08abf83-f6a6-4603-a8e5-a6e0bf4a55c8', '9fae04d9-c04e-48cf-bd87-1825b2f538d5'], #Basics of Photography (Under Construction) | 2014-02-12 04:25:19.417528
            ['ad2ab11f-7aa5-432a-b670-028c3a916947', '2bffabe4-5729-4840-a9ed-91a65fcc8175'], #Wilderness Survival                        | 2014-02-05 20:43:16.590515
            ['ede9ef6b-81ec-450d-9f37-4930fe526726', '952f24ce-bec6-41cc-bd12-21b046a08c10'], #Statistics (Pre‑MBA)                       | 2014-12-05 04:34:45.937506
            ['aaa5619b-6b54-418c-aa8c-664ff3992036', 'c5770b25-e522-4da3-aa9d-14fafbb17e9d'] #Basic Math: Fractions                      | 2014-03-02 22:28:41.201764
        ]

        # the only differences between these versions is (sometimes)
        # the was_published and the modified_at.  Keep the ones where
        # was_published is true or where there is a modified_at
        version_ids.each do |pair|
            versions = Lesson::Stream::Version.where(version_id: pair).reorder(:was_published, :modified_at)
            next unless versions.size == 2
            if versions.second.was_published
                version_to_delete = versions.first
            elsif versions.second.modified_at.nil? && !versions.first.modified_at.nil?
                version_to_delete = versions.second
            else
                version_to_delete = versions.first
            end
            execute "DELETE FROM lesson_streams_versions WHERE version_id = '#{version_to_delete.version_id}'"
        end
    end

    def ids_for_lessons_missing_versions
        Lesson.
            joins('LEFT OUTER JOIN
                    lessons_versions ON lessons.id = lessons_versions.id AND
                    lessons.updated_at = lessons_versions.updated_at').
            where('lessons_versions.version_id IS NULL').
            pluck('id')
    end

    def create_missing_lesson_versions
        ids = ids_for_lessons_missing_versions

        return if ids.empty?

        id_string = ids.map { |id| "'#{id}'" }.join(',')

        sql = "
            INSERT into lessons_versions (
                SELECT
                    uuid_generate_v4(),
                    'U',
                    NOW(),
                    lessons.*
                FROM lessons
                WHERE lessons.id IN (#{id_string})
            );
        "
         execute(sql)
    end

    def add_unique_index_on_id_updated_at
        remove_index :lessons_versions, :name => 'index_lessons_versions_on_id_and_updated_at'
        add_index :lessons_versions, [:id, :updated_at], :unique => true

        remove_index :lesson_streams_versions, :name => 'index_lesson_streams_versions_on_id_and_updated_at'
        add_index :lesson_streams_versions, [:id, :updated_at], :unique => true
    end

    def recreate_triggers
        recreate_trigger("lesson")
        recreate_trigger("lesson_streams")

    end

    def add_foreign_keys
        execute("ALTER TABLE lessons
                ADD FOREIGN KEY (id, updated_at)
                REFERENCES lessons_versions (id, updated_at)
                INITIALLY DEFERRED");

        execute("ALTER TABLE lesson_streams
                ADD FOREIGN KEY (id, updated_at)
                REFERENCES lesson_streams_versions (id, updated_at)
                INITIALLY DEFERRED");
    end

    def update_triggers
        klasses = [Lesson, Lesson::Stream]
        klasses.each do |klass|
            klass.connection.schema_cache.clear!
            klass.reset_column_information
            table_name = klass.table_name
            klass.column_names.each do |col_name|
                execute "ALTER TABLE #{table_name}_versions ALTER COLUMN #{col_name} DROP NOT NULL" unless col_name == 'id'
            end

            recreate_trigger_sql = "
                    CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                        BEGIN
                            --
                            -- Create a row in audit to reflect the operation performed on the table,
                            -- make use of the special variable TG_OP to work out the operation.
                            --
                            IF (TG_OP = 'DELETE') THEN
                                INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.id;
                                RETURN OLD;
                            ELSIF (TG_OP = 'UPDATE') THEN
                                INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
                                RETURN NEW;
                            ELSIF (TG_OP = 'INSERT') THEN
                                INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                                RETURN NEW;
                            END IF;
                            RETURN NULL; -- result is ignored since this is an AFTER trigger
                        END;
                    $#{table_name}_version$ LANGUAGE plpgsql;

                    DROP TRIGGER #{table_name}_versions on #{table_name};
                    CREATE TRIGGER #{table_name}_versions
                    AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
                    FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
                "
            execute(recreate_trigger_sql)
        end
    end


end
