class DropCustomerId < ActiveRecord::Migration[4.2]
    def up
        remove_column :users, :customer_id, :string
        remove_column :users_versions, :customer_id, :string

        # remove the customer_id check from the trigger
        create_trigger('users', [
            'email',
            'encrypted_password',
            'reset_password_token',
            'reset_password_sent_at',
            'first_name',
            'last_name',
            #'customer_id',
            'sign_up_code',
            'reset_password_redirect_url',
            'provider',
            'uid'
        ])

    end

    def down

        add_column :users, :customer_id, :string
        add_column :users_versions, :customer_id, :string

        create_trigger('users', [
            'email',
            'encrypted_password',
            'reset_password_token',
            'reset_password_sent_at',
            'first_name',
            'last_name',
            'customer_id',
            'sign_up_code',
            'reset_password_redirect_url',
            'provider',
            'uid'
        ])

    end

    def create_trigger(table_name, columns_to_watch)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.connection.schema_cache.clear!
        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"

        if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
        update_command = "
            IF #{if_string} THEN
            #{insert_on_update}
            END IF;
            "

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end
end
