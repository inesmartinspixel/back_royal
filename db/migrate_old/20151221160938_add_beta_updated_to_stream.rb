class AddBetaUpdatedToStream < ActiveRecord::Migration[4.2]
    def change
        add_column :lesson_streams, :beta, :boolean, default: false, null: false
        add_column :lesson_streams_versions, :beta, :boolean

        add_column :lesson_streams, :just_updated, :boolean, default: false, null: false
        add_column :lesson_streams_versions, :just_updated, :boolean

        unless self.reverting?
            execute "UPDATE lesson_streams_versions SET beta=false"
            execute "UPDATE lesson_streams_versions SET just_updated=false"
        end
    end
end
