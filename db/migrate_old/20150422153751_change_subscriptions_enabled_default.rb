class ChangeSubscriptionsEnabledDefault < ActiveRecord::Migration[4.2]
    def up
        change_column :users, :subscriptions_enabled, :boolean, :default => false
    end

    def down
        change_column :users, :subscriptions_enabled, :boolean, :default => true
    end
end
