class AddPromotedToCohorts < ActiveRecord::Migration[4.2]
    def up
        add_column :cohorts, :promoted, :boolean, default: false, null: false
        add_column :cohorts_versions, :promoted, :boolean

        current_promoted_id = ENV['PROMOTED_COHORT_ID'];

        if !current_promoted_id.blank?
            execute "UPDATE cohorts SET promoted = true WHERE id='#{current_promoted_id}'"
        end

    end

    def down
        remove_column :cohorts, :promoted
        remove_column :cohorts_versions, :promoted
    end
end
