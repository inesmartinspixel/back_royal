class AddUnlinkBlankFromAnswerToChallenges < ActiveRecord::Migration[4.2]
    def change
        frames = Lesson.all.map(&:content).map(&:frames).flatten.select { |f| f.is_a?(Lesson::Content::FrameList::Frame::Componentized) }
        components = frames.map(&:components).flatten.select
        challenges_components = components.select { |c| c.is_a?(Lesson::Content::FrameList::Frame::Componentized::Component::Challenges) }

        linked = []
        unlinked = []

        challenges_components.each do |challenges_component|
            if ['blanks_on_image', 'compose_blanks_on_image', 'fill_in_the_blanks', 'compose_blanks'].include?(challenges_component.editor_template)

            blank_labels = challenges_component.blank_labels
            challenges = challenges_component.challenges

            blank_labels.each_with_index do |blank_label, i|
                challenge = challenges[i]
                if blank_label == challenge.correct_answer_text || (challenge.correct_answer_image && blank_label == challenge.correct_answer_image.label)
                    linked << {challenge: challenge, blank_label: blank_label}
                else
                    unlinked << {challenge: challenge, blank_label: blank_label}
                end
            end
          end

        end

        unlinked.each do |entry|
            blank_label = entry[:blank_label]
            challenge = entry[:challenge]
            correct_answer_label = challenge.correct_answer_image ? challenge.correct_answer_image.label : challenge.correct_answer_text
            frame_index = challenge.lesson_content.frames.index(challenge.frame)+1
            challenge_index = challenge.frame.main_ui_component.challenges.index(challenge)
            editor_template = challenge.frame.main_ui_component.editor_template
            puts "#{challenge.lesson.lesson_guid}:#{challenge.lesson.title}: Frame #{frame_index} Challenge #{challenge_index} #{editor_template} --> #{correct_answer_label} != #{blank_label}"

            # make the change
            challenge.unlink_blank_from_answer = true
        end

        # we don't need to worry about publishing, because this only
        # affects the editor
        unlinked.map { |e| e[:challenge] }.map(&:lesson).uniq.each do |lesson|
            begin
                lesson.save!
            rescue Exception => e
            end
        end
    end
end
