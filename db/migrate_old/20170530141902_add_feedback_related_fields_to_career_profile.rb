class AddFeedbackRelatedFieldsToCareerProfile < ActiveRecord::Migration[5.0]
    def change
        add_column :career_profiles, :profile_feedback, :text
        add_column :career_profiles, :feedback_last_sent_at, :datetime
        add_column :career_profiles_versions, :profile_feedback, :text
        add_column :career_profiles_versions, :feedback_last_sent_at, :datetime
    end
end
