class AddUserNotificationFlags < ActiveRecord::Migration[4.2]

    def up

        add_column :users, :notify_email_daily,         :boolean, default: true, null: false
        add_column :users, :notify_email_content,       :boolean, default: true, null: false
        add_column :users, :notify_email_features,      :boolean, default: true, null: false
        add_column :users, :notify_email_reminders,     :boolean, default: true, null: false
        add_column :users, :notify_email_newsletter,    :boolean, default: true, null: false


        add_column :users_versions, :notify_email_daily,         :boolean
        add_column :users_versions, :notify_email_content,       :boolean
        add_column :users_versions, :notify_email_features,      :boolean
        add_column :users_versions, :notify_email_reminders,     :boolean
        add_column :users_versions, :notify_email_newsletter,    :boolean

        create_trigger('users', [
            'email',
            'encrypted_password',
            'reset_password_token',
            'reset_password_sent_at',
            'first_name',
            'last_name',
            'sign_up_code',
            'reset_password_redirect_url',
            'provider',
            'uid',
            'subscriptions_enabled',
            'notify_email_daily',
            'notify_email_content',
            'notify_email_features',
            'notify_email_reminders',
            'notify_email_newsletter'
        ])

    end

    def down

        remove_column :users, :notify_email_daily
        remove_column :users, :notify_email_content
        remove_column :users, :notify_email_features
        remove_column :users, :notify_email_reminders
        remove_column :users, :notify_email_newsletter


        remove_column :users_versions, :notify_email_daily
        remove_column :users_versions, :notify_email_content
        remove_column :users_versions, :notify_email_features
        remove_column :users_versions, :notify_email_reminders
        remove_column :users_versions, :notify_email_newsletter

        create_trigger('users', [
            'email',
            'encrypted_password',
            'reset_password_token',
            'reset_password_sent_at',
            'first_name',
            'last_name',
            'sign_up_code',
            'reset_password_redirect_url',
            'provider',
            'uid'
        ])
    end

    def create_trigger(table_name, columns_to_watch)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.connection.schema_cache.clear!
        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"

        if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
        update_command = "
            IF #{if_string} THEN
            #{insert_on_update}
            END IF;
            "

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end
end
