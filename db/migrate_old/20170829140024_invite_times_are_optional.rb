class InviteTimesAreOptional < ActiveRecord::Migration[5.1]
    def up
        change_column_null :open_positions, :available_interview_times, true
        change_column_null :open_positions_versions, :available_interview_times, true
    end
    def down
        change_column_null :open_positions_versions, :available_interview_times, true
        change_column_null :open_positions, :available_interview_times, false
    end
end
