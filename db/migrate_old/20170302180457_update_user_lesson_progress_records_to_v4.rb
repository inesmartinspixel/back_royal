class UpdateUserLessonProgressRecordsToV4 < ActiveRecord::Migration[5.0]
    def up

        # update UserLessonProgressRecords
        ViewHelpers.set_versions(self, 20170302180457)

    end

    def down

        ViewHelpers.set_versions(self, 20170302180457 - 1)
    end
end
