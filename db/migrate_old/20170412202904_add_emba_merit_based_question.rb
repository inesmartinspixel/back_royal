class AddEmbaMeritBasedQuestion < ActiveRecord::Migration[5.0]
    def change
        add_column :career_profiles, :consider_merit_based, :boolean, default: true
        add_column :career_profiles_versions, :consider_merit_based, :boolean
    end
end
