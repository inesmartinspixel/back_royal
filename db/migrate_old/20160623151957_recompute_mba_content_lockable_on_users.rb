class RecomputeMbaContentLockableOnUsers < ActiveRecord::Migration[4.2]
    def up
        execute "
            with stream_entries AS MATERIALIZED (
                select unnest(stream_entries) as entry from playlists where locale_pack_id=(
                    select playlist_pack_ids[1] from cohorts where name_locales->>'en' = 'MBA1'
                )
            ),
            stream_locale_pack_ids AS MATERIALIZED (
                select (entry->>'locale_pack_id')::uuid as stream_locale_pack_id from stream_entries
            ),
            users_with_completed AS MATERIALIZED (
                SELECT user_id, count(*) FROM lesson_streams_progress
                    LEFT JOIN users ON users.id = lesson_streams_progress.user_id
                    WHERE completed_at IS NOT NULL
                        AND locale_pack_id IN (SELECT stream_locale_pack_id from stream_locale_pack_ids)
                        AND mba_content_lockable = true
                    GROUP BY user_id
                    HAVING count(*) = 7
            )

            UPDATE users
                SET mba_content_lockable = false
                WHERE users.id IN (SELECT user_id from users_with_completed);
        "
    end
end
