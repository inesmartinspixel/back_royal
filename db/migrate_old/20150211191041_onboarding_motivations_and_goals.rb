class OnboardingMotivationsAndGoals < ActiveRecord::Migration[4.2]

    def change

        create_table :onboarding_motivations, id: :uuid do |t|
            t.timestamps
            t.text :description
        end

        create_table :onboarding_motivations_users, id: false do |t|
            t.timestamps
            t.uuid :onboarding_motivation_id
            t.uuid :user_id
        end

        create_table :onboarding_goals, id: :uuid do |t|
            t.timestamps
            t.text :description
        end

        create_table :onboarding_goals_users, id: false do |t|
            t.timestamps
            t.uuid :onboarding_goal_id
            t.uuid :user_id
        end

        create_table :lesson_streams_onboarding_goals, id: false do |t|
            t.timestamps
            t.uuid :lesson_stream_id
            t.uuid :onboarding_goal_id
        end


        # we have to use custom index names due to generated string length exceeding the 63 chars

        add_index :onboarding_motivations, :created_at

        add_index :onboarding_motivations_users, :created_at
        add_index :onboarding_motivations_users, [ :onboarding_motivation_id, :user_id ], :unique => true, :name => "onboarding_motivation_id_user_id"
        add_foreign_key "onboarding_motivations_users", "onboarding_motivations", column: "onboarding_motivation_id", primary_key: "id"
        add_foreign_key "onboarding_motivations_users", "users", column: "user_id", primary_key: "id"

        add_index :onboarding_goals, :created_at

        add_index :onboarding_goals_users, :created_at
        add_index :onboarding_goals_users, [ :onboarding_goal_id, :user_id ], :unique => true, :name => "onboarding_goal_id_user_id"
        add_foreign_key "onboarding_goals_users", "onboarding_goals", column: "onboarding_goal_id", primary_key: "id"
        add_foreign_key "onboarding_goals_users", "users", column: "user_id", primary_key: "id"

        add_index :lesson_streams_onboarding_goals, :created_at
        add_index :lesson_streams_onboarding_goals, [ :lesson_stream_id, :onboarding_goal_id ], :unique => true, :name => "onboarding_goal_id_lesson_stream_id"
        add_foreign_key "lesson_streams_onboarding_goals", "lesson_streams", column: "lesson_stream_id", primary_key: "id"
        add_foreign_key "lesson_streams_onboarding_goals", "onboarding_goals", column: "onboarding_goal_id", primary_key: "id"



        # we can't define "change" and "up".  This is easier that
        # this block will only be run when going up
        populate_seed_data unless self.reverting?

    end


    def populate_seed_data
        return # OnboardingMotivation no longer exists
        motivations = [
            'I want to get ahead in my career.',
            'I want to improve my business acumen.',
            'I want to change my career.',
            'I want to start my own business.'
        ]

        motivations.each do |motivation|
            OnboardingMotivation.create!({:description => motivation})
        end

        goals = [
            { :description => 'Create a strategic plan for my project or business.', :lesson_streams => ['87b85177-fdf6-4cf8-a29a-c44b0ef77530', '9b680532-54e0-4ade-a048-1f1881e667ea']},
            { :description => 'Manage my finances better.', :lesson_streams => ['222d17ff-718d-4bbc-b4df-444e71e7aec9', '964fe746-9fb7-46c8-883c-4b653579733d ']},
            { :description => 'Run a digital marketing campaign.', :lesson_streams => ['abd47e21-4b59-4ccf-910d-fb1c18b2e9df', '1deeff68-ed22-4399-a967-e6689c0aed3f', '35cf54b6-4522-44c3-b762-531bf25436f0', '1d93f964-1086-4096-a9a7-26ac283c626a']},
            { :description => 'Create and select more compelling photographs.', :lesson_streams => ['5b46c407-6061-45a9-aa3e-5dcf7a6f76d6']},
            { :description => 'Understand financial and accounting terminology.', :lesson_streams => ['50eeda2d-1b44-4a1d-b425-dead8982526f', '222d17ff-718d-4bbc-b4df-444e71e7aec9']},
            { :description => 'Balance the books for a small business.', :lesson_streams => ['50eeda2d-1b44-4a1d-b425-dead8982526f']},
            { :description => 'Analyze business metrics.', :lesson_streams => ['8d720fb5-0320-4bf4-8c40-50808267a1be', '48b273ee-d9ee-4c12-8482-3cfd37e34542', 'd79316c4-a419-41ff-ae42-be14fe70af44', '5c4ea530-21ae-46ec-bfcd-ccfbf6f9c49a']},
            { :description => 'Make better investing decisions.', :lesson_streams => ['964fe746-9fb7-46c8-883c-4b653579733d']},
            { :description => 'Interpret economic processes.', :lesson_streams => ['f6296448-533f-456f-bdc3-1b0c83cf039f', '95437d2a-0211-4899-825d-e2107c6729e6']},
            { :description => 'Discuss high-level marketing strategy with my peers.', :lesson_streams => ['abd47e21-4b59-4ccf-910d-fb1c18b2e9df', '9b680532-54e0-4ade-a048-1f1881e667ea']},
            { :description => 'Execute and analyze a test-driven marketing campaign.', :lesson_streams => ['1deeff68-ed22-4399-a967-e6689c0aed3f', '8d720fb5-0320-4bf4-8c40-50808267a1be', '48b273ee-d9ee-4c12-8482-3cfd37e34542', 'd79316c4-a419-41ff-ae42-be14fe70af44']},
            { :description => 'Make better visual design decisions.', :lesson_streams => ['5b46c407-6061-45a9-aa3e-5dcf7a6f76d6', '2d98a1c6-7a19-4fb9-a6a1-fb662ba9ce82']},
            { :description => 'Grasp the impact of market forces on my business.', :lesson_streams => ['95437d2a-0211-4899-825d-e2107c6729e6']},
            { :description => 'Leverage a proven framework to make critical business decisions.', :lesson_streams => ['87b85177-fdf6-4cf8-a29a-c44b0ef77530', '9b680532-54e0-4ade-a048-1f1881e667ea', 'abd47e21-4b59-4ccf-910d-fb1c18b2e9df']},
            { :description => 'Understand how current events impact my business.', :lesson_streams => ['f6296448-533f-456f-bdc3-1b0c83cf039f']},
            { :description => 'Use basic math in everyday life.', :lesson_streams => ['c36a0ccd-fc05-45fe-aead-9935703756b8']}
        ]

        # when running all migrations, the Lesson::Stream class may not
        # be updated with the current state of the table schema. reset it
        Lesson::Stream.connection.schema_cache.clear!
        Lesson::Stream.reset_column_information

        goals.each do |goal_config|
            goal = OnboardingGoal.new({:description => goal_config[:description]})
            goal.lesson_streams = Lesson::Stream.where(id: goal_config[:lesson_streams])
            goal.save!
        end


    end

end
