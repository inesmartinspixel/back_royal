class AddSeenFramesToLessonProgress < ActiveRecord::Migration[4.2]

    def up
        add_column :lesson_progress, :completed_frames, :json, :default => {}
        add_column :lesson_progress, :challenge_scores, :json, :default => {}
        backfill

        execute "CREATE TYPE lesson_progress_json_v2 AS (
            lesson_id uuid,
            frame_bookmark_id uuid,
            frame_history json,
            completed_frames json,
            challenge_scores json,
            started_at int,
            completed_at int,
            complete boolean,
            last_progress_at int,
            id uuid
        )"
        # do not drop lesson_progress_json_v1. that would defeat the whole purpose of versioning these things
    end

    def down
        remove_column :lesson_progress, :completed_frames
        remove_column :lesson_progress, :challenge_scores
        execute "DROP TYPE lesson_progress_json_v2"
    end

    def backfill
        query = LessonProgress.where('frame_history is not null')
        total = query.count
        completed = 0
        start = Time.now
        query.find_in_batches(batch_size: 50).each do |batch|
            puts "#{completed} of #{total} lesson progresses processed in #{(Time.now - start).to_timestamp} seconds"
            batch.each do |lesson_progress|
                lesson_progress.completed_frames = {}
                lesson_progress.frame_history.each do |id|
                    lesson_progress.completed_frames[id] = true
                end
                lesson_progress.save!
                completed = completed + 1
            end
        end
    end
end
