class FixCohortPeriodViews < ActiveRecord::Migration[5.0]
    def up
        ViewHelpers.migrate(self, 20170411201741)
    end

    def down
        ViewHelpers.migrate(self, 20170411201741)
    end
end
