class AddSeoImageToLessons < ActiveRecord::Migration[4.2]
    def change
        add_column :lessons, :seo_image_id, :uuid
        add_column :lessons_versions, :seo_image_id, :uuid
    end
end
