class NormalizeCareerProfiles < ActiveRecord::Migration[5.0]

    def up
        execute "UPDATE users SET how_did_you_hear_about_us = 'Not sure' WHERE how_did_you_hear_about_us ILIKE 'Not sure%' OR how_did_you_hear_about_us = 'other' OR (how_did_you_hear_about_us IS NULL AND id IN (SELECT user_id FROM career_profiles WHERE last_calculated_complete_percentage = 100))"
        execute "UPDATE users SET program_type_confirmed = 'true' WHERE id IN (SELECT user_id FROM cohort_applications)"
    end

    def down
        # noop
    end

end
