class AddPlaylistsModel < ActiveRecord::Migration[4.2]

    def up

        # create the table and constraints
        create_table    :playlists, id: :uuid do |t|

            # core fields
            t.text      :title, null: false
            t.text      :description
            t.json      :stream_entries, array: true, default: []
            t.uuid      :image_id

            # IsContentItem fields
            t.uuid      :entity_metadata_id
            t.uuid      :author_id
            t.uuid      :last_editor_id
            t.datetime  :modified_at
            t.boolean   :was_published, null: false, default: false
            # t.boolean   :pinned, null: false, default: false
            # t.text      :pinned_title
            # t.text      :pinned_description

            # timestamps
            t.timestamps
        end

        create_versions_table_and_trigger('playlists')

        temporarily_delete_user_views do

            add_column :users, :active_playlist_id, :uuid
            add_column :users_versions, :active_playlist_id, :uuid

            add_foreign_key :users, :playlists, column: "active_playlist_id", primary_key: "id"

            create_trigger('users', [
                'email',
                'encrypted_password',
                'reset_password_token',
                'reset_password_sent_at',
                'first_name',
                'last_name',
                'sign_up_code',
                'reset_password_redirect_url',
                'provider',
                'uid',
                'subscriptions_enabled',
                'notify_email_daily',
                'notify_email_content',
                'notify_email_features',
                'notify_email_reminders',
                'notify_email_newsletter',
                'free_trial_started',
                'confirmed_profile_info',
                'optimizely_referer',
                'active_playlist_id'
                # the json columns must be left out of here, since they can't be compared
            ])
        end

        add_column :content_publishers, :playlist_version_id, :uuid
        add_foreign_key "content_publishers", "playlists_versions", column: "playlist_version_id", primary_key: "version_id"
        add_column :content_publishers_versions, :playlist_version_id, :uuid

        create_trigger('content_publishers')

    end

    def down

        remove_column :content_publishers_versions, :playlist_version_id
        remove_column :content_publishers, :playlist_version_id
        create_trigger('content_publishers')

        temporarily_delete_user_views do

            create_trigger('users', [
                'email',
                'encrypted_password',
                'reset_password_token',
                'reset_password_sent_at',
                'first_name',
                'last_name',
                'sign_up_code',
                'reset_password_redirect_url',
                'provider',
                'uid',
                'subscriptions_enabled',
                'notify_email_daily',
                'notify_email_content',
                'notify_email_features',
                'notify_email_reminders',
                'notify_email_newsletter',
                'free_trial_started',
                'confirmed_profile_info',
                'optimizely_referer'
                # the json columns must be left out of here, since they can't be compared
            ])

            remove_column :users_versions, :active_playlist_id
            remove_column :users, :active_playlist_id

        end

        drop_versions_table_and_trigger('playlists')
        drop_table :playlists
    end

    def create_versions_table_and_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.connection.schema_cache.clear!
        klass.reset_column_information

        # create the version table, including all columns from the source table
        # plus a few extras
        create_table "#{table_name}_versions", id: false, force: :cascade do |t|
            t.uuid     "version_id", null: false, default: "uuid_generate_v4()"
            t.string   "operation", limit: 1, null: false
            t.datetime "version_created_at", null: false

            puts "*** #{table_name}_versions"

            klass.column_names.each do |column_name|
                type = klass.columns_hash

                column_config = klass.columns_hash[column_name]
                meth = column_config.type.to_sym
                options = {}
                options[:limit] = column_config.limit unless column_config.limit.nil?
                options[:null] = false if column_config.null == false
                options[:array] = column_config.array
                # ignore defaults, since they should not be used in the audit table

                puts "t.#{meth}, #{column_name}, #{options.inspect}"
                t.send(meth, column_name, options)
            end
        end

        # for some reason the primary flag on create_table was not working
        execute "ALTER TABLE #{table_name}_versions ADD PRIMARY KEY (version_id);"

        # create indexes
        if klass.column_names.include?('id') && klass.column_names.include?('updated_at')
            add_index "#{table_name}_versions", [:id, :updated_at]
        elsif klass.column_names.include?('id')
            add_index "#{table_name}_versions", :id
        end

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "
        execute(trigger_sql)
    end

    def drop_versions_table_and_trigger(table_name)
        drop_table "#{table_name}_versions"
        execute "DROP TRIGGER #{table_name}_versions ON #{table_name}"
    end

    def create_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end


    def temporarily_delete_user_views(&block)
        # this used to do something, but no more
        yield
    end

end
