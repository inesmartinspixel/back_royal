class DropUnusedCareerProfileFields < ActiveRecord::Migration[5.1]
    def change

        remove_column :career_profiles, :job_search_stage, :text
        remove_column :career_profiles, :ideal_job_start_time, :text

        remove_column :career_profiles_versions, :job_search_stage, :text
        remove_column :career_profiles_versions, :ideal_job_start_time, :text

    end
end
