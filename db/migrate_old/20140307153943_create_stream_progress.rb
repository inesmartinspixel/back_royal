class CreateStreamProgress < ActiveRecord::Migration[4.2]
    def change
        create_table :lesson_streams_progress do |t|
            t.timestamps
            t.string :lesson_bookmark_guid
            t.datetime :started_at
            t.datetime :completed_at
            t.references :lesson_stream
            t.references :user
        end

        add_index(:lesson_streams_progress, :lesson_stream_id)
        add_index(:lesson_streams_progress, :user_id)

    end
end
