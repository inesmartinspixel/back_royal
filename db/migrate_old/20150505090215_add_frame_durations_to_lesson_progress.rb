class AddFrameDurationsToLessonProgress < ActiveRecord::Migration[4.2]
    def up

        add_column :lesson_progress, :frame_durations, :json, :default => {}

        execute "CREATE TYPE lesson_progress_json_v3 AS (
            lesson_id uuid,
            frame_bookmark_id uuid,
            frame_history json,
            completed_frames json,
            challenge_scores json,
            frame_durations json,
            started_at int,
            completed_at int,
            complete boolean,
            last_progress_at int,
            id uuid
        )"
        # do not drop lesson_progress_json_v1. that would defeat the whole purpose of versioning these things

    end

    def down
        remove_column :lesson_progress, :frame_durations
    end
end