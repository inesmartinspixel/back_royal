class CreateCollectionProgress < ActiveRecord::Migration[4.2]
    def change
        create_table :collection_progress do |t|
            t.references :collection, index: true
            t.references :user, index: true
            t.date :started_at
            t.date :completed_at

            t.timestamps
        end
    end
end
