require File.expand_path('../20161116201437_views_for_mba_score_query.rb', __FILE__)

class MaterializeContentViews < ActiveRecord::Migration[5.0]
    def up

        %w(
        published_streams
        published_playlists
        published_lessons
        published_stream_lessons
        published_playlist_streams
        published_playlist_lessons
        published_stream_locale_packs
        published_playlist_locale_packs
        published_lesson_locale_packs
        published_stream_lesson_locale_packs
        cohort_playlist_locale_packs
        hacked_access_groups_cohorts
        cohort_stream_locale_packs
        cohort_lesson_locale_packs
        institution_playlist_locale_packs
        institution_stream_locale_packs
        institution_lesson_locale_packs
        cohort_content_details
        ).each do |name|
            execute("DROP VIEW IF EXISTS #{name} CASCADE")
        end

        ViewHelpers.set_versions(self, 20170220200244)
    end

    def down

        %w(
        published_cohorts
        published_streams
        published_playlists
        published_lessons
        published_stream_lessons
        published_playlist_streams
        published_playlist_lessons
        published_stream_locale_packs
        published_playlist_locale_packs
        published_lesson_locale_packs
        published_stream_lesson_locale_packs
        cohort_playlist_locale_packs
        hacked_access_groups_cohorts
        cohort_stream_locale_packs
        cohort_lesson_locale_packs
        institution_playlist_locale_packs
        institution_stream_locale_packs
        institution_lesson_locale_packs
        cohort_content_details
        ).each do |name|
            execute("DROP MATERIALIZED VIEW IF EXISTS #{name} CASCADE")
            migration = ViewsForMbaScoreQuery.new
            migration.send(name.to_sym) if migration.respond_to?(name.to_sym)
        end
    end
end
