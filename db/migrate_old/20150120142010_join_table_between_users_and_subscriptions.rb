class JoinTableBetweenUsersAndSubscriptions < ActiveRecord::Migration[4.2]
    def change
        create_table :subscriptions_users do |t|
            t.timestamps
            t.references :subscription
            t.references :user
        end

        # let the join table own the relationship so it can be two-sided
        remove_column :subscriptions, :user_id

    end
end
