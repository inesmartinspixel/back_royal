class MigrateDailyLessonConfig < ActiveRecord::Migration[4.2]
    def change
        create_table :daily_lesson_configs, id: :uuid do |t|
            t.timestamps
            t.uuid :lesson_id
            t.date :date
        end

        add_index :daily_lesson_configs, :lesson_id
        add_index :daily_lesson_configs, :date
        add_foreign_key :daily_lesson_configs, :lessons
    end
end
