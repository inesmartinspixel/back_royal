class AddCertImageToStreams < ActiveRecord::Migration[4.2]
    def change
        add_column :lesson_streams, :certificate_image_id, :integer
    end
end
