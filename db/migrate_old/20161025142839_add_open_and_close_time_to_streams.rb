class AddOpenAndCloseTimeToStreams < ActiveRecord::Migration[4.2]
    def change
        add_column :lesson_streams, :exam_open_time, :timestamp, :null => true
        add_column :lesson_streams, :exam_close_time, :timestamp, :null => true
        add_column :lesson_streams_versions, :exam_open_time, :timestamp, :null => true
        add_column :lesson_streams_versions, :exam_close_time, :timestamp, :null => true

        add_column :lesson_streams_progress, :close_time, :timestamp, :null => true
    end
end