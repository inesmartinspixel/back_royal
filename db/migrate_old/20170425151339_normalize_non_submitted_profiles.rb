class NormalizeNonSubmittedProfiles < ActiveRecord::Migration[5.0]

    def up
        users = User.joins(:career_profile).where("users.how_did_you_hear_about_us IS NULL AND career_profiles.last_calculated_complete_percentage = 96")
        users.each do |user|
            user.how_did_you_hear_about_us = 'Not sure'
            user.career_profile[:last_calculated_complete_percentage] = 100
            user.career_profile.save!
            user.save!
        end
    end

    def down
        #noop
    end

end
