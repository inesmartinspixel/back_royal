class CreateStreams < ActiveRecord::Migration[4.2]
    def change

        create_table :lesson_streams do |t|
            t.timestamps
            t.string :title
            t.string :description
        end

        create_table :lesson_to_stream_joins do |t|
            t.timestamps
            t.integer :stream_id
            t.integer :lesson_id
            t.integer :position
        end

    end
end