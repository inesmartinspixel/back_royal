class AddIndustryRoleToWorkExperiences < ActiveRecord::Migration[5.1]
    def change
        add_column :work_experiences, :role, :text
        add_column :work_experiences_versions, :role, :text

        add_column :work_experiences, :industry, :text
        add_column :work_experiences_versions, :industry, :text

        add_index :work_experiences, [:role]
        add_index :work_experiences, [:industry]
    end
end
