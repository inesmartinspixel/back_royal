class SetDefaultSkipAutoPeriodExpulsion < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        # batch update cohort applications outside of a transaction to prevent
        # large table locks
        CohortApplication.select('id')
            .where(skip_period_expulsion: nil)
            .find_in_batches do |batch|

                CohortApplication.where(skip_period_expulsion: nil)
                            .where(id: batch.map(&:id))
                            .update_all(skip_period_expulsion: false)
        end
    end
end
