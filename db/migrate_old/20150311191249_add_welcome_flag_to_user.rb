class AddWelcomeFlagToUser < ActiveRecord::Migration[4.2]
    def change
        add_column :users, :has_seen_welcome, :boolean, :default => false
        add_column :users_versions, :has_seen_welcome, :boolean, :default => false
    end
end
