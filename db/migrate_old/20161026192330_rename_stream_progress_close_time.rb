class RenameStreamProgressCloseTime < ActiveRecord::Migration[4.2]
    def change
        rename_column "lesson_streams_progress", "close_time", "time_runs_out_at"
    end
end
