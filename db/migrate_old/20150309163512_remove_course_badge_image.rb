class RemoveCourseBadgeImage < ActiveRecord::Migration[4.2]
    def change
        # this removes the separate badge image from courses
        # the user generated certificate is still there though-
        # in lesson_stream_progress.certificate_image_id
        remove_column :lesson_streams, :certificate_image_id, :uuid
        remove_column :lesson_streams_versions, :certificate_image_id, :uuid
    end
end
