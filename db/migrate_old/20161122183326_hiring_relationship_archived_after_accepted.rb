class HiringRelationshipArchivedAfterAccepted < ActiveRecord::Migration[5.0]
    def change
        add_column :hiring_relationships, :archived_after_accepted, :bool, :default => false
        add_column :hiring_relationships_versions, :archived_after_accepted, :bool
    end
end
