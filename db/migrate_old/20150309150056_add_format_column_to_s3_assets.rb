class AddFormatColumnToS3Assets < ActiveRecord::Migration[4.2]
    def up
        add_column :s3_assets, :formats, :json
    end

    def down
        remove_column :s3_assets, :formats
    end

end
