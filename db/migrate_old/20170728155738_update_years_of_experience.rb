class UpdateYearsOfExperience < ActiveRecord::Migration[5.1]
    def up
        # Change YearsOfExperience to no longer use career_profile_active, but instead use can_edit_career_profile,
        # last_calculated_complete_percentage, and interested_in_joining_new_company to determine if the profile is
        # considered active or inactive.
        ViewHelpers.migrate(self, 20170728155738)
    end

    def down
        ViewHelpers.rollback(self, 20170728155738)
    end
end
