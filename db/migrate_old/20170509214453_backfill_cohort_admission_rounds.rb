class BackfillCohortAdmissionRounds < ActiveRecord::Migration[5.0]
    def up

        if Rails.env == 'test'
            return
        end

        # Cohorts

        # SELECT id,name,admission_rounds FROM cohorts;
        # ...
        #
        # -[ RECORD 10 ]---+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        # id               | 6d171de6-eeea-49d8-b5d2-684c97923bfb
        # name             | MBA7
        # admission_rounds | {"{\"application_deadline_days_offset\":-35,\"decision_date_days_offset\":-4,\"__iguana_type\":\"AdmissionRound\"}","{\"application_deadline_days_offset\":-14,\"decision_date_days_offset\":-4,\"__iguana_type\":\"AdmissionRound\"}"}
        # -[ RECORD 11 ]---+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        # id               | 8c1abbd1-f4ff-4236-b526-5d7c1ab1df8d
        # name             | EMBA3
        # admission_rounds | {"{\"application_deadline_days_offset\":-49,\"decision_date_days_offset\":-18,\"__iguana_type\":\"AdmissionRound\"}","{\"application_deadline_days_offset\":-28,\"decision_date_days_offset\":-18,\"__iguana_type\":\"AdmissionRound\"}"}
        # -[ RECORD 12 ]---+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        # id               | 9659a73b-fa80-41ae-9767-e06f0403bc85
        # name             | MBA6
        # admission_rounds | {"{\"application_deadline_days_offset\":-7,\"decision_date_days_offset\":-3,\"__iguana_type\":\"AdmissionRound\"}"}
        # -[ RECORD 13 ]---+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        # id               | 806d8eb9-1fa8-4d4a-95e8-e7557e3a00f3
        # name             | EMBA2
        # admission_rounds | {"{\"application_deadline_days_offset\":-21,\"decision_date_days_offset\":-17,\"__iguana_type\":\"AdmissionRound\"}"}

        cohorts_admission_rounds = {
            "6d171de6-eeea-49d8-b5d2-684c97923bfb" => [{"application_deadline_days_offset":-35,"decision_date_days_offset":-4,"__iguana_type":"AdmissionRound"}, {"application_deadline_days_offset":-14,"decision_date_days_offset":-4,"__iguana_type":"AdmissionRound"}],
            "8c1abbd1-f4ff-4236-b526-5d7c1ab1df8d" => [{"application_deadline_days_offset":-49,"decision_date_days_offset":-18,"__iguana_type":"AdmissionRound"}, {"application_deadline_days_offset":-28,"decision_date_days_offset":-18,"__iguana_type":"AdmissionRound"}],
            "9659a73b-fa80-41ae-9767-e06f0403bc85" => [{"application_deadline_days_offset":-7,"decision_date_days_offset":-3,"__iguana_type":"AdmissionRound"}],
            "806d8eb9-1fa8-4d4a-95e8-e7557e3a00f3" => [{"application_deadline_days_offset":-21,"decision_date_days_offset":-17,"__iguana_type":"AdmissionRound"}]
        }
        cohorts_admission_rounds.each do |id, admission_rounds|
            cohort = Cohort.find(id)
            cohort.admission_rounds = admission_rounds
            cohort.publish!
        end



        # Templates

        # SELECT id,name,admission_rounds FROM curriculum_templates;
        # ...
        #
        # -[ RECORD 4 ]----+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        # id               | 165769f7-3336-46cf-bb15-dd881ec16e28
        # name             | MBA April 2017 Template
        # admission_rounds | {"{\"application_deadline_days_offset\":-35,\"decision_date_days_offset\":-4,\"__iguana_type\":\"AdmissionRound\"}","{\"application_deadline_days_offset\":-14,\"decision_date_days_offset\":-4,\"__iguana_type\":\"AdmissionRound\"}"}

        curriculum_templates_admission_rounds = {
            "165769f7-3336-46cf-bb15-dd881ec16e28" => [{"application_deadline_days_offset":-35,"decision_date_days_offset":-4,"__iguana_type":"AdmissionRound"}, {"application_deadline_days_offset":-14,"decision_date_days_offset":-4,"__iguana_type":"AdmissionRound"}]
        }
        curriculum_templates_admission_rounds.each do |id, admission_rounds|
            curriculum_template = CurriculumTemplate.find(id)
            curriculum_template.admission_rounds = admission_rounds
            curriculum_template.save!
        end


    end

    def down
        # no-op
    end

end
