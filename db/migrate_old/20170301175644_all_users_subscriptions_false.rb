class AllUsersSubscriptionsFalse < ActiveRecord::Migration[5.0]
    def up
        execute "UPDATE users SET subscriptions_enabled = false WHERE subscriptions_enabled = true"
    end
    def down
        # at time of writing -- andrew@jump450.com
        execute "UPDATE users SET subscriptions_enabled = true WHERE id='8ff9d407-7553-4f77-a7ef-4c9a118de84a'"
    end
end
