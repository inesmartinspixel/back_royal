class CreateReportFrameEvents < ActiveRecord::Migration[4.2]
	def change
		create_table :report_frame_events do |t|
			t.string :lesson_title
			t.string :lesson_guid
			t.integer :stream_id
			t.string :stream_title
			t.integer :frame_index
			t.string :frame_guid
			t.text :frame_text
			t.string :event_type
			t.json :event_json
			t.integer :papertrail_version_id
			t.string :editor_template
			t.references :user
			t.string :lesson_play_id
			t.string :lesson_frame_id

			t.timestamps
		end

		add_index(:report_frame_events, :user_id)
		add_index(:report_frame_events, :created_at)

	end
end
