class UpdateLastCalculatedCompletePercentage < ActiveRecord::Migration[5.0]
    def up
        records = CareerProfile.joins(:user).where(users: {can_edit_career_profile: true}).where(last_calculated_complete_percentage: nil, career_profile_active: true)
        # Set the last_calculated_complete_percentage to 100 for each record returned from the query above.
        # I manually logged in as each user in prod and validated that their career profile was actually complete,
        # so this should be okay to do.
        records.each do |record|
            record.last_calculated_complete_percentage = 100
            record.save!(validate: false)
        end
    end

    def down
        # These are the career profile ids for the records returned in the rails query in the up() method
        career_profile_ids = [
            'd1d98de6-e6b5-4612-a783-a24c0fce858d', # brent.s.cross@gmail.com
            'e2215f6f-2f74-4430-96d0-bff197ccc661', # nyrmiller@aol.com
            'ddf94f4e-8f34-438f-9b39-ab94e11afb45', # tripp.pettigrew@gmail.com
            '479c979f-d6f4-45c2-9bc5-a9890b142f5e', # jcdebiasio@gmail.com
            '610c2a93-eb4a-45bf-9b96-2bd92ed18bf8', # pudjeeb.b@gmail.com
            'e25afab7-74b7-4adc-bf8d-d47d1c10eb3f', # nathanjohnson2007@gmail.com
            '4d018128-71f6-4de4-a41c-03da983a937f', # mjcartwright0@gmail.com
            'eee64026-2005-43a8-86f2-ddfe84c629b2', # emilie.burke@gmail.com
            'd29989cd-701e-4bb2-a75c-644288d29bf9', # brendan.whiting@me.com
            '6aeef420-ed58-413f-81ca-47f9ff00051b', # schowdhury@berkeley.edu
            'fe7effaf-e98f-4976-945e-c2b89577cc31', # lizagraziano@gmail.com
            'c52b79d2-0304-4bd7-a233-1e294fab5280'  # abbytmars@gmail.com
        ]
        career_profile_ids.each do |career_profile_id|
            record = CareerProfile.find_by_id(career_profile_id)
            if record
                record.last_calculated_complete_percentage = nil
                record.save!(validate: false)
            end
        end
    end
end
