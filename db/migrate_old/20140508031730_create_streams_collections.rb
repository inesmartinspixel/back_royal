class CreateStreamsCollections < ActiveRecord::Migration[4.2]
    def change
        create_table :lesson_streams_collections do |t|
            t.references :collection, index: true
            t.references :lesson_stream, index: true

            t.timestamps
        end
    end
end
