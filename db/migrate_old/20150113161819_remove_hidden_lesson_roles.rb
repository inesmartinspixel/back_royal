class RemoveHiddenLessonRoles < ActiveRecord::Migration[4.2]
    def change

        # destroy all 'hidden' Lesson resource roles ... it's the default policy now
        Role.where(:name => 'hidden', :resource_type => 'Lesson').destroy_all

        # rename improperly assigned Lesson 'editor' roles to 'lesson_editor' (checked with Alexie about this)
        Role.where(:name => 'editor', :resource_type => 'Lesson').update_all(:name => 'lesson_editor')

    end
end
