class AddUniqueIndexToEventsGuid < ActiveRecord::Migration[4.2]
    def change
        add_index :events, :guid, :unique => true
    end
end
