class AddUserTypeTimeIndexToEvents < ActiveRecord::Migration[4.2]
    disable_ddl_transaction!

    def change
        add_index(:events, [ :user_id, :event_type, :estimated_time ], algorithm: :concurrently)
    end
end
