class UpdateHiringManagerPriorityToFloat < ActiveRecord::Migration[5.0]
    def up
        change_column :hiring_relationships, :hiring_manager_priority, :float
    end

    def down
        change_column :hiring_relationships, :hiring_manager_priority, :integer
    end
end
