class RemoveProfileGreScore < ActiveRecord::Migration[4.2]
    def up
        remove_column :career_profiles, :score_on_gre
        remove_column :career_profiles_versions, :score_on_gre

        remove_column :cohort_applications, :score_on_gre
        remove_column :cohort_applications_versions, :score_on_gre
    end

    def down
        add_column :career_profiles, :score_on_gre, :text
        add_column :career_profiles_versions, :score_on_gre, :text

        add_column :cohort_applications, :score_on_gre, :text
        add_column :cohort_applications_versions, :score_on_gre, :text
    end
end
