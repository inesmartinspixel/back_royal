class AddGuidColumns < ActiveRecord::Migration[4.2]

    require 'digest'

    include ActionView::Helpers::NumberHelper

    disable_ddl_transaction!

    def up

        # Let's clean up output a bit ...
        ActiveRecord::Migration.verbose = false

        # we disable automated transactions (above) but start a transaction we can
        # explicitly control and finish prior to calling VACUUM
        ActiveRecord::Base.transaction do

            # Enable support for UUID functions
            enable_extension "uuid-ossp"


            # Speed up commits
            execute "UPDATE pg_settings SET setting='off' WHERE name = 'synchronous_commit'"


            # Increase buffers from 8MB to 100MB
            execute "SET temp_buffers = 100"


            # 1. Data cleanup for rows where referential integrity has been violated (due to lack of FK constraints)
            puts "*************************** Deleting violations of referential integrity"

            execute "DELETE FROM collection_progress WHERE user_id NOT IN (SELECT id FROM users)"
            execute "DELETE FROM concept_progress WHERE user_id NOT IN (SELECT id FROM users)"
            execute "DELETE FROM lesson_progress WHERE user_id NOT IN (SELECT id FROM users)"
            execute "DELETE FROM lesson_progress WHERE lesson_guid NOT IN (SELECT lesson_guid FROM lessons)"
            execute "DELETE FROM lesson_streams_progress WHERE user_id NOT IN (SELECT id FROM users)"
            execute "DELETE FROM lesson_streams_progress WHERE lesson_stream_id NOT IN (SELECT id FROM lesson_streams)"
            execute "DELETE FROM lesson_streams_collections WHERE lesson_stream_id NOT IN (SELECT id FROM lesson_streams)"
            execute "DELETE FROM collections WHERE id NOT IN (SELECT collection_id from lesson_streams_collections)"
            execute "DELETE FROM collection_concepts WHERE collection_id NOT IN (SELECT id from collections)"
            execute "DELETE FROM multiple_choice_challenge_progress WHERE user_id NOT IN (SELECT id FROM users)"
            execute "DELETE FROM institutions_groups WHERE tag_id NOT IN (SELECT id FROM tags)"
            execute "DELETE FROM roles WHERE resource_type='Lesson' AND resource_id NOT IN (SELECT id FROM lessons)"
            execute "DELETE FROM institutions_groups WHERE institution_id NOT IN (SELECT id FROM institutions)"

            # this index was missing and helps to improve performance of table update / delete below
            add_index "report_frame_events", ["stream_id"], name: "index_report_frame_events_on_stream_id", using: :btree

            execute "DELETE FROM report_frame_events WHERE stream_id NOT IN (SELECT id FROM lesson_streams)"
            execute "DELETE FROM report_frame_events WHERE user_id NOT IN (SELECT id FROM users)"

            execute "CREATE TABLE tmp AS SELECT report_frame_events.* FROM report_frame_events INNER JOIN events ON report_frame_events.source_event_id = events.id"
            execute "DROP TABLE report_frame_events"
            execute "ALTER TABLE tmp RENAME TO report_frame_events"

            add_index "report_frame_events", ["created_at"], name: "index_report_frame_events_on_created_at", using: :btree
            add_index "report_frame_events", ["estimated_time"], name: "index_report_frame_events_on_estimated_time", using: :btree
            add_index "report_frame_events", ["user_id"], name: "index_report_frame_events_on_user_id", using: :btree
            add_index "report_frame_events", ["stream_id"], name: "index_report_frame_events_on_stream_id", using: :btree
            add_index "report_frame_events", ["source_event_id"], name: "index_report_frame_events_on_source_event_id", unique: true, using: :btree


            # 2. Adds a new columns for UUID generation / future use
            puts "*************************** Addding UUID columns temporarily"
            table_names.each_with_index do |table_name, i|
                puts "\t#{table_name}"

                next unless table_name != "events" && table_name != "report_frame_events"

                execute "ALTER TABLE #{table_name} ADD COLUMN id_new uuid DEFAULT uuid_generate_v4()" # this should create new values
            end


            # 3. change type of _id columns to string
            puts "**************************** Updating Foreign Keys to Strings"
            columns.each_with_index do |column, i|

                table_name, column_name = column.split('.')
                next unless table_name != "events" && table_name != "report_frame_events"

                puts "\t#{column}"

                execute "ALTER TABLE #{table_name} ALTER COLUMN #{column_name} SET DATA TYPE text"
            end


            # 4. Special Handling of Events / Report Frame Events
            puts "**************************** Special Handling of events / report_frame_events"

            puts "\tcreating events_new table"

            execute "
                CREATE TABLE events_new (
                    id uuid NOT NULL DEFAULT uuid_generate_v4(),
                    user_id uuid,
                    event_type character varying(255),
                    payload json,
                    created_at timestamp without time zone,
                    updated_at timestamp without time zone,
                    estimated_time timestamp without time zone,
                    client_reported_time timestamp without time zone,
                    hit_server_at timestamp without time zone,
                    total_buffered_seconds double precision
                )"

            puts "\tinserting into events_new table"

            execute "
                INSERT INTO events_new (
                    SELECT
                        cast(events.guid as uuid),
                        COALESCE(users.id_new, cast(md5(cast(events.user_id as text)) as uuid)),
                        events.event_type,
                        events.payload,
                        events.created_at,
                        events.updated_at,
                        events.estimated_time,
                        events.client_reported_time,
                        events.hit_server_at,
                        events.total_buffered_seconds
                    FROM events
                    LEFT JOIN users ON events.user_id = users.id
                )"

            puts "\tcreating report_frame_events_new table"

            execute "
                CREATE TABLE report_frame_events_new (
                    id uuid NOT NULL DEFAULT uuid_generate_v4(),
                    source_event_id uuid,
                    stream_id uuid,
                    lesson_id uuid,
                    frame_id uuid,
                    user_id uuid,
                    stream_title character varying(255),
                    lesson_title character varying(255),
                    frame_index integer,
                    frame_text text,
                    event_type character varying(255),
                    editor_template character varying(255),
                    lesson_play_id character varying(255),
                    lesson_frame_id character varying(255),
                    created_at timestamp without time zone,
                    updated_at timestamp without time zone,
                    duration_active double precision,
                    duration_idle double precision,
                    duration_total double precision,
                    estimated_time timestamp without time zone,
                    client_reported_time timestamp without time zone,
                    frame_interaction_duration double precision,
                    lesson_complete boolean
                )"

            puts "\tinserting into report_frame_events_new table"

            execute "
                INSERT INTO report_frame_events_new (
                    SELECT
                        uuid_generate_v4(),
                        cast(events.guid as uuid),
                        lesson_streams.id_new,
                        cast(lessons.lesson_guid as uuid),
                        cast(report_frame_events.frame_guid as uuid),
                        users.id_new,
                        report_frame_events.stream_title,
                        report_frame_events.lesson_title,
                        report_frame_events.frame_index,
                        report_frame_events.frame_text,
                        report_frame_events.event_type,
                        report_frame_events.editor_template,
                        report_frame_events.lesson_play_id,
                        report_frame_events.lesson_frame_id,
                        report_frame_events.created_at,
                        report_frame_events.updated_at,
                        report_frame_events.duration_active,
                        report_frame_events.duration_idle,
                        report_frame_events.duration_total,
                        report_frame_events.estimated_time,
                        report_frame_events.client_reported_time,
                        report_frame_events.frame_interaction_duration,
                        report_frame_events.lesson_complete
                    FROM report_frame_events
                    INNER JOIN events ON report_frame_events.source_event_id = events.id
                    INNER JOIN lesson_streams ON report_frame_events.stream_id = lesson_streams.id
                    INNER JOIN lessons ON report_frame_events.lesson_guid = lessons.lesson_guid
                    INNER JOIN users ON report_frame_events.user_id = users.id
                )";


            puts "\tdropping and renaming events / report_frame_events via new tables"

            execute "DROP TABLE events"
            execute "DROP TABLE report_frame_events"

            execute "ALTER TABLE events_new RENAME TO events"
            execute "ALTER TABLE report_frame_events_new RENAME TO report_frame_events"

            puts "\tadding indices to events"

            execute "ALTER TABLE ONLY events ADD CONSTRAINT events_pkey PRIMARY KEY (id)"
            add_index "events", ["created_at"], name: "index_events_on_created_at", using: :btree
            add_index "events", ["estimated_time", "id"], name: "index_events_on_estimated_time_and_id", using: :btree
            add_index "events", ["event_type"], name: "index_events_on_event_type", using: :btree
            add_index "events", ["user_id", "estimated_time", "id"], name: "user-time-id", using: :btree
            add_index "events", ["user_id"], name: "index_events_on_user_id", using: :btree

            # be sure to create payload JSON indices!
            execute "CREATE INDEX page_load_id ON events USING btree ((((payload ->> 'page_load_id'::text))::character varying), estimated_time, id)"

            puts "\tadding indices to report_frame_events"

            execute "ALTER TABLE ONLY report_frame_events ADD CONSTRAINT report_frame_events_pkey PRIMARY KEY (id)"
            add_index "report_frame_events", ["created_at"], name: "index_report_frame_events_on_created_at", using: :btree
            add_index "report_frame_events", ["estimated_time"], name: "index_report_frame_events_on_estimated_time", using: :btree
            add_index "report_frame_events", ["source_event_id"], name: "index_report_frame_events_on_source_event_id", unique: true, using: :btree
            add_index "report_frame_events", ["user_id"], name: "index_report_frame_events_on_user_id", using: :btree


            # 5. Batch update foreign keys using PK table GUIDs
            puts "**************************** Updating foreign keys"
            columns.each do |column|

                column_table_name, column_name = column.split('.')

                next unless column_table_name != "events" && column_table_name != "report_frame_events"


                puts "\t#{column}"

                # since column name doesn't always easily point to originating PK table, use lookup hash
                pk_table_name = FK_TO_PK_TABLE[column_name]

                if pk_table_name

                    execute_batch_update(column, pk_table_name)

                # Version specialized handling
                elsif pk_table_name.nil? && column_name == 'item_id'

                    { "lessons" => "Lesson", "lesson_streams" => "Lesson::Stream" }.each do |source_table, type|
                        execute_batch_update(column, source_table, type, "item_type")
                    end

                # Tag specialized handling
                elsif pk_table_name.nil? && column_name == 'entity_id'

                    { "lessons" => "Lesson", "lesson_streams" => "Lesson::Stream", "users" => "User" }.each do |source_table, type|
                        next if type == "Lesson"
                        execute_batch_update(column, source_table, type, "entity_type")
                    end

                end # if pk_table_name
            end # columns.each



            # 6. Document Schema Rewrites
            puts "**************************** Rewriting document schemas"

            # generate a list of
            id_to_guid_map = Hash.new { |h, k| h[k] = Digest::MD5.new.update(k.to_s).to_s }
            User.select('id', 'id_new').each do |user|
                id_to_guid_map[user.id] = user.id_new
            end


            puts "\tlesson_streams.chapters: lesson_guids -> lesson_ids, test_out_lesson_guid -> test_out_lesson_id"
            execute "UPDATE lesson_streams SET chapters=(replace(replace(replace(cast(chapters as text), 'lesson_guids', 'lesson_ids'), 'test_out_lesson_guid', 'test_out_lesson_id'), 'lesson_guids', 'lesson_ids')::JSON)"

            puts "\tlessons.content_json: frame_guid -> frame_id, concept_guid -> concept_id"
            replace_frame_content('lessons', 'content_json')

            puts "\tlessons.content_json: \"author_id\" -> UUID value"

            Lesson.find_in_batches(:batch_size => 100) do |lessons|
                lessons.each do |lesson|
                    new_json = lesson.content_json.gsub(/\"author_id\":(\d+)/){ |s,x| "\"author_id\":\"#{id_to_guid_map[$1.to_i]}\"" }
                    lesson.update_column(:content_json, new_json)
                end
            end

            puts "\tcontent_publishers.published_version_object: \"author_id\" -> UUID value"

            ContentPublisher.where("lesson_id IS NOT NULL AND published_version_object IS NOT NULL").find_in_batches(:batch_size => 100) do |published_lessons|
                published_lessons.each do |published_lesson|
                    new_json = published_lesson.published_version_object.gsub(/\"author_id\":(\d+)/){ |s,x| "\"author_id\":\"#{id_to_guid_map[$1.to_i]}\"" }
                    published_lesson.update_column(:published_version_object, new_json)
                end
            end

            puts "\tversions.object: \"author_id\" -> UUID value"

            # PaperTrail has been removed
            # PaperTrail::Version.where("item_type='Lesson' AND object IS NOT NULL").find_in_batches(:batch_size => 100) do |versions|
            #     versions.each do |version|
            #         new_json = version.object.gsub(/\"author_id\":(\d+)/){ |s,x| "\"author_id\":\"#{id_to_guid_map[$1.to_i]}\"" }
            #         version.update_column(:object, new_json)
            #     end
            # end



            ["content_publishers", "versions"].each do |table_name|

                object_column = (table_name == "content_publishers" ? "published_version_object" : "object")
                stream_where_condition = (table_name == "content_publishers" ? "lesson_stream_id IS NOT NULL" : "item_type='Lesson::Stream'")
                lesson_where_condition = (table_name == "content_publishers" ? "lesson_id IS NOT NULL" : "item_type='Lesson'")

                puts "\t#{table_name}.streams: lesson_guids -> lesson_ids, version_guid -> version_id, test_out_lesson_guid -> test_out_lesson_id, ordered_concept_guids -> ordered_concept_ids"
                execute "UPDATE #{table_name} SET #{object_column}=replace(replace(replace(replace(#{object_column}, 'ordered_concept_guids', 'ordered_concept_ids'), 'test_out_lesson_guid', 'test_out_lesson_id'), 'version_guid', 'version_id'), 'lesson_guids', 'lesson_ids') WHERE #{stream_where_condition}"

                puts "\t#{table_name}.lessons: frame_guid -> frame_id, concept_guid -> id"
                replace_frame_content(table_name, object_column)

                puts "\t#{table_name}.*: id -> UUID value"
                pk_clause = (table_name == "content_publishers" ? "COALESCE(#{table_name}.lesson_id, #{table_name}.lesson_stream_id)" : "#{table_name}.item_id")
                execute "UPDATE #{table_name}
                        SET #{object_column}=(
                                SELECT replace(#{table_name}.#{object_column}, (cast(regexp_matches(#{table_name}.#{object_column}, '\\m(id: \\d+)') as text[]))[1], 'id: ' || #{pk_clause} )
                            )"


                puts "\t#{table_name}.*: author_id -> UUID value"
                execute "UPDATE #{table_name}
                        SET #{object_column}=(
                                SELECT
                                    replace(#{table_name}.#{object_column}, (cast(regexp_matches(#{table_name}.#{object_column}, '\\m(author_id: \\d+)') as text[]))[1], 'author_id: ' ||
                                        (

                                            COALESCE(
                                                (SELECT users.id_new FROM users WHERE users.id = ( SELECT (cast(regexp_matches(#{table_name}.#{object_column}, '\\mauthor_id: (\\d+)') as int[]))[1] )),
                                                (cast(md5(( SELECT (cast(regexp_matches(#{table_name}.#{object_column}, '\\mauthor_id: (\\d+)') as text[]))[1] )) as uuid))
                                            )

                                        )
                                    )
                            )"



                puts "\t#{table_name}.streams: image_id -> UUID value"
                execute "UPDATE #{table_name}
                        SET #{object_column}=(
                                SELECT
                                    replace(#{table_name}.#{object_column}, (cast(regexp_matches(#{table_name}.#{object_column}, '\\m(image_id: \\d+)') as text[]))[1], 'image_id: ' ||
                                        (

                                            COALESCE(
                                                (SELECT s3_assets.id_new FROM s3_assets WHERE s3_assets.id = ( SELECT (cast(regexp_matches(#{table_name}.#{object_column}, '\\mimage_id: (\\d+)') as int[]))[1] )),
                                                (cast(md5(( SELECT (cast(regexp_matches(#{table_name}.#{object_column}, '\\mimage_id: (\\d+)') as text[]))[1] )) as uuid))
                                            )

                                        )
                                    )
                            ) WHERE #{table_name}.#{stream_where_condition} AND #{table_name}.#{object_column} ~ 'image_id: \\d+'"


                puts "\t#{table_name}.streams: certificate_image_id -> UUID value"
                execute "UPDATE #{table_name}
                        SET #{object_column}=(
                                SELECT
                                    replace(#{table_name}.#{object_column}, (cast(regexp_matches(#{table_name}.#{object_column}, '\\m(certificate_image_id: \\d+)') as text[]))[1], 'certificate_image_id: ' ||
                                        (

                                            COALESCE(
                                                (SELECT s3_assets.id_new FROM s3_assets WHERE s3_assets.id = ( SELECT (cast(regexp_matches(#{table_name}.#{object_column}, '\\mcertificate_image_id: (\\d+)') as int[]))[1] )),
                                                (cast(md5(( SELECT (cast(regexp_matches(#{table_name}.#{object_column}, '\\mcertificate_image_id: (\\d+)') as text[]))[1] )) as uuid))
                                            )

                                        )
                                    )
                            ) WHERE #{table_name}.#{stream_where_condition} AND #{table_name}.#{object_column} ~ 'certificate_image_id: \\d+'"
            end



            # 7. Write out a list of id mappings, just in case ...   =]
            puts "**************************** Logging ID -> UUID mappings"
            dir = Rails.root.join('tmp', 'id_to_guid_maps')
            FileUtils.mkdir_p(dir)
            table_names.each do |table_name, i|

                next unless table_name != "events" && table_name != "report_frame_events"

                puts "\t#{table_name}"

                guid_column = tables_with_existing_guid_columns[table_name] || "id_new"

                # keep track of mappings once the transaction has succeeded
                f = File.open(dir.join(table_name), 'a+')
                (execute "SELECT id, #{guid_column} FROM #{table_name}").each do |record|
                    f.puts([record['id'], record[guid_column]].join(', '))
                end

            end


            # 8. Drop old _id fields and rename to
            puts "**************************** Converting Primary Keys to UUIDs"
            table_names.each_with_index do |table_name, i|

                next unless table_name != "events" && table_name != "report_frame_events"

                puts "\t#{table_name}"

                guid_column = tables_with_existing_guid_columns[table_name] || "id_new"

                execute "ALTER TABLE #{table_name} ALTER COLUMN id DROP DEFAULT"
                execute "DROP SEQUENCE IF EXISTS #{table_name}_id_seq"

                execute "ALTER TABLE #{table_name} ALTER COLUMN id SET DATA TYPE uuid USING COALESCE(cast(#{guid_column} as uuid), id_new)"
                execute "ALTER TABLE #{table_name} ALTER COLUMN id SET DEFAULT uuid_generate_v4()"
                execute "ALTER TABLE #{table_name} DROP COLUMN id_new"

            end


            # 9. change type of _id columns to uuid
            puts "**************************** Converting Foreign Keys to UUIDs"
            columns.each_with_index do |column, i|

                table_name, column_name = column.split('.')

                next unless table_name != "events" && table_name != "report_frame_events"

                puts "\t#{column}"

                # since column name doesn't always easily point to originating PK table, use lookup hash
                pk_table_name = FK_TO_PK_TABLE[column_name]

                execute "ALTER TABLE #{table_name} ALTER COLUMN #{column_name} SET DATA TYPE uuid USING cast(#{column_name} as uuid)"

                # add foreign keys for any columns ...

                # users can be transient, and we don't want FK on event or versions fields
                if pk_table_name && table_name != "events" && table_name != "versions"

                    if table_name == "report_frame_events" && ['stream_id', 'user_id'].include?(column_name)
                        next
                    end

                    if pk_table_name == "users" && !table_name.match(/progress/)
                        next
                    end

                    add_foreign_key table_name, pk_table_name, column: column_name, primary_key: "id"

                end

            end



            # 10. Remove a completely unused columns
            puts "**************************** Removing unused columns"

            # since lesson id's are now UUIDs, safe to re-point these columns to the PK
            ["multiple_choice_challenge_progress", "multiple_choice_challenge_usage_reports", "lesson_progress"].each do |table_name|

                puts "\t#{table_name}.lesson_guid"

                rename_column table_name, "lesson_guid", "lesson_id"

                execute "ALTER TABLE #{table_name} ALTER COLUMN lesson_id SET DATA TYPE uuid USING cast(lesson_id as uuid)"
            end

            # redundant information, so safe to drop
            remove_column "concept_progress", "concept_guid"
            remove_column "collection_progress", "collection_guid"

            # remove all old GUID columns
            tables_with_existing_guid_columns.each do |key, value|
                next unless key != "events" && key != "report_frame_events"

                puts "\t#{key}.#{value}"

                remove_column key, value
            end


            # 11. updating created_at indexes
            puts "**************************** Adding created_at indexes"
            table_names.each do |table_name|
                puts "\t#{table_name}"
                add_index table_name, :created_at unless index_exists?(table_name, :created_at)
            end


            # 12. Miscellaneous
            puts "**************************** Miscellaneous cleanup"

            rename_column "lesson_streams", "ordered_concept_guids", "ordered_concept_ids"

            execute "UPDATE lesson_progress SET frame_bookmark_guid=NULL WHERE frame_bookmark_guid = '0'"
            execute "ALTER TABLE lesson_progress ALTER COLUMN frame_bookmark_guid SET DATA TYPE uuid USING cast(frame_bookmark_guid as uuid)"
            rename_column "lesson_progress", "frame_bookmark_guid", "frame_bookmark_id"

            execute "ALTER TABLE lesson_streams_progress ALTER COLUMN lesson_bookmark_guid SET DATA TYPE uuid USING cast(lesson_bookmark_guid as uuid)"
            rename_column "lesson_streams_progress", "lesson_bookmark_guid", "lesson_bookmark_id"

            execute "ALTER TABLE lesson_streams ALTER COLUMN version_guid SET DATA TYPE uuid USING cast(version_guid as uuid)"
            rename_column "lesson_streams", "version_guid", "version_id"

            execute "ALTER TABLE lessons ALTER COLUMN version_guid SET DATA TYPE uuid USING cast(version_guid as uuid)"
            rename_column "lessons", "version_guid", "version_id"

            execute "ALTER TABLE content_publishers ALTER COLUMN version_guid SET DATA TYPE uuid USING cast(version_guid as uuid)"
            rename_column "content_publishers", "version_guid", "version_id"

            execute "ALTER TABLE multiple_choice_challenge_progress ALTER COLUMN frame_guid SET DATA TYPE uuid USING cast(frame_guid as uuid)"
            execute "ALTER TABLE multiple_choice_challenge_progress ALTER COLUMN challenge_id SET DATA TYPE uuid USING cast(challenge_id as uuid)"
            rename_column "multiple_choice_challenge_progress", "frame_guid", "frame_id"


            execute "ALTER TABLE multiple_choice_challenge_usage_reports ALTER COLUMN frame_guid SET DATA TYPE uuid USING cast(frame_guid as uuid)"
            execute "ALTER TABLE multiple_choice_challenge_usage_reports ALTER COLUMN challenge_id SET DATA TYPE uuid USING cast(challenge_id as uuid)"
            execute "ALTER TABLE multiple_choice_challenge_usage_reports ALTER COLUMN answer_id SET DATA TYPE uuid USING cast(answer_id as uuid)"
            rename_column "multiple_choice_challenge_usage_reports", "frame_guid", "frame_id"


            # there are less than 2000, so no batching necessary
            # ConceptProgress.all.each do |concept_progress|
            #     if concept_progress.collected_in && concept_progress.collected_in['lesson']['lesson_guid']
            #         concept_progress.collected_in['lesson']['id'] = concept_progress.collected_in['lesson'].delete('lesson_guid')
            #         concept_progress.save!
            #     end
            # end

        end #transaction

        # 13. Vacuum Full
        puts "**************************** Vacuum Full"
        execute "VACUUM FULL"


        # Back to verbose output
        ActiveRecord::Migration.verbose = true

    end




    #####################################
    ## Batch Update
    #####################################


    def execute_batch_update(column, source_table, type = nil, variant_column = nil)
        guid_column = tables_with_existing_guid_columns[source_table] || "id_new"
        column_table_name, column_name = column.split('.')

        variant_clause = ""
        if type && variant_column
            variant_clause = " AND #{variant_column} = '#{type}' "
        end

        # this can be confusing. we want to batch update by pulling a list of all the IDs and mapping them to their appropriate
        # GUID values (using pre-existing guid column or newly generated). However, in the case of some tables (versions), relational
        # integrity is already shot and the FK value no longer exists in the PK tables. So we need to do a UNION for all those missing
        # FK IDs and generate new GUIDS for them. The UNION of these two queries is then fed to the UPDATE which may have the optional
        # variant column support
        batch = "UPDATE #{column_table_name} SET #{column_name}=uuid_lookup.uuid " +
                "FROM (" +
                            "SELECT id, cast(#{guid_column} as text) FROM #{source_table} " +
                                "UNION " +
                            "SELECT cast(#{column_name} as int), cast(uuid_generate_v4() as text) FROM #{column_table_name} WHERE #{column_name} NOT IN (SELECT cast(id as text) FROM #{source_table}) " + variant_clause +
                       ") AS uuid_lookup (id, uuid) " +
                "WHERE #{column} = cast(uuid_lookup.id as text)" + variant_clause

        execute batch
    end


    #####################################
    ## Table / Column Lookups
    #####################################

    def columns

        @columns ||= %w(
            events.user_id
            report_frame_events.stream_id
            report_frame_events.user_id
            report_frame_events.source_event_id
            versions.item_id
            versions.whodunnit
            lesson_progress.user_id
            multiple_choice_challenge_progress.user_id
            concept_progress.concept_id
            concept_progress.user_id
            users_roles.user_id
            users_roles.role_id
            roles.resource_id
            lesson_streams_progress.lesson_stream_id
            lesson_streams_progress.user_id
            lesson_streams_progress.certificate_image_id
            content_publishers.lesson_id
            content_publishers.lesson_stream_id
            lessons.author_id
            collection_progress.collection_id
            collection_progress.user_id
            lesson_to_stream_joins.stream_id
            lesson_to_stream_joins.lesson_id
            lesson_streams_collections.collection_id
            lesson_streams_collections.lesson_stream_id
            concepts.image_id
            lesson_streams.author_id
            lesson_streams.image_id
            lesson_streams.certificate_image_id
            collection_concepts.collection_id
            collection_concepts.concept_id
            institutions_users.institution_id
            institutions_users.user_id
            institutions_groups.institution_id
            institutions_groups.tag_id
            subscriptions_users.user_id
            subscriptions_users.subscription_id
            tags.entity_id
        )


    end


    FK_TO_PK_TABLE = {
        'stream_id' => 'lesson_streams',
        'user_id' => 'users',
        'item_id' =>  nil, # depends on version type
        'concept_id' => 'concepts', # should use the same ones we already have
        'role_id' => 'roles',
        'resource_id' => 'lessons',
        'lesson_stream_id' => 'lesson_streams',
        'certificate_image_id' => 's3_assets',
        'lesson_id' => 'lessons',
        'author_id' => 'users',
        'collection_id' => 'collections',
        'image_id' => 's3_assets',
        'institution_id' => 'institutions',
        'tag_id' => 'tags',
        'subscription_id' => 'subscriptions',
        'entity_id' => nil, # depends on entity_type ... can be ( User, Group, Lesson::Stream, Lesson)
        'source_event_id' => 'events',
        'whodunnit' => 'users'
    }


    def tables_with_existing_guid_columns
        {
            'concepts' => 'concept_guid',
            'collections' => 'collection_guid',
            'events' => 'guid',
            'versions' => 'version_guid',
            'lessons' => 'lesson_guid'
        }
    end


    def table_names
        # skip these tables
        # - users_roles (no primary key)

        %w(
            events
            report_frame_events
            s3_assets
            versions
            lesson_progress
            multiple_choice_challenge_progress
            concept_progress
            multiple_choice_challenge_usage_reports
            tags
            roles
            lesson_streams_progress
            content_publishers
            lessons
            users
            collection_progress
            lesson_to_stream_joins
            lesson_streams_collections
            collections
            concepts
            lesson_streams
            collection_concepts
            institutions_users
            institutions
            subscriptions
            institutions_groups
            subscriptions_users
        )

    end

    def replace_frame_content(table_name, column_name)
        execute "UPDATE #{table_name} SET #{column_name}=replace(replace(replace(#{column_name}, 'concept_guid', 'id'), '\"frame_guid\"', '\"id\"'), 'frame_guid', 'frame_id')"
    end

end
