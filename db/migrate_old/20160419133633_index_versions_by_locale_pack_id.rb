class IndexVersionsByLocalePackId < ActiveRecord::Migration[4.2]
    def change

        add_index :lessons_versions, [:locale_pack_id]
        add_index :lesson_streams_versions, [:locale_pack_id]
        add_index :playlists_versions, [:locale_pack_id]
    end
end
