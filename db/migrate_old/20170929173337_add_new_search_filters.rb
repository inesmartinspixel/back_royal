class AddNewSearchFilters < ActiveRecord::Migration[5.1]
    def change
        add_column :career_profile_searches, :preferred_primary_areas_of_interest, :text, array: true, default: [], null: false
        add_column :career_profile_searches, :industries, :text, array: true, default: [], null: false
    end
end
