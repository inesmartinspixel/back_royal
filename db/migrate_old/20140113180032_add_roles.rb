class AddRoles < ActiveRecord::Migration[4.2]
    # add "official" roles as a migration instead of db:seed, b/c required from now on
    def up
        roles = ['learner', 'editor', 'admin']
        roles.each do |role|
            Role.find_or_create_by(:name => role)
            puts 'Created role: ' << role
        end
    end

    def down
        # todo: does devise already manage the admin role for us? should we never delete it?
        roles = ['learner', 'editor', 'admin']
        roles.each do |role|
            if !Role.where(:name => role).first.nil?
                Role.where(:name => role).first.destroy
            end
        end
    end
end
