class AddDimensionsToS3Assets < ActiveRecord::Migration[4.2]
    def change
    	add_column :s3_assets, :dimensions, :json
    end
end
