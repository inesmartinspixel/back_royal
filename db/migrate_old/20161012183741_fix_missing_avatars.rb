class FixMissingAvatars < ActiveRecord::Migration[4.2]
    def change

        # set lindseyallard2@gmail.com | https://lh5.googleusercontent.com/-HL2oNcVGQS0/AAAAAAAAAAI/AAAAAAAAAHE/WzBeutX0Ayw/photo.jpg
        # set brookegerstein@gmail.com  https://uploads.smart.ly/avatars/2cb12b3635b158b625469d93f4df30d5/original/2cb12b3635b158b625469d93f4df30d5.jpg

        # if the users exist and avatar_url is null

        if user1 = User.find_by_email("lindseyallard2@gmail.com")
            user1.avatar_url = "https://lh5.googleusercontent.com/-HL2oNcVGQS0/AAAAAAAAAAI/AAAAAAAAAHE/WzBeutX0Ayw/photo.jpg"
            user1.avatar_provider = 'google_oauth2'
            user1.save!
        end

        if user2 = User.find_by_email("brookegerstein@gmail.com")
            user2.avatar_url = "https://uploads.smart.ly/avatars/2cb12b3635b158b625469d93f4df30d5/original/2cb12b3635b158b625469d93f4df30d5.jpg"
            user2.avatar_provider = 's3'
            user2.save!
        end
    end
end
