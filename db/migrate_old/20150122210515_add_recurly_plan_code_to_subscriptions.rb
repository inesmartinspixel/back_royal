class AddRecurlyPlanCodeToSubscriptions < ActiveRecord::Migration[4.2]
    def change
        add_column :subscriptions, :recurly_plan_code, :string
    end
end
