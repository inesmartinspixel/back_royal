class AddExamOverrideToCohortApplications < ActiveRecord::Migration[5.1]

    def change

        add_column :cohort_applications, :disable_exam_locking, :boolean
        add_column :cohort_applications_versions, :disable_exam_locking, :boolean

        # Make the new column default false in a safe way
        # ideally we would also make it NOT NULL, but that is not a safe change in a production database:
        #       https://www.braintreepayments.com/blog/safe-operations-for-high-volume-postgresql/#footnote-1
        change_column :cohort_applications, :disable_exam_locking, :boolean, :default => false

    end
end
