class DuplicateEventsTable < ActiveRecord::Migration[5.0]
    def up

        execute('alter table events rename to archived_events')

        execute(%Q~

        CREATE TABLE events (
            id uuid DEFAULT uuid_generate_v4() NOT NULL,
            user_id uuid,
            event_type character varying(255),
            payload json,
            created_at timestamp without time zone,
            updated_at timestamp without time zone,
            estimated_time timestamp without time zone,
            client_reported_time timestamp without time zone,
            hit_server_at timestamp without time zone,
            total_buffered_seconds double precision
        );

        ALTER TABLE ONLY events
            ADD CONSTRAINT events_pkey_new PRIMARY KEY (id);

        -- Doesn't actually exist in current schema
        CREATE INDEX index_events_on_created_at_new ON events USING btree (created_at);

        CREATE INDEX index_events_on_event_type_and_created_at_new ON events USING btree (event_type, created_at);

        CREATE INDEX index_events_on_event_type_and_estimated_time_new ON events USING btree (event_type, estimated_time);

        CREATE INDEX index_events_on_user_id_and_estimated_time_new ON events USING btree (user_id, estimated_time);

        CREATE INDEX index_events_on_user_id_and_event_type_and_created_at_new ON events USING btree (user_id, event_type, created_at);

        CREATE INDEX index_events_on_user_id_and_event_type_and_estimated_time_new ON events USING btree (user_id, event_type, estimated_time);

        CREATE INDEX page_load_id_new ON events USING btree ((((payload ->> 'page_load_id'::text))::character varying), estimated_time, id);

        ~)

    end

    def down
        execute 'drop table events'
        execute('alter table archived_events rename to events')
    end
end
