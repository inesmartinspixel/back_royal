class RemoveDeviseConfirmable < ActiveRecord::Migration[4.2]

    def up
        remove_index :users, :confirmation_token
        remove_columns :users, :confirmation_token, :confirmed_at, :confirmation_sent_at, :unconfirmed_email, :confirm_success_url
    end

    def down
        add_column :users, :confirmation_token, :string
        add_column :users, :confirmed_at, :datetime
        add_column :users, :confirmation_sent_at, :datetime
        add_column :users, :confirm_success_url, :string
        add_column :users, :unconfirmed_email, :string
        add_index :users, :confirmation_token,   :unique => true

        ## Backfill confirmed status
        User.all.each do |user|
            user.update!({:provider => 'email',
                :confirmed_at => DateTime.now, :confirm_success_url => '/', :confirmation_sent_at => DateTime.now,
                :uid => user.email})
        end
    end

end
