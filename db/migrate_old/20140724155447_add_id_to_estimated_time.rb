class AddIdToEstimatedTime < ActiveRecord::Migration[4.2]
	def change
		remove_index :events, :estimated_time
		add_index :events, [:estimated_time, :id]
	end
end
