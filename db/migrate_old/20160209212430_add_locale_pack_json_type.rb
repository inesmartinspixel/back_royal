class AddLocalePackJsonType < ActiveRecord::Migration[4.2]
    def up
        execute "CREATE TYPE locale_pack_json_v1 AS (id uuid, content_items json)"
    end

    def down
        execute "DROP TYPE locale_pack_json_v1"
    end
end