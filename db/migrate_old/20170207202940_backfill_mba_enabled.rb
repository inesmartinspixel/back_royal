class BackfillMbaEnabled < ActiveRecord::Migration[5.0]
    def up
        mba_enabled_sql = "UPDATE users
            SET mba_enabled = true
            FROM access_groups_users, access_groups
            WHERE users.id = access_groups_users.user_id
            AND access_groups_users.access_group_id = access_groups.id
            AND (access_groups.name = 'SMARTER' OR access_groups.name = 'SUPERVIEWER')
            AND users.id NOT IN (SELECT id FROM users JOIN institutions_users ON institutions_users.user_id = users.id)"
        ActiveRecord::Base.connection.execute(mba_enabled_sql)
    end
end