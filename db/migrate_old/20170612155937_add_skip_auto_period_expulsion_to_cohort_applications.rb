class AddSkipAutoPeriodExpulsionToCohortApplications < ActiveRecord::Migration[5.1]
    def change
        add_column :cohort_applications, :skip_period_expulsion, :boolean
        add_column :cohort_applications_versions, :skip_period_expulsion, :boolean

        # Make the new column default false in a safe way
        # ideally we would also make it NOT NULL, but that is not a safe change in a production database:
        #       https://www.braintreepayments.com/blog/safe-operations-for-high-volume-postgresql/#footnote-1
        unless reverting?
            change_column :cohort_applications, :skip_period_expulsion, :boolean, :default => false
        end
    end
end
