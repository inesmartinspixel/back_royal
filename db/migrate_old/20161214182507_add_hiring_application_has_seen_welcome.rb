class AddHiringApplicationHasSeenWelcome < ActiveRecord::Migration[5.0]
    def change
        add_column :hiring_applications_versions, :has_seen_welcome, :boolean
        add_column :hiring_applications, :has_seen_welcome, :boolean, default: false, null: false
    end
end
