require File.expand_path('../componentized_migrators/remove_disable_correct_answers_migrator', __FILE__)

class RemoveDisableCorrectAnswers < ActiveRecord::Migration[4.2]
    def up
        migrator = RemoveDisableCorrectAnswersMigrator.new
        migrator.change
    end
end