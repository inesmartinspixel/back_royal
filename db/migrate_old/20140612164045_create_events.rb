class CreateEvents < ActiveRecord::Migration[4.2]
    def change
        create_table :events do |t|
            t.references :user
            t.string :event_type
            t.integer :client_utc_timestamp
            t.float :client_offset_from_utc
            t.integer :client_seconds_buffered_for
            t.json :payload

            t.timestamps
        end
        add_index(:events, :user_id)
        add_index(:events, :created_at)
        add_index(:events, :client_utc_timestamp)

    end
end
