class AddFrameInteractionDurationToFrameEvents < ActiveRecord::Migration[4.2]
    def change
        add_column :report_frame_events, :frame_interaction_duration, :float
        add_column :report_frame_events, :lesson_complete, :boolean
    end
end
