class AddExamStuffToStream < ActiveRecord::Migration[4.2]
    def up
        add_column :lesson_streams, :exam, :boolean, :null => false, :default => false
        add_column :lesson_streams_versions, :exam, :boolean

        add_column :lesson_streams, :time_limit_hours, :float
        add_column :lesson_streams_versions, :time_limit_hours, :float

        execute "UPDATE lesson_streams_versions SET exam = false"
    end

    def down
        remove_column :lesson_streams, :exam
        remove_column :lesson_streams_versions, :exam
        remove_column :lesson_streams, :time_limit_hours
        remove_column :lesson_streams_versions, :time_limit_hours
    end
end
