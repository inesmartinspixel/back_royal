class AddOptimizelyIdToExperiments < ActiveRecord::Migration[4.2]
    def change

        add_column :experiments, :optimizely_id, :text
        add_column :experiment_variations, :optimizely_id, :text

        add_index :experiments, :optimizely_id, :unique => true
        add_index :experiment_variations, :optimizely_id, :unique => true

    end
end
