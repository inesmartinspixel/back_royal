class BackfillSeoFields < ActiveRecord::Migration[4.2]
    def change
        execute('alter table lessons disable trigger lessons_versions')
        backfill_seo_fields
        execute('alter table lessons enable trigger lessons_versions')
    end

    def backfill_seo_fields
        batch = 0

        # audit table lessons_versions is now no longer directly saveable because it
        # has an id field that is not the primary key, so for the purposes of a data
        # migration, we make a shell class that we use to save changes to the old / previous versions
        lesson_version_klass = Class.new(ActiveRecord::Base) do
            self.table_name = 'lessons_versions'
            self.primary_key = "version_id"
        end

        Lesson.find_in_batches(batch_size: 10) do |lessons|
            puts " ************ Processing lesson batch ##{batch}"

            lessons.each_with_index do |lesson, i|
                puts " ************ Processing lesson #{lesson.title}"

                lesson.seo_title = lesson.title
                lesson.seo_description = lesson.description
                lesson.seo_canonical_url = "/lesson/#{lesson.title.parameterize}/show/#{lesson.id}"
                lesson.save!

                # manually do some of the param cleanup activerecord would have normally done for us, since we
                # need to update in sql
                seo_title = lesson.seo_title.gsub(/\'/, "").gsub(/\(/, "").gsub(/\)/, "") if lesson.seo_title
                seo_description = lesson.seo_description.gsub(/\'/, "").gsub(/\(/, "").gsub(/\)/, "") if lesson.seo_description
                seo_canonical_url = lesson.seo_canonical_url.gsub(/\'/, "").gsub(/\(/, "").gsub(/\)/, "") if lesson.seo_canonical_url

                # migrate all the versions too
                execute("update lessons_versions set (seo_title, seo_description, seo_canonical_url) = ('#{seo_title}', '#{seo_description}', '#{seo_canonical_url}') where id='#{lesson.id}'")

            end

            batch += 1
        end

    end
end
