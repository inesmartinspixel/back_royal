class AddNewStatusesToUserIdIndexOnCohortApplications < ActiveRecord::Migration[5.0]
    def up
        remove_index :cohort_applications, :user_id
        add_index :cohort_applications, :user_id, where: "status in ('accepted', 'pending', 'pre_accepted', 'emba_targeted')", unique: true
    end

    def down
        remove_index :cohort_applications, :user_id
        add_index :cohort_applications, :user_id, where: "status in ('accepted', 'pending')", unique: true
    end
end
