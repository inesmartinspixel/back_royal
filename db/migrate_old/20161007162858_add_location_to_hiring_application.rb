class AddLocationToHiringApplication < ActiveRecord::Migration[4.2]
    def change
        add_column :hiring_applications, :place_id, :text
        add_column :hiring_applications_versions, :place_id, :text
        add_column :hiring_applications, :place_details, :json, :default => {}, :null => false
        add_column :hiring_applications_versions, :place_details, :json
    end
end
