class FilterOutEventsInTheFuture < ActiveRecord::Migration[5.0]
def up

        # update UserLessonProgressRecords
        ViewHelpers.set_versions(self, 20170302155419)

    end

    def down

        ViewHelpers.set_versions(self, 20170302155419 - 1)
    end
end
