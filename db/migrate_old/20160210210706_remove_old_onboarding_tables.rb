class RemoveOldOnboardingTables < ActiveRecord::Migration[4.2]

    def up
        execute "DROP TABLE lesson_streams_onboarding_goals_versions"
        execute "DROP TABLE lesson_streams_onboarding_goals"

        execute "DROP TABLE onboarding_goals_users_versions"
        execute "DROP TABLE onboarding_goals_users"

        execute "DROP TABLE onboarding_goals_versions"
        execute "DROP TABLE onboarding_goals"

        execute "DROP TABLE onboarding_motivations_users_versions"
        execute "DROP TABLE onboarding_motivations_users"

        execute "DROP TABLE onboarding_motivations_versions"
        execute "DROP TABLE onboarding_motivations"

    end

    def down

        # pg_dump back_royal_development -st onboarding_motivations

        execute "CREATE TABLE onboarding_motivations (
                    id uuid DEFAULT uuid_generate_v4() NOT NULL,
                    created_at timestamp without time zone,
                    updated_at timestamp without time zone,
                    description text NOT NULL
                )"


        execute "ALTER TABLE ONLY onboarding_motivations ADD CONSTRAINT onboarding_motivations_pkey PRIMARY KEY (id)"
        execute "CREATE INDEX index_onboarding_motivations_on_created_at ON onboarding_motivations USING btree (created_at)"
        execute "CREATE TRIGGER onboarding_motivations_versions AFTER INSERT OR DELETE OR UPDATE ON onboarding_motivations FOR EACH ROW EXECUTE PROCEDURE process_onboarding_motivations_version()"



        # pg_dump back_royal_development -st onboarding_motivations_versions

        execute "CREATE TABLE onboarding_motivations_versions (
                    version_id uuid DEFAULT uuid_generate_v4(),
                    operation character varying(1) NOT NULL,
                    version_created_at timestamp without time zone NOT NULL,
                    id uuid NOT NULL,
                    created_at timestamp without time zone,
                    updated_at timestamp without time zone,
                    description text NOT NULL
                )"

        execute "CREATE INDEX index_onboarding_motivations_versions_on_id_and_updated_at ON onboarding_motivations_versions USING btree (id, updated_at)"


        # pg_dump back_royal_development -st onboarding_motivations_users

        execute "CREATE TABLE onboarding_motivations_users (
                    onboarding_motivation_id uuid NOT NULL,
                    user_id uuid NOT NULL
                )"


        execute "CREATE UNIQUE INDEX onboarding_motivation_id_user_id ON onboarding_motivations_users USING btree (onboarding_motivation_id, user_id)"
        execute "CREATE TRIGGER onboarding_motivations_users_versions AFTER INSERT OR DELETE OR UPDATE ON onboarding_motivations_users FOR EACH ROW EXECUTE PROCEDURE process_onboarding_motivations_users_version()"
        execute "ALTER TABLE ONLY onboarding_motivations_users ADD CONSTRAINT fk_rails_4f88ed834b FOREIGN KEY (onboarding_motivation_id) REFERENCES onboarding_motivations(id)"
        execute "ALTER TABLE ONLY onboarding_motivations_users ADD CONSTRAINT fk_rails_bb9ec7ccb5 FOREIGN KEY (user_id) REFERENCES users(id)"



        # pg_dump back_royal_development -st onboarding_motivations_users_versions

        execute "CREATE TABLE onboarding_motivations_users_versions (
                    version_id uuid DEFAULT uuid_generate_v4(),
                    operation character varying(1) NOT NULL,
                    version_created_at timestamp without time zone NOT NULL,
                    onboarding_motivation_id uuid NOT NULL,
                    user_id uuid NOT NULL
                )"


        # pg_dump back_royal_development -st onboarding_goals

        execute "CREATE TABLE onboarding_goals (
                    id uuid DEFAULT uuid_generate_v4() NOT NULL,
                    created_at timestamp without time zone,
                    updated_at timestamp without time zone,
                    description text NOT NULL
                )"

        execute "ALTER TABLE ONLY onboarding_goals ADD CONSTRAINT onboarding_goals_pkey PRIMARY KEY (id)"
        execute "CREATE INDEX index_onboarding_goals_on_created_at ON onboarding_goals USING btree (created_at)"
        execute "CREATE TRIGGER onboarding_goals_versions AFTER INSERT OR DELETE OR UPDATE ON onboarding_goals FOR EACH ROW EXECUTE PROCEDURE process_onboarding_goals_version()"



        # pg_dump back_royal_development -st onboarding_goals_versions

        execute "CREATE TABLE onboarding_goals_versions (
                    version_id uuid DEFAULT uuid_generate_v4(),
                    operation character varying(1) NOT NULL,
                    version_created_at timestamp without time zone NOT NULL,
                    id uuid NOT NULL,
                    created_at timestamp without time zone,
                    updated_at timestamp without time zone,
                    description text NOT NULL
                )"

        execute "CREATE INDEX index_onboarding_goals_versions_on_id_and_updated_at ON onboarding_goals_versions USING btree (id, updated_at)"


        # pg_dump back_royal_development -st onboarding_goals_users

        execute "CREATE TABLE onboarding_goals_users (
                    onboarding_goal_id uuid NOT NULL,
                    user_id uuid NOT NULL
                )"

        execute "CREATE UNIQUE INDEX onboarding_goal_id_user_id ON onboarding_goals_users USING btree (onboarding_goal_id, user_id)"
        execute "CREATE TRIGGER onboarding_goals_users_versions AFTER INSERT OR DELETE OR UPDATE ON onboarding_goals_users FOR EACH ROW EXECUTE PROCEDURE process_onboarding_goals_users_version()"
        execute "ALTER TABLE ONLY onboarding_goals_users ADD CONSTRAINT fk_rails_1989bda449 FOREIGN KEY (user_id) REFERENCES users(id)"
        execute "ALTER TABLE ONLY onboarding_goals_users ADD CONSTRAINT fk_rails_3cc7ff2179 FOREIGN KEY (onboarding_goal_id) REFERENCES onboarding_goals(id)"


        # pg_dump back_royal_development -st onboarding_goals_users_versions

        execute "CREATE TABLE onboarding_goals_users_versions (
                    version_id uuid DEFAULT uuid_generate_v4(),
                    operation character varying(1) NOT NULL,
                    version_created_at timestamp without time zone NOT NULL,
                    onboarding_goal_id uuid NOT NULL,
                    user_id uuid NOT NULL
                )"


        # pg_dump back_royal_development -st lesson_streams_onboarding_goals

        execute "CREATE TABLE lesson_streams_onboarding_goals (
                    lesson_stream_id uuid NOT NULL,
                    onboarding_goal_id uuid NOT NULL
                )"
        execute "CREATE UNIQUE INDEX onboarding_goal_id_lesson_stream_id ON lesson_streams_onboarding_goals USING btree (lesson_stream_id, onboarding_goal_id)"
        execute "CREATE TRIGGER lesson_streams_onboarding_goals_versions AFTER INSERT OR DELETE OR UPDATE ON lesson_streams_onboarding_goals FOR EACH ROW EXECUTE PROCEDURE process_lesson_streams_onboarding_goals_version()"
        execute "ALTER TABLE ONLY lesson_streams_onboarding_goals ADD CONSTRAINT fk_rails_c1907964fa FOREIGN KEY (lesson_stream_id) REFERENCES lesson_streams(id)"
        execute "ALTER TABLE ONLY lesson_streams_onboarding_goals ADD CONSTRAINT fk_rails_d389d1766c FOREIGN KEY (onboarding_goal_id) REFERENCES onboarding_goals(id)"



        # pg_dump back_royal_development -st lesson_streams_onboarding_goals_versions

        execute "CREATE TABLE lesson_streams_onboarding_goals_versions (
                version_id uuid DEFAULT uuid_generate_v4(),
                operation character varying(1) NOT NULL,
                version_created_at timestamp without time zone NOT NULL,
                lesson_stream_id uuid NOT NULL,
                onboarding_goal_id uuid NOT NULL
            )"

    end


end
