class FixEntityMetadataType < ActiveRecord::Migration[4.2]
    def up

        # we need to remove the character limit from description,
        # because it does not exist in the actual table
        execute "
            CREATE TYPE entity_metadata_json_v2 AS (
                id uuid,
                title varchar,
                description varchar,
                canonical_url text,
                tweet_template text,
                image json)"

    end

    def down
        execute "DROP TYPE entity_metadata_json_v2"
    end
end
