class AddUnlockedForMbaUsersToLessonStream < ActiveRecord::Migration[4.2]
    def change
        add_column :lesson_streams, :unlocked_for_mba_users, :boolean, :default => false
        add_column :lesson_streams_versions, :unlocked_for_mba_users, :boolean, :default => false
    end
end
