class RemoveKeyTopics < ActiveRecord::Migration[4.2]
    def change

        remove_column :lesson_streams, :key_topics, :json
        remove_column :lesson_streams_versions, :key_topics, :json

    end
end
