class ViewsForMbaScoreQuery < ActiveRecord::Migration[5.0]
    def up

        execute 'CREATE EXTENSION tablefunc' # crosstab

        # queries that returns one row for every published content item
        published_streams
        published_playlists
        published_lessons

        # query that returns one row for every published lesson than is in a published stream,
        # with basic info about both
        published_stream_lessons

        # query that returns one row for every published stream than is in a published playlist,
        # with basic info about both
        published_playlist_streams

        # query that returns one row for every published lesson that is in a published
        # playlist, with basic info about both, as well as the stream that links them.
        published_playlist_lessons

        # queries that returns one row for every published content item
        published_stream_locale_packs
        published_playlist_locale_packs
        published_lesson_locale_packs

        # This one is kind of tricky, and requires some fudging, because lessons are
        # connected to streams, lesson_locale_packs are not connected to stream_locale_packs.
        # That means that, theoretically, the english version of a stream can have a totally
        # different list of lessons than the spanish stream.  In practice, though, we rarely
        # if ever differ on that.  If we did, this table probably wouldn't make sense to even have
        #
        # As it is, it gives on record for each distinct, stream_locale_pack_id/lesson_locale_pack_id
        # in a published stream.  So, if there is an English stream and a Spanish stream that agree
        # on a list of 6 lessons, then there will be 6 rows in this table.  If, however, the two
        # streams disagreed on what the 6th lesson, should be, then you would get 7 records in this
        # table, 5 for the lessons that agree, 1 for the English-only lesson, and 1 for the Spanish-only
        # lesson
        published_stream_lesson_locale_packs

        # query that returns one row for each playlist in each cohort
        cohort_playlist_locale_packs

        # query that returns all of the groups for a cohort, plus ones with
        # the name "#{COHORT_NAME}_EXAM"
        hacked_access_groups_cohorts

        # query that returns one row for each stream that is available
        # to members of a cohort, either because
        # - it is in one of the cohort's playlists
        # - it is in one of the cohort's groups
        # - it is in a group called "#{COHORT_NAME}_EXAM"
        #
        # Each stream will be marked as in_curriculum (in a playlist), exam (exam = true), or
        # elective (neither of the above)
        cohort_stream_locale_packs


        # query that returns one row for each lesson that is available to
        # members of a cohort, (see criteria above in cohort_stream)
        cohort_lesson_locale_packs

        # Same as above, but for institutions instead of cohorts.  Includes any
        # playlist or stream that is linked directly to an institution through access groups
        institution_playlist_locale_packs
        institution_stream_locale_packs
        institution_lesson_locale_packs

        # query that returns one row for each cohort application, with useful information
        # about hwo the status changed over time and whether the user re-applied for another
        # cohort later
        cohort_applications_plus

        # query with one row for each cohort/user/lesson combination, including
        # all lessons that are available to all users in the cohort and where the user
        # has some progress on the lesson.
        # Only includes
        # one row per user/lesson, even if the lesson is in multiple playlists. Only includes
        # users who were at one point accepted into the cohort
        cohort_user_lesson_progress_records

        # query that returns one row for each cohort, with information about the numbers
        # of playlists/streams/lessons of different types
        cohort_content_details

        # query that returns one row for each user for each of the 26 weeks after the cohort_start_date,
        # including all users who were ever accepted into a cohort, with information about what
        # the user did each week
        cohort_user_weeks

        # query that returns one row for each user/cohort combination if that user was ever
        # accepted into the cohort.  Includes columns with information about the users
        # current progress through the available lessons and scores on tests and assessments
        cohort_user_progress_records

        # adds column for each week, basically transposing cohort_user_weeks onto
        # cohort_user_progress_records.  This is only done separately from cohort_user_progress_records
        # because it is slow, so if you don't need it, you can use cohort_user_progress_records itself
        cohort_user_progress_records_with_weeks

        # query that returns one row per cohort, with information about
        # the percentage of students who were accepted, enrolled, graduated, etc
        cohort_conversion_rates


    end

    def published_streams
        execute %Q~
            create view published_streams as
            select
                lesson_streams.id,
                lesson_streams.locale_pack_id,
                lesson_streams_versions.title,
                lesson_streams_versions.version_id,
                lesson_streams_versions.chapters,
                lesson_streams_versions.locale,
                lesson_streams_versions.exam
            from lesson_streams
                join lesson_streams_versions on lesson_streams.id = lesson_streams_versions.id
                join content_publishers on content_publishers.lesson_stream_version_id = lesson_streams_versions.version_id

        ~
    end

    def published_lessons
        execute %Q~
            create view published_lessons as
            select
                lessons.id,
                lessons.locale_pack_id,
                lessons_versions.title,
                lessons_versions.version_id,
                lessons_versions.locale,
                lessons_versions.assessment,
                lessons_versions.test
            from lessons
                join lessons_versions on lessons.id = lessons_versions.id
                join content_publishers on content_publishers.lesson_version_id = lessons_versions.version_id
        ~
    end

    def published_playlists
        execute %Q~
            create view published_playlists as
            select
                playlists.id,
                playlists.locale_pack_id,
                playlists_versions.title,
                playlists_versions.version_id,
                playlists_versions.locale,
                playlists_versions.stream_entries
            from playlists
                join playlists_versions on playlists.id = playlists_versions.id
                join content_publishers on content_publishers.playlist_version_id = playlists_versions.version_id

        ~
    end

    def published_stream_locale_packs
        execute %Q~
            create view published_stream_locale_packs as
            select
                published_streams.locale_pack_id
                , lesson_streams.title -- we use the working english title in case it is not published in english
                , string_agg(published_streams.locale, ',') as locales
                , max(case when published_streams.exam then 1 else 0 end) = 1 as exam
            from published_streams
                join lesson_streams on lesson_streams.locale_pack_id = published_streams.locale_pack_id and lesson_streams.locale='en'
            group by
                published_streams.locale_pack_id
                , lesson_streams.title
        ~
    end

    def published_playlist_locale_packs
        execute %Q~
            create view published_playlist_locale_packs as
            select
                published_playlists.locale_pack_id
                , playlists.title -- we use the working english title in case it is not published in english
                , string_agg(published_playlists.locale, ',') as locales
            from published_playlists
                join playlists on playlists.locale_pack_id = published_playlists.locale_pack_id and playlists.locale='en'
            group by
                published_playlists.locale_pack_id
                , playlists.title
        ~
    end

    def published_lesson_locale_packs
        execute %Q~
            create view published_lesson_locale_packs as
            select
                published_lessons.locale_pack_id
                , lessons.title -- we use the working english title in case it is not published in english
                , string_agg(published_lessons.locale, ',') as locales
                , max(case when published_lessons.test then 1 else 0 end) = 1 as test
                , max(case when published_lessons.assessment then 1 else 0 end) = 1 as assessment
            from published_lessons
                join lessons on lessons.locale_pack_id = published_lessons.locale_pack_id and lessons.locale='en'
            group by
                published_lessons.locale_pack_id
                , lessons.title
        ~
    end

    def published_stream_lessons
        execute %Q|
            create view published_stream_lessons as
            with stream_lessons_1 AS MATERIALIZED (
                select published_streams.*,
                    trim(json_array_elements((json_array_elements(published_streams.chapters)->>'lesson_ids')::json)::text, '\"')::uuid lesson_id
                from published_streams
            )
            , stream_lessons_2 AS MATERIALIZED (
                select
                    coalesce(lessons_versions.test, false) test,
                    coalesce(lessons_versions.assessment, false) assessment,
                    stream_lessons_1.title as stream_title,
                    stream_lessons_1.locale as stream_locale,
                    lessons_versions.title as lesson_title,
                    stream_lessons_1.locale_pack_id as stream_locale_pack_id,
                    stream_lessons_1.version_id as stream_version_id,
                    stream_lessons_1.id as stream_id,
                    published_lessons.locale_pack_id as lesson_locale_pack_id,
                    lessons_versions.version_id as lesson_version_id,
                    lessons_versions.locale as lesson_locale,
                    lessons_versions.id as lesson_id
                from stream_lessons_1
                    join published_lessons on stream_lessons_1.lesson_id = published_lessons.id
                    join lessons_versions on published_lessons.version_id = lessons_versions.version_id
            )
            select * from stream_lessons_2
        |
    end

    def published_stream_lesson_locale_packs
        execute %Q~
            create view published_stream_lesson_locale_packs as
            select
                published_stream_locale_packs.title as stream_title
                , published_lesson_locale_packs.title as lesson_title
                , published_lesson_locale_packs.test as test
                , published_lesson_locale_packs.assessment as assessment
                , published_stream_locale_packs.locale_pack_id as stream_locale_pack_id
                , published_lesson_locale_packs.locale_pack_id as lesson_locale_pack_id
                , published_stream_locale_packs.locales as stream_locales
                , published_lesson_locale_packs.locales as lesson_locales
            from
                published_stream_lessons
                join published_stream_locale_packs on published_stream_locale_packs.locale_pack_id = published_stream_lessons.stream_locale_pack_id
                join published_lesson_locale_packs on published_lesson_locale_packs.locale_pack_id = published_stream_lessons.lesson_locale_pack_id
        ~
    end

    def published_playlist_streams
        execute %Q~
            create view published_playlist_streams as
            with stream_locale_packs AS MATERIALIZED (
                select
                    id as playlist_id
                    , locale_pack_id as playlist_locale_pack_id
                    , title as playlist_title
                    , version_id as playlist_version_id
                    , locale as playlist_locale
                    , (unnest(published_playlists.stream_entries)->>'locale_pack_id')::uuid as stream_locale_pack_id
                from published_playlists
            )
           select
               stream_locale_packs.*
               , case when locale_streams.title is not null then locale_streams.title else en_streams.title end as stream_title
               , case when locale_streams.id is not null then locale_streams.id else en_streams.id end as stream_id
               , case when locale_streams.version_id is not null then locale_streams.version_id else en_streams.version_id end as stream_version_id
               , case when locale_streams.locale is not null then locale_streams.locale else en_streams.locale end as stream_locale
           from stream_locale_packs
               left join published_streams as en_streams on
                   stream_locale_packs.stream_locale_pack_id = en_streams.locale_pack_id
                       and en_streams.locale = 'en'
               left join published_streams as locale_streams on
                   stream_locale_packs.stream_locale_pack_id = locale_streams.locale_pack_id
                       and locale_streams.locale = stream_locale_packs.playlist_locale
        ~
    end

    def published_playlist_lessons
        execute %Q|
            create view published_playlist_lessons as
            select
                published_playlist_streams.*
                , published_stream_lessons.lesson_title
                , published_stream_lessons.lesson_locale_pack_id
                , published_stream_lessons.lesson_version_id
                , published_stream_lessons.lesson_locale as lesson_locale
            from published_playlist_streams
                join published_stream_lessons on published_stream_lessons.stream_id = published_playlist_streams.stream_id
        |
    end

    def cohort_playlist_locale_packs
        execute %Q|
            create view cohort_playlist_locale_packs as
            with cohort_playlist_locale_packs_1 AS MATERIALIZED (
                select
                    cohorts.id as cohort_id,
                    cohorts.name_locales->>'en' as cohort_name,
                    unnest(playlist_pack_ids) as playlist_locale_pack_id
                from cohorts
            )
            , cohort_playlist_locale_packs_2 AS MATERIALIZED (
                select
                    cohort_playlist_locale_packs_1.cohort_id,
                    cohort_playlist_locale_packs_1.cohort_name,
                    published_playlist_locale_packs.locale_pack_id = (select playlist_pack_ids[1] from cohorts where id = cohort_id) as foundations,
                    published_playlist_locale_packs.title as playlist_title,
                    published_playlist_locale_packs.locales as playlist_locales,
                    published_playlist_locale_packs.locale_pack_id as playlist_locale_pack_id
                from cohort_playlist_locale_packs_1
                    join published_playlist_locale_packs on published_playlist_locale_packs.locale_pack_id = cohort_playlist_locale_packs_1.playlist_locale_pack_id
            )

            select * from cohort_playlist_locale_packs_2
        |
    end

    def institution_playlist_locale_packs
        execute %Q|
            create view institution_playlist_locale_packs as
            with step_1 AS MATERIALIZED (
                select
                    institutions.id as institution_id,
                    institutions.name as institution_name,
                    access_groups_playlist_locale_packs.locale_pack_id as playlist_locale_pack_id
                from institutions
                    join access_groups_institutions on access_groups_institutions.institution_id = institutions.id
                    join access_groups_playlist_locale_packs on access_groups_institutions.access_group_id = access_groups_playlist_locale_packs.access_group_id
            )
            select
                step_1.institution_id,
                step_1.institution_name,
                published_playlist_locale_packs.title as playlist_title,
                published_playlist_locale_packs.locales as playlist_locales,
                published_playlist_locale_packs.locale_pack_id as playlist_locale_pack_id
            from step_1
                join published_playlist_locale_packs on published_playlist_locale_packs.locale_pack_id = step_1.playlist_locale_pack_id
            group by
                step_1.institution_id,
                step_1.institution_name,
                published_playlist_locale_packs.title,
                published_playlist_locale_packs.locales,
                published_playlist_locale_packs.locale_pack_id
        |
    end

    def hacked_access_groups_cohorts
        execute %Q~
            create view hacked_access_groups_cohorts as
            select
                access_group_id,
                cohort_id
            from access_groups_cohorts
            union
            select
                access_groups.id access_group_id
                , cohorts.id cohort_id
            from cohorts
                join access_groups
                    on (cohorts.name_locales->>'en')::text || '_EXAM' = access_groups.name
        ~
    end

    def cohort_stream_locale_packs
        execute %Q~
            create view cohort_stream_locale_packs as
            select
                cohorts.id as cohort_id
                , cohorts.name_locales->>'en' as cohort_name
                , max(case when cohort_playlist_locale_packs.foundations then 1 else 0 end) = 1 as foundations
                , sum(case when cohort_playlist_locale_packs.playlist_locale_pack_id is not null then 1 else 0 end) > 0 as in_curriculum
                , case when exam = true then false
                    else sum(case when cohort_playlist_locale_packs.playlist_locale_pack_id is not null and exam = false then 1 else 0 end) = 0
                    end as elective
                , published_stream_locale_packs.exam as exam

                , published_stream_locale_packs.title as stream_title
                , published_stream_locale_packs.locale_pack_id as stream_locale_pack_id
                , published_stream_locale_packs.locales as stream_locales

                , count(distinct cohort_playlist_locale_packs.playlist_locale_pack_id) as playlist_count
            from
                cohorts
                join hacked_access_groups_cohorts on hacked_access_groups_cohorts.cohort_id = cohorts.id
                join access_groups_lesson_stream_locale_packs on access_groups_lesson_stream_locale_packs.access_group_id = hacked_access_groups_cohorts.access_group_id
                join published_stream_locale_packs on published_stream_locale_packs.locale_pack_id = access_groups_lesson_stream_locale_packs.locale_pack_id
                left join published_playlist_streams on published_playlist_streams.stream_locale_pack_id = published_stream_locale_packs.locale_pack_id
                left join cohort_playlist_locale_packs on
                    cohorts.id = cohort_playlist_locale_packs.cohort_id
                    and published_playlist_streams.playlist_locale_pack_id = cohort_playlist_locale_packs.playlist_locale_pack_id

            group by
                cohorts.id
                , cohorts.name_locales->>'en'
                , published_stream_locale_packs.title
                , published_stream_locale_packs.locale_pack_id
                , published_stream_locale_packs.locales
                , published_stream_locale_packs.exam
        ~
    end

    def institution_stream_locale_packs
        execute %Q~
            create view institution_stream_locale_packs as
            select
                institutions.id as institution_id
                , institutions.name as institution_name
                , sum(case when institution_playlist_locale_packs.playlist_locale_pack_id is not null then 1 else 0 end) > 0 as in_playlist
                , published_stream_locale_packs.exam as exam

                , published_stream_locale_packs.title as stream_title
                , published_stream_locale_packs.locale_pack_id as stream_locale_pack_id
                , published_stream_locale_packs.locales as stream_locales

                , count(distinct institution_playlist_locale_packs.playlist_locale_pack_id) as playlist_count
            from
                institutions
                join access_groups_institutions on access_groups_institutions.institution_id = institutions.id
                join access_groups_lesson_stream_locale_packs on access_groups_lesson_stream_locale_packs.access_group_id = access_groups_institutions.access_group_id
                join published_stream_locale_packs on published_stream_locale_packs.locale_pack_id = access_groups_lesson_stream_locale_packs.locale_pack_id
                left join published_playlist_streams on published_playlist_streams.stream_locale_pack_id = published_stream_locale_packs.locale_pack_id
                left join institution_playlist_locale_packs on
                    institutions.id = institution_playlist_locale_packs.institution_id
                    and published_playlist_streams.playlist_locale_pack_id = institution_playlist_locale_packs.playlist_locale_pack_id
            group by
                institutions.id
                , institutions.name
                , published_stream_locale_packs.title
                , published_stream_locale_packs.locale_pack_id
                , published_stream_locale_packs.locales
                , published_stream_locale_packs.exam
        ~
    end

    def cohort_lesson_locale_packs
        execute %Q~
            create view cohort_lesson_locale_packs as
            select
                cohort_stream_locale_packs.*
                , published_stream_lesson_locale_packs.test
                , published_stream_lesson_locale_packs.assessment
                , published_stream_lesson_locale_packs.lesson_title
                , published_stream_lesson_locale_packs.lesson_locale_pack_id
                , published_stream_lesson_locale_packs.lesson_locales
            from cohort_stream_locale_packs
                join published_stream_lesson_locale_packs on published_stream_lesson_locale_packs.stream_locale_pack_id = cohort_stream_locale_packs.stream_locale_pack_id
        ~
    end

    def institution_lesson_locale_packs
        execute %Q~
            create view institution_lesson_locale_packs as
            select
                institution_stream_locale_packs.*
                , published_stream_lesson_locale_packs.test
                , published_stream_lesson_locale_packs.assessment
                , published_stream_lesson_locale_packs.lesson_title
                , published_stream_lesson_locale_packs.lesson_locale_pack_id
                , published_stream_lesson_locale_packs.lesson_locales
            from institution_stream_locale_packs
                join published_stream_lesson_locale_packs on published_stream_lesson_locale_packs.stream_locale_pack_id = institution_stream_locale_packs.stream_locale_pack_id
        ~
    end

    def cohort_content_details
        execute %Q~
            create view cohort_content_details as
            with step_1 AS MATERIALIZED (
                select
                    cohort_lesson_locale_packs.cohort_id
                    , cohort_lesson_locale_packs.cohort_name
                    , array_length(cohorts.playlist_pack_ids, 1) as playlist_count
                    , count(distinct case when cohort_stream_locale_packs.foundations then cohort_stream_locale_packs.stream_locale_pack_id else null end) as foundations_stream_count
                    , count(distinct case when cohort_stream_locale_packs.in_curriculum then cohort_stream_locale_packs.stream_locale_pack_id else null end) as in_curriculum_stream_count
                    , count(distinct case when cohort_stream_locale_packs.exam then cohort_stream_locale_packs.stream_locale_pack_id else null end) as exam_stream_count
                    , count(distinct case when cohort_stream_locale_packs.elective then cohort_stream_locale_packs.stream_locale_pack_id else null end) as elective_stream_count
                    , count(distinct case when cohort_lesson_locale_packs.foundations then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as foundations_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.in_curriculum then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as in_curriculum_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.test then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as test_lesson_count
                    , count(distinct case when cohort_lesson_locale_packs.elective then cohort_lesson_locale_packs.lesson_locale_pack_id else null end) as elective_lesson_count

                    -- duration weeks is the number of weeks from the start to end - 1 week for the final exam
                    , floor(EXTRACT(epoch FROM (cohorts.end_date - cohorts.start_date)) / extract(epoch from interval '7 days')) - 1 duration_weeks
                from
                    cohort_lesson_locale_packs
                    join cohort_stream_locale_packs
                        on cohort_lesson_locale_packs.stream_locale_pack_id = cohort_stream_locale_packs.stream_locale_pack_id
                    join cohorts
                        on cohorts.id = cohort_lesson_locale_packs.cohort_id
                group by
                    cohort_lesson_locale_packs.cohort_id
                    , cohort_lesson_locale_packs.cohort_name
                    , cohorts.playlist_pack_ids
                    , cohorts.end_date
                    , cohorts.start_date
            )
            select
                step_1.*
                , in_curriculum_lesson_count / duration_weeks as expected_pace
                , (in_curriculum_lesson_count - foundations_lesson_count) / duration_weeks as expected_pace_fp
            from step_1
        ~
    end

    def cohort_applications_plus
        execute %Q~
            create view cohort_applications_plus as

            -- first, combine all of the versions for a record into a single record
            with cohort_states AS MATERIALIZED (
                select
                    aps.id
                    , aps.user_id
                    , aps.cohort_id
                    , users.email
                    , cohorts.name_locales->>'en' as cohort_name
                    , cohorts.start_date as cohort_start_date
                    , cohort_applications.applied_at
                    , min(case when aps.status = 'rejected' then version_created_at else null end) rejected_at
                    , min(case when aps.status = 'accepted' then version_created_at else null end) accepted_at
                    , min(case when aps.status = 'deferred' then version_created_at else null end) deferred_at
                    , min(case when aps.status = 'expelled' then version_created_at else null end) expelled_at
                    , max(case when aps.status = 'accepted' then 1 else 0 end) = 1 as was_accepted
                    , cohort_applications.status as current_status
                from cohort_applications_versions aps
                    join cohorts on aps.cohort_id = cohorts.id
                    join cohort_applications on cohort_applications.id = aps.id
                    join external_users on external_users.id = aps.user_id
                    join users on users.id = external_users.id
                group by
                    aps.id,
                    aps.user_id,
                    email,
                    aps.cohort_id,
                    cohorts.name_locales->>'en',
                    cohorts.start_date,
                    cohort_applications.status,
                    cohort_applications.applied_at
            )

            -- then, for each record, look for records for the same user on different cohorts, and
            -- add some info about that
            , cohort_states_2 AS MATERIALIZED (
                select
                    cohort_states.id
                    , cohort_states.user_id
                    , cohort_states.cohort_id
                    , cohort_states.cohort_name
                    , cohort_states.current_status
                    , cohort_states.applied_at
                    , cohort_states.rejected_at
                    , cohort_states.accepted_at
                    , cohort_states.deferred_at
                    , case when cohort_states.deferred_at is null then cohort_states.expelled_at else null end as expelled_at -- if someone was expelled then deferred, count them as only deferred, not expelled
                    , cohort_states.was_accepted
                    , string_agg(case when later_cohort_states.cohort_name is not null then later_cohort_states.cohort_name || ':' || later_cohort_states.current_status else null end, ',') as applied_to_later
                    , string_agg(case when previous_cohort_states.cohort_name is not null then previous_cohort_states.cohort_name || ':' || previous_cohort_states.current_status else null end, ',') as applied_to_previous
                    , cohort_states.expelled_at is not null and cohort_states.deferred_at is not null as expelled_then_deferred
                from cohort_states
                    left join cohort_states as later_cohort_states
                        on cohort_states.user_id = later_cohort_states.user_id and later_cohort_states.cohort_start_date > cohort_states.cohort_start_date
                    left join cohort_states as previous_cohort_states
                        on cohort_states.user_id = previous_cohort_states.user_id and previous_cohort_states.cohort_start_date < cohort_states.cohort_start_date
                group by
                    cohort_states.id
                    , cohort_states.user_id
                    , cohort_states.cohort_id
                    , cohort_states.cohort_name
                    , cohort_states.current_status
                    , cohort_states.applied_at
                    , cohort_states.rejected_at
                    , cohort_states.accepted_at
                    , cohort_states.deferred_at
                    , cohort_states.expelled_at
                    , cohort_states.was_accepted
            )
            select * from cohort_states_2
        ~
    end

    def cohort_user_lesson_progress_records
        execute %Q|
            create view cohort_user_lesson_progress_records as
            select
                cohort_applications_plus.cohort_id
                , cohort_applications_plus.cohort_name
                , cohort_applications_plus.user_id as user_id
                , cohort_applications_plus.current_status
                , cohort_lesson_locale_packs.in_curriculum
                , cohort_lesson_locale_packs.assessment
                , cohort_lesson_locale_packs.test
                , cohort_lesson_locale_packs.elective
                , cohort_lesson_locale_packs.foundations
                , cohort_lesson_locale_packs.lesson_title
                , cohort_lesson_locale_packs.lesson_locale_pack_id
                , cohort_lesson_locale_packs.playlist_count
                , lpr.started_at
                , lpr.completed_at
                , lpr.total_lesson_time
                , lpr.last_lesson_activity_time
                , lpr.lesson_finish_count
                , lpr.average_assessment_score_first
                , lpr.average_assessment_score_last
                , lpr.total_lesson_time_on_desktop
                , lpr.total_lesson_time_on_mobile_app
                , lpr.total_lesson_time_on_mobile_web
                , lpr.total_lesson_time_on_unknown
                , lpr.completed_on_client_type
                , lpr.lesson_reset_count
                , lpr.last_lesson_reset_at
            from cohort_lesson_locale_packs
                join cohort_applications_plus
                    on cohort_applications_plus.cohort_id = cohort_lesson_locale_packs.cohort_id and cohort_applications_plus.was_accepted = true
                join etl.user_lesson_progress_records lpr
                    on lpr.locale_pack_id = cohort_lesson_locale_packs.lesson_locale_pack_id and lpr.user_id = cohort_applications_plus.user_id

            group by
                cohort_applications_plus.cohort_id
                , cohort_applications_plus.cohort_name
                , cohort_applications_plus.user_id
                , cohort_applications_plus.current_status
                , cohort_lesson_locale_packs.in_curriculum
                , cohort_lesson_locale_packs.assessment
                , cohort_lesson_locale_packs.test
                , cohort_lesson_locale_packs.elective
                , cohort_lesson_locale_packs.lesson_title
                , cohort_lesson_locale_packs.lesson_locale_pack_id
                , cohort_lesson_locale_packs.foundations
                , cohort_lesson_locale_packs.playlist_count
                , lpr.started_at
                , lpr.completed_at
                , lpr.total_lesson_time
                , lpr.last_lesson_activity_time
                , lpr.lesson_finish_count
                , lpr.average_assessment_score_first
                , lpr.average_assessment_score_last
                , lpr.total_lesson_time_on_desktop
                , lpr.total_lesson_time_on_mobile_app
                , lpr.total_lesson_time_on_mobile_web
                , lpr.total_lesson_time_on_unknown
                , lpr.completed_on_client_type
                , lpr.lesson_reset_count
                , lpr.last_lesson_reset_at
        |
    end

    def cohort_user_progress_records
        execute %Q|
            create or replace view cohort_user_progress_records as
            with with_counts AS MATERIALIZED (
                select
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , cohort_user_lesson_progress_records.user_id
                    , cohort_applications_plus.current_status
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            when foundations = true and completed_at is null then 0
                            else null
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when in_curriculum = true and completed_at is not null then 1
                            when in_curriculum = true and completed_at is null then 0
                            else null
                            end
                        ) as curriculum_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            when test = true and completed_at is null then 0
                            else null
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            when elective = true and completed_at is null then 0
                            else null
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_last else null end) as average_assessment_score_last
                    , avg(case when test then average_assessment_score_first else null end) as avg_test_score
                    , max(case when in_curriculum = true then cohort_user_lesson_progress_records.completed_at else null end) as last_curriculum_lesson_completed_at
                    , min(case when in_curriculum = true then cohort_user_lesson_progress_records.completed_at else null end) as first_curriculum_lesson_completed_at
                from cohort_user_lesson_progress_records
                    join cohort_applications_plus on
                        cohort_applications_plus.cohort_id = cohort_user_lesson_progress_records.cohort_id
                        and cohort_applications_plus.user_id = cohort_user_lesson_progress_records.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , cohort_user_lesson_progress_records.user_id
                    , cohort_applications_plus.current_status
            )
            , with_averages AS MATERIALIZED (
                select
                    with_counts.*
                    , case
                        when with_counts.cohort_name='MBA1'
                            then (
                                0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                + 0.3*(case when average_assessment_score_last is null then 0 else average_assessment_score_last end)
                            )
                        else
                            -- FIXME: eventually we want to have this logic work for everyone
                            null
                        end as final_score
                    , foundations_lessons_complete::float / cohort_content_details.foundations_lesson_count as foundations_perc_complete
                    , curriculum_lessons_complete::float / cohort_content_details.in_curriculum_lesson_count as curriculum_perc_complete
                    , test_lessons_complete::float / cohort_content_details.test_lesson_count as test_perc_complete
                    , elective_lessons_complete::float / cohort_content_details.elective_lesson_count as elective_perc_complete
                from with_counts
                    join cohort_content_details on with_counts.cohort_id = cohort_content_details.cohort_id
            )
            , with_grad_requirements AS MATERIALIZED (
                select
                    with_averages.*
                    , final_score > 0.7 and curriculum_perc_complete = 1 as meets_graduation_requirements
                from with_averages
            )
            select * from with_grad_requirements
        |
    end

    def cohort_user_progress_records_with_weeks
        execute %Q|
            create view cohort_user_progress_records_with_weeks as
            with flattened_weeks AS MATERIALIZED (
                SELECT * FROM crosstab(
                  $$SELECT cohort_id, user_id, rn, lessons_completed_in_week
                     FROM  (
                        select
                            cohort_user_weeks.cohort_id
                            , cohort_user_weeks.user_id
                            , lessons_completed_in_week
                            , row_number() OVER (PARTITION BY cohort_user_weeks.user_id
                                           ORDER BY week_index ASC NULLS LAST) AS rn
                        from cohort_user_weeks
                        order by
                            cohort_user_weeks.cohort_id
                            , cohort_user_weeks.user_id
                            , week_index
                        ) sub
                     ORDER  BY user_id
                  $$
                  , 'VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(23),(24),(25),(26)'
                  ) AS t (
                    cohort_id uuid
                    , user_id uuid
                    , week1 int
                    , week2 int
                    , week3 int
                    , week4 int
                    , week5 int
                    , week6 int
                    , week7 int
                    , week8 int
                    , week9 int
                    , week10 int
                    , week11 int
                    , week12 int
                    , week13 int
                    , week14 int
                    , week15 int
                    , week16 int
                    , week17 int
                    , week18 int
                    , week19 int
                    , week20 int
                    , week21 int
                    , week22 int
                    , week23 int
                    , week24 int
                    , week25 int
                    , week26 int
                  )
            )
            select
                cohort_user_progress_records.cohort_name
                , current_status
                , curriculum_lessons_complete
                , curriculum_perc_complete
                , test_lessons_complete
                , test_perc_complete
                , elective_lessons_complete
                , elective_perc_complete
                , average_assessment_score_first
                , average_assessment_score_last
                , avg_test_score
                , flattened_weeks.*
            from cohort_user_progress_records
                join flattened_weeks on
                    cohort_user_progress_records.user_id = flattened_weeks.user_id
                    and cohort_user_progress_records.cohort_id = flattened_weeks.cohort_id
        |
    end

    # def mba1_final_scores
    #     execute %Q|
    #         create view mba1_final_scores as
    #         select
    #             email
    #             , (
    #                 0.7*(case when avg_test_score is null then 0 else avg_test_score end)
    #                 + 0.3*(case when average_assessment_score_last is null then 0 else average_assessment_score_last end)
    #             ) as final_score
    #             , cohort_user_progress_records.*
    #         from cohort_user_progress_records
    #             join users on user_id = users.id
    #         where
    #             current_status='accepted'
    #             and cohort_name='MBA1'
    #         order by
    #             test_lessons_complete desc,
    #             curriculum_lessons_complete desc
    #     |
    # end

    def cohort_user_weeks
        execute %Q~
            create or replace view cohort_user_weeks as
            select
                cohort_applications_plus.user_id
                , cohort_applications_plus.cohort_id
                , cohort_applications_plus.cohort_name
                , week_index
                , case when week_index = 0 then '2000/01/01'::timestamp else cohorts.start_date + (week_index || ' weeks')::interval end as week_start
                , cohorts.start_date + ((week_index+1) || ' weeks')::interval as week_end

                , count(
                    case when
                        in_curriculum = true
                        and completed_at > (case when week_index = 0 then '2000/01/01'::timestamp else cohorts.start_date + (week_index || ' weeks')::interval end)
                        and completed_at <= (cohorts.start_date + ((week_index+1) || ' weeks')::interval )
                        then true
                    else null end
                ) as lessons_completed_in_week
                , count(case when in_curriculum = true and completed_at <= (cohorts.start_date + ((week_index+1) || ' weeks')::interval) then true else null end) as lessons_completed_by_end_of_week
                , case
                    when cohort_applications_plus.deferred_at < (cohorts.start_date + ((week_index+1) || ' weeks')::interval) then 'deferred'
                    when cohort_applications_plus.expelled_at < (cohorts.start_date + ((week_index+1) || ' weeks')::interval) then 'expelled'
                    else 'active'
                end as status_at_week_end
                , week_index*cohort_content_details.expected_pace as expected_curriculum_lessons_complete
                , cohort_content_details.foundations_lesson_count + week_index*cohort_content_details.expected_pace_fp as expected_curriculum_lessons_complete_fp
            from
                cohort_applications_plus
                    cross join (
                        SELECT
                            week_index
                        FROM
                            generate_series(0, 26) AS week_index
                    ) user_weeks
                    join cohorts
                        on cohort_applications_plus.cohort_id = cohorts.id
                    join cohort_content_details
                        on cohort_content_details.cohort_id = cohorts.id
                    left join cohort_user_lesson_progress_records lp
                        on lp.user_id = cohort_applications_plus.user_id
                        and lp.cohort_id = cohort_applications_plus.cohort_id
            where
                cohort_applications_plus.was_accepted = true
            group by
                cohort_applications_plus.user_id
                , cohort_applications_plus.cohort_id
                , cohort_applications_plus.cohort_name
                , cohort_applications_plus.deferred_at
                , cohort_applications_plus.expelled_at
                , week_index
                , cohorts.start_date
                , cohorts.end_date
                , cohort_content_details.expected_pace
                , cohort_content_details.expected_pace_fp
                , cohort_content_details.foundations_lesson_count
            having
                cohorts.start_date + (week_index || ' weeks')::interval < now()
        ~
    end

    def cohort_conversion_rates
        execute %Q~
            create view cohort_conversion_rates as
            with step_1 AS MATERIALIZED (
                select
                    aps.cohort_id
                    , aps.cohort_name
                    , count(*) as num_applied
                    , count(case when aps.was_accepted = true then true else null end) as num_accepted

                    -- enrolled is defined as being accepted, completing Business Foundations, AND completing at least one lesson after being accepted.
                    , count(case when
                        (aps.accepted_at is not null
                            and cohort_user_progress_records.last_curriculum_lesson_completed_at > aps.accepted_at
                            and cohort_user_progress_records.foundations_perc_complete = 1
                        ) then true
                        else null
                        end) as num_enrolled
                    , count(case when
                        (aps.accepted_at is not null
                            and cohort_user_progress_records.last_curriculum_lesson_completed_at > aps.accepted_at
                            and cohort_user_progress_records.foundations_perc_complete = 1
                            and aps.deferred_at is null
                        ) then true
                        else null
                        end) as num_enrolled_not_deferred
                    , count(case when
                        (aps.accepted_at is not null
                            and cohort_user_progress_records.last_curriculum_lesson_completed_at > aps.accepted_at
                            and cohort_user_progress_records.foundations_perc_complete = 1
                            and aps.deferred_at is not null
                        ) then true
                        else null
                        end) as num_enrolled_and_deferred
                    , count(case when aps.current_status = 'graduated' then true else null end) as num_graduated
                    , count(case when cohort_user_progress_records.meets_graduation_requirements = true then true else null end) as num_meets_grad_req
                from cohort_applications_plus aps
                    left join cohort_user_progress_records
                        on aps.user_id = cohort_user_progress_records.user_id
                        and aps.cohort_id = cohort_user_progress_records.cohort_id
                group by
                    aps.cohort_id
                    , aps.cohort_name
                order by aps.cohort_name
            )
            , step_2 AS MATERIALIZED (
                select
                    cohort_id
                    , cohort_name
                    , num_accepted / (case when num_applied = 0 then 1 else num_applied end)::float as acceptance_rate

                    -- yield is defined AS MATERIALIZED (#enrolled - #enrolled_and_deferred) / (#accepted - #enrolled_and_deferred)
                    , num_enrolled_not_deferred / (case when (num_accepted - num_enrolled_and_deferred) = 0 then 1 else (num_accepted - num_enrolled_and_deferred) end)::float as yield

                    -- graduation rate is defined as #graduated / (#enrolled - #enrolled_and_deferred)
                    , num_graduated / (case when num_enrolled_not_deferred = 0 then 1 else num_enrolled_not_deferred end)::float  as graduation_rate
                    , num_meets_grad_req / (case when num_enrolled_not_deferred = 0 then 1 else num_enrolled_not_deferred end)::float  as meets_grad_req_rate
                    , step_1.num_applied
                    , step_1.num_accepted
                    , step_1.num_enrolled
                    , step_1.num_enrolled_not_deferred
                    , step_1.num_enrolled_and_deferred
                    , step_1.num_graduated
                    , step_1.num_meets_grad_req
                from step_1
            )
            select * from step_2
        ~
    end

    def down

        execute "DROP VIEW IF EXISTS cohort_conversion_rates"
        execute "DROP VIEW IF EXISTS cohort_user_weeks"
        execute "DROP VIEW IF EXISTS cohort_user_progress_records_with_weeks"
        execute "DROP VIEW IF EXISTS cohort_user_progress_records"
        execute "DROP VIEW IF EXISTS cohort_user_lesson_progress_records"
        execute "DROP VIEW IF EXISTS cohort_content_details"
        execute "DROP VIEW IF EXISTS cohort_applications_plus"
        execute 'DROP VIEW IF EXISTS institution_lesson_locale_packs'
        execute 'DROP VIEW IF EXISTS institution_stream_locale_packs'
        execute 'DROP VIEW IF EXISTS institution_playlist_locale_packs'
        execute 'DROP VIEW IF EXISTS cohort_lesson_locale_packs'
        execute 'DROP VIEW IF EXISTS cohort_stream_locale_packs'
        execute 'DROP VIEW IF EXISTS hacked_access_groups_cohorts'
        execute 'DROP VIEW IF EXISTS cohort_playlist_locale_packs'
        execute 'DROP VIEW IF EXISTS published_playlist_lessons'
        execute 'DROP VIEW IF EXISTS published_stream_lesson_locale_packs'
        execute 'DROP VIEW IF EXISTS published_stream_lessons'
        execute 'DROP VIEW IF EXISTS published_playlist_streams'
        execute 'DROP VIEW IF EXISTS published_playlist_locale_packs'
        execute 'DROP VIEW IF EXISTS published_lesson_locale_packs'
        execute 'DROP VIEW IF EXISTS published_stream_locale_packs'
        execute 'DROP VIEW IF EXISTS published_playlists'
        execute 'DROP VIEW IF EXISTS published_lessons'
        execute 'DROP VIEW IF EXISTS published_streams'
        execute "DROP EXTENSION IF EXISTS tablefunc"
    end
end
