class CleanupAfterContentLocalizationStuff < ActiveRecord::Migration[4.2]
    def up

        drop_table :access_groups_lesson_streams, force: :cascade
        drop_table :access_groups_playlists, force: :cascade

        ##################################################################

        temporarily_delete_user_views do

            remove_column :users, :active_playlist_id, :uuid
            remove_column :users_versions, :active_playlist_id, :uuid

            create_trigger('users', [
                'email',
                'encrypted_password',
                'reset_password_token',
                'reset_password_sent_at',
                'first_name',
                'last_name',
                'sign_up_code',
                'reset_password_redirect_url',
                'provider',
                'uid',
                'subscriptions_enabled',
                'notify_email_daily',
                'notify_email_content',
                'notify_email_features',
                'notify_email_reminders',
                'notify_email_newsletter',
                'free_trial_started',
                'confirmed_profile_info',
                'optimizely_referer',
                'active_playlist_locale_pack_id'
                # the json columns must be left out of here, since they can't be compared
            ])
        end

        ##################################################################

        drop_table :lesson_streams_users


        ##################################################################

        remove_column :lesson_progress, :lesson_id
        remove_column :lesson_streams_progress, :lesson_stream_id
        change_column :lesson_progress, :locale_pack_id, :uuid, null: false
        change_column :lesson_streams_progress, :locale_pack_id, :uuid, null: false


    end

    def down

        ["playlists", "lesson_streams"].each do |table_name|

            item_name = table_name.singularize
            join_table = "access_groups_#{table_name}"

            # Create primary table
            create_table    join_table, id: :uuid do |t|
                t.uuid      "access_group_id"
                t.uuid      "#{item_name}_id"
            end

            add_foreign_key join_table, "access_groups", column: "access_group_id", primary_key: "id"
            add_foreign_key join_table, "#{table_name}", column: "#{item_name}_id", primary_key: "id"

            add_index(join_table, [ "access_group_id", "#{item_name}_id" ], unique: true, name: "index_access_groups_and_#{table_name}")

            # Create version table and triggers
            create_versions_table_and_trigger(join_table)
        end


        ##################################################################

        temporarily_delete_user_views do

            add_column :users, :active_playlist_id, :uuid
            add_column :users_versions, :active_playlist_id, :uuid

            add_foreign_key :users, :playlists, column: "active_playlist_id", primary_key: "id"

            create_trigger('users', [
                'email',
                'encrypted_password',
                'reset_password_token',
                'reset_password_sent_at',
                'first_name',
                'last_name',
                'sign_up_code',
                'reset_password_redirect_url',
                'provider',
                'uid',
                'subscriptions_enabled',
                'notify_email_daily',
                'notify_email_content',
                'notify_email_features',
                'notify_email_reminders',
                'notify_email_newsletter',
                'free_trial_started',
                'confirmed_profile_info',
                'optimizely_referer',
                'active_playlist_id',
                'active_playlist_locale_pack_id'
                # the json columns must be left out of here, since they can't be compared
            ])
        end

        ##################################################################

        create_table :lesson_streams_users, id: false do |t|
            t.uuid      :user_id,           null: false
            t.uuid      :lesson_stream_id,  null: false
        end

        add_index(:lesson_streams_users, [:lesson_stream_id, :user_id], :unique => true)
        add_foreign_key "lesson_streams_users", "lesson_streams", column: "lesson_stream_id", primary_key: "id"
        add_foreign_key "lesson_streams_users", "users", column: "user_id", primary_key: "id"

        ##################################################################

        add_column :lesson_progress, :lesson_id, :uuid
        add_column :lesson_streams_progress, :lesson_stream_id, :uuid
        change_column :lesson_progress, :locale_pack_id, :uuid, null: true
        change_column :lesson_streams_progress, :locale_pack_id, :uuid, null: true

    end

    ################################
    ## Helper Methods
    ################################



    def create_versions_table_and_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.connection.schema_cache.clear!
        klass.reset_column_information

        # create the version table, including all columns from the source table
        # plus a few extras
        create_table "#{table_name}_versions", id: false, force: :cascade do |t|
            t.uuid     "version_id", null: false, default: "uuid_generate_v4()"
            t.string   "operation", limit: 1, null: false
            t.datetime "version_created_at", null: false

            puts "*** #{table_name}_versions"

            klass.column_names.each do |column_name|
                type = klass.columns_hash

                column_config = klass.columns_hash[column_name]
                meth = column_config.type.to_sym
                options = {}
                options[:limit] = column_config.limit unless column_config.limit.nil?
                options[:null] = false if column_config.null == false
                options[:array] = column_config.array
                # ignore defaults, since they should not be used in the audit table

                puts "t.#{meth}, #{column_name}, #{options.inspect}"
                t.send(meth, column_name, options)
            end
        end

        # for some reason the primary flag on create_table was not working
        execute "ALTER TABLE #{table_name}_versions ADD PRIMARY KEY (version_id);"

        # create indexes
        if klass.column_names.include?('id') && klass.column_names.include?('updated_at')
            add_index "#{table_name}_versions", [:id, :updated_at]
        elsif klass.column_names.include?('id')
            add_index "#{table_name}_versions", :id
        end

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "
        execute(trigger_sql)
    end

    def drop_versions_table_and_trigger(table_name)
        drop_table "#{table_name}_versions"
        execute "DROP TRIGGER #{table_name}_versions ON #{table_name}"
    end

    def create_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end

    def temporarily_delete_user_views(&block)
        # this used to do something, but no more
        yield
    end

end
