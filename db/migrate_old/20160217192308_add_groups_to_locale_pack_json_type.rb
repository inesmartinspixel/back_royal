class AddGroupsToLocalePackJsonType < ActiveRecord::Migration[4.2]
    def up
        execute "CREATE TYPE locale_pack_json_v2 AS (id uuid, content_items json, groups json)"
    end

    def down
        execute "DROP TYPE locale_pack_json_v2"
    end
end