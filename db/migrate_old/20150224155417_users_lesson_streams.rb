class UsersLessonStreams < ActiveRecord::Migration[4.2]

    def change

        ###############################
        ##  Users <-> Streams Lookup
        ###############################

        create_table :lesson_streams_users, id: false do |t|
            t.uuid      :user_id,           null: false
            t.uuid      :lesson_stream_id,  null: false
        end

        add_index(:lesson_streams_users, [:lesson_stream_id, :user_id], :unique => true)
        add_foreign_key "lesson_streams_users", "lesson_streams", column: "lesson_stream_id", primary_key: "id"
        add_foreign_key "lesson_streams_users", "users", column: "user_id", primary_key: "id"


        ###############################
        ##  Migrate Existing Users
        ###############################

        execute "INSERT INTO lesson_streams_users (user_id, lesson_stream_id) (
                    (SELECT ogu.user_id, lsog.lesson_stream_id FROM onboarding_goals_users ogu INNER JOIN lesson_streams_onboarding_goals lsog ON ogu.onboarding_goal_id = lsog.onboarding_goal_id GROUP BY ogu.user_id, lsog.lesson_stream_id ORDER BY ogu.user_id)
                        UNION
                    (SELECT lsp.user_id, lsp.lesson_stream_id FROM lesson_streams_progress lsp GROUP BY lsp.user_id, lsp.lesson_stream_id ORDER BY lsp.user_id)
                )"

        ###############################
        ##  Misc Schema Cleanup
        ###############################


        # Clean up onboarding tables by adding not-null constraints
        change_column :onboarding_motivations, :description, :text, null: false
        change_column :onboarding_motivations_users, :onboarding_motivation_id, :uuid, null: false
        change_column :onboarding_motivations_users, :user_id, :uuid, null: false
        change_column :onboarding_goals, :description, :text, null: false
        change_column :onboarding_goals_users, :onboarding_goal_id, :uuid, null: false
        change_column :onboarding_goals_users, :user_id, :uuid, null: false
        change_column :lesson_streams_onboarding_goals, :lesson_stream_id, :uuid, null: false
        change_column :lesson_streams_onboarding_goals, :onboarding_goal_id, :uuid, null: false

        # add missing FK for users
        add_foreign_key "institutions_users", "users", column: "user_id", primary_key: "id"
        add_foreign_key "subscriptions_users", "users", column: "user_id", primary_key: "id"
        add_foreign_key "users_roles", "users", column: "user_id", primary_key: "id"

        # drop unnecessary lookup table ids
        remove_column :institutions_groups,         :id
        remove_column :institutions_users,          :id
        remove_column :lesson_streams_collections,  :id
        remove_column :subscriptions_users,         :id

        # drop unnecesssary timestamps from lookup tables
        remove_column :institutions_groups,              :created_at
        remove_column :institutions_users,               :created_at
        remove_column :lesson_streams_collections,       :created_at
        remove_column :lesson_streams_onboarding_goals,  :created_at
        remove_column :lesson_to_stream_joins,           :created_at
        remove_column :onboarding_goals_users,           :created_at
        remove_column :onboarding_motivations_users,     :created_at
        remove_column :subscriptions_users,              :created_at

        remove_column :institutions_groups,              :updated_at
        remove_column :institutions_users,               :updated_at
        remove_column :lesson_streams_collections,       :updated_at
        remove_column :lesson_streams_onboarding_goals,  :updated_at
        remove_column :lesson_to_stream_joins,           :updated_at
        remove_column :onboarding_goals_users,           :updated_at
        remove_column :onboarding_motivations_users,     :updated_at
        remove_column :subscriptions_users,              :updated_at


    end

end
