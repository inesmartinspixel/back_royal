class AddTypesForStreamJson < ActiveRecord::Migration[4.2]
    def up

        execute "CREATE TYPE user_json_v1 AS (id uuid, name varchar(256))"
        execute "CREATE TYPE image_json_v1 AS (id uuid, formats json, dimensions json)"
        execute "CREATE TYPE collection_progress_json_v1 AS (started_at int, updated_at int)"
        execute "CREATE TYPE concept_progress_json_v1 AS (collected_at int, collected_in json)"
        execute "CREATE TYPE client_requirements_json_v1 AS (min_allowed_version int, supported_in_latest_available_version bool)"
        execute "CREATE TYPE lesson_progress_json_v1 AS (
            lesson_id uuid,
            frame_bookmark_id uuid,
            frame_history json,
            started_at int,
            completed_at int,
            complete boolean,
            last_progress_at int,
            id uuid
        )"

    end

    def down
        execute "DROP TYPE user_json_v1"
        execute "DROP TYPE image_json_v1"
        execute "DROP TYPE collection_progress_json_v1"
        execute "DROP TYPE concept_progress_json_v1"
        execute "DROP TYPE client_requirements_json_v1"
        execute "DROP TYPE lesson_progress_json_v1"
    end


end
