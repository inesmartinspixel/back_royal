class ChangeFreeTrialToProTrialInColNames < ActiveRecord::Migration[4.2]
    def change
        remove_column :subscriptions, :free_trial_start_date, :datetime
        remove_column :subscriptions, :free_trial_end_date, :datetime

        add_column :subscriptions, :pro_trial_start_date, :datetime
        add_column :subscriptions, :pro_trial_end_date, :datetime
    end
end
