class UnUniqueIndexOnHiringRelationships < ActiveRecord::Migration[4.2]
    def up
        remove_index(:hiring_relationships, [:candidate_id])
        add_index(:hiring_relationships, [:candidate_id])
    end

    def down
        remove_index(:hiring_relationships, [:candidate_id])
        add_index(:hiring_relationships, [:candidate_id], :unique => true)
    end
end
