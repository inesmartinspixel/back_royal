class CreateHiringTeams < ActiveRecord::Migration[5.1]
    def up

        create_table :hiring_teams, id: :uuid do |t|
            t.timestamps                        null: false
            t.text :title, null: false
        end

        create_versions_table_and_trigger('hiring_teams')

        create_trigger_validate_hiring_teams_on_relationship_change
        create_trigger_validate_hiring_teams_on_team_change

        add_column :users, :hiring_team_id, :uuid
        add_column :users_versions, :hiring_team_id, :uuid
        add_index :users, :hiring_team_id
        add_foreign_key :users, :hiring_teams

        create_trigger('users', [
            'email',
            'encrypted_password',
            'reset_password_token',
            'reset_password_sent_at',
            'name',
            'nickname',
            'sign_up_code',
            'reset_password_redirect_url',
            'provider',
            'uid',
            'notify_email_newsletter',
            'free_trial_started',
            'confirmed_profile_info',
            'optimizely_referer',
            'active_playlist_locale_pack_id',
            'has_seen_welcome',
            'pref_decimal_delim',
            'pref_locale',
            'school',
            'avatar_url',
            'avatar_provider',
            'mba_content_lockable',
            'job_title',
            'phone',
            'phone_extension',
            'country',
            'has_seen_accepted',
            'can_edit_career_profile',
            'professional_organization_option_id',
            'pref_show_photos_names',
            'identity_verified',
            'sex',
            'ethnicity',
            'race',
            'how_did_you_hear_about_us',
            'address_line_1',
            'address_line_2',
            'city',
            'state',
            'zip',
            'birthdate',
            'anything_else_to_tell_us',
            'pref_keyboard_shortcuts',
            'mba_enabled',
            'fallback_program_type',
            'program_type_confirmed',
            'invite_code',
            'notify_hiring_updates',
            'notify_hiring_spotlights',
            'notify_hiring_content',
            'notify_hiring_candidate_activity',
            'hiring_team_id'
            # the json columns must be left out of here, since they can't be compared
        ])

    end

    def down

         create_trigger('users', [
            'email',
            'encrypted_password',
            'reset_password_token',
            'reset_password_sent_at',
            'name',
            'nickname',
            'sign_up_code',
            'reset_password_redirect_url',
            'provider',
            'uid',
            'notify_email_newsletter',
            'free_trial_started',
            'confirmed_profile_info',
            'optimizely_referer',
            'active_playlist_locale_pack_id',
            'has_seen_welcome',
            'pref_decimal_delim',
            'pref_locale',
            'school',
            'avatar_url',
            'avatar_provider',
            'mba_content_lockable',
            'job_title',
            'phone',
            'phone_extension',
            'country',
            'has_seen_accepted',
            'can_edit_career_profile',
            'professional_organization_option_id',
            'pref_show_photos_names',
            'identity_verified',
            'sex',
            'ethnicity',
            'race',
            'how_did_you_hear_about_us',
            'address_line_1',
            'address_line_2',
            'city',
            'state',
            'zip',
            'birthdate',
            'anything_else_to_tell_us',
            'pref_keyboard_shortcuts',
            'mba_enabled',
            'fallback_program_type',
            'program_type_confirmed',
            'invite_code',
            'notify_hiring_updates',
            'notify_hiring_spotlights',
            'notify_hiring_content',
            'notify_hiring_candidate_activity'
            # the json columns must be left out of here, since they can't be compared
        ])

        remove_column :users, :hiring_team_id
        remove_column :users_versions, :hiring_team_id
        drop_all_for_table :hiring_teams

        execute("drop trigger validate_hiring_teams_on_relationship_change on hiring_relationships")
        execute("drop trigger validate_hiring_teams_on_team_change on users")
    end

    def create_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end

    def find_dupes(hiring_team_matcher)
        "
            WITH pairs AS MATERIALIZED (
                SELECT
                    candidate_id,
                    hiring_team_id
                FROM
                    hiring_relationships
                    JOIN users hiring_managers
                        ON hiring_managers.id = hiring_manager_id
                WHERE
                    hiring_manager_status = 'accepted'
                    AND hiring_managers.hiring_team_id = (#{hiring_team_matcher})
            )
            SELECT
                candidate_id
                , count(*)
            FROM pairs
            GROUP BY candidate_id
            HAVING COUNT(*) > 1
        "
    end

    def create_trigger_validate_hiring_teams_on_relationship_change
        find_dupes = find_dupes("select hiring_team_id from users where id = NEW.hiring_manager_id")

        # Any time a relationship is changed to 'accepted' or it is already 'accepted' and the hiring manager changes,
        # check that there are no dupes
        trigger_sql = %Q~
            CREATE OR REPLACE FUNCTION validate_hiring_teams_on_relationship_change() RETURNS TRIGGER AS $hiring_relationship$
                BEGIN
                    IF (
                        NEW.hiring_manager_status = 'accepted'
                        AND (
                            TG_OP = 'INSERT'
                            OR OLD.hiring_manager_status != NEW.hiring_manager_status
                            OR OLD.hiring_manager_id != NEW.hiring_manager_id
                        )
                        AND EXISTS(#{find_dupes})
                    ) THEN
                        RAISE EXCEPTION 'Multiple connections to candidate';
                    END IF;

                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $hiring_relationship$ LANGUAGE plpgsql;

            CREATE TRIGGER validate_hiring_teams_on_relationship_change
            AFTER INSERT OR UPDATE ON hiring_relationships
            FOR EACH ROW EXECUTE PROCEDURE validate_hiring_teams_on_relationship_change();
        ~

        execute(trigger_sql)
    end

    def create_trigger_validate_hiring_teams_on_team_change
        find_dupes = find_dupes("NEW.hiring_team_id")

        # any time that a user's hiring_team changes, check that there are no dupes
        trigger_sql = %Q~
            CREATE OR REPLACE FUNCTION validate_hiring_teams_on_team_change() RETURNS TRIGGER AS $user$
                BEGIN
                    IF (
                        NEW.hiring_team_id IS NOT NULL
                        AND (
                            TG_OP = 'INSERT'
                            OR OLD.hiring_team_id IS NULL
                            OR OLD.hiring_team_id != NEW.hiring_team_id
                        )
                        AND EXISTS(#{find_dupes})
                    ) THEN
                        RAISE EXCEPTION 'Multiple connections to candidate';
                    END IF;

                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $user$ LANGUAGE plpgsql;

            CREATE TRIGGER validate_hiring_teams_on_team_change
            AFTER INSERT OR UPDATE ON users
            FOR EACH ROW EXECUTE PROCEDURE validate_hiring_teams_on_team_change();
        ~

        execute(trigger_sql)
    end

    def drop_all_for_table(table_name)
        drop_versions_table_and_trigger table_name
        drop_table table_name
    end

    def drop_versions_table_and_trigger(table_name)
        drop_table "#{table_name}_versions"
        execute "DROP TRIGGER #{table_name}_versions ON #{table_name}"
    end

    def create_versions_table_and_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.connection.schema_cache.clear!
        klass.reset_column_information

        # create the version table, including all columns from the source table
        # plus a few extras
        create_table "#{table_name}_versions", id: false, force: :cascade do |t|
            t.uuid     "version_id", null: false, default: "uuid_generate_v4()"
            t.string   "operation", limit: 1, null: false
            t.datetime "version_created_at", null: false

            puts "*** #{table_name}_versions"

            klass.column_names.each do |column_name|
                type = klass.columns_hash

                column_config = klass.columns_hash[column_name]
                meth = column_config.type.to_sym
                options = {}
                options[:limit] = column_config.limit unless column_config.limit.nil?
                options[:null] = false if column_config.null == false
                options[:array] = column_config.array
                # ignore defaults, since they should not be used in the audit table

                puts "t.#{meth}, #{column_name}, #{options.inspect}"
                t.send(meth, column_name, options)
            end
        end

        # for some reason the primary flag on create_table was not working
        execute "ALTER TABLE #{table_name}_versions ADD PRIMARY KEY (version_id);"

        # create indexes
        if klass.column_names.include?('id') && klass.column_names.include?('updated_at')
            add_index "#{table_name}_versions", [:id, :updated_at]
        elsif klass.column_names.include?('id')
            add_index "#{table_name}_versions", :id
        end

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "
        execute(trigger_sql)
    end
end
