class ChangeCareerProfileFieldsToNotNull < ActiveRecord::Migration[5.0]
    def up
        change_column :career_profiles, :job_search_stage, :text, :default => "ready_to_start", :null => false
        change_column :career_profiles, :ideal_job_start_time, :text, :default => "soon_as_possible", :null => false
    end

    def down
        change_column :career_profiles, :job_search_stage, :text, :default => "ready_to_start"
        change_column :career_profiles, :ideal_job_start_time, :text, :default => "soon_as_possible"
    end
end
