class AddSummariesToStreams < ActiveRecord::Migration[4.2]
    def change
        add_column :lesson_streams, :summaries, :json
        add_column :lesson_streams_versions, :summaries, :json
    end
end