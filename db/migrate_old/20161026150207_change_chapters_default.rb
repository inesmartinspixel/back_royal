class ChangeChaptersDefault < ActiveRecord::Migration[4.2][5.0]
    def up
      change_column_default :lesson_streams, :chapters, []
    end
    def down
      change_column_default :lesson_streams, :chapters, {}
    end
end
