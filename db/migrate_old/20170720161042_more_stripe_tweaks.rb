class MoreStripeTweaks < ActiveRecord::Migration[5.1]
    def change

        # Cohorts + Templates - program_guide_url (old will be deleted later)
        add_column :cohorts, :program_guide_url, :text
        add_column :cohorts_versions, :program_guide_url, :text
        add_column :curriculum_templates, :program_guide_url, :text
        add_column :curriculum_templates_versions, :program_guide_url, :text

        # Cohorts + Templates - stripe_plan_amount (int following Stripe conventions)
        add_column :cohorts, :stripe_plan_amount, :integer
        add_column :cohorts_versions, :stripe_plan_amount, :integer
        add_column :curriculum_templates, :stripe_plan_amount, :integer
        add_column :curriculum_templates_versions, :stripe_plan_amount, :integer

        # CohortApplications - stripe_coupon_amount_off, stripe_coupon_percent_off
        # (ints following Stripe conventions)
        add_column :cohort_applications, :stripe_coupon_amount_off, :integer
        add_column :cohort_applications_versions, :stripe_coupon_amount_off, :integer

        add_column :cohort_applications, :stripe_coupon_percent_off, :integer
        add_column :cohort_applications_versions, :stripe_coupon_percent_off, :integer

    end
end
