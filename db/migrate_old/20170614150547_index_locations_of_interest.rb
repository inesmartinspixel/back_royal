class IndexLocationsOfInterest < ActiveRecord::Migration[5.1]
    def change
        add_index :career_profiles, :locations_of_interest, using: :gin
    end
end
