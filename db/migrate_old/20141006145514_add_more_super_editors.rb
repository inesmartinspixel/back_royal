class AddMoreSuperEditors < ActiveRecord::Migration[4.2]
    def change
        super_editor_emails = ["tom@bizy.com", "taph72@gmail.com", "marshall@work-around.com", "marshall@workaroundcapital.com", "allison@pedago.com", "harper.allison@gmail.com"]
        super_editor_role = Role.find_or_create_by(name: :super_editor)

        super_editor_emails.each do |email|
            user = User.where(:email => email)
            user.add_role :super_editor if !user.blank?
        end
    end
end


