class CreateInitialAdmissionsRounds < ActiveRecord::Migration[5.0]
    def up
        execute("select cohort_id from cohort_promotions").to_a.each do |result|
            cohort_id = result['cohort_id']
            cohort = Cohort.where("id = ?", cohort_id).first
            cohort.title = 'Test'
            if ['emba', 'mba'].include?(cohort.program_type)
                cohort.admission_rounds = [{
                    "application_deadline_days_offset" => -((cohort.start_date - cohort.attributes['application_deadline']) / 1.day).floor,
                    "decision_date_days_offset" => 0
                }]
                cohort.publish!
            end
        end
    end
end
