class AddLastInvitationAtToOpenPositions < ActiveRecord::Migration[5.1]
    def change

        add_column :open_positions, :last_invitation_at, :timestamp
        add_column :open_positions_versions, :last_invitation_at, :timestamp

    end
end
