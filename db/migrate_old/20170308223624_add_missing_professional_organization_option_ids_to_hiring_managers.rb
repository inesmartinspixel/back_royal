class AddMissingProfessionalOrganizationOptionIdsToHiringManagers < ActiveRecord::Migration[5.0]
    def up

        # Each of these users are hiring managers, but they don't have a professional organization
        # option associated with them. This happened because these users were made hiring managers
        # out of the normal flow for signing up as a hiring manager. It looks like wendy@pubnub.com
        # and greg@novoed.com signed up as normal users and then were made hiring managers later and
        # pcurtis@optoro.com was created in the admin section. At this time, the only way to associate
        # a professional organization option to a user is through the hiring manager sign up form,
        # which is why these users don't have organizations associated with them.
        # NOTE: Each of the professional organization option's text values were manually spot checked
        # to ensure that the proper organization was associated with each hiring manager.

        # wendy@pubnub.com
        user = User.find_by_id("33f82f9f-8b05-4998-976b-639dd57c45e9")
        if user
            user.professional_organization = ProfessionalOrganizationOption.find_by({locale: "en", text: "PubNub"})
            user.save!
        end

        # greg@novoed.com
        user = User.find_by_id("a8285397-7914-4d2f-b741-91cfe266945c")
        if user
            user.professional_organization = ProfessionalOrganizationOption.find_by({locale: "en", text: "NovoEd"})
            user.save!
        end

        # pcurtis@optoro.com
        user = User.find_by_id("efa6aee8-0644-4749-9c93-e5c0b1c3e995")
        if user
            user.professional_organization = ProfessionalOrganizationOption.find_by({locale: "en", text: "Optoro"})
            user.save!
        end
    end

    def down
        # wendy@pubnub.com
        user = User.find_by_id("33f82f9f-8b05-4998-976b-639dd57c45e9")
        if user
            user.professional_organization_option_id = nil
            user.save!
        end

        # greg@novoed.com
        user = User.find_by_id("a8285397-7914-4d2f-b741-91cfe266945c")
        if user
            user.professional_organization_option_id = nil
            user.save!
        end

        # pcurtis@optoro.com
        user = User.find_by_id("efa6aee8-0644-4749-9c93-e5c0b1c3e995")
        if user
            user.professional_organization_option_id = nil
            user.save!
        end
    end
end
