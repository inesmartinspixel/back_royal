class ConvertContentToJson < ActiveRecord::Migration[4.2]
    def up
        execute('ALTER TABLE lessons ALTER COLUMN content_json TYPE JSON USING content_json::JSON')
        execute('ALTER TABLE lessons_versions ALTER COLUMN content_json TYPE JSON USING content_json::JSON')
    end

    def down
        execute('ALTER TABLE lessons ALTER COLUMN content_json TYPE TEXT USING content_json::TEXT')
        execute('ALTER TABLE lessons_versions ALTER COLUMN content_json TYPE TEXT USING content_json::TEXT')
    end
end
