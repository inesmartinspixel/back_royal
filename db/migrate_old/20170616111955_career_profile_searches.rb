class CareerProfileSearches < ActiveRecord::Migration[5.1]
    def change

        # see https://lab.io/articles/04/13/uuids-rails-5-1/
        enable_extension 'pgcrypto'

        create_table :career_profile_searches, id: :uuid do |t|
           #t.uuid     :id, null: false, default: "uuid_generate_v4()"
            t.timestamps                        null: false
            t.uuid      :user_id, null: false

            t.text :locations, array: true, default: [], null: false
            t.boolean :only_local, default: false, null: false
            t.text :primary_areas_of_interest, array: true, default: [], null: false
            t.text :years_experience, array: true, default: [], null: false

            t.timestamp :last_search_at
            t.integer :search_count
        end
    end
end