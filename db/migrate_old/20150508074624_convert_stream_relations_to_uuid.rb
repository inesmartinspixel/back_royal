class ConvertStreamRelationsToUuid < ActiveRecord::Migration[4.2]
    def up

        # I could not figure out how to convert these types, so this
        # migration is destructive.  I'm guessing that should be okay
        ['lesson_streams', 'lesson_streams_versions'].each do |table_name|
            ['recommended_stream_ids', 'related_stream_ids'].each do |column_name|
                remove_column table_name, column_name
                add_column table_name, column_name, :uuid, array: true, default: '{}'
            end
        end

    end

    def down

        ['lesson_streams', 'lesson_streams_versions'].each do |table_name|
            ['recommended_stream_ids', 'related_stream_ids'].each do |column_name|

                remove_column table_name, column_name
                add_column table_name, column_name, :string, array: true, default: '{}'

            end
        end

    end
end
