class IndexCareerProfileSearches < ActiveRecord::Migration[5.1]
    def change
        add_index :career_profile_searches, :user_id
    end
end
