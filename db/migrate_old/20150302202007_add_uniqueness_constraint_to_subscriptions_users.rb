class AddUniquenessConstraintToSubscriptionsUsers < ActiveRecord::Migration[4.2]
    def change
     add_index(:subscriptions_users, [:subscription_id, :user_id], :unique => true)
    end
end
