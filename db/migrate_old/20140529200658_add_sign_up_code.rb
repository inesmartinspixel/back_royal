class AddSignUpCode < ActiveRecord::Migration[4.2]
    def change
        add_column :users, :sign_up_code, :string
    end
end
