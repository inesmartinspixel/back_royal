class AddEstimatedTimeToEvents < ActiveRecord::Migration[4.2]

    def up

        [:events, :report_frame_events].each do |table|
            add_column table, :estimated_time, :datetime
            add_column table, :client_reported_time, :datetime
            add_index table, :estimated_time
        end

        remove_index :events, :client_utc_timestamp
        remove_column :events, :client_utc_timestamp
        remove_column :events, :client_offset_from_utc
        remove_column :events, :client_seconds_buffered_for

    end

    def down

        add_column :events, :client_utc_timestamp, :integer
        add_column :events, :client_offset_from_utc, :float
        add_column :events, :client_seconds_buffered_for, :integer
        add_index :events, :client_utc_timestamp

        [:events, :report_frame_events].each do |table|
            remove_index table, :estimated_time
            remove_column table, :estimated_time, :datetime
            remove_column table, :client_reported_time, :datetime
        end

    end

end
