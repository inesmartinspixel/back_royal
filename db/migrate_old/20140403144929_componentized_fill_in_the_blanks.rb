require File.expand_path('../componentized_migrators/no_interaction_migrator', __FILE__)
require File.expand_path('../componentized_migrators/blanks_migrator', __FILE__)

class ComponentizedFillInTheBlanks < ActiveRecord::Migration[4.2]
	def up
		migrator = BlanksMigrator.new
		migrator.change
  	end
end
