class FilterCareerProfiles < ActiveRecord::Migration[5.1]
    def up
        add_index :career_profiles, :primary_areas_of_interest, using: :gin

        execute %Q{
            create extension if not exists postgis;
            ALTER TABLE career_profiles ADD COLUMN location geography(POINT,4326);
            ALTER TABLE career_profiles_versions ADD COLUMN location geography(POINT,4326);
            CREATE INDEX career_profiles_on_location
                ON career_profiles
                USING GIST (location);
        }
    end

    def down
        execute %Q~
            DROP INDEX career_profiles_on_location;
            ALTER TABLE career_profiles_versions DROP COLUMN location;
            ALTER TABLE career_profiles DROP COLUMN location;
        ~

        remove_index :career_profiles, :primary_areas_of_interest
    end
end
