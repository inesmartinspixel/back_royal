class AddRedshiftDumpRecords < ActiveRecord::Migration[5.0]
    def change

        create_table :periodic_events_to_redshift_dumps, id: :uuid do |t|
            t.timestamps                                    null: false
            t.timestamp     :completed_at,                  null: true
            t.timestamp     :min_event_created_at,          null: true
            t.timestamp     :max_event_created_at,          null: true
            t.uuid          :locked_by_dump_id,             null: true
            t.timestamp     :failed_at,                     null: true
            t.text          :last_error,                    null: true
        end

    end
end