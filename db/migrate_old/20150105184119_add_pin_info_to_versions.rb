class AddPinInfoToVersions < ActiveRecord::Migration[4.2]

    disable_ddl_transaction!

    def change

        [:lessons, :lesson_streams, :versions].each do |table|

    		add_column table, :pinned, :boolean, :default => false
            add_column table, :pinned_title, :string
            add_column table, :pinned_description, :text
            add_column table, :was_published, :boolean, :default => false
            add_column table, :version_guid, :string

            add_index table, :version_guid, unique: true
        end

        add_column :versions, :object_updated_at, :datetime

        add_column :content_publishers, :version_guid, :string
        add_column :content_publishers, :object_updated_at, :datetime

        # # we can't define "change" and "up".  This is easier that
        # # this block will only be run when going up
        # update_existing_records unless self.reverting?
	end

    # With the conversion away from paper trail, this data migration no longer works

    # def update_existing_records
    #     DataMigrator.go
    # end

    # module DataMigrator

    #     def self.go
    #         query = {
    #             # uncomment these 4 special lessons to test that the right versions get created in
    #             # different cases
    #             # lesson_guid: [
    #             #     'bfaf9aaf-07d7-488a-b37d-93a235d28c9e', # - last_saved is published version
    #             #     '90d11a6e-20fc-45fc-a947-fea746fb6591', # - published version has no matching version
    #             #     '872efa54-c0c0-4abe-ab68-a329efbf0373', # - published version has a matching version
    #             #     'a786cc52-a0de-4881-a67b-376847a76155' # - not published
    #             # ]
    #         }
    #         total = Lesson.where(query).count
    #         i = 0
    #         lessons_map = {}
    #         Lesson.where(query).find_in_batches(batch_size: 10).each do |lessons|

    #             lessons.each do |lesson|
    #                 print_unless_test_env "Updating lessons: #{100*i.to_f / total}% complete" if i % 50 == 0
    #                 i = i + 1
    #                 lessons_map[lesson.id] = lesson
    #                 lesson.version_guid = SecureRandom.uuid
    #                 ActiveRecord::Base.record_timestamps = false
    #                 Lesson.paper_trail_off!
    #                 lesson.save!(validate: false)
    #                 Lesson.paper_trail_on!
    #                 ActiveRecord::Base.record_timestamps = true
    #             end
    #         end
    #         print_unless_test_env "Updating lessons: #{100*i.to_f / total}% complete"

    #         total = Lesson::Stream.count
    #         i = 0
    #         streams_map = {}
    #         Lesson::Stream.find_in_batches(batch_size: 50).each do |streams|
    #             streams.each do |stream|
    #                 print_unless_test_env "Updating streams: #{100*i.to_f / total}% complete" if i % 50 == 0
    #                 i = i + 1
    #                 streams_map[stream.id] = stream
    #                 stream.version_guid = SecureRandom.uuid
    #                 ActiveRecord::Base.record_timestamps = false
    #                 Lesson::Stream.paper_trail_off!
    #                 stream.save!(validate: false)
    #                 Lesson::Stream.paper_trail_on!
    #                 ActiveRecord::Base.record_timestamps = true
    #             end
    #         end
    #         print_unless_test_env "Updating streams: #{100*i.to_f / total}% complete"

    #         query = {event: 'update', item_id: (lessons_map.keys + streams_map.keys)}
    #         total = PaperTrail::Version.where(query).count
    #         i = 0
    #         Lesson.paper_trail_off!
    #         Lesson::Stream.paper_trail_off!

    #         PaperTrail::Version.where(query).find_in_batches(batch_size: 50) do |versions|

    #             versions.each do |version|
    #                 print_unless_test_env "Updating versions: #{100*i.to_f / total}% complete" if i % 50 == 0
    #                 i = i + 1

    #                 # give all versions a version_guid and an object_updated_at time,
    #                 # and set it inside the serialized object
    #                 version.version_guid = SecureRandom.uuid
    #                 reified = version.reify
    #                 version.object_updated_at = reified.updated_at
    #                 reified.version_guid = version.version_guid

    #                 # was_published might be overridden below if this is
    #                 # actually the published version
    #                 reified.was_published = false
    #                 version.was_published = false

    #                 version.object = PaperTrail.serializer.dump(reified.send(:object_attrs_for_paper_trail, reified))
    #                 version.save!
    #             end
    #         end
    #         print_unless_test_env "Updating versions: #{100*i.to_f / total}% complete"

    #         query = 'published_version_object IS NOT NULL'
    #         total = ContentPublisher.where(query).count
    #         i = 0

    #         ContentPublisher.where(query).find_in_batches(batch_size: 50).each do |content_publishers|
    #             content_publishers.each do |content_publisher|
    #                 print_unless_test_env "Updating content publishers: #{100*i.to_f / total}% complete" if i % 50 == 0
    #                 i = i + 1

    #                 objects_to_update = []

    #                 # grab the current version of the record so
    #                 # can decide if it is published right now.  If it is,
    #                 # then pin the version that corresponds with the published
    #                 # version and update the content_publisher with the version_guid
    #                 # and the updated_at
    #                 if content_publisher.lesson_id
    #                     live_version = lessons_map[content_publisher.lesson_id]
    #                 else
    #                     live_version = streams_map[content_publisher.lesson_stream_id]
    #                 end

    #                 next unless live_version

    #                 published_version = live_version.published_version
    #                 objects_to_update << published_version

    #                 print_unless_test_env "Updating content publisher for #{live_version.class} #{live_version.title.inspect}"

    #                 live_version_is_published = live_version.published_version.updated_at.to_timestamp == live_version.updated_at.to_timestamp

    #                 if live_version_is_published
    #                     print_unless_test_env " - Live version is published"
    #                     version_guid = live_version.version_guid
    #                     published_version.version_guid = live_version.version_guid
    #                     objects_to_update << live_version
    #                 else
    #                     version_for_published_object = live_version.versions.detect do |v|
    #                         next unless reified = v.reify
    #                         reified.updated_at.to_timestamp == published_version.updated_at.to_timestamp
    #                     end

    #                     if version_for_published_object.nil?
    #                         print_unless_test_env " - The published_version is not in the versions table.  Creating a record"
    #                         version_guid = SecureRandom.uuid
    #                         version_for_published_object = PaperTrail::Version.new({
    #                             item_type: live_version.class.name,
    #                             item_id: live_version.id,
    #                             event: 'update',
    #                             whodunnit: nil,
    #                             object_updated_at: published_version.updated_at
    #                         })
    #                     else
    #                         print_unless_test_env " - The published_version already has a record in the versions table.  updating it"
    #                         version_guid = version_for_published_object.version_guid
    #                     end
    #                 end

    #                 (objects_to_update + [version_for_published_object]).compact.each do |record|
    #                     if live_version.is_a?(Lesson)
    #                         record.pinned = published_version.pinned = true
    #                         record.pinned_title = published_version.pinned_title = published_version.pinned = "Version published when pinning first rolled out"
    #                         record.pinned_description = published_version.pinned_description = nil
    #                     end

    #                     record.was_published = true
    #                     record.version_guid = version_guid
    #                 end

    #                 content_publisher.object_updated_at = published_version.updated_at
    #                 content_publisher.version_guid = published_version.version_guid

    #                 # we serialize the object slightly differently for content publishers and versions (for no
    #                 # good reason, I think.  Probably a mistake in content publisher)
    #                 content_publisher.published_version_object = PaperTrail.serializer.dump(published_version)
    #                 if version_for_published_object
    #                     version_for_published_object.object = PaperTrail.serializer.dump(published_version.send(:object_attrs_for_paper_trail, published_version))
    #                 end

    #                 # do not set new timestamps on live version
    #                 ActiveRecord::Base.record_timestamps = false
    #                 live_version.skip_update_version_guid_on_save = true
    #                 live_version.save!
    #                 content_publisher.save!

    #                 # if this is a new record, it will end up with no created_at and updated_at
    #                 # timestamps.  That seems like the least confusing thing though, I guess
    #                 version_for_published_object.save! if version_for_published_object
    #                 ActiveRecord::Base.record_timestamps = true
    #             end

    #         end
    #     end

    #     def self.print_unless_test_env(msg)
    #         puts msg unless Rails.env == 'test'
    #     end

    # end

end