class BackfillAssessmentLessons < ActiveRecord::Migration[4.2]
    def up
        migrated = []
        VersionMigrator.new(Lesson).where("title LIKE 'TEST:%'").each do |lesson|
            label = lesson.respond_to?(:version_created_at) ? "#{lesson.title} version #{lesson.version_created_at}" : lesson.title
            migrated << label
            lesson.assessment = true
        end

        puts "The following lessons have been set as 'assessment' lessons:"
        puts migrated
    end
end
