class AddSalaryRangesToHiringApplications < ActiveRecord::Migration[5.0]
    def change
        add_column :hiring_applications, :salary_ranges, :text, :array => true, :default => []
        add_column :hiring_applications_versions, :salary_ranges, :text, :array => true
    end
end
