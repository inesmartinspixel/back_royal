class BringLessonTypeAndFrameCountUp < ActiveRecord::Migration[4.2]
    def change

        add_column :lessons, :lesson_type, :string
        add_column :lessons_versions, :lesson_type, :string
        add_column :lessons, :frame_count, :int
        add_column :lessons_versions, :frame_count, :int

        Lesson.connection.schema_cache.clear!
        Lesson.reset_column_information
        Lesson::Version.reset_column_information

        unless self.reverting?
            VersionMigrator.new(Lesson).where('content_json IS NOT NULL').each do |lesson|
                lesson.lesson_type = lesson.content_json['lesson_type']
                lesson.frame_count = lesson.content_json['frames'].size

                # fix up old stuff where bugs caused us to put
                # extra stuff in the content
                lesson.content_json = lesson.content_json.slice('lesson_type', 'frames')

            end
        end
    end
end
