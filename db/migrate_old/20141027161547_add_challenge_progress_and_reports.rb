class AddChallengeProgressAndReports < ActiveRecord::Migration[4.2]
    def change

        create_table :multiple_choice_challenge_progress do |t|
            t.integer :user_id
            t.string :lesson_guid
            t.string :frame_guid
            t.string :editor_template
            t.string :challenge_id
            t.string :first_answer_id
            t.datetime :first_answer_estimated_time
            t.boolean :first_answer_correct
            t.timestamps
        end

        create_table :multiple_choice_challenge_usage_reports do |t|
            t.string :lesson_guid
            t.string :frame_guid
            t.string :editor_template
            t.string :challenge_id
            t.string :answer_id
            t.integer :count, :default => 0
            t.timestamps
        end


        # user_id / challenge_id should theoretically be good enough, but we have duplicate
        # frame_guids and challenge_ids across different lessons (sadly), so it's not
        add_index(:multiple_choice_challenge_progress, [
            :user_id,
            :lesson_guid,
            :frame_guid,
            :challenge_id,
        ], :unique => true, :name => 'multiple_choice_challenge_progress_unique')

        # unused, but possibly useful
        add_index(:multiple_choice_challenge_progress, :editor_template, :name => 'multiple_choice_challenge_progress_ed_template')


        add_index(:multiple_choice_challenge_usage_reports, [
            :lesson_guid,
            :frame_guid,
            :challenge_id,
            :answer_id
        ], :unique => true, :name => 'multiple_choice_challenge_usage_reports_unique')

        # unused, but possibly useful
        add_index(:multiple_choice_challenge_usage_reports, :editor_template, :name =>'multiple_choice_challenge_usage_reports_ed_template')

    end
end