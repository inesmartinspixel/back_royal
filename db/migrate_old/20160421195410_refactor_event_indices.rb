class RefactorEventIndices < ActiveRecord::Migration[4.2]
    disable_ddl_transaction!

    def up

        # this is too slow to run during an eb rollout.  In that case, we run the following
        # commands manually in psql:
        #     CREATE INDEX CONCURRENTLY "index_events_on_event_type_and_estimated_time" ON "events"  ("event_type", "estimated_time")
        #     CREATE INDEX CONCURRENTLY "index_events_on_user_id_and_estimated_time" ON "events"  ("user_id", "estimated_time")
        #     DROP INDEX CONCURRENTLY "index_events_on_estimated_time_and_id"
        #     DROP INDEX CONCURRENTLY "index_events_on_user_id"
        return if Rails.env.production?
        add_index(:events, [ :event_type, :estimated_time ], algorithm: :concurrently)
        add_index(:events, [ :user_id, :estimated_time ], algorithm: :concurrently)

        remove_index(:events, :name => "index_events_on_estimated_time_and_id")
        remove_index(:events, :name => "index_events_on_user_id")

    end

    def down

        return if Rails.env.production?

        add_index "events", ["user_id"], name: "index_events_on_user_id", using: :btree, algorithm: :concurrently
        add_index "events", ["estimated_time", "id"], name: "index_events_on_estimated_time_and_id", using: :btree, algorithm: :concurrently

        remove_index(:events, [ :event_type, :estimated_time ])
        remove_index(:events, [ :user_id, :estimated_time ])

    end
end