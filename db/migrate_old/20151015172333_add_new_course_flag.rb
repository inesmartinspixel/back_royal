class AddNewCourseFlag < ActiveRecord::Migration[4.2]
    def change
        add_column :lesson_streams, :just_added, :boolean, default: false, null: false
        add_column :lesson_streams_versions, :just_added, :boolean

        execute "UPDATE lesson_streams_versions SET just_added=false"
    end
end