class BackfillEventsWithGuidsHitServerAtAndTimes < ActiveRecord::Migration[4.2]

    # we need distance_of_time_in_words
    include ActionView::Helpers::DateHelper

    disable_ddl_transaction!

    def up

        start_time = Time.now
		Event.all_by_page_load_id("client_reported_time IS NOT NULL") do |page_load_id, events, percent_complete|

			first_event_in_session = events[0]
            session_start_server_time = first_event_in_session.estimated_time
            session_start_client_time = first_event_in_session.client_reported_time

            Event.where(["payload->>'page_load_id' = ?", page_load_id]).update_all(["
        		hit_server_at = CASE
                    WHEN hit_server_at IS NOT NULL THEN
                        hit_server_at
                    ELSE
                        created_at
                END,

                total_buffered_seconds = CASE
                    WHEN total_buffered_seconds IS NOT NULL THEN
                        total_buffered_seconds
                    ELSE
                        cast(payload->>'buffered_time' as double precision)
                END,

                estimated_time = cast(? as TIMESTAMP) + (client_reported_time - ?)

        		", session_start_server_time, session_start_client_time])

            percent_complete = '%.2f' % (100*percent_complete)
            puts "----------- Adding times, total_buffered_seconds, and hit_server_at: #{percent_complete}% complete, #{distance_of_time_in_words(Time.now - start_time)} elapsed) -----------"
		end

        start_time = Time.now
        i = 0
        Event.where({guid: nil}).find_in_batches(batch_size: 500) do |events|
            ActiveRecord::Base.transaction do
                events.each do |event|
                    event.guid = SecureRandom.uuid
                    event.save(validate: false)
                end
            end
            i = i + events.size
            puts "----------- Adding guids: #{i} events processed, #{distance_of_time_in_words(Time.now - start_time)} elapsed) -----------"
        end

	end
end
