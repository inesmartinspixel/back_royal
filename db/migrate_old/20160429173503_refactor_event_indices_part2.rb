class RefactorEventIndicesPart2 < ActiveRecord::Migration[4.2]

    disable_ddl_transaction!

    def up

        execute "DROP INDEX CONCURRENTLY \"index_events_on_event_type\""
        execute "DROP INDEX CONCURRENTLY \"index_events_on_created_at\""
        execute "DROP INDEX CONCURRENTLY \"user-time-id\""

    end

    def down

        execute "CREATE INDEX CONCURRENTLY \"user-time-id ON events\" USING btree (user_id, estimated_time, id)"
        execute "CREATE INDEX CONCURRENTLY \"index_events_on_created_at\" ON events USING btree (created_at)"
        execute "CREATE INDEX CONCURRENTLY \"index_events_on_event_type\" ON events USING btree (event_type)"

    end

end
