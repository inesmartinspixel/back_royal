class AddEventTypeIndexToEvents < ActiveRecord::Migration[4.2]
    def change
        add_index(:events, :event_type)
    end
end