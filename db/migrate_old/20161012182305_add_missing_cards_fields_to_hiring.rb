class AddMissingCardsFieldsToHiring < ActiveRecord::Migration[4.2]
    def change
        add_column :hiring_applications, :funding, :text
        add_column :hiring_applications_versions, :funding, :text

        add_column :hiring_applications, :industry, :text
        add_column :hiring_applications_versions, :industry, :text
    end
end
