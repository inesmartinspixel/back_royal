class AddNumRequiredSpecializations < ActiveRecord::Migration[5.0]

    def change
        add_column :cohorts, :num_required_specializations, :int, null: false, default: 0
        add_column :cohorts_versions, :num_required_specializations, :int, null: false, default: 0
        add_column :curriculum_templates, :num_required_specializations, :int, null: false, default: 0
        add_column :curriculum_templates_versions, :num_required_specializations, :int, null: false, default: 0
    end
end