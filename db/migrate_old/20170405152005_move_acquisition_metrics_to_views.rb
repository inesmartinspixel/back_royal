class MoveAcquisitionMetricsToViews < ActiveRecord::Migration[5.0]
    def up
        ViewHelpers.migrate(self, 20170405152005)
    end

    def down
        ViewHelpers.rollback(self, 20170405152005)
    end
end
