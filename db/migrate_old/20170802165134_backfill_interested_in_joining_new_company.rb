class BackfillInterestedInJoiningNewCompany < ActiveRecord::Migration[5.1]
    def up
        # As part of the changes for how we're now going to determine if profiles are active or inactive
        # within our career network, we need to backfill the values of interested_in_joining_new_company
        # for some profiles so they retain their current active or inactive status.
        execute("
            WITH users_to_migrate AS MATERIALIZED (
                SELECT career_profiles.id
                FROM career_profiles
                    JOIN users
                        ON users.id = career_profiles.user_id
                WHERE interested_in_joining_new_company != 'not_interested'
                    AND career_profile_active = false
                    AND can_edit_career_profile = true
                    AND last_calculated_complete_percentage = 100
            )
            UPDATE career_profiles
            SET interested_in_joining_new_company = 'not_interested'
            WHERE career_profiles.id IN (SELECT id FROM users_to_migrate)
        ")

        execute("
            UPDATE career_profiles
            SET interested_in_joining_new_company = 'neutral'
            WHERE interested_in_joining_new_company = 'not_interested'
                AND career_profile_active = true
        ")
    end

    def down
        # no op
    end
end
