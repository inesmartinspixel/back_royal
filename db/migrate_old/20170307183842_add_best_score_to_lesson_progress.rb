class AddBestScoreToLessonProgress < ActiveRecord::Migration[5.0]
    def change

        add_column :lesson_progress, :best_score, :float

    end
end
