class AddFinalScoreToCohortApplications < ActiveRecord::Migration[5.1]

    def up
        add_column :cohort_applications, :final_score, :float
        add_column :cohort_applications_versions, :final_score, :float
        ViewHelpers.migrate(self, 20170703170913)
    end

    def down
        ViewHelpers.rollback(self, 20170703170913)
        remove_column :cohort_applications, :final_score, :float
        remove_column :cohort_applications_versions, :final_score, :float
    end

end
