class AddApplicationDetailsToApplication < ActiveRecord::Migration[4.2]
    def up

        [:cohort_applications, :cohort_applications_versions].each do |table_name|

            versions = table_name == :cohort_applications_versions

            change_table table_name do |t|
                # basic info
                t.text      :street_address
                t.text      :city_state
                t.text      :country_identifier
                t.text      :phone_number
                t.text      :time_zone_identiifier,     (versions ? {} : {:default => 'America/New_York', :null => false})

                # test scores
                t.text      :score_on_gmat
                t.text      :score_on_gre
                t.text      :score_on_sat
                t.text      :score_on_act

                # short answers
                t.json      :short_answers,             (versions ? {} : {:default => {}, :null => false}) # keys are translation keys
            end
        end
    end

    def down
        [:cohort_applications, :cohort_applications_versions].each do |table_name|

            change_table table_name do |t|
               # basic info
                t.remove      :street_address
                t.remove      :city_state
                t.remove      :country_identifier
                t.remove      :phone_number
                t.remove      :time_zone_identiifier

                # test scores
                t.remove      :score_on_gmat
                t.remove      :score_on_gre
                t.remove      :score_on_sat
                t.remove      :score_on_act

                # short answers
                t.remove      :short_answers
            end
        end
    end
end
