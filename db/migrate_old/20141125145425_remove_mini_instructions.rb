class RemoveMiniInstructions < ActiveRecord::Migration[4.2]

    disable_ddl_transaction!

    def change
        migrator = RemoveMiniInstructionsMigrator.new
        migrator.change
    end
end
