class AddPublishedVersionObjectToContentPublishers < ActiveRecord::Migration[4.2]
    def change
        # published_version_object : the serialized object corresponding to the current published version,
        # like papertrail's version table's object column
        add_column :content_publishers, :published_version_object, :text
    end
end
