class AddHiringManagerClosedInfoToHiringRelationship < ActiveRecord::Migration[5.1]
    def change
        add_column :hiring_relationships, :hiring_manager_closed_info, :json, :default => {}, :null => false
        add_column :hiring_relationships_versions, :hiring_manager_closed_info, :json
    end
end
