class MigrateUsersToSmarter < ActiveRecord::Migration[4.2]

    def up

        execute "
                INSERT INTO access_groups_users (access_group_id, user_id) (
                    SELECT
                        (SELECT id FROM access_groups WHERE access_groups.name = 'SMARTER' LIMIT 1) as ag_id,
                        users.id
                    FROM users
                    INNER JOIN access_groups_users ON users.id = access_groups_users.user_id
                    INNER JOIN access_groups ON access_groups_users.access_group_id = access_groups.id
                    WHERE access_groups.name IN ('MBA', 'PRE_MBA')
                ) ON CONFLICT DO NOTHING
            "


        ["access_groups_users",
         "access_groups_lesson_stream_locale_packs",
         "access_groups_cohorts",
         "access_groups_playlist_locale_packs"].each do |fk_table|
            execute "
                    DELETE FROM #{fk_table}
                    WHERE access_group_id IN (
                        SELECT access_groups.id
                        FROM access_groups
                        WHERE access_groups.name IN ('MBA', 'PRE_MBA')
                    )
                "
        end

        execute "DELETE FROM access_groups WHERE access_groups.name IN ('MBA', 'PRE_MBA')"

        affected_users_sql = "
                            SELECT * FROM users
                            INNER JOIN access_groups_users ON users.id = access_groups_users.user_id
                            INNER JOIN access_groups ON access_groups_users.access_group_id = access_groups.id
                            WHERE access_groups.name = 'SMARTER'
                            "


        smarter_group = AccessGroup.find_by_name('SMARTER')
        if smarter_group
            smarter_group.users.each do |user|
                user.identify
            end
        end

    end

    def down
        # best we can do
        execute "INSERT INTO access_groups (name, created_at, updated_at) VALUES
                    ('MBA', NOW(), NOW() ),
                    ('PRE_MBA', NOW(), NOW() )
            "
    end

end
