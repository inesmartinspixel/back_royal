class AddLastCalculatedCompletePercentageToProfiles < ActiveRecord::Migration[5.0]
    def change

        add_column :career_profiles, :last_calculated_complete_percentage, :float, :null => true
        add_column :career_profiles_versions, :last_calculated_complete_percentage, :float, :null => true

        add_column :hiring_applications, :last_calculated_complete_percentage, :float, :null => true
        add_column :hiring_applications_versions, :last_calculated_complete_percentage, :float, :null => true

    end
end
