class AdmissionRoundViewUpdates < ActiveRecord::Migration[5.0]
    def up
        ViewHelpers.migrate(self, 20170426142452)
    end

    def down
        ViewHelpers.rollback(self, 20170426142452)
    end
end
