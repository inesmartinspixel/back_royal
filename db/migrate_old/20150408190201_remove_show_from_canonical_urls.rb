class RemoveShowFromCanonicalUrls < ActiveRecord::Migration[4.2]

    def change
        remove_show_from_canonical_urls unless self.reverting?
    end

    def remove_show_from_canonical_urls
        EntityMetadata.find_in_batches(batch_size: 10) do |batch|
            batch.each do |entity_metadata|
                if entity_metadata.canonical_url =~ /show/
                    entity_metadata.canonical_url = entity_metadata.canonical_url.gsub("/show", "")
                    entity_metadata.save!
                end
            end
        end
    end

end

