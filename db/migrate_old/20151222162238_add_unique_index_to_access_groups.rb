class AddUniqueIndexToAccessGroups < ActiveRecord::Migration[4.2]
    def up
        remove_index :access_groups, :name
        add_index :access_groups, :name, :unique => true

        add_index :lesson_fulltext, :lesson_id, :unique => true
    end

    def down
        remove_index :access_groups, :name
        add_index :access_groups, :name

        remove_index :lesson_fulltext, :lesson_id
    end
end
