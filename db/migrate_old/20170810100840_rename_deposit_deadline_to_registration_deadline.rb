class RenameDepositDeadlineToRegistrationDeadline < ActiveRecord::Migration[5.1]
    def change

        rename_column :cohorts, :deposit_deadline_days_offset, :registration_deadline_days_offset
        rename_column :cohorts_versions, :deposit_deadline_days_offset, :registration_deadline_days_offset

        rename_column :curriculum_templates, :deposit_deadline_days_offset, :registration_deadline_days_offset
        rename_column :curriculum_templates_versions, :deposit_deadline_days_offset, :registration_deadline_days_offset

    end
end
