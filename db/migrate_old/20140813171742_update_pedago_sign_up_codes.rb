class UpdatePedagoSignUpCodes < ActiveRecord::Migration[4.2]
    def change
        User.connection.execute("UPDATE USERS SET sign_up_code='WORKAROUND' WHERE sign_up_code='PEDAGOER'")
        User.connection.execute("UPDATE USERS SET sign_up_code='WORKAROUND' WHERE sign_up_code='PEDAGOERS'")
    end
end
