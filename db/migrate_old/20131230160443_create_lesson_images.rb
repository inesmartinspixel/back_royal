class CreateLessonImages < ActiveRecord::Migration[4.2]
    def change
        create_table :lesson_images do |t|
            t.timestamps
            t.integer :lesson_id
            t.attachment :file
        end
    end
end