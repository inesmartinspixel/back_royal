class AddFeaturedToWorkExperience < ActiveRecord::Migration[5.0]
    def change
        add_column :work_experiences, :featured, :boolean, :default => false, :null => false
        add_column :work_experiences_versions, :featured, :boolean
    end
end
