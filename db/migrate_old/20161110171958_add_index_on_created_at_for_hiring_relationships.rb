class AddIndexOnCreatedAtForHiringRelationships < ActiveRecord::Migration[5.0]
    def change
        add_index :hiring_relationships, [:candidate_id, :hiring_manager_status, :updated_at], :name => "last_candidate_relationship_accepted_at"
        add_index :hiring_relationships, [:hiring_manager_id, :candidate_status, :updated_at], :name => "last_hiring_manager_relationship_accepted_at"
    end
end
