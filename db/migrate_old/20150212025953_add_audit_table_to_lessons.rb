# removing has_paper_trail from the real code, but
# it is required to make reify work in this
# migration
@paper_trail_exists = true
begin
    class Lesson
        has_paper_trail
    end

    class Lesson::Stream
        has_paper_trail
    end
rescue NameError => err
    # if has_paper_trail is gone, do not bother with this
    @paper_trail_exists = false
end

class AddAuditTableToLessons < ActiveRecord::Migration[4.2]

    def up

        add_column :lessons, :last_editor_id, :uuid
        add_column :lesson_streams, :last_editor_id, :uuid

        create_lesson_versions_table

        create_streams_versions_table
        if @paper_trail_exists
            migrate_existing_versions(Lesson::Stream)
            migrate_existing_versions(Lesson)
        elsif ContentPublisher.any?
            raise "You cannot run this migration if you have content publishers in the db.  Delete them or something"
        end

        # add the primary keys after migrating old stuff.  Makes things simpler
        execute "ALTER TABLE lessons_versions ADD PRIMARY KEY (version_id);"
        execute "ALTER TABLE lesson_streams_versions ADD PRIMARY KEY (version_id);"

        # remove_column :lessons, :version_id
        # remove_column :lesson_streams, :version_id
        # remove_column :content_publishers, :published_version_index
        # remove_column :content_publishers, :published_version_object

        add_column :content_publishers, :lesson_version_id, :uuid
        add_column :content_publishers, :lesson_stream_version_id, :uuid
        migrate_content_publishers
        add_foreign_key "content_publishers", "lesson_streams_versions", column: "lesson_stream_version_id", primary_key: "version_id"
        add_foreign_key "content_publishers", "lessons_versions", column: "lesson_version_id", primary_key: "version_id"


        # ensure we're actually using booleans (not null) values
        update_boolean :lessons, :archived
        update_boolean :lessons, :pinned
        update_boolean :lessons, :was_published
        update_boolean :lessons_versions, :archived
        update_boolean :lessons_versions, :pinned
        update_boolean :lessons_versions, :was_published

        update_boolean :lesson_streams, :pinned
        update_boolean :lesson_streams, :was_published
        update_boolean :lesson_streams_versions, :pinned
        update_boolean :lesson_streams_versions, :was_published


        create_trigger(Lesson)
        create_trigger(Lesson::Stream)

        add_index :lessons_versions, [:id, :updated_at]
        add_index :lesson_streams_versions, [:id, :updated_at]

    end

    def down
        remove_column :lessons, :last_editor_id, :uuid
        remove_column :lesson_streams, :last_editor_id, :uuid
        # add_column :lessons, :version_id, :uuid
        # add_column :lesson_streams, :version_id, :uuid
        # add_column :content_publishers, :published_version_index, :integer
        # add_column :content_publishers, :published_version_object, :text
        drop_table :lessons_versions
        drop_table :lesson_streams_versions
        execute('DROP TRIGGER lessons_versions ON lessons')
        execute('DROP TRIGGER lesson_streams_versions ON lesson_streams')
    end


    # # Some code to limit db size for fast migration testing
    # published_stream_ids = ContentPublisher.where('published_version_object is not null').pluck('lesson_stream_id').compact
    # all_stream_ids = Lesson::Stream.pluck('id')
    # some_unpublished_ones = (all_stream_ids - published_stream_ids).slice(0, 10)
    # stream_ids = published_stream_ids.slice(0, 10) + some_unpublished_ones
    # Lesson::Stream.where('id NOT IN (?)', stream_ids).destroy_all
    # PaperTrail::Version.where("item_id NOT IN (?) AND item_type='Lesson::Stream'", stream_ids).delete_all
    # streams = Lesson::Stream.where(id: all_stream_ids)

    # lesson_ids = streams.map(&:lessons).flatten.compact.map(&:id).uniq
    # Lesson.where('id NOT IN (?)', lesson_ids).destroy_all
    # PaperTrail::Version.where("item_id NOT IN (?) AND item_type='Lesson'", lesson_ids).delete_all



    def update_boolean(table_name, column_name)
        execute "UPDATE #{table_name} SET #{column_name}=FALSE WHERE #{column_name} IS NULL"
        execute "ALTER TABLE #{table_name} ALTER COLUMN #{column_name} SET NOT NULL"
    end


    def create_lesson_versions_table
        create_table "lessons_versions", id: false, force: :cascade do |t|
            t.uuid     "version_id", null: false, default: "uuid_generate_v4()"
            t.string   "operation", limit: 1, null: false
            t.datetime "version_created_at", null: false

            t.uuid     "id"
            t.datetime "created_at",                                     null: false
            t.datetime "updated_at",                                     null: false
            t.text     "content_json"
            t.uuid     "author_id"
            t.string   "title",              limit: 255
            t.text     "description"
            t.datetime "modified_at"
            t.boolean  "archived"
            t.boolean  "pinned"
            t.string   "pinned_title",       limit: 255
            t.text     "pinned_description"
            t.boolean  "was_published"
            t.uuid     "last_editor_id"
        end
    end

    def create_streams_versions_table
        create_table "lesson_streams_versions", id: false, force: :cascade do |t|
            t.uuid     "version_id", null: false, default: "uuid_generate_v4()"
            t.string   "operation", limit: 1, null: false
            t.datetime "version_created_at", null: false

            t.uuid     "id"
            t.datetime "created_at"
            t.datetime "updated_at"
            t.string   "title"
            t.text     "description"
            t.uuid     "author_id"
            t.uuid     "image_id"
            t.json     "chapters"
            t.json     "key_topics"
            t.json     "ordered_concept_ids"
            t.uuid     "certificate_image_id"
            t.datetime "modified_at"
            t.boolean  "pinned"
            t.string   "pinned_title"
            t.text     "pinned_description"
            t.boolean  "was_published"
            t.uuid     "last_editor_id"
        end
    end

    def migrate_existing_versions(klass)
        versions_klass = Class.new(ActiveRecord::Base) do
            self.table_name = "#{klass.table_name}_versions"
        end

        conditions = klass == Lesson ? {archived: false} : {}
        ids = klass.where(conditions).pluck(:id)

        ids.each_with_index do |id, i|
            puts "migrating versions for #{klass.table_name.singularize} #{i+1} of #{ids.size}" if i == 0 || i % 10 == 9

            whodunnit = nil
            # we can only migrate existing update events, so going backwards we
            # will not have records for create and delete
            conditions = {item_type: klass.name, item_id: id, event: 'update'}
            versions = PaperTrail::Version.
                where(conditions).
                where('object is not null').
                reorder('created_at asc')

            instance = klass.find(id)
            if instance.pinned
                last_pinned_index = 99999999
            else
                pinned_indexes = []
                versions.each_with_index do |version, i|
                    pinned_indexes << i if version.pinned
                end
                # if pinned_indexes.size > 2
                #     puts "#{klass.table_name.singularize} #{id} has multiple pinned versions: #{pinned_indexes.inspect}"
                # end
                last_pinned_index = pinned_indexes.last || 0
            end

            versions_to_copy = []
            versions.each_with_index do |version, i|
                if i >= last_pinned_index || version.pinned
                    versions_to_copy << version
                end
            end

            if versions.size != versions_to_copy.size
                puts " - pruning #{versions.size - versions_to_copy.size} versions from #{instance.title.inspect}"
            end

            versions_to_copy.each_with_index do |version, i|

                reified = version.reify
                whodunnit = reified.author_id if whodunnit.nil?
                audit = versions_klass.create!(reified.attributes.merge({
                    version_id: version.id,
                    operation: 'U',
                    last_editor_id: whodunnit,
                    version_created_at: reified.updated_at
                }))
                audit.id = reified.id

                # the whodunnit on this version is the editor who created the next one
                whodunnit = version.whodunnit
            end

            # create a version to match the live lesson
            versions_klass.create!(instance.attributes.merge({
                version_id: instance.version_id,
                operation: 'U',
                last_editor_id: whodunnit,
                version_created_at: instance.updated_at # wanted to use version.created_at, but not always there
            }))

            instance.update_column(:last_editor_id, whodunnit)
        end
    end

    def migrate_content_publishers
        ContentPublisher.where(published_version_object: nil).delete_all
        ContentPublisher.all.each do |content_publisher|
            if content_publisher.lesson_id
                key = :lesson_version_id
            else
                key = :lesson_stream_version_id
            end
            content_publisher.update_column(key, content_publisher.attributes['version_id'])
        end
    end

    def create_trigger(klass)
        # see http://www.postgresql.org/docs/9.1/static/plpgsql-trigger.html

        table_name = klass.table_name

        # sql = "
        #     CREATE OR REPLACE FUNCTION process_#{item}_version() RETURNS TRIGGER AS $#{item}_version$
        #         BEGIN
        #             --
        #             -- Create a row in audit to reflect the operation performed on the table,
        #             -- make use of the special variable TG_OP to work out the operation.
        #             --
        #             IF (TG_OP = 'DELETE') THEN
        #                 INSERT INTO #{item}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
        #                 RETURN OLD;
        #             ELSIF (TG_OP = 'UPDATE') THEN
        #                 INSERT INTO #{item}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;
        #                 RETURN NEW;
        #             ELSIF (TG_OP = 'INSERT') THEN
        #                 INSERT INTO #{item}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
        #                 RETURN NEW;
        #             END IF;
        #             RETURN NULL; -- result is ignored since this is an AFTER trigger
        #         END;
        #     $#{item}_version$ LANGUAGE plpgsql;

        #     CREATE TRIGGER #{item}_versions
        #     AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
        #     FOR EACH ROW EXECUTE PROCEDURE process_#{item}_version();
        # "

        klass.connection.schema_cache.clear!
        klass.reset_column_information
        old_dot_column_names = []
        new_dot_column_names = []
        klass.column_names.each do |col_name|

            # lessons and versions have a version_id column that will
            # be deleted once the rollout is complete, but so do the
            # target audit tables, so we need to work around that
            next if col_name == "version_id"
            old_dot_column_names << "OLD.#{col_name}"
            new_dot_column_names << "NEW.#{col_name}"
        end

        sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), #{old_dot_column_names.join(', ')};
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), #{new_dot_column_names.join(', ')};
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), #{new_dot_column_names.join(', ')};
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute(sql)
    end
end
