class BestScoreViewChanges < ActiveRecord::Migration[5.0]
    def up
        ViewHelpers.migrate(self, 20170526143117)
    end

    def down
        ViewHelpers.rollback(self, 20170526143117)
    end
end