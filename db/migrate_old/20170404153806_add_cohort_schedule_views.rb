class AddCohortScheduleViews < ActiveRecord::Migration[5.0]
    def up
        ViewHelpers.migrate(self, 20170404153806)
    end

    def down
        ViewHelpers.rollback(self, 20170404153806)
    end
end
