class UnMaterializeUsersCanSeeView < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20170918194659)
    end

    def down
        ViewHelpers.rollback(self, 20170918194659)
    end
end
