class RemoveConceptsAndCollections < ActiveRecord::Migration[4.2]
    def change
        drop_table :collection_concepts_versions
        drop_table :collection_concepts
        drop_table :concept_progress
        drop_table :concepts_versions
        drop_table :concepts
        drop_table :collection_progress
        drop_table :collections_versions
        drop_table :lesson_streams_collections_versions
        drop_table :lesson_streams_collections
        drop_table :collections

        remove_column :lesson_streams_versions, :ordered_concept_ids
        remove_column :lesson_streams, :ordered_concept_ids
    end
end
