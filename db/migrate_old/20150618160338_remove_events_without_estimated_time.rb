class RemoveEventsWithoutEstimatedTime < ActiveRecord::Migration[4.2]

    def up
        execute "DELETE FROM report_frame_events WHERE source_event_id IN (SELECT id FROM events WHERE estimated_time IS NULL)"
        execute "DELETE FROM events WHERE estimated_time IS NULL"
    end

    def down
        # hard to undelete data  =]
    end

end
