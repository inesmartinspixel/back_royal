class AddEmbaInterestDeadline < ActiveRecord::Migration[5.0]
    def change

        add_column :cohorts, :emba_interest_days_offset, :integer, :default => 0
        add_column :cohorts_versions, :emba_interest_days_offset, :integer
        add_column :curriculum_templates, :emba_interest_days_offset, :integer, :default => 0
        add_column :curriculum_templates_versions, :emba_interest_days_offset, :integer

    end
end
