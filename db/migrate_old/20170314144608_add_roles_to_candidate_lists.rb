class AddRolesToCandidateLists < ActiveRecord::Migration[5.0]
    def change
        add_column :career_profile_lists, :role_descriptors, :text, :array => true, :null => false, :default => []
        add_column :career_profile_lists_versions, :role_descriptors, :text, :array => true
    end
end
