class ChangeAllTagsWithClassEntityTypeToLesson < ActiveRecord::Migration[4.2]
    def change
        puts '-- Migrating tags Class entity_types to Lesson entity_type --'
        migrate_tags_with_class_entity_type_to_lesson
    end

    # these tags with entity_type Class instead of Lesson originally got mistakenly created whenever we'd update a lesson.
    # A bug in is_content_type saved the lesson tag's entity_type as self.class instead of self.class.name
    def migrate_tags_with_class_entity_type_to_lesson

        # batch = 0
        # Tag.where(entity_type: "Class").find_in_batches(batch_size: 10) do |tags|
        #     puts " ************ Processing tag batch ##{batch}"

        #     tags.each_with_index do |tag, i|

        #         puts " ************ Processing tag: \"#{tag.text}\" entity_type: \"#{tag.entity_type}\" entity_id: \"#{tag.entity_id}\" ID: #{tag.id} : #{i+1} of #{tags.size}"

        #         # ensure that we are updating a lesson tag
        #         if Lesson.find_by_lesson_guid(tag.entity_id)
        #             tag.entity_type = "Lesson"
        #             tag.save!
        #         end
        #     end

        #     batch += 1
        # end

    end

end
