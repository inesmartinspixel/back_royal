class AddPastDueToSusbcriptions < ActiveRecord::Migration[4.2]
    def change

        add_column :subscriptions, :past_due, :boolean
        add_column :subscriptions_versions, :past_due, :boolean

    end
end
