class CreateConceptProgress < ActiveRecord::Migration[4.2]
    def change
        create_table :concept_progress do |t|
            t.references :concept, index: true
            t.references :user, index: true
            t.date :collected_at

            t.timestamps
        end
    end
end
