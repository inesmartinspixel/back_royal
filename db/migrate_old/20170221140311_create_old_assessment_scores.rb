class CreateOldAssessmentScores < ActiveRecord::Migration[5.0]
    def up

        # Before June 2016 (build 4628), we did not log scores in lesson:finish events.
        # In those cases, we calculate the score from the challenges, but it was
        # difficult to be 100% consistent with the UI that way, so we moved to
        # letting the client log the final score
        #
        # For the pre-06/2016 instances, the logic in etl was too complex to
        # port over to sql.  Instead, we're going to cache the data that has been
        # created by etl, and use it for calculating old scores.  Maybe one day we
        # will be able to drop this, but for now it is needed.
        create_table("etl.old_assessment_scores", :id => false) do |t|
            t.uuid :user_id,                :null => false
            t.uuid :locale_pack_id,         :null => false
            t.float :average_assessment_score_first,            :null => false
            t.float :average_assessment_score_last,             :null => false
        end

        execute %Q|
            insert into etl.old_assessment_scores
            select
                user_id
                , locale_pack_id
                , average_assessment_score_first
                , average_assessment_score_last
            from etl.user_lesson_progress_records
            where
                average_assessment_score_first is not null
        |
    end

    def down
        drop_table 'etl.old_assessment_scores'
    end
end
