class AddDuplicatedFromAndDuplicatedToToPlaylists < ActiveRecord::Migration[4.2]
    def change
        [:playlists, :playlists_versions].each do |table|
            add_column table, :duplicated_from_id, :uuid
            add_column table, :duplicated_to_id, :uuid
        end
    end
end
