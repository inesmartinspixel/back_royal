class BackfillSatScoresForCareerProfiles < ActiveRecord::Migration[5.0]
    def up

        change_column :career_profiles, :sat_max_score, :text, :default => '1600', :null => false

        # Career profiles with score_on_sat values greater than their sat_max_score values.
        # These profiles should have sat_max_score set to '2400'.
        ActiveRecord::Base.connection.execute(%q!
            UPDATE career_profiles
                SET sat_max_score = '2400'
            WHERE career_profiles.score_on_sat ~ '^\d+$'
                AND career_profiles.score_on_sat::int > career_profiles.sat_max_score::int
        !)

        # Career profiles with score_on_sat values containing non-numeric characters
        # or values less than the lowest possible SAT score (400).
        # These profiles should have score_on_sat set to NULL.
        ActiveRecord::Base.connection.execute(%q!
            UPDATE career_profiles
                SET score_on_sat = NULL
            WHERE career_profiles.score_on_sat ~ '\D+'
                OR career_profiles.score_on_sat::int < 400
        !)

        # Career Profiles with score_on_sat values greater than the highest possible SAT score (2400).
        # These profiles should have score_on_sat set to '2400'.
        ActiveRecord::Base.connection.execute(%q!
            UPDATE career_profiles
                SET score_on_sat = '2400'
            WHERE career_profiles.score_on_sat ~ '^\d+$'
                AND career_profiles.score_on_sat::int > 2400
        !)
    end

    def down
        change_column :career_profiles, :sat_max_score, :text
    end
end
