class AddColumnsAndIndexesToEvents < ActiveRecord::Migration[4.2]
	def up

		add_column :events, :hit_server_at, :datetime
        add_column :events, :guid, :string

        # as far as I know, active record does not know how to add
        # an index to a property in a json hash
        self.connection.execute(%Q|CREATE  INDEX  "page_load_id" ON "events"(cast(payload->>'page_load_id' AS varchar), estimated_time, id)|)
        add_index :events, [:user_id, :estimated_time, :id], :name => 'user-time-id'
	end

	def down
		remove_column :events, :hit_server_at
		remove_column :events, :guid
		remove_index :events, :name => 'page_load_id'
		remove_index :events, :name => 'user-time-id'
	end
end
