class IncludeFormatInImages < ActiveRecord::Migration[4.2]
    def up

        image_ids = Lesson::Stream::Version.distinct.reorder('image_id').pluck('image_id') +
                    Lesson::StreamProgress.distinct.reorder('certificate_image_id').pluck('certificate_image_id') +
                    Lesson.distinct.reorder('seo_image_id').pluck('seo_image_id')

        images = S3Asset.where(id: image_ids).where('file_fingerprint IS NOT NULL').reorder('created_at desc')

        images.each_with_index do |image, i|
            puts "processing image #{i+1} of #{images.size}"

            image.formats_will_change!
            image.save!
        end

    end
end
