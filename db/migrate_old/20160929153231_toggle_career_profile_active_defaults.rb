class ToggleCareerProfileActiveDefaults < ActiveRecord::Migration[4.2]
    def up
        execute "UPDATE career_profiles SET career_profile_active = false"
        change_column :career_profiles, :career_profile_active, :boolean, :null => false, :default => false
    end

    def down
        execute "UPDATE career_profiles SET career_profile_active = true"
        change_column :career_profiles, :career_profile_active, :boolean, :null => false, :default => true
    end
end
