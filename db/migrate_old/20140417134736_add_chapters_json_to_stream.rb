class AddChaptersJsonToStream < ActiveRecord::Migration[4.2]
    def change
        add_column :lesson_streams, :chapters, :json
    end
end
