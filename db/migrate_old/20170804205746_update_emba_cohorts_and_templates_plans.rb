class UpdateEmbaCohortsAndTemplatesPlans < ActiveRecord::Migration[5.1]
    def up

        execute "UPDATE curriculum_templates SET stripe_plan_id = 'emba_800', stripe_plan_amount = 80000 WHERE program_type = 'emba'"

        # The next block causes an error if trying to migrate from before this migration to after
        # 20170810100840_rename_deposit_deadline_to_registration_deadline due to a change to a column used in the
        # after_publish_change callback. Since it has been run on prod we are just going to remove it.
        # Ideally, something like this should probably be done in a rake task.

        # Cohort.where("program_type = 'emba'").edit_and_republish do |cohort|
        #     cohort.stripe_plan_id = 'emba_800'
        #     cohort.stripe_plan_amount = 80000
        # end

    end

    def down
        execute "UPDATE curriculum_templates SET stripe_plan_id = NULL, stripe_plan_amount = NULL"

        # See comment in the up

        # Cohort.where("program_type = 'emba'").edit_and_republish do |cohort|
        #     cohort.stripe_plan_id = nil
        #     cohort.stripe_plan_amount = nil
        # end

    end
end
