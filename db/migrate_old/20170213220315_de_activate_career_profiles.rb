class DeActivateCareerProfiles < ActiveRecord::Migration[5.0]
    def change
        execute('
            update career_profiles
                set career_profile_active=false
            where
                career_profile_active=true
                and user_id in (select id from users where can_edit_career_profile=false)')
    end
end
