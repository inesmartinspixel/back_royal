class ChangePeriodEndAtToExpiresAt < ActiveRecord::Migration[4.2]
    def up

        remove_column :subscriptions, :current_period_end
        remove_column :subscriptions_versions, :current_period_end

        add_column :subscriptions, :expires_at, :datetime, null: true
        add_column :subscriptions_versions, :expires_at, :datetime

    end

    def down
        add_column :subscriptions, :current_period_end, :datetime
        add_column :subscriptions_versions, :current_period_end, :datetime

        remove_column :subscriptions, :expires_at
        remove_column :subscriptions_versions, :expires_at
    end
end
