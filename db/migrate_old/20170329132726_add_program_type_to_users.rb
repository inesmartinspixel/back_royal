class AddProgramTypeToUsers < ActiveRecord::Migration[5.0]
    def up

        add_column :users, :program_type, :string
        add_column :users_versions, :program_type, :string
        add_column :users, :program_type_confirmed, :boolean, :default => false, :null => true
        add_column :users_versions, :program_type_confirmed, :boolean

        # users with applications have confirmed program_types
        User.joins(:cohort_applications => :cohort).where("cohorts.program_type" => 'emba').update_all(program_type: 'emba', program_type_confirmed: true)
        User.joins(:cohort_applications => :cohort).where("cohorts.program_type" => 'mba', program_type: nil).update_all(program_type: 'mba', program_type_confirmed: true)

        # other mba_enabled users have unconfirmed program types
        User.where(mba_enabled: true, program_type: nil).update_all(program_type: 'mba', program_type_confirmed: false)

        # institutional users have institutional program type
        if User.joins(:institutions).where(mba_enabled: true).any?
            raise "Why are there institutional mba_enabled users?"
        end
        User.joins(:institutions).update_all(program_type: 'institutional', program_type_confirmed: true)

        User.joins(:hiring_application).update_all(program_type: 'demo', program_type_confirmed: true)

        # other folks have demo program type
        User.where(program_type: nil).update_all(program_type: 'demo', program_type_confirmed: true)

        create_trigger('users', [
            'email',
            'encrypted_password',
            'reset_password_token',
            'reset_password_sent_at',
            'name',
            'nickname',
            'sign_up_code',
            'reset_password_redirect_url',
            'provider',
            'uid',
            'subscriptions_enabled',
            'notify_email_daily',
            'notify_email_content',
            'notify_email_features',
            'notify_email_reminders',
            'notify_email_newsletter',
            'free_trial_started',
            'confirmed_profile_info',
            'optimizely_referer',
            'active_playlist_locale_pack_id',
            'has_seen_welcome',
            'pref_decimal_delim',
            'pref_locale',
            'school',
            'avatar_url',
            'avatar_provider',
            'mba_content_lockable',
            'job_title',
            'phone',
            'phone_extension',
            'country',
            'has_seen_accepted',
            'can_edit_career_profile',
            'professional_organization_option_id',
            'pref_show_photos_names',
            'identity_verified',
            'sex',
            'ethnicity',
            'race',
            'how_did_you_hear_about_us',
            'address_line_1',
            'address_line_2',
            'city',
            'state',
            'zip',
            'birthdate',
            'anything_else_to_tell_us',
            'pref_keyboard_shortcuts',
            'mba_enabled',
            'program_type',
            'program_type_confirmed'
            # the json columns must be left out of here, since they can't be compared
        ])

        ViewHelpers.migrate(self, 20170329132726)
    end

    def down

        remove_column :users, :program_type, :string
        remove_column :users_versions, :program_type, :string
        remove_column :users, :program_type_confirmed, :string
        remove_column :users_versions, :program_type_confirmed, :string

        create_trigger('users', [
            'email',
            'encrypted_password',
            'reset_password_token',
            'reset_password_sent_at',
            'name',
            'nickname',
            'sign_up_code',
            'reset_password_redirect_url',
            'provider',
            'uid',
            'subscriptions_enabled',
            'notify_email_daily',
            'notify_email_content',
            'notify_email_features',
            'notify_email_reminders',
            'notify_email_newsletter',
            'free_trial_started',
            'confirmed_profile_info',
            'optimizely_referer',
            'active_playlist_locale_pack_id',
            'has_seen_welcome',
            'pref_decimal_delim',
            'pref_locale',
            'school',
            'avatar_url',
            'avatar_provider',
            'mba_content_lockable',
            'job_title',
            'phone',
            'phone_extension',
            'country',
            'has_seen_accepted',
            'can_edit_career_profile',
            'professional_organization_option_id',
            'pref_show_photos_names',
            'identity_verified',
            'sex',
            'ethnicity',
            'race',
            'how_did_you_hear_about_us',
            'address_line_1',
            'address_line_2',
            'city',
            'state',
            'zip',
            'birthdate',
            'anything_else_to_tell_us',
            'pref_keyboard_shortcuts',
            'mba_enabled'
            # the json columns must be left out of here, since they can't be compared
        ])
        ViewHelpers.rollback(self, 20170329132726)
    end

    def create_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end

end

