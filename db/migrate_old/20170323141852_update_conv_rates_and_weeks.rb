class UpdateConvRatesAndWeeks < ActiveRecord::Migration[5.0]
    def up

        # changes to
        # - cohort_conversion_rates
        # - cohort_user_weeks
        # - cohort_status_changes
        # - cohort_user_progress_records
        ViewHelpers.migrate(self, 20170323141852)
    end

    def down
        ViewHelpers.rollback(self, 20170323141852)
    end
end
