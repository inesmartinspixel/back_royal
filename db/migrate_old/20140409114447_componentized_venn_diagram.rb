require File.expand_path('../componentized_migrators/venn_diagram_migrator', __FILE__)

class ComponentizedVennDiagram < ActiveRecord::Migration[4.2]

    disable_ddl_transaction!

    def up
        migrator = VennDiagramMigrator.new
        migrator.change
    end
end


