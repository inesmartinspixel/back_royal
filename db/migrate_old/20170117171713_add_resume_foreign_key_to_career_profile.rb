class AddResumeForeignKeyToCareerProfile < ActiveRecord::Migration[5.0]
    def up
        change_column :career_profiles, :resume_id, "uuid USING resume_id::uuid"
        change_column :career_profiles_versions, :resume_id, "uuid USING resume_id::uuid"
        add_foreign_key :career_profiles, :s3_assets, column: :resume_id
    end

    def down
        remove_foreign_key :career_profiles, column: :resume_id
        change_column :career_profiles, :resume_id, "text USING resume_id::text"
        change_column :career_profiles_versions, :resume_id, "text USING resume_id::text"
    end
end
