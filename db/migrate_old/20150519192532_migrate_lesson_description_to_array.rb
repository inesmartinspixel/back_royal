class MigrateLessonDescriptionToArray < ActiveRecord::Migration[4.2]

    def up
        # convert lessons description field from text to array of text containing old value
        execute "alter table lessons alter description drop default"
        execute "alter table lessons alter description type text[] using array[description]"
        execute "alter table lessons alter description set default '{}'"

        # convert lessons_versions description field from text to array of text containing old value
        execute "alter table lessons_versions alter description type text[] using array[description]"

    end

end
