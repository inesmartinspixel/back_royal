class AddRecurlyUuidToSubscription < ActiveRecord::Migration[4.2]
    def change
        add_column :subscriptions, :recurly_uuid, :string
    end
end
