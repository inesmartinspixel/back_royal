class AddMoreFieldsToHiringApplications < ActiveRecord::Migration[4.2]
    def change

        # Company
        add_column :hiring_applications, :logo_url, :text
        add_column :hiring_applications_versions, :logo_url, :text

        # place_id and place_details coming in a later migration
        # company_year already added
        # company_employee_count already added
        # company_annual_revenue already added

        add_column :hiring_applications, :website_url, :text
        add_column :hiring_applications_versions, :website_url, :text

        # Team
        add_column :hiring_applications, :hiring_for, :text # my own, another, or multiple
        add_column :hiring_applications_versions, :hiring_for, :text

        add_column :hiring_applications, :team_name, :text
        add_column :hiring_applications_versions, :team_name, :text

        add_column :hiring_applications, :team_mission, :text
        add_column :hiring_applications_versions, :team_mission, :text

        add_column :hiring_applications, :role_descriptors, :text, :array => true, :default => []
        add_column :hiring_applications_versions, :role_descriptors, :text, :array => true

        # You
        # user.avatar_url
        # user.job_title
        # job_role already added
        # user.phone

        add_column :hiring_applications, :fun_fact, :text
        add_column :hiring_applications_versions, :fun_fact, :text

        # Candidates
        add_column :hiring_applications, :where_descriptors, :text, :array => true, :default => []
        add_column :hiring_applications_versions, :where_descriptors, :text, :array => true

        add_column :hiring_applications, :position_descriptors, :text, :array => true, :default => []
        add_column :hiring_applications_versions, :position_descriptors, :text, :array => true
    end
end
