class RemoveEmbaInterestDeadline < ActiveRecord::Migration[5.1]
    def change
        remove_column :cohorts, :emba_interest_days_offset, :integer, :default => 0
        remove_column :cohorts_versions, :emba_interest_days_offset, :integer, :default => 0

        remove_column :curriculum_templates, :emba_interest_days_offset, :integer, :default => 0
        remove_column :curriculum_templates_versions, :emba_interest_days_offset, :integer, :default => 0
    end
end
