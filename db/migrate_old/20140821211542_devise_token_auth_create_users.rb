class DeviseTokenAuthCreateUsers < ActiveRecord::Migration[4.2]

  # Adds new columns, tweaks indices, and performs backfills to support devise_token_auth schema

  def self.up

    ## Recoverable
    add_column :users, :reset_password_redirect_url, :string

    ## Confirmable
    add_column :users, :confirmation_token, :string
    add_column :users, :confirmed_at, :datetime
    add_column :users, :confirmation_sent_at, :datetime
    add_column :users, :confirm_success_url, :string
    add_column :users, :unconfirmed_email, :string # Only if using reconfirmable

    ## Unique OAuth ID
    add_column :users, :provider, :string
    add_column :users, :uid, :string, :null => false, :default => ''

    ## Tokens
    add_column :users, :tokens, :text

    ## Remove old constraints
    change_column :users, :email, :string, :null => true

    ## Backfill confirmed status
    User.all.each do |user|
        user.update!({:provider => 'email',
                                :confirmed_at => DateTime.now, :confirm_success_url => '/', :confirmation_sent_at => DateTime.now,
                                :uid => user.email})
    end

    ## New Indexes
    add_index :users, :confirmation_token,   :unique => true
    add_index :users, :uid,                  :unique => true


  end

  def self.down
    remove_index :users, :confirmation_token
    remove_index :users, :uid
    remove_columns :users, :reset_password_redirect_url, :confirmation_token, :confirmed_at, :confirmation_sent_at, :confirm_success_url, :unconfirmed_email, :provider, :uid, :tokens
    change_column :users, :email, :string, :null => false
  end


end
