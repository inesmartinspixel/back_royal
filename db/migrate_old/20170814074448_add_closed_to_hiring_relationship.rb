class AddClosedToHiringRelationship < ActiveRecord::Migration[5.1]
    def change

        add_column :hiring_relationships, :hiring_manager_closed, :boolean
        add_column :hiring_relationships_versions, :hiring_manager_closed, :boolean

        add_column :hiring_relationships, :candidate_closed, :boolean
        add_column :hiring_relationships_versions, :candidate_closed, :boolean


        unless reverting?

            # update all currently archived conversations to closed
            execute(%Q~

-- first, grab a record for each user/conversation pair (this should
-- return twice as many rows as there are conversations, since each
-- conversation has 2 participants)
--
-- The record includes a boolean indicating if the user has archived
-- this conversation

with user_conversations AS MATERIALIZED (
    SELECT
        "mailboxer_conversations".id conversation_id
        , "mailboxer_receipts"."receiver_id" as user_id

        -- do a subquery to grab the other recipient id
        -- associated with this conversation
        , (
            SELECT
                (array_agg(distinct rec2.receiver_id))[1]
            from
                mailboxer_conversations conv2
                join mailboxer_notifications not2
                    on not2.conversation_id = conv2.id
                join mailboxer_receipts rec2
                    on rec2."notification_id" = not2."id"
                    and rec2.receiver_id != "mailboxer_receipts"."receiver_id"
            where conv2.id = "mailboxer_conversations".id
            group by conv2.id
        ) as other_guy
        , count(*) as message_count

        -- if all messages are trashed for this user, then this conversation
        -- is considered to be archived
        , count(case when trashed then true else null end) = count(*) as archived
    FROM "mailboxer_conversations"
        INNER JOIN "mailboxer_notifications"
            ON "mailboxer_notifications"."conversation_id" = "mailboxer_conversations"."id" AND
               "mailboxer_notifications"."type" IN ('Mailboxer::Message')
        INNER JOIN "mailboxer_receipts" ON "mailboxer_receipts"."notification_id" = "mailboxer_notifications"."id"
    WHERE "mailboxer_notifications"."type" = 'Mailboxer::Message' AND
          "mailboxer_receipts"."receiver_type" = 'User'
    group by
        "mailboxer_conversations".id
        , "mailboxer_receipts"."receiver_id"
)



-- The only necessary thing in this step is adding in the
-- is_hm so we know which records are for hiring managers.
-- I added in emaisl here too, but that was just to help me
-- debug.  This should still return twice as many records as
-- there are conversations, but it actually returns a bit
-- less than than because there are a few conversations for users
-- who have been deleted
, conversations_2 AS MATERIALIZED (
    SELECT
        users.email
        , hiring_applications.id is not null as is_hm
        , other_guys.email other_guys_email
        , user_conversations.*
    FROM user_conversations
        JOIN users ON user_conversations.user_id = users.id
        JOIN users other_guys ON other_guys.id = user_conversations.other_guy
        left join hiring_applications
            on hiring_applications.user_id = users.id
    ORDER BY users.email
        , other_guys.email
)


-- split up the full list into one list for hiring managers
-- and one for candidates.  Each of these should have as many
-- records as there are conversations (with the caveat about the
-- deleted users)
, hiring_manager_conversations AS MATERIALIZED (
    select * from conversations_2 where is_hm=true
)

, candidate_conversations AS MATERIALIZED (
    select * from conversations_2 where is_hm=false
)

-- Join the hiring managers and the users together, recording
-- a boolean for each one indicating if they have closed (archived)
-- the conversation
, closed_info AS MATERIALIZED (
    SELECT
        hiring_manager_conversations.user_id as hiring_manager_id
        , candidate_conversations.user_id as candidate_id
        , hiring_manager_conversations.archived as hiring_manager_closed
        , candidate_conversations.archived as candidate_closed
    from hiring_manager_conversations
        left outer join candidate_conversations
            on hiring_manager_conversations.conversation_id = candidate_conversations.conversation_id
)

-- Finally, update each hiring relationship that has been closed by
-- someone, setting the appropriate closed flag(s)
UPDATE hiring_relationships
SET hiring_manager_closed=closed_info_subquery.hiring_manager_closed,
    candidate_closed=closed_info_subquery.candidate_closed
FROM (select * from closed_info) AS closed_info_subquery
WHERE
    hiring_relationships.hiring_manager_id=closed_info_subquery.hiring_manager_id
    and hiring_relationships.candidate_id=closed_info_subquery.candidate_id
            ~)

        # also close relationships that were archived before there were any
        # conversations
        execute "update hiring_relationships set candidate_closed=true where candidate_archived_without_conversation=true"
        execute "update hiring_relationships set hiring_manager_closed=true where hiring_manager_archived_without_conversation=true"
        end

    end
end
