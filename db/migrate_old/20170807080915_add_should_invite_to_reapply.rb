class AddShouldInviteToReapply < ActiveRecord::Migration[5.1]
    def change

        add_column :cohort_applications, :should_invite_to_reapply, :boolean
        add_column :cohort_applications_versions, :should_invite_to_reapply, :boolean

    end
end
