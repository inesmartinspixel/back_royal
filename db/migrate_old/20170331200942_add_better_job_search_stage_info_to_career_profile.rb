class AddBetterJobSearchStageInfoToCareerProfile < ActiveRecord::Migration[5.0]
    def up
        add_column :career_profiles, :interested_in_joining_new_company, :text
        add_column :career_profiles, :locations_of_interest, :text, :array => true
        add_column :career_profiles_versions, :interested_in_joining_new_company, :text
        add_column :career_profiles_versions, :locations_of_interest, :text, :array => true

        # Immediately backfill locations_of_interest to ['flexible']
        ActiveRecord::Base.connection.execute("UPDATE career_profiles SET locations_of_interest = '{flexible}'")

        # Backfill interested_in_joining_new_company based on job_search_stage.
        # Doing this in a rake task takes too long, resulting in task getting killed,
        # so we have to do the backfilling here.
        #
        # job_search_stage          -> interested_in_joining_new_company
        # -------------------------------------------------------------
        # 'not_looking'             -> 'neutral'
        # 'ready_to_start'          -> 'interested'
        # 'currently_interviewing'  -> 'very_interested'
        ActiveRecord::Base.connection.execute("UPDATE career_profiles SET interested_in_joining_new_company = 'neutral' WHERE job_search_stage = 'not_looking'")
        ActiveRecord::Base.connection.execute("UPDATE career_profiles SET interested_in_joining_new_company = 'interested' WHERE job_search_stage = 'ready_to_start'")
        ActiveRecord::Base.connection.execute("UPDATE career_profiles SET interested_in_joining_new_company = 'very_interested' WHERE job_search_stage = 'currently_interviewing'")
    end

    def down
        remove_column :career_profiles, :interested_in_joining_new_company, :text
        remove_column :career_profiles, :locations_of_interest, :text, :array => true
        remove_column :career_profiles_versions, :interested_in_joining_new_company, :text
        remove_column :career_profiles_versions, :locations_of_interest, :text, :array => true
    end
end
