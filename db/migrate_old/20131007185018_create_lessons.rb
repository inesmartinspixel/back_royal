class CreateLessons < ActiveRecord::Migration[4.2]
    def change
        create_table :lessons do |t|
            t.timestamps
            t.string :lesson_guid
            t.string :revision_guid
            t.text :content
            t.boolean :current
        end

        add_index(:lessons, :lesson_guid)
        add_index(:lessons, :revision_guid, :unique => true)
        add_index(:lessons, :current, :unique => true) # there can be many records with current=nil, but only one with current=true (This should have been lesson_guid, current. But it is removed in following migration anyhow)

    end
end
