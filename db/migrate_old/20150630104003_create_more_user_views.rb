class CreateMoreUserViews < ActiveRecord::Migration[4.2]
    def up

        # *************************************************************** #

        # We no longer need these views.  Because of the difficulty with
        # views and dependencies, the only way I could figure out to
        # fix this was to go back and mess with old migrations.

        # *************************************************************** #







        # execute %Q|
        #     CREATE VIEW consumer_users AS
        #     SELECT users.*
        #     FROM users
        #         JOIN users_roles ON users_roles.user_id = users.id
        #         JOIN roles ON users_roles.role_id = roles.id
        #         LEFT JOIN institutions_users ON institutions_users.user_id = users.id
        #     WHERE
        #         roles.name = 'learner'
        #         AND email NOT LIKE '%pedago.com%'
        #         AND email NOT LIKE '%smart.ly%'
        #         AND email NOT LIKE '%workaround.com%'
        #         AND institutions_users.institution_id IS NULL;
        # |

        # execute %Q|
        #     CREATE VIEW consumer_users_events AS
        #     SELECT events.*
        #     FROM events
        #         JOIN consumer_users ON events.user_id = consumer_users.id
        # |

        # execute %Q|
        #     CREATE VIEW institutional_users AS
        #     SELECT users.*,
        #         institutions.name as institution_name
        #     FROM users
        #         JOIN users_roles ON users_roles.user_id = users.id
        #         JOIN roles ON users_roles.role_id = roles.id
        #         JOIN institutions_users ON institutions_users.user_id = users.id
        #         JOIN institutions ON institutions_users.institution_id = institutions.id
        #     WHERE
        #         roles.name = 'learner'
        #         AND email NOT LIKE '%pedago.com%'
        #         AND email NOT LIKE '%smart.ly%'
        #         AND email NOT LIKE '%workaround.com%';
        # |

        # execute %Q|
        #     CREATE VIEW institutional_users_events AS
        #     SELECT events.*
        #     FROM events
        #         JOIN institutional_users ON events.user_id = institutional_users.id
        # |
    end

    def down
        # execute 'DROP VIEW consumer_users_events'
        # execute 'DROP VIEW consumer_users'
        # execute 'DROP VIEW institutional_users_events'
        # execute 'DROP VIEW institutional_users'
    end
end
