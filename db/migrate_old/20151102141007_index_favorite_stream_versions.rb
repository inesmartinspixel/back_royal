class IndexFavoriteStreamVersions < ActiveRecord::Migration[4.2]
    def change

        add_index :lesson_streams_users_versions, [:user_id, :version_created_at], :name => :users_and_date_on_stream_users_versions
    end
end
