# Recurly identifies accounts on their end by a customer_id field
# we are supposed to manage server-side and provide to API calls that create and
# update recurly accounts. Backfill these, ensuring they are unique.
# Newly created users will get this populated after_create
class BackfillRecurlyCustomerIds < ActiveRecord::Migration[4.2]
    def change
        backfill_recurly_customer_ids
    end

    def backfill_recurly_customer_ids
        batch = 0
        User.find_in_batches(batch_size: 10) do |users|
            puts " ************ Processing user batch ##{batch}"

            users.each_with_index do |user, i|
                puts " ************ Processing user #{user.email}"

                if user.customer_id.blank?
                    user.customer_id = SecureRandom.uuid
                    user.save!
                end
            end

            batch += 1
        end
    end
end
