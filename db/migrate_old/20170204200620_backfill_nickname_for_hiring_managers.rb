class BackfillNicknameForHiringManagers < ActiveRecord::Migration[5.0]
    def up
        hiring_manager_query = User.joins("JOIN hiring_applications ON users.id = hiring_applications.user_id")
            .where("users.nickname IS NULL")

        puts "Backfilling nickname for #{hiring_manager_query.count} hiring managers"
        hiring_manager_count = 0
        hiring_manager_query.each do |user|
            user.nickname = user.name
            user.save(validate: false) # bypass validations
            hiring_manager_count += 1
        end
        puts "Backfilled #{hiring_manager_count} nicknames for hiring managers"
    end
end
