class AddProcessedTargetedCareerProfileIds < ActiveRecord::Migration[5.0]
    def change
        add_column :hiring_applications, :processed_targeted_career_profile_ids, :boolean, default: false, :null => false
        add_column :hiring_applications_versions, :processed_targeted_career_profile_ids, :boolean
    end
end
