class AddTweetTemplateTextField < ActiveRecord::Migration[4.2]

    def up
        add_column :global_metadata, :default_tweet_template, :text
        add_column :entity_metadata, :tweet_template, :text
        execute "DROP TYPE entity_metadata_json_v1"
        execute "CREATE TYPE entity_metadata_json_v1 AS (id uuid, title varchar(256), description varchar(256), canonical_url text, tweet_template text, image json)"

        bos = GlobalMetadata.find_by_site_name("BlueOcean")
        if !bos.nil?
            bos.default_tweet_template = "Master #BlueOcean Strategy with the new course by #Smartly- free! https://smart.ly/blue-ocean-strategy via @SmartlyHQ @BlueOceanStrtgy @HarvardBiz"
            bos.save!
        end

        smartly = GlobalMetadata.find_by_site_name("Smartly")
        if !smartly.nil?
            smartly.default_tweet_template = "I finished a course by @SmartlyHQ - fun 5-min lessons to get smarter at work https://smart.ly"
            smartly.save!
        end

        create_default_lesson_tweet_template
        create_default_stream_tweet_template

    end

    def create_default_lesson_tweet_template
        Lesson.find_in_batches(batch_size: 10) do |batch|
            batch.each do |lesson|
                lesson.entity_metadata.tweet_template = "I finished '#{lesson.title}' by @SmartlyHQ"
                lesson.entity_metadata.save!
            end
        end
    end

    def create_default_stream_tweet_template
        Lesson::Stream.find_in_batches(batch_size: 10) do |batch|
            batch.each do |stream|
                stream.entity_metadata.tweet_template = "I finished '#{stream.title}' by @SmartlyHQ - fun 5-min lessons to get smarter at work https://smart.ly"
                stream.entity_metadata.save!
            end
        end
        stream = Lesson::Stream.find_by_title("Blue Ocean Strategy")
        if !stream.nil?
            stream.entity_metadata.tweet_template = "Master #BlueOcean Strategy with the new course by #Smartly- free! https://smart.ly/blue-ocean-strategy via @SmartlyHQ @BlueOceanStrtgy @HarvardBiz"
            stream.entity_metadata.save!
        end
    end

    def down
        remove_column :global_metadata, :default_tweet_template
        remove_column :entity_metadata, :tweet_template
        execute "DROP TYPE entity_metadata_json_v1"
        execute "CREATE TYPE entity_metadata_json_v1 AS (id uuid, title varchar(256), description varchar(256), canonical_url text, image json)"
    end

end
