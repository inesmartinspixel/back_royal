require File.expand_path('../20161116201437_views_for_mba_score_query.rb', __FILE__)

class MaterializeProgressViews < ActiveRecord::Migration[5.0]
    def up

        # I would have thought etl already had these permissions, and maybe it
        # does on prod.  But my local did not so we ensure it here.  We do not
        # try to undo this in the down.
        execute('alter default privileges in schema etl grant select on tables to etl;')
        execute 'CREATE EXTENSION IF NOT EXISTS tablefunc' # crosstab

        DbLinker.setup_dblink('red_royal')

        %w(
            cohort_applications_plus
            cohort_user_lesson_progress_records
            cohort_status_changes
            cohort_user_weeks
            cohort_user_progress_records
            cohort_user_progress_records_with_weeks
            cohort_conversion_rates
        ).each do |name|
            execute("DROP VIEW IF EXISTS #{name} CASCADE")
        end

        execute 'alter table etl.user_lesson_progress_records rename to user_lesson_progress_records_bak'
        execute 'alter table etl.user_progress_records rename to user_progress_records_bak'
        ViewHelpers.set_versions(self, 20170221140313)
    end

    def down

        ViewHelpers.set_versions(self, 20170221140313 - 1)

        execute 'alter table etl.user_lesson_progress_records_bak rename to user_lesson_progress_records'
        execute 'alter table etl.user_progress_records_bak rename to user_progress_records'

        views = %w(
            cohort_applications_plus
            cohort_user_lesson_progress_records
            cohort_user_weeks
            cohort_status_changes
            cohort_user_progress_records
            cohort_user_progress_records_with_weeks
            cohort_conversion_rates
            playlist_progress
        )

        views.each do |name|
            old_migration = ViewsForMbaScoreQuery.new
            old_migration.send(name.to_sym) if old_migration.respond_to?(name.to_sym)
        end
    end
end
