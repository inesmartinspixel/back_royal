class UpdateProfilesForExpandedEmployeeOptions < ActiveRecord::Migration[4.2]
    def up

        CareerProfile.where("'500_plus' = ANY(company_sizes_of_interest)").each do |career_profile|
            career_profile.company_sizes_of_interest.delete('500_plus')
            career_profile.company_sizes_of_interest << '500-4999'
            career_profile.company_sizes_of_interest << '5000_plus'
        end

        execute "UPDATE hiring_applications SET company_employee_count = '500-4999' WHERE id='d104285e-9a9f-41db-a7ef-64856242698c'"

    end

    def down


        CareerProfile.where("'500-4999' = ANY(company_sizes_of_interest)").each do |career_profile|
            career_profile.company_sizes_of_interest.delete('500-4999')
            career_profile.company_sizes_of_interest.delete('5000_plus')
            career_profile.company_sizes_of_interest << '500_plus'
        end

        execute "UPDATE hiring_applications SET company_employee_count = '500_plus' WHERE id='d104285e-9a9f-41db-a7ef-64856242698c'"
    end
end
