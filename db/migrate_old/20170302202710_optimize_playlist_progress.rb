class OptimizePlaylistProgress < ActiveRecord::Migration[5.0]
    def up

        # update PlaylistProgress and PublishedPlaylists
        ViewHelpers.set_versions(self, 20170302202710)

    end

    def down

        ViewHelpers.set_versions(self, 20170302202710 - 1)

    end
end
