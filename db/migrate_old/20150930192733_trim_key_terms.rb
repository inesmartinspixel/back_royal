class TrimKeyTerms < ActiveRecord::Migration[4.2]
    def up

        VersionMigrator.new(Lesson).each() do |lesson|
            changed = false
            if lesson.key_terms && lesson.key_terms.any?
                stripped = lesson.key_terms.collect(&:strip)
                if lesson.key_terms != stripped
                    changed = true
                    lesson.key_terms = stripped
                end
            end
            :skip unless changed
        end

    end
end
