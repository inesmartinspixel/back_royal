class CleanupOauthFakeEmails < ActiveRecord::Migration[4.2]
    def up
        cleanup_fake_emails
        fix_nil_emails
    end

    def cleanup_fake_emails

        users = User.where("email like '%example.com' AND provider != 'email'")
        total = users.count
        users.each_with_index do |user, i|
            user.email = nil
            user.confirmed_profile_info = false
            user.save!
        end
        puts "processed #{total} users"

    end

    def fix_nil_emails

        users_versions = Class.new(ActiveRecord::Base) do
            self.table_name = 'users_versions'
        end

        users = User.where("email is null")
        total = users.count
        users.each_with_index do |user, i|
            emails = users_versions.where(id: user.id).pluck('email').compact.uniq.reject { |email| email.match(/example.com/) }
            user.email = emails.first
            user.email = nil unless user.valid?  # avoid duplicate key errors
            user.confirmed_profile_info = false
            user.save!
            puts " - updated email for user #{user.id} to #{user.email}"
        end
        puts "processed #{total} users"

    end
end
