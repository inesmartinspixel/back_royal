class AddArchivedColumnsToHiringRelationships < ActiveRecord::Migration[4.2]
    def change

        add_column :hiring_relationships, :hiring_manager_archived_without_conversation, :boolean, :default => false
        add_column :hiring_relationships_versions, :hiring_manager_archived_without_conversation, :boolean
        add_column :hiring_relationships, :candidate_archived_without_conversation, :boolean, :default => false
        add_column :hiring_relationships_versions, :candidate_archived_without_conversation, :boolean

    end
end