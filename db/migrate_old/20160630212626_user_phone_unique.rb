class UserPhoneUnique < ActiveRecord::Migration[4.2]
    def change
        add_index :users, :phone, where: "provider = 'phone_no_password'", unique: true
    end
end
