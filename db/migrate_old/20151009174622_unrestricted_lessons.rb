class UnrestrictedLessons < ActiveRecord::Migration[4.2]
    def change

        add_column :lessons, :unrestricted, :boolean, :default => false
        add_column :lessons_versions, :unrestricted, :boolean, :default => false

    end
end
