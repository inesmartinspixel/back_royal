class AddProviderUserInfoToUsers < ActiveRecord::Migration[4.2]
    def change

        add_column :users, :provider_user_info, :json
        add_column :users_versions, :provider_user_info, :json

        # we do not add provider_user_info to the create_trigger list because
        # there is no comparison operator for json fields. This is
        # a little unfortunate, as we would probably like to store
        # changes to this, but it's not worth any extra work, I think.

    end
end
