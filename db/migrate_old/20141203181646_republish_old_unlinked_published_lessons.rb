class RepublishOldUnlinkedPublishedLessons < ActiveRecord::Migration[4.2]

    def change
        lessons = Lesson.all.select { |l| l.has_published_version? && l.published_version.content.frames.map(&:frame_type).uniq != ['componentized'] }; 'end'

        lessons.each do |lesson|
            lesson.publish!
        end

    end

end
