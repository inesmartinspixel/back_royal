class RenameJmuGroup < ActiveRecord::Migration[4.2]

    def up
        execute "UPDATE access_groups SET name = 'JMUFall2016'  WHERE name = 'JMU2016'"
    end

    def down
        execute "UPDATE access_groups SET name = 'JMU2016'  WHERE name = 'JMUFall2016'"
    end

end