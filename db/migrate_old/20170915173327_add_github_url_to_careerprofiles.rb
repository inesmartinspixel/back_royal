class AddGithubUrlToCareerprofiles < ActiveRecord::Migration[5.1]
    def up
        add_column :career_profiles, :github_profile_url, :text
        add_column :career_profiles_versions, :github_profile_url, :text
    end

    def down
        remove_column :career_profiles, :github_profile_url
        remove_column :career_profiles_versions, :github_profile_url
    end
end
