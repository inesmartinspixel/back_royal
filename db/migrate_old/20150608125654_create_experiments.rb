class CreateExperiments < ActiveRecord::Migration[4.2]
    def change

        create_table :experiments, id: :uuid do |t|
            t.timestamps

            t.string :title

        end

        add_index :experiments, :title, :unique => true


        create_table :experiment_variations, id: :uuid do |t|
            t.timestamps

            t.string :title,           null: false
            t.uuid :experiment_id,           null: false

        end

        add_index :experiment_variations, [:experiment_id, :title], :unique => true

        create_table :experiment_variations_users, id: :uuid do |t|
            t.uuid      :user_id,           null: false
            t.uuid      :experiment_id,     null: false # joining with experiments so that we can add a unique constraint
            t.uuid      :experiment_variation_id,  null: false
        end

        add_index :experiment_variations_users, [:user_id, :experiment_id], :unique => true

        add_foreign_key :experiment_variations, :experiments, column: :experiment_id, primary_key: :id
        add_foreign_key :experiment_variations_users, :experiment_variations, column: :experiment_variation_id, primary_key: :id

        # purposely not adding a foreign key on users because
        # 1) even if we deleted a user we wouldn't want to delete this data
        # 2) we should be able to assign experiments to anonymous users

    end
end
