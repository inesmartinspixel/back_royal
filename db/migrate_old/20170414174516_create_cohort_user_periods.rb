class CreateCohortUserPeriods < ActiveRecord::Migration[5.0]
    def up

        ActiveRecord::Base.connection.execute(%Q~
            CREATE AGGREGATE array_cat_agg(anyarray) (
                SFUNC=array_cat,
                STYPE=anyarray
            );

            CREATE FUNCTION array_sort_unique(ANYARRAY) RETURNS ANYARRAY
            LANGUAGE SQL
            AS $body$
              SELECT ARRAY(
                SELECT DISTINCT $1[s.i]
                FROM generate_series(array_lower($1,1), array_upper($1,1)) AS s(i)
                ORDER BY 1
              );
              $body$;
        ~)

        ViewHelpers.migrate(self, 20170414174516)
    end

    def down
        ViewHelpers.rollback(self, 20170414174516)

        ActiveRecord::Base.connection.execute(%Q~
            DROP AGGREGATE array_cat_agg(anyarray);
            DROP FUNCTION array_sort_unique(ANYARRAY);
        ~)
    end
end
