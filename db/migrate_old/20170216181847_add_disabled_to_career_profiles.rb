class AddDisabledToCareerProfiles < ActiveRecord::Migration[5.0]
    def change
        add_column :career_profiles, :do_not_create_relationships, :boolean, :default => false, :null => false
        add_column :career_profiles_versions, :do_not_create_relationships, :boolean
    end
end
