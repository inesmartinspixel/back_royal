class CreateCollectionConcepts < ActiveRecord::Migration[4.2]
	def change
		create_table :collection_concepts do |t|
			t.references :collection, index: true
			t.references :concept, index: true

			t.timestamps
		end
	end
end
