class AddLessonFullTextSupport < ActiveRecord::Migration[4.2]

    def up
        # create the table and constraints
        create_table :lesson_fulltext, id: false, force: :cascade do |t|
            t.uuid      :lesson_id, null: false
            t.tsvector  :content_vector
            t.tsvector  :comments_vector
            t.text      :content_text
            t.text      :comments_text
            t.timestamps
        end
        add_foreign_key :lesson_fulltext, :lessons

        # add support for trigram index optimization of non-left-anchored LIKE searches
        execute "CREATE EXTENSION pg_trgm"
    end

    def down
        remove_foreign_key :lesson_fulltext, :lessons
        drop_table :lesson_fulltext, force: :cascade
        execute "DROP EXTENSION pg_trgm"
    end

end
