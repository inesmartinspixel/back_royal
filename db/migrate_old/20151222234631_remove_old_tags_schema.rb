class RemoveOldTagsSchema < ActiveRecord::Migration[4.2]

    def up

        execute "DROP TABLE institutions_groups_versions"
        execute "DROP TABLE institutions_groups"

        execute "DROP TABLE tags_versions"
        execute "DROP TABLE tags"

    end

    def down

        # pg_dump back_royal_development -st tags

        execute "CREATE TABLE tags (
                    id uuid DEFAULT uuid_generate_v4() NOT NULL,
                    entity_type character varying(255),
                    entity_id uuid,
                    text character varying(255),
                    created_at timestamp without time zone,
                    updated_at timestamp without time zone
                )"

        execute "ALTER TABLE ONLY tags ADD CONSTRAINT tags_pkey PRIMARY KEY (id)"
        execute "CREATE INDEX index_tags_on_created_at ON tags USING btree (created_at)"
        execute "CREATE INDEX index_tags_on_entity_type_and_entity_id ON tags USING btree (entity_type, entity_id)"
        execute "CREATE INDEX index_tags_on_entity_type_and_text ON tags USING btree (entity_type, text)"
        execute "CREATE INDEX index_tags_on_text ON tags USING btree (text)"
        execute "CREATE TRIGGER tags_versions AFTER INSERT OR DELETE OR UPDATE ON tags FOR EACH ROW EXECUTE PROCEDURE process_tags_version()"


        # pg_dump back_royal_development -st tags_versions

        execute "CREATE TABLE tags_versions (
            version_id uuid DEFAULT uuid_generate_v4(),
            operation character varying(1) NOT NULL,
            version_created_at timestamp without time zone NOT NULL,
            id uuid NOT NULL,
            entity_type character varying(255),
            entity_id uuid,
            text character varying(255),
            created_at timestamp without time zone,
            updated_at timestamp without time zone
        )"

        execute "CREATE INDEX index_tags_versions_on_id_and_updated_at ON tags_versions USING btree (id, updated_at)"


        # pg_dump back_royal_development -st institutions_groups


        execute "CREATE TABLE institutions_groups (
                    institution_id uuid,
                    tag_id uuid
                )"

        execute "CREATE TRIGGER institutions_groups_versions AFTER INSERT OR DELETE OR UPDATE ON institutions_groups FOR EACH ROW EXECUTE PROCEDURE process_institutions_groups_version()"
        execute "ALTER TABLE ONLY institutions_groups ADD CONSTRAINT fk_rails_72c4de7cd3 FOREIGN KEY (institution_id) REFERENCES institutions(id)"
        execute "ALTER TABLE ONLY institutions_groups ADD CONSTRAINT fk_rails_fb462ebc9e FOREIGN KEY (tag_id) REFERENCES tags(id)"



        # pg_dump back_royal_development -st institutions_groups_versions

        execute "CREATE TABLE institutions_groups_versions (
            version_id uuid DEFAULT uuid_generate_v4(),
            operation character varying(1) NOT NULL,
            version_created_at timestamp without time zone NOT NULL,
            institution_id uuid,
            tag_id uuid
        )"


    end
end
