require File.expand_path('../20161116201437_views_for_mba_score_query.rb', __FILE__)

class UpdateGradRequirements < ActiveRecord::Migration[5.0]
    def up
        cohort_user_progress_records
    end

    def down
        ViewsForMbaScoreQuery.new.cohort_user_progress_records
    end

    def cohort_user_progress_records
        execute %Q~
            create or replace view cohort_user_progress_records as
            with with_counts AS MATERIALIZED (
                select
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , cohort_user_lesson_progress_records.user_id
                    , cohort_applications_plus.current_status
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            when foundations = true and completed_at is null then 0
                            else null
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when in_curriculum = true and completed_at is not null then 1
                            when in_curriculum = true and completed_at is null then 0
                            else null
                            end
                        ) as curriculum_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            when test = true and completed_at is null then 0
                            else null
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            when elective = true and completed_at is null then 0
                            else null
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_last else null end) as average_assessment_score_last
                    , avg(case when test then average_assessment_score_first else null end) as avg_test_score
                    , max(case when in_curriculum = true then cohort_user_lesson_progress_records.completed_at else null end) as last_curriculum_lesson_completed_at
                    , min(case when in_curriculum = true then cohort_user_lesson_progress_records.completed_at else null end) as first_curriculum_lesson_completed_at
                from cohort_user_lesson_progress_records
                    join cohort_applications_plus on
                        cohort_applications_plus.cohort_id = cohort_user_lesson_progress_records.cohort_id
                        and cohort_applications_plus.user_id = cohort_user_lesson_progress_records.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_user_lesson_progress_records.cohort_id
                    , cohort_user_lesson_progress_records.cohort_name
                    , cohort_user_lesson_progress_records.user_id
                    , cohort_applications_plus.current_status
            )
            , with_averages AS MATERIALIZED (
                select
                    with_counts.*
                    , case
                        when curriculum_lessons_complete = in_curriculum_lesson_count and test_lessons_complete = test_lesson_count and with_counts.cohort_name='MBA1'
                            then (
                                0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                + 0.3*(case when average_assessment_score_last is null then 0 else average_assessment_score_last end)
                            )
                        else
                            -- FIXME: eventually we want to have this logic work for everyone
                            null
                        end as final_score
                    , foundations_lessons_complete::float / cohort_content_details.foundations_lesson_count as foundations_perc_complete
                    , curriculum_lessons_complete::float / cohort_content_details.in_curriculum_lesson_count as curriculum_perc_complete
                    , test_lessons_complete::float / cohort_content_details.test_lesson_count as test_perc_complete
                    , elective_lessons_complete::float / cohort_content_details.elective_lesson_count as elective_perc_complete
                from with_counts
                    join cohort_content_details on with_counts.cohort_id = cohort_content_details.cohort_id
            )
            , with_grad_requirements AS MATERIALIZED (
                select
                    with_averages.*
                    , coalesce(final_score > 0.7, false) as meets_graduation_requirements
                from with_averages
            )
            select * from with_grad_requirements
        ~
    end
end
