class AddInternalAttributesToCareerProfiles < ActiveRecord::Migration[5.0]
    def change
        add_column :career_profiles, :internal_quality_score, :text
        add_column :career_profiles_versions, :internal_quality_score, :text

        add_column :career_profiles, :internal_employment_notes, :text
        add_column :career_profiles_versions, :internal_employment_notes, :text

        add_column :career_profiles, :career_profile_active_last_updated_at, :datetime
        add_column :career_profiles_versions, :career_profile_active_last_updated_at, :datetime
    end
end
