class AddScormTokenToInstitutions < ActiveRecord::Migration[4.2]

    def up
        add_column :institutions, :scorm_token, :uuid
        add_column :institutions_versions, :scorm_token, :uuid
    end

    def down
        remove_column :institutions, :scorm_token, :uuid
        remove_column :institutions_versions, :scorm_token, :uuid
    end

end
