require File.expand_path('../20161116201437_views_for_mba_score_query.rb', __FILE__)

class AddInfoToCohortUserWeeks < ActiveRecord::Migration[5.0]
    def up
        cohort_user_progress_records
        cohort_user_weeks
    end

    def down
        # cannot go down
        # down_migration = ViewsForMbaScoreQuery.new
        # down_migration.cohort_user_weeks
        # down_migration.cohort_user_progress_records
    end

    def cohort_user_progress_records
        execute %Q|
            create or replace view cohort_user_progress_records as
            with with_counts AS MATERIALIZED (
                select
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , cohort_applications_plus.user_id
                    , cohort_applications_plus.current_status
                    , sum(case
                            when foundations = true and completed_at is not null then 1
                            when foundations = true and completed_at is null then 0
                            else null
                            end
                        ) as foundations_lessons_complete
                    , sum(case
                            when in_curriculum = true and completed_at is not null then 1
                            when in_curriculum = true and completed_at is null then 0
                            else null
                            end
                        ) as curriculum_lessons_complete
                    , sum(case
                            when test = true and completed_at is not null then 1
                            when test = true and completed_at is null then 0
                            else null
                            end
                        ) as test_lessons_complete
                    , sum(case
                            when elective = true and completed_at is not null then 1
                            when elective = true and completed_at is null then 0
                            else null
                            end
                        ) as elective_lessons_complete
                    , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                    , avg(case when assessment then average_assessment_score_last else null end) as average_assessment_score_last
                    , avg(case when test then average_assessment_score_first else null end) as avg_test_score
                    , max(case when in_curriculum = true then cohort_user_lesson_progress_records.completed_at else null end) as last_curriculum_lesson_completed_at
                    , min(case when in_curriculum = true then cohort_user_lesson_progress_records.completed_at else null end) as first_curriculum_lesson_completed_at
                from cohort_applications_plus
                    left join cohort_user_lesson_progress_records on
                        cohort_applications_plus.cohort_id = cohort_user_lesson_progress_records.cohort_id
                        and cohort_applications_plus.user_id = cohort_user_lesson_progress_records.user_id
                where
                    cohort_applications_plus.was_accepted=true
                group by
                    cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , cohort_applications_plus.user_id
                    , cohort_applications_plus.current_status
            )
            , with_averages AS MATERIALIZED (
                select
                    with_counts.*
                    , case
                        when with_counts.cohort_name='MBA1'
                            then (
                                0.7*(case when avg_test_score is null then 0 else avg_test_score end)
                                + 0.3*(case when average_assessment_score_last is null then 0 else average_assessment_score_last end)
                            )
                        else
                            -- FIXME: eventually we want to have this logic work for everyone
                            null
                        end as final_score
                    , foundations_lessons_complete::float / cohort_content_details.foundations_lesson_count as foundations_perc_complete
                    , curriculum_lessons_complete::float / cohort_content_details.in_curriculum_lesson_count as curriculum_perc_complete
                    , test_lessons_complete::float / cohort_content_details.test_lesson_count as test_perc_complete
                    , elective_lessons_complete::float / cohort_content_details.elective_lesson_count as elective_perc_complete
                from with_counts
                    join cohort_content_details on with_counts.cohort_id = cohort_content_details.cohort_id
            )
            , with_grad_requirements AS MATERIALIZED (
                select
                    with_averages.*
                    , final_score > 0.7 and curriculum_perc_complete = 1 as meets_graduation_requirements
                from with_averages
            )
            select * from with_grad_requirements;
        |
    end

    def cohort_user_weeks
        execute %Q~
            create or replace view cohort_user_weeks as
            with step_1 AS MATERIALIZED (
                select
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , week_index
                    , case when week_index = 0 then '2000/01/01'::timestamp else cohorts.start_date + (week_index || ' weeks')::interval end as week_start
                    , cohorts.start_date + ((week_index+1) || ' weeks')::interval as week_end

                    , count(
                        case when
                            in_curriculum = true
                            and completed_at > (case when week_index = 0 then '2000/01/01'::timestamp else cohorts.start_date + (week_index || ' weeks')::interval end)
                            and completed_at <= (cohorts.start_date + ((week_index+1) || ' weeks')::interval )
                            then true
                        else null end
                    ) as lessons_completed_in_week
                    , count(case when in_curriculum = true and completed_at <= (cohorts.start_date + ((week_index+1) || ' weeks')::interval) then true else null end) as lessons_completed_by_end_of_week
                    , case
                        when cohort_applications_plus.deferred_at < (cohorts.start_date + ((week_index+1) || ' weeks')::interval) then 'deferred'
                        when cohort_applications_plus.expelled_at < (cohorts.start_date + ((week_index+1) || ' weeks')::interval) then 'expelled'
                        else 'enrolled'
                    end as status_at_week_end

                    , case when
                        week_index*cohort_content_details.expected_pace > cohort_content_details.in_curriculum_lesson_count
                            then cohort_content_details.in_curriculum_lesson_count
                        else
                            week_index*cohort_content_details.expected_pace
                        end as expected_curriculum_lessons_complete

                    , cohort_content_details.foundations_lesson_count + week_index*cohort_content_details.expected_pace_fp as expected_curriculum_lessons_complete_fp
                    , count(case when foundations = true and completed_at <= (cohorts.start_date + ((week_index+1) || ' weeks')::interval) then true else null end) as foundations_lessons_completed_by_end_of_week

                    , count(case when lp.completed_at > accepted_at and lp.completed_at <  cohorts.start_date + ((week_index+1) || ' weeks')::interval then true else null end) > 0 as completed_lesson_after_acceptance
                    , cohort_content_details.foundations_lesson_count
                from
                    cohort_applications_plus
                        cross join (
                            SELECT
                                week_index
                            FROM
                                generate_series(0, 26) AS week_index
                        ) user_weeks
                        join cohorts
                            on cohort_applications_plus.cohort_id = cohorts.id
                        join cohort_content_details
                            on cohort_content_details.cohort_id = cohorts.id
                        left join cohort_user_lesson_progress_records lp
                            on lp.user_id = cohort_applications_plus.user_id
                            and lp.cohort_id = cohort_applications_plus.cohort_id
                where
                    cohort_applications_plus.was_accepted = true
                group by
                    cohort_applications_plus.user_id
                    , cohort_applications_plus.cohort_id
                    , cohort_applications_plus.cohort_name
                    , cohort_applications_plus.deferred_at
                    , cohort_applications_plus.expelled_at
                    , cohort_applications_plus.accepted_at
                    , week_index
                    , cohorts.start_date
                    , cohorts.end_date
                    , cohort_content_details.expected_pace
                    , cohort_content_details.expected_pace_fp
                    , cohort_content_details.foundations_lesson_count
                    , cohort_content_details.in_curriculum_lesson_count
                having
                    cohorts.start_date + (week_index || ' weeks')::interval < now()
            )
            , step_2 AS MATERIALIZED (
                select
                        step_1.*
                        , completed_lesson_after_acceptance AND foundations_lessons_completed_by_end_of_week >= foundations_lesson_count as enrolled_by_end_of_week
                from step_1
            )
            select
                    user_id
                    , cohort_id
                    , cohort_name
                    , week_index
                    , week_start
                    , week_end
                    , lessons_completed_in_week
                    , lessons_completed_by_end_of_week
                    , case when status_at_week_end = 'enrolled' and enrolled_by_end_of_week = false
                        then 'unenrolled'
                      else status_at_week_end
                      end as status_at_week_end
                    , expected_curriculum_lessons_complete
                    , expected_curriculum_lessons_complete_fp
                    , foundations_lessons_completed_by_end_of_week
                    , completed_lesson_after_acceptance AND foundations_lessons_completed_by_end_of_week >= foundations_lesson_count as enrolled_by_end_of_week
            from step_2
        ~
    end
end
