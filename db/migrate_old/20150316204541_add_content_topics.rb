class AddContentTopics < ActiveRecord::Migration[4.2]
    def up

        create_table :content_topics, id: :uuid do |t|
            t.string      :name,           null: false
            t.timestamps
        end

        create_table :content_topics_daily_lesson_configs, id: false do |t|
            t.uuid      :daily_lesson_config_id,           null: false
            t.uuid      :content_topic_id,  null: false
        end

        create_table :content_topics_lesson_streams, id: false do |t|
            t.uuid      :lesson_stream_id,           null: false
            t.uuid      :content_topic_id,  null: false
        end

        add_index :content_topics, :name, :unique => true
        add_index :content_topics_daily_lesson_configs, [:daily_lesson_config_id, :content_topic_id], :unique => true, :name => "index_unique_topics_daily_lesson_configs"
        add_index :content_topics_lesson_streams, [:lesson_stream_id, :content_topic_id], :unique => true, :name => "index_unique_topics_streams"

        add_foreign_key :content_topics_daily_lesson_configs, :content_topics, column: :content_topic_id, primary_key: :id
        add_foreign_key :content_topics_lesson_streams, :content_topics, column: :content_topic_id, primary_key: :id
        add_foreign_key :content_topics_daily_lesson_configs, :daily_lesson_configs, column: :daily_lesson_config_id, primary_key: :id
        add_foreign_key :content_topics_lesson_streams, :lesson_streams, column: :lesson_stream_id, primary_key: :id

        tables.each do |table_name|
            create_versions_table_and_trigger(table_name)
        end
    end

    def down

        tables.each do |table_name|
            drop_versions_table_and_trigger(table_name)
            drop_table table_name
        end
    end

    def tables
        %w(content_topics_daily_lesson_configs content_topics_lesson_streams content_topics)
    end

    def create_versions_table_and_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.connection.schema_cache.clear!
        klass.reset_column_information


        # create the version table, including all columns from the source table
        # plus a few extras
        create_table "#{table_name}_versions", id: false, force: :cascade do |t|
            t.uuid     "version_id", primary: true, default: "uuid_generate_v4()"
            t.string   "operation", limit: 1, null: false
            t.datetime "version_created_at", null: false

            klass.column_names.each do |column_name|
                type = klass.columns_hash

                column_config = klass.columns_hash[column_name]
                meth = column_config.type.to_sym
                options = {}
                options[:limit] = column_config.limit unless column_config.limit.nil?
                options[:null] = false if column_config.null == false
                # ignore defaults, since they should not be used in the audit table

                t.send(meth, column_name, options)
            end
        end

        # create indexes
        if klass.column_names.include?('id') && klass.column_names.include?('updated_at')
            add_index "#{table_name}_versions", [:id, :updated_at]
        elsif klass.column_names.include?('id')
            add_index "#{table_name}_versions", :id
        end

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "
        execute(trigger_sql)
    end

    def drop_versions_table_and_trigger(table_name)
        execute "DROP TABLE #{table_name}_versions"
        execute "DROP TRIGGER #{table_name}_versions ON #{table_name}"
    end
end
