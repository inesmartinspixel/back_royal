# Devise needs additional fields for lockout strategies
# See also: http://stackoverflow.com/questions/11910176/how-to-make-devise-lockable-with-number-of-failed-attempts
class AddLockableToUsers < ActiveRecord::Migration[4.2]

    def change
        change_table(:users) do |t|
            t.integer   :failed_attempts, :default => 0
            t.string    :unlock_token
            t.datetime  :locked_at
        end
        add_index(:users, :unlock_token, :unique => true)
    end

end