class AddSalaryRangeToCareerProfiles < ActiveRecord::Migration[5.0]
    def change
        add_column :career_profiles, :salary_range, :text
        add_column :career_profiles_versions, :salary_range, :text
    end
end
