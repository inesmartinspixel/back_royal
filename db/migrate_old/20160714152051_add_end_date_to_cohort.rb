class AddEndDateToCohort < ActiveRecord::Migration[4.2]
    def change
        add_column :cohorts, :end_date, :datetime
        add_column :cohorts_versions, :end_date, :datetime

        unless reverting?
            Cohort.all.each do |cohort|
                cohort.end_date = Time.now + 90.days
                cohort.save!
            end

            change_column :cohorts, :end_date, :datetime, :null => false
        end

    end
end
