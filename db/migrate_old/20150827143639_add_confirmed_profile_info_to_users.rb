class AddConfirmedProfileInfoToUsers < ActiveRecord::Migration[4.2]
    def up
        temporarily_delete_user_views do
            add_column :users, :confirmed_profile_info, :boolean, :default => true
            add_column :users_versions, :confirmed_profile_info, :boolean

            create_trigger('users', [
                'email',
                'encrypted_password',
                'reset_password_token',
                'reset_password_sent_at',
                'first_name',
                'last_name',
                'sign_up_code',
                'reset_password_redirect_url',
                'provider',
                'uid',
                'subscriptions_enabled',
                'notify_email_daily',
                'notify_email_content',
                'notify_email_features',
                'notify_email_reminders',
                'notify_email_newsletter',
                'free_trial_started',
                'confirmed_profile_info'
            ])
        end
    end

    def down
        temporarily_delete_user_views do
            remove_column :users, :confirmed_profile_info
            remove_column :users_versions, :confirmed_profile_info

            create_trigger('users', [
                'email',
                'encrypted_password',
                'reset_password_token',
                'reset_password_sent_at',
                'first_name',
                'last_name',
                'sign_up_code',
                'reset_password_redirect_url',
                'provider',
                'uid',
                'subscriptions_enabled',
                'notify_email_daily',
                'notify_email_content',
                'notify_email_features',
                'notify_email_reminders',
                'notify_email_newsletter',
                'free_trial_started'
            ])
        end
    end


    def create_trigger(table_name, columns_to_watch)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"

        if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
        update_command = "
            IF #{if_string} THEN
            #{insert_on_update}
            END IF;
            "

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end

    def temporarily_delete_user_views(&block)
        # this used to do something, but no more
        yield
    end
end
