class CreateSubscriptions < ActiveRecord::Migration[4.2]
    def change
        # Our own personal record of user subscriptions, to mirror Recurly.
        # Why do we need this? "Recurly does not allow accounts to have trial subscriptions without a valid credit card.
        # If you choose to offer trials without credit cards, your application can track the state of the trial
        # and then make the subscribe API calls to Recurly when the trial ends."
        # https://docs.recurly.com/plans
        # Accounts with subscription_type='free' only exist in our system
        # Accounts with subscription_type='pro' exist in both us and recurly, and are linked
        # to the recurly account through the customer_id field in users
        create_table :subscriptions do |t|
            t.timestamps
            t.references :user
            t.string :subscription_type
            t.boolean :free_trial
            t.date :free_trial_start_date
            t.integer :free_trial_days_valid

        end

    end
end
