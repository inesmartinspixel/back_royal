class SwitchNotWillingToRelocateOffFlexible < ActiveRecord::Migration[5.1]
    disable_ddl_transaction!

    def up
        change_column_default(:career_profiles, :locations_of_interest, ["none"])
    end

    def down
        change_column_default(:career_profiles, :locations_of_interest, ["flexible"])
    end
end
