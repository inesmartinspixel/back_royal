class AddScheduleUrlToCohorts < ActiveRecord::Migration[5.0]
    def change
        add_column :cohorts, :external_schedule_url, :text
        add_column :cohorts_versions, :external_schedule_url, :text
    end
end