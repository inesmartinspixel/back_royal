class CreateTimeSeriesThings < ActiveRecord::Migration[4.2]
    def change
        return unless Rails.env == 'test'
        create_table :time_series_things, id: :uuid do |t|
            t.uuid :user_id, null: false
            t.timestamp :time, null: false
        end
    end
end
