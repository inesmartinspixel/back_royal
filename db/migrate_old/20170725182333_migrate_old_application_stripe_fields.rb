class MigrateOldApplicationStripeFields < ActiveRecord::Migration[5.1]

    disable_ddl_transaction!

    def up
        old_users_clause = "cohort_id IN (SELECT id FROM cohorts WHERE program_type = 'emba' AND name != 'EMBA4')"

        # we don't want this change to trigger events
        execute "UPDATE cohort_applications SET registered = TRUE WHERE status = 'accepted' AND #{old_users_clause}"
        execute "UPDATE cohort_applications SET registered = FALSE WHERE status <> 'accepted' AND #{old_users_clause}"


    end

    def down
        all_emba_clause = "cohort_id IN (SELECT id FROM cohorts WHERE program_type = 'emba')"

        # we don't want this change to trigger events
        execute "UPDATE cohort_applications SET registered = NULL WHERE #{all_emba_clause}"

    end

end
