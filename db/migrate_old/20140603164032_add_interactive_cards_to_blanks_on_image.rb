require File.expand_path('../componentized_migrators/blanks_on_image_interactive_cards_migrator', __FILE__)

class AddInteractiveCardsToBlanksOnImage < ActiveRecord::Migration[4.2]

    def up
        migrator = BlanksOnImageInteractiveCardsMigrator.new
        migrator.change
    end

end