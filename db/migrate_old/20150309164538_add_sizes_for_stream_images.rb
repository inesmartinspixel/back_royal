class AddSizesForStreamImages < ActiveRecord::Migration[4.2]
    def up

        image_ids = Lesson::Stream::Version.distinct.reorder('image_id').pluck('image_id')

        # Delete a few old images that are in an unusable format
        bad_image_ids = S3Asset.where(id: image_ids).where('file_fingerprint IS NULL').pluck('id')
        VersionMigrator.new(Lesson::Stream).where(image_id: bad_image_ids).each do |stream|
            stream.image_id = nil
        end
        S3Asset.where(id: bad_image_ids).delete_all

        images = S3Asset.where(id: image_ids).where('file_fingerprint IS NOT NULL').reorder('created_at desc')
        images.each_with_index do |image, i|
            puts "processing image #{i+1} of #{images.size} (#{image.file.url})"

            image.styles = {

                # student dashboard
                "50x50"  => '50x50>',
                "100x100"  => '100x100>',

                # stream dashboard
                "110x110"   => '110x110>',
                "220x220"   => '220x220>'
            }.to_json

            image.file.reprocess!
            image.save!

        end

    end

    def down
    end
end
