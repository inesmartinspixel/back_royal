class BackfillLessonEditorPermsForLessonAuthors < ActiveRecord::Migration[4.2]
    def change
        puts '-- Backfilling scoped lesson editing permissions to lesson authors --'
        backfill_lesson_edit_permissons
    end


    def backfill_lesson_edit_permissons

        batch = 0
        Lesson.find_in_batches(batch_size: 10) do |lessons|
            puts " ************ Processing lesson batch ##{batch}"

            lessons.each_with_index do |lesson, i|

                puts " ************ Processing lesson: \"#{lesson.title}\" ID: #{lesson.id} : #{i+1} of #{lessons.size}"
                author = lesson.author
                author.add_role :lesson_editor, lesson

            end

            batch += 1
        end

    end
end

