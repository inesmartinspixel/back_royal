class CreateLocalePackJsonV4 < ActiveRecord::Migration[4.2]
    def up
        execute "CREATE TYPE locale_pack_json_v4 AS (id uuid, content_items json, groups json, content_topics json, practice_locale_pack_id uuid, is_practice_for_locale_pack_id uuid, practice_content_items json)"
    end

    def down
        execute "DROP TYPE locale_pack_json_v4"
    end
end
