class CreateContentPublishers < ActiveRecord::Migration[4.2]
    def change

        create_table :content_publishers do |t|
            t.timestamps
            t.integer :lesson_id
            t.integer :lesson_stream_id
            t.integer :published_version_index
        end

        add_index :content_publishers, [:lesson_id], :unique => true
        add_index :content_publishers, [:lesson_stream_id], :unique => true

    end
end
