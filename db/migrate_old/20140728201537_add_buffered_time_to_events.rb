class AddBufferedTimeToEvents < ActiveRecord::Migration[4.2]
    def change
        add_column :events, :total_buffered_seconds, :float
    end
end
