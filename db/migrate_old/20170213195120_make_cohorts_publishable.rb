class MakeCohortsPublishable < ActiveRecord::Migration[5.0]
    def change

        [:cohorts, :cohorts_versions].each do |table|
            add_column table, :was_published, :boolean, :default => false
        end

        add_column :content_publishers, :cohort_version_id, :uuid
        add_foreign_key "content_publishers", "cohorts_versions", column: "cohort_version_id", primary_key: "version_id"
        add_column :content_publishers_versions, :cohort_version_id, :uuid

    end
end
