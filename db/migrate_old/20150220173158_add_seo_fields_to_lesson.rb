class AddSeoFieldsToLesson < ActiveRecord::Migration[4.2]

    def change
        # optimal length: under 55 chars; google typically only displays the first 50-60 characters
        # Place Important Keywords Close to the Front of the Title Tag
        # use the brand name at the end of the title tag
        # http://moz.com/learn/seo/title-tag
        add_column :lessons, :seo_title, :string
        add_column :lessons_versions, :seo_title, :string

        # optimal length for search engines: roughly 155 Characters
        # its important that descriptions be unique across pages
        # http://moz.com/learn/seo/meta-description
        add_column :lessons, :seo_description, :string
        add_column :lessons_versions, :seo_description, :string

        # Since the title is persisted into the url that gets put into the sitemap
        # when we change the title we risk creating what looks to google like
        # duplicate pages: http://moz.com/learn/seo/duplicate-content
        # So persist a canonical url that we can choose to have stay the same
        # independent of title changes.
        # TODO in the future: 301 redirects from new page urls to this canonical
        add_column :lessons, :seo_canonical_url, :text
        add_column :lessons_versions, :seo_canonical_url, :text
    end

end
