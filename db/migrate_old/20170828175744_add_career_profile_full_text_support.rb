class AddCareerProfileFullTextSupport < ActiveRecord::Migration[5.1]

    def up
        create_table :career_profile_fulltext, id: false, force: :cascade do |t|
            t.uuid      :career_profile_id, null: false
            t.tsvector  :keyword_vector
            t.text      :keyword_text
            t.timestamps
        end
        add_foreign_key :career_profile_fulltext, :career_profiles

        if Rails.env == 'test'
            create_table :fulltext_items, id: :uuid do |t|
            end
        end
    end

    def down
        remove_foreign_key :career_profile_fulltext, :career_profiles
        drop_table :career_profile_fulltext, force: :cascade

        if Rails.env == 'test'
            drop_table :fulltext_items
        end
    end

end