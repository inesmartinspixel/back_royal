class AuthTokensJson < ActiveRecord::Migration[4.2]

    def up
        execute "ALTER TABLE users ALTER COLUMN tokens TYPE JSON USING tokens::JSON;"
        execute "ALTER TABLE users_versions ALTER COLUMN tokens TYPE JSON USING tokens::JSON;"
    end

    def down
        execute "ALTER TABLE users ALTER COLUMN tokens TYPE TEXT USING tokens::text;"
        execute "ALTER TABLE users_versions ALTER COLUMN tokens TYPE TEXT USING tokens::text;"
    end

end



