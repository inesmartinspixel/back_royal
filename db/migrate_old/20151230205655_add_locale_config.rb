class AddLocaleConfig < ActiveRecord::Migration[4.2]
    def up
        add_column :users, :pref_locale, :string, default: 'en', null: false
        add_column :users_versions, :pref_locale, :string
    end

    def down
        remove_column :users, :pref_locale
        remove_column :users_versions, :pref_locale
    end
end