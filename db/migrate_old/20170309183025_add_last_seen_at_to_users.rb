class AddLastSeenAtToUsers < ActiveRecord::Migration[5.0]

    def up
        temporarily_delete_user_views do
            add_column :users, :last_seen_at, :datetime
            add_column :users_versions, :last_seen_at, :datetime

            add_index :users, :last_seen_at

            # do not change the users triggers
            # because we don't want to trigger off of last_seen_at anyhow
        end
        User.reset_column_information
    end

    def down
        temporarily_delete_user_views do
            remove_column :users, :last_seen_at, :datetime
            remove_column :users_versions, :last_seen_at, :datetime
        end
        User.reset_column_information
    end

    def temporarily_delete_user_views(&block)
        # ViewHelpers.set_versions(self, 0) # drop all the views

        # this used to do something, but no more
        yield

        # ViewHelpers.set_versions(self, 20170309183025 - 1) # recreate them
    end
end
