class BackfillSomeAddressFields < ActiveRecord::Migration[5.0]
    def up

        users_with_migrated_field = 0

        ActiveRecord::Base.transaction do
            # Go through all users who have a career_profile that has a place_details
            User.includes(:career_profile)
                .where("career_profiles.id IS NOT NULL AND career_profiles.place_details::text != '{}'::text")
                .references(:career_profiles).each_with_index do |user, i|

                user.city = user.career_profile.place_details["locality"]["long"] unless user.career_profile.place_details["locality"].nil?
                user.state = user.career_profile.place_details["administrative_area_level_1"]["long"] unless user.career_profile.place_details["administrative_area_level_1"].nil?
                user.country = user.career_profile.place_details["country"]["short"] unless user.career_profile.place_details["country"].nil?
                user.zip = user.career_profile.place_details["postal_code"]["short"] unless user.career_profile.place_details["postal_code"].nil?
                user.save!(validate: false)

                if user.city || user.state || user.country || user.zip
                    users_with_migrated_field += 1
                    puts "#{user.email}"
                end
            end
        end

        puts "Migrated some fields for #{users_with_migrated_field} users"
    end
end
