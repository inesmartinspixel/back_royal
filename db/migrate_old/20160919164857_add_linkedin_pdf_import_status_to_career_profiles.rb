class AddLinkedinPdfImportStatusToCareerProfiles < ActiveRecord::Migration[4.2]
    def change
        # should be empty, "In progress", "Failed", or "Successful"
        add_column :career_profiles, :linkedin_pdf_import_status, :text
        add_column :career_profiles_versions, :linkedin_pdf_import_status, :text
    end
end
