class SetupEtlUser < ActiveRecord::Migration[4.2]

    # we actually do want transactions here, but the CREATE USER
    # call has to happen outside the transaction, because it is
    # likely to fail
    disable_ddl_transaction!

    def up

        begin
            execute('CREATE USER etl')
            unless ENV['RDS_PASSWORD'].blank?
                execute("ALTER ROLE etl PASSWORD '#{ENV['RDS_PASSWORD']}'")
            end
        rescue ActiveRecord::StatementInvalid => err
            raise err unless err.message.match('PG::DuplicateObject')
        end

        ActiveRecord::Base.transaction do
            current_user = execute('SELECT current_user')[0]['current_user']

            # on RDS, it is necessary for the current_user to be a member of
            # etl in order to create a schema owned by etl.  See
            # - http://stackoverflow.com/questions/26684643/error-must-be-member-of-role-when-creating-schema-in-postgresql
            # - http://stackoverflow.com/questions/26684643/error-must-be-member-of-role-when-creating-schema-in-postgresql/34898033#34898033
            execute("GRANT etl to \"#{current_user}\"")

            execute('CREATE SCHEMA AUTHORIZATION etl')

            # in tests, the etl project needs to be able to create test
            # data in the back_royal public tables.  But in the wild, the etl
            # script should not write to those tables
            privileges = Rails.env == 'test' ? 'ALL PRIVILEGES' : 'SELECT'


            execute("GRANT USAGE ON ALL SEQUENCES IN SCHEMA public TO etl")
            execute("GRANT #{privileges} ON ALL TABLES IN SCHEMA public TO etl")
            execute("ALTER DEFAULT PRIVILEGES FOR ROLE \"#{current_user}\" IN SCHEMA public GRANT #{privileges} ON TABLES TO etl")

            # sanity check
            result = execute("select schema_owner from information_schema.schemata where schema_name='etl'")
            record =  result.to_a[0]
            raise "No etl schema found" unless record
            owner = record['schema_owner']
            raise "Wrong owner for etl schema #{owner.inspect} != #{'etl'.inspect}" unless owner == 'etl'
        end

        execute('ALTER SCHEMA etl OWNER TO etl')
    end

    def down

        ActiveRecord::Base.transaction do
            execute('DROP SCHEMA etl CASCADE')
            execute('DROP OWNED BY etl')
        end
        # do not drop the user, because users exist outside of the
        # database, so if we drop it then rolling back the test db
        # kills development and vice versa

    end
end
