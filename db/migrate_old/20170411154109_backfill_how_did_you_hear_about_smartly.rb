class BackfillHowDidYouHearAboutSmartly < ActiveRecord::Migration[5.0]

    def up
        execute "UPDATE users SET how_did_you_hear_about_us = 'Not sure' WHERE how_did_you_hear_about_us IS NULL AND id IN (SELECT user_id FROM career_profiles WHERE last_calculated_complete_percentage = 100)"
    end

    def down
        execute "UPDATE users SET how_did_you_hear_about_us = NULL WHERE how_did_you_hear_about_us = 'Not sure'"
    end
end
