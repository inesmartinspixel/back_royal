class FilterLessonsWithNoProgress < ActiveRecord::Migration[5.0]
    def up

        # update UserLessonProgressRecords
        ViewHelpers.set_versions(self, 20170301211232)

    end

    def down

        ViewHelpers.set_versions(self, 20170301211232 - 1)

    end
end
