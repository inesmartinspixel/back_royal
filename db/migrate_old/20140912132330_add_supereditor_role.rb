class AddSupereditorRole < ActiveRecord::Migration[4.2]
    def change
        super_editor_emails = ["daniel@pedago.com", "mintz.daniel@gmail.com", "caroline@pedago.com", "dagati.caroline@gmail.com"]

        super_editor_emails.each do |email|
            user = User.find_by_email(email)

            user.add_role :super_editor if !user.blank?
        end
    end
end
