class AddFieldsToHiringApplications < ActiveRecord::Migration[4.2]
    def change
        add_column :hiring_applications, :job_role, :text
        add_column :hiring_applications, :company_year, :int
        add_column :hiring_applications, :company_employee_count, :text
        add_column :hiring_applications, :company_annual_revenue, :text
        add_column :hiring_applications, :company_sells_recruiting_services, :boolean

        add_column :hiring_applications_versions, :job_role, :text
        add_column :hiring_applications_versions, :company_year, :int
        add_column :hiring_applications_versions, :company_employee_count, :text
        add_column :hiring_applications_versions, :company_annual_revenue, :text
        add_column :hiring_applications_versions, :company_sells_recruiting_services, :boolean

    end
end