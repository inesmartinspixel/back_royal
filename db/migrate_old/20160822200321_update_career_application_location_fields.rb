class UpdateCareerApplicationLocationFields < ActiveRecord::Migration[4.2]
    def up

        drop_trigger(:cohort_applications)

        # remove old place fields
        remove_column :cohort_applications, :street_address
        remove_column :cohort_applications_versions, :street_address
        remove_column :cohort_applications, :city_state
        remove_column :cohort_applications_versions, :city_state
        remove_column :cohort_applications, :country_identifier
        remove_column :cohort_applications_versions, :country_identifier
        remove_column :cohort_applications, :time_zone_identiifier
        remove_column :cohort_applications_versions, :time_zone_identiifier

        # add new place fields
        add_column :cohort_applications, :place_id, :text
        add_column :cohort_applications_versions, :place_id, :text
        add_column :cohort_applications, :place_details, :json, :default => {}, :null => false
        add_column :cohort_applications_versions, :place_details, :json, :default => {}, :null => false

        create_trigger(:cohort_applications)

    end


    def down

        drop_trigger(:cohort_applications)


        # (Revert) remove old place fields
        add_column :cohort_applications, :street_address, :text
        add_column :cohort_applications_versions, :street_address, :text
        add_column :cohort_applications, :city_state, :text
        add_column :cohort_applications_versions, :city_state, :text
        add_column :cohort_applications, :country_identifier, :text
        add_column :cohort_applications_versions, :country_identifier, :text
        add_column :cohort_applications, :time_zone_identiifier, :text # intentionally misspelled (see cohort_application.rb)
        add_column :cohort_applications_versions, :time_zone_identiifier, :text # intentionally misspelled (see cohort_application.rb)

        # (Revert) add new place fields
        remove_column :cohort_applications, :place_id
        remove_column :cohort_applications_versions, :place_id
        remove_column :cohort_applications, :place_details
        remove_column :cohort_applications_versions, :place_details

        create_trigger(:cohort_applications)

    end


   # Helper Methods

    def drop_trigger(table_name)
        execute "DROP TRIGGER #{table_name}_versions ON #{table_name}"
    end

    def create_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("DROP TRIGGER IF EXISTS #{table_name}_versions ON #{table_name}")
        execute(trigger_sql)
    end

end
