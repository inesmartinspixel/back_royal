class AddLocalesToContentTopics < ActiveRecord::Migration[4.2]
    def up
        add_column :content_topics, :locales, :json, :default=>{}
        add_column :content_topics_versions, :locales, :json

        # backfill the new locales column
        ContentTopic.all.each do |content_topic|
            content_topic.locales = {en: content_topic.name}
            content_topic.save!
        end

    end

    def down
        remove_column :content_topics, :locales
        remove_column :content_topics_versions, :locales
    end
end
