class FixVersionIssueAndRefreshViews < ActiveRecord::Migration[5.0]

    def up
        # there are actually no changes here.  We need this migration
        # because there was an issue in prod where migrations got run out of
        # order and we're fixing that.
        #
        # Also refrseshing all views within  a migration for the first time
        ViewHelpers.migrate(self, 20170330133007)
    end

    def down
        ViewHelpers.rollback(self, 20170330133007)
    end
end