class AddTitleAndDescriptionToCohorts < ActiveRecord::Migration[5.0]
    def change
        add_column :cohorts, :title, :text
        add_column :cohorts_versions, :title, :text

        add_column :cohorts, :description, :text
        add_column :cohorts_versions, :description, :text

        add_column :curriculum_templates, :title, :text
        add_column :curriculum_templates_versions, :title, :text

        # curriculum_templates already has a description field
    end
end
