class AddGraduationStatusToCohortApplications < ActiveRecord::Migration[5.0]
    def change
        add_column :cohort_applications, :graduation_status, :text, :default => "pending"
        add_column :cohort_applications_versions, :graduation_status, :text
    end
end
