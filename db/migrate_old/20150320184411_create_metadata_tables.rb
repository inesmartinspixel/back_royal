class CreateMetadataTables < ActiveRecord::Migration[4.2]
  def change

    create_table :entity_metadata, id: :uuid do |t|
        t.timestamps

        t.string :title
        t.string :description
        t.text :canonical_url
        t.uuid :image_id

    end

    create_table :global_metadata, id: :uuid do |t|
        t.timestamps

        t.string :site_name
        t.string :default_title
        t.string :default_description
        t.uuid :default_image_id
        t.string :default_canonical_url
        t.string :twitter_account_name
        t.string :twitter_account_id
        t.string :facebook_account_name
    end
    add_index :global_metadata, :site_name, :unique => true

    # assuming a 1-1 relationship between lessons/streams/other trackable entities and seo_metadata
    add_column :lessons, :entity_metadata_id, :uuid
    add_foreign_key :lessons, :entity_metadata, column: :entity_metadata_id
    add_column :lessons_versions, :entity_metadata_id, :uuid
    add_column :lesson_streams, :entity_metadata_id, :uuid
    add_foreign_key :lesson_streams, :entity_metadata, column: :entity_metadata_id
    add_column :lesson_streams_versions, :entity_metadata_id, :uuid

    create_or_migrate_entity_metadata unless self.reverting?
    create_default_global_metadata unless self.reverting?

    # todo in a later migration
    # remove_column :lessons, :seo_title, :string
    # remove_column :lessons_versions, :seo_title, :string
    # remove_column :lessons, :seo_description, :string
    # remove_column :lessons_versions, :seo_description, :string
    # remove_column :lessons, :seo_canonical_url, :text
    # remove_column :lessons_versions, :seo_canonical_url, :text
    # remove_column :lessons, :seo_image_id, :uuid
    # remove_column :lessons_versions, :seo_image_id, :uuid

  end

  def create_or_migrate_entity_metadata
        batch = 0

        entity_metadata_map = {}

        VersionMigrator.new(Lesson::Stream).select('title', 'description').each do |stream|
            #puts " ************ Processing Stream: #{stream.title}"
            metadata = entity_metadata_map[stream.attributes['id']] ||= EntityMetadata.create!({
                title: stream.title,
                description: stream.description,
                canonical_url: "/course/#{stream.title.parameterize}/show/#{stream.id}"
            })
            stream.entity_metadata_id = metadata.id
        end

        VersionMigrator.new(Lesson).select('title', 'seo_description', 'seo_canonical_url', 'seo_image_id', 'seo_title').each do |lesson|
            #puts " ************ Processing Lesson: #{lesson.title}"

            metadata = entity_metadata_map[lesson.attributes['id']] ||= EntityMetadata.create!({
                title: lesson.seo_title || lesson.title || ' - ',
                description: lesson.seo_description,
                canonical_url: lesson.seo_canonical_url || "/lesson/#{lesson.title.parameterize}/show/#{lesson.id}",
                image_id: lesson.seo_image_id
            })
            lesson.entity_metadata_id = metadata.id
        end

  end

  def create_default_global_metadata
    GlobalMetadata.find_or_create_by({
        site_name: "Smartly",
        default_title: "Smartly - Get Smarter at Work in Fun, 5-Minute Lessons",
        default_description: "Master marketing, finance, business strategy, & more with Smartly's fun, 5-minute lessons and courses. Sign up completely free to start learning now!",
        default_canonical_url: "https://smart.ly",
        twitter_account_name: "@SmartlyHQ",
        twitter_account_id: "2585893555",
        facebook_account_name: "SmartlyHQ"
        # email_sharing_text:
    })

    GlobalMetadata.find_or_create_by({
        site_name: "BlueOcean",
        default_title: "Blue Ocean Strategy | Smartly - Free Online Learning",
        default_description: "Take the free course on Blue Ocean Strategy by Smartly, a revolutionary approach to online learning. Find uncontested market space &amp; beat the competition.",
        default_canonical_url: "https://smart.ly/blue-ocean-strategy",
        twitter_account_name: "@SmartlyHQ",
        twitter_account_id: "2585893555",
        facebook_account_name: "SmartlyHQ"
    })
  end

end
