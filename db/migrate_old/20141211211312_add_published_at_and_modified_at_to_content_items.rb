class AddPublishedAtAndModifiedAtToContentItems < ActiveRecord::Migration[4.2]

    def change
        add_column :content_publishers, :published_at, :datetime
        add_column :lessons, :modified_at, :datetime
        add_column :lesson_streams, :modified_at, :datetime
    end

end
