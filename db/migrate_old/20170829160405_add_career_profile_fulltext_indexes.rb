class AddCareerProfileFulltextIndexes < ActiveRecord::Migration[5.1]
    def up
        add_index :career_profile_fulltext, :keyword_vector, using: :gin
        execute "CREATE INDEX career_profile_fulltext_keyword_text ON career_profile_fulltext USING GIN (keyword_text gin_trgm_ops)"
    end

    def down
        remove_index :career_profile_fulltext, :keyword_vector
        execute "DROP INDEX career_profile_fulltext_keyword_text"
    end
end
