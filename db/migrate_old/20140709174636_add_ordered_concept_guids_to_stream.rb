class AddOrderedConceptGuidsToStream < ActiveRecord::Migration[4.2]
    def change
        add_column :lesson_streams, :ordered_concept_guids, :json
    end
end
