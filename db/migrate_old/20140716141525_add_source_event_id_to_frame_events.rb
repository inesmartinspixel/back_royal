class AddSourceEventIdToFrameEvents < ActiveRecord::Migration[4.2]

	def up
		add_column :report_frame_events, :source_event_id, :integer
		add_index :report_frame_events, :source_event_id, :unique => true

		# We've never successfully populated the event_json in the past.  We might as
		# well just use a source_event_id instead.  No reason to denormalize
		remove_column :report_frame_events, :event_json
	end

	def down
		remove_index(:report_frame_events, :source_event_id)
		remove_column :report_frame_events, :source_event_id

		add_column :report_frame_events, :event_json, :json
	end

end
