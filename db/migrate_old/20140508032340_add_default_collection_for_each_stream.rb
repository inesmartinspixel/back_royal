class AddDefaultCollectionForEachStream < ActiveRecord::Migration[4.2]
    def change
        puts '-- Adding default collection for each stream --'
        process_streams
    end

    def process_streams
        batch = 0
        Lesson::Stream.find_in_batches(batch_size: 10) do |streams|
            puts " ************ Processing streams batch ##{batch}"

            streams.each_with_index do |stream, i|
                puts " ************ Processing Stream ID: #{stream.id} : #{i+1} of #{streams.size}"
                if stream.collections.count == 0
                    ActiveRecord::Base.transaction do
                        collection = Collection.create(:collection_guid => SecureRandom.uuid, :name => "Default Collection")
                        collection.lesson_streams << stream
                        collection.save!
                    end
                end

            end
            batch += 1
        end
    end
end