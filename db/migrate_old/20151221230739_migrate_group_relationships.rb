class MigrateGroupRelationships < ActiveRecord::Migration[4.2]

    def up

        # these should've been made as not-null
        ["users", "institutions", "playlists", "lesson_streams"].each do |table_name|
            item_name = table_name.singularize
            join_table = "access_groups_#{table_name}"
            change_column_null join_table, "access_group_id", false
            change_column_null join_table, "#{item_name}_id", false
        end

        # cleanup bad data
        execute "DELETE FROM tags WHERE entity_type = 'Class'"
        execute "DELETE FROM tags WHERE entity_type != 'Group' AND entity_id IS NULL"

        # we were eagerly populating these, but we actually need to retain the old IDs from the tag entities where they already exist
        execute "DELETE FROM access_groups_institutions"
        execute "DELETE FROM access_groups_lesson_streams"
        execute "DELETE FROM access_groups_playlists"
        execute "DELETE FROM access_groups_users"
        execute "DELETE FROM access_groups"

        # backfill the new access_groups table
        execute "INSERT INTO access_groups
                    (id, name, created_at, updated_at) (
                        SELECT
                            id, text, created_at, updated_at
                        FROM
                            tags
                        WHERE
                            entity_type='Group'
                        ORDER BY created_at, updated_at ASC
                    )"


        # institutions are a special case in that it already has a mapping relationship directly
        # with prior tag.id but lack a primary key on the looking rows
        execute "DELETE FROM institutions_groups WHERE tag_id IN ( SELECT id FROM tags WHERE entity_type != 'Group' )"
        execute "INSERT INTO access_groups_institutions
                    (access_group_id, institution_id) (
                        SELECT
                            tag_id, institution_id
                        FROM
                            institutions_groups
                        WHERE NOT EXISTS
                            ( SELECT 1 FROM access_groups_institutions agi
                                WHERE agi.access_group_id = institutions_groups.tag_id
                                AND  agi.institution_id = institutions_groups.institution_id)
                    )"

        # all legacy relationships
        %w(Lesson::Stream Playlist User).each do |entity_type|
            migrate_relational_tag_data(entity_type)
        end

    end


    def down

        %w(User Playlist Lesson::Stream).each do |entity_type|
            rollback_relational_tag_data(entity_type)
        end

        execute "DELETE FROM access_groups_institutions"
        execute "DELETE FROM access_groups"

    end


    def migrate_relational_tag_data(entity_type)

        item_type = entity_type == 'Lesson::Stream' ? 'lesson_stream' : entity_type.downcase
        table_name = item_type.pluralize
        join_table_name = "access_groups_#{table_name}"

        # we have to ignore tags that refer to entities or groups that no longer
        # exist, since trying to insert them will break foreign key constraints.
        execute "DELETE FROM tags WHERE entity_type='#{entity_type}' AND ( entity_id NOT IN (SELECT id FROM #{table_name}) OR text NOT IN (SELECT name FROM access_groups) )"
        execute "DELETE FROM tags WHERE entity_type='#{entity_type}' AND ( entity_id NOT IN (SELECT id FROM #{table_name}) OR text NOT IN (SELECT name FROM access_groups) )"


        execute "INSERT INTO #{join_table_name}
                    (id, #{item_type}_id, access_group_id) (
                        SELECT
                            tags.id, tags.entity_id, (SELECT id FROM access_groups WHERE access_groups.name = tags.text)
                        FROM
                            tags
                        WHERE tags.entity_type='#{entity_type}'
                        AND NOT EXISTS (
                            SELECT 1 FROM #{join_table_name}
                                WHERE #{join_table_name}.id = id
                        )
                    )"

    end


    def rollback_relational_tag_data(entity_type)

        item_type = entity_type == 'Lesson::Stream' ? 'lesson_stream' : entity_type.downcase
        table_name = item_type.pluralize

        execute "DELETE FROM access_groups_#{table_name} WHERE id IN
            ( SELECT id FROM tags WHERE entity_type='#{entity_type}' )"

    end

end
