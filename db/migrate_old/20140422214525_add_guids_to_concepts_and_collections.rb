class AddGuidsToConceptsAndCollections < ActiveRecord::Migration[4.2]
    def up
        add_column :concepts, :concept_guid, :string
        add_index(:concepts, :concept_guid)

        add_column :concept_progress, :concept_guid, :string
        add_index(:concept_progress, :concept_guid)

        add_column :concepts, :collection_guid, :string
        add_index(:concepts, :collection_guid)

        add_column :collections, :collection_guid, :string
        add_index(:collections, :collection_guid)

        add_column :collection_progress, :collection_guid, :string
        add_index(:collection_progress, :collection_guid)
    end

    def down
        remove_index(:concepts, :concept_guid)
        remove_column :concepts, :concept_guid

        remove_index(:concept_progress, :concept_guid)
        remove_column :concept_progress, :concept_guid

        remove_index(:concepts, :collection_guid)
        remove_column :concepts, :collection_guid

        remove_index(:collections, :collection_guid)
        remove_column :collections, :collection_guid

        remove_index(:collection_progress, :collection_guid)
        remove_column :collection_progress, :collection_guid
    end
end
