class RemoveBrokenStreamJoins < ActiveRecord::Migration[4.2]
    def up
        Lesson::ToStreamJoin.
            joins('LEFT OUTER JOIN lessons ON lessons.id = lesson_id').
            joins('LEFT OUTER JOIN lesson_streams ON lesson_streams.id = stream_id').
            where('lessons.id IS NULL OR lesson_streams.id IS NULL').destroy_all
    end
end
