class AddShareableWithClassmates < ActiveRecord::Migration[5.1]
  def change

        add_column :cohort_applications, :shareable_with_classmates, :boolean
        add_column :cohort_applications_versions, :shareable_with_classmates, :boolean

    end
end
