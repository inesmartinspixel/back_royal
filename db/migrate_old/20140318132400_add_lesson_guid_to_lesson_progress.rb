class AddLessonGuidToLessonProgress < ActiveRecord::Migration[4.2]
    def up
        remove_index(:lesson_progress, :lesson_id)
        remove_column :lesson_progress, :lesson_id

        add_column :lesson_progress, :lesson_guid, :string
        add_index(:lesson_progress, :lesson_guid)
    end

    def down
        remove_index(:lesson_progress, :lesson_guid)
        remove_column :lesson_progress, :lesson_guid

        add_column :lesson_progress, :lesson_id, :integer
        add_index(:lesson_progress, :lesson_id)

    end
end
