class AddArchivedFlagToLessons < ActiveRecord::Migration[4.2]
    def change
        add_column :lessons, :archived, :boolean, :default => false
    end
end
