class UpdateRoleDescriptorsForSpecificHiringApplications < ActiveRecord::Migration[5.0]

    # ids for hiring applications with 'role_descriptors' that include the 'finance' key
    # that should be modified to have the new 'operations' key instead of 'finance'.
    # NOTE: I manually viewed each of their applications and made a judgement called based
    # on the context of their profile content whether the 'finance' key should be switched
    # to 'operations'.
    HIRING_APPLICATION_IDS = [
        "02b07469-455a-4927-9840-69a06d9ee766", # Kate Marsh
        "97699bac-aa6c-45e1-a8cd-5da880bb53d9", # Paige Curtis
        "5c636d78-bbf7-4c53-9de3-afd0f28d4cdb", # Claire McCarthy
        "aa766a76-e147-4312-8245-26cd87605a2f", # Kristine Alipio
        "c0c86a23-3ea4-4079-964e-dbaa41da2797"  # Maung Lin
    ]

    def up
        hiring_applications = HiringApplication.where("'finance' = ANY(role_descriptors)").where(id: HIRING_APPLICATION_IDS)
        hiring_applications.each do |hiring_application|
            index = hiring_application.role_descriptors.index("finance")
            hiring_application.role_descriptors[index] = "operations"
            hiring_application.save!
        end
    end

    def down
        hiring_applications = HiringApplication.where(id: HIRING_APPLICATION_IDS)
        hiring_applications.each do |hiring_application|
            index = hiring_application.role_descriptors.index("operations")
            hiring_application.role_descriptors[index] = "finance"
            hiring_application.save!
        end
    end
end
