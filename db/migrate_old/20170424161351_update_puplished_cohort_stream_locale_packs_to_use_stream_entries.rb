class UpdatePuplishedCohortStreamLocalePacksToUseStreamEntries < ActiveRecord::Migration[5.0]
    def up
        ViewHelpers.migrate(self, 20170424161351)
    end

    def down
        ViewHelpers.rollback(self, 20170424161351)
    end
end
