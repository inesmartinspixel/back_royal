class CreateCollections < ActiveRecord::Migration[4.2]
    def change
        create_table :collections do |t|
            t.references :user, index: true
            # todo: t.references :course
            t.string :name

            t.timestamps
        end
    end
end
