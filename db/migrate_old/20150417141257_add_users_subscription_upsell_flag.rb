class AddUsersSubscriptionUpsellFlag < ActiveRecord::Migration[4.2]
    def up

        # assume any new users will be subscription users by default
        add_column :users, :subscriptions_enabled, :boolean, default: true, null: false
        add_column :users_versions, :subscriptions_enabled, :boolean

        # any existing users in the system should be marked as non-subscription
        User.update_all(:subscriptions_enabled => false)

    end

    def down
        remove_column :users, :subscriptions_enabled
        remove_column :users_versions, :subscriptions_enabled
    end

end