class AddDurationToFrameEvent < ActiveRecord::Migration[4.2]
    def change
        add_column :report_frame_events, :duration_active, :float
        add_column :report_frame_events, :duration_idle, :float
        add_column :report_frame_events, :duration_total, :float
    end
end
