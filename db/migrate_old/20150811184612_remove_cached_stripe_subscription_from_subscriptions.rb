class RemoveCachedStripeSubscriptionFromSubscriptions < ActiveRecord::Migration[4.2]
    def change
        remove_column :subscriptions, :cached_stripe_subscription, :json
        remove_column :subscriptions_versions, :cached_stripe_subscription, :json
    end
end
