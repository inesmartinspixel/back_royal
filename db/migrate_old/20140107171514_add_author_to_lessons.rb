class AddAuthorToLessons < ActiveRecord::Migration[4.2]
    def change
        add_column :lessons, :author_id, :integer
    end
end