class BackfillComingSoon < ActiveRecord::Migration[4.2]
    def up
        set_to_false = []
        left_alone = []
        VersionMigrator.new(Lesson::Stream).each do |stream|
            if stream.respond_to?(:coming_soon)
                label = stream.respond_to?(:version_created_at) ? "#{stream.title} version #{stream.version_created_at}" : stream.title
                label = "#{label} old coming soon: #{stream.coming_soon}"

                if stream.coming_soon.nil?
                    set_to_false << label
                    stream.coming_soon = false
                    label = "#{label} new coming soon: #{stream.coming_soon}"
                else
                    left_alone << label
                end
            end
        end

        puts "The following streams have had coming soon set to false:"
        puts set_to_false

        puts "The following streams were left alone:"
        puts left_alone
    end
end
