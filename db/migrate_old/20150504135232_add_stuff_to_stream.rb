class AddStuffToStream < ActiveRecord::Migration[4.2]
    def change

        # Credits
        add_column :lesson_streams, :credits, :text
        add_column :lesson_streams_versions, :credits, :text

        # What You'll Learn
        add_column :lesson_streams, :what_you_will_learn, :string, array: true, default: []
        add_column :lesson_streams_versions, :what_you_will_learn, :string, array: true

        # Recommended Streams
        add_column :lesson_streams, :recommended_stream_ids, :string, array: true, default: []
        add_column :lesson_streams_versions, :recommended_stream_ids, :string, array: true

        # Related Streams
        add_column :lesson_streams, :related_stream_ids, :string, array: true, default: []
        add_column :lesson_streams_versions, :related_stream_ids, :string, array: true

        # Additional Resources - Downloads
        add_column :lesson_streams, :resource_downloads, :json
        add_column :lesson_streams_versions, :resource_downloads, :json

        # Additional Resources - External Links
        add_column :lesson_streams, :resource_links, :json
        add_column :lesson_streams_versions, :resource_links, :json
    end
end
