class UpdateMoreAboutYouFields < ActiveRecord::Migration[4.2]
  def up

        drop_trigger(:career_profiles)

        # rename old skills column
        rename_column :career_profiles, :skills_and_interests, :skills
        rename_column :career_profiles_versions, :skills_and_interests, :skills

        # new awards and interests section
        add_column :career_profiles, :awards_and_interests, :text, :default => [], :array => true, :null => false
        add_column :career_profiles_versions, :awards_and_interests, :text, :array => true

        create_trigger(:career_profiles)

    end


    def down

        drop_trigger(:career_profiles)

        # (Revert) rename old skills column
        rename_column :career_profiles, :skills, :skills_and_interests
        rename_column :career_profiles_versions, :skills, :skills_and_interests

        # (Revert) new awards and interests section
        remove_column :career_profiles, :awards_and_interests
        remove_column :career_profiles_versions, :awards_and_interests

        create_trigger(:career_profiles)

    end


    # Helper Methods

    def drop_trigger(table_name)
        execute "DROP TRIGGER #{table_name}_versions ON #{table_name}"
    end

    def create_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("DROP TRIGGER IF EXISTS #{table_name}_versions ON #{table_name}")
        execute(trigger_sql)
    end
end
