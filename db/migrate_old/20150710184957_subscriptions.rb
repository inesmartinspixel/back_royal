class Subscriptions < ActiveRecord::Migration[4.2]
    def up
        temporarily_delete_user_views do
            drop_table :subscriptions_users_versions, force: :cascade
            drop_table :subscriptions_versions, force: :cascade
            drop_table :subscriptions, force: :cascade
            drop_table :subscriptions_users, force: :cascade

            add_column :users, :subscription_type, :string
            add_column :users, :stripe_customer_id, :string

            add_column :users_versions, :subscription_type, :string
            add_column :users_versions, :stripe_customer_id, :string


            create_trigger('users', [
                'email',
                'encrypted_password',
                'reset_password_token',
                'reset_password_sent_at',
                'first_name',
                'last_name',
                'sign_up_code',
                'reset_password_redirect_url',
                'provider',
                'uid',
                'subscriptions_enabled',
                'notify_email_daily',
                'notify_email_content',
                'notify_email_features',
                'notify_email_reminders',
                'notify_email_newsletter',
                'subscription_type',
                'stripe_customer_id'
            ])

        end

    end

    def down
        temporarily_delete_user_views do
            remove_column :users, :subscription_type
            remove_column :users, :stripe_customer_id

            remove_column :users_versions, :subscription_type
            remove_column :users_versions, :stripe_customer_id


            create_trigger('users', [
                'email',
                'encrypted_password',
                'reset_password_token',
                'reset_password_sent_at',
                'first_name',
                'last_name',
                'sign_up_code',
                'reset_password_redirect_url',
                'provider',
                'uid',
                'subscriptions_enabled',
                'notify_email_daily',
                'notify_email_content',
                'notify_email_features',
                'notify_email_reminders',
                'notify_email_newsletter'
            ])


            create_table "subscriptions", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
                t.datetime "created_at"
                t.datetime "updated_at"
                t.string  "subscription_type",    limit: 255
                t.datetime "pro_trial_start_date"
                t.datetime "pro_trial_end_date"
            end

            add_index "subscriptions", ["created_at"], name: "index_subscriptions_on_created_at", using: :btree

            create_table "subscriptions_users", id: false, force: :cascade do |t|
                t.uuid "subscription_id"
                t.uuid "user_id"
            end

            add_index "subscriptions_users", ["subscription_id", "user_id"], name: "index_subscriptions_users_on_subscription_id_and_user_id", unique: true, using: :btree

            create_table "subscriptions_users_versions", id: false, force: :cascade do |t|
                t.uuid    "version_id",                  default: "uuid_generate_v4()"
                t.string  "operation",          limit: 1,                                null: false
                t.datetime "version_created_at",                                          null: false
                t.uuid    "subscription_id"
                t.uuid    "user_id"
            end

            create_table "subscriptions_versions", id: false, force: :cascade do |t|
                t.uuid    "version_id",                      default: "uuid_generate_v4()"
                t.string  "operation",            limit: 1,                                  null: false
                t.datetime "version_created_at",                                              null: false
                t.uuid    "id",                                                              null: false
                t.datetime "created_at"
                t.datetime "updated_at"
                t.string  "subscription_type",    limit: 255
                t.string  "recurly_uuid"
                t.string  "recurly_plan_code"
                t.datetime "pro_trial_start_date"
                t.datetime "pro_trial_end_date"
            end

            add_index "subscriptions_versions", ["id", "updated_at"], name: "index_subscriptions_versions_on_id_and_updated_at", using: :btree
        end

    end


    def create_trigger(table_name, columns_to_watch)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.connection.schema_cache.clear!
        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"

        if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
        update_command = "
            IF #{if_string} THEN
            #{insert_on_update}
            END IF;
            "

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end

    def temporarily_delete_user_views(&block)
        # this used to do something, but no more
        yield
    end

end
