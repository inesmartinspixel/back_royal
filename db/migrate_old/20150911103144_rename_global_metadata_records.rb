class RenameGlobalMetadataRecords < ActiveRecord::Migration[4.2]
    def up

        remove_column :global_metadata, :default_tweet_template, :text
        remove_column :global_metadata, :facebook_account_name, :string
        remove_column :global_metadata, :twitter_account_id, :string
        remove_column :global_metadata, :twitter_account_name, :string


        if blue_ocean = GlobalMetadata.find_by_site_name("BlueOcean")
            blue_ocean.update_attribute(:site_name, "blue_ocean_strategy#index")
        end

        existing_defaults = GlobalMetadata.find_by_site_name('__defaults')

        if home = GlobalMetadata.find_by_site_name("Smartly")
            if existing_defaults
                home.destroy
            else
                home.update_attribute(:site_name, "__defaults")
            end
        end

    end

    def down

        add_column :global_metadata, :default_tweet_template, :text
        add_column :global_metadata, :facebook_account_name, :string
        add_column :global_metadata, :twitter_account_id, :string
        add_column :global_metadata, :twitter_account_name, :string

        if blue_ocean = GlobalMetadata.find_by_site_name("blue_ocean_strategy#index")
            blue_ocean.update_attribute(:site_name, "BlueOcean")
        end

        if home = GlobalMetadata.find_by_site_name("__defaults")
            home.update_attribute(:site_name, "Smartly")
        end

    end
end
