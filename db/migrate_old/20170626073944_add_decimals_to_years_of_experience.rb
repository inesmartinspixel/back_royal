class AddDecimalsToYearsOfExperience < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20170626073944)
    end

    def down
        ViewHelpers.rollback(self, 20170626073944)
    end
end
