class YearsOfExperience < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20170608162605)
    end

    def down
        ViewHelpers.rollback(self, 20170608162605)
    end
end
