class DeleteConceptImages < ActiveRecord::Migration[4.2]

    def up
        remove_column :concepts, :image_id
    end

    def down
        add_column :concepts, :image_id, :uuid
    end

end
