class NoCohortUserPeriodsInFuture < ActiveRecord::Migration[5.0]
    def up
        ViewHelpers.migrate(self, 20170421201751)
    end

    def down
        ViewHelpers.rollback(self, 20170421201751)
    end
end
