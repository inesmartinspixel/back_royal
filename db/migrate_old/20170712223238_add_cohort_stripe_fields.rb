class AddCohortStripeFields < ActiveRecord::Migration[5.1]

    def up

        # Cohorts + Templates - stripe_plan_id
        add_column :cohorts, :stripe_plan_id, :text
        add_column :cohorts_versions, :stripe_plan_id, :text
        add_column :curriculum_templates, :stripe_plan_id, :text
        add_column :curriculum_templates_versions, :stripe_plan_id, :text

        # Cohorts + Templates - program_guide
        add_column :cohorts, :program_guide, :text
        add_column :cohorts_versions, :program_guide, :text
        add_column :curriculum_templates, :program_guide, :text
        add_column :curriculum_templates_versions, :program_guide, :text

        # Applications - total_num_required_stripe_payments
        add_column :cohort_applications, :total_num_required_stripe_payments, :integer
        add_column :cohort_applications_versions, :total_num_required_stripe_payments, :integer

        # Applications - stripe_coupon_id
        add_column :cohort_applications, :stripe_coupon_id, :text
        add_column :cohort_applications_versions, :stripe_coupon_id, :text

        # Subscriptions - remove old paradigm expires_at
        remove_column :subscriptions, :expires_at, :datetime, null: true
        remove_column :subscriptions_versions, :expires_at

        # Subscriptions - standardize around stripe_ prefix
        rename_column :subscriptions, :plan_id, :stripe_plan_id
        rename_column :subscriptions_versions, :plan_id, :stripe_plan_id

        # Users - subscriptions_enabled (becomes derived)
        temporarily_delete_user_views do

            remove_column :users, :subscriptions_enabled, :boolean, :default => false
            remove_column :users_versions, :subscriptions_enabled, :boolean

            # up
            create_trigger('users', [
                'email',
                'encrypted_password',
                'reset_password_token',
                'reset_password_sent_at',
                'name',
                'nickname',
                'sign_up_code',
                'reset_password_redirect_url',
                'provider',
                'uid',
                'notify_email_daily',
                'notify_email_content',
                'notify_email_features',
                'notify_email_reminders',
                'notify_email_newsletter',
                'free_trial_started',
                'confirmed_profile_info',
                'optimizely_referer',
                'active_playlist_locale_pack_id',
                'has_seen_welcome',
                'pref_decimal_delim',
                'pref_locale',
                'school',
                'avatar_url',
                'avatar_provider',
                'mba_content_lockable',
                'job_title',
                'phone',
                'phone_extension',
                'country',
                'has_seen_accepted',
                'can_edit_career_profile',
                'professional_organization_option_id',
                'pref_show_photos_names',
                'identity_verified',
                'sex',
                'ethnicity',
                'race',
                'how_did_you_hear_about_us',
                'address_line_1',
                'address_line_2',
                'city',
                'state',
                'zip',
                'birthdate',
                'anything_else_to_tell_us',
                'pref_keyboard_shortcuts',
                'mba_enabled',
                'fallback_program_type',
                'program_type_confirmed',
                'invite_code'
                # the json columns must be left out of here, since they can't be compared
            ])
        end
    end


    def down

        # Cohorts + Templates - stripe_plan_id
        remove_column :cohorts, :stripe_plan_id, :text
        remove_column :cohorts_versions, :stripe_plan_id, :text
        remove_column :curriculum_templates, :stripe_plan_id, :text
        remove_column :curriculum_templates_versions, :stripe_plan_id, :text

        # Cohorts + Templates - program_guide
        remove_column :cohorts, :program_guide, :text
        remove_column :cohorts_versions, :program_guide, :text
        remove_column :curriculum_templates, :program_guide, :text
        remove_column :curriculum_templates_versions, :program_guide, :text

        # Applications - total_num_required_stripe_payments
        remove_column :cohort_applications, :total_num_required_stripe_payments, :integer
        remove_column :cohort_applications_versions, :total_num_required_stripe_payments, :integer

        # Applications - stripe_coupon_id
        remove_column :cohort_applications, :stripe_coupon_id, :text
        remove_column :cohort_applications_versions, :stripe_coupon_id, :text

        # Subscriptions - remove old paradigm expires_at
        add_column :subscriptions, :expires_at, :datetime, null: true
        add_column :subscriptions_versions, :expires_at, :datetime

        # Subscriptions - standardize around stripe_ prefix
        rename_column :subscriptions, :stripe_plan_id, :plan_id
        rename_column :subscriptions_versions, :stripe_plan_id, :plan_id

        temporarily_delete_user_views do

            # Users - subscriptions_enabled (not derived)
            add_column :users, :subscriptions_enabled, :boolean, :default => false
            add_column :users_versions, :subscriptions_enabled, :boolean

            create_trigger('users', [
                'email',
                'encrypted_password',
                'reset_password_token',
                'reset_password_sent_at',
                'name',
                'nickname',
                'sign_up_code',
                'reset_password_redirect_url',
                'provider',
                'uid',
                'subscriptions_enabled',
                'notify_email_daily',
                'notify_email_content',
                'notify_email_features',
                'notify_email_reminders',
                'notify_email_newsletter',
                'free_trial_started',
                'confirmed_profile_info',
                'optimizely_referer',
                'active_playlist_locale_pack_id',
                'has_seen_welcome',
                'pref_decimal_delim',
                'pref_locale',
                'school',
                'avatar_url',
                'avatar_provider',
                'mba_content_lockable',
                'job_title',
                'phone',
                'phone_extension',
                'country',
                'has_seen_accepted',
                'can_edit_career_profile',
                'professional_organization_option_id',
                'pref_show_photos_names',
                'identity_verified',
                'sex',
                'ethnicity',
                'race',
                'how_did_you_hear_about_us',
                'address_line_1',
                'address_line_2',
                'city',
                'state',
                'zip',
                'birthdate',
                'anything_else_to_tell_us',
                'pref_keyboard_shortcuts',
                'mba_enabled',
                'fallback_program_type',
                'program_type_confirmed',
                'invite_code'
                # the json columns must be left out of here, since they can't be compared
            ])

        end
    end


    def create_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end


    def temporarily_delete_user_views(&block)
        # ViewHelpers.set_versions(self, 0) # drop all the views

        # this used to do something, but no more
        yield

        # ViewHelpers.set_versions(self, 20170712223238 - 1) # recreate them
    end

end
