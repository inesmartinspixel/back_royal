class CreateUserCanSeeViewsTwo < ActiveRecord::Migration[5.0]
    def up

        # changes to
        ViewHelpers.set_versions(self, 20170310204902)

    end

    def down

        ViewHelpers.set_versions(self, 20170310204902 - 1)

    end
end
