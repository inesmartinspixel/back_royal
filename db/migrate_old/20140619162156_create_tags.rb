class CreateTags < ActiveRecord::Migration[4.2]
    def change
        create_table :tags do |t|
            t.string :entity_type
            t.string :entity_id # could be guid or id, so stick with string column type
            t.string :text

            t.timestamps
        end
        # compound index allowing quick lookup by id within entity_type, used for existance checks
        add_index(:tags, [:entity_type, :entity_id])

        # compound index allowing quick lookup by tag text within entity_type, to be used for search within lesson/course/whatever
        add_index(:tags, [:entity_type, :text])

        # quick lookup by tag value for any entity, to be used for search across lesson/course/whatever
        add_index(:tags, :text)
    end
end
