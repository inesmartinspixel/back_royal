require 'webdack/uuid_migration/helpers'

class CustomizeMailboxer < ActiveRecord::Migration[4.2]
    def change

        columns_to_uuid :mailboxer_receipts, :receiver_id
        columns_to_uuid :mailboxer_notifications, :sender_id
        columns_to_uuid :mailboxer_notifications, :notified_object_id

    end
end
