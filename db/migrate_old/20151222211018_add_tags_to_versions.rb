class AddTagsToVersions < ActiveRecord::Migration[4.2]

    def up
        execute("alter table lessons disable trigger lessons_versions")

        execute "UPDATE lessons_versions SET tag = ( SELECT text FROM tags WHERE entity_type='Lesson' AND entity_id = lessons_versions.id )"
        execute "UPDATE lessons SET tag = ( SELECT text FROM tags WHERE entity_type='Lesson' AND entity_id = lessons.id )"

        execute("alter table lessons enable trigger lessons_versions")
    end

    def down
        execute("alter table lessons disable trigger lessons_versions")

        execute "UPDATE lessons_versions SET tag = nil"
        execute "UPDATE lessons SET tag = nil"

        execute("alter table lessons enable trigger lessons_versions")
    end
end