class BackfillProgramTypeConfirmed < ActiveRecord::Migration[5.0]
    def up
        # We decided to just set all users up to this point to confirmed
        # User.update_all(program_type_confirmed: true) # Why does this take so long?
        User.in_batches.update_all(program_type_confirmed: true)
    end
end
