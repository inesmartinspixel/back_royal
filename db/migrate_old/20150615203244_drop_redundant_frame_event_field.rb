class DropRedundantFrameEventField < ActiveRecord::Migration[4.2]
    def change
        remove_column :report_frame_events, :lesson_frame_id
    end
end
