require File.expand_path('../componentized_migrators/no_interaction_migrator', __FILE__)
require File.expand_path('../componentized_migrators/this_or_that_migrator', __FILE__)

class ComponentizedThisOrThat < ActiveRecord::Migration[4.2]
	def up
		migrator = NoInteractionMigrator.new
		migrator.change

		migrator = ThisOrThatMigrator.new
		migrator.change
  	end
end
