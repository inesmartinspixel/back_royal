class FixCohortUserWeeks < ActiveRecord::Migration[5.0]
    def up

        # changes to CohortUserWeeks
        ViewHelpers.set_versions(self, 20170303185136)

    end

    def down

        ViewHelpers.set_versions(self, 20170303185136 - 1)

    end
end
