class AddUserCustomCertImageToStreamProgress < ActiveRecord::Migration[4.2]
    def change
        add_column :lesson_streams_progress, :certificate_image_id, :integer
    end
end
