class AddDefaultNotNullToLocationsOfInterest < ActiveRecord::Migration[5.0]
    def up
        # change_column will not query to replace the null values when you change null to false, even if you have a default set
        execute("UPDATE career_profiles SET locations_of_interest = '{flexible}' WHERE locations_of_interest IS NULL")
        change_column :career_profiles, :locations_of_interest, :text, :array => true, :default => ["flexible"], :null => false
    end

    def down
        change_column :career_profiles, :locations_of_interest, :text, :array => true
    end
end
