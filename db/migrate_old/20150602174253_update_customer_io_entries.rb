class UpdateCustomerIoEntries < ActiveRecord::Migration[4.2]

    def up

        return unless ENV['CUSTOMER_IO_SITE_ID'] && ENV['CUSTOMER_IO_API_KEY']

        customerio = Customerio::Client.new(ENV['CUSTOMER_IO_SITE_ID'], ENV['CUSTOMER_IO_API_KEY'])

        batch = 0
        User.find_in_batches(batch_size: 10) do |users|
            puts " ************ Processing user batch ##{batch}"

            users.each_with_index do |user, i|

                customerio.identify(
                    :id => user.id,
                    :email => user.email,
                    :emailDomain => user.email.split('@')[1],
                    :firstName => user.first_name,
                    :lastName => user.last_name,
                    :groups => user.groups.map(&:text).sort.join(','),
                    :institutions => user.institutions.map(&:name).sort.join(','),
                    :globalRole =>  user.global_role.name,
                    :subscriptions => user.subscriptions.map(&:subscription_type).sort.join(','),
                    :notify_email_daily => user.notify_email_daily,
                    :notify_email_content => user.notify_email_content,
                    :notify_email_features => user.notify_email_features,
                    :notify_email_reminders => user.notify_email_reminders,
                    :notify_email_newsletter => user.notify_email_newsletter,
                    :created_at => user.created_at
                )

            end

            batch += 1
        end
    end

    def down
        # hard to unmigrate this!  =]
    end

end
