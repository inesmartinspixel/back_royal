class AddMatchedAtToHiringRelationships < ActiveRecord::Migration[5.1]
    def change

        add_column :hiring_relationships, :matched_at, :timestamp
        add_column :hiring_relationships_versions, :matched_at, :timestamp

    end
end
