class CreateCareerProfiles < ActiveRecord::Migration[4.2]
    def up
        create_table    :career_profiles, id: :uuid do |t|
            t.timestamps

            # basic info
            t.uuid      :user_id
            t.text      :street_address
            t.text      :city_state
            t.text      :country_identifier
            t.text      :phone_number
            t.text      :time_zone_identiifier,     :default => 'America/New_York', :null => false
            t.boolean   :career_profile_active,     :default => true, :null => false

            # test scores
            t.text      :score_on_gmat
            t.text      :score_on_gre
            t.text      :score_on_sat
            t.text      :score_on_act

            # short answers
            t.json      :short_answers,             :default => {}, :null => false # keys are translation keys

            # job preferences
            t.boolean   :willing_to_relocate,       :default => true, :null => false
            t.boolean   :open_to_remote_work,       :default => true, :null => false
            t.text      :authorized_to_work_in_us   # the form sets this to true by default, but seems wrong to set it before someone has confirmed that
            t.text      :primary_area_of_interest
            t.json      :job_sectors_of_interest,   :default => {}, :null => false # keys are translation keys
            t.text      :job_search_stage
            t.json      :employment_types_of_interest, :default => {}, :null => false
            t.text      :ideal_job_start_time
            t.json      :company_sizes_of_interest, :default => {}, :null => false
            t.text      :desired_base_salary

            # about your mba
            t.text      :top_mba_subjects,  :default => [], :array => true, :null => false
            t.text      :favorite_mba_subject

            # more about you
            t.text      :top_personal_descriptors,  :default => [], :array => true, :null => false
            t.text      :top_motivations,           :default => [], :array => true, :null => false
            t.text      :top_workplace_strengths,   :default => [], :array => true, :null => false
            t.text      :skills_and_interests,      :default => [], :array => true, :null => false

            # personal bio
            t.text      :bio
            t.text      :career_goals
            t.text      :personal_fact

            # employer preferences
            t.text      :preferred_company_culture_descriptors, :default => [], :array => true, :null => false
            t.text      :preferred_manager_descriptors,         :default => [], :array => true, :null => false
            t.text      :preferred_compensation_descriptors,    :default => [], :array => true, :null => false

            # resume and links
            t.text      :linked_in_url
            t.text      :resume_id
            t.text      :personal_website_url
            t.text      :blog_url
            t.text      :twitter_profile_url
            t.text      :facebook_profile_url

        end
        add_index :career_profiles, :user_id, :unique => true
        add_foreign_key :career_profiles, :users
        create_versions_table_and_trigger(:career_profiles)

        create_table    :work_experiences, id: :uuid do |t|
            t.timestamps

            t.uuid      :user_id,           :null => false
            t.text      :job_title,         :null => false
            t.date      :start_date,        :null => false
            t.date      :end_date,          :null => true # null means current
            t.text      :responsibilities,  :null => false, :array => true, :default => []
        end
        add_foreign_key :work_experiences, :users
        create_versions_table_and_trigger(:work_experiences)

        create_table    :education_experiences, id: :uuid do |t|
            t.timestamps

            t.uuid      :user_id,           :null => false
            t.text      :institution_name,  :null => false
            t.integer   :graduation_year,   :null => false
            t.text      :degree,            :null => false
            t.text      :major,             :null => false
            t.text      :minor,             :null => true
            t.text      :gpa,               :null => true
        end
        add_foreign_key :education_experiences, :users
        create_versions_table_and_trigger(:education_experiences)
    end

    def down
        drop_versions_table_and_trigger(:education_experiences)
        drop_table :education_experiences

        drop_versions_table_and_trigger(:work_experiences)
        drop_table :work_experiences

        drop_versions_table_and_trigger(:career_profiles)
        drop_table :career_profiles
    end

    def create_versions_table_and_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.connection.schema_cache.clear!
        klass.reset_column_information

        # create the version table, including all columns from the source table
        # plus a few extras
        create_table "#{table_name}_versions", id: false, force: :cascade do |t|
            t.uuid     "version_id", null: false, default: "uuid_generate_v4()"
            t.string   "operation", limit: 1, null: false
            t.datetime "version_created_at", null: false

            puts "*** #{table_name}_versions"

            klass.column_names.each do |column_name|
                type = klass.columns_hash

                column_config = klass.columns_hash[column_name]
                meth = column_config.type.to_sym
                options = {}
                options[:limit] = column_config.limit unless column_config.limit.nil?
                options[:null] = false if column_config.null == false
                options[:array] = column_config.array
                # ignore defaults, since they should not be used in the audit table

                puts "t.#{meth}, #{column_name}, #{options.inspect}"
                t.send(meth, column_name, options)
            end
        end

        # for some reason the primary flag on create_table was not working
        execute "ALTER TABLE #{table_name}_versions ADD PRIMARY KEY (version_id);"

        # create indexes
        if klass.column_names.include?('id') && klass.column_names.include?('updated_at')
            add_index "#{table_name}_versions", [:id, :updated_at], :name => "idx_#{table_name}_versions_id_updt"
        elsif klass.column_names.include?('id')
            add_index "#{table_name}_versions", :id
        end

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "
        execute(trigger_sql)
    end

    def drop_versions_table_and_trigger(table_name)
        drop_table "#{table_name}_versions"
        execute "DROP TRIGGER #{table_name}_versions ON #{table_name}"
    end

    def create_trigger(table_name, columns_to_watch = nil)

        klass = Class.new(ActiveRecord::Base) do
            self.table_name = table_name
        end

        klass.reset_column_information

        insert_on_update = "INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'U', NOW(), NEW.*;"
        if columns_to_watch.nil?
            update_command = insert_on_update
        else
            if_string = columns_to_watch.map { |col| "NEW.#{col} <> OLD.#{col}" }.join(' OR ')
            update_command = "
                IF #{if_string} THEN
                #{insert_on_update}
                END IF;
                "
        end

        # create the trigger
        trigger_sql = "
            CREATE OR REPLACE FUNCTION process_#{table_name}_version() RETURNS TRIGGER AS $#{table_name}_version$
                BEGIN
                    --
                    -- Create a row in audit to reflect the operation performed on the table,
                    -- make use of the special variable TG_OP to work out the operation.
                    --
                    IF (TG_OP = 'DELETE') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'D', NOW(), OLD.*;
                        RETURN OLD;
                    ELSIF (TG_OP = 'UPDATE') THEN
                        #{update_command}
                        RETURN NEW;
                    ELSIF (TG_OP = 'INSERT') THEN
                        INSERT INTO #{table_name}_versions SELECT uuid_generate_v4(), 'I', NOW(), NEW.*;
                        RETURN NEW;
                    END IF;
                    RETURN NULL; -- result is ignored since this is an AFTER trigger
                END;
            $#{table_name}_version$ LANGUAGE plpgsql;

            CREATE TRIGGER #{table_name}_versions
            AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
            FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_version();
        "

        execute("drop trigger #{table_name}_versions on #{table_name}")
        execute(trigger_sql)
    end
end