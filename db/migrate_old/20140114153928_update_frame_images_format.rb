class UpdateFrameImagesFormat < ActiveRecord::Migration[4.2]
  def change

    puts 'Updating Frames images format'

    Lesson.all.each do |lesson|

        next unless lesson.lesson_type == "frame_list"
        next unless lesson.content.frames

        puts "Processing Lesson: #{lesson.title}"

        lesson.content.frames.each do |frame, index|

            next unless frame.images && frame.images.is_a?(Hash)
            images = [];

            puts "  Updating Frame: #{index}"

            frame.images.each do |label, image|
                next unless image.is_a?(Hash)
                images.push(image.merge('label' => label))
            end
            frame.images = images


            next unless frame.images
            map = {};


            frame.images.each do |image|
                next if image['id']
                id = SecureRandom.uuid
                map[image['label']] = id
                image['id'] = id
            end

            image_ids = frame.images.map { |i| i['id'] }

            # update answers
            unless ['blanks', 'text_on_image'].include?(frame['frame_type'])
                if frame.frame_type != "image_hotspot"
                    answers = []
                    if frame['data'] && frame['data']['questions']
                        answers += frame['data']['questions'].map { |q| q['answers'] }.flatten
                    end

                    if frame['answers'] && frame['frame_type'] == "multiple_choice_image"
                        answers += frame['answers']
                    end

                    answers.each do |answer|
                        if map.key?(answer['label']) && !map.value?(answer['label'])
                            #puts "update answer label from #{answer['label']} to #{map[answer['label']]}"
                            answer['label'] = map[answer['label']]
                        elsif !image_ids.include?(answer['label'])
                            puts "Warning!!!! Not updating answer label #{answer['label'].inspect}"
                        end
                    end

                end

                # update blanks
                if frame['data'] && frame['data']['blanks']
                    frame['data']['blanks'].each do |blank|
                        next if blank.is_a?(String)
                        if map.key?(blank['answer'])
                            #puts "update blank answer from #{blank['answer']} to #{map[blank['answer']]}"
                            blank['answer'] = map[blank['answer']]
                        elsif !image_ids.include?(blank['answer'])
                            puts "Warning!!!! Not updating blank answer #{blank['answer'].inspect} for #{frame.frame_type} frame"
                        end
                    end
                end
            end

            # update main_image_name
            main_image_label = frame['main_image_name']
            if main_image_label
                id = map[main_image_label]
                if !id
                    raise "No image found for main image #{main_image_label}"
                end
                #puts "updating main image #{main_image_label}"
                frame['main_image_id'] = id
                frame['main_image_name'] = nil
            end

            #update prompt image
            if frame['prompt_image'] && !frame['prompt_image'].blank?
                id = map[frame['prompt_image']]
                if !id
                    raise "No image found for prompt_image #{frame['prompt_image'].inspect}"
                end
                # puts "updating prompt_image #{main_image_label}"
                frame['prompt_image'] = id
            end


        end

        begin
            lesson.save!
        rescue Exception => e
            puts "Failed to save #{lesson.title.inspect} / #{lesson.id}: #{e.message}"
        end

    end


  end
end
