class UpdateCareerNetworkOnlyCohortType < ActiveRecord::Migration[5.0]
    def up
        execute "UPDATE cohorts SET program_type='career_network_only' WHERE program_type='careers_network_only'"
        execute "UPDATE cohorts_versions SET program_type='career_network_only' WHERE program_type='careers_network_only'"
    end

    def down
        execute "UPDATE cohorts SET program_type='careers_network_only' WHERE program_type='career_network_only'"
        execute "UPDATE cohorts_versions SET program_type='careers_network_only' WHERE program_type='career_network_only'"
    end
end
