class AddProgramtTypeRemoveTitleFromCurriculumTemplates < ActiveRecord::Migration[5.0]
    def change
        remove_column :curriculum_templates, :title, :text
        remove_column :curriculum_templates_versions, :title, :text

        add_column :curriculum_templates, :program_type, :text
        add_column :curriculum_templates_versions, :program_type, :text

        unless reverting?

            execute("update curriculum_templates set program_type = 'career_network_only'")
            execute("update curriculum_templates set program_type = 'emba' where name like 'EMBA%'")
            execute("update curriculum_templates set program_type = 'mba' where name like 'MBA%'")
            change_column :curriculum_templates, :program_type, :text, :null => false
        end

    end
end
