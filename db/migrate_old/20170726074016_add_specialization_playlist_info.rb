class AddSpecializationPlaylistInfo < ActiveRecord::Migration[5.1]
    def up
        ViewHelpers.migrate(self, 20170726074016)
    end

    def down
        ViewHelpers.rollback(self, 20170726074016)
    end
end
