class AllowNullApplicationDeadline < ActiveRecord::Migration[5.0]
    def up
        change_column :cohorts, :application_deadline, :timestamp, :null => true
    end

    def down
        change_column :cohorts, :application_deadline, :timestamp, :null => false
    end
end
