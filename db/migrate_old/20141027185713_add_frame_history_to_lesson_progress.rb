class AddFrameHistoryToLessonProgress < ActiveRecord::Migration[4.2]
    def change
        add_column :lesson_progress, :frame_history, :json
    end
end
