user = User.find_or_create_by({
    email: 'nate@pedago.com'
})
user.password = user.password_confirmation = 'password'
user.add_role(:admin)
user.save!

Tag.find_or_create_by(:entity_type => "Group", :text => "MBA")