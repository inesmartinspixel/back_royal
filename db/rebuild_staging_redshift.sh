#!/bin/bash

############################################################################################################
# BEGIN CONFIG BLOCK
############################################################################################################

# profile names
SOURCE_PROFILE_NAME=production
TOOLS_PROFILE_NAME=tools
TARGET_PROFILE_NAME=staging

# account ids
SOURCE_ACCOUNT_ID=435931806597
TARGET_ACCOUNT_ID=933376372560

# snapshots
TRANSIT_SNAPSHOT_ID_SOURCE="rebuild-staging-redshift-${SOURCE_PROFILE_NAME}"

# clusters
SOURCE_CLUSTER_ID="smartly-production-events"
TARGET_CLUSTER_ID="smartly-staging-events"

############################################################################################################



# CD to script directory
cd "$(dirname "$0")"


# checks return value from running and exits if non-zero, loggint to Sentry
# see also: https://blog.sentry.io/2017/11/28/sentry-bash
eval "$(sentry-cli bash-hook)"


# begin timing
start_time="$(date +%s)"

# find any lingering transit snapshot
echo "Deleting any lingering transit snapshots ..."
aws redshift revoke-snapshot-access --profile ${SOURCE_PROFILE_NAME} snapshot-identifier ${TRANSIT_SNAPSHOT_ID_SOURCE} --account-with-restore-access ${TARGET_ACCOUNT_ID} 2> /dev/null || true
aws redshift delete-cluster-snapshot --profile ${SOURCE_PROFILE_NAME} --snapshot-identifier ${TRANSIT_SNAPSHOT_ID_SOURCE} 2> /dev/null || true


# find and delete any existing target clusters
EXISTING_CLUSTER=$(aws redshift describe-clusters \
    --profile ${TARGET_PROFILE_NAME} \
    --query 'Clusters[*].[ClusterIdentifier]' \
    --output text )
if [[ $EXISTING_CLUSTER == *"${TARGET_CLUSTER_ID}"* ]]
then
    echo "Deleting ${TARGET_CLUSTER_ID} cluster ..."
    aws redshift delete-cluster \
        --profile ${TARGET_PROFILE_NAME} \
        --cluster-identifier ${TARGET_CLUSTER_ID} \
        --skip-final-cluster-snapshot
    sleep 600 && aws redshift wait cluster-deleted --profile ${TARGET_PROFILE_NAME} --cluster-identifier ${TARGET_CLUSTER_ID}
else
    echo "No existing ${TARGET_CLUSTER_ID} cluster found."
fi


# find the latest production snapshot for restore
echo "Looking for latest automated ${SOURCE_CLUSTER_ID} snapshot ..."
SNAPSHOT_START_TIME=$(date -d "8 hours ago" -u +"%Y-%m-%dT%H:%MZ")
SNAPSHOT_IDENTIFIER="$(aws redshift describe-cluster-snapshots \
    --profile ${SOURCE_PROFILE_NAME} \
    --cluster-identifier ${SOURCE_CLUSTER_ID} \
    --snapshot-type automated \
    --start-time "${SNAPSHOT_START_TIME}" \
    --query="reverse(sort_by(Snapshots, &SnapshotCreateTime))[0]|SnapshotIdentifier" \
    --output text)"


# copy the automated snapshot to manual so that it can be shared
echo "Copying the automated snapshot to a manual source snapshot ..."
aws redshift copy-cluster-snapshot \
    --profile ${SOURCE_PROFILE_NAME} \
    --source-snapshot-identifier ${SNAPSHOT_IDENTIFIER} \
    --target-snapshot-identifier ${TRANSIT_SNAPSHOT_ID_SOURCE} \
    --source-snapshot-cluster-identifier ${SOURCE_CLUSTER_ID}
sleep 600 && aws redshift wait snapshot-available --profile ${SOURCE_PROFILE_NAME} --snapshot-identifier ${TRANSIT_SNAPSHOT_ID_SOURCE}


############################################################################################################
# NOTE: Here is where we would insert any logic that would share access to the tools account, then
# spool up a new cluster with the provided snapshot within taht account for manipulation.
# In reality, this is not a little difficult given the size of the data set, which is why we share directly
# with staging and rebuild frequently with this destructive script.
############################################################################################################


# authorize access to the tools account
echo "Authorizing transit snapshot access to the staging account ..."
aws redshift authorize-snapshot-access \
    --profile ${SOURCE_PROFILE_NAME} \
    --snapshot-identifier ${TRANSIT_SNAPSHOT_ID_SOURCE} \
    --account-with-restore-access ${TARGET_ACCOUNT_ID}


# restore the local target snapshot
echo "Restoring the local target snapshot ..."
aws redshift restore-from-cluster-snapshot \
    --profile ${TARGET_PROFILE_NAME} \
    --snapshot-identifier ${TRANSIT_SNAPSHOT_ID_SOURCE} \
    --owner-account ${SOURCE_ACCOUNT_ID} \
    --cluster-identifier ${TARGET_CLUSTER_ID} \
    --availability-zone "us-east-1c" \
    --no-publicly-accessible \
    --cluster-subnet-group-name "smartly-staging-redshiftsubstagingaz2-12abiuogcu96k" \
    --cluster-parameter-group-name "smartly-staging-redshiftparamsmartlyredshift-12k94vibqxkfv" \
    --vpc-security-group-ids "sg-0e8fadf8c6db3cd46" \
    --iam-roles "arn:aws:iam::933376372560:role/SmartlyStagingRedshiftStreams" "arn:aws:iam::933376372560:role/SmartlyStagingRedshiftDevStreams" \
    --preferred-maintenance-window 'Thu:04:30-Thu:05:00' \
    --automated-snapshot-retention-period 1


# revoke access to the tools account
echo "Revoking transit snapshot access from the staging account ..."
aws redshift revoke-snapshot-access \
    --profile ${SOURCE_PROFILE_NAME} \
    --snapshot-identifier ${TRANSIT_SNAPSHOT_ID_SOURCE} \
    --account-with-restore-access ${TARGET_ACCOUNT_ID}


echo "Deleting transit snapshot"
aws redshift delete-cluster-snapshot \
    --profile ${SOURCE_PROFILE_NAME} \
    --snapshot-identifier ${TRANSIT_SNAPSHOT_ID_SOURCE}


echo "Process complete - Duration: $((($(date +%s)-$start_time)/60)) minutes"