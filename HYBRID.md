# Native Application Wrappers

## [PhoneGap](http://phonegap.com/) / [Cordova](http://cordova.apache.org/)

These are the most popular cross-compilation frameworks leveraging web codebases into native apps. Both package all assets (JS/HTML/CSS/etc) as part of the binary distribution, using WebViews to render content. Both feature a plugin system to expose support to underlying device hardware.

Phonegap is free for use under Adobe license terms. Cordova is open-sourced under Apache License, Version 2.0.

[PhoneGap Build](https://build.phonegap.com/) is a paid service that automates compilation of PhoneGap releases through a simplified upload -> download mechanism. They distinguish between public and private to create their pricing tiers.

### Strengths

- Boasts support for an impressive number of devices
- Distribution contains all essential assets so startup time should just be hinged on native process spin-up + initial DOM paint
- Plugin system abstracts away platform-specifics in implementation of native hardware features

### Weaknesses

- Significant learning curve
- Requires client obsolesence management / legacy API support
- Requires ensuring we do everything 'single-page', meaning all login functionality would have to be moved into JS view templates + API calls
- No support for cookie-based authentication
- Where plugin system is deficient (emerging platform features, non-normalized features) there aren't any alternatives short of writing your own native code + plugin bridge

### Suggested Reading

- [Angular tips](http://briantford.com/blog/angular-phonegap) such as registering a "cordovaReady" factory wrap-function for services
- [Devise customization](http://stackoverflow.com/questions/13623562/phonegap-mobile-rails-authentication-devise-authentication-from-scratch) for implementing token authentication for PhoneGap
- [Plugin Development Guide](http://docs.phonegap.com/en/3.0.0/guide_hybrid_plugins_index.md.html)

### Development Setup

*Assumes XCode / DevTools have been setup already*

- Download Cordova - `npm install -g cordova`
- Download Gradle - `brew install gradle`
- Download and install [Android Studio](https://developer.android.com/studio/#downloads) and enable HAXM vitualization support in the process
- Change directory to `./hybrid`
- Install gem dependencies - `BUNDLE_GEMFILE=Gemfile.hybrid bundle install`
- Install iOS Simulator Bridge - `npm install -g ios-sim --unsafe-perm=true`
- Install iOS Deploy - `npm install -g ios-deploy`
- Validate iOS environment can boot by running `cordova emulate ios`
- Open Android Studio > Configure > SDK Manager and download the minimum SDK version (check with Cordova version)
- Open Android Studio > Configure > AVD Manager and ensure you have a test device image setup (ie - Pixel 3 running recent Android version)
- Validate Android environment can boot by running `cordova emulate android`
- Open a project in Android Studio and click the little Device icon in the upper-right of the project window. Install a valid device image and dependencies.
- Follow the instructions in each `merges/<platform>/google_oauth_id.sample.js` config file

### Development Process

- For local development / testing, ensure you are running `npm run devserver` just as normal. This will ensure that all JS / SCSS / HTML files trigger watches that re-compile the main application source.
- As the application source is rebuilt, simply invoking in the simulator via `cordova emulate android` or `cordova emulate ios` will re-launch with any changes made
    + Alternatively, `cordova run android` or `cordova run ios` _(requires certificate signing)_ can be used to debug on device, once hardware has been properly setup _(see below)_
- Both Chrome and Safari can be used to debug iOS and Android respectively, although Chrome provides a significantly better debugging experience. Case in point, Safari only seems to console log for when the debugger is attached, and does not retain debug information prior to this point.
    + Within Chrome, navigate a new tab to `chrome://inspect`. This will produce a list of external devices, including the Android Emulator. Simply click `inspect` to bring up a familar Chrome Debugging Tools window.
    + Within Safari after launch, navigate to _Develop > iOS Simulator > Smartly > index.html_ to launch the debugging tools

### Hardware Setup

- For Android device testing, be sure to [enable USB debugging](http://developer.android.com/tools/device.html#setting-up) on the device. After that, debug builds should be deployable without any additional setup.

- For iOS device testing, you will need to setup iOS Provisioning as well as loading the XCode Project. See also: [iOS Platform Guide](http://cordova.apache.org/docs/en/edge/guide_platforms_ios_index.md.html#iOS%20Platform%20Guide)

### iOS Physical Device Provisioning

- Ask a team admin to add your Apple ID to **both** `itunesconnect` and `developer.apple.com` environments. These two environments are separate entities, and you'll need access to both.
- Ensure the iOS device you wish to provision is connected via USB to your machine (you need to "trust" the machine as well).
- Log in with your Apple ID in Xcode at `Xcode -> Preferences -> Accounts` (you should see the Pedago LLC team show up).
- While on this screen, click `Download Manual Profiles` and `Manage Certificates`. If needed, create a signing certificate for your machine here.
- Open `hybrid/platforms/ios/Smartly.xcworkspace` in Xcode
- Select the `Smartly` project, then navigate to `General -> Signing`.
    + Ensure that "Automatically manage signing" is checked.
    + Select `Pedago LLC` as the team.
    + You _should_ see something along the lines of "this device is not provisioned..." with a button to automatically provision your device. Click it.
- You should now be able to deploy Smartly to your physical iOS device with `cordova run ios --device`

### Performance Tuning

- Hybrid shells present unique challenges. See also: [Cordova Apps - Rendering Performance](http://blog.nparashuram.com/2014/10/measuring-rendering-performance-metrics.html)

### Beta-Testing Deployment (iOS)

- Navigate to hybrid directory `cd hybrid`
- Ensure that you're on a clean copy of master with no changes
- run `BUNDLE_GEMFILE=Gemfile.hybrid fastlane ios upload`
- After successful submission, open [iTunes Connect](https://itunesconnect.apple.com/WebObjects/iTunesConnect.woa/ra/ng/app)
- Navigate to the Smartly application entry and select the **Prerelease** tab
- Verify that the new build successfully processed (will take ~5-10m)
  - If the processing phase seems to take longer than expected, increment the build number in XCode using a dot-suffix (ie - 1234 -> 1234.1) and re-submit
- Optionally invite any additional internal users via the **Internal Users** tab on the **Prereleases** page. External users can only be invited after the app has passed the Apple Approval Process


### Google Native Testing

** General **

- Set your `application.yml`'s `OAUTH_GOOGLE_KEY` to *Production's* value
- Update `cordova_bootstrap.js` to hardcode the `window.CORDOVA.endpointRoot` value to your internal IP (don't forget the port)
- Update `client_config_interceptor.js` to immediately `return response` so as not to trigger an upgrade error
- Manually uninstall any copies of Smartly already on the target device
- Be sure to `git reset` and revert the `application.yml` change once you're done!

### Push Notification Setup

** NOTE: **

This has already been done, but is documented here in case it ever needs to happen again. ** Please, do not mess with these files/settings! **

** General **

- Both Android and iOS use FCM (Firebase Cloud Messaging) for push noticications, but there are a few idiosyncracies for each environment.
- Both environments utilize the same .json server key to authenticate with FCM. This file is located in 1password as `smartly-production-firebase-adminsdk-....json`.
    + NOTE: this file contains very sensitive information, and should be kept as locked up as possible. NEVER put it under version control.

** Android **

- Our Android environment uses a .json file, `hybrid/certs/google-services.json` (this file is generated in the Firebase console), to specify entitlements and services that our Android application needs.
- This file is copied to `hybrid/platforms/android/app/google-services.json` in `hybrid/config.xml`.

** iOS **

- Our iOS environment users a .plist file, `hybrid/certs/GoogleService-Info.plist` (this file is generated in the Firebase console), to specify entitlements and services that our iOS application needs.
- This file is specified as a `resource-file` in `hybrid/config.xml` and is copied to `hybrid/platforms/ios/Smartly/Resources/certs/GoogleService-Info.plist`
    + Without this file, iOS will default to registering with APNs (we don't want this).
- Firebase needs an APNs authentication key to send FCM notifications to iOS applications.
    + This key is available in 1password as `SmartlyAPNsKey`
    + This key can be created in the developer.apple console.
    + This key establishes connectivity between our application and the Apple Push Notification service.
    + Unlike APNs certificates, this key will _never_ expire, and can be used for _all_ of our applications (local development, staging, prod).
    + This key needs to be downloaded in a .p8 format and uploaded to the firebase console in order for Firebase to authenticate with APNs on our application's behalf.
        + Do this here: https://console.firebase.google.com/project/smartly-production/settings/general/ios:com.smartly.hybrid


### Push Notification Testing

** General **

- Ensure `npm run devserver` is running
- Start delayed jobs with `bin/delayed_job start`

** Android **

- NOTE: The official Android emulator supports remote notifications out-of-the-box, though you may need to download the `Google Play Services` package under `Android Studio -> SDK Manager -> SDK Tools`.
- Deploy Smartly to an emulator with `cordova emulate android`, or to a physical device with `cordova run android` (USB debugging will need to be enabled on your physical device).
- The build/compilation process _should_ take care of all the FCM stuff mentioned above.
- Login with an existing user, or register a new user.
- Ensure that an Event was created with `event_type => 'cordova:device-registered'`.
- Ensure the corresponding delayed job ran.
- There should be a `mobile_devices` record for the user you authenticated.
    + This device _should_ now be identified on customer.io for this user (https://fly.customer.io/env/<ENV_USED_FOR_TESTING>/people/<USER_ID>/devices).
    + Grab the device's token (you can either grab the device's token in back_royal, or in customer.io).
    + Go to "Device Tokens" below

** iOS **

- NOTE: iOS does _not_ support remote notifications in any kind of simulated environment. You'll have to deploy Smartly to a physical device to test iOS push notifications.
- Deploy Smartly to a physical, provisioned, iOS device with `cordova run ios`
- The build/compilation process _should_ take care of all the FCM stuff mentioned above.
- Login with an existing user, or register a new user.
- Ensure that an Event was created with `event_type => 'cordova:device-registered'`.
- Ensure the corresponding delayed job ran.
- There should be a `mobile_devices` record for the user you authenticated.
    + This device _should_ now be identified on customer.io for this user (https://fly.customer.io/env/<ENV_USED_FOR_TESTING>/people/<USER_ID>/devices).
    + Grab the device's token (you can either grab the device's token in back_royal, or in customer.io).
    + Go to "Device Tokens" below

** Device Tokens **

- Once you have a device's token, you can either test push notifications in the Firebase console, or on customer.io directly.
- Firebase:
    + Go to https://console.firebase.google.com/project/smartly-production/notification/compose`
    + Select `Single device` for `Target`
    + Enter the device token in `FCM registration token`
    + Enter `Message text` (you shouldn't need to enter anything else)
    + Press `Send message`
- Customer.io:
    + Go to https://fly.customer.io/env/<ENV_USED_FOR_TESTING>/settings/actions/push
    + Go to `Testing` section
    + Enter the device token in `A custom token`
    + Press `Send`

### Production Builds

**iOS**

- `cd hybrid`
- `BUNDLE_GEMFILE=Gemfile.hybrid fastlane ios make_build`
- Deploy to target device with `ios-deploy -b path/to/ipa/file`

**Android**

- `cd hybrid`
- `BUNDLE_GEMFILE=Gemfile.hybrid fastlane android make_build`
- Deploy to target device with `adb install platforms/android/app/build/outputs/apk/release/app-release.apk`

- NOTE: If encountering Manifest merge errors after updating plugins or platforms (ie - `com.android.support:support-v4`)
  - see also: http://blog.danlew.net/2015/09/09/dont-use-dynamic-versions-for-your-dependencies/


### Miya Miya Local Development

 - From one terminal tab run: `npm run devserver -- --env.criticalLocale="ar"`
 - From another tab run: `cd hybrid; cp overrides/Miya\ Miya/www/client_overrides.js www/; cordova emulate ios`
 - Be sure to run `git reset --hard; git clean --force -d` to reset any changes made from the overrides


### Fastlane Notes

**iOS**
- When you run `BUNDLE_GEMFILE=Gemfile.hybrid fastlane ios make_build` for the first time, you'll be prompted for the passphrase to the repository containing our certificates and provisioning profiles. The passphrase is in 1Password as "Fastlane Certificate Repo Passphrase". Once you enter that passphrase, Fastlane will store it in your keychain. You should not need to enter it agian.

- When you run `BUNDLE_GEMFILE=Gemfile.hybrid fastlane ios upload`, you will be prompted for your Apple ID every time. Unforunately our team does not currently have a "shared" apple developer account that we use for codesigning, but that's something we could explore in the future. It would certainly make signing certificates and provisioning profiles simpler. However, those are now both handled for us by Fastlane/Match so it may not even be worth looking at now.

- When you first attempt to upload a binary to Appstore Connect, if you Apple ID has MFA enabled, you will be prompted to create an applicaiton specific password. You can do this at https://appleid.apple.com. Once you create a password and enter it when prompted, Fastlane should save it to your keychain and you should not be prompted again.


**Android**
- Ensure you have both `SMARTLY_ANDROID_STORE` and `SMARTLY_ANDROID_KEY` environment variables. These values are stored in your keychain and exported by `.back_royal.bash`. Also ensure that you have the `hybrid/smartly-android-upload.keystore`. If you do not have either of these variables or the keystore file, contact a dev who does and they can get you setup. Otherwise, you should be able to run `BUNDLE_GEMFILE=Gemfile.hybrid fastlane android make_build`.

- Download the `.json` file in 1Password named "Google Play Service Account Keys" and place it in the `hybrid/certs` directory. It is not under version control, and should remain that way. You should now be able authenticate with Google Play Publish API when running `BUNDLE_GEMFILE=Gemfile.hybrid fastlane android upload`.


**Cutting builds for both platforms**
- Ensure you've met the requirements mentioned above enabling you to cut iOS and Android builds, and run `BUNDLE_GEMFILE=Gemfile.hybrid fastlane upload_all`.


## Android Compatability Issues

|    Name     |    Phone Make     | Android Version | Chrome Version |    Current Status    |
|-------------|-------------------|-----------------|----------------|----------------------|
| Scott       | Galaxy S6         | 5.1.1           | 48.0.2564.95   | Unstable?            |
| Brent       | Nexus 5x          | 6.0.1           | 48.0.2564.95   | Seems stable         |
| Matt        | MotoX 2014 Pur    | 6.0.0           | 48.0.2564.95   | Seems stable         |
