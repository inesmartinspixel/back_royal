#!/bin/bash

# Run this from within its containing directory. For a more verbose implementation, see also:
# https://github.com/busterc/cordova-resource-generators

ICON="icon.png"
ICON_PUSH="icon_push.png"

convert="convert $ICON"
convert_push="convert $ICON_PUSH"


# For Android's main icon, use Android Studio's asset preparation tool to trim, preview, etc.
# Copy the output directories into the appropriate res/icons/android directory. This ensures
# we have legacy assets at the appropriate resolutions as well.
# See also: https://netsyms.com/blog/post/adding-android-oreo-adaptive-icons-to-a-cordova-project 

# NOTE: When we can migrrate to minSdkVersion 26+, we can revist and only generate 
# ic_launcher_foreground files with $convert, similar to iOS.

mkdir -p android
cp $ICON android/icon.png
$convert_push -resize 16x16 android/mipmap-ldpi/icon_push.png
$convert_push -resize 24x24 android/mipmap-mdpi/icon_push.png
$convert_push -resize 36x36 android/mipmap-hdpi/icon_push.png
$convert_push -resize 48x48 android/mipmap-xhdpi/icon_push.png
$convert_push -resize 72x72 android/mipmap-xxhdpi/icon_push.png
$convert_push -resize 96x96 android/mipmap-xxxhdpi/icon_push.png


mkdir -p ios
cp $ICON ios/icon.png
$convert -resize 29 ios/icon-29.png
$convert -resize 40 ios/icon-40.png
$convert -resize 50 ios/icon-50.png
$convert -resize 57 ios/icon-57.png
$convert -resize 58 ios/icon-29-2x.png
$convert -resize 87 ios/icon-29-3x.png
$convert -resize 72 ios/icon-72.png
$convert -resize 76 ios/icon-76.png
$convert -resize 80 ios/icon-40-2x.png
$convert -resize 100 ios/icon-50-2x.png
$convert -resize 114 ios/icon-57-2x.png
$convert -resize 120 ios/icon-60-2x.png
$convert -resize 144 ios/icon-72-2x.png
$convert -resize 152 ios/icon-76-2x.png
$convert -resize 167 ios/icon-83.5-2x.png
$convert -resize 180 ios/icon-60-3x.png
$convert -resize 1024 ios/icon-1024.png
$convert -resize 1024 ios/icon-app-store.png