#!/bin/bash

# Run this from within its containing directory. For a more verbose implementation, see also:
# https://github.com/busterc/cordova-resource-generators

SCREEN="hi-res-screen.png"
BGCOLOR="#FFFFFF"

convert="convert $SCREEN -background $BGCOLOR -gravity center"

mkdir -p android
$convert -resize 800x800 -extent 1280x1920  android/splash-port-xxxhdpi.png
$convert -resize 640x640 -extent 960x1600   android/splash-port-xxhdpi.png
$convert -resize 512x512 -extent 720x1280   android/splash-port-xhdpi.png
$convert -resize 320x320 -extent 480x800    android/splash-port-hdpi.png
$convert -resize 256x256 -extent 320x480    android/splash-port-mdpi.png
$convert -resize 128x128 -extent 200x320    android/splash-port-ldpi.png
$convert -resize 800x800 -extent 1920x1280  android/splash-land-xxxhdpi.png
$convert -resize 640x640 -extent 1600x960   android/splash-land-xxhdpi.png
$convert -resize 512x512 -extent 1280x720   android/splash-land-xhdpi.png
$convert -resize 320x320 -extent 800x480    android/splash-land-hdpi.png
$convert -resize 256x256 -extent 480x320    android/splash-land-mdpi.png
$convert -resize 128x128 -extent 320x200    android/splash-land-ldpi.png


mkdir -p ios
$convert -resize 512x512      -extent 1334x1334    ios/Default@2x~iphone~anyany.png
$convert -resize 512x512      -extent 750x1334     ios/Default@2x~iphone~comany.png
$convert -resize 512x512      -extent 1334x750     ios/Default@2x~iphone~comcom.png
$convert -resize 1024x1024    -extent 2208x2208    ios/Default@3x~iphone~anyany.png
$convert -resize 1024x1024    -extent 2208x1242    ios/Default@3x~iphone~anycom.png
$convert -resize 1024x1024    -extent 1242x2208    ios/Default@3x~iphone~comany.png
$convert -resize 1024x1024    -extent 2732x2732    ios/Default@2x~ipad~anyany.png
$convert -resize 1024x1024    -extent 1278x2732    ios/Default@2x~ipad~comany.png