#!/bin/bash

# Run this from within its containing directory. For a more verbose implementation, see also:
# https://github.com/busterc/cordova-resource-generators

ICON="icon.png"

convert="convert $ICON"

mkdir -p android
cp $ICON android/icon.png
$convert -resize 36x36 android/icon-36-ldpi.png
$convert -resize 48x48 android/icon-48-mdpi.png
$convert -resize 72x72 android/icon-72-hdpi.png
$convert -resize 96x96 android/icon-96-xhdpi.png
$convert -resize 144x144 android/icon-144-xxhdpi.png
$convert -resize 192x192 android/icon-192-xxxhdpi.png

mkdir -p ios
cp $ICON ios/icon.png
$convert -resize 29 ios/icon-29.png
$convert -resize 40 ios/icon-40.png
$convert -resize 50 ios/icon-50.png
$convert -resize 57 ios/icon-57.png
$convert -resize 58 ios/icon-29-2x.png
$convert -resize 87 ios/icon-29-3x.png
$convert -resize 72 ios/icon-72.png
$convert -resize 76 ios/icon-76.png
$convert -resize 80 ios/icon-40-2x.png
$convert -resize 100 ios/icon-50-2x.png
$convert -resize 114 ios/icon-57-2x.png
$convert -resize 120 ios/icon-60-2x.png
$convert -resize 144 ios/icon-72-2x.png
$convert -resize 152 ios/icon-76-2x.png
$convert -resize 167 ios/icon-83.5-2x.png
$convert -resize 180 ios/icon-60-3x.png
$convert -resize 1024 ios/icon-1024.png
$convert -resize 1024 ios/icon-app-store.png