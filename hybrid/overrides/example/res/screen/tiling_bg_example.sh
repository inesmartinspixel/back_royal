#!/bin/bash

# Run this from within its containing directory.
BG="tiling_bg_3x.png"
LOGO="middle_logo_3x.png"

# $1 = size (750x1334), $2 = output path (ios/Default-667h.png)
generateSplashLowDPI () {
    convert -resize 16% $BG tmp.png
    convert -size $1 tile:tmp.png tmp.png
    composite -gravity center \( -resize 16% $LOGO \) tmp.png $2
}
generateSplash1x () {
    convert -resize 33% $BG tmp.png
    convert -size $1 tile:tmp.png tmp.png
    composite -gravity center \( -resize 33% $LOGO \) tmp.png $2
}
generateSplash2x () {
    convert -resize 66% $BG tmp.png
    convert -size $1 tile:tmp.png tmp.png
    composite -gravity center \( -resize 66% $LOGO \) tmp.png $2
}
generateSplash3x () {
    convert -size $1 tile:$BG tmp.png
    composite -gravity center $LOGO tmp.png $2
}

mkdir -p android
generateSplash2x      720x1280   android/splash-land-xhdpi.png
generateSplash1x      480x800    android/splash-land-hdpi.png
generateSplash1x      320x480    android/splash-land-mdpi.png
generateSplashLowDPI  200x320    android/splash-land-ldpi.png
generateSplash2x      1280x720   android/splash-port-xhdpi.png
generateSplash1x      800x480    android/splash-port-hdpi.png
generateSplash1x      480x320    android/splash-port-mdpi.png
generateSplashLowDPI  320x200    android/splash-port-ldpi.png

mkdir -p ios
generateSplash1x   320x480     ios/Default~iphone.png
generateSplash2x   640x960     ios/Default@2x~iphone.png
generateSplash2x   640x1136    ios/Default-568h@2x~iphone.png
generateSplash1x   768x1024    ios/Default-Portrait~ipad.png
generateSplash2x   1536x2048   ios/Default-Portrait@2x~ipad.png
generateSplash1x   1024x768    ios/Default-Landscape~ipad.png
generateSplash2x   2048x1536   ios/Default-Landscape@2x~ipad.png
generateSplash2x   750x1334    ios/Default-667h.png
generateSplash3x   1242x2208   ios/Default-736h.png
generateSplash3x   2208x1242   ios/Default-Landscape-736h.png

rm tmp.png