#!/bin/bash

find-rename-regex() (
  set -eu
  find_and_replace="$1"
  PATH="$(echo "$PATH" | gsed -E 's/(^|:)[^\/][^:]*//g')" \
    find ../platforms/ios -depth -execdir rename "${2:--f}" "s/${find_and_replace}" '{}' \;
)


# Prepare any special overrides if a valid one is provided
TEMPLATE_NAME="$1"
FILE_NAME=$(echo "$1" | gsed "s/ /\\\\ /g")
UNDERSCORE_TEMPLATE_NAME=$(echo "$1" | gsed "s/ /_/g")

if [ ! -d "$TEMPLATE_NAME" ]; then
    echo "No directory found for: $1"
    exit 1
fi
echo "Using overrides for $1 ..."
eval cp -R "${FILE_NAME}/*" ..

# Replace all MiyaMiya references in platforms/ios structure, post-copy

echo "Depth first renaming of Quantic files. This may take awhile ..."
find-rename-regex "Quantic/${TEMPLATE_NAME}/g"

echo "Replacing all string references ..."
find ../platforms/android \( ! -regex '^www\/assets\/.*' \) -type f -print0 | xargs -0 gsed -i "s/Quantic/${TEMPLATE_NAME}/g"

# FIXME: This is not working (?) Pods are particularly tricky ... 
# The project .pbxproj files have entries in them that follow differing conventions:
#
#    Framework (others?) references are underscored. eg: 
#           `Pods_Quantic.framework` -> `Pods_Miya_Miya.framework`
#
#    All path elements in the files are now newly quoted. eg:
#           `path = Pods-Quantic-Info.plist` -> `path = "Pods-Miya Miya-Info.plist"`
#
#    Non-path elements are handled how? eg:
#           `Quantic-Info.plist` -> ???
#           `Pods-Quantic.release.xcconfig` -> ???
# 

find ../platforms/ios -name "*.pbxproj" -print0 | xargs -0 gsed -i "s/Pods_Quantic.framework/Pods_${UNDERSCORE_TEMPLATE_NAME}.framework/g"
find ../platforms/ios -name "*.pbxproj" -print0 | xargs -0 gsed -i -r "s/path = ([^\"]*Quantic[^;]*?);/path = \"\1\";/g"
find ../platforms/ios -name "*.pbxproj" -print0 | xargs -0 gsed -i -r "s/name = ([^\"]*Quantic[^;]*?);/name = \"\1\";/g"
find ../platforms/ios -name "*.pbxproj" -print0 | xargs -0 gsed -i -r "s/productName = ([^\"]*Quantic[^;]*?);/productName = \"\1\";/g"
find ../platforms/ios \( ! -regex '^www\/assets\/.*' \) -type f -print0 | xargs -0 gsed -i "s/Quantic/${TEMPLATE_NAME}/g"