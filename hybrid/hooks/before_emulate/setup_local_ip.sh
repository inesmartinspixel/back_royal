#!/bin/sh

command="subdomain=$NGROK_SUBDOMAIN"
running=`ps ax | grep -v grep | grep $command`

echo 'window.CORDOVA = {};' | tee ./merges/ios/remote_endpoint.js ./merges/android/remote_endpoint.js >/dev/null
if [ -n "$running" ]; then
    echo "ngrok is running - using tunnel endpoint"
    echo window.CORDOVA.endpointRoot=\'https://$NGROK_SUBDOMAIN.ngrok.io\' | tee -a ./merges/ios/remote_endpoint.js ./merges/android/remote_endpoint.js >/dev/null
else
    echo "ngrok is not running - defaulting to local ipv4"
    local_ip=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1' | head -n 1)
    echo window.CORDOVA.endpointRoot=\'http://$local_ip:3000\' | tee -a ./merges/ios/remote_endpoint.js ./merges/android/remote_endpoint.js >/dev/null
fi