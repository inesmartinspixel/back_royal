#!/bin/sh

echo "Removing plugins ..."

cordova plugin rm cordova-plugin-sqlite-2 --save
cordova plugin rm cordova-plugin-screen-orientation --save
cordova plugin rm cordova-plugin-camera --save
cordova plugin rm cordova-android-support-gradle-release --save
cordova plugin rm cordova-plugin-ionic-webview --save
cordova plugin rm cordova-plugin-googleplus --save
cordova plugin rm cordova-custom-config --save
cordova plugin rm cordova-plugin-spinner-dialog --save
cordova plugin rm cordova-plugin-network-information -- save
cordova plugin rm cordova-plugin-whitelist --save
cordova plugin rm cordova-plugin-dialogs --save
cordova plugin rm cordova-plugin-x-socialsharing --save
cordova plugin rm cordova-plugin-market --save
cordova plugin rm cordova-plugin-facebook4 --save
cordova plugin rm cordova-plugin-media --save
cordova plugin rm cordova-plugin-inappbrowser --save
cordova plugin rm cordova-plugin-device --save
cordova plugin rm cordova-plugin-splashscreen --save
cordova plugin rm cordova-plugin-apprate --save
cordova plugin rm phonegap-plugin-push --save
cordova plugin rm com-darryncampbell-cordova-plugin-intent --save
cordova plugin rm cordova-plugin-file --save
cordova plugin rm cordova-plugin-sign-in-with-apple --save
# cordova plugin rm cordova-plugin-calendar # see below

rm -rf ./plugins ./platforms/ios/Quantic/Plugins/ .platforms/android/src/de .platforms/android/src/nl .platforms/android/src/org .platforms/android/src/com/xmartlabs


echo "Adding plugins ..."

cordova plugin add cordova-plugin-file --save
cordova plugin add com-darryncampbell-cordova-plugin-intent --save
# NOTE: whenever upgrading `phonegap-plugin-push`, FCM_VERSION needs to align with
# the `PLAY_SERVICES_VERSION` variable referenced by `cordova-plugin-googleplus`!!
cordova plugin add phonegap-plugin-push --save --variable ANDROID_SUPPORT_V13_VERSION=27.+ --variable FCM_VERSION=11.8.0 --save
# FIXME: update `cordova-plugin-apprate` to standard versioning once > 1.4.0
cordova plugin add https://github.com/pushandplay/cordova-plugin-apprate#master --save
cordova plugin add https://github.com/apache/cordova-plugin-splashscreen --save
cordova plugin add cordova-plugin-device --save
cordova plugin add https://github.com/apache/cordova-plugin-inappbrowser --save
cordova plugin add https://github.com/booleanbetrayal/cordova-plugin-media#ambient_output --save
cordova plugin add https://github.com/jeduan/cordova-plugin-facebook4.git#master --variable APP_ID=978172552216701 --variable APP_NAME=Quantic --save
cordova plugin add https://github.com/xmartlabs/cordova-plugin-market --save
cordova plugin add https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin.git#master --save
cordova plugin add cordova-plugin-dialogs --save
cordova plugin add cordova-plugin-whitelist --save
cordova plugin add cordova-plugin-network-information --save
cordova plugin add https://github.com/Paldom/SpinnerDialog --save
cordova plugin add cordova-custom-config --save
cordova plugin add cordova-plugin-ionic-webview --save
cordova plugin add cordova-android-support-gradle-release --fetch --variable ANDROID_SUPPORT_VERSION=26+ --save
cordova plugin add cordova-plugin-camera --save
cordova plugin add cordova-plugin-screen-orientation --save
cordova plugin add https://github.com/twogate/cordova-plugin-sign-in-with-apple.git#master --save
cordova plugin add cordova-plugin-sqlite-2 --save
cordova plugin add https://github.com/EddyVerbruggen/cordova-plugin-googleplus --save --variable REVERSED_CLIENT_ID=com.googleusercontent.apps.557194579793-kp7l6fq8tpbjovovn8tjjqavqnqeeh2l --variable PLAY_SERVICES_VERSION=11.8.0

# NOTE: This will add calendar permission entries to:
#  hybrid/platforms/android/app/src/main/AndroidManifest.xml
#  hybrid/platforms/android/android.json
# If you see the manual setup instructions for Android, you'll notice
# that it mentions you don't need these permissions if only using the
# interactively API, which we are, assumably because you're offloading
# the calendar event creation to the system calendar, rather than doing it
# directly. So, make sure to remove those entries after updating this plugin.
# We decided to do this rather than follow all the manual instructions.
# See https://github.com/EddyVerbruggen/Calendar-PhoneGap-Plugin#android
#
# I made an issue on the repository for adding a config variable to control
# this. If that issue is resolved, then we can remove all of these comments
# and uncomment the CLI commands.
# See https://github.com/EddyVerbruggen/Calendar-PhoneGap-Plugin/issues/516
# cordova plugin add cordova-plugin-calendar --variable CALENDAR_USAGE_DESCRIPTION="This app can add requested events to your calendar" --variable CONTACTS_USAGE_DESCRIPTION="This app might access contacts when adding a requested calendar event if searching for other people to invite"