'use strict';

// Override this file in any client override directory `hybrid/overrides/<client>`
// (see also: JoinConfig) and provide the following:
//
// window.CORDOVA.forcedUrlPrefix = 'CLIENT_NAME'; // a key from AppConfig#join_config