'use strict';

// Need some sort of indication that we're bootstrapped in Cordova, as well as something to
// contain convenience methods. No jQuery access since this is actually loaded before common.
// So, we set window.CORDOVA with necessary configuration data.
window.CORDOVA = window.CORDOVA || {};
window.CORDOVA.rootPath = window.decodeURIComponent(window.location.href).replace('index.html', '').replace('file://', '');

// We have a script in /hooks/before_emulate/setup_local_ip.sh that sets this value
// If local IP is defined, then connect there, otherwise use production. If you need
// to change to another host, here's a good place to do it.
// window.CORDOVA.endpointRoot = 'https://some-other-host.com'


// cordova container initialization
document.addEventListener('deviceready', function() {

    // replace default window.open functionality for inAppBrowser plugin
    window.open = window.cordova.InAppBrowser.open;

    // Use the Internationalization API now that it is supported on iOS > 9.x to set the user's locale
    // See https://cordova.apache.org/news/2017/11/20/migrate-from-cordova-globalization-plugin.html
    if (window.Intl && typeof window.Intl === 'object') {
        var language = window.navigator.language;

        // We want the language code only. The window.navigator.language value can be suffixed with
        // additional codes such as the country code (ex. en-US vs en-GB) that we do not support.
        var languageCode = language.split(/[^A-Za-z]+/)[0].toLowerCase();
        var supportedLanguages = ['en', 'es', 'sw', 'ar', 'zh', 'it'];

        // Setting this allows for `pref_locale` to be sent to the server upon registration
        window.serverDeterminedLocale = supportedLanguages.indexOf(languageCode) > -1 ? languageCode : 'en';
    } else {
        window.serverDeterminedLocale = 'en';
    }

    //============================================
    // cordova-plugin-apprate configuration
    //============================================

    // See https://github.com/pushandplay/cordova-plugin-apprate#simple-setup-and-call
    // FIXME: https://trello.com/c/Q9Pe46IV/927-feat-properly-pass-ios-and-android-app-identifiers-to-web-client-configs
    window.AppRate.preferences.storeAppURL = {
        ios: '1014850662',
        android: 'market://details?id=com.smartly.hybrid'
    };

    // iOS has a 3x per user per year limit for the number of times a user can be prompted for an in-app review.
    // With this in mind, we want to be very selective about when we prompt the user to rate our app, so we make
    // sure that we DO NOT prompt again when a new app version is installed and instead define our own logic for
    // determining when we should prompt them for an in-app review.
    // See meetsRequirementsForMobileAppRatingPrompt in user.js for more information.
    window.AppRate.preferences.promptAgainForEachNewVersion = false;

    // custom locale strings for cordova-plugin-apprate
    var appRateCustomLocales = {
        ar: {
            language: 'ar',
            title: 'هل تمانع التصويت ٪@؟',
            message: '',
            cancelButtonLabel: 'لا شكرا',
            laterButtonLabel: 'ذكرني لاحقا',
            rateButtonLabel: 'معدل الآن',
            yesButtonLabel: 'نعم فعلا',
            noButtonLabel: 'لا',
            appRatePromptTitle: 'هل تحب استخدام ٪@؟',
            feedbackPromptTitle: 'هل تريد أن تعطينا رأيك؟',
            appRatePromptMessage: '',
            feedbackPromptMessage: ''
        },
        en: {
            language: 'en',
            title: 'Would you mind rating %@?',
            message: '',
            cancelButtonLabel: 'No, Thanks',
            laterButtonLabel: 'Remind Me Later',
            rateButtonLabel: 'Rate It Now',
            yesButtonLabel: 'Yes',
            noButtonLabel: 'No',
            appRatePromptTitle: 'Do you like using %@?',
            feedbackPromptTitle: 'Would you like to give us feedback?',
            appRatePromptMessage: '',
            feedbackPromptMessage: ''
        },
        es: {
            language: 'es',
            title: '¿Te importaría calificar %@?',
            message: '',
            cancelButtonLabel: 'No, gracias',
            laterButtonLabel: 'Recuérdame más tarde',
            rateButtonLabel: 'Califícalo ahora',
            yesButtonLabel: 'Sí',
            noButtonLabel: 'No',
            appRatePromptTitle: '¿Te gusta usar %@?',
            feedbackPromptTitle: '¿Te gustaría darnos tu opinión?',
            appRatePromptMessage: '',
            feedbackPromptMessage: ''
        },
        it: {
            language: 'it',
            title: 'Ti dispiacerebbe valutare %@?',
            message: '',
            cancelButtonLabel: 'No grazie',
            laterButtonLabel: 'Ricordamelo più tardi',
            rateButtonLabel: 'Valutalo ora',
            yesButtonLabel: 'sì',
            noButtonLabel: 'No',
            appRatePromptTitle: 'Ti piace usare %@?',
            feedbackPromptTitle: 'Vorresti darci un feedback?',
            appRatePromptMessage: '',
            feedbackPromptMessage: ''
        },
        sw: {
            language: 'sw',
            title: 'Ungependa kuzingatia %@?',
            message: '',
            cancelButtonLabel: 'La, Shukrani',
            laterButtonLabel: 'Nikumbushe baadaye',
            rateButtonLabel: 'Kihesabu Hiyo Sasa',
            yesButtonLabel: 'Ndiyo',
            noButtonLabel: 'Hapana',
            appRatePromptTitle: 'Je, ungependa kutumia %@?',
            feedbackPromptTitle: 'Ungependa kutupa maoni?',
            appRatePromptMessage: '',
            feedbackPromptMessage: ''
        },
        zh: {
            language: 'zh',
            title: '你介意评分％@？',
            message: '',
            cancelButtonLabel: '不用了，谢谢',
            laterButtonLabel: '稍后提醒我',
            rateButtonLabel: '立即评分',
            yesButtonLabel: '是',
            noButtonLabel: '没有',
            appRatePromptTitle: '你喜欢使用％@吗？',
            feedbackPromptTitle: '你想给我们反馈意见吗？',
            appRatePromptMessage: '',
            feedbackPromptMessage: ''
        }
    };
    // cordova-plugin-apprate supports providing custom locales for the various dialog modals that appear
    // when prompting for an app review. The exception is the dialog modal responsible for accepting the
    // user's rating for the app, which is localized using the device's language setting. This means that
    // if the currentUser's pref_locale is different from the device's language setting or if there's no
    // custom locale config for the device's language setting, this dialog modal will be localized differently
    // from the rest of the modals, resulting in a jarring and unexpected UX. To help minimize the risk of
    // this happening, we've opted to use window.serverDeterminedLocale, which we've configured on Cordova
    // to be determined by the device's language setting and falling back to 'en' if necessary.
    window.AppRate.preferences.customLocale = appRateCustomLocales[window.serverDeterminedLocale] || appRateCustomLocales.en;

    // Capture any arguments passed to Android's LAUNCHER intent
    if (window.device.platform === 'Android') {
        getAndroidIntentArgs();
    }

}, false);


// Called when the application is resumed from a background state
document.addEventListener('resume', function() {
    // Similar to the call in `deviceready`, we want to capture any
    // arguments passed to Android's LAUNCHER intent when the application
    // is resumed from a background state.
    if (window.device.platform === 'Android') {
        getAndroidIntentArgs(true);
    }
}, false);

// This logic requires libraries that are part of the 'common' amalgamation, which doesn't
// get loaded until later on in the resource loading hierarchy (see hybrid/www/index.html),
// so we wait until the load event on the window has fired, at which point these libraries
// should be available for use.
window.addEventListener('load', function() {

    // iPads don't have a notch, but they do have a safe-area-inset-bottom set, so detect them specially.
    function isIpad() {
        return window.device.isVirtual ? window.navigator.appVersion.includes('iPad') : window.device.model.includes('iPad');
    }

    function isNonZeroPx(styles, propVal) {
        return styles.getPropertyValue(propVal) && styles.getPropertyValue(propVal) !== '0px';
    }

    var htmlElem = $('html');

    // Set the "lang" attribute for styling in the pre-login pages
    // See also translation_helper.js#refreshLangAndDir, which we do not have access to yet
    htmlElem.attr('lang', window.serverDeterminedLocale);

    // Determine notch support status based on computed test styles (see also: `notch.scss`)
    // NOTE: initial styling pass does not seem to register in iOS with `getComputedStyle()`
    // until AFTER an subsequent reflow has occurred. We don't seem to be able to reliably
    // force a reflow on-device, so we're going to defer / retry for a short period.
    var notchCheckCount = 0,
        notchTestElem = $('.notch-test'),
        notchCheckInt = setInterval(function() {

            var notchTestComputedStyles = window.getComputedStyle(notchTestElem[0]);

            if (!isIpad() && (isNonZeroPx(notchTestComputedStyles, 'padding-top') || isNonZeroPx(notchTestComputedStyles, 'padding-bottom'))) {
                htmlElem.addClass('notch');
                notchCheckCount = 5; // short circuit
            }

            // cleanup on threshold
            notchCheckCount++;
            if (notchCheckCount > 4) {
                clearInterval(notchCheckInt);
                notchTestElem.remove();
            }

        }, 200);

});


/**
 * @description Gets Android's LAUNCHER intent and dispatches an
 *  `androidApplicationResumedFromNotification` event if instructed to do so.
 * @param boolean dispatchEvent - Whether or not to dispatch the event
 */
function getAndroidIntentArgs(dispatchEvent) {

    // Capture launch arguments passed to Launcher Intent (Android only)
    try {
        // Android's `onMessageReceived` callback does not work as expected
        // while our Android application is in the background or closed. However,
        // we can access and inspect the LAUNCHER activity's launch arguments
        // which will contain any push notification data, if the application
        // was launched from interacting with a push notification.
        window.plugins.intentShim.getIntent(
            function(intent) {
                window.pushNotificationState = {
                    androidIntentArgs: intent.extras,
                    openedFromPushNotification: !!(
                        intent.extras &&
                        intent.extras['CIO-Delivery-ID'] &&
                        intent.extras['CIO-Delivery-Token']
                    )
                };

                if (window.pushNotificationState.openedFromPushNotification && dispatchEvent) {
                    // See also: remoteNotificationHelper#setupCallbacks
                    var event = new Event('androidApplicationResumedFromNotification');
                    window.document.dispatchEvent(event);
                }
            },
            function(error) {
                throw error;
            });
    } catch (err) {
        // noop - just swallow the error
    }

};