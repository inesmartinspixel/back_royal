# == Schema Information
#
# Table name: published_cohort_admission_rounds
#
#  cohort_id            :uuid
#  name                 :text
#  index                :bigint
#  program_type         :text
#  isolated_network     :boolean
#  application_deadline :datetime
#  decision_date        :datetime
#  cohort_start_date    :datetime
#  promoted             :boolean
#

require 'spec_helper'

describe AdmissionRound do

    fixtures :cohorts

    before(:each) do
        SafeCache.clear
    end

    describe "promoted" do

        before(:each) do

            allow_any_instance_of(Cohort).to receive(:valid?).and_return(true)
            cohort_attrs = Cohort.first.attributes
            cohort_attrs.delete("playlist_pack_ids")
            cohort_attrs.delete("application_deadline")
            cohort_attrs.delete("id")
            cohort_attrs.delete("enrollment_warning_days_offset")
            cohort_attrs.delete("enrollment_expulsion_days_offset")
            cohort_attrs.delete("stripe_plan_id")
            cohort_attrs.delete("stripe_plan_amount")

            now = Time.now

            @old = Cohort.create!(cohort_attrs.merge(name: 'old', start_date: now.add_dst_aware_offset(-3.days), program_type: 'test', admission_rounds: [
                {
                    decision_date_days_offset: 0,
                    application_deadline_days_offset: -1
                }
            ]))
            @application_deadline_passed = Cohort.create!(cohort_attrs.merge(name: 'app_deadline_passed', start_date: now.add_dst_aware_offset(1.day), program_type: 'test', admission_rounds: [
                {
                    decision_date_days_offset: 0,
                    application_deadline_days_offset: -2
                }
            ]))
            @current = Cohort.create!(cohort_attrs.merge(name: 'current', start_date: now.add_dst_aware_offset(6.days), program_type: 'test', admission_rounds: [
                # past
                {
                    decision_date_days_offset: 0,
                    application_deadline_days_offset: -8
                },
                # current
                {
                    decision_date_days_offset: 0,
                    application_deadline_days_offset: -4
                },
                # future
                {
                    decision_date_days_offset: 0,
                    application_deadline_days_offset: -2
                }
            ]))
            @future = Cohort.create!(cohort_attrs.merge(name: 'future', start_date: now.add_dst_aware_offset(10.days), program_type: 'test', admission_rounds: [
                {
                    decision_date_days_offset: 0,
                    application_deadline_days_offset: -2
                }
            ]))
            # another program type
            Cohort.create!(cohort_attrs.merge(name: 'other_mba', start_date: now.add_dst_aware_offset(6.days), program_type: 'mba', admission_rounds: [
                {
                    decision_date_days_offset: 0,
                    application_deadline_days_offset: -4
                }
            ]))
            [@old, @application_deadline_passed, @current, @future].map(&:publish!)
            RefreshMaterializedContentViews.refresh
        end

        it "should return the right one" do

            promoted = AdmissionRound.promoted('test')
            expect(promoted.cohort).to eq(@current)
            expect(promoted.application_deadline).to be_within(1).of(@current.start_date.add_dst_aware_offset(-4.days))

        end

        it "should cache after returning the right one" do
            expect(ContentViewsRefresh).to receive(:debounced_value).at_least(1).times.and_return(42)
            promoted = AdmissionRound.promoted('test')
            expect(promoted.cohort).to eq(@current)
            expect(promoted.application_deadline).to be_within(1).of(@current.start_date.add_dst_aware_offset(-4.days))

            # should cache the round
            expect(AdmissionRound).to receive(:promoted_with_cache).exactly(1).times.and_call_original
            expect(AdmissionRound).not_to receive(:promoted_without_cache).and_call_original
            expect(SafeCache).not_to receive(:delete).and_call_original
            expect(SafeCache).to receive(:fetch).with("promoted_admission_round/test/42").and_call_original
            promoted = AdmissionRound.promoted('test')
            expect(promoted.cohort).to eq(@current)
            expect(promoted.application_deadline).to be_within(1).of(@current.start_date.add_dst_aware_offset(-4.days))
        end

        it "should bust cache if time moves on" do
            promoted = AdmissionRound.promoted('test')
            expect(promoted.cohort).to eq(@current)
            expect(promoted.application_deadline).to be_within(1).of(@current.start_date.add_dst_aware_offset(-4.days))

            # mock now to be further in the future; ensure it busts the cache
            allow(AdmissionRound).to receive(:now).and_return(Time.now.add_dst_aware_offset(2.days))
            expect(AdmissionRound).to receive(:promoted_with_cache).exactly(2).times.and_call_original
            expect(AdmissionRound).to receive(:promoted_without_cache).exactly(1).times.and_call_original
            expect(SafeCache).to receive(:delete).and_call_original
            promoted = AdmissionRound.promoted('test')
            # Note that because we're not mocking time in the DB, the view is still going to return the
            # same admission round as before; that's why we're not looking for an actual future admission
            # round here in the spec
        end
    end

    describe "daylight savings time" do

        it "should work across daylight savings time" do
            allow_any_instance_of(Cohort).to receive(:valid?).and_return(true)
            cohort_attrs = Cohort.first.attributes
            cohort_attrs.delete("playlist_pack_ids")
            cohort_attrs.delete("application_deadline")
            cohort_attrs.delete("id")
            cohort_attrs.delete("enrollment_warning_days_offset")
            cohort_attrs.delete("enrollment_expulsion_days_offset")
            cohort_attrs.delete("stripe_plan_id")
            cohort_attrs.delete("stripe_plan_amount")

            cohort = Cohort.create!(cohort_attrs.merge(start_date: Time.parse('2018/03/15 00:00 UTC'), program_type: 'test', admission_rounds: [
                {
                    decision_date_days_offset: 0,
                    application_deadline_days_offset: -10
                }
            ]))
            cohort.publish!
            RefreshMaterializedContentViews.refresh

            wrong_time = cohort.start_date.utc - 10.days
            correct_time = cohort.start_date.add_dst_aware_offset(-10.days)
            expect(wrong_time).not_to eq(correct_time) # sanity check

            expect(AdmissionRound.find_by_cohort_id(cohort.id).application_deadline).to eq(correct_time)
        end

    end

    describe "::promoted_rounds_event_attributes" do

        it "should retreive event_attributes for AdmissionRound::promoted_admission_rounds" do
            mock_mba_promoted_admission_round = double('AdmissionRound', {program_type: 'mba'})
            mock_emba_promoted_admission_round = double('AdmissionRound', {program_type: 'emba'})
            mock_promoted_admission_rounds = [mock_mba_promoted_admission_round, mock_emba_promoted_admission_round]
            expect(AdmissionRound).to receive(:promoted_admission_rounds).and_return(mock_promoted_admission_rounds)

            timezone = nil
            mock_mba_round_event_attrs = {foo: 'foo'}
            mock_emba_round_event_attrs = {bar: 'bar'}
            expect(mock_mba_promoted_admission_round).to receive(:event_attributes).with(timezone).and_return(mock_mba_round_event_attrs)
            expect(mock_emba_promoted_admission_round).to receive(:event_attributes).with(timezone).and_return(mock_emba_round_event_attrs)

            result = AdmissionRound.promoted_rounds_event_attributes(timezone)
            expect(result).to eq({
                promoted_admission_rounds: {
                    mba: mock_mba_round_event_attrs,
                    emba: mock_emba_round_event_attrs
                }
            })
        end

        it "should retreive event_attributes for passed in promoted_admission_rounds" do
            mock_mba_promoted_admission_round = double('AdmissionRound', {program_type: 'mba'})
            mock_emba_promoted_admission_round = double('AdmissionRound', {program_type: 'emba'})
            mock_promoted_admission_rounds = [mock_mba_promoted_admission_round, mock_emba_promoted_admission_round]
            expect(AdmissionRound).not_to receive(:promoted_admission_rounds)

            timezone = nil
            mock_mba_round_event_attrs = {foo: 'foo'}
            mock_emba_round_event_attrs = {bar: 'bar'}
            expect(mock_mba_promoted_admission_round).to receive(:event_attributes).with(timezone).and_return(mock_mba_round_event_attrs)
            expect(mock_emba_promoted_admission_round).to receive(:event_attributes).with(timezone).and_return(mock_emba_round_event_attrs)

            result = AdmissionRound.promoted_rounds_event_attributes(timezone, mock_promoted_admission_rounds)
            expect(result).to eq({
                promoted_admission_rounds: {
                    mba: mock_mba_round_event_attrs,
                    emba: mock_emba_round_event_attrs
                }
            })
        end

        it "should retreive event_attributes for promoted_admission_rounds with timezone passed in" do
            mock_mba_promoted_admission_round = double('AdmissionRound', {program_type: 'mba'})
            mock_emba_promoted_admission_round = double('AdmissionRound', {program_type: 'emba'})
            mock_promoted_admission_rounds = [mock_mba_promoted_admission_round, mock_emba_promoted_admission_round]
            expect(AdmissionRound).to receive(:promoted_admission_rounds).and_return(mock_promoted_admission_rounds)

            timezone = 'Europe/Warsaw'
            mock_mba_round_event_attrs = {foo: 'foo'}
            mock_emba_round_event_attrs = {bar: 'bar'}
            expect(mock_mba_promoted_admission_round).to receive(:event_attributes).with(timezone).and_return(mock_mba_round_event_attrs)
            expect(mock_emba_promoted_admission_round).to receive(:event_attributes).with(timezone).and_return(mock_emba_round_event_attrs)

            result = AdmissionRound.promoted_rounds_event_attributes(timezone)
            expect(result).to eq({
                promoted_admission_rounds: {
                    mba: mock_mba_round_event_attrs,
                    emba: mock_emba_round_event_attrs
                }
            })
        end
    end

    describe "event_attributes" do

        it "should work" do
            promoted_mba_round = AdmissionRound.promoted('mba')

            attrs = promoted_mba_round.event_attributes(nil)
            {
                application_deadline: promoted_mba_round.application_deadline.relative_to_threshold.to_timestamp,
                cohort_start_date: promoted_mba_round.published_cohort.start_date.to_timestamp,
                cohort_title: promoted_mba_round.published_cohort.title
            }.each do |key, expected_val|
                expect(attrs[key]).to eq(expected_val), "unexpected value for #{key.inspect}"
            end
        end

        it "should indicate if this is the last round in the cycle" do
            cohort = cohorts(:published_mba_cohort)
            expect(cohort.admission_rounds.size).to eq(2)
            timezone = nil

            expect(AdmissionRound.where(cohort_id: cohort.id, index: 1).first.event_attributes(timezone)[:last_round_in_cycle]).to be(false)
            expect(AdmissionRound.where(cohort_id: cohort.id, index: 2).first.event_attributes(timezone)[:last_round_in_cycle]).to be(true)
        end

        it "should return application_deadline relative_to_threshold and timezone when timezone is passed in" do
            now = Time.now
            timezone = 'Europe/Warsaw'
            promoted_mba_round = AdmissionRound.promoted('mba')
            expect(promoted_mba_round.application_deadline).to receive(:relative_to_threshold).with(timezone: timezone).and_return(now)
            event_attrs = promoted_mba_round.event_attributes(timezone)
            expect(event_attrs[:application_deadline]).to eq(now.to_timestamp)
        end
    end

    describe "midpoint" do

        it "should work" do
            previous_round = nil
            rounds = AdmissionRound.where(program_type: 'mba').reorder('cohort_start_date', 'application_deadline').limit(2)

            expect(rounds.size).to eq(2)
            rounds.each do |round|
                if previous_round
                    expect(round.midpoint).to eq(previous_round.application_deadline + (round.application_deadline - previous_round.application_deadline) / 2)
                else
                    expect(round.midpoint).to be_nil
                end
                previous_round = round
            end
        end

    end

    describe "start_of_cycle" do
        attr_reader :cohort, :admission_round

        before(:each) do
            @cohort = cohorts(:published_mba_cohort)
            @admission_round = AdmissionRound.find_by_cohort_id(cohort.id)
        end

        it "should work when there is a previous cycle" do
            previous_cohort = Cohort.new(cohort.attributes.except("id", "playlist_pack_ids", "application_deadline", "enrollment_warning_days_offset", "enrollment_expulsion_days_offset", "stripe_plan_id", "stripe_plan_amount").merge(
                "start_date" => cohort.start_date - 1.year
            ))
            previous_cohort.publish!
            RefreshMaterializedContentViews.refresh
            expect(AdmissionRound.find_by_cohort_id(cohort.id).start_of_cycle).to eq(previous_admission_round.application_deadline)
        end

        it "should be nil when there is no previous cycle" do
            expect(previous_admission_round).to be_nil
            expect(admission_round.start_of_cycle).to be_nil
        end

        def previous_admission_round
            AdmissionRound.where("cohort_start_date < ?", cohort.start_date)
                .where(program_type: cohort.program_type)
                .reorder("cohort_start_date desc, application_deadline desc")
                .first
        end

    end

end
