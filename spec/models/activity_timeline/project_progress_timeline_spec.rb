require 'spec_helper'

describe ActivityTimeline::ProjectProgressTimeline do

    fixtures(:users)

    describe "project_progress_modification_events" do

        attr_accessor :admin, :progress

        before(:each) do
            @admin = users(:admin)
            @progress = ProjectProgress.create_or_update!(users(:learner), @admin, {
                requirement_identifier: "learner_project",
                score: 1,
                status: 'submitted'
            })
        end

        it "should include insert events" do
            events = get_events(:project_progress_modification_events, progress.id, 'modification')
            expect(events.count).to eq(2)
            events.each do |event|
                expect(event.category).to eq('project')
                expect(event.time).to be_within(5.seconds).of(Time.now)

                # somehow on ci these can end up a couple microseconds off
                expect(event.secondary_sort).to be_within(0.001).of(progress.updated_at.to_f)
                expect(event.labels).to eq(['Learner project'])
            end

            expect(events.map(&:changes)).to match_array([[{
                attr: :score,
                from: nil,
                to: 1
            }], [{
                attr: :status,
                from: nil,
                to: 'submitted'
            }]])
        end

        it "should include update events" do
            progress.update(score: 5)

            event = get_events(:project_progress_modification_events, progress.id, 'modification').last

            expect(event.category).to eq('project')
            expect(event.time).to be_within(5.seconds).of(Time.now)

            # somehow on ci these can end up a couple microseconds off
            expect(event.secondary_sort).to be_within(0.001).of(progress.updated_at.to_f)
            expect(event.labels).to eq(['Learner project'])
            expect(event.changes).to eq([{
                attr: :score,
                from: 1,
                to: 5
            }])
        end

        it "should include editor info" do
            EditorTracking.transaction(admin) do
                progress.update(id_verified: true)
            end

            event = get_events(:project_progress_modification_events, progress.id, 'modification').last

            expect(event.editor_id).to eq(admin.id)
            expect(event.editor_name).to eq(admin.name)
        end
    end

    def get_events(meth, progress_id, event)
        instance = ActivityTimeline::ProjectProgressTimeline.new(progress_id).preload

        # once preloaded, there should be no more requests necessary to create the events
        expect(ActiveRecord::Base.connection).not_to receive(:exec_query).and_call_original
        instance.send(meth).select do |timeline_event|
            timeline_event.event == event
        end.sort_by(&:time)
    end


end