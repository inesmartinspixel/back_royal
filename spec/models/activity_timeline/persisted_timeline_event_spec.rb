# == Schema Information
#
# Table name: persisted_timeline_events
#
#  id             :uuid             not null, primary key
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  time           :datetime         not null
#  event          :text             not null
#  secondary_sort :float
#  text           :text
#  subtext        :text
#  editor_id      :uuid
#  editor_name    :text
#  labels         :text             default([]), not null, is an Array
#  category       :text             not null
#

require 'spec_helper'

describe ActivityTimeline::PersistedTimelineEvent do

    describe "create_from_hash!" do

        it "should set all expected attrs" do
            editor_id = SecureRandom.uuid
            user = User.first
            event = ActivityTimeline::PersistedTimelineEvent.create_from_hash!(
                {
                    time: 42,
                    event: 'note',
                    category: 'category',
                    text: 'some text',
                    subtext: 'some more',
                    editor_id: editor_id,
                    editor_name: 'Ed. E. Ter'
                },
                {
                    object: 'user',
                    object_id: user.id
                }
            )
            expect(event.time).to eq(Time.at(42))
            expect(event.event).to eq('note')
            expect(event.text).to eq('some text')
            expect(event.subtext).to eq('some more')
            expect(event.editor_id).to eq(editor_id)
            expect(event.editor_name).to eq('Ed. E. Ter')
        end

        it "should work with a user_id passed in as the object_id" do
            editor_id = SecureRandom.uuid
            user = User.first
            event = ActivityTimeline::PersistedTimelineEvent.create_from_hash!(
                {
                    time: 42,
                    event: 'note',
                    category: 'category'
                },
                {
                    object: 'user',
                    object_id: user.id
                }
            )
            expect(user.persisted_timeline_events).to include(event)
        end

        it "should work with a user passed in" do
            editor_id = SecureRandom.uuid
            user = User.first
            event = ActivityTimeline::PersistedTimelineEvent.create_from_hash!(
                {
                    time: 42,
                    event: 'note',
                    category: 'category'
                },
                {
                    user: user
                }
            )
            expect(user.persisted_timeline_events).to include(event)
        end
    end
end
