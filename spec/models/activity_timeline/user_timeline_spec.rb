require 'spec_helper'

describe ActivityTimeline::UserTimeline do

    fixtures(:users)

    describe "cohort_application_events" do

        it "should include event for creation" do
            version = CohortApplication::Version.where(operation: 'I').first
            version.cohort # preload so we don't mess up the eager loading assertion
            event = get_events(:cohort_application_events, version.user_id, 'creation').first

            expect(event.category).to eq('cohort_application')
            expect(event.time).to be_within(0.001).of(version.version_created_at)
            expect(event.secondary_sort).to be_within(0.001).of(version.updated_at.to_f)
            expect(event.labels).to eq([version.cohort.name, version.status])

            expect(version.version_editor_name).not_to be_nil # sanity check
            expect(event.editor_name).to eq(version.version_editor_name)
        end

        it "should include events for deletion" do
            cohort_application = CohortApplication.first
            cohort_application.cohort # preload so we don't mess up the eager loading assertion
            editor = User.first
            EditorTracking.transaction(editor) do
                cohort_application.destroy
            end
            event = get_events(:cohort_application_events, cohort_application.user_id, 'deletion').first

            expect(event.category).to eq('cohort_application')
            expect(event.time).to be_within(5.seconds).of(Time.now)
            expect(event.secondary_sort).to eq(0)
            expect(event.labels).to eq([cohort_application.cohort.name, cohort_application.status])
            expect(event.editor_name).to eq(editor.name)
            expect(event.editor_id).to eq(editor.id)

        end

        it "should include events for modifications" do
            cohort_application = CohortApplication.where(status: 'pending', admissions_decision: nil).first
            old_cohort = cohort_application.cohort
            new_cohort = Cohort.where.not(id: cohort_application.cohort_id).first
            allow(cohort_application).to receive(:valid?).and_return(true)
            editor = User.first
            EditorTracking.transaction(editor) do
                cohort_application.update(
                    cohort_id: new_cohort.id,
                    status: 'accepted',
                    admissions_decision: 'new decision',
                    registered: true,
                    graduation_status: 'graduated',
                    locked_due_to_past_due_payment: true
                )
            end
            events = get_events(:cohort_application_events, cohort_application.user_id, 'modification').last(6)

            0.upto(5) do |i|
                expect(events[i].category).to eq('cohort_application')
                expect(events[i].time).to be_within(5.seconds).of(Time.now)

                # somehow on ci these can end up a couple microseconds off
                expect(events[i].secondary_sort).to be_within(0.001).of(cohort_application.updated_at.to_f)
                expect(events[i].labels).to eq([new_cohort.name, 'accepted'])
                expect(events[i].editor_name).to eq(editor.name)
                expect(events[i].editor_id).to eq(editor.id)
            end

            expect(events[0].changes).to eq([{
                attr: :cohort,
                from: old_cohort.name,
                to: new_cohort.name
            }])
            expect(events[1].changes).to eq([{
                attr: :status,
                from: 'pending',
                to: 'accepted'
            }])
            expect(events[2].changes).to eq([{
                attr: :admissions_decision,
                from: nil,
                to: 'new decision'
            }])
            expect(events[3].changes).to eq([{
                attr: :graduation_status,
                from: 'pending',
                to: 'graduated'
            }])
            expect(events[4].changes).to eq([{
                attr: :registered,
                from: false,
                to: true
            }])
            expect(events[5].changes).to eq([{
                attr: :locked_due_to_past_due_payment,
                from: false,
                to: true
            }])
        end

        describe "with deleted cohort" do

            attr_accessor :cohort_application

            before(:each) do
                self.cohort_application = users(:pending_mba_cohort_user).cohort_applications[0]
                self.cohort_application.update(
                    status: 'rejected'
                )
            end

            it "should use versions table when cohort is nil" do
                deleted_cohort_id = SecureRandom.uuid
                CohortApplication::Version.where(id: cohort_application.id).update_all(:cohort_id => deleted_cohort_id)
                Cohort::Version.where(id: cohort_application.cohort_id).update_all(:id => deleted_cohort_id, :name => 'foobar')
                event = get_events(:cohort_application_events, cohort_application.user_id, 'modification').last
                expect(event.labels).to eq(['foobar', 'rejected'])
            end

            it "should defer to \'COHORT DELETED\' if no versions records" do
                CohortApplication::Version.where(id: cohort_application.id).update_all(:cohort_id => SecureRandom.uuid)
                event = get_events(:cohort_application_events, cohort_application.user_id, 'modification').last
                expect(event.labels).to eq(['COHORT DELETED', 'rejected'])
            end

        end

    end

    describe "enrollment_modification_events" do
        def assert(attr, from, to, updated_at)
            expect(@event.category).to eq('enrollment')
            expect(@event.time).to be_within(5.seconds).of(Time.now)
            expect(@event.secondary_sort).to be_within(0.001).of(updated_at.to_f)
            expect(@event.labels).to eq([])
            expect(@event.changes).to eq([{
                attr: attr,
                from: from,
                to: to
            }])
            expect(@event.editor_name).to eq(@editor.name)
            expect(@event.editor_id).to eq(@editor.id)
        end

        it "should include event when identity_verified changes" do
            user = User.where(identity_verified: false).first
            @editor = User.first
            EditorTracking.transaction(@editor) do
                user.update(identity_verified: true)
            end
            @event = get_events(:enrollment_modification_events, user.id, 'modification').last
            assert(:identity_verified, false, true, user.updated_at)
        end

        it "should include event when transcripts_verified changes" do
            user = User.where(transcripts_verified: false).first
            @editor = User.first
            EditorTracking.transaction(@editor) do
                user.update(transcripts_verified: true)
            end
            @event = get_events(:enrollment_modification_events, user.id, 'modification').last
            assert(:transcripts_verified, false, true, user.updated_at)
        end

        it "should include event when english_language_proficiency_documents_approved changes" do
            user = User.where(english_language_proficiency_documents_approved: false).first
            @editor = User.first
            EditorTracking.transaction(@editor) do
                user.update(english_language_proficiency_documents_approved: true)
            end
            @event = get_events(:enrollment_modification_events, user.id, 'modification').last
            assert(:english_proficiency_approved, false, true, user.updated_at)
        end

        it "should include event when enrollment agreement is signed" do
            cohort_application = CohortApplication.left_outer_joins(:user => :signable_documents).where("signable_documents.id is null").first
            user = cohort_application.user
            @editor = User.first
            signable_document = SignableDocument.create!(
                user_id: user.id,
                document_type: 'enrollment_agreement',
                metadata: {
                    program_type: cohort_application.program_type,
                    cohort_id: cohort_application.cohort_id
                },
                signed_at: Time.now
            )
            cohort_application.reload
            event = get_events(:enrollment_agreement_events, user.id, 'performed_action').last

            expect(event.category).to eq('enrollment')
            expect(event.time).to be_within(0.001).of(signable_document.signed_at)
            expect(event.event).to eq('performed_action')
            expect(event.text).to eq('Signed enrollment agreement')
        end

        it "should not include an event for an unsigned enrollment agreement" do
            cohort_application = CohortApplication.left_outer_joins(:user => :signable_documents).where("signable_documents.id is null").first
            user = cohort_application.user
            @editor = User.first
            signable_document = SignableDocument.create!(
                user_id: user.id,
                document_type: 'enrollment_agreement',
                metadata: {
                    program_type: cohort_application.program_type,
                    cohort_id: cohort_application.cohort_id
                },
                signed_at: nil
            )
            cohort_application.reload
            event = get_events(:enrollment_agreement_events, user.id, 'performed_action').last

            expect(event).to be_nil

        end

    end

    describe "student_network_modification_events" do

        it "should include event when network privacy setting changes" do
            user = User.where(pref_student_network_privacy: 'full').first
            editor = User.first
            EditorTracking.transaction(editor) do
                user.update(pref_student_network_privacy: 'anonymous')
            end

            event = get_events(:student_network_modification_events, user.id, 'modification').last

            expect(event.category).to eq('student_network')
            expect(event.time).to be_within(5.seconds).of(Time.now)
            expect(event.secondary_sort).to be_within(0.001).of(user.updated_at.to_f)
            expect(event.labels).to eq([])
            expect(event.changes).to eq([{
                attr: :privacy,
                from: 'full',
                to: 'anonymous'
            }])
            expect(event.editor_name).to eq(editor.name)
            expect(event.editor_id).to eq(editor.id)

        end

    end

    describe "career_profile_modification_events" do

        it "should include event when interested_in_joining_new_company changes" do
            career_profile = CareerProfile.where(interested_in_joining_new_company: 'not_interested').first
            user = career_profile.user
            career_profile.update(interested_in_joining_new_company: 'interested')

            event = get_events(:career_profile_modification_events, user.id, 'modification').last

            expect(event.category).to eq('career_profile')
            expect(event.time).to be_within(5.seconds).of(Time.now)

            # somehow on ci these can end up a couple microseconds off
            expect(event.secondary_sort).to be_within(0.001).of(career_profile.updated_at.to_f)
            expect(event.labels).to eq([])
            expect(event.changes).to eq([{
                attr: :status,
                from: 'not_interested',
                to: 'interested'
            }])

        end

        it "should include event when can_edit_career_profile changes" do
            user = User.where(can_edit_career_profile: false).first
            user.update(can_edit_career_profile: true)

            event = get_events(:career_profile_modification_events, user.id, 'modification').last

            expect(event.category).to eq('career_profile')
            expect(event.time).to be_within(5.seconds).of(Time.now)

            # somehow on ci these can end up a couple microseconds off
            expect(event.secondary_sort).to be_within(0.001).of(user.updated_at.to_f)
            expect(event.labels).to eq([])
            expect(event.changes).to eq([{
                attr: :has_career_network_access,
                from: false,
                to: true
            }])

        end

        it "should include editor info" do
            user = User.where(can_edit_career_profile: false).first
            editor = User.first
            EditorTracking.transaction(editor) do
                user.update(can_edit_career_profile: true)
            end

            event = get_events(:career_profile_modification_events, user.id, 'modification').last

            expect(event.editor_id).to eq(editor.id)
            expect(event.editor_name).to eq(editor.name)
        end
    end

    describe "id_verification_events" do
        before(:each) do
            @user = User.first
            @user.user_id_verifications.delete_all
        end

        describe "successful user_id_verification events" do
            it "should properly include when verified_by_idology" do
                idology_verification = IdologyVerification.create!(
                    user_id: @user.id,
                    idology_id_number: 12345,
                    scan_capture_url: 'scan_capture_url'
                )
                verification = UserIdVerification.create!(
                    cohort_id: Cohort.first.id,
                    id_verification_period_index: 0,
                    user_id: @user.id,
                    verification_method: 'verified_by_idology',
                    idology_verification_id: idology_verification.id,
                    verified_at: Time.now
                )

                event = get_events(:id_verification_events, @user.id, 'performed_action').last

                expect(event.category).to eq('enrollment')
                expect(event.time).to be_within(0.001).of(verification.verified_at)
                expect(event.secondary_sort).to be_within(0.001).of(verification.created_at.to_f)
                expect(event.event).to eq('performed_action')
                expect(event.text).to eq('ID Verified')
                expect(event.editor_id).to be_nil
                expect(event.editor_name).to eq('IDology')
            end

            it "should properly include when verified_by_admin" do
                admin = User.where.not(id: @user.id).first
                verification = UserIdVerification.create!(
                    cohort_id: Cohort.first.id,
                    id_verification_period_index: 0,
                    user_id: @user.id,
                    verification_method: 'verified_by_admin',
                    verified_at: Time.now,
                    verifier_id: admin.id,
                    verifier_name: admin.name
                )

                event = get_events(:id_verification_events, @user.id, 'performed_action').last

                expect(event.category).to eq('enrollment')
                expect(event.time).to be_within(0.001).of(verification.verified_at)
                expect(event.secondary_sort).to be_within(0.001).of(verification.created_at.to_f)
                expect(event.event).to eq('performed_action')
                expect(event.text).to eq('ID Verified')
                expect(event.editor_id).to eq(admin.id)
                expect(event.editor_name).to eq(admin.name)
            end

            it "should properly include when waived_due_to_late_enrollment" do
                verification = UserIdVerification.create!(
                    cohort_id: Cohort.first.id,
                    id_verification_period_index: 0,
                    user_id: @user.id,
                    verification_method: 'waived_due_to_late_enrollment',
                    verified_at: Time.now
                )

                event = get_events(:id_verification_events, @user.id, 'performed_action').last

                expect(event.category).to eq('enrollment')
                expect(event.time).to be_within(0.001).of(verification.verified_at)
                expect(event.secondary_sort).to be_within(0.001).of(verification.created_at.to_f)
                expect(event.event).to eq('performed_action')
                expect(event.text).to eq('ID Verification Waived')
                expect(event.editor_id).to be_nil
                expect(event.editor_name).to be_nil
            end
        end

        it "should include failed idology_verification events" do
            failed_idology_verification = IdologyVerification.create!(
                user_id: @user.id,
                idology_id_number: 12345,
                scan_capture_url: 'scan_capture_url',
                verified: false
            )
            event = get_events(:id_verification_events, @user.id, 'performed_action').last
            expect(event.category).to eq('enrollment')
            expect(event.time).to be_within(0.001).of(failed_idology_verification.created_at)
            expect(event.secondary_sort).to be_within(0.001).of(failed_idology_verification.created_at.to_f)
            expect(event.event).to eq('performed_action')
            expect(event.text).to eq('ID Verification Failed')
            expect(event.editor_name).to eq('IDology')
        end
    end

    describe "project_progress_modification_events" do

        attr_accessor :learner, :admin, :progress

        before(:each) do
            @learner = users(:learner)
            @admin = users(:admin)
            @progress = ProjectProgress.create_or_update!(@learner, @admin, {
                requirement_identifier: "learner_project",
                score: 1
            })
        end

        it "should include change events" do
            progress.update(score: 5)

            event = get_events(:project_progress_modification_events, learner.id, 'modification').last

            expect(event.category).to eq('project')
            expect(event.time).to be_within(5.seconds).of(Time.now)

            # somehow on ci these can end up a couple microseconds off
            expect(event.secondary_sort).to be_within(0.001).of(progress.updated_at.to_f)
            expect(event.labels).to eq(['Learner project'])
            expect(event.changes).to eq([{
                attr: :score,
                from: 1,
                to: 5
            }])
        end

        it "should include editor info" do
            EditorTracking.transaction(admin) do
                progress.update(id_verified: true)
            end

            event = get_events(:project_progress_modification_events, learner.id, 'modification').last

            expect(event.editor_id).to eq(admin.id)
            expect(event.editor_name).to eq(admin.name)
        end
    end

    def get_events(meth, user_id, event)
        instance = ActivityTimeline::UserTimeline.new(user_id).preload

        # once preloaded, there should be no more requests necessary to create the events
        expect(ActiveRecord::Base.connection).not_to receive(:exec_query).and_call_original
        instance.send(meth).select do |timeline_event|
            timeline_event.event == event
        end.sort_by(&:time)
    end
end