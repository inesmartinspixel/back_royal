require 'spec_helper'

describe Airtable::Application do

    before(:each) do
        @cohort_application ||= CohortApplication.first
        @cohort_application.update_column(:airtable_record_id, "rec123")
    end

    it "should have syncable" do
        expect(Airtable::Application.singleton_class.included_modules.include?(Airtable::Syncable)).to be(true)
        expect(Airtable::Application.singleton_class.method_defined?(:database_id_column)).to be(true)
        expect(Airtable::Application.singleton_class.method_defined?(:get_airtable_hash)).to be(true)
    end

    describe "klass_for_base_key" do

        before(:each) do
            @original_app_env_name = ENV['APP_ENV_NAME']
        end

        after(:each) do
            ENV['APP_ENV_NAME'] = @original_app_env_name
        end


        it "should raise if called with unsupported airtable_base_key" do
            expect{
                Airtable::Application.klass_for_base_key("NOT_AN_AIRTABLE_BASE_KEY")
            }.to raise_error(RuntimeError)
        end

        it "should raise if no corresponding ENV variable found in non-staging environments" do
            ENV['APP_ENV_NAME'] = 'production'
            ENV['AIRTABLE_BASE_KEY_LOCAL'] = nil
            expect{
                Airtable::Application.klass_for_base_key("AIRTABLE_BASE_KEY_LOCAL")
            }.to raise_error(RuntimeError)
        end

        it "should not raise if no corresponding ENV variable found in staging environment" do
            ENV['APP_ENV_NAME'] = 'staging'
            ENV['AIRTABLE_BASE_KEY_LOCAL'] = nil
            expect{
                Airtable::Application.klass_for_base_key("AIRTABLE_BASE_KEY_LOCAL")
            }.not_to raise_error
        end

        describe "with passed in airtable_base_key" do

            before(:each) do
                ENV['AIRTABLE_BASE_KEY_LIVE'] = '12345'
                ENV['AIRTABLE_BASE_KEY_ARCHIVE'] = '54321'
                @airtable_class = Airtable::Application
            end

            it "should return self and not subclass if 'AIRTABLE_BASE_KEY_LIVE' is passed in" do
                expect(@airtable_class).not_to receive(:create_subclass)
                klass = @airtable_class.klass_for_base_key("AIRTABLE_BASE_KEY_LIVE")
                expect(klass).to be(@airtable_class)
            end

            it "should create_subclass Airtable::Application with appropriate class variables when 'AIRTABLE_BASE_KEY_LIVE' is not passed in" do
                # Mock out the subclass so we don't alter state
                mock_subclass = OpenStruct.new({base_key: nil, table_name: nil})
                expect(@airtable_class).to receive(:create_subclass).and_return(mock_subclass)
                klass = @airtable_class.klass_for_base_key('AIRTABLE_BASE_KEY_ARCHIVE')

                # assert new subclass is created
                expect(klass).to be(mock_subclass)
                # assert appropriate class variables are set
                expect(klass.base_key).to eq('54321')
                expect(klass.table_name).to eq(Airtable::Application.table_name)
            end

            after(:each) do
                ENV['AIRTABLE_BASE_KEY_LIVE'] = nil
                ENV['AIRTABLE_BASE_KEY_ARCHIVE'] = nil
            end

        end

    end

    describe "fix_decision" do
        it "should work" do
            mock_app = OpenStruct.new({
                "Decision": "Reject for No Interview",
                "Dev - Synced Decision": "Reject for No Interview"
            })
            expect(Airtable::Application).to receive(:find_by_id_with_fallback).and_return(mock_app)
            expect(mock_app).to receive(:save)
            airtable_hash = Airtable::Application.fix_decision(anything, "Reject")
            expect(airtable_hash["Decision"]).to eq("Reject")
            expect(airtable_hash["Dev - Synced Decision"]).to eq("Reject")
        end
    end

    describe "get_airtable_hash" do
        it "should set the 'Decision' column if admissions_decision = 'manual_admin_decision'" do
            instance = CohortApplication.first
            instance.update!(admissions_decision: "manual_admin_decision")
            hash = Airtable::Application.get_airtable_hash(instance)
            expect(hash["Decision"]).to eq("manual_admin_decision")
        end

        it "should sync nil values back to Airtable" do
            instance = CohortApplication.where(admissions_decision: nil).first
            hash = Airtable::Application.get_airtable_hash(instance)
            expect(hash.key?("Dev - Synced Decision")).to eq(true)
            expect(hash["Dev - Synced Decision"]).to be_nil
        end

        describe "education" do
            it "should link to found education rows" do
                instance = CohortApplication.first
                instance.career_profile.education_experiences = [CareerProfile::EducationExperience.first]
                expect(Airtable::University).to receive(:find_by_name).and_return(OpenStruct.new({
                    id: "foo"
                }))
                hash = Airtable::Application.get_airtable_hash(instance)
                expect(hash["1 University/College"]).to eq(["foo"])
            end

            it "should not link if no education experiences" do
                instance = CohortApplication.first
                instance.career_profile.education_experiences = []
                hash = Airtable::Application.get_airtable_hash(instance)
                expect(hash["1 University/College"]).to eq(nil)
            end

            it "should not link if no education rows found" do
                instance = CohortApplication.first
                instance.career_profile.education_experiences = [CareerProfile::EducationExperience.first]
                expect(Airtable::University).to receive(:find_by_name).and_return(nil)
                hash = Airtable::Application.get_airtable_hash(instance)
                expect(hash["1 University/College"]).to eq(nil)
            end

            it "should send plaintext" do
                instance = CohortApplication.first
                experience = CareerProfile::EducationExperience.first
                instance.career_profile.education_experiences = [experience]
                expect(Airtable::University).to receive(:find_by_name).and_return(nil)
                hash = Airtable::Application.get_airtable_hash(instance)
                expect(hash["1 University/College name"]).to eq(experience.educational_organization.text)
            end

            it "should set plaintext to nil if no record" do
                instance = CohortApplication.first
                instance.career_profile.education_experiences = []
                hash = Airtable::Application.get_airtable_hash(instance)
                expect(hash["1 University/College name"]).to be_nil
            end
        end

        describe "work" do
            it "should link to found work rows" do
                instance = CohortApplication.first
                instance.career_profile.work_experiences = [CareerProfile::WorkExperience.first]
                expect(Airtable::Company).to receive(:find_by_name).and_return(OpenStruct.new({
                    id: "foo"
                }))
                hash = Airtable::Application.get_airtable_hash(instance)
                expect(hash["1 Company or organization"]).to eq(["foo"])
            end

            it "should not link if no work experiences" do
                instance = CohortApplication.first
                instance.career_profile.work_experiences = []
                hash = Airtable::Application.get_airtable_hash(instance)
                expect(hash["1 Company or organization"]).to eq(nil)
            end

            it "should not link if no work rows found" do
                instance = CohortApplication.first
                instance.career_profile.work_experiences = [CareerProfile::WorkExperience.first]
                expect(Airtable::Company).to receive(:find_by_name).and_return(nil)
                hash = Airtable::Application.get_airtable_hash(instance)
                expect(hash["1 Company or organization"]).to eq(nil)
            end

            it "should send plaintext" do
                instance = CohortApplication.first
                experience = CareerProfile::WorkExperience.first
                instance.career_profile.work_experiences = [experience]
                expect(Airtable::Company).to receive(:find_by_name).and_return(nil)
                hash = Airtable::Application.get_airtable_hash(instance)
                expect(hash["1 Company or organization name"]).to eq(experience.professional_organization.text)
            end

            it "should set plaintext to nil if no record" do
                instance = CohortApplication.first
                instance.career_profile.work_experiences = []
                hash = Airtable::Application.get_airtable_hash(instance)
                expect(hash["1 Company or organization name"]).to be_nil
            end
        end

        describe "city" do
            it "should link to found row" do
                instance = CohortApplication.first
                instance.user.update_column(:city, "Fooville")
                instance.reload
                expect(Airtable::City).to receive(:find_by_name).and_return(OpenStruct.new({
                    id: "foo"
                }))
                hash = Airtable::Application.get_airtable_hash(instance)
                expect(hash["City"]).to eq(["foo"])
            end

            it "should not link if no city" do
                instance = CohortApplication.first
                instance.user.update_column(:city, nil)
                instance.reload
                hash = Airtable::Application.get_airtable_hash(instance)
                expect(hash["City"]).to eq(nil)
            end

            it "should not link if no rows found in Airtable" do
                instance = CohortApplication.first
                instance.user.update_column(:city, "Fooville")
                instance.reload
                expect(Airtable::City).to receive(:find_by_name).and_return(nil)
                hash = Airtable::Application.get_airtable_hash(instance)
                expect(hash["City"]).to eq(nil)
            end

            it "should send plaintext" do
                instance = CohortApplication.first
                instance.user.update_column(:city, "Fooville")
                instance.reload
                expect(Airtable::City).to receive(:find_by_name).and_return(nil)
                hash = Airtable::Application.get_airtable_hash(instance)
                expect(hash["City name"]).to eq("Fooville")
            end

            it "should set plaintext to nil if no record" do
                instance = CohortApplication.first
                instance.user.update_column(:city, nil)
                instance.reload
                hash = Airtable::Application.get_airtable_hash(instance)
                expect(hash["City name"]).to be_nil
            end
        end
    end
end
