require 'spec_helper'

describe Airtable::Syncable do

    before(:each) do
        @airtable_class = Airtable::Application
        @database_record = CohortApplication.first
        @database_record.update_columns({
            airtable_record_id: "airtable-id"
        })
    end

    describe "find_by_id_with_fallback" do

        it "should find_by_id if found with the airtable_record_id" do
            allow(@airtable_class).to receive(:base_key).and_return('12345')
            expect(@airtable_class).to receive(:find_by_id).with("airtable-id").and_return({:foo => "foo"})
            expect(@airtable_class).not_to receive(:all)
            expect(@airtable_class.find_by_id_with_fallback(@database_record)).to eq({:foo => "foo"})
        end

        it "should call filter if not found with the airtable_record_id" do
            allow(@airtable_class).to receive(:base_key).and_return('12345')
            allow(@airtable_class).to receive(:find_by_id).with("airtable-id").and_return(nil)
            expect(@airtable_class).to receive(:all).with(filter: "{Application ID} = '#{@database_record.id}'").and_return([{:foo => "foo"}])
            expect(@airtable_class.find_by_id_with_fallback(@database_record)).to eq({:foo => "foo"})
        end

        # Other parts of the app rely on the fact that the `find_by_id_with_fallback`
        # method raises an error if the fallback to the `all` method raises an error.
        # See delete_from_airtable_job.rb.
        it "should raise if fallback to filter by Application ID raised an error" do
            allow(@airtable_class).to receive(:base_key).and_return('12345')
            allow(@airtable_class).to receive(:find_by_id).with("airtable-id").and_return(nil)
            expect(@airtable_class).to receive(:all).with(filter: "{Application ID} = '#{@database_record.id}'").and_raise("Could not find base 'Foo'")
            expect {
                @airtable_class.find_by_id_with_fallback(@database_record)
            }.to raise_error(RuntimeError, "Could not find base 'Foo'")
        end

        it "should raise if class does not have base_key" do
            allow(@airtable_class).to receive(:base_key).and_return(nil)
            expect {
                @airtable_class.find_by_id_with_fallback(@database_record)
            }.to raise_error(RuntimeError)
        end

        it "should raise if class does not have table_name" do
            allow(@airtable_class).to receive(:base_key).and_return('12345')
            allow(@airtable_class).to receive(:table_name).and_return(nil)
            expect {
                @airtable_class.find_by_id_with_fallback(@database_record)
            }.to raise_error(RuntimeError)
        end

    end

    describe "find_by_id" do
        it "should return nil if called with nil" do
            expect(@airtable_class.find_by_id(nil)).to be_nil
        end

        it "should return nil if the record is not found in Airtable" do
            allow(@airtable_class).to receive(:find).and_raise(Airrecord::Error.new("test"))
            expect(@airtable_class.find_by_id("some record id")).to be_nil
        end

        it "should return the record if found in Airtable" do
            allow(@airtable_class).to receive(:find).and_return({:foo => "foo"})
            expect(@airtable_class.find_by_id("some record id")).to eq({:foo => "foo"})
        end
    end

    describe "find_or_create" do
        it "should return an already created application" do
            expect(@airtable_class).to receive(:find_by_id_with_fallback).with(@database_record).and_return({:foo => "foo"})
            expect(@airtable_class.find_or_create(@database_record, 'Foobar')).to eq({:foo => "foo"})
        end

        it "should call create" do
            expect(@airtable_class).to receive(:find_by_id_with_fallback).with(@database_record).and_return(nil)
            expect(@airtable_class).to receive(:create).with(@database_record, 'Foobar')
            @airtable_class.find_or_create(@database_record, 'Foobar')
        end
    end

    describe "create" do
        it "should work" do
            expect(@airtable_class).to receive(:get_airtable_hash).with(@database_record).and_call_original
            expect_any_instance_of(@airtable_class).to receive(:create)
            allow_any_instance_of(@airtable_class).to receive(:id).and_return('some_id')
            expect(EditorTracking).to receive(:transaction) do |user, &block|
                expect(user).to eq('Foobar')
                expect(@database_record).to receive(:update_column).with(:airtable_record_id, 'some_id')
                block.call
            end
            @airtable_class.create(@database_record, 'Foobar')
        end
    end

    describe "update" do
        it "should work" do
            mock = OpenStruct.new(:foo => "foo", :save => "mock")
            expect(@airtable_class).to receive(:find_by_id_with_fallback).with(@database_record).and_return(mock)
            expect(@airtable_class).to receive(:get_airtable_hash).with(@database_record).and_return({
                foo: "bar"
            })
            expect(mock).to receive(:save)
            expect(@airtable_class.update(@database_record).foo).to eq("bar")
        end

        it "should return nil if not found" do
            expect(@airtable_class).to receive(:find_by_id_with_fallback).with(@database_record).and_return(nil)
            expect(@airtable_class.update(@database_record)).to be_nil
        end
    end

    describe "destroy" do
        it "should work" do
            mock = OpenStruct.new(:foo => "foo", :save => "mock")
            expect(@airtable_class).to receive(:find_by_id_with_fallback).with(@database_record).and_return(mock)
            expect(mock).to receive(:destroy)
            @airtable_class.destroy(@database_record)
        end

        it "should not throw error if record not found" do
            expect(@airtable_class).to receive(:find_by_id_with_fallback).with(@database_record).and_return(nil)
            expect { @airtable_class.destroy(@database_record) }.not_to raise_error
        end
    end
end