require 'spec_helper'

describe Playlist::ToJsonFromApiParams do

    fixtures(:users, :playlists, :cohorts)


    before(:each) do
        # grab a user with no progress
        @user = User.joins("LEFT JOIN lesson_streams_progress ON users.id = lesson_streams_progress.user_id")
                    .where("lesson_streams_progress.id is null")
                    .first
        expect(@user).not_to be_nil
    end

    it "should have expected keys" do
        playlist = Playlist.first
        result = get_single_playlist_json(playlist, least_restrictive_params)
        expect(result.keys).to match_array([
            "id", "version_id", "title", "description", "modified_at",
            "updated_at", "author", "stream_entries",
            "was_published", "entity_metadata", "image", "locale", "locale_pack",
            "published_at", "published_version_id", "tag", "related_cohort_names",
            "programs_included_in"
        ])
    end

    it "should respect fields" do
        playlist = Playlist.first
        result = get_single_playlist_json(playlist, least_restrictive_params({fields: ['id']}))
        expect(result.keys).to match_array([
            "id"
        ])
    end

    it "should respect except" do
        playlist = Playlist.first
        result = get_single_playlist_json(playlist, least_restrictive_params({except: ['title']}))
        expect(result.keys.include?('title')).to be(false)
    end

    describe "keys with simple values" do

        it "should have expected values" do
            playlist = Playlist.first

            result = get_single_playlist_json(playlist, least_restrictive_params)
            {
                "id" => playlist.id,
                "version_id" => playlist.versions.last.version_id,
                "title" => playlist.title,
                "description" => playlist.description,
                "modified_at" => playlist.modified_at.to_timestamp,
                "updated_at" => playlist.updated_at.to_timestamp,
                "tag" => playlist.tag
            }.each do |key, value|
                expect(value).not_to be_nil, "Expecting all simple values to be set. #{key.inspect} is not"
                expect(result[key]).to eq(value), "Unexpected value for #{key.inspect}. expected #{value.inspect} got #{result[key].inspect}"
            end
        end
    end

    describe "author" do

        it "should have expected value" do
            playlist = Playlist.first
            result = get_single_playlist_json(playlist, least_restrictive_params)
            expect(result['author']).to eq(playlist.author.as_json(for: :public))
        end

    end

    describe "with id param" do

        it "should load up a single playlist" do
            playlist = Playlist.first
            json = Playlist::ToJsonFromApiParams.new(least_restrictive_params(id: playlist.id)).json
            results = ActiveSupport::JSON.decode(json)
            expect(results.size).to be(1)
            expect(results[0]['id']).to eq(playlist.id)
        end

        it "should work if id is passed in filters hash" do
            playlist = Playlist.first
            json = Playlist::ToJsonFromApiParams.new(least_restrictive_params(filters:
                {id: playlist.id}
            )).json
            results = ActiveSupport::JSON.decode(json)
            expect(results.size).to be(1)
            expect(results[0]['id']).to eq(playlist.id)
        end

        it "should load up multiple ids if array is passed in" do
            playlist_ids = Playlist.limit(2).pluck('id')
            json = Playlist::ToJsonFromApiParams.new(least_restrictive_params(filters:
                {id: playlist_ids}
            )).json
            results = ActiveSupport::JSON.decode(json)
            expect(results.size).to be(2)
            expect(results.map { |e| e['id'] }).to match_array(playlist_ids)
        end

    end

    describe "locale_pack" do
        it "should include the locale_pack" do
            playlist = playlists(:en_item)
            playlists_in_pack = [playlists(:en_item), playlists(:es_item), playlists(:zh_item)]

            json = get_json({fields: ['id', 'locale_pack'], filters: {id: playlist.id, published: false}})[0]
            expect(json['locale_pack']).not_to be_nil
            expect(json['locale_pack']['id']).to eq(playlist.locale_pack_id)
            expect(json['locale_pack']['content_items']).to match_array(playlists_in_pack.map { |playlist|
                {
                    'id' => playlist.id,
                    'title' => playlist.title,
                    'locale' => playlist.locale
                }
            })
        end

        it "should respect the published filter" do
            playlist = playlists(:en_item)
            playlists(:es_item).unpublish!
            playlists_in_pack = [playlists(:en_item), playlists(:zh_item)]
            playlists_in_pack.map(&:publish!)

            json = get_json({fields: ['id', 'locale_pack'], filters: {id: playlist.id, published: true}})[0]
            expect(json['locale_pack']).not_to be_nil
            expect(json['locale_pack']['id']).to eq(playlist.locale_pack_id)
            expect(json['locale_pack']['content_items']).to match_array(playlists_in_pack.map { |playlist|
                {
                    'id' => playlist.id,
                    'title' => playlist.title,
                    'locale' => playlist.locale
                }
            })
        end
    end

    describe "filters" do

        it "should filter by complete" do

            playlist, another_playlist = Playlist.all_published.where(locale: 'en').limit(2).to_a
            playlist = Playlist.find(playlist.id)  # stupid hack to get around "ContentPublisher is marked as readonly" error
            expect(another_playlist).not_to be_nil

            # make sure the 2 playlists are different
            if another_playlist.stream_locale_pack_ids.sort == playlist.stream_locale_pack_ids.sort
                another_stream = Lesson::Stream.all_published
                                    .where.not(locale_pack_id: playlist.stream_locale_pack_ids)
                                    .where.not(locale_pack_id: nil)
                                    .first
                expect(another_stream).not_to be_nil
                playlist.stream_entries.push({
                    'description' => "adsfasdfasdfasd",
                    'stream_id' => another_stream.id,
                    'locale_pack_id' => another_stream.locale_pack_id
                })
                playlist.publish!
            end
            expect(another_playlist.stream_locale_packs).not_to match_array(playlist.stream_locale_packs)

            # Playlist.where("playlists.id not in (?)", [playlist.id, another_playlist.id]).each(&:unpublish!)
            result = get_json(least_restrictive_params({
                user: @user,
                filters: {
                    complete: false
                }
            }))

            expect(result.map { |e| e['id']}).to include(playlist.id)
            expect(result.map { |e| e['id']}).to include(another_playlist.id)

            complete_streams(another_playlist.stream_locale_pack_ids)

            result = get_json(least_restrictive_params({
                user: @user,
                filters: {
                    complete: false
                }
            }))

            expect(result.map { |e| e['id']}).to include(playlist.id)
            expect(result.map { |e| e['id']}).not_to include(another_playlist.id)
        end

        describe "in_users_locale" do
            attr_accessor :en_only_item, :en_item, :es_item, :es_only_item

            before(:each) do
                @en_only_item, @en_item, @es_item, @es_only_item = [
                    playlists(:en_only_item),
                    playlists(:en_item),
                    playlists(:es_item),
                    playlists(:published_only_in_es)
                ]
            end

            describe "in_users_locale_or_en" do

                it "should only show english content to an english user" do
                    @user = users(:en_superviewer)

                    result = get_json(least_restrictive_params({
                        user: @user,
                        filters: {
                            in_users_locale_or_en: true
                        }
                    }))

                    ids = result.map { |e| e['id'] }
                    expect(ids).to include(en_item.id)
                    expect(ids).to include(en_only_item.id)
                    expect(ids).not_to include(es_item.id)
                    expect(ids).not_to include(es_only_item.id)
                end

                it "should show spanish content to a spanish user but fallback to english if necessary" do
                    @user = users(:en_superviewer)
                    @user.update_columns(pref_locale: 'es')

                    result = get_json(least_restrictive_params({
                        user: @user,
                        filters: {
                            in_users_locale_or_en: true
                        }
                    }))

                    ids = result.map { |e| e['id'] }
                    expect(ids).not_to include(en_item.id)
                    expect(ids).to include(en_only_item.id)
                    expect(ids).to include(es_item.id)
                    expect(ids).to include(es_only_item.id)
                end
            end
        end
    end

    describe "with limit" do

        it "should limit results" do
            result = get_json(least_restrictive_params({
                limit: 1
            }))
            expect(result.size).to eq(1)
        end

    end

    describe "related_cohort_names" do

        it "should load related cohort_names from required_playlist_pack_ids" do
            cohorts = Cohort.limit(2)
            playlist = Playlist.first

            # required_playlist_pack_ids are derived from the playlist_collections attribute
            cohorts[0].update_attribute('playlist_collections', [title: '', required_playlist_pack_ids: [playlist.locale_pack_id]])
            cohorts[1].update_attribute('playlist_collections', [title: '', required_playlist_pack_ids: []])

            result = get_json(least_restrictive_params({
                filters: {
                    locale_pack_id: [playlist.locale_pack_id]
                },
                fields: ['related_cohort_names']
            }))
            expect(result[0]['related_cohort_names']).to include(cohorts[0].name)
            expect(result[0]['related_cohort_names']).not_to include(cohorts[1].name)
        end

        it "should load related cohort_names from specialization_playlist_ids" do
            cohorts = Cohort.limit(2)
            playlist = Playlist.first
            cohorts[0].update_attribute('specialization_playlist_pack_ids', [playlist.locale_pack_id])
            cohorts[1].update_attribute('specialization_playlist_pack_ids', [])
            cohort = cohorts[0]
            result = get_json(least_restrictive_params({
                filters: {
                    locale_pack_id: cohort.specialization_playlist_pack_ids
                },
                fields: ['related_cohort_names']
            }))
            expect(result[0]['related_cohort_names']).to include(cohorts[0].name)
            expect(result[0]['related_cohort_names']).not_to include(cohorts[1].name)

        end
    end

    describe "programs_included_in" do
        it "should load programs for which the playlist is included in" do
            cohort = Cohort.promoted_cohort('mba')
            expect(cohort).not_to be_nil

            result = get_json(least_restrictive_params({
                filters: {
                    locale_pack_id: [cohort.required_playlist_pack_ids[0]]
                },
                fields: ["programs_included_in"]
            }))

            expect(result[0]["programs_included_in"]).not_to be_nil
            expect(result[0]["programs_included_in"]).to include("mba")
        end
    end

    def least_restrictive_params(overrides = {})
        {
            filters: {published: false}
        }.deep_merge(overrides);
    end

    def get_json(params)
        json = Playlist::ToJsonFromApiParams.new(params).json
        ActiveSupport::JSON.decode(json)
    end

    def get_single_playlist_json(playlist, params)
        expect(playlist).not_to be_nil
        results = get_json(params)
        results_for_playlist = results.select { |r| r['id'] == playlist.attributes['id'] }
        expect(results_for_playlist.size).to be <=1
        results_for_playlist.first
    end

    def image_json(image)
        {
            'id' => image.id,
            'formats' => image.formats.deep_stringify_keys,
            'dimensions' => image.dimensions.deep_stringify_keys
        }
    end

    def complete_streams(locale_pack_ids)
        locale_pack_ids.each do |locale_pack_id|

            record = Lesson::StreamProgress.where({
                user: @user,
                locale_pack_id: locale_pack_id
            }).first_or_initialize

            if !record.completed_at
                record.started_at =  Time.now
                record.completed_at =  Time.now
                record.save!
            end
        end
    end
end
