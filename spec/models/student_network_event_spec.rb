# == Schema Information
#
# Table name: student_network_events
#
#  id                                             :uuid             not null, primary key
#  created_at                                     :datetime         not null
#  updated_at                                     :datetime         not null
#  title                                          :text             not null
#  description                                    :text             not null
#  event_type                                     :text             not null
#  start_time                                     :datetime
#  end_time                                       :datetime
#  image_id                                       :uuid
#  external_rsvp_url                              :text
#  place_id                                       :text
#  place_id_anonymized                            :text
#  place_details                                  :json             not null
#  place_details_anonymized                       :json             not null
#  location_name                                  :text
#  published                                      :boolean          default(FALSE), not null
#  published_at                                   :datetime
#  internal_notes                                 :text
#  author_id                                      :uuid             not null
#  visible_to_current_degree_students             :boolean          default(TRUE), not null
#  visible_to_graduated_degree_students           :boolean          default(TRUE), not null
#  visible_to_non_degree_users                    :boolean          default(FALSE), not null
#  location                                       :geography(Point,
#  location_anonymized                            :geography(Point,
#  timezone                                       :text
#  visible_to_accepted_degree_students_in_cohorts :uuid             default([]), not null, is an Array
#  rsvp_status                                    :text             default("not_required"), not null
#  date_tbd                                       :boolean          default(FALSE), not null
#  date_tbd_description                           :text
#

require 'spec_helper'

describe StudentNetworkEvent do

    fixtures(:student_network_events)

    describe "validations" do

        it "should validate inclusion rsvp_status in rsvp_statuses" do
            event = StudentNetworkEvent.first

            %w(not_required required closed).each do |status|
                event.rsvp_status = status
                event.external_rsvp_url = status == 'required' ? 'foobar' : nil # get around external_rsvp_url validations
                expect(event).to be_valid
            end

            event.rsvp_status = 'foobar'
            expect(event).not_to be_valid
            expect(event.errors.full_messages).to eq(["Rsvp status foobar is not a valid rsvp status"])
        end

        it "should validate presence of external_rsvp_url if rsvp is required" do
            event = StudentNetworkEvent.first

            event.rsvp_status = 'required'
            event.external_rsvp_url = nil
            expect(event).not_to be_valid

            event.external_rsvp_url = 'foobar'
            expect(event).to be_valid
        end

    end

    describe "create_from_hash!" do
        fixtures(:users)

        it "should work" do
            author = users(:admin)
            expect(StudentNetworkEvent).to receive(:initialize_from_hash).and_call_original
            expect_any_instance_of(StudentNetworkEvent).to receive(:merge_hash).and_call_original

            hash = {
                title: 'Foo Event',
                description: 'Bar Event',
                event_type: 'meetup',
                start_time: Time.now + 6.months,
                end_time: Time.now + 6.months,
                author_id: author.id
            }
            instance = StudentNetworkEvent.create_from_hash!(hash)

            expect(instance.id).not_to be_nil
            assert_instance_matches_hash(hash, instance)
        end

    end

    describe "update_from_hash!" do

        it "should work" do
            expect_any_instance_of(StudentNetworkEvent).to receive(:merge_hash).and_call_original

            hash = {
                id: StudentNetworkEvent.first.id,
                title: 'Renamed Title',
                description: 'Renamed Description',
            }
            instance = StudentNetworkEvent.update_from_hash!(hash)

            assert_instance_matches_hash(hash, instance)
        end

        it "should raise if the record does not exist" do
            expect {
                StudentNetworkEvent.update_from_hash!({
                    id: SecureRandom.uuid
                })
            }.to raise_error(ActiveRecord::RecordNotFound)
        end

    end

    describe "merge_hash" do

        it "should work" do
            instance = StudentNetworkEvent.new
            hash = {
                title: 'An event',
                description: 'A short description',
                event_type: 'some_type',
                start_time: Time.now,
                end_time: Time.now,
                image_id: SecureRandom.uuid,
                rsvp_status: 'not_required',
                external_rsvp_url: 'https://goo.gle',
                place_id: '12345',
                place_details: {},
                location_name: 'Cupertino',
                published: true,
                internal_notes: 'notes',
                visible_to_current_degree_students: true,
                visible_to_graduated_degree_students: true,
                visible_to_accepted_degree_students_in_cohorts: [SecureRandom.uuid, SecureRandom.uuid],
                visible_to_non_degree_users: true
            }
            instance.merge_hash(hash)

            assert_instance_matches_hash(hash, instance)
        end

    end

    describe "as_json" do

        attr_accessor :instance

        before(:each) do
            @instance = StudentNetworkEvent.first
        end

        it "should include base fields" do
            json = instance.as_json

            %w(
                id
                title
                event_type
                image_id
                rsvp_status
                timezone
                created_at
                updated_at
                recommended
                anonymized
                start_time
                end_time
                date_tbd
                date_tbd_description
            ).each do |attr|
                expect(json.keys.include?(attr)).to be(true)
            end
        end

        it "should include admin fields when necessary" do
            json = instance.as_json({:fields=>"ADMIN_FIELDS"})

            %w(
                published
                published_at
                internal_notes
                author_id
                visible_to_current_degree_students
                visible_to_graduated_degree_students
                visible_to_accepted_degree_students_in_cohorts
                visible_to_non_degree_users
            ).each do |attr|
                expect(json.keys.include?(attr)).to be(true)
            end
        end

        describe "when opts[:user_has_full_student_network_events_access]" do
            attr_accessor :json

            before(:each) do
                @json = instance.as_json({:user_has_full_student_network_events_access=>true})
            end

            it "should set anonymized to false" do
                expect(json["anonymized"]).to be(false)
            end

            it "should include non-anonymized and sensitive fields" do
                %w(
                    description
                    external_rsvp_url
                    place_id
                    place_details
                    location
                    location_name
                    start_time
                    end_time
                ).each do |attr|
                    expect(json.keys.include?(attr)).to be(true)
                end
            end
        end

        describe "when !opts[:user_has_full_student_network_events_access]" do

            attr_accessor :json

            before(:each) do
                @json = instance.as_json({:user_has_full_student_network_events_access=>false})
            end

            it "should include anonymized fields" do
                %w(
                    place_id_anonymized
                    place_details_anonymized
                    location_anonymized
                ).each do |attr|
                    expect(json.keys.include?(attr)).to be(true)
                end
            end

            it "should not include non-anonymized and sensitive fields" do
                %w(
                    description
                    external_rsvp_url
                    place_id
                    place_details
                    location
                    location_name
                ).each do |attr|
                    expect(json.keys.include?(attr)).to be(false), "missing #{attr}"
                end
            end

            it "should set anonymized to true" do
                expect(json["anonymized"]).to be(true)
            end
        end

    end

    describe "location, place_details, and timezone columns" do

        attr_accessor :instance, :hq_place_details, :mock_anonymized_place_id, :mock_anonymized_place_details, :mock_google_place_details_response, :mock_google_timezone_response

        before(:each) do
            @instance = StudentNetworkEvent.first
            instance.update_column(:location, nil)
            instance.update_column(:location_anonymized, nil)

            @hq_place_details = {
                "street_number"=>{"short"=>"3000", "long"=>"3000"},
                "route"=>{"short"=>"K St NW", "long"=>"K Street Northwest"},
                "neighborhood"=>{"short"=>"Northwest Washington", "long"=>"Northwest Washington"},
                "locality"=>{"short"=>"Washington", "long"=>"Washington"},
                "administrative_area_level_1"=>{"short"=>"DC", "long"=>"District of Columbia"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "postal_code"=>{"short"=>"20007", "long"=>"20007"},
                "formatted_address"=>"3000 K St NW, Washington, DC 20007, USA",
                "utc_offset"=>-240,
                "lat"=>38.9017801,
                "lng"=>-77.0597323
            }

            @mock_anonymized_place_id = 'ChIJW-T2Wt7Gt4kRKl2I1CJFUsI' # Washington, D.C.
            @mock_anonymized_place_details = {
                "locality"=>{"short"=>"Washington", "long"=>"Washington"},
                "administrative_area_level_1"=>{"short"=>"DC", "long"=>"District of Columbia"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>38.9071923,
                "lng"=>-77.0368707
            }

            # Mock out Location.rb calls so we don't actually try to hit Google's API
            @mock_google_place_details_response = {
                "geometry"=> {
                    "location"=>{"lat"=>38.9071923, "lng"=>-77.0368707},
                },
                "place_id"=>"ChIJW-T2Wt7Gt4kRKl2I1CJFUsI"
            }
            allow(Location).to receive(:find_place_from_text).and_return(mock_google_place_details_response)
            allow(Location).to receive(:place_details_for_place_id).and_return(mock_anonymized_place_details)

            @mock_google_timezone_response = OpenStruct.new({
                name: 'America/New_York'
            })
            allow(Timezone).to receive(:lookup).and_return(@mock_google_timezone_response)
        end

        it "should set location when place_details is set" do
            instance.place_details = hq_place_details
            instance.save!
            expect(instance.reload.location).not_to be(nil)
        end

        it "should call Location#find_place_from_text with relevant information" do
            expect(Location).to receive(:find_place_from_text).with("Washington, District of Columbia, United States").exactly(1).times.and_return(mock_google_place_details_response)
            instance.place_details = hq_place_details
            instance.save!
        end

        it "should set place_id_anonymized and place_details_anonymized when place_details is set or changed" do
            expect(instance).to receive(:set_place_details_anonymized).exactly(:once).and_call_original
            instance.place_details = hq_place_details
            instance.save!
            expect(instance.reload.place_id_anonymized).to eq(mock_anonymized_place_id)
            expect(instance.reload.place_details_anonymized).to eq(mock_anonymized_place_details.to_h)
        end

        it "should set location_anonymized when place_details_anonymized is set" do
            instance.place_details_anonymized = mock_anonymized_place_details
            instance.save!
            expect(instance.reload.location_anonymized).not_to be(nil)
        end

    end

    describe "recommended?" do

        attr_accessor :instance

        before(:each) do
            @instance = StudentNetworkEvent.first
        end

        it "should be true for special events and conferences" do
            expect(instance).to receive(:event_type).and_return('special_event')
            expect(instance.recommended?).to be(true)

            expect(instance).to receive(:event_type).and_return('conference')
            expect(instance.recommended?).to be(true)
        end

        it "should be true if no location" do
            expect(instance).to receive(:event_type).and_return('meetup') # avoid false-positives
            expect(instance).to receive(:location).and_return(nil)
            expect(instance.recommended?).to be(true)
        end

        it "should be true if geographically_relevant_to_place_details_or_fallback_location?" do
            expect(instance).to receive(:event_type).and_return('meetup') # avoid false-positives
            expect(instance).to receive(:location).and_return('foobar') # avoid false-positives
            expect(instance).to receive(:geographically_relevant_to_place_details_or_fallback_location?).and_return(true)
            expect(instance.recommended?).to be(true)
        end

        it "shoud be true if user is an alumnus" do
            expect(instance).to receive(:event_type).and_return('meetup') # avoid false-positives
            expect(instance).to receive(:location).and_return('foobar') # avoid false-positives
            expect(instance).to receive(:geographically_relevant_to_place_details_or_fallback_location?).and_return(false) # avoid false-positives
            expect(instance.recommended?(nil, true)).to be(true)
        end

        it "should be false otherwise" do
            expect(instance).to receive(:event_type).and_return('meetup') # avoid false-positives
            expect(instance).to receive(:location).and_return('foobar') # avoid false-positives
            expect(instance).to receive(:geographically_relevant_to_place_details_or_fallback_location?).and_return(false) # avoid false-positives
            expect(instance.recommended?).to be(false)
        end

    end

    describe "geographically_relevant_to_place_details_or_fallback_location?" do

        attr_accessor :instance, :mock_place_details, :mock_user_location, :mock_event_location, :inside_50_mi, :outside_50_mi

        before(:each) do
            @instance = StudentNetworkEvent.first
            @mock_place_details = {}
            @mock_user_location = double('location')
            @mock_event_location = double('location')
            @inside_50_mi = (0..50).to_a.sample
            @outside_50_mi = 51
        end

        it "should be true if passed in _place_details are within 50 miles of the event" do
            expect(Location).to receive(:from_place_details).with(mock_place_details).and_return(mock_user_location).ordered
            expect(Location).to receive(:from_place_details).with(instance.place_details.symbolize_keys).and_return(mock_event_location).ordered
            expect(mock_event_location).to receive(:distance_between).with(mock_user_location, {units: :mi}).and_return(inside_50_mi)
            expect(instance.geographically_relevant_to_place_details_or_fallback_location?(mock_place_details)).to be(true)
        end

        it "should be false if passed in _place_details not are within 50 miles of the event" do
            expect(Location).to receive(:from_place_details).with(mock_place_details).and_return(mock_user_location).ordered
            expect(Location).to receive(:from_place_details).with(instance.place_details.symbolize_keys).and_return(mock_event_location).ordered
            expect(mock_event_location).to receive(:distance_between).with(mock_user_location, {units: :mi}).and_return(outside_50_mi)
            expect(instance.geographically_relevant_to_place_details_or_fallback_location?(mock_place_details)).to be(false)
        end

        it "should be true if no passed in _place_details and event is within 50 miles of Washington, DC" do
            expect(Location).to receive(:from_place_details).with(instance.place_details.symbolize_keys).and_return(mock_event_location)
            expect(mock_event_location).to receive(:distance_between).with(Location.locations[:washington_dc], {units: :mi}).and_return(inside_50_mi)
            expect(instance.geographically_relevant_to_place_details_or_fallback_location?).to be(true)
        end

        it "should be false if no passed in _place_details and event is not within 50 miles of Washington, DC" do
            expect(Location).to receive(:from_place_details).with(instance.place_details.symbolize_keys).and_return(mock_event_location)
            expect(mock_event_location).to receive(:distance_between).with(Location.locations[:washington_dc], {units: :mi}).and_return(outside_50_mi)
            expect(instance.geographically_relevant_to_place_details_or_fallback_location?).to be(false)
        end

    end

    describe "update_fulltext" do
        it "should update_fulltext when degree visible_to fields change" do
            emba_cohort_id = Cohort.promoted_cohort('emba')['id']
            mba_cohort_id = Cohort.promoted_cohort('mba')['id']
            event = student_network_events(:cupertino_present_meetup)

            expect(event).to receive(:update_fulltext).exactly(3).times
            event.update!(visible_to_current_degree_students: !event.visible_to_current_degree_students)
            event.update!(visible_to_graduated_degree_students: !event.visible_to_graduated_degree_students)
            event.update!(visible_to_accepted_degree_students_in_cohorts: [emba_cohort_id, mba_cohort_id])
            event.update!(visible_to_non_degree_users: !event.visible_to_non_degree_users) # does not trigger
        end
    end

    describe "degree_program_types_visible_to" do
        attr_accessor :event

        before(:each) do
            @event = student_network_events(:cupertino_present_meetup)
            event.update_columns(
                visible_to_current_degree_students: false,
                visible_to_graduated_degree_students: false,
                visible_to_accepted_degree_students_in_cohorts: []
            )
            allow(Cohort::ProgramTypeConfig).to receive(:degree_program_types).and_return(['foo', 'bar'])
        end

        it "should be correct when visible_to_current_degree_students" do
            event.update!(visible_to_current_degree_students: true)
            expect(event.send(:degree_program_types_visible_to)).to match_array(['foo', 'bar'])
        end

        it "should be correct when visible_to_graduated_degree_students" do
            event.update!(visible_to_graduated_degree_students: true)
            expect(event.send(:degree_program_types_visible_to)).to match_array(['foo', 'bar'])
        end

        it "should be correct when visible_to_accepted_degree_students_in_cohorts" do
            emba_cohort_id = Cohort.promoted_cohort('emba')['id']
            mba_cohort_id = Cohort.promoted_cohort('mba')['id']
            event.update!(visible_to_accepted_degree_students_in_cohorts: [emba_cohort_id, mba_cohort_id])
            expect(event.send(:degree_program_types_visible_to)).to match_array(['emba', 'mba'])
        end

        it "should be empty when visible_to_non_degree_users" do
            event.update!(visible_to_non_degree_users: true)
            expect(event.send(:degree_program_types_visible_to)).to be_empty
        end
    end

    def assert_instance_matches_hash(hash, instance)
        hash.keys.each do |key|
            expect(instance.send(key)).to eq(hash[key])
        end
    end

end
