# == Schema Information
#
# Table name: hiring_teams
#
#  id                    :uuid             not null, primary key
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  title                 :text             not null
#  owner_id              :uuid
#  subscription_required :boolean          default(TRUE), not null
#  hiring_plan           :text
#  domain                :text             not null
#

require 'spec_helper'
require 'stripe_helper'

describe HiringTeam do

    include StripeHelper

    fixtures(:hiring_relationships, :users)

    describe "create_from_hash!" do
        it "should work" do
            expect_any_instance_of(HiringTeam).to receive(:merge_hash).and_call_original
            hiring_manager = HiringApplication.left_outer_joins(:user).where(users: {hiring_team_id: nil}).first.user
            instance = HiringTeam.create_from_hash!({
                title: 'Team Rocket',
                hiring_manager_ids: [hiring_manager.id],
                domain: 'rocket.com'
            })
            expect(instance.title).to eq('Team Rocket')
        end

        it "should raise if multiple connections to same candidate" do
            hiring_relationship = hiring_relationships(:accepted_accepted)

            another_hiring_manager = find_fresh_hiring_manager
            new_hiring_relationship = HiringRelationship.create!({
                hiring_manager_id: another_hiring_manager.id,
                candidate_id: hiring_relationship.candidate_id,
                hiring_manager_status: 'accepted',
                candidate_status: 'accepted'
            })

            # sanity check
            expect(new_hiring_relationship.reload.hiring_manager_status).to eq('accepted')

            expect {
                HiringTeam.create_from_hash!({
                    title: 'Team Rocket',
                    domain: 'rocket.com',
                    hiring_manager_ids: [hiring_relationship.hiring_manager.id, another_hiring_manager.id]
                })
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Hiring team has multiple connections to the same candidate. Please notify engineering to resolve the issue.")
        end

        it "should not raise if multiple connections to same candidate but only one is accepted" do
            hiring_relationship = hiring_relationships(:accepted_accepted)

            another_hiring_manager = find_fresh_hiring_manager
            HiringRelationship.create!({
                hiring_manager_id: another_hiring_manager.id,
                candidate_id: hiring_relationship.candidate_id,
                hiring_manager_status: 'saved_for_later',
                candidate_status: 'accepted'
            })

            expect {
                HiringTeam.create_from_hash!({
                    title: 'Team Rocket',
                    hiring_manager_ids: [hiring_relationship.hiring_manager.id, another_hiring_manager.id],
                    domain: 'rocket.com'
                })
            }.not_to raise_error
        end
    end

    describe "update_from_hash!" do
        it "should work" do
            team = HiringTeam.create!(title: 'The Browns', domain: 'browns.com')
            hiring_manager = HiringApplication.left_outer_joins(:user).where(users: {hiring_team_id: nil}).first.user
            hiring_manager.hiring_application.update_column(:status, 'accepted') # to pass validations

            expect_any_instance_of(HiringTeam).to receive(:merge_hash).and_call_original
            instance = HiringTeam.update_from_hash!({
                id: team.id,
                title: 'Team Rocket',
                hiring_manager_ids: [hiring_manager.id],
                owner_id: hiring_manager.id,
                subscription_required: false,
                hiring_plan: HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING
            })
            expect(instance.title).to eq('Team Rocket')
            expect(instance.subscription_required).to be(false)
            expect(instance.hiring_plan).to eq(HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING)
        end

        it "should raise if mulitiple connections to same candidate" do
            team = HiringTeam.create(title: 'The Browns', domain: 'browns.com')
            hiring_relationship = hiring_relationships(:accepted_accepted)
            hiring_relationship.hiring_manager.hiring_team = team
            hiring_relationship.hiring_manager.save!

            another_hiring_manager = find_fresh_hiring_manager
            HiringRelationship.create!({
                hiring_manager_id: another_hiring_manager.id,
                candidate_id: hiring_relationship.candidate_id,
                hiring_manager_status: 'accepted',
                candidate_status: 'accepted'
            })
            hiring_manager_ids = team.hiring_manager_ids
            expect {
                HiringTeam.update_from_hash!({
                    id: team.id,
                    title: team.title,
                    hiring_manager_ids: hiring_manager_ids.push(another_hiring_manager.id)
                })
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Hiring team has multiple connections to the same candidate. Please notify engineering to resolve the issue.")

        end

        it "should be possible to remove the owner, provided they are not accepted" do
            team = HiringTeam.joins(:owner).detect { |t| t.hiring_managers.count > 1 }
            team.owner.hiring_application.update(status: 'pending')
            orig_owner_id = team.owner_id
            attrs = team.as_json
            attrs['hiring_manager_ids'].delete(orig_owner_id)
            attrs['owner_id'] = attrs['hiring_manager_ids'].first
            instance = HiringTeam.update_from_hash!(attrs)
            expect(instance.owner_id).to eq(attrs['owner_id'])
            expect(instance.hiring_manager_ids).not_to include(orig_owner_id)
        end

        it "should not allow removing an accepted hiring manager from a team" do
            team = HiringTeam.joins(:owner).detect { |t| t.hiring_managers.count > 1 }
            non_owner_id = (team.hiring_manager_ids - [team.owner_id]).first
            attrs = team.as_json

            attrs['hiring_manager_ids'].delete(non_owner_id)
            HiringApplication.find_by_user_id(non_owner_id).update(status: 'accepted')
            expect {
                HiringTeam.update_from_hash!(attrs)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Hiring manager cannot be removed: Hiring team cannot be unset for a user with an accepted HiringApplication")
        end

        it "should destroy pay_per_post subscriptions if changing to unlimited" do
            team = HiringTeam.joins(:owner => :hiring_application).where(subscription_required: true, hiring_applications: {status: 'accepted'}).first
            team.update!(hiring_plan: HiringTeam::HIRING_PLAN_PAY_PER_POST)
            expect_any_instance_of(HiringTeam).to receive(:cancel_pay_per_post_subscriptions_before_switching_to_unlimited)
            instance = HiringTeam.update_from_hash!({
                id: team.id,
                subscription_required: false,
                hiring_plan: HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING
            })
        end

        it "should destroy unlimited subscription if setting subscription_required=false" do
            team = HiringTeam.joins(:owner => :hiring_application)
                    .left_outer_joins(:subscriptions)
                    .where("subscriptions.id is null")
                    .where(subscription_required: true, hiring_applications: {status: 'accepted'}).first
            team.update!(hiring_plan: HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING)
            subscription = create_subscription(team, default_plan.id)
            instance = HiringTeam.update_from_hash!({
                id: team.id,
                subscription_required: false
            })
            expect(Subscription.find_by_id(subscription.id)).to be_nil

            # if we're not careful, the destroyed subscription will still be in the list
            # when we try to generate json for the team
            expect(instance.as_json['subscriptions']).to be_empty
        end

        it "should not allow setting subscription_required to false if the owner does not have an accepted hiring application" do
            team = HiringTeam.joins(:owner).where(subscription_required: true).first
            team.owner.hiring_application.update_column(:status, 'pending')
            attrs = {
                id: team.id,
                subscription_required: false,
                hiring_plan: HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING
            }
            expect {
                HiringTeam.update_from_hash!(attrs)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Subscription required can only be false when the hiring manager is already accepted")
        end

    end

    describe "merge_hash" do

        it "should work" do
            team = HiringTeam.create!(title: 'The Browns', domain: 'browns.com')
            hiring_manager = HiringApplication.left_outer_joins(:user).where(users: {hiring_team_id: nil}).first.user

            expect_any_instance_of(HiringTeam).to receive(:merge_hash).and_call_original
            expect(team.hiring_plan).not_to eq(HiringTeam::HIRING_PLAN_PAY_PER_POST)

            team.merge_hash({
                id: team.id,
                title: 'Team Rocket',
                hiring_manager_ids: [hiring_manager.id],
                owner_id: hiring_manager.id,
                domain: 'newdomain.com',
                hiring_plan: HiringTeam::HIRING_PLAN_PAY_PER_POST
            })
            expect(team.title).to eq('Team Rocket')
            expect(team.hiring_managers.to_a).to eq([hiring_manager])
            expect(team.owner).to eq(hiring_manager)
            expect(team.domain).to eq('newdomain.com')
            expect(team.hiring_plan).to eq(HiringTeam::HIRING_PLAN_PAY_PER_POST)
        end

        it "should identify when hiring managers are added or removed" do
            team = HiringTeam.create!(title: 'The Browns', domain: 'browns.com')
            hiring_manager = HiringApplication.left_outer_joins(:user).where(users: {hiring_team_id: nil}).first.user

            expect_any_instance_of(User).to receive(:identify).at_least(2).times

            team.merge_hash({
                id: team.id,
                title: 'Team Rocket',
                hiring_manager_ids: [hiring_manager.id]
            })

            team.merge_hash({
                id: team.id,
                title: 'Team Rocket',
                hiring_manager_ids: []
            })

        end


    end

    describe "single connection enforcement" do

        it "should allow for having multiple pending or rejected relationships for the same candidate" do
            hiring_manager = users(:hiring_manager_with_team)
            teammate = users(:hiring_manager_teammate)
            candidate = users(:learner)

            expect {
                hiring_manager.ensure_candidate_relationships([candidate.id])
                teammate.ensure_candidate_relationships([candidate.id])
            }.not_to raise_error
        end

        describe "on hiring relationship changes" do
            # FIXME use the fixtures
            it "should raise if changing the status on a relationship results in two connections to a candidate for a team" do
                hiring_relationship, another_hiring_manager = get_teammates_with_an_accepted_candidate
                another_hiring_relationship = HiringRelationship.create!(
                    hiring_manager_id: another_hiring_manager.id,
                    candidate_id: hiring_relationship.candidate_id,
                    hiring_manager_status: 'hidden'
                )

                expect {
                    another_hiring_relationship.update_attribute(:hiring_manager_status, "accepted")
                }.to raise_error(ActiveRecord::StatementInvalid)
            end

            it "should raise if creating a relationship results in two connections to a candidate for a team" do
                hiring_relationship, another_hiring_manager = get_teammates_with_an_accepted_candidate

                expect {
                    HiringRelationship.create!(
                        hiring_manager_id: another_hiring_manager.id,
                        candidate_id: hiring_relationship.candidate_id,
                        hiring_manager_status: 'accepted'
                    )
                }.to raise_error(ActiveRecord::StatementInvalid)
            end

            # very unlikely to happen, if ever, but the trigger should support it
            it "should allow for deleting a relationship and then creating another for the same candidate" do
                hiring_relationship, another_hiring_manager = get_teammates_with_an_accepted_candidate

                # if we delete the existing relationshp, then
                # it should not block creation of another
                hiring_relationship.destroy

                expect {
                    HiringRelationship.create!(
                        hiring_manager_id: another_hiring_manager.id,
                        candidate_id: hiring_relationship.candidate_id,
                        hiring_manager_status: 'accepted'
                    )
                }.not_to raise_error
            end

            def get_teammates_with_an_accepted_candidate
                team = HiringTeam.create(title: 'The Browns', domain: 'browns.com')
                hiring_relationship = hiring_relationships(:accepted_accepted)
                hiring_relationship.hiring_manager.hiring_team = team
                hiring_relationship.hiring_manager.save!

                another_hiring_manager = User.left_outer_joins(:candidate_relationships).where("hiring_relationships.id is null").first
                another_hiring_manager.hiring_team = team
                another_hiring_manager.save!

                [hiring_relationship, another_hiring_manager]
            end
        end

        describe "on hiring manager changes" do

            it "should raise if adding a hiring manager to a team results in two connections to a candidate for a team" do
                team = HiringTeam.create(title: 'The Browns', domain: 'browns.com')
                hiring_relationship = hiring_relationships(:accepted_accepted)
                hiring_relationship.hiring_manager.hiring_team = team
                hiring_relationship.hiring_manager.save!

                another_hiring_manager = find_fresh_hiring_manager
                HiringRelationship.create!(
                    hiring_manager_id: another_hiring_manager.id,
                    candidate_id: hiring_relationship.candidate_id,
                    hiring_manager_status: 'accepted'
                )

                expect {
                    another_hiring_manager.hiring_team = team
                    another_hiring_manager.save!
                }.to raise_error(ActiveRecord::StatementInvalid)
            end

            it "should raise if changing the team for a hiring manager results in two connections to a candidate" do
                team = HiringTeam.create(title: 'The Browns', domain: 'browns.com')
                hiring_relationship = hiring_relationships(:accepted_accepted)
                hiring_relationship.hiring_manager.hiring_team = team
                hiring_relationship.hiring_manager.save!

                another_hiring_manager = find_fresh_hiring_manager
                another_team = HiringTeam.create(title: 'The Tribe', domain: 'tribe.com')
                another_hiring_manager.hiring_team = another_team
                another_hiring_manager.save!
                HiringRelationship.create!(
                    hiring_manager_id: another_hiring_manager.id,
                    candidate_id: hiring_relationship.candidate_id,
                    hiring_manager_status: 'accepted'
                )

                expect {
                    another_hiring_manager.hiring_team = team
                    another_hiring_manager.save!
                }.to raise_error(ActiveRecord::StatementInvalid)
            end

            it "should allow for a new connection to be made to a candidate when an old hiring manager was connected moved to a new team" do
                team = HiringTeam.create(title: 'The Browns', domain: 'browns.com')
                hiring_relationship = hiring_relationships(:accepted_accepted)
                hiring_relationship.hiring_manager.hiring_team = team
                hiring_relationship.hiring_manager.save!

                another_hiring_manager = find_fresh_hiring_manager
                HiringRelationship.create!(
                    hiring_manager_id: another_hiring_manager.id,
                    candidate_id: hiring_relationship.candidate_id,
                    hiring_manager_status: 'accepted'
                )

                # if we remove the currently connected hiring manager from the current team,
                # adding them to a new one, then we should be able to connect with the candidate
                ensure_fresh_hiring_team(hiring_relationship.hiring_manager)

                expect {
                    another_hiring_manager.hiring_team = team
                    another_hiring_manager.save!
                }.not_to raise_error
            end

            it "should allow for a new connection to be made to a candidate when an old hiring manager was connected but has changed teams" do
                team = HiringTeam.create(title: 'The Browns', domain: 'browns.com')
                hiring_relationship = hiring_relationships(:accepted_accepted)
                hiring_relationship.hiring_manager.hiring_team = team
                hiring_relationship.hiring_manager.save!

                another_hiring_manager = find_fresh_hiring_manager
                HiringRelationship.create!(
                    hiring_manager_id: another_hiring_manager.id,
                    candidate_id: hiring_relationship.candidate_id,
                    hiring_manager_status: 'accepted'
                )

                # if we move the currently connected hiring manager to another
                # team, then we should be able to connect with the candidate
                another_team = HiringTeam.create(title: 'The Tribe', domain: 'tribe.com')
                hiring_relationship.hiring_manager.hiring_team = another_team
                hiring_relationship.hiring_manager.save!

                expect {
                    another_hiring_manager.hiring_team = team
                    another_hiring_manager.save!
                }.not_to raise_error
            end
        end

    end

    describe "nullify_hiring_managers" do

        it "should fail to set hiring_team_id to nil on all managers if any are accepted" do
            team = HiringTeam.first
            expect(team).not_to receive(:delete_stripe_customer_if_stripe_on)
            hiring_managers = team.hiring_managers

            hiring_managers.each do |hiring_manager|
                hiring_manager.hiring_application.update(status: 'accepted')
                expect(hiring_manager.hiring_team_id).to eq(team.id)
            end

            expect {
                team.destroy!
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Hiring team cannot be unset for a user with an accepted HiringApplication")

        end


        it "should set hiring_team_id to nil on all managers if none are accepted" do
            team = HiringTeam.joins(:hiring_managers)
                        .left_outer_joins(:subscriptions)
                        .where("subscriptions.id is null").first
            expect(team).to receive(:delete_stripe_customer_if_stripe_on)
            hiring_managers = team.hiring_managers

            hiring_managers.each do |hiring_manager|
                expect(hiring_manager.hiring_team_id).to eq(team.id)
                hiring_manager.hiring_application.update(status: 'pending')
            end

            team.destroy!

            hiring_managers.each do |hiring_manager|
                expect(hiring_manager.reload.hiring_team_id).to be_nil
            end
        end

    end

    describe "owner" do

        it "should be invalid if the owner is not a team member" do
            team = HiringTeam.first
            user = User.where(hiring_team_id: nil).first
            team.owner = user
            expect(team.valid?).to be(false)
            expect(team.errors.full_messages).to include("Owner must be a member of the team")

        end

        it "should be valid if the owner is a team member" do
            team = HiringTeam.first
            user = User.where(hiring_team_id: nil).first
            user.hiring_team_id = team.id
            user.save!
            team.owner = user
            expect(team.valid?).to be(true)

        end

    end

    describe "should_anonymize_interest?" do

        it "should be false if has_full_access" do
            hiring_team = HiringTeam.first
            expect(hiring_team).to receive(:has_full_access?).and_return(true)
            expect(hiring_team).not_to receive(:freemium_interest_ids)
            expect(hiring_team.should_anonymize_interest?(CandidatePositionInterest.new)).to be(false)
        end

        it "should be true if !has_full_access and not in freemium_interest_ids" do
            hiring_team = HiringTeam.first
            interest_id = SecureRandom.uuid
            expect(hiring_team).to receive(:has_full_access?).at_least(1).times.and_return(false)
            expect(hiring_team).to receive(:freemium_interest_ids).at_least(1).times.and_return([interest_id])
            expect(hiring_team.should_anonymize_interest?(CandidatePositionInterest.new(
                id: interest_id
            ))).to be(false)
            expect(hiring_team.should_anonymize_interest?(CandidatePositionInterest.new(
                id: SecureRandom.uuid
            ))).to be(true)
        end

    end

    describe "freemium_interests" do
        attr_accessor :hiring_team, :has_full_access, :interests, :hiring_manager, :open_position

        before(:each) do
            @hiring_manager = users(:hiring_manager_with_team)
            @open_position = @hiring_manager.open_positions.first
            @hiring_team = @hiring_manager.hiring_team
            CandidatePositionInterest.delete_all

            allow_any_instance_of(HiringTeam).to receive(:has_full_access?) do
                has_full_access
            end
        end

        it "should order position interests by admin_status and hiring_manager_priority and anonymize all but the first three when not in good standing" do
            opts = [
                {
                    admin_status: 'outstanding',
                    hiring_manager_priority: 3
                },
                {
                    admin_status: 'outstanding',
                    hiring_manager_priority: 4
                },
                {
                    admin_status: 'reviewed',
                    hiring_manager_priority: 1
                },
                {
                    admin_status: 'reviewed',
                    hiring_manager_priority: 2
                }
            ]
            interests = create_interests(opts)
            self.has_full_access = false

            # first three show the full profile.  4th one is anonymized
            assert_freemium_interests(hiring_team, [
                interests[0],
                interests[1],
                interests[2]
            ])
        end

        it "should move interests on which the hiring manager has already taken action to the top when deciding what to anonymize" do
            opts = [
                {
                    admin_status: 'reviewed',
                    hiring_manager_status: 'invited'
                },
                {
                    admin_status: 'outstanding',
                    hiring_manager_priority: 1
                },
                {
                    admin_status: 'outstanding',
                    hiring_manager_priority: 2
                },
                {
                    admin_status: 'outstanding',
                    hiring_manager_priority: 3
                }
            ]
            interests = create_interests(opts)
            self.has_full_access = false

            # first three show the full profile.  4th one is anonymized
            assert_freemium_interests(hiring_team, [
                interests[0],
                interests[1],
                interests[2]
            ])
        end

        it "should show more than 3 non-anonymized if more than 3 have been reviewed by the hiring manager" do
            # this could happen if the hiring manager had full access in the past, and now has failed to pay.
            # We should still show full profiles for anything ze acted on in the past
            opts = [
                {
                    admin_status: 'reviewed',
                    hiring_manager_status: 'invited'
                },
                {
                    admin_status: 'reviewed',
                    hiring_manager_status: 'invited'
                },
                {
                    admin_status: 'reviewed',
                    hiring_manager_status: 'invited'
                },
                {
                    admin_status: 'reviewed',
                    hiring_manager_status: 'invited'
                },
                {
                    admin_status: 'outstanding',
                    hiring_manager_priority: 1
                }
            ]
            interests = create_interests(opts)
            self.has_full_access = false

            # all reviewed ones show the full profile.  last one is anonymized
            assert_freemium_interests(hiring_team, [
                interests[0],
                interests[1],
                interests[2],
                interests[3]
            ])
        end

        it "should ignore interests rejected by candidate" do
            opts = [
                {
                    candidate_status: 'rejected',
                    hiring_manager_priority: 1
                },
                {
                    hiring_manager_priority: 2
                },
                {
                    hiring_manager_priority: 2
                },
                {
                    hiring_manager_priority: 2
                },
                {
                    hiring_manager_priority: 3
                }
            ]
            interests = create_interests(opts)
            self.has_full_access = false

            # first three that are accepted by the candidate show the full profile.  4th one is anonymized
            # rejected one is anonymized, but it doesn't make any practical difference, since hm cannot see it
            assert_freemium_interests(hiring_team, [
                interests[1],
                interests[2],
                interests[3]
            ])
        end

        it "should ignore interests with hiring_manager_status=hidden" do
            opts = [
                {
                    hiring_manager_status: 'hidden',
                    hiring_manager_priority: 1
                },
                {
                    hiring_manager_priority: 2
                },
                {
                    hiring_manager_priority: 2
                },
                {
                    hiring_manager_priority: 2
                },
                {
                    hiring_manager_priority: 3
                }
            ]
            interests = create_interests(opts)
            self.has_full_access = false

            # first three that are not hidden show the full profile.  4th one is anonymized
            # hidden one is anonymized, but it doesn't make any practical difference, since hm cannot see it
            assert_freemium_interests(hiring_team, [
                interests[1],
                interests[2],
                interests[3]
            ])
        end


        it "should work with multiple open_positions" do
            @open_position_2 = @hiring_manager.open_positions.reject { |p| p == @open_position}.first
            @open_position_2.candidate_position_interests.destroy_all

            opts = [
                # interests for position 1
                {
                    admin_status: 'reviewed',
                    hiring_manager_priority: 1
                },
                {
                    admin_status: 'reviewed',
                    hiring_manager_priority: 2
                },
                {
                    admin_status: 'reviewed',
                    hiring_manager_priority: 3
                },
                {
                    admin_status: 'reviewed',
                    hiring_manager_priority: 4
                }
            ]
            interests_for_position_1 = create_interests(opts)

            opts = [
                # interests for position 2
                {
                    admin_status: 'reviewed',
                    hiring_manager_priority: 1,
                    open_position_id: @open_position_2.id
                },
                {
                    admin_status: 'reviewed',
                    hiring_manager_priority: 2,
                    open_position_id: @open_position_2.id
                },
                {
                    admin_status: 'reviewed',
                    hiring_manager_priority: 3,
                    open_position_id: @open_position_2.id
                },
                {
                    admin_status: 'reviewed',
                    hiring_manager_priority: 4,
                    open_position_id: @open_position_2.id
                }
            ]
            interests_for_position_2 = create_interests(opts)
            self.has_full_access = false


            assert_freemium_interests(hiring_team, [
                interests_for_position_1[0],
                interests_for_position_1[1],
                interests_for_position_1[2],


                interests_for_position_2[0],
                interests_for_position_2[1],
                interests_for_position_2[2]
            ])
        end

        def assert_freemium_interests(hiring_team, expected_freemium_interests)
            expect(hiring_team.freemium_interest_ids.to_a).to match_array(expected_freemium_interests.map(&:id))
            expect(hiring_team.freemium_candidate_ids.to_a).to match_array(expected_freemium_interests.map(&:candidate_id).uniq)
        end

        def create_interests(opts)
            existing_ids = CandidatePositionInterest.pluck(:candidate_id)
            candidates = CareerProfile.active.where.not(user_id: existing_ids).limit(5).map(&:user)
            interests = []

            # we shuffle these to make sure that the specs are not just accidentally
            # passing because of the order the options are entered into the db.  But the
            # interests get returned in the order in which the options were passed in
            opts.each { |entry| entry['id'] = SecureRandom.uuid } # make each entry uniq so that .index works below
            shuffled = opts.shuffle
            shuffled.each_with_index do |entry, i|
                candidate = candidates[i]
                index = opts.index(entry)
                interests[index] = CandidatePositionInterest.create!({
                    candidate_id: candidate.id,
                    open_position_id: @open_position.id,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'pending'
                }.merge(entry))
            end
            interests
        end
    end

    describe "as_json" do
        it "should send back hiring_managers if requested" do
            team = HiringTeam.first
            json = team.as_json({fields: ['hiring_managers']})
            expect(json['hiring_managers'].to_a.map { |e| e['id']}).to match_array(team.hiring_managers.pluck(:id))
        end

        it "should send expected stripe_plans to users based on hiring_plan" do
            team = HiringTeam.first
            team.hiring_plan = HiringTeam::HIRING_PLAN_LEGACY
            expect(team.as_json['stripe_plans'].map { |plan| plan[:id] }).to match_array(["hiring_per_match_20", "hiring_unlimited_300"])

            team.hiring_plan = nil
            expect(team.as_json['stripe_plans'].map { |plan| plan[:id] }).to match_array(["hiring_pay_per_post_89", "hiring_unlimited_w_sourcing_249"])

            team.hiring_plan = HiringTeam::HIRING_PLAN_PAY_PER_POST
            expect(team.as_json['stripe_plans'].map { |plan| plan[:id] }).to match_array(["hiring_pay_per_post_89", "hiring_unlimited_w_sourcing_249"])

            team.hiring_plan = HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING
            expect(team.as_json['stripe_plans'].map { |plan| plan[:id] }).to match_array(["hiring_pay_per_post_89", "hiring_unlimited_w_sourcing_249"])
        end

        it "should send expected stripe_plans to non-legacy users" do

        end
    end

    describe "ensure_domain" do

        it "should add a domain to the team only if there is not one already" do
            owner_ids = HiringTeam.pluck(:owner_id)
            user = User.where.not(id: owner_ids).where(hiring_team_id: nil).first
            domain = "#{SecureRandom.uuid}.com"
            user.email = "someone@#{domain}"
            user.save!

            user.hiring_team = HiringTeam.new(
                owner_id: user.id,
                title: 'title'
            )
            user.save!
            expect(user.hiring_team.domain).to eq(domain)

            other_domain = "#{SecureRandom.uuid}.com"
            user = User.where.not(id: owner_ids + [user.id]).first
            user.hiring_team = HiringTeam.new(
                owner_id: user.id,
                domain: other_domain,
                title: 'title'
            )
            user.save!
            expect(user.hiring_team.domain).to eq(other_domain)
        end

    end

    describe "create_hiring_plan_changed_event" do

        attr_accessor :hiring_team

        before :each do
            owner_ids = HiringTeam.pluck(:owner_id)
            user = User.where.not(id: owner_ids).where(hiring_team_id: nil).first
            user.hiring_team = HiringTeam.new(
                owner_id: user.id,
                title: 'title'
            )
            user.save!
            @hiring_team = user.hiring_team.reload
        end

        it "should log an event if hiring_plan has not changed" do
            expect(Event).not_to receive(:create_server_event!)
            hiring_team.save!
        end

        it "should log an event if hiring_plan has changed" do
            old_hiring_plan = hiring_team.hiring_plan
            hiring_team.hiring_plan = HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING
            event = Event.new
            expect(Event).to receive(:create_server_event!).with(
                anything,
                hiring_team.owner_id,
                'hiring_team:hiring_plan_changed',
                {
                    hiring_plan: HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING,
                    old_hiring_plan: old_hiring_plan
                }
            ).and_return(event)
            hiring_team.save!
        end
    end

    def ensure_fresh_hiring_team(hiring_manager)
        if hiring_manager.hiring_team
            hiring_manager.hiring_team.owner_id = nil
            hiring_manager.hiring_team.save!
        end
        hiring_manager.hiring_team = HiringTeam.create!({title: "Fresh Team", domain: 'freshteam.com'})
        hiring_manager.save!
    end

    def find_fresh_hiring_manager
        User.left_outer_joins(:candidate_relationships, :hiring_team).where("hiring_relationships.id is null and hiring_teams.id is null").first
    end
end
