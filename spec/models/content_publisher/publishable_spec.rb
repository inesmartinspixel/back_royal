require 'spec_helper'

describe ContentPublisher::Publishable do

    describe "all_published" do

        # ran into issues with this association when updating to rails 5.2 so added a spec for it
        it "should work to preload the published version" do
            messed_up = Lesson::Stream.all_published
                .map(&:published_version)
                .detect do |published_version|
                    stream = Lesson::Stream.find(published_version['id'])
                    reloaded_published_version = stream.content_publisher.published_version

                    # find a stream where the published version returned above does not match the
                    # one from the content publisher (There should not be any like that. That's the bug)
                    reloaded_published_version['version_id'] != published_version['version_id']
                end
            expect(messed_up).to be_nil
        end

    end

    describe "make_derived_content_table_updates" do

        class PublishableThing
            def self.has_many(*args); end
            def self.before_destroy(*args); end
            include ContentPublisher::Publishable
        end

        # we can't mock update_on_content_change directly, since
        # the code is introspecting on the expected arguments, so
        # we mock update_on_content_change_called_with
        class ClassThatExpectsIdentifiers
            def self.update_on_content_change(klass:, identifiers:)
                self.update_on_content_change_called_with(klass, identifiers)
            end
        end

        class ClassThatExpectsVersionPairs
            def self.update_on_content_change(klass:, version_pairs:)
                self.update_on_content_change_called_with(klass, version_pairs)
            end
        end

        class ClassThatExpectsBoth
            def self.update_on_content_change(klass:, identifiers:, version_pairs:)
                self.update_on_content_change_called_with(klass, identifiers, version_pairs)
            end
        end

        it "should include identifiers when needed" do
            expect(ClassThatExpectsIdentifiers).to receive(:update_on_content_change_called_with).with(PublishableThing, [1,2,3,4,5])
            PublishableThing.make_derived_content_table_updates([
                # old version and new version with different identifiers
                {klass_to_update: ClassThatExpectsIdentifiers, identifier: :identifier, old_version: {identifier: 1}, new_version: {identifier: 2}},

                # old version and new version with same identifier
                {klass_to_update: ClassThatExpectsIdentifiers, identifier: :identifier, old_version: {identifier: 3}, new_version: {identifier: 3}},

                # no old version
                {klass_to_update: ClassThatExpectsIdentifiers, identifier: :identifier, old_version: nil, new_version: {identifier: 4}},

                # no new version
                {klass_to_update: ClassThatExpectsIdentifiers, identifier: :identifier, old_version: {identifier: 5}, new_version: nil},
            ])

        end

        it "should include version_pairs when needed" do
            expect(ClassThatExpectsVersionPairs).to receive(:update_on_content_change_called_with).with(PublishableThing, [
                [{identifier: 1}, {identifier: 2}],
                [{identifier: 3}, {identifier: 3}],
                [nil, {identifier: 4}],
                [{identifier: 5}, nil]
            ])
            PublishableThing.make_derived_content_table_updates([
                # old version and new version with different identifiers
                {klass_to_update: ClassThatExpectsVersionPairs, identifier: :identifier, old_version: {identifier: 1}, new_version: {identifier: 2}},

                # old version and new version with same identifier
                {klass_to_update: ClassThatExpectsVersionPairs, identifier: :identifier, old_version: {identifier: 3}, new_version: {identifier: 3}},

                # no old version
                {klass_to_update: ClassThatExpectsVersionPairs, identifier: :identifier, old_version: nil, new_version: {identifier: 4}},

                # no new version
                {klass_to_update: ClassThatExpectsVersionPairs, identifier: :identifier, old_version: {identifier: 5}, new_version: nil},
            ])
        end

        it "should include both when needed" do

            expect(ClassThatExpectsBoth).to receive(:update_on_content_change_called_with).with(PublishableThing, [1,2,3,4,5], [
                [{identifier: 1}, {identifier: 2}],
                [{identifier: 3}, {identifier: 3}],
                [nil, {identifier: 4}],
                [{identifier: 5}, nil]
            ])
            PublishableThing.make_derived_content_table_updates([
                # old version and new version with different identifiers
                {klass_to_update: ClassThatExpectsBoth, identifier: :identifier, old_version: {identifier: 1}, new_version: {identifier: 2}},

                # old version and new version with same identifier
                {klass_to_update: ClassThatExpectsBoth, identifier: :identifier, old_version: {identifier: 3}, new_version: {identifier: 3}},

                # no old version
                {klass_to_update: ClassThatExpectsBoth, identifier: :identifier, old_version: nil, new_version: {identifier: 4}},

                # no new version
                {klass_to_update: ClassThatExpectsBoth, identifier: :identifier, old_version: {identifier: 5}, new_version: nil},
            ])
        end

    end

    describe "refresh_content_views_and_tables_on_publish_change" do
        it "should work if publishing for the first time" do
            cohort = Cohort.where(was_published: false).first
            expect(RefreshMaterializedContentViews).to receive(:refresh)
            expect_derived_content_table_updates(nil, 'new version')
            cohort.name = 'new version'
            cohort.publish!
        end

        it "should work if something published is changed, saved multiple times, then published later" do
            cohort = Cohort.all_published.first
            orig_name = cohort.published_version.name

            expect(RefreshMaterializedContentViews).to receive(:refresh)
            expect_derived_content_table_updates(orig_name, 'version 3')

            cohort.update!(name: "version 2")
            cohort.update!(name: 'version 3')
            cohort.publish!
        end

        it "should work if something published is unpublished" do
            cohort = Cohort.all_published.first
            orig_name = cohort.published_version.name

            expect(RefreshMaterializedContentViews).to receive(:refresh)
            expect_derived_content_table_updates(orig_name, nil)
            cohort.unpublish!
        end

        def expect_derived_content_table_updates(old_version_name, new_version_name)
            expect(IsDerivedContentTable).to receive(:get_derived_content_table_update_triggers) do |args|
                expect(args[:content_klass]).to be(Cohort)
                expect(args[:old_version]&.name).to eq(old_version_name)
                expect(args[:new_version]&.name).to eq(new_version_name)
                expect(args[:access_groups_did_change]).to be(false)
                :triggers
            end
            expect(Cohort).to receive(:make_derived_content_table_updates).with(:triggers)
        end
    end

    describe "after_access_group_update" do

        it "should update derived content tables when adding a group" do
            stream = Lesson::Stream.all_published.first
            orig_group_ids = stream.access_groups.pluck(:id)
            access_group = AccessGroup.where.not(id: orig_group_ids).first
            stream.access_groups << access_group
            expect(RefreshMaterializedContentViews).to receive(:refresh)
            expect(stream).to receive(:make_all_necessary_derived_content_table_updates).with(
                old_version: stream.published_version,
                new_version: stream.published_version,
                access_groups_did_change: true
            )
            stream.after_access_group_update(orig_group_ids)

        end

        it "should update derived content tables when removing a group" do
            stream = Lesson::Stream.all_published.joins(:locale_pack => :access_groups).first
            orig_group_ids = stream.access_groups.pluck(:id)
            stream.access_groups.delete(stream.access_groups.last)
            expect(RefreshMaterializedContentViews).to receive(:refresh)
            expect(stream).to receive(:make_all_necessary_derived_content_table_updates).with(
                old_version: stream.published_version,
                new_version: stream.published_version,
                access_groups_did_change: true
            )
            stream.after_access_group_update(orig_group_ids)
        end

        it "should do nothing if groups are not changing" do
            stream = Lesson::Stream.all_published.joins(:locale_pack => :access_groups).first
            orig_group_ids = stream.access_groups.pluck(:id)
            expect(RefreshMaterializedContentViews).not_to receive(:perform_now)
            expect(stream).not_to receive(:make_all_necessary_derived_content_table_updates)
            stream.after_access_group_update(orig_group_ids)
        end

        it "should do nothing if not published" do
            stream = Lesson::Stream.first
            expect(stream).to receive(:has_published_version?).and_return(false)
            orig_group_ids = stream.access_groups.pluck(:id)
            access_group = AccessGroup.where.not(id: orig_group_ids).first
            stream.access_groups << access_group
            expect(RefreshMaterializedContentViews).not_to receive(:perform_now)
            expect(stream).not_to receive(:make_all_necessary_derived_content_table_updates)
            stream.after_access_group_update(orig_group_ids)
        end

    end
end
