require 'spec_helper'

describe AutoSuggestOption::ToJsonFromApiParams do

    fixtures(:users, :career_profiles)

    before(:each) do
    end

    it "should have expected keys" do
        option = SkillsOption.first
        result = get_single_option_json(option, least_restrictive_params)
        expect(result.keys).to match_array([
            "id", "text", "locale", "created_at", "updated_at", "suggest", "type"
        ])
    end

    it "should respect fields" do
        option = SkillsOption.first
        result = get_single_option_json(option, least_restrictive_params({fields: ['id']}))
        expect(result.keys).to match_array([
            "id"
        ])
    end

    it "should respect except" do
        option = SkillsOption.first
        result = get_single_option_json(option, least_restrictive_params({except: ['locale']}))
        expect(result.keys.include?('locale')).to be(false)
    end


    describe "keys with simple values" do

        it "should have expected values" do
            option = SkillsOption.first

            result = get_single_option_json(option, least_restrictive_params)
            {
                "id" => option.id,
                "text" => option.text,
                "locale" => option.locale
            }.each do |key, value|
                expect(value).not_to be_nil, "Expecting all simple values to be set. #{key.inspect} is not"
                expect(result[key]).to eq(value), "Unexpected value for #{key.inspect}. expected #{value.inspect} got #{result[key].inspect}"
            end
        end
    end

    describe "filters" do

        it "should filter by search_text" do

            option = SkillsOption.create!({locale: 'en', text: 'test 1', suggest: true})
            another_option = SkillsOption.create!({locale: 'en', text: 'test 2', suggest: true})

            expect(another_option).not_to be_nil
            expect(another_option.text).not_to eq(option.text)

            result = get_json(least_restrictive_params({
                filters: {
                    search_text: 'test'
                }
            }))

            expect(result.map { |e| e['id']}).to include(option.id)
            expect(result.map { |e| e['id']}).to include(another_option.id)

            result = get_json(least_restrictive_params({
                filters: {
                    search_text: 'test 2'
                }
            }))
            expect(result.map { |e| e['id']}).not_to include(option.id)
            expect(result.map { |e| e['id']}).to include(another_option.id)
        end


        it "should filter by locale" do
            option = SkillsOption.create!({locale: 'es', text: 'test 1', suggest: true})

            result = get_json(least_restrictive_params({
                filters: {
                    search_text: 'test',
                    in_locale_or_en: 'es'
                }
            }))
            expect(result.map { |e| e['id']}).to eq([option.id])
        end

        it "should support student network interests" do
            option = StudentNetworkInterestsOption.first
            result = get_single_option_json(option, least_restrictive_params({filters: {type: 'student_network_interests'} }))
            expect(result.keys).to match_array([
                "id", "created_at", "updated_at", "type", "suggest", "text", "locale"
            ])
        end

        it "should support organizational types" do
            option = ProfessionalOrganizationOption.first
            result = get_single_option_json(option, least_restrictive_params({filters: {type: 'professional_organization'} }))
            expect(result.keys).to match_array([
                "id", "created_at", "updated_at", "type", "suggest", "text", "locale", "source_id", "image_url"
            ])
        end

        it "should require suggested if not admin" do
            option = SkillsOption.first
            invalid_params = least_restrictive_params()
            invalid_params[:filters] = invalid_params[:filters].except!(:suggested)
            expect(Proc.new {
                get_single_option_json(option, invalid_params, false)
            }).to raise_error("Only suggested options are currently supported for non-admins.")

        end

        it "should not require suggested if an admin" do
            option = SkillsOption.first
            invalid_params = least_restrictive_params()
            invalid_params[:filters] = invalid_params[:filters].except!(:suggested)
            expect(Proc.new {
                get_single_option_json(option, invalid_params, true)
            }).not_to raise_error
        end

        # FIXME: https://trello.com/c/y6IM4obh
        # it "should raise if search_text does not meet minimum requirements" do
        #     expect(Proc.new {
        #         result = get_json(least_restrictive_params({
        #             filters: {
        #                 search_text: 'te',
        #                 in_locale_or_en: 'en'
        #             }
        #         }))
        #     }).to raise_error("Must provide minimum required characters for search_text.")
        # end


    end


    describe "with limit" do

        it "should limit results" do
            result = get_json(least_restrictive_params({
                limit: 1
            }))
            expect(result.size).to eq(1)
        end

    end


    def least_restrictive_params(overrides = {})
        {
            filters: {
                type: 'skills',
                in_locale_or_en: 'en',
                suggested: true
            }
        }.deep_merge(overrides);
    end

    def get_json(params, admin = false)
        json = AutoSuggestOption::ToJsonFromApiParams.new(params, admin).json
        ActiveSupport::JSON.decode(json)
    end


    def get_single_option_json(option, params, admin = false)
        expect(option).not_to be_nil
        results = get_json(params, admin)
        results_for_option = results.select { |r| r['id'] == option.attributes['id'] }
        expect(results_for_option.size).to be <=1
        results_for_option.first
    end

end
