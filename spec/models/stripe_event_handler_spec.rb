require 'spec_helper'

describe StripeEventHandler do

    attr_reader :user

    include StripeHelper

    fixtures(:users)

    attr_accessor :user

    # FIXME: Remove once stripe_ruby_mock supports 2018-02-05+
    MockBillingTransactionOutcome = Struct.new(:reason, :seller_message)

    before(:each) do
        @user = users(:user_with_subscriptions_enabled)
        create_default_plan
        create_customer_with_default_source(user)

        # apparently some of these things are not supported in stripe-ruby-mock
        allow_any_instance_of(Stripe::Invoice).to receive(:number).and_return("invoice_number")
        allow_any_instance_of(Stripe::Invoice).to receive(:hosted_invoice_url).and_return("invoice_hosted_invoice_url")
        allow_any_instance_of(Stripe::Invoice).to receive(:invoice_pdf).and_return("invoice_invoice_pdf")
    end

    # https://trello.com/c/rl6mBCme
    it "should not reprocess an event that is already processed" do
        orig_handlers = StripeEventHandler.handlers
        event_handler = orig_handlers['all'].detect { |handler| handler[:order] == 9000 }
        user = User.first
        user.update(name: '0')
        i = 0
        expect(StripeEventHandler).to receive(:handlers).at_least(1).times.and_return({
            'all' => [
                event_handler,
                {
                    order: 1,
                    block: Proc.new {
                        user.update(name: ((i+=1).to_s))
                    }
                },
                {
                    order: 999999999,
                    block: Proc.new {
                        user.update(name: ((i+=1).to_s))
                    }
                }
            ],
            'test.event' => []
        })
        orig_count = Event.count
        handle

        # the first time, evrything runs normally
        expect(Event.count).to be(orig_count + 1)
        expect(user.reload.name).to eq("2")
        handle

        # the second time, nothing is saved
        expect(Event.count).to be(orig_count + 1)
        expect(user.reload.name).to eq("2")
    end

    describe "with no owner" do

        it "should log a warning to sentry and trigger no handlers in livemode" do
            event = get_event
            expect(event).to receive(:livemode).and_return(true)
            @user.destroy
            expect_any_instance_of(StripeEventHandler).not_to receive(:with_raven_context)
            expect(Raven).to receive(:capture_exception).with("No owner found for customer #{@user.id}")
            handle(event)
        end

        it "should do nothing in testmode" do
            event = get_event
            expect(event).to receive(:livemode).and_return(false)
            @user.destroy
            expect_any_instance_of(StripeEventHandler).not_to receive(:with_raven_context)
            expect(Raven).not_to receive(:capture_exception)
            handle(event)
        end
    end

    describe "subscription helper" do

        # see StripeEventHandler#subscription
        it "should not create a subscription if it has already been canceled" do
            @stripe_subscription = Subscription.create_stripe_subscription(user, @default_plan.id)
            event = get_event({
                "type" => "customer.subscription.created",
                "data"=> { "object" => @stripe_subscription.as_json }
            })
            @stripe_subscription.delete

            expect {
                handle(event)
            }.not_to change {
                Subscription.count
            }
        end

        # see StripeEventHandler#subscription
        it "should not create a subscription if it is incomplete" do
            @stripe_subscription = Subscription.create_stripe_subscription(user, @default_plan.id)
            event = get_event({
                "type" => "customer.subscription.created",
                "data"=> { "object" => @stripe_subscription.as_json.merge('status' => 'incomplete') }
            })

            expect {
                handle(event)
            }.not_to change {
                Subscription.count
            }
        end

        # see Subscription#destroy_if_canceled
        it "should handle a race condition that could otherwise leave a canceled subscription in our db"  do
            @subscription = nil
            @stripe_subscription = Subscription.create_stripe_subscription(user, @default_plan.id)
            allow_any_instance_of(StripeEventHandler).to receive(:subscription) do
                if @subscription.nil?

                    # Make it look like another process created and deleted
                    # a record for this same stripe subscription, but this
                    # process doesn't know about that yet and so still creates
                    # one and returns it
                    ActiveRecord::Base.connection.execute(%Q~
                        insert into subscriptions_versions
                        (id, stripe_subscription_id, operation, version_created_at, stripe_plan_id)
                        values
                        ('#{SecureRandom.uuid}', '#{@stripe_subscription.id}', 'I', now(), '#{@default_plan.id}'),
                        ('#{SecureRandom.uuid}', '#{@stripe_subscription.id}', 'D', now(), '#{@default_plan.id}')
                    ~)
                    @subscription = Subscription.create_or_reconcile_from_stripe_subscription!(@user, @stripe_subscription)
                end
                @subscription
            end
            event = get_event({
                "type" => "customer.subscription.created",
                "data"=> { "object" => @stripe_subscription.as_json }
            })

            expect {
                handle(event)
            }.not_to change {
                Subscription.count
            }

            # The mocked insert and delete we added are responsible for the first
            # insert and delete operations.
            #
            # Handling the create event should result in another insert and another
            # delete.
            assert_operations(['I', 'D', 'I', 'D'], @stripe_subscription.id)
        end

        def assert_operations(expected_operations, stripe_subscription_id)
            versions = ActiveRecord::Base.connection.execute("select operation from subscriptions_versions where stripe_subscription_id='#{stripe_subscription_id}'").to_a
            expect(versions.map { |v| v['operation']}).to match_array(expected_operations)
        end

    end

    describe "customer.subscription.deleted handling" do

        it "should delete the subscription" do
            subscription = create_subscription(user)
            handle_delete(subscription)
            expect(Subscription.find_by_id(subscription.id)).to be_nil
        end

        it "should log to slack" do
            subscription = create_subscription(user)
            stripe_subscription = subscription.stripe_subscription
            stripe_subscription.metadata['cancellation_reason'] = 'custom_reason'
            stripe_subscription.save
            expect_log_to_slack.with(
                "Subscription cancelled for #{user.email} with reason - custom_reason (#{@default_plan[:nickname]})",
                "#F8000C")
            handle_delete(subscription)
        end

        it "should do nothing if there is no subscription" do
            orig_count = Subscription.count
            handle({
                "type" => "customer.subscription.deleted",
                "data" => {
                    "object" => {
                        "id" => SecureRandom.uuid
                    }
                }
            })
            expect(Subscription.count).to eq(orig_count)
        end

        it "should not processs an incomplete Subscription" do
            expect_any_instance_of(StripeEventHandler).not_to receive(:log_to_slack)
            handle({
                "type" => "customer.subscription.deleted",
                "data" => {
                    "object" => {
                        "id" => SecureRandom.uuid,
                        "status" => "incomplete"
                    }
                }
            })
        end

        def handle_delete(subscription, overrides = {})
          stripe_subscription = subscription.stripe_subscription
          json = stripe_subscription.as_json
          subscription.stripe_subscription.delete
          handle({
                "type" => "customer.subscription.deleted",
                "data" => {
                    "object" => json.merge(overrides)
                }
            })
        end

    end

    describe "customer.subscription.created handling" do

        before(:each) do
            @stripe_subscription = Subscription.create_stripe_subscription(user, @default_plan.id)
            get_event({
                "type" => "customer.subscription.created",
                "data"=> { "object" => @stripe_subscription.as_json }
            })
        end

        it "should call create_or_reconcile_from_stripe_subscription and work if the application has not already been reconciled" do
            expect(Subscription).to receive(:create_or_reconcile_from_stripe_subscription!).and_call_original
            handle(@stripe_event)
        end

        it "should call create_or_reconcile_from_stripe_subscription and work if the application has already been reconciled" do
            Subscription.create_or_reconcile_from_stripe_subscription!(user, @stripe_subscription)
            expect(Subscription).to receive(:create_or_reconcile_from_stripe_subscription!).and_call_original
            handle(@stripe_event)
        end

        it "should not processs an incomplete Subscription" do
            expect_any_instance_of(StripeEventHandler).not_to receive(:log_to_slack)
            event = get_event({
                "type" => "customer.subscription.created",
                "data"=> { "object" => @stripe_subscription.as_json.merge('status' => 'incomplete') }
            })
            handle(@stripe_event)
        end

        it "should log to slack" do
            expect_log_to_slack.with(
                "#{user.email} started a subscription (#{@default_plan.nickname})",
                "#F3E000")
            handle(@stripe_event)
        end

        it "should set application stripe_plan_id and registered" do
            handle(@stripe_event)
            application = user.application_for_stripe_plan_id(@default_plan.id)
            expect(application.reload.registered).to eq(true)
            expect(application.reload.stripe_plan_id).to eq(@default_plan.id)
        end

    end

    describe "customer.subscription.updated handling" do

        before(:each) do
            @stripe_subscription = Subscription.create_for_owner(owner: user, stripe_plan_id: @default_plan.id).stripe_subscription
        end

        it "should call create_or_reconcile_from_stripe_subscription" do
            get_event({"type" => "customer.subscription.updated", "data"=> { "object" => {"id" => @stripe_subscription.id } } })
            expect(Subscription).to receive(:create_or_reconcile_from_stripe_subscription!)
            handle(@stripe_event)
        end

        it "should not call create_or_reconcile_from_stripe_subscription if there's a cancellation_reason supplied" do
            get_event({"type" => "customer.subscription.updated", "data"=> { "object" => { "id" => @stripe_subscription.id, "metadata" => {"cancellation_reason": "some_reason"}}}})
            expect(Subscription).not_to receive(:create_or_reconcile_from_stripe_subscription!)
            handle(@stripe_event)
        end

        it "should not processs an incomplete Subscription" do
            get_event({"type" => "customer.subscription.updated", "data"=> { "object" => { "id" => @stripe_subscription.id, "status" =>  "incomplete" }}})
            expect(Subscription).not_to receive(:create_or_reconcile_from_stripe_subscription!)
            handle(@stripe_event)
        end


    end

    describe 'invoice.payment_failed notifications' do

        before(:each) do
            user.ensure_stripe_customer
            invoices = stub_invoices(user, [
                {date: Time.at(0), total: 1000, id: 'paid_subscription_invoice', charge: '12345'}
            ])
            get_event({
                "type" => "invoice.payment_failed",
                "data" => {
                    "object" => invoices[0].as_json
                }
            })
            @stripe_subscription_id = @stripe_event.data.object.subscription

            allow_any_instance_of(Stripe::Charge).to receive(:outcome) do
                @outcome
            end
        end

        it "should log to Slack with bank outcome support" do
            charge = Stripe::Charge.create(customer: @user.id, livemode: false, amount: 1000, currency: 'usd')

            @outcome = MockBillingTransactionOutcome.new("insufficient_funds", "The bank returned the decline code `insufficient_funds`.")
            #expect_any_instance_of(Stripe::Charge).to receive(:outcome).at_least(1).times.and_return(MockBillingTransactionOutcome.new("insufficient_funds", "The bank returned the decline code `insufficient_funds`."))

            expect(Stripe::Charge).to receive(:retrieve).with('12345').and_return(charge)
            expect_log_to_slack.with(
                "A payment of *$10.00* from *#{user.email}* was declined with reason - The bank returned the decline code `insufficient_funds`. (insufficient_funds)",
                "#F8000C"
            )
            handle @stripe_event
        end

        it "should log to Slack with generic reason support if not bank outcome" do
            charge = Stripe::Charge.create(customer: @user.id, livemode: false, amount: 1000, currency: 'usd', failure_code: "card_declined", failure_message: "Your card has insufficient funds.")

            #expect_any_instance_of(Stripe::Charge).to receive(:outcome).at_least(1).times.and_return(nil)

            expect(Stripe::Charge).to receive(:retrieve).with('12345').and_return(charge)
            expect_log_to_slack.with(
                "A payment of *$10.00* from *#{user.email}* was declined with reason - Your card has insufficient funds. (card_declined)",
                "#F8000C"
            )
            handle @stripe_event
        end

        it "should not process invoices where credits were applied" do

            # Don't even think this is theoretically possible in Stripe, since it reflects a Stripe-internal value
            invoices = stub_invoices(user, [
                {date: Time.at(0), total: -1000, id: 'paid_subscription_invoice'}
            ])
            get_event({
                "type" => "invoice.payment_failed",
                "data" => {
                    "object" => invoices[0].as_json
                }
            })

            expect_any_instance_of(StripeEventHandler).not_to receive(:log_to_slack)
            handle @stripe_event
        end

        it "should log to slack and notify sentry if there is no charge on the invoice, the user has a card, and the invoice amount_paid is not 0" do
            invoices = stub_invoices(user, [
                {date: Time.at(0), total: 1000, id: 'paid_subscription_invoice', charge: nil, amount_paid: 1000}
            ])
            get_event({
                "type" => "invoice.payment_failed",
                "data" => {
                    "object" => invoices[0].as_json
                }
            })
            expect(user.default_stripe_card).not_to be_nil # sanity check
            expect(invoices[0].amount_paid).not_to eq(0)

            # we believe this could ever happen, so we tell sentry
            expect(Raven).to receive(:capture_exception).with('No charge on invoice event', {
                extra: {
                    invoice_id:  invoices[0].id
                }
            })

            expect_log_to_slack.with(
                "A payment of *$10.00* from *#{user.email}* was declined with reason - No reason provided",
                "#F8000C"
            )
            handle @stripe_event
        end

        it "should log to slack and if there is no charge because the user has no card" do
            invoices = stub_invoices(user, [
                {date: Time.at(0), total: 1000, id: 'paid_subscription_invoice', charge: nil}
            ])
            allow_any_instance_of(Stripe::Invoice).to receive(:charge).and_return(nil)
            expect_any_instance_of(User).to receive(:default_stripe_card).at_least(1).times.and_return(nil)
            get_event({
                "type" => "invoice.payment_failed",
                "data" => {
                    "object" => invoices[0].as_json
                }
            })

            expect(Raven).not_to receive(:capture_exception)

            expect_log_to_slack.with(
                "A payment of *$10.00* from *#{user.email}* was declined with reason - User has no default card",
                "#F8000C"
            )
            handle @stripe_event
        end


    end

    describe 'invoice.payment_succeeded notifications' do

        before(:each) do
            user.ensure_stripe_customer
        end

        before(:each) do
            invoices = stub_invoices(user, [
                {date: Time.at(0), total: 1000, id: 'paid_subscription_invoice', paid: true }
            ])
            get_event({
                "type" => "invoice.payment_succeeded",
                "data" => {
                    "object" => invoices[0].as_json
                }
            })
        end

        it "should log to slack and call record_invoice_payment" do
            expect_any_instance_of(Subscription).to receive(:record_invoice_payment)
            expect_any_instance_of(Subscription).to receive(:all_payments_complete?).and_return(false)
            expect_log_to_slack.with(
                "*#{user.email}* made a payment of *$10.00*",
                "#36A64F")
            handle @stripe_event
        end

        it "should log to slack again if all_payments_complete" do
            expect_any_instance_of(Subscription).to receive(:record_invoice_payment)
            expect_any_instance_of(Subscription).to receive(:all_payments_complete?).and_return(true)
            log_count = 0

            expect_log_to_slack do |message, color|
                log_count += 1
                if log_count == 1
                    expect(message).to eq("*#{user.email}* made a payment of *$10.00*")
                    expect(color).to eq("#36A64F")
                elsif log_count == 2
                    expect(message).to eq("*#{user.email}* made a payment of *$10.00*")
                    expect(color).to eq("#36A64F")
                end
            end.exactly(2).times

            handle @stripe_event
        end

        it "should not process invoices where credits were applied" do

            invoices = stub_invoices(user, [
                {date: Time.at(0), total: -1000, id: 'paid_subscription_invoice'}
            ])
            get_event({
                "type" => "invoice.payment_succeeded",
                "data" => {
                    "object" => invoices[0].as_json
                }
            })

            expect_any_instance_of(Subscription).not_to receive(:record_invoice_payment)
            expect_any_instance_of(StripeEventHandler).not_to receive(:log_to_slack)
            handle @stripe_event
        end

        it "should not attempt to call record_invoice_payment if no subscription is attached" do

            invoices = stub_invoices(user, [
                {date: Time.at(0), total: -1000, id: 'paid_subscription_invoice'}
            ], nil, true) # disable_subscription

            get_event({
                "type" => "invoice.payment_succeeded",
                "data" => {
                    "object" => invoices[0].as_json
                }
            })

            expect_any_instance_of(Subscription).not_to receive(:record_invoice_payment)
            expect_any_instance_of(StripeEventHandler).not_to receive(:log_to_slack)
            handle @stripe_event
        end

    end

    describe 'charge.failed notifications' do

        before(:each) do
            user.ensure_stripe_customer
        end

        it "should log to Slack with bank outcome support" do
            charge = Stripe::Charge.create(customer: @user.id, livemode: false, amount: 1000, currency: 'usd')

            get_event({
                "type" => "charge.failed",
                "data" => {
                    "object" => charge
                }
            })

            # FIXME: Uncomment once stripe_ruby_mock supports 2018-02-05+
            expect_any_instance_of(Stripe::Charge).to receive(:outcome).at_least(1).times.and_return(MockBillingTransactionOutcome.new("insufficient_funds", "The bank returned the decline code `insufficient_funds`."))

            expect_log_to_slack.with(
                "A payment of *$10.00* from *#{user.email}* was declined with reason - The bank returned the decline code `insufficient_funds`. (insufficient_funds)",
                "#F8000C",
            )
            handle @stripe_event
        end

        it "should log to Slack with generic reason support if not bank outcome" do
            charge = Stripe::Charge.create(customer: @user.id, livemode: false, amount: 1000, currency: 'usd', failure_code: "card_declined", failure_message: "Your card has insufficient funds.")

            get_event({
                "type" => "charge.failed",
                "data" => {
                    "object" => charge
                }
            })

            # FIXME: Uncomment once stripe_ruby_mock supports 2018-02-05+
            expect_any_instance_of(Stripe::Charge).to receive(:outcome).at_least(1).times.and_return(nil)

            expect_log_to_slack.with(
                "A payment of *$10.00* from *#{user.email}* was declined with reason - Your card has insufficient funds. (card_declined)",
                "#F8000C"
            )
            handle @stripe_event
        end


        it "should log to Slack if an inaccessible invoice is referenced (pre-subscription)" do
            charge = Stripe::Charge.create(customer: @user.id, livemode: false, amount: 1000, currency: 'usd', failure_code: "card_declined", failure_message: "Your card has insufficient funds.")

            charge.invoice = "DOES_NOT_EXIST"
            error = Stripe::InvalidRequestError.new("No such invoice: #{charge.invoice}", nil)
            expect(Stripe::Invoice).to receive(:retrieve).with(charge.invoice).and_raise(error)

            get_event({
                "type" => "charge.failed",
                "data" => {
                    "object" => charge
                }
            })

            # FIXME: Uncomment once stripe_ruby_mock supports 2018-02-05+
            expect_any_instance_of(Stripe::Charge).to receive(:outcome).at_least(1).times.and_return(nil)

            expect_log_to_slack.with(
                "A payment of *$10.00* from *#{user.email}* was declined with reason - Your card has insufficient funds. (card_declined)",
                "#F8000C"
            )
            handle @stripe_event
        end

    end

    describe 'charge.refunded notifications' do
        before(:each) do
            user.ensure_stripe_customer
        end

        it "should record_refund and log appropriate message to Slack when fully refunded with a single refund" do
            stub_invoices(user, [
                {date: Time.at(0), total: 1000, id: 'paid_subscription_invoice'}
            ])

            get_event({
                "type" => "charge.refunded",
                "data" => {
                    "object" => {
                        "id" => '12345',
                        "amount" => 1000,
                        "currency" => 'USD',
                        "invoice" => 'paid_subscription_invoice',
                        "refunded" => true,
                        'refunds' => {
                            'data' => [{
                                'id' => 'refund_1',
                                'amount' => 1000,
                                'charge': '12345'
                            }]
                        }
                    }
                }
            })
            expect_any_instance_of(User).to receive(:record_refund)
            expect(Refund).to receive(:find_or_create_by_stripe_refund).exactly(1).times do |refund|
                expect(refund.id).to eq('refund_1')
            end
            expect_log_to_slack.with(
                "Refunded *#{user.email}* a payment of *$10.00*",
                "#FF9300")
            handle @stripe_event
        end

        it "should record_refund and log appropriate message to Slack when fully refunded with multiple refunds" do
            stub_invoices(user, [
                {date: Time.at(0), total: 1000, id: 'paid_subscription_invoice'}
            ])

            get_event({
                "type" => "charge.refunded",
                "data" => {
                    "object" => {
                        "id" => '12345',
                        'amount' => 1000,
                        "currency" => 'USD',
                        "invoice" => 'paid_subscription_invoice',
                        "refunded" => true,
                        'refunds' => {
                            'data' => [{
                                # refunds are ordered with the most recent refund first, so this is the
                                # relevant refund and should be the amount that gets logged to Slack
                                'amount' => 400,
                                'id' => 'refund_1'
                            }, {
                                # this is a refund that happened previously, so it should have
                                # already been recorded previously and shouldn't be recorded now
                                'amount' => 600,
                                'id' => 'refund_2'
                            }],
                            'total_count' => 2
                        }
                    }
                }
            })

            refund_ids = []
            expect(Refund).to receive(:find_or_create_by_stripe_refund).exactly(2).times do |refund|
                refund_ids << refund.id
            end
            expect_any_instance_of(User).to receive(:record_refund)
            expect_log_to_slack.with(
                "Issued final partial refund of *$4.00* for a charge of *$10.00* to *user_with_subscriptions_enabled@pedago.com*",
                "#FF9300")
            handle @stripe_event
            expect(refund_ids).to eq(['refund_1', 'refund_2'])
        end

        it "should not attempt to record_refund if not fully refunded" do
            stub_invoices(user, [
                {date: Time.at(0), total: 1000, id: 'paid_subscription_invoice'}
            ])

            get_event({
                "type" => "charge.refunded",
                "data" => {
                    "object" => {
                        "id" => '12345',
                        'amount' => 1000,
                        "currency" => 'USD',
                        "invoice" => 'paid_subscription_invoice',
                        "refunded" => false,
                        'refunds' => {
                            'data' => [{
                                'amount' => 500
                            }],
                            'total_count' => 1
                        }
                    }
                }
            })

            # in another spec we are explicitly checking the args passed to find_or_create_by_stripe_refund,
            # so here it's fine to just mock it out more generally
            allow(Refund).to receive(:find_or_create_by_stripe_refund)

            expect_any_instance_of(User).not_to receive(:record_refund)
            expect_log_to_slack.with(
                "Issued a partial refund of *$5.00* for a charge of *$10.00* to *user_with_subscriptions_enabled@pedago.com*",
                "#FF9300")
            handle @stripe_event
        end

        it "should not process invoices where credits were applied" do
            # Pretty sure this isn't a possible scenario
            stub_invoices(user, [
                {date: Time.at(0), total: 1000, id: 'paid_subscription_invoice'}
            ])

            get_event({
                "type" => "charge.refunded",
                "data" => {
                    "object" => {
                        "id" => '12345',
                        "amount_refunded" => -500,
                        "currency" => 'USD',
                        "invoice" => 'paid_subscription_invoice',
                        "refunded" => false,
                        'refunds' => {
                            'data' => [{
                                'amount' => -500
                            }],
                            'total_count' => 1
                        }
                    }
                }
            })

            # in another spec we are explicitly checking the args passed to find_or_create_by_stripe_refund,
            # so here it's fine to just mock it out more generally
            allow(Refund).to receive(:find_or_create_by_stripe_refund)

            expect_any_instance_of(User).not_to receive(:record_refund)
            expect(LogToSlack).not_to receive(:perform_later)
            handle @stripe_event
        end
    end

    describe 'disputes' do
        before(:each) do
            user.ensure_stripe_customer
            mock_charge = double(:mock_charge)
            allow(mock_charge).to receive(:customer).and_return(user.id)
            allow(mock_charge).to receive(:invoice).and_return(nil)
            allow(mock_charge).to receive(:statement_descriptor).and_return(:statement_descriptor)
            allow(Stripe::Charge).to receive(:retrieve).and_return(mock_charge)
        end

        it 'should log_to_slack for charge.dispute.created event' do
            get_event({
                "type" => "charge.dispute.created"
            })
            expect_log_to_slack.with(
                "*user_with_subscriptions_enabled@pedago.com* has *disputed* a charge of $250.00. :warning:",
                "#FF9300")
            handle @stripe_event
        end

        it 'should log_to_slack for charge.dispute.funds_withdrawn event' do
            get_event({
                "type" => "charge.dispute.funds_withdrawn"
            })
            expect_log_to_slack.with(
                "*user_with_subscriptions_enabled@pedago.com* - $250.00 in disputed funds have been withdrawn. :warning:",
                "#F8000C")
            handle @stripe_event
        end

        it 'should log_to_slack for charge.dispute.funds_reinstated event' do
            get_event({
                "type" => "charge.dispute.funds_reinstated"
            })
            expect_log_to_slack.with(
                "*user_with_subscriptions_enabled@pedago.com* - $250.00 in disputed funds have been reinstated.",
                "#36A64F")
            handle @stripe_event
        end

        describe 'charge.dispute.closed' do

            it 'should work when won' do
                get_event({
                    "type" => "charge.dispute.closed",
                    "data" => {
                        "object" => {
                            "status" => 'won'
                        }
                    }
                })
                expect_log_to_slack.with(
                    "*user_with_subscriptions_enabled@pedago.com* - dispute of $250.00 won.",
                    "#36A64F")
                handle @stripe_event
            end

            it 'should work when lost' do
                get_event({
                    "type" => "charge.dispute.closed",
                    "data" => {
                        "object" => {
                            "status" => 'lost'
                        }
                    }
                })
                expect_log_to_slack.with(
                    "*user_with_subscriptions_enabled@pedago.com* - dispute of $250.00 lost. :warning:",
                    "#F8000C")
                handle @stripe_event
            end

            it 'should work when warning_closed' do
                get_event({
                    "type" => "charge.dispute.closed",
                    "data" => {
                        "object" => {
                            "status" => 'warning_closed'
                        }
                    }
                })
                expect_log_to_slack.with(
                    "*user_with_subscriptions_enabled@pedago.com* - dispute of $250.00 closed without escalation.",
                    "#36A64F")
                handle @stripe_event
            end
        end

    end

    describe "invoice.created handling" do

        before(:each) do
            @stripe_subscription = Subscription.create_stripe_subscription(user, @default_plan.id)
            allow_any_instance_of(StripeEventHandler).to receive(:stripe_subscription) do
                @stripe_subscription
            end
        end

        it "should pay the invoice" do
            expect_any_instance_of(Stripe::Invoice).to receive(:pay)
            handle({
                "type" => "invoice.created",
                "data" => {
                    "object" => {
                        "closed" => false
                    }
                }
            })
        end

        it "should not pay a closed invoice" do
            expect_any_instance_of(Stripe::Invoice).not_to receive(:pay)
            handle({
                "type" => "invoice.created",
                "data" => {
                    "object" => {
                        "closed" => true
                    }
                }
            })
        end

        it "should not pay if there is no active subscription" do
            expect_any_instance_of(Stripe::Invoice).not_to receive(:pay)
            expect(@stripe_subscription).to receive(:status).and_return('incomplete').at_least(1).times
            handle({
                "type" => "invoice.created",
                "data" => {
                    "object" => {
                        "closed" => false
                    }
                }
            })
        end

        it "should not catch InvalidRequest error" do
            expect_any_instance_of(Stripe::Invoice).to receive(:pay).and_raise(Stripe::InvalidRequestError.new(nil, nil))
            expect(Proc.new{
                handle({
                    "type" => "invoice.created",
                    "data" => {
                        "object" => {
                            "closed" => false
                        }
                    }
                })
            }).not_to raise_error
        end

        it "should eat card declined errors" do
            expect_any_instance_of(Stripe::Invoice).to receive(:pay).and_raise(Stripe::CardError.new(nil, nil))
            expect(Proc.new{
                handle({
                    "type" => "invoice.created",
                    "data" => {
                        "object" => {
                            "closed" => false
                        }
                    }
                })
            }).not_to raise_error
        end

    end

    describe "event logging" do

        it "should log to events table" do
            handle({
                "type" => "test.event",
                "data" => {
                    "object" => {"object" => "generic"}
                }
            })
            assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
            expect(event.payload).to eq(@stripe_event.as_json.merge({
                'stripe_event' => true,
                'server_event' => true
            }))
        end

        it "should log to segmentio" do
            expect_any_instance_of(Event).to receive(:log_to_external_systems)
            handle
        end

        it "should log revenue on a charge.succeeded event" do

            stub_invoices(user, [
                {date: Time.at(0), total: 1000, id: 'paid_subscription_invoice'}
            ])

            handle({
                'type' => 'charge.succeeded',
                'data' => {
                    'object' => {
                        'amount' => 1999,
                        'currency' => 'USD',
                        'invoice': 'paid_subscription_invoice'
                    }
                }
            })
            assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
            expect(event.payload['revenue']).to eq(19.99)
            expect(event.payload['currency']).to eq('USD')
        end

        it "should log revenue on a charge.refunded event" do

            stub_invoices(user, [
                {date: Time.at(0), total: 1000, id: 'paid_subscription_invoice'}
            ])

            # in another spec we are explicitly checking the args passed to find_or_create_by_stripe_refund,
            # so here it's fine to just mock it out more generally
            allow(Refund).to receive(:find_or_create_by_stripe_refund)

            handle({
                'type' => 'charge.refunded',
                'data' => {
                    'object' => {
                        'id': '12345',
                        'currency': 'USD',
                        'invoice': 'paid_subscription_invoice',
                        'refunds': {
                            'data': [{
                                # refunds are ordered with the most recent refund first, so this is the
                                # relevant refund and should have its amount recorded as the revenue
                                'amount': 999
                            }, {
                                # this is a refund that happened previously, so it should have
                                # already been recorded previously and shouldn't be recorded now
                                'amount': 1000
                            }],
                            'total_count': 2
                        }
                    }
                }
            })

            assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
            expect(event.payload['revenue']).to eq(-9.99)
            expect(event.payload['currency']).to eq('USD')
        end

        describe "with customer.subscription.updated event" do

            before(:each) do
                @stripe_subscription = Subscription.create_for_owner(owner: user, stripe_plan_id: @default_plan.id).stripe_subscription
            end

            it "should log event with set_to_cancel=true if cancel_at_period_end set to true" do
                handle({
                    "type" => "customer.subscription.updated",
                    "data" => {
                        "object" => {
                            "cancel_at_period_end" => true,
                            "id" => @stripe_subscription.id
                        },
                        "previous_attributes" => {
                            "cancel_at_period_end" => false
                        }
                    }
                })
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event.payload["set_to_cancel"]).to be(true)
            end

            it "should log event with set_to_cancel=false if cancel_at_period_end set to false" do
                handle({
                    "type" => "customer.subscription.updated",
                    "data" => {
                        "object" => {
                            "cancel_at_period_end" => false,
                            "id" => @stripe_subscription.id
                        },
                        "previous_attributes" => {
                            "cancel_at_period_end" => true
                        }
                    }
                })
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event.payload["set_to_cancel"]).to be(false)
            end

            it "should log event with set_to_cancel=false if cancel_at_period_end does not change" do
                handle({
                    "type" => "customer.subscription.updated",
                    "data" => {
                        "object" => {
                            "id" => @stripe_subscription.id,
                            "cancel_at_period_end" => true
                        },
                        "previous_attributes" => {}
                    }
                })
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event.payload["set_to_cancel"]).to be(false)
            end

            it "should log event with paid_subscription_set_to_cancel=true if it is a paid subscription" do
                handle({
                    "type" => "customer.subscription.updated",
                    "data" => {
                        "object" => {
                            "cancel_at_period_end" => true,
                            "id" => @stripe_subscription.id,
                            "status" => "active"
                        },
                        "previous_attributes" => {
                            "cancel_at_period_end" => false
                        }
                    }
                })
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event.payload["paid_subscription_set_to_cancel"]).to be(true)
                expect(event.payload["free_trial_set_to_cancel"]).to be(false)
            end

            it "should log event with paid_subscription_set_to_cancel=true if it is a free trial subscription" do
                handle({
                    "type" => "customer.subscription.updated",
                    "data" => {
                        "object" => {
                            "id" => @stripe_subscription.id,
                            "cancel_at_period_end" => true,
                            "status" => "trialing"
                        },
                        "previous_attributes" => {
                            "cancel_at_period_end" => false
                        }
                    }
                })
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event.payload["paid_subscription_set_to_cancel"]).to be(false)
                expect(event.payload["free_trial_set_to_cancel"]).to be(true)
            end

        end

        describe "with subscription event" do

            it "should log event with special subscription properties" do
                handle({
                    "data" => {
                        "object" => {
                            "object" => "subscription",
                            "trial_start" => 12345,
                            "plan" => {
                                "id" => "plan_id",
                                "name" => "plan_name"
                            },
                            "status" => "status"
                        }
                    }
                })
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event.payload["plan_id"]).to eq("plan_id")
                # expect(event.payload["plan_name"]).to eq("plan_name") # FIXME: Uncomment once stripe_ruby_mock supports 2018-02-05+
                expect(event.payload["subscription_status"]).to eq("status")
            end

            it "should add some info" do
                stripe_subscription = Subscription.create_for_owner(owner: user, stripe_plan_id: @default_plan.id).stripe_subscription

                expect_any_instance_of(Subscription).to receive(:next_period_amount).and_return(4200)
                handle({
                    "type" => "customer.subscription.created",
                    "data" => {
                        "object" => stripe_subscription.as_json
                    }
                })
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))

                begin
                    hash['plan_id'] = stripe_subscription.plan.id
                    hash['plan_name'] = stripe_subscription.plan.nickname
                rescue NoMethodError; end

                expect(event.payload["next_period_amount"]).to eq(42)
                expect(event.payload["current_period_end"]).to eq(stripe_subscription.current_period_end)
                expect(event.payload["current_period_start"]).to eq(stripe_subscription.current_period_start)
                expect(event.payload["cancel_at_period_end"]).to eq(stripe_subscription.cancel_at_period_end)
                expect(event.payload["plan_id"]).to eq(stripe_subscription.plan.id)
                expect(event.payload["plan_name"]).to eq(stripe_subscription.plan.name)
            end
        end

        describe "with customer.updated event" do

            it "should set card_changed=true if the card is changed" do
                handle(sample_customer_event_attrs.deep_merge({
                    "type" => "customer.updated",
                    "data" => {
                        "object" => {
                            "sources": {
                                "object": "list",
                                "data": [
                                    {
                                        "id": "card_newcard",
                                        "object": "card",
                                        "last4": "4242",
                                        "brand": "Visa"
                                    },
                                    {
                                        "id": "card_oldcard",
                                        "object": "card",
                                        "last4": "4444",
                                        "brand": "Mastercard"
                                    }
                                ]
                                },
                                "default_source" => "card_newcard"
                        },
                        "previous_attributes" => {
                            "default_source" => "card_oldcard"
                        }
                    }
                }))

                # ensure that we use the customer from the event, rather
                # than pulling again from db
                expect(Stripe::Customer).not_to receive(:retrieve)
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event.payload["card_changed"]).to be(true)
                expect(event.payload["card_added"]).to be(true)
                expect(event.payload["default_card.last4"]).to eq("4242")
                expect(event.payload["default_card.brand"]).to eq("Visa")
                expect(event.payload["previous_default_card.last4"]).to eq("4444")
                expect(event.payload["previous_default_card.brand"]).to eq("Mastercard")
            end

            it "should set card_changed=false if the card is not changed" do
                handle(sample_customer_event_attrs.deep_merge({
                    "type" => "customer.updated",
                    "data" => {
                        "previous_attributes" => {}
                    }
                }))
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event.payload["card_changed"]).to be(false)
                expect(event.payload["card_added"]).to be(false)
            end

            it "should work when card is changed from nil to something" do
                handle(sample_customer_event_attrs.deep_merge({
                    "type" => "customer.updated",
                    "data" => {
                        "object" => {
                            "sources": {
                                "object": "list",
                                "data": [
                                    {
                                        "id": "card_newcard",
                                        "object": "card",
                                        "last4": "4242",
                                        "brand": "Visa"
                                    }
                                ]
                                },
                                "default_source" => "card_newcard"
                        },
                        "previous_attributes" => {
                            "default_source" => nil
                        }
                    }
                }))

                # ensure that we use the customer from the event, rather
                # than pulling again from db
                expect(Stripe::Customer).not_to receive(:retrieve)
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event.payload["card_changed"]).to be(false)
                expect(event.payload["card_added"]).to be(true)
                expect(event.payload["default_card.last4"]).to eq("4242")
                expect(event.payload["default_card.brand"]).to eq("Visa")
                expect(event.payload["previous_default_card.last4"]).to be_nil
                expect(event.payload["previous_default_card.brand"]).to be_nil
            end

        end

        describe "with invoice event" do

            it "should work if there is no subscription" do
                handle(sample_invoice_attrs.deep_merge({
                    "type" => "invoice.payment_succeeded"
                }))
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event.payload["has_free_trial"]).to be_nil
            end

            it "should include basic invoice info" do

                attrs = sample_invoice_attrs.deep_merge({
                    "type" => "invoice.payment_succeeded"
                })
                instance = handle(attrs)
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))

                assert invoice = instance.invoice
                expect(event.payload['total']).to eq(invoice.total / 100.0)
                expect(event.payload['invoice_id']).to eq(invoice.id)
                expect(event.payload['invoice_date']).to eq(invoice.created)
                expect(event.payload['invoice_number']).to eq(invoice.number)

                %w(currency hosted_invoice_url invoice_pdf).map(&:to_sym).each do |meth|
                    expect(invoice.send(meth)).not_to be_nil # sanity check
                    expect(event.payload[meth.to_s]).to eq(invoice.send(meth))

                end
            end

            it "should include plan info" do
                attrs = sample_invoice_attrs.deep_merge({
                    "type" => "invoice.payment_succeeded"
                })
                instance = handle(attrs)
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))

                assert invoice = instance.invoice
                expect(event.payload['plan_id']).to eq(invoice.lines.data[0].plan.id)
                expect(event.payload['plan_name']).to eq(invoice.lines.data[0].plan.nickname)
            end


            it "should work if there is no plan attached" do

                attrs = sample_invoice_attrs.deep_merge({
                    "type" => "invoice.payment_succeeded"
                })
                attrs['data']['object']['lines']['data'].each { |line|
                    line.delete('plan')
                }

                instance = handle(attrs)
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))

                assert invoice = instance.invoice
                expect(event.payload['plan_id']).to be_nil
                expect(event.payload['plan_name']).to be_nil
            end

            it "should work with metered plan" do

                hiring_team = HiringTeam.joins(:owner).first
                invoice, charge = stub_metered_invoice(hiring_team)
                subscription = hiring_team.primary_subscription
                product = subscription.plan.product
                allow_any_instance_of(StripeEventHandler).to receive(:invoice).and_return(invoice)
                allow_any_instance_of(StripeEventHandler).to receive(:charge).and_return(charge)
                allow_any_instance_of(StripeEventHandler).to receive(:subscription).and_return(subscription)

                attrs = sample_invoice_attrs.deep_merge({
                    "type" => "invoice.payment_succeeded",
                    "data" => {
                        "object" => {
                            "customer" => hiring_team.id
                        }
                    }
                })

                instance = handle(attrs)
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))

                expect(event.payload['metered_plan']).to be(true)
                expect(event.payload['lines']).to eq([{
                    unit_quantity: 42,
                    unit_amount: subscription.plan.amount / 100.0,
                    unit_name: "#{product.name} (per #{product.unit_label})",
                    amount: invoice.amount / 100.0
                }.stringify_keys])
                expect(event.payload['product_name']).to eq(product.name)
                expect(event.payload['has_free_trial']).to be(false)
            end

            it "should work with non-metered plan" do
                hiring_team = HiringTeam.joins(:owner).first
                invoice, charge = stub_invoice_with_charge(hiring_team)
                subscription = hiring_team.primary_subscription
                product = subscription.plan.product
                allow_any_instance_of(StripeEventHandler).to receive(:invoice).and_return(invoice)
                allow_any_instance_of(StripeEventHandler).to receive(:charge).and_return(charge)
                allow_any_instance_of(StripeEventHandler).to receive(:subscription).and_return(subscription)

                attrs = sample_invoice_attrs.deep_merge({
                    "type" => "invoice.payment_succeeded",
                    "data" => {
                        "object" => {
                            "customer" => hiring_team.id
                        }
                    }
                })

                instance = handle(attrs)
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))

                expect(event.payload['metered_plan']).to be(false)
                expect(event.payload['lines']).to be_nil
                expect(event.payload['product_name']).to eq(product.name)
                expect(event.payload['has_free_trial']).to be(false)

                # tossing a couple extra assertions in here that are really valid for any kind of subscription
                expect(event.payload['product_name']).to eq(product.name)
            end

            it "should set has_free_trial=true if there is a trial" do

                hiring_team = HiringTeam.joins(:owner).first
                create_customer_with_default_source(hiring_team)
                trial_end = 30.days.from_now.to_timestamp
                subscription = create_for_owner_and_plan!(hiring_team, default_plan.id, nil, trial_end)
                invoice, charge = stub_invoice_with_charge(hiring_team)
                allow_any_instance_of(StripeEventHandler).to receive(:invoice).and_return(invoice)
                allow_any_instance_of(StripeEventHandler).to receive(:charge).and_return(charge)
                allow_any_instance_of(StripeEventHandler).to receive(:subscription).and_return(subscription)

                attrs = sample_invoice_attrs.deep_merge({
                    "type" => "invoice.payment_succeeded",
                    "data" => {
                        "object" => {
                            "customer" => hiring_team.id
                        }
                    }
                })
                instance = handle(attrs)
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event.payload['has_free_trial']).to be(true)
            end
        end

        describe "with invoice.payment_failed event" do

            it "should include info about the failed charge" do
                handle(sample_invoice_attrs.deep_merge({
                    "type" => "invoice.payment_failed"
                }))
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event.payload["total"]).to eq(49.0)
                # expect(event.payload["plan_name"]).to eq("Shortened Monthly Plan") # FIXME: Uncomment once stripe_ruby_mock supports 2018-02-05+
                expect(event.payload["plan_id"]).to eq("shortened_monthly_49")
            end

        end

        describe "with invoice.upcoming event" do

            before(:each) do
                @hiring_team = HiringTeam.where('owner_id is not null').all.detect { |ht| ht.hiring_managers.size > 1 }
                @owner = @hiring_team.owner
                create_customer_with_default_source(@hiring_team)
                @subscription = Subscription.create_for_owner(owner: @hiring_team, stripe_plan_id: @default_plan.id)
                @team_member = (@hiring_team.hiring_managers - [@owner]).first
                @event_attrs = invoice_upcoming_attrs( @subscription)
            end

            it "should log expected attributes" do

                open_position = OpenPosition.first
                open_position.update!(subscription_id: @subscription.id, hiring_manager_id: @owner.id)

                handle(@event_attrs)

                # hiring team owner event
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event.payload["amount"]).to eq(@event_attrs['data']['object']['amount_due'] / 100.0)
                expect(event.payload["next_payment_attempt"]).to eq(@event_attrs['data']['object']['next_payment_attempt'])

                # hiring team member event
                assert_nil Event.find_by_id(StripeEventHandler.get_uuid("#{@stripe_event.id}-#{@team_member.id}"))
            end

            it "should log if no position" do
                handle(@event_attrs)

                # hiring team owner event
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                # hiring team member event
                assert_nil Event.find_by_id(StripeEventHandler.get_uuid("#{@stripe_event.id}-#{@team_member.id}"))


                expect(event.payload["amount"]).to eq(@event_attrs['data']['object']['amount_due'] / 100.0)
                expect(event.payload["next_payment_attempt"]).to eq(@event_attrs['data']['object']['next_payment_attempt'])
                expect(event.payload.keys).not_to include('open_position_id')
            end

        end

        describe "with subscription that has a position" do

            before(:each) do
                @hiring_team = HiringTeam.where('owner_id is not null').all.detect { |ht| ht.hiring_managers.size > 1 }
                @owner = @hiring_team.owner
                create_customer_with_default_source(@hiring_team)
                @subscription = Subscription.create_for_owner(owner: @hiring_team, stripe_plan_id: @default_plan.id)
                @team_member = (@hiring_team.hiring_managers - [@owner]).first
                @event_attrs = invoice_upcoming_attrs( @subscription)
            end

            it "should log two events if position is for a non-owner" do
                open_position = OpenPosition.first
                open_position.update!(subscription_id: @subscription.id, hiring_manager_id: @team_member.id)

                handle(@event_attrs)

                # hiring team owner event
                assert event1 = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event1.payload["is_hiring_team_owner"]).to eq(true)
                expect(event1.payload["is_open_position_creator"]).to eq(false)
                expect(event1.user_id).to eq(open_position.hiring_team.owner_id)

                # hiring team member event
                assert event2 = Event.find(StripeEventHandler.get_uuid("#{@stripe_event.id}-#{@team_member.id}"))
                expect(event2.payload["is_hiring_team_owner"]).to eq(false)
                expect(event2.payload["is_open_position_creator"]).to eq(true)
                expect(event2.user_id).to eq(open_position.hiring_manager_id)

                [event1, event2].each do |event|
                    expect(event.payload["plan_id"]).to eq(@subscription.stripe_plan_id)
                    expect(event.payload["hiring_plan"]).to eq(@hiring_team.hiring_plan)
                    event_attributes = open_position.event_attributes(event.user_id).stringify_keys
                    expect(event.payload).to have_entries(open_position.event_attributes(event.user_id).stringify_keys)
                end
            end

            it "should log one event if position is for the owner" do

                open_position = OpenPosition.first
                open_position.update!(subscription_id: @subscription.id, hiring_manager_id: @owner.id)

                handle(@event_attrs)

                # hiring team owner event
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event.payload["is_hiring_team_owner"]).to eq(true)
                expect(event.payload["is_open_position_creator"]).to eq(true)

                # hiring team member event
                assert_nil Event.find_by_id(StripeEventHandler.get_uuid("#{@stripe_event.id}-#{@team_member.id}"))
            end

        end

        describe "with invoice.payment_succeeded event" do

            before(:each) do
                allow_any_instance_of(Stripe::Invoice).to receive(:amount_paid)
            end

            it "should indicate if this is the first payment for a subscription with no free trial" do
                subscription = Subscription.create_for_owner(owner: user, stripe_plan_id: @default_plan.id)
                subscription_start = subscription.stripe_subscription.created

                event_attrs = sample_invoice_attrs.deep_merge({
                    "type" => "invoice.payment_succeeded",
                    "data" => {
                        "object" => {
                            "subscription" => subscription.stripe_subscription_id
                        }
                    }
                })
                event_attrs['data']['object']['lines']['data'][0]['period'] = {
                    "start" => subscription_start.to_timestamp,
                    "end" => (subscription_start + 1.month).to_timestamp
                }
                handle(event_attrs)
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event.payload["first_payment_for_subscription"]).to eq(true)
                expect(event.payload["has_free_trial"]).to eq(false)

            end

            it "should indicate if this is the first payment for a subscription with a free trial" do
                trial_end = 1.day.from_now
                subscription = Subscription.create_for_owner(owner: user, stripe_plan_id: @default_plan.id, trial_end_ts: trial_end.to_timestamp)

                event_attrs = sample_invoice_attrs.deep_merge({
                    "type" => "invoice.payment_succeeded",
                    "data" => {
                        "object" => {
                            "subscription" => subscription.stripe_subscription_id
                        }
                    }
                })
                event_attrs['data']['object']['lines']['data'][0]['period'] = {
                    "start" => trial_end.to_timestamp,
                    "end" => (trial_end + 1.month).to_timestamp
                }
                handle(event_attrs)
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event.payload["first_payment_for_subscription"]).to eq(true)
                expect(event.payload["has_free_trial"]).to eq(true)

            end

            it "should indicate if this is not the first payment for a subscription" do
                subscription = Subscription.create_for_owner(owner: user, stripe_plan_id: @default_plan.id)
                subscription_start = subscription.stripe_subscription.created

                event_attrs = sample_invoice_attrs.deep_merge({
                    "type" => "invoice.payment_succeeded",
                    "data" => {
                        "object" => {
                            "subscription" => subscription.stripe_subscription_id
                        }
                    }
                })
                event_attrs['data']['object']['lines']['data'][0]['period'] = {
                    "start" => (subscription_start + 1.month).to_timestamp,
                    "end" => (subscription_start + 2.months).to_timestamp
                }
                handle(event_attrs)
                assert event = Event.find(StripeEventHandler.get_uuid(@stripe_event.id))
                expect(event.payload["first_payment_for_subscription"]).to eq(false)
                expect(event.payload["has_free_trial"]).to eq(false)
            end

        end

    end

    describe "identify" do
        it "should make an identify call" do
            expect_any_instance_of(User).to receive(:identify)
            handle
        end
    end

    describe "format_currency" do
        it "should format stripe values to human-readable English values" do
            I18n.locale = :zh
            expect_any_instance_of(StripeEventHandler::Formatter).to receive(:number_to_currency).with(100.0, locale: :en).and_call_original
            expect(StripeEventHandler.new(get_event({ "data"=> { "object" => {} }})).format_currency(10000)).to eq("$100.00")
            I18n.locale = :en
        end
    end

    describe "log_to_slack" do
        it "should log to the slack channel from the subscription" do
            subscription = create_subscription(user)
            expect_any_instance_of(StripeEventHandler).to receive(:stripe_url_for_event).and_return('some-url')
            expect(LogToSlack).to receive(:perform_later).with(
                user.subscription_slack_channel,
                nil,
                [{
                    color: 'fuschia',
                    text: "something happened [#{user.subscription_identifier_for_slack(subscription, subscription.stripe_subscription)}]\nsome-url",
                    mrkdwn_in: ['text']
                }]
            )

            StripeEventHandler.new(get_event({
                "data"=> { "object" => subscription.stripe_subscription.as_json }
            })).log_to_slack('something happened', 'fuschia')
        end

        it "should log to engineering alerts if there is somehow no owner" do
            expect_any_instance_of(StripeEventHandler).to receive(:owner).at_least(1).times.and_return(nil)
            expect_any_instance_of(StripeEventHandler).to receive(:stripe_url_for_event).and_return('some-url')
            expect(LogToSlack).to receive(:perform_later).with(
                Slack.engineering_alerts_channel,
                nil,
                [{
                    color: 'fuschia',
                    text: "something happened [UNKNOWN]\nsome-url",
                    mrkdwn_in: ['text']
                }]
            )

            StripeEventHandler.new(get_event).log_to_slack('something happened', 'fuschia')
        end

    end

    def expect_log_to_slack
        expect_any_instance_of(StripeEventHandler).to receive(:log_to_slack)
    end

    def handle(overrides = {})
        event = overrides.is_a?(Stripe::Event) ? overrides : get_event(overrides)
        StripeEventHandler.new(event)
    end

    def get_event(overrides = {})
        overrides['type'] ||= "test.event"

        if overrides["type"]&.starts_with? "charge.dispute"
            @stripe_event = Stripe::Event.construct_from(sample_dispute_attrs.deep_merge(overrides))
        elsif overrides["type"]&.starts_with? "charge"
            @stripe_event = Stripe::Event.construct_from(sample_charge_attrs.deep_merge(overrides))
        elsif overrides["type"]&.starts_with? "invoice"
            @stripe_event = Stripe::Event.construct_from(sample_invoice_attrs.deep_merge(overrides))
        elsif overrides["type"]&.starts_with? "customer.subscription"
            @stripe_event = Stripe::Event.construct_from(sample_subscription_attrs.deep_merge(overrides))
        elsif overrides["type"]&.starts_with? "customer"
            @stripe_event = Stripe::Event.construct_from(sample_customer_event_attrs.deep_merge(overrides))
        elsif overrides["type"] == "test.event"
            @stripe_event = Stripe::Event.construct_from(sample_subscription_attrs.deep_merge(overrides))
        else
            raise "Unexpected type #{overrides['type'].inspect}"
        end
    end


    # FIXME: Update once stripe_ruby_mock supports 2018-02-05+
    def sample_customer_event_attrs
        {
            "id" => "evt_16e5fo2nDm85BVWRkVFwsOF6",
            "created" => 1440602328,
            "livemode" => false,
            "type" => "customer.created",
            "data" => {
                "object" => {
                    "object" => "customer",
                    "created" => 1440534459,
                    "id" => @user.id,
                    "livemode" => false,
                    "description" => nil,
                    "email" => @user.email,
                    "shipping" => nil,
                    "delinquent" => false,
                    "metadata" => {},
                    "subscriptions" => {
                        # in the wild, this is populated
                    },
                    "discount" => nil,
                    "balance" => 0,
                    "currency" => "usd",
                    "sources" => {
                        "object" => "list",
                        "total_count" => 3,
                        "has_more" => false,
                        "url" => "/v1/customers/b890d4e6-fd42-4204-81ac-172972c2a128/sources",
                        "data" => [
                            {
                                "id" => "card_16e5fm2nDm85BVWRXCrDvg3b",
                                "object" => "card",
                                "last4" => "4242",
                                "brand" => "Visa",
                                "customer" => "b890d4e6-fd42-4204-81ac-172972c2a128"
                            },
                            {
                                "id" => "card_16do2E2nDm85BVWR3C4SXA1n",
                                "object" => "card",
                                "last4" => "4444",
                                "brand" => "MasterCard",
                                "customer" => "b890d4e6-fd42-4204-81ac-172972c2a128"
                            }
                        ]
                    },
                    "default_source" => "card_16e5fm2nDm85BVWRXCrDvg3b"
                }
            },
            "object" => "event",
            "pending_webhooks" => 4,
            "request" => "req_6rpKrlXCPFMT8J",
            "api_version" => "2015-07-07"
        }
    end

    # different from regular invoice event because there is
    # no invoice yet
    def invoice_upcoming_attrs(subscription)

        {
            "id" => "evt_16do1o2nDm85BVWRnu8XNuLs",
            "created" => 1440534500,
            "livemode" => false,
            "type" => "invoice.upcoming",
            "data" => {
                "object" => {
                    "object": "invoice",
                    "amount_due": 32500,
                    "amount_paid": 0,
                    "amount_remaining": 32500,
                    "customer": subscription.stripe_subscription.customer,
                    "subscription": subscription.stripe_subscription_id,
                    "discount": {
                        "object": "discount",
                        "coupon": {
                            "id": "discount_550",
                            "object": "coupon",
                            "amount_off": 55000
                        },
                        "end": nil,
                        "start": 1557654696
                    },
                    "lines": {
                        "object": "list",
                        "data": [{
                            "object": "line_item",
                            "amount": 87500,
                            "description": "1 × Smartly EMBA (at $875.00 / month)",
                            "period": {
                                "end": 1562925096,
                                "start": 1560333096
                            },
                            "plan": {
                                "id": subscription.stripe_plan_id,
                                "object": "plan",
                                "amount": 87500,
                                "interval": "month"
                            }
                        }],
                        "has_more": false,
                        "total_count": 1
                    },
                    "next_payment_attempt": 1560336696,
                    "period_end": 1560333096,
                    "period_start": 1557654696,
                    "subtotal": 87500,
                    "total": 32500
                }
            }
        }.deep_stringify_keys
    end

    # FIXME: Update once stripe_ruby_mock supports 2018-02-05+
    def sample_invoice_attrs
        {
            "id" => "evt_16do1o2nDm85BVWRnu8XNuLs",
            "created" => 1440534500,
            "livemode" => false,
            "type" => "invoice.payment_succeeded",
            "data" => {
                "object" => {
                    "date" => 1440534500,
                    "id" => "in_16do1o2nDm85BVWR1tA2Xt7S",
                    "period_start" => 1440534473,
                    "period_end" => 1440534500,
                    "lines" => {
                        "object" => "list",
                        "total_count" => 1,
                        "has_more" => false,
                        "url" => "/v1/invoices/in_16do1o2nDm85BVWR1tA2Xt7S/lines",
                        "data" => [
                            {
                                "id" => "sub_6rX6uH0sXKaQmy",
                                "object" => "line_item",
                                "type" => "subscription",
                                "livemode" => false,
                                "amount" => 4900,
                                "currency" => "usd",
                                "proration" => false,
                                "period" => {
                                    "start" => 1440534500,
                                    "end" => 1440620900
                                },
                                "subscription" => nil,
                                "quantity" => 1,
                                "plan" => {
                                    "interval" => "day",
                                    "name" => "Shortened Monthly Plan",
                                    "created" => 1440429810,
                                    "amount" => 4900,
                                    "currency" => "usd",
                                    "id" => "shortened_monthly_49",
                                    "object" => "plan",
                                    "livemode" => false,
                                    "interval_count" => 1,
                                    "trial_period_days" => 0,
                                    "metadata" => {
                                        "sort_key" => "0",
                                        "upgrade_to" => "shortened_mba_216",
                                        "upgrade_message" => "Upgrade to the 2-Year \"MBA\" Plan for $216, or $9/mo"
                                    },
                                    "statement_descriptor" => nil
                                },
                                "description" => nil,
                                "discountable" => true,
                                "metadata" => {}
                            }
                        ]
                    },
                    "subtotal" => 4900,
                    "total" => 4900,
                    "customer" => @user.id,
                    "object" => "invoice",
                    "attempted" => true,
                    "closed" => true,
                    "forgiven" => false,
                    "paid" => true,
                    "livemode" => false,
                    "attempt_count" => 1,
                    "amount_due" => 4900,
                    "currency" => "usd",
                    "starting_balance" => 0,
                    "ending_balance" => 0,
                    "next_payment_attempt" => nil,
                    "webhooks_delivered_at" => nil,
                    "charge" => nil, # in the wild, there would be a charge, but this simplifies things here for now
                    "discount" => nil,
                    "application_fee_amount" => nil,
                    "subscription" => "sub_6rX6uH0sXKaQmy",
                    "tax_percent" => nil,
                    "tax" => nil,
                    "metadata" => {},
                    "statement_descriptor" => nil,
                    "description" => nil,
                    "receipt_number" => nil
                }
            },
            "object" => "event",
            "pending_webhooks" => 4,
            "request" => "req_6rX6RhEUOdFatt",
            "api_version" => "2015-07-07"
        }
    end

    def sample_subscription_attrs
        {
           "id" => "evt_16RUKg2nDm85BVWRd160oDNd",
           "created" => 1437598854,
           "livemode" => false,
           "type" => "test.event",
           "data" => {
              "object" => {
                 "id" => "sub_6enwmJOlABMh40",
                 "plan" => {
                    "interval" => "month",
                    "name" => "Monthly Subscription",
                    "created" => 1436816888,
                    "amount" => 2500,
                    "currency" => "usd",
                    "id" => @default_plan.id,
                    "object" => "plan",
                    "livemode" => false,
                    "interval_count" => 1,
                    "trial_period_days" => 0,
                    "metadata" => {

                    },
                    "statement_descriptor" => "Smartly"
                 },
                 "object" => "subscription",
                 "start" => 1437598853,
                 "status" => "active",
                 "customer" => user.id,
                 "cancel_at_period_end" => false,
                 "current_period_start" => 1437598853,
                 "current_period_end" => 1440277253,
                 "ended_at" => nil,
                 "trial_start" => nil,
                 "trial_end" => nil,
                 "canceled_at" => nil,
                 "quantity" => 1,
                 "application_fee_percent" => nil,
                 "discount" => nil,
                 "tax_percent" => nil,
                 "metadata" => {

                 }
              }
           },
           "object" => "event",
           "pending_webhooks" => 0,
           "request" => "req_6enwn7M3uD2GOc",
           "api_version" => "2015-07-07"
        }
    end

    def sample_charge_attrs
        {
            "id" => "evt_16RUKg2nDm85BVWRd160oDNc",
            "created" => 1437598854,
            "livemode" => false,
            "type" => "charge.event",
            "data" => {
                "object" => {
                    "id" => "ch_1BbxQn2nDm85BVWRSLY4zgtm",
                    "object" => "charge",
                    "amount" => 25000,
                    "amount_refunded" => 0,
                    "application" => nil,
                    "application_fee_amount" => nil,
                    "balance_transaction" => "txn_1Bbsyj2nDm85BVWRlYtJ3xDh",
                    "captured" => true,
                    "created" => 1513975669,
                    "currency" => "usd",
                    "customer" => @user.id,
                    "description" => nil,
                    "destination" => nil,
                    "dispute" => nil,
                    "failure_code" => nil,
                    "failure_message" => nil,
                    "fraud_details" => {},
                    "invoice" => "in_16do1o2nDm85BVWR1tA2Xt7S",
                    "livemode" => false,
                    "metadata" => {},
                    "on_behalf_of" => nil,
                    "order" => nil,
                    "outcome" => {
                        "network_status" => "approved_by_network",
                        "reason" => nil,
                        "risk_level" => "normal",
                        "seller_message" => "Payment complete.",
                        "type" => "authorized"
                    },
                    "paid" => true,
                    "receipt_email" => nil,
                    "receipt_number" => nil,
                    "refunded" => false,
                    "refunds" => {
                        "object" => "list",
                        "data" => [],
                        "has_more" => false,
                        "total_count" => 0,
                        "url" => "/v1/charges/ch_1BbxQn2nDm85BVWRSLY4zgtm/refunds"
                    },
                    "review" => nil,
                    "shipping" => nil,
                    "source" => {
                        "id" => "card_1BbxQf2nDm85BVWRLBTFScng",
                        "object" => "card",
                        "address_city" => nil,
                        "address_country" => nil,
                        "address_line1" => nil,
                        "address_line1_check" => nil,
                        "address_line2" => nil,
                        "address_state" => nil,
                        "address_zip" => nil,
                        "address_zip_check" => nil,
                        "brand" => "Visa",
                        "country" => "US",
                        "customer" => "c349b8dd-daff-473c-bcb6-d045b82c0534",
                        "cvc_check" => "pass",
                        "dynamic_last4" => nil,
                        "exp_month" => 1,
                        "exp_year" => 2023,
                        "fingerprint" => "4jyzEmGW8C2ry6xa",
                        "funding" => "credit",
                        "last4" => "4242",
                        "metadata" => {},
                        "name" => "jonathan.cordon@gmail.com",
                        "tokenization_method" => nil
                    },
                    "source_transfer" => nil,
                    "statement_descriptor" => "Monthly Plan",
                    "status" => "succeeded",
                    "transfer_group" => nil
                },
            },
            "object" => "event",
            "pending_webhooks" => 0,
            "request" => "req_6enwn7M3uD2GOc",
            "api_version" => "2015-07-07"
        }
    end

    def sample_dispute_attrs
        {
            "id" => "evt_1GLcAm2nDm85BVWRUDf6Krb1",
            "created" => 1437598854,
            "livemode" => false,
            "type" => "charge.event",
            "data" => {
                "object" => {
                    "id" => "dp_1GLcAk2nDm85BVWRazU9Jrj8",
                    "object" => "dispute",
                    "amount" => 25000,
                    "balance_transaction" => "txn_1GLcAk2nDm85BVWRNIZfQVar",
                    "balance_transactions" => [
                        {
                          "id" => "txn_1GLcAk2nDm85BVWRNIZfQVar",
                          "object" => "balance_transaction",
                          "amount" => -42500,
                          "available_on" => 1584057600,
                          "created" => 1583962382,
                          "currency" => "usd",
                          "description" => "Chargeback withdrawal for ch_1GLcAk2nDm85BVWRN8bxoSnS",
                          "exchange_rate" => nil,
                          "fee" => 1500,
                          "fee_details" => [
                            {
                              "amount" => 1500,
                              "application" => nil,
                              "currency" => "usd",
                              "description" => "Dispute fee",
                              "type" => "stripe_fee"
                            }
                          ],
                          "net" => -44000,
                          "reporting_category" => "dispute",
                          "source" => "dp_1GLcAk2nDm85BVWRazU9Jrj8",
                          "status" => "pending",
                          "type" => "adjustment"
                        }
                    ],
                    "charge" => "ch_1GLcAk2nDm85BVWRN8bxoSnS",
                    "created" => 1513975669,
                    "currency" => "usd",
                    "evidence" => {
                        "access_activity_log" => nil,
                        "billing_address" => nil,
                        "cancellation_policy" => nil,
                        "cancellation_policy_disclosure" => nil,
                        "cancellation_rebuttal" => nil,
                        "customer_communication" => nil,
                        "customer_email_address" => "923f9bc1-76e7-4b9c-b78c-9d2d38bdbfc8@example.com",
                        "customer_name" => "923f9bc1-76e7-4b9c-b78c-9d2d38bdbfc8@example.com",
                        "customer_purchase_ip" => "73.58.190.33",
                        "customer_signature" => nil,
                        "duplicate_charge_documentation" => nil,
                        "duplicate_charge_explanation" => nil,
                        "duplicate_charge_id" => nil,
                        "product_description" => nil,
                        "receipt" => nil,
                        "refund_policy" => nil,
                        "refund_policy_disclosure" => nil,
                        "refund_refusal_explanation" => nil,
                        "service_date" => nil,
                        "service_documentation" => nil,
                        "shipping_address" => nil,
                        "shipping_carrier" => nil,
                        "shipping_date" => nil,
                        "shipping_documentation" => nil,
                        "shipping_tracking_number" => nil,
                        "uncategorized_file" => nil,
                        "uncategorized_text" => nil
                    },
                    "evidence_details" => {
                        "due_by" => 1584748799,
                        "has_evidence" => false,
                        "past_due" => false,
                        "submission_count" => 0
                      },
                      "is_charge_refundable" => false,
                      "livemode" => false,
                      "metadata" => {
                      },
                      "payment_intent" => "pi_1GLcAk2nDm85BVWRHrJoSmCS",
                      "reason" => "fraudulent",
                      "status" => "needs_response"
                },
            },
            "object" => "event",
            "pending_webhooks" => 0,
            "request" => {
                "id" => "req_2z3uebq3BxQU2H",
                "idempotency_key" => "e58cc325-1457-4095-809c-7ab560252545"
            },
            "api_version" => "2015-07-07"
        }
    end

    def mock_invoice_charge
        mock_charge = double(:mock_charge)
        allow(mock_charge).to receive(:statement_descriptor).and_return(:statement_descriptor)
        allow_any_instance_of(Stripe::Charge).to receive(:retrieve).and_return(mock_charge)
    end


end