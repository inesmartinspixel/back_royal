# == Schema Information
#
# Table name: project_progress
#
#  id                     :uuid             not null, primary key
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  user_id                :uuid             not null
#  requirement_identifier :text             not null
#  score                  :float
#  waived                 :boolean          default(FALSE)
#  marked_as_passed       :boolean          default(FALSE)
#  status                 :text             default("unsubmitted"), not null
#  id_verified            :boolean
#

require 'spec_helper'

describe ProjectProgress do

    fixtures :cohorts

    describe "create_or_update!" do
        attr_reader :user, :editor, :now

        before(:each) do
            @user, @editor = User.left_outer_joins(:project_progresses).where("project_progress.id is null").limit(2)
            @now = Time.now
            allow(Time).to receive(:now).and_return(@now)
        end

        it "should create a new record" do
            project_progress = create_record
            expect(project_progress).not_to be_nil
            expect(project_progress.score).to eq(1)
            expect(project_progress.requirement_identifier).to eq('ri')
            expect(user.project_progresses.to_a).to eq([project_progress])
        end

        it "should not create a record if there is no score and none of the flags are set" do
            project_progress = ProjectProgress.create_or_update!(user, editor, {
                requirement_identifier: 'ri',
                score: nil,
                waived: false,
                marked_as_passed: false
            })
            user.project_progresses.reload
            expect(project_progress).to be_nil
            expect(user.project_progresses).to be_empty
        end

        it "should call merge_hash with hash" do
            hash = {
                requirement_identifier: 'ri',
                score: 2,
                status: 'submitted',
                id_verified: true
            }
            expect_any_instance_of(ProjectProgress).to receive(:merge_hash).with(hash)
            ProjectProgress.create_or_update!(user, editor, hash)
        end

        def create_record
            project_progress = ProjectProgress.create_or_update!(user, editor, {
                requirement_identifier: 'ri',
                score: 1
            })
            user.project_progresses.reload
            project_progress
        end

    end

    describe "merge_hash" do

        it "should work" do

            project_progress = ProjectProgress.new
            project_progress.merge_hash({
                score: 5,
                waived: false,
                marked_as_passed: false,
                status: 'submitted',
                id_verified: true
            })
            expect(project_progress.score).to eq(5)
            expect(project_progress.waived).to eq(false)
            expect(project_progress.marked_as_passed).to eq(false)
            expect(project_progress.status).to eq('submitted')
            expect(project_progress.id_verified).to eq(true)
        end

    end

    describe "validate_score_set_in_only_one_way" do

        it "should prevent setting scores in multiple ways" do

            user_id = User.first.id

            project_progress = ProjectProgress.new(
                user_id: user_id,
                score: 1
            )
            expect(project_progress.valid?).to be(true)

            project_progress = ProjectProgress.new(
                user_id: user_id,
                score: 1,
                waived: true
            )
            expect(project_progress.valid?).to be(false)
            expect(project_progress.errors.full_messages).to include("Only one of waived, marked_as_passed, and score can be set")

            project_progress = ProjectProgress.new(
                user_id: user_id,
                score: 1,
                marked_as_passed: true
            )
            expect(project_progress.valid?).to be(false)
            expect(project_progress.errors.full_messages).to include("Only one of waived, marked_as_passed, and score can be set")

            project_progress = ProjectProgress.new(
                user_id: user_id,
                waived: true,
                marked_as_passed: true
            )
            expect(project_progress.valid?).to be(false)
            expect(project_progress.errors.full_messages).to include("Only one of waived, marked_as_passed, and score can be set")
        end
    end


    it "should validate status" do
        project_progress = ProjectProgress.new(
            user_id: User.first.id,
            requirement_identifier: 'rid'
        )

        ProjectProgress.valid_statuses.each do |status|
            project_progress.status = status
            expect(project_progress.valid?).to be(true)
        end

        project_progress.status = 'some_status'
        expect(project_progress.valid?).to be(false)
    end

    describe "unpassed_for_project?" do

        attr_reader :progress_record,:standard_project, :presentation_project, :cohort

        before(:each) do
            @progress_record = ProjectProgress.new
            @standard_project = LearnerProject.new(project_type: LearnerProject::STANDARD_PROJECT_TYPE)
            @presentation_project = LearnerProject.new(project_type: LearnerProject::PRESENTATION_PROJECT_TYPE)
            @cohort = cohorts(:published_mba_cohort)
        end

        describe "when score is present" do

            it "should be false for standard projects with a passing score" do
                progress_record.score = 2
                expect(progress_record.unpassed_for_project?(standard_project, cohort)).to be(false)
            end

            it "should be true for standard projects with a failing score" do
                progress_record.score = 1
                expect(progress_record.unpassed_for_project?(standard_project, cohort)).to be(true)
            end

            it "should be false for presentation projects with a passing score and id_verified" do
                progress_record.score = 2
                progress_record.id_verified = true
                expect(progress_record.unpassed_for_project?(presentation_project, cohort)).to be(false)
            end

            it "should be true for presentation projects with a failing score" do
                progress_record.score = 1
                progress_record.id_verified = true
                expect(progress_record.unpassed_for_project?(presentation_project, cohort)).to be(true)
            end

            it "should be true for presentation projects with a passing score and !id_verified" do
                progress_record.score = 2
                progress_record.id_verified = false
                expect(progress_record.unpassed_for_project?(presentation_project, cohort)).to be(true)
            end

        end

        it "should be false if waived" do
            progress_record.waived = true
            expect(progress_record.unpassed_for_project?(standard_project, cohort)).to be(false)
        end

        it "should be false if marked_as_passed" do
            progress_record.marked_as_passed = true
            expect(progress_record.unpassed_for_project?(standard_project, cohort)).to be(false)
        end

        it "should be true otherwise" do
            progress_record.score = nil
            progress_record.waived = false
            progress_record.marked_as_passed = false
            expect(progress_record.unpassed_for_project?(standard_project, cohort)).to be(true)
        end

    end

end
