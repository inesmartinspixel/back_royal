# == Schema Information
#
# Table name: refunds
#
#  id                      :uuid             not null, primary key
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  billing_transaction_id  :uuid             not null
#  provider                :text             not null
#  provider_transaction_id :text             not null
#  refund_time             :datetime         not null
#  amount                  :float            not null
#

require 'spec_helper'
require 'stripe_helper'

describe Refund do

    include StripeHelper

    describe "find_or_create_by_stripe_refund" do
        it "should work" do
            expect(Raven).not_to receive(:capture_exception)

            billing_transaction = BillingTransaction.create!(
                transaction_time: Time.now,
                transaction_type: 'payment',
                amount: 10,
                amount_refunded: 0,
                currency: 'usd',
                provider: BillingTransaction::PROVIDER_STRIPE,
                provider_transaction_id: 1337
            )

            Refund.find_or_create_by_stripe_refund(OpenStruct.new(created: Time.now, amount: 500, id: 1, charge: 1337))
            expect(Refund.where(provider_transaction_id: 1).size).to be(1)

            Refund.find_or_create_by_stripe_refund(OpenStruct.new(created: Time.now, amount: 500, id: 1, charge: 1337))
            expect(Refund.where(provider_transaction_id: 1).size).to be(1)

            Refund.find_or_create_by_stripe_refund(OpenStruct.new(created: Time.now, amount: 500, id: 2, charge: 1337))
            expect(Refund.where(provider_transaction_id: 2).size).to be(1)

            billing_transaction.reload
            expect(billing_transaction.refunds.size).to be(2)
            expect(billing_transaction.amount_refunded).to eq(10)
        end
    end
end
