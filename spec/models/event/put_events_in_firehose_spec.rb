require 'spec_helper'

describe Event::PutEventsInFirehose do
    attr_accessor :delivery_stream_name, :mock_firehose_client

    before(:each) do
        self.delivery_stream_name = 'delivery_stream_name'
        allow(Event::PutEventsInFirehose).to receive(:delivery_stream_name) { delivery_stream_name }

        self.mock_firehose_client = 'mock_firehose_client'
        allow(Aws::Firehose::Client).to receive(:new).and_return(mock_firehose_client)
    end

    describe :handled_properties do

        it "should work" do
            # This assertion is really asserting the behavior of the private map_payload method,
            # but it's important to make sure that we don't bother calling auto_truncate_payload
            # in the handled_properties case because it can result in unnecessary errors being
            # thrown. See https://trello.com/c/HblQ3Umr.
            expect(Event::PutEventsInFirehose).not_to receive(:auto_truncate_payload)

            handled_properties = Event::PutEventsInFirehose.handled_properties
            expect(handled_properties).to include('subscription_id')
            expect(handled_properties).to include('feedbackIsPositive')
        end
    end

    describe :put do

        it "should do nothing if no delivery_stream_name" do
            mock_events = 'events'
            self.delivery_stream_name = nil
            expect(Event::PutEventsInFirehose).not_to receive(:serialize_event)
            Event::PutEventsInFirehose.put([mock_event])
        end


        it "should log serialized_events to firehose" do
            mock_events = 'events'
            expect(Event::PutEventsInFirehose).to receive(:serialize_event).exactly(2).times.and_return('serialized')
            expect(mock_firehose_client).to receive(:put_record_batch).with(
                delivery_stream_name: delivery_stream_name,
                records: [{data: 'serialized'}, {data: 'serialized'}]
            )
            Event::PutEventsInFirehose.put([mock_event, mock_event])
        end

        it "should return the response" do
            expect(mock_firehose_client).to receive(:put_record_batch).and_return('response')
            expect(Event::PutEventsInFirehose.put([mock_event])).to eq('response')
        end
    end

    describe :put_and_queue_failures do

        it "should queue all persisted events if request fails" do
            err = RuntimeError.new("The sky is falling!")
            expect(Event::PutEventsInFirehose).to receive(:put).and_raise(err)

            calls = []
            expect(PutFailedEventsInFirehoseJob).to receive(:perform_later).exactly(2).times do |id|
                calls << id
            end
            expect(Raven).to receive(:capture_exception).with(err)

            Event::PutEventsInFirehose.put_and_queue_failures([{'id' => 1}, {'id' => 2}, {'id' => 3, 'event_type' => 'click'}])
            expect(calls).to match_array([1,2])
        end

        it "should queue failed events" do
            expect(Event::PutEventsInFirehose).to receive(:put).and_return(
                OpenStruct.new({
                    request_responses: [
                        OpenStruct.new({
                            error_code: nil
                        }),
                        OpenStruct.new({
                            error_code: 42,
                            error_message: 'All messed up'
                        }),
                        OpenStruct.new({
                            error_code: 42,
                            error_message: 'FUBAR'
                        })
                    ]
                })
            )

            expect(PutFailedEventsInFirehoseJob).to receive(:perform_later).with(2)
            expect(Raven).to receive(:capture_exception).with('Firehose error: All messed up', {
                level: 'warning',
                extra: {
                    error_code: 42,
                    event_id: 2
                },
                fingerprint: ['Failed to send events to firehose', 'All messed up']
            })

            Event::PutEventsInFirehose.put_and_queue_failures([{'id' => 1}, {'id' => 2}, {'id' => 3, 'event_type' => 'click'}])
        end
    end

    describe :serialize_event do
        it "should remove nil keys" do
            expect(to_serialized_hash({'payload' => {}}).keys).not_to include('failure_code')
        end

        it "should include things from map_columns" do
            time = Time.parse('2018/01/01 12:42')
            serialized_time = to_serialized_hash({'created_at' => time})['created_at']
            expect(Time.find_zone("UTC").parse(serialized_time)).to eq(time)
        end

        it "should include things from payload" do
            expect(to_serialized_hash({'payload' => {'aborted' => true}})).to have_entries({'aborted' => true})
        end

        it "should use limit_string for truncation" do
            # if we do truncation ourselves, we should not hit the auto-truncation
            expect(LogToS3).not_to receive(:log)

            # make a string that is 60 characters long
            string = ''
            60.times { string += 'a'}

            # confirm that it gets truncated to 56
            expect(to_serialized_hash({'payload' => {'origModalKeys' => string}})['orig_modal_keys'].size).to eq(56)
        end

        describe 'autotruncate' do
            attr_accessor :column_name, :max_length, :long_string

            before(:each) do
                self.column_name = 'stripe_coupon_id'
                column =  RedshiftEvent.columns_hash[column_name]
                expect(column.sql_type).to eq('character varying(128)') # sanity check
                self.max_length = 128


                # make a string that is too long
                self.long_string = ''
                (max_length + 5).times { self.long_string += 'a'}

                allow(LogToS3).to receive(:log)
            end

            it "should autotruncate string columns" do
                # confirm that it gets truncated
                serialized = to_serialized_hash({
                    'payload' => {
                        column_name => long_string
                    }
                })
                expect(serialized[column_name].size).to eq(max_length)
            end

            it "should autotruncate json columns" do
                # confirm that it gets truncated
                serialized = to_serialized_hash({
                    'payload' => {
                        column_name => {'prop' => long_string}
                    }
                })
                expect(serialized[column_name].size).to eq(max_length)
            end

            it "should send log to sentry and s3 when autotruncating" do
                event = Event.first
                event.payload[column_name] = long_string

                another_column_name = 'hiring_manager_status'
                event.payload[another_column_name] = long_string
                expect(LogToS3).to receive(:log).with('auto-truncated-events', "#{event.id} - #{another_column_name},#{column_name}", event.to_json)
                expect(Raven).to receive(:capture_exception).with('Auto-truncated event before sending to Kinesis Firehose', {
                    level: 'warning',
                    extra: {
                        event_id: event['id'],
                        auto_truncated_columns: [another_column_name, column_name],
                        original_event_stored_at: "https://s3.console.aws.amazon.com/s3/buckets/#{LogToS3.bucket_name}/auto-truncated-events/?region=#{LogToS3.region}&tab=overview"
                    }
                })

                to_serialized_hash(event)
            end
        end



        def to_serialized_hash(e)
            serialized = Event::PutEventsInFirehose.send(:serialize_event, e)
            ActiveSupport::JSON.decode(serialized)
        end
    end

    def mock_event(more = {})
        {'created_at' => Time.now}.merge(more.stringify_keys)
    end
end