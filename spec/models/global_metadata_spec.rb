# == Schema Information
#
# Table name: global_metadata
#
#  id                    :uuid             not null, primary key
#  created_at            :datetime
#  updated_at            :datetime
#  site_name             :string
#  default_title         :string
#  default_description   :string
#  default_image_id      :uuid
#  default_canonical_url :string
#

require 'spec_helper'

describe GlobalMetadata do
    describe "update" do

        it "should update_from_hash!" do
            global_metadata_hash = {
                "site_name" => "smartly",
                "default_title" => "a concept",
                "default_description" => "the awesomeness concept",
                "default_canonical_url" => "stuff.com"
            }

            global_metadata = GlobalMetadata.create!(global_metadata_hash)

            global_metadata_hash = {
                "default_title" => "another title",
                "site_name" => "smartly",
                "id" => global_metadata.id
            }
            global_metadata = GlobalMetadata.update_from_hash!(global_metadata_hash)
            expect(global_metadata.default_title).to eq("another title")

        end

    end

end
