# == Schema Information
#
# Table name: career_profiles
#
#  id                                                   :uuid             not null, primary key
#  created_at                                           :datetime
#  updated_at                                           :datetime
#  user_id                                              :uuid
#  score_on_gmat                                        :text
#  score_on_sat                                         :text
#  score_on_act                                         :text
#  short_answers                                        :json             not null
#  willing_to_relocate                                  :boolean          default(TRUE), not null
#  open_to_remote_work                                  :boolean          default(TRUE), not null
#  authorized_to_work_in_us                             :text
#  top_mba_subjects                                     :text             default([]), not null, is an Array
#  top_personal_descriptors                             :text             default([]), not null, is an Array
#  top_motivations                                      :text             default([]), not null, is an Array
#  top_workplace_strengths                              :text             default([]), not null, is an Array
#  bio                                                  :text
#  personal_fact                                        :text
#  preferred_company_culture_descriptors                :text             default([]), not null, is an Array
#  li_profile_url                                       :text
#  resume_id                                            :uuid
#  personal_website_url                                 :text
#  blog_url                                             :text
#  tw_profile_url                                       :text
#  fb_profile_url                                       :text
#  job_sectors_of_interest                              :text             default([]), not null, is an Array
#  employment_types_of_interest                         :text             default([]), not null, is an Array
#  company_sizes_of_interest                            :text             default([]), not null, is an Array
#  primary_areas_of_interest                            :text             default([]), not null, is an Array
#  place_id                                             :text
#  place_details                                        :json             not null
#  last_calculated_complete_percentage                  :float
#  sat_max_score                                        :text             default("1600"), not null
#  score_on_gre_verbal                                  :text
#  score_on_gre_quantitative                            :text
#  score_on_gre_analytical                              :text
#  short_answer_greatest_achievement                    :text
#  primary_reason_for_applying                          :text
#  do_not_create_relationships                          :boolean          default(FALSE), not null
#  salary_range                                         :text
#  short_answer_leadership_challenge                    :text
#  interested_in_joining_new_company                    :text
#  locations_of_interest                                :text             default(["\"none\""]), not null, is an Array
#  profile_feedback                                     :text
#  feedback_last_sent_at                                :datetime
#  location                                             :geography(Point,
#  github_profile_url                                   :text
#  long_term_goal                                       :text
#  last_confirmed_at_by_student                         :datetime
#  last_confirmed_at_by_internal                        :datetime
#  last_updated_at_by_student                           :datetime
#  last_confirmed_internally_by                         :string
#  short_answer_scholarship                             :string
#  consider_early_decision                              :boolean
#  student_network_looking_for                          :text             default([]), not null, is an Array
#  native_english_speaker                               :boolean
#  earned_accredited_degree_in_english                  :boolean
#  survey_years_full_time_experience                    :text
#  survey_most_recent_role_description                  :text
#  survey_highest_level_completed_education_description :text
#  short_answer_why_pursuing                            :text
#  short_answer_use_skills_to_advance                   :text
#  short_answer_ask_professional_advice                 :text
#  has_no_formal_education                              :boolean          default(FALSE), not null
#  sufficient_english_work_experience                   :boolean
#  english_work_experience_description                  :text
#

require 'spec_helper'

describe CareerProfile do

    fixtures(:users)

    it "should reidentify user if profile changes" do
        instance = users(:user_with_career_profile).career_profile

        expect_any_instance_of(User).to receive(:identify)
        instance.bio = "foo"
        instance.save!
    end

    it "should call update_in_airtable on the user if profile changes" do
        instance = users(:user_with_career_profile).career_profile

        expect_any_instance_of(User).to receive(:update_in_airtable)
        instance.bio = "foo"
        instance.save!
    end

    it "should create a server event if feedback_last_sent_at changes" do
        expected_feedback = 'This is some feedback'
        allow(Event).to receive(:create_server_event!).and_call_original

        user = users(:user_with_career_profile)
        career_profile = user.career_profile
        career_profile.profile_feedback = expected_feedback
        career_profile.feedback_last_sent_at = Time.now
        career_profile.save!
        expect(Event).to have_received(:create_server_event!).with(
            anything,
            career_profile.user_id,
            'career_profile:send_feedback',
            {
                cohort_title: user.relevant_cohort.title,
                profile_feedback: expected_feedback
            }
        )
    end

    it "should not create a server event if feedback_last_sent_at does not change" do
        expected_feedback = 'This is some feedback'
        career_profile = users(:user_with_career_profile).career_profile
        allow(Event).to receive(:create_server_event!).and_call_original
        career_profile.profile_feedback = expected_feedback
        career_profile.save!
        expect(Event).not_to have_received(:create_server_event!)
    end

    describe "create_from_hash!" do

        it "should work" do
            expect_any_instance_of(CareerProfile).to receive(:merge_hash).and_call_original
            attrs = valid_attrs
            instance = CareerProfile.create_from_hash!(attrs)
            expect(instance.user_id).to eq(attrs['user_id'])
        end

    end

    describe "update_from_hash" do

        it "should work" do
            instance = users(:user_with_career_profile).career_profile
            expect_any_instance_of(CareerProfile).to receive(:merge_hash).and_call_original
            CareerProfile.update_from_hash!({
                "id" => instance.id,
                "authorized_to_work_in_us" => "I'll get my visa at the border"
            })
            expect(instance.reload.authorized_to_work_in_us).to eq("I'll get my visa at the border")
        end

        it "should not update the user_id" do
            user = users(:user_with_career_profile)
            instance = user.career_profile
            another_user = User.where.not(id: user.id).first
            CareerProfile.update_from_hash!({
                "id" => instance.id,
                "user_id" => another_user.id
            })
            expect(instance.reload.user_id).to eq(user.id)
        end

    end

    describe "merge_hash" do

        before(:each) do
            @resume_id = SecureRandom.uuid
        end

        it "should set basic info" do
            career_profile = CareerProfile.new
            career_profile.merge_hash(basic_info_attrs)
            assert_attrs(career_profile, basic_info_attrs.except("user_id"))
        end

        it "should set education fields" do
            career_profile = CareerProfile.new
            career_profile.merge_hash(education_attrs)
            assert_attrs(career_profile, education_attrs)
        end

        it "should set work fields" do
            career_profile = CareerProfile.new
            career_profile.merge_hash(work_attrs)
            assert_attrs(career_profile, work_attrs)
        end

        it "should set test scores" do
            career_profile = CareerProfile.new
            career_profile.merge_hash(test_score_attrs)
            assert_attrs(career_profile, test_score_attrs)
        end

        it "should set application questions" do
            career_profile = CareerProfile.new
            career_profile.merge_hash(application_questions_attrs)
            assert_attrs(career_profile, application_questions_attrs)
        end

        it "should add job preferences" do
            career_profile = CareerProfile.new
            career_profile.merge_hash(job_preferences_attrs)
            assert_attrs(career_profile, job_preferences_attrs)
        end

        it "should add personal bio" do
            career_profile = CareerProfile.new
            career_profile.merge_hash(personal_bio_attrs)
            assert_attrs(career_profile, personal_bio_attrs)
        end

        it "should add employer preferences" do
            career_profile = CareerProfile.new
            career_profile.merge_hash(employer_preferences_attrs)
            assert_attrs(career_profile, employer_preferences_attrs)
        end

        it "should add resume and links" do
            career_profile = CareerProfile.new
            career_profile.merge_hash(resume_and_links_attrs)
            assert_attrs(career_profile, resume_and_links_attrs)
        end

        it "should should normalize and URL fields" do
            career_profile = CareerProfile.new
            resume_and_links_attrs.merge({ :li_profile_url => "another_value" })
            resume_and_links_attrs.merge({ :li_profile_url => "https://another_value" })
            career_profile.merge_hash(resume_and_links_attrs)
            assert_attrs(career_profile, resume_and_links_attrs)
        end

        it "should set last_calculated_complete_percentage" do
            career_profile = CareerProfile.new
            expect(career_profile.last_calculated_complete_percentage).not_to equal(0.42)
            career_profile.merge_hash({:last_calculated_complete_percentage => 0.42})
            expect(career_profile.last_calculated_complete_percentage).to equal(0.42)
        end

        it "should add profile feedback" do
            career_profile = CareerProfile.new
            career_profile.merge_hash(profile_feedback_attrs)
            assert_attrs(career_profile, profile_feedback_attrs)
        end

        it "should update feedback_last_sent_at if send_feedback_email is included in meta param" do
            career_profile = CareerProfile.new
            expect(career_profile.feedback_last_sent_at).to be_nil
            career_profile.merge_hash(profile_feedback_attrs, :send_feedback_email => true)
            expect(career_profile.feedback_last_sent_at).not_to be_nil
        end

        it "should not update feedback_last_sent_at if send_feedback_email is not included in meta param" do
            career_profile = CareerProfile.new
            expect(career_profile.feedback_last_sent_at).to be_nil
            career_profile.merge_hash(profile_feedback_attrs)
            expect(career_profile.feedback_last_sent_at).to be_nil
        end

        it "should update last_updated_at_by_student" do
            career_profile = CareerProfile.new
            expect(career_profile.last_updated_at_by_student).to be_nil
            now = Time.now
            career_profile.merge_hash(:last_updated_at_by_student => now)
            expect(career_profile.last_updated_at_by_student).not_to be_nil
            expect(career_profile.last_updated_at_by_student).to eq(now)
        end

        it "should update last_confirmed_at_by_internal and last_confirmed_internally_by if confirmed_by_internal is included in meta param" do
            career_profile = CareerProfile.new
            expect(career_profile.last_confirmed_at_by_student).to be_nil
            expect(career_profile.last_confirmed_at_by_internal).to be_nil
            expect(career_profile.last_confirmed_internally_by).to be_nil
            career_profile.merge_hash(profile_feedback_attrs, :confirmed_by_internal => true, :confirmed_internally_by => "Foo Bar")
            expect(career_profile.last_confirmed_at_by_student).to be_nil
            expect(career_profile.last_confirmed_at_by_internal).not_to be_nil
            expect(career_profile.last_confirmed_internally_by).to eq("Foo Bar")
        end

        it "should raise if confirmed_by_internal, but confirmed_internally_by is not present" do
            career_profile = CareerProfile.new
            expect {
                career_profile.merge_hash(profile_feedback_attrs, :confirmed_by_internal => true)
            }.to raise_error("Must specify internal user who confirmed career profile if confirmed_by_internal")
        end

        it "should update last_confirmed_at_by_student if confirmed_by_student is included in meta param" do
            career_profile = CareerProfile.new
            expect(career_profile.last_confirmed_at_by_student).to be_nil
            career_profile.merge_hash(profile_feedback_attrs, :confirmed_by_student => true)
            expect(career_profile.last_confirmed_at_by_student).not_to be_nil
        end

        describe "backfill locations_of_interest=none for old clients" do
            it "should set locations_of_interest=['none'] if an old client is updating to willing_to_relocate=false" do
                career_profile = CareerProfile.create!({
                    locations_of_interest: ['austin', 'flexible'],
                    user_id: user_without_profile.id
                })

                career_profile.merge_hash(career_profile.as_json(view: 'editable').merge({
                    willing_to_relocate: false
                }))

                assert_attrs(career_profile, {
                    :locations_of_interest => ["none"]
                })
            end

            describe "invalid locations_of_interest" do
                before(:each) do
                    @career_profile = CareerProfile.create!({
                        user_id: user_without_profile.id
                    })
                end

                it "should update invalid locations_of_interest" do
                    @career_profile.merge_hash({
                        locations_of_interest: ['none', 'new_york']
                    })

                    assert_attrs(@career_profile, {
                        :locations_of_interest => ["new_york"]
                    })
                end

                it "should not update valid locations_of_interest" do
                    @career_profile.merge_hash({
                        locations_of_interest: ['washington_dc', 'new_york']
                    })

                    assert_attrs(@career_profile, {
                        :locations_of_interest => ['washington_dc', 'new_york']
                    })
                end

                it "should not update valid locations_of_interest when none" do
                    @career_profile.merge_hash({
                        locations_of_interest: ['none']
                    })

                    assert_attrs(@career_profile, {
                        :locations_of_interest => ['none']
                    })
                end
            end
        end

        describe "more_about_you fields" do
            attr_accessor :career_profile

            before(:each) do
                @career_profile = CareerProfile.new({
                    user_id: user_without_profile.id
                })
                career_profile.merge_hash(more_about_you_attrs)
            end

            it "should add skills and awards" do
                assert_attrs(career_profile, more_about_you_attrs)
                career_profile.save!
                assert_attrs(career_profile.reload, more_about_you_attrs)

                # check that doing it over again doesn't screw it up
                career_profile.merge_hash(more_about_you_attrs)
                assert_attrs(career_profile, more_about_you_attrs)
                career_profile.save!
                assert_attrs(career_profile.reload, more_about_you_attrs)
            end

            it "should overwrite skills and awards after save" do
                new_attrs = more_about_you_attrs({
                    :skills                => [{ text: "c", locale: "en"}, { text: "d", locale: "en" }],
                    :awards_and_interests  => [{ text: "c", locale: "en"}, { text: "d", locale: "en" }]
                })
                career_profile.merge_hash(new_attrs)
                assert_attrs(career_profile, new_attrs)
                career_profile.save!
                assert_attrs(career_profile.reload, new_attrs)
            end

            it "should assign position on save" do
                career_profile = CareerProfile.new({
                    user_id: user_without_profile.id
                })
                attrs_with_flipped_lists = more_about_you_attrs.merge({
                    :skills                => more_about_you_attrs[:skills].reverse,
                    :awards_and_interests  => more_about_you_attrs[:awards_and_interests].reverse
                })
                career_profile.merge_hash(attrs_with_flipped_lists)
                assert_attrs(career_profile, attrs_with_flipped_lists)
                career_profile.save!
                assert_attrs(career_profile.reload, attrs_with_flipped_lists)
            end

        end

        describe "skills" do
            attr_accessor :career_profile

            before(:each) do
                @career_profile = CareerProfile.new({
                    user_id: User.left_outer_joins(:career_profile).where("career_profiles.id is null").first.id
                })
                career_profile.merge_hash(skills_attrs)
            end

            it "should add skills" do
                assert_attrs(career_profile, skills_attrs)
                career_profile.save!
                assert_attrs(career_profile.reload, skills_attrs)

                # check that doing it over again doesn't screw it up
                career_profile.merge_hash(skills_attrs)
                assert_attrs(career_profile, skills_attrs)
                career_profile.save!
                assert_attrs(career_profile.reload, skills_attrs)
            end

            it "should overwrite skills after save" do
                new_attrs = skills_attrs({
                    :skills  => [{ text: "c", locale: "en"}, { text: "d", locale: "en" }]
                })
                career_profile.merge_hash(new_attrs)
                assert_attrs(career_profile, new_attrs)
                career_profile.save!
                assert_attrs(career_profile.reload, new_attrs)
            end

            it "should assign position on save" do
                career_profile = CareerProfile.new({
                    user_id: User.left_outer_joins(:career_profile).where("career_profiles.id is null").first.id
                })
                attrs_with_flipped_lists = skills_attrs.merge({
                    :skills  => skills_attrs[:skills].reverse
                })
                career_profile.merge_hash(attrs_with_flipped_lists)
                assert_attrs(career_profile, attrs_with_flipped_lists)
                career_profile.save!
                assert_attrs(career_profile.reload, attrs_with_flipped_lists)
            end
        end

        describe "student_network_interests" do
            attr_accessor :career_profile

            before(:each) do
                @career_profile = CareerProfile.new({
                    user_id: User.left_outer_joins(:career_profile).where("career_profiles.id is null").first.id
                })
                career_profile.merge_hash(student_network_interests_attrs)
            end

            it "should add student_network_interests" do
                assert_attrs(career_profile, student_network_interests_attrs)
                career_profile.save!
                assert_attrs(career_profile.reload, student_network_interests_attrs)

                # check that doing it over again doesn't screw it up
                career_profile.merge_hash(student_network_interests_attrs)
                assert_attrs(career_profile, student_network_interests_attrs)
                career_profile.save!
                assert_attrs(career_profile.reload, student_network_interests_attrs)
            end

            it "should overwrite student_network_interests after save" do
                new_attrs = student_network_interests_attrs({
                    :student_network_interests  => [{ text: "c", locale: "en"}, { text: "d", locale: "en" }]
                })
                career_profile.merge_hash(new_attrs)
                assert_attrs(career_profile, new_attrs)
                career_profile.save!
                assert_attrs(career_profile.reload, new_attrs)
            end

            it "should assign position on save" do
                career_profile = CareerProfile.new({
                    user_id: User.left_outer_joins(:career_profile).where("career_profiles.id is null").first.id
                })
                attrs_with_flipped_lists = student_network_interests_attrs.merge({
                    :student_network_interests  => student_network_interests_attrs[:student_network_interests].reverse
                })
                career_profile.merge_hash(attrs_with_flipped_lists)
                assert_attrs(career_profile, attrs_with_flipped_lists)
                career_profile.save!
                assert_attrs(career_profile.reload, attrs_with_flipped_lists)
            end
        end

        describe "update_associations_from_hash!" do

            attr_accessor :career_profile

            before(:each) do
                @career_profile = users(:user_with_career_profile).career_profile
            end

            describe "education experiences" do

                describe "adding an education experience" do

                    it "should validate will_not_complete when degree_program" do
                        education_experience = CareerProfile::EducationExperience.new(
                            career_profile_id: @career_profile.id,
                            degree_program: true
                        )
                        expect(education_experience).not_to be_valid
                    end

                    it "should not validate will_not_complete when not degree_program" do
                        education_experience = CareerProfile::EducationExperience.new(
                            career_profile_id: @career_profile.id,
                            degree_program: false
                        )
                        expect(education_experience).to be_valid
                    end

                    it "should support adding a complete formal degree program education experience" do
                        career_profile.update_associations_from_hash!({
                            "education_experiences" => [education_experience_attrs]
                        })

                        expect(career_profile.education_experiences.size).to eq(1)
                        career_profile.save!
                        expect(CareerProfile::EducationExperience.where(career_profile_id: career_profile.id).count).to eq(1)
                        assert_attrs(career_profile.education_experiences.first, education_experience_attrs)
                    end

                    it "should support adding an incomplete formal degree program education experience" do
                        updated_attrs = education_experience_attrs.merge({
                            "will_not_complete" => true
                        })

                        career_profile.update_associations_from_hash!({
                            "education_experiences" => [updated_attrs]
                        })

                        expect(career_profile.education_experiences.size).to eq(1)
                        career_profile.save!
                        expect(CareerProfile::EducationExperience.where(career_profile_id: career_profile.id).count).to eq(1)
                        assert_attrs(career_profile.education_experiences.first, updated_attrs)
                    end

                    it "should support adding a non-degree program education experience" do
                        updated_attrs = education_experience_attrs.merge({
                            "degree_program" => false
                        })

                        career_profile.update_associations_from_hash!({
                            "education_experiences" => [updated_attrs]
                        })

                        expect(career_profile.education_experiences.size).to eq(1)
                        career_profile.save!
                        expect(CareerProfile::EducationExperience.where(career_profile_id: career_profile.id).count).to eq(1)
                        assert_attrs(career_profile.education_experiences.first, updated_attrs)
                    end

                end

                it "should update an education experience" do
                    expect(career_profile.education_experiences.size).to eq(1) # sanity check
                    education_experience = career_profile.education_experiences.first

                    # grab the existing attributes from the existing experience
                    # and change a couple things so we can make sure the changes
                    # get persisted
                    updated_attrs = education_experience.attributes.merge({
                        "graduation_year" => 1942,
                        "educational_organization" => { text: "haaarvard", locale: "en" }
                    })

                    career_profile.update_associations_from_hash!({
                        "education_experiences" => [updated_attrs]
                    })

                    expect(career_profile.education_experiences.size).to eq(1)
                    expected_attrs = updated_attrs.except("updated_at", "educational_organization_option_id", "official_transcript_required")
                    assert_attrs(career_profile.education_experiences.first, expected_attrs)

                    career_profile.save!

                    expect(CareerProfile::EducationExperience.where(career_profile_id: career_profile.id).count).to eq(1)
                    assert_attrs(career_profile.education_experiences.first.reload, expected_attrs)

                end

                it "should remove an education experience" do
                    expect(career_profile.education_experiences.size).to eq(1) # sanity check
                    expect(CareerProfile::EducationExperience.where(career_profile_id: career_profile.id).count).to eq(1)
                    career_profile.update_associations_from_hash!({
                        "education_experiences" => []
                    })
                    expect(CareerProfile::EducationExperience.where(career_profile_id: career_profile.id).count).to eq(0)
                end

                it "should order education experiences by graduation_year" do

                    career_profile.update_associations_from_hash!({
                        "education_experiences" => [
                            education_experience_attrs.merge({ :graduation_year => 2000 }),
                            education_experience_attrs.merge({
                                :graduation_year => 2010,
                                :educational_organization => {
                                    :text => "Assassin University",
                                    :locale => "en"
                                }
                            })
                        ]
                    })
                    career_profile.save!
                    career_profile.reload

                    expect(career_profile.education_experiences.count).to eq(2)
                    expect(career_profile.education_experiences[0].educational_organization.text).to eq("Assassin University")
                    expect(career_profile.education_experiences[1].educational_organization.text).to eq(education_experience_attrs[:educational_organization][:text])
                end

                it "should not error on multiple organizations with the same name" do
                    career_profile.update_associations_from_hash!({
                        "education_experiences" => [
                            education_experience_attrs.merge({ :graduation_year => 2000 }),
                            education_experience_attrs.merge({ :graduation_year => 2005 })
                        ]
                    })
                    career_profile.save!
                    career_profile.reload

                    expect(career_profile.education_experiences.count).to eq(2)
                    expect(career_profile.education_experiences[0].educational_organization.text).to eq(education_experience_attrs[:educational_organization][:text])
                    expect(career_profile.education_experiences[1].educational_organization.text).to eq(education_experience_attrs[:educational_organization][:text])
                end

                it "should re-assign official_transcript_required when necessary" do

                    expect(career_profile.education_experiences).not_to be_empty

                    # NOTE: this will destroy the existing education experience.  That is
                    # actually important to test, because we've seen bugs in determine_transcript_requirements
                    # when an education_experience is being destroyed
                    career_profile.update_associations_from_hash!({
                        "education_experiences" => [
                            education_experience_attrs.merge({ :graduation_year => 2000, :degree => 'juris_doctor' }),
                            education_experience_attrs.merge({ :graduation_year => 2005, :degree => 'doctor_of_education' })
                        ]
                    })

                    experience_1 = career_profile.education_experiences.detect { |exp| exp.degree == 'juris_doctor'}
                    experience_2 = career_profile.education_experiences.detect { |exp| exp.degree == 'doctor_of_education'}

                    career_profile.save!
                    reloaded_career_profile = CareerProfile.find(career_profile.id) # reload just to be sure

                    expect(experience_1.reload.official_transcript_required).to be(false)
                    expect(experience_2.reload.official_transcript_required).to be(true)
                    reloaded_career_profile.update_associations_from_hash!({
                        "education_experiences" => [
                            experience_1.attributes.merge({ :graduation_year => 2007, :degree => 'juris_doctor' }),
                            experience_2.attributes
                        ]
                    })
                    reloaded_career_profile.save!
                    expect(experience_1.reload.official_transcript_required).to be(true)
                    expect(experience_2.reload.official_transcript_required).to be(false)
                end

                describe "transcripts_verified" do

                    it "should set transcripts_verified to true if is_admin and user.should_have_transcripts_auto_verified?" do
                        expect(career_profile.user).to receive(:should_have_transcripts_auto_verified?).and_return(true)
                        expect(career_profile.user).to receive(:update!).with(transcripts_verified: true).and_call_original

                        career_profile.update_associations_from_hash!({
                            "education_experiences" => [
                                education_experience_attrs.merge({ :graduation_year => 2000, :degree => 'juris_doctor' }),
                                education_experience_attrs.merge({ :graduation_year => 2005, :degree => 'doctor_of_education' })
                            ]
                        }, {:is_admin => true})
                        expect(career_profile.reload.user.transcripts_verified).to be(true)
                    end

                    it "should not set transcripts_verified if no meta is passed in" do
                        expect(career_profile.user).not_to receive(:update!)

                        career_profile.update_associations_from_hash!({
                            "education_experiences" => [
                                education_experience_attrs.merge({ :graduation_year => 2000, :degree => 'juris_doctor' }),
                                education_experience_attrs.merge({ :graduation_year => 2005, :degree => 'doctor_of_education' })
                            ]
                        })
                        expect(career_profile.reload.user.transcripts_verified).to be(false)
                    end

                    it "should not set transcripts_verified if !is_admin" do
                        expect(career_profile.user).not_to receive(:update!)

                        career_profile.update_associations_from_hash!({
                            "education_experiences" => [
                                education_experience_attrs.merge({ :graduation_year => 2000, :degree => 'juris_doctor' }),
                                education_experience_attrs.merge({ :graduation_year => 2005, :degree => 'doctor_of_education' })
                            ]
                        }, {:is_admin => false})
                        expect(career_profile.reload.user.transcripts_verified).to be(false)
                    end

                    it "should not set transcripts_verified if !user.should_have_transcripts_auto_verified?" do
                        expect(career_profile.user).to receive(:should_have_transcripts_auto_verified?).and_return(false)
                        expect(career_profile.user).not_to receive(:update!)

                        career_profile.update_associations_from_hash!({
                            "education_experiences" => [
                                education_experience_attrs.merge({ :graduation_year => 2000, :degree => 'juris_doctor' }),
                                education_experience_attrs.merge({ :graduation_year => 2005, :degree => 'doctor_of_education' })
                            ]
                        }, {:is_admin => true})
                        expect(career_profile.reload.user.transcripts_verified).to be(false)
                    end
                end

            end

            describe "peer recommendations" do

                it "should add a peer recommendation" do
                    career_profile.update_associations_from_hash!({
                        "peer_recommendations" => [peer_recommendation_attrs]
                    })

                    expect(career_profile.peer_recommendations.size).to eq(1)
                    career_profile.save!
                    expect(PeerRecommendation.where(career_profile_id: career_profile.id).count).to eq(1)
                    assert_attrs(career_profile.peer_recommendations.first, peer_recommendation_attrs)
                end

                it "should remove old record and create new one if email has changed" do
                    peer_recommendation = PeerRecommendation.create!(peer_recommendation_attrs.merge({career_profile_id: career_profile.id}))
                    expect(career_profile.peer_recommendations.size).to eq(1) # sanity check
                    expect(PeerRecommendation.where(career_profile_id: career_profile.id).count).to eq(1)

                    # change the email attribute on the hash that's going to be passed in to the udpate_associations_from_hash! call
                    modified_peer_recommendation = peer_recommendation.attributes.with_indifferent_access.merge({ 'email': 'joe.schmoe@gmail.com' })
                    career_profile.update_associations_from_hash!({
                        "peer_recommendations" => [modified_peer_recommendation]
                    })
                    career_profile.save!

                    expect(career_profile.peer_recommendations.size).to eq(1) # sanity check
                    expect(PeerRecommendation.where(career_profile_id: career_profile.id).count).to eq(1)
                    expect(PeerRecommendation.where(career_profile_id: career_profile.id).first.id).not_to eq(peer_recommendation.id) # should be a new record
                    expect(PeerRecommendation.where(career_profile_id: career_profile.id).first.email).to eq('joe.schmoe@gmail.com')
                end

                it "should remove a peer recommendation" do
                    PeerRecommendation.create!(peer_recommendation_attrs.merge({career_profile_id: career_profile.id}))
                    expect(career_profile.peer_recommendations.size).to eq(1) # sanity check
                    expect(PeerRecommendation.where(career_profile_id: career_profile.id).count).to eq(1)
                    career_profile.update_associations_from_hash!({
                        "peer_recommendations" => []
                    })
                    career_profile.save!
                    expect(PeerRecommendation.where(career_profile_id: career_profile.id).count).to eq(0)
                end

                it "should handle duplicate emails" do
                    career_profile.update_associations_from_hash!({
                        "peer_recommendations" => [{
                            email: 'johnny.appleseed@orchard.com'
                        }, {
                            email: 'johnny.appleseed@orchard.com'
                        }]
                    })
                    career_profile.save!

                    expect(career_profile.peer_recommendations.count).to eq(1)
                    expect(career_profile.peer_recommendations[0].email).to eq('johnny.appleseed@orchard.com')
                end

                it "should handle swapping emails between recommendations" do
                    first_peer_recommendation = PeerRecommendation.create!(peer_recommendation_attrs.merge({career_profile_id: career_profile.id, email: 'john.doe@gmail.com'}))
                    second_peer_recommendation = PeerRecommendation.create!(peer_recommendation_attrs.merge({career_profile_id: career_profile.id, email: 'joe.schmoe@gmail.com'}))
                    career_profile.reload

                    # swap the emails of the peer recommendations in preparation to pass these hashes into
                    # the update_associations_from_hash! call
                    modified_first_peer_recommendation = first_peer_recommendation.attributes.with_indifferent_access.merge({ 'email' => second_peer_recommendation.email })
                    modified_second_peer_recommendation = second_peer_recommendation.attributes.with_indifferent_access.merge({ 'email' => first_peer_recommendation.email })

                    # sanity check
                    expect(modified_first_peer_recommendation['email']).to eq('joe.schmoe@gmail.com')
                    expect(modified_second_peer_recommendation['email']).to eq('john.doe@gmail.com')

                    career_profile.update_associations_from_hash!({
                        "peer_recommendations" => [modified_first_peer_recommendation, modified_second_peer_recommendation]
                    })
                    career_profile.save!

                    expect(career_profile.peer_recommendations.count).to eq(2)
                    expect(career_profile.peer_recommendations[0].id).to eq(first_peer_recommendation.id)
                    expect(career_profile.peer_recommendations[0].email).to eq(first_peer_recommendation.email)
                    expect(career_profile.peer_recommendations[1].id).to eq(second_peer_recommendation.id)
                    expect(career_profile.peer_recommendations[1].email).to eq(second_peer_recommendation.email)
                end
            end

            describe "work experiences" do

                it "should add a work experience" do
                    career_profile.update_associations_from_hash!({
                        "work_experiences" => [work_experience_attrs]
                    })

                    expect(career_profile.work_experiences.size).to eq(1)
                    career_profile.save!
                    expect(CareerProfile::WorkExperience.where(career_profile_id: career_profile.id).count).to eq(1)
                    expected_attrs = work_experience_attrs.except("start_date", "end_date")
                    expected_profile = career_profile.work_experiences.first
                    assert_attrs(expected_profile, expected_attrs)
                end

                it "should update a work experience" do
                    expect(career_profile.work_experiences.size).to eq(1) # sanity check
                    work_experience = career_profile.work_experiences.first

                    # grab the existing attributes from the existing experience
                    # and change a couple things so we can make sure the changes
                    # get persisted
                    updated_attrs = work_experience.attributes.merge({
                        "job_title" => "mega_ninja",
                        "responsibilities" => ["dispatching enemies"],
                        "professional_organization" => { text: "Ninja Inc.", locale: "en" },
                        "featured": true,
                        "employment_type": "part_time"
                    })

                    career_profile.update_associations_from_hash!({
                        "work_experiences" => [updated_attrs]
                    })

                    expect(career_profile.work_experiences.size).to eq(1)
                    expected_attrs = updated_attrs.except("updated_at", "professional_organization_option_id", "start_date", "end_date")
                    assert_attrs(career_profile.work_experiences.first, expected_attrs)

                    career_profile.save!

                    expect(CareerProfile::WorkExperience.where(career_profile_id: career_profile.id).count).to eq(1)
                    assert_attrs(career_profile.work_experiences.first.reload, expected_attrs)

                end

                it "should remove a work experience" do
                    expect(career_profile.work_experiences.size).to eq(1) # sanity check
                    expect(CareerProfile::WorkExperience.where(career_profile_id: career_profile.id).count).to eq(1)
                    career_profile.update_associations_from_hash!({
                        "work_experiences" => []
                    })
                    career_profile.save!
                    expect(CareerProfile::WorkExperience.where(career_profile_id: career_profile.id).count).to eq(0)
                end

                it "should order work experiences by end_date" do
                    career_profile.update_associations_from_hash!({
                        "work_experiences" => [
                            work_experience_attrs.merge({ :end_date => 5.years.ago.time.to_timestamp * 1000 }),
                            work_experience_attrs.merge({
                                :end_date => 1.years.ago.time.to_timestamp * 1000,
                                :professional_organization => {
                                    :text => "AssassinCorp",
                                    :locale => "en"
                                }
                            })
                        ]
                    })
                    career_profile.save!
                    career_profile.reload
                    expect(career_profile.work_experiences.count).to eq(2)
                    expect(career_profile.work_experiences[0].professional_organization.text).to eq("AssassinCorp")
                    expect(career_profile.work_experiences[1].professional_organization.text).to eq(work_experience_attrs[:professional_organization][:text])
                end

                it "should not error on multiple organizations with the same name" do
                    career_profile.update_associations_from_hash!({
                        "work_experiences" => [
                            work_experience_attrs.merge({ :end_date => 5.years.ago.time.to_timestamp * 1000 }),
                            work_experience_attrs.merge({
                                :end_date => 1.years.ago.time.to_timestamp * 1000,
                            })
                        ]
                    })
                    career_profile.save!
                    career_profile.reload

                    expect(career_profile.work_experiences.count).to eq(2)
                    expect(career_profile.work_experiences[0].professional_organization.text).to eq(work_experience_attrs[:professional_organization][:text])
                    expect(career_profile.work_experiences[1].professional_organization.text).to eq(work_experience_attrs[:professional_organization][:text])
                end

            end

        end

        it "should log to sentry if decreasing last_calculated_complete_percentage from 100" do
            career_profile = users(:user_with_career_profile).career_profile
            expected_extra = {
                "user_id" => career_profile.user_id,
                "orig_last_calculated_complete_percentage" => career_profile.last_calculated_complete_percentage,
                "new_last_calculated_complete_percentage" => 1
            }

            expect(Raven).to receive(:capture_exception) do |exception, opts|
                expect(exception.message).to eq("Decreasing last_calculated_complete_percentage on a career_profile")
                expect(opts).to eq({
                    :extra => expected_extra
                })
            end

            career_profile.merge_hash("last_calculated_complete_percentage" => 1)
        end

        it "should not log to sentry if decreasing last_calculated_complete_percentage when last_calculated_complete_percentage != 100" do
            career_profile = CareerProfile.first
            career_profile.update_attribute(:last_calculated_complete_percentage, 75)

            expect(Raven).not_to receive(:capture_exception)
            career_profile.merge_hash("last_calculated_complete_percentage" => 1)
        end

    end

    describe "as_json" do

        describe "with options" do
            attr_accessor :career_profile

            before(:each) do
                user = User.joins(:cohort_applications => :cohort).
                    where("cohorts.program_type" => 'mba',
                        "cohort_applications.status" => 'accepted',
                        "users.can_edit_career_profile" => true).first

                @career_profile = user.career_profile
            end

            it "should call as_json_for_student_network when view='student_network_profiles'" do
                expect(career_profile).to receive(:as_json_for_student_network)
                expect(career_profile).not_to receive(:as_json_for_career_network)
                json = career_profile.as_json({view: 'student_network_profiles'})
            end

            it "should call as_json_for_career_network when view='career_profiles'" do
                expect(career_profile).not_to receive(:as_json_for_student_network)
                expect(career_profile).to receive(:as_json_for_career_network)
                json = career_profile.as_json({view: 'career_profiles'})
            end

            it "should raise on unexpected view" do
                expect {
                    career_profile.as_json({view: 'one_seriously_unexpected_view'})
                }.to raise_error(ArgumentError)
            end

        end

    end

    describe "log_peer_recommendation_request_events" do

        attr_accessor :career_profile

        describe "without pending application" do

            it "should do nothing" do
                @career_profile = CareerProfile.first
                allow(career_profile.user).to receive(:pending_application).and_return(nil)
                assert_nothing
            end
        end

        describe "with pending application" do

            describe "for cohort that does not support peer recommendations" do

                it "should do nothing" do
                    @career_profile = users(:pending_career_network_only_cohort_user).career_profile
                    allow(career_profile.user.pending_application.cohort).to receive(:supports_peer_recommendations?).and_return(false)
                    assert_nothing
                end
            end

            describe "for cohort that supports peer recommendations" do

                attr_accessor :first_peer_recommendation, :second_peer_recommendation

                before(:each) do
                    @career_profile = users(:pending_mba_cohort_user).career_profile
                    allow(career_profile.user.pending_application.cohort).to receive(:supports_peer_recommendations?).and_return(true)

                    @first_peer_recommendation = PeerRecommendation.create!({'career_profile_id': career_profile.id, 'email': 'john.doe@gmail.com', 'contacted': true})
                    @second_peer_recommendation = PeerRecommendation.create!({'career_profile_id': career_profile.id, 'email': 'joe.schmoe@gmail.com', 'contacted': false})
                    career_profile.peer_recommendations = [first_peer_recommendation, second_peer_recommendation]
                end

                it "should wrap logic in transaction" do
                    expect(RetriableTransaction).to receive(:transaction)
                    career_profile.log_peer_recommendation_request_events
                end

                it "should set contacted=true for all associated non-contacted peer recommendations" do
                    expect_any_instance_of(PeerRecommendation).to receive(:update_attribute).once.with(:contacted, true).and_call_original
                    expect(PeerRecommendation.find_by_email('joe.schmoe@gmail.com').contacted).to be(false) # sanity check

                    career_profile.log_peer_recommendation_request_events
                    expect(PeerRecommendation.find_by_email('joe.schmoe@gmail.com').contacted).to be(true)
                end

                it "should log cohort:peer_recommendation_request events for each peer recommendation that gets updated" do
                    mock_event = double("Event instance")
                    # expect this only once because the career profile has been mocked in the before(:each)
                    # to only have one peer recommendation where 'contacted' is false
                    expect(Event).to receive(:create_server_event!).once.with(
                        anything,
                        career_profile.user_id,
                        'cohort:peer_recommendation_request',
                        {
                            applicant_name: career_profile.user.name,
                            peer_recommender_email: 'joe.schmoe@gmail.com',
                            peer_recommendation_form_url: "https://smart.ly/peer-recommendations/#{career_profile.user_id}?from=joe.schmoe@gmail.com",
                            program_type: career_profile.user.program_type
                        }
                    ).and_return(mock_event)
                    expect(mock_event).to receive(:log_to_external_systems).with(no_args)

                    career_profile.log_peer_recommendation_request_events
                end

                it "should log to Slack for each peer recommendation that gets updated" do
                    # expect this only once because the career profile has been mocked in the before(:each)
                    # to only have one peer recommendation where 'contacted' is false
                    expect(LogToSlack).to receive(:perform_later).once.with(
                        Slack.peer_review_notify_channel,
                        "*New Peer Recommendation Sent*",
                        [
                            {
                                pretext: "From: #{career_profile.user.email}, To: #{second_peer_recommendation.email}"
                            }
                        ]
                    )

                    career_profile.log_peer_recommendation_request_events
                end
            end
        end

        def assert_nothing
            expect(RetriableTransaction).not_to receive(:transaction)
            expect_any_instance_of(PeerRecommendation).not_to receive(:update_attribute)
            expect(Event).not_to receive(:create_server_event!)
            expect(LogToSlack).not_to receive(:perform_later!)
            career_profile.log_peer_recommendation_request_events
        end
    end

    describe "career_profile_active?" do
        CareerProfile.active_interest_levels.each do |active_interest_levels|
            it "should return true if interested_in_joining_new_company is #{active_interest_levels}" do
                career_profile = users(:user_with_career_profile).career_profile
                career_profile.interested_in_joining_new_company = active_interest_levels
                career_profile.save!

                expect(career_profile.career_profile_active?).to be(true)
            end
        end

        CareerProfile.inactive_interest_levels.push(nil).each do |inactive_status|
            it "should return false if interested_in_joining_new_company is #{inactive_status}" do
                career_profile = users(:user_with_career_profile).career_profile
                career_profile.interested_in_joining_new_company = inactive_status
                career_profile.save!

                expect(career_profile.career_profile_active?).to be(false)
            end
        end
    end

    describe "career_profile_editable_and_complete?" do
        it "should return true if user.can_edit_career_profile and last_calculated_complete_percentage is 100" do
            career_profile = users(:user_with_career_profile).career_profile
            expect(career_profile.user.can_edit_career_profile).to be(true)
            career_profile.last_calculated_complete_percentage = 100
            career_profile.save!
            expect(career_profile.career_profile_editable_and_complete?).to be(true)
        end

        it "should return false if !user.can_edit_career profile or last_calculated_complete_percentage != 100" do
            career_profile = users(:user_with_career_profile).career_profile
            expect(career_profile.user.can_edit_career_profile).to be(true)
            career_profile.last_calculated_complete_percentage = 77
            career_profile.save!
            expect(career_profile.career_profile_editable_and_complete?).to be(false)

            career_profile.user.can_edit_career_profile = false;
            career_profile.last_calculated_complete_percentage = 100
            career_profile.save!
            expect(career_profile.career_profile_editable_and_complete?).to be(false)
        end
    end

    describe "just_deactivated?" do
        attr_accessor :career_profile

        before(:each) do
            @career_profile = users(:user_with_career_profile).career_profile
        end

        describe "when !saved_change_to_interested_in_joining_new_company?" do

            it "should return false" do
                expect(career_profile).to receive(:saved_change_to_interested_in_joining_new_company?).and_return(false)
                expect(career_profile.just_deactivated?).to be(false)
            end
        end

        describe "when saved_change_to_interested_in_joining_new_company?" do

            before(:each) do
                expect(career_profile).to receive(:saved_change_to_interested_in_joining_new_company?).and_return(true)
            end

            describe "when interested_in_joining_new_company before last save was not an active interest level" do

                it "should return false" do
                    expect(career_profile).to receive(:attribute_before_last_save).with(:interested_in_joining_new_company).and_return(CareerProfile.inactive_interest_levels[0])
                    expect(career_profile.just_deactivated?).to be(false)
                end
            end

            describe "when interested_in_joining_new_company before last save was an active interest level" do

                before(:each) do
                    expect(career_profile).to receive(:attribute_before_last_save).with(:interested_in_joining_new_company).and_return(CareerProfile.active_interest_levels[0])
                end

                describe "when interested_in_joining_new_company is an active interest level" do

                    it "should return false" do
                        expect(career_profile).to receive(:interested_in_joining_new_company).and_return(CareerProfile.active_interest_levels[1])
                        expect(career_profile.just_deactivated?).to be(false)
                    end
                end

                describe "when interested_in_joining_new_company is an inactive interest level" do

                    it "should return true" do
                        expect(career_profile).to receive(:interested_in_joining_new_company).and_return(CareerProfile.inactive_interest_levels[0])
                        expect(career_profile.just_deactivated?).to be(true)
                    end
                end
            end
        end
    end

    describe "viewable_by_hiring_manager_through_deep_link?" do
        attr_accessor :career_profile

        before(:each) do
            @career_profile = users(:user_with_career_profile).career_profile
        end

        it "should return false if user passed is not a hiring manager" do
            expect(career_profile.viewable_by_hiring_manager_through_deep_link?(career_profile.user)).to be(false)
        end

        describe "when hiring manager is passed in" do
            attr_accessor :hiring_manager

            before(:each) do
                @hiring_manager = users(:hiring_manager)
            end

            it "should return false if the career profile's owner isn't available_for_relationships and no teammembers have a connection with candidate" do
                expect(User).to receive(:available_for_relationships).with(hiring_manager.professional_organization_option_id).and_return([])
                expect(HiringRelationship).to receive(:where).with(
                    candidate_id: career_profile.user_id,
                    hiring_manager_id: hiring_manager.hiring_team&.hiring_manager_ids || hiring_manager.id,
                    hiring_manager_status: HiringRelationship.viewable_hiring_manager_statuses
                ).and_return([])
                expect(career_profile.viewable_by_hiring_manager_through_deep_link?(hiring_manager)).to be(false)
            end

            it "should return true if the career profile's owner is available_for_relationships" do
                expect(User).to receive(:available_for_relationships).with(hiring_manager.professional_organization_option_id).and_return([career_profile.user])
                expect(career_profile.viewable_by_hiring_manager_through_deep_link?(hiring_manager)).to be(true)
            end

            it "should return true if profile's owner is not available_for_relationships but the hiring manager has a connection with the candidate" do
                expect(User).to receive(:available_for_relationships).with(hiring_manager.professional_organization_option_id).and_return([])
                expect(HiringRelationship).to receive(:where).with(
                    candidate_id: career_profile.user_id,
                    hiring_manager_id: hiring_manager.hiring_team&.hiring_manager_ids || hiring_manager.id,
                    hiring_manager_status: HiringRelationship.viewable_hiring_manager_statuses
                ).and_return([{}]) # just needs to return something so that it looks like there are hiring relationships that meet these conditions
                expect(career_profile.viewable_by_hiring_manager_through_deep_link?(hiring_manager)).to be(true)
            end
        end
    end

    describe "reject_candidate_position_interests_not_reviewed_by_hiring_manager" do
        attr_accessor :career_profile

        before(:each) do
            @career_profile = users(:user_with_career_profile).career_profile
        end

        it "should reject all of the user's candidate position interests that haven't been reviewed by a hiring maanger yet" do
            career_profile.candidate_position_interests.delete_all # clear all associated candidate position interests to create a clean slate for this spec

            # create new candidate position interests, one for each valid CandidatePositionInterest hiring manager status
            hiring_manager_statuses = CandidatePositionInterest.hiring_manager_statuses
            open_positions = OpenPosition.limit(hiring_manager_statuses.length)
            hiring_manager_statuses.each_with_index do |hiring_manager_status, i|
                CandidatePositionInterest.create!(candidate_id: career_profile.user_id, open_position_id: open_positions[i].id, candidate_status: 'accepted', hiring_manager_status: hiring_manager_status)
            end

            career_profile.reject_candidate_position_interests_not_reviewed_by_hiring_manager # invoke

            # assert the candidate_status on the candidate position interests that weren't
            # reviewed by the hiring manager was properly updated to 'rejected'
            career_profile.candidate_position_interests.each do |candidate_position_interest|
                if CandidatePositionInterest.reviewed_hiring_manager_statuses.include?(candidate_position_interest.hiring_manager_status)
                    # candidate position interests that were already reviewed by the hiring manager shouldn't have been updated,
                    # as such we expect the candidate_status to be the same ('accepted')
                    expect(candidate_position_interest.candidate_status).to eq('accepted')
                else
                    # otherwise, the candidate_status should have been updated to 'rejected'
                    expect(candidate_position_interest.candidate_status).to eq('rejected')
                end
            end
        end

        describe "after save" do

            describe "when !just_deactivated?" do

                it "should not be called" do
                    expect(career_profile).to receive(:just_deactivated?).and_return(false)
                    expect(career_profile).not_to receive(:reject_candidate_position_interests_not_reviewed_by_hiring_manager)
                    career_profile.save!
                end
            end

            describe "when just_deactivated?" do

                it "should be called" do
                    expect(career_profile).to receive(:just_deactivated?).and_return(true)
                    expect(career_profile).to receive(:reject_candidate_position_interests_not_reviewed_by_hiring_manager)
                    career_profile.save!
                end
            end
        end
    end

    describe "program_type" do

        it "should return program_type only if it's confirmed" do
            user = users(:user_with_career_profile)
            career_profile = user.career_profile
            user.program_type_confirmed = false

            expect(career_profile.as_json(view: 'editable')['program_type']).to be(nil)

            user.program_type_confirmed = true
            expect(career_profile.as_json(view: 'editable')['program_type']).to eq(user.program_type)
        end
    end

    describe "validate_appropriate_do_not_create_relationships_change" do
        it "should fail if do_not_create_relationships changed from false -> true and career profile is in a CareerProfileList" do
            career_profile = CareerProfile.where(id: CareerProfileList.pluck(:career_profile_ids).flatten, do_not_create_relationships: false).first
            career_profile.do_not_create_relationships = true
            expect(career_profile.valid?).to be(false)
            expect(career_profile.errors.full_messages).to include("Do not create relationships can only be true if candidate is not in a CareerProfileList")
        end
    end

    describe "only_one_featured_work_experience" do

        it "should not fail if no work experiences" do
            career_profile = CareerProfile.first
            career_profile.work_experiences = []
            expect(career_profile.valid?).to be(true)
            expect(career_profile.errors.full_messages).not_to include("Career profile can only have one featured work experience")
        end

        it "should fail if multiple work experiences are saved with 'featured' set to true" do
            career_profile = CareerProfile.first
            career_profile.work_experiences = [
                CareerProfile::WorkExperience.new({
                    professional_organization: ProfessionalOrganizationOption.first,
                    start_date: Time.now,
                    end_date: Time.now,
                    job_title: 'Foo Handler',
                    featured: true,
                    responsibilities: ['Handling the foo', 'Washing the foo']
                }), CareerProfile::WorkExperience.new({
                    professional_organization: ProfessionalOrganizationOption.second,
                    start_date: Time.now,
                    end_date: Time.now,
                    job_title: 'Bar Engineer',
                    featured: true,
                    responsibilities: ['Designing bar', 'Creating bar']
                })
            ]
            expect(career_profile.valid?).to be(false)
            expect(career_profile.errors.full_messages).to include("Career profile can only have one featured work experience")
        end

    end

    describe "validate_score_on_sat" do

        it "should pass if score_on_sat is nil" do
            career_profile = CareerProfile.first
            career_profile.score_on_sat = nil
            expect(career_profile.valid?).to be(true)
            expect(career_profile.errors.full_messages).not_to include("Score on sat is invalid")
        end

        it "should pass if score_on_sat contains only numerics, is less than the lowest possible SAT score (400), and greater than sat_max_score" do
            career_profile = CareerProfile.first
            career_profile.score_on_sat = '1500'
            career_profile.sat_max_score = '1600'
            expect(career_profile.valid?).to be(true)
            expect(career_profile.errors.full_messages).not_to include("Score on sat is invalid")
        end

        it "should fail if score_on_sat contains at least one non-numeric character" do
            career_profile = CareerProfile.first
            career_profile.score_on_sat = 'N/A'
            career_profile.sat_max_score = '1600'
            expect(career_profile.valid?).to be(false)
            expect(career_profile.errors.full_messages).to include("Score on sat is invalid")
        end

        it "should fail if score_on_sat is less than the lowest possible SAT score (400)" do
            career_profile = CareerProfile.first
            career_profile.score_on_sat = '350'
            expect(career_profile.valid?).to be(false)
            expect(career_profile.errors.full_messages).to include("Score on sat is invalid")
        end

        it "should fail if score_on_sat is greater than sat_max_score" do
            career_profile = CareerProfile.first
            career_profile.score_on_sat = '1800'
            career_profile.sat_max_score = '1600'
            expect(career_profile.valid?).to be(false)
            expect(career_profile.errors.full_messages).to include("Score on sat is invalid")
        end

    end

    describe "city_name" do
        it "should return the name of the city when specified in locality" do
            career_profile = CareerProfile.first
            career_profile.update_attribute(:place_details, {
                locality: {
                    long: 'Footown'
                }
            })
            expect(career_profile.reload.city_name).to eq('Footown')
        end

        it "should return the name of the city when specified in administrative_area_level_3" do
            career_profile = CareerProfile.first
            career_profile.update_attribute(:place_details, {
                administrative_area_level_3: {
                    long: 'Footown'
                }
            })
            expect(career_profile.reload.city_name).to eq('Footown')
        end

        it "should handle nil place_details" do
            career_profile = CareerProfile.first
            career_profile.update_attribute(:place_details, {})
            expect(career_profile.reload.city_name).to be(nil)
        end

        # Note: This shouldn't typically happen with a place_details returned from Google Places API, but safeguarding nonetheless
        it "should handle nil city fields" do
            career_profile = CareerProfile.first
            career_profile.update_attribute(:place_details, {
                test: 'foo'
            })
            expect(career_profile.reload.city_name).to be(nil)
        end
    end

    describe "consider_early_decision" do

        before(:each) do
            expect_any_instance_of(CareerProfile).to receive(:merge_hash).and_call_original
            attrs = valid_attrs
            @instance = CareerProfile.create_from_hash!(attrs)
        end

        it "should default to nil" do
            expect(@instance.consider_early_decision).to be(nil)
        end

        it "can be set to true/false" do
            @instance.consider_early_decision = true;
            @instance.save!
            @instance.reload
            expect(@instance.consider_early_decision).to eq(true)

            @instance.consider_early_decision = false;
            @instance.save!
            @instance.reload
            expect(@instance.consider_early_decision).to eq(false)
        end

    end

    describe "should_update_fulltext?" do
        it "should be based on the saved attributes" do
            career_profile = CareerProfile.first
            target_attributes = [
                :city_name,
                :personal_fact,
                :skills,
                :awards_and_interests,
                :top_personal_descriptors,
                :top_workplace_strengths,
                :job_sectors_of_interest,
                :student_network_interests,
                :student_network_looking_for
            ]

            CareerProfile.new.attributes.keys.map(&:to_sym).each do |attribute|
                is_target_attr = target_attributes.include?(attribute)
                allow(career_profile).to receive(:saved_changes) { Hash[attribute, true] }
                expect(career_profile.should_update_fulltext?).to be(is_target_attr)
            end
        end

        describe "after_save" do
            it "should call update_fulltext if should_update_fulltext?" do
                career_profile = CareerProfile.first
                allow(career_profile).to receive(:should_update_fulltext?) { true }
                expect(career_profile).to receive(:update_fulltext)
                career_profile.update(personal_fact: 'changed')
            end

            it "should not call update_fulltext if not should_update_fulltext?" do
                career_profile = CareerProfile.first
                allow(career_profile).to receive(:should_update_fulltext?) { false }
                expect(career_profile).not_to receive(:update_fulltext)
                career_profile.update(personal_fact: 'changed')
            end
        end
    end

    describe "get_geo_cluster_json" do

        it "should work" do

            ny1 = CareerProfile.where(location: "0101000020E6100000D04C9C81367252C075649B0AA7804440").first
            ny2 = CareerProfile.where(location: "0101000020E61000004098DBBDDC8052C04F35C4C25A8B4440").first
            canada = CareerProfile.where(location: "0101000020E61000005CC5E23785DD53C098AC8A7093074640").first

            json = CareerProfile.where(id: [ny1.id, ny2.id, canada.id]).get_geo_cluster_json(3, 12)
            result = ActiveSupport::JSON.decode(json)

            features = result['features'].sort_by { |f| f['geometry']['coordinates'][0] }.reverse
            expect(features.size).to eq(2)

            # New York
            expect(features[0]['geometry']).to eq({"type"=>"Point", "coordinates"=>[-73.8990249, 41.0469025]})
            expect(features[0]['properties']['count']).to eq(2)

            # Canada
            expect(features[1]['geometry']).to eq({"type"=>"Point", "coordinates"=>[-79.461256, 44.059187]})
            expect(features[1]['properties']['count']).to eq(1)
        end

    end

    describe "indicates_user_should_upload_transcripts?" do

        it "should return true if there are any education experiences requiring a transcript" do
            career_profile = CareerProfile.first
            expect(career_profile).to receive(:education_experiences_indicating_transcript_required).and_return([CareerProfile::EducationExperience.first])
            expect(career_profile.indicates_user_should_upload_transcripts?).to be(true)
        end

        it "should be false if there are no education experiences requiring a transcript" do
            career_profile = CareerProfile.first
            expect(career_profile).to receive(:education_experiences_indicating_transcript_required).and_return([])
            expect(career_profile.indicates_user_should_upload_transcripts?).to be(false)
        end
    end

    describe "indicates_user_should_upload_english_language_proficiency_documents?" do

        attr_reader :career_profile

        before(:each) do
            @career_profile = CareerProfile.first
        end

        it "should be false if native_english_speaker" do
            career_profile.update_column(:native_english_speaker, true)
            expect(career_profile.indicates_user_should_upload_english_language_proficiency_documents?).to be(false)
        end

        it "should be false if native_english_speaker is unknown" do
            career_profile.update_column(:native_english_speaker, nil)
            expect(career_profile.indicates_user_should_upload_english_language_proficiency_documents?).to be(false)
        end

        describe "when native_english_speaker is false" do

            before(:each) do
                career_profile.update_column(:native_english_speaker, false)
            end

            it "should be false when earned_accredited_degree_in_english is true" do
                career_profile.update_column(:earned_accredited_degree_in_english, true)
                expect(career_profile.indicates_user_should_upload_english_language_proficiency_documents?).to be(false)
            end

            it "should be false when earned_accredited_degree_in_english is unknown" do
                career_profile.update_column(:earned_accredited_degree_in_english, true)
                expect(career_profile.indicates_user_should_upload_english_language_proficiency_documents?).to be(false)
            end

            describe "when earned_accredited_degree_in_english is false" do

                before(:each) do
                    career_profile.update_column(:earned_accredited_degree_in_english, false)
                end

                it "should be false when sufficient_english_work_experience is true" do
                    career_profile.update_column(:sufficient_english_work_experience, true)
                    expect(career_profile.indicates_user_should_upload_english_language_proficiency_documents?).to be(false)
                end

                it "should be true when sufficient_english_work_experienceis false" do
                    career_profile.update_column(:sufficient_english_work_experience, false)
                    expect(career_profile.indicates_user_should_upload_english_language_proficiency_documents?).to be(true)
                end

                it "should be true when sufficient_english_work_experience is unknown" do
                    career_profile.update_column(:sufficient_english_work_experience, nil)
                    expect(career_profile.indicates_user_should_upload_english_language_proficiency_documents?).to be(true)
                end

            end

        end

    end

    describe "real_locations_of_interest" do
        it "should not return none nor flexible" do
            career_profile = CareerProfile.first
            career_profile.locations_of_interest = ['new_york', 'san_francisco', 'none', 'flexible']
            expect(career_profile.real_locations_of_interest).to match_array(['new_york', 'san_francisco'])
        end
    end

    describe "real_primary_areas_of_interest" do
        it "should not return other" do
            career_profile = CareerProfile.first
            career_profile.primary_areas_of_interest = ['legal', 'finance', 'other']
            expect(career_profile.real_primary_areas_of_interest).to match_array(['legal', 'finance'])
        end
    end

    describe "missing_transcripts?" do
        attr_accessor :career_profile, :education_experiences

        before(:each) do
            @career_profile = users(:user_with_career_profile).career_profile
            @education_experiences = CareerProfile::EducationExperience
                .where(transcript_waiver: nil)
                .order(:id)
                .limit(3)
            @education_experiences.each { |e| e.transcripts.destroy_all }
            expect(education_experiences.size).to be 3 # sanity check
            expect(career_profile).to receive(:education_experiences_indicating_transcript_required).and_return(education_experiences.to_a)
        end

        it "should be true if some education experiences still need a transcript or waiver" do
            education_experiences.first.transcripts = [S3TranscriptAsset.first]
            education_experiences.second.transcript_waiver = 'test'
            # but nothing for the third one
            expect(career_profile.missing_transcripts?).to be(true)
        end

        it "should be false if all education experiences have a transcript or waiver" do
            education_experiences.first.transcripts = [S3TranscriptAsset.first]
            education_experiences.second.transcript_waiver = 'test'
            education_experiences.third.transcript_waiver = 'test again'
            expect(career_profile.missing_transcripts?).to be(false)
        end
    end

    describe "education_experiences_indicating_transcript_required" do
        it "should work" do
            career_profile = users(:user_with_career_profile).career_profile
            education_experiences = CareerProfile::EducationExperience.order(:id).limit(4)
            education_experiences.first.update_columns(will_not_complete: true, graduation_year: Date.current.year + 1)
            education_experiences.second.update_columns(will_not_complete: false, graduation_year: Date.current.year - 1)
            education_experiences.third.update_columns(will_not_complete: false, graduation_year: Date.current.year + 1)
            education_experiences.fourth.update_columns(will_not_complete: false, graduation_year: Date.current.year)

            career_profile.education_experiences = education_experiences.to_a
            career_profile.save!

            expect(career_profile.education_experiences_indicating_transcript_required.pluck(:id)).to match_array([education_experiences.second.id, education_experiences.fourth.id])
            expect(career_profile.education_experiences_indicating_transcript_required.pluck(:will_not_complete)).to match_array([false, false])
        end
    end

    def assert_attrs(obj, attrs)
        attrs = attrs.stringify_keys

        # find any keys that refer to associations and handle them specially
        attrs.each do |key, val|

            # if this key refers to an association instead of a vanilla attribute
            if obj.class.reflections.keys.include?(key)

                # turn it into an array even if it isn't so we can use the
                # same code for has_one and has_many relationships
                actual_list = Array.wrap(obj.send(key.to_sym))
                expected_list = Array.wrap(val)

                expect(actual_list.size).to eq(expected_list.size), "unexpected size for #{key.inspect}. #{actual_list.size} != #{expected_list.size}"

                actual_list.each_with_index do |actual_item, i|
                    assert_attrs(actual_item, expected_list[i])
                end

                # ensure we remove this key so as not to trip up the next expectation
                attrs.delete(key)
            end
        end

        actual_attrs = obj.attributes.slice(*attrs.keys.map(&:to_s)).stringify_keys
        expect(actual_attrs).to eq(attrs.stringify_keys)
    end

    def user_without_profile
        User.left_outer_joins(:career_profile).where("career_profiles.id is null").first
    end

    def valid_attrs(attrs = {})
        basic_info_attrs
            .merge(test_score_attrs)
            .merge({
                user_id: user_without_profile.id
            })
            .merge(attrs)
    end

    def work_experience_attrs(attrs = {})
        {
            :professional_organization => {
                :text => "Assassin's Inc.",
                :locale => "en"
            },
            :job_title => "ninja",
            :start_date => Time.now.to_timestamp * 1000,
            :end_date => Time.now.to_timestamp * 1000,
            :featured => false,
            :responsibilities => ["a", "b"]
        }.with_indifferent_access.merge(attrs)
    end

    def education_experience_attrs(attrs = {})
        {
            :educational_organization => {
                :text => "School of Assassins",
                :locale => "en"
            },
            :graduation_year => 2000,
            :degree => "some value",
            :major => "some value",
            :minor => "some value",
            :gpa => "some value",
            :degree_program => true,
            :will_not_complete => false
        }.with_indifferent_access.merge(attrs)
    end

    def peer_recommendation_attrs(attrs = {})
        {
            :email => 'johnny.appleseed@gmail.com'
        }.with_indifferent_access.merge(attrs)
    end

    def job_preferences_attrs(attrs = {})
        {
            :willing_to_relocate                => true,
            :open_to_remote_work                => true,
            :authorized_to_work_in_us           => "some value",
            :employment_types_of_interest       => ["some type", "some other type"],
            :company_sizes_of_interest          => ["some size", "some other size"],
            :interested_in_joining_new_company  => "neutral",
            :locations_of_interest              => ["flexible"],
            :salary_range                       => 'prefer_not_to_disclose'
        }.with_indifferent_access.merge(attrs)
    end

    def education_attrs(attrs = {})
        {
            :survey_highest_level_completed_education_description => 'some value',
            :native_english_speaker => false,
            :earned_accredited_degree_in_english => true,
            :has_no_formal_education => true
        }.with_indifferent_access.merge(attrs)
    end

    def work_attrs(attrs = {})
        {
            :survey_years_full_time_experience => 'some value',
            :survey_most_recent_role_description => 'some value'
        }.with_indifferent_access.merge(attrs)
    end

    def test_score_attrs(attrs = {})
        {
            :score_on_gmat  => '42',
            :score_on_sat   => '1500',
            :sat_max_score  => '1600',
            :score_on_act   => '42'
        }.with_indifferent_access.merge(attrs)
    end

    def basic_info_attrs(attrs = {})
        {
            :place_id               => 'an_id',
            :place_details          => {'key' => 'value'}
        }.with_indifferent_access.merge(attrs)
    end

    def application_questions_attrs(attrs = {})
        {
            :short_answer_greatest_achievement      => 'a',
            :short_answer_leadership_challenge      => 'b',
            :primary_reason_for_applying            => 'c',
            :short_answer_scholarship               => 'd',
            :short_answer_why_pursuing              => 'e',
            :short_answer_use_skills_to_advance     => 'f',
            :short_answer_ask_professional_advice   => 'g'
        }.with_indifferent_access.merge(attrs)
    end

    def more_about_you_attrs(attrs = {})
        {
            :top_mba_subjects           => ["a", "b"],
            :top_personal_descriptors   => ["a", "b"],
            :top_motivations            => ["a", "b"],
            :top_workplace_strengths    => ["a", "b"],
            :skills                     => [{ text: "a", locale: "en"}, { text: "b", locale: "en" }],
            :awards_and_interests       => [{ text: "a", locale: "en"}, { text: "b", locale: "en" }]
        }.with_indifferent_access.merge(attrs)
    end

    def skills_attrs(attrs = {})
        {
            :skills => [{ text: "a", locale: "en"}, { text: "b", locale: "en" }]
        }.with_indifferent_access.merge(attrs)
    end

    def student_network_interests_attrs(attrs = {})
        {
            :student_network_interests => [{ text: "a", locale: "en"}, { text: "b", locale: "en" }]
        }.with_indifferent_access.merge(attrs)
    end

    def personal_bio_attrs(attrs = {})
        {
            :bio            => "some value",
            :personal_fact  => "some value"
        }.with_indifferent_access.merge(attrs)
    end

    def employer_preferences_attrs(attrs = {})
        {
            :primary_areas_of_interest      => ["some area", "some other area"],
            :job_sectors_of_interest        => ["some sector", "some other sector"],
            :preferred_company_culture_descriptors  => ["a", "b"]
        }.with_indifferent_access.merge(attrs)
    end

    def resume_and_links_attrs(attrs = {})
        {
            :li_profile_url         => "http://some_value",
            :resume_id              => @resume_id,
            :personal_website_url   => "http://some_value",
            :blog_url               => "http://some_value",
            :tw_profile_url         => "http://some_value",
            :fb_profile_url         => "http://some_value",
            :github_profile_url     => "http://some_value"
        }.with_indifferent_access.merge(attrs)
    end

    def profile_feedback_attrs(attrs = {})
        {
            :profile_feedback   => "some value"
        }.with_indifferent_access.merge(attrs)
    end
end
