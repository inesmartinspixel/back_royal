require 'spec_helper'

describe ActivityTimeline do

    fixtures(:users)

    describe "events" do

        it "should delegate to get_events and sort the results" do

            instance = ActivityTimeline.new
            instance.preloaded = true
            allow_any_instance_of(ActivityTimeline::DerivedTimelineEvent).to receive(:valid?).and_return(true)
            expect(instance).to receive(:get_events).and_return([
                ActivityTimeline::DerivedTimelineEvent.new(
                    time: Time.at(0)
                ),
                ActivityTimeline::DerivedTimelineEvent.new(
                    time: Time.at(1)
                )
            ])
            expect(instance.events.map(&:time)).to eq([Time.at(1), Time.at(0)])

        end

        it "should notify sentry and not blow up if a derived timeline event is invalid" do
            instance = ActivityTimeline.new
            instance.preloaded = true
            expect(Raven).to receive(:capture_exception) do |message, opts|
                expect(message).to eq("Timeline event is invalid")
                expect(opts[:extra][:errors]).to include("Time can't be blank")
                expect(opts[:extra][:event]['category']).to eq('invalid')
            end
            expect(instance).to receive(:get_events).and_return([
                ActivityTimeline::DerivedTimelineEvent.new(
                    # invalid because no time
                    time: nil,
                    category: 'invalid'
                ),
                ActivityTimeline::DerivedTimelineEvent.new(
                    time: Time.at(1),
                    category: 'category',
                    event: 'event'
                )
            ])
            expect(instance.events.map(&:time)).to eq([Time.at(1)])
        end

    end


    describe "get_change_events" do

        it "should return an array of DerivedTimelineEvents containing changes between versions" do
            instance = ActivityTimeline.new

            now = Time.now
            allow(Time).to receive(:now).and_return(now)

            admin = users(:admin)

            expect_any_instance_of(OpenStruct).to receive(:attr_changed_to_column_default_on_insert?).and_return(false)

            versions = [
                OpenStruct.new(
                    operation: 'U',
                    attr: 'foobar',
                    updated_at: Time.now,
                    version_editor_id: admin.id,
                    version_editor_name: admin.name,
                    label: 'some_label'
                )
            ]

            events = instance.send(:get_change_events, category: 'category',
                versions: versions,
                attrs: [{attr: :attr}],
                labels: Proc.new { |version|
                    [version.label]
                }
            )
            expect(events.count).to eq(1)
            expect(events[0].category).to eq('category')
            expect(events[0].labels).to eq(['some_label'])
            expect(events[0].event).to eq('modification')
            expect(events[0].editor_id).to eq(admin.id)
            expect(events[0].editor_name).to eq(admin.name)
            expect(events[0].changes).to eq([{
                attr: :attr,
                from: nil,
                to: 'foobar'
            }])
        end

    end

end