require 'spec_helper'

describe Cohort::CurriculumWithProjects do
    attr_reader :schedulable_item

    class SchedulableItem < ActiveRecord::Base
        attr_accessor :periods, :learner_project_ids
        include Cohort::CurriculumWithProjects

        def learner_project_ids
            unless defined? @learner_project_ids
                @learner_project_ids = []
            end
            @learner_project_ids
        end

        def learner_project_ids=(val)
            @learner_project_ids = val
        end
    end

    before(:each) do
        @schedulable_item = SchedulableItem.new(program_type: 'mba')
    end

    describe "validate_project_periods" do

        it "should validate that project periods contain project_style values" do
            schedulable_item.periods = [{
                "style" => 'project',
                "project_style" => 'some_style',
                "project_documents" => []
            },{
                "style" => 'standard',
                "project_documents" => []
            }]
            schedulable_item.validate_project_periods
            expect(schedulable_item.errors.full_messages).to be_empty
            schedulable_item.validate_project_periods
            expect(schedulable_item.errors.full_messages).to be_empty
            schedulable_item.periods[0]["project_style"] = nil
            schedulable_item.validate_project_periods
            expect(schedulable_item.errors.full_messages).to include("Period 1 must have a project style"), schedulable_item.errors.full_messages.inspect
        end
    end

    describe "validate_no_duplicate_requirement_identifiers" do

        it "should validate that there are no duplicate requirement identifiers" do
            projects = [
                LearnerProject.new(requirement_identifier: 'a'),
                LearnerProject.new(requirement_identifier: 'a'),
                LearnerProject.new(requirement_identifier: 'b')
            ]
            item_projects = []
            allow(schedulable_item).to receive(:learner_projects) do
                item_projects
            end
            schedulable_item.validate_no_duplicate_requirement_identifiers
            expect(schedulable_item.errors.full_messages).to be_empty

            item_projects = [projects[0], projects[2]]
            schedulable_item.validate_no_duplicate_requirement_identifiers
            expect(schedulable_item.errors.full_messages).to be_empty

            item_projects = [projects[0], projects[1], projects[2]]
            schedulable_item.validate_no_duplicate_requirement_identifiers
            expect(schedulable_item.errors.full_messages).to include('Learner projects include two different projects for "a"'), schedulable_item.errors.full_messages.inspect
        end

    end

end