require 'spec_helper'

describe TimezoneMixin do
    attr_accessor :record

    before(:each) do
        @record = CareerProfile.first
    end

    describe "enqueue_update_user_timezone_job" do

        it "should not be called on save if !saved_change_to_place_id?" do
            expect(record).to receive(:saved_change_to_place_id?).and_return(false)
            expect(record).not_to receive(:enqueue_update_user_timezone_job)
            record.save
        end

        it "should be called on save if saved_change_to_place_id?" do
            expect(record).to receive(:saved_change_to_place_id?).and_return(true)
            expect(record).to receive(:enqueue_update_user_timezone_job)
            record.save
        end

        it "should not enqueue UpdateUserTimezoneJob delayed job if no place_id" do
            expect(record).to receive(:place_id).and_return(nil)
            expect(UpdateUserTimezoneJob).not_to receive(:perform_later)
            record.enqueue_update_user_timezone_job
        end

        it "should enqueue UpdateUserTimezoneJob delayed job if place_id" do
            expect(record).to receive(:place_id).and_return('some_id')
            expect(UpdateUserTimezoneJob).to receive(:perform_later).with(record.class.name, record.id)
            record.enqueue_update_user_timezone_job
        end
    end

    describe "lookup_timezone" do

        it "should not lookup timezone if no place_id is present" do
            expect(record).to receive(:place_id).and_return(nil)
            expect(Timezone).not_to receive(:lookup)
            record.lookup_timezone
        end

        it "should lookup timezone using lat and lng values in place_details hash if place_id is present" do
            expect(record).to receive(:place_id).and_return('some_id')
            expect(record).to receive(:place_details).at_least(:once).and_return({'lat' => 'lat_value', 'lng' => 'lng_value'})
            expect(Timezone).to receive(:lookup).with('lat_value', 'lng_value').and_return(Timezone::Zone.new('America/Chicago'))
            record.lookup_timezone
        end

        it "should return the name of the timezone" do
            timezone_name = 'America/Chicago'
            expect(record).to receive(:place_id).and_return('some_id')
            expect(record).to receive(:place_details).at_least(:once).and_return({'lat' => 'lat_value', 'lng' => 'lng_value'})
            expect(Timezone).to receive(:lookup).with('lat_value', 'lng_value').and_return(Timezone::Zone.new(timezone_name))
            expect(record.lookup_timezone).to eq(timezone_name)
        end
    end
end