require 'spec_helper'

describe CohortApplication::DecisionConcern do

    fixtures(:cohorts, :users)

    before(:each) do
        @cohort_application = CohortApplication.where(admissions_decision: nil, cohort_id: cohorts(:published_mba_cohort).id).first

        # Ensure that the user only has this one application so we don't run into fk violations
        @cohort_application.user.cohort_applications.where.not(id: @cohort_application.id).destroy_all

    end

    describe "schedule_perform_admissions_decision" do

        it "should be called after_update" do
            expect(@cohort_application).to receive(:schedule_perform_admissions_decision)
            @cohort_application.update_attribute(:admissions_decision, "foo")
        end

        describe "no admissions_decision" do

            before(:each) do
                @cohort_application.update(cohort_id: Cohort.promoted_cohort("mba")["id"])
            end

            it "should not schedule anything if a change has not been made to the admissions_decision" do
                expect(PerformAdmissionsDecisionJob).not_to receive(:set)
                expect(PerformAdmissionsDecisionJob).not_to receive(:perform_later)
                @cohort_application.save! # should not trigger the scheduling since admissions_decision was not changed
            end

            it "should not schedule anything if the change is manual_admin_decision" do
                @cohort_application.update(admissions_decision: nil)
                expect(PerformAdmissionsDecisionJob).not_to receive(:set)
                expect(PerformAdmissionsDecisionJob).not_to receive(:perform_later)
                @cohort_application.update(admissions_decision: "manual_admin_decision")
            end

            it "should not schedule anything if the change is blank" do
                @cohort_application.update_columns(admissions_decision: "TBC - Manager") # don't trigger callbacks
                expect(PerformAdmissionsDecisionJob).not_to receive(:set)
                expect(PerformAdmissionsDecisionJob).not_to receive(:perform_later)
                @cohort_application.update(admissions_decision: nil)
            end

            it "should not schedule anything if the change is TBC" do
                @cohort_application.update_columns(admissions_decision: nil) # don't trigger callbacks
                expect(PerformAdmissionsDecisionJob).not_to receive(:set)
                expect(PerformAdmissionsDecisionJob).not_to receive(:perform_later)
                @cohort_application.update(admissions_decision: "TBC - Manager")
            end
        end

        describe "admissions_decision" do
            # We chose to undo this feature for now; it might come back later
            #
            # it "should schedule for eight days in the future if program_type is mba" do
            #     expect(PerformAdmissionsDecisionJob).to receive(:set).with(wait_until: @cohort_application.applied_at + 8.days).and_call_original
            #     expect_any_instance_of(ActiveJob::ConfiguredJob).to receive(:perform_later)

            #     @cohort_application.update_columns({
            #         cohort_id: Cohort.promoted_cohort("mba")["id"],
            #         admissions_decision: "foo"
            #     })

            #     allow(@cohort_application).to receive(:saved_change_to_admissions_decision?).and_return(true)
            #     @cohort_application.schedule_perform_admissions_decision
            # end

            # it "should schedule for eight days in the future if program_type is emba" do
            #     expect(PerformAdmissionsDecisionJob).to receive(:set).with(wait_until: @cohort_application.applied_at + 8.days).and_call_original
            #     expect_any_instance_of(ActiveJob::ConfiguredJob).to receive(:perform_later)

            #     @cohort_application.update_columns({
            #         cohort_id: Cohort.promoted_cohort("emba")["id"],
            #         admissions_decision: "foo"
            #     })

            #     allow(@cohort_application).to receive(:saved_change_to_admissions_decision?).and_return(true)
            #     @cohort_application.reload.schedule_perform_admissions_decision
            # end

            it "should schedule immediately if the program type is mba" do
                expect(PerformAdmissionsDecisionJob).not_to receive(:set)
                expect(PerformAdmissionsDecisionJob).to receive(:perform_later)

                @cohort_application.cohort_id = Cohort.promoted_cohort("mba")["id"]
                @cohort_application.admissions_decision = "foo"
                @cohort_application.save!
            end

            it "should schedule immediately if the program type is mba" do
                expect(PerformAdmissionsDecisionJob).not_to receive(:set)
                expect(PerformAdmissionsDecisionJob).to receive(:perform_later)

                @cohort_application.cohort_id = Cohort.promoted_cohort("emba")["id"]
                @cohort_application.admissions_decision = "foo"
                @cohort_application.save!
            end

            it "should schedule immediately if the program type is not mba or emba" do
                expect(PerformAdmissionsDecisionJob).not_to receive(:set)
                expect(PerformAdmissionsDecisionJob).to receive(:perform_later)

                @cohort_application.cohort_id = Cohort.promoted_cohort("career_network_only")["id"]
                @cohort_application.admissions_decision = "foo"
                @cohort_application.save!
            end
        end
    end

    describe "perform_admissions_decision" do

        it "should set retargeted_from_program_type if the program_type changed" do
            @cohort_application.update(admissions_decision: "Accept to Business Certificate", cohort_id: Cohort.promoted_cohort("mba")["id"])
            expect(@cohort_application).to receive(:save!).and_call_original
            @cohort_application.perform_admissions_decision

            @cohort_application.reload
            expect(@cohort_application.program_type).to eq("the_business_certificate")
            expect(@cohort_application.retargeted_from_program_type).to eq("mba")
        end

        it "should not set retargeted_from_program_type if the program_type did not change" do
            @cohort_application.update(admissions_decision: "Accept to Business Certificate", cohort_id: Cohort.promoted_cohort("the_business_certificate")["id"])
            expect(@cohort_application).to receive(:save!).and_call_original
            @cohort_application.perform_admissions_decision

            @cohort_application.reload
            expect(@cohort_application.program_type).to eq("the_business_certificate")
            expect(@cohort_application.retargeted_from_program_type).to be_nil
        end

        it "should correctly process 'Waitlist'" do
            expect(@cohort_application).to receive(:event_attributes).and_return({foo: "bar"})
            expect(Event).to receive(:create_server_event!) do |id, user_id, event_type, payload|
                expect(user_id).to eq(@cohort_application.user_id)
                expect(event_type).to eq('cohort:waitlisted')
                expect(payload).to eq({foo: "bar"})
                expects_log_to_external_systems
            end
            @cohort_application.update(admissions_decision: 'Waitlist')
            @cohort_application.perform_admissions_decision
        end

        describe "rejection decisions" do
            it "should correctly process 'Reject'" do
                @cohort_application.update(admissions_decision: "Reject", cohort_id: Cohort.promoted_cohort("mba")["id"])

                expect(@cohort_application.status).not_to eq('rejected')
                @cohort_application.perform_admissions_decision
                expect(@cohort_application.reload.status).to eq('rejected')
            end

            it "should correctly process 'Reject for Accreditation'" do
                @cohort_application.update(admissions_decision: "Reject for Accreditation", cohort_id: Cohort.promoted_cohort("mba")["id"])

                expect(@cohort_application.status).not_to eq('rejected')
                @cohort_application.perform_admissions_decision
                expect(@cohort_application.reload.status).to eq('rejected')
            end

            it "should correctly process 'Reject for No Interview'" do
                @cohort_application.update(admissions_decision: "Reject for No Interview", cohort_id: Cohort.promoted_cohort("mba")["id"])

                expect(@cohort_application.status).not_to eq('rejected')
                @cohort_application.perform_admissions_decision
                expect(@cohort_application.reload.status).to eq('rejected')
            end

            it "should account for rejected_or_expelled handling" do
                @cohort_application.update(admissions_decision: "Reject", cohort_id: Cohort.promoted_cohort("mba")["id"])

                expect(@cohort_application.status).not_to eq('rejected')

                expect(@cohort_application).to receive(:rejected_or_expelled).and_return('expelled')
                @cohort_application.perform_admissions_decision
                expect(@cohort_application.reload.status).to eq('expelled')
            end

            describe "non-mba non-pending" do
                it "should not set status to rejected if non-mba and non-pending" do
                    @cohort_application.update(admissions_decision: "Reject", cohort_id: Cohort.promoted_cohort("the_business_certificate")["id"], status: "accepted")
                    @cohort_application.perform_admissions_decision
                    expect(@cohort_application.reload.status).to eq("accepted")
                end

                it "should set status to rejected if non-mba but pending" do
                    @cohort_application.update(admissions_decision: "Reject", cohort_id: Cohort.promoted_cohort("the_business_certificate")["id"], status: "pending")
                    @cohort_application.perform_admissions_decision
                    expect(@cohort_application.reload.status).to eq("rejected")
                end
            end
        end

        it "should correctly process 'Reject and Convert to EMBA'" do
            @cohort_application.update(admissions_decision: "Reject and Convert to EMBA", cohort_id: Cohort.promoted_cohort("the_business_certificate")["id"])
            expect(@cohort_application).to receive(:reject_and_convert_to_emba)
            @cohort_application.perform_admissions_decision
        end

        describe "Pre-Accept to MBA" do
            it "should correctly process 'Pre-Accept to MBA'" do
                @cohort_application.update(admissions_decision: "Pre-Accept to MBA", cohort_id: Cohort.promoted_cohort("mba")["id"])
                @cohort_application.perform_admissions_decision
                expect(@cohort_application.status).to eq("pre_accepted")
            end

            it "should log to Sentry if trying to set for non-MBA application" do
                expect(Raven).to receive(:capture_exception).with("Can't set decision for non-MBA application to 'Pre-Accept to MBA'", {
                    extra: {
                        application_id: @cohort_application.id
                    }
                })
                @cohort_application.update(admissions_decision: "Pre-Accept to MBA", cohort_id: Cohort.promoted_cohort("emba")["id"])
                @cohort_application.perform_admissions_decision
                expect(@cohort_application.status).to eq("pending")
            end
        end

        it "should correctly process 'Accept to Business Certificate'" do
            @cohort_application.update(admissions_decision: "Accept to Business Certificate", cohort_id: Cohort.promoted_cohort("mba")["id"])
            expect(@cohort_application).to receive(:save!).and_call_original
            @cohort_application.perform_admissions_decision
            expect(@cohort_application.status).to eq("accepted")
            expect(@cohort_application.cohort_id).to eq(Cohort.promoted_cohort("the_business_certificate")["id"])
        end

        it "should correctly process 'Accept to Career Network Only'" do
            @cohort_application.update(admissions_decision: "Accept to Career Network Only", cohort_id: Cohort.promoted_cohort("mba")["id"])
            expect(@cohort_application).to receive(:save!).and_call_original
            @cohort_application.perform_admissions_decision
            expect(@cohort_application.status).to eq("accepted")
            expect(@cohort_application.cohort_id).to eq(Cohort.promoted_cohort("career_network_only")["id"])
        end

        describe "EMBA" do

            before(:each) do
                # ensure_institution_state_for_fallback_program_type gets called on the user when we save
                # the cohort_application and can mess with some of the assertions in these specs that are
                # focused on testing just the relevant logic. Since it's not important for these specs,
                # we just stub it out.
                allow(@cohort_application.user).to receive(:ensure_institution_state_for_fallback_program_type)
            end

            it "should correctly process EMBA decision after a non-pending, non-MBA application was 'Invite to Interview'" do
                @cohort_application.update_columns(
                    admissions_decision: "Accept to Business Certificate",
                    cohort_id: Cohort.promoted_cohort("the_business_certificate")["id"],
                    status: 'accepted'
                )
                allow(@cohort_application).to receive(:previous_admissions_decision).and_return("Invite to Interview")

                @cohort_application.update(admissions_decision: "EMBA w/ No Scholarship")
                expect(@cohort_application).to receive(:save!).twice.and_call_original
                @cohort_application.perform_admissions_decision
                expect(@cohort_application.status).to eq("rejected") # rejected
                expect(@cohort_application.cohort_id).to eq(Cohort.promoted_cohort("the_business_certificate")["id"]) # still biz cert

                # We expect that a new EMBA pending application was created
                query = CohortApplication.where(
                    user_id: @cohort_application.user_id,
                    cohort_id: Cohort.promoted_cohort("emba")["id"],
                    status: "pre_accepted"
                )
                expect(query.count).to be(1)

                app = query.first
                expect(app.scholarship_level).to eq(Cohort.promoted_cohort("emba").scholarship_levels.detect { |level| level["name"] == "No Scholarship" })
            end

            it "should correctly process 'EMBA w/ No Scholarship" do
                @cohort_application.update(admissions_decision: "EMBA w/ No Scholarship", cohort_id: Cohort.promoted_cohort("mba")["id"])
                expect(@cohort_application).to receive(:save!).and_call_original
                @cohort_application.perform_admissions_decision
                expect(@cohort_application.status).to eq("pending")
                expect(@cohort_application.cohort_id).to eq(Cohort.promoted_cohort("emba")["id"])
                expect(@cohort_application.applicable_coupon_info["id"]).to eq("none")
            end

            describe "w/ Full Scholarship" do
                it "should correctly process when mba" do
                    mba_cohort = Cohort.where(program_type: "mba", was_published: true).first
                    sister_emba_cohort = Cohort.where(program_type: "emba", was_published: true).first
                    @cohort_application.update(admissions_decision: "EMBA w/ Full Scholarship", cohort_id: mba_cohort.id)

                    expect(@cohort_application.cohort).to receive(:sister_cohort).and_return(sister_emba_cohort)
                    expect(Cohort).not_to receive(:promoted_cohort)
                    expect(@cohort_application).to receive(:save!).and_call_original

                    # We shouldn't log an "Unexpected EMBA decision" in this case
                    expect(Raven).not_to receive(:capture_exception)

                    @cohort_application.perform_admissions_decision

                    expect(@cohort_application.status).to eq("pre_accepted")
                    expect(@cohort_application.cohort_id).to eq(sister_emba_cohort.id)
                    expect(@cohort_application.applicable_coupon_info["id"]).to eq("discount_100_percent")
                end

                it "should correctly process when emba" do
                    emba_cohort = Cohort.where(program_type: "emba", was_published: true).first
                    @cohort_application.update(admissions_decision: "EMBA w/ Full Scholarship", cohort_id: emba_cohort.id)

                    expect(Cohort).not_to receive(:promoted_cohort)
                    expect(@cohort_application).not_to receive(:sister_cohort)
                    expect(@cohort_application).to receive(:save!).and_call_original

                    @cohort_application.perform_admissions_decision

                    expect(@cohort_application.status).to eq("pre_accepted")
                    expect(@cohort_application.cohort_id).to eq(emba_cohort.id)
                    expect(@cohort_application.applicable_coupon_info["id"]).to eq("discount_100_percent")
                end

                it "should correctly process when other" do
                    emba_cohort = Cohort.where(program_type: "emba", was_published: true).first
                    @cohort_application.update(admissions_decision: "EMBA w/ Full Scholarship", cohort_id: Cohort.where(program_type: "the_business_certificate").first.id)

                    expect(@cohort_application).not_to receive(:sister_cohort)
                    expect(Cohort).to receive(:promoted_cohort).and_return(emba_cohort.published_version)
                    expect(@cohort_application).to receive(:save!).and_call_original

                    @cohort_application.perform_admissions_decision

                    expect(@cohort_application.status).to eq("pre_accepted")
                    expect(@cohort_application.cohort_id).to eq(emba_cohort.id)
                    expect(@cohort_application.applicable_coupon_info["id"]).to eq("discount_100_percent")
                end
            end

            describe "w/ X Scholarship" do
                it "should correcty process when mba" do
                    test_paid_emba_decision_for_mba("EMBA w/ Level 1 Scholarship", 15000)
                    test_paid_emba_decision_for_mba("EMBA w/ Level 2 Scholarship", 25000)
                    test_paid_emba_decision_for_mba("EMBA w/ Level 3 Scholarship", 35000)
                    test_paid_emba_decision_for_mba("EMBA w/ Level 4 Scholarship", 45000)
                    test_paid_emba_decision_for_mba("EMBA w/ Level 5 Scholarship", 55000)
                end

                it "should correctly process when emba" do
                    test_paid_emba_decision_for_emba("EMBA w/ Level 1 Scholarship", 15000)
                    test_paid_emba_decision_for_emba("EMBA w/ Level 2 Scholarship", 25000)
                    test_paid_emba_decision_for_emba("EMBA w/ Level 3 Scholarship", 35000)
                    test_paid_emba_decision_for_emba("EMBA w/ Level 4 Scholarship", 45000)
                    test_paid_emba_decision_for_emba("EMBA w/ Level 5 Scholarship", 55000)
                end

                it "should correctly process when other" do
                    test_paid_emba_decision_for_other("EMBA w/ Level 1 Scholarship", 15000)
                    test_paid_emba_decision_for_other("EMBA w/ Level 2 Scholarship", 25000)
                    test_paid_emba_decision_for_other("EMBA w/ Level 3 Scholarship", 35000)
                    test_paid_emba_decision_for_other("EMBA w/ Level 4 Scholarship", 45000)
                    test_paid_emba_decision_for_other("EMBA w/ Level 5 Scholarship", 55000)
                end

                def test_paid_emba_decision_for_mba(decision, coupon_amount)
                    mba_cohort = Cohort.where(program_type: "mba", was_published: true).first
                    sister_emba_cohort = Cohort.where(program_type: "emba", was_published: true).first
                    @cohort_application.update(admissions_decision: decision, cohort_id: mba_cohort.id)

                    expect(@cohort_application.cohort).to receive(:sister_cohort).and_return(sister_emba_cohort)
                    expect(Cohort).not_to receive(:promoted_cohort)
                    expect(@cohort_application).to receive(:save!).and_call_original

                    expect(Raven).to receive(:capture_exception).with("Unexpected EMBA decision", {
                        extra: {
                            application_id: @cohort_application.id,
                            program_type: 'emba'
                        },
                        :level=>"warning"
                    })

                    @cohort_application.perform_admissions_decision

                    expect(@cohort_application.status).to eq("pending")
                    expect(@cohort_application.cohort_id).to eq(sister_emba_cohort["id"])

                    expect(@cohort_application.applicable_coupon_info["id"]).not_to be_nil
                    expect(@cohort_application.applicable_coupon_info["id"]).to eq("discount_#{coupon_amount / 100}")

                end

                def test_paid_emba_decision_for_emba(decision, coupon_amount)
                    emba_cohort = Cohort.where(program_type: "emba", was_published: true).first
                    @cohort_application.update(admissions_decision: decision, cohort_id: emba_cohort.id)

                    expect(Cohort).not_to receive(:promoted_cohort)
                    expect(@cohort_application).not_to receive(:sister_cohort)
                    expect(@cohort_application).to receive(:save!).and_call_original

                    @cohort_application.perform_admissions_decision

                    expect(@cohort_application.status).to eq("pre_accepted")
                    expect(@cohort_application.cohort_id).to eq(emba_cohort.id)
                    expect(@cohort_application.applicable_coupon_info["id"]).to eq("discount_#{coupon_amount / 100}")
                end

                def test_paid_emba_decision_for_other(decision, coupon_amount)
                    emba_cohort = Cohort.where(program_type: "emba", was_published: true).first
                    @cohort_application.update(admissions_decision: decision, cohort_id: Cohort.where(program_type: "the_business_certificate").first.id)

                    expect(@cohort_application).not_to receive(:sister_cohort)
                    expect(Cohort).to receive(:promoted_cohort).and_return(emba_cohort.published_version)
                    expect(@cohort_application).to receive(:save!).and_call_original

                    @cohort_application.perform_admissions_decision

                    expect(@cohort_application.status).to eq("pending")
                    expect(@cohort_application.cohort_id).to eq(emba_cohort.id)
                    expect(@cohort_application.applicable_coupon_info["id"]).not_to be_nil
                    expect(@cohort_application.applicable_coupon_info["id"]).to eq("discount_#{coupon_amount / 100}")
                end
            end

            describe "Invite to Interview" do

                def expect_event_logged(attributes)
                    expect(Event).to receive(:create_server_event!) do |id, user_id, event_type, payload|
                        expect(user_id).to eq(@cohort_application.user_id)
                        expect(event_type).to eq('cohort:invite_to_interview')
                        expect(payload).to eq(attributes)
                        expects_log_to_external_systems
                    end
                end

                it "should log event" do
                    @cohort_application.update_columns(admissions_decision: 'foo', rubric_inherited: true)
                    @cohort_application.update(admissions_decision: "Invite to Interview")

                    expect_event_logged(@cohort_application.event_attributes.merge({
                        previous_admissions_decision: 'foo',
                        rubric_inherited: true
                    }))

                    @cohort_application.perform_admissions_decision
                end

                it "should include promoted EMBA cohort attrs for accepted Biz Cert applications" do
                    mock_cohort = double('Cohort')
                    mock_cohort_event_attrs = {
                        foo: 'bar'
                    }
                    expect(mock_cohort).to receive(:program_type).and_return("emba")
                    expect(mock_cohort).to receive(:event_attributes).and_return(mock_cohort_event_attrs)
                    expect(Cohort).to receive(:promoted_cohort).with('emba').and_return(mock_cohort)

                    expect(@cohort_application).to receive(:status).at_least(1).times.and_return('accepted')
                    expect(@cohort_application).to receive(:program_type).at_least(1).times.and_return('the_business_certificate')
                    expect(@cohort_application).to receive(:admissions_decision).at_least(1).times.and_return('Invite to Interview')
                    expect(@cohort_application).to receive(:suggested_emba_cohort_attrs).and_call_original
                    expect(@cohort_application).to receive(:requires_slack_room_id?).and_return(false) # get around validations for this spec

                    expect_event_logged(@cohort_application.event_attributes.merge({
                        suggested_program_type: 'emba',
                        suggested_cohort: {
                            foo: 'bar'
                        }
                    }))
                    @cohort_application.perform_admissions_decision
                end
            end

            describe "Invite to Convert to EMBA" do

                it "should log event with sister cohort attrs" do
                    mock_cohort = double('Cohort')
                    mock_cohort_event_attrs = {foo: 'foo'}
                    expect(mock_cohort).to receive(:program_type).and_return("emba")
                    expect(mock_cohort).to receive(:event_attributes).with(@cohort_application.user.timezone).and_return(mock_cohort_event_attrs)
                    expect(@cohort_application.published_cohort).to receive(:sister_cohort).at_least(1).times.and_return(mock_cohort)
                    expect(@cohort_application).to receive(:suggested_emba_cohort_attrs).and_call_original

                    expect(Event).to receive(:create_server_event!) do |id, user_id, event_type, payload|
                        expect(user_id).to eq(@cohort_application.user_id)
                        expect(event_type).to eq('cohort:invite_to_convert_program_type')
                        expect(payload).to eq(@cohort_application.event_attributes.merge({
                            suggested_program_type: 'emba',
                            suggested_cohort: mock_cohort_event_attrs
                        }))
                        expects_log_to_external_systems
                    end

                    @cohort_application.update(admissions_decision: "Invite to Convert to EMBA")
                    @cohort_application.perform_admissions_decision

                end
            end

            describe "Accept w/ Career Network" do

                before(:each) do
                    @cohort_application = CohortApplication.left_outer_joins(cohort: :slack_rooms)
                                            .where("cohort_slack_rooms.id is null")
                                            .where(status: 'pending').first
                    @cohort_application.user.can_edit_career_profile = false
                    @cohort_application.user.save!
                end

                it "should accept application and update User.can_edit_career_profile if program_type_config.supports_career_network_access_on_airtable_decision?" do
                    allow(@cohort_application.program_type_config).to receive(:supports_career_network_access_on_airtable_decision?).and_return(true)

                    @cohort_application.update(admissions_decision: "Accept w/ Career Network")
                    @cohort_application.perform_admissions_decision
                    expect(@cohort_application.reload.status).to eq('accepted')
                    expect(@cohort_application.user.reload.can_edit_career_profile).to be(true)
                end

                it "should not accept application or update User.can_edit_career_profile if !program_type_config.supports_career_network_access_on_airtable_decision?" do
                    allow(@cohort_application.program_type_config).to receive(:supports_career_network_access_on_airtable_decision?).and_return(false)

                    @cohort_application.update(admissions_decision: "Accept w/ Career Network")
                    @cohort_application.perform_admissions_decision
                    expect(@cohort_application.reload.status).to eq('pending')
                    expect(@cohort_application.user.reload.can_edit_career_profile).to be(false)
                end

            end

        end

        def expects_log_to_external_systems
            fake_event = "fake_event"
            expect(fake_event).to receive(:log_to_external_systems).with(no_args)
            fake_event
        end
    end

    describe "#reject_and_create_pending_emba_application" do

        it "should reject the current application and then create and return a new pending EMBA cohort application for the first EMBA cohort whose decision_date hasn't passed yet" do
            user = users(:pending_the_business_certificate_cohort_user)
            cohort_application = user.cohort_applications.first
            now = Time.now
            allow(Time).to receive(:now).and_return(now)
            expect(cohort_application.status).not_to eq('rejected')

            pending_emba_application = cohort_application.send(:reject_and_create_pending_emba_application)

            cohort = AdmissionRound.where(program_type: 'emba').where("decision_date > '#{now}'").reorder(:decision_date).first.cohort
            expect(cohort_application.reload.status).to eq('rejected')

            expect(pending_emba_application.user_id).to eq(user.id)
            expect(pending_emba_application.cohort_id).to eq(cohort['id'])
            expect(pending_emba_application.status).to eq('pending')
            expect(pending_emba_application.applied_at).to eq(now)
        end
    end

    describe "#reject_and_convert_to_emba" do

        it "should do nothing if !program_type_config.supports_reject_with_conversion_to_emba?" do
            cohort_application = CohortApplication.first
            expect(cohort_application.program_type_config).to receive(:supports_reject_with_conversion_to_emba?).and_return(false)
            expect(cohort_application).not_to receive(:reject_and_create_pending_emba_application)
            expect_any_instance_of(CohortApplication).not_to receive(:save!)
            cohort_application.send(:reject_and_convert_to_emba)
        end

        it "should reject_and_create_pending_emba_application and then set admissions_decision on the emba application to the current application's admissions_decision" do
            # We mock out the call to reject_and_create_pending_emba_application and its return value
            # with a pre-created pending EMBA application because of the unique constraints at the
            # database level on only having a single pending cohort application.
            user = users(:rejected_the_business_certificate_cohort_user)
            rejected_cohort_application = user.cohort_applications.first
            allow(rejected_cohort_application).to receive(:admissions_decision).and_return('FOO')
            emba_cohort = Cohort.where(program_type: "emba", was_published: true).first
            emba_application = CohortApplication.create!(
                user_id: user.id,
                cohort_id: emba_cohort.id,
                applied_at: Time.now,
                status: 'pending'
            )

            expect(rejected_cohort_application).to receive(:reject_and_create_pending_emba_application).and_return(emba_application)
            expect(emba_application.admissions_decision).not_to eq('FOO')

            rejected_cohort_application.send(:reject_and_convert_to_emba)

            expect(emba_application.reload.admissions_decision).to eq('FOO')
        end
    end

    describe "#revert_rejection_and_conversion_to_emba" do

        it "should be called after_update" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:revert_rejection_and_conversion_to_emba)
            cohort_application.save!
        end

        it "should not do anything if program_type for current application is not 'emba'" do
            user = users(:accepted_mba_cohort_user)
            cohort_application = user.cohort_applications.first
            expect_any_instance_of(CohortApplication).not_to receive(:update!)
            cohort_application.send(:revert_rejection_and_conversion_to_emba)
        end

        describe "when program_type is 'emba'" do
            attr_accessor :user, :cohort_application

            before(:each) do
                @user = users(:pending_emba_cohort_user)
                @cohort_application = user.cohort_applications.first
            end

            it "should not do anything if status has not changed to 'rejected'" do
                expect_any_instance_of(CohortApplication).not_to receive(:update!)
                cohort_application.send(:revert_rejection_and_conversion_to_emba)
            end

            describe "when saved_change_to_status? to 'rejected'" do

                before(:each) do
                    expect(cohort_application).to receive(:saved_change_to_status?).with(to: 'rejected').and_return(true)
                end

                it "should not do anything if no previous cohort_application is found" do
                    # only one cohort application, so no previous application should be found
                    expect(cohort_application.user.cohort_applications.size).to eq(1)
                    expect_any_instance_of(CohortApplication).not_to receive(:update!)
                    cohort_application.send(:revert_rejection_and_conversion_to_emba)
                end

                describe "when previous cohort application is present" do
                    attr_accessor :previous_cohort_application

                    before(:each) do
                        @previous_cohort_application = CohortApplication.create!(
                            user_id: user.id,
                            cohort_id: cohorts(:published_the_business_certificate_cohort).id,
                            applied_at: cohort_application.applied_at - 1.day,
                            status: 'rejected'
                        )
                        allow(cohort_application.user).to receive(:cohort_applications).and_return([cohort_application, previous_cohort_application])
                    end

                    it "should not do anything if it wasn't rejected_and_converted_to_emba?" do
                        expect(previous_cohort_application).to receive(:rejected_and_converted_to_emba?).and_return(false)
                        expect_any_instance_of(CohortApplication).not_to receive(:update!)
                        cohort_application.send(:revert_rejection_and_conversion_to_emba)
                    end

                    describe "when previous cohort application was rejected_and_converted_to_emba?" do

                        before(:each) do
                            expect(previous_cohort_application).to receive(:rejected_and_converted_to_emba?).and_return(true)
                        end

                        it "should reject current cohort application and update the previous cohort application to be accepted with an updated applied_at timestamp" do
                            now = Time.now
                            allow(Time).to receive(:now).and_return(now)
                            expect(previous_cohort_application).to receive(:update!).with(applied_at: now, status: 'accepted')
                            cohort_application.send(:revert_rejection_and_conversion_to_emba)
                        end
                    end
                end
            end
        end
    end
end
