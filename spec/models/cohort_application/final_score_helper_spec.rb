require 'spec_helper'

describe CohortApplication::FinalScoreHelper do

    fixtures(:users, :cohorts)

    before(:each) do
        SafeCache.clear # projects are cached
    end

    describe "calculate_final_score" do

        # we should be counting electives for mba/emba
        # but not for business cert, unless...

        it "should be nil if not accepted" do
            expect(CohortApplication::FinalScoreHelper).not_to receive(:get_project_score)
            cohort_application = CohortApplication.where.not(status: 'accepted').first
            expect(CohortApplication::FinalScoreHelper.calculate_final_score(cohort_application)).to eq({
                final_score: nil,
                project_score: nil,
                meets_graduation_requirements: false
            })
        end

        # for full coverage, we would need to test each branch of the elsif here,
        # but it's pretty much just configuration so seems kind of pointless
        it "should call weighted_score otherwise" do
            cohort_application = CohortApplication.where(status: 'accepted').first

            expect(CohortApplication::FinalScoreHelper).to receive(:get_project_score).with(cohort_application).and_return([0.5, 1])
            weights = {weights: 'ok'}
            expect(CohortApplication::FinalScoreHelper).to receive(:weights).with(cohort_application).and_return(weights)
            expect(CohortApplication::FinalScoreHelper).to receive(:weighted_score).with(
                cohort_application,
                0.5,
                weights
            ).and_return(0.42)
            expect(CohortApplication::FinalScoreHelper).to receive(:meets_graduation_requirements).with(cohort_application, 0.42).and_return(true)
            expect(CohortApplication::FinalScoreHelper.calculate_final_score(cohort_application)).to eq({
                final_score: 0.42,
                project_score: 0.5,
                meets_graduation_requirements: true
            })
        end

        it "should respect the project_weight that comes back from project_score" do
            cohort_application = CohortApplication.where(status: 'accepted').first

            # here we are setting the project_weight to 0.6
            expect(CohortApplication::FinalScoreHelper).to receive(:get_project_score).with(cohort_application).and_return([0.5, 0.6])
            weights = {
                project_score: 0.1
            }
            expect(CohortApplication::FinalScoreHelper).to receive(:weights).with(cohort_application).and_return(weights)
            expect(CohortApplication::FinalScoreHelper).to receive(:weighted_score).with(
                cohort_application,
                0.5,
                {
                    project_score: 0.1*0.6
                }
            ).and_return(0.42)
            expect(CohortApplication::FinalScoreHelper).to receive(:meets_graduation_requirements).with(cohort_application, 0.42).and_return(true)
            expect(CohortApplication::FinalScoreHelper.calculate_final_score(cohort_application)).to eq({
                final_score: 0.42,
                project_score: 0.5,
                meets_graduation_requirements: true
            })
        end

    end

    describe "weights" do

        attr_accessor :cohort_application

        before(:each) do
            self.cohort_application = CohortApplication.first
        end

        describe "legacy cohorts" do

            it "should work for MBA1" do
                expect(cohort_application).to receive(:published_cohort).and_return(
                    OpenStruct.new(
                        name: 'MBA1'
                    )
                )
                assert_weights(
                    {
                        avg_test_score: 0.7,
                        average_assessment_score_best: 0.3
                    }
                )
            end

            it "should work for MBA2-7" do
                expect(cohort_application).to receive(:program_type).at_least(1).times.and_return('mba')
                expect(cohort_application).to receive(:published_cohort).at_least(1).times.and_return(
                    OpenStruct.new(
                        start_date: Time.parse('2017/09/17')
                    )
                )
                assert_weights(
                    {
                        avg_test_score: 0.7,
                        average_assessment_score_best: 0.2,
                        participation_score: 0.1
                    }
                )
            end

            it "should work for MBA8-21" do
                expect(cohort_application).to receive(:program_type).at_least(1).times.and_return('mba')
                expect(cohort_application).to receive(:published_cohort).at_least(1).times.and_return(
                    OpenStruct.new(
                        start_date: Time.parse('2019/06/16')
                    )
                )
                assert_weights(
                    {
                        avg_test_score: 0.7,
                        average_assessment_score_best: 0.2,
                        project_score: 0.1
                    }
                )
            end

            it "should work for EMBA1-3" do
                expect(cohort_application).to receive(:program_type).at_least(1).times.and_return('emba')
                expect(cohort_application).to receive(:published_cohort).at_least(1).times.and_return(
                    OpenStruct.new(
                        start_date: Time.parse('2017/09/24')
                    )
                )
                assert_weights(
                    {
                        avg_test_score: 0.6,
                        average_assessment_score_best: 0.2,
                        project_score: 0.1,
                        participation_score: 0.1
                    }
                )
            end

            it "should work for EMBA4-17" do
                expect(cohort_application).to receive(:program_type).at_least(1).times.and_return('emba')
                expect(cohort_application).to receive(:published_cohort).at_least(1).times.and_return(
                    OpenStruct.new(
                        start_date: Time.parse('2019/06/30')
                    )
                )
                assert_weights(
                    {
                        avg_test_score: 0.6,
                        average_assessment_score_best: 0.2,
                        project_score: 0.2
                    }
                )
            end

        end

        describe "current cohorts" do

            it "should work for MBA22+" do
                expect(cohort_application).to receive(:program_type).at_least(1).times.and_return('mba')
                expect(cohort_application).to receive(:published_cohort).at_least(1).times.and_return(
                    OpenStruct.new(
                        start_date: Time.parse('2019/06/17')
                    )
                )
                assert_weights(
                    {
                        avg_test_score: 0.7,
                        average_assessment_score_best: 0.1,
                        project_score: 0.2
                    }
                )
            end

            it "should work for EMBA18+" do
                expect(cohort_application).to receive(:program_type).at_least(1).times.and_return('emba')
                expect(cohort_application).to receive(:published_cohort).at_least(1).times.and_return(
                    OpenStruct.new(
                        start_date: Time.parse('2019/07/01')
                    )
                )
                assert_weights(
                    {
                        avg_test_score: 0.6,
                        average_assessment_score_best: 0.1,
                        project_score: 0.3
                    }
                )
            end

            it "should work for other program_types" do
                expect(cohort_application).to receive(:program_type).at_least(1).times.and_return('the_business_certificate')
                assert_weights(
                    {
                        avg_test_score: 0.7,
                        average_assessment_score_best: 0.3
                    }
                )
            end

        end

        def assert_weights(expected)
            expect(CohortApplication::FinalScoreHelper.send(:weights, cohort_application)).to eq(expected)
        end

    end

    describe "weighted_score" do

        before(:each) do
            allow(CohortApplication::FinalScoreHelper).to receive(:meets_graduation_requirements).and_return(true)
        end

        it "should work" do
            cohort = cohorts(:published_mba_cohort)
            cohort_application = cohort.cohort_applications.find_by_status('accepted')
            scores = setup_scores(cohort, cohort_application)

            assert_weighted_score(cohort_application, scores, {
                avg_test_score: 0.1,
                average_assessment_score_best: 0.4,
                project_score: 0.3,
                participation_score: 0.2
            })

            assert_weighted_score(cohort_application, scores, {
                avg_test_score: 0.3,
                average_assessment_score_best: 0.7
            })

            assert_weighted_score(cohort_application, scores, {
                avg_test_score: 0.3,
                average_assessment_score_best: 0.4,
                project_score: 0.3
            })

            assert_weighted_score(cohort_application, scores, {
                avg_test_score: 0.3,
                average_assessment_score_best: 0.4,
                participation_score: 0.3
            })

        end

        it "should work when the weights do not add up to 1" do

            cohort = cohorts(:published_mba_cohort)
            cohort_application = cohort.cohort_applications.find_by_status('accepted')
            scores = setup_scores(cohort, cohort_application)

            # This is how the weights would look if there were two projects
            # and one of them was waived for this user.  The weights only
            # add up to 0.95
            weights = {
                avg_test_score: 0.7,
                average_assessment_score_best: 0.2,
                project_score: 0.05
            }

            total_weight = 0.95
            total_weighted_scores = (weights[:avg_test_score] * scores[:avg_test_score]) +
                            (weights[:average_assessment_score_best] * scores[:average_assessment_score_best]) +
                            (weights[:project_score] * scores[:project_score])
            expected_score = total_weighted_scores / total_weight

            expect(CohortApplication::FinalScoreHelper.send(:weighted_score, cohort_application, scores[:project_score], weights)).to be_within(0.000001).of(expected_score)
        end

        def setup_scores(cohort, cohort_application)
            cohort_lesson_locale_pack_ids = cohort.get_all_lesson_locale_pack_ids
            test_lesson_locale_packs = Lesson.where(test: true, locale_pack_id: cohort_lesson_locale_pack_ids).pluck(:locale_pack_id)
            expect(test_lesson_locale_packs.size).to be > 2 # sanity check
            assessment_lesson_locale_packs = Lesson.where(assessment: true, locale_pack_id: cohort_lesson_locale_pack_ids).pluck(:locale_pack_id)
            expect(assessment_lesson_locale_packs.size).to be > 2 # sanity check

            cohort_application.user.lesson_progresses.delete_all

            {
                test_lesson_locale_packs[0] => 0.3,
                test_lesson_locale_packs[1] => 0.5,

                assessment_lesson_locale_packs[0] => 0.7,
                assessment_lesson_locale_packs[1] => 0.8
            }.each do |locale_pack_id, score|
                LessonProgress.create!(
                    user_id: cohort_application.user_id,
                    started_at: Time.now,
                    completed_at: Time.now,
                    best_score: score,
                    locale_pack_id: locale_pack_id
                )
            end

            # 0.6 participation score
            ActiveRecord::Base.connection.execute(%Q~
                insert into user_participation_scores (user_id, score) values ('#{cohort_application.user_id}', 0.6);
            ~)

            {
                # averages should just be based on lessons that have been completed
                avg_test_score: (0.3 + 0.5) / 2,
                average_assessment_score_best: (0.7 + 0.8) / 2,
                project_score: 0.5,
                participation_score: 0.6
            }
        end

        def assert_weighted_score(cohort_application, scores, weights)
            expected_score = (weights[:avg_test_score] ? weights[:avg_test_score] * scores[:avg_test_score] : 0) +
                            (weights[:average_assessment_score_best] ? weights[:average_assessment_score_best] * scores[:average_assessment_score_best] : 0) +
                            (weights[:project_score] ? weights[:project_score] * scores[:project_score] : 0) +
                            (weights[:participation_score] ? weights[:participation_score] * scores[:participation_score] : 0)

            expect(CohortApplication::FinalScoreHelper.send(:weighted_score, cohort_application, scores[:project_score], weights)).to be_within(0.000001).of(expected_score)
        end

    end

    describe "meets_graduation_requirements" do

        attr_reader :passing_final_score, :failing_final_score

        before(:each) do
            @passing_final_score = 0.71
            @failing_final_score = 0.69
        end

        it "should work for mba cohorts before MBA8" do
            cohort_application = CohortApplication.where(status: 'accepted', cohorts: {program_type: 'mba'}).joins(:cohort).first
            cohort_application.cohort.start_date = '2017/09/17'
            cohort_application.cohort.publish!

            curriculum_complete = false

            allow(cohort_application).to receive(:curriculum_complete?) do
                curriculum_complete
            end

            # false if not all streams complete
            expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, passing_final_score)).to be(false)
            curriculum_complete = true

            # false if final_score < 0.7
            expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, failing_final_score)).to be(false)

            # true otherwise
            expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, passing_final_score)).to be(true)
        end

        describe "MBA current" do

            attr_reader :cohort_application

            before(:each) do
                @cohort_application = CohortApplication.where(status: 'accepted', cohorts: {program_type: 'mba'}).joins(:cohort).first
                @cohort_application.cohort.start_date = '2017/09/19'
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::CAPSTONE_PROJECT_TYPE).and_return(0)
            end

            it "should be true if one or less unpassed standard project" do
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::PRESENTATION_PROJECT_TYPE).and_return(0)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::STANDARD_PROJECT_TYPE).and_return(1)
                allow(cohort_application).to receive(:curriculum_complete?).and_return(true)
                expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, passing_final_score)).to be(true)
            end

            it "should be false if more than one unpassed standard project" do
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::PRESENTATION_PROJECT_TYPE).and_return(0)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::STANDARD_PROJECT_TYPE).and_return(2)
                allow(cohort_application).to receive(:curriculum_complete?).and_return(true)
                expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, passing_final_score)).to be(false)
            end

            it "should be false if any unpassed presentation projects" do
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::PRESENTATION_PROJECT_TYPE).and_return(1)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::STANDARD_PROJECT_TYPE).and_return(0)
                allow(cohort_application).to receive(:curriculum_complete?).and_return(true)
                expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, passing_final_score)).to be(false)
            end

            it "should be false if curriculum is not complete" do
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::PRESENTATION_PROJECT_TYPE).and_return(0)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::STANDARD_PROJECT_TYPE).and_return(0)
                allow(cohort_application).to receive(:curriculum_complete?).and_return(false)
                expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, passing_final_score)).to be(false)
            end

            it "should be false if final score is less than 0.7" do
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::PRESENTATION_PROJECT_TYPE).and_return(0)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::STANDARD_PROJECT_TYPE).and_return(0)
                allow(cohort_application).to receive(:curriculum_complete?).and_return(true)
                expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, failing_final_score)).to be(false)
            end

        end

        describe "EMBA" do

            attr_reader :cohort_application

            before(:each) do
                @cohort_application = CohortApplication.where(status: 'accepted', cohorts: {program_type: 'emba'}).joins(:cohort).first
            end

            it "should be false if capstone is unpassed" do
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::CAPSTONE_PROJECT_TYPE).and_return(1)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::PRESENTATION_PROJECT_TYPE).and_return(0)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::STANDARD_PROJECT_TYPE).and_return(0)
                allow(cohort_application).to receive(:curriculum_complete?).and_return(true)
                allow(cohort_application).to receive(:required_specializations_complete?).and_return(true)
                expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, passing_final_score)).to be(false)
            end

            it "should be true if one or less unpassed standard project" do
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::CAPSTONE_PROJECT_TYPE).and_return(0)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::PRESENTATION_PROJECT_TYPE).and_return(0)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::STANDARD_PROJECT_TYPE).and_return(1)
                allow(cohort_application).to receive(:curriculum_complete?).and_return(true)
                allow(cohort_application).to receive(:required_specializations_complete?).and_return(true)
                expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, passing_final_score)).to be(true)
            end

            it "should be false if more than one unpassed standard project" do
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::CAPSTONE_PROJECT_TYPE).and_return(0)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::PRESENTATION_PROJECT_TYPE).and_return(0)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::STANDARD_PROJECT_TYPE).and_return(2)
                allow(cohort_application).to receive(:curriculum_complete?).and_return(true)
                allow(cohort_application).to receive(:required_specializations_complete?).and_return(true)
                expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, passing_final_score)).to be(false)
            end

            it "should be false if any unpassed presentation projects" do
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::CAPSTONE_PROJECT_TYPE).and_return(0)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::PRESENTATION_PROJECT_TYPE).and_return(1)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::STANDARD_PROJECT_TYPE).and_return(0)
                allow(cohort_application).to receive(:curriculum_complete?).and_return(true)
                allow(cohort_application).to receive(:required_specializations_complete?).and_return(true)
                expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, passing_final_score)).to be(false)
            end

            it "should be false if curriculum is not complete" do
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::CAPSTONE_PROJECT_TYPE).and_return(0)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::PRESENTATION_PROJECT_TYPE).and_return(0)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::STANDARD_PROJECT_TYPE).and_return(0)
                allow(cohort_application).to receive(:curriculum_complete?).and_return(false)
                allow(cohort_application).to receive(:required_specializations_complete?).and_return(true)
                expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, passing_final_score)).to be(false)
            end

            it "should be false if specializations are not complete" do
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::CAPSTONE_PROJECT_TYPE).and_return(0)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::PRESENTATION_PROJECT_TYPE).and_return(0)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::STANDARD_PROJECT_TYPE).and_return(0)
                allow(cohort_application).to receive(:curriculum_complete?).and_return(true)
                allow(cohort_application).to receive(:required_specializations_complete?).and_return(false)
                expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, passing_final_score)).to be(false)
            end

            it "should be false if final score is less than 0.7" do
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::CAPSTONE_PROJECT_TYPE).and_return(0)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::PRESENTATION_PROJECT_TYPE).and_return(0)
                allow(CohortApplication::FinalScoreHelper).to receive(:unpassed_project_count_of_type).with(cohort_application, LearnerProject::STANDARD_PROJECT_TYPE).and_return(0)
                allow(cohort_application).to receive(:curriculum_complete?).and_return(true)
                allow(cohort_application).to receive(:required_specializations_complete?).and_return(true)
                expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, failing_final_score)).to be(false)
            end

        end

        it "should work for the business certificate" do

            cohort_application = CohortApplication.where(status: 'accepted', cohorts: {program_type: 'the_business_certificate'}).joins(:cohort).first

            curriculum_complete = false

            allow(cohort_application).to receive(:curriculum_complete?) do
                curriculum_complete
            end

            # false if not all streams complete
            expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, passing_final_score)).to be(false)
            curriculum_complete = true
            # false if final_score < 0.7
            expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, failing_final_score)).to be(false)
            # true otherwise
            expect(CohortApplication::FinalScoreHelper.send(:meets_graduation_requirements, cohort_application, passing_final_score)).to be(true)
        end
    end

    describe "unpassed_project_count_of_type" do

        it "should return a count of failing project_progress_statuses with the same project_type" do
            cohort_application = CohortApplication.where(status: 'accepted', cohorts: {program_type: 'emba'}).joins(:cohort).first
            allow(CohortApplication::FinalScoreHelper).to receive(:get_project_progress_statuses).with(cohort_application).and_return([
                OpenStruct.new({
                    project_type: "a_project_type",
                    requirement_identifier: 'rid1',
                    unpassed: false,
                }),OpenStruct.new({
                    project_type: "a_project_type",
                    requirement_identifier: 'rid2',
                    unpassed: true,
                })
            ])
            expect(CohortApplication::FinalScoreHelper.send(:unpassed_project_count_of_type, cohort_application, 'a_project_type')).to eq(1)
            expect(CohortApplication::FinalScoreHelper.send(:unpassed_project_count_of_type, cohort_application, 'another_project_type')).to eq(0)
        end

    end

    describe "get_project_progress_statuses" do

        it "should create an array of structs with valuable project_progress attributes" do
            cohort_application = CohortApplication.where(status: 'accepted', cohorts: {program_type: 'emba'}).joins(:cohort).first
            cohort = cohort_application.cohort

            learner_projects = [
                LearnerProject.create!(title: 'regular_1', requirement_identifier: 'regular_1'),
                LearnerProject.create!(title: 'regular_2', requirement_identifier: 'regular_2'),
                LearnerProject.create!(title: 'presentation_1', requirement_identifier: 'presentation_1', project_type: LearnerProject::PRESENTATION_PROJECT_TYPE),
                LearnerProject.create!(title: 'presentation_2', requirement_identifier: 'presentation_2', project_type: LearnerProject::PRESENTATION_PROJECT_TYPE),
                LearnerProject.create!(title: 'capstone', requirement_identifier: 'emba_capstone', project_type: LearnerProject::CAPSTONE_PROJECT_TYPE)
            ]
            cohort.learner_project_ids = learner_projects.map(&:id)
            expect(cohort.learner_project_ids.size).to eq(5)
            cohort.periods = []
            allow(cohort).to receive(:valid?).and_return(true)
            cohort.publish!

            admin = users(:admin)

            # NOTE: I only create one standard and one presentation progress
            # here intentionally. `get_project_progress_statuses` should create
            # empty structs for projects where the user has no progress
            ProjectProgress.create_or_update!(cohort_application.user, admin, {
                requirement_identifier: 'regular_1',
                score: 2
            })
            ProjectProgress.create_or_update!(cohort_application.user, admin, {
                requirement_identifier: 'presentation_1',
                score: 2,
                id_verified: true
            })
            ProjectProgress.create_or_update!(cohort_application.user, admin, {
                requirement_identifier: 'emba_capstone',
                score: 3
            })

            expect(CohortApplication::FinalScoreHelper.send(:get_project_progress_statuses, cohort_application.reload)).to match_array([
                OpenStruct.new({
                    project_type: "standard",
                    requirement_identifier: 'regular_1',
                    unpassed: false,
                    gradable: true
                }),OpenStruct.new({
                    project_type: "standard",
                    requirement_identifier: 'regular_2',
                    unpassed: true,
                    gradable: false
                }),OpenStruct.new({
                    project_type: "presentation",
                    requirement_identifier: 'presentation_1',
                    unpassed: false,
                    gradable: true
                }),OpenStruct.new({
                    project_type: "presentation",
                    requirement_identifier: 'presentation_2',
                    unpassed: true,
                    gradable: false
                }),OpenStruct.new({
                    project_type: "capstone",
                    requirement_identifier: 'emba_capstone',
                    unpassed: false,
                    gradable: true
                })
            ])
        end

    end

    describe "get_project_score" do
        attr_reader :cohort_application, :cohort, :capstone_project, :regular_project

        before(:each) do
            allow(CohortApplication::FinalScoreHelper).to receive(:meets_graduation_requirements).and_return(true)
        end

        it "should work when there are project_progresses and none are waived" do
            setup_cohort_with_projects

            # as long as the user has no progress, get_project_score returns nil and we fall back
            # to the old user_project_scores table
            CohortApplication::FinalScoreHelper.send(:get_project_progress_statuses, cohort_application)
            expect(CohortApplication::FinalScoreHelper.send(:get_project_score, cohort_application)[0]).to be_nil

            capstone_progress = ProjectProgress.create!(
                user_id: cohort_application.user_id,
                requirement_identifier: capstone_project.requirement_identifier,
                score: 2 # not passing
            )
            cohort_application.user.project_progresses.reload
            CohortApplication::FinalScoreHelper.send(:get_project_progress_statuses, cohort_application)
            expect(CohortApplication::FinalScoreHelper.send(:get_project_score, cohort_application)[0]).to eq(0)

            # capstone has double weight, so when it has a passing score the
            # total score is 0.67
            capstone_progress.update(score: 3)
            cohort_application.user.project_progresses.reload
            CohortApplication::FinalScoreHelper.send(:get_project_progress_statuses, cohort_application)
            expect(CohortApplication::FinalScoreHelper.send(:get_project_score, cohort_application)[0]).to be_within(0.01).of(0.67)

            regular_project_progress = ProjectProgress.create!(
                user_id: cohort_application.user_id,
                requirement_identifier: regular_project.requirement_identifier,
                score: 1 # not passing. minimum is 2
            )
            cohort_application.user.project_progresses.reload
            expect(CohortApplication::FinalScoreHelper.send(:get_project_score, cohort_application)[0]).to be_within(0.01).of(0.67)

            regular_project_progress.update(score: 2)
            cohort_application.user.project_progresses.reload
            expect(CohortApplication::FinalScoreHelper.send(:get_project_score, cohort_application)[0]).to eq(1)

            # marked_as_passed is just as good as having a passing score
            regular_project_progress.update(score: nil, marked_as_passed: true)
            cohort_application.user.project_progresses.reload
            expect(CohortApplication::FinalScoreHelper.send(:get_project_score, cohort_application)[0]).to eq(1)
        end

        it "should work when there are project_progresses and some are waived" do
            setup_cohort_with_projects

            # when the capstone is waived, then it's like 2/3 of the
            # required projects don't exist for this user
            capstone_progress = ProjectProgress.create!(
                user_id: cohort_application.user_id,
                requirement_identifier: capstone_project.requirement_identifier,
                score: nil,
                waived: true
            )
            cohort_application.user.project_progresses.reload
            CohortApplication::FinalScoreHelper.send(:get_project_progress_statuses, cohort_application)

            score, weight = CohortApplication::FinalScoreHelper.send(:get_project_score, cohort_application)
            expect(score).to be_within(0.01).of(0)
            expect(weight).to be_within(0.01).of(0.33)

            # when the regular project is waived, then it's like 1/3 of the
            # required projects don't exist for this user.  Since the user has
            # a passing grade on the only unwaived project, the total project_score is 1
            capstone_progress.update(score: 5, waived: false)
            regular_progress = ProjectProgress.create!(
                user_id: cohort_application.user_id,
                requirement_identifier: regular_project.requirement_identifier,
                score: nil,
                waived: true
            )
            cohort_application.user.project_progresses.reload
            score, weight = CohortApplication::FinalScoreHelper.send(:get_project_score, cohort_application)
            expect(score).to be_within(0.01).of(1)
            expect(weight).to be_within(0.01).of(0.67)

        end

        it "should work when project score is set in user_project_scores" do
            setup_cohort_with_projects
            cohort_application = cohort.cohort_applications.find_by_status('accepted')
            ActiveRecord::Base.connection.execute(%Q~
                insert into user_project_scores (user_id, score) values ('#{cohort_application.user_id}', 0.42);
            ~)
            CohortApplication::FinalScoreHelper.send(:get_project_progress_statuses, cohort_application)
            expect(CohortApplication::FinalScoreHelper.send(:get_project_score, cohort_application)).to eq([0.42, 1])
        end

        it "should work when project score is set in user_project_scores and there is a project_progress with a null score" do
            setup_cohort_with_projects
            cohort_application = cohort.cohort_applications.find_by_status('accepted')
            ActiveRecord::Base.connection.execute(%Q~
                insert into user_project_scores (user_id, score) values ('#{cohort_application.user_id}', 0.42);
            ~)
            capstone_progress = ProjectProgress.create!(
                user_id: cohort_application.user_id,
                requirement_identifier: capstone_project.requirement_identifier,
                score: nil # since the score is nil, this should be ignored and we fallback to user_project_scores
            )
            CohortApplication::FinalScoreHelper.send(:get_project_progress_statuses, cohort_application)
            expect(CohortApplication::FinalScoreHelper.send(:get_project_score, cohort_application)).to eq([0.42, 1])
        end

        def setup_cohort_with_projects
            @cohort_application = CohortApplication.where(status: 'accepted', cohorts: {program_type: 'emba'}).joins(:cohort).first
            @cohort = cohort_application.cohort
            allow(cohort).to receive(:valid?).and_return(true)
            @capstone_project = LearnerProject.create!(title: 'title',requirement_identifier: 'capstone', project_type: LearnerProject::CAPSTONE_PROJECT_TYPE)
            @regular_project = LearnerProject.create!(title: 'title',requirement_identifier: 'regular_project')
            cohort.learner_project_ids = [capstone_project.id, regular_project.id]
            cohort.periods.each { |p| p['learner_project_ids'] = []}
            cohort.publish!
        end

    end
end