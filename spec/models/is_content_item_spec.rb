require 'spec_helper'

describe IsContentItem do
    fixtures :users, :lessons, :lesson_streams, :playlists

    class ContentItem

        def self.has_many(*args)
        end
        def self.has_one(*args)
        end
        def self.before_destroy(*args)
        end
        def self.after_destroy(*args)
        end
        def self.belongs_to(*args)
        end
        def self.after_save(*args)
        end
        def self.after_commit(*args)
        end
        def self.has_and_belongs_to_many(*args)
        end
        def self.validates_presence_of(*args)
        end
        def self.validate(*args)
        end

        attr_accessor :was_published, :last_editor, :updated_at, :modified_at, :author, :archived, :title, :locale, :id, :locale_pack

        include IsContentItem

        def merge_hash(*args)
        end
        def ensure_locale_pack
        end
        def supports_access_groups?
            false
        end

        def errors
            err = Struct.new(:full_messages)
            err.new([])
        end

        def []=(key, val)
            self.send(:"#{key}=", val)
        end
    end

    before(:each) do
        @user = users(:admin)
        @instance = ContentItem.new
    end

    describe "::create_from_hash!" do
        before(:each) do
            @hash = {
                'author' => {'id' => @user.id},
                'title': 'some title',
                'locale': 'en'
            }.with_indifferent_access
            @meta = {}
            expect(ContentItem).to receive(:new).and_return(@instance)
        end

        it "should save in the happy path case" do
            @now = Time.now
            allow(Time).to receive(:now) {
                @now
            }

            expect(@instance).to receive(:entity_metadata_id=)
            expect(@instance).to receive(:author=).with(@user)
            expect(@instance).to receive(:last_editor=).with(@user)
            expect(@instance).to receive(:merge_hash).with(@hash, @meta)
            expect(@instance).to receive(:modified_at=).with(@now)
            expect(@instance).to receive(:save!).and_return(true)

            ContentItem.create_from_hash!(@hash, @meta)

            expect(@instance.title).to eq(@hash['title'])
            expect(@instance.locale).to eq(@hash['locale'])
        end
    end

    describe "::duplicate!" do

        # only lessons get pinned
        it "lessons should set duplicated_to and duplicated_from" do
            klass = Lesson
            user = User.first
            original = klass.first
            duplicate = klass.duplicate!(user, {
                title: "New title"
            }, {
                duplicate_from_id: original.id,
                duplicate_from_updated_at: original.updated_at
            })

            expect(duplicate.versions.last.duplicated_from_id).to eq(original.id)
            expect(duplicate.versions.last.pinned).to be(true)
            expect(duplicate.versions.last.pinned_title).to eq("Duplicated from '#{original.title}'")

            expect(original.versions.last.duplicated_to_id).to eq(duplicate.id)
            expect(original.versions.last.pinned).to be(true)
            expect(original.versions.last.pinned_title).to eq("Duplicated to '#{duplicate.title}'")

            # later versions of each record should not have duplicated_from_id and
            # duplicated_to_id set

            klass.update_from_hash!(user, {id: original.id, updated_at: original.reload.updated_at})
            klass.update_from_hash!(user, {id: duplicate.id, updated_at: duplicate.updated_at})
            expect(duplicate.versions.last.duplicated_from_id).to be_nil
            expect(original.versions.last.duplicated_to_id).to be_nil

        end

        # there is some setup that needs to be done on each class to make this work,
        # so we test each one
        [Lesson, Lesson::Stream].each do |klass|

            describe "#{klass.name}" do

                describe "in_same_locale_pack=true" do

                    it "should work when the original has a locale_pack" do
                        user = User.first
                        original = klass
                            .where(locale: 'en')
                            .where.not(locale_pack_id: nil).first

                        duplicate = klass.duplicate!(user, {
                            title: "New title",
                            locale: 'es'
                        }, {
                            duplicate_from_id: original.id,
                            duplicate_from_updated_at: original.updated_at,
                            in_same_locale_pack: true
                        })

                        expect(duplicate.locale_pack).to eq(original.locale_pack)
                        expect(duplicate.locale).to eq('es')

                        # only lessons get pinned
                        if klass == Lesson
                            expect(original.versions.last.pinned_title).to eq("Duplicated to Spanish")
                            expect(duplicate.versions.last.pinned_title).to eq("Duplicated from English")
                        end
                    end

                    it "should fail when the original is non-English with no locale_pack" do
                        user = User.first
                        original = klass
                            .where(locale: 'es')
                            .where(locale_pack_id: nil).first
                        expect(Proc.new{
                            duplicate = klass.duplicate!(user, {
                                title: "New title",
                                locale: 'zh'
                            }, {
                                duplicate_from_id: original.id,
                                duplicate_from_updated_at: original.updated_at,
                                in_same_locale_pack: true
                            })
                        }).to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Locale pack must include an English item")
                    end
                end

                describe "in_same_locale_pack=false" do

                    it "should work when the original has a locale_pack" do
                        user = User.first
                        original = klass
                            .where(locale: 'en')
                            .where.not(locale_pack_id: nil).first

                        duplicate = klass.duplicate!(user, {
                            title: "New title"
                        }, {
                            duplicate_from_id: original.id,
                            duplicate_from_updated_at: original.updated_at,
                            in_same_locale_pack: false
                        })

                        expect(duplicate.locale_pack).not_to eq(original.locale_pack)
                        expect(duplicate.locale).to eq('en')


                    end
                end
            end
        end

    end

    describe "::update_from_hash!" do

        describe "with no instance in db" do
            it "should raise if there is no record found" do
                expect(ContentItem).to receive(:find_by_id).and_return(nil)
                expect { ContentItem.update_from_hash!(@user, {:id => 'notFound'}) }.to raise_error(
                    ActiveRecord::RecordNotFound,
                    "Couldn't find ContentItem with #{:id}=notFound")
            end

        end

        describe "with instance in db" do

            before(:each) do
                @instance.updated_at = 1.day.ago
                expect(ContentItem).to receive(:find_by_id).and_return(@instance)
            end

            it "should raise if there is no updated_at passed in" do
                expect {
                    ContentItem.update_from_hash!(@user, {})
                }.to raise_error(CrudFromHashHelper::NoDateStampProvided)
            end

            it "should raise if the incoming updated_at is behind the live instance's updated_at" do
                @instance.updated_at = Time.now
                expect {
                    ContentItem.update_from_hash!(@user, {'updated_at' => 10.days.ago})
                }.to raise_error(CrudFromHashHelper::OldVersion)
            end

            it "should save a record in the happy path case" do
                @now = Time.now
                allow(Time).to receive(:now) {
                    @now
                }
                hash = {'updated_at' => Time.now}
                meta = {}.with_indifferent_access
                expect(hash).to receive(:unsafe_with_indifferent_access).and_return(hash) # easiest way to check that this hash gets passed into merge_hash
                expect(@instance).to receive(:save!).and_return(true)
                expect(@instance).to receive(:last_editor=).with(@user)
                expect(@instance).to receive(:modified_at=).with(@now)
                expect(@instance).to receive(:merge_hash).with(hash, meta)
                result = ContentItem.update_from_hash!(@user, hash, meta)
                expect(result).to be(@instance)
            end

            describe "archived" do

                it "should update archived value" do
                    hash = {'updated_at' => Time.now, 'archived' => true}
                    meta = {}
                    expect(@instance).to receive(:archived=).with(true)
                    expect(@instance).to receive(:has_published_version?).and_return(false)
                    expect(@instance).to receive(:save!).and_return(true)
                    result = ContentItem.update_from_hash!(@user, hash, meta)
                    expect(result).to be(@instance)
                end

                it "should raise if trying to unarchive a published lesson without permission" do
                    hash = {'updated_at' => Time.now, 'archived' => true}
                    meta = {}
                    expect(@instance).to receive(:has_published_version?).and_return(true)
                    expect(@instance).not_to receive(:save!)
                    expect_any_instance_of(Ability).to receive(:can?).and_return(false)
                    expect {
                        ContentItem.update_from_hash!(@user, hash)
                    }.to raise_error(ContentPublisher::Publishable::UnauthorizedError)
                end

            end

            describe "publishing" do

                it "should publish if user has ability" do
                    expect_any_instance_of(Ability).to receive(:can?).and_return(true)
                    expect(@instance).to receive(:publish!).with({update_published_at: true})
                    ContentItem.update_from_hash!(@user, {
                        'updated_at' => Time.now,
                    }, {
                        'should_publish' => true
                    })
                end

                it "should unpublish if user has ability" do
                    expect_any_instance_of(Ability).to receive(:can?).and_return(true)
                    expect(@instance).to receive(:save!).and_return(true)
                    expect(@instance).to receive(:unpublish!)
                    ContentItem.update_from_hash!(@user, {
                        'updated_at' => Time.now,
                    }, {
                        'should_unpublish' => true
                    })
                end

                it "should raise if unauthorized user tries to publish" do
                    expect_any_instance_of(Ability).to receive(:can?).and_return(false)
                    expect(@instance).not_to receive(:save!)
                    expect(@instance).not_to receive(:publish!)
                    expect {
                        ContentItem.update_from_hash!(@user, {
                            'updated_at' => Time.now,
                        }, {
                            'should_publish' => true
                        })
                    }.to raise_error(ContentPublisher::Publishable::UnauthorizedError)
                end

                it "should raise if unauthorized user tries to unpublish" do
                    expect_any_instance_of(Ability).to receive(:can?).and_return(false)
                    expect(@instance).not_to receive(:save!)
                    expect(@instance).not_to receive(:publish!)
                    expect {
                        ContentItem.update_from_hash!(@user, {
                            'updated_at' => Time.now,
                        }, {
                            'should_unpublish' => true
                        })
                    }.to raise_error(ContentPublisher::Publishable::UnauthorizedError)
                end
            end

        end

        describe "locale_packs" do

            # there is some setup that needs to be done on each class to make this work,
            # so we test each one
            [Lesson, Lesson::Stream, Playlist].each do |klass|
                describe "#{klass.name}::locale_pack" do
                    attr_accessor :es_item, :en_item, :zh_item, :en_only_item, :es_only_item

                    before(:each) do
                        if klass == Lesson
                            meth = :lessons
                        elsif klass == Lesson::Stream
                            meth = :lesson_streams
                        elsif klass == Playlist
                            meth = :playlists
                        end

                        @es_item, @en_item, @zh_item, @en_only_item, @es_only_item = [
                            send(meth, :es_item),
                            send(meth, :en_item),
                            send(meth, :zh_item),
                            send(meth, :en_only_item),
                            send(meth, :es_only_item)
                        ]

                        # we're going to hit errors if we end up removing a stream
                        # from a required group
                        if klass == Lesson::Stream
                            Playlist.delete_all
                        end
                    end

                    describe "with english content item" do

                        it "should support adding items to this locale_pack" do
                            item = en_only_item
                            locale_pack = item.locale_pack
                            locale_pack_json = locale_pack.as_json

                            # add the es_only_item
                            locale_pack_json['content_items'] << {
                                'id' => es_only_item.id
                            }

                            hash = item.as_json.merge({
                                'locale_pack' => locale_pack_json
                            })
                            result = klass.update_from_hash!(@user, hash, {})

                            expect(item.reload.locale_pack).to eq(locale_pack)
                            expect(es_only_item.reload.locale_pack).to eq(locale_pack)
                            expect(es_only_item.reload.last_editor_id).to eq(@user.id)
                        end

                        it "should support removing items from this locale_pack" do
                            item = en_item
                            es_item.unpublish! # unpublish it so we can remove it
                            locale_pack = item.locale_pack
                            locale_pack_json = locale_pack.as_json

                            # remove the es_item from the locale_pack
                            locale_pack_json['content_items'].delete_if { |content_item|
                                content_item['id'] == es_item.id
                            }

                            hash = item.as_json.merge({
                                'locale_pack' => locale_pack_json
                            })
                            result = klass.update_from_hash!(@user, hash, {})

                            expect(item.reload.locale_pack).to eq(locale_pack)
                            expect(es_item.reload.locale_pack).to be_nil
                            expect(es_item.reload.last_editor_id).to eq(@user.id)
                        end

                        it "should support replacing items in this locale_pack" do
                            item = en_item
                            es_item.unpublish! # unpublish it so we can remove it
                            locale_pack = item.locale_pack
                            locale_pack_json = locale_pack.as_json

                            # remove the es_item from the locale_pack
                            locale_pack_json['content_items'].delete_if { |content_item|
                                content_item['id'] == es_item.id
                            }

                            # add the es_only_item
                            locale_pack_json['content_items'] << {
                                'id' => es_only_item.id
                            }

                            hash = item.as_json.merge({
                                'locale_pack' => locale_pack_json
                            })
                            result = klass.update_from_hash!(@user, hash, {})

                            expect(item.reload.locale_pack).to eq(locale_pack)

                            expect(es_item.reload.locale_pack).to be_nil
                            expect(es_item.reload.last_editor_id).to eq(@user.id)

                            expect(es_only_item.reload.locale_pack).to eq(locale_pack)
                            expect(es_only_item.reload.last_editor_id).to eq(@user.id)
                        end

                        it "should support emptying the locale_pack and changing the locale" do
                            item = en_item
                            es_item.update_attribute(:last_editor_id, User.where.not(id: @user.id).first.id)
                            en_item.locale_pack.content_items.each(&:unpublish!)
                            hash = item.as_json.merge({
                                'locale_pack' => nil,
                                'locale' => 'es'
                            })
                            result = klass.update_from_hash!(@user, hash, {})
                            item.reload
                            expect(item.locale_pack).to be_nil
                            expect(item.locale).to eq('es')

                            es_item.reload
                            expect(es_item.locale_pack).to be_nil

                            # since we had to save the related items, the last_editor should
                            # be updated on those
                            expect(es_item.last_editor_id).to eq(@user.id)
                        end

                    end

                    describe "with non-english content item" do

                        it "should raise an invalidated error if removing this item's locale_pack when this item is published" do
                            item = klass.all_published.where.not(locale: 'en').first

                            hash = item.as_json.merge({
                                'locale_pack' => nil
                            })
                            expect(Proc.new{
                                klass.update_from_hash!(@user, hash, {})
                            }).to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Locale pack must be present if published")

                            expect(item.reload.locale_pack).not_to be_nil

                        end

                        it "should support changing the locale to english" do
                            expect(es_only_item.locale_pack).to be_nil
                            hash = es_only_item.as_json.merge({
                                'locale' => 'en',
                                'locale_pack' => {
                                    'id' => SecureRandom.uuid,
                                    'content_items' => [
                                        {'id' => es_only_item.id}
                                    ]
                                }
                            })
                            klass.update_from_hash!(@user, hash, {})
                            es_only_item.reload
                            expect(es_only_item.locale).to eq('en')
                            expect(es_only_item.locale_pack).not_to be_nil
                        end

                        it "should support changing the locale to non-english" do
                            hash = es_only_item.as_json.merge({
                                'locale' => 'zh'
                            })
                            klass.update_from_hash!(@user, hash, {})
                            es_only_item.reload
                            expect(es_only_item.locale).to eq('zh')
                        end

                        it "should error if locale pack has no english version" do
                            hash = es_only_item.as_json
                            expect(Proc.new{
                                klass.update_from_hash!(@user, hash, {should_publish: true})
                            }).to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Locale pack must include an English item")
                        end
                    end

                end
            end

            describe "groups" do
                it "should update groups" do
                    item = lesson_streams(:published_stream)
                    groups = AccessGroup.limit(3)

                    hash = {
                        'id' => item.id,
                        'updated_at' => Time.now,
                        'locale_pack' => {
                            'id' => item.locale_pack_id,
                            'content_items' => [
                                {'id' => item.id}
                            ],
                            'groups' => groups.as_json
                        }

                    }
                    result = Lesson::Stream.update_from_hash!(@user, hash, {})
                    expect(item.reload.locale_pack.access_groups).to match_array(groups)
                end

                it "should call after_access_group_update after the groups have changed" do
                    institution = Institution.first
                    group = AccessGroup.create!(name: 'new_group_for_testing_refresh')
                    institution.add_to_group('new_group_for_testing_refresh')
                    item = lesson_streams(:published_stream)
                    new_groups = [
                        AccessGroup.find_by_name('SUPERVIEWER'),
                        AccessGroup.find_by_name('new_group_for_testing_refresh')
                    ]

                    orig_group_ids = item.access_groups.pluck(:id)

                    hash = {
                        'id' => item.id,
                        'updated_at' => Time.now,
                        'locale_pack' => {
                            'id' => item.locale_pack_id,
                            'content_items' => [
                                {'id' => item.id}
                            ],
                            'groups' => new_groups.as_json
                        }

                    }

                    expect_any_instance_of(Lesson::Stream).to receive(:after_access_group_update) do |stream, _orig_group_ids|

                        # make sure this is called after the access groups have been updated
                        expect(stream.access_groups).to eq(new_groups)

                        # make sure the original group ids are passed in
                        expect(_orig_group_ids).to eq(orig_group_ids)
                    end
                    result = Lesson::Stream.update_from_hash!(@user, hash, {})
                end

                it "should not refresh content views if not updating groups" do
                    item = lesson_streams(:published_stream)
                    groups = item.locale_pack.access_groups
                    expect(RefreshMaterializedContentViews).not_to receive(:perform_now)

                    hash = {
                        'id' => item.id,
                        'updated_at' => Time.now,
                        'locale_pack' => {
                            'id' => item.locale_pack_id,
                            'content_items' => [
                                {'id' => item.id}
                            ],
                            'groups' => groups.as_json
                        }

                    }
                    result = Lesson::Stream.update_from_hash!(@user, hash, {})
                    expect(item.reload.locale_pack.access_groups).to match_array(groups)
                end
            end
        end

    end


    it "should prune old versions when pinning" do
        @now = Time.now
        allow(Time).to receive(:now) {
            @now
        }
        # It is unreasonable to try to mock out everything
        # that we would need to check if this works against
        # our generic ContentItem class, so I'm just using Lesson
        # as an example
        instance = Lesson.first
        @now = start = Time.now

        instance.pinned = true
        instance.title = 'pinned version 1'
        instance.save!

        @now = @now + 1 # make sure updated_at changes
        instance.pinned = false
        instance.was_published = false
        instance.title = 'unpinned version 1'
        instance.save!

        @now = @now + 1 # make sure updated_at changes
        instance.pinned = false
        instance.title = 'unpinned version 2'
        instance.save!

        expect(instance.versions.where("updated_at >= ?", start).reorder(:updated_at).pluck('title')).to eq([
            'pinned version 1',
            'unpinned version 1',
            'unpinned version 2'
        ])

        @now = @now + 1 # make sure updated_at changes
        instance.pinned = true
        instance.title = 'pinned version 2'
        instance.save!

        expect(instance.versions.where("updated_at >= ?", start).reorder(:updated_at).pluck('title')).to eq([
            'pinned version 1',
            'pinned version 2'
        ])

        @now = @now + 1 # make sure updated_at changes
        instance.pinned = false
        instance.title = 'unpinned version 3'
        instance.save!

        expect(instance.versions.where("updated_at >= ?", start).reorder(:updated_at).pluck('title')).to eq([
            'pinned version 1',
            'pinned version 2',
            'unpinned version 3'
        ])
    end

end