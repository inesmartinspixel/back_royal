# == Schema Information
#
# Table name: lesson_progress
#
#  id                               :uuid             not null, primary key
#  created_at                       :datetime
#  updated_at                       :datetime
#  frame_bookmark_id                :uuid
#  started_at                       :datetime
#  completed_at                     :datetime
#  user_id                          :uuid
#  frame_history                    :json
#  completed_frames                 :json
#  challenge_scores                 :json
#  frame_durations                  :json
#  locale_pack_id                   :uuid             not null
#  best_score                       :float
#  last_checked_for_cert_generation :datetime
#

require 'spec_helper'

describe LessonProgress do

    fixtures(:s3_assets, :lesson_streams, :lessons, :users)

    before(:each) do
        @user = User.left_outer_joins(:lesson_progresses).where("lesson_progress.id is null").first
        @stream = lesson_streams(:published_stream)
        @lessons = @stream.lessons.select(&:has_published_version?)
        expect(@lessons.size > 1).to be(true)
    end

    describe "create_or_update!" do

        it "should create a lesson progress" do
            lesson_progress = LessonProgress.create_or_update!({
                :locale_pack_id => @lessons[0].locale_pack_id,
                :frame_bookmark_id => SecureRandom.uuid,
                :user_id => @user.id
            })

            expect(lesson_progress.locale_pack_id).to eq(@lessons[0].locale_pack_id)
            expect(lesson_progress.user_id).to eq(@user.id)
            expect(lesson_progress.started_at).not_to be_nil
        end

        it "should create a single record for a user doing progress in two locales" do
            en_lesson, es_lesson = [lessons(:en_item), lessons(:es_item)]
            LessonProgress.delete_all
            LessonProgress.create_or_update!({
                :locale_pack_id => en_lesson.locale_pack_id,
                :user_id => @user.id
            })
            LessonProgress.create_or_update!({
                :locale_pack_id => es_lesson.locale_pack_id,
                :user_id => @user.id
            })
            records = LessonProgress.all
            expect(records.length).to eq(1)
            expect(records[0].locale_pack_id).to eq(en_lesson.locale_pack_id)
        end

        it "should update a record if one already exists" do
            # not completing a lesson, should not identify or sync with airtable
            expect_any_instance_of(User).not_to receive(:identify)
            expect_any_instance_of(User).not_to receive(:update_in_airtable)
            lesson_progress = LessonProgress.first
            expect {
                LessonProgress.create_or_update!({
                    :locale_pack_id => lesson_progress.locale_pack_id,
                    :user_id => lesson_progress.user_id
                })
            }.to change {lesson_progress.reload.updated_at }
        end

        it "should store a frame_history, replacing with whatever the client passes in" do
            allow_any_instance_of(Lesson::Content::FrameList::Frame::Componentized).to receive(:valid?).and_return(true)
            frame_id = SecureRandom.uuid
            lesson = lessons(:frame_list_lesson)
            @stream = Lesson::Stream.joins(:lessons).where("lessons.id = '#{lesson.id}'").first

            frame_history1 = [SecureRandom.uuid, SecureRandom.uuid]
            frame_history2 = [SecureRandom.uuid, SecureRandom.uuid]
            LessonProgress.create_or_update!({
                :locale_pack_id => lesson.locale_pack_id,
                :frame_bookmark_id => SecureRandom.uuid,
                :user_id => @user.id,
                :frame_history => frame_history1
            })

            LessonProgress.create_or_update!({
                :locale_pack_id => lesson.locale_pack_id,
                :frame_bookmark_id => SecureRandom.uuid,
                :user_id => @user.id,
                :frame_history => frame_history2
            })

            lp_events = LessonProgress.where({:locale_pack_id => lesson.locale_pack_id, :user_id => @user.id})
            expect(lp_events.length).to eq(1)
            expect(lp_events[0].frame_history).to eq(frame_history2)

            LessonProgress.create_or_update!({
                :locale_pack_id => lesson.locale_pack_id,
                :frame_bookmark_id => SecureRandom.uuid,
                :user_id => @user.id,
                :frame_history => nil
            })

            lp_events = LessonProgress.where({:locale_pack_id => lesson.locale_pack_id, :user_id => @user.id})
            expect(lp_events.length).to eq(1)
            expect(lp_events[0].frame_history).to eq(nil)

        end

        it "should update frame_bookmark_id" do
            id = SecureRandom.uuid
            lesson_progress = LessonProgress.first
            LessonProgress.create_or_update!({
                :locale_pack_id => lesson_progress.locale_pack_id,
                :user_id => lesson_progress.user_id,
                :frame_bookmark_id => id
            })
            expect(lesson_progress.reload.frame_bookmark_id).to eq(id)
        end

        it "should update completed_frames" do
            lesson_progress = LessonProgress.first
            lesson_progress.update!(completed_frames: {
                a: true,
                b: true
            })
            LessonProgress.create_or_update!({
                :locale_pack_id => lesson_progress.locale_pack_id,
                :user_id => lesson_progress.user_id,
                :completed_frames => {
                    b: true,
                    c: true
                }
            })

            # existing and incoming results should be merged
            expect(lesson_progress.reload.completed_frames).to eq({
                a: true,
                b: true,
                c: true
            }.stringify_keys)
        end

        it "should set completed_frames back to an empty hash" do
            lesson_progress = LessonProgress.first
            lesson_progress.update!(completed_frames: {
                a: true,
                b: true
            })
            LessonProgress.create_or_update!({
                :locale_pack_id => lesson_progress.locale_pack_id,
                :user_id => lesson_progress.user_id,
                :completed_frames => {}
            })

            # existing and incoming results should be merged
            expect(lesson_progress.reload.completed_frames).to eq({}.stringify_keys)
        end

        it "should update challenge_scores for a non-test lesson" do
            lesson_progress = LessonProgress.first
            lesson_progress.update!(challenge_scores: {
                a: 1,
                b: 1,
                c: 0
            })
            expect_any_instance_of(LessonProgress).to receive(:for_test_or_assessment_lesson?).at_least(1).times.and_return(false)

            LessonProgress.create_or_update!({
                :locale_pack_id => lesson_progress.locale_pack_id,
                :user_id => lesson_progress.user_id,
                :challenge_scores => {
                    b: 0,
                    c: 1,
                    d: 1
                }
            })

            # for non-test lessons, the last value wins
            expect(lesson_progress.reload.challenge_scores).to eq({
                a: 1,
                b: 0,
                c: 1,
                d: 1
            }.stringify_keys)
        end

        it "should update challenge_scores for a test lesson" do
            lesson_progress = LessonProgress.first
            lesson_progress.update!(challenge_scores: {
                a: 1,
                b: 1,
                c: 0
            })

            expect_any_instance_of(LessonProgress).to receive(:for_test_or_assessment_lesson?).at_least(1).times.and_return(true)
            LessonProgress.create_or_update!({
                :locale_pack_id => lesson_progress.locale_pack_id,
                :user_id => lesson_progress.user_id,
                :challenge_scores => {
                    b: 0,
                    c: 1,
                    d: 1
                }
            })

            # for test lessons, scores can never change, so
            # b and c keep the original values
            expect(lesson_progress.reload.challenge_scores).to eq({
                a: 1,
                b: 1,
                c: 0,
                d: 1
            }.stringify_keys)
        end

        it "should set challenge_scores back to an empty hash" do
            lesson_progress = LessonProgress.first
            lesson_progress.update!(challenge_scores: {
                a: 1,
                b: 1,
                c: 0
            })

            LessonProgress.create_or_update!({
                :locale_pack_id => lesson_progress.locale_pack_id,
                :user_id => lesson_progress.user_id,
                :challenge_scores => {}
            })

            expect(lesson_progress.reload.challenge_scores).to eq({})
        end

        it "should update frame_durations" do
            lesson_progress = LessonProgress.first
            LessonProgress.create_or_update!({
                :locale_pack_id => lesson_progress.locale_pack_id,
                :user_id => lesson_progress.user_id,
                :frame_durations => {'frame' => 1}
            })
            expect(lesson_progress.reload.frame_durations).to eq({'frame' => 1})
        end

        it "should complete a lesson without an application" do
            @user.cohort_applications.delete_all
            expect_any_instance_of(User).to receive(:identify)
            expect_any_instance_of(User).not_to receive(:update_in_airtable) # no pending application, so no update
            lp = LessonProgress.create_or_update!({
                :locale_pack_id => @lessons[0].locale_pack_id,
                :frame_bookmark_id => SecureRandom.uuid,
                :user_id => @user.id,
                :complete => true
            })

            lp = LessonProgress.where({:locale_pack_id => @lessons[0].locale_pack_id, :user_id => @user.id}).first
            expect(lp.locale_pack_id).to eq(@lessons[0].locale_pack_id)
            expect(lp.started_at).not_to be_nil
            # check that the lesson has been completed
            expect(lp.completed_at).not_to be_nil
        end

        it "should complete a lesson with an accepted application" do
            @user = User.left_outer_joins(:cohort_applications).where("cohort_applications.id is null").first
            @user.lesson_progresses.delete_all

            # set up an accepted application
            cohort = Cohort.first
            CohortApplication.create!({
                cohort_id: cohort.id,
                status: 'accepted',
                cohort_slack_room_id: cohort.slack_rooms[0]&.id,
                user_id: @user.id,
                applied_at: Time.now - 5.year
            })
            @user.reload

            expect_any_instance_of(User).to receive(:identify)
            expect_any_instance_of(User).not_to receive(:update_in_airtable) # no pending application, so no update
            lp = LessonProgress.create_or_update!({
                :locale_pack_id => @lessons[0].locale_pack_id,
                :frame_bookmark_id => SecureRandom.uuid,
                :user_id => @user.id,
                :complete => true
            })

            lp = LessonProgress.where({:locale_pack_id => @lessons[0].locale_pack_id, :user_id => @user.id}).first
            expect(lp.locale_pack_id).to eq(@lessons[0].locale_pack_id)
            expect(lp.started_at).not_to be_nil
            # check that the lesson has been completed
            expect(lp.completed_at).not_to be_nil
        end

        it "should complete a lesson with a pending application" do
            @user = User.left_outer_joins(:cohort_applications).where("cohort_applications.id is null").first
            @user.lesson_progresses.delete_all

            # set up pending application
            CohortApplication.create!({
                cohort_id: Cohort.first.id,
                status: 'pending',
                user_id: @user.id,
                applied_at: Time.now - 5.year
            })
            @user.reload

            expect_any_instance_of(User).to receive(:identify)
            expect_any_instance_of(User).to receive(:update_in_airtable)

            lp = LessonProgress.create_or_update!({
                :locale_pack_id => @lessons[0].locale_pack_id,
                :frame_bookmark_id => SecureRandom.uuid,
                :user_id => @user.id,
                :complete => true
            })

            lp = LessonProgress.where({:locale_pack_id => @lessons[0].locale_pack_id, :user_id => @user.id}).first
            expect(lp.locale_pack_id).to eq(@lessons[0].locale_pack_id)
            expect(lp.started_at).not_to be_nil
            # check that the lesson has been completed
            expect(lp.completed_at).not_to be_nil
        end

        it "should raise on a validation failure" do
            expect{LessonProgress.create_or_update!({
                :frame_bookmark_id => SecureRandom.uuid,
                :user_id => @user.id
            })}.to raise_error(ActiveRecord::RecordNotSaved)

            expect{LessonProgress.create_or_update!({
                :locale_pack_id => @lessons[0].locale_pack_id,
                :frame_bookmark_id => SecureRandom.uuid
            })}.to raise_error(ActiveRecord::RecordInvalid)
        end


        it "should raise on attempt to create duplicate" do
            lp = LessonProgress.create_or_update!({
                :locale_pack_id => @lessons[0].locale_pack_id,
                :user_id => @user.id
            })

            # spoof the edge case where sometimes two create_or_updates get kicked off
            # at close to the same time
            expect{LessonProgress.create!({
                :user_id => @user.id,
                :started_at => Time.now(),
                :locale_pack_id => @lessons[0].locale_pack_id
            })}.to raise_error(ActiveRecord::RecordNotUnique)

        end

        it "should save even if lesson is invalid" do
            LessonProgress.destroy_all
            expect_any_instance_of(Lesson).not_to receive(:valid?).and_return(false)
            expect{LessonProgress.create_or_update!({
                :user_id => @user.id,
                :started_at => Time.now(),
                :locale_pack_id => @lessons[0].locale_pack_id
            })}.not_to raise_error
        end

        it "should only set the completed_at the first time a lesson is completed" do
            lesson = Lesson.all_published.first
            LessonProgress.create_or_update!({
                :locale_pack_id => lesson.locale_pack_id,
                :user_id => @user.id,
                :complete => true
            })

            lesson_progress = LessonProgress.where(:locale_pack_id => lesson.locale_pack_id, :user_id => @user.id).first
            orig_completed_at = lesson_progress.completed_at
            LessonProgress.create_or_update!({
                :locale_pack_id => lesson.locale_pack_id,
                :user_id => @user.id,
                :complete => true
            })

            expect(lesson_progress.reload.completed_at).to eq(orig_completed_at)
        end

        it "should raise an invalid error if updating the progress on a completed test lesson" do
            stream = lesson_streams(:exam_stream)
            lesson = stream.lessons[0]
            expect(lesson.test).to be(true)
            expect(Raven).to receive(:capture_exception)
            user = users(:learner)
            LessonProgress.create_or_update!({
                :locale_pack_id => lesson.locale_pack_id,
                :user_id => user.id,
                :complete => true
            })
            expect {
                LessonProgress.create_or_update!({
                    :locale_pack_id => lesson.locale_pack_id,
                    :user_id => user.id,
                    :complete => true
                })
            }.to raise_error(ActiveRecord::RecordInvalid, 'Validation failed: Test lesson is complete')
        end

        describe "best_score" do
            attr_reader :lesson, :user

            before(:each) do
                @lesson = Lesson.all_published.first
                @user = User.left_outer_joins(:lesson_progresses).where("lesson_progress.id is null").first
            end

            it "should update best score if there is none" do
                lesson_progress = LessonProgress.create_or_update!({
                    :locale_pack_id => lesson.locale_pack_id,
                    :user_id => user.id,
                    :best_score => nil
                })
                LessonProgress.create_or_update!({
                    :locale_pack_id => lesson_progress.locale_pack_id,
                    :user_id => lesson_progress.user_id,
                    :best_score => 0.42
                })
                expect(lesson_progress.reload.best_score).to eq(0.42)
            end
            it "should update best score if the new one is better" do
                lesson_progress = LessonProgress.create_or_update!({
                    :locale_pack_id => lesson.locale_pack_id,
                    :user_id => user.id,
                    :best_score => 0.42
                })
                LessonProgress.create_or_update!({
                    :locale_pack_id => lesson_progress.locale_pack_id,
                    :user_id => lesson_progress.user_id,
                    :best_score => 0.84
                })
                expect(lesson_progress.reload.best_score).to eq(0.84)
            end
            it "should not update best score if the new one is worse" do
                lesson_progress = LessonProgress.create_or_update!({
                    :locale_pack_id => lesson.locale_pack_id,
                    :user_id => user.id,
                    :best_score => 0.42
                })
                LessonProgress.create_or_update!({
                    :locale_pack_id => lesson_progress.locale_pack_id,
                    :user_id => lesson_progress.user_id,
                    :best_score => 0.21
                })
                expect(lesson_progress.reload.best_score).to eq(0.42)
            end
            it "should not update best score if there is no score" do
                lesson_progress = LessonProgress.create_or_update!({
                    :locale_pack_id => lesson.locale_pack_id,
                    :user_id => user.id,
                    :best_score => 0.42
                })
                LessonProgress.create_or_update!({
                    :locale_pack_id => lesson_progress.locale_pack_id,
                    :user_id => lesson_progress.user_id
                })
                expect(lesson_progress.reload.best_score).to eq(0.42)
            end
            it "should warn about increasing the score on a test lesson" do
                lesson_progress = LessonProgress.create_or_update!({
                    :locale_pack_id => lesson.locale_pack_id,
                    :user_id => user.id,
                    :best_score => 0.1
                })
                expect_any_instance_of(LessonProgress).to receive(:for_test_lesson?).at_least(1).times.and_return(true)
                expect(Raven).to receive(:capture_exception).with("Unexpected update to best_score.  Could be a hacker",
                    {
                        :extra => {
                            :attack_types=>["increasing best score on test lesson"],
                            :lesson_progress_id=>lesson_progress.id,
                            :locale_pack_id=>lesson_progress.locale_pack_id,
                            :new_best_score=>0.5,
                            :old_best_score=>0.1,
                            :user_id=>lesson_progress.user_id,
                            expected_score: 0.5
                        },
                     :level=>"warning"
                })
                LessonProgress.create_or_update!({
                    :locale_pack_id => lesson_progress.locale_pack_id,
                    :user_id => lesson_progress.user_id,
                    :best_score => 0.5,
                    :challenge_scores => {a: 0, b: 1}
                })
                expect(lesson_progress.reload.best_score).to eq(0.5)
            end
            it "should warn if the score does not match the challenge_scores" do

                lesson_progress = LessonProgress.create_or_update!({
                    :locale_pack_id => lesson.locale_pack_id,
                    :user_id => user.id,
                    :best_score => nil
                })
                expect(Raven).to receive(:capture_exception).with("Unexpected update to best_score.  Could be a hacker",
                    {
                        :extra => {
                            :attack_types=>["challenge scores do not add up to provided score"],
                            :lesson_progress_id=>lesson_progress.id,
                            :locale_pack_id=>lesson_progress.locale_pack_id,
                            :new_best_score=>0.5,
                            :old_best_score=>nil,
                            :user_id=>lesson_progress.user_id,
                            :expected_score => 0.25
                        },
                     :level=>"warning"
                })
                LessonProgress.create_or_update!({
                    :locale_pack_id => lesson_progress.locale_pack_id,
                    :user_id => lesson_progress.user_id,
                    :best_score => 0.5,
                    :challenge_scores => {a: 0, b: 0, c: 0, d: 1}
                })
                expect(lesson_progress.reload.best_score).to eq(0.5)
            end

            # see comment in code
            it "should not warn when resetting challenge scores on an assessment lesson" do
                lesson_progress = LessonProgress.create_or_update!({
                    :locale_pack_id => lesson.locale_pack_id,
                    :user_id => user.id,
                    :best_score => nil,
                    :challenge_scores => {
                        a: 1,
                        b: 0
                    }
                })
                expect_any_instance_of(LessonProgress).to receive(:for_assessment_lesson?).at_least(1).times.and_return(true)
                expect(Raven).not_to receive(:capture_exception)
                LessonProgress.create_or_update!({
                    :locale_pack_id => lesson_progress.locale_pack_id,
                    :user_id => lesson_progress.user_id,
                    :best_score => 0.5,
                    :challenge_scores => {}
                })
                expect(lesson_progress.reload.best_score).to eq(0.5)
            end

            it "should not warn due to best_score precision discrepancies" do
                lesson_progress = LessonProgress.create_or_update!({
                    :locale_pack_id => lesson.locale_pack_id,
                    :user_id => user.id,
                    :best_score => 0.689655172413793,
                    :challenge_scores => {
                        a: 1,
                        b: 0
                    }
                })
                expect(LessonProgress).not_to receive(:warn_on_potential_best_score_hack)
                LessonProgress.create_or_update!({
                    :locale_pack_id => lesson_progress.locale_pack_id,
                    :user_id => lesson_progress.user_id,
                    :best_score => 0.6896551724137931,
                    :challenge_scores => {}
                })
                expect(lesson_progress.reload.best_score).to eq(0.689655172413793)
            end
        end

        describe "official_test_score" do
            attr_reader :lesson, :user

            before(:each) do
                @lesson = Lesson.all_published.first
                @user = User.left_outer_joins(:lesson_progresses).where("lesson_progress.id is null").first
            end

            it "should mirror best_score if it is for a test lesson" do
                lesson_progress = LessonProgress.create_or_update!({
                    :locale_pack_id => lesson.locale_pack_id,
                    :user_id => user.id,
                    :best_score => 0.42,
                })
                allow_any_instance_of(LessonProgress).to receive(:for_test_lesson?) { true }
                expect(lesson_progress.reload.official_test_score).to eq(0.42)
            end

            it "should not mirror best_score if it is not for a test lesson" do
                lesson_progress = LessonProgress.create_or_update!({
                    :locale_pack_id => lesson.locale_pack_id,
                    :user_id => user.id,
                    :best_score => 0.42
                })
                allow_any_instance_of(LessonProgress).to receive(:for_test_lesson?) { false }
                expect(lesson_progress.reload.official_test_score).to be_nil
            end

        end

        it "should not raise an invalid error if updating the progress for an admin on a completed test lesson" do
            stream = lesson_streams(:exam_stream)
            lesson = stream.lessons[0]
            expect(lesson.test).to be(true)
            expect(Raven).not_to receive(:capture_exception)
            user = users(:admin)
            LessonProgress.create_or_update!({
                :locale_pack_id => lesson.locale_pack_id,
                :user_id => user.id,
                :complete => true
            })
            expect {
                LessonProgress.create_or_update!({
                    :locale_pack_id => lesson.locale_pack_id,
                    :user_id => user.id,
                    :complete => true
                })
            }.not_to raise_error
        end

        describe "dupe key error handling" do
            # https://trello.com/c/8MSGodcJ/758-bug-duplicate-key-error-saving-lesson-progress
            it "should recover from ignorable duplicate key error" do
                LessonProgress.destroy_all
                attrs = {
                    :user_id => @user.id,
                    :frame_history => {"a" => "b"},
                    :locale_pack_id => @lessons[0].locale_pack_id
                }

                # In the middle of the save, another process saves one.
                # This can happen if http queue retries a request that already saved
                # successfully on the server, but the lesson_progress table is backed
                # up so the two inserts get queued
                expect(LessonProgress).to receive(:test_hook) {
                    LessonProgress.create!(attrs.merge(:started_at => Time.now))
                    true
                }
                # see https://trello.com/c/lk2wDckw for why we need a transaction here
                LessonProgress.transaction do
                    expect{LessonProgress.create_or_update!(attrs)}.not_to raise_error
                end

                lesson_progress = LessonProgress.first
                expect(lesson_progress.user_id).to eq(attrs[:user_id])
                expect(lesson_progress.locale_pack_id).to eq(attrs[:locale_pack_id])
                expect(lesson_progress.frame_history).to eq(attrs[:frame_history])
            end

            # https://trello.com/c/8MSGodcJ/758-bug-duplicate-key-error-saving-lesson-progress
            it "should raise if non-ignorable duplicate key error" do
                LessonProgress.destroy_all
                attrs = {
                    :user_id => @user.id,
                    :frame_history => {"a" => "b"},
                    :locale_pack_id => @lessons[0].locale_pack_id
                }

                # In the middle of the save, another process saves one.
                # This can happen if http queue retries a request that already saved
                # successfully on the server, but the lesson_progress table is backed
                # up so the two inserts get queued
                expect(LessonProgress).to receive(:test_hook) {
                    LessonProgress.create!(attrs.merge(:frame_history => {"a" => "different"}, :started_at => Time.now))
                    true
                }
                expect{LessonProgress.create_or_update!(attrs)}.to raise_error(ActiveRecord::RecordNotUnique)
            end
        end

        describe "certificate generation" do
            before :each do
                while @stream.lessons.size > 3
                    @stream.remove_lesson!(@stream.lessons.first)
                end
                @lessons = @stream.lessons
                @stream.publish!

                @stream.lessons.map(&:publish!)
                Lesson::StreamProgress.where(user_id: @user.id, locale_pack_id: @stream.locale_pack_id).destroy_all
            end

            it "should not generate certificate if not last lesson" do
                s3_asset1 = s3_assets('certificate_image_1')
                allow(Lesson::StreamProgress).to receive(:generate_certificate_image).and_return(s3_asset1)
                expect(Lesson::StreamProgress).to receive(:generate_certificate_image).exactly(0).times

                # shouldnt pregenerate for anything except last lesson
                LessonProgress.create_or_update!({
                    :locale_pack_id => @lessons[0].locale_pack_id,
                    :user_id => @user.id,
                })
                Lesson::StreamProgress.create_or_update!({
                    :locale_pack_id => @stream.locale_pack_id,
                    :lesson_bookmark_id => @lessons[0].id,
                    :user_id => @user.id
                })

            end

            it "should generate certificate if last lesson" do
                s3_asset1 = s3_assets('certificate_image_1')
                allow(Lesson::StreamProgress).to receive(:generate_certificate_image).and_return(s3_asset1)
                expect(Lesson::StreamProgress).to receive(:generate_certificate_image).exactly(1).times

                # get to last lesson
                LessonProgress.create_or_update!({
                    :locale_pack_id => @lessons[1].locale_pack_id,
                    :user_id => @user.id,
                    :complete => true
                })
                LessonProgress.create_or_update!({
                    :locale_pack_id => @lessons[2].locale_pack_id,
                    :user_id => @user.id,
                    :complete => true
                })

                # should pregenerate for last lesson
                Lesson::StreamProgress.create_or_update!({
                    :locale_pack_id => @stream.locale_pack_id,
                    :lesson_bookmark_id => @lessons[2].id, # last lesson in the stream
                    :user_id => @user.id
                })

                # start last lesson
                LessonProgress.create_or_update!({
                    :locale_pack_id => @lessons[0].locale_pack_id,
                    :user_id => @user.id
                })
            end

            it "shouldn't check if it needs to generate certificates if it just did so" do
                expect_any_instance_of(LessonProgress).to receive(:pregenerate_stream_completion_certificates).exactly(1).times.and_call_original
                LessonProgress.create_or_update!({
                    :locale_pack_id => @lessons[1].locale_pack_id,
                    :user_id => @user.id
                })
                LessonProgress.create_or_update!({
                    :locale_pack_id => @lessons[1].locale_pack_id,
                    :user_id => @user.id
                })
            end

            it "shouldnt regenerate certificate if there is already an existing cert for same day" do
                s3_asset_1 = s3_assets('certificate_image_1')

                stream_progress = Lesson::StreamProgress.first
                stream_progress.certificate_image = s3_asset_1
                stream_progress.save!
                lesson = stream_progress.lesson_stream.lessons.detect { |l| l.locale_pack_id.present? }

                # since its now the next day, should regenerate
                allow_any_instance_of(Lesson::StreamProgress).to receive(:on_last_lesson).and_return(true)
                allow(DateTime).to receive(:now).and_return(s3_asset_1.updated_at + 1.second)
                allow(Time).to receive(:now).and_return(s3_asset_1.updated_at + 1.second)

                s3_asset_2 = s3_assets(:certificate_image_2)
                expect(Lesson::StreamProgress).not_to receive(:generate_certificate_image)
                expect_any_instance_of(S3Asset).not_to receive(:destroy) # the old image should be destroyed (mock this to prevent foreign key issues)

                LessonProgress.create_or_update!({
                    :locale_pack_id => lesson.locale_pack_id,
                    :user_id => stream_progress.user_id
                })
                expect(stream_progress.reload.certificate_image).to eq(s3_asset_1)

            end

            it "should regenerate certificate if there is an existing cert but its too old" do

                s3_asset_1 = s3_assets('certificate_image_1')

                stream_progress = Lesson::StreamProgress.first
                stream_progress.certificate_image = s3_asset_1
                stream_progress.save!
                lesson = stream_progress.lesson_stream.lessons.detect { |l| l.locale_pack_id.present? }

                # since its now the next day, should regenerate
                allow_any_instance_of(Lesson::StreamProgress).to receive(:on_last_lesson).and_return(true)
                allow(DateTime).to receive(:now).and_return(s3_asset_1.updated_at + 1.day)
                allow(Time).to receive(:now).and_return(s3_asset_1.updated_at + 1.day)

                s3_asset_2 = s3_assets(:certificate_image_2)
                expect(Lesson::StreamProgress).to receive(:generate_certificate_image).at_least(1).times.and_return(s3_asset_2)

                # I added this next line because running into issues with multiple stream progresses
                # destroying their s3 assets
                Lesson::StreamProgress.where.not(id: stream_progress.id).destroy_all
                expect_any_instance_of(S3Asset).to receive(:destroy) # the old image should be destroyed (mock this to prevent foreign key issues)

                LessonProgress.create_or_update!({
                    :locale_pack_id => lesson.locale_pack_id,
                    :user_id => stream_progress.user_id
                })
                expect(stream_progress.reload.certificate_image).to eq(s3_asset_2)


            end

        end

    end

    describe "clear_all_progress" do
        it "clears progress for a user" do
            LessonProgress.delete_all
            # progress for this lesson
            LessonProgress.create_or_update!({
                :locale_pack_id => @lessons[0].locale_pack_id,
                :frame_bookmark_id => SecureRandom.uuid,
                :user_id => @user.id
            })

            # progress for another lesson
            LessonProgress.create_or_update!({
                :locale_pack_id => @lessons[1].locale_pack_id,
                :frame_bookmark_id => SecureRandom.uuid,
                :user_id => @user.id
            })

            # progress for another user
            another_user = User.where("id != '#{@user.id}'").first
            LessonProgress.create_or_update!({
                :locale_pack_id => @lessons[0].locale_pack_id,
                :frame_bookmark_id => SecureRandom.uuid,
                :user_id => another_user.id
            })

            expect(LessonProgress.all.length).to eq(3)
            LessonProgress.clear_all_progress(@user.id)
            expect(LessonProgress.all.length).to eq(1)
            LessonProgress.clear_all_progress(another_user.id)
            expect(LessonProgress.all.length).to eq(0)
        end

    end

    describe "as_json" do
        before(:each) do
            @lesson_progress = LessonProgress.where("completed_at is not null").first

        end

        it "should work as an instance method" do
            expect(@lesson_progress.as_json).to eq({
                "user_id"=>@lesson_progress.user_id,
                "updated_at"=>@lesson_progress.updated_at.to_timestamp,
                "created_at"=>@lesson_progress.created_at.to_timestamp,
                "locale_pack_id"=>@lesson_progress.locale_pack_id,
                "frame_bookmark_id"=>@lesson_progress.frame_bookmark_id,
                "frame_history"=>nil,
                "challenge_scores"=>@lesson_progress.challenge_scores,
                "completed_frames"=>@lesson_progress.completed_frames,
                "started_at"=>@lesson_progress.started_at.to_timestamp,
                "completed_at"=>@lesson_progress.completed_at ? @lesson_progress.completed_at.to_timestamp : nil,
                "complete"=>!@lesson_progress.completed_at.nil?,
                "last_progress_at"=>@lesson_progress.last_progress_at.to_timestamp,
                "id"=>@lesson_progress.id,
                "best_score"=>@lesson_progress.best_score,
                "frame_durations"=>@lesson_progress.frame_durations,
                "for_assessment_lesson"=>@lesson_progress.for_assessment_lesson?,
                "for_test_lesson"=>@lesson_progress.for_test_lesson?
            })
        end

        it "should match the results from ToJsonForApiParams" do

            actual = @lesson_progress.as_json.deep_stringify_keys
            expected = Lesson::ToJsonFromApiParams.new({
                user_id: @lesson_progress.user_id,
                filters: {locale_pack_id: @lesson_progress.locale_pack_id},
                fields: ['id', 'lesson_progress']
            }).to_a.first['lesson_progress']

            expect(actual).to eq(expected)

        end

    end

    describe "destroy" do

        it "should log a reset event" do
            lesson_progress = LessonProgress.joins(:user).where('users.pref_locale' => 'en').first
            lesson = lesson_progress.lessons.where(locale: 'en').first.published_version
            expect(SecureRandom).to receive(:uuid).and_return('uuid').at_least(1).times
            expect(Event).to receive(:create_server_event!).with('uuid', lesson_progress.user_id, 'lesson:reset', {
                lesson_id: lesson['id'],
                lesson_title: lesson.title,
                lesson_complete: !!lesson_progress.completed_at,
                lesson_version_id: lesson.version_id
            }).and_call_original
            lesson_progress.destroy!
        end

    end

    describe "update_application_final_score" do

        it "should be called when changing the best_score on a test lesson" do
            lesson_progress = LessonProgress.where(best_score: nil).first
            lesson_progress.lesson.test = true
            lesson_progress.lesson.assessment = false
            lesson_progress.lesson.publish!
            allow(lesson_progress.user).to receive(:application_for_relevant_cohort).and_return(CohortApplication.first)
            expect(lesson_progress.user.application_for_relevant_cohort).to receive(:set_final_score).exactly(2).times.and_call_original
            expect(lesson_progress.user.application_for_relevant_cohort).to receive(:save!).exactly(2).times

            lesson_progress.update(best_score: 0.42)
            lesson_progress.update(best_score: 0.43)
        end

        it "should be called when changing the best_score on an assessment lesson" do
            lesson_progress = LessonProgress.where(best_score: nil).first
            lesson_progress.lesson.test = false
            lesson_progress.lesson.assessment = true
            lesson_progress.lesson.publish!
            application = CohortApplication.first
            allow(lesson_progress.user).to receive(:application_for_relevant_cohort).and_return(CohortApplication.first)
            expect(lesson_progress.user.application_for_relevant_cohort).to receive(:set_final_score).exactly(2).times.and_call_original
            expect(lesson_progress.user.application_for_relevant_cohort).to receive(:save!).exactly(2).times

            lesson_progress.update(best_score: 0.42)
            lesson_progress.update(best_score: 0.43)
        end

        it "should not be called when changing the best_score on a regular lesson" do
            lesson_progress = LessonProgress.where(best_score: nil).first
            lesson_progress.lesson.test = false
            lesson_progress.lesson.assessment = false
            lesson_progress.lesson.publish!
            allow(lesson_progress.user).to receive(:application_for_relevant_cohort).and_return(CohortApplication.first)
            expect(lesson_progress.user.application_for_relevant_cohort).not_to receive(:set_final_score)
            expect(lesson_progress.user.application_for_relevant_cohort).not_to receive(:save!)

            lesson_progress.update(best_score: 0.42)
            lesson_progress.update(best_score: 0.43)
        end

        it "should not be called when not changing the best_score" do
            lesson_progress = LessonProgress.where(best_score: nil).first
            lesson_progress.lesson.test = true
            lesson_progress.lesson.publish!
            allow(lesson_progress.user).to receive(:application_for_relevant_cohort).and_return(CohortApplication.first)
            expect(lesson_progress.user.application_for_relevant_cohort).not_to receive(:set_final_score)
            expect(lesson_progress.user.application_for_relevant_cohort).not_to receive(:save!)

            lesson_progress.update(frame_bookmark_id: SecureRandom.uuid)
            lesson_progress.update(frame_bookmark_id: SecureRandom.uuid)
        end

        it "should be called on destroying a test lesson" do
            lesson_progress = LessonProgress.where(best_score: nil).first
            lesson_progress.lesson.test = true
            lesson_progress.lesson.assessment = false
            lesson_progress.lesson.publish!
            allow(lesson_progress.user).to receive(:application_for_relevant_cohort).and_return(CohortApplication.first)
            expect(lesson_progress.user.application_for_relevant_cohort).to receive(:set_final_score).exactly(1).times.and_call_original
            expect(lesson_progress.user.application_for_relevant_cohort).to receive(:save!).exactly(1).times
            lesson_progress.destroy!
        end

        it "should be called on destroying an assessment lesson" do
            lesson_progress = LessonProgress.where(best_score: nil).first
            lesson_progress.lesson.test = false
            lesson_progress.lesson.assessment = true
            lesson_progress.lesson.publish!
            allow(lesson_progress.user).to receive(:application_for_relevant_cohort).and_return(CohortApplication.first)
            expect(lesson_progress.user.application_for_relevant_cohort).to receive(:set_final_score).exactly(1).times.and_call_original
            expect(lesson_progress.user.application_for_relevant_cohort).to receive(:save!).exactly(1).times
            lesson_progress.destroy!
        end

        it "should not be called on destroying a normal lesson" do

            lesson_progress = LessonProgress.where(best_score: nil).first
            lesson_progress.lesson.test = false
            lesson_progress.lesson.assessment = false
            lesson_progress.lesson.publish!
            allow(lesson_progress.user).to receive(:application_for_relevant_cohort).and_return(CohortApplication.first)
            expect(lesson_progress.user.application_for_relevant_cohort).not_to receive(:set_final_score)
            expect(lesson_progress.user.application_for_relevant_cohort).not_to receive(:save!)

            lesson_progress.destroy!
        end
    end

    describe "for_assessment_lesson" do
        it "should be true when it should be true" do
            assert_for_something_lesson('assessment', true)
        end
        it "should be false when it should be false" do
            assert_for_something_lesson('assessment', false)
        end
    end

    describe "for_test_lesson" do
        it "should be true when it should be true" do
            assert_for_something_lesson('test', true)
        end
        it "should be false when it should be false" do
            assert_for_something_lesson('test', false)
        end
    end

    def assert_for_something_lesson(type, val)
        meth = :"for_#{type}_lesson?"
        lesson = Lesson.all_published.where(type => val).first
        lesson_progress = LessonProgress.create_or_update!({
            :locale_pack_id => lesson.locale_pack_id,
            :frame_bookmark_id => SecureRandom.uuid,
            :user_id => @user.id
        })
        expect(lesson_progress.send(meth)).to eq(val)
    end

end
