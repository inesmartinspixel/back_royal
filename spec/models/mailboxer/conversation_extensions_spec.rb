require 'spec_helper'
require 'mailboxer_spec_helper'

describe Mailboxer::ConversationExtensions do

    include MailboxerSpecHelper

    fixtures(:users, :hiring_relationships)

    before(:each) do
        @user = users(:user_with_multiple_conversations)
        @another_user = User.where("id not in (#{Mailboxer::Receipt.select('receiver_id').to_sql})").first
    end

    describe "create_from_hash!" do

        it "should create a new conversation with an initial message" do
            conversation = nil
            expect {
                conversation = create_conversation(@user, @another_user, CandidatePositionInterest.first)
            }.to change  { Mailboxer::Conversation.count }.by(1)

            expect(conversation).not_to be_nil
            expect(conversation.subject).to eq("new subject")
            new_message = conversation.messages.last
            expect(new_message).not_to be_nil
            expect(new_message.sender).to eq(@user)
            expect(new_message.body).to eq("new message body")
            expect(new_message.metadata).to eq({"some" => "metadata"})
            expect(conversation.new_messages).to eq([new_message])
        end

        it "should set candidate_position_interest_id attribute on conversation" do
            candidate_position_interest = CandidatePositionInterest.first
            conversation = create_conversation(@user, @another_user, candidate_position_interest)
            expect(conversation.candidate_position_interest_id).to eq(candidate_position_interest.id)
        end

        it "should raise if trying to create conversation with no message body" do
            expect {
                Mailboxer::Conversation.create_from_hash!({
                    subject: "new subject",
                    messages: [{
                        "body" => "",
                        "sender_id" => @user.id,
                        "sender_type" => 'user'
                    }],
                    recipients: [
                        {
                            'type' => 'user',
                            'id' => @user.id
                        },
                        {
                            'type' => 'user',
                            'id' => @another_user.id
                        }]
                }, @user)
            }.to raise_error("Message body should not be blank")
        end

        it "should raise if trying to create a conversation with no other participants" do
            expect {
                Mailboxer::Conversation.create_from_hash!({
                    subject: "new subject",
                    messages: [{
                        "body" => "",
                        "sender_id" => @user.id,
                        "sender_type" => 'user'
                    }],
                    recipients: [
                        {
                            'type' => 'user',
                            'id' => @user.id
                        }
                    ]
                }, @user)
            }.to raise_error("We only support a single recipient")
        end

        describe "ensure only one conversation" do
            it "should not call update if this is a new conversation" do
                expect(Mailboxer::Conversation).not_to receive(:update_from_hash!)
                create_conversation(@user, @another_user, CandidatePositionInterest.first)
            end

            it "should instead merge and update if already an existing conversation" do
                expect(Mailboxer::Conversation).to receive(:update_from_hash!).and_call_original
                first_conversation = create_conversation(@another_user, @user, CandidatePositionInterest.first)
                second_conversation = create_conversation(@user, @another_user, CandidatePositionInterest.first)

                expect(first_conversation.id).to eq(second_conversation.id)
                expect(second_conversation.messages.pluck('body')).to eq([first_conversation.messages.first.body, second_conversation.messages.first.body])
            end
        end

        describe "events" do

            it "should log mailboxer:message_received event" do
                # make sure to grab one with no conversation
                hiring_relationship = hiring_relationships(:pending_hidden)
                expect_message_received_event(hiring_relationship)
                conversation = create_conversation(hiring_relationship.hiring_manager, hiring_relationship.candidate, CandidatePositionInterest.first, hiring_relationship)
            end

            it "should log mailboxer:message_sent event" do
                # make sure to grab one with no conversation
                hiring_relationship = hiring_relationships(:pending_hidden)
                expect_message_sent_event(hiring_relationship)
                conversation = create_conversation(hiring_relationship.hiring_manager, hiring_relationship.candidate, CandidatePositionInterest.first, hiring_relationship)
            end

            it "should log conversation_created event" do
                # make sure to grab one with no conversation
                hiring_relationship = hiring_relationships(:pending_hidden)

                mock_sent_or_received_event = "mock_sent_or_received_event"
                allow(mock_sent_or_received_event).to receive(:log_to_external_systems)
                allow_any_instance_of(Mailboxer::Notification).to receive(:log_create_events).with(hiring_relationship).and_return([mock_sent_or_received_event])
                allow_any_instance_of(Mailboxer::Receipt).to receive(:log_create_events).with(hiring_relationship).and_return([mock_sent_or_received_event])
                expect(hiring_relationship).to receive(:just_accepted?).at_least(1).times.and_return('just_accepted')

                mock_event = "mock_event:mailboxer:conversation_created"
                expect(mock_event).not_to receive(:log_to_external_systems)

                # candidate
                expect(Event).to receive(:create_server_event!).exactly(2).times do |id, user_id, event_type, payload|
                    role = if user_id == hiring_relationship.hiring_manager_id
                        "hiring_manager"
                    elsif user_id == hiring_relationship.candidate_id
                        "candidate"
                    else
                        raise "who is this jackass?"
                    end

                    expect(event_type).to eq("mailboxer:conversation_created")

                    expect(payload[:conversation_id]).not_to be_nil
                    expect(payload).to eq(
                        conversation_id: payload[:conversation_id], # we don't know what this will be
                        participant_ids: [hiring_relationship.hiring_manager_id, hiring_relationship.candidate_id] - [user_id],
                        hiring_relationship_id: hiring_relationship.id,
                        hiring_relationship_role: role,
                        just_accepted_relationship: 'just_accepted'
                    )

                    mock_event

                end

                conversation = create_conversation(hiring_relationship.hiring_manager, hiring_relationship.candidate, CandidatePositionInterest.first, hiring_relationship)
            end
        end

        def create_conversation(sender, recipient, candidate_position_interest, hiring_relationship = nil)
            Mailboxer::Conversation.create_from_hash!({
                subject: "new subject",
                candidate_position_interest_id: candidate_position_interest.id,
                messages: [{
                    "body" => "new message body",
                    "sender_id" => sender.id,
                    "sender_type" => 'user',
                    "metadata" => {"some" => "metadata"},
                    "receipts" => [{
                        "receiver_id" => sender.id
                    }]
                }],
                recipients: [
                    {
                        'type' => 'user',
                        'id' => sender.id
                    },
                    {
                        'type' => 'user',
                        'id' => recipient.id
                    }]
            }, sender, hiring_relationship)
        end
    end

    describe "update_from_hash!" do

        it "should raise if trying to send a message from a user other than the allowed ones" do
            conversation = @user.mailbox.inbox.first
            other_recipient = (conversation.recipients - [@user]).first
            orig_count = conversation.messages.count
            hash = conversation.as_json
            hash['messages'] << {
                "body" => "new message body",
                "sender_id" => @user.id,
                "sender_type" => 'user'
            }
            expect(Proc.new {
                Mailboxer::Conversation.update_from_hash!(hash, other_recipient)
            }).to raise_error(Mailboxer::DisallowedError)
        end

        it "should add a message" do
            conversation = @user.mailbox.inbox.first
            orig_count = conversation.messages.count
            candidate_position_interest = CandidatePositionInterest.first
            result = update_with_new_message(conversation, nil)

            reloaded = users(:user_with_multiple_conversations).mailbox.inbox.first
            expect(reloaded.messages.count).to eq(orig_count + 1)
            new_message = reloaded.messages.last
            expect(new_message.sender).to eq(@user)
            expect(new_message.body).to eq("new message body")
            expect(new_message.metadata).to eq({"some" => "metadata"})
            expect(result.new_messages).to eq([new_message])
        end

        it "should set candidate_position_interest_id attribute on conversation" do
            conversation = @user.mailbox.inbox.first
            candidate_position_interest = CandidatePositionInterest.first
            Mailboxer::Conversation.update_from_hash!({
                id: conversation.id,
                candidate_position_interest_id: candidate_position_interest.id
            }, @user)
            expect(conversation.reload.candidate_position_interest_id).to eq(candidate_position_interest.id)
        end

        it "should update is_read on receipt" do
            update_receipt(:is_read?, 'is_read', true)
        end

        it "should update mailbox_type on receipt" do
            update_receipt(:mailbox_type, 'mailbox_type', 'sentbox')
        end

        it "should update trashed on receipt" do
            update_receipt(:trashed?, 'trashed', true)
        end

        it "should add a message from a new participant" do

            conversation = @user.mailbox.inbox.first
            orig_count = conversation.messages.count
            new_participant = User.where("id not in (#{Mailboxer::Receipt.select(:receiver_id).to_sql})").first
            result = update_with_new_message(conversation, new_participant)

            expect(new_participant.mailbox.inbox.reload).to eq([conversation])

            reloaded = users(:user_with_multiple_conversations).mailbox.inbox.first
            expect(reloaded.messages.count).to eq(orig_count + 1)
            new_message = reloaded.messages.last
            expect(new_message.sender).to eq(new_participant)
            expect(new_message.body).to eq("new message body")
            expect(new_message.metadata).to eq({"some" => "metadata"})
            expect(result.new_messages).to eq([new_message])

            # we had a caching issue where this was not getting update in the
            # returned as_json.  See unsetting of original_message in
            # merge_hash
            expect(result.as_json['recipients'].map { |e| e['id']}).to include(new_participant.id)

        end

        describe "events" do

            it "should log mailboxer:message_received event" do
                # make sure to grab one with no conversation
                hiring_relationship = hiring_relationships(:pending_hidden)
                @user = hiring_relationship.hiring_manager
                conversation = create_conversation(hiring_relationship.hiring_manager, hiring_relationship.candidate, CandidatePositionInterest.first, hiring_relationship)

                expect_message_received_event(hiring_relationship)

                update_with_new_message(conversation)
            end

            it "should log mailboxer:message_sent event" do
                # make sure to grab one with no conversation
                hiring_relationship = hiring_relationships(:pending_hidden)
                @user = hiring_relationship.hiring_manager
                conversation = create_conversation(hiring_relationship.hiring_manager, hiring_relationship.candidate, CandidatePositionInterest.first, hiring_relationship)

                expect_message_sent_event(hiring_relationship)

                update_with_new_message(conversation)
            end

            it "should log mailboxer:message_marked_as_read_event" do
                # make sure to grab one with no conversation
                hiring_relationship = hiring_relationships(:pending_hidden)
                @user = hiring_relationship.hiring_manager
                @conversation = create_conversation(hiring_relationship.hiring_manager, hiring_relationship.candidate, CandidatePositionInterest.first, hiring_relationship)
                @user = hiring_relationship.candidate
                @conversation.receipts.each { |r| r.update_attribute('is_read', false) }

                expect_message_marked_as_read_event(hiring_relationship)
                update_receipt(:is_read?, 'is_read', true)
            end

            it "should log expected events when a new user enters a conversation" do
                conversation = @user.mailbox.inbox.first
                original_participant_ids = conversation.participant_ids
                @user = User.where("id not in (#{Mailboxer::Receipt.select(:receiver_id).to_sql})").first
                start = Time.now
                update_with_new_message(conversation)

                events = Event.where("created_at > ?", start)

                # each participant should have received one event
                expect(events.map { |e| [e.event_type, e.user_id]}).to match_array([
                    ['mailboxer:message_received', original_participant_ids[0]],
                    ['mailboxer:message_received', original_participant_ids[1]],
                    ['mailboxer:message_sent', @user.id]
                ])
            end

            it "should log expected events when different roles log messages" do
                hiring_manager = users(:hiring_manager_with_team)
                teammate = users(:hiring_manager_teammate)
                conversation = hiring_manager.mailbox.inbox.first
                candidate = conversation.hiring_relationship.candidate

                # teammate sending message
                start = Time.now
                update_with_new_message(conversation, teammate)
                events = Event.where("created_at > ?", start)

                # each participant should have received one event
                hiring_manager_event = events.detect { |e| e.user_id == hiring_manager.id }
                expect(hiring_manager_event.event_type).to eq('mailboxer:message_received')
                expect(hiring_manager_event.payload['hiring_relationship_role']).to eq('hiring_manager')
                expect(hiring_manager_event.payload['sender_hiring_relationship_role']).to eq('hiring_teammate')

                candidate_event = events.detect { |e| e.user_id == candidate.id }
                expect(candidate_event.event_type).to eq('mailboxer:message_received')
                expect(candidate_event.payload['hiring_relationship_role']).to eq('candidate')
                expect(candidate_event.payload['sender_hiring_relationship_role']).to eq('hiring_teammate')

                hiring_teammate_event = events.detect { |e| e.user_id == teammate.id }
                expect(hiring_teammate_event.event_type).to eq('mailboxer:message_sent')
                expect(hiring_teammate_event.payload['hiring_relationship_role']).to eq('hiring_teammate')

                # candidate sending message
                start = Time.now
                update_with_new_message(conversation, candidate)
                events = Event.where("created_at > ?", start)

                # each participant should have received one event
                hiring_manager_event = events.detect { |e| e.user_id == hiring_manager.id }
                expect(hiring_manager_event.event_type).to eq('mailboxer:message_received')
                expect(hiring_manager_event.payload['hiring_relationship_role']).to eq('hiring_manager')
                expect(hiring_manager_event.payload['sender_hiring_relationship_role']).to eq('candidate')

                candidate_event = events.detect { |e| e.user_id == candidate.id }
                expect(candidate_event.event_type).to eq('mailboxer:message_sent')
                expect(candidate_event.payload['hiring_relationship_role']).to eq('candidate')

                hiring_teammate_event = events.detect { |e| e.user_id == teammate.id }
                expect(hiring_teammate_event.event_type).to eq('mailboxer:message_received')
                expect(hiring_teammate_event.payload['hiring_relationship_role']).to eq('hiring_teammate')
                expect(hiring_teammate_event.payload['sender_hiring_relationship_role']).to eq('candidate')

                # hiring manager sending message
                start = Time.now
                update_with_new_message(conversation, hiring_manager)
                events = Event.where("created_at > ?", start)

                # each participant should have received one event
                hiring_manager_event = events.detect { |e| e.user_id == hiring_manager.id }
                expect(hiring_manager_event.event_type).to eq('mailboxer:message_sent')
                expect(hiring_manager_event.payload['hiring_relationship_role']).to eq('hiring_manager')

                candidate_event = events.detect { |e| e.user_id == candidate.id }
                expect(candidate_event.event_type).to eq('mailboxer:message_received')
                expect(candidate_event.payload['hiring_relationship_role']).to eq('candidate')
                expect(candidate_event.payload['sender_hiring_relationship_role']).to eq('hiring_manager')

                hiring_teammate_event = events.detect { |e| e.user_id == teammate.id }
                expect(hiring_teammate_event.event_type).to eq('mailboxer:message_received')
                expect(hiring_teammate_event.payload['hiring_relationship_role']).to eq('hiring_teammate')
                expect(hiring_teammate_event.payload['sender_hiring_relationship_role']).to eq('hiring_manager')
            end

        end

        def update_with_new_message(conversation, user = nil)
            user ||= @user
            hash = conversation.as_json
            hash['messages'] << {
                "body" => "new message body",
                "sender_id" => user.id,
                "sender_type" => 'user',
                "metadata" => {"some" => "metadata"},
                "receipts" => [{
                    "receiver_id" => user.id,
                    "is_read" => true,
                    "trashed" => false,
                    "deleted" => false,
                    "mailbox_type" => 'sentbox'
                }]
            }
            Mailboxer::Conversation.update_from_hash!(hash, user, conversation.hiring_relationship)
        end

        def update_receipt(prop, key, val)
            conversation = @conversation || @user.mailbox.inbox.first
            receipt = conversation.receipts_for(@user).reject { |r| r.notification.sender_id == @user.id }.first

            hash = conversation.as_json(user_id: @user.id)
            receipt_hash = hash['messages'].map { |m| m['receipts'] }.flatten.detect { |r| r['id'] == receipt.id }

            # sanity check: check that the initial value is different from the target
            expect(receipt.send(prop.to_sym)).not_to eq(val)

            # set the new value in the json
            receipt_hash[key.to_s] = val
            Mailboxer::Conversation.update_from_hash!(hash, @user)

            # reload the receipt and check that the value changed
            receipt = Mailboxer::Receipt.find(receipt.id)
            expect(receipt.send(prop.to_sym)).to eq(val)
        end

    end

    describe "get_conversation_for_sender_and_recipient" do
        it "should return an existing conversation between two users" do
            expect(Mailboxer::Conversation.get_conversation_for_sender_and_recipient(@user, @another_user)).to be_nil
            conversation = create_conversation(@user, @another_user, CandidatePositionInterest.first)
            expect(Mailboxer::Conversation.get_conversation_for_sender_and_recipient(@user, @another_user)).not_to be_nil
            expect(Mailboxer::Conversation.get_conversation_for_sender_and_recipient(@another_user, @user)).not_to be_nil
        end
    end

    describe "last_message_at" do
        it "should work" do
            users = User.limit(2)

            message = users[1].send_message(users[0], "body", "subject").message
            conversation = Mailboxer::Conversation.reorder(:created_at).last
            expect(conversation.last_message_at).to eq(message.updated_at)

            message = users[1].reply_to_conversation(conversation, "reply").message
            expect(conversation.reload.last_message_at).to eq(message.updated_at)
        end
    end

    describe "contains_message_from_user?" do

        it "should return false if no messages" do
            conversation = Mailboxer::Conversation.first
            allow(conversation).to receive(:messages).and_return([])
            expect(conversation.contains_message_from_user?(@user.id)).to be(false)
        end

        it "should return false if none of the messages' sender_id values matches user_id arg" do
            first_message = double("first_message Mailboxer::Message instance double", sender_id: @another_user.id)
            second_message = double("second_message Mailboxer::Message instance double", sender_id: @another_user.id)
            third_message = double("third_message Mailboxer::Message instance double", sender_id: @another_user.id)

            messages = [first_message, second_message, third_message]
            conversation = Mailboxer::Conversation.first

            allow(conversation).to receive(:messages).and_return(messages)
            expect(conversation.contains_message_from_user?(@user.id)).to be(false)
        end

        it "should return true if a messages' sender_id value matches user_id arg" do
            first_message = double("first_message Mailboxer::Message instance double", sender_id: @another_user.id)
            second_message = double("second_message Mailboxer::Message instance double", sender_id: @another_user.id)
            third_message = double("third_message Mailboxer::Message instance double", sender_id: @user.id)

            messages = [first_message, second_message, third_message]
            conversation = Mailboxer::Conversation.first

            allow(conversation).to receive(:messages).and_return(messages)
            expect(conversation.contains_message_from_user?(@user.id)).to be(true)
        end
    end

    def expect_message_received_event(hiring_relationship)
        expect(hiring_relationship).to receive(:just_accepted?).at_least(1).times.and_return('just_accepted')
        mock_conversation_or_notification_event = "mock_conversation_or_notification_event"
        allow(mock_conversation_or_notification_event).to receive(:log_to_external_systems)
        allow_any_instance_of(Mailboxer::Conversation).to receive(:log_create_events).and_return(mock_conversation_or_notification_event)
        allow_any_instance_of(Mailboxer::Notification).to receive(:log_create_events).and_return(mock_conversation_or_notification_event)

        mock_event = "mock_event:mailboxer:message_received"
        expect(mock_event).to receive(:log_to_external_systems)
        expect(Event).to receive(:create_server_event!).with(
            anything,
            hiring_relationship.candidate_id,
            "mailboxer:message_received", {
                receipt_id: anything,
                sender_id: hiring_relationship.hiring_manager_id,
                hiring_relationship_id: hiring_relationship.id,
                hiring_relationship_role: 'candidate',
                sender_hiring_relationship_role: 'hiring_manager',
                sender_avatar_url: hiring_relationship.hiring_manager.avatar_url_with_fallback,
                sender_name: hiring_relationship.hiring_manager.preferred_name,
                sender_company_logo_url: hiring_relationship.hiring_manager.company_logo_url,
                sender_company_name: hiring_relationship.hiring_manager.company_name,
                message: 'new message body',
                just_accepted_relationship: hiring_relationship.just_accepted?,
                hiring_manager_name: hiring_relationship.hiring_manager.preferred_name,
                hiring_manager_company_name: hiring_relationship.hiring_manager.company_name,
                candidate_name: hiring_relationship.candidate.preferred_name
            }
        ).and_return(mock_event)
    end

    def expect_message_sent_event(hiring_relationship)
        expect(hiring_relationship).to receive(:just_accepted?).at_least(1).times.and_return('just_accepted')
        mock_event = "mock_event:mailboxer:message_sent"
        allow_any_instance_of(Mailboxer::Conversation).to receive(:log_create_events)
        allow_any_instance_of(Mailboxer::Receipt).to receive(:log_create_events)

        expect(mock_event).to receive(:log_to_external_systems)
        expect(Event).to receive(:create_server_event!).with(
            anything,
            hiring_relationship.hiring_manager_id,
            "mailboxer:message_sent", {
                notification_id: anything,
                recipient_ids: [hiring_relationship.candidate_id],
                hiring_relationship_id: hiring_relationship.id,
                hiring_relationship_role: 'hiring_manager',
                just_accepted_relationship: hiring_relationship.just_accepted?,
                hiring_manager_name: hiring_relationship.hiring_manager.preferred_name,
                hiring_manager_company_name: hiring_relationship.hiring_manager.company_name,
                candidate_name: hiring_relationship.candidate.preferred_name
            }
        ).and_return(mock_event)
    end

    def expect_message_marked_as_read_event(hiring_relationship)
        mock_event = "mock_event"
        allow_any_instance_of(Mailboxer::Conversation).to receive(:log_create_events)
        allow_any_instance_of(Mailboxer::Receipt).to receive(:log_create_events)
        allow_any_instance_of(Mailboxer::Notification).to receive(:log_create_events)

        expect(mock_event).not_to receive(:log_to_external_systems)
        expect(Event).to receive(:create_server_event!).with(
            anything,
            hiring_relationship.candidate_id,
            "mailboxer:message_marked_as_read", {
                receipt_id: anything,
                sender_id: hiring_relationship.hiring_manager_id,
                hiring_relationship_id: hiring_relationship.id,
                hiring_relationship_role: 'candidate',
                sender_hiring_relationship_role: 'hiring_manager',
                sender_avatar_url: hiring_relationship.hiring_manager.avatar_url_with_fallback,
                sender_name: hiring_relationship.hiring_manager.preferred_name,
                sender_company_logo_url: hiring_relationship.hiring_manager.company_logo_url,
                sender_company_name: hiring_relationship.hiring_manager.company_name,
                hiring_manager_name: hiring_relationship.hiring_manager.preferred_name,
                hiring_manager_company_name: hiring_relationship.hiring_manager.company_name,
                candidate_name: hiring_relationship.candidate.preferred_name
            }
        ).and_return(mock_event)
    end

end