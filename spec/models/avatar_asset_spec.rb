# == Schema Information
#
# Table name: avatar_assets
#
#  id                :uuid             not null, primary key
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  file_file_name    :string
#  file_content_type :string
#  file_file_size    :integer
#  file_updated_at   :datetime
#  file_fingerprint  :string
#  source_url        :text
#  url               :text
#

require 'spec_helper'

describe AvatarAsset do

    before(:each) do
        @file_path = File.join(::Rails.root, "/spec/models", "test_image.jpg")
        file = File.new(@file_path, 'rb')
        allow(file).to receive(:content_type).and_return("image/jpg")

        @attr = {
            :file => file
        }
    end

    it "should create a new instance given a valid attribute" do
        AvatarAsset.new(@attr)
    end

    it "should receive file_file_name from :photo" do
        asset = AvatarAsset.new(@attr)
        expect(asset.file_file_name).to eq("test_image.jpg")
    end

    describe "path" do

        it "should define path as avatar_assets/:self.id/:fingerprint.:created_at.:filename" do
            asset = AvatarAsset.new
            # call asset.path once so that id and created_at get generated and we can
            # determine the expected value
            asset.path
            expected_value = "avatar_assets/#{asset.id}/:fingerprint.#{asset.created_at.to_timestamp}.:filename"
            expect(asset.path).to match(expected_value)
        end

        it "should have an attachment path of :path" do
            expect(AvatarAsset.attachment_definitions[:file][:path]).to eq(:path)
        end

        it "should generate a path from the url" do
            asset = AvatarAsset.new({
                url: "https://uploads.smart.ly/avatar_assets/this/is/where/it/really.is"
            })
            expect(asset.path).to eq("avatar_assets/this/is/where/it/really.is")
        end

    end

    describe "fetch_file_from_url" do

        it "should work" do
            asset = AvatarAsset.new
            url = 'http://path/to/avatar'

            mock_file = double("file")
            expect(mock_file).to receive(:scheme).and_return('http')
            expect(mock_file).to receive(:open).and_return(:opened_file)
            expect(URI).to receive(:parse).with(url).and_return(mock_file)

            expect(asset).to receive(:file=).with(:opened_file)
            asset.fetch_file_from_url(url)
        end

        it "should error if not a url" do
            asset = AvatarAsset.new
            url = '/etc/password'

            mock_file = double("file")
            expect(mock_file).to receive(:scheme).and_return('file')
            expect(URI).to receive(:parse).with(url).and_return(mock_file)

            expect {
                asset.fetch_file_from_url(url)
            }.to raise_error("Disallowed scheme")
        end

    end

end
