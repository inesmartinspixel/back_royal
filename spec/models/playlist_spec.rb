# == Schema Information
#
# Table name: playlists
#
#  id                 :uuid             not null, primary key
#  title              :text             not null
#  description        :text
#  stream_entries     :json             is an Array
#  image_id           :uuid
#  entity_metadata_id :uuid
#  author_id          :uuid
#  last_editor_id     :uuid
#  modified_at        :datetime
#  was_published      :boolean          default(FALSE), not null
#  created_at         :datetime
#  updated_at         :datetime
#  locale_pack_id     :uuid
#  locale             :text             default("en"), not null
#  duplicated_from_id :uuid
#  duplicated_to_id   :uuid
#  tag                :text
#

require 'spec_helper'

describe Playlist do

    fixtures(:lesson_streams, :users, :playlists, :cohorts)

    attr_accessor :playlist

    before(:each) do
        @author = User.first
        @playlist = Playlist.first
    end

    # NOTE: `create_from_hash` and `update_from_hash blocks` are effectivel testing IsContentItem support.
    #       see the `merge_hash` block for unique property handling.

    describe "create_from_hash!" do

        it "should save a playlist" do
            playlist = Playlist.create_from_hash!({
                title: 'title',
                author: User.first
            })
            expect(Playlist.find(playlist.id)).not_to be(nil)
        end

    end

    describe "update_from_hash" do

        it "should update the playlist" do

            playlist = Playlist.first
            updated_at = playlist.updated_at.to_timestamp + 1

            Playlist.update_from_hash!(User.first, {
                id: playlist.id,
                title: 'new title',
                updated_at: updated_at.to_timestamp
            })

            expect(Playlist.find(playlist.id).title).to eq('new title')
        end

    end

    describe "stream_locale_pack_ids_for_playlist" do

        before(:each) do
            SafeCache.clear
        end

        it "should refresh when content views get refreshed" do
            playlist = Playlist.all_published.where(locale: 'en').first

            expect(Playlist::ToJsonFromApiParams).to receive(:new).exactly(2).times.and_call_original

            Playlist.stream_locale_pack_ids_for_playlist(playlist.locale_pack_id, 'en')
            Playlist.stream_locale_pack_ids_for_playlist(playlist.locale_pack_id, 'en')
            RefreshMaterializedContentViews.refresh

            Playlist.stream_locale_pack_ids_for_playlist(playlist.locale_pack_id, 'en')

        end

        it "should respect the locale argument" do
            en_playlist = playlists(:playlist_with_multiple_locales).published_version
            es_playlist = Playlist.all_published.where(locale: 'es', locale_pack_id: playlist.locale_pack_id).first

            stream_locale_pack_id = Lesson::Stream.all_published.where.not(locale_pack_id: es_playlist.stream_locale_pack_ids).first.locale_pack_id
            es_playlist.stream_entries << {'locale_pack_id' => stream_locale_pack_id} # make it different
            es_playlist.publish!

            expect(Playlist.stream_locale_pack_ids_for_playlist(playlist.locale_pack_id, 'en')).to eq(en_playlist.stream_locale_pack_ids)
            expect(Playlist.stream_locale_pack_ids_for_playlist(playlist.locale_pack_id, 'es')).to eq(es_playlist.stream_locale_pack_ids)
        end

    end

    describe "merge_hash" do

        it "should update scalar properties" do

            playlist.merge_hash({
                'description' => 'new description',
                'stream_entries' => {'new' => 'stream_entries'},
                'tag' => 'new tag'
            })
            expect(playlist.description).to eq('new description')
            expect(playlist.stream_entries).to eq({'new' => 'stream_entries'})
            expect(playlist.tag).to eq('new tag')

        end
    end

    describe "raise_error_if_required_by_any_cohorts" do

        # Ideally, we'd assert that this method gets called BEFORE the publish change,
        # but it's kind of hard to make that assertion because you can't really tell
        # exactly when you're before or after the publish change, so we'll just have
        # to settle for asserting that it gets called when published and unpublished
        it "should be called before publish change" do
            expect(playlist).to receive(:raise_error_if_required_by_any_cohorts)
            playlist.publish!
            expect(playlist).to receive(:raise_error_if_required_by_any_cohorts)
            playlist.unpublish!
        end

        it "should be called before_destroy" do
            # a locale pack is needed to create the playlist and we don't want any other playlists with the same
            # locale_pack_id to raise an error, so it's just easier to create a brand new playlist that's the only
            # one that's for the associated locale pack
            locale_pack = Playlist::LocalePack.create!
            playlist = Playlist.create!(title: 'FooBarBaz', locale_pack_id: locale_pack.id, author_id: User.first.id)
            expect(playlist).to receive(:raise_error_if_required_by_any_cohorts)
            playlist.destroy!
        end

        it "should not do anything if was_published" do
            expect(playlist).to receive(:was_published).and_return(true)
            expect {
                playlist.raise_error_if_required_by_any_cohorts
            }.not_to raise_error
        end

        describe "when !was_published" do
            attr_accessor :playlist, :cohort

            before(:each) do
                @cohort = cohorts(:published_mba_cohort)
                playlist_locale_pack_id = cohort.required_playlist_pack_ids.first
                @playlist = Playlist.where(locale_pack_id: playlist_locale_pack_id).first
                allow(@playlist).to receive(:was_published).and_return(false)
            end

            it "should raise an error if the playlist is required by a published cohort" do
                expect {
                    playlist.raise_error_if_required_by_any_cohorts
                }.to raise_error(ActiveRecord::RecordInvalid)
                expect(playlist.errors.full_messages).to include("Playlist #{playlist.title} is required by the published version of cohort #{cohort.name}")
            end

            it "should raise an error if the playlist is required by the working version of the cohort" do
                expect {
                    playlist.raise_error_if_required_by_any_cohorts
                }.to raise_error(ActiveRecord::RecordInvalid)
                expect(playlist.errors.full_messages).to include("Playlist #{playlist.title} is required by the working draft version of cohort #{cohort.name}")
            end

            it "should not raise an error if the playlist is not required by a cohort (published or working version)" do
                locale_pack = Playlist::LocalePack.create! # a locale pack is needed to create the playlist
                playlist = Playlist.create!(title: 'FooBarBaz', locale_pack_id: locale_pack.id, author_id: User.first.id)
                allow(playlist).to receive(:was_published).and_return(false)
                expect {
                    playlist.raise_error_if_required_by_any_cohorts
                }.not_to raise_error
            end
        end
    end

    describe "stream validations" do

        it 'should be valid on publish if everything is ok' do
            stream = lesson_streams(:stream_with_all_groups)
            groups = AccessGroup.all
            playlist.ensure_locale_pack.add_to_group(groups[0].name)
            playlist.stream_entries = [
                {
                    stream_id: stream.id,
                    locale_pack_id: stream.locale_pack_id,
                    description: 'this is a good one'
                }
            ]
            playlist.streams.map(&:publish!)

            playlist.ensure_locale_pack

            assert_valid_on_publish(playlist)
        end

        it 'should be invalid on publish if stream_entries is an empty array' do
            playlist.stream_entries = []
            errors = assert_invalid_on_publish
            expect(errors[:stream_entries]).to eq(["must have at least one entry"])
        end

        it 'should be invalid on save if the playlist has no locale_pack' do
            playlist = Playlist.where(locale: 'en').first
            stream = Lesson::Stream.where(locale: 'es').first
            playlist.stream_entries = [
                {
                    stream_id: stream.id,
                    description: 'this is a good one'
                }
            ]

            expect(playlist.valid?).to eq(false)
            expect(playlist.errors.full_messages).to include("All stream entries have locale packs must be true"), playlist.errors.full_messages.inspect
        end

        it 'should be invalid on save if the playlist has wrong locale_pack' do
            playlist = Playlist.where(locale: 'en').first
            stream = Lesson::Stream.where(locale: 'es').first
            playlist.stream_entries = [
                {
                    stream_id: stream.id,                    description: 'this is a good one',
                    locale_pack_id: SecureRandom.uuid
                }
            ]

            expect(playlist.valid?).to eq(false)
            expect(playlist.errors.full_messages).to include("All stream entries have locale packs must be true"), playlist.errors.full_messages.inspect
        end

        it 'should be invalid on save if stream_entries has no stream_id' do
            playlist.stream_entries = [{
            }]
            expect(playlist.valid?).to eq(false)
            expect(playlist.errors.full_messages).to include("All stream entries include existing streams must be true"), playlist.errors.full_messages.inspect
        end

        it 'should be invalid on save on if stream_entries contains nonexistent stream ids' do
            playlist.stream_entries = [{
                stream_id: SecureRandom.uuid,
                locale_pack_id: SecureRandom.uuid
            }]
            expect(playlist.valid?).to eq(false)
            expect(playlist.errors.full_messages).to include("All stream entries include existing streams must be true"), playlist.errors.full_messages.inspect
        end

        describe 'stream publishing' do
            attr_accessor :en_stream, :es_stream, :zh_stream, :streams
            attr_accessor :en_playlist, :es_playlist, :zh_playlist

            before(:each) do
                @en_stream, @es_stream, @zh_stream = @streams = [
                    lesson_streams(:en_item),
                    lesson_streams(:es_item),
                    lesson_streams(:zh_item)
                ]
                expect(streams.map(&:has_published_version?).uniq).to eq([true]) # sanity check

                @en_playlist, @es_playlist, @zh_playlist = [
                    playlists(:en_item),
                    playlists(:es_item),
                    playlists(:zh_item)
                ]

                {
                    en_playlist => en_stream,
                    es_playlist => es_stream,
                    zh_playlist => zh_stream,
                }.each do |playlist, stream|
                    playlist.stream_entries = [{
                        description: 'adasdasd',
                        locale_pack_id: stream.locale_pack_id,
                        stream_id: stream.id
                    }]
                end

            end

            it 'should be invalid on publish if one of the included streams does not have a version published in its locale or english' do
                # mock out the check during the stream save so I can check that
                # we test during the playlist save
                allow_any_instance_of(Lesson::Stream).to receive(:raise_unless_all_playlists_including_stream_are_valid)
                lesson_streams(:en_item).unpublish!
                lesson_streams(:es_item).unpublish!

                errors = assert_invalid_on_publish(es_playlist)
                expect(errors.full_messages).to include("All required streams are published must be true"), errors.inspect

                errors = assert_invalid_on_publish(en_playlist)
                expect(errors.full_messages).to include("All required streams are published must be true"), errors.inspect
            end

            it 'should be valid on publish if one of the included streams is only published in English' do
                lesson_streams(:es_item).unpublish!
                assert_valid_on_publish(en_playlist)
                assert_valid_on_publish(es_playlist)
            end

            it 'should be valid on publish if one of the included streams is only published in its locale' do
                # mock out the check during the stream save so I can check that
                # we test during the playlist save
                allow_any_instance_of(Lesson::Stream).to receive(:raise_unless_all_playlists_including_stream_are_valid)
                lesson_streams(:en_item).unpublish!
                errors = assert_invalid_on_publish(en_playlist)
                expect(errors.full_messages).to include("All required streams are published must be true"), errors.inspect

                assert_valid_on_publish(es_playlist)
            end

        end

        describe 'as_json' do
            it 'should work with a special character in stream_entries' do
                playlist = Playlist.first

                playlist.stream_entries[0]['description'] = "Ѭ"
                playlist.save!

                reloaded = Playlist.find(playlist.id)
                reloaded.attributes
                expect{ reloaded.as_json }.not_to raise_error
            end

        end


        def assert_valid_on_publish(playlist = nil)
            playlist ||= @playlist
            allow(playlist).to receive(:was_published?).and_return(true)
            expect(playlist.valid?).to eq(true), playlist.errors.full_messages.inspect
        end

        def assert_invalid_on_publish(playlist = nil)
            playlist ||= @playlist
            allow(playlist).to receive(:was_published?).and_return(false)
            expect(playlist.valid?).to be(true), playlist.errors.full_messages.inspect
            allow(playlist).to receive(:was_published?).and_return(true)
            expect(playlist.valid?).to be(false)
            playlist.errors
        end

    end


end
