# == Schema Information
#
# Table name: cohort_applications
#
#  id                                 :uuid             not null, primary key
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  applied_at                         :datetime         not null
#  cohort_id                          :uuid             not null
#  user_id                            :uuid             not null
#  status                             :text             default("pending"), not null
#  accepted_at                        :datetime
#  graduation_status                  :text             default("pending")
#  expelled_at                        :datetime
#  disable_exam_locking               :boolean          default(FALSE)
#  skip_period_expulsion              :boolean          default(FALSE)
#  final_score                        :float
#  completed_at                       :datetime
#  graduated_at                       :datetime
#  total_num_required_stripe_payments :integer
#  registered                         :boolean          default(FALSE), not null
#  retargeted_from_program_type       :text
#  should_invite_to_reapply           :boolean
#  admissions_decision                :text
#  shareable_with_classmates          :boolean
#  notes                              :text
#  scholarship_level                  :json
#  num_charged_payments               :integer
#  num_refunded_payments              :integer
#  airtable_record_id                 :string
#  stripe_plan_id                     :text
#  airtable_base_key                  :string
#  locked_due_to_past_due_payment     :boolean          default(FALSE), not null
#  cohort_slack_room_id               :uuid
#  registered_at                      :datetime
#  registered_early                   :boolean          default(FALSE)
#  allow_early_registration_pricing   :boolean          default(FALSE)
#  stripe_plans                       :json             is an Array
#  scholarship_levels                 :json             is an Array
#  refund_issued_at                   :datetime
#  refund_attempt_failed              :boolean          default(FALSE), not null
#  original_program_type              :text             not null
#  invited_to_interview_at            :datetime
#  project_score                      :float
#  meets_graduation_requirements      :boolean
#  rubric_round_1_tag                 :string
#  rubric_round_1_reason_mba_invite   :string           is an Array
#  rubric_round_1_reason_mba_reject   :string           is an Array
#  rubric_round_1_reason_emba_invite  :string           is an Array
#  rubric_round_1_reason_emba_reject  :string           is an Array
#  rubric_final_decision              :string
#  rubric_final_decision_reason       :string
#  rubric_interviewer                 :string
#  rubric_interview                   :string
#  rubric_interview_scheduled         :boolean
#  rubric_interview_recommendation    :string
#  rubric_reason_for_declining        :string           is an Array
#  rubric_likelihood_to_yield         :string
#  rubric_risks_to_yield              :string           is an Array
#  rejected_after_pre_accepted        :boolean          default(FALSE), not null
#  rubric_inherited                   :boolean          default(FALSE), not null
#  rubric_interview_date              :datetime
#  rubric_motivation_score            :text
#  rubric_contribution_score          :text
#  rubric_insightfulness_score        :text
#  payment_grace_period_end_at        :datetime
#

require 'spec_helper'
require 'stripe_helper'

describe CohortApplication do

    include StripeHelper

    fixtures(:cohorts, :users)

    before(:each) do
        @user = User.joins("LEFT JOIN cohort_applications ON cohort_applications.user_id = users.id")
                        .where("cohort_applications.id is null")
                        .where(can_edit_career_profile: false)
                        .first
        @mba_cohort = cohorts(:published_mba_cohort)
        @emba_cohort = cohorts(:published_emba_cohort)
        @cno_cohort = cohorts(:published_career_network_only_cohort)
        @bc_cohort = cohorts(:published_the_business_certificate_cohort)
    end

    describe "create_from_hash!" do

        it "should work" do
            expect_any_instance_of(CohortApplication).to receive(:merge_hash).and_call_original
            instance = CohortApplication.create_from_hash!(valid_attrs)
            expect(instance.user_id).to eq(valid_attrs['user_id'])
        end

        describe "is_admin" do
            it "should not allow setting extra attributes if is_admin isn't true" do
                expect_any_instance_of(CohortApplication).to receive(:merge_hash).and_call_original
                instance = CohortApplication.create_from_hash!(valid_attrs.merge({graduation_status: 'failed'}))
                expect(instance.graduation_status).to eq('pending')
            end

            it "should allow setting extra attributes if is_admin is true" do
                expect_any_instance_of(CohortApplication).to receive(:merge_hash).and_call_original
                instance = CohortApplication.create_from_hash!(valid_attrs.merge({
                    status: 'accepted',
                    graduation_status: 'failed',
                    cohort_slack_room_id: Cohort.find(valid_attrs[:cohort_id]).slack_rooms[0]&.id
                }), {is_admin: true})
                expect(instance.status).to eq('accepted')
                expect(instance.graduation_status).to eq('failed')
            end

            describe "manual_admin_decision" do
                it "should set admissions_decision to manual_admin_decision if is_admin is true and status is not pending" do
                    expect_any_instance_of(CohortApplication).to receive(:merge_hash).and_call_original
                    instance = CohortApplication.create_from_hash!(valid_attrs.merge({status: 'pre_accepted'}), {is_admin: true})
                    expect(instance.admissions_decision).to eq('manual_admin_decision')
                end

                it "should not set admissions_decision to manual_admin_decision if status is pending" do
                    expect_any_instance_of(CohortApplication).to receive(:merge_hash).and_call_original
                    instance = CohortApplication.create_from_hash!(valid_attrs.merge({status: 'pending'}), {is_admin: true})
                    expect(instance.admissions_decision).not_to eq('manual_admin_decision')
                end
            end
        end
    end

    describe "update_from_hash" do

        it "should work" do
            user = users(:pending_mba_cohort_user)
            instance = user.cohort_applications.first
            new_cohort = Cohort.where.not(id: user.cohort_applications.pluck('cohort_id')).where(stripe_plans: nil).first
            expect_any_instance_of(CohortApplication).to receive(:merge_hash).and_call_original
            CohortApplication.update_from_hash!({
                "id" => instance.id,
                "status" => "accepted",
                "cohort_id" => new_cohort.id
            }, {is_admin: true})
            expect(instance.reload.status).to eq("accepted")
            expect(instance.reload.cohort.name).to eq(new_cohort.name)
        end

        it "should not update the user_id" do
            user = users(:pending_mba_cohort_user)
            instance = user.cohort_applications.first
            another_user = User.where.not(id: user.id).first
            CohortApplication.update_from_hash!({
                "id" => instance.id,
                "user_id" => another_user.id
            }, {is_admin: true})
            expect(instance.reload.user_id).to eq(user.id)
        end

        describe "is_admin" do
            it "should not allow setting extra attributes if is_admin is not true" do
                user = users(:pending_mba_cohort_user)
                instance = user.cohort_applications.first
                expect_any_instance_of(CohortApplication).to receive(:merge_hash).and_call_original
                CohortApplication.update_from_hash!({
                    "id" => instance.id,
                    "graduation_status" => "failed"
                })
                expect(instance.reload.graduation_status).to eq("pending")
            end

            describe "admissions_decision" do
                it "should not set to manual_admin_decision if status change is to 'pending'" do
                    user = users(:accepted_mba_cohort_user)
                    instance = user.last_application
                    instance.status = "pending"
                    instance.admissions_decision = nil
                    instance.save!

                    CohortApplication.update_from_hash!({
                        "id" => instance.id,
                        "status" => "pending"
                    }, {is_admin: true})
                    expect(instance.reload.admissions_decision).to be_nil
                end

                it "should not set to manual_admin_decision if not changing the status" do
                    user = users(:accepted_mba_cohort_user)
                    instance = user.last_application
                    instance.admissions_decision = nil
                    instance.save!

                    CohortApplication.update_from_hash!({
                        "id" => instance.id,
                        "status" => "accepted"
                    }, {is_admin: true})
                    expect(instance.reload.admissions_decision).to be_nil
                end

                it "should set admissions_decision to manual_admin_decision if changing to not-pending" do
                    user = users(:pending_mba_cohort_user)
                    instance = user.last_application
                    instance.admissions_decision = nil
                    instance.save!

                    CohortApplication.update_from_hash!({
                        "id" => instance.id,
                        "status" => "accepted",
                        "cohort_slack_room_id" => instance.cohort.slack_rooms[0]&.id
                    }, {is_admin: true})
                    expect(instance.reload.admissions_decision).to eq("manual_admin_decision")
                end

                it "should set admissions_decision to manual_admin_decision if changing to not-pending and admissions_decision is set" do
                    user = users(:pending_mba_cohort_user)
                    instance = user.last_application
                    instance.admissions_decision = "Invite to Interview"
                    instance.save!

                    CohortApplication.update_from_hash!({
                        "id" => instance.id,
                        "status" => "accepted",
                        "cohort_slack_room_id" => instance.cohort.slack_rooms[0]&.id
                    }, {is_admin: true})
                    expect(instance.reload.admissions_decision).to eq("manual_admin_decision")
                end
            end
        end
    end

    describe "merge_hash" do

        it "should set stuff if is_admin is true" do
            cohort_application = CohortApplication.new(cohort_id: Cohort.first.id)
            now = Time.now.utc
            cohort_application.merge_hash({
                cohort_id: Cohort.first.id,
                status: 'accepted',
                graduation_status: 'honors',
                skip_period_expulsion: true,
                disable_exam_locking: true,
                retargeted_from_program_type: 'program_type',
                locked_due_to_past_due_payment: true,
                payment_grace_period_end_at: now.to_timestamp
            }, {is_admin: true})

            expect(cohort_application.status).to eq('accepted')
            expect(cohort_application.graduation_status).to eq('honors')
            expect(cohort_application.skip_period_expulsion).to be(true)
            expect(cohort_application.disable_exam_locking).to be(true)
            expect(cohort_application.retargeted_from_program_type).to eq('program_type')
            expect(cohort_application.locked_due_to_past_due_payment).to be(true)
            expect(cohort_application.payment_grace_period_end_at).to eq(Time.at(now.to_timestamp))
        end

        it "should use rejected_or_expelled value if is_admin and application status is rejected_or_expelled" do
            cohort_application = CohortApplication.new(cohort_id: Cohort.first.id)

            expect(cohort_application).to receive(:rejected_or_expelled).and_return('expelled')
            cohort_application.merge_hash({
                cohort_id: Cohort.first.id,
                status: 'rejected_or_expelled'
            }, {is_admin: true})
            expect(cohort_application.status).to eq('expelled')

            expect(cohort_application).to receive(:rejected_or_expelled).and_return('rejected')
            cohort_application.merge_hash({
                cohort_id: Cohort.first.id,
                status: 'rejected_or_expelled'
            }, {is_admin: true})
            expect(cohort_application.status).to eq('rejected')
        end

        it "should not allow setting extra params if is_admin is not true" do
            cohort_application = CohortApplication.new(cohort_id: Cohort.first.id)
            cohort_application.merge_hash({
                graduation_status: 'honors',
                skip_period_expulsion: true,
                disable_exam_locking: true
            })
            expect(cohort_application.graduation_status).to eq('pending')
            expect(cohort_application.skip_period_expulsion).not_to be(true)
            expect(cohort_application.disable_exam_locking).not_to eq(true)
        end

        it "should update applied_at if proper meta is supplied" do
            cohort_application = CohortApplication.create_from_hash!(valid_attrs)
            original_applied_at = cohort_application.applied_at = 3.days.ago
            cohort_application.merge_hash({
                status: "rejected"
            }, {is_admin: true});
            expect(cohort_application.status).to eq('rejected')
            expect(cohort_application.applied_at).to eq(original_applied_at)

            cohort_application.merge_hash({}, {update_applied_at: true});
            expect(cohort_application.applied_at).not_to eq(original_applied_at)
        end

        describe "status" do
            it "should allow if not changing the status" do
                application = CohortApplication.where(status: "pending").first
                expect(application.cohort).not_to receive(:user_can_change_status?)
                application.merge_hash({status: "pending"});
                expect(application.status).to eq("pending")
            end

            it "should allow if updating status and cohort is user_can_save_status?" do
                application = CohortApplication.where(status: "pending").first
                expect(application.published_cohort).to receive(:user_can_change_status?).with(application.status, "accepted").and_return(true)
                application.merge_hash({status: "accepted"});
                expect(application.status).to eq("accepted")
            end

            it "should raise UnauthorizedError if updating status and cohort is not user_can_save_status?" do
                application = CohortApplication.where(status: "pending").first
                expect(application.published_cohort).to receive(:user_can_change_status?).and_return(false)

                expect(Proc.new {
                    application.merge_hash({status: "accepted"});
                }).to raise_error(CohortApplication::UnauthorizedError)
            end
        end

        it "should set user_expelled_due_to_period_expulsion_action on the user to true if included in the meta hash" do
            user = users(:accepted_mba_cohort_user)
            application = user.accepted_application.merge_hash({
                status: 'expelled'
            }, {
                user_expelled_due_to_period_expulsion_action: true,
                is_admin: true
            })
            expect(application.user_expelled_due_to_period_expulsion_action).to be(true)
        end

        it "should auto assign a slack room if requested" do

            # find a pre_accepted one so we can unset the slack room without
            # breaking validations
            cohort_application = CohortApplication.joins(cohort: :slack_rooms).where(status: 'pre_accepted').first
            cohort_application.update_columns(cohort_slack_room_id: nil)
            slack_room_id = cohort_application.cohort.slack_rooms.first.id

            expect(Cohort::SlackRoomAssignmentJob).to receive(:assign_one_application).with(cohort_application)

            CohortApplication.update_from_hash!({
                'id' => cohort_application.id,
                'cohort_slack_room_id' => 'AUTO_ASSIGN'
            }, {is_admin: true})
        end

        it "should not auto-assign a slack room if !cohort.supports_slack_rooms? even if requested" do
            # find a pre_accepted one so we can unset the slack room without
            # breaking validations
            cohort_application = CohortApplication.joins(cohort: :slack_rooms).where(status: 'pre_accepted').first
            cohort_application.update_columns(cohort_slack_room_id: nil)
            slack_room_id = cohort_application.cohort.slack_rooms.first.id

            expect(Cohort::SlackRoomAssignmentJob).not_to receive(:assign_one_application)

            CohortApplication.update_from_hash!({
                'id' => cohort_application.id,
                'cohort_slack_room_id' => 'AUTO_ASSIGN',

                # change cohort_id to a cohort where supports_slack_rooms? is false
                'cohort_id' => Cohort.find_by_program_type('the_business_certificate').attributes['id']
            }, {is_admin: true})
            expect(cohort_application.reload.program_type_config.supports_slack_rooms?).to be(false)
        end
    end

    describe "as_json" do

        it "should not include converted_to_emba" do
            user = users(:pending_emba_cohort_user)
            cohort_application = user.cohort_applications.first
            expect(cohort_application).not_to receive(:converted_to_emba?)
            json = cohort_application.as_json
            expect(json.keys).not_to include('converted_to_emba')
        end

        it "should work with zapier" do
            user_with_career_profile = users(:user_with_career_profile)
            json = user_with_career_profile.cohort_applications.last.as_json({zapier: true})
            expect(json["name"]).to eq(user_with_career_profile.name)
        end

        describe "airtable" do

            it "should work" do
                user_with_career_profile = users(:user_with_career_profile)
                cohort_application = user_with_career_profile.cohort_applications.last

                expect(cohort_application).to receive(:converted_to_emba?).and_return(false)

                referrer = User.where.not(id: user_with_career_profile.id).first
                user_with_career_profile.referred_by = User.where.not(id: user_with_career_profile.id).first

                user_with_career_profile.career_profile.resume = S3Asset.first
                user_with_career_profile.career_profile.place_details = {
                    formatted_address: '123 Timbuktoo Street'
                }
                user_with_career_profile.save!
                json = cohort_application.as_json({airtable: true})

                expect(json["name"]).to eq(user_with_career_profile.name)
                expect(json["sufficient_english_work_experience"]).to eq(user_with_career_profile.career_profile.sufficient_english_work_experience)
                expect(json["english_work_experience_description"]).to eq(user_with_career_profile.career_profile.english_work_experience_description)
                expect(json["survey_highest_level_completed_education_description"]).to eq(user_with_career_profile.career_profile.survey_highest_level_completed_education_description)
                expect(json["survey_years_full_time_experience"]).to eq(user_with_career_profile.career_profile.survey_years_full_time_experience)
                expect(json["survey_most_recent_role_description"]).to eq(user_with_career_profile.career_profile.survey_most_recent_role_description)
                expect(json["converted_to_emba"]).to eq(false)

                expect(user_with_career_profile.career_profile.resume.file.url).not_to be_nil
                expect(json["resume_url"]).to eq(user_with_career_profile.career_profile.resume.file.url)

                expect(json["place_details_formatted_address"]).to eq("123 Timbuktoo Street")
            end

            describe "education_experiences" do

                it "should have reserve first 3 education slots for degree programs and last 3 for non-degree program education experiences" do
                    user_with_career_profile = users(:user_with_career_profile)
                    career_profile = user_with_career_profile.career_profile

                    # in the fixtures this user's career profile doesn't have a non-degree program
                    # education experience, so we create one for them here for testing purposes
                    career_profile.education_experiences.build({
                        career_profile_id: career_profile.id,
                        degree_program: false,
                        graduation_year: 2019,
                        major: 'Some Program',
                        will_not_complete: false,
                        educational_organization_option_id: CareerProfile::EducationalOrganizationOption.first.id
                    }).save!
                    degree_program_education_experiences = user_with_career_profile.career_profile.education_experiences.select { |education_experience| education_experience.degree_program }
                    non_degree_program_education_experiences = user_with_career_profile.career_profile.education_experiences.select { |education_experience| !education_experience.degree_program }

                    # verify the number of degree program education experiences and non-degree
                    # program education experiences as this will impact the assertions below
                    expect(degree_program_education_experiences.size).to eq(1)
                    expect(non_degree_program_education_experiences.size).to eq(1)

                    json = user_with_career_profile.cohort_applications.last.as_json({airtable: true})

                    # the first 3 education entries should be reserved for degree program education experiences
                    expect(json['education_1_name']).to eq(degree_program_education_experiences.first.educational_organization.text)
                    expect(json['education_1_graduation_year']).to eq(degree_program_education_experiences.first.graduation_year)
                    expect(json['education_1_degree']).to eq(degree_program_education_experiences.first.degree)
                    expect(json['education_1_major']).to eq(degree_program_education_experiences.first.major)
                    expect(json['education_1_minor']).to eq(degree_program_education_experiences.first.minor)
                    expect(json['education_1_gpa']).to eq(degree_program_education_experiences.first.gpa)
                    expect(json['education_1_gpa_description']).to eq(degree_program_education_experiences.first.gpa_description)
                    expect(json['education_1_degree_program']).to eq(degree_program_education_experiences.first.degree_program)
                    expect(json['education_1_will_not_complete']).to eq(degree_program_education_experiences.first.will_not_complete)

                    # education entries 2 & 3 should have nil valuse for each key since the
                    # user's career profile has only 1 degree program education experience
                    (2..3).each do |i|
                        expect(json["education_#{i}_name"]).to be_nil
                        expect(json["education_#{i}_graduation_year"]).to be_nil
                        expect(json["education_#{i}_degree"]).to be_nil
                        expect(json["education_#{i}_major"]).to be_nil
                        expect(json["education_#{i}_minor"]).to be_nil
                        expect(json["education_#{i}_gpa"]).to be_nil
                        expect(json["education_#{i}_gpa_description"]).to be_nil
                        expect(json["education_#{i}_degree_program"]).to be_nil
                        expect(json["education_#{i}_will_not_complete"]).to be_nil
                    end

                    # the next 3 education entries should be reserved for non-degree program education experiences
                    expect(json['education_4_name']).to eq(non_degree_program_education_experiences.first.educational_organization.text)
                    expect(json['education_4_graduation_year']).to eq(non_degree_program_education_experiences.first.graduation_year)
                    expect(json['education_4_degree']).to eq(non_degree_program_education_experiences.first.degree)
                    expect(json['education_4_major']).to eq(non_degree_program_education_experiences.first.major)
                    expect(json['education_4_minor']).to eq(non_degree_program_education_experiences.first.minor)
                    expect(json['education_4_gpa']).to eq(non_degree_program_education_experiences.first.gpa)
                    expect(json['education_4_gpa_description']).to eq(non_degree_program_education_experiences.first.gpa_description)
                    expect(json['education_4_degree_program']).to eq(non_degree_program_education_experiences.first.degree_program)
                    expect(json['education_4_will_not_complete']).to eq(non_degree_program_education_experiences.first.will_not_complete)

                    # education entries 5 & 6 should have nil valuse for each key since the
                    # user's career profile has only 1 non-degree program education experience
                    (5..6).each do |i|
                        expect(json["education_#{i}_name"]).to be_nil
                        expect(json["education_#{i}_graduation_year"]).to be_nil
                        expect(json["education_#{i}_degree"]).to be_nil
                        expect(json["education_#{i}_major"]).to be_nil
                        expect(json["education_#{i}_minor"]).to be_nil
                        expect(json["education_#{i}_gpa"]).to be_nil
                        expect(json["education_#{i}_gpa_description"]).to be_nil
                        expect(json["education_#{i}_degree_program"]).to be_nil
                        expect(json["education_#{i}_will_not_complete"]).to be_nil
                    end
                end
            end

            describe "work_experiences" do

                it "should reserve the first 2 work slots for full_time work experiences and that last 2 work slots for part_time work experiences" do
                    user_with_career_profile = users(:user_with_career_profile)

                    # in the fixtures this user's career profile doesn't have a part_time
                    # work experience, so we create one for them here for testing purposes
                    CareerProfile::WorkExperience.create!(
                        career_profile_id: user_with_career_profile.career_profile.id,
                        job_title: 'Some Job Title',
                        start_date: Date.new(2016, 10, 1),
                        responsibilities: ['foo', 'bar', 'baz'],
                        professional_organization_option_id: ProfessionalOrganizationOption.first.id,
                        employment_type: 'part_time'
                    )

                    full_time_work_experiences = user_with_career_profile.career_profile.work_experiences.select { |work_experience| work_experience.employment_type == 'full_time' }
                    part_time_work_experiences = user_with_career_profile.career_profile.work_experiences.select { |work_experience| work_experience.employment_type == 'part_time' }

                    # verify the number of full_time work experiences and part_time
                    # work experiences as this will impact the assertions below
                    expect(full_time_work_experiences.size).to eq(1)
                    expect(part_time_work_experiences.size).to eq(1)

                    json = user_with_career_profile.cohort_applications.last.as_json({airtable: true})

                    # the first 2 work entries should be reserved for full_time work experiences
                    expect(json["work_1_name"]).to eq(full_time_work_experiences.first.professional_organization.text)
                    expect(json["work_1_job_title"]).to eq(full_time_work_experiences.first.job_title)
                    expect(json["work_1_industry"]).to eq(full_time_work_experiences.first.industry)
                    expect(json["work_1_role"]).to eq(full_time_work_experiences.first.role)
                    expect(json["work_1_responsibilities"]).to eq(full_time_work_experiences.first.responsibilities.to_sentence)
                    expect(json["work_1_length_string"]).to eq(full_time_work_experiences.first.length_string)
                    expect(json["work_1_employment_type"]).to eq(full_time_work_experiences.first.employment_type)

                    # work entry 2 should have nil valuse for each key since the
                    # user's career profile has only 1 full_time work experience
                    expect(json["work_2_name"]).to be_nil
                    expect(json["work_2_job_title"]).to be_nil
                    expect(json["work_2_industry"]).to be_nil
                    expect(json["work_2_role"]).to be_nil
                    expect(json["work_2_responsibilities"]).to be_nil
                    expect(json["work_2_length_string"]).to be_nil
                    expect(json["work_2_employment_type"]).to be_nil

                    # the next 2 work entries should be reserved for part_time work experiences
                    expect(json["work_3_name"]).to eq(part_time_work_experiences.first.professional_organization.text)
                    expect(json["work_3_job_title"]).to eq(part_time_work_experiences.first.job_title)
                    expect(json["work_3_industry"]).to eq(part_time_work_experiences.first.industry)
                    expect(json["work_3_role"]).to eq(part_time_work_experiences.first.role)
                    expect(json["work_3_responsibilities"]).to eq(part_time_work_experiences.first.responsibilities.to_sentence)
                    expect(json["work_3_length_string"]).to eq(part_time_work_experiences.first.length_string)
                    expect(json["work_3_employment_type"]).to eq(part_time_work_experiences.first.employment_type)

                    # work entry 4 should have nil valuse for each key since the
                    # user's career profile has only 1 part_time work experience
                    expect(json["work_4_name"]).to be_nil
                    expect(json["work_4_job_title"]).to be_nil
                    expect(json["work_4_industry"]).to be_nil
                    expect(json["work_4_role"]).to be_nil
                    expect(json["work_4_responsibilities"]).to be_nil
                    expect(json["work_4_length_string"]).to be_nil
                    expect(json["work_4_employment_type"]).to be_nil
                end
            end

            describe "deferred_and_good_standing" do
                before(:each) do
                    @user = User.left_outer_joins(:cohort_applications).where("cohort_applications.id is null").first
                    @cohorts = Cohort.limit(3)
                    @user.cohort_applications = [
                        CohortApplication.create({
                            status: 'rejected',
                            applied_at: Time.now - 2.years,
                            user: @user,
                            cohort: @cohorts.first
                        }),
                        CohortApplication.create({
                            status: 'deferred',
                            applied_at: Time.now - 1.years,
                            user: @user,
                            cohort: @cohorts.second
                        }),
                        CohortApplication.create({
                            status: 'pending',
                            applied_at: Time.now,
                            user: @user,
                            cohort: @cohorts.third
                        })
                    ]
                    @user.save!
                    @user.reload
                end

                it "should be true if deferred, pending" do
                    @user.cohort_applications.second.update_column(:status, "deferred")
                    @user.cohort_applications.first.update_column(:status, "pending")
                    json = @user.last_application.as_json({airtable: true})
                    expect(json["deferred_and_good_standing"]).to be(true)
                end

                it "should be true if deferred, pre_accepted" do
                    @user.cohort_applications.second.update_column(:status, "deferred")
                    @user.cohort_applications.first.update_column(:status, "pre_accepted")
                    json = @user.last_application.as_json({airtable: true})
                    expect(json["deferred_and_good_standing"]).to be(true)
                end

                it "should be true if deferred, deferred" do
                    @user.cohort_applications.second.update_column(:status, "deferred")
                    @user.cohort_applications.first.update_column(:status, "deferred")
                    json = @user.last_application.as_json({airtable: true})
                    expect(json["deferred_and_good_standing"]).to be(true)
                end

                it "should be false if deferred, expelled, pending" do
                    @user.cohort_applications.third.update_column(:status, "deferred")
                    @user.cohort_applications.second.update_column(:status, "expelled")
                    @user.cohort_applications.first.update_column(:status, "pending")
                    json = @user.last_application.as_json({airtable: true})
                    expect(json["deferred_and_good_standing"]).to be(false)
                end

                it "should be false if deferred, rejected, pending" do
                    @user.cohort_applications.third.update_column(:status, "deferred")
                    @user.cohort_applications.second.update_column(:status, "rejected")
                    @user.cohort_applications.first.update_column(:status, "pending")
                    json = @user.last_application.as_json({airtable: true})
                    expect(json["deferred_and_good_standing"]).to be(false)
                end

                it "should be false if not deferred" do
                    @user.cohort_applications.third.delete
                    @user.cohort_applications.second.delete
                    @user.cohort_applications.first.update_column(:status, "pending")
                    @user.reload
                    json = @user.last_application.as_json({airtable: true})
                    expect(json["deferred_and_good_standing"]).to be_nil
                end
            end

            describe "prior_applications_string" do
                before(:each) do
                    @user = User.left_outer_joins(:cohort_applications).where("cohort_applications.id is null").first
                    @cohorts = Cohort.limit(3)
                    @user.cohort_applications = [
                        CohortApplication.create({
                            status: 'rejected',
                            applied_at: Time.now - 2.years,
                            user: @user,
                            cohort: @cohorts.first
                        }),
                        CohortApplication.create({
                            status: 'deferred',
                            applied_at: Time.now - 1.years,
                            user: @user,
                            cohort: @cohorts.second
                        }),
                        CohortApplication.create({
                            status: 'pending',
                            applied_at: Time.now,
                            user: @user,
                            cohort: @cohorts.third
                        })
                    ]
                    @user.save!
                    @user.reload
                end

                it "should properly create prior_applications_string when syncing most recent application" do
                    json = @user.last_application.as_json({airtable: true})
                    expect(json["prior_applications_string"]).to eq("#{@cohorts.first.name.upcase} - REJECTED, #{@cohorts.second.name.upcase} - DEFERRED")
                end

                # We don't actually sync older ones, but testing this anyway
                it "should properly create prior_applications_string when syncing older application" do
                    json = @user.cohort_applications.second.as_json({airtable: true})
                    expect(json["prior_applications_string"]).to eq("#{@cohorts.first.name.upcase} - REJECTED")
                end
            end
        end

        describe "notes" do
            it "should only serialize notes if an admin" do
                user_with_career_profile = users(:user_with_career_profile)
                application = user_with_career_profile.cohort_applications.last

                json = application.as_json
                expect(json).not_to have_key("notes")

                json = application.as_json({is_admin: true})
                expect(json).to have_key("notes")
                expect(json["notes"]).to eq(application.notes)
            end
        end

        describe "invited_to_interview_url" do

            it "should work" do
                user = users(:pending_emba_cohort_user)
                cohort_application = user.cohort_applications.first
                expect(cohort_application.user).to receive(:timezone_with_fallback).and_return('Some/Timezone')
                expect(cohort_application.user).to receive(:country).and_return('SomeCountry')

                json = cohort_application.as_json
                expect(json.keys).to include('invited_to_interview_url')
                expect(json["invited_to_interview_url"]).to eq("https://quantic.edu/interview?email=#{user.email}&name=#{user.name}&tz=Some/Timezone&a1=SomeCountry")
            end

            it "should fallback to US if user has no country set" do
                user = users(:pending_emba_cohort_user)
                cohort_application = user.cohort_applications.first
                expect(cohort_application.user).to receive(:timezone_with_fallback).and_return('Some/Timezone')
                expect(cohort_application.user).to receive(:country).and_return(nil)

                json = cohort_application.as_json
                expect(json.keys).to include('invited_to_interview_url')
                expect(json["invited_to_interview_url"]).to eq("https://quantic.edu/interview?email=#{user.email}&name=#{user.name}&tz=Some/Timezone&a1=US")
            end
        end
    end

    describe "validations" do

        it "requires an user, cohort, and applied_at" do

            expect(Proc.new {
                CohortApplication.create!({})
            }).to raise_error(ActiveRecord::RecordNotFound)

            expect(Proc.new {
                CohortApplication.create!({user: @user})
            }).to raise_error(ActiveRecord::RecordNotFound)

            expect(Proc.new {
                CohortApplication.create!({user: @user, cohort: @mba_cohort})
            }).to raise_error(ActiveRecord::StatementInvalid)

            expect(Proc.new {
                CohortApplication.create!({applied_at: Time.now, user: @user, cohort: @mba_cohort})
            }).not_to raise_error
        end

        it "requires a valid status" do
            cohort_application = CohortApplication.create({status: 'invalid', cohort_slack_room_id: @mba_cohort.slack_rooms[0]&.id, applied_at: Time.now, user: @user, cohort: @mba_cohort})
            expect(cohort_application.valid?).to be(false)
            cohort_application.status = 'pending'
            expect(cohort_application.valid?).to be(true)
            cohort_application.status = 'rejected'
            expect(cohort_application.valid?).to be(true)
            cohort_application.status = 'accepted'
            expect(cohort_application.valid?).to be(true)
            cohort_application.status = 'deferred'
            expect(cohort_application.valid?).to be(true)
            cohort_application.status = 'pre_accepted'
            expect(cohort_application.valid?).to be(true)
        end

        it "requires a cohort_slack_room_id only if accepted with slack rooms" do
            expect(@mba_cohort.slack_rooms).not_to be_empty
            cohort_application = CohortApplication.create({status: 'pre_accepted', applied_at: Time.now, user: @user, cohort: @mba_cohort})
            expect(cohort_application.valid?).to be(true)
            cohort_application.status = 'accepted'
            expect(cohort_application.valid?).to be(false)

            cohort_application = CohortApplication.left_outer_joins(cohort: :slack_rooms).where("cohort_slack_rooms.id is null").first
            cohort_application.status = 'accepted'
            expect(cohort_application.valid?).to be(true)
        end

        describe "graduation_status" do
            it "requires a pending graduation_status if status is not accepted" do
                cohort_application = CohortApplication.create({status: 'pending', applied_at: Time.now, user: @user, cohort: @mba_cohort})
                cohort_application.graduation_status = 'graduated'
                expect(cohort_application.valid?).to be(false)
                expect(cohort_application.errors.full_messages).to eq(["Graduation status must be pending if status is not accepted (unless biz cert)"])

                cohort_application.graduation_status = 'pending'
                expect(cohort_application.valid?).to be(true)
            end

            it "allows a rejected biz cert application to be graduated" do
                cohort_application = CohortApplication.create({
                    status: 'pending',
                    applied_at: Time.now,
                    user: @user,
                    cohort_id: Cohort.promoted_cohort('the_business_certificate')['id']
                })
                cohort_application.graduation_status = 'graduated'
                expect(cohort_application.valid?).to be(false)
                expect(cohort_application.errors.full_messages).to eq(["Graduation status must be pending if status is not accepted (unless biz cert)"])

                cohort_application.status = 'rejected'
                expect(cohort_application.valid?).to be(true)
            end
        end
    end

    describe "before_save" do

        it "should execute on save" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:before_save)
            cohort_application.save!
        end

        it "should ensure_career_profile on the user" do
            cohort_application = CohortApplication.first
            expect(cohort_application.user).to receive(:ensure_career_profile)

            cohort_application.before_save
        end

        it "should set expelled_at to nil if status is not 'expelled'" do
            cohort_application = users(:expelled_mba_cohort_user).cohort_applications.first
            expect(cohort_application.expelled_at).not_to be(nil)

            cohort_application.status = 'deferred'
            cohort_application.before_save

            expect(cohort_application.expelled_at).to be(nil)
        end

        it "should set accepted_at to nil if status is not 'accepted'" do
            cohort_application = users(:accepted_mba_cohort_user).cohort_applications.first
            expect(cohort_application.accepted_at).not_to be(nil)

            cohort_application.status = 'deferred'
            cohort_application.before_save

            expect(cohort_application.accepted_at).to be(nil)
        end

        it "should default stripe coupon information if not already set" do
            user = users(:accepted_mba_cohort_user)
            expect(@emba_cohort.stripe_plans).not_to be_nil # sanity
            application = user.accepted_application.merge_hash({
                cohort_id: @emba_cohort.id
            }, {
                is_admin: true
            })
            application.valid? # need to run before_validation callbacks to get stripe_plans set up
            application.before_save

            applicable_coupon_info = application.applicable_coupon_info
            expect(applicable_coupon_info["id"]).to eq("none")
            expect(applicable_coupon_info["amount_off"]).to eq(0)
            expect(applicable_coupon_info["percent_off"]).to eq(0)
        end

        describe "when status_changed?" do
            attr_accessor :cohort_application

            describe "to 'expelled'" do

                before(:each) do
                    @cohort_application = users(:accepted_mba_cohort_user).cohort_applications.first
                    @cohort_application.status = 'expelled'
                end

                it "should set expelled_at" do
                    expect(cohort_application.expelled_at).to be(nil)
                    cohort_application.before_save
                    expect(cohort_application.expelled_at).not_to be(nil)
                end

                it "should set interested_in_joining_new_company on user's career_profile to 'not_interested'" do
                    expect(cohort_application.user.career_profile.interested_in_joining_new_company).not_to eq('not_interested')
                    cohort_application.before_save
                    expect(cohort_application.user.career_profile.interested_in_joining_new_company).to eq('not_interested')
                end
            end

            describe "to 'accepted'" do

                before(:each) do
                    @cohort_application = users(:pending_mba_cohort_user).cohort_applications.first
                    @cohort_application.status = 'accepted'

                    expect(cohort_application.user.can_edit_career_profile).to be(false)
                end

                it "should set accepted_at" do
                    expect(cohort_application.accepted_at).to be(nil)
                    cohort_application.before_save
                    expect(cohort_application.accepted_at).not_to be(nil)
                end

                describe "should set active playlist to the first concentration based on program_type_config" do
                    attr_accessor :playlist_locale_pack

                    before(:each) do
                        @playlist_locale_pack = Playlist.first.locale_pack
                        @cohort_application.user.update_attribute(:active_playlist_locale_pack_id, nil)
                    end

                    it "when program_type_config.supports_auto_active_playlist_selection_on_acceptance?" do
                        allow(cohort_application.program_type_config).to receive(:supports_auto_active_playlist_selection_on_acceptance?).and_return(true)

                        expect(cohort_application.published_cohort).to receive(:get_first_concentration_playlist_locale_pack).and_return(true)
                        expect(cohort_application.user).to receive(:is_first_concentration_playlist_complete?).and_return(false)
                        expect(cohort_application.published_cohort).to receive(:get_first_concentration_playlist_locale_pack).and_return(playlist_locale_pack)

                        cohort_application.before_save
                        expect(cohort_application.user.active_playlist_locale_pack_id).to eq(playlist_locale_pack.id)
                    end

                    it "when !program_type_config.supports_auto_active_playlist_selection_on_acceptance?" do
                        allow(cohort_application.program_type_config).to receive(:supports_auto_active_playlist_selection_on_acceptance?).and_return(false)

                        expect(cohort_application.published_cohort).not_to receive(:get_first_concentration_playlist_locale_pack)
                        expect(cohort_application.user).not_to receive(:is_first_concentration_playlist_complete?)
                        expect(cohort_application.published_cohort).not_to receive(:get_first_concentration_playlist_locale_pack)

                        cohort_application.before_save
                        expect(cohort_application.user.active_playlist_locale_pack_id).to be(nil)
                    end

                end

                it "should enqueue SetCanEditCareerProfileAfterAcceptanceJob delayed_job for two weeks from now when program_type_config.supports_delayed_career_network_access_on_acceptance?" do
                    allow(cohort_application.program_type_config).to receive(:supports_delayed_career_network_access_on_acceptance?).and_return(true)

                    delayed_job_instance_mock = double("SetCanEditCareerProfileAfterAcceptanceJob mock")
                    expect(delayed_job_instance_mock).to receive(:perform_later).with(cohort_application.user_id)
                    expect(SetCanEditCareerProfileAfterAcceptanceJob).to receive(:set).with(wait: 14.days).and_return(delayed_job_instance_mock)

                    cohort_application.before_save

                    # the responsibility of changing the can_edit_career_profile attr belongs to the SetCanEditCareerProfileAfterAcceptanceJob
                    # delayed job, so this shouldn't have changed yet.
                    expect(cohort_application.user.can_edit_career_profile).to be(false)
                end

                it "should set the can_edit_career_profile to true when program_type_config.supports_career_network_access_on_acceptance?" do
                    allow(cohort_application.program_type_config).to receive(:supports_career_network_access_on_acceptance?).and_return(true)

                    cohort_application.before_save

                    expect(cohort_application.user.can_edit_career_profile).to be(true)
                end

                it "should not set can_edit_career_profile or enqueue a job" do
                    allow(cohort_application.program_type_config).to receive(:supports_delayed_career_network_access_on_acceptance?).and_return(false)
                    allow(cohort_application.program_type_config).to receive(:supports_career_network_access_on_acceptance?).and_return(false)

                    delayed_job_instance_mock = double("SetCanEditCareerProfileAfterAcceptanceJob mock")
                    expect(delayed_job_instance_mock).not_to receive(:perform_later)
                    expect(SetCanEditCareerProfileAfterAcceptanceJob).not_to receive(:set)

                    cohort_application.before_save

                    expect(cohort_application.user.can_edit_career_profile).to be(false)
                end

                it "should call ensure_slack_room_assignment_job" do
                    expect(cohort_application.published_cohort).to receive(:ensure_slack_room_assignment_job)
                    cohort_application.before_save
                end
            end

            describe "to 'pre_accepted'" do

                before(:each) do
                    @cohort_application = users(:pending_mba_cohort_user).cohort_applications.first
                    @cohort_application.status = 'pre_accepted'
                end

                it "should call ensure_slack_room_assignment_job" do
                    expect(cohort_application.published_cohort).to receive(:ensure_slack_room_assignment_job)
                    cohort_application.before_save
                end
            end
        end

        describe "when graduation_status_changed?" do
            before(:each) do
                @now = Time.now
                allow(Time).to receive(:now).and_return(@now)
                @cohort_application = CohortApplication.where(graduation_status: 'pending').first
            end

            it "should set graduated_at when graduated" do
                @cohort_application.graduation_status = 'graduated'
                @cohort_application.before_save
                @cohort_application.graduated_at = @now
                expect(@cohort_application.graduated_at).to eq(@now)
            end

            it "should set graduated_at when honors" do
                @cohort_application.graduation_status = 'honors'
                @cohort_application.before_save
                @cohort_application.graduated_at = @now
                expect(@cohort_application.graduated_at).to eq(@now)
            end

            it "should not set graduated_at if not graduated or honors" do
                @cohort_application.graduation_status = 'failed'
                @cohort_application.before_save
                expect(@cohort_application.graduated_at).to be_nil
            end
        end

        describe "registered change" do
            it "should do nothing when registered is not changing" do
                application = CohortApplication.first
                expect(application.published_cohort).not_to receive(:ensure_slack_room_assignment_job)
                now = Time.now
                application.update_columns({
                    registered: true,
                    registered_at: now,
                    disable_exam_locking: false
                })
                application.update_attribute(:disable_exam_locking, true)
                expect(application.registered_at).to eq(now)
            end

            it "should set to now when registered changing to true" do
                application = CohortApplication.first
                expect(application.published_cohort).to receive(:ensure_slack_room_assignment_job)
                application.update_columns({
                    registered: false,
                    registered_at: nil
                })
                now = Time.now
                allow(Time).to receive(:now).and_return(now)
                application.update_attribute(:registered, true)
                expect(application.registered_at).to eq(now)
            end

            it "should set to registered_early to true when changing to true and deadline is applicable" do
                application = CohortApplication.first
                application.update_columns({
                    registered: false,
                    registered_at: nil
                })
                now = Time.now
                expect(application).to receive(:applicable_registration_period).and_return("early")
                allow(Time).to receive(:now).and_return(now)
                application.update_attribute(:registered, true)
                expect(application.registered_at).to eq(now)
                expect(application.registered_early).to be(true)
            end

            it "should set to registered_early to false when changing to true and deadline is not applicable" do
                application = CohortApplication.first
                application.update_columns({
                    registered: false,
                    registered_at: nil
                })
                now = Time.now
                expect(application).to receive(:applicable_registration_period).and_return("standard")
                allow(Time).to receive(:now).and_return(now)
                application.update_attribute(:registered, true)
                expect(application.registered_at).to eq(now)
                expect(application.registered_early).to be(false)
            end

            it "should set to nil when registered changing to false" do
                application = CohortApplication.first
                expect(application.published_cohort).to receive(:ensure_slack_room_assignment_job)
                application.update_columns({
                    registered: true,
                    registered_at: Time.now
                })
                application.update_attribute(:registered, false)
                expect(application.registered_at).to be_nil
            end
        end
    end

    describe "create_status_changed_event after_save callback" do

        before(:each) do
            # turn these events off so we can check the ones we care about
            allow_any_instance_of(CohortApplication).to receive(:create_retargeted_changed_event)
        end

        it "should set expected attrs" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:status_changed_info)
                .with(cohort_application.published_cohort.enrollment_deadline, cohort_application.status.dup)
                .and_return({status_changed_info: "was set"})

            assert_event_logged do |id, user_id, event_type, attrs|

                expect(event_type).to eq("cohort:changed")
                expect(attrs[:cohort_id]).to eq(cohort_application.cohort_id)
                expect(attrs[:cohort_title]).to eq(cohort_application.published_cohort.title)
                expect(attrs[:program_type]).to eq(cohort_application.published_cohort.program_type)
                expect(attrs[:status_changed_info]).to eq("was set")

            end

            cohort_application.update_attribute(:status, "changed")
        end

        describe "status_changed_info" do
            it "should send the previous status when the application is being updated to a new status" do
                user = User.first
                user.cohort_applications.delete_all
                cohort_application = CohortApplication.create!({
                    user_id: user.id,
                    cohort_id: Cohort.first.id,
                    applied_at: Time.now
                })
                expect(cohort_application).to receive(:status_changed_info).with(anything, 'pending').and_call_original
                cohort_application.update_attribute(:status, 'pre_accepted')
            end

            it "should send nil as the previous status when the application was just created" do
                user = User.first
                user.cohort_applications.delete_all
                cohort_application = CohortApplication.new({
                    user_id: user.id,
                    cohort_id: Cohort.first.id,
                    status: "pre_accepted",
                    applied_at: Time.now
                })
                expect(cohort_application).to receive(:status_changed_info).with(anything, nil).and_call_original
                cohort_application.save!
            end
        end

        describe "cohort:rejected event" do

            it "should set relevant email" do
                cohort_application = CohortApplication.where(
                    status: 'pending',
                    cohort_id: cohorts(:published_mba_cohort).id
                ).first
                assert_event_logged do |id, user_id, event_type, attrs|
                    expect(attrs[:relevant_email]).to eq('you are rejected from mba/emba')
                end
                cohort_application.update_attribute(:status, "rejected")
            end

            it "should not set relevant email when not mba|emba" do
                cohort_application = CohortApplication.where(
                    status: 'pending',
                    cohort_id: Cohort.where.not(program_type: ['mba', 'emba']).first
                ).first
                assert_event_logged do |id, user_id, event_type, attrs|
                    expect(attrs[:relevant_email]).to be_nil
                end
                cohort_application.update_attribute(:status, "rejected")
            end

            it "should not set relevant email when retargeted" do
                cohort_application = CohortApplication.where(
                    status: 'pending',
                    cohort_id: cohorts(:published_mba_cohort).id
                ).first
                cohort_application.retargeted_from_program_type = 'other'
                assert_event_logged do |id, user_id, event_type, attrs|
                    expect(attrs[:relevant_email]).to be_nil
                end
                cohort_application.update_attribute(:status, "rejected")
            end

            describe "when send_retargeted_email_for_program_type" do

                it "should include send_retargeted_email_for_program_type and target_cohort event_attributes in payload" do
                    cohort = cohorts(:published_mba_cohort)
                    cohort_application = cohort.cohort_applications.where(status: 'pending').first
                    cohort_application.send_retargeted_email_for_program_type = 'foo'
                    mock_cohort_event_attrs = {bar: 'bar'}
                    mock_promoted_cohort = double('Cohort')

                    expect(Cohort).to receive(:promoted_cohort).with('foo').and_return(mock_promoted_cohort)
                    expect(mock_promoted_cohort).to receive(:event_attributes).with(cohort_application.user.timezone).and_return(mock_cohort_event_attrs)
                    assert_event_logged do |id, user_id, event_type, attrs|
                        expect(attrs['send_retargeted_email_for_program_type']).to eq('foo')
                        expect(attrs['target_cohort']).to eq(mock_cohort_event_attrs)
                    end
                    cohort_application.update_attribute(:status, "rejected")
                end
            end
        end

        describe "cohort:pre_accepted event" do

            it "should set relevant_email when moving from \'pending\' to \'pre_accepted\'" do
                cohort_application = CohortApplication.where(
                    status: 'pending',
                    cohort_id: cohorts(:published_mba_cohort).id
                ).first

                assert_event_logged do |id, user_id, event_type, attrs|
                    expect(attrs[:relevant_email]).to eq("you are pre_accepted to #{cohort_application.program_type}")
                end
                cohort_application.update_attribute(:status, "pre_accepted")
            end

            it "should not set relevant_email when moving from \'deferred\' to \'pre_accepted\'" do
                cohort_application = CohortApplication.where(
                    status: 'deferred',
                    cohort_id: cohorts(:published_mba_cohort).id
                ).first

                assert_event_logged do |id, user_id, event_type, attrs|
                    expect(attrs[:relevant_email]).to be_nil
                end
                cohort_application.update_attribute(:status, "pre_accepted")
            end

            it "should not set relevant_email when retargeted_from_program_type without a full scholarship" do
                cohort_application = CohortApplication.where(
                    status: 'pending',
                    cohort_id: cohorts(:published_mba_cohort).id
                ).first

                assert_event_logged do |id, user_id, event_type, attrs|
                    expect(attrs[:relevant_email]).to be_nil
                end
                cohort_application.update({
                    status: "pre_accepted",
                    retargeted_from_program_type: 'mba',
                    cohort_id: cohorts(:published_emba_cohort).id,
                    total_num_required_stripe_payments: 12,
                    scholarship_level: { "standard" => { "default_plan" => { "percent_off" => 10 } }, "early" => { "default_plan" => { "percent_off" => 10 } } }
                })
            end

            it "should set relevant_email when retargeted with a full scholarship" do
                cohort_application = CohortApplication.where(
                    status: 'pending',
                    cohort_id: cohorts(:published_mba_cohort).id
                ).first

                assert_event_logged do |id, user_id, event_type, attrs|
                    expect(attrs[:relevant_email]).to eq("you are retargeted and pre_accepted with full scholarship to emba")
                end
                cohort_application.update({
                    status: "pre_accepted",
                    retargeted_from_program_type: 'mba',
                    cohort_id: cohorts(:published_emba_cohort).id,
                    total_num_required_stripe_payments: 12,
                    scholarship_level: { "name": "Full Scholarship", "standard" => { "default_plan" => { "percent_off" => 100 } }, "early" => { "default_plan" => { "percent_off" => 100 } } }
                })
            end

        end

        describe "cohort:accepted event" do
            it "should set slack_url" do
                cohort = cohorts(:published_mba_cohort)
                slack_room = cohort.slack_rooms.first

                cohort_application = CohortApplication.where(
                    status: 'pending',
                    cohort_id: cohort.id
                ).first

                assert_event_logged do |id, user_id, event_type, attrs|
                    expect(attrs[:cohort_slack_url]).to eq(slack_room.url)
                end
                cohort_application.update(status: "accepted", cohort_slack_room_id: slack_room.id)
            end
        end

        describe "with career_profile" do

            it "should include career_profile.consider_early_decision" do
                user = users(:user_with_career_profile)
                user.career_profile.consider_early_decision = true;
                user.cohort_applications.delete_all

                cohort_application = CohortApplication.new(
                    user: user,
                    cohort_id: cohorts(:published_mba_cohort).id,
                    status: 'pending',
                    applied_at: Time.now
                )

                assert_event_logged do |id, user_id, event_type, attrs|
                    expect(attrs[:consider_early_decision]).to eq(true)
                end
                cohort_application.update_attribute(:status, "pre_accepted")
            end

        end

        def assert_event_logged(&block)
            expect(Event).to receive(:create_server_event!).times do |id, user_id, event_type, attrs|

                yield(id, user_id, event_type, attrs)
                mock_event = "mock_event_for_#{user_id}"
                expect(mock_event).to receive(:log_to_external_systems).with(nil, false)
                mock_event
            end
        end

    end

    describe "event_attributes" do

        it "should include published_cohort event_attributes" do
            application = CohortApplication.first
            expect(application.published_cohort).to receive(:event_attributes).with(application.user.timezone).and_return({})
            application.event_attributes
        end

        it "should include the scholarship level's name if the cohort supports payments" do
            cohort_application = CohortApplication.where.not(scholarship_level: nil).first
            allow(cohort_application).to receive(:published_cohort).and_return(@emba_cohort)
            allow(@emba_cohort).to receive(:supports_payments?).and_return(true)
            hash = cohort_application.event_attributes
            expect(hash[:scholarship_level_name]).to eq(cohort_application.scholarship_level['name'])
        end

        describe "admissions_decision" do
            before(:each) do
                @cohort_application = CohortApplication.first
                CohortApplication::Version.where(id: @cohort_application.id).delete_all
            end

            it "should work with no decisions in versions" do
                @cohort_application.update_column(:admissions_decision, nil)
                expect(CohortApplication::Version.where(id: @cohort_application.id).size).to be(1)
                hash = @cohort_application.event_attributes
                expect(hash[:admissions_decision]).to be_nil
            end

            it "should work when only one decision in versions" do
                @cohort_application.update(admissions_decision: "Invite to Interview")
                expect(CohortApplication::Version.where(id: @cohort_application.id).size).to be(1)
                hash = @cohort_application.event_attributes
                expect(hash[:admissions_decision]).to eq('Invite to Interview')
            end

            it "should work when multiple decisions in versions" do
                @cohort_application.update(admissions_decision: "previous decision")
                @cohort_application.update(admissions_decision: "Invite to Interview")
                expect(CohortApplication::Version.where(id: @cohort_application.id).size).to be(2)
                hash = @cohort_application.event_attributes
                expect(hash[:admissions_decision]).to eq('Invite to Interview')
            end
        end

        describe "previous_admissions_decision" do

            before(:each) do
                @cohort_application = CohortApplication.first
                CohortApplication::Version.where(id: @cohort_application.id).delete_all
            end

            it "should work with no decisions in versions" do
                @cohort_application.update_column(:admissions_decision, nil)
                expect(CohortApplication::Version.where(id: @cohort_application.id).size).to be(1)
                hash = @cohort_application.event_attributes
                expect(hash[:previous_admissions_decision]).to be_nil
            end

            it "should work when only one decision in versions" do
                @cohort_application.update(admissions_decision: "Invite to Interview")
                expect(CohortApplication::Version.where(id: @cohort_application.id).size).to be(1)
                hash = @cohort_application.event_attributes
                expect(hash[:previous_admissions_decision]).to be_nil
            end

            it "should work when multiple decisions in versions" do
                @cohort_application.update(admissions_decision: "previous decision")
                @cohort_application.update(admissions_decision: "Invite to Interview")
                expect(CohortApplication::Version.where(id: @cohort_application.id).size).to be(2)
                hash = @cohort_application.event_attributes
                expect(hash[:previous_admissions_decision]).to eq('previous decision')
            end
        end
    end

    describe "should_sync_fallback_program_type?" do

        it "should be true if acceptable_status? and the user's program_type is one of the program types supported by a cohort" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:acceptable_status?).and_return(true, true, false)
            # the user's `program_type` method won't get called 3 times because the logic
            # short-circuits because acceptable_status? is false the third method time
            expect(cohort_application).to receive(:program_type).and_return('mba', 'demo')
            expect(cohort_application.should_sync_fallback_program_type?).to be(true)
            expect(cohort_application.should_sync_fallback_program_type?).to be(false)
            expect(cohort_application.should_sync_fallback_program_type?).to be(false)
        end
    end

    describe "sync_fallback_program_type" do

        it "should update the user's fallback_program_type to the cohort application's program_type" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:program_type).and_return('foobar')
            expect(cohort_application.user).to receive(:update!).with(fallback_program_type: 'foobar')
            cohort_application.sync_fallback_program_type
        end

        it "should be called after_save if should_sync_fallback_program_type?" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:should_sync_fallback_program_type?).and_return(true)
            expect(cohort_application).to receive(:sync_fallback_program_type)
            cohort_application.save!
        end

        it "should not be called after_save if !should_sync_fallback_program_type?" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:should_sync_fallback_program_type?).and_return(false)
            expect(cohort_application).not_to receive(:sync_fallback_program_type)
            cohort_application.save!
        end
    end

    describe "create_graduation_status_changed_event after_save callback" do
        attr_reader :application, :mock_event


        before(:each) do
            @application = CohortApplication.where(graduation_status: 'pending', status: 'accepted').first
            @mock_event = "mock_event"
        end

        it "should work with graduated" do
            expect(Raven).not_to receive(:capture_exception)
            expect(mock_event).to receive(:log_to_external_systems).with(nil, false)
            expect(Event).to receive(:create_server_event!).with(
                anything,
                application.user_id,
                "cohort:graduated",
                application.event_attributes.merge(
                    honors: false,
                    cohort_end_date: application.published_cohort.end_date.to_timestamp
                )
            ).and_return(mock_event)
            application.update_attribute(:graduation_status, 'graduated')
        end

        it "should work with honors" do
            expect(Raven).not_to receive(:capture_exception)
            expect(mock_event).to receive(:log_to_external_systems).with(nil, false)
            expect(Event).to receive(:create_server_event!).with(
                anything,
                application.user_id,
                "cohort:graduated",
                application.event_attributes.merge(
                    honors: true,
                    cohort_end_date: application.published_cohort.end_date.to_timestamp
                )
            ).and_return(mock_event)
            application.update_attribute(:graduation_status, 'honors')
        end

        it "should work with failed" do
            expect(Raven).not_to receive(:capture_exception)
            expect(mock_event).to receive(:log_to_external_systems).with(nil, false)
            expect(Event).to receive(:create_server_event!).with(
                anything,
                application.user_id,
                "cohort:failed",
                application.event_attributes.merge(
                    honors: false,
                    cohort_end_date: application.published_cohort.end_date.to_timestamp
                )
            ).and_return(mock_event)
            application.update_attribute(:graduation_status, 'failed')
        end

        it "should do nothing if no change" do
            expect(Raven).not_to receive(:capture_exception)
            expect(Event).not_to receive(:create_server_event!)
            application.update(:applied_at => Time.now)

        end

        it "should send exception on an unknown value" do
            expect(Raven).to receive(:capture_exception)
            expect(Event).not_to receive(:create_server_event!)
            application.update_attribute(:graduation_status, 'badang!!!!')
        end

    end

    describe "remove_from_career_network" do

        attr_reader :application, :user

        before(:each) do
            @application = CohortApplication.where(graduation_status: 'pending', status: 'accepted').first
            @user = User.where(can_edit_career_profile: true).first
            expect(@application).to receive(:user).at_least(1).times.and_return(@user)
        end

        it "should be called after_save if just_failed?" do
            expect(@application).to receive(:just_failed?).and_return(true)
            expect(@application).to receive(:remove_from_career_network)
            @application.save!
        end

        it "should not be called after_save if !just_failed?" do
            expect(@application).to receive(:just_failed?).and_return(false)
            expect(@application).not_to receive(:remove_from_career_network)
            @application.save!
        end

        it "should set self.user.can_edit_career_profile to false and save user" do
            expect(user).to receive(:can_edit_career_profile=).exactly(1).times.with(false).and_call_original
            expect(user).to receive(:save!).exactly(1).times
            @application.remove_from_career_network
        end
    end

    describe "reset_user_cohort_applications_association" do

        it "should be called after_create" do
            expect_any_instance_of(CohortApplication).to receive(:reset_user_cohort_applications_association)
            user = User.left_outer_joins(:cohort_applications).where("cohort_applications.id is null").first
            CohortApplication.create!(user_id: user.id, cohort_id: Cohort.first.id, applied_at: Time.now, status: 'pending')
        end

        it "should be called after_update" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:reset_user_cohort_applications_association)
            cohort_application.update!(applied_at: Time.now)
        end

        it "should reset the user's cohort_applications association if the user association is loaded" do
            cohort_application = CohortApplication.first

            # Load the user association and it's cohort_applications association. Just referencing the
            # cohort_applications association apparently doesn't load it, but this seems to work. Strictly
            # speaking, the method resets the cohort_applications association even if it's not loaded, so
            # we don't need to load up the cohort_applications association here, but it's helpful in this
            # spec to be able to tell that the cohort_applications association has been reset.
            cohort_application.user.cohort_applications.each { |cohort_application| }

            expect(cohort_application.association(:user).loaded?).to be(true)
            expect(cohort_application.user.association(:cohort_applications).loaded?).to be(true)

            cohort_application.reset_user_cohort_applications_association
            expect(cohort_application.association(:user).loaded?).to be(true)
            expect(cohort_application.user.association(:cohort_applications).loaded?).to be(false)
        end
    end

    describe "create_registered_changed_event after_save callback" do

        it "should set send registered and unregistered events" do

            cohort_application = CohortApplication.where.not(:scholarship_level => nil).first

            assert_event_logged do |id, user_id, event_type, attrs|
                expect(event_type).to eq("cohort:registered")
                expect(attrs[:cohort_id]).to eq(cohort_application.cohort_id)
                expect(attrs[:cohort_title]).to eq(cohort_application.published_cohort.title)
                expect(attrs[:program_type]).to eq(cohort_application.published_cohort.program_type)
                expect(attrs[:stripe_coupon_id]).to eq(cohort_application.applicable_coupon_info["id"])
            end

            cohort_application.update_attribute(:registered, true)

            assert_event_logged do |id, user_id, event_type, attrs|
                expect(event_type).to eq("cohort:unregistered")
                expect(attrs[:cohort_id]).to eq(cohort_application.cohort_id)
                expect(attrs[:cohort_title]).to eq(cohort_application.published_cohort.title)
                expect(attrs[:program_type]).to eq(cohort_application.published_cohort.program_type)
                expect(attrs[:stripe_coupon_id]).to eq(cohort_application.applicable_coupon_info["id"])
            end

            cohort_application.update_attribute(:registered, false)
        end
    end

    describe "create_retargeted_changed_event after_save callback" do

        it "should send event" do

            cohort_application = CohortApplication.where(
                status: 'pending',
                cohort_id: cohorts(:published_mba_cohort).id
            ).first

            # turn off these events so we find the ones we care about
            allow(cohort_application).to receive(:create_status_changed_event)

            assert_event_logged do |id, user_id, event_type, attrs|
                expect(event_type).to eq("cohort:retargeted_from_program_type_changed")
                expect(attrs[:retargeted_from_program_type]).to eq('mba')
            end

            emba_cohort = cohorts(:published_emba_cohort)
            cohort_application.update({
                status: "pre_accepted",
                retargeted_from_program_type: 'mba',
                cohort_id: emba_cohort.id,
                total_num_required_stripe_payments: 12,
                scholarship_level: emba_cohort.scholarship_levels[0]
            })

            assert_event_logged do |id, user_id, event_type, attrs|
                expect(event_type).to eq("cohort:retargeted_from_program_type_changed")
                expect(attrs[:retargeted_from_program_type]).to be_nil
            end

            cohort_application.update({
                retargeted_from_program_type: nil
            })
        end
    end

    describe "clear_subscription_on_zero_payments after_save callback" do
        it "should destroy the primary_subscription if changed to 0" do
            user = users(:user_with_subscriptions_enabled)
            expect(user.primary_subscription).to be_nil # sanity check
            user.create_stripe_customer
            instance = user.last_application
            instance.cohort.stripe_plans = [{id: default_plan.id, amount: default_plan.amount, frequency: 'monthly'}]
            instance.total_num_required_stripe_payments = 12
            subscription = Subscription.new({
                stripe_subscription_id: 'some_subscription_id',
                stripe_product_id: instance.cohort.program_type,
                stripe_plan_id: instance.cohort.stripe_plans[0]['id'],
                past_due: false
            })
            instance.stripe_plan_id = subscription.stripe_plan_id
            instance.save!
            subscription.user = user
            subscription.save!
            user.save!
            user.reload
            expect(user.primary_subscription).not_to be_nil

            instance = user.last_application
            instance.total_num_required_stripe_payments = 0
            instance.save!

            user.subscriptions.reload
            expect(user.primary_subscription).to be_nil
        end
    end

    describe "add_user_to_extra_courses_access_group after_save callback" do
        it "should be called on after save" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:add_user_to_extra_courses_access_group)

            cohort_application.save!
        end

        it "should not add user to EXTRA COURSES if already in group" do
            cohort_application = CohortApplication.first

            # stub method calls out to make it appear that the cohort application
            # status was changed to 'rejected' and in MBA or EMBA, and user is
            # already in EXTRA COURSES access group
            expect(cohort_application).to receive(:saved_change_to_status?).and_return(true)
            expect(cohort_application).to receive(:status).and_return('rejected')
            expect(cohort_application).to receive(:mba_or_emba?).and_return(true)

            expect(cohort_application.user).to receive(:in_group?).with('EXTRA COURSES').and_return(true)
            expect(cohort_application.user).not_to receive(:add_to_group).with('EXTRA COURSES')

            cohort_application.add_user_to_extra_courses_access_group
        end

        it "should add user to EXTRA COURSES access group upon being rejected from MBA or EMBA" do
            cohort_application = CohortApplication.first

            # stub method calls out to make it appear that the cohort application
            # status was changed to 'rejected', in MBA or EMBA, and user is not
            # already in EXTRA COURSES access group
            expect(cohort_application).to receive(:saved_change_to_status?).and_return(true)
            expect(cohort_application).to receive(:status).and_return('rejected')
            expect(cohort_application).to receive(:mba_or_emba?).and_return(true)
            expect(cohort_application.user).to receive(:in_group?).with('EXTRA COURSES').and_return(false)

            expect(cohort_application.user).to receive(:add_to_group).with('EXTRA COURSES').and_return(true)

            cohort_application.add_user_to_extra_courses_access_group
        end
    end

    describe "log_peer_recommendation_request_events after_create callback" do

        it "should log_peer_recommendation_request_events for career profile" do
            # get a user that has a career profile and no cohort application
            user = User.where("id NOT IN (#{CohortApplication.select(:user_id).to_sql})").where("id IN (#{CareerProfile.select(:user_id).to_sql})").first
            cohort = cohorts(:published_mba_cohort)

            expect_any_instance_of(CohortApplication).to receive(:log_peer_recommendation_request_events).and_call_original
            expect_any_instance_of(CareerProfile).to receive(:log_peer_recommendation_request_events)

            CohortApplication.create!({
                user_id: user.id,
                cohort_id: cohort.id,
                applied_at: Time.now
            })
        end
    end

    describe "set_notify_candidate_positions_preferences_on_first_submission before_create callback" do
        before(:each) do
            @user = User.joins(:career_profile).where(notify_candidate_positions: nil, notify_candidate_positions_recommended: nil).first
            @user.cohort_applications.delete_all
            @cohort = cohorts(:published_mba_cohort)
        end

        it "should not set either preference if already set" do
            @user.update_columns(notify_candidate_positions: false, notify_candidate_positions_recommended: 'never')
            @user.career_profile.update_columns(interested_in_joining_new_company: 'very_interested')
            CohortApplication.create!({
                user_id: @user.id,
                cohort_id: @cohort.id,
                applied_at: Time.now
            })
            @user.reload
            expect(@user.notify_candidate_positions).to be(false)
            expect(@user.notify_candidate_positions_recommended).to eq('never')
        end

        it "should set both if interested_in_joining_new_company is very_interested" do
            @user.career_profile.update_columns(interested_in_joining_new_company: 'very_interested')
            CohortApplication.create!({
                user_id: @user.id,
                cohort_id: @cohort.id,
                applied_at: Time.now
            })
            @user.reload
            expect(@user.notify_candidate_positions).to be(true)
            expect(@user.notify_candidate_positions_recommended).to eq('daily')
        end

        it "should set to weekly for both if interested_in_joining_new_company is interested" do
            @user.career_profile.update_columns(interested_in_joining_new_company: 'interested')
            CohortApplication.create!({
                user_id: @user.id,
                cohort_id: @cohort.id,
                applied_at: Time.now
            })
            @user.reload
            expect(@user.notify_candidate_positions).to be(true)
            expect(@user.notify_candidate_positions_recommended).to eq('daily')
        end

        it "should set to weekly and bi_weekly if interested_in_joining_new_company is neutral" do
            @user.career_profile.update_columns(interested_in_joining_new_company: 'neutral')
            CohortApplication.create!({
                user_id: @user.id,
                cohort_id: @cohort.id,
                applied_at: Time.now
            })
            @user.reload
            expect(@user.notify_candidate_positions).to be(true)
            expect(@user.notify_candidate_positions_recommended).to eq('bi_weekly')
        end

        it "should set to never for both if interested_in_joining_new_company is not_interested" do
            @user.career_profile.update_columns(interested_in_joining_new_company: 'not_interested')
            CohortApplication.create!({
                user_id: @user.id,
                cohort_id: @cohort.id,
                applied_at: Time.now
            })
            @user.reload
            expect(@user.notify_candidate_positions).to be(false)
            expect(@user.notify_candidate_positions_recommended).to eq('never')
        end

        it "should capture exception if unknown interested_in_joining_new_company value" do
            @user.career_profile.update_columns(interested_in_joining_new_company: "foo_bar")
            expect(Raven).to receive(:capture_exception).with("Cannot set notify_candidate_positions preferences with this interested_in_joining_new_company value", {
                extra: {
                    interested_in_joining_new_company: "foo_bar"
                }
            })
            CohortApplication.create!({
                user_id: @user.id,
                cohort_id: @cohort.id,
                applied_at: Time.now
            })
        end
    end

    describe "period_start_info" do

        it "should work" do
            cohort = cohorts(:published_mba_cohort)
            user = User.left_outer_joins(:cohort_applications).where("cohort_applications.id is null").first
            application = CohortApplication.create!({
                user_id: user.id,
                cohort_id: cohort.id,
                applied_at: Time.now
            })
            period = cohort.periods[1]

            expect(application).to receive(:curriculum_status).and_return('foo')
            allow_any_instance_of(User).to receive(:cohort_percent_complete).with(cohort.id).and_return(0.42324)
            allow_any_instance_of(Cohort).to receive(:expected_percent_complete).with(1).and_return(0.53474)

            expected_required_courses_completed_target = period['stream_entries'].select { |e| e['required']}.size
            expect(expected_required_courses_completed_target > 0).to be(true)

            hash = application.period_start_info(period, 'test_relevant_email')

            expect(hash[:cohort_id]).to eq(cohort.id)
            expect(hash[:program_type]).to eq(cohort.program_type)
            expect(hash[:period_index]).to eq(2)
            expect(hash[:period_style]).to eq(cohort.periods[1]["style"])
            expect(hash[:period_title]).to eq(cohort.periods[1]["title"])
            expect(hash[:period_start]).to eq(cohort.start_time_for_period(period).to_timestamp)
            expect(hash[:period_end]).to eq(cohort.end_time_for_period(period).to_timestamp)
            expect(hash[:relevant_email]).to eq('test_relevant_email')
            expect(hash[:required_courses_completed_actual]).to eq(user.required_courses_completed_overall(cohort.id))
            expect(hash[:required_courses_completed_target]).to eq(expected_required_courses_completed_target)
            expect(hash[:required_courses_entire_schedule]).to eq(cohort.get_required_stream_count)
            expect(hash[:curriculum_percent_complete]).to eq(42)
            expect(hash[:curriculum_expected_complete]).to eq(53)
            expect(hash[:curriculum_status]).to eq('foo')

            expect(hash[:course_entries].size).to eq(period['stream_entries'].size)

        end
    end

    describe "curriculum_status" do
        before(:each) do
            cohort = cohorts(:published_mba_cohort)
            @application = CohortApplication.where(cohort_id: cohort.id, graduation_status: 'pending').first
        end

        it "should return graduation status if 'failed', 'graduated', or 'honors'" do
            @application.update_column(:graduation_status, 'failed')
            expect(@application.curriculum_status).to eq('failed')

            @application.update_column(:graduation_status, 'graduated')
            expect(@application.curriculum_status).to eq('graduated')

            @application.update_column(:graduation_status, 'honors')
            expect(@application.curriculum_status).to eq('honors')
        end

        it "should return 'finished' if past the last schedule period and no past due streams" do
            stream_lpids = Lesson.limit(2).pluck(:locale_pack_id)
            expect(@application.published_cohort).to receive(:current_period_index).and_return(nil)
            expect(@application.published_cohort).to receive(:expected_stream_lpids_complete).and_return(stream_lpids)
            expect(@application.user).to receive(:completed_stream_locale_pack_ids).and_return(stream_lpids + Lesson::Stream.limit(10).pluck(:locale_pack_id))
            expect(@application.curriculum_status).to eq('finished')
        end

        it "should return 'week_0' if before the start of the schedule" do
            expect(@application.published_cohort).to receive(:current_period_index).and_return(-1)
            expect(@application.curriculum_status).to eq('week_0')
        end

        it "should return 'week_1' if it is the first period" do
            expect(@application.published_cohort).to receive(:expected_stream_lpids_complete)
            expect(@application.published_cohort).to receive(:current_period_index).and_return(0)
            expect(@application.curriculum_status).to eq('week_1')
        end

        describe "not_on_track" do
            it "should return 'not_on_track' if more than four past due streams and currently in the schedule" do
                expect(@application.published_cohort).to receive(:current_period_index).and_return(1)
                expect(@application.user).to receive(:completed_stream_locale_pack_ids).and_return([])


                expect(@application.published_cohort).to receive(:expected_stream_lpids_complete).with(1).and_return(Lesson.limit(5).pluck(:locale_pack_id))
                expect(@application.published_cohort).to receive(:expected_stream_lpids_complete).with(2).and_return(Lesson.limit(5).pluck(:locale_pack_id))

                expect(@application.curriculum_status).to eq('not_on_track')
            end

            it "should return 'not_on_track' if more than four past due streams and after the schedule" do
                expect(@application.published_cohort).to receive(:current_period_index).and_return(nil)
                expect(@application.user).to receive(:completed_stream_locale_pack_ids).and_return([])
                expect(@application.published_cohort).to receive(:expected_stream_lpids_complete).and_return(Lesson.limit(5).pluck(:locale_pack_id))
                expect(@application.curriculum_status).to eq('not_on_track')
            end
        end

        it "should return 'almost_there' if there are past due streams but less than or equal to four" do
            expect(@application.published_cohort).to receive(:current_period_index).and_return(1)
            expect(@application.user).to receive(:completed_stream_locale_pack_ids).and_return([])

            expect(@application.published_cohort).to receive(:expected_stream_lpids_complete).with(1).and_return(Lesson.limit(1).pluck(:locale_pack_id)).ordered
            expect(@application.published_cohort).to receive(:expected_stream_lpids_complete).with(2).and_return(Lesson.limit(1).pluck(:locale_pack_id)).ordered

            expect(@application.curriculum_status).to eq('almost_there')
        end

        it "should return 'on_track_finished' if user has completed the streams for this period" do
            stream_lpids = Lesson::Stream.limit(5).pluck(:locale_pack_id)
            expect(@application.published_cohort).to receive(:current_period_index).and_return(1)
            expect(@application.user).to receive(:completed_stream_locale_pack_ids).and_return(stream_lpids)

            expect(@application.published_cohort).to receive(:expected_stream_lpids_complete).with(1).and_return(stream_lpids).ordered
            expect(@application.published_cohort).to receive(:expected_stream_lpids_complete).with(2).and_return(stream_lpids).ordered

            expect(@application.curriculum_status).to eq('on_track_finished')
        end

        it "should return 'on_track' there are no past due streams for the last period" do
            stream_lpids = Lesson::Stream.limit(5).pluck(:locale_pack_id)
            expect(@application.published_cohort).to receive(:current_period_index).and_return(1)
            expect(@application.user).to receive(:completed_stream_locale_pack_ids).and_return(stream_lpids)

            expect(@application.published_cohort).to receive(:expected_stream_lpids_complete).with(1).and_return(stream_lpids).ordered
            expect(@application.published_cohort).to receive(:expected_stream_lpids_complete)
                .with(2).and_return(Lesson::Stream.where.not(locale_pack_id: stream_lpids).limit(5).pluck(:locale_pack_id)).ordered

            expect(@application.curriculum_status).to eq('on_track')
        end
    end

    describe "status_changed_info" do

        it "should set info" do
            cohort_application = CohortApplication.where.not(admissions_decision: nil).first
            hash = cohort_application.status_changed_info(Time.now, 'foo')
            expect(hash[:admissions_decision]).to eq(cohort_application.admissions_decision)
            expect(hash[:previous_cohort_status]).to eq('foo')
        end

        it "should set just_used_deferral_link if changing to deferred" do
            cohort_application = CohortApplication.where(status: 'deferred').first
            cohort_application.just_used_deferral_link = true
            hash = cohort_application.status_changed_info(Time.now, 'foo')
            expect(hash[:just_used_deferral_link]).to eq(true)
        end

        describe "special expulsion info" do
            attr_reader :cohort
            before(:each) do
                allow_any_instance_of(CohortApplication).to receive(:program_type).and_return('test')
                @cohort = cohorts(:published_mba_cohort)
            end

            it "should set expelled_manually=false if user_expelled_due_to_period_expulsion_action" do
                user = User.left_outer_joins(:cohort_applications).where("cohort_applications.id is null").first
                application = CohortApplication.create!({
                    user_id: user.id,
                    cohort_id: cohort.id,
                    status: "expelled",
                    applied_at: Time.now
                })
                user.reload

                application.user_expelled_due_to_period_expulsion_action = true
                hash = application.status_changed_info(Time.now, application.status)
                expect(hash[:expelled_manually]).to be(false)
            end

            it "should set before_enrollment_deadline" do
                user = User.left_outer_joins(:cohort_applications).where("cohort_applications.id is null").first
                application = CohortApplication.create!({
                    user_id: user.id,
                    cohort_id: cohort.id,
                    status: "expelled",
                    applied_at: Time.now
                })
                user.reload


                hash = application.status_changed_info(Time.now + 1.day, application.status)
                expect(hash[:before_enrollment_deadline]).to be(true)

                hash = application.status_changed_info(Time.now - 1.day, application.status)
                expect(hash[:before_enrollment_deadline]).to be(false)
            end

            it "should expelled_manually when not user_expelled_due_to_period_expulsion_action" do
                user = User.left_outer_joins(:cohort_applications).where("cohort_applications.id is null").first
                application = CohortApplication.create!({
                    user_id: user.id,
                    cohort_id: cohort.id,
                    status: "expelled",
                    applied_at: Time.now
                })
                user.reload

                application.user_expelled_due_to_period_expulsion_action = false
                hash = application.status_changed_info(Time.now, application.status)
                expect(hash[:expelled_manually]).to be(true)
            end
        end

    end

    describe "payment_past_due?" do

        it "should be false if has_paid_in_full?" do
            instance = CohortApplication.new
            expect(instance).to receive(:has_paid_in_full?).and_return(true)
            expect(instance).not_to receive(:primary_subscription)
            expect(instance).not_to receive(:locked_due_to_past_due_payment?)
            expect(instance).not_to receive(:past_payment_grace_period?)
            expect(instance.payment_past_due?).to be(false)
        end

        it "should use value from primary_subscription if there is one" do
            instance = CohortApplication.new
            expect(instance).to receive(:has_paid_in_full?).and_return(false)
            expect(instance).to receive(:primary_subscription).at_least(1).times.and_return(OpenStruct.new(past_due: 'past_due_value'))
            expect(instance).not_to receive(:locked_due_to_past_due_payment?)
            expect(instance).not_to receive(:past_payment_grace_period?)
            expect(instance.payment_past_due?).to eq('past_due_value')
        end

        it "should be true if locked_due_to_past_due_payment?" do
            instance = CohortApplication.new
            expect(instance).to receive(:has_paid_in_full?).and_return(false)
            expect(instance).to receive(:primary_subscription).and_return(nil)
            expect(instance).to receive(:locked_due_to_past_due_payment?).and_return(true)
            expect(instance).not_to receive(:past_payment_grace_period?)
            expect(instance.payment_past_due?).to be(true)
        end

        it "should be true if past_payment_grace_period?" do
            instance = CohortApplication.new
            expect(instance).to receive(:has_paid_in_full?).and_return(false)
            expect(instance).to receive(:locked_due_to_past_due_payment?).and_return(false)
            expect(instance).to receive(:primary_subscription).and_return(nil)
            expect(instance).to receive(:past_payment_grace_period?).and_return(true)
            expect(instance.payment_past_due?).to be(true)
        end

        it "should be false otherwise" do
            instance = CohortApplication.new
            expect(instance).to receive(:has_paid_in_full?).and_return(false)
            expect(instance).to receive(:primary_subscription).and_return(nil)
            expect(instance).to receive(:locked_due_to_past_due_payment?).and_return(false)
            expect(instance).to receive(:past_payment_grace_period?).and_return(false)
            expect(instance.payment_past_due?).to be(false)
        end

    end

    describe "past_payment_grace_period?" do

        it "should be false if no payment_grace_period_end_at" do
            instance = CohortApplication.new(payment_grace_period_end_at: nil)
            expect(instance.past_payment_grace_period?).to be(false)
        end

        it "should be false if payment_grace_period_end_at is in the future" do
            instance = CohortApplication.new(payment_grace_period_end_at: Time.now + 1.minute)
            expect(instance.past_payment_grace_period?).to be(false)
        end

        it "should be true if payment_grace_period_end_at is in the past" do
            instance = CohortApplication.new(payment_grace_period_end_at: Time.now - 1.minute)
            expect(instance.past_payment_grace_period?).to be(true)
        end

    end

    describe "validate_stripe_details" do

        attr_accessor :instance

        before(:each) do
            expect(@emba_cohort.stripe_plans).not_to be_nil
            @instance = CohortApplication.create!({
                user_id: @user.id,
                cohort_id: @emba_cohort.id,
                applied_at: Time.now,
                total_num_required_stripe_payments: 12,
                scholarship_level: { "standard" => { "default_plan" => { "amount_off" => 10000 } }, "early" => { "default_plan" => { "amount_off" => 10000 } }  }
            })
        end

        it "should be invalid if missing total_num_required_stripe_payments, not full scholarship, and registered or charged" do

            # unset but without registration / charges
            instance.total_num_required_stripe_payments = nil
            expect(instance.valid?).to be(true)

            # unset with registration
            instance.registered = true
            expect(instance.valid?).to be(false)

            # unset with charges
            instance.registered = false
            instance.num_charged_payments = 1
            instance.num_charged_payments = 0
            expect(instance.valid?).to be(false)

            # unset with full scholarship
            instance.registered = true
            instance.num_charged_payments = nil
            instance.num_charged_payments = nil
            expect(instance).to receive(:has_full_scholarship?).at_least(1).times.and_return(true)
            expect(instance.valid?).to be(true)

        end

        it "should be invalid if missing stripe_plan_id, not full scholarship, and registered or charged" do

            # unset but without registration / charges
            instance.stripe_plan_id = nil
            expect(instance.valid?).to be(true)

            # unset with registration
            instance.registered = true
            expect(instance.valid?).to be(false)

            # unset with charges
            instance.registered = false
            instance.num_charged_payments = 1
            instance.num_charged_payments = 0
            expect(instance.valid?).to be(false)

            # unset with full scholarship
            instance.registered = true
            instance.num_charged_payments = nil
            instance.num_charged_payments = nil
            instance.scholarship_level = { "name": "Full Scholarship", "standard" => { "default_plan" => { "percent_off" => 100 } }, "early" => { "default_plan" => { "percent_off" => 100 } } }
            expect(instance.valid?).to be(true)

        end

        it "should be invalid if missing an appropriate scholarship_level with coupon" do

            # improperly set
            instance.scholarship_level = { "standard" => { "default_plan" => { } }, "early" => { "default_plan" => { } } }
            expect(instance.valid?).to be(false)

            # unset with registration
            instance.registered = true
            expect(instance.valid?).to be(false)

            # unset with charges
            instance.registered = false
            instance.num_charged_payments = 1
            instance.num_charged_payments = 0
            expect(instance.valid?).to be(false)

            # unset with full scholarship
            instance.registered = true
            instance.num_charged_payments = nil
            instance.num_charged_payments = nil
            instance.scholarship_level = { "name": "Full Scholarship", "standard" => { "default_plan" => { "percent_off" => 100 } }, "early" => { "default_plan" => { "percent_off" => 100 } } }
            expect(instance.valid?).to be(true)

        end

        it "should add error if the cohort has stripe plans but not the application" do
            application = cohorts(:published_emba_cohort).cohort_applications.first
            allow(application).to receive(:set_stripe_plans_and_scholarship_levels)
            application.stripe_plans = nil
            expect(application.valid?).to be(false)
            expect(application.errors.full_messages).to include('Stripe plans must be set if the cohort has stripe plans')
            application.stripe_plans = []
            expect(application.valid?).to be(false)
            expect(application.errors.full_messages).to include('Stripe plans must be set if the cohort has stripe plans')
        end

        it "should add error if the cohort supports scholarship levels but application does not have them" do
            application = cohorts(:published_emba_cohort).cohort_applications.first
            allow(application).to receive(:set_stripe_plans_and_scholarship_levels)
            application.scholarship_levels = nil
            expect(application.valid?).to be(false)
            expect(application.errors.full_messages).to include('Scholarship levels must be set if the cohort has scholarship levels')
            application.scholarship_levels = []
            expect(application.valid?).to be(false)
            expect(application.errors.full_messages).to include('Scholarship levels must be set if the cohort has scholarship levels')
        end

        it "should not error if the cohort does not support scholarship levels" do
            application = cohorts(:published_the_business_certificate_cohort).cohort_applications.first
            allow(application).to receive(:set_stripe_plans_and_scholarship_levels)
            application.scholarship_levels = nil
            expect(application.valid?).to be(true)
            expect(application.errors.full_messages).to be_empty
        end

        it "should add error if the stripe_plan_id is not in the list" do
            application = cohorts(:published_emba_cohort).cohort_applications.first
            allow(application).to receive(:set_stripe_plans_and_scholarship_levels)
            application.stripe_plan_id = 'not this one'
            expect(application.valid?).to be(false)
            expect(application.errors.full_messages).to include('Stripe plan must be included in stripe_plans')
        end
    end

    describe "validate_some_other_stripe_details_before_commit" do

        it "should raise if registered, requires payments, and no active subscription" do
            cohort_application = CohortApplication.joins(:cohort).where(cohorts: {program_type: 'emba'}, status: 'accepted').first
            cohort_application.user.primary_subscription&.destroy

            expect {
                # The following needs to be wrapped in its own transaction, so the
                # before_commit hooks will fire
                ActiveRecord::Base.transaction do
                    cohort_application.update({
                        total_num_required_stripe_payments: 12,
                        registered: true,
                        stripe_plan_id: cohort_application.stripe_plan_ids.first
                    })
                end
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Registered must not be true if there are required payments and no subscription.")
        end
    end

    describe "sync with airtable" do

        describe "should_sync_with_airtable?" do

            it "should be false if program type does not support airtable sync" do
                application = CohortApplication.first
                expect(application).to receive(:supports_airtable_sync?).and_return(false)
                expect(application.should_sync_with_airtable?).to be(false)
            end

            describe "when supports_airtable_sync?" do

                before(:each) do
                    allow_any_instance_of(CohortApplication).to receive(:supports_airtable_sync?).and_return(true)
                end

                it "should be true if user is can_edit_career_profile = true even if application is rejected" do
                    application = CohortApplication.where(status: "rejected").first
                    expect(application).not_to be_nil # sanity check
                    expect(application.user).to receive(:can_edit_career_profile).and_return(true)
                    expect(application.should_sync_with_airtable?).to be(true)
                end

                describe "when !can_edit_career_profile" do

                    before(:each) do
                        allow_any_instance_of(User).to receive(:can_edit_career_profile).and_return(false)
                    end

                    it "should be false if updating a rejected application" do
                        application = CohortApplication.where(status: "rejected").first
                        expect(application).not_to be_nil # sanity check
                        expect(application.should_sync_with_airtable?).to be(false)
                    end

                    it "should be false if no saved change to status for expelled application" do
                        application = CohortApplication.where(status: "expelled").first
                        expect(application).not_to be_nil # sanity check
                        expect(application.should_sync_with_airtable?).to be(false)
                    end

                    it "should be false if switching between rejected and expelled" do
                        application = CohortApplication.where(status: "expelled").first
                        expect(application).not_to be_nil # sanity check
                        expect(application.should_sync_with_airtable?).to be(false)
                    end

                    it "should be true if application is switching TO rejected" do
                        application = CohortApplication.where(status: "pending").first
                        expect(application).not_to be_nil # sanity check

                        # should_sync_with_airtable? relies on saved_changes, so it's just easier to call update! rather than mock it out
                        application.update!(status: "rejected")
                        expect(application.should_sync_with_airtable?).to be(true)
                    end

                    it "should be true if application is switching FROM rejected" do
                        application = CohortApplication.where(status: "rejected").first
                        expect(application).not_to be_nil # sanity check

                        # should_sync_with_airtable? relies on saved_changes, so it's just easier to call update! rather than mock it out
                        application.update!(status: "pending")
                        expect(application.should_sync_with_airtable?).to be(true)
                    end
                end
            end
        end

        describe "create_in_airtable" do

            it "should be called on create" do
                @user = users(:user_with_career_profile)
                @user.cohort_applications.destroy_all
                expect_any_instance_of(CohortApplication).to receive(:create_in_airtable)
                CohortApplication.create!({user_id: @user.id, cohort_id: Cohort.first.id, status: 'pending', applied_at: Time.now})
            end

            it "should perform SyncWithAirtableJob later with debounce when supports_airtable_sync?" do
                cohort_application = CohortApplication.first
                expect(cohort_application).to receive(:supports_airtable_sync?).and_return(true)
                expect(SyncWithAirtableJob).to receive(:perform_later_with_debounce).with(cohort_application.id, SyncWithAirtableJob::CREATE)
                cohort_application.send(:create_in_airtable)
            end

            it "should not queue job if program type does not support airtable sync" do
                cohort_application = CohortApplication.first
                expect(cohort_application).to receive(:supports_airtable_sync?).and_return(false)
                expect(SyncWithAirtableJob).not_to receive(:perform_later_with_debounce)
                cohort_application.send(:create_in_airtable)
            end
        end

        describe "update_in_airtable" do
            it "should be called if application changes" do
                instance = users(:user_with_career_profile).last_application
                expect(SyncWithAirtableJob).to receive(:perform_later_with_debounce).at_least(:once).with(instance.id, SyncWithAirtableJob::UPDATE, SyncWithAirtableJob::DEFAULT_PRIORITY)
                instance.update_attribute(:status, "expelled")
            end

            it "should not be called if only changing cohort_slack_room_id and updated_at" do
                application = CohortApplication.where(cohort_slack_room_id: nil).first
                application.update_columns(updated_at: Time.at(0))
                expect(application).not_to receive(:update_in_airtable)
                application.update!(cohort_slack_room_id: CohortSlackRoom.first.id, updated_at: Time.now)
            end

            it "should not be called if only changing rubric columns synced from Airtable to our database" do
                application = CohortApplication.where(cohort_slack_room_id: nil).first

                nilify_hash = {}
                Airtable::Application.from_airtable_column_map.each do |database_key, airtable_key|
                    nilify_hash[database_key] = nil
                end
                application.update_columns(nilify_hash)

                update_hash = {}
                Airtable::Application.from_airtable_column_map.each do |database_key, airtable_key|
                    update_hash[database_key] = 'foo'
                end

                expect(application).not_to receive(:update_in_airtable)
                application.update!(update_hash)
            end

            it "should not be called if inheriting the rubric" do
                application = CohortApplication.where(cohort_slack_room_id: nil).first
                application.update_columns(rubric_inherited: false)
                expect(application).not_to receive(:update_in_airtable)
                application.update!(airtable_base_key: 'FOO_KEY', rubric_round_1_tag: 'Foo Tag', rubric_inherited: true)
            end

            it "should not be called if only changing the airtable_base_key" do
                application = CohortApplication.where(cohort_slack_room_id: nil).first
                application.update_columns(airtable_base_key: nil)
                expect(application).not_to receive(:update_in_airtable)
                application.update!(airtable_base_key: 'FOO_KEY')
            end

            it "should not schedule a SyncWithAirtableJob if !should_sync_with_airtable?" do
                instance = users(:user_with_career_profile).last_application
                expect(instance).to receive(:should_sync_with_airtable?).and_return(false)
                expect(SyncWithAirtableJob).not_to receive(:perform_later_with_debounce)
                instance.update_in_airtable
            end

            describe "when should_sync_with_airtable?" do

                before(:each) do
                    allow_any_instance_of(CohortApplication).to receive(:should_sync_with_airtable?).and_return(true)
                end

                it "should schedule a CREATE instead if applied_at is updating and the record is archived in Airtable" do
                    instance = users(:user_with_career_profile).last_application
                    expect(SyncWithAirtableJob).to receive(:perform_later_with_debounce).with(instance.id, SyncWithAirtableJob::CREATE, SyncWithAirtableJob::DEFAULT_PRIORITY)
                    instance.update!({
                        applied_at: Time.now,
                        airtable_base_key: "AIRTABLE_BASE_KEY_ARCHIVE_X"
                    })
                end

                it "should pass along priority argument to SyncWithAirtableJob call" do
                    priority = 4
                    instance = users(:user_with_career_profile).last_application
                    expect(SyncWithAirtableJob).to receive(:perform_later_with_debounce).with(instance.id, SyncWithAirtableJob::UPDATE, priority)
                    instance.update_in_airtable(priority)
                end

                it "should work if no priority is given" do
                    instance = users(:user_with_career_profile).last_application
                    expect(SyncWithAirtableJob).to receive(:perform_later_with_debounce).with(instance.id, SyncWithAirtableJob::UPDATE, SyncWithAirtableJob::DEFAULT_PRIORITY)
                    instance.update_in_airtable # no priority given, so should use DEFAULT_PRIORITY
                end
            end
        end

        describe "destroy_in_airtable" do

            it "should be called on destroy" do
                instance = users(:user_with_career_profile).last_application
                expect(instance).to receive(:destroy_in_airtable)
                instance.destroy!
            end

            it "should not perform SyncWithAirtableJob later with debounce if application is_archived_in_airtable?" do
                instance = users(:user_with_career_profile).last_application
                expect(instance).to receive(:is_archived_in_airtable?).and_return(true)
                expect(SyncWithAirtableJob).not_to receive(:perform_later_with_debounce)
                instance.send(:destroy_in_airtable)
            end

            it "should not queue job if program type does not support airtable sync" do
                instance = users(:user_with_career_profile).last_application
                allow(instance).to receive(:supports_airtable_sync?).and_return(false)
                expect(SyncWithAirtableJob).not_to receive(:perform_later_with_debounce)
                instance.send(:destroy_in_airtable)
            end
        end
    end

    describe "applicable_registration_period" do

        attr_accessor :cohort_application, :now, :earlier, :later

        before(:each) do
            @cohort_application = CohortApplication.first
            @now = Time.now
            @later = now + 10.days
            @earlier = now - 10.days

            expect(cohort_application.published_cohort).to receive(:supports_early_registration_deadline?).and_return(true)
            allow(Time).to receive(:now).and_return(now)
        end

        it "should return 'early' when within the deadline" do
            expect(cohort_application.published_cohort).to receive(:early_registration_deadline).at_least(1).times.and_return(later)
            expect(cohort_application.applicable_registration_period).to eq("early")
        end

        it "should return 'early' when previously marked registered_early" do
            cohort_application.registered_early = true
            expect(cohort_application.applicable_registration_period).to eq("early")
        end

        it "should return 'standard' when not within the deadline or not previously registered_early" do
            expect(cohort_application.published_cohort).to receive(:early_registration_deadline).at_least(1).times.and_return(earlier)
            expect(cohort_application.applicable_registration_period).to eq("standard")
        end

        it "should return 'standard' if the user previously registered not early" do
            expect(cohort_application.published_cohort).not_to receive(:early_registration_deadline)
            expect(cohort_application).to receive(:has_made_payments?).and_return(true)
            expect(cohort_application.applicable_registration_period).to eq("standard")
        end

    end

    describe "has_full_scholarship?" do
        it "should return true when the scholarship level name is Full Scholarship" do
            cohort_application = CohortApplication.first
            cohort_application.scholarship_level = { "name": "Full Scholarship", "standard" => { "default_plan" => { "percent_off" => 100 } }, "early" => { "default_plan" => { "percent_off" => 100 } } }
            expect(cohort_application.has_full_scholarship?).to be(true)
        end

        it "should return false if the scholarship level name is not Full Scholarship" do
            cohort_application = CohortApplication.first
            cohort_application.scholarship_level = { "name": "Not Full Scholarship" }
            expect(cohort_application.has_full_scholarship?).to be(false)
        end
    end

    describe "set_total_num_default_value" do

        it "should handle one-time payments properly" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:get_plan_frequency).and_return('once')
            cohort_application.set_total_num_default_value('plan_id')
            expect(cohort_application.total_num_required_stripe_payments).to eq(1)
        end

        it "should handle monthly payments properly" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:get_plan_frequency).and_return('monthly')
            cohort_application.set_total_num_default_value('plan_id')
            expect(cohort_application.total_num_required_stripe_payments).to eq(12)
        end

        it "should handle bi_annual payments properly" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:get_plan_frequency).and_return('bi_annual')
            cohort_application.set_total_num_default_value('plan_id')
            expect(cohort_application.total_num_required_stripe_payments).to eq(2)
        end

        it "should raise on an invalid frequency" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:get_plan_frequency).and_return('tri_monthly')
            expect(Proc.new {
                cohort_application.set_total_num_default_value('plan_id')
            }).to raise_error("Invalid frequency for plan plan_id!")
        end

    end

    describe "required_to_provide_payment?" do

        it "should return false if cohort does not support payments" do
            cohort_application = CohortApplication.first
            expect(cohort_application.published_cohort).to receive(:supports_payments?).and_return(false)
            expect(cohort_application.required_to_provide_payment?).to be(false)
        end

        it "should return false if cohort does support payments but has_full_scholarship? is true" do
            cohort_application = CohortApplication.first
            expect(cohort_application.published_cohort).to receive(:supports_payments?).and_return(true)
            expect(cohort_application).to receive(:has_full_scholarship?).and_return(true)
            expect(cohort_application.required_to_provide_payment?).to be(false)
        end

        it "should return true if cohort does support payments but has_full_scholarship? is false" do
            cohort_application = CohortApplication.first
            expect(cohort_application.published_cohort).to receive(:supports_payments?).and_return(true)
            expect(cohort_application).to receive(:has_full_scholarship?).and_return(false)
            expect(cohort_application.required_to_provide_payment?).to be(true)
        end

        it "should return false if total_num_required_stripe_payments is 0" do
            cohort_application = CohortApplication.first
            expect(cohort_application.published_cohort).to receive(:supports_payments?).and_return(true)
            expect(cohort_application).to receive(:has_full_scholarship?).and_return(false)
            cohort_application.total_num_required_stripe_payments = 0
            expect(cohort_application.required_to_provide_payment?).to be(false)
        end

    end

    describe "handle_payment_grace_period" do
        attr_accessor :cohort_application, :original_payment_grace_period_end_at

        before(:each) do
            @cohort_application = CohortApplication.first
            @original_payment_grace_period_end_at = Time.now
            cohort_application.update_column(:payment_grace_period_end_at, original_payment_grace_period_end_at)
        end

        # Some of the contents of this spec are also tested below in other specs, but I wanted to make sure
        # that this method had some overlapping coverage to make sure that it worked as expected on create.
        it "should be called before create" do
            user = User.left_outer_joins(:cohort_applications).where("cohort_applications.id is null").first
            cohort = cohorts(:published_emba_cohort)
            slack_room = cohort.slack_rooms.first
            mock_default_payment_grace_period_end_at = Time.now
            expect_any_instance_of(CohortApplication).to receive(:required_to_provide_payment?).and_return(true)
            expect_any_instance_of(CohortApplication).to receive(:handle_payment_grace_period).and_call_original
            expect_any_instance_of(CohortApplication).to receive(:default_payment_grace_period_end_at).and_return(mock_default_payment_grace_period_end_at)
            cohort_application = CohortApplication.create!(
                user_id: user.id,
                cohort_id: cohort.id,
                applied_at: Time.now,
                status: 'accepted',
                registered: false,
                cohort_slack_room_id: slack_room.id # this needs to be set for the creation to succeed
            )
            expect(cohort_application.payment_grace_period_end_at).to eq(mock_default_payment_grace_period_end_at)
        end

        it "should be called before update" do
            expect(cohort_application).to receive(:handle_payment_grace_period)
            cohort_application.save!
        end

        it "should set payment_grace_period_end_at to default_payment_grace_period_end_at if status_changed? to accepted but user hasn't registered yet and they're required to provide payment" do
            expect(cohort_application).to receive(:status_changed?).with(to: 'accepted').and_return(true)
            expect(cohort_application).to receive(:registered).and_return(false)
            expect(cohort_application).to receive(:required_to_provide_payment?).and_return(true)
            expect(cohort_application).to receive(:default_payment_grace_period_end_at).and_return(original_payment_grace_period_end_at + 1.week) # just an arbitrary time for sipmler testing
            cohort_application.handle_payment_grace_period
            expect(cohort_application.payment_grace_period_end_at).to eq(original_payment_grace_period_end_at + 1.week)
        end

        it "should set payment_grace_period_end_at to nil if status_changed? to accepted but user hasn't registered yet and they're NOT required to provide payment" do
            expect(cohort_application).to receive(:status_changed?).with(to: 'accepted').and_return(true)
            expect(cohort_application).to receive(:registered).exactly(:twice).and_return(false)
            expect(cohort_application).to receive(:required_to_provide_payment?).and_return(false)
            expect(cohort_application).not_to receive(:default_payment_grace_period_end_at)
            cohort_application.handle_payment_grace_period
            expect(cohort_application.payment_grace_period_end_at).to be_nil
        end

        it "should set payment_grace_period_end_at to nil if status_changed? to something other than accepted and user is not registered" do
            expect(cohort_application).to receive(:status_changed?).with(to: 'accepted').and_return(false)
            expect(cohort_application).to receive(:registered).and_return(false)
            expect(cohort_application).not_to receive(:default_payment_grace_period_end_at)
            cohort_application.handle_payment_grace_period
            expect(cohort_application.payment_grace_period_end_at).to be_nil
        end

        it "should set payment_grace_period_end_at to nil if status did not change and user is not registered" do
            expect(cohort_application).to receive(:status_changed?).with(to: 'accepted').and_return(false)
            expect(cohort_application).to receive(:registered).and_return(false)
            expect(cohort_application).not_to receive(:default_payment_grace_period_end_at)
            cohort_application.handle_payment_grace_period
            expect(cohort_application.payment_grace_period_end_at).to be_nil
        end

        it "should not change payment_grace_period_end_at if status did not change and user is registered" do
            expect(cohort_application).to receive(:status_changed?).with(to: 'accepted').and_return(false)
            expect(cohort_application).to receive(:registered).and_return(true)
            expect(cohort_application).not_to receive(:default_payment_grace_period_end_at)
            cohort_application.handle_payment_grace_period
            expect(cohort_application.payment_grace_period_end_at).to eq(original_payment_grace_period_end_at)
        end
    end

    describe "default_payment_grace_period_end_at" do
        attr_accessor :cohort_application, :registration_deadline, :now

        before(:each) do
            @now = Time.now
            allow(Time).to receive(:now).and_return(now)
            user = users(:accepted_mba_cohort_user)
            @cohort_application = user.accepted_application
        end

        describe "when registration_deadline has not passed" do

            before(:each) do
                @registration_deadline = now + 2.weeks
                expect(@cohort_application.published_cohort).to receive(:registration_deadline).and_return(registration_deadline)
            end

            it "should return time a week after registration_deadline" do
                expect(cohort_application.default_payment_grace_period_end_at).to eq(registration_deadline + 1.week)
            end
        end

        describe "when registration_deadline has passed" do

            before(:each) do
                @registration_deadline = now - 1.second
                expect(@cohort_application.published_cohort).to receive(:registration_deadline).and_return(registration_deadline)
            end

            it "should set payment_grace_period_end_at to a week from now" do
                expect(cohort_application.default_payment_grace_period_end_at).to eq(now + 1.week)
            end
        end
    end

    describe "ensure_payment_grace_period" do

        it "should set payment_grace_period_end_at to default_payment_grace_period_end_at if payment_grace_period_end_at is not already set" do
            cohort_application = CohortApplication.first
            cohort_application.update_column(:payment_grace_period_end_at, nil)
            mock_payment_grace_period_end_at = Time.now
            expect(cohort_application).to receive(:default_payment_grace_period_end_at).and_return(mock_payment_grace_period_end_at)
            cohort_application.ensure_payment_grace_period
            expect(cohort_application.payment_grace_period_end_at).to eq(mock_payment_grace_period_end_at)
        end

        it "should not set payment_grace_period_end_at if it's already set" do
            cohort_application = CohortApplication.first
            mock_payment_grace_period_end_at = Time.now
            cohort_application.update_column(:payment_grace_period_end_at, mock_payment_grace_period_end_at)
            expect(cohort_application).not_to receive(:default_payment_grace_period_end_at)
            cohort_application.ensure_payment_grace_period
            expect(cohort_application.payment_grace_period_end_at).to eq(mock_payment_grace_period_end_at)
        end
    end

    describe "verify_lock_content_access_job" do
        attr_accessor :cohort_application, :payment_grace_period_end_at

        before(:each) do
            @cohort_application = CohortApplication.first
            @payment_grace_period_end_at = Time.now
            @cohort_application.update_column(:payment_grace_period_end_at, @payment_grace_period_end_at)
        end

        it "should do nothing if no payment_grace_period_end_at is present" do
            expect(Delayed::Job.where(queue: 'locked_due_to_past_due_payment').count).to eq(0) # sanity check
            expect(cohort_application).to receive(:payment_grace_period_end_at).and_return(nil)
            cohort_application.verify_lock_content_access_job
            expect(Delayed::Job.where(queue: 'locked_due_to_past_due_payment').count).to eq(0)
        end

        it "should enqueue LockContentAccessDueToPastDuePayment job set to run_at payment_grace_period_end_at if no pending_lock_content_access_jobs exist" do
            expect(cohort_application).to receive(:pending_lock_content_access_jobs).and_return([])
            expect(LockContentAccessDueToPastDuePayment).to receive(:set).with(wait_until: payment_grace_period_end_at).and_call_original
            cohort_application.verify_lock_content_access_job
        end

        it "should update run_at for one of the jobs to payment_grace_period_end_at and destroy the others if multiple pending_lock_content_access_jobs are detected" do
            first_job = LockContentAccessDueToPastDuePayment.perform_later(cohort_application.id)
            second_job = LockContentAccessDueToPastDuePayment.perform_later(cohort_application.id)
            expect(cohort_application).to receive(:pending_lock_content_access_jobs).and_return([first_job, second_job])
            expect(second_job).to receive(:update!).with(run_at: payment_grace_period_end_at)
            expect(first_job).to receive(:destroy)
            cohort_application.verify_lock_content_access_job
        end

        describe "after create" do

            it "should not be called if payment_grace_period_end_at is not present" do
                user = User.first
                user.cohort_applications = [] # ensure we don't hit a unique index constraint error

                expect_any_instance_of(CohortApplication).not_to receive(:verify_lock_content_access_job)
                CohortApplication.create!(
                    user_id: user.id,
                    cohort_id: Cohort.first.id,
                    status: 'pending',
                    applied_at: Time.now
                )
            end

            it "should be called if payment_grace_period_end_at is present" do
                user = User.first
                user.cohort_applications = [] # ensure we don't hit a unique index constraint error

                cohort = cohorts(:published_emba_cohort)
                expect_any_instance_of(CohortApplication).to receive(:verify_lock_content_access_job)
                CohortApplication.create!(
                    user_id: user.id,
                    cohort_id: cohort.id,
                    status: 'pre_accepted',
                    applied_at: Time.now,
                    registered: true,
                    total_num_required_stripe_payments: 0,
                    stripe_plan_id: cohort.stripe_plans.first['id'],
                    payment_grace_period_end_at: Time.now
                )
            end
        end

        describe "after update" do

            it "should not be called if payment_grace_period_end_at is not present" do
                allow(cohort_application).to receive(:payment_grace_period_end_at).and_return(nil)
                expect(cohort_application).not_to receive(:verify_lock_content_access_job)
                cohort_application.save!
            end

            it "should be called if payment_grace_period_end_at is present" do
                allow(cohort_application).to receive(:payment_grace_period_end_at).and_return(Time.now)
                expect(cohort_application).to receive(:verify_lock_content_access_job)
                cohort_application.save!
            end
        end
    end

    describe "pending_lock_content_access_jobs" do

        it "should return a relation containing all pending LockContentAccessDueToPastDuePayment jobs" do
            cohort_application = CohortApplication.first
            result = cohort_application.pending_lock_content_access_jobs
            expect(result.size).to eq(0)

            2.times { LockContentAccessDueToPastDuePayment.set(wait: 1.week).perform_later(cohort_application.id) }

            jobs = Delayed::Job.where(queue: LockContentAccessDueToPastDuePayment.queue_name).where("handler like '%#{cohort_application.id}%'").to_a
            expect(jobs.size).to eq(2) # sanity check

            pending_job = jobs.pop
            locked_job = jobs.pop.update!(locked_at: Time.now)

            pending_jobs = cohort_application.pending_lock_content_access_jobs
            expect(pending_jobs.size).to eq(1)
            expect(pending_jobs.first.id).to eq(pending_job.id)
        end
    end

    describe "destroy_all_pending_lock_content_access_jobs" do

        it "should be called on destroy" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:destroy_all_pending_lock_content_access_jobs)
            cohort_application.destroy
        end

        it "should destroy_all pending LockContentAccessDueToPastDuePayment delayed jobs" do
            cohort_application = CohortApplication.first
            2.times { LockContentAccessDueToPastDuePayment.set(wait: 1.week).perform_later(cohort_application.id) }
            jobs = Delayed::Job.where("queue = 'lock_content_access_due_to_past_due_payment' AND handler like ?", "%#{cohort_application.id}%")
            expect(jobs.size).to eq(2)
            locked_job = jobs.first
            locked_job.update!(locked_at: Time.now)

            cohort_application.destroy_all_pending_lock_content_access_jobs
            jobs = Delayed::Job.where("queue = 'lock_content_access_due_to_past_due_payment' AND handler like ?", "%#{cohort_application.id}%")
            expect(jobs.size).to eq(1)
            expect(jobs.first.id).to eq(locked_job.id)
        end
    end

    describe "should_destroy_all_pending_lock_content_access_jobs?" do
        attr_accessor :cohort_application

        before(:each) do
            @cohort_application = CohortApplication.first
            allow(cohort_application).to receive(:saved_change_to_registered?).and_return(true)
        end

        it "should be called after create" do
            expect_any_instance_of(CohortApplication).to receive(:should_destroy_all_pending_lock_content_access_jobs?)
            CohortApplication.create!(user_id: @user.id, applied_at: Time.now, cohort_id: @mba_cohort.id, status: 'pending')
        end

        it "should be called after save" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:should_destroy_all_pending_lock_content_access_jobs?)
            cohort_application.save!
        end

        it "should be false if only_changing_cohort_slack_room_id?" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:only_changing_cohort_slack_room_id?).and_return(true)
            expect(cohort_application.should_destroy_all_pending_lock_content_access_jobs?).to be(false)
        end

        it "should be true if saved_change_to_payment_grace_period_end_at to nil" do
            expect(cohort_application).to receive(:only_changing_cohort_slack_room_id?).and_return(false)
            expect(cohort_application).to receive(:saved_change_to_payment_grace_period_end_at?).with(to: nil).and_return(true)
            expect(cohort_application.should_destroy_all_pending_lock_content_access_jobs?).to be(true)
        end

        it "should be false if status is 'pre_accepted'" do
            expect(cohort_application).to receive(:only_changing_cohort_slack_room_id?).and_return(false)
            expect(cohort_application).to receive(:status).and_return('pre_accepted')
            expect(cohort_application.should_destroy_all_pending_lock_content_access_jobs?).to be(false)
        end

        it "should be false if status is 'accepted'" do
            expect(cohort_application).to receive(:only_changing_cohort_slack_room_id?).and_return(false)
            expect(cohort_application).to receive(:status).and_return('accepted')
            expect(cohort_application.should_destroy_all_pending_lock_content_access_jobs?).to be(false)
        end

        it "should be true if status is not 'pre_accepted' or 'accepted'" do
            expect(cohort_application).to receive(:only_changing_cohort_slack_room_id?).and_return(false)
            expect(cohort_application).to receive(:status).and_return('foo')
            expect(cohort_application.should_destroy_all_pending_lock_content_access_jobs?).to be(true)
        end
    end

    describe "log_cohort_content_locked_event after_save callback" do

        it "should be called after create" do
            user = User.first
            user.cohort_applications = [] # ensure we don't hit a unique index constraint error

            expect_any_instance_of(CohortApplication).to receive(:log_cohort_content_locked_event)
            CohortApplication.create!(
                user_id: user.id,
                cohort_id: Cohort.first.id,
                status: 'pending',
                applied_at: Time.now
            )
        end

        it "should be called after update" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:log_cohort_content_locked_event)
            cohort_application.update_attribute(:locked_due_to_past_due_payment, true)
        end

        it "should do nothing if not locked_due_to_past_due_payment?" do
            cohort_application = CohortApplication.first

            expect(cohort_application).to receive(:saved_change_to_locked_due_to_past_due_payment?).and_return(true)
            expect(cohort_application).to receive(:locked_due_to_past_due_payment?).and_return(false)
            expect(Event).not_to receive(:create_server_event!)

            cohort_application.log_cohort_content_locked_event
        end

        it "should log cohort:content_locked_due_to_past_due_payment event if locked_due_to_past_due_payment?" do
            cohort_application = CohortApplication.first

            expect(cohort_application).to receive(:saved_change_to_locked_due_to_past_due_payment?).and_return(true)
            expect(cohort_application).to receive(:locked_due_to_past_due_payment?).and_return(true)
            mock_event = double("Event instance")
            expect(Event).to receive(:create_server_event!).with(
                anything,
                cohort_application.user_id,
                'cohort:content_locked_due_to_past_due_payment',
                {}
            ).and_return(mock_event)
            expect(mock_event).to receive(:log_to_external_systems).with(no_args)

            cohort_application.log_cohort_content_locked_event
        end
    end

    describe "should_enqueue_check_in_with_inactive_user_job?" do

        before(:each) do
            @cohort_application = CohortApplication.first
        end

        it "should be false if !saved_change_to_status?" do
            assert_should_enqueue_check_in_with_inactive_user_job(false, {saved_change_to_status: false})
        end

        it "should be false if !indicates_user_should_be_checked_in_with?" do
            assert_should_enqueue_check_in_with_inactive_user_job(false, {indicates_user_should_be_checked_in_with: false})
        end

        it "should be true if all saved_change_to_status? and indicates_user_should_be_checked_in_with?" do
            assert_should_enqueue_check_in_with_inactive_user_job(true)
        end

        def assert_should_enqueue_check_in_with_inactive_user_job(expected_value, opts = {})
            opts = opts.with_indifferent_access

            allow(@cohort_application).to receive(:saved_change_to_status?).and_return(opts.key?(:saved_change_to_status) ? opts['saved_change_to_status'] : true) # stub this to be true by default
            allow(@cohort_application).to receive(:indicates_user_should_be_checked_in_with?).and_return(opts.key?(:indicates_user_should_be_checked_in_with) ? opts['indicates_user_should_be_checked_in_with'] : true) # stub this to be true by default

            expect(@cohort_application.should_enqueue_check_in_with_inactive_user_job?).to be(expected_value)
        end
    end

    describe "indicates_user_should_be_checked_in_with?" do

        before(:each) do
            @cohort_application = CohortApplication.first
        end

        it "should return false if status is not 'accepted'" do
            assert_indicates_user_should_be_checked_in_with(false, {status: 'pending'}) # just needs to be something other than 'accepted'
        end

        it "should return false if completed_at is present" do
            assert_indicates_user_should_be_checked_in_with(false, {completed_at: Time.now})
        end

        it "should return false if program_type_config.supports_checking_in_with_inactive_user? is false" do
            assert_indicates_user_should_be_checked_in_with(false, {supports_checking_in_with_inactive_user: false})
        end

        it "should return true if all conditions are met" do
            assert_indicates_user_should_be_checked_in_with(true)
        end

        def assert_indicates_user_should_be_checked_in_with(expected_value, opts = {})
            opts = opts.with_indifferent_access

            allow(@cohort_application).to receive(:status).and_return(opts['status'] || 'accepted') # stub this to be 'accepted' by default
            allow(@cohort_application).to receive(:completed_at).and_return(opts['completed_at'] || nil) # stub this to be nil by default
            mock_program_type_config = double("Cohort::ProgramTypeConfig instance")
            allow(@cohort_application).to receive(:program_type_config).and_return(mock_program_type_config) # stub this to return a mock program type config
            allow(mock_program_type_config).to receive(:supports_checking_in_with_inactive_user?).and_return(opts.key?(:supports_checking_in_with_inactive_user) ? opts['supports_checking_in_with_inactive_user'] : true) # stub this to be true by default

            expect(@cohort_application.indicates_user_should_be_checked_in_with?).to be(expected_value)
        end
    end

    describe "enqueue_check_in_with_inactive_user_job" do

        before(:each) do
            @cohort_application = CohortApplication.first
        end

        it "should not enqueue a Cohort::CheckInWithInactiveUser delayed job if one already exists" do
            expect(@cohort_application.user).to receive(:check_in_with_inactive_user_job).and_return({}) # doesn't matter what it returns, just so long as it's truthy
            expect(Cohort::CheckInWithInactiveUser).not_to receive(:set)
            @cohort_application.enqueue_check_in_with_inactive_user_job
        end

        describe "when no pending job already exists" do

            it "should enqueue Cohort::CheckInWithInactiveUser delayed job" do
                allow(@cohort_application.user).to receive(:check_in_with_inactive_user_job).and_return(nil)
                now = Time.now
                allow(Time).to receive(:now).and_return(now)
                mock_job = double("Cohort::CheckInWithInactiveUser instance")
                expect(Cohort::CheckInWithInactiveUser).to receive(:set).with(wait_until: now + 2.weeks).and_return(mock_job)
                expect(mock_job).to receive(:perform_later).with(@cohort_application.user_id)
                @cohort_application.enqueue_check_in_with_inactive_user_job
            end
        end

        describe "after create" do

            it "should not be called if !should_enqueue_check_in_with_inactive_user_job?" do
                expect_any_instance_of(CohortApplication).to receive(:should_enqueue_check_in_with_inactive_user_job?).and_return(false)
                expect_any_instance_of(CohortApplication).not_to receive(:enqueue_check_in_with_inactive_user_job)
                CohortApplication.create!(user_id: @user.id, applied_at: Time.now, cohort_id: @mba_cohort.id, status: 'pending')
            end

            it "should be called if should_enqueue_check_in_with_inactive_user_job?" do
                expect_any_instance_of(CohortApplication).to receive(:should_enqueue_check_in_with_inactive_user_job?).and_return(true)
                expect_any_instance_of(CohortApplication).to receive(:enqueue_check_in_with_inactive_user_job)
                CohortApplication.create!(user_id: @user.id, applied_at: Time.now, cohort_id: @mba_cohort.id, status: 'pending')
            end
        end

        describe "after update" do

            it "should not be called if !should_enqueue_check_in_with_inactive_user_job?" do
                expect(@cohort_application).to receive(:should_enqueue_check_in_with_inactive_user_job?).and_return(false)
                expect_any_instance_of(CohortApplication).not_to receive(:enqueue_check_in_with_inactive_user_job)
                @cohort_application.save!
            end

            it "should be called if should_enqueue_check_in_with_inactive_user_job?" do
                expect(@cohort_application).to receive(:should_enqueue_check_in_with_inactive_user_job?).and_return(true)
                expect_any_instance_of(CohortApplication).to receive(:enqueue_check_in_with_inactive_user_job)
                @cohort_application.save!
            end
        end
    end

    describe "destroy_check_in_with_inactive_user_job" do

        it "should be called on destroy" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:destroy_check_in_with_inactive_user_job)
            cohort_application.destroy
        end

        it "should be called after save if should_destroy_check_in_with_inactive_user_job?" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:should_destroy_check_in_with_inactive_user_job?).and_return(true)
            expect(cohort_application).to receive(:destroy_check_in_with_inactive_user_job)
            cohort_application.save!
        end

        it "should destroy_check_in_with_inactive_user_job on user" do
            cohort_application = CohortApplication.first
            expect(cohort_application.user).to receive(:destroy_check_in_with_inactive_user_job)
            cohort_application.destroy_check_in_with_inactive_user_job
        end


        it "should not raise if there is no user" do
            cohort_application = CohortApplication.first
            allow(cohort_application).to receive(:user).and_return(nil)
            expect {
                cohort_application.destroy_check_in_with_inactive_user_job
            }.not_to raise_error
        end

    end

    describe "should_destroy_check_in_with_inactive_user_job?" do
        attr_accessor :cohort_application

        before(:each) do
            @cohort_application = CohortApplication.first
        end

        it "should be called after create" do
            expect_any_instance_of(CohortApplication).to receive(:should_destroy_check_in_with_inactive_user_job?)
            CohortApplication.create!(user_id: @user.id, applied_at: Time.now, cohort_id: @mba_cohort.id, status: 'pending')
        end

        it "should be called after save" do
            expect(cohort_application).to receive(:should_destroy_check_in_with_inactive_user_job?)
            cohort_application.save!
        end

        it "should be false if !saved_change_to_status?" do
            expect(cohort_application).to receive(:saved_change_to_status?).and_return(false)
            expect(cohort_application.should_destroy_check_in_with_inactive_user_job?).to be(false)
        end

        it "should be false if saved_change_to_status? and status is not 'deferred' or 'expelled'" do
            expect(cohort_application).to receive(:saved_change_to_status?).and_return(true)
            expect(cohort_application).to receive(:status).and_return('pending')
            expect(cohort_application.should_destroy_check_in_with_inactive_user_job?).to be(false)
        end

        it "should be false if !saved_change_to_completed_at?" do
            expect(cohort_application).to receive(:saved_change_to_completed_at?).and_return(false)
            expect(cohort_application.should_destroy_check_in_with_inactive_user_job?).to be(false)
        end

        it "should be false if saved_change_to_completed_at? but no completed_at" do
            expect(cohort_application).to receive(:saved_change_to_completed_at?).and_return(true)
            expect(cohort_application).to receive(:completed_at).and_return(nil)
            expect(cohort_application.should_destroy_check_in_with_inactive_user_job?).to be(false)
        end

        it "should be false if saved_change_to_completed_at?, completed_at is present, but program_type_config.supports_checking_in_with_inactive_user? is false" do
            expect(cohort_application).to receive(:saved_change_to_completed_at?).and_return(true)
            expect(cohort_application).to receive(:completed_at).and_return('some_value') # doesn't matter so long as it's truthy
            expect(cohort_application.program_type_config).to receive(:supports_checking_in_with_inactive_user?).and_return(false)
            expect(cohort_application.should_destroy_check_in_with_inactive_user_job?).to be(false)
        end

        it "should be true if saved_change_to_status?, status is 'deferred', and program_type_config.supports_checking_in_with_inactive_user?" do
            expect(cohort_application).to receive(:saved_change_to_status?).and_return(true)
            expect(cohort_application).to receive(:status).and_return('deferred')
            expect(cohort_application.program_type_config).to receive(:supports_checking_in_with_inactive_user?).and_return(true)
            expect(cohort_application.should_destroy_check_in_with_inactive_user_job?).to be(true)
        end

        it "should be true if saved_change_to_status?, status is 'expelled', and program_type_config.supports_checking_in_with_inactive_user?" do
            expect(cohort_application).to receive(:saved_change_to_status?).and_return(true)
            expect(cohort_application).to receive(:status).and_return('expelled')
            expect(cohort_application.program_type_config).to receive(:supports_checking_in_with_inactive_user?).and_return(true)
            expect(cohort_application.should_destroy_check_in_with_inactive_user_job?).to be(true)
        end

        it "should be true if saved_change_to_completed_at?, completed_at is present, and program_type_config.supports_checking_in_with_inactive_user?" do
            expect(cohort_application).to receive(:saved_change_to_completed_at?).and_return(true)
            expect(cohort_application).to receive(:completed_at).and_return('some_value') # doesn't matter so long as it's truthy
            expect(cohort_application.program_type_config).to receive(:supports_checking_in_with_inactive_user?).and_return(true)
            expect(cohort_application.should_destroy_check_in_with_inactive_user_job?).to be(true)
        end
    end

    describe "indicates_user_should_upload_identification_document?" do
        attr_accessor :cohort_application, :cohort

        before(:each) do
            @cohort_application = CohortApplication.first
            @cohort = Cohort.first
        end

        def mock_attrs(opts = {})
            allow(cohort_application).to receive(:published_cohort).and_return(cohort)
            allow(cohort).to receive(:requires_id_upload?).and_return(opts.key?(:requires_id_upload) ? opts[:requires_id_upload] : true)
            allow(cohort_application).to receive(:status).and_return(opts.key?(:status) ? opts[:status] : 'accepted')
            allow(cohort_application).to receive(:graduation_status).and_return(opts.key?(:graduation_status) ? opts[:graduation_status] : 'not_failed')
        end

        it "should return false if !published_cohort.requires_id_upload?" do
            mock_attrs(requires_id_upload: false)
            expect(cohort_application.indicates_user_should_upload_identification_document?).to be(false)
        end

        it "should return false if status is not 'accepted'" do
            mock_attrs(status: 'not_accepted')
            expect(cohort_application.indicates_user_should_upload_identification_document?).to be(false)
        end

        it "should return false if graduation_status is 'failed'" do
            mock_attrs(graduation_status: 'failed')
            expect(cohort_application.indicates_user_should_upload_identification_document?).to be(false)
        end

        it "should return true if requires_id_upload?, 'accepted', and not 'failed'" do
            mock_attrs
            expect(cohort_application.indicates_user_should_upload_identification_document?).to be(true)
        end
    end

    describe "indicates_user_should_upload_transcripts?" do
        attr_accessor :cohort_application, :cohort

        before(:each) do
            @cohort_application = CohortApplication.first
            @cohort = Cohort.first
        end

        def mock_attrs(opts = {})
            allow(cohort_application).to receive(:published_cohort).and_return(cohort)
            allow(cohort).to receive(:supports_document_upload?).and_return(opts.key?(:supports_document_upload) ? opts[:supports_document_upload] : true)
            allow(cohort_application).to receive(:status).and_return(opts.key?(:status) ? opts[:status] : 'accepted')
            allow(cohort_application).to receive(:graduation_status).and_return(opts.key?(:graduation_status) ? opts[:graduation_status] : 'not_failed')
        end

        it "should return false if !published_cohort.supports_document_upload?" do
            mock_attrs(supports_document_upload: false)
            expect(cohort_application.indicates_user_should_upload_transcripts?).to be(false)
        end

        it "should return false if status is not 'accepted'" do
            mock_attrs(status: 'not_accepted')
            expect(cohort_application.indicates_user_should_upload_transcripts?).to be(false)
        end

        it "should return false if graduation_status is 'failed'" do
            mock_attrs(graduation_status: 'failed')
            expect(cohort_application.indicates_user_should_upload_transcripts?).to be(false)
        end

        it "should return true if supports_document_upload?, 'accepted', and not 'failed'" do
            mock_attrs
            expect(cohort_application.indicates_user_should_upload_transcripts?).to be(true)
        end
    end

    describe "indicates_user_should_upload_english_language_proficiency_documents?" do
        attr_accessor :cohort_application, :cohort

        before(:each) do
            @cohort_application = CohortApplication.first
            @cohort = Cohort.first
        end

        def mock_attrs(opts = {})
            mock_program_type_config = double("Cohort::ProgramTypeConfig")
            allow(cohort_application).to receive(:program_type_config).and_return(mock_program_type_config)
            allow(mock_program_type_config).to receive(:requires_english_language_proficiency?).and_return(opts.key?(:requires_english_language_proficiency) ? opts[:requires_english_language_proficiency] : true)
            allow(cohort_application).to receive(:status).and_return(opts.key?(:status) ? opts[:status] : 'accepted')
            allow(cohort_application).to receive(:graduation_status).and_return(opts.key?(:graduation_status) ? opts[:graduation_status] : 'not_failed')
        end

        it "should return false if !program_type_config.requires_english_language_proficiency?" do
            mock_attrs(requires_english_language_proficiency: false)
            expect(cohort_application.indicates_user_should_upload_english_language_proficiency_documents?).to be(false)
        end

        it "should return false if status is not 'pending', 'pre_accepted', or 'accepted'" do
            mock_attrs(status: 'expelled')
            expect(cohort_application.indicates_user_should_upload_english_language_proficiency_documents?).to be(false)
        end

        it "should return false if graduation_status is 'failed'" do
            mock_attrs(graduation_status: 'failed')
            expect(cohort_application.indicates_user_should_upload_english_language_proficiency_documents?).to be(false)
        end

        it "should return true if supports_document_upload?, acceptable_status?, and not 'failed'" do
            mock_attrs
            expect(cohort_application).to receive(:acceptable_status?).and_return(true)
            expect(cohort_application.indicates_user_should_upload_english_language_proficiency_documents?).to be(true)
        end
    end

    describe "handle_invite_to_reapply" do

        it "should mark should_invite_to_reapply if rejecting a pre_accepted user in a supports_invite_to_reapply cohort" do
            application = CohortApplication.where(status: 'pre_accepted').first
            expect(application.published_cohort).to receive(:supports_invite_to_reapply?).and_return(true).at_least(1).times
            expect(application.should_invite_to_reapply).to be(false)
            application.update_attribute(:status, 'rejected')
            expect(application.should_invite_to_reapply).to be(true)
        end

        it "should not mark should_invite_to_reapply if not rejecting" do
            application = CohortApplication.where(status: 'pre_accepted').first
            expect(application.published_cohort).not_to receive(:supports_invite_to_reapply?)
            expect(application.should_invite_to_reapply).to be(false)
            application.update_attribute(:status, 'accepted')
            expect(application.should_invite_to_reapply).to be(false)
        end

        it "should not mark should_invite_to_reapply if not pre_accepted" do
            application = CohortApplication.where(status: 'pending').first
            expect(application.published_cohort).not_to receive(:supports_invite_to_reapply?)
            expect(application.should_invite_to_reapply).to be(false)
            application.update_attribute(:status, 'rejected')
            expect(application.should_invite_to_reapply).to be(false)
        end

        it "should not mark should_invite_to_reapply if not supports_invite_to_reapply" do
            application = CohortApplication.where(status: 'pre_accepted').first
            expect(application.published_cohort).to receive(:supports_invite_to_reapply?).and_return(false).at_least(1).times
            expect(application.should_invite_to_reapply).to be(false)
            application.update_attribute(:status, 'rejected')
            expect(application.should_invite_to_reapply).to be(false)
        end

        it "should unmark should_invite_to_reapply if no longer rejected" do
            application = CohortApplication.where(status: 'rejected').first
            application.update_attribute(:should_invite_to_reapply, true)
            application.update_attribute(:status, 'accepted')
            application.update_attribute(:should_invite_to_reapply, false)
        end

    end

    describe "reset_decision_if_reapplying_to_promotable_program" do
        it "should reset the admissions_decision if the user is changing from status rejected to pending in a promotable program type" do
            promotable_cohort = Cohort.where(program_type: 'the_business_certificate').first
            application = CohortApplication.first
            application.update_columns(
                    cohort_id: promotable_cohort.id,
                    status: "rejected",
                    admissions_decision: "foo",
                    scholarship_level: promotable_cohort.supports_recurring_payments? ? promotable_cohort.scholarship_levels[0] : nil
                )
            expect(application.admissions_decision).to eq("foo") # sanity check
            expect(SyncWithAirtableJob).to receive(:perform_later).with(application.id, SyncWithAirtableJob::generate_fix_decision_action(nil))
            application.update(status: "pending") # this is what would happen if a user reapplies to a promotable cohort
            expect(application.admissions_decision).to be_nil
        end

        it "should not reset the admissions_decision if not a promotable program type" do
            application = CohortApplication.first
            application.update_columns(cohort_id: @mba_cohort.id, status: "rejected", admissions_decision: "foo")
            expect(application.admissions_decision).to eq("foo") # sanity check
            application.update(status: "pending")
            expect(application.admissions_decision).to eq("foo")
        end

        it "should not reset the admissions_decision if not changing from rejected to pending" do
            promotable_cohort = Cohort.where(program_type: 'the_business_certificate').first
            application = CohortApplication.first
            application.update_columns(cohort_id: promotable_cohort.id, status: "rejected", admissions_decision: "foo")
            expect(application.admissions_decision).to eq("foo") # sanity check
            application.update(status: "deferred")
            expect(application.admissions_decision).to eq("foo")
        end

        it "should reset the admissions_decision but not queue a sync_with_airtable job if program type does not support airtable sync" do
            promotable_cohort = Cohort.where(program_type: 'the_business_certificate').first
            application = CohortApplication.first
            application.update_columns(
                    cohort_id: promotable_cohort.id,
                    status: "rejected",
                    admissions_decision: "foo",
                    scholarship_level: promotable_cohort.supports_recurring_payments? ? promotable_cohort.scholarship_levels[0] : nil
                )
            expect(application.admissions_decision).to eq("foo") # sanity check
            allow(application).to receive(:supports_airtable_sync?).and_return(false)
            expect(SyncWithAirtableJob).not_to receive(:perform_later)
            application.update(status: "pending") # this is what would happen if a user reapplies to a promotable cohort
            expect(application.admissions_decision).to be_nil
        end
    end

    describe "fix_reject_for_no_interview_if_conducted" do
        it "should change the admissions_decision to 'Reject' if 'Reject for No Interview' and we detect an interview was conducted" do
            application = CohortApplication.first
            application.update_columns(admissions_decision: "Reject for No Interview", rubric_interview: "Foo")
            expect(SyncWithAirtableJob).to receive(:perform_later).with(application.id, SyncWithAirtableJob.generate_fix_decision_action("Reject"))
            application.update!(rubric_interview: "Conducted")
            expect(application.reload.admissions_decision).to eq("Reject") # be sure to reload to check persistance of the new decision
        end

        it "should not change the admissions_decision if not 'Conducted'" do
            application = CohortApplication.first
            application.update_columns(admissions_decision: "Reject for No Interview", rubric_interview: "Foo")
            expect(SyncWithAirtableJob).not_to receive(:perform_later)
            application.update!(rubric_interview: "Bar")
            expect(application.admissions_decision).to eq("Reject for No Interview")
        end

        it "should not change the admissions_decision if not 'Reject for No Interview'" do
            application = CohortApplication.first
            application.update_columns(rubric_interview: "Conducted")
            expect(SyncWithAirtableJob).not_to receive(:perform_later)
            application.update!(admissions_decision: "Reject for some reason")
            expect(application.admissions_decision).to eq("Reject for some reason")
        end

        it "should not change the admissions_decision and should log to Sentry if program type does not support airtable sync" do
            application = CohortApplication.first
            application.update_columns(admissions_decision: "Reject for No Interview", rubric_interview: "Conducted")
            allow(application).to receive(:supports_airtable_sync?).and_return(false)
            expect(SyncWithAirtableJob).not_to receive(:perform_later)
            expect(Raven).to receive(:capture_exception).with('Trying to fix_reject_for_no_interview_if_conducted for an application that does not support airtable sync', {
                extra: {
                    application_id: application.id
                }
            })
            application.send(:fix_reject_for_no_interview_if_conducted)
            expect(application.admissions_decision).to eq('Reject for No Interview')
        end

        it "should not trigger a perform_admissions_decision_job" do
            application = CohortApplication.first
            application.update_columns(admissions_decision: "Reject for No Interview", rubric_interview: "Foo")
            expect(SyncWithAirtableJob).to receive(:perform_later)
            expect(PerformAdmissionsDecisionJob).not_to receive(:perform_later)
            application.update!(rubric_interview: "Conducted")
        end
    end

    describe "check_if_json_changed" do
        before(:each) do
            @cohort_application = CohortApplication.first
        end

        it "should return :destroyed if record in database has been destroyed" do
            result = @cohort_application.check_if_json_changed do
                CohortApplication.find(@cohort_application.id).destroy!
            end
            expect(result).to be(:destroyed)
        end

        it "should return :unchanged if the record, and associated records, did not change" do
            result = @cohort_application.check_if_json_changed do
                1 + 1
            end
            expect(result).to be(:unchanged)
        end

        describe "changed" do
            it "should return :changed if the application changed" do
                result = @cohort_application.check_if_json_changed do
                    CohortApplication.find(@cohort_application.id).touch
                end
                expect(result).to be(:changed)
            end

            it "should return :changed if the user changed" do
                result = @cohort_application.check_if_json_changed do
                    User.find(@cohort_application.user_id).touch
                end
                expect(result).to be(:changed)
            end

            it "should return :changed if the career_profile changed" do
                result = @cohort_application.check_if_json_changed do
                    CareerProfile.find(@cohort_application.user.career_profile.id).touch
                end
                expect(result).to be(:changed)
            end
        end
    end

    describe "handle_id_verifications_migration" do

        attr_reader :application, :old_cohort, :new_cohort

        def defer_into_new_cohort
            CohortApplication.create!(
                user_id: application.user_id,
                cohort_id: new_cohort.id,
                applied_at: Time.now,
                status: 'pre_accepted'
            )
        end

        describe "when a previous cohort has verification records associated" do

            before(:each) do
                @application = CohortApplication.find_by_status('deferred')

                # delete other applications to prevent dupe key errors
                (application.user.cohort_applications - [application]).each(&:destroy)

                @old_cohort = application.cohort
                old_cohort.assign_attributes(
                    start_date: Time.now - 25.days,
                    id_verification_periods: [
                        {start_date_days_offset: 0, due_date_days_offset: 10},
                        {start_date_days_offset: 11, due_date_days_offset: 20},
                        {start_date_days_offset: 21, due_date_days_offset: 30},
                        {start_date_days_offset: 31, due_date_days_offset: 40}
                    ]
                )
                old_cohort.publish!

                @new_cohort = Cohort.where.not(id: application.cohort_id).first
                new_cohort.assign_attributes(
                    start_date: Time.now - 25.days,
                    id_verification_periods: [
                        {start_date_days_offset: 0, due_date_days_offset: 10},
                        {start_date_days_offset: 11, due_date_days_offset: 20},
                        {start_date_days_offset: 21, due_date_days_offset: 30},
                        {start_date_days_offset: 31, due_date_days_offset: 40}
                    ]
                )
                new_cohort.publish!
            end

            it "should create verifications only for previous periods if the user does not have enough verifications" do
                create_user_id_verifications(2)
                defer_into_new_cohort

                # the two past periods get verification records.  The last one does not
                expect(application.user.user_id_verifications.where(cohort_id: new_cohort.id).pluck('id_verification_period_index')).to match_array([0,1])
            end

            it "should create verifications for the last period if the user has enough verifications" do
                create_user_id_verifications(3)
                defer_into_new_cohort

                # the two past periods get verification records.  The last one does not
                actual_indexes =  application.user.user_id_verifications.where(cohort_id: new_cohort.id).pluck('id_verification_period_index')
                expect(actual_indexes).to match_array([0,1,2])
            end

            def create_user_id_verifications(count)
                1.upto(count) do |i|
                    index = i-1
                    UserIdVerification.create!(
                        cohort_id: old_cohort.id,
                        user_id: application.user_id,
                        id_verification_period_index: index,
                        verified_at: Time.now,
                        verification_method: 'verified_by_admin',
                        verifier_id: SecureRandom.uuid,
                        verifier_name: 'Mr. Foo'
                    )
                end
            end


        end

        describe "when the user is a legacy identity_verified user" do

            before(:each) do
                @application = CohortApplication.find_by_status('deferred')
                application.user.update(identity_verified: true)

                # delete other applications to prevent dupe key errors
                (application.user.cohort_applications - [application]).each(&:destroy)

                @old_cohort = application.cohort
                old_cohort.assign_attributes(
                    start_date: Time.now - 25.days,
                    id_verification_periods: nil
                )
                old_cohort.publish!

                @new_cohort = Cohort.where.not(id: application.cohort_id).first
                new_cohort.assign_attributes(
                    start_date: Time.now - 25.days,
                    id_verification_periods: [
                        {start_date_days_offset: 0, due_date_days_offset: 10},
                        {start_date_days_offset: 11, due_date_days_offset: 20},
                        {start_date_days_offset: 21, due_date_days_offset: 30},
                        {start_date_days_offset: 31, due_date_days_offset: 40}
                    ]
                )
                new_cohort.publish!
            end

            it "should create verifications for all verifications" do
                defer_into_new_cohort
                expect(application.user.user_id_verifications.where(cohort_id: new_cohort.id).pluck('id_verification_period_index')).to match_array([0,1,2,3])
            end

        end

    end

    describe "waive_verification_for_late_enrollment" do

        it "should not create new UserIdVerification record for period that already has a verification record" do
            application = CohortApplication.find_by_status('deferred')

            cohort = application.cohort
            cohort.assign_attributes(
                start_date: Time.now - 25.days,
                id_verification_periods: [
                    {start_date_days_offset: 0, due_date_days_offset: 10},
                    {start_date_days_offset: 11, due_date_days_offset: 20},
                    {start_date_days_offset: 21, due_date_days_offset: 30},
                    {start_date_days_offset: 31, due_date_days_offset: 40}
                ]
            )
            cohort.publish!

            expect(application.user.user_id_verifications.size).to eq(0)

            verification_record = UserIdVerification.create!(
                cohort_id: cohort.id,
                user_id: application.user_id,
                id_verification_period_index: 0,
                verified_at: Time.now,
                verification_method: 'verified_by_admin',
                verifier_id: SecureRandom.uuid,
                verifier_name: 'Mr. Foo'
            )

            expect(application.reload.user.user_id_verifications.size).to eq(1)
            expect {
                application.waive_verification_for_late_enrollment(cohort.id_verification_periods)
            }.not_to raise_error # should avoid unique constraint error on user_id, cohort_id, and id_verification_period_index
            expect(application.reload.user.user_id_verifications.size).to eq(4)
            expect(application.user.user_id_verifications.pluck(:id)).to include(verification_record.id)
        end
    end

    describe "transfer and backfill progress" do
        attr_accessor :old_cohort, :new_cohort, :cohort_application, :cohort_application_attrs

        describe "transfer_progress_from_deferred_or_expelled_cohort" do
            it "should do nothing if not accepted or pre-accepted" do
                cohort_application = CohortApplication.where.not(cohort_id: cohorts(:published_mba_cohort).id).first
                cohort_application.status = 'not_accepted'
                expect(CohortApplication).not_to receive(:transfer_stream_progress_for_equivalent_streams)
                cohort_application.transfer_progress_from_deferred_or_expelled_cohort
            end

            it "should do nothing when no previous applications" do
                cohort_application = CohortApplication.where.not(cohort_id: cohorts(:published_mba_cohort).id).first
                (cohort_application.user.cohort_applications - [cohort_application]).map(&:delete)
                expect(cohort_application).not_to receive(:transfer_stream_progress_for_equivalent_streams)
                cohort_application.transfer_progress_from_deferred_or_expelled_cohort
            end

            it "should do nothing when previous applications are not expelled or deferred" do
                cohort_application = CohortApplication.where.not(cohort_id: cohorts(:published_mba_cohort).id).first
                (cohort_application.user.cohort_applications - [cohort_application]).map(&:delete)
                previous_application = CohortApplication.create!(
                    user_id: cohort_application.user_id,
                    applied_at: cohort_application.applied_at - 1.year,
                    status: 'rejected',
                    cohort_id: Cohort.all_published.where(program_type: cohort_application.program_type).where.not(id: cohort_application.cohort_id).first['id']
                )
                cohort_application.user.cohort_applications.reload
                expect(cohort_application).not_to receive(:transfer_stream_progress_for_equivalent_streams)
                cohort_application.transfer_progress_from_deferred_or_expelled_cohort
            end

            describe "when deferred or expelled from a previous cohort" do

                before(:each) do
                    setup_old_and_new_cohorts
                end

                it "should not try to transfer exams for a non-mba cohort" do
                    expect(cohort_application).to receive(:program_type).at_least(1).times.and_return('non_mba')
                    # Probability Fundamentals, Data and Decisions Exam, Marketing & Pricing Exam,
                    # Finance Exam, Advanced Finance Exam, and Strategy & Innovation Exam
                    expect(cohort_application).to receive(:transfer_stream_progress_for_equivalent_streams).exactly(6).times
                    expect(cohort_application).not_to receive(:transfer_exam_progress)
                    cohort_application.transfer_progress_from_deferred_or_expelled_cohort
                end

                it "should call transfer_exam_progress" do

                    stream_lpids_to_force_if_new_progress = [SecureRandom.uuid]

                    # midterm is complete and gets transferred.  Final is not
                    expect(cohort_application).to receive(:transfer_exam_progress)
                        .with(:midterm_exam_locale_pack_id, new_cohort, [old_cohort.published_version], stream_lpids_to_force_if_new_progress) do |a,b,c,d|
                            true
                        end
                    expect(cohort_application).to receive(:transfer_exam_progress).with(:final_exam_locale_pack_id, new_cohort, [old_cohort.published_version], stream_lpids_to_force_if_new_progress).and_return(false)

                    # we don't log an event if we only transferred the midterm and / or final
                    expect(Event).not_to receive(:create_server_event!)
                    cohort_application.transfer_progress_from_deferred_or_expelled_cohort(stream_lpids_to_force_if_new_progress)
                end

                it "should not blow up trying to transfer exam stuff if previous cohort is emba" do
                    emba_cohort = cohorts(:published_emba_cohort)
                    CohortApplication.create!(cohort_application_attrs.merge({cohort_id: emba_cohort.id}))
                    expect(cohort_application).to receive(:equivalent_stream_locale_pack_ids).and_return([])
                    expect(cohort_application).to receive(:transfer_stream_progress_for_equivalent_streams)
                        .with([new_cohort.midterm_exam_locale_pack_id], false)
                    expect(emba_cohort).not_to receive(:midterm_exam_locale_pack_id)
                    expect(emba_cohort).not_to receive(:final_exam_locale_pack_id)
                    cohort_application.transfer_progress_from_deferred_or_expelled_cohort
                end

                it "should transfer_stream_progress_for_equivalent_streams for specific streams" do
                    expect(cohort_application).to receive(:equivalent_stream_locale_pack_ids).and_return([[
                        'f9140971-4b98-4578-9f14-53986f02eb7b',
                        '9b20b827-a7b2-4203-93d8-5c918352e34e'
                    ], [
                        'a846219d-3dcd-49e6-a6a6-29935326e944',
                        '549ec44a-56e8-454e-866b-bba7424e70b9'
                    ]])
                    expect(cohort_application).to receive(:transfer_stream_progress_for_equivalent_streams)
                        .with(['f9140971-4b98-4578-9f14-53986f02eb7b', '9b20b827-a7b2-4203-93d8-5c918352e34e'], false)
                        .once
                    expect(cohort_application).to receive(:transfer_stream_progress_for_equivalent_streams)
                        .with(['a846219d-3dcd-49e6-a6a6-29935326e944', '549ec44a-56e8-454e-866b-bba7424e70b9'], false)
                        .once
                    cohort_application.transfer_progress_from_deferred_or_expelled_cohort
                end

                it "should force transfer_stream_progress_for_equivalent_streams if stream included in stream_lpids_to_force_if_new_progress" do
                    expect(cohort_application).to receive(:transfer_stream_progress_for_equivalent_streams)
                        .with(['f9140971-4b98-4578-9f14-53986f02eb7b', '9b20b827-a7b2-4203-93d8-5c918352e34e'], true)
                        .once
                    cohort_application.transfer_progress_from_deferred_or_expelled_cohort(['9b20b827-a7b2-4203-93d8-5c918352e34e'])
                end

                it "should not force transfer_stream_progress_for_equivalent_streams if stream is not included in stream_lpids_to_force_if_new_progress" do
                    expect(cohort_application).to receive(:transfer_stream_progress_for_equivalent_streams)
                        .with(['f9140971-4b98-4578-9f14-53986f02eb7b', '9b20b827-a7b2-4203-93d8-5c918352e34e'], false)
                        .once
                    cohort_application.transfer_progress_from_deferred_or_expelled_cohort([SecureRandom.uuid])
                end

                it "should include transferred_courses in event" do
                    expect(cohort_application).to receive(:equivalent_stream_locale_pack_ids).and_return([[
                        'f9140971-4b98-4578-9f14-53986f02eb7b',
                        '9b20b827-a7b2-4203-93d8-5c918352e34e'
                    ]])
                    expect(cohort_application).to receive(:transfer_stream_progress_for_equivalent_streams)
                        .with(['f9140971-4b98-4578-9f14-53986f02eb7b', '9b20b827-a7b2-4203-93d8-5c918352e34e'], false)
                        .once
                        .and_return({from: {:id => 'old_stream_1'}, :to => {:id => 'new_stream_1'}})

                    expect {
                        cohort_application.transfer_progress_from_deferred_or_expelled_cohort
                    }.to change {Event.count}.by(1)
                    expect(Event.reorder(:created_at).last.payload['transferred_courses']).to match_array(
                        [
                            {"from"=>{"id"=>"old_stream_1"}, "to"=>{"id"=>"new_stream_1"}}
                        ]
                    )
                end

                it "should not include exams from equivalent_stream_locale_pack_ids in event" do
                    expect(cohort_application).to receive(:equivalent_stream_locale_pack_ids).and_return([[
                        'f9140971-4b98-4578-9f14-53986f02eb7b',
                        '9b20b827-a7b2-4203-93d8-5c918352e34e'
                    ]])
                    expect(cohort_application).to receive(:transfer_stream_progress_for_equivalent_streams)
                        .with(['f9140971-4b98-4578-9f14-53986f02eb7b', '9b20b827-a7b2-4203-93d8-5c918352e34e'], false)
                        .once
                        .and_return({from: {:id => 'old_stream_1'}, :to => {:id => 'new_stream_1', :exam => true}})

                    expect {
                        cohort_application.transfer_progress_from_deferred_or_expelled_cohort
                    }.not_to change {Event.count}
                end

                it "should not log an event if nothing was backfilled or transferred" do
                    cohort_application.user.lesson_streams_progresses.destroy_all # ensure nothing gets backfilled
                    allow(cohort_application).to receive(:transfer_stream_progress_for_equivalent_streams).and_return(nil)
                    expect(Event).not_to receive(:create_server_event!)
                    cohort_application.transfer_progress_from_deferred_or_expelled_cohort
                end
            end
        end

        describe "transfer_exam_progress" do
            attr_reader :exam_id_1, :exam_id_2

            before(:each) do
                setup_old_and_new_cohorts
                @exam_id_1, @exam_id_2 = [SecureRandom.uuid, SecureRandom.uuid]
                allow_any_instance_of(Cohort).to receive(:some_exam_locale_pack_id) do |instance|
                    {
                        old_cohort['id'] => @exam_id_1,
                        new_cohort['id'] => @exam_id_2
                    }[instance['id']]
                end
            end

            it "should add course to transferred_courses if transferred" do
                expect(cohort_application).to receive(:transfer_stream_progress_for_equivalent_streams)
                    .with([exam_id_1, exam_id_2], false)
                    .and_return(:event_payload)

                transferred_courses = []
                result = cohort_application.send(:transfer_exam_progress, :some_exam_locale_pack_id, new_cohort, [old_cohort], [])

                expect(result).to be(true)
                expect(transferred_courses).to eq([]) # exams aren't sent to customer.io
            end

            it "should not add course to transferred_courses if not transferred" do
                expect(cohort_application).to receive(:transfer_stream_progress_for_equivalent_streams)
                    .with([exam_id_1, exam_id_2], false)
                    .and_return(nil)

                transferred_courses = []
                result = cohort_application.send(:transfer_exam_progress, :some_exam_locale_pack_id, new_cohort, [old_cohort], [])

                expect(result).to be(false)
                expect(transferred_courses).to eq([])
            end

            it "should respect stream_lpids_to_force_if_new_progress" do
                stream_lpids_to_force_if_new_progress = [exam_id_2]

                expect(cohort_application).to receive(:transfer_stream_progress_for_equivalent_streams)
                    .with([exam_id_1, exam_id_2], true) # true since exam_id_2 is in stream_lpids_to_force_if_new_progress
                    .and_return(:event_payload)
                transferred_courses = []

                cohort_application.send(:transfer_exam_progress, :some_exam_locale_pack_id, new_cohort, [old_cohort], stream_lpids_to_force_if_new_progress)
            end

        end

        describe "transfer_stream_progress_for_equivalent_streams" do

            it "should do nothing if stream is not in this cohort is nil" do
                cohort_application = CohortApplication.first
                expect(cohort_application.user).not_to receive(:lesson_streams_progress)
                expect(cohort_application).to receive(:stream_locale_pack_ids).and_return([])
                cohort_application.send(:transfer_stream_progress_for_equivalent_streams, [Lesson::Stream.first.locale_pack_id])
            end

            it "should do nothing if the new cohort has the same stream" do
                cohort_application = CohortApplication.first
                expect(cohort_application.user).not_to receive(:lesson_streams_progress)
                same_locale_pack_id = Lesson::Stream.first.locale_pack_id
                expect(cohort_application).to receive(:stream_locale_pack_ids).and_return([same_locale_pack_id])
                cohort_application.send(:transfer_stream_progress_for_equivalent_streams, [same_locale_pack_id])
            end

            it "should do nothing if the user did not finish the old stream" do
                new_cohort_application, old_cohort_application = setup_transferrable_mba_exams
                add_progress_on_midterm(old_cohort_application) # no completed_at
                expect(Lesson::StreamProgress).not_to receive(:new)
                new_cohort_application.send(:transfer_stream_progress_for_equivalent_streams, [
                    old_cohort_application.published_cohort.midterm_exam_locale_pack_id,
                    new_cohort_application.published_cohort.midterm_exam_locale_pack_id
                ])
            end

            it "should do nothing if the user already has progress on the new stream and not force_if_new_progress" do
                new_cohort_application, old_cohort_application = setup_transferrable_mba_exams
                add_progress_on_midterm(old_cohort_application, {
                    completed_at: Time.now - 1.day
                })
                add_progress_on_midterm(new_cohort_application)
                expect(Lesson::StreamProgress).not_to receive(:new)
                new_cohort_application.user.lesson_streams_progresses.reload
                new_cohort_application.send(:transfer_stream_progress_for_equivalent_streams,
                    [
                        old_cohort_application.published_cohort.midterm_exam_locale_pack_id,
                        new_cohort_application.published_cohort.midterm_exam_locale_pack_id
                    ]
                )
            end

            it "should transfer stuff when user already has progress but force_if_new_progress is true" do
                new_cohort_application, old_cohort_application = setup_transferrable_mba_exams
                add_progress_on_midterm(old_cohort_application, {
                    completed_at: Time.now - 1.day
                })
                add_progress_on_midterm(new_cohort_application)
                new_cohort_application.user.lesson_streams_progresses.reload

                now = Time.now
                allow(Time).to receive(:now).and_return(now)
                new_midterm_lpid = new_cohort_application.published_cohort.midterm_exam_locale_pack_id

                new_cohort_application.send(:transfer_stream_progress_for_equivalent_streams,
                    [
                        old_cohort_application.published_cohort.midterm_exam_locale_pack_id,
                        new_midterm_lpid
                    ],
                    true
                )
                new_stream_progress = new_cohort_application.user.lesson_streams_progresses.where(locale_pack_id: new_midterm_lpid).first
                expect(new_stream_progress.completed_at.to_timestamp).to eq(now.to_timestamp)
                expect(new_stream_progress.waiver).to eq(Lesson::StreamProgress::WAIVER_EQUIVALENT_STREAM_ALREADY_COMPLETED)
            end

            it "should transfer stuff when exam" do
                new_cohort_application, old_cohort_application = setup_transferrable_mba_exams
                old_stream_progress = add_progress_on_midterm(old_cohort_application, {
                    completed_at: Time.now - 1.day,
                    official_test_score: 0.42
                })
                new_cohort_application.user.lesson_streams_progresses.reload
                expect_any_instance_of(Lesson::StreamProgress).to receive(:generate_certificate_image!).and_call_original
                expect(Cohort::ReportExamResultJob).not_to receive(:set) # check on skip_exam_events
                new_cohort_application.send(:transfer_stream_progress_for_equivalent_streams,
                    [
                        old_cohort_application.published_cohort.midterm_exam_locale_pack_id,
                        new_cohort_application.published_cohort.midterm_exam_locale_pack_id
                    ]
                )

                new_stream_progress = new_cohort_application.user.lesson_streams_progresses.find_by_locale_pack_id(new_cohort_application.cohort.midterm_exam_locale_pack_id)

                expect(new_stream_progress).not_to be_nil
                expect(new_stream_progress.official_test_score).to eq(old_stream_progress.official_test_score)
                expect(new_stream_progress.completed_at).to be_within(1.second).of(old_stream_progress.completed_at)
                expect(new_stream_progress.waiver).to eq(Lesson::StreamProgress::WAIVER_EQUIVALENT_STREAM_ALREADY_COMPLETED)
            end

            it "should not mark stream_progress as waived if the user has already completed all the lessons" do
                new_cohort_application, old_cohort_application = setup_transferrable_mba_exams
                add_progress_on_midterm(old_cohort_application, {
                    completed_at: Time.now - 1.day
                })
                new_cohort_application.user.lesson_streams_progresses.reload
                new_midterm_lpid = new_cohort_application.published_cohort.midterm_exam_locale_pack_id

                expect_any_instance_of(Lesson::StreamProgress).to receive(:all_lessons_complete?).and_return(true)
                new_cohort_application.send(:transfer_stream_progress_for_equivalent_streams,
                    [
                        old_cohort_application.published_cohort.midterm_exam_locale_pack_id,
                        new_midterm_lpid
                    ]
                )

                stream_progress = Lesson::StreamProgress.where(user_id: new_cohort_application.user.id, locale_pack_id: new_midterm_lpid).first
                expect(stream_progress.waiver).to be_nil
            end
        end

        describe "backfill" do
            it "should mark required streams up to the midterm complete if midterm complete when deferring" do
                new_cohort_application, old_cohort_application = setup_transferrable_mba_exams
                new_cohort, old_cohort = [new_cohort_application, old_cohort_application].map {|a| a.cohort}
                expect(new_cohort_application.user.lesson_streams_progresses.size).to be(0) # sanity check

                # Figure out the stream_entries we expect to be completed
                stream_entries_up_to_midterm = []
                required_stream_entries_up_to_midterm = []
                new_cohort.periods.each do |p|
                    stream_entries_up_to_midterm += p['stream_entries']
                    required_stream_entries_up_to_midterm += p['stream_entries'].select { |e| e['required'] }
                    break if p['style'] == 'exam' && p['exam_style'] == 'intermediate'
                end

                # Sanity checks
                expect(required_stream_entries_up_to_midterm.size).to be > 0
                expect(stream_entries_up_to_midterm.size).to be > required_stream_entries_up_to_midterm.size # cause of optional

                # Give the user a completed midterm on the old application
                add_progress_on_midterm(old_cohort_application, {
                    completed_at: Time.now - 1.day
                })

                # Arbitrarily add progress on one of the new required streams to simulate
                # the user having already completed a stream that would have been backfilled
                arbitrarily_completed_at = Time.now - 1.year
                Lesson::StreamProgress.create!({
                    user_id: new_cohort_application.user_id,
                    locale_pack_id: required_stream_entries_up_to_midterm.first['locale_pack_id'],
                    started_at: Time.now - 1.month,
                    completed_at: arbitrarily_completed_at
                })

                # Sanity check
                expect(new_cohort_application.user.reload.lesson_streams_progresses.size).to be(2)

                # Test the event getting sent to customer.io
                mock_event = {}
                expect(mock_event).to receive(:log_to_external_systems)
                expect(Event).to receive(:create_server_event!) do |id, user_id, event_type, payload|
                    expect(user_id).to eq(new_cohort_application.user_id)
                    expect(event_type).to eq('cohort:transferred_progress')
                    expect(payload[:midterm_complete]).to eq(true)
                    expect(payload[:final_complete]).to eq(false)

                    # only the midterm was transferred, but we don't add exams to the event
                    expect(payload[:transferred_courses].size).to be(0)

                    # required, uncompleted courses were backfilled (minus midterm, and that arbitrarily completed one)
                    expect(payload[:backfilled_courses].size).to be(required_stream_entries_up_to_midterm.size - 2)

                    mock_event
                end

                # Call the transfer logic explicitly
                allow_any_instance_of(Lesson::StreamProgress).to receive(:all_lessons_complete?).and_return(false)
                new_cohort_application.send(:transfer_progress_from_deferred_or_expelled_cohort)

                # Check that all required streams before the midterm are completed
                required_stream_entries_up_to_midterm.each do |stream_entry|
                    progress = Lesson::StreamProgress.where(
                        user_id: new_cohort_application.user_id,
                        locale_pack_id: stream_entry['locale_pack_id']
                    ).first
                    expect(progress.completed_at).not_to be_nil
                end

                new_cohort_application.user.reload

                # After destroying all progress and making a completed progress on the midterm,
                # we should expect the number of progresses that are now complete after
                # the backfill
                expected_stream_progresses = required_stream_entries_up_to_midterm.size + 1 # plus the old midterm
                expect(new_cohort_application.user.lesson_streams_progresses.size).to be(expected_stream_progresses)

                required_stream_entries_up_to_midterm.each_with_index do |stream_entry, i|
                    stream_progress = new_cohort_application
                        .user
                        .lesson_streams_progresses
                        .where(locale_pack_id: stream_entry['locale_pack_id'])
                        .first

                    # This is the one we arbitrarily completed already
                    if i == 0
                        expect(stream_progress.completed_at.to_timestamp).to be(arbitrarily_completed_at.to_timestamp)
                        expect(stream_progress.waiver).to be_nil
                    # Exams are transferred
                    elsif stream_progress.lesson_stream.locale_pack_id == new_cohort.midterm_exam_locale_pack_id
                        expect(stream_progress.waiver).to eq(Lesson::StreamProgress::WAIVER_EQUIVALENT_STREAM_ALREADY_COMPLETED)
                    else
                        expect(stream_progress.completed_at).to be > arbitrarily_completed_at
                        expect(stream_progress.waiver).to eq(Lesson::StreamProgress::WAIVER_EXAM_ALREADY_COMPLETED)
                    end
                end
            end

            it "should mark required streams up to the final complete if final complete when deferring" do
                new_cohort_application, old_cohort_application = setup_transferrable_mba_exams
                new_cohort, old_cohort = [new_cohort_application, old_cohort_application].map {|a| a.cohort}
                expect(new_cohort_application.user.lesson_streams_progresses.size).to be(0) # sanity check

                # Figure out the stream_entries we expect to be completed
                stream_entries_up_to_final = []
                required_stream_entries_up_to_final = []
                new_cohort.periods.each do |p|
                    stream_entries_up_to_final += p['stream_entries']
                    required_stream_entries_up_to_final += p['stream_entries'].select { |e| e['required'] }
                    break if p['style'] == 'exam' && p['exam_style'] == 'final'
                end

                # Sanity checks
                expect(required_stream_entries_up_to_final.size).to be > 0
                expect(stream_entries_up_to_final.size).to be > required_stream_entries_up_to_final.size # cause of optional

                # Give the user a completed midterm on the old application
                # Note: Not strictly necessary, just mimicking an organic progression
                add_progress_on_midterm(old_cohort_application, {
                    completed_at: Time.now - 1.day
                })

                # Give the user a completed final on the old application
                Lesson::StreamProgress.create!({
                    user_id: new_cohort_application.user_id,
                    locale_pack_id: old_cohort.final_exam_locale_pack_id,
                    started_at: Time.now - 1.month,
                    completed_at: Time.now - 1.day
                })

                # Arbitrarily add progress on one of the new required streams to simulate
                # the user having already completed a stream that would have been backfilled
                arbitrarily_completed_at = Time.now - 1.year
                Lesson::StreamProgress.create!({
                    user_id: new_cohort_application.user_id,
                    locale_pack_id: required_stream_entries_up_to_final.second['locale_pack_id'],
                    started_at: Time.now - 1.month,
                    completed_at: arbitrarily_completed_at
                })

                # Sanity check
                expect(new_cohort_application.user.reload.lesson_streams_progresses.size).to be(3)

                # Test the event getting sent to customer.io
                mock_event = {}
                expect(mock_event).to receive(:log_to_external_systems)
                allow_any_instance_of(Lesson::StreamProgress).to receive(:handle_completed_foundations_events) # also logs an event
                expect(Event).to receive(:create_server_event!) do |id, user_id, event_type, payload|
                    expect(user_id).to eq(new_cohort_application.user_id)
                    expect(event_type).to eq('cohort:transferred_progress')
                    expect(payload[:midterm_complete]).to eq(true)
                    expect(payload[:final_complete]).to eq(true)

                    # only midterm and final were transferred, but we don't add exams to the event
                    expect(payload[:transferred_courses].size).to be(0)

                    # required, uncompleted courses were backfilled (minus midterm and final, and that arbitrarily completed one)
                    expect(payload[:backfilled_courses].size).to be(required_stream_entries_up_to_final.size - 3)

                    mock_event
                end

                # Call the transfer logic explicitly
                allow_any_instance_of(Lesson::StreamProgress).to receive(:all_lessons_complete?).and_return(false)
                new_cohort_application.send(:transfer_progress_from_deferred_or_expelled_cohort)

                # Check that all required streams before the midterm are completed
                required_stream_entries_up_to_final.each do |stream_entry|
                    progress = Lesson::StreamProgress.where(
                        user_id: new_cohort_application.user_id,
                        locale_pack_id: stream_entry['locale_pack_id']
                    ).first
                    expect(progress.completed_at).not_to be_nil
                end

                # After destroying all progress and making a completed progress on the final,
                # we should expect the number of progresses that are now complete after
                # the backfill
                expected_stream_progresses = required_stream_entries_up_to_final.size + 2 # plus the old midterm and final
                expect(new_cohort_application.user.reload.lesson_streams_progresses.size).to be(expected_stream_progresses)

                required_stream_entries_up_to_final.each_with_index do |stream_entry, i|
                    stream_progress = new_cohort_application
                        .user
                        .lesson_streams_progresses
                        .where(locale_pack_id: stream_entry['locale_pack_id'])
                        .first

                    # This is the one we arbitrarily completed already
                    if i == 1
                        expect(stream_progress.completed_at.to_timestamp).to be(arbitrarily_completed_at.to_timestamp)
                        expect(stream_progress.waiver).to be_nil
                    # Exams are transferred
                    elsif [new_cohort.midterm_exam_locale_pack_id, new_cohort.final_exam_locale_pack_id]
                            .include?(stream_progress.lesson_stream.locale_pack_id)
                        expect(stream_progress.waiver).to eq(Lesson::StreamProgress::WAIVER_EQUIVALENT_STREAM_ALREADY_COMPLETED)
                    else
                        expect(stream_progress.completed_at).to be > arbitrarily_completed_at
                        expect(stream_progress.waiver).to eq(Lesson::StreamProgress::WAIVER_EXAM_ALREADY_COMPLETED)
                    end
                end
            end

            it "should mark regular streams in a playlist complete if the exam for that playlist is complete" do
                emba_cohort = cohorts(:published_emba_cohort)
                old_cohort = Cohort.where.not(id: emba_cohort.id).first

                user = users(:learner)
                user.lesson_streams_progresses.destroy_all

                # Make sure we have a prior deferred application
                CohortApplication.create!(
                    user_id: user.id,
                    cohort_id: old_cohort.id,
                    status: 'deferred',
                    applied_at: Time.now - 1.days
                )

                # And a new one to the emba
                emba_application = CohortApplication.create!(
                    user_id: user.id,
                    cohort_id: emba_cohort.id,
                    status: 'pre_accepted',
                    applied_at: Time.now
                )

                required_playlist = Playlist.all_published.where(locale_pack_id: emba_cohort.required_playlist_pack_ids.first, locale: 'en').first
                specialization_playlist = Playlist.all_published.where(locale_pack_id: emba_cohort.specialization_playlist_pack_ids.second, locale: 'en').first

                stream_query = Lesson::Stream
                    .all_published
                    .select(:locale_pack_id)
                    .where(locale: 'en')
                    .where.not(locale_pack_id: nil)
                    .reorder(nil)

                regular_streams_pack_ids = stream_query.where(exam: false).limit(10)
                regular_stream_entries = regular_streams_pack_ids.map do |stream|
                    {
                        locale_pack_id: stream.locale_pack_id,
                        description: "Stream #{stream.locale_pack_id} for playlist"
                    }
                end

                exam_stream_entries = stream_query
                    .where(exam: true)
                    .where.not(locale_pack_id: regular_streams_pack_ids.pluck(:locale_pack_id))
                    .limit(2)
                    .map do |stream|
                        {
                            locale_pack_id: stream.locale_pack_id,
                            description: "Exam stream #{stream.locale_pack_id} for playlist"
                        }
                    end

                # Set up our content on the specializations
                required_playlist.stream_entries = regular_stream_entries[0..4] + [exam_stream_entries.first]
                required_playlist.title = 'Testing required playlist for backfill'
                required_playlist.publish!

                specialization_playlist.stream_entries = regular_stream_entries[5..9] + [exam_stream_entries.second]
                specialization_playlist.title = 'Testing required playlist for backfill'
                specialization_playlist.publish!

                emba_cohort.publish!

                expect(required_playlist.stream_entries.size).to be > 4
                expect(specialization_playlist.stream_entries.size).to be > 4

                # Arbitrarily complete a required playlist stream
                arbitrarily_completed_at = Time.now - 1.days
                Lesson::StreamProgress.create!({
                    user_id: user.id,
                    locale_pack_id: required_playlist.stream_entries.second['locale_pack_id'],
                    started_at: Time.now - 1.month,
                    completed_at: arbitrarily_completed_at
                })

                # Complete the required playlist exam
                Lesson::StreamProgress.create!({
                    user_id: user.id,
                    locale_pack_id: required_playlist.stream_entries.last['locale_pack_id'],
                    started_at: Time.now - 1.month,
                    completed_at: arbitrarily_completed_at
                })

                # Arbitrarily complete some specialization playlist streams
                Lesson::StreamProgress.create!({
                    user_id: user.id,
                    locale_pack_id: specialization_playlist.stream_entries.second['locale_pack_id'],
                    started_at: Time.now - 1.month,
                    completed_at: arbitrarily_completed_at
                })

                Lesson::StreamProgress.create!({
                    user_id: user.id,
                    locale_pack_id: specialization_playlist.stream_entries.fourth['locale_pack_id'],
                    started_at: Time.now - 1.month,
                    completed_at: arbitrarily_completed_at
                })

                # Complete the specialization playlist exam
                Lesson::StreamProgress.create!({
                    user_id: user.id,
                    locale_pack_id: specialization_playlist.stream_entries.last['locale_pack_id'],
                    started_at: Time.now - 1.month,
                    completed_at: arbitrarily_completed_at
                })

                expect(user.reload.lesson_streams_progresses.size).to be(5)

                # Test the event getting sent to customer.io
                mock_event = {}
                expect(mock_event).to receive(:log_to_external_systems)
                allow_any_instance_of(Lesson::StreamProgress).to receive(:handle_completed_foundations_events) # also logs an event
                expect(Event).to receive(:create_server_event!) do |id, user_id, event_type, payload|
                    expect(user_id).to eq(emba_application.user_id)
                    expect(event_type).to eq('cohort:transferred_progress')
                    expect(payload[:midterm_complete]).to eq(false)
                    expect(payload[:final_complete]).to eq(false)

                    # Nothing transferred
                    expect(payload[:transferred_courses].size).to be(0)

                    # required, uncompleted courses were backfilled (minus the exams and arbitrarily completed ones)
                    expect(payload[:backfilled_courses].size).to be(required_playlist.stream_entries.size + specialization_playlist.stream_entries.size - 5)

                    mock_event
                end

                allow_any_instance_of(Lesson::StreamProgress).to receive(:all_lessons_complete?).and_return(false)
                emba_application.send(:transfer_progress_from_deferred_or_expelled_cohort)

                expect(user.reload.lesson_streams_progresses.size)
                    .to be(required_playlist.stream_entries.size + specialization_playlist.stream_entries.size)

                emba_application.user.lesson_streams_progresses.each do |progress|
                    stream_pack_id = progress.lesson_stream.locale_pack_id

                    # If it's a stream we preconfigured progress for
                    if stream_pack_id == required_playlist.stream_entries.second['locale_pack_id'] ||
                        stream_pack_id == specialization_playlist.stream_entries.second['locale_pack_id'] ||
                        stream_pack_id == specialization_playlist.stream_entries.fourth['locale_pack_id'] ||
                        stream_pack_id == required_playlist.stream_entries.last['locale_pack_id'] ||
                        stream_pack_id == specialization_playlist.stream_entries.last['locale_pack_id']

                        expect(progress.completed_at.to_timestamp).to be(arbitrarily_completed_at.to_timestamp)
                        expect(progress.waiver).to be(nil)
                    # Otherwise we backfilled it
                    else
                        expect(progress.completed_at).to be > arbitrarily_completed_at
                        expect(progress.waiver).to eq(Lesson::StreamProgress::WAIVER_EXAM_ALREADY_COMPLETED)
                    end
                end
            end

            describe "backfill_stream_progresses_for_incomplete_streams_whose_exam_is_completed" do
                attr_accessor :user, :stream

                def setup_user_for_backfill(all_lessons_complete = false)
                    @user = users(:accepted_mba_cohort_user)
                    @stream = Lesson::Stream.all_published.first
                    user.lesson_streams_progresses.destroy_all
                    user.lesson_progresses.destroy_all
                    expect_any_instance_of(Lesson::StreamProgress).to receive(:all_lessons_complete?).and_return(all_lessons_complete)
                end

                it "should mark stream_progress as waived if the user has not completed all the lessons" do
                    setup_user_for_backfill
                    user.last_application.send(:backfill_stream_progresses_for_incomplete_streams_whose_exam_is_completed, [stream.locale_pack_id])
                    stream_progress = Lesson::StreamProgress.where(user_id: user.id, locale_pack_id: stream.locale_pack_id).first
                    expect(stream_progress.waiver).to eq(Lesson::StreamProgress::WAIVER_EXAM_ALREADY_COMPLETED)
                end

                it "should not mark stream_progress as waived if the user has already completed all the lessons" do
                    setup_user_for_backfill(true)
                    user.last_application.send(:backfill_stream_progresses_for_incomplete_streams_whose_exam_is_completed, [stream.locale_pack_id])
                    stream_progress = Lesson::StreamProgress.where(user_id: user.id, locale_pack_id: stream.locale_pack_id).first
                    expect(stream_progress.waiver).to be_nil
                end
            end
        end

        def setup_transferrable_mba_exams
            old_cohort = cohorts(:published_mba_cohort_with_exams)
            new_cohort = cohorts(:published_mba_cohort)
            new_cohort.periods = old_cohort.periods

            stream_locale_pack_ids = new_cohort.periods.map { |p| p['stream_entries'] }.flatten.map { |e| e['locale_pack_id']}
            other_exams = Lesson::Stream.all_published
                .where.not(locale_pack_id: stream_locale_pack_ids)
                .where(locale: 'en')
                .limit(2)

            # Grab a stream and replace the exam in the new cohort with
            # that stream.  This stream won't necessarily be marked as 'exam', but
            # it doesn't really matter here
            another_midterm = other_exams.first
            midterm_period = new_cohort.periods
                .detect { |p| p['style'] == 'exam' && p['exam_style'] == 'intermediate' }
            midterm_stream_entry = midterm_period['stream_entries'].detect { |e| e['required'] }
            midterm_stream_entry['locale_pack_id'] = another_midterm.locale_pack_id

            another_final = other_exams.second
            final_period = new_cohort.periods
                .detect { |p| p['style'] == 'exam' && p['exam_style'] == 'final' }
            final_stream_entry = final_period['stream_entries'].detect { |e| e['required'] }
            final_stream_entry['locale_pack_id'] = another_final.locale_pack_id

            new_cohort.publish!

            # create an accepted application for the new cohort and a deferred one
            # for the old cohort
            new_cohort_application = CohortApplication.where(cohort_id: new_cohort.id, status: 'accepted').first
            new_cohort_application.user.lesson_streams_progresses.destroy_all
            old_cohort_application = CohortApplication.create!(
                user_id: new_cohort_application.user_id,
                cohort_id: old_cohort.id,
                status: 'deferred',
                applied_at: new_cohort_application.applied_at - 6.months
            )

            RefreshMaterializedContentViews.refresh(true)
            [new_cohort_application, old_cohort_application]
        end

        def add_progress_on_midterm(cohort_application, attrs = {})
            Lesson::StreamProgress.create!({
                user_id: cohort_application.user_id,
                locale_pack_id: cohort_application.cohort.midterm_exam_locale_pack_id,
                started_at: Time.now - 1.month
            }.merge(attrs))
        end

        def setup_old_and_new_cohorts
            @old_cohort = cohorts(:published_mba_cohort_with_exams)
            @new_cohort = cohorts(:published_mba_cohort)
            @new_cohort.periods = old_cohort.periods
            @new_cohort.publish!
            @cohort_application = CohortApplication.where(cohort_id: @new_cohort.id, status: 'pre_accepted').first
            @cohort_application_attrs = {
                user_id: @cohort_application.user_id,
                cohort_id: @old_cohort.id,
                applied_at: @cohort_application.applied_at - 6.months,
                status: 'deferred'
            }
            CohortApplication.create!(cohort_application_attrs)
            allow(cohort_application).to receive(:transfer_stream_progress_for_equivalent_streams)
        end

    end

    describe "accept_on_registration" do

        it "should accept a user on registration if configured to do so" do
            cohort_application = CohortApplication.where(registered: false, status: 'pre_accepted').first
            expect(cohort_application.program_type_config).to receive(:supports_accept_on_registration?).and_return(true)
            cohort_application.update(registered: true)
            expect(cohort_application.reload.status).to eq('accepted')
        end

        it "should not accept a user on registration if not configured to do so" do
            cohort_application = CohortApplication.where(registered: false, status: 'pre_accepted').first
            expect(cohort_application.program_type_config).to receive(:supports_accept_on_registration?).and_return(false)
            cohort_application.update(registered: true)
            expect(cohort_application.reload.status).to eq('pre_accepted')
        end

        it "should not accept a user on registration if not pre_accepted" do
            cohort_application = CohortApplication.where(registered: false, status: 'pending').first
            expect(cohort_application.program_type_config).to receive(:supports_accept_on_registration?).at_least(1).times.and_return(true)
            cohort_application.update(registered: true)
            expect(cohort_application.reload.status).to eq('pending')

            cohort_application.update(status: 'pre_accepted')
            expect(cohort_application.reload.status).to eq('accepted')

        end

    end

    describe "career_profile_fulltext" do

        it "should be updated if status is changing to or from accepted" do
            cohort_application = users(:pending_mba_cohort_user).pending_application

            expect(cohort_application).to receive(:update_career_profile_fulltext).exactly(2).times

            cohort_application.update(status: 'rejected')
            cohort_application.update(status: 'accepted', cohort_slack_room_id: cohort_application.cohort.slack_rooms[0]&.id) # call update_fulltext
            cohort_application.update(status: 'deferred') # call update_fulltext
            cohort_application.update(status: 'expelled')
        end

    end

    describe "identify" do
        it "should call identify on user when calling identify_user" do
            instance = CohortApplication.first
            expect(instance.user).to receive(:identify)
            instance.identify_user
        end

        it "should identify user if created" do
            expect_any_instance_of(CohortApplication).to receive(:identify_user).and_call_original
            CohortApplication.create!({applied_at: Time.now, user: @user, cohort: @mba_cohort})
        end

        it "should identify user if updated" do
            instance = CohortApplication.where(status: 'pending').first
            expect(instance).to receive(:identify_user).and_call_original
            instance.update!(status: 'rejected')
        end

        it "should identify user if destroyed" do
            instance = CohortApplication.first
            expect(instance).to receive(:identify_user).and_call_original
            instance.destroy!
        end

        it "should not identify if user being destroyed" do
            instance = CohortApplication.first
            expect(instance).not_to receive(:identify_user)
            instance.user.destroy!
        end

        it "should not identify if only changing the cohort_slack_room_id" do
            application = CohortApplication.where(cohort_slack_room_id: nil).first
            expect(application).not_to receive(:identify_user)
            application.update!(cohort_slack_room_id: CohortSlackRoom.first.id)
        end

        it "should not identify if only changing the airtable_base_key" do
            application = CohortApplication.where(airtable_base_key: nil).first
            expect(application).not_to receive(:identify_user)
            application.update!(airtable_base_key: 'FOO_KEY')
        end
    end

    describe "get_refund_percent_details" do
        it "should work" do

            # make sure that we have added the new complexity around what it means to
            # have started a playlist
            cohort = cohorts(:published_emba_cohort)
            user = cohort.accepted_users.left_outer_joins(:lesson_streams_progresses).where("lesson_streams_progress.id is null").first
            expect(user).not_to be_nil
            application = user.cohort_applications.find_by_cohort_id(cohort.id)

            foundations_stream_locale_pack_ids, concentrations = setup_content(cohort)

            # with no progress, 100% refund
            assert_refund_percent(application, 1, 0, 0)

            # with only foundations complete, 100% refund
            foundations_stream_locale_pack_ids.each do |locale_pack_id|
                complete_stream(user, locale_pack_id)
            end
            assert_refund_percent(application, 1, 0, 0)

            # with one or more concentrations started, 90%
            concentrations.each_with_index do |playlist, i|
                locale_pack_ids = playlist.stream_locale_pack_ids - foundations_stream_locale_pack_ids
                expect(locale_pack_ids.size).to be > 1
                start_stream(user, locale_pack_ids.first)
                assert_refund_percent(application, 0.9, i+1, 0)
            end

            # with one concentration completed, 75%
            complete_concentration(user, concentrations[0])
            assert_refund_percent(application, 0.75, concentrations.size, 1)

            # with 2 or 3 concentrations completed, 50%
            complete_concentration(user, concentrations[1])
            complete_concentration(user, concentrations[2])
            assert_refund_percent(application, 0.5, concentrations.size, 3)

            # with 4 concentrations completed, 0%
            complete_concentration(user, concentrations[3])
            assert_refund_percent(application, 0, concentrations.size, 4)

        end

        def complete_concentration(user, concentration)
            concentration.stream_locale_pack_ids.each do |locale_pack_id|
                complete_stream(user, locale_pack_id)
            end
        end

        def assert_refund_percent(application, expected_percent, started_concentration_count, completed_concentration_count)
            application.user.lesson_streams_progresses.reload
            expect(application.get_refund_percent_details).to eq({
                refund_percent: expected_percent,
                started_concentration_count: started_concentration_count,
                completed_concentration_count: completed_concentration_count
            })
        end

        def setup_content(cohort)
            foundations_stream_locale_pack_ids = cohort.get_foundations_lesson_stream_locale_pack_ids('en')
            concentrations = Playlist.all_published.where(locale: 'en', locale_pack_id: cohort.required_playlist_pack_ids.slice(1, 999))
            expect(concentrations.size).to be >= 4

            other_stream_locale_pack_ids = concentrations.map(&:stream_locale_pack_ids).flatten - foundations_stream_locale_pack_ids
            unassigned_streams = Lesson::Stream.all_published.where(locale: 'en').where.not(locale_pack_id: foundations_stream_locale_pack_ids + other_stream_locale_pack_ids).to_a

            # ensure every concentraion has one foundations stream and
            # at least 2 non-foundations streams
            concentrations.each do |concentration|
                if (concentration.stream_locale_pack_ids & foundations_stream_locale_pack_ids).empty?
                    concentration.stream_entries << {'locale_pack_id' => foundations_stream_locale_pack_ids.first}
                end

                while (concentration.stream_locale_pack_ids - foundations_stream_locale_pack_ids).size < 2
                    unassigned_stream = unassigned_streams.pop
                    concentration.stream_entries << {'locale_pack_id' => unassigned_stream.locale_pack_id}
                end
                concentration.publish!
            end

            RefreshMaterializedContentViews.refresh
            Rails.cache.clear
            [foundations_stream_locale_pack_ids, concentrations]
        end

        def start_stream(user, locale_pack_id)
            Lesson::StreamProgress.create_or_update!(
                :user_id => user.id,
                :locale_pack_id => locale_pack_id
            )
        end

        def complete_stream(user, locale_pack_id)
            Lesson::StreamProgress.create_or_update!(
                :user_id => user.id,
                :locale_pack_id => locale_pack_id,
                :complete => true
            )
        end
    end

    describe "refund_details" do

        it "should be nil for non-emba user" do
            cohort_application = cohorts(:published_mba_cohort).cohort_applications.first
            expect(cohort_application).not_to receive(:stripe_plan_id)
            expect(cohort_application.refund_details).to be_nil
        end

        it "should be nil if no stripe_plan_id" do
            cohort_application = cohorts(:published_emba_cohort).cohort_applications.first
            cohort_application.stripe_plan_id = nil
            expect(cohort_application.published_cohort).not_to receive(:supports_payments?)
            expect(cohort_application.refund_details).to be_nil
        end

        it "should be nil if cohort does not require payment" do
            cohort_application = cohorts(:published_emba_cohort).cohort_applications.first
            cohort_application.stripe_plan_id = default_plan.id
            expect(cohort_application.published_cohort).to receive(:supports_payments?).and_return(false)
            expect(cohort_application).not_to receive(:get_refund_percent)
            expect(cohort_application.refund_details).to be_nil
        end

        it "should be nil if rejected with no payments" do
            cohort_application = cohorts(:published_emba_cohort).cohort_applications.where(status: 'rejected').first
            cohort_application.stripe_plan_id = default_plan.id
            expect(cohort_application.published_cohort).to receive(:supports_payments?).and_return(true)
            expect(cohort_application).to receive(:total_amount_paid_in_stripe).and_return(0)
            expect(cohort_application).not_to receive(:get_refund_percent)
            expect(cohort_application.refund_details).to be_nil
        end

        it "should be nil if pre_accepted and not registered" do
            cohort_application = cohorts(:published_emba_cohort).cohort_applications.where(status: 'pre_accepted', registered: false).first
            cohort_application.stripe_plan_id = default_plan.id
            expect(cohort_application.published_cohort).to receive(:supports_payments?).and_return(true)
            expect(cohort_application).not_to receive(:get_refund_percent)
            expect(cohort_application.refund_details).to be_nil
        end

        it "should return the correct details" do
            cohort_application = cohorts(:published_emba_cohort).cohort_applications.find_by_status('accepted')
            cohort_application.stripe_plan_id = default_plan.id
            expect(cohort_application).to receive(:total_required_stripe_amount).at_least(1).times.and_return(4200)
            expect(cohort_application).to receive(:total_amount_paid_in_stripe).at_least(1).times.and_return(4200)
            expect(cohort_application).to receive(:get_refund_percent_details).at_least(1).times.and_return({
                refund_percent: 0.5,
                started_concentration_count: 2,
                completed_concentration_count: 3
            })
            expect(cohort_application).to receive(:stripe_plan).at_least(1).times.and_return({'id' => 'stripe_plan_id'})
            expect(cohort_application).to receive(:applicable_coupon_info).at_least(1).times.and_return({'id' => 'coupon_id'})

            # ensure there is no custom message
            cohort_application.refund_attempt_failed = false
            expect(cohort_application).to receive(:has_full_scholarship?).at_least(1).times.and_return(false)
            expect(cohort_application).to receive(:promised_payment_outside_of_stripe?).at_least(1).times.and_return(false)
            cohort_application.refund_issued_at = nil

            expect(cohort_application.refund_details).to eq({
                custom_message: nil,
                can_issue_refund: true,
                total_required_stripe_amount: cohort_application.total_required_stripe_amount,
                total_tuition_retained_after_refund: 0.5*cohort_application.total_required_stripe_amount,
                total_amount_paid_in_stripe: cohort_application.total_amount_paid_in_stripe,
                refund_percent: 0.5,
                has_started_playlist: true,
                playlists_completed: 3,
                refund_amount: 2100,
                stripe_plan_id: 'stripe_plan_id',
                coupon_id: 'coupon_id'
            })
        end

        describe "refund_amount" do
            attr_reader :cohort_application
            before(:each) do
                @cohort_application = cohorts(:published_emba_cohort).cohort_applications.find_by_status('accepted')
                @cohort_application.stripe_plan_id = default_plan.id
            end

            it "should be 0 if the user has paid less than the amount owed to smartly" do
                expect(cohort_application).to receive(:total_required_stripe_amount).at_least(1).times.and_return(8000)
                expect(cohort_application).to receive(:get_refund_percent_details).and_return({
                    refund_percent: 0.75,
                    started_concentration_count: 3,
                    completed_concentration_count: 3
                })
                expect(cohort_application).to receive(:total_amount_paid_in_stripe).at_least(1).times.and_return(1000)
                expect(cohort_application.refund_details[:refund_amount]).to eq(0)
            end

            it "should be something if the user has paid more than the amount owed to smartly" do
                expect(cohort_application).to receive(:total_required_stripe_amount).at_least(1).times.and_return(8000)
                expect(cohort_application).to receive(:get_refund_percent_details).and_return({
                    refund_percent: 0.75,
                    started_concentration_count: 3,
                    completed_concentration_count: 3
                })
                expect(cohort_application).to receive(:total_amount_paid_in_stripe).at_least(1).times.and_return(3000)
                expect(cohort_application.refund_details[:refund_amount]).to eq(3000 - 8000*0.25)
            end

            it "should be 0 if the refund_percent is 0" do
                expect(cohort_application).to receive(:total_required_stripe_amount).at_least(1).times.and_return(8000)
                expect(cohort_application).to receive(:get_refund_percent_details).and_return({
                    refund_percent: 0,
                    started_concentration_count: 3,
                    completed_concentration_count: 3
                })
                expect(cohort_application).to receive(:total_amount_paid_in_stripe).at_least(1).times.and_return(3000)
                expect(cohort_application.refund_details[:refund_amount]).to eq(0)
            end
        end

        describe "custom_message/can_issue_refund" do

            before(:each) do
                allow_any_instance_of(CohortApplication).to receive(:total_required_stripe_amount).and_return(4200)
                allow_any_instance_of(CohortApplication).to receive(:total_amount_paid_in_stripe).and_return(4200)
                allow_any_instance_of(CohortApplication).to receive(:get_refund_percent_details).and_return({
                    refund_percent: 1,
                    started_concentration_count: 0,
                    completed_concentration_count: 0
                })
            end

            it "should be set if refund_attempt_failed?" do
                cohort_application = cohorts(:published_emba_cohort).cohort_applications.find_by_status('accepted')
                cohort_application.stripe_plan_id = default_plan.id
                cohort_application.refund_attempt_failed = true
                expect(cohort_application.refund_details[:custom_message]).to start_with("A refund attempt failed in the past,")
                expect(cohort_application.refund_details[:can_issue_refund]).to be(false)
            end

            it "should be set if has_full_scholarship?" do
                cohort_application = cohorts(:published_emba_cohort).cohort_applications.find_by_status('accepted')
                cohort_application.stripe_plan_id = default_plan.id
                cohort_application.refund_attempt_failed = false
                expect(cohort_application).to receive(:has_full_scholarship?).at_least(1).times.and_return(true)
                expect(cohort_application.refund_details[:custom_message]).to start_with("No refund will be issued because this user has a full scholarship")
                expect(cohort_application.refund_details[:can_issue_refund]).to be(false)
            end

            it "should be set if promised_payment_outside_of_stripe?" do
                cohort_application = cohorts(:published_emba_cohort).cohort_applications.find_by_status('accepted')
                cohort_application.stripe_plan_id = default_plan.id
                cohort_application.refund_attempt_failed = false
                expect(cohort_application).to receive(:has_full_scholarship?).at_least(1).times.and_return(false)
                expect(cohort_application).to receive(:promised_payment_outside_of_stripe?).at_least(1).times.and_return(true)
                expect(cohort_application.refund_details[:custom_message]).to start_with("This user appears to have made payments outside of stripe")
                expect(cohort_application.refund_details[:can_issue_refund]).to be(false)
            end

            it "should be set if refund_issued_at" do
                cohort_application = cohorts(:published_emba_cohort).cohort_applications.find_by_status('accepted')
                cohort_application.stripe_plan_id = default_plan.id
                cohort_application.refund_attempt_failed = false
                expect(cohort_application).to receive(:has_full_scholarship?).at_least(1).times.and_return(false)
                expect(cohort_application).to receive(:promised_payment_outside_of_stripe?).at_least(1).times.and_return(false)
                cohort_application.refund_issued_at = Time.parse('2017/03/26 12:00:00')
                expect(cohort_application.refund_details[:custom_message]).to start_with("A refund was already issued on 2017-03-26.")
                expect(cohort_application.refund_details[:can_issue_refund]).to be(false)
            end

            it "should not be set if total_amount_paid_in_stripe is 0" do
                cohort_application = cohorts(:published_emba_cohort).cohort_applications.find_by_status('accepted')
                cohort_application.stripe_plan_id = default_plan.id
                allow_any_instance_of(CohortApplication).to receive(:total_amount_paid_in_stripe).and_return(0)
                expect(cohort_application.refund_details).to be_nil
            end

            it "should set can_issue_refund=false with no custom message if refund_amount = 0" do
                cohort_application = cohorts(:published_emba_cohort).cohort_applications.find_by_status('accepted')
                cohort_application.stripe_plan_id = default_plan.id
                cohort_application.refund_attempt_failed = false
                expect(cohort_application).to receive(:has_full_scholarship?).at_least(1).times.and_return(false)
                expect(cohort_application).to receive(:promised_payment_outside_of_stripe?).at_least(1).times.and_return(false)
                cohort_application.refund_issued_at = nil
                expect(cohort_application).to receive(:get_refund_percent_details).and_return({
                    refund_percent: 0,
                    started_concentration_count: 0,
                    completed_concentration_count: 0
                })
                result = cohort_application.refund_details
                expect(result[:custom_message]).to be_nil
                expect(result[:can_issue_refund]).to be(false)
            end
        end
    end

    describe "issue_refund!" do
        attr_reader :cohort_application, :total_amount
        before(:each) do
            @total_amount = 2200
            allow_any_instance_of(CohortApplication).to receive(:refund_details).and_return({
                refund_amount: @total_amount
            })
            expect(Stripe::Refund.list).to be_empty # sanity check
            @cohort_application = cohorts(:published_emba_cohort).cohort_applications.find_by_status('accepted')
        end

        it "should raise if there is still a subscription" do
            if !cohort_application.user.primary_subscription
                create_subscription(cohort_application.user)
            end
            expect {
                cohort_application.issue_refund!
            }.to raise_error(RuntimeError, "Cannot issue refund until subscription has been destroyed")
            expect(cohort_application.reload.refund_attempt_failed).to be(false)
        end

        it "should raise if refund_attempt_failed?" do
            cohort_application.refund_attempt_failed = true
            expect {
                cohort_application.issue_refund!
            }.to raise_error(RuntimeError, "Cannot issue refund if a previous refund attempt failed")
        end

        it "should raise if no subscription found in stripe" do
            expect(cohort_application.refund_attempt_failed).to be(false)
            expect {
                cohort_application.issue_refund!
            }.to raise_error(RuntimeError, "No canceled subscription found in stripe")
        end

        it "should raise if there are not enough charges to refund" do
            subscription = create_subscription(cohort_application.user)

            # this invoice will have amount_due=1000
            invoice, charge = create_invoice_with_charge(cohort_application.user)
            mock_expand_for_invoices_and_charges(cohort_application.user, {invoice => charge})

            # have to destroy the susbcription before calling issue_refund!
            subscription.destroy
            cohort_application.user.subscriptions.reload
            expect(Stripe::Refund).not_to receive(:create)
            expect {
                cohort_application.issue_refund!
            }.to raise_error(RuntimeError, "Not enough charges found to issue the required refund")

            # refund_attempt_failed should have been set to true
            expect(cohort_application.reload.refund_attempt_failed).to be(true)
        end

        it "should handle a failed create refund request to stripe" do
            subscription = create_subscription(cohort_application.user)

            # make sure that we have enough invoices to make a full refund
            invoices = {}
            [
                create_invoice_with_charge(cohort_application.user),
                create_invoice_with_charge(cohort_application.user),
                create_invoice_with_charge(cohort_application.user),
                create_invoice_with_charge(cohort_application.user)
            ].each do |pair|
                invoice, charge = pair
                invoices[invoice] = charge
            end
            mock_expand_for_invoices_and_charges(cohort_application.user, invoices)

            # have to destroy the susbcription before calling issue_refund!
            subscription.destroy
            cohort_application.user.subscriptions.reload
            expect(Stripe::Refund).to receive(:create).and_raise(RuntimeError.new("Stripe fail!"))
            expect {
                cohort_application.issue_refund!
            }.to raise_error(RuntimeError, "Stripe fail!")

            # refund_attempt_failed should have been set to true
            expect(cohort_application.reload.refund_attempt_failed).to be(true)
        end

        it "should refund invoices with non-refunded charge" do
            subscription = create_subscription(cohort_application.user)

            # all invoices will have amount_due=1000
            invoice_with_no_charge = create_invoice(cohort_application.user)

            refunded_invoice, refunded_charge = create_invoice_with_charge(cohort_application.user)
            Stripe::Refund.create(
                charge: refunded_charge.id,
                amount: refunded_invoice.total
            )

            invoice1, charge1 = create_invoice_with_charge(cohort_application.user)
            invoice2, charge2 = create_invoice_with_charge(cohort_application.user)
            invoice3, charge3 = create_invoice_with_charge(cohort_application.user)

            mock_expand_for_invoices_and_charges(cohort_application.user, {
                invoice_with_no_charge => nil,
                refunded_invoice => refunded_charge,
                invoice1 => charge1,
                invoice2 => charge2,
                invoice3 => charge3,
            })

            # have to destroy the susbcription before calling issue_refund!
            subscription.destroy
            cohort_application.user.subscriptions.reload
            cohort_application.issue_refund!
            expect(cohort_application.reload.refund_attempt_failed).to be(false)

            # all invoices will have amount_due=1000
            # stripe-ruby-mock does not support filtering list by charge
            refunds = Stripe::Refund.list
            expect(refunds.detect { |r| r.charge == charge3.id }.amount).to eq(1000)
            expect(refunds.detect { |r| r.charge == charge2.id }.amount).to eq(1000)
            expect(refunds.detect { |r| r.charge == charge1.id }.amount).to eq(total_amount - 2000)
        end

        it "should not refund an invoice with an unpaid charge" do
            allow(cohort_application).to receive(:refund_details).and_return({
                refund_amount: 1000
            })

            subscription = create_subscription(cohort_application.user)

            invoice1, charge1 = create_invoice_with_charge(cohort_application.user)
            invoice2, charge2 = create_invoice_with_unpaid_charge(cohort_application.user)

            mock_expand_for_invoices_and_charges(cohort_application.user, {
                invoice1 => charge1,
                invoice2 => charge2,
            })

            # have to destroy the susbcription before calling issue_refund!
            subscription.destroy
            cohort_application.user.subscriptions.reload
            cohort_application.issue_refund!
            expect(cohort_application.reload.refund_attempt_failed).to be(false)

            # all invoices will have amount_due=1000
            # stripe-ruby-mock does not support filtering list by charge
            refunds = Stripe::Refund.list
            expect(refunds.detect { |r| r.charge == charge2.id }).to be(nil)
            expect(refunds.detect { |r| r.charge == charge1.id }.amount).to eq(1000)
        end

        it "should refund invoices from different subscriptions" do
            allow(cohort_application).to receive(:refund_details).and_return({
                refund_amount: 2000
            })

            first_subscription = create_subscription(cohort_application.user)
            invoice1, charge1 = create_invoice_with_charge(cohort_application.user)

            first_subscription.destroy
            second_subscription = create_subscription(cohort_application.user)
            invoice2, charge2 = create_invoice_with_charge(cohort_application.user)

            mock_expand_for_invoices_and_charges(cohort_application.user, {
                invoice1 => charge1,
                invoice2 => charge2
            })

            # have to destroy the subscription before calling issue_refund!
            second_subscription.destroy
            cohort_application.user.subscriptions.reload
            cohort_application.issue_refund!
            expect(cohort_application.reload.refund_attempt_failed).to be(false)

            # all invoices will have amount_due=1000
            # stripe-ruby-mock does not support filtering list by charge
            refunds = Stripe::Refund.list
            expect(refunds.detect { |r| r.charge == charge2.id }.amount).to eq(1000)
            expect(refunds.detect { |r| r.charge == charge1.id }.amount).to eq(1000)
        end

        def create_invoice(user)
            @invoice_date ||= Time.at(0)
            invoice = create_invoices(user, [
                {date: (@invoice_date += 1.month)}
            ]).first

            # stripe-ruby-mock gives no way to set the amount here.
            # it is always 1000.  Really. I read the code
            expect(invoice.amount_due).to eq(1000)
            invoice
        end

        def create_invoice_with_charge(user)
            invoice = create_invoice(user)
            invoice.pay # create a charge
            charge = Stripe::Charge.retrieve(invoice.charge)
            [invoice, charge]
        end

        def create_invoice_with_unpaid_charge(user)
            invoice = create_invoice(user)
            invoice.charge = Stripe::Charge.create(customer: user.id, livemode: false, amount: 1000, currency: 'usd', paid: false)
            [invoice, invoice.charge]
        end

        def mock_expand_for_invoices_and_charges(user, hash)
            # stripe ruby mock does not support `expand`, so we have
            # to implement it ourselves
            hash.each do |invoice, charge|
                allow(invoice).to receive(:charge).and_return(charge)
            end
            expect(Stripe::Invoice).to receive(:list)
                .with({
                    customer: user.id,
                    expand: ['data.charge', 'data.charge.refunds']
                })
                .and_return(hash.keys.sort_by(&:created).reverse)
        end
    end

    describe "applicable_coupon_info" do

        attr_reader :cohort_application

        before(:each) do
            cohort = cohorts(:published_emba_cohort)
            @cohort_application = cohort.cohort_applications.find_by_status('accepted')
            cohort_application.stripe_plans << { 'id' => bi_annual_plan.id, 'amount_off' => 480000, 'percent_off' => nil }
            cohort_application.scholarship_levels = cohort.scholarship_levels
            cohort_application.scholarship_level = cohort_application.scholarship_levels.detect { |sl| sl['name'] == 'Level 2'}

            @applicable_registration_period = 'standard'
            allow(cohort_application).to receive(:applicable_registration_period) do
                @applicable_registration_period
            end
        end

        it "should account for the scholarship_level" do
            expect(cohort_application.applicable_coupon_info).to eq({"id"=>"discount_250", "amount_off"=>25000, "percent_off"=>nil})
            cohort_application.scholarship_level = cohort_application.scholarship_levels.detect { |sl| sl['name'] == 'No Scholarship'}
            expect(cohort_application.applicable_coupon_info).to eq({"id"=>"none", "amount_off"=>0, "percent_off"=>0})
        end

        it "should account for applicable_registration_period" do
            cohort_application.scholarship_level['standard'][default_plan.id] = { 'id' => 'default_plan_coupon_standard' }
            cohort_application.scholarship_level['early'][default_plan.id] = { 'id' => 'default_plan_coupon_early' }

            expect(cohort_application.applicable_coupon_info['id']).to eq('default_plan_coupon_standard')

            @applicable_registration_period = 'early'
            expect(cohort_application.applicable_coupon_info['id']).to eq('default_plan_coupon_early')
        end

        it "should handle plan defaulting and explicit assignment" do
            expect(cohort_application.stripe_plan_id).to be_nil
            expect(cohort_application.applicable_coupon_info).to eq({"id"=>"discount_250", "amount_off"=>25000, "percent_off"=>nil})

            cohort_application.scholarship_level['standard'][bi_annual_plan.id] = { 'id' => 'bi_annual_plan_coupon_standard' }
            cohort_application.stripe_plan_id = bi_annual_plan.id
            expect(cohort_application.applicable_coupon_info['id']).to eq('bi_annual_plan_coupon_standard')
        end

        it "should accept a stripe_plan_id parameter" do
            cohort_application.scholarship_level['standard'][bi_annual_plan.id] = { 'id' => 'bi_annual_plan_coupon_standard' }
            expect(cohort_application.applicable_coupon_info(bi_annual_plan.id)['id']).to eq('bi_annual_plan_coupon_standard')
        end

        it "should warn to sentry when nil" do
            # construct a situation that would trigger nil
            cohort_application.stripe_plan_id = 'foobar'
            expect(Raven).to receive(:capture_exception).with('applicable_coupon_info is nil', {
                level: 'warning',
                extra: {
                    plan_id: nil,
                    self_stripe_plan_id: cohort_application.stripe_plan_id,
                    default_stripe_plan: cohort_application.default_stripe_plan,
                    stripe_plan_id: cohort_application.stripe_plan_id,
                    scholarship_level: cohort_application.scholarship_level,
                    applicable_registration_period: cohort_application.applicable_registration_period,
                }
            })
            expect(cohort_application.applicable_coupon_info).to be_nil
        end

    end

    describe "stripe_plan_num_intervals" do
        it "should work" do
            cohort = cohorts(:published_emba_cohort)
            cohort_application = cohort.cohort_applications.find_by_status('accepted')
            stripe_plan = cohort_application.stripe_plans.first
            cohort_application.stripe_plan_id = stripe_plan['id']

            expect(stripe_plan['id']).to eq(default_plan.id)
            expect(stripe_plan['frequency']).to eq('monthly')
            expect(cohort_application.stripe_plan_num_intervals).to eq(12)

            stripe_plan['frequency'] = 'bi_annual'
            expect(cohort_application.stripe_plan_num_intervals).to eq(2)

            stripe_plan['frequency'] = 'once'
            expect(cohort_application.stripe_plan_num_intervals).to eq(1)
        end
    end

    describe "payment_amount_per_interval" do

        before(:each) do
            allow_any_instance_of(Cohort::Version).to receive(:early_registration_deadline).and_return(Time.now - 1.year)
        end
        # Normally, we would mock out more stuff here so that these specs would
        # be less brittle.  But, we are also trying to test our sql helper here,
        # so we want to make sure we are setting up users in a realistic way.

        it "should work for a user with a full scholarship" do
            cohort_application = CohortApplication.where("scholarship_levels::text ilike '%full%'").first
            cohort_application.scholarship_level = cohort_application.scholarship_levels.detect { |sl| sl["name"] == "Full Scholarship" }
            cohort_application.save!
            expect(cohort_application.amount_per_interval).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'payment_amount_per_interval')).to eq(cohort_application.amount_per_interval / 100.0)
        end

        it "should work for a user with a scholarship who registered early" do
            cohort_application = CohortApplication.where("scholarship_levels::text ilike '%amount_off%'").first
            amount_off = 25000
            cohort_application.scholarship_level = cohort_application.scholarship_levels.detect { |sl| sl["early"]["default_plan"]["amount_off"] == amount_off }
            cohort_application.stripe_plan_id = 'default_plan'
            cohort_application.registered_early = true
            cohort_application.registered = true
            cohort_application.save!(validate: false)
            expect(cohort_application.amount_per_interval).to eq(cohort_application.stripe_plan_amount - amount_off)
            expect(get_value_from_sql_helper(cohort_application, 'payment_amount_per_interval')).to eq(cohort_application.amount_per_interval / 100.0)
        end

        it "should work for a user with a scholarship who did not register early" do
            cohort_application = CohortApplication.where("scholarship_levels::text ilike '%amount_off%'").first
            amount_off = 25000
            cohort_application.scholarship_level = cohort_application.scholarship_levels.detect { |sl| sl["standard"]["default_plan"]["amount_off"] == amount_off }
            cohort_application.stripe_plan_id = 'default_plan'
            cohort_application.registered_early = false
            cohort_application.registered = true
            cohort_application.save!(validate: false)
            expect(cohort_application.amount_per_interval).to eq(cohort_application.stripe_plan_amount - amount_off)
            expect(get_value_from_sql_helper(cohort_application, 'payment_amount_per_interval')).to eq(cohort_application.amount_per_interval / 100.0)
        end

        it "should work for a user with no scholarship" do
            cohort_application = CohortApplication.where("scholarship_levels::text ilike '%No Scholarship%'").first
            cohort_application.scholarship_level = cohort_application.scholarship_levels.detect { |sl| sl["name"] == "No Scholarship" }
            cohort_application.stripe_plan_id = 'default_plan'
            cohort_application.registered = true
            cohort_application.save!(validate: false)
            expect(cohort_application.amount_per_interval).to eq(cohort_application.stripe_plan_amount)
            expect(get_value_from_sql_helper(cohort_application, 'payment_amount_per_interval')).to eq(cohort_application.amount_per_interval / 100.0)
        end
    end

    describe "fee and tuition sql helpers" do

        # Normally, we would separate out these specs and make them more unit-ee,
        # but since we want to check that the sql helpers agree with the ruby,
        # we need to combine stuff

        it "should work with monthly plan that has fees" do
            cohort_application = CohortApplication.where.not(scholarship_levels: nil).first
            amount = 87500
            cohort_application.stripe_plans << {id: 'emba_875', amount: amount, frequency: 'monthly' }.stringify_keys
            cohort_application.stripe_plans << {id: 'single_payment', amount: amount*12 - 7500*12, frequency: 'once' }.stringify_keys
            cohort_application.stripe_plan_id = 'emba_875'
            cohort_application.registered = true
            cohort_application.save!(validate: false)

            # total_fee_amount
            expect(cohort_application.fee_amount_per_interval).to eq(7500)
            expect(get_value_from_sql_helper(cohort_application, 'fee_amount_per_interval')).to eq(cohort_application.fee_amount_per_interval / 100.0)

            # total_fee_amount
            expect(cohort_application.total_fee_amount).to eq(7500 * 12)
            expect(get_value_from_sql_helper(cohort_application, 'total_fee_amount')).to eq(cohort_application.total_fee_amount / 100.0)

            # total_fees_plus_tuition
            expect(cohort_application.total_fees_plus_tuition).to eq(12 * amount)
            expect(get_value_from_sql_helper(cohort_application, 'total_fees_plus_tuition')).to eq(cohort_application.total_fees_plus_tuition / 100.0)

            # total_base_tuition
            expect(cohort_application.total_base_tuition).to eq(cohort_application.total_fees_plus_tuition - cohort_application.total_fee_amount)
            expect(get_value_from_sql_helper(cohort_application, 'total_base_tuition')).to eq(cohort_application.total_base_tuition / 100.0)

        end

        it "should work with monthly plan that has no fees" do
            cohort_application = CohortApplication.where.not(scholarship_levels: nil).first
            amount = 80000
            cohort_application.stripe_plans << {id: 'emba_800', amount: amount, frequency: 'monthly' }.stringify_keys
            cohort_application.stripe_plans << {id: 'single_payment', amount: amount*12, frequency: 'once' }.stringify_keys
            cohort_application.stripe_plan_id = 'emba_800'
            cohort_application.registered = true
            cohort_application.save!(validate: false)

            # total_fee_amount
            expect(cohort_application.fee_amount_per_interval).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'fee_amount_per_interval')).to eq(cohort_application.fee_amount_per_interval / 100.0)

            # total_fee_amount
            expect(cohort_application.total_fee_amount).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'total_fee_amount')).to eq(cohort_application.total_fee_amount / 100.0)

            # total_fees_plus_tuition
            expect(cohort_application.total_fees_plus_tuition).to eq(12 * amount)
            expect(get_value_from_sql_helper(cohort_application, 'total_fees_plus_tuition')).to eq(cohort_application.total_fees_plus_tuition / 100.0)

            # total_base_tuition
            expect(cohort_application.total_base_tuition).to eq(cohort_application.total_fees_plus_tuition - cohort_application.total_fee_amount)
            expect(get_value_from_sql_helper(cohort_application, 'total_base_tuition')).to eq(cohort_application.total_base_tuition / 100.0)

        end

        it "should work with bi_annual plan that has no fees" do
            cohort_application = CohortApplication.where.not(scholarship_levels: nil).first
            amount = 80000
            cohort_application.stripe_plans << {id: 'bi_annual', amount: amount, frequency: 'bi_annual' }.stringify_keys
            cohort_application.stripe_plans << {id: 'single_payment', amount: amount*2, frequency: 'once' }.stringify_keys
            cohort_application.stripe_plan_id = 'bi_annual'
            cohort_application.registered = true
            cohort_application.save!(validate: false)

            # total_fee_amount
            expect(cohort_application.fee_amount_per_interval).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'fee_amount_per_interval')).to eq(cohort_application.fee_amount_per_interval / 100.0)

            # total_fee_amount
            expect(cohort_application.total_fee_amount).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'total_fee_amount')).to eq(cohort_application.total_fee_amount / 100.0)

            # total_fees_plus_tuition
            expect(cohort_application.total_fees_plus_tuition).to eq(2 * amount)
            expect(get_value_from_sql_helper(cohort_application, 'total_fees_plus_tuition')).to eq(cohort_application.total_fees_plus_tuition / 100.0)

            # total_base_tuition
            expect(cohort_application.total_base_tuition).to eq(cohort_application.total_fees_plus_tuition - cohort_application.total_fee_amount)
            expect(get_value_from_sql_helper(cohort_application, 'total_base_tuition')).to eq(cohort_application.total_base_tuition / 100.0)

        end

        it "should work with single_payment plan that has no fees" do
            cohort_application = CohortApplication.where.not(scholarship_levels: nil).first
            amount = 80000
            cohort_application.stripe_plans << {id: 'single_payment', amount: amount, frequency: 'once' }.stringify_keys
            cohort_application.stripe_plan_id = 'single_payment'
            cohort_application.registered = true
            cohort_application.save!(validate: false)

            # total_fee_amount
            expect(cohort_application.fee_amount_per_interval).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'fee_amount_per_interval')).to eq(cohort_application.fee_amount_per_interval / 100.0)

            # total_fee_amount
            expect(cohort_application.total_fee_amount).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'total_fee_amount')).to eq(cohort_application.total_fee_amount / 100.0)

            # total_fees_plus_tuition
            expect(cohort_application.total_fees_plus_tuition).to eq(amount)
            expect(get_value_from_sql_helper(cohort_application, 'total_fees_plus_tuition')).to eq(cohort_application.total_fees_plus_tuition / 100.0)

            # total_base_tuition
            expect(cohort_application.total_base_tuition).to eq(cohort_application.total_fees_plus_tuition - cohort_application.total_fee_amount)
            expect(get_value_from_sql_helper(cohort_application, 'total_base_tuition')).to eq(cohort_application.total_base_tuition / 100.0)

        end

        # sometimes full scholarship users have stripe_plan_id defined.  Sometimes not
        it "should work with a full scholarship that has a stripe_plan_id" do
            cohort_application = CohortApplication.where("scholarship_level::text ilike '%full%'").first
            amount = 960000
            cohort_application.stripe_plans << {frequency: 'once', amount: amount, id: 'one_time_plan'}.as_json
            cohort_application.stripe_plan_id = 'default_plan'
            cohort_application.registered = true
            cohort_application.save!(validate: false)

            # total_fee_amount
            expect(cohort_application.fee_amount_per_interval).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'fee_amount_per_interval')).to eq(cohort_application.fee_amount_per_interval / 100.0)

            # total_fee_amount
            expect(cohort_application.total_fee_amount).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'total_fee_amount')).to eq(cohort_application.total_fee_amount / 100.0)

            # total_fees_plus_tuition
            expect(cohort_application.total_fees_plus_tuition).to eq(amount)
            expect(get_value_from_sql_helper(cohort_application, 'total_fees_plus_tuition')).to eq(cohort_application.total_fees_plus_tuition / 100.0)

            # total_base_tuition
            one_time_payment_plan = cohort_application.stripe_plans.detect { |plan| plan['frequency'] == 'once' }
            expect(cohort_application.total_base_tuition).to eq(one_time_payment_plan['amount'])
            expect(get_value_from_sql_helper(cohort_application, 'total_base_tuition')).to eq(cohort_application.total_base_tuition / 100.0)

        end

        # sometimes full scholarship users have stripe_plan_id defined.  Sometimes not
        it "should work with a full scholarship that has no stripe_plan_id" do
            cohort_application = CohortApplication.where("scholarship_level::text ilike '%full%'").first
            amount = 960000
            cohort_application.stripe_plans << {frequency: 'once', amount: amount, id: 'one_time_plan'}.as_json
            cohort_application.scholarship_level['standard']['one_time_plan'] = {percent_off: 100}.as_json
            cohort_application.save!

            # total_fee_amount
            expect(cohort_application.fee_amount_per_interval).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'fee_amount_per_interval')).to eq(cohort_application.fee_amount_per_interval / 100.0)

            # total_fee_amount
            expect(cohort_application.total_fee_amount).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'total_fee_amount')).to eq(cohort_application.total_fee_amount / 100.0)

            # total_fees_plus_tuition
            expect(cohort_application.total_fees_plus_tuition).to eq(amount)
            expect(get_value_from_sql_helper(cohort_application, 'total_fees_plus_tuition')).to eq(cohort_application.total_fees_plus_tuition / 100.0)

            # total_base_tuition
            one_time_payment_plan = cohort_application.stripe_plans.detect { |plan| plan['frequency'] == 'once' }
            expect(cohort_application.total_base_tuition).to eq(one_time_payment_plan['amount'])
            expect(get_value_from_sql_helper(cohort_application, 'total_base_tuition')).to eq(cohort_application.total_base_tuition / 100.0)

        end

        # we have some legacy users that only have monthly and bi_annual plans.
        it "should work with a user that has no once plan" do
            cohort_application = CohortApplication.where("scholarship_level::text ilike '%full%'").first

            # total_fee_amount
            expect(cohort_application.fee_amount_per_interval).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'fee_amount_per_interval')).to eq(cohort_application.fee_amount_per_interval / 100.0)

            # total_fee_amount
            expect(cohort_application.total_fee_amount).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'total_fee_amount')).to eq(cohort_application.total_fee_amount / 100.0)

            # total_fees_plus_tuition
            expect(cohort_application.total_fees_plus_tuition).to eq(9600*100)
            expect(get_value_from_sql_helper(cohort_application, 'total_fees_plus_tuition')).to eq(cohort_application.total_fees_plus_tuition / 100.0)

            # total_base_tuition
            expect(cohort_application.total_base_tuition).to eq(9600*100)
            expect(get_value_from_sql_helper(cohort_application, 'total_base_tuition')).to eq(cohort_application.total_base_tuition / 100.0)
        end

        it "should work for a free user" do
            cohort_application = cohorts(:published_mba_cohort).cohort_applications.where(status: 'accepted').first
            expect(cohort_application.stripe_plans).to be_nil
            expect(cohort_application.scholarship_level).to be_nil
            expect(cohort_application.scholarship_levels).to be_nil

            # total_fee_amount
            expect(cohort_application.fee_amount_per_interval).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'fee_amount_per_interval')).to eq(0)

            # total_fee_amount
            expect(cohort_application.total_fee_amount).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'total_fee_amount')).to eq(0)

            # total_fees_plus_tuition
            expect(cohort_application.total_fees_plus_tuition).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'total_fees_plus_tuition')).to eq(0)

            # total_base_tuition
            expect(cohort_application.total_base_tuition).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'total_base_tuition')).to eq(0)

            # net_required_payment
            expect(cohort_application.net_required_payment).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'net_required_payment')).to eq(0)

        end

        # Note that paid_cert users DO have stripe_plans but DO NOT have the notion of a scholarship
        it "should work for paid_cert" do
            cohort_application = CohortApplication.first
            amount = 133700
            cohort_application.stripe_plans = [{frequency: 'once', amount: amount, id: 'one_time_plan'}].as_json
            cohort_application.scholarship_level = nil
            cohort_application.stripe_plan_id = 'one_time_plan'
            cohort_application.save!(validate: false)

            expect(cohort_application.scholarship_level).to be_nil

            # fee_amount_per_interval
            expect(cohort_application.fee_amount_per_interval).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'fee_amount_per_interval')).to eq(cohort_application.fee_amount_per_interval / 100.0)

            # total_fee_amount
            expect(cohort_application.total_fee_amount).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'total_fee_amount')).to eq(cohort_application.total_fee_amount / 100.0)

            # total_fees_plus_tuition
            expect(cohort_application.total_fees_plus_tuition).to eq(amount)
            expect(get_value_from_sql_helper(cohort_application, 'total_fees_plus_tuition')).to eq(cohort_application.total_fees_plus_tuition / 100.0)

            # total_base_tuition
            expect(cohort_application.total_base_tuition).to eq(amount)
            expect(get_value_from_sql_helper(cohort_application, 'total_base_tuition')).to eq(cohort_application.total_base_tuition / 100.0)
        end
    end

    describe "discount_per_interval and total_discount" do
        it "should respect early registration" do
            cohort_application = CohortApplication.where.not(scholarship_level: nil).first
            cohort_application.stripe_plan_id = 'default_plan'
            cohort_application.registered = true
            cohort_application.scholarship_level = cohort_application.scholarship_levels.detect { |sl| sl["standard"]["default_plan"]["amount_off"] == 25000 }
            cohort_application.scholarship_level["early"]['default_plan']['amount_off'] = 35000
            cohort_application.save!(validate: false)

            # sanity check
            expected_discount = cohort_application.scholarship_level['early']['default_plan']['amount_off'] - cohort_application.scholarship_level['standard']['default_plan']['amount_off']
            expect(expected_discount).to be > 0

            # discount should not be applied if the user did not register early
            cohort_application.registered_early = false
            cohort_application.save!(validate: false)
            expect(cohort_application.discount_per_interval).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'discount_per_interval')).to eq(cohort_application.discount_per_interval / 100.0)
            expect(cohort_application.total_discount).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'total_discount')).to eq(cohort_application.total_discount / 100.0)

            # discount should be applied if the user did register early
            cohort_application.registered_early = true
            cohort_application.save!(validate: false)
            expect(cohort_application.discount_per_interval).to eq(cohort_application.scholarship_level['early']['default_plan']['amount_off'] - cohort_application.scholarship_level['standard']['default_plan']['amount_off'])
            expect(get_value_from_sql_helper(cohort_application, 'discount_per_interval')).to eq(cohort_application.discount_per_interval / 100.0)
            expect(cohort_application.total_discount).to eq(12 * cohort_application.discount_per_interval)
            expect(get_value_from_sql_helper(cohort_application, 'total_discount')).to eq(cohort_application.total_discount / 100.0)
        end

        it "should work with full scholarship" do
            cohort_application = CohortApplication.where("scholarship_level::text ilike '%full%'").first
            expect(cohort_application.discount_per_interval).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'discount_per_interval')).to eq(cohort_application.discount_per_interval / 100.0)
            expect(cohort_application.total_discount).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'total_discount')).to eq(cohort_application.total_discount / 100.0)
        end
    end

    describe "total_scholarship and net_required_payment" do
        it "should work when there is a discount" do
            cohort_application = CohortApplication.where.not(scholarship_level: nil).first
            cohort_application.stripe_plan_id = 'default_plan'
            amount_off = 25000
            cohort_application.scholarship_level["early"]["default_plan"]["amount_off"] = amount_off
            cohort_application.scholarship_level["standard"]["default_plan"]["amount_off"] = amount_off - 1000 # discount of 1000 each interval
            cohort_application.registered_early = true
            cohort_application.registered = true
            cohort_application.save!(validate: false)

            expect(cohort_application.total_discount).to be > 0 # sanity check
            expect(cohort_application.stripe_plan_num_intervals).to be > 0 # sanity check
            expect(cohort_application.total_scholarship).to eq(amount_off*cohort_application.stripe_plan_num_intervals - cohort_application.total_discount)
            expect(get_value_from_sql_helper(cohort_application, 'total_scholarship')).to eq(cohort_application.total_scholarship / 100.0)

            expect(cohort_application.net_required_payment).to eq(cohort_application.total_fees_plus_tuition - cohort_application.total_discount - cohort_application.total_scholarship)
            expect(get_value_from_sql_helper(cohort_application, 'net_required_payment')).to eq(cohort_application.net_required_payment / 100.0)
        end

        it "should work when there is no discount" do
            cohort_application = CohortApplication.where.not(scholarship_level: nil).first
            cohort_application.stripe_plan_id = 'default_plan'
            amount_off = 25000
            cohort_application.scholarship_level["early"]["default_plan"]["amount_off"] = amount_off
            cohort_application.scholarship_level["standard"]["default_plan"]["amount_off"] = amount_off # no discount
            cohort_application.registered_early = false
            cohort_application.registered = true
            cohort_application.save!(validate: false)

            expect(cohort_application.total_discount).to eq(0) # sanity check
            expect(cohort_application.total_scholarship).to eq(amount_off*cohort_application.stripe_plan_num_intervals)
            expect(get_value_from_sql_helper(cohort_application, 'total_scholarship')).to eq(cohort_application.total_scholarship / 100.0)

            expect(cohort_application.net_required_payment).to eq(cohort_application.total_fees_plus_tuition - cohort_application.total_discount - cohort_application.total_scholarship)
            expect(get_value_from_sql_helper(cohort_application, 'net_required_payment')).to eq(cohort_application.net_required_payment / 100.0)
        end

        it "should work when there applicable_coupon_info is nil" do
            cohort_application = CohortApplication.where.not(scholarship_level: nil).first
            cohort_application.stripe_plan_id = 'default_plan'
            amount_off = 25000
            cohort_application.scholarship_level["early"] = {} # this should mess things up
            cohort_application.registered_early = false
            cohort_application.registered = false
            cohort_application.save!(validate: false)

            # sanity checks
            expect(cohort_application.applicable_registration_period).to eq('early')
            expect(cohort_application.applicable_coupon_info).to eq(nil)
            expect(cohort_application.total_discount).to eq(0)
            expect(cohort_application.total_scholarship).to eq(0)

            expect(cohort_application.net_required_payment).to eq(cohort_application.total_fees_plus_tuition - cohort_application.total_discount - cohort_application.total_scholarship)
            expect(get_value_from_sql_helper(cohort_application, 'net_required_payment')).to eq(cohort_application.net_required_payment / 100.0)
        end

        it "should work with full scholarship" do
            cohort_application = CohortApplication.where("scholarship_level::text ilike '%full%'").first
            amount = 960000
            cohort_application.stripe_plans << {frequency: 'once', amount: amount, id: 'one_time_plan'}.as_json
            cohort_application.scholarship_level['standard']['one_time_plan'] = {percent_off: 100}.as_json
            cohort_application.save!(validate: false)
            expect(cohort_application.total_scholarship).to eq(amount)
            expect(get_value_from_sql_helper(cohort_application, 'total_scholarship')).to eq(cohort_application.total_scholarship / 100.0)

            expect(cohort_application.net_required_payment).to eq(0)
            expect(get_value_from_sql_helper(cohort_application, 'net_required_payment')).to eq(cohort_application.net_required_payment / 100.0)
        end
    end

    describe "stripe_plan_interval" do

        it "should work" do
            ca = CohortApplication.where.not(stripe_plans: nil).first
            ca.stripe_plan_id = 'default_plan'
            ca.stripe_plan['frequency'] = 'bi_annual'
            ca.save!(validate: false)
            expect(get_value_from_sql_helper(ca, 'stripe_plan_interval')).to eq('6 months')

            ca.stripe_plan['frequency'] = 'monthly'
            ca.save!(validate: false)
            expect(get_value_from_sql_helper(ca, 'stripe_plan_interval')).to eq('1 month')

            ca.stripe_plan['frequency'] = 'once'
            ca.save!(validate: false)
            expect(get_value_from_sql_helper(ca, 'stripe_plan_interval')).to be_nil

        end

    end

    def get_value_from_sql_helper(cohort_application, helper)
        row = ActiveRecord::Base.connection.execute("select total_fee_amount(cohort_applications) total_fee_amount, cohort_applications.id, cohort_applications.stripe_plan_id, #{helper}(cohort_applications) val from cohort_applications where id='#{cohort_application.id}'").to_a.first
        #pp row
        row['val']
    end

    describe "set_stripe_plans_and_scholarship_levels" do

        it "should set stripe plans only if they are not yet set" do
            cohort = cohorts(:published_emba_cohort)
            users = User.left_outer_joins(:cohort_applications).where(cohort_applications: {id: nil}).limit(2)
            application = CohortApplication.create!(valid_attrs(cohort_id: cohort.id, user_id: users[0].id))
            expect(application.stripe_plans).to eq(cohort.stripe_plans)

            custom_plans = [default_plan.to_h.merge(id: 'another_plan').as_json]
            application = CohortApplication.create!(valid_attrs(cohort_id: cohort.id, user_id: users[1].id, stripe_plans: custom_plans))
            expect(application.stripe_plans).to eq(custom_plans)
        end

    end

    describe "reset_payment_information_if_needed" do
        it "should reset payment fields if the cohort program type is not paid" do
            application = cohorts(:published_emba_cohort).cohort_applications.first

            expect(application.stripe_plans).not_to be(nil)
            expect(application.scholarship_levels).not_to be(nil)
            expect(application.scholarship_level).not_to be(nil)

            application.cohort = cohorts(:published_mba_cohort)
            application.reset_payment_information_if_needed

            expect(application.stripe_plans).to be(nil)
            expect(application.scholarship_levels).to be(nil)
            expect(application.scholarship_level).to be(nil)
        end

        it "should do nothing if the cohort program type is paid" do
            application = cohorts(:published_emba_cohort).cohort_applications.first

            expect(application.stripe_plans).not_to be(nil)
            expect(application.scholarship_levels).not_to be(nil)
            expect(application.scholarship_level).not_to be(nil)

            application.reset_payment_information_if_needed

            expect(application.stripe_plans).not_to be(nil)
            expect(application.scholarship_levels).not_to be(nil)
            expect(application.scholarship_level).not_to be(nil)
        end
    end

    describe "original_program_type" do

        it "reflect the existing program_type if saved without a change in cohort program_type" do
            application = CohortApplication.create!(valid_attrs)
            expect(application.original_program_type).not_to be_nil
            expect(application.original_program_type).to eq(application.program_type)
        end

        it "reflect the original program_type if saved with a change in cohort program_type" do
            application = CohortApplication.create!(valid_attrs)
            new_cohort = Cohort.where.not(program_type: application.program_type).first
            application.cohort = new_cohort
            application.save!
            expect(application.original_program_type).not_to be_nil
            expect(application.original_program_type).not_to eq(application.program_type)
            expect(application.original_program_type).not_to eq(new_cohort.program_type)
        end
    end

    describe "rejected_or_expelled" do

        before(:each) do
            @now = Time.now
            @cohorts = Cohort.where(program_type: 'mba', was_published: true).order(:start_date)

            # This is already done at the top of this spec file but since it is so crucial to these specs
            # working correctly I am redefining in this describe.
            @user = User.joins("LEFT JOIN cohort_applications ON cohort_applications.user_id = users.id")
                .joins(:career_profile)
                .where("cohort_applications.id is null")
                .where.not(career_profiles: { id: CareerProfileList.pluck('career_profile_ids').flatten} ) # needed so we can update can_edit_career_profile
                .first
        end

        it "should just reject if a fresh pending user" do
            app = CohortApplication.create!(
                cohort_id: @cohorts.first.id,
                user_id: @user.id,
                status: 'pending',
                applied_at: @now
            )

            allow_any_instance_of(Cohort::Version).to receive(:acceptance_date) do |value|
                # Note the `@now - 1.day` here. I'm basically demonstrating that acceptance_date does
                # not matter when they are a fresh user having their pending app rejected.
                @now - 1.day if app.published_cohort['id'] == value['id']
            end
            expect(app.rejected_or_expelled).to eq('rejected')
        end

        it "should expel if graduation_status is 'failed' and no other accepted or deferred applications" do
            app = CohortApplication.create!(
                cohort_id: @cohorts.first.id,
                user_id: @user.id,
                status: 'accepted',
                applied_at: @now,
                graduation_status: 'failed',
                cohort_slack_room_id: @cohorts.first.slack_rooms.first.id
            )
            expect(app.rejected_or_expelled).to eq('expelled')
        end

        it "should not use acceptance_date of previously rejected applications" do
            app1 = CohortApplication.create!(
                cohort_id: @cohorts.first.id,
                user_id: @user.id,
                status: 'rejected',
                applied_at: @now - 2.years
            )

            app2 = CohortApplication.create!(
                cohort_id: @cohorts.second.id,
                user_id: @user.id,
                status: 'rejected',
                applied_at: @now - 1.year
            )

            app3 = CohortApplication.create!(
                cohort_id: @cohorts.third.id,
                user_id: @user.id,
                status: 'deferred',
                applied_at: @now
            )

            allow_any_instance_of(Cohort::Version).to receive(:acceptance_date) do |value|
                case value['id']
                when app1.published_cohort['id']
                    @now - 2.days
                when app2.published_cohort['id']
                    @now - 1.day
                when app3.published_cohort['id']
                    @now + 1.day
                end
            end
            expect(app3.rejected_or_expelled).to eq('rejected')
        end

        it "should not use acceptance_date of previously expelled applications" do
            app1 = CohortApplication.create!(
                cohort_id: @cohorts.first.id,
                user_id: @user.id,
                status: 'deferred',
                applied_at: @now - 2.years
            )

            app2 = CohortApplication.create!(
                cohort_id: @cohorts.second.id,
                user_id: @user.id,
                status: 'expelled',
                applied_at: @now - 1.years
            )

            app3 = CohortApplication.create!(
                cohort_id: @cohorts.third.id,
                user_id: @user.id,
                status: 'deferred',
                applied_at: @now
            )

            # This time when they deferred they should be rejected since it is the first app after
            # their prior expulsion
            allow_any_instance_of(Cohort::Version).to receive(:acceptance_date) do |value|
                case value['id']
                when app1.published_cohort['id']
                    @now - 2.years
                when app2.published_cohort['id']
                    @now - 1.years
                when app3.published_cohort['id']
                    @now + 1.days
                end
            end
            expect(app3.rejected_or_expelled).to eq('rejected')
        end

        it "should use acceptance_date of current cycle if edge case where deferred then gets rejected" do
            app = CohortApplication.create!(
                cohort_id: @cohorts.first.id,
                user_id: @user.id,
                status: 'deferred',
                applied_at: @now
            )

            allow_any_instance_of(Cohort::Version).to receive(:acceptance_date) do |value|
                @now + 1.day if app.published_cohort['id'] == value['id']
            end
            expect(app.rejected_or_expelled).to eq('rejected')
        end

        it "should always reject a user from a cohort that does not support deferrals" do
            app = CohortApplication.create!(
                cohort_id: Cohort.find_by_program_type('the_business_certificate').id,
                user_id: @user.id,
                status: 'pending',
                applied_at: @now
            )
            expect(app).not_to receive(:connection_execute)
            expect(app.rejected_or_expelled).to eq('rejected')
        end

        describe "usage of acceptance_date from prior cohort when continously enrolled" do
            it "should work when deferred" do
                app1 = CohortApplication.create!(
                    cohort_id: @cohorts.first.id,
                    user_id: @user.id,
                    status: 'deferred',
                    applied_at: @now - 1.year
                )
                app2 = CohortApplication.create!(
                    cohort_id: @cohorts.second.id,
                    user_id: @user.id,
                    status: 'deferred',
                    applied_at: @now
                )

                allow_any_instance_of(Cohort::Version).to receive(:acceptance_date) do |value|
                    case value['id']
                    when app1.published_cohort['id']
                        @now - 1.day
                    when app2.published_cohort['id']
                        @now + 1.day
                    end
                end
                expect(app2.rejected_or_expelled).to eq('expelled')
            end

            it "should work when pre_accepted" do
                app1 = CohortApplication.create!(
                    cohort_id: @cohorts.first.id,
                    user_id: @user.id,
                    status: 'deferred',
                    applied_at: @now - 1.year
                )
                app2 = CohortApplication.create!(
                    cohort_id: @cohorts.second.id,
                    user_id: @user.id,
                    status: 'pre_accepted',
                    applied_at: @now
                )

                allow_any_instance_of(Cohort::Version).to receive(:acceptance_date) do |value|
                    case value['id']
                    when app1.published_cohort['id']
                        @now - 1.day
                    when app2.published_cohort['id']
                        @now + 1.day
                    end
                end
                expect(app2.rejected_or_expelled).to eq('expelled')
            end

            # This spec just illustrates that a deferred user should be pre_accepted in the next
            # cohort cycle.
            it "should not work when pending" do
                app1 = CohortApplication.create!(
                    cohort_id: @cohorts.first.id,
                    user_id: @user.id,
                    status: 'deferred',
                    applied_at: @now - 1.year
                )
                app2 = CohortApplication.create!(
                    cohort_id: @cohorts.second.id,
                    user_id: @user.id,
                    status: 'pending',
                    applied_at: @now
                )

                allow_any_instance_of(Cohort::Version).to receive(:acceptance_date) do |value|
                    case value['id']
                    when app1.published_cohort['id']
                        @now - 1.day
                    when app2.published_cohort['id']
                        @now + 1.day
                    end
                end
                expect(app2.rejected_or_expelled).to eq('rejected')
            end
        end
    end

    describe "should_set_invited_to_interview_at?" do

        it "should be true if admissions_decision changed to 'Invite to Interview'" do
            cohort_application = CohortApplication.first
            expect(cohort_application.should_set_invited_to_interview_at?).to be(false)

            cohort_application.admissions_decision = 'NOT Invite to Interview'
            expect(cohort_application.should_set_invited_to_interview_at?).to be(false)

            cohort_application.admissions_decision = 'Invite to Interview'
            expect(cohort_application.should_set_invited_to_interview_at?).to be(true)
        end
    end

    describe "set_invited_to_interview_at" do

        it "should be called before_save if should_set_invited_to_interview_at?" do
            cohort_application = CohortApplication.first
            expect(cohort_application).to receive(:should_set_invited_to_interview_at?).and_return(true)
            expect(cohort_application).to receive(:set_invited_to_interview_at)
            cohort_application.save!

            expect(cohort_application).to receive(:should_set_invited_to_interview_at?).and_return(false)
            expect(cohort_application).not_to receive(:set_invited_to_interview_at)
            cohort_application.save!
        end

        it "should set invited_to_interview_at to now" do
            now = Time.now
            cohort_application = CohortApplication.first
            expect(Time).to receive(:now).and_return(now)
            expect(cohort_application.invited_to_interview_at).not_to eq(now)
            cohort_application.set_invited_to_interview_at
            expect(cohort_application.invited_to_interview_at).to eq(now)
        end
    end

    describe "invited_to_interview?" do

        it "should be true iff pending, admissions_decision = 'Invite to Interview', and they were invited to interview less than 2 weeks ago and haven't conducted it yet" do
            now = Time.now
            allow(Time).to receive(:now).and_return(now)
            cohort_application = CohortApplication.first

            allow(cohort_application).to receive(:status).and_return('pending')
            allow(cohort_application).to receive(:admissions_decision).and_return('Invite to Interview')
            allow(cohort_application).to receive(:invited_to_interview_at).and_return(now - 14.days + 1.minute) # just less than 14 days ago
            expect(cohort_application.invited_to_interview?).to be(true)

            allow(cohort_application).to receive(:status).and_return('not_pending')
            expect(cohort_application.invited_to_interview?).to be(false)
            allow(cohort_application).to receive(:status).and_return('pending')
            expect(cohort_application.invited_to_interview?).to be(true)

            allow(cohort_application).to receive(:admissions_decision).and_return('NOT Invite to Interview')
            expect(cohort_application.invited_to_interview?).to be(false)
            allow(cohort_application).to receive(:admissions_decision).and_return('Invite to Interview')
            expect(cohort_application.invited_to_interview?).to be(true)

            allow(cohort_application).to receive(:rubric_interview).and_return('Conducted')
            expect(cohort_application.invited_to_interview?).to be(false)
            allow(cohort_application).to receive(:rubric_interview).and_return(nil)
            expect(cohort_application.invited_to_interview?).to be(true)

            allow(cohort_application).to receive(:invited_to_interview_at).and_return(nil) # never invited to interview
            expect(cohort_application.invited_to_interview?).to be(false)
            allow(cohort_application).to receive(:invited_to_interview_at).and_return(now - 14.days) # exactly 14 days ago
            expect(cohort_application.invited_to_interview?).to be(false)
            allow(cohort_application).to receive(:invited_to_interview_at).and_return(now - 14.days - 1.minute) # just more than 14 days ago
            expect(cohort_application.invited_to_interview?).to be(false)
            allow(cohort_application).to receive(:invited_to_interview_at).and_return(now - 14.days + 1.minute) # just less than 14 days ago
            expect(cohort_application.invited_to_interview?).to be(true)
        end
    end

    describe "set_rejected_after_pre_accepted" do

        it "should be called on update if status_changed?" do
            user = users(:pre_accepted_mba_cohort_user)
            cohort_application = user.cohort_applications.first
            expect(cohort_application).to receive(:set_rejected_after_pre_accepted)
            cohort_application.update!(status: 'rejected')
        end

        it "should set rejected_after_pre_accepted to true if status changed from 'pre_accepted' to 'rejected'" do
            user = users(:pre_accepted_mba_cohort_user)
            cohort_application = user.cohort_applications.first
            expect(cohort_application.rejected_after_pre_accepted).to be(false)
            expect(cohort_application).to receive(:set_rejected_after_pre_accepted).and_call_original
            cohort_application.update!(status: 'rejected')
            expect(cohort_application.rejected_after_pre_accepted).to be(true)
        end

        it "should set rejected_after_pre_accepted to false otherwise" do
            user = users(:rejected_mba_cohort_user)
            cohort_application = user.cohort_applications.first
            cohort_application.update_column(:rejected_after_pre_accepted, true)
            expect(cohort_application).to receive(:set_rejected_after_pre_accepted).and_call_original
            cohort_application.update!(status: 'pre_accepted')
            expect(cohort_application.rejected_after_pre_accepted).to be(false)
        end
    end

    describe "warn_if_switching_from_mba_emba?" do
        it "should only be true if MBA or EMBA, pending, and Invited to Interview or to Convert to EMBA" do
            user = users(:pending_mba_cohort_user)
            instance = user.cohort_applications.first
            instance.admissions_decision = 'Invite to Interview'
            expect(instance.warn_if_switching_from_mba_emba?).to be(true)

            instance.admissions_decision = 'Invite to Convert to EMBA'
            expect(instance.warn_if_switching_from_mba_emba?).to be(true)

            instance.cohort = @emba_cohort
            expect(instance.warn_if_switching_from_mba_emba?).to be(true)

            instance.admissions_decision = 'Reject'
            expect(instance.warn_if_switching_from_mba_emba?).to be(false)

            instance.admissions_decision = 'Invite to Interview'
            instance.status = 'accepted'
            expect(instance.warn_if_switching_from_mba_emba?).to be(false)

            instance.status = 'pending'
            instance.cohort = @cno_cohort
            expect(instance.warn_if_switching_from_mba_emba?).to be(false)

        end
    end

    describe "converted_pending_application_to_emba_upon_invitation?" do

        it "should be true if program_type is 'emba', status is 'pending', and admissions_decision is 'Invite to Convert to EMBA'" do
            user = users(:pending_emba_cohort_user)
            cohort_application = user.cohort_applications.first

            allow(cohort_application).to receive(:program_type).and_return('emba')
            allow(cohort_application).to receive(:status).and_return('pending')
            allow(cohort_application).to receive(:admissions_decision).and_return('Invite to Convert to EMBA')
            expect(cohort_application.converted_pending_application_to_emba_upon_invitation?).to be(true)

            allow(cohort_application).to receive(:program_type).and_return('not_emba')
            expect(cohort_application.converted_pending_application_to_emba_upon_invitation?).not_to be(true)
            allow(cohort_application).to receive(:program_type).and_return('emba')

            allow(cohort_application).to receive(:status).and_return('not_pending')
            expect(cohort_application.converted_pending_application_to_emba_upon_invitation?).not_to be(true)
            allow(cohort_application).to receive(:status).and_return('pending')

            allow(cohort_application).to receive(:admissions_decision).and_return('Invite NOT to Convert to EMBA')
            expect(cohort_application.converted_pending_application_to_emba_upon_invitation?).not_to be(true)
        end
    end

    describe "can_convert_to_emba?" do

        it "should be true if program_type is not 'emba', status is 'pending', and admissions_decision is 'Invite to Convert to EMBA'" do
            user = users(:pending_emba_cohort_user)
            cohort_application = user.cohort_applications.first

            allow(cohort_application).to receive(:program_type).and_return('not_emba')
            allow(cohort_application).to receive(:status).and_return('pending')
            allow(cohort_application).to receive(:admissions_decision).and_return('Invite to Convert to EMBA')
            expect(cohort_application.can_convert_to_emba?).to be(true)

            allow(cohort_application).to receive(:program_type).and_return('emba')
            expect(cohort_application.can_convert_to_emba?).not_to be(true)
            allow(cohort_application).to receive(:program_type).and_return('not_emba')

            allow(cohort_application).to receive(:status).and_return('not_pending')
            expect(cohort_application.can_convert_to_emba?).not_to be(true)
            allow(cohort_application).to receive(:status).and_return('pending')

            allow(cohort_application).to receive(:admissions_decision).and_return('Invite NOT to Convert to EMBA')
            expect(cohort_application.can_convert_to_emba?).not_to be(true)
        end
    end

    describe "converted_to_emba?" do

        it "should be true if the program_type changed to 'emba' and the admissions_decision was 'Invite to Convert to EMBA' and didn't change" do
            user = users(:pending_mba_cohort_user)
            cohort_application = user.cohort_applications.first

            # shouldn't have any versions of the cohort application where admissions_decision was 'Invite to Convert to EMBA'
            # and we shouldn't have any versions where the program_type was 'emba', so converted_to_emba? should be false
            expect(CohortApplication::Version.where(id: cohort_application.id).pluck(:admissions_decision)).not_to include('Invite to Convert to EMBA')
            expect(CohortApplication::Version.includes(:cohort).where(id: cohort_application.id).pluck(:program_type)).not_to include('emba')
            expect(cohort_application.converted_to_emba?).to be(false)

            cohort_application.update_column(:admissions_decision, 'Invite to Convert to EMBA')

            # now we should have a version where the admissions_decision was set to 'Invite to Convert to EMBA',
            # but still not any where the program_type was 'emba', so converted_to_emba? should still be false
            expect(CohortApplication::Version.where(id: cohort_application.id).pluck(:admissions_decision)).to include('Invite to Convert to EMBA')
            expect(cohort_application.converted_to_emba?).to be(false)

            # Kind of annoying, but as we progress through this spec and update columns, the version_created_at timestamps
            # on the new versions will have the same values. Since the converted_to_emba? method orders by version_created_at
            # we need to ensure that the version_created_at timestamps on the version records are unique by updating the
            # version_created_at timestmap on each versions record. This is more reflective of an organic flow anyway.
            CohortApplication::Version.where(id: cohort_application.id, admissions_decision: 'Invite to Convert to EMBA').first.update_column(:version_created_at, Time.now + 1.minute)

            cohort_application.update_column(:cohort_id, @emba_cohort.id)
            CohortApplication::Version.includes(:cohort).where(id: cohort_application.id, admissions_decision: 'Invite to Convert to EMBA', cohort_id: @emba_cohort.id).first.update_column(:version_created_at, Time.now + 2.minutes)

            # Now we have versions that emulate the organic flow for a cohort application that would have applied to a cohort
            # other than the EMBA, been invited to convert to the EMBA, and then converted, so converted_to_emba? should be true
            expect(CohortApplication::Version.includes(:cohort).where(id: cohort_application.id).pluck(:program_type)).to include('emba')
            expect(cohort_application.converted_to_emba?).to be(true)
        end

        it "should be false if program_type didn't change to emba" do
            user = users(:pending_mba_cohort_user)
            cohort_application = user.cohort_applications.first

            # even though we have a version of the cohort application where the admissions_decision is 'Invite to Convert to EMBA',
            # if the program_type hasn't changed to emba afterwards, converted_to_emba? should be false
            cohort_application.update_column(:admissions_decision, 'Invite to Convert to EMBA')
            expect(CohortApplication::Version.includes(:cohort).where(id: cohort_application.id).pluck(:program_type)).not_to include('emba')
            expect(cohort_application.converted_to_emba?).to be(false)
        end

        it "should be false if admissions_decision wasn't changed to 'Invite to Convert to EMBA' before switch to emba" do
            user = users(:pending_mba_cohort_user)
            cohort_application = user.cohort_applications.first

            # shouldn't have any versions where admissions_decision was 'Invite to Convert to EMBA',
            # so converted_to_emba? should be false
            expect(CohortApplication::Version.where(id: cohort_application.id).pluck(:admissions_decision)).not_to include('Invite to Convert to EMBA')
            expect(cohort_application.converted_to_emba?).to be(false)

            cohort_application.update_column(:cohort_id, @emba_cohort.id)

            # we've created a version where the program_type is emba, but none where the admissions_decision is 'Invite to Convert to EMBA',
            # so converted_to_emba? should be false
            expect(CohortApplication::Version.includes(:cohort).where(id: cohort_application.id).pluck(:program_type)).to include('emba')
            expect(cohort_application.converted_to_emba?).to be(false)

            # now we create a version after the program_type has switched to emba where the admissions_decision is 'Invite to Convert to EMBA',
            # but since the change in admissions_decision happened after the program_type switch, converted_to_emba? should be false
            cohort_application.update_column(:admissions_decision, 'Invite to Convert to EMBA')

            # Kind of annoying, but as we progress through this spec and update columns, the version_created_at timestamps
            # on the new versions will have the same values. Since the converted_to_emba? method orders by version_created_at
            # we need to ensure that the version_created_at timestamps on the version records are unique by updating the
            # version_created_at timestmap on each versions record. This is more reflective of an organic flow anyway.
            CohortApplication::Version.where(id: cohort_application.id, admissions_decision: 'Invite to Convert to EMBA').first.update_column(:version_created_at, Time.now + 1.minute)
            expect(cohort_application.converted_to_emba?).to be(false)
        end

        it "should handle program_type flip-flopping appropriately" do
            user = users(:pending_emba_cohort_user)
            cohort_application = user.cohort_applications.first

            expect(CohortApplication::Version.includes(:cohort).where(id: cohort_application.id).pluck(:program_type).size).to eq(1)
            expect(cohort_application.program_type).to eq('emba')

            cohort_application.update_column(:cohort_id, @mba_cohort.id)

            # The cohort application started off as an EMBA cohort application, but then they switched the program_type to 'mba'.
            # At this point we shouldn't have any versions where the admissions_decision is 'Invite to Convert to EMBA', so
            # converted_to_emba? should be false.
            expect(CohortApplication::Version.includes(:cohort).where(id: cohort_application.id).pluck(:program_type)).to include('mba')
            expect(CohortApplication::Version.where(id: cohort_application.id).pluck(:admissions_decision)).not_to include('Invite to Convert to EMBA')

            cohort_application.update_column(:admissions_decision, 'Invite to Convert to EMBA')

            # Kind of annoying, but as we progress through this spec and update columns, the version_created_at timestamps
            # on the new versions will have the same values. Since the converted_to_emba? method orders by version_created_at
            # we need to ensure that the version_created_at timestamps on the version records are unique by updating the
            # version_created_at timestmap on each versions record. This is more reflective of an organic flow anyway.
            CohortApplication::Version.where(id: cohort_application.id, admissions_decision: 'Invite to Convert to EMBA').first.update_column(:version_created_at, Time.now + 1.minute)

            # The admissions_decision has now changed to 'Invite to Convert to EMBA', but even though the cohort_application started
            # off with the 'emba' program_type, converted_to_emba? should still be false because they haven't switched to the 'emba'
            # program_type after they were invited i.e. their admissions_decision was changed to 'Invite to Convert to EMBA'.
            expect(cohort_application.admissions_decision).to eq('Invite to Convert to EMBA')
            expect(cohort_application.converted_to_emba?).to be(false)

            # Let's pretend the user accidentally changed their program_type to something other than 'emba' after they were invited
            # to convert to the EMBA.
            cohort_application.update_column(:cohort_id, @bc_cohort.id)

            # Kind of annoying, but as we progress through this spec and update columns, the version_created_at timestamps
            # on the new versions will have the same values. Since the converted_to_emba? method orders by version_created_at
            # we need to ensure that the version_created_at timestamps on the version records are unique by updating the
            # version_created_at timestmap on each versions record. This is more reflective of an organic flow anyway.
            CohortApplication::Version.where(id: cohort_application.id, cohort_id: @bc_cohort.id).first.update_column(:version_created_at, Time.now + 2.minutes)

            # Since their program_type wasn't changed to 'emba' after their admissions_decision was set to 'Invite to Convert to EMBA',
            # converted_to_emba? should be false.
            expect(cohort_application.converted_to_emba?).to be(false)

            cohort_application.update_column(:cohort_id, @emba_cohort.id)

            # Kind of annoying, but as we progress through this spec and update columns, the version_created_at timestamps
            # on the new versions will have the same values. Since the converted_to_emba? method orders by version_created_at
            # we need to ensure that the version_created_at timestamps on the version records are unique by updating the
            # version_created_at timestmap on each versions record. This is more reflective of an organic flow anyway.
            CohortApplication::Version.where(id: cohort_application.id, cohort_id: @emba_cohort.id, admissions_decision: 'Invite to Convert to EMBA').first.update_column(:version_created_at, Time.now + 3.minutes)

            # Now they've finally changed their program_type to 'emba' after they were invited to convert to the EMBA,
            # so converted_to_emba? should be true because they converted to the EMBA while their admissions_decision
            # was still 'Invite to Convert to EMBA'
            expect(cohort_application.admissions_decision).to eq('Invite to Convert to EMBA')
            expect(cohort_application.converted_to_emba?).to be(true)
        end
    end

    describe "prior_applications_decisions_string" do
        it "should work" do
            now = Time.now

            learner = users(:learner)
            learner.cohort_applications.destroy_all

            app = CohortApplication.create_from_hash!({
                cohort_id: Cohort.ids.first,
                status: 'pending',
                user_id: @user.id,
                applied_at: now
            })

            app.update_columns(status: 'rejected', applied_at: now)
            another_app = CohortApplication.create_from_hash!({
                cohort_id: Cohort.ids.second,
                status: 'pending',
                user_id: @user.id
            })
            another_app.update_columns(applied_at: now + 1.minute)

            app.update!(admissions_decision: 'foo')
            CohortApplication::Version.where(id: app.id, admissions_decision: 'foo').first.update_columns(version_created_at: now)
            expect(app.send(:prior_applications_decisions_string)).to be_empty
            expect(another_app.send(:prior_applications_decisions_string)).to eq("#{app.cohort.name} - foo".upcase)

            app.update!(admissions_decision: nil)
            expect(app.send(:prior_applications_decisions_string)).to be_empty
            expect(another_app.send(:prior_applications_decisions_string)).to eq("#{app.cohort.name} - foo".upcase)

            app.update!(admissions_decision: 'bar')
            CohortApplication::Version.where(id: app.id, admissions_decision: 'bar').first.update_columns(version_created_at: now + 1.minute)
            expect(app.send(:prior_applications_decisions_string)).to be_empty
            expect(another_app.send(:prior_applications_decisions_string)).to eq("#{app.cohort.name} - foo,\n#{app.cohort.name} - bar".upcase)

            another_app.update!(admissions_decision: 'baz')
            CohortApplication::Version.where(id: another_app.id, admissions_decision: 'baz').first.update_columns(version_created_at: now + 2.minutes)
            expect(app.send(:prior_applications_decisions_string)).to be_empty
            expect(another_app.send(:prior_applications_decisions_string)).to eq("#{app.cohort.name} - foo,\n#{app.cohort.name} - bar".upcase)

            another_app.update_columns(status: 'rejected')
            yet_another_app = CohortApplication.create_from_hash!({
                cohort_id: Cohort.ids.third,
                status: 'pending',
                user_id: @user.id
            })
            yet_another_app.update_columns(applied_at: now + 2.minutes)

            expect(another_app.send(:prior_applications_decisions_string)).to eq("#{app.cohort.name} - foo,\n#{app.cohort.name} - bar".upcase)
            expect(yet_another_app.send(:prior_applications_decisions_string)).to eq("#{app.cohort.name} - foo,\n#{app.cohort.name} - bar,\n#{another_app.cohort.name} - baz".upcase)
        end
    end

    describe "should_have_enrollment_agreement?" do

        before(:each) do
            @application_requiring_agreement = CohortApplication.where.not(scholarship_level: nil).first
            allow(@application_requiring_agreement).to receive(:published_cohort).and_return(@emba_cohort)
            allow(@emba_cohort).to receive(:requires_enrollment_agreement?).and_return(true)
            allow(@emba_cohort).to receive(:supports_payments?).and_return(false)
            allow(@emba_cohort).to receive(:enrollment_agreement_template_id).and_return('some_template_id')
            @application_requiring_agreement.status = 'accepted'
        end

        it "should be true with an appropriate cohort and status" do
            expect(@application_requiring_agreement.should_have_enrollment_agreement?).to be(true)
        end

        it "should be false with an invalid status" do
            @application_requiring_agreement.status = 'pending'
            expect(@application_requiring_agreement.should_have_enrollment_agreement?).to be(false)
        end

        it "should be false without an enrollment_agreement_template_id" do
            allow(@emba_cohort).to receive(:enrollment_agreement_template_id).and_return(nil)
            expect(@application_requiring_agreement.should_have_enrollment_agreement?).to be(false)
        end

        it "should be false if not requires_enrollment_agreement?" do
            allow(@emba_cohort).to receive(:requires_enrollment_agreement?).and_return(false)
            expect(@application_requiring_agreement.should_have_enrollment_agreement?).to be(false)
        end

        it "should be false if user is deferred and it is prior to the cohort decision date" do
            deferred_app = CohortApplication.create_from_hash!(valid_attrs)
            deferred_app.update(status: 'deferred')

            expect(@application_requiring_agreement.user).to receive(:deferred_info).exactly(2).times {
                OpenStruct.new(last_deferred_application: deferred_app)
            }

            expect(@emba_cohort).to receive(:decision_date) { Time.now + 1.day } # prior to decision
            expect(@application_requiring_agreement.should_have_enrollment_agreement?).to be(false)

            expect(@emba_cohort).to receive(:decision_date) { Time.now - 1.day } # after decision
            expect(@application_requiring_agreement.should_have_enrollment_agreement?).to be(true)
        end

        it "should be reflect registered status if supports_payments?" do
            allow(@emba_cohort).to receive(:supports_payments?).and_return(true)
            @application_requiring_agreement.registered = true
            expect(@application_requiring_agreement.should_have_enrollment_agreement?).to be(true)
            @application_requiring_agreement.registered = false
            expect(@application_requiring_agreement.should_have_enrollment_agreement?).to be(false)
        end
    end

    describe "enrollment_agreement" do

        it "should return an agreement for this program_type" do
            ca = CohortApplication.first
            ca.user.signable_documents.delete_all
            signable_document = SignableDocument.create!({
                document_type: SignableDocument::TYPE_ENROLLMENT_AGREEMENT,
                user_id: ca.user_id,
                metadata: {
                    program_type: ca.program_type
                }
            })
            ca.user.signable_documents.reload
            expect(ca.enrollment_agreement).to eq(signable_document)
        end

        it "should not return an agreement for a different program_type" do
            ca = CohortApplication.first
            ca.user.signable_documents.delete_all
            signable_document = SignableDocument.create!({
                document_type: SignableDocument::TYPE_ENROLLMENT_AGREEMENT,
                user_id: ca.user_id,
                metadata: {
                    program_type: 'another program_type'
                }
            })
            ca.user.signable_documents.reload
            expect(ca.enrollment_agreement).to be_nil
        end

    end

    describe "create_enrollment_agreement" do
        it "should work" do
            user = users(:pending_emba_cohort_user)
            cohort_application = user.cohort_applications.first
            expect(cohort_application).to receive(:enrollment_agreement_template_id).at_least(1).times.and_return('template_id')

            # we need to return an actual document so the reloading in the method works
            document = SignableDocument.create!(
                :user_id => user.id,
                document_type: 'enrollment_agreement'
            )
            expect(cohort_application).to receive(:sign_now_smart_field_values).and_return({ some: 'values' })
            expect(SignableDocument).to receive(:create_with_sign_now).with({
                document_type: SignableDocument::TYPE_ENROLLMENT_AGREEMENT,
                user_id: user.id,
                template_id: cohort_application.enrollment_agreement_template_id,
                smart_field_values: { some: 'values' },
                metadata: {
                    cohort_id: cohort_application.cohort_id,
                    program_type: cohort_application.program_type,
                    cohort_application_id: cohort_application.id,
                    follow_up_attempt: false
                }
            }).and_return(document)
            cohort_application.create_enrollment_agreement(false)
        end
    end

    describe "enrollment_agreement resiliency" do

        before(:each) do
            document = SignNow::Document.new(
                id: SecureRandom.uuid
            )
            allow(document).to receive(:generate_signing_link).and_return(OpenStruct.new({
                url: "http://path/to_doc",
                expiry: Time.now + 1.week
            }))
            allow(document).to receive(:update_smart_fields)

            allow(SignNow::Document).to receive(:create).and_return(document)
            allow_any_instance_of(Cohort::Version).to receive(:enrollment_agreement_template_id).and_return('template_id')
        end

        describe "need_to_generate_new_enrollment_agreement?" do

            it "should be false if should_have_enrollment_agreement? == false" do
                cohort_application = CohortApplication.new
                expect(cohort_application).to receive(:should_have_enrollment_agreement?).and_return(false)
                expect(cohort_application).not_to receive(:enrollment_agreement)
                expect(cohort_application.need_to_generate_new_enrollment_agreement?).to be(false)
            end

            it "should be false if should_have_enrollment_agreement?, but the agreement is already signed" do
                cohort_application = CohortApplication.first
                enrollment_agreement = cohort_application.create_enrollment_agreement(false)
                expect(enrollment_agreement).not_to be_nil

                enrollment_agreement.metadata['cohort_id'] = cohort_application.cohort_id
                enrollment_agreement.signed_at = Time.now
                enrollment_agreement.save!

                expect(cohort_application.need_to_generate_new_enrollment_agreement?).to be(false)
            end

            it "should be false if should_have_enrollment_agreement?, the agreement is unsigned, but not yet expired" do
                cohort_application = CohortApplication.first
                enrollment_agreement = cohort_application.create_enrollment_agreement(false)
                expect(enrollment_agreement).not_to be_nil

                enrollment_agreement.metadata['cohort_id'] = cohort_application.cohort_id
                enrollment_agreement.save!

                expect(cohort_application.need_to_generate_new_enrollment_agreement?).to be(false)
            end

            it "should be true if should_have_enrollment_agreement?, the agreement is unsigned, and expired" do
                cohort_application = CohortApplication.first
                enrollment_agreement = cohort_application.create_enrollment_agreement(false)
                expect(enrollment_agreement).not_to be_nil

                enrollment_agreement.metadata['cohort_id'] = cohort_application.cohort_id
                enrollment_agreement.sign_now_signing_link_expiry = Time.now - 1.minute
                enrollment_agreement.save!

                expect(cohort_application.need_to_generate_new_enrollment_agreement?).to be(true)
            end

            it "should be true if should_have_enrollment_agreement?, the agreement is unsigned, not-expired, but for a differing cohort" do
                cohort_application = CohortApplication.first
                enrollment_agreement = cohort_application.create_enrollment_agreement(false)
                expect(enrollment_agreement).not_to be_nil

                enrollment_agreement.metadata['cohort_id'] = SecureRandom.uuid
                enrollment_agreement.sign_now_signing_link_expiry = Time.now + 1.minute
                enrollment_agreement.save!

                expect(cohort_application.need_to_generate_new_enrollment_agreement?).to be(true)
            end

        end

        describe "ensure_enrollment_agreement_if_necessary" do

            attr_accessor :cohort_application, :enrollment_agreement

            before(:each) do
                @cohort_application = CohortApplication.first
                @enrollment_agreement = cohort_application.create_enrollment_agreement(false)
                expect(enrollment_agreement).not_to be_nil
            end

            it "should do nothing if need_to_generate_new_enrollment_agreement? == false" do
                expect(cohort_application).to receive(:need_to_generate_new_enrollment_agreement?).and_return(false)
                expect(cohort_application).not_to receive(:enrollment_agreement)
                cohort_application.ensure_enrollment_agreement_if_necessary
            end

            it "should perform immediately by default" do
                expect(cohort_application).to receive(:need_to_generate_new_enrollment_agreement?).and_return(true)
                expect(EnsureEnrollmentAgreementJob).to receive(:perform_now).with(cohort_application.id)
                cohort_application.ensure_enrollment_agreement_if_necessary
            end

            it "should support the enqueue flag" do
                expect(cohort_application).to receive(:need_to_generate_new_enrollment_agreement?).and_return(true)
                expect(EnsureEnrollmentAgreementJob).to receive(:ensure_job).with(cohort_application.id)
                cohort_application.ensure_enrollment_agreement_if_necessary(true)
            end

        end

        describe "queue_ensure_enrollment_agreement_job_if_necessary" do

            it "should call ensure_enrollment_agreement_if_necessary with the enqueue flag" do
                cohort_application = CohortApplication.new
                expect(cohort_application).to receive(:ensure_enrollment_agreement_if_necessary).with(true)
                cohort_application.queue_ensure_enrollment_agreement_job_if_necessary
            end

        end

    end

    describe "sign_now_smart_field_values" do
        it "should return nil if not MBA or EMBA" do
            cohort = cohorts(:published_emba_cohort)
            cohort_application = cohort.cohort_applications.find_by_status('accepted')
            cohort_application.update_column(:cohort_id, @bc_cohort.id)
            expect(cohort_application.sign_now_smart_field_values).to be_nil
        end

        it "should return appropriate values for MBA" do
            cohort = cohorts(:published_mba_cohort)
            cohort_application = cohort.cohort_applications.find_by_status('accepted')
            expect(cohort_application.sign_now_smart_field_values).to eq({
                email: cohort_application.user.email,
                program_name: cohort_application.published_cohort.title,
                program_start: cohort_application.published_cohort.start_date.strftime("%m/%d/%Y"),
                program_end: cohort_application.published_cohort.end_date.strftime("%m/%d/%Y"),
                tuition: "$0.00",
                fees: "$0.00",
                total_cost: "$0.00",
                created_at: DateTime.now.strftime("%m/%d/%Y")
            })
        end

        it "should return appropriate values for EMBA" do
            cohort = cohorts(:published_emba_cohort)
            cohort_application = cohort.cohort_applications.find_by_status('accepted')
            stripe_plan = cohort_application.stripe_plans.first
            cohort_application.stripe_plan_id = stripe_plan['id']
            cohort_application.scholarship_level = cohort_application.scholarship_levels[1]
            cohort_application.registered = true
            cohort_application.registered_early = true

            expect(cohort_application.sign_now_smart_field_values).to eq({
                email: cohort_application.user.email,
                program_name: cohort_application.published_cohort.title,
                program_start: cohort_application.published_cohort.start_date.strftime("%m/%d/%Y"),
                program_end: cohort_application.published_cohort.end_date.strftime("%m/%d/%Y"),
                tuition: "$9,600.00",
                scholarship: "$1,800.00",
                discount: "$1,200.00",
                fees: "$0.00",
                total_cost: "$6,600.00",
                created_at: DateTime.now.strftime("%m/%d/%Y")
            })
        end
    end

    describe "set_final_score" do

        it "should not call FinalScoreHelper.calculate_final_score if program type does not support final score" do
            cohort = cohorts(:published_career_network_only_cohort)
            cohort_application = cohort.cohort_applications.first
            expect(CohortApplication::FinalScoreHelper).not_to receive(:calculate_final_score)
            cohort_application.set_final_score
        end

        it "should call FinalScoreHelper.calculate_final_score if program type supports final score" do
            cohort = cohorts(:published_mba_cohort)
            cohort_application = cohort.cohort_applications.first
            expect(CohortApplication::FinalScoreHelper).to receive(:calculate_final_score).with(cohort_application).exactly(1).times.and_call_original
            cohort_application.set_final_score
        end

        it "should not set the scores if a very tiny amount" do
            cohort = cohorts(:published_mba_cohort)
            cohort_application = cohort.cohort_applications.first
            cohort_application.update_columns(final_score: 1.33333337, project_score: 1.44444447)
            expect(CohortApplication::FinalScoreHelper).to receive(:calculate_final_score).and_return({
                final_score: 1.33333338,
                project_score: 1.44444448
            })
            cohort_application.set_final_score
            expect(cohort_application.final_score).to be(1.33333337)
            expect(cohort_application.final_score_changed?).to be(false)
            expect(cohort_application.project_score).to be(1.44444447)
            expect(cohort_application.project_score_changed?).to be(false)
        end

    end

    describe "transfer_payment_info_from_previous_application" do

        it "should transfer information when a payment has been made on a previous application" do
            cohort_application, previous_cohort_application = create_application_with_previous_expelled_application(
                num_charged_payments: 2,
                num_refunded_payments: 1
            )

            %w(
                stripe_plan_id
                total_num_required_stripe_payments
                num_charged_payments
                num_refunded_payments
                stripe_plans
                scholarship_levels
                scholarship_level
                registered_early
            ).each do |key|
                expect(previous_cohort_application[key]).not_to be_nil, "#{key} is nil" #sanity check
                expect(cohort_application[key]).to eq(previous_cohort_application[key]), "#{key} does not match"
            end
        end

        it "should not transfer certain information" do
            cohort_application, previous_cohort_application = create_application_with_previous_expelled_application(
                num_charged_payments: 2,
                num_refunded_payments: 1
            )

            ['registered', 'payment_grace_period_end_at', 'locked_due_to_past_due_payment'].each do |key|
                expect(previous_cohort_application[key]).not_to be_nil, "#{key} is nil" #sanity check
                expect(cohort_application[key]).not_to eq(previous_cohort_application[key]), "#{key} matches"
            end
        end

        it "should not transfer information when no payment has been made on a previous application" do
            cohort_application, previous_cohort_application = create_application_with_previous_expelled_application(
                num_charged_payments: 0,
                num_refunded_payments: 0
            )

            expect(cohort_application.stripe_plan_id).to be_nil
        end

        it "should not transfer information to an application for a different program" do
            cohort_application, previous_cohort_application = create_application_with_previous_expelled_application(
                num_charged_payments: 2,
                num_refunded_payments: 1,
                new_cohort_program_type: 'mba'
            )

            expect(cohort_application.stripe_plan_id).to be_nil
        end

        def create_application_with_previous_expelled_application(num_charged_payments:, num_refunded_payments:, new_cohort_program_type: 'emba')
            cohort = cohorts(:published_emba_cohort)
            user = cohort.accepted_users.first
            previous_cohort_application = user.cohort_applications[0]
            previous_cohort_application.update_columns(
                status: 'expelled',
                stripe_plan_id: 'some_plan',
                total_num_required_stripe_payments: 12,
                num_charged_payments: num_charged_payments,
                num_refunded_payments: num_refunded_payments,
                stripe_plans: [{"id"=>"some_plan", "name"=>"Monthly Plan", "amount"=>"80000", "frequency"=>"monthly"}],
                scholarship_levels: [{"name"=>"Level 1",
                    "standard"=>{"emba_800"=>{"id"=>"discount_150", "amount_off"=>15000, "percent_off"=>nil}, "emba_bi_4800"=>{"id"=>"discount_1400", "amount_off"=>140000, "percent_off"=>nil}},
                    "early"=>{"emba_800"=>{"id"=>"discount_150", "amount_off"=>15000, "percent_off"=>nil}, "emba_bi_4800"=>{"id"=>"discount_1400", "amount_off"=>140000, "percent_off"=>nil}}},
                ],
                scholarship_level: {"name"=>"Level 1",
                    "standard"=>{"emba_800"=>{"id"=>"discount_150", "amount_off"=>15000, "percent_off"=>nil}, "emba_bi_4800"=>{"id"=>"discount_1400", "amount_off"=>140000, "percent_off"=>nil}},
                    "early"=>{"emba_800"=>{"id"=>"discount_150", "amount_off"=>15000, "percent_off"=>nil}, "emba_bi_4800"=>{"id"=>"discount_1400", "amount_off"=>140000, "percent_off"=>nil}}},
                registered_early: true,
                registered: true,
                payment_grace_period_end_at: Time.now,
                locked_due_to_past_due_payment: true
            )

            cohort_application = CohortApplication.create!(
                applied_at: Time.now,
                user_id: user.id,
                cohort_id: Cohort.all_published.where(program_type: new_cohort_program_type).where.not(id: cohort.id).first.id,
                status: 'pending'
            )

            [cohort_application, previous_cohort_application]
        end


    end

    describe "queue_ensure_stripe_customer_job" do

        attr_accessor :cohort_application

        before(:each) do
            @cohort_application = CohortApplication.create!(
                user_id: @user.id,
                cohort_id: @emba_cohort.id,
                applied_at: Time.now,
                status: 'pending'
            )
        end

        it "should be called from an after_save if saved_change_to_accepted_or_pre_accepted? and program_type_config supports payments" do
            allow(cohort_application.program_type_config).to receive(:supports_payments?).and_return(true)
            expect(cohort_application).to receive(:queue_ensure_stripe_customer_job)
            cohort_application.update!(status: 'pre_accepted')
        end

        it "should not be called if !saved_change_to_accepted_or_pre_accepted?" do
            expect(cohort_application).not_to receive(:queue_ensure_stripe_customer_job)
            cohort_application.update!(status: 'rejected')
        end

        it "should not be called if program_type_config does not support payments" do
            allow(cohort_application.program_type_config).to receive(:supports_payments?).and_return(false)
            expect(cohort_application).not_to receive(:queue_ensure_stripe_customer_job)
            cohort_application.update!(status: 'pre_accepted')
        end

        it "should enqueue a EnsureStripeCustomerJob" do
            allow(cohort_application.program_type_config).to receive(:supports_payments?).and_return(true)
            expect(EnsureStripeCustomerJob).to receive(:perform_later).with(cohort_application.user.id)
            cohort_application.update!(status: 'pre_accepted')
        end

    end

    def valid_attrs(attrs = {})
        {
            cohort_id: Cohort.ids.first,
            status: 'pending',
            user_id: @user.id,
            applied_at: Time.now
        }.with_indifferent_access.merge(attrs)
    end

    def assert_attrs(obj, attrs)
        actual_attrs = obj.attributes.slice(*attrs.keys.map(&:to_s)).stringify_keys
        expect(actual_attrs).to eq(attrs.stringify_keys)
    end

    def assert_event_logged(&block)
        expect(Event).to receive(:create_server_event!).times do |id, user_id, event_type, attrs|

            yield(id, user_id, event_type, attrs)
            mock_event = "mock_event_for_#{user_id}"
            if event_type == 'cohort:retargeted_from_program_type_changed'
                expect(mock_event).to receive(:log_to_external_systems).with(no_args)
            else
                expect(mock_event).to receive(:log_to_external_systems).with(nil, false)
            end
            mock_event
        end
    end

end
