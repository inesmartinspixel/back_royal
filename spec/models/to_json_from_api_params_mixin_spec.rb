require 'spec_helper'

describe ToJsonFromApiParamsHelpers::ToJsonFromApiParamsMixin do

    before(:each) do
        @MyClass = Class.new do
            include ToJsonFromApiParamsHelpers::ToJsonFromApiParamsMixin

            def initialize(params)
                @params = params.with_indifferent_access
            end
        end


    end

    describe "set_filters" do

        it "should sanitize uuid values" do
            expect {
                @MyClass.new({
                    filters: {some_uuid: 'not a uuid'}
                }).set_filters(some_uuid: :uuid)
            }.to raise_error(ArgumentError, 'Unsupported value for filter "some_uuid" of type uuid: "not a uuid"')

            uuid = SecureRandom.uuid
            expect(
                @MyClass.new({
                    filters: {some_uuid: uuid}
                }).set_filters(some_uuid: :uuid) 
            ).to eq({some_uuid: uuid}.with_indifferent_access)
        end

        it "should sanitize boolean values" do
            expect {
                @MyClass.new({
                    filters: {some_boolean: 'not a boolean'}
                }).set_filters(some_boolean: :bool)
            }.to raise_error(ArgumentError, 'Unsupported value for filter "some_boolean" of type bool: "not a boolean"')

            expect(
                @MyClass.new({
                    filters: {some_boolean: true}
                }).set_filters(some_boolean: :bool) 
            ).to eq({some_boolean: true}.with_indifferent_access)
        end

        it "should sanitize array_of_uuids values" do
            expect {
                @MyClass.new({
                    filters: {some_uuid: ['not a uuid']}
                }).set_filters(some_uuid: :array_of_uuids)
            }.to raise_error(ArgumentError, 'Unsupported value for filter "some_uuid" of type array_of_uuids: ["not a uuid"]')

            uuid = SecureRandom.uuid
            expect(
                @MyClass.new({
                    filters: {some_uuid: [uuid]}
                }).set_filters(some_uuid: :array_of_uuids) 
            ).to eq({some_uuid: [uuid]}.with_indifferent_access)
        end

        it "should sanitize time values" do
            expect {
                @MyClass.new({
                    filters: {some_time: 'not a time'}
                }).set_filters(some_time: :time)
            }.to raise_error(ArgumentError, 'Unsupported value for filter "some_time" of type time: "not a time"')

            time = Time.now
            expect(
                @MyClass.new({
                    filters: {some_time: time}
                }).set_filters(some_time: :time) 
            ).to eq({some_time: time}.with_indifferent_access)
        end

        it "should sanitize based on a regex" do
            regexp = /^\w+$/
            expect {
                @MyClass.new({
                    filters: {some_string: 'no numbers allowed 123'}
                }).set_filters(some_string: regexp)
            }.to raise_error(ArgumentError, 'Unsupported value for filter "some_string" of type regexp: "no numbers allowed 123"')

            expect(
                @MyClass.new({
                    filters: {some_string: 'okay'}
                }).set_filters(some_string: regexp) 
            ).to eq({some_string: 'okay'}.with_indifferent_access)
        end

    end

    describe "set_fields" do

        it "should support aliases" do

            expect(
                @MyClass.new({
                    fields: ['TEST_ALIAS']
                }).set_fields(
                    available_fields: ['a', 'b', 'c'], 
                    aliases: {TEST_ALIAS: ['a', 'c']}
                ) 
            ).to eq(['TEST_ALIAS', 'a', 'c'])

        end

    end

end
