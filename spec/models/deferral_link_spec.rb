# == Schema Information
#
# Table name: deferral_links
#
#  id                         :uuid             not null, primary key
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  user_id                    :uuid             not null
#  expires_at                 :datetime         not null
#  used                       :boolean          default(FALSE), not null
#  from_cohort_application_id :uuid             not null
#  to_cohort_application_id   :uuid
#
require 'spec_helper'

describe DeferralLink do
    fixtures(:users)

    describe "active" do
        attr_accessor :user,:from_cohort_application, :deferral_link

        before(:each) do
            @user = users(:accepted_emba_cohort_user)
            @from_cohort_application = user.accepted_application
            @deferral_link = DeferralLink.create!(
                user: user, 
                expires_at: Time.now + 30.days, 
                from_cohort_application: from_cohort_application
            )
        end

        it "should be true when applicable" do
            expect(deferral_link.active?).to be(true)
        end

        it "should be false if used" do
            deferral_link.update!(used: true)
            expect(deferral_link.active?).to be(false)
        end

        it "should be false if expired" do
            allow(Time).to receive(:now).and_return(Time.now + 31.days)
            expect(deferral_link.active?).to be(false)
        end

        it "should be false if the triggering application is no longer a valid deferral status" do
            from_cohort_application.update!(status: 'rejected')
            expect(deferral_link.active?).to be(false)
        end

        it "should be false if the triggering application is no longer the user's last" do
            from_cohort_application.update!(status: 'deferred')
            CohortApplication.create!(
                status: 'pending', 
                user_id: user.id, 
                cohort_id: Cohort.where.not(id: user.cohort_applications.pluck(:id)).first.id,
                applied_at: Time.now
            )
            user.reload
            expect(deferral_link.active?).to be(false)
        end
    end

    describe "validate" do
        it "should validate that an active deferral link does not exist" do
            user = users(:accepted_emba_cohort_user)
            active_deferral_link = DeferralLink.create!(user: user, expires_at: Time.now + 30.days, from_cohort_application_id: user.accepted_application.id)
            
            invalid_deferral_link = DeferralLink.create(user: user, expires_at: Time.now + 30.days, from_cohort_application_id: user.accepted_application.id)
            expect(invalid_deferral_link.valid?).to be(false)
            expect(invalid_deferral_link.errors.full_messages).to include('User already has an active deferral link')
        end

        it "should validate that the status is allowed" do
            user = users(:accepted_emba_cohort_user)
            user.last_application.update!(status: 'rejected')
            invalid_deferral_link = DeferralLink.create(user: user, expires_at: Time.now + 30.days, from_cohort_application_id: user.last_application.id)
            expect(invalid_deferral_link.valid?).to be(false)
            expect(invalid_deferral_link.errors.full_messages).to include('User does not have an appropriate application status to create a deferral link')
        end
    end

    describe "upcoming_cohorts" do
        attr_accessor :user, :deferral_link, :published_mba_cohorts

        before(:each) do
            @user = users(:accepted_mba_cohort_user)
            @deferral_link = DeferralLink.create!(
                user: user, 
                expires_at: Time.now + 30.days, 
                from_cohort_application: user.accepted_application
            )

            @published_mba_cohorts = Cohort.all_published.where(program_type: 'mba').order(:start_date)
            expect(published_mba_cohorts.size).to be >= 3
        end

        it "should return cohorts after the cohort that generated the deferral link" do
            user.accepted_application.update!(cohort: published_mba_cohorts.second)
            allow(Time.now).to receive(:utc).and_return(published_mba_cohorts.second.start_date - 1.day)
            expect(deferral_link.reload.upcoming_cohorts.pluck(:id)).to match_array(published_mba_cohorts[3..].pluck(:id))
        end

        it "should return cohorts after now even if they are after the cohort that generated the deferral link" do
            user.accepted_application.update!(cohort: published_mba_cohorts.first)
            allow(Time.now).to receive(:utc).and_return(published_mba_cohorts.second.start_date + 1.day)
            expect(deferral_link.reload.upcoming_cohorts.pluck(:id)).to match_array(published_mba_cohorts[3..].pluck(:id))
        end

        it "should return cohorts that start within a year of the cohort that generated the deferral link" do
            user.accepted_application.update!(cohort: published_mba_cohorts.first)
            allow(Time.now).to receive(:utc).and_return(published_mba_cohorts.second.start_date - 1.day)
            published_mba_cohorts.second.update_columns(start_date: published_mba_cohorts.first.start_date + 2.years)
            expect(deferral_link.reload.upcoming_cohorts.pluck(:id)).to match_array(published_mba_cohorts[3..].pluck(:id))
        end
    end

    describe "as_json" do
        it "should include active and from_cohort_application if requested" do
            user = users(:accepted_emba_cohort_user)
            deferral_link = DeferralLink.create!(
                user: user, 
                expires_at: Time.now + 30.days, 
                from_cohort_application: user.accepted_application
            )

            regular_keys = deferral_link.as_json.keys
            expect(regular_keys).not_to include('active')
            expect(regular_keys).not_to include('from_cohort_application')

            with_fields_keys = deferral_link.as_json(fields: ['active', 'from_cohort_application']).keys
            expect(deferral_link.as_json(fields: ['active', 'from_cohort_application'])['active']).to be(true)
            expect(deferral_link.as_json(fields: ['active', 'from_cohort_application'])['from_cohort_application']).to eq(
                deferral_link.from_cohort_application.as_json
            )
        end
    end
end
