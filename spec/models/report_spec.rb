require 'spec_helper'
require 'report_spec_helper'

describe Report do
    include ReportSpecHelper

    describe "get_filter_options" do

        before(:each) do
            SafeCache.clear
        end

        it "should load sign_up_code options" do
            users = User.where.not(sign_up_code: nil).limit(2).to_a
            sign_up_code = users.first.sign_up_code
            users.second.update_attribute('sign_up_code', nil) # ensure there is a blank value

            filter_options = Report.get_filter_options('en')

            sign_up_code_entry = filter_options.detect { |entry| entry[:filter_type] == 'SignUpCodeFilter'}
            expect(sign_up_code_entry).not_to be_nil
            expect(sign_up_code_entry[:options]).to include({
                value: sign_up_code,
                text: sign_up_code
            })
            expect(sign_up_code_entry[:options]).to include({
                value: "__NULL__",
                text: "(No Sign Up Code)"
            })
        end

        it "should load cohort status/graduation_status options" do
            filter_options = Report.get_filter_options('en')

            cohort_status_entry = filter_options.detect { |entry| entry[:filter_type] == 'CohortStatusFilter'}
            expect(cohort_status_entry).not_to be_nil
            expect(cohort_status_entry[:options]).to include({
                value: 'pending',
                text: 'pending'
            })

            # there's a fixture with this graduation status
            expect(cohort_status_entry[:options]).to include({
                value: 'graduated',
                text: 'graduated'
            })
            expect(cohort_status_entry[:options]).to include({
                value: "__NULL__",
                text: "(No Applications)"
            })
        end

        it "should load institution options" do
            institution = Institution.first

            filter_options = Report.get_filter_options('en')

            institution_entry = filter_options.detect { |entry| entry[:filter_type] == 'InstitutionFilter'}
            expect(institution_entry).not_to be_nil
            expect(institution_entry[:options]).to include({
                value: institution.id,
                text: institution.name
            })
            expect(institution_entry[:options]).to include({
                value: "__NULL__",
                text: "(No Institution)"
            })
        end

        it "should load role options" do
            role = Role.first

            filter_options = Report.get_filter_options('en')

            role_entry = filter_options.detect { |entry| entry[:filter_type] == 'RoleFilter'}
            expect(role_entry).not_to be_nil
            expect(role_entry[:options]).to include({
                value: role.name,
                text: role.name
            })
        end

        describe "group options" do

            it "should load for admins" do
                group = AccessGroup.first

                filter_options = Report.get_filter_options('en')

                group_entry = filter_options.detect { |entry| entry[:filter_type] == 'GroupFilter'}
                expect(group_entry).not_to be_nil
                expect(group_entry[:options]).to include({
                    value: group.name,
                    text: group.name
                })
                expect(group_entry[:options]).to include({
                    value: "__NULL__",
                    text: "(No Groups)"
                })
            end

            it "should load correct groups for institutional users" do
                institution = Institution.first
                first_user = User.first
                second_user = User.second

                first_user.groups = [AccessGroup.first, AccessGroup.second]
                first_user.institutions = [institution]
                first_user.save!

                second_user.groups = [AccessGroup.second, AccessGroup.third]
                second_user.institutions = [institution]
                second_user.save!

                filter_options = Report.internal_get_filter_options_for_institution(institution, 'en')

                group_entry = filter_options.detect { |entry| entry[:filter_type] == 'GroupFilter'}
                expect(group_entry).not_to be_nil
                [AccessGroup.first, AccessGroup.second, AccessGroup.third].each do |group|
                    expect(group_entry[:options]).to include({
                        value: group.name,
                        text: group.name
                    })
                end
                expect(group_entry[:options]).to include({
                    value: nil,
                    text: "(No Groups)"
                })
            end

        end

    end

end