require 'spec_helper'

describe ClientConfig do

    before(:each) do
        @MyClientConfig = Class.new(ClientConfig)
    end

    # See message in client_config.rb about why all this is commented out.
    # describe 'dsl' do

    #     describe 'versions_above(n).support(...)' do

    #         it 'should support a feature_id' do
    #             @MyClientConfig.versions_above(42).support(:some_feature)
    #             expect(@MyClientConfig.new(42).supports?(:some_feature)).to be(false)
    #             expect(@MyClientConfig.new(43).supports?(:some_feature)).to be(true)
    #         end

    #         it 'should support a class' do
    #             @MyClientConfig.versions_above(42).support(String)
    #             expect(@MyClientConfig.new(42).supports?(String)).to be(false)
    #             expect(@MyClientConfig.new(43).supports?(String)).to be(true)
    #         end

    #     end

    # end

    # describe 'supports?' do

    #     # happy-path behavior is tested in the dsl section above

    #     it 'should raise UnknownFeatureException of feature is not registered' do
    #         expect(Proc.new {
    #             @MyClientConfig.new(42).supports?(:some_unknown_feature)
    #         }).to raise_error(ClientConfig::UnknownFeatureException)
    #     end

    # end

    # describe 'componentized support' do

    #     it 'should support a bunch of old components with all versions' do
    #         client_config = @MyClientConfig.new(1)
    #         expect(client_config.supports?(Lesson::Content::FrameList::Frame::Componentized::Component::Answer)).to be(true)
    #         expect(client_config.supports?(Lesson::Content::FrameList::Frame::Componentized::Component::Text)).to be(true)
    #     end

    # end

    


end