# == Schema Information
#
# Table name: learner_projects
#
#  id                     :uuid             not null, primary key
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  title                  :text             not null
#  requirement_identifier :text             not null
#  tag                    :text
#  internal_notes         :text
#  project_documents      :json             not null, is an Array
#  project_type           :text             default("standard"), not null
#

require 'spec_helper'

describe LearnerProject do

    fixtures :cohorts

    describe "create_from_hash!" do

        it "should work" do
            expect_any_instance_of(LearnerProject).to receive(:merge_hash).and_call_original
            instance = LearnerProject.create_from_hash!(valid_attrs)
            valid_attrs.keys.each do |attr|
                expect(instance[attr]).to eq(valid_attrs[attr])
            end
        end

    end

    describe "update_from_hash" do

        it "should work" do
            instance = LearnerProject.first
            expect_any_instance_of(LearnerProject).to receive(:merge_hash).and_call_original
            LearnerProject.update_from_hash!({
                "id" => instance.id,
                "title" => "changed",
                "updated_at" => instance.updated_at.to_timestamp
            })
            expect(instance.reload.title).to eq("changed")
        end
    end

    describe "merge_hash" do

        it "should set stuff" do
            instance = LearnerProject.first
            now = Time.now
            instance.merge_hash({
                title: 'changed',
                internal_notes: 'changed',
                project_documents: [{'a' => 'project'}],
                tag: 'changed',
                requirement_identifier: 'changed identifier'
            }.with_indifferent_access)
            expect(instance.title).to eq('changed')
            expect(instance.internal_notes).to eq('changed')
            expect(instance.tag).to eq('changed')
            expect(instance.project_documents).to eq([{'a' => 'project'}])
            expect(instance.requirement_identifier).to eq('changed identifier')
        end
    end

    describe "destroy" do
        attr_reader :cohort, :project
        before(:each) do
            CohortPromotion.delete_all # prevent validation errors on unpublish
            @cohort = Cohort.where("learner_project_ids[1] is not null").first
            @project = LearnerProject.find(cohort.all_learner_project_ids).first

            # make sure that no other cohort is causing the error
            Cohort.where.not(id: cohort.id).each do |other_cohort|
                other_cohort.unpublish!
                other_cohort.destroy
            end
        end

        it "should raise if working version of cohort is using project" do
            cohort.unpublish!
            expect {
                project.destroy
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Learner project cannot be destroyed because it is used in #{cohort.name.inspect}")
        end

        it "should raise if published version of cohort is using project" do
            cohort.learner_project_ids = []
            cohort.periods = []
            cohort.save!

            # sanity checks
            expect(cohort.all_learner_project_ids).not_to include(project.id)
            expect(cohort.published_version.all_learner_project_ids).to include(project.id)

            expect {
                project.destroy
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Learner project cannot be destroyed because it is used in #{cohort.name.inspect}")
        end

        it "should raise if curriculum_template is using project" do
            # remove the cohort that is connected to the project so we know it is the
            # curriculum_template causing the issue
            cohort.unpublish!
            cohort.destroy

            curriculum_template = CurriculumTemplate.create!({
                name: "test template",
                program_type: 'mba',
                learner_project_ids: [project.id]
            })
            expect {
                project.destroy
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Learner project cannot be destroyed because it is used in #{curriculum_template.name.inspect}")
        end

        it "should not raise if unused" do
            project = LearnerProject.create!(title: 'tadasd a ', requirement_identifier: 'requirement_identifier')
            expect {
                project.destroy
            }.not_to raise_error
        end

    end

    describe "as_json" do

        it "should include cohort info" do
            cohort = cohorts(:published_mba_cohort)
            cohorts = Cohort.all
            curriculum_templates = CurriculumTemplate.all
            project = LearnerProject.find(cohort.all_learner_project_ids).first
            expect(project).to receive(:get_related_cohort_json).with(cohorts, curriculum_templates).and_return(:related_cohort_json)
            json = project.as_json(cohorts: cohorts, curriculum_templates: curriculum_templates, fields: ['ADMIN_FIELDS'])
            expect(json['related_cohorts']).to eq(:related_cohort_json)
        end

        it "should not include admin info if ADMIN_FIELDS is not present" do
            project = LearnerProject.first
            project.update_columns(internal_notes: 'super secret internal notes')
            expect(project).not_to receive(:get_related_cohort_json)
            json = project.as_json
            expect(json['internal_notes']).to be_nil
        end

    end

    describe "get_related_cohort_json" do

        it "should include working cohorts" do
            cohort = cohorts(:published_mba_cohort)
            project = LearnerProject.find(cohort.all_learner_project_ids).first
            CohortPromotion.delete_all # prevent validation errors on unpublish
            cohort.unpublish!

            expect(project.get_related_cohort_json(Cohort.all, [])).to include({
                id: cohort.id,
                name: cohort.name,
                type: 'Cohort'
            }.as_json)
        end

        it "should include published cohorts" do
            cohort = cohorts(:published_mba_cohort)
            project = LearnerProject.find(cohort.all_learner_project_ids).first
            cohort.learner_project_ids = []
            cohort.periods = []
            cohort.save!

            # sanity checks
            expect(cohort.all_learner_project_ids).not_to include(project.id)
            expect(cohort.published_version.all_learner_project_ids).to include(project.id)

            expect(project.get_related_cohort_json(Cohort.all, [])).to include({
                id: cohort.id,
                name: cohort.name,
                type: 'Cohort'
            }.as_json)
        end

        it "should include curriculum templates" do
            project = LearnerProject.first
            curriculum_template = CurriculumTemplate.create!({
                name: "test template",
                program_type: 'mba',
                learner_project_ids: [project.id]
            })

            expect(project.get_related_cohort_json([], CurriculumTemplate.all)).to include({
                id: curriculum_template.id,
                name: curriculum_template.name,
                type: 'CurriculumTemplate'
            }.as_json)
        end

    end

    describe "project_type" do

        it "should validated project_type" do
            learner_project = LearnerProject.new(
                title: 'a wicked cool project',
                requirement_identifier: 'rid'
            )

            LearnerProject.valid_project_types.each do |project_type|
                learner_project.project_type = project_type
                expect(learner_project.valid?).to be(true)
            end

            learner_project.project_type = 'some_project_type'
            expect(learner_project.valid?).to be(false)
        end

    end

    describe "scoring_weight" do

        it "should return 4 for capstone project_type" do
            project = LearnerProject.new(project_type: LearnerProject::CAPSTONE_PROJECT_TYPE)
            expect(project.scoring_weight('emba')).to eq(4)
        end

        describe "standard project_type" do

            it "should return 2 for emba program_type" do
                project = LearnerProject.new(project_type: LearnerProject::STANDARD_PROJECT_TYPE)
                expect(project.scoring_weight('emba')).to eq(2)
            end

            it "should return 4 for mba program_type" do
                project = LearnerProject.new(project_type: LearnerProject::STANDARD_PROJECT_TYPE)
                expect(project.scoring_weight('mba')).to eq(4)
            end

        end

        it "should return 1 for presentation project_type" do
            project = LearnerProject.new(project_type: LearnerProject::PRESENTATION_PROJECT_TYPE)
            expect(project.scoring_weight('emba')).to eq(1)
        end

        it "should raise if not valid project_type" do
            project_type = 'not_a_valid_project_type'
            project = LearnerProject.new(project_type: project_type)
            expect {
                project.scoring_weight('emba')
            }.to raise_error("Trying to determine scoring weights for unsupported project_type #{project_type}")
        end

        it "should raise if not emba or mba program_type" do
            program_type = 'not_emba_or_mba'
            project = LearnerProject.new(project_type: LearnerProject::STANDARD_PROJECT_TYPE)
            expect {
                project.scoring_weight(program_type)
            }.to raise_error("Trying to determine scoring weights for unsupported program #{program_type}")
        end

    end

    describe "passing_score" do

        describe "standard projects" do

            attr_accessor :project

            before(:each) do
                @project = LearnerProject.new(project_type: LearnerProject::STANDARD_PROJECT_TYPE)
            end

            it "should return 1 for legacy emba cohort" do
                cohort = cohorts(:published_emba_cohort)
                expect(cohort).to receive(:start_date).and_return(Time.parse('2019/07/01'))
                expect(@project.passing_score(cohort)).to eq(1)
            end

            it "should return 1 for legacy mba cohort" do
                cohort = cohorts(:published_mba_cohort)
                expect(cohort).to receive(:start_date).and_return(Time.parse('2019/07/29'))
                expect(@project.passing_score(cohort)).to eq(1)
            end

            it "should return 2 for emba current" do
                cohort = cohorts(:published_emba_cohort)
                expect(cohort).to receive(:start_date).and_return(Time.parse('2019/07/02'))
                expect(@project.passing_score(cohort)).to eq(2)
            end

            it "should return 2 for mba current" do
                cohort = cohorts(:published_mba_cohort)
                expect(cohort).to receive(:start_date).and_return(Time.parse('2019/07/30'))
                expect(@project.passing_score(cohort)).to eq(2)
            end

        end

        it "should return 3 for capstone project_type" do
            project = LearnerProject.new(project_type: LearnerProject::CAPSTONE_PROJECT_TYPE)
            expect(project.passing_score(Cohort.first)).to eq(3)
        end

        it "should return 2 for presentation project_type" do
            project = LearnerProject.new(project_type: LearnerProject::PRESENTATION_PROJECT_TYPE)
            expect(project.passing_score(Cohort.first)).to eq(2)
        end

    end

    def valid_attrs(attrs = {})
        {
            title: 'some project',
            internal_notes: 'some notes',
            requirement_identifier: 'requirement_identifier',
            project_type: 'presentation'
        }.with_indifferent_access.merge(attrs)
    end

end
