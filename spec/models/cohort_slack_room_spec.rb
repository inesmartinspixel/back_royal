# == Schema Information
#
# Table name: cohort_slack_rooms
#
#  id                    :uuid             not null, primary key
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  cohort_id             :uuid             not null
#  title                 :text             not null
#  url                   :text             not null
#  admin_token           :text
#  bot_user_access_token :text
#
require 'spec_helper'

describe CohortSlackRoom do

    fixtures(:lesson_streams, :cohorts, :playlists, :users)

    describe "verify_setup_slack_channels_job" do

        it "should destroy_all all pending Cohort::SetupSlackChannelsJobs for the slack room and enqueue a new one" do
            slack_room = CohortSlackRoom.first
            Delayed::Job.delete_all
            Cohort::SetupSlackChannelsJob.perform_later(slack_room.id)
            query = Delayed::Job.where(queue: Cohort::SetupSlackChannelsJob.queue_name, identifier: slack_room.id, locked_at: nil, failed_at: nil)
            expect(query.count).to eq(1)
            job = query.first

            slack_room.verify_setup_slack_channels_job

            query.reload
            new_job = query.first
            expect(query.count).to eq(1)
            expect(new_job.id).not_to eq(job.id)
        end

        it "should not enqueue new Cohort::SetupSlackChannelsJob if no bot_user_access_token" do
            slack_room = CohortSlackRoom.first
            slack_room.update_column(:bot_user_access_token, nil)
            Delayed::Job.delete_all
            slack_room.verify_setup_slack_channels_job
            expect(Delayed::Job.count).to eq(0)
        end

        it "should be called after_save if bot_user_access_token is present and changed" do
            slack_room = CohortSlackRoom.first
            expect(slack_room).to receive(:verify_setup_slack_channels_job)
            slack_room.update!(bot_user_access_token: 'some_access_token')
        end
    end
end
