# == Schema Information
#
# Table name: signable_documents
#
#  id                           :uuid             not null, primary key
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  document_type                :text             not null
#  user_id                      :uuid             not null
#  sign_now_document_id         :text
#  sign_now_signing_link        :text
#  sign_now_signing_link_expiry :datetime
#  signed_at                    :datetime
#  metadata                     :json             not null
#  file_file_name               :string
#  file_content_type            :string
#  file_file_size               :bigint
#  file_updated_at              :datetime
#  file_fingerprint             :string
#  persisted_path               :string
#

require 'spec_helper'

describe SignableDocument do

    describe "::create_with_sign_now" do

        it "should have specs" do
            user = User.first
            doc = SignNow::Document.new(
                id: SecureRandom.uuid
            )
            expiry = DateTime.parse('2019-03-12')
            expect(doc).to receive(:generate_signing_link).and_return(OpenStruct.new({
                url: "http://path/to_doc",
                expiry: expiry
            }))
            expect(SignNow::Document).to receive(:create).with(template_id: 'template_id').and_return(doc)

            expect_any_instance_of(SignNow::Document).to receive(:update_smart_fields).with({ some: 'values' })

            signable_document = SignableDocument.create_with_sign_now(
                template_id: 'template_id',
                smart_field_values: { some: 'values' },
                document_type: SignableDocument::TYPE_ENROLLMENT_AGREEMENT,
                user_id: user.id,
                metadata: {
                    'some' => 'metadata'
                }
            )

            expect(signable_document.document_type).to eq(SignableDocument::TYPE_ENROLLMENT_AGREEMENT)
            expect(signable_document.user_id).to eq(user.id)
            expect(signable_document.sign_now_document_id).to eq(doc.id)
            expect(signable_document.sign_now_signing_link).to eq("http://path/to_doc")
            expect(signable_document.sign_now_signing_link_expiry).to eq(expiry)
            expect(signable_document.metadata).to eq({
                'some' => 'metadata',
                'template_id' => 'template_id'
            })
        end


        it "should return the original if a duplicate is attempted" do
            user = User.first
            doc = SignNow::Document.new(
                id: SecureRandom.uuid
            )

            expiry = DateTime.parse('2019-03-12')
            expect(doc).to receive(:generate_signing_link).exactly(2).times.and_return(OpenStruct.new({
                url: "http://path/to_doc",
                expiry: expiry
            }))

            expect(SignNow::Document).to receive(:create).exactly(2).times.and_return(doc)
            expect_any_instance_of(SignNow::Document).to receive(:update_smart_fields).exactly(2).times

            SignableDocument.create_with_sign_now(
                template_id: 'template_id',
                smart_field_values: { some: 'values' },
                document_type: SignableDocument::TYPE_ENROLLMENT_AGREEMENT,
                user_id: user.id,
                metadata: {
                    cohort_id: '1234',
                    program_type: 'emba'
                }
            )

            signable_document = nil
            expect {
                signable_document = SignableDocument.create_with_sign_now(
                    template_id: 'different_template_id',
                    smart_field_values: { some: 'different_values' },
                    document_type: SignableDocument::TYPE_ENROLLMENT_AGREEMENT,
                    user_id: user.id,
                    metadata: {
                        cohort_id: '5678',
                        program_type: 'emba'
                    }
                )
            }.not_to raise_error

            expect(signable_document.document_type).to eq(SignableDocument::TYPE_ENROLLMENT_AGREEMENT)
            expect(signable_document.user_id).to eq(user.id)
            expect(signable_document.sign_now_document_id).to eq(doc.id)
            expect(signable_document.sign_now_signing_link).to eq("http://path/to_doc")
            expect(signable_document.sign_now_signing_link_expiry).to eq(expiry)
            expect(signable_document.metadata).to eq({
                'cohort_id' => '1234',
                'program_type' => 'emba',
                'template_id' => 'template_id'
            })
        end

    end

    describe "update_signed_at" do

        it "should be set only when first adding file" do
            doc = SignableDocument.create!(valid_attrs)
            expect(doc.signed_at).to be_nil
            doc.update(file_fingerprint: 'file_fingerprint')
            expect(doc.signed_at).not_to be_nil

            expect {
                doc.update(file_fingerprint: 'changed_file_fingerprint')
            }.not_to change {
                doc.signed_at
            }
        end
    end

    describe "signing_link" do
        it "should support a special case redirect for new attribute documents" do
            doc = SignableDocument.create!(valid_attrs.merge({
                sign_now_signing_link: 'http://path/to/link'
            }))
            expect(doc.signing_link).to eq('https://quantic.edu/ea/sign?id=cohort_application_id')
        end

        it "should support direct provider URL for legacy attributes" do
            doc = SignableDocument.create!(valid_legacy_attrs.merge({
                sign_now_signing_link: 'http://path/to/link'
            }))
            expect(doc.signing_link).to eq('http://path/to/link')
        end
    end

    describe "log_info" do

        it "should populate the link value with the special case enrollment agreement redirect" do
            doc = SignableDocument.create!(valid_attrs.merge({
                sign_now_signing_link: 'http://path/to/link'
            }))

            expect(doc.log_info).to eq({
                document_type: 'enrollment_agreement',
                signable_document_id: doc.id,
                link: doc.signing_link,
                program_type: 'mba',
                cohort_id: 'cohort_id',
                template_id: 'template_id',
                cohort_application_id: 'cohort_application_id',
                follow_up_attempt: false
            })
        end

        it "should populate the link value with the SignNow URL if legacy attrs" do
            doc = SignableDocument.create!(valid_legacy_attrs.merge({
                sign_now_signing_link: 'http://path/to/link'
            }))

            expect(doc.log_info).to eq({
                document_type: 'enrollment_agreement',
                signable_document_id: doc.id,
                link: doc.sign_now_signing_link,
                program_type: 'mba',
                cohort_id: 'cohort_id',
                template_id: 'template_id'
            })
        end
    end

    describe "log_completed_event" do

        it "should be tested" do
            doc = SignableDocument.create!(valid_attrs.merge({
                sign_now_signing_link: 'http://path/to/link'
            }))
            event = Event.new
            expect(Event).to receive(:create_server_event!).with(
                anything,
                doc.user_id,
                'signable_document:complete',
                doc.log_info
            ).and_return(event)

            expect(event).to receive(:log_to_external_systems)


            doc.update(signed_at: Time.now)

        end
    end

    describe "log_signing_link_generated_event_if_necessary" do

        it "should have specs" do
            doc = SignableDocument.create!(valid_attrs)

            event = Event.new
            expect(Event).to receive(:create_server_event!).with(
                anything,
                doc.user_id,
                'signable_document:sign_now_link_generated',
                doc.log_info
            ).and_return(event)

            expect(event).to receive(:log_to_external_systems)

            doc.update(sign_now_signing_link: 'http://link/to/doc')

        end
    end

    describe "check_and_queue_download_if_necessary" do

        it "should not queue a download if it already has file info" do
            doc = SignableDocument.create!(valid_attrs.merge({file_fingerprint: 'fingerprint'}))
            expect(ProcessCompletedSignNowDocumentJob).not_to receive(:ensure_job)
            doc.check_and_queue_download_if_necessary
        end

        it "should not queue a download if all required fields haven't been completed" do
            doc = SignableDocument.create!(valid_attrs.merge({file_fingerprint: nil}))
            expect_any_instance_of(SignNow::Document).to receive(:get_attributes) {
                {
                    'fields' => [
                        {
                            'json_attributes' => {
                                'required' => true
                            },
                            'element_id' => 'not-nil-1'
                        },{
                            'json_attributes' => {
                                'required' => true
                            },
                            'element_id' => 'not-nil-2'
                        },{
                            'json_attributes' => {
                                'required' => true
                            },
                            'element_id' => nil
                        }
                    ]
                }
            }
            expect(ProcessCompletedSignNowDocumentJob).not_to receive(:ensure_job)
            doc.check_and_queue_download_if_necessary
        end

        it "should not queue a download if all required fields haven't been completed" do
            doc = SignableDocument.create!(valid_attrs.merge({file_fingerprint: nil}))
            expect_any_instance_of(SignNow::Document).to receive(:get_attributes) {
                {
                    'fields' => [
                        {
                            'json_attributes' => {
                                'required' => true
                            },
                            'element_id' => 'not-nil-1'
                        }
                    ],
                    'field_invites' => [
                        {
                            'status' => 'pending'
                        }
                    ]
                }
            }
            expect(ProcessCompletedSignNowDocumentJob).not_to receive(:ensure_job)
            doc.check_and_queue_download_if_necessary
        end

        it "should ensure a download if all required fields are populated and invites fulfilled" do
            doc = SignableDocument.create!(valid_attrs.merge({file_fingerprint: nil}))
            expect_any_instance_of(SignNow::Document).to receive(:get_attributes) {
                {
                    'fields' => [
                        {
                            'json_attributes' => {
                                'required' => true
                            },
                            'element_id' => 'not-nil-1'
                        }
                    ],
                    'field_invites' => [
                        {
                            'status' => 'fulfilled'
                        }
                    ]
                }
            }
            expect(ProcessCompletedSignNowDocumentJob).to receive(:ensure_job)
            doc.check_and_queue_download_if_necessary
        end

    end

    def valid_attrs
        {
            document_type: SignableDocument::TYPE_ENROLLMENT_AGREEMENT,
            user_id: User.first.id,
            metadata: {
                program_type: 'mba',
                cohort_id: 'cohort_id',
                template_id: 'template_id',
                cohort_application_id: 'cohort_application_id',
                follow_up_attempt: false
            }
        }
    end

    def valid_legacy_attrs
        {
            document_type: SignableDocument::TYPE_ENROLLMENT_AGREEMENT,
            user_id: User.first.id,
            metadata: {
                program_type: 'mba',
                cohort_id: 'cohort_id',
                template_id: 'template_id'
            }
        }
    end

end
