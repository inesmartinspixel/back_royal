# == Schema Information
#
# Table name: subscriptions
#
#  id                     :uuid             not null, primary key
#  created_at             :datetime
#  updated_at             :datetime
#  stripe_plan_id         :text             not null
#  stripe_subscription_id :text             not null
#  past_due               :boolean
#  stripe_product_id      :text             not null
#  cancel_at_period_end   :boolean          default(FALSE), not null
#  current_period_end     :datetime         not null
#

require 'spec_helper'

describe Subscription do
    attr_reader :user

    include StripeHelper

    fixtures(:users)

    before(:each) do
        @user = users(:user_with_subscriptions_enabled)
    end

    describe "create_or_reconcile_from_stripe_subscription!" do

        it "should do nothing if the subscription is canceled" do
            user = users(:user_with_subscriptions_enabled)
            create_customer_with_default_source(user)
            stripe_subscription = user.ensure_stripe_customer.subscriptions.create(plan: default_plan.id)
            stripe_subscription.delete
            subscription = Subscription.create_or_reconcile_from_stripe_subscription!(user, stripe_subscription)
            expect(subscription).to be_nil
        end

        it "should do nothing if the subscription is incomplete" do
            user = users(:user_with_subscriptions_enabled)
            create_customer_with_default_source(user)
            stripe_subscription = user.ensure_stripe_customer.subscriptions.create(plan: default_plan.id)
            expect(stripe_subscription).to receive(:status).at_least(1).times { 'incomplete' }
            subscription = Subscription.create_or_reconcile_from_stripe_subscription!(user, stripe_subscription)
            expect(subscription).to be_nil
        end


        it "should create a new subscription for a user" do
            user = users(:user_with_subscriptions_enabled)
            create_customer_with_default_source(user)
            stripe_subscription = user.ensure_stripe_customer.subscriptions.create(plan: default_plan.id)
            stripe_subscription.metadata = {test: 'value'}
            stripe_subscription.save
            subscription = Subscription.create_or_reconcile_from_stripe_subscription!(user, stripe_subscription)
            expect(subscription).not_to be_nil
            expect(subscription.stripe_subscription.id).to eq(stripe_subscription.id)
            expect(subscription.stripe_subscription.metadata['test']).to eq('value')

        end

        it "should create a new subscription for a hiring team" do
            hiring_team = HiringTeam.joins(:owner).first
            expect(hiring_team).not_to be_nil
            create_customer_with_default_source(hiring_team)
            stripe_subscription = hiring_team.ensure_stripe_customer.subscriptions.create(plan: default_plan.id)
            stripe_subscription.metadata = {test: 'value'}
            stripe_subscription.save
            subscription = Subscription.create_or_reconcile_from_stripe_subscription!(hiring_team, stripe_subscription)
            expect(subscription).not_to be_nil
            expect(subscription.stripe_subscription.id).to eq(stripe_subscription.id)
            expect(subscription.stripe_subscription.metadata['test']).to eq('value')

        end

        it "should update an existing subscription" do
            create_customer_with_default_source(user)
            existing_subscription = create_subscription(user)
            stripe_subscription = existing_subscription.stripe_subscription

            # make a change to the stripe_subscription and make sure it gets added to the
            # subscription
            stripe_subscription.metadata = {test: 'value'}
            stripe_subscription.save

            subscription = Subscription.create_or_reconcile_from_stripe_subscription!(user, stripe_subscription)
            expect(subscription).not_to be_nil
            expect(subscription).to eq(existing_subscription)
            expect(subscription.stripe_subscription.id).to eq(stripe_subscription.id)
            expect(subscription.stripe_subscription.metadata['test']).to eq('value')
        end

    end

    describe "create_for_owner" do

        it "should raise if there is no stripe_customer" do
            expect {
                Subscription.create_for_owner(owner: user, stripe_plan_id: default_plan.id)
            }.to raise_error(RuntimeError, "No stripe customer")
        end

        it "should raise if the created subscription is incomplete" do
            create_customer_with_default_source(user)
            expect(user.stripe_customer.subscriptions).to receive(:create) { OpenStruct.new(status: 'incomplete') }
            expect {
                Subscription.create_for_owner(owner: user, stripe_plan_id: default_plan.id)
            }.to raise_error(Subscription::IncompleteSubscription, "There was an issue charging the provided card. Please contact your card provider or try another payment source.")
        end

        it "should process metadata" do
            create_customer_with_default_source(user)
            subscription = Subscription.create_for_owner(owner: user, stripe_plan_id: default_plan.id, metadata: {some: 'metadata'})
            expect(subscription.stripe_subscription.metadata['some']).to eq('metadata')
        end

        it "should call raise_if_duplicate_subscription!" do
            create_customer_with_default_source(user)
            expect(user).to receive(:raise_if_duplicate_subscription!).with(:metadata).and_raise(RuntimeError.new)
            expect {
                Subscription.create_for_owner(owner: user, stripe_plan_id: default_plan.id, metadata: :metadata)
            }.to raise_error(RuntimeError)

        end

        it "should support passing in a trial_end_ts" do
            create_customer_with_default_source(user)
            trial_end_ts = 1.day.from_now.to_timestamp
            subscription = Subscription.create_for_owner(owner: user, stripe_plan_id: default_plan.id, trial_end_ts: trial_end_ts)
            expect(subscription.stripe_subscription.trial_end).to eq(trial_end_ts)
        end

        it "should respect the trial period on a plan, using trial_from_plan" do
            create_customer_with_default_source(user)
            subscription = Subscription.create_for_owner(owner: user, stripe_plan_id: plan_with_trial_period.id)
            expect(Time.at(subscription.stripe_subscription.trial_end)).to be_within(10.minutes).of(14.days.from_now)
        end
    end

    describe "raise_if_duplicate_primary_subscription!" do
        it "should raise if there is already an initiated subscription" do
            create_customer_with_default_source(user)
            expect(user.stripe_customer.subscriptions).to receive(:data) { [OpenStruct.new(status: 'active')] }
            expect {
                Subscription.create_for_owner(owner: user, stripe_plan_id: default_plan.id)
            }.to raise_error(Subscription::CannotCreateDuplicateSubscription, "There is already a subscription in stripe for User=#{user.id}")
        end

        it "should raise if there is already an internal subscription" do
            create_customer_with_default_source(user)
            expect(user).to receive(:subscriptions) { [Subscription.new(), Subscription.new()] }
            expect {
                Subscription.create_for_owner(owner: user, stripe_plan_id: default_plan.id)
            }.to raise_error(Subscription::CannotCreateDuplicateSubscription, "There is already a subscription in the database for User=#{user.id}")
        end
    end

    describe "reconcile_with_stripe" do

        before(:each) do
            create_customer_with_default_source(user)
        end

        it "should update past_due" do

            subscription = create_subscription(user)
            expect(subscription.past_due).to be(false)

            allow(subscription).to receive(:status).at_least(1).times.and_return("past_due")
            subscription.reconcile_with_stripe
            expect(subscription.past_due).to be(true)

            allow(subscription).to receive(:status).at_least(1).times.and_return("active")
            subscription.reconcile_with_stripe
            expect(subscription.past_due).to be(false)
        end

        it "should update cancel_at_period_end" do

            subscription = create_subscription(user)
            expect(subscription.cancel_at_period_end).to be(false)

            cancel_at_period_end  = nil
            allow(subscription.stripe_subscription).to receive(:cancel_at_period_end) do
                cancel_at_period_end
            end

            cancel_at_period_end = true
            subscription.reconcile_with_stripe
            expect(subscription.cancel_at_period_end).to be(true)

            cancel_at_period_end = false
            subscription.reconcile_with_stripe
            expect(subscription.cancel_at_period_end).to be(false)
        end

        it "should update current_period_end" do

            subscription = create_subscription(user)
            expect(subscription.cancel_at_period_end).to be(false)

            cancel_at_period_end  = nil
            time = Time.parse('2017/01/01')
            allow(subscription.stripe_subscription).to receive(:current_period_end).and_return(time.to_timestamp)

            subscription.reconcile_with_stripe
            expect(subscription.current_period_end).to eq(time)
        end

    end

    describe "after_create" do

        before(:each) do
            create_customer_with_default_source(user)
        end

        it "should warn if multiple subscriptions for a user" do
            expect(Raven).to receive(:capture_exception).with(RuntimeError.new("Owner User:#{user.id} has multiple primary subscriptions"))
            2.times do
                stripe_subscription = user.ensure_stripe_customer.subscriptions.create(plan: default_plan.id)
                Subscription.create_or_reconcile_from_stripe_subscription!(user, stripe_subscription)
            end
        end

    end

    describe "open_invoices" do
        before(:each) do
            create_customer_with_default_source(user)
            @subscription = create_subscription(user)
        end

        it "should find them" do
            # create multiple invoices to check that we filter the right ones
            open_invoice = Stripe::Invoice.create(customer: user.id, subscription: @subscription.stripe_subscription_id)
            open_invoice2 = Stripe::Invoice.create(customer: user.id, subscription: @subscription.stripe_subscription_id)
            closed_invoice = Stripe::Invoice.create(customer: user.id, subscription: @subscription.stripe_subscription_id)
            closed_invoice.auto_advance = false
            closed_invoice.save
            Stripe::Invoice.create(customer: user.id, subscription: "not this one")

            @subscription.send(:reset_invoices)
            expect(@subscription.send(:open_invoices).map(&:id)).to match_array([open_invoice.id, open_invoice2.id])
        end

    end

    describe "unpaid_invoices" do
        before(:each) do
            create_customer_with_default_source(user)
            @subscription = create_subscription(user)
        end

        it "should find them" do
            # create multiple invoices to check that we filter the right ones
            unpaid_invoice = Stripe::Invoice.create(customer: user.id, subscription: @subscription.stripe_subscription_id, paid: false, attempted: true)
            unpaid_invoice2 = Stripe::Invoice.create(customer: user.id, subscription: @subscription.stripe_subscription_id, paid: false, attempted: true)
            Stripe::Invoice.create(customer: user.id, subscription: @subscription.stripe_subscription_id, paid: false, attempted: false)
            Stripe::Invoice.create(customer: user.id, subscription: @subscription.stripe_subscription_id, paid: true, attempted: true)

            @subscription.send(:reset_invoices)
            expect(@subscription.send(:unpaid_invoices).map(&:id)).to match_array([unpaid_invoice.id, unpaid_invoice2.id])
        end

    end

    describe "charge_unpaid_invoices" do

        before(:each) do
            create_customer_with_default_source(user)
        end

        it "should pay the invoices, reconcile and save" do
            subscription = create_subscription(user)
            invoice = Stripe::Invoice.create(customer: user.id, subscription: subscription.stripe_subscription_id)
            expect(subscription).to receive(:unpaid_invoices).and_return([invoice])

            expect(invoice).to receive(:pay)
            expect(subscription).to receive(:reset_stripe_subscription)
            expect(subscription).to receive(:reconcile_with_stripe)
            expect(subscription).to receive(:save!)
            subscription.charge_unpaid_invoices
        end

    end

    describe "next_period_amount" do

        # rather than mocking this, it would be nice to actually add
        # a coupon to the subscription.  But stripe-ruby-mock
        # does not support per-subscription discounts.  See
        # https://github.com/rebelidealist/stripe-ruby-mock/issues/126
        class MockDiscount

            attr_accessor :amount_off, :percent_off

            def initialize(amount_off, percent_off)
                @amount_off = amount_off
                @percent_off = percent_off
            end

            def coupon
                Struct.new(:amount_off, :percent_off).new(@amount_off, @percent_off)
            end

        end

        before(:each) do
            create_customer_with_default_source(user)
        end


        it "should work with no discount" do
            subscription = create_subscription(user)
            expect(subscription.next_period_amount).to eq(subscription.plan.amount)
        end

        it "should work with a percent discount" do
            subscription = create_subscription(user)
            allow(subscription).to receive(:discount).and_return(MockDiscount.new(nil, 50))
            expect(subscription.next_period_amount).to eq(0.5 * subscription.plan.amount)
        end

        it "should work with an amount discount" do
            subscription = create_subscription(user)
            allow(subscription).to receive(:discount).and_return(MockDiscount.new(50, nil))
            expect(subscription.next_period_amount).to eq(subscription.plan.amount - 50)
        end

    end

    describe "ensure_stripe_customer_current" do

        before(:each) do
            allow_any_instance_of(User).to receive(:force_ensure_stripe_customer_current_in_test_mode).and_return(true)
            allow_any_instance_of(HiringTeam).to receive(:force_ensure_stripe_customer_current_in_test_mode).and_return(true)
        end

        it "should ensure that the email stays updated if it is changed when there is no subscription for a user" do
            user.force_ensure_stripe_customer_current_in_test_mode = true

            original_email = user.email
            create_customer_with_default_source(user)

            # if the email changes when there is no subscription, we don't
            # update in stripe (for performance reasons.  We don't want to have
            # to ask stripe if there is a customer any time any user changes emails)
            expect(user.subscriptions).to be_empty
            user.email = 'changed@email.com'
            user.save!
            expect(User.find(user.id).stripe_customer.email).to eq(original_email)

            # when the subscription is created, the customer should be updated
            create_subscription(user)
            expect(User.find(user.id).stripe_customer.email).to eq(user.email)

            # if it changes when there is a subscription, it should update in stripe
            user.email = 'changed-again@email.com'
            user.save!
            expect(User.find(user.id).stripe_customer.email).to eq(user.email)
        end

        it "should ensure that the email stays updated if it is changed when there is no subscription for a hiring team" do
            hiring_team = HiringTeam.joins(:owner)
                            .left_outer_joins(:subscriptions)
                            .where("subscriptions.id is null")
                            .first

            create_customer_with_default_source(hiring_team)
            user = hiring_team.owner
            original_email = user.email

            # if the email changes when there is no subscription, we don't
            # update in stripe (for performance reasons.  We don't want to have
            # to ask stripe if there is a customer any time any user changes emails)
            expect(hiring_team.subscriptions).to be_empty
            user.email = 'changed@email.com'
            user.save!
            expect(HiringTeam.find(hiring_team.id).stripe_customer.email).to eq(original_email)

            # when the subscription is created, the customer should be updated
            create_subscription(hiring_team)
            expect(HiringTeam.find(hiring_team.id).stripe_customer.email).to eq(user.email)

            # if it changes when there is a subscription, it should update in stripe
            user.email = 'changed-again@email.com'
            user.save!
            expect(HiringTeam.find(hiring_team.id).stripe_customer.email).to eq(user.email)

            # if the owner changes, then the email in stripe should change
            new_owner = User.left_outer_joins(:hiring_team).where("hiring_teams.id is null").where.not(id: user.id).first
            hiring_team.hiring_managers << new_owner
            hiring_team.owner = new_owner
            hiring_team.save!
            expect(HiringTeam.find(hiring_team.id).stripe_customer.email).to eq(new_owner.email)

        end

    end

    describe "cancel_and_destroy" do
        it "should work" do
            subscription = create_subscription(user)
            stripe_subscription_id = subscription.stripe_subscription_id
            expect(subscription).to receive(:destroy)
            subscription.cancel_and_destroy(reason: 'why not?')

            reloaded_stripe_subscription = user.stripe_customer.subscriptions.retrieve({:id => stripe_subscription_id})
            expect(reloaded_stripe_subscription.metadata['cancellation_reason']).to eq('why not?')
        end

        it "should work when the provided block destroys the subscription" do

            subscription = create_subscription(user)
            stripe_subscription_id = subscription.stripe_subscription_id
            expect(subscription).not_to receive(:destroy)
            subscription.cancel_and_destroy(reason: 'why not?') do
                cloned = Subscription.find(subscription.id)
                cloned.subscription_user_join.delete
                cloned.delete
            end

            reloaded_stripe_subscription = user.stripe_customer.subscriptions.retrieve({:id => stripe_subscription_id})
            expect(reloaded_stripe_subscription.metadata['cancellation_reason']).to eq('why not?')
        end

        it "should prorate if indicated" do
            subscription = create_subscription(user)
            stripe_subscription_id = subscription.stripe_subscription_id

            expect(subscription.stripe_subscription).to receive(:delete).with(prorate: true)
            subscription.cancel_and_destroy(reason: 'no reason', prorate: true)
        end

        it "should not prorate if not indicated" do
            subscription = create_subscription(user)
            stripe_subscription_id = subscription.stripe_subscription_id

            expect(subscription.stripe_subscription).to receive(:delete).with(prorate: false)
            subscription.cancel_and_destroy(reason: 'no reason')
        end
    end

    describe "metered?" do

        attr_accessor :hiring_team

        before(:each) do
            @hiring_team = HiringTeam.left_outer_joins(:subscriptions)
                                .where("subscriptions.id is null")
                                .joins(:owner).first
        end

        it "should be true if the Stripe plan's usage_type is metered" do
            expect(metered_plan.usage_type).to eq("metered")
            subscription = create_subscription(hiring_team, metered_plan.id)
            expect(subscription.metered?).to be(true)
        end

        it "should be false if the Stripe plan's usage_type is not metered" do
            expect(default_plan.usage_type).not_to eq("metered")
            subscription = create_subscription(hiring_team, default_plan.id)
            expect(subscription.metered?).to be(false)
        end
    end

    describe "increment_usage?" do
        it "should delegate to the owner" do
            hiring_team = HiringTeam.left_outer_joins(:subscriptions)
                                .where("subscriptions.id is null")
                                .joins(:owner).first
            subscription = create_subscription(hiring_team, metered_plan.id)

            timestamp = Time.now.to_timestamp
            expect(hiring_team).to receive(:increment_usage).once.with(subscription, timestamp)
            subscription.increment_usage(timestamp)
        end
    end

    describe "delete_stripe_subscription" do

        attr_accessor :hiring_team

        before(:each) do
            @hiring_team = HiringTeam.left_outer_joins(:subscriptions)
                                .where("subscriptions.id is null")
                                .joins(:owner).first
        end

        it "should not attempt to invoice any accrued usage immediately if metered?" do
            expect(default_plan.usage_type).not_to eq("metered")
            subscription = create_subscription(hiring_team, default_plan.id)

            expect(subscription.stripe_subscription).to receive(:delete)
            expect(Stripe::InvoiceItem).not_to receive(:create)
            expect(Stripe::Invoice).not_to receive(:create)
            expect_any_instance_of(Stripe::Invoice).not_to receive(:pay)

            subscription.send(:delete_stripe_subscription)
        end

        it "should attempt to invoice any accrued usage immediately if metered?" do
            expect(metered_plan.usage_type).to eq("metered")
            subscription = create_subscription(hiring_team, metered_plan.id)

            # Ideally, we would be testing Usage flushing by actually creating records (like this),
            # but stripe-ruby-mock currently does not support UsageRecord API and will not reflect
            # any changes in the upcoming invoice.
            #
            # 10.times do
            #     subscription.increment_usage(Time.now.to_timestamp)
            # end
            #
            # Fortunately, we can still test general `upcoming_invoice` immediate invoicing, since
            # We'll have one from the subscription we just created (but haven't auto-paid yet).

            upcoming_invoice = subscription.send(:upcoming_invoice)
            expect(upcoming_invoice.amount_due).to be > 0

            expect(subscription.stripe_subscription).to receive(:delete)
            expect(Stripe::InvoiceItem).to receive(:create).with({ customer: hiring_team.id, amount: upcoming_invoice.amount_due, currency: "USD"}).and_call_original
            expect(Stripe::Invoice).to receive(:create).with({ customer: hiring_team.id }).and_call_original
            expect_any_instance_of(Stripe::Invoice).to receive(:pay)

            subscription.send(:delete_stripe_subscription)
        end

        it "should not raise if receiving a CardError during immediate-invoicing" do
            expect(metered_plan.usage_type).to eq("metered")
            subscription = create_subscription(hiring_team, metered_plan.id)

            upcoming_invoice = subscription.send(:upcoming_invoice)
            expect(upcoming_invoice.amount_due).to be > 0

            expect(subscription.stripe_subscription).to receive(:delete)
            expect(Stripe::InvoiceItem).to receive(:create).with({ customer: hiring_team.id, amount: upcoming_invoice.amount_due, currency: "USD"}).and_call_original
            expect(Stripe::Invoice).to receive(:create).with({ customer: hiring_team.id }).and_call_original
            expect_any_instance_of(Stripe::Invoice).to receive(:pay) do
                raise Stripe::CardError.new("insufficient_funds", 500)
            end

            expect(Raven).to receive(:capture_message).with("Accrued usage invoicing on subscription cancellation failed. Invoice will be auto-retried, but should be monitored. Subscription will be cancelled.")

            expect {
                subscription.send(:delete_stripe_subscription)
            }.not_to raise_error

        end

    end

    describe "destroy_if_canceled" do

        # https://trello.com/c/0bvtVNaT
        it "should still work if a subscription has been destroyed more than once" do

            id = SecureRandom.uuid
            ActiveRecord::Base.connection.execute(%Q~
                insert into subscriptions (id, stripe_plan_id, stripe_subscription_id, stripe_product_id, current_period_end)
                VALUES
                ('#{id}', 'stripe_plan_id', 'stripe_subscription_id', 'stripe_product_id', now());

                delete from subscriptions where id = '#{id}';

                insert into subscriptions (stripe_plan_id, stripe_subscription_id, stripe_product_id, current_period_end)
                VALUES
                ('stripe_plan_id', 'stripe_subscription_id', 'stripe_product_id', now());
            ~)

            allow_any_instance_of(Subscription).to receive(:trigger_before_destroy_on_owner)
            allow_any_instance_of(Subscription).to receive(:delete_stripe_subscription)
            allow_any_instance_of(Subscription).to receive(:trigger_after_commit_on_owner)

            expect {
                # Before, this would error because there were two separate records in the versions
                # table with destroy operations for this stripe_subscription_id.  If this spec
                # passes, then it's fixed
                Subscription.find_by_stripe_subscription_id('stripe_subscription_id').destroy
            }.not_to raise_error

        end
    end

    describe "trigger_handle_past_due_changes_on_owner" do
        attr_accessor :subscription

        before(:each) do
            create_customer_with_default_source(user)
            @subscription = create_subscription(user)
        end

        it "should be called if subscription is not fake and a change to past_due was saved" do
            expect(subscription).to receive(:fake_subscription?).and_return(false)
            expect(subscription).to receive(:saved_change_to_past_due?).and_return(true)
            expect(subscription).to receive(:trigger_handle_past_due_changes_on_owner)
            subscription.save!

            expect(subscription).to receive(:fake_subscription?).and_return(false)
            expect(subscription).to receive(:saved_change_to_past_due?).and_return(false)
            expect(subscription).not_to receive(:trigger_handle_past_due_changes_on_owner)
            subscription.save!

            expect(subscription).to receive(:fake_subscription?).and_return(true)
            expect(subscription).not_to receive(:saved_change_to_past_due?)
            expect(subscription).not_to receive(:trigger_handle_past_due_changes_on_owner)
            subscription.save!
        end

        it "should call handle_past_due_changes on owner" do
            expect(subscription.owner).to receive(:handle_past_due_changes)
            subscription.send(:trigger_handle_past_due_changes_on_owner)
        end
    end
end
