# == Schema Information
#
# Table name: published_streams
#
#  id             :uuid
#  locale_pack_id :uuid
#  title          :string
#  version_id     :uuid
#  chapters       :json
#  locale         :text
#  exam           :boolean
#

require 'spec_helper'

describe PublishedStream do

    fixtures(:users, :cohorts, :institutions, :lesson_streams)

    describe "for_user" do


        it "should return a stream from a group" do

            stream = Lesson::Stream.all_published.joins(:locale_pack => :access_groups).first
            access_group = stream.locale_pack.access_groups.first
            user = get_user_with_no_cohort_institutions_groups


            expect(PublishedStream.for_user(user, ['locale_pack_id']).where(locale_pack_id: stream.locale_pack_id)).to be_empty

            user.access_groups << access_group
            expect(PublishedStream.for_user(user, ['locale_pack_id']).where(locale_pack_id: stream.locale_pack_id)).not_to be_empty
        end

        it "should return a stream from a cohort" do

            cohort = cohorts(:published_mba_cohort)
            stream_locale_pack_id = cohort.get_required_stream_locale_pack_ids.first
            user = get_user_with_no_cohort_institutions_groups

            expect(PublishedStream.for_user(user, ['locale_pack_id']).where(locale_pack_id: stream_locale_pack_id)).to be_empty

            @relevant_cohort = cohort
            expect(PublishedStream.for_user(user, ['locale_pack_id']).where(locale_pack_id: stream_locale_pack_id)).not_to be_empty

        end

        it "should return a stream from an institution" do

            institution = institutions(:institution_with_streams)

            record = ActiveRecord::Base.connection.execute(%Q~
                select
                    institution_id
                    , stream_locale_pack_id
                from
                    institution_stream_locale_packs
                where
                    institution_id = '#{institution.id}'
                limit 1
            ~).to_a[0]
            stream_locale_pack_id = record['stream_locale_pack_id']

            user = get_user_with_no_cohort_institutions_groups

            expect(PublishedStream.for_user(user, ['locale_pack_id']).where(locale_pack_id: stream_locale_pack_id)).to be_empty

            user.institutions << institution
            expect(PublishedStream.for_user(user, ['locale_pack_id']).where(locale_pack_id: stream_locale_pack_id)).not_to be_empty
        end

        it "should return published versions" do
            user = cohorts(:published_mba_cohort).accepted_users.first
            stream = Lesson::Stream.find_by_locale_pack_id(user.relevant_cohort.get_required_stream_locale_pack_ids.first)

            # make sure that there is a version since the published one
            stream.save!

            version_id = PublishedStream.for_user(user, ['version_id']).where(locale_pack_id: stream.locale_pack_id).first['version_id']
            expect(version_id).to eq(stream.published_version.version_id)

            allow(stream).to receive(:raise_unless_all_playlists_including_stream_are_valid)
            stream.unpublish!
            RefreshMaterializedContentViews.refresh(true)
            expect(PublishedStream.for_user(user, ['version_id']).where(locale_pack_id: stream.locale_pack_id)).to be_empty
        end

        it "should return the expected locales" do
            es_stream = lesson_streams(:es_item)
            en_stream = lesson_streams(:en_item)
            locale_pack = es_stream.locale_pack

            # sanity checks
            expect(es_stream.locale_pack_id).to eq(en_stream.locale_pack_id)
            expect(es_stream.has_published_version?).to be(true)
            expect(en_stream.has_published_version?).to be(true)

            access_group = AccessGroup.first
            locale_pack.access_groups << access_group
            user = get_user_with_no_cohort_institutions_groups
            user.access_groups << access_group

            # English user gets English stream when both English and Spanish are available
            user.update(pref_locale: 'en')
            expect(PublishedStream.for_user(user, ['id']).where(locale_pack_id: locale_pack.id).first['id']).to eq(en_stream.id)

            # Spanish user gets Spanish stream
            user.update(pref_locale: 'es')
            expect(PublishedStream.for_user(user, ['id']).where(locale_pack_id: locale_pack.id).first['id']).to eq(es_stream.id)

            es_stream.unpublish!
            RefreshMaterializedContentViews.refresh(true)

            # When only English is available, English user still gets English stream
            user.update(pref_locale: 'en')
            expect(PublishedStream.for_user(user, ['id']).where(locale_pack_id: locale_pack.id).first['id']).to eq(en_stream.id)

            # Since Spanish is unavailable, Spanish user gets English stream too
            user.update(pref_locale: 'es')
            expect(PublishedStream.for_user(user, ['id']).where(locale_pack_id: locale_pack.id).first['id']).to eq(en_stream.id)
        end

        def get_user_with_no_cohort_institutions_groups
            user = User.left_outer_joins(:access_groups)
                        .left_outer_joins(:institutions)
                        .where(access_groups: {id: nil}, institutions: {id: nil})
                        .first

            @relevant_cohort = nil
            allow(user).to receive(:relevant_cohort) {
                @relevant_cohort
            }
            user
        end

    end

    describe "for_group" do
        it "should return streams for a group" do
            stream = Lesson::Stream.all_published.joins(:locale_pack => :access_groups).first
            access_group = stream.locale_pack.access_groups.first
            stream.locale_pack.access_groups = []

            expect(PublishedStream.for_group_name(access_group.name, ['locale_pack_id']).where(locale_pack_id: stream.locale_pack_id))
                .to be_empty

            stream.locale_pack.access_groups << access_group
            expect(PublishedStream.for_group_name(access_group.name, ['locale_pack_id']).where(locale_pack_id: stream.locale_pack_id))
                .not_to be_empty
        end
    end

    describe "for_cohort" do
        it "should return streams for a cohort" do
            cohort = cohorts(:published_mba_cohort)
            stream_locale_pack_id = cohort.get_required_stream_locale_pack_ids.first

            expect(PublishedStream.for_cohort(cohort['id'], ['locale_pack_id']).where(locale_pack_id: stream_locale_pack_id))
                .not_to be_empty

            cohort.periods = []
            cohort.playlist_collections = [{title: '', required_playlist_pack_ids: []}]
            cohort.publish!
            RefreshMaterializedContentViews.refresh_and_rebuild_derived_content_tables
            expect(PublishedStream.for_cohort(cohort['id'], ['locale_pack_id']).where(locale_pack_id: stream_locale_pack_id))
                .to be_empty


        end
    end

    describe "for_institution" do
        it "should return streams for an institution" do
            institution = institutions(:institution_with_streams)

            record = ActiveRecord::Base.connection.execute(%Q~
                select
                    institution_id
                    , stream_locale_pack_id
                from
                    institution_stream_locale_packs
                where
                    institution_id = '#{institution.id}'
                limit 1
            ~).to_a[0]
            stream_locale_pack_id = record['stream_locale_pack_id']

            expect(PublishedStream.for_institution(institution.id, ['locale_pack_id']).where(locale_pack_id: stream_locale_pack_id))
                .not_to be_empty

            institution.access_groups = []
            institution.playlist_pack_ids = []
            institution.save!
            RefreshMaterializedContentViews.refresh(true)
            expect(PublishedStream.for_institution(institution.id, ['locale_pack_id']).where(locale_pack_id: stream_locale_pack_id))
                .to be_empty
        end
    end
end
