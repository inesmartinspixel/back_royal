# == Schema Information
#
# Table name: mobile_devices
#
#  id           :uuid             not null, primary key
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :uuid             not null
#  device_token :text             not null
#  platform     :text             not null
#  last_used_at :datetime         not null
#

require 'spec_helper'

describe MobileDevice do

    before(:each) do
        @device_attrs = {
            user_id: SecureRandom.uuid,
            device_token: SecureRandom.uuid,
            platform: 'android',
            last_used_at: Time.now
        }
    end

    it "should work" do
        mobile_device = MobileDevice.new(@device_attrs)
        expect(mobile_device).to be_valid
    end

    it "should validate presence of user_id" do
        @device_attrs.merge!(user_id: nil)
        mobile_device = MobileDevice.new(@device_attrs)
        expect(mobile_device).not_to be_valid
    end

    it "should validate presence of device_token" do
        @device_attrs.merge!(device_token: nil)
        mobile_device = MobileDevice.new(@device_attrs)
        expect(mobile_device).not_to be_valid
    end

    it "should validate presence of platform" do
        @device_attrs.merge!(platform: nil)
        mobile_device = MobileDevice.new(@device_attrs)
        expect(mobile_device).not_to be_valid
    end

    it "should validate presence of last_used_at" do
        @device_attrs.merge!(last_used_at: nil)
        mobile_device = MobileDevice.new(@device_attrs)
        expect(mobile_device).not_to be_valid
    end

    it "it should capture exception for unexpected device platform" do
        expect(Raven).to receive(:capture_exception)
        @device_attrs.merge!(platform: 'Colby\'s Super Awesome Phone Company')
        mobile_device = MobileDevice.new(@device_attrs)
        mobile_device.save!
    end
end
