require 'spec_helper'

describe Cohort::ProgramTypeConfig do

    it "should be accessible from cohort" do
        cohort = Cohort.find_by_program_type('test')
        expect(cohort.program_type_config.class).to be(Cohort::ProgramTypeConfig::Test)
    end

    it "should be accessible from cohort_application" do
        cohort_application = Cohort.find_by_program_type('test').cohort_applications.first
        expect(cohort_application.program_type_config.class).to be(Cohort::ProgramTypeConfig::Test)
    end

    describe "::program_types" do
        it "should return a list of program_types" do
            expect(Cohort::ProgramTypeConfig.program_types).to include('test')
            expect(Cohort::ProgramTypeConfig.program_types.size).to eq(Cohort::ProgramTypeConfig::Base.subclasses.size)
        end
    end

    describe "::configs" do
        it "should return a list of instances" do
            expect(Cohort::ProgramTypeConfig.configs).to include(Cohort::ProgramTypeConfig::Test)
            expect(Cohort::ProgramTypeConfig.configs.size).to eq(Cohort::ProgramTypeConfig::Base.subclasses.size)
        end
    end

    describe "::signup_program_types" do
        it "should return program types with supports_signup?" do
            # sanity check
            expect(Cohort::ProgramTypeConfig['mba'].supports_signup?).to be(true)
            expect(Cohort::ProgramTypeConfig['test'].supports_signup?).to be(false)

            expect(Cohort::ProgramTypeConfig.signup_program_types).to include('mba')
            expect(Cohort::ProgramTypeConfig.signup_program_types).not_to include('test')
        end
    end

    describe "::degree_program_types" do
        it "should work" do
            expect(Cohort::ProgramTypeConfig.degree_program_types).to include('mba')
            expect(Cohort::ProgramTypeConfig.degree_program_types).not_to include('the_business_certificate')
        end
    end

    describe "program_type" do
        it "should return a program_type" do
            expect(Cohort::ProgramTypeConfig['test'].program_type).to eq('test')
        end
    end

    describe "provides_full_student_network_access?" do

        it "should be true for degree but not paid cert programs" do
            expect(Cohort::ProgramTypeConfig['mba'].provides_full_student_network_access?).to be(true)
            expect(Cohort::ProgramTypeConfig['emba'].provides_full_student_network_access?).to be(true)
            expect(Cohort::ProgramTypeConfig['paid_cert_data_analytics'].provides_full_student_network_access?).to be(false)
            expect(Cohort::ProgramTypeConfig['career_network_only'].provides_full_student_network_access?).to be(false)
            expect(Cohort::ProgramTypeConfig['the_business_certificate'].provides_full_student_network_access?).to be(false)
        end
    end

    describe "log_foundations_completed_events?" do

        it "should be true for pending applications in the DegreeProgram" do
            application = CohortApplication.first
            allow(application).to receive(:program_type).and_return('mba')
            check_log_foundations(application, 'pending')
        end

        it "should be true for pending or pre_accepted applications in the_business_certificate program" do
            application = CohortApplication.first
            allow(application).to receive(:program_type).and_return('the_business_certificate')
            check_log_foundations(application, 'pending', 'pre_accepted')
        end

        it "should be false for other certificates" do
            application = CohortApplication.first
            allow(application).to receive(:program_type).and_return('paid_cert_data_analytics')
            check_log_foundations(application)
        end

        def check_log_foundations(application, *true_for_statuses)
            expectations = {
                pending: false,
                pre_accepted: false,
                accepted: false,
                rejected: false,
                expelled: false,
                deferred: false
            }
            true_for_statuses.each do |status|
                expectations[status.to_sym] = true
            end

            expectations.each do |status, expected_value|
                application.status = status
                expect(application.log_foundations_completed_events?).to eq(expected_value), "Unexpected value with status #{status.to_s.inspect}"
            end
        end
    end

    describe "user_can_change_status?" do
        before(:each) do
            @application = CohortApplication.first
        end

        describe "generic" do
            before(:each) do
                @config = Cohort::ProgramTypeConfig['test']
            end

            it "should return true when setting status from nil to pending" do
                @application.update_column(:status, 'pending')
                expect(@config.user_can_change_status?(nil, @application.status)).to be(true)
            end
        end

        describe "ManualPromotion" do
            before(:each) do
                @application.cohort.update_column(:program_type, "career_network_only")
                @config = Cohort::ProgramTypeConfig['career_network_only']
            end

            it "should return true when user is reapplying" do
                @application.update_column(:status, "accepted")
                expect(@config.user_can_change_status?(@application.status, "pending")).to be(false)

                @application.update_column(:status, "rejected")
                expect(@config.user_can_change_status?(@application.status, "pending")).to be(true)
            end
        end

        describe "PaidCertificate" do
            before(:each) do
                @application.cohort.update_column(:program_type, "paid_cert_data_analytics")
                @config = Cohort::ProgramTypeConfig['paid_cert_data_analytics']
            end

            it "should return true when pre_accepted" do
                expect(@config.user_can_change_status?(@application.status, "pre_accepted")).to be(true)
            end

            it "should return true when accepted" do
                expect(@config.user_can_change_status?(@application.status, "accepted")).to be(true)
            end
        end
    end

    describe "user_can_create_application?" do
         before(:each) do
            @application = CohortApplication.first
        end

        describe "generic" do
            before(:each) do
                @config = Cohort::ProgramTypeConfig['test']
            end

            it "should return true" do
                expect(@config.user_can_create_application?(@application.user)).to be(true)
            end
        end

        describe "PaidCertificate" do
            before(:each) do
                @application.cohort.update_column(:program_type, "paid_cert_data_analytics")
                @config = Cohort::ProgramTypeConfig['paid_cert_data_analytics']
            end

            it "should return true when user is can_purchase_paid_certs?" do
                @application.user.update_column(:can_purchase_paid_certs, true)
                expect(@config.user_can_create_application?(@application.user)).to be(true)
            end
        end
    end

    describe "user_can_destroy_application?" do
         before(:each) do
            @application = CohortApplication.first
        end

        describe "generic" do
            before(:each) do
                @config = Cohort::ProgramTypeConfig['test']
            end

            it "should return true" do
                expect(@config.user_can_destroy_application?(@application.status)).to be(false)
            end
        end

        describe "PaidCertificate" do
            before(:each) do
                @application.cohort.update_column(:program_type, "paid_cert_data_analytics")
                @config = Cohort::ProgramTypeConfig['paid_cert_data_analytics']
            end

            it "should return true when status is pre_accepted" do
                @application.update_column(:status, 'pre_accepted')
                expect(@config.user_can_destroy_application?(@application.status)).to be(true)
            end
        end
    end

    describe "supports_delayed_career_network_access_on_acceptance?" do

        it "should be true for degree programs" do
            ['mba', 'emba'].each do|program_type|
                config = Cohort::ProgramTypeConfig["#{program_type}"]
                expect(config.supports_delayed_career_network_access_on_acceptance?).to be(true)
            end
        end

        it "should be false for all other program types" do
            ['career_network_only', 'the_business_certificate', 'paid_cert_data_analytics'].each do|program_type|
                config = Cohort::ProgramTypeConfig["#{program_type}"]
                expect(config.supports_delayed_career_network_access_on_acceptance?).to be(false)
            end
        end

    end

    describe "supports_career_network_access_on_acceptance?" do

        it "should be true for career_network_only" do
            ['career_network_only'].each do|program_type|
                config = Cohort::ProgramTypeConfig["#{program_type}"]
                expect(config.supports_career_network_access_on_acceptance?).to be(true)
            end
        end

        it "should be false for all other program types" do
            ['mba', 'emba', 'the_business_certificate', 'paid_cert_data_analytics'].each do|program_type|
                config = Cohort::ProgramTypeConfig["#{program_type}"]
                expect(config.supports_career_network_access_on_acceptance?).to be(false)
            end
        end

    end

    describe "supports_career_network_access_on_airtable_decision?" do

        it "should be true for free and paid certificates" do
            ['the_business_certificate', 'paid_cert_data_analytics'].each do|program_type|
                config = Cohort::ProgramTypeConfig["#{program_type}"]
                expect(config.supports_career_network_access_on_airtable_decision?).to be(true)
            end
        end

        it "should be false for all other program types" do
            ['mba', 'emba', 'career_network_only'].each do|program_type|
                config = Cohort::ProgramTypeConfig["#{program_type}"]
                expect(config.supports_career_network_access_on_airtable_decision?).to be(false)
            end
        end

    end

    describe "supports_auto_active_playlist_selection_on_acceptance?" do

        it "should be true for degree programs" do
            ['mba', 'emba'].each do|program_type|
                config = Cohort::ProgramTypeConfig["#{program_type}"]
                expect(config.supports_auto_active_playlist_selection_on_acceptance?).to be(true)
            end
        end

        it "should be false for all other program types" do
            ['career_network_only', 'the_business_certificate', 'paid_cert_data_analytics'].each do|program_type|
                config = Cohort::ProgramTypeConfig["#{program_type}"]
                expect(config.supports_auto_active_playlist_selection_on_acceptance?).to be(false)
            end
        end

    end

    describe "supports_airtable_sync?" do
        it "should be true for programs other than jordanian_math" do
            Cohort::ProgramTypeConfig.program_types.reject { |type| type == 'jordanian_math'}.each do |program_type|
                config = Cohort::ProgramTypeConfig["#{program_type}"]
                expect(config.supports_airtable_sync?).to be(true)
            end
        end

        it "should be false for jordanian_math" do
            config = Cohort::ProgramTypeConfig["jordanian_math"]
            expect(config.supports_airtable_sync?).to be(false)
        end
    end

end