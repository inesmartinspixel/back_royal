require 'spec_helper'

describe HiringTeamInvite do

    fixtures(:users)

    describe "create_from_hash!" do
        it "should work" do
            allow_any_instance_of(HiringApplication).to receive(:log_created_event)
            inviter = users(:hiring_manager_with_team)
            invitee_email = 'someone@somewhere.com'
            invitee_name = 'bob doe'

            # prevent things that will log other events
            expect_any_instance_of(HiringApplication).to receive(:log_status_event).at_least(1).times # ignore these events so we can assert on the others
            expect_any_instance_of(User).to receive(:log_create_event)

            event = Event.new
            expect(event).to receive(:log_to_external_systems)

            expect(Event).to receive(:create_server_event!).with(
                anything,
                anything,
                'invited_to_hiring_team',
                {
                    inviter_name: inviter.name,
                    inviter_id: inviter.id
                }
            ).and_return(event)

            invitee = HiringTeamInvite.invite!({
                inviter_id: inviter.id,
                invitee_email: invitee_email,
                invitee_name: invitee_name
            })
            expect(invitee.hiring_team_id).to eq(inviter.hiring_team_id)
            expect(invitee.email).to eq(invitee_email)
            expect(invitee.name).to eq(invitee_name)
            expect(invitee.provider).to eq('hiring_team_invite')
            expect(invitee.uid).to eq(invitee_email)

            %w(
                company_year place_id place_details company_employee_count
                company_annual_revenue industry funding
                company_sells_recruiting_services website_url
                team_mission role_descriptors where_descriptors
                position_descriptors salary_ranges
            ).each do |key|
                expect(invitee.hiring_application[key]).to eq(inviter.hiring_application[key])
            end

            expect(invitee.hiring_application.status).to eq('accepted')
            expect(invitee.professional_organization).to eq(inviter.professional_organization)
        end

        it "should work for inviter with no hiring team" do
            inviter = HiringApplication.joins(:user => :professional_organization).where({"users.hiring_team_id" => nil}).first.user
            invitee_email = 'someone@somewhere.com'
            invitee_name = 'bob doe'

            invitee = HiringTeamInvite.invite!({
                inviter_id: inviter.id,
                invitee_email: invitee_email,
                invitee_name: invitee_name
            })
            expect(inviter.reload.hiring_team_id).not_to be_nil
            expect(invitee.hiring_team_id).to eq(inviter.hiring_team_id)
        end

        it "should log error if invitee already exists" do

            inviter = users(:hiring_manager_with_team)
            invitee = users(:hiring_manager_teammate)

            expect {
                HiringTeamInvite.invite!({
                    invitee_name: invitee.name,
                    inviter_id: inviter.id,
                    invitee_email: invitee.email
                })
            }.to raise_error(HiringTeamInvite::InviteeExists) do |error|
                #expect(error.is_a?(HiringTeamInvite::InviteeExists)).to be(true)
                expect(error.details).to eq({
                    error_type: 'invitee_exists',
                    invitee_hiring_team_id: invitee.hiring_team_id,
                    invitee_provider: invitee.provider
                })
            end
        end
    end

end
