# == Schema Information
#
# Table name: institutions_users
#
#  institution_id :uuid
#  user_id        :uuid
#  active         :boolean          default(FALSE), not null
#  id             :uuid             not null, primary key
#

require 'spec_helper'

describe InstitutionUserJoin do
    fixtures(:users)

    before(:each) do
        allow_any_instance_of(InstitutionUserJoin).to receive(:ensure_no_acceptable_application_for_user).and_call_original
    end

    describe "before_destroy" do
        it "should not allowing removing when acceptable application" do
            user = users(:accepted_mba_cohort_user)
            join = user.institution_joins.where(institution_id: user.last_application.institution.id).first
            statuses_message = CohortApplication.acceptable_statuses.to_sentence(last_word_connector: ', or ')
            expect {
                join.destroy!
            }.to raise_error("Validation failed: User has an application with status #{statuses_message} for this institution")
        end

        it "should allow removing when no acceptable application" do
            user = users(:rejected_mba_cohort_user)
            user.ensure_institution(user.last_application.institution)
            join = user.institution_joins.where(institution_id: user.last_application.institution).first
            expect {
                join.destroy!
            }.not_to raise_error
        end
    end

    describe "identify" do
        it "should call identify on user when calling identify_user" do
            instance = InstitutionUserJoin.first
            expect(instance.user).to receive(:identify)
            instance.identify_user
        end
        
        it "should identify user if created" do
            user_with_no_institution = User.left_outer_joins(:institutions).where(institutions: {id: nil}).first
            institution = Institution.first
            expect_any_instance_of(InstitutionUserJoin).to receive(:identify_user).and_call_original
            InstitutionUserJoin.create!(user_id: user_with_no_institution.id, institution_id: institution.id)
        end

        it "should identify user if updated" do
            instance = InstitutionUserJoin.where(active: false).first
            expect(instance).to receive(:identify_user).and_call_original
            instance.update!(active: true)
        end

        it "should identify user if destroyed" do
            instance = InstitutionUserJoin.where(active: false).first
            expect(instance).to receive(:identify_user).and_call_original
            instance.destroy!
        end

        it "should not identify if user being destroyed" do
            instance = InstitutionUserJoin.where(active: false).first

            # FIXME: We should do this in a better and higher way, but just for now to get this work in 
            allow(instance.user).to receive(:delete_stripe_customer_if_stripe_on)

            expect(instance).not_to receive(:identify_user)
            instance.user.destroy!
        end
    end
end
