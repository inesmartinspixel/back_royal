require 'spec_helper'

describe Subscription::SubscriptionConcern do
    attr_reader :user

    include StripeHelper

    fixtures(:cohorts, :users)

    before(:each) do
        @user = users(:user_with_subscriptions_enabled)
    end

    describe 'create_stripe_customer' do

        it "should create a customer and set the customer_id" do
            customer = user.create_stripe_customer
            expect(customer.email).to eq(user.email)
            expect(user.id).to eq(customer.id)
        end

        describe 'create_for_owner' do
            before(:each) do
                create_customer_with_default_source(user)
                user.ensure_stripe_customer
            end

            it 'should create a subscription' do
                stripe_subscription = Subscription.create_for_owner(owner: user, stripe_plan_id: default_plan.id).stripe_subscription
                expect(stripe_subscription).not_to be(nil)
            end

            it 'should set discount and coupon' do
                stripe_subscription = Subscription.create_for_owner(owner: user, stripe_plan_id: default_plan.id, coupon_id: default_coupon.id).stripe_subscription
                expect(stripe_subscription.discount).not_to be(nil)
                expect(stripe_subscription.discount.coupon.id).to eq(default_coupon.id)
            end

            it 'should set trial_end' do
                trial_end_ts = (Time.now + 1.hour).to_timestamp
                stripe_subscription = Subscription.create_for_owner(owner: user, stripe_plan_id: default_plan.id, trial_end_ts: trial_end_ts).stripe_subscription
                expect(stripe_subscription.trial_end).to eq(trial_end_ts)
            end

            it 'should call create_or_reconcile_from_stripe_subscription!' do
                expect(Subscription).to receive(:create_or_reconcile_from_stripe_subscription!) do |_user, stripe_subscription|
                    expect(_user).to eq(user)
                    expect(stripe_subscription.is_a?(Stripe::Subscription)).to be(true)
                end
                Subscription.create_for_owner(owner: user, stripe_plan_id: default_plan.id)
            end

        end

        describe 'user destroy hook' do

            it "should delete the stripe customer when user deleted from db" do
                expect(Stripe).to receive(:api_key).at_least(1).times.and_return('api_key')
                customer = user.create_stripe_customer
                expect(customer).to receive(:delete)
                user.destroy
            end

            it "should not error if no stripe customer" do
                expect(Proc.new {
                    user.destroy
                }).not_to raise_error
            end
        end

        describe 'stripe_customer' do

            it "should load and cache the customer" do
                customer = user.create_stripe_customer
                expect(customer.id).to eq(Stripe::Customer.retrieve(user.id).id)
            end

        end

        describe 'update_stripe_card' do

            before(:each) do
                create_customer_with_default_source(user)
            end

            it "should create a source and save it as the default" do
                orig_card = user.stripe_customer.default_source

                new_card = user.update_stripe_card(stripe_helper.generate_card_token)
                expect(user.stripe_customer.default_source).to eq(new_card.id)
                expect(new_card.id).not_to eq(orig_card) #sanity check
            end

            it "should use an idempotency key" do
                token = stripe_helper.generate_card_token
                expect(user.stripe_customer.sources).to receive(:create).with({:source=>token}, {:idempotency_key=>token}).and_call_original
                user.update_stripe_card(token)
            end

        end

        describe 'default_stripe_card' do

            it "should find the card in the list based on the default_source, which is an id" do
                create_customer_with_default_source(user)
                card_id = user.stripe_customer.default_source
                expect(user.default_stripe_card.id).to eq(card_id)

                # this would be a better test if we added more cards
                # and made sure to get the right one, but strange things
                # happen with the stripe mock when I try to do that
            end

        end

    end

end