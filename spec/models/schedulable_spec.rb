require 'spec_helper'

describe Cohort::Schedulable do

    class SchedulableItem < ActiveRecord::Base
        include Cohort::Schedulable
    end

    before(:each) do
        @schedulable_item = SchedulableItem.new(program_type: 'mba', periods: [])
    end

    it "should be valid if the sum of additional_specialization_playlists_complete is less than or equal to specialization_playlist_pack_ids" do
        @schedulable_item.num_required_specializations = 2
        @schedulable_item.specialization_playlist_pack_ids = [Playlist::LocalePack.first.id, Playlist::LocalePack.second.id]
        @schedulable_item.save!

        # With equal
        @schedulable_item.periods = [{
            "additional_specialization_playlists_complete" => 2
        }]
        expect(@schedulable_item).to be_valid

        # With empty array
        @schedulable_item.periods = []
        expect(@schedulable_item).to be_valid
    end

    it "should be invalid if the sum of additional_specialization_playlists_complete is more than num_required_specializations" do
        @schedulable_item.num_required_specializations = 2
        @schedulable_item.specialization_playlist_pack_ids = [Playlist::LocalePack.first.id]
        @schedulable_item.save!

        @schedulable_item.periods = [{
            "additional_specialization_playlists_complete" => 3 # is greater than the cohort's playlist_pack_ids
        }]
        expect(@schedulable_item.valid?).to be(false)
        expect(@schedulable_item.errors.full_messages).to include("Periods requires 3 specialization playlists but there are only 1"), @schedulable_item.errors.full_messages.inspect
    end

    it "should validate_no_overlap_between_specialization_and_required_playlists" do
        playlist_pack_id = SecureRandom.uuid
        @schedulable_item.specialization_playlist_pack_ids = [playlist_pack_id]
        expect(@schedulable_item).to receive(:validate_no_overlap_between_specialization_and_required_playlists).and_call_original
        expect(@schedulable_item).to receive(:required_playlist_pack_ids).and_return([playlist_pack_id])
        expect(@schedulable_item.valid?).to be(false)
        expect(@schedulable_item.errors.full_messages).to include("Specialization playlist pack ids must not overlap with required playlist pack ids"), @schedulable_item.errors.full_messages.inspect
    end

    it "should validate_number_of_playlist_collections" do
        @schedulable_item.playlist_collections = [{
            title: 'Foo',
            required_playlist_pack_ids: []
        }, {
            title: 'Bar',
            required_playlist_pack_ids: []
        }]
        expect(@schedulable_item).to receive(:validate_number_of_playlist_collections).and_call_original
        expect(@schedulable_item).to receive(:supports_multiple_playlist_collections?).and_return(false)
        expect(@schedulable_item.valid?).to be(false)
        expect(@schedulable_item.errors.full_messages).to include("Playlist collections must only contain one playlist collection"), @schedulable_item.errors.full_messages.inspect
    end

    it "should validate_no_duplicate_playlist_collection_titles" do
        @schedulable_item.playlist_collections = [{
            title: 'Foo',
            required_playlist_pack_ids: []
        }, {
            title: 'Foo',
            required_playlist_pack_ids: []
        }]
        expect(@schedulable_item).to receive(:validate_no_duplicate_playlist_collection_titles).and_call_original
        expect(@schedulable_item.valid?).to be(false)
        expect(@schedulable_item.errors.full_messages).to include("Playlist collections must not have duplicate titles"), @schedulable_item.errors.full_messages.inspect
    end

    it "should validate_no_duplicate_required_playlist_pack_ids_in_playlist_collections" do
        playlist_pack_id = SecureRandom.uuid
        expect(@schedulable_item).to receive(:validate_no_duplicate_required_playlist_pack_ids_in_playlist_collections).and_call_original
        allow(@schedulable_item.playlist_collections).to receive(:present?).and_return(true)
        allow(@schedulable_item).to receive(:required_playlist_pack_ids_from_playlist_collections).and_return([playlist_pack_id, playlist_pack_id])
        expect(@schedulable_item.valid?).to be(false)
        expect(@schedulable_item.errors.full_messages).to include("Playlist collections must not have duplicate required playlist pack ids"), @schedulable_item.errors.full_messages.inspect
    end

    describe "required_playlist_pack_ids" do

        it "should derive its return value from required_playlist_pack_ids_from_playlist_collections" do
            expect(@schedulable_item).to receive(:required_playlist_pack_ids_from_playlist_collections).and_return(['foo', 'bar'])
            expect(@schedulable_item.required_playlist_pack_ids).to eq(['foo', 'bar'])
        end
    end

    describe "required_playlist_pack_ids_from_playlist_collections" do

        it "should return an array of all of the required_playlist_pack_ids in the playlist_collections (ordering preserved)" do
            @schedulable_item.playlist_collections = [{
                required_playlist_pack_ids: ['foo', 'bar']
            }, {
                required_playlist_pack_ids: ['baz', 'qux']
            }, {
                required_playlist_pack_ids: ['quux', 'quuz']
            }]
            # Note that the playlist collection ordering is preserved in the return value
            expect(@schedulable_item.required_playlist_pack_ids_from_playlist_collections).to eq(['foo', 'bar', 'baz', 'qux', 'quux', 'quuz'])
        end
    end

    describe "periods" do
        it "should be invalid if a stream shows up as required in multiple periods" do
            streams = Lesson::Stream.where(locale: 'en').limit(2)
            pack_ids = streams.map{|s| s.locale_pack.id}


            @schedulable_item.periods = [
                {
                    "stream_entries" => [{
                        "locale_pack_id" => pack_ids[0],
                        "required" => true
                    }, {
                        "locale_pack_id" => pack_ids[1],
                        "required" => false # ok to have it duplicated in optional
                    }]
                }, {
                    "stream_entries" => [{
                        "locale_pack_id" => pack_ids[1],
                        "required" => true
                    }]
                }
            ]

            expect(@schedulable_item.valid?).to be(true), @schedulable_item.errors.full_messages.inspect

            @schedulable_item.periods[0]["stream_entries"] << {
                "locale_pack_id" => pack_ids[1],
                "required" => true
            }

            stream_title = Lesson::Stream.where(locale: 'en', locale_pack_id: pack_ids[1]).first.title
            expect(@schedulable_item.valid?).to be(false)
            expect(@schedulable_item.errors.full_messages).to include("Periods includes the stream #{stream_title.inspect} more than once"), @schedulable_item.errors.full_messages.inspect
        end

        it "should be invalid if stream entries has the same stream more than once in a period" do
            pack_ids = Lesson::Stream::LocalePack.limit(1).pluck("id")

            @schedulable_item.periods = [
                {
                    "stream_entries" => [{
                        "locale_pack_id" => pack_ids[0]
                    }, {
                        "locale_pack_id" => pack_ids[0]
                    }]
                }
            ]

            expect(@schedulable_item.valid?).to be(false)
            expect(@schedulable_item.errors.full_messages).to include("Period 1 has the same stream included twice"), @schedulable_item.errors.full_messages.inspect
        end

        it "should be invalid if a stream has no locale_pack_id" do
            @schedulable_item.periods = [
                {
                    "stream_entries" => [{}]
                }
            ]

            expect(@schedulable_item.valid?).to be(false)
            expect(@schedulable_item.errors.full_messages).to include("Period 1 stream entry 1 has no locale_pack_id"), @schedulable_item.errors.full_messages.inspect
        end

        it "should be invalid if an exercise does not contain a message, channel and date offset" do
            @schedulable_item.periods = [
                {
                    "exercises" => [{
                        "message" => "Only a message",
                        "channel" => "channel",
                        "hours_offset_from_end" => 3
                    }]
                }
            ]

            exercise = @schedulable_item.periods[0]['exercises'][0]
            expect(@schedulable_item.valid?).to be(true)


            ['channel', 'hours_offset_from_end', 'message'].each do |key|
                orig = exercise[key]
                exercise.delete(key)
                expect(@schedulable_item.valid?).to be(false)
                expect(@schedulable_item.errors.full_messages).to include("Period 1 exercise 1 must have a #{key}"), @schedulable_item.errors.full_messages.inspect
                exercise[key] = orig
            end

        end

        it "should be invalid if not every slack room is assigned a message" do
            stub_slack_rooms
            @exercise['slack_room_overrides'][@slack_rooms[0].id]['message'] = 'message for slack room 1'
            expect(@schedulable_item.valid?).to be(false)
            expect(@schedulable_item.errors.full_messages).to include("Period 1 exercise 1 must have a message for \"#{@slack_rooms[1].title}\""), @schedulable_item.errors.full_messages.inspect
        end

        it "should be invalid if not every slack room is assigned a document" do
            stub_slack_rooms
            @exercise['slack_room_overrides'][@slack_rooms[0].id]['document'] = {a: 'document'}
            expect(@schedulable_item.valid?).to be(false)
            expect(@schedulable_item.errors.full_messages).to include("Period 1 exercise 1 must have a document for \"#{@slack_rooms[1].title}\""), @schedulable_item.errors.full_messages.inspect
        end

        it "should be invalid if there are overrides for a slack room that does not exist" do
            stub_slack_rooms
            @exercise['slack_room_overrides']['some_other_id'] = {'message' => 'Who is this for?'}
            expect(@schedulable_item.valid?).to be(false)
            expect(@schedulable_item.errors.full_messages).to include("Period 1 exercise 1 must have a message for \"#{@slack_rooms[1].title}\""), @schedulable_item.errors.full_messages.inspect
        end

        it "should be valid if all slack rooms have messages and none have documents" do
            stub_slack_rooms
            @exercise['slack_room_overrides'][@slack_rooms[0].id]['message'] = 'message for slack room 1'
            @exercise['slack_room_overrides'][@slack_rooms[1].id]['message'] = 'message for slack room 2'
            expect(@schedulable_item.valid?).to be(true), @schedulable_item.errors.full_messages.inspect
        end

        it "should be valid if all slack rooms have messages and documents" do
            stub_slack_rooms
            @exercise['slack_room_overrides'][@slack_rooms[0].id]['message'] = 'message for slack room 1'
            @exercise['slack_room_overrides'][@slack_rooms[1].id]['message'] = 'message for slack room 2'
            @exercise['slack_room_overrides'][@slack_rooms[0].id]['document'] = {a: 'document'}
            @exercise['slack_room_overrides'][@slack_rooms[1].id]['document'] = {a: 'document'}
            expect(@schedulable_item.valid?).to be(true), @schedulable_item.errors.full_messages.inspect
        end

        def stub_slack_rooms
            @slack_rooms ||= CohortSlackRoom.limit(2).to_a
            allow(@schedulable_item).to receive(:slack_rooms).and_return(@slack_rooms)

            @schedulable_item.periods = [
                {
                    "exercises" => [{
                        "channel" => "channel",
                        "hours_offset_from_end" => 3,
                        "slack_room_overrides" => {
                            @slack_rooms[0].id => {},
                            @slack_rooms[1].id =>{}
                        }
                    }]
                }
            ]
            @exercise = @schedulable_item.periods[0]['exercises'][0]
        end

    end


    describe "stripe" do

        it "should be invalid if a providing stripe_plans without scholarship_levels" do
            expect(@schedulable_item).to receive(:supports_scholarship_levels?).and_return(true)
            @schedulable_item.stripe_plans = [
                {
                    "id" => "default_plan",
                    "amount" => 80000,
                    "frequency" => "monthly"
                }, {
                    "id" => "alternate_plan",
                    "amount" => 480000,
                    "frequency" => "bi_annual"
                }
            ]

            expect(@schedulable_item.valid?).to be(false)
            expect(@schedulable_item.errors.full_messages).to include("Stripe plans cannot be set without scholarship_levels"), @schedulable_item.errors.full_messages.inspect
        end

        it "should be valid if a providing stripe_plans without scholarship_levels and supports_scholarship_levels? is false" do
            expect(@schedulable_item).to receive(:supports_scholarship_levels?).and_return(false)
            @schedulable_item.stripe_plans = [
                {
                    "id" => "default_plan",
                    "amount" => 80000,
                    "frequency" => "monthly"
                }, {
                    "id" => "alternate_plan",
                    "amount" => 480000,
                    "frequency" => "bi_annual"
                }
            ]

            expect(@schedulable_item.valid?).to be(true)
        end

        it "should be invalid if a providing stripe_plans without scholarship_levels" do
            @schedulable_item.scholarship_levels = [
                {
                    "name" => "Level 1",
                    "standard" => {
                        "default_plan" => {
                            "id" => "discount_150",
                            "amount_off" => 15000,
                            "percent_off" => 0
                        },
                        "alternate_plan" => {
                            "id" => "discount_1400",
                            "amount_off" => 140000,
                            "percent_off" => 0
                        }
                    },
                    "early" => {
                        "default_plan" => {
                            "id" => "discount_150",
                            "amount_off" => 15000,
                            "percent_off" => 0
                        },
                        "alternate_plan" => {
                            "id" => "discount_1400",
                            "amount_off" => 140000,
                            "percent_off" => 0
                        }
                    }
                }
            ]

            expect(@schedulable_item.valid?).to be(false)
            expect(@schedulable_item.errors.full_messages).to include("Scholarship levels cannot be set without stripe_plans"), @schedulable_item.errors.full_messages.inspect
        end

        it "should be valid with plans that match scholarship_levels" do

            @schedulable_item.stripe_plans = [
                {
                    "id" => "default_plan",
                    "amount" => 80000,
                    "frequency" => "monthly"
                }, {
                    "id" => "alternate_plan",
                    "amount" => 480000,
                    "frequency" => "bi_annual"
                }
            ]

            @schedulable_item.scholarship_levels = [
                {
                    "name" => "Level 1",
                    "standard" => {
                        "default_plan" => {
                            "id" => "discount_150",
                            "amount_off" => 15000,
                            "percent_off" => 0
                        },
                        "alternate_plan" => {
                            "id" => "discount_1400",
                            "amount_off" => 140000,
                            "percent_off" => 0
                        }
                    },
                    "early" => {
                        "default_plan" => {
                            "id" => "discount_150",
                            "amount_off" => 15000,
                            "percent_off" => 0
                        },
                        "alternate_plan" => {
                            "id" => "discount_1400",
                            "amount_off" => 140000,
                            "percent_off" => 0
                        }
                    }
                }
            ]

            expect(@schedulable_item.valid?).to be(true), @schedulable_item.errors.full_messages.inspect
        end

        it "should be invalid with plans that overlap frequencies" do

            @schedulable_item.stripe_plans = [
                {
                    "id" => "default_plan",
                    "amount" => 80000,
                    "frequency" => "monthly"
                }, {
                    "id" => "alternate_plan",
                    "amount" => 480000,
                    "frequency" => "monthly"
                }
            ]

            @schedulable_item.scholarship_levels = [
                {
                    "name" => "Level 1",
                    "standard" => {
                        "default_plan" => {
                            "id" => "discount_150",
                            "amount_off" => 15000,
                            "percent_off" => 0
                        },
                        "alternate_plan" => {
                            "id" => "discount_1400",
                            "amount_off" => 140000,
                            "percent_off" => 0
                        }
                    },
                    "early" => {
                        "default_plan" => {
                            "id" => "discount_150",
                            "amount_off" => 15000,
                            "percent_off" => 0
                        },
                        "alternate_plan" => {
                            "id" => "discount_1400",
                            "amount_off" => 140000,
                            "percent_off" => 0
                        }
                    }
                }
            ]

            expect(@schedulable_item.valid?).to be(false)
            expect(@schedulable_item.errors.full_messages).to include("Stripe plans can only have 1 type of billing frequency plan per template"), @schedulable_item.errors.full_messages.inspect
        end

        it "should be invalid with levels without names" do

            @schedulable_item.stripe_plans = [
                {
                    "id" => "default_plan",
                    "amount" => 80000,
                    "frequency" => "monthly"
                }, {
                    "id" => "alternate_plan",
                    "amount" => 480000,
                    "frequency" => "bi_annual"
                }
            ]

            @schedulable_item.scholarship_levels = [
                {
                    "standard" => {
                        "default_plan" => {
                            "id" => "discount_150",
                            "amount_off" => 15000,
                            "percent_off" => 0
                        },
                        "alternate_plan" => {
                            "id" => "discount_1400",
                            "amount_off" => 140000,
                            "percent_off" => 0
                        }
                    },
                    "early" => {
                        "default_plan" => {
                            "id" => "discount_150",
                            "amount_off" => 15000,
                            "percent_off" => 0
                        },
                        "alternate_plan" => {
                            "id" => "discount_1400",
                            "amount_off" => 140000,
                            "percent_off" => 0
                        }
                    }
                }
            ]

            expect(@schedulable_item.valid?).to be(false)
            expect(@schedulable_item.errors.full_messages).to include("Scholarship levels must be named"), @schedulable_item.errors.full_messages.inspect
        end

        it "should be invalid with levels that don't match plans" do

            @schedulable_item.stripe_plans = [
                {
                    "id" => "plan_a",
                    "amount" => 80000,
                    "frequency" => "monthly"
                }, {
                    "id" => "plan_b",
                    "amount" => 480000,
                    "frequency" => "bi_annual"
                }
            ]

            @schedulable_item.scholarship_levels = [
                {
                    "standard" => {
                        "plan_b" => {
                            "id" => "discount_150",
                            "amount_off" => 15000,
                            "percent_off" => 0
                        },
                        "plan_c" => {
                            "id" => "discount_1400",
                            "amount_off" => 140000,
                            "percent_off" => 0
                        }
                    },
                    "early" => {
                        "plan_b" => {
                            "id" => "discount_150",
                            "amount_off" => 15000,
                            "percent_off" => 0
                        },
                        "plan_c" => {
                            "id" => "discount_1400",
                            "amount_off" => 140000,
                            "percent_off" => 0
                        }
                    }
                }
            ]

            expect(@schedulable_item.valid?).to be(false)
            expect(@schedulable_item.errors.full_messages).to include("Scholarship levels must be named"), @schedulable_item.errors.full_messages.inspect
        end

    end

end