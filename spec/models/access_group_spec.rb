# == Schema Information
#
# Table name: access_groups
#
#  id         :uuid             not null, primary key
#  name       :text             not null
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe AccessGroup do

    fixtures(:access_groups, :lesson_streams)

    describe "INDESTRUCTIBLE_ACCESS_GROUPS" do

        it "should include OPEN COURSES and EXTRA COURSES" do
            expect(AccessGroup::INDESTRUCTIBLE_ACCESS_GROUPS).to include('OPEN COURSES')
            expect(AccessGroup::INDESTRUCTIBLE_ACCESS_GROUPS).to include('EXTRA COURSES')
        end
    end

    describe "ensure_can_be_destroyed" do

        it "should raise RecordInvalid error if attempting to destroy indestructible access group" do
            access_group = access_groups(:default_group)
            expect(AccessGroup::INDESTRUCTIBLE_ACCESS_GROUPS).to receive(:include?).with(access_group.name).and_return(true)
            expect {
                access_group.ensure_can_be_destroyed
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: #{access_group.name.capitalize} is indestructible. Refer to engineering for assistance.")
        end

        it "should not raise error if access group can be destroyed" do
            access_group = access_groups(:default_group)
            expect(AccessGroup::INDESTRUCTIBLE_ACCESS_GROUPS).to receive(:include?).with(access_group.name).and_return(false)
            expect {
                access_group.ensure_can_be_destroyed
            }.not_to raise_error
        end
    end

    describe "before_destroy" do

        it "should ensure_can_be_destroyed" do
            access_group = access_groups(:default_group)
            expect(access_group).to receive(:ensure_can_be_destroyed)
            access_group.destroy
        end
    end

    describe "update_derived_content_tables_on_destroy" do
        it "should work" do
            access_group = AccessGroup.create!(name: 'test')
            stream = lesson_streams(:en_only_item)
            access_group.lesson_stream_locale_packs << stream.locale_pack
            expect_any_instance_of(Lesson::Stream).to receive(:get_derived_content_table_update_triggers) do |stream, args|
                expect(args[:old_version]).to eq(stream.published_version)
                expect(args[:new_version]).to eq(stream.published_version)
                expect(args[:access_groups_did_change]).to be(true)
                [:trigger1, :trigger2]
            end
            expect(Lesson::Stream).to receive(:make_derived_content_table_updates).with([:trigger1, :trigger2])
            expect(RefreshMaterializedContentViews).to receive(:refresh)

            access_group.destroy

        end
    end

    describe "update_from_hash!" do
        it "should make derived content table updates" do
            access_group = AccessGroup.create!(name: 'test')
            stream = lesson_streams(:en_only_item)
            expect_any_instance_of(Lesson::Stream).to receive(:get_derived_content_table_update_triggers) do |stream, args|
                expect(args[:old_version]).to eq(stream.published_version)
                expect(args[:new_version]).to eq(stream.published_version)
                expect(args[:access_groups_did_change]).to be(true)
                [:trigger1, :trigger2]
            end
            expect(Lesson::Stream).to receive(:make_derived_content_table_updates).with([:trigger1, :trigger2])
            expect(RefreshMaterializedContentViews).to receive(:refresh)

            access_group.update_from_hash!(access_group.as_json.merge('stream_locale_pack_ids' => [stream.locale_pack_id]))
        end
    end
end
