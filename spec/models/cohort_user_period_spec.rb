# == Schema Information
#
# Table name: cohort_user_periods
#
#  id                                                              :uuid             not null, primary key
#  user_id                                                         :uuid             not null
#  cohort_id                                                       :uuid             not null
#  cohort_name                                                     :text             not null
#  index                                                           :integer          not null
#  required_lessons_completed_in_period                            :integer          not null
#  specialization_lessons_completed_in_period                      :integer          not null
#  period_start                                                    :datetime         not null
#  period_end                                                      :datetime         not null
#  required_lesson_pack_ids_completed_in_period                    :uuid             not null, is an Array
#  specialization_lesson_pack_ids_completed_in_period              :uuid             not null, is an Array
#  status_at_period_end                                            :text             not null
#  cumulative_required_lessons_completed                           :integer          not null
#  cumulative_required_up_to_now_lessons_completed                 :integer          not null
#  cumulative_required_and_specialization_lessons_completed_in_las :integer          not null
#  cumulative_specializations_completed                            :integer          not null
#  expected_specialization_lessons_completed                       :integer          not null
#  cumulative_specialization_lessons_completed                     :integer          not null
#  cumulative_specialization_up_to_now_lessons_completed           :integer          not null
#  total_up_to_now_lessons_completed                               :integer          not null
#  total_lessons_expected                                          :integer          not null
#  updated_at                                                      :datetime         not null
#

require 'spec_helper'

describe CohortUserPeriod do
    attr_reader :cohort

    fixtures(:cohorts)

    before(:each) do
        @cohort = cohorts(:published_mba_cohort)
        cohort.start_date = Time.now - 17.days
        cohort.publish!
        RefreshMaterializedContentViews.refresh(true)
        CohortUserPeriod.delete_all
    end

    it "should write records for periods in the past" do
        periods = PublishedCohortPeriod.where(cohort_id: cohort.id)
        past_periods = periods.select { |p| p.period_end < Time.now }
        future_periods = periods.select { |p| p.period_end > Time.now }
        expect(past_periods).not_to be_empty
        expect(future_periods).not_to be_empty

        CohortUserPeriod.write_new_records
        expect(CohortUserPeriod.where(cohort_id: cohort.id).distinct.pluck(:index)).to match_array(past_periods.map(&:index))
        expect(CohortUserPeriod.maximum(:updated_at)).to be_within(10.seconds).of(Time.now)
    end

    it "should write a new record when a cohort is published" do
        CohortUserPeriod.write_new_records

        orig_name = cohort.name
        cohort.name = 'updated'
        cohort.save!

        # writing new records should change nothing since we
        # have not published the change to the cohort
        CohortUserPeriod.write_new_records
        expect(CohortUserPeriod.where(cohort_id: cohort.id).distinct.pluck('cohort_name')).to eq([orig_name])


        # publishing should trigger a re-write
        cohort.publish!({update_published_at: true})
        RefreshMaterializedContentViews.refresh(true)
        RefreshMaterializedInternalReportsViews.perform_now(true) # cohort_name is actually pulled from cohort_applications_plus so this needs to be updated
        CohortUserPeriod.write_new_records
        expect(CohortUserPeriod.where(cohort_id: cohort.id).distinct.pluck('cohort_name')).to eq(['updated'])
    end

    it "should write records for a user who is added to the cohort after it starts" do
        CohortUserPeriod.write_new_records
        cohort_application_id = ActiveRecord::Base.connection.execute("
            select id
            from cohort_applications_plus
            where was_accepted = false and cohort_id='#{cohort.id}' limit 1 "
        ).to_a.first['id']
        cohort_application = CohortApplication.find(cohort_application_id)
        expect(CohortUserPeriod.where(cohort_id: cohort.id, user_id: cohort_application.user_id)).to be_empty
        cohort_application.status = 'accepted'
        cohort_application.save!(validate: false)
        RefreshMaterializedInternalReportsViews.perform_now(true) # need to update cohort_applications_plus
        CohortUserPeriod.write_new_records
        expect(CohortUserPeriod.where(cohort_id: cohort.id, user_id: cohort_application.user_id)).not_to be_empty
    end
end
