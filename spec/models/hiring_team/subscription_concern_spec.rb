require 'spec_helper'

describe HiringTeam::SubscriptionConcern do
    attr_reader :hiring_team

    include StripeHelper

    fixtures(:hiring_teams, :users)

    before(:each) do
        @hiring_team = hiring_teams(:hiring_team)
        hiring_team.update(subscription_required: true)
    end

    describe "increment_usage" do

        it "should record usage with Stripe if there is a metered subscription" do
            create_subscription(hiring_team, metered_plan.id)
            subscription_item_id = hiring_team.primary_subscription.subscription_item.id
            timestamp = Time.now.to_timestamp
            expect(Stripe::SubscriptionItem).to receive(:create_usage_record).with(
            subscription_item_id,
                {
                quantity: 1,
                timestamp: timestamp,
                action: 'increment'
            })
            hiring_team.increment_usage(hiring_team.primary_subscription, timestamp)
        end

        it "should raise if the subscription is not metered" do
            create_subscription(hiring_team, default_plan.id)
            expect(Stripe::SubscriptionItem).not_to receive(:create_usage_record)
            expect {
                hiring_team.increment_usage(hiring_team.primary_subscription, Time.now.to_timestamp)
            }.to raise_error("Attempting to increment usage on a non-metered subscription!")
        end

    end

    describe "after_create" do
        it "should log an event" do
            mock_event = "mock_event"
            expect(mock_event).to receive(:log_to_external_systems)
            expect_any_instance_of(Subscription).to receive(:metered?).and_return('metered')

            expect(Event).to receive(:create_server_event!).with(anything, hiring_team.owner.id, 'hiring_subscription:created', {
                plan_id: default_plan.id,
                metered_plan: 'metered'
            }).and_return(mock_event)

            create_subscription(hiring_team, default_plan.id)
        end
    end

    describe "handle_create_subscription_request" do

        before(:each) do
            # the controller will ensure that this hiring_team has a valid card
            # before we get here
            create_customer_with_default_source(hiring_team)
        end

        it "should call create_for_owner and create an internal subscription" do
            hiring_team.update!(hiring_plan: nil)

            expect(Subscription).to receive(:create_for_owner)
                .with(owner: hiring_team, stripe_plan_id: unlimited_with_sourcing_plan.id, trial_end_ts: nil, metadata: {}) # no trial
                .and_call_original

            subscription = hiring_team.handle_create_subscription_request({}, {}, unlimited_with_sourcing_plan.id)

            reloaded = HiringTeam.find(hiring_team.id)
            expect(subscription).not_to be_nil
            expect(subscription).to eq(reloaded.primary_subscription)
        end

        it "should call create_for_owner, destroy previous subscription, and create another internal subscription when switching from pay_per_post to unlimited" do
            hiring_team.update!(hiring_plan: HiringTeam::HIRING_PLAN_PAY_PER_POST)

            expect(Subscription).to receive(:create_for_owner)
                .with(owner: hiring_team, stripe_plan_id: unlimited_with_sourcing_plan.id, trial_end_ts: nil, metadata: {}) # no trial
                .and_call_original
            expect(hiring_team).to receive(:cancel_pay_per_post_subscriptions_before_switching_to_unlimited)

            subscription = hiring_team.handle_create_subscription_request({}, {}, unlimited_with_sourcing_plan.id)

            reloaded = HiringTeam.find(hiring_team.id)
            expect(subscription).not_to be_nil
            expect(subscription).to eq(reloaded.primary_subscription)
        end

        it "should raise if stripe_plan is not available for hiring_plan" do
            hiring_team = hiring_teams(:hiring_team)
            expect(HiringTeam).to receive(:stripe_plan).and_return(OpenStruct.new(product_metadata: {available_for_hiring_plan: false}))

            expect {
                hiring_team.handle_create_subscription_request({}, {}, 'foo_plan')
            }.to raise_error("Cannot create subscription for stripe_plan that is unavailable for team's hiring_plan")

        end

        describe "re-registration on legacy hiring_plan" do

            before(:each) do
                hiring_team.update!(hiring_plan: HiringTeam::HIRING_PLAN_LEGACY)
            end

            it "should cancel an existing subscription if provided proper meta" do
                create_subscription(hiring_team, hiring_unlimited_plan.id)

                expect(Subscription).to receive(:create_for_owner)
                    .with(owner: hiring_team, stripe_plan_id: metered_plan.id, trial_end_ts: nil, metadata: {}) # no trial
                    .and_call_original

                # by calling stripe_customer, we load up existing subscriptions.  This ensures
                # that we're appripriately calling reset_stripe_customer later in order to
                # prevent a duplicate subscription error
                hiring_team_id = hiring_team.id
                hiring_team = HiringTeam.find(hiring_team_id)
                hiring_team.stripe_customer
                orig_stripe_subscription_id = hiring_team.primary_subscription.stripe_subscription_id

                subscription = hiring_team.handle_create_subscription_request({}, { cancel_current_subscription: true }, metered_plan.id)

                reloaded = HiringTeam.find(hiring_team.id)
                expect(subscription).not_to be_nil
                expect(subscription).to eq(reloaded.primary_subscription)

                orig_stripe_subscription = Stripe::Subscription.retrieve(orig_stripe_subscription_id)
                expect(orig_stripe_subscription.metadata['cancellation_reason']).to eq('Changing plan to hiring_per_match_20')
            end

            it "should free trial if changing from metered to licensed subscription" do
                create_subscription(hiring_team, metered_plan.id)

                # by calling stripe_customer, we load up existing subscriptions.  This ensures
                # that we're appripriately calling reset_stripe_customer later in order to
                # prevent a duplicate subscription error
                hiring_team_id = hiring_team.id
                hiring_team = HiringTeam.find(hiring_team_id)
                hiring_team.stripe_customer
                orig_stripe_subscription_id = hiring_team.primary_subscription.stripe_subscription_id

                now = Time.now
                expect(Time).to receive(:now).at_least(1).times.and_return(now)
                expected_trial_ts = (now + 2.minutes).to_timestamp

                expect(Subscription).to receive(:create_for_owner)
                    .with(owner: hiring_team, stripe_plan_id: hiring_unlimited_plan.id, trial_end_ts: expected_trial_ts, metadata: {}) # 5m trial
                    .and_call_original

                subscription = hiring_team.handle_create_subscription_request({}, { cancel_current_subscription: true }, hiring_unlimited_plan.id)

                reloaded = HiringTeam.find(hiring_team.id)
                expect(subscription).not_to be_nil
                expect(subscription).to eq(reloaded.primary_subscription)

                orig_stripe_subscription = Stripe::Subscription.retrieve(orig_stripe_subscription_id)
                expect(orig_stripe_subscription.metadata['cancellation_reason']).to eq('Changing plan to hiring_unlimited_300')
            end

            it "should raise if there is not an existing subscription" do
                expect {
                    hiring_team.handle_create_subscription_request({}, { cancel_current_subscription: true }, hiring_unlimited_plan.id)
                }.to raise_error("Attempting to cancel a non-existent current subscription!")
                reloaded = HiringTeam.find(hiring_team.id)
                expect(reloaded.primary_subscription).to be_nil
            end

            it "should raise if the existing subscription's plan is the same as being created" do
                create_subscription(hiring_team, hiring_unlimited_plan.id)

                subscription = hiring_team.primary_subscription

                expect {
                    hiring_team.handle_create_subscription_request({}, { cancel_current_subscription: true }, hiring_unlimited_plan.id)
                }.to raise_error("Attempting to cancel a subscription when while setting the new plan to the same as the existing one!")

                reloaded = HiringTeam.find(hiring_team.id)
                expect(subscription).not_to be_nil
                expect(subscription).to eq(reloaded.primary_subscription)

            end

        end

        describe "pay-per-post" do

            before(:each) do
                hiring_team.update!(hiring_plan: HiringTeam::HIRING_PLAN_PAY_PER_POST)
            end

            it "should raise if position is not for hiring team" do
                open_position = OpenPosition.where(subscription_id: nil).first
                another_hiring_team = HiringTeam.where.not(id: hiring_team.id).first
                open_position.update(hiring_manager_id: another_hiring_team.hiring_managers.first.id)

                expect {
                    hiring_team.handle_create_subscription_request({}, {open_position_id: open_position.id}, pay_per_post_plan.id)
                }.to raise_error("Expected open position to be connected to hiring team for subscription")
            end

            it "should raise if pay_per_post plan and no open_position" do
                expect {
                    hiring_team.handle_create_subscription_request({}, {}, pay_per_post_plan.id)
                }.to raise_error("Must provide an open_position when creating a subscription for a pay_per_post plan")
            end

            it "should raise if open_position not found" do
                expect {
                    hiring_team.handle_create_subscription_request({}, {open_position_id: SecureRandom.uuid}, pay_per_post_plan.id)
                }.to raise_error(ActiveRecord::RecordNotFound)
            end

            it "should set the hiring_plan to HIRING_PLAN_PAY_PER_POST if hiring_plan is not already set" do
                hiring_team.update_column(:hiring_plan, nil)
                expect(hiring_team.hiring_plan).to be_nil # sanity check
                open_position = create_open_position_subscription
                expect(hiring_team.hiring_plan).to eq(HiringTeam::HIRING_PLAN_PAY_PER_POST)
            end

            it "should be possible to switch to unlimited when there is a subscription for an open position" do
                create_open_position_subscription
                hiring_team.reset_stripe_customer
                hiring_team.subscriptions
                hiring_team.handle_create_subscription_request({}, {}, unlimited_with_sourcing_plan.id)
                expect(hiring_team.reload.hiring_plan).to eq(HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING)
            end

            def create_open_position_subscription
                open_position = OpenPosition.where(subscription_id: nil).first
                open_position.update_column(:hiring_manager_id, hiring_team.hiring_managers.first.id)
                expect(open_position.hiring_manager_id).to eq(hiring_team.hiring_managers.first.id)

                hiring_team.handle_create_subscription_request({}, {open_position_id: open_position.id}, pay_per_post_plan.id)
                open_position
            end

            it "should add open_position_id to metadata, attach subscription to open position, and publish position" do
                hiring_team.hiring_plan = HiringTeam::HIRING_PLAN_PAY_PER_POST
                hiring_team.save!
                open_position = OpenPosition.where(subscription_id: nil, featured: false).first
                open_position.update!(hiring_manager_id: hiring_team.hiring_managers.first.id, archived: true, auto_expiration_date: Time.now)

                expect(Subscription).to receive(:create_for_owner)
                    .with(owner: hiring_team, stripe_plan_id: pay_per_post_plan.id, trial_end_ts: nil, metadata: {open_position_id: open_position.id})
                    .and_call_original

                subscription = hiring_team.handle_create_subscription_request({}, {open_position_id: open_position.id}, pay_per_post_plan.id)

                reloaded = HiringTeam.find(hiring_team.id)
                expect(subscription).not_to be_nil

                # new subscription should have the expected owner
                expect(subscription.owner).to eq(hiring_team)

                # open_position_id should be in metadata
                expect(subscription.stripe_subscription.metadata['open_position_id']).to eq(open_position.id)

                # pay_per_post subscriptions are not "active" subscriptions
                expect(reloaded.primary_subscription).to be_nil

                # subscription should be associated with open_position
                expect(open_position.reload.subscription).to eq(subscription)

                # publish
                expect(open_position.featured).to eq(true)
                expect(open_position.archived).to eq(false)

                # implemented in reconcile_auto_expiration_date
                expect(open_position.auto_expiration_date).to be_nil
            end
        end

        describe "unlimited-with-sourcing" do
            it "should work when hiring_plan is unlimited-with-sourcing" do
                owner = users(:hiring_manager_with_team)
                hiring_team = owner.hiring_team
                hiring_team.update_columns(hiring_plan: HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING)
                expect(Subscription).to receive(:create_for_owner).with(
                    owner: hiring_team,
                    stripe_plan_id: unlimited_with_sourcing_plan.id,
                    trial_end_ts: nil,
                    metadata: {}
                )
                hiring_team.handle_create_subscription_request({}, {}, unlimited_with_sourcing_plan.id)
            end
        end
    end

    describe "is_pay_per_post_plan?" do
        it "should work" do
            expect(HiringTeam.is_pay_per_post_plan?('hiring_pay_per_post_89')).to be(true)
            expect(HiringTeam.is_pay_per_post_plan?('hiring_unlimited_w_sourcing_249')).to be(false)
        end
    end

    describe "is_unlimited_with_sourcing_plan?" do
        it "should work" do
            expect(HiringTeam.is_unlimited_with_sourcing_plan?('hiring_pay_per_post_89')).to be(false)
            expect(HiringTeam.is_unlimited_with_sourcing_plan?('hiring_unlimited_w_sourcing_249')).to be(true)
        end
    end

    describe "raise_if_duplicate_subscription!" do
        it "should raise if there is already a subscription for this open position" do
            open_position = OpenPosition.joins(:hiring_team => :owner).left_outer_joins(:subscription).where("subscriptions.id is null").first
            create_subscription(open_position.hiring_team, pay_per_post_plan.id, nil, {open_position_id: open_position.id})
            expect{
                create_subscription(open_position.hiring_team, pay_per_post_plan.id, nil, {open_position_id: open_position.id})
            }.to raise_error(Subscription::CannotCreateDuplicateSubscription)
        end

        it "should not raise if there is a subscription for a different open position" do
            open_position_1, open_position_2 = OpenPosition.joins(:hiring_team => :owner).left_outer_joins(:subscription).where("subscriptions.id is null").limit(2)
            open_position_2.update(hiring_manager_id: open_position_1.hiring_manager_id)
            hiring_team = open_position_1.hiring_team
            expect(open_position_2.hiring_team).to eq(open_position_1.hiring_team) # sanity check

            create_subscription(hiring_team, pay_per_post_plan.id, nil, {open_position_id: open_position_1.id})

            expect {
                create_subscription(hiring_team, pay_per_post_plan.id, nil, {open_position_id: open_position_2.id})
            }.not_to raise_error
        end

        it "should raise if there is already an primary_subscription" do
            create_subscription(hiring_team, default_plan.id)
            expect{
                create_subscription(hiring_team, default_plan.id)
            }.to raise_error(Subscription::CannotCreateDuplicateSubscription)
        end
    end

    describe "before_subscription_destroy" do

        it "should auto archive an active associated open_position" do
            open_position = cancel_open_position_subscription(reason: nil)
            expect(open_position.archived).to be(true)
            expect(open_position.auto_expiration_date).not_to be_nil
            expect(open_position.subscription).to be_nil
        end

        it "should not auto archive an open_position when !should_have_auto_expiration_date?" do
            open_position = create_open_position_subscription
            expect_any_instance_of(OpenPosition).to receive(:should_have_auto_expiration_date?).at_least(1).times.and_return(false)
            open_position.subscription.cancel_and_destroy(reason: nil)
            open_position.reload
            expect(open_position.archived).to be(false)
            expect(open_position.auto_expiration_date).to be_nil
            expect(open_position.subscription).to be_nil # Should still destroy the subscription
        end

        it "should not error if cancelled via Stripe Dashboard with an open position" do
            open_position = create_open_position_subscription()
            expect(open_position.subscription).to receive(:stripe_subscription).at_least(1).times { nil }
            expect {
                open_position.subscription.destroy
            }.not_to raise_error
            open_position.reload
            expect(open_position.archived).to be(true)
            expect(open_position.subscription).to be_nil
        end

        it "should not raise with an primary subscription" do
            subscription = create_subscription(hiring_team, default_plan.id)
            expect {
                subscription.destroy
            }.not_to raise_error
        end

        it "should reset the hiring_plan on if destroying the primary_subscription" do
            hiring_team.hiring_plan = HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING
            hiring_team.save
            subscription = create_subscription(hiring_team, default_plan.id)
            subscription.destroy
            expect(hiring_team.reload.hiring_plan).to be_nil
        end

        def create_open_position_subscription
            open_position = OpenPosition.joins(:hiring_team => :owner)
                                .left_outer_joins(:subscription)
                                .where("subscriptions.id is null")
                                .where(archived: false).first
            open_position.subscription = create_subscription(open_position.hiring_team, pay_per_post_plan.id, nil, {open_position_id: open_position.id})
            open_position.save!
            expect(open_position.hiring_team).to receive(:before_subscription_destroy).and_call_original
            open_position
        end

        def cancel_open_position_subscription(reason:)
            open_position = create_open_position_subscription
            open_position.subscription.cancel_and_destroy(reason: reason)
            open_position.reload
            open_position
        end

    end

    describe "has_full_access?" do

        attr_accessor :team

        before(:each) do
            @team = HiringTeam.where('owner_id IS NOT NULL')
                    .left_outer_joins(:subscriptions)
                    .where("subscriptions.id is null")
                    .where(subscription_required: true)
                    .limit(1).first
        end

        it "should be true if payment is not required" do
            team.subscription_required = false
            expect(team.has_full_access?).to be(true)
        end

        it "should be false if payment is required and there is no subscription" do
            team.subscription_required = true
            expect(team.primary_subscription).to be_nil # sanity check
            expect(team.has_full_access?).to be(false)
        end

        it "should be true if the associated subscription is not past due" do
            expect(team.primary_subscription).to be_nil # sanity check
            create_customer_with_default_source(team)
            subscription = Subscription.new({
                stripe_subscription_id: 'some_subscription_id',
                stripe_plan_id: default_plan.id,
                stripe_product_id: default_plan.product.id,
                past_due: false
            })
            subscription.hiring_team = team
            subscription.save!
            expect(team.reload.has_full_access?).to be(true)
        end

        it "should be false if the associated subscription is past due" do
            expect(team.primary_subscription).to be_nil # sanity check
            expect(team.subscription_required).to be(true) # sanity check
            create_customer_with_default_source(team)
            subscription = Subscription.new({
                stripe_subscription_id: 'some_subscription_id',
                stripe_plan_id: default_plan.id,
                stripe_product_id: default_plan.product.id,
                past_due: true
            })
            subscription.hiring_team = team
            subscription.save!
            team.reload
            expect(team.has_full_access?).to be(false)
        end

        it "should be true if pay-per-post" do
            team.subscription_required = true
            team.hiring_plan = HiringTeam::HIRING_PLAN_PAY_PER_POST
            expect(team.primary_subscription).to be_nil # sanity check
            expect(team.has_full_access?).to be(true)
        end
    end

    describe "cancel_pay_per_post_subscriptions_before_switching_to_unlimited" do
        it "should work" do
            hiring_team = HiringTeam.first
            subscription = Subscription.first
            open_position = OpenPosition.first
            expect(hiring_team).to receive(:pay_per_post_subscriptions).at_least(1).times.and_return([subscription])
            expect(hiring_team).to receive(:open_position_for_subscription).and_return(open_position)
            expect(subscription).to receive(:cancel_and_destroy).with(reason: HiringTeam::CANCELLATION_REASON_SWITCHING_TO_UNLIMITED, prorate: true)
            expect(subscription).to receive(:stripe_subscription).and_return(:stripe_subscription)
            hiring_team.cancel_pay_per_post_subscriptions_before_switching_to_unlimited
        end

        it "should not reset auto_expiration_date on open_position_for_subscription if already set" do
            hiring_team = HiringTeam.first
            subscription = Subscription.first
            open_position = OpenPosition.where(featured: true, archived: false).where.not(auto_expiration_date: nil).first
            original_auto_expiration_date = open_position.auto_expiration_date
            allow(hiring_team).to receive(:pay_per_post_subscriptions).at_least(1).times.and_return([subscription])
            allow(hiring_team).to receive(:open_position_for_subscription).and_return(open_position)
            allow(subscription).to receive(:cancel_and_destroy).with(reason: HiringTeam::CANCELLATION_REASON_SWITCHING_TO_UNLIMITED, prorate: true)
            allow(subscription).to receive(:stripe_subscription).and_return(:stripe_subscription)
            hiring_team.cancel_pay_per_post_subscriptions_before_switching_to_unlimited
            expect(open_position.reload.auto_expiration_date).to eq(original_auto_expiration_date)
        end

        it "should set auto_expiration_date on open_position_for_subscription if position is featured and not archived" do
            now = Time.now
            allow(Time).to receive(:now).and_return(now)
            hiring_team = HiringTeam.first
            subscription = Subscription.first
            open_position = OpenPosition.where(featured: true, archived: false).first
            open_position.update_columns(auto_expiration_date: nil)
            allow(hiring_team).to receive(:pay_per_post_subscriptions).at_least(1).times.and_return([subscription])
            allow(hiring_team).to receive(:open_position_for_subscription).and_return(open_position)
            allow(subscription).to receive(:cancel_and_destroy).with(reason: HiringTeam::CANCELLATION_REASON_SWITCHING_TO_UNLIMITED, prorate: true)
            allow(subscription).to receive(:stripe_subscription).and_return(:stripe_subscription)
            hiring_team.cancel_pay_per_post_subscriptions_before_switching_to_unlimited
            expect(open_position.reload.auto_expiration_date).to eq((now + 60.days).beginning_of_day) # nil originally and should not have changed
        end
    end
end