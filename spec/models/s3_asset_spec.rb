# == Schema Information
#
# Table name: s3_assets
#
#  id                :uuid             not null, primary key
#  created_at        :datetime
#  updated_at        :datetime
#  file_file_name    :string(255)
#  file_content_type :string(255)
#  file_file_size    :integer
#  file_updated_at   :datetime
#  directory         :string(255)
#  styles            :text
#  file_fingerprint  :string(255)
#  dimensions        :json
#  formats           :json
#  source_url        :text
#

require 'spec_helper'

describe S3Asset do

    before(:each) do

        @file_path = File.join(::Rails.root, "/spec/models", "test_image.jpg")
        file = File.new(@file_path, 'rb')
        allow(file).to receive(:content_type).and_return("image/jpg")

        @attr = {
            :file => file,
            :directory => "content_images/",
            :styles => {
                "150x150" => '150x150>',
                "565x275" => '565x275>'
            }.to_json
        }
    end

    it "should create a new instance given a valid attribute" do
        S3Asset.new(@attr)
    end

    it "should receive file_file_name from :photo" do
        asset = S3Asset.new(@attr)
        expect(asset.file_file_name).to eq("test_image.jpg")
    end

    it "should have an attachment :path of :directory/:fingerprint/:style/:fingerprint.:extension" do
        expect(S3Asset.attachment_definitions[:file][:path]).to eq(":directory/:fingerprint/:style/:fingerprint.:extension")
    end

    it "should properly handle stripping of leading and trailing slashes from directory names" do
        asset = S3Asset.new(@attr.merge(:directory => "/leading/and/trailing/slashes/"))
        expect(asset.formatted_directory).to eq("leading/and/trailing/slashes")
    end

    describe 'add_dimensions' do

        it 'should work with an svg' do
            file = {}
            expect(file).to receive(:path).and_return(@file_path)
            expect(file).to receive(:content_type).and_return("image/svg+xml")
            expect(File).to receive(:read).and_return("<svg width=\"420\" height=\"72\" xmlns=\"http://www.w3.org/2000/svg\" ></svg>")
            asset = S3Asset.new

            # add_dimensions_without_stub is the real add_dimensions.  Because rspec2 does
            # not let you stub a method and get access to the instance it's hard for us
            # to stub this method out in a reasonable way, so we override it completely
            # in spec_helper.rb.  See https://github.com/rspec/rspec-mocks/issues/175
            asset.add_dimensions_without_stub(file)
            expect(asset.dimensions['width']).to eq(420)
            expect(asset.dimensions['height']).to eq(72)
        end

        it 'should work with a file' do
            file = {}
            expect(file).to receive(:content_type).twice.and_return("image/jpeg")
            gemoetry = Struct.new(:width, :height).new(420, 72)
            expect(Paperclip::Geometry).to receive(:from_file).with(file).and_return(gemoetry)
            asset = S3Asset.new

            # add_dimensions_without_stub is the real add_dimensions.  Because rspec2 does
            # not let you stub a method and get access to the instance it's hard for us
            # to stub this method out in a reasonable way, so we override it completely
            # in spec_helper.rb.  See https://github.com/rspec/rspec-mocks/issues/175
            asset.add_dimensions_without_stub(file)
            expect(asset.dimensions['width']).to eq(420)
            expect(asset.dimensions['height']).to eq(72)

        end

    end

    describe "styles" do

        it 'should be removed if file is an svg' do

            file = {}
            expect(Paperclip.io_adapters).to receive(:for).with(file, {:hash_digest=>Digest::SHA256}).and_return(Struct.new(:content_type).new("image/svg+xml"))
            expect_any_instance_of(S3Asset).to receive(:file=)
            asset = S3Asset.new(
                file: file,
                styles: {
                    "150x150" => '150x150>',
                    "565x275" => '565x275>'
                }.to_json
            )
            expect(asset.decoded_styles).to eq({})

        end
    end

    describe "set_formats" do

        # we just have to assume that the before_save has been
        # set up correctly becaue we can't really save these in tests
        # without hitting AWS

        it "should set formats" do
            asset = S3Asset.new(@attr)
            asset.set_formats
            fp = asset.file_fingerprint
            expect(asset.formats).to eq({
                "150x150"=>{"url"=>"https://#{ENV['AWS_UPLOADS_BUCKET']}/content_images/#{fp}/150x150/#{fp}.jpg", "width"=>150, "height"=>150},
                "565x275"=>{"url"=>"https://#{ENV['AWS_UPLOADS_BUCKET']}/content_images/#{fp}/565x275/#{fp}.jpg", "width"=>565, "height"=>275},
                "original"=>{"url"=>"https://#{ENV['AWS_UPLOADS_BUCKET']}/content_images/#{fp}/original/#{fp}.jpg"}
            })
        end

    end

end
