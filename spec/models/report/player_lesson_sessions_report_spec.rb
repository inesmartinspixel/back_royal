require 'spec_helper'
require 'report_spec_helper'

describe Report::PlayerLessonSessionsReport do

    attr_accessor :user, :lesson, :stream, :record

    include ReportSpecHelper

    fixtures(:institutions)

    before(:each) do
        @lesson = Lesson.all_published.first.published_version
        @stream = Lesson::Stream.all_published.first.published_version
        @user = User.first
        @record = create_record
    end

    it "should load some data" do
        report = get_report

        expect(report.tabular_data('en')[0]).to eq([
            record.user_id,
            user.account_id,
            lesson.title,
            stream.title,
            lesson.locale,
            record.started_at.to_timestamp,
            record.last_lesson_activity_time.to_timestamp,
            record.total_lesson_time,
            record.started_in_this_session,
            record.completed_in_this_session,
            user.created_at.to_timestamp
        ])

    end

    describe "internal_get_filter_options" do

        it "should load course options" do
            filter_options = Report::PlayerLessonSessionsReport.internal_get_filter_options('en')

            course_entry = filter_options.detect { |opt| opt[:filter_type] == 'CourseFilter' }

            expect(course_entry).not_to be_nil

            stream = Lesson::Stream.all_published.where(locale: 'en').first
            entry_for_stream = course_entry[:options].detect { |opt| opt[:value] == stream.locale_pack_id }
            expect(entry_for_stream).not_to be_nil
            expect(entry_for_stream[:text]).to eq(stream.published_version.title)
        end

        it "should load lesson options" do
            filter_options = Report::PlayerLessonSessionsReport.internal_get_filter_options('en')

            lesson_entry = filter_options.detect { |opt| opt[:filter_type] == 'LessonFilter' }

            expect(lesson_entry).to be_nil # removing for now because it's ugly and brings up some issues
            # see https://trello.com/c/kQp6zGuR/634-feat-support-lessons-filter-for-internal-users
            # expect(lesson_entry).not_to be_nil

            # lesson = Lesson.all_published.where(locale: 'en').first
            # entry_for_lesson = lesson_entry[:options].detect { |opt| opt[:value] == lesson.locale_pack_id }
            # expect(entry_for_lesson).not_to be_nil
            # expect(entry_for_lesson[:text]).to eq(lesson.published_version.title)
        end

        it "should load locale options" do
            filter_options = Report::PlayerLessonSessionsReport.internal_get_filter_options('en')

            locale_entry = filter_options.detect { |opt| opt[:filter_type] == 'LessonLocaleFilter' }

            expect(locale_entry).not_to be_nil

            expect(locale_entry[:options].detect { |e| e[:value] == 'es' && e[:text] == 'Spanish'}).not_to be_nil
        end

        it "should load client_type options" do
            filter_options = Report::PlayerLessonSessionsReport.internal_get_filter_options('en')

            client_type_entry = filter_options.detect { |opt| opt[:filter_type] == 'ClientTypeFilter' }

            expect(client_type_entry).not_to be_nil

            expect(client_type_entry[:options]).to eq([
                {:text=>"All", :value=>"all"},
                 {:text=>"Desktop", :value=>"desktop"},
                 {:text=>"Mobile App", :value=>"mobile_app"},
                 {:text=>"Mobile Web", :value=>"mobile_web"},
                 {:text=>"Unknown", :value=>"unknown"}
            ])
        end
    end

    describe "internal_get_filter_options_for_institution" do

        it "should show only the courses and lessons for the institution" do
            institution = institutions(:institution_with_streams)
            course_in_institution = institution.lesson_streams.detect(&:has_published_version?)
            course_not_in_institution = Lesson::Stream.all_published.where.not(locale_pack_id: institution.lesson_stream_locale_packs.ids).first

            filter_options = Report::PlayerLessonSessionsReport.internal_get_filter_options_for_institution(institution, 'en')
            course_entry = filter_options.detect { |opt| opt[:filter_type] == 'CourseFilter' }
            expect(course_entry).not_to be_nil

            entry_for_stream = course_entry[:options].detect { |opt| opt[:value] == course_in_institution.locale_pack_id }
            expect(entry_for_stream).not_to be_nil

            entry_for_other_stream = course_entry[:options].detect { |opt| opt[:value] == course_not_in_institution.locale_pack_id }
            expect(entry_for_other_stream).to be_nil

        end

        it "should not show locale options" do
            institution = Institution.first
            filter_options = Report::PlayerLessonSessionsReport.internal_get_filter_options_for_institution(institution, 'en')
            locale_entry = filter_options.detect { |opt| opt[:filter_type] == 'LessonLocaleFilter' }
            expect(locale_entry).to be_nil
        end
    end

    describe "filters" do

        before(:each) do
            record = create_record
            expect(Report.create_from_hash!({
                'report_type' => 'PlayerLessonSessionsReport',
                'filters' => [],
                'fields' => ['user_id']
            }).tabular_data('en').any?).to be(true)
        end

        it "should filter by course" do
            report = Report.create_from_hash!({
                'report_type' => 'PlayerLessonSessionsReport',
                'filters' => [{
                    'filter_type' => 'CourseFilter',
                    'value' => [SecureRandom.uuid]
                }],
                'fields' => ['user_id']
            })
            expect(report.tabular_data('en')).to eq([])
        end

        it "should filter by locale" do
            report = Report.create_from_hash!({
                'report_type' => 'PlayerLessonSessionsReport',
                'filters' => [{
                    'filter_type' => 'LessonLocaleFilter',
                    'value' => ['xx']
                }],
                'fields' => ['user_id']
            })
            expect(report.tabular_data('en')).to eq([])
        end

        it "should filter by client_type" do
            report = Report.create_from_hash!({
                'report_type' => 'PlayerLessonSessionsReport',
                'filters' => [{
                    'filter_type' => 'ClientTypeFilter',
                    'value' => ['mobile_web']
                }],
                'fields' => ['user_id']
            })
            expect(report.tabular_data('en').first).to be_nil

            report = Report.create_from_hash!({
                'report_type' => 'PlayerLessonSessionsReport',
                'filters' => [{
                    'filter_type' => 'ClientTypeFilter',
                    'value' => ['mobile_app']
                }],
                'fields' => ['user_id']
            })
            expect(report.tabular_data('en').first).not_to be_nil
        end

        it "should filter by time" do
            report = Report.create_from_hash!({
                'report_type' => 'PlayerLessonSessionsReport',
                'filters' => [],
                'date_range' => {
                    'type' => 'between',
                    'start_day' => Time.at(0).strftime('%Y-%m-%d'),
                    'finish_day' => Time.at(0).strftime('%Y-%m-%d')
                },
                'fields' => ['user_id', 'registered_at']
            })
            expect(report.tabular_data('en').first).to be_nil
        end

    end

    def create_record
        record = Report::PlayerLessonSession.create!({
            user_id: user.id,
            session_id: SecureRandom.uuid,
            lesson_locale: lesson.locale,
            lesson_id:lesson.attributes['id'],
            lesson_locale_pack_id:lesson.locale_pack_id,
            lesson_stream_id: stream.attributes['id'],
            lesson_stream_locale_pack_id:stream.locale_pack_id,
            started_at: Time.now - 2.minutes,
            last_lesson_activity_time: Time.now - 1.minutes,
            total_lesson_time: 41,
            started_in_this_session: true,
            completed_in_this_session: true,
            client_type: 'mobile_app'
        })
     end

    def get_report(config = {})
        Report.create_from_hash!({
            'report_type' => 'PlayerLessonSessionsReport',
            'filters' => [],
            'fields' => [
                'user_id',
                'account_id',
                'lesson_title',
                'lesson_stream_title',
                'lesson_locale',
                'started_at',
                'last_lesson_activity_time',
                'total_lesson_time',
                'started_in_this_session',
                'completed_in_this_session',
                'registered_at'
            ]
        }.merge(config))
    end

end