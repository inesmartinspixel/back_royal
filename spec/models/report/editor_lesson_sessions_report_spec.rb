require 'spec_helper'
require 'report_spec_helper'

describe Report::EditorLessonSessionsReport do

    attr_accessor :user, :lesson, :stream, :record

    include ReportSpecHelper

    before(:each) do
        Report::EditorLessonSession.destroy_all
        @lesson =  Lesson.where.not(tag: nil).where(locale: 'en').first
        @user = User.first
        @record = create_record
    end

    it "should load some data" do
        report = get_report

        expect(report.tabular_data('en')[0]).to eq([
            record.user_id,
            user.account_id,
            '('+lesson.tag+') '+lesson.title,
            lesson.locale,
            record.started_at.to_timestamp,
            record.last_activity_time.to_timestamp,
            record.total_time,
            record.save_count
        ])

    end

    describe "internal_get_filter_options" do

        it "should load lesson options" do
            filter_options = Report::EditorLessonSessionsReport.internal_get_filter_options('en')

            lesson_entry = filter_options.detect { |opt| opt[:filter_type] == 'LessonFilter' }

            expect(lesson_entry).not_to be_nil

            @lesson = Lesson.where.not(tag: nil).where(locale: 'en').first
            entry_for_lesson = lesson_entry[:options].detect { |opt| opt[:value] == lesson.id }
            expect(entry_for_lesson).not_to be_nil
            expect(entry_for_lesson[:text]).to eq('('+lesson.tag+') '+lesson.title)

            @lesson = Lesson.where(locale: 'es').first
            entry_for_lesson = lesson_entry[:options].detect { |opt| opt[:value] == lesson.id }
            expect(entry_for_lesson).not_to be_nil

            # using match so we don't have to worry about the tag
            expect(entry_for_lesson[:text]).to match(lesson.title + ' (es)')
        end

        it "should load locale options" do
            filter_options = Report::EditorLessonSessionsReport.internal_get_filter_options('en')

            locale_entry = filter_options.detect { |opt| opt[:filter_type] == 'LessonLocaleFilter' }

            expect(locale_entry).not_to be_nil

            expect(locale_entry[:options].detect { |e| e[:value] == 'es' && e[:text] == 'Spanish'}).not_to be_nil
        end
    end

    describe "filters" do

        before(:each) do
            record = create_record
            expect(Report.create_from_hash!({
                'report_type' => 'EditorLessonSessionsReport',
                'filters' => [],
                'fields' => ['user_id']
            }).tabular_data('en').any?).to be(true)
        end

        it "should filter by lesson" do
            report = Report.create_from_hash!({
                'report_type' => 'EditorLessonSessionsReport',
                'filters' => [{
                    'filter_type' => 'LessonFilter',
                    'value' => [SecureRandom.uuid]
                }],
                'fields' => ['user_id']
            })
            expect(report.tabular_data('en')).to eq([])
        end

        it "should filter by locale" do
            report = Report.create_from_hash!({
                'report_type' => 'EditorLessonSessionsReport',
                'filters' => [{
                    'filter_type' => 'LessonLocaleFilter',
                    'value' => ['xx']
                }],
                'fields' => ['user_id']
            })
            expect(report.tabular_data('en')).to eq([])
        end

    end

    def create_record
        record = Report::EditorLessonSession.create!({
            user_id: user.id,
            lesson_id:lesson.attributes['id'],
            started_at: Time.now - 2.minutes,
            last_activity_time: Time.now - 1.minutes,
            total_time: 41,
            save_count: 3
        })
     end

    def get_report(config = {})
        Report.create_from_hash!({
            'report_type' => 'EditorLessonSessionsReport',
            'filters' => [],
            'fields' => [
                'user_id',
                'account_id',
                'lesson_title',
                'lesson_locale',
                'started_at',
                'last_activity_time',
                'total_time',
                'save_count'
            ]
        }.merge(config))
    end

end