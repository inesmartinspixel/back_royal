require 'spec_helper'
require 'report_spec_helper'

describe Report::UsersReport do

    include ReportSpecHelper
    fixtures(:users)

    class OldAssessmentScore < ApplicationRecord
        self.table_name = 'old_assessment_scores'
    end

    it "should load some data" do
        user = ReportSpecHelper.setup_user_for_users_report_specs

        report = get_report
        row = report.tabular_data('en').detect { |r| r[0] == user.id }

        expect(row[0]).to eq(user.id)
        expect(row[1]).to eq(user.account_id)

        # registered for cohort stuff (no accepted or pre_accepted application, so it should be nil)
        expect(user.pre_accepted_application).to be_nil
        expect(user.accepted_application).to be_nil
        expect(row[2]).to be_nil

        # see fixture_builder to see where this stuff comes from

        # lesson completion stuff (started 3, finished 1)
        expect(row[3]).to eq(3)
        expect(row[4]).to match_array(user.lesson_progresses.pluck(:locale_pack_id))
        expect(row[5]).to eq(2)
        expect(row[6]).to match_array(user.lesson_progresses.where.not(completed_at: nil).pluck(:locale_pack_id))

        # stream completion stuff (started 2, finished 1)
        expect(row[7]).to eq(2)
        expect(row[8]).to match_array(user.lesson_streams_progresses.pluck(:locale_pack_id))
        expect(row[9]).to eq(1)
        expect(row[10]).to match_array(user.lesson_streams_progresses.where.not(completed_at: nil).pluck(:locale_pack_id))

        expect(row[11]).to eq(63) # total_lesson_time
        expect(row[12]).to eq(0.76) # asessment_score_first
        expect(row[13]).to eq(0.98) # asessment_score_best

        # last_lesson_activity_time
        expect(row[14]).to eq(RedshiftEvent.where(user_id: user.id, event_type: ['lesson:frame:unload', 'lesson:finish']).pluck(:estimated_time).max.to_timestamp)

    end

    it "should set some values to 0" do
        user = User.left_outer_joins(:lesson_progresses, :lesson_streams_progresses, :cohort_applications)
            .where("lesson_progress.id is null and lesson_streams_progress.id is null and cohort_applications.id is null")
            .first
        expect(user).not_to be_nil

        report = get_report

        row = report.tabular_data('en').detect { |r| r[0] == user.id }

        expect(user.pre_accepted_application).to be_nil
        expect(user.accepted_application).to be_nil

        expect(row).to eq([
            user.id,
            user.account_id,
            nil,    # registered_for_cohort (no accepted or pre_accepted application, so this should be nil)
            0,      # started_lesson_count
            [],     # started_lesson_locale_pack_ids
            0,      # completed_lesson_count
            [],     # completed_lesson_locale_pack_ids
            0,      # started_stream_count
            [],     # started_stream_locale_pack_ids
            0,      # completed_stream_count
            [],     # completed_stream_locale_pack_ids
            0,      # total_lesson_time
            nil,    # average_assessment_score_first
            nil,    # average_assessment_score_best
            nil,    # last_lesson_activity_time
        ])

    end

    describe "filters" do

        it "should filter by courses started" do
            report = Report.create_from_hash!({
                'report_type' => 'UsersReport',
                'filters' => [{
                    'filter_type' => 'CourseStartedFilter',
                    'value' => [SecureRandom.uuid]
                }],
                'fields' => ['user_id', 'started_lesson_count']
            })
            expect(report.tabular_data('en')).to eq([])
        end

        it "should filter by courses completed" do
            report = Report.create_from_hash!({
                'report_type' => 'UsersReport',
                'filters' => [{
                    'filter_type' => 'CourseCompletedFilter',
                    'value' => [SecureRandom.uuid]
                }],
                'fields' => ['user_id', 'started_lesson_count']
            })
            expect(report.tabular_data('en')).to eq([])
        end

        it "should filter by application regstered" do
            report = Report.create_from_hash!({
                'report_type' => 'UsersReport',
                'filters' => [{
                    'filter_type' => 'ApplicationRegisteredFilter',
                    'value' => 'true'
                }],
                'fields' => ['user_id', 'started_lesson_count']
            })
            expect(report.tabular_data('en')).to eq([])
        end

        it "should filter by institution" do
            institution = Institution.joins(:users).first
            report = Report.create_from_hash!({
                'report_type' => 'UsersReport',
                'filters' => [{
                    'filter_type' => 'InstitutionFilter',
                    'value' => [institution.id]
                }],
                'fields' => ['user_id', 'started_lesson_count']
            })
            expect(report.tabular_data('en').size).to eq(institution.users.count)
        end

        it "should filter by cohort" do
            cohort = Cohort.joins(:cohort_applications, :users).first
            report = Report.create_from_hash!({
                'report_type' => 'UsersReport',
                'filters' => [{
                    'filter_type' => 'CohortFilter',
                    'value' => [cohort.id]
                }],
                'fields' => ['user_id', 'started_lesson_count']
            })
            expect(report.tabular_data('en').size).to eq(cohort.users.count)
        end

    end

    describe "internal_get_filter_options" do

        it "should work" do
            filter_options = Report::UsersReport.internal_get_filter_options('en')

            course_completed_entry = filter_options.detect { |opt| opt[:filter_type] == 'CourseCompletedFilter' }
            course_started_entry = filter_options.detect { |opt| opt[:filter_type] == 'CourseStartedFilter' }

            expect(course_completed_entry).not_to be_nil
            expect(course_started_entry).not_to be_nil

            published_streams = Lesson::Stream.all_published.where(locale: 'en').select('locale_pack_id', 'title').map(&:published_version)
            [course_completed_entry, course_started_entry].each do |entry|

                expect(entry[:options].size).to eq(published_streams.size + 1)
                expect(entry[:options].map { |o| o[:value]}).to match_array(published_streams.map(&:locale_pack_id) + [nil])
            end

            expect(course_completed_entry[:options].map { |o| o[:text]}).to match_array(published_streams.map(&:title) + ["(No Courses Completed)"])
            expect(course_started_entry[:options].map { |o| o[:text]}).to match_array(published_streams.map(&:title) + ["(No Courses Started)"])
        end
    end

    describe "tabular_data" do
        # FIXME: add helpers to report_spec_helper.rb for courses_completed, courses_started, and ApplicationRegistered, then include here to test
        # FIXME: ensure that we test the case where a column value is null, too
    end

    def get_report(config = {})
        Report.create_from_hash!({
            'report_type' => 'UsersReport',
            'filters' => [],
            'fields' => [
                'user_id',
                'account_id',
                'registered_for_cohort',
                'started_lesson_count',
                'started_lesson_locale_pack_ids',
                'completed_lesson_count',
                'completed_lesson_locale_pack_ids',

                'started_stream_count',
                'started_stream_locale_pack_ids',
                'completed_stream_count',
                'completed_stream_locale_pack_ids',

                'total_lesson_time',
                'average_assessment_score_first',
                'average_assessment_score_best',
                'last_lesson_activity_time'
            ]
        }.merge(config))
    end

end