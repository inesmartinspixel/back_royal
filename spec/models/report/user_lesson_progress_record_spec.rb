# == Schema Information
#
# Table name: user_lesson_progress_records
#
#  user_id                         :uuid             not null
#  locale_pack_id                  :uuid             not null
#  started_at                      :datetime         not null
#  completed_at                    :datetime
#  best_score_from_lesson_progress :float
#  total_lesson_time               :float
#  last_lesson_activity_time       :datetime         not null
#  total_lesson_time_on_desktop    :float
#  total_lesson_time_on_mobile_app :float
#  total_lesson_time_on_mobile_web :float
#  total_lesson_time_on_unknown    :float
#  completed_on_client_type        :text
#  last_lesson_reset_at            :datetime
#  lesson_reset_count              :integer
#  lesson_finish_count             :integer
#  average_assessment_score_first  :float
#  average_assessment_score_best   :float
#  official_test_score             :float
#

require 'spec_helper'

describe Report::UserLessonProgressRecord do
    fixtures(:users)

    include ReportSpecHelper

    describe "write_new_records" do

        it "should insert a new record" do
            user, lesson = insert_lesson_progress_and_start_event
            Report::UserLessonProgressRecord.write_new_records
            expect(get_record(user.id, lesson)).not_to be_nil
        end

        it "should do nothing when there are no changes" do
            Report::UserLessonProgressRecord.write_new_records
            expect {
                Report::UserLessonProgressRecord.write_new_records
            }.not_to change { Report::UserLessonProgressRecord.all.map(&:last_lesson_activity_time).sort }

        end

        it "should update a record when any of a few different events are logged" do
            user = ReportSpecHelper.setup_user_with_one_finish_on_an_assessment_lesson
            record = Report::UserLessonProgressRecord
                        .joins("join lessons on lessons.locale_pack_id = user_lesson_progress_records.locale_pack_id")
                        .where(user_id: user.id).first
            expect(record).not_to be_nil
            lesson = Lesson.find_by_locale_pack_id(record.locale_pack_id)
            time = Report::UserLessonProgressRecord.maximum(:last_lesson_activity_time) + 1
            ['lesson:frame:unload', 'lesson:start', 'lesson:complete', 'lesson:reset', 'lesson:finish'].each do |event_type|
                id = SecureRandom.uuid
                time = time + 1

                RedshiftEvent.create!(
                    id: id,
                    user_id: record.user_id,
                    event_type: event_type,
                    lesson_id: lesson.id,
                    estimated_time: time
                )

                Report::UserLessonProgressRecord.write_new_records
                expect(get_record(record.user_id, lesson).last_lesson_activity_time).to eq(time), "Unexpected last_lesson_activity_time after inserting #{event_type} event"
            end
        end

        it "should use created_at to set last_lesson_activity_time if the estimated_time is later" do
            user = ReportSpecHelper.setup_user_with_one_finish_on_an_assessment_lesson
            record = Report::UserLessonProgressRecord
                        .joins("join lessons on lessons.locale_pack_id = user_lesson_progress_records.locale_pack_id")
                        .where(user_id: user.id).first
            expect(record).not_to be_nil
            lesson = Lesson.find_by_locale_pack_id(record.locale_pack_id)
            time = Report::UserLessonProgressRecord.maximum(:last_lesson_activity_time) + 1
            id = SecureRandom.uuid
            time = time + 1

            RedshiftEvent.create!(
                id: id,
                user_id: record.user_id,
                event_type: 'lesson:frame:unload',
                lesson_id: lesson.id,
                estimated_time: time + 1.minute,
                created_at: time
            )

            Report::UserLessonProgressRecord.write_new_records
            expect(get_record(record.user_id, lesson).last_lesson_activity_time).to eq(time), "Unexpected last_lesson_activity_time"
        end

        # This spec tests the way we bucket the content when backfilling historic records
        it "should work when there is more than a month of records to backfill" do
            user = ReportSpecHelper.setup_user_with_one_finish_on_an_assessment_lesson
            record = Report::UserLessonProgressRecord
                        .joins("join lessons on lessons.locale_pack_id = user_lesson_progress_records.locale_pack_id")
                        .where(user_id: user.id).first
            expect(record).not_to be_nil
            lesson = Lesson.find_by_locale_pack_id(record.locale_pack_id)
            RedshiftEvent.delete_all
            Report::UserLessonProgressRecord.delete_all

            time = Time.now - 40.days

            RedshiftEvent.create!(
                id: SecureRandom.uuid,
                user_id: record.user_id,
                event_type: 'lesson:frame:unload',
                lesson_id: lesson.id,
                estimated_time: time,
                created_at: time
            )
            time += 1.month
            RedshiftEvent.create!(
                id: SecureRandom.uuid,
                user_id: record.user_id,
                event_type: 'lesson:frame:unload',
                lesson_id: lesson.id,
                estimated_time: time,
                created_at: time
            )

            # First call to select_new_records_sql will process up to 1 month after the first event.
            # The second one will process up to today
            expect(Report::UserLessonProgressRecord).to receive(:select_new_records_sql).exactly(2).times.and_call_original
            Report::UserLessonProgressRecord.write_new_records
            # not sure why just this one needs the to_i on ci.
            expect(get_record(record.user_id, lesson).last_lesson_activity_time.to_timestamp).to eq(time.to_timestamp), "Unexpected last_lesson_activity_time"
        end

        describe "fields" do

            it "should set things from lesson_progress" do
                user, lesson = insert_lesson_progress_and_start_event
                lesson_progress = LessonProgress.find_by_user_id_and_locale_pack_id(user.id, lesson.locale_pack_id)
                lesson_progress.update(
                    completed_at: Time.parse('2017/09/20 12:00:00'),
                    best_score: 0.42
                )
                Report::UserLessonProgressRecord.write_new_records
                record = get_record(user.id, lesson)
                expect(record).not_to be_nil

                expect(record.started_at).to eq(lesson_progress.started_at)
                expect(record.completed_at).to eq(lesson_progress.completed_at)
                expect(record.best_score_from_lesson_progress).to eq(lesson_progress.best_score)
            end

            it "should set time fields" do
                user, lesson = insert_lesson_progress_and_start_event

                mobile_clients = ['ios', 'android']
                mobile_web_os_names = ['Windows Phone', 'iOS', 'blackberry', 'Android', 'Tizen']
                desktop_os_names = ['Windows', 'Mac OS', 'Chromium OS', 'Ubuntu']
                unknown_os_names = ['something_else']

                mobile_clients.each do |client|
                    add_some_lesson_time(user, lesson, client, 'os_ignored')
                end


                (mobile_web_os_names + desktop_os_names + unknown_os_names).each do |os_name|
                    add_some_lesson_time(user, lesson, 'web', os_name)
                end

                Report::UserLessonProgressRecord.write_new_records
                record = get_record(user.id, lesson)
                expect(record).not_to be_nil

                expect(record.total_lesson_time_on_desktop).to eq(42*desktop_os_names.size)
                expect(record.total_lesson_time_on_mobile_app).to eq(42*mobile_clients.size)
                expect(record.total_lesson_time_on_mobile_web).to eq(42*mobile_web_os_names.size)
                expect(record.total_lesson_time_on_unknown).to eq(42*unknown_os_names.size)
                expect(record.total_lesson_time).to eq(42*(mobile_clients+mobile_web_os_names + desktop_os_names + unknown_os_names).size)

            end

            it "should add completed_on_client_type" do
                user, lesson = insert_lesson_progress_and_start_event

                RedshiftEvent.create!(
                    id: SecureRandom.uuid,
                    page_load_id: (page_load_id = SecureRandom.uuid),
                    user_id: user.id,
                    event_type: 'page_load:load',
                    estimated_time: Time.now,
                    client: 'ios'
                )

                RedshiftEvent.create!(
                    id: SecureRandom.uuid,
                    page_load_id: page_load_id,
                    user_id: user.id,
                    event_type: 'lesson:complete',
                    lesson_id: lesson.id,
                    estimated_time: Time.now
                )

                Report::UserLessonProgressRecord.write_new_records
                record = get_record(user.id, lesson)
                expect(record).not_to be_nil
                expect(record.completed_on_client_type).to eq('mobile_app')

            end

            it "should assume facebook browser is on mobile web" do
                user, lesson = insert_lesson_progress_and_start_event

                RedshiftEvent.create!(
                    id: SecureRandom.uuid,
                    page_load_id: (page_load_id = SecureRandom.uuid),
                    user_id: user.id,
                    event_type: 'page_load:load',
                    estimated_time: Time.now,
                    client: 'web',
                    browser_name: 'Facebook'
                )

                RedshiftEvent.create!(
                    id: SecureRandom.uuid,
                    page_load_id: page_load_id,
                    user_id: user.id,
                    event_type: 'lesson:complete',
                    lesson_id: lesson.id,
                    estimated_time: Time.now
                )

                Report::UserLessonProgressRecord.write_new_records
                record = get_record(user.id, lesson)
                expect(record).not_to be_nil
                expect(record.completed_on_client_type).to eq('mobile_web')

            end

            it "should add reset info" do

                user, lesson = insert_lesson_progress_and_start_event

                Report::UserLessonProgressRecord.write_new_records
                record = get_record(user.id, lesson)
                expect(record).not_to be_nil
                expect(record.last_lesson_reset_at).to be_nil
                expect(record.lesson_reset_count).to eq(0)

                RedshiftEvent.create!(
                    id: SecureRandom.uuid,
                    user_id: user.id,
                    event_type: 'lesson:reset',
                    lesson_id: lesson.id,
                    estimated_time: Time.parse('2017/06/20 12:00:01')
                )

                last_reset = RedshiftEvent.create!(
                    id: SecureRandom.uuid,
                    user_id: user.id,
                    event_type: 'lesson:reset',
                    lesson_id: lesson.id,
                    estimated_time: Time.parse('2017/06/20 12:00:10')
                )

                Report::UserLessonProgressRecord.write_new_records
                record = get_record(user.id, lesson)
                expect(record).not_to be_nil
                expect(record.last_lesson_reset_at).to eq(last_reset.estimated_time)
                expect(record.lesson_reset_count).to eq(2)
            end

            it "should add lesson_finish_count" do
                user, lesson = insert_lesson_progress_and_start_event

                RedshiftEvent.create!(
                    id: SecureRandom.uuid,
                    user_id: user.id,
                    event_type: 'lesson:finish',
                    lesson_id: lesson.id,
                    estimated_time: Time.now
                )

                RedshiftEvent.create!(
                    id: SecureRandom.uuid,
                    user_id: user.id,
                    event_type: 'lesson:finish',
                    lesson_id: lesson.id,
                    estimated_time: Time.now
                )

                Report::UserLessonProgressRecord.write_new_records
                record = get_record(user.id, lesson)
                expect(record).not_to be_nil
                expect(record.lesson_finish_count).to be(2)
            end

            describe "average_assessment_score_first" do

                it "should pull a score from old_assessment_scores" do
                    user = ReportSpecHelper.setup_user_with_old_assessment_scores
                    old_score_record = ActiveRecord::Base.connection.execute("select * from old_assessment_scores where user_id = '#{user.id}'").to_a.first
                    lesson = Lesson.all_published.where(locale_pack_id: old_score_record['locale_pack_id']).first

                    RedshiftEvent.create!(
                        id: SecureRandom.uuid,
                        user_id: user.id,
                        event_type: 'lesson:finish',
                        lesson_id: lesson.id,
                        estimated_time: Time.now,
                        score: 0.01
                    )
                    Report::UserLessonProgressRecord.write_new_records
                    record = get_record(user.id, lesson)
                    expect(record).not_to be_nil
                    expect(record.average_assessment_score_first).to be(old_score_record['average_assessment_score_first'])

                end

                it "should grab the first score from a lesson finish event" do
                    user, lesson = insert_lesson_progress_and_start_event
                    event = RedshiftEvent.create!(
                        id: SecureRandom.uuid,
                        user_id: user.id,
                        event_type: 'lesson:finish',
                        lesson_id: lesson.id,
                        estimated_time: Time.now,
                        score: 0.42
                    )
                    Report::UserLessonProgressRecord.write_new_records
                    record = get_record(user.id, lesson)
                    expect(record).not_to be_nil
                    expect(record.average_assessment_score_first).to be(event.score)

                end

            end

            describe "average_assessment_score_best" do

                it "should pull a score from old_assessment_scores" do
                    user = ReportSpecHelper.setup_user_with_old_assessment_scores
                    old_score_record = ActiveRecord::Base.connection.execute("select * from old_assessment_scores where user_id = '#{user.id}'").to_a.first
                    lesson = Lesson.all_published.where(locale_pack_id: old_score_record['locale_pack_id']).first
                    lesson_progress = user.lesson_progresses.where(locale_pack_id: lesson.locale_pack_id).first
                    lesson_progress.update(best_score: 0.01)

                    Report::UserLessonProgressRecord.write_new_records
                    record = get_record(user.id, lesson)
                    expect(record).not_to be_nil
                    expect(record.average_assessment_score_best).to be(old_score_record['average_assessment_score_last'])
                end

                it "should pull a better score from lesson_progress if there is a lower on in old_assessment_scores" do
                    user = ReportSpecHelper.setup_user_with_old_assessment_scores
                    assessment_lesson =  user.lesson_progresses.joins(:lessons).where("assessment = true").first.lessons.first
                    time = Time.parse('2017/01/01')
                    # two finishes, so that we can see the _last in the report
                    2.times do |i|
                        RedshiftEvent.create!(
                            id: SecureRandom.uuid,
                            user_id: user.id,
                            event_type: 'lesson:finish',
                            estimated_time: time + i.seconds,
                            lesson_id: assessment_lesson.id
                            # no score, because this is an old one
                        )
                    end
                    old_score_record = ActiveRecord::Base.connection.execute("select * from old_assessment_scores where user_id = '#{user.id}'").to_a.first
                    lesson = Lesson.all_published.where(locale_pack_id: old_score_record['locale_pack_id']).first
                    lesson_progress = user.lesson_progresses.where(locale_pack_id: lesson.locale_pack_id).first
                    lesson_progress.update(best_score: 0.99)

                    Report::UserLessonProgressRecord.write_new_records
                    record = get_record(user.id, lesson)
                    expect(record).not_to be_nil
                    expect(record.average_assessment_score_best).to be(lesson_progress.best_score)
                end

                it "should pull a score from lesson_progress if there is none in old_assessment_scores" do
                    user, lesson = insert_lesson_progress_and_start_event
                    lesson_progress = user.lesson_progresses.where(locale_pack_id: lesson.locale_pack_id).first
                    lesson_progress.update(best_score: 0.99)

                    Report::UserLessonProgressRecord.write_new_records
                    record = get_record(user.id, lesson)
                    expect(record).not_to be_nil
                    expect(record.average_assessment_score_best).to be(lesson_progress.best_score)
                end

            end

            describe "official_test_score" do

                it "should be the average_assessment_score_best on test lesson" do
                    user, lesson, lesson_progress = setup_best_score
                    lesson.update(test: true)
                    lesson.publish!

                    Report::UserLessonProgressRecord.write_new_records
                    record = get_record(user.id, lesson)
                    expect(record).not_to be_nil
                    expect(record.official_test_score).to be(lesson_progress.best_score)
                end

                it "should be null on non-test lessons" do
                    user, lesson, lesson_progress = setup_best_score
                    lesson.update(test: false)
                    lesson.publish!

                    Report::UserLessonProgressRecord.write_new_records
                    record = get_record(user.id, lesson)
                    expect(record).not_to be_nil
                    expect(record.official_test_score).to be_nil
                end

                def setup_best_score
                    user, lesson = insert_lesson_progress_and_start_event
                    lesson_progress = user.lesson_progresses.where(locale_pack_id: lesson.locale_pack_id).first
                    lesson_progress.update(best_score: 0.99)
                    [user, lesson, lesson_progress]
                end
            end

            def add_some_lesson_time(user, lesson, client, os_name)
                RedshiftEvent.create!(
                    id: SecureRandom.uuid,
                    page_load_id: (page_load_id = SecureRandom.uuid),
                    user_id: user.id,
                    event_type: 'page_load:load',
                    estimated_time: Time.now,
                    client: client,
                    os_name: os_name
                )

                RedshiftEvent.create!(
                    id: SecureRandom.uuid,
                    page_load_id: page_load_id,
                    user_id: user.id,
                    event_type: 'lesson:frame:unload',
                    lesson_id: lesson.id,
                    estimated_time: Time.now,
                    duration_total: 42
                )
            end

        end

        def get_record(user_id, lesson)
            Report::UserLessonProgressRecord.where(user_id: user_id, locale_pack_id: lesson.locale_pack_id).first
        end

        def insert_lesson_progress_and_start_event
            user = User.where.not(id: RedshiftEvent.distinct.pluck(:user_id)).first
            lesson = Lesson.all_published.where.not(:locale_pack_id => user.lesson_progresses.pluck(:locale_pack_id)).first

            LessonProgress.create!(
                user_id: user.id,
                locale_pack_id: lesson.locale_pack_id,
                started_at: Time.now
            )
            event = RedshiftEvent.create!(
                id: SecureRandom.uuid,
                user_id: user.id,
                event_type: 'lesson:start',
                lesson_id: lesson.id,
                estimated_time: Time.now
            )

            [user, lesson]
        end

    end

end
