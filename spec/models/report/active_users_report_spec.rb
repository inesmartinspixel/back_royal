require 'spec_helper'
require 'report_spec_helper'

describe Report::ActiveUsersReport do

    include ReportSpecHelper

    it "should load some data" do
        ReportSpecHelper.setup_user_for_users_report_specs
        record = Report::LessonActivityByCalendarDateRecord.first
        expect(record).not_to be_nil
        expected_count =  Report::LessonActivityByCalendarDateRecord.where(date: record.date).count
        day = record.date.utc

        report = get_report({
            'date_range' => {
                'type' => 'between',
                'start_day' => day.strftime('%Y-%m-%d'),
                'finish_day' => day.strftime('%Y-%m-%d')
            }
        })

        expect(report.time_series_data('en')).to eq({
            day.to_timestamp.to_s => expected_count
        })

    end

    def get_report(config = {})
        Report.create_from_hash!({
            'report_type' => 'ActiveUsersReport',
            'filters' => [],
            'group_bys' => [],
            'date_zoom_id' => 'day'
        }.merge(config))
    end

end