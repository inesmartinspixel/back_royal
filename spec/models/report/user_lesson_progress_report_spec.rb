require 'spec_helper'
require 'report_spec_helper'

describe Report::UserLessonProgressReport do
    attr_accessor :events

    include ReportSpecHelper
    fixtures(:users)

    before(:each) do


        # this probably does not need to be so specific, but it was copied and pasted from
        # fixture builder when we stopped supporting redshift events over there
        @user_with_old_assessment_scores = user = ReportSpecHelper.setup_user_with_old_assessment_scores

        assessment_lesson =  user.lesson_progresses.joins(:lessons).where("assessment = true").first.lessons.first
        time = Time.parse('2017/01/01')
        # two finishes, so that we can see the _last in the report
        2.times do |i|
            RedshiftEvent.create!(
                id: SecureRandom.uuid,
                user_id: user.id,
                event_type: 'lesson:finish',
                estimated_time: time + i.seconds,
                lesson_id: assessment_lesson.id
                # no score, because this is an old one
            )
        end

        @user_for_users_report_spec = ReportSpecHelper.setup_user_for_users_report_specs
    end

    it "should load some data" do

        user = @user_for_users_report_spec
        lesson_progress = user.lesson_progresses
                            .where.not(completed_at: nil)
                            .where(locale_pack_id: Lesson.where(assessment: true).pluck(:locale_pack_id))
                            .first

        report = get_report
        row = report.tabular_data('en').detect { |r| r[0] == user.id && r[2] == lesson_progress.locale_pack_id }

        events = RedshiftEvent.where(user_id: user.id, event_type: ['lesson:frame:unload', 'lesson:start', 'lesson:complete', 'lesson:reset', 'lesson:finish'], lesson_id: lesson_progress.lessons.pluck(:id))

        expect(row).to eq([
            user.id,
            user.account_id,
            lesson_progress.locale_pack_id,
            21, # total_lesson_time,
            0.76, # average_assessment_score_first,
            0.98, # average_assessment_score_best,
            3, # lesson_finish_count,

            # last_lesson_activity_time
            events.map(&:estimated_time).max.to_timestamp,

            lesson_progress.started_at.to_timestamp, # started_at
            lesson_progress.completed_at.to_timestamp, # completed_at
            0,
            nil
        ])

    end

    it "should not return finish count if not an assessment lesson" do
        user = @user_for_users_report_spec

        # sanity check
        user_lesson_progress_record = Report::UserLessonProgressRecord.where(
            user_id: user.id,
            locale_pack_id: Lesson.where(assessment: false).pluck(:locale_pack_id)
        ).where("lesson_finish_count > ? ", 0).first
        expect(user_lesson_progress_record).not_to be_nil
        expect(user_lesson_progress_record.average_assessment_score_first).to be_nil

        report = get_report('fields' => ['user_id', 'locale_pack_id', 'assessment_finish_count'])
        row = report.tabular_data('en').detect { |r| r[0] == user.id && r[1] == user_lesson_progress_record.locale_pack_id }

        expect(row).to eq([user.id, user_lesson_progress_record.locale_pack_id, nil])
    end

    it "should not return average_assessment_score_best if only one finish" do
        user = ReportSpecHelper.setup_user_with_one_finish_on_an_assessment_lesson
        expect(Report::UserLessonProgressRecord.where(user_id: user.id).first.average_assessment_score_best).not_to be_nil

        report = get_report('fields' => ['user_id', 'average_assessment_score_best'])
        row = report.tabular_data('en').detect { |r| r[0] == user.id }

        expect(row).to eq([user.id, nil])
    end

    it "should return values from old_assessment_scores" do
        user = @user_with_old_assessment_scores

        report = get_report('fields' => ['user_id', 'lesson_finish_count', 'average_assessment_score_first', 'average_assessment_score_best'])
        row = report.tabular_data('en').detect { |r| r[0] == user.id }

        expect(row).to eq([user.id, 3, 0.42, 0.84 ])
    end

    def get_report(config = {})
        Report.create_from_hash!({
            'report_type' => 'UserLessonProgressReport',
            'filters' => [],
            'fields' => [
                'user_id',
                'account_id',
                'locale_pack_id',
                'total_lesson_time',
                'average_assessment_score_first',
                'average_assessment_score_best',
                'assessment_finish_count',
                'last_lesson_activity_time',
                'started_at',
                'completed_at',
                'lesson_reset_count',
                'last_lesson_reset_at'
            ]
        }.merge(config))
    end

end