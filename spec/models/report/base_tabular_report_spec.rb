require 'spec_helper'
require 'report_spec_helper'

describe Report::BaseTabularReport do
    include ReportSpecHelper

    fixtures(:users)

    class Report::TabularThingsReport < Report::BaseTabularReport

        def self.field_configs
            super.merge({
                'color' => {
                    'column' => 'color'
                },
                'shape' => {
                    'column' => 'shape'
                },
                'size' => {
                    'column' => 'size',
                    'joins' => ['foo'],
                    'aggregate' => true
                }
            })
        end

        REPORT_CONFIG = {
            'klass' => ReportSpecHelper::TabularThing
        }
    end

    class Report::TabularThingsReportWithTimeRange < Report::BaseTabularReport

        def self.field_configs
            super.merge({
                'color' => {
                    'column' => 'color'
                },
                'shape' => {
                    'column' => 'shape'
                }
            })
        end

        REPORT_CONFIG = {
            'klass' => ReportSpecHelper::TabularThing,
            'time_column' => 'created_at'
        }
    end

    before(:each) do
        @default_group = AccessGroup.find_by_name("REPORTGROUP")
    end

    describe "select" do
        it "should work" do
            report = get_report({
                'fields' => ['id', 'color', 'shape']
            })

            columns, join_identifiers, group_bys = report.select([])

            expect(columns).to eq(['tabular_things.id as column_0', 'color as column_1', 'shape as column_2'])
            expect(join_identifiers).to eq([])
            expect(group_bys).to eq(['tabular_things.id', 'color', 'shape'])
        end

        it "should raise RuntimeError if no field config for field" do
            report = get_report({
                'fields' => ['id', 'color', 'shape', 'speed']
            })

            expect(report.class.field_configs['speed']).to be_nil
            expect {
                report.select([])
            }.to raise_error(RuntimeError, 'Unsupported field "speed"')
        end

        it "should add join table name to join_identifiers" do
            report = get_report({
                'fields' => ['id', 'color', 'size', 'shape']
            })

            join_identifiers = report.select([])[1]

            expect(report.class.field_configs['size']['joins'].present?).to be(true)
            expect(join_identifiers).to eq(['foo'])
        end

        it "should add column to group_bys unless field_config is for aggregate" do
            report = get_report({
                'fields' => ['id', 'color', 'size', 'shape']
            })

            group_bys = report.select([])[2]

            expect(report.class.field_configs['size']['aggregate']).to be(true)
            expect(group_bys.include?('size')).to be(false) # size is an aggregate so it should not be included
        end
    end

    describe "tabular_data" do

        describe "fields" do
            it "should respect fields" do

                first_row = get_report({
                    'fields' => ['id', 'color']
                }).tabular_data('en')[0]

                id = first_row[0]
                thing = ReportSpecHelper::TabularThing.find(id)
                expect(first_row).to match_array([
                    thing.id,
                    thing.color
                ])
            end

            it "should support account_id field" do
                email_user = User.where(provider: 'email')
                    .where.not(email: nil)
                    .where(id: ReportSpecHelper::TabularThing.pluck(:user_id)).first

                phone_user = users(:phone_no_password_user)
                id_user = users(:user_with_no_email)
                oauth_user = users(:fb_user_with_email)

                rows = get_report({
                    'fields' => ['user_id', 'account_id']
                }).tabular_data('en')

                expect(rows.detect { |r| r[0] == email_user.id}[1]).to eq(email_user.email)
                expect(rows.detect { |r| r[0] == phone_user.id}[1]).to eq(phone_user.phone)
                expect(rows.detect { |r| r[0] == id_user.id}[1]).to eq(id_user.id)
                expect(rows.detect { |r| r[0] == oauth_user.id}[1]).to eq(oauth_user.email)
            end
        end

        # FIXME: this should eventually go away.  See comment
        # on Report#filter_by_time
        it "should filter by time" do

            time = Time.parse('2016-01-01 UTC').utc.beginning_of_day
            registered_ats = relevant_users.map(&:created_at).sort
            expect(registered_ats.uniq.size).to be > 1 # sanity check
            first_day = registered_ats.first.beginning_of_day
            expected_count = registered_ats.count { |r| r.beginning_of_day == first_day }

            expect(count_results(get_report(
                'report_type' => 'TabularThingsReportWithTimeRange',
                'date_range' => {
                    'type' => 'between',
                    'start_day' => first_day.strftime('%Y-%m-%d'),
                    'finish_day' => first_day.strftime('%Y-%m-%d')
                },
                'fields' => ['account_id'] # we need account_id to perform the user joins since we dropped email
            ))).to be(expected_count)
        end

        describe_email_or_name_filter
        describe_sign_up_code_filter
        describe_institution_filter
        describe_role_filter
        describe_group_filter
        describe_cohort_status_filter

    end

    def count_results(report)
        report.tabular_data('en').size
    end

    def get_report(config = {})
        Report.create_from_hash!({
            'report_type' => 'TabularThingsReport',
            'filters' => [],
            'fields' => ['id', 'user_id', 'color', 'shape']
        }.merge(config))
    end

    def relevant_users
       User.where("users.id" => ReportSpecHelper::TabularThing.pluck('user_id'))
    end

    def create_things(user)
        ReportSpecHelper.create_tabular_things(user)
    end

end