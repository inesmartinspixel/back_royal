# == Schema Information
#
# Table name: player_lesson_sessions
#
#  id                           :uuid             not null, primary key
#  session_id                   :uuid             not null
#  user_id                      :uuid             not null
#  lesson_id                    :uuid             not null
#  lesson_stream_id             :uuid
#  started_at                   :datetime         not null
#  last_lesson_activity_time    :datetime         not null
#  total_lesson_time            :float            not null
#  started_in_this_session      :boolean          not null
#  completed_in_this_session    :boolean          not null
#  lesson_locale_pack_id        :uuid             not null
#  lesson_stream_locale_pack_id :uuid
#  lesson_locale                :text             not null
#  client_type                  :text             not null
#

require 'spec_helper'
require 'report_spec_helper'

describe Report::PlayerLessonSessionsReport do

    include ReportSpecHelper

    describe "write_new_records" do
        attr_accessor :user, :lesson, :session_id, :lesson, :stream

        before(:each) do
            @time = Time.parse('2018/01/01')
            @user = User.where.not(id: RedshiftEvent.distinct.pluck(:user_id)).first
            @lesson = Lesson.all_published.first
            @stream = Lesson::Stream.all_published.first
            @session_id = SecureRandom.uuid
        end

        it "should create a whole session in one go" do
            create_page_load_event
            create_event('lesson:start')
            lesson_play_event = create_event('lesson:play')
            create_event('lesson:frame:unload')
            create_event('lesson:frame:unload')
            create_event('lesson:frame:unload')
            lesson_finish_event = create_event('lesson:finish')
            create_event('lesson:complete')

            Report::PlayerLessonSession.write_new_records

            record = Report::PlayerLessonSession.where(user_id: user.id, lesson_id: lesson.id).first
            expect(record).not_to be_nil

            expect(record.session_id).to eq(session_id)
            expect(record.lesson_locale_pack_id).to eq(lesson.locale_pack_id)
            expect(record.lesson_stream_id).to eq(stream.id)
            expect(record.lesson_stream_locale_pack_id).to eq(stream.locale_pack_id)
            expect(record.lesson_locale).to eq(lesson.locale)
            expect(record.client_type).not_to be_nil


            expect(record.started_at).to eq(lesson_play_event.estimated_time)
            expect(record.last_lesson_activity_time).to eq(lesson_finish_event.estimated_time)
            expect(record.total_lesson_time).to eq(lesson_finish_event.estimated_time - lesson_play_event.estimated_time)

            expect(record.started_in_this_session).to be(true)
            expect(record.completed_in_this_session).to be(true)
        end

        it "should create a session in two parts" do
            create_page_load_event
            create_event('lesson:start')
            lesson_play_event = create_event('lesson:play')
            create_event('lesson:frame:unload')
            Report::PlayerLessonSession.write_new_records


            create_event('lesson:frame:unload')
            create_event('lesson:frame:unload')
            lesson_finish_event = create_event('lesson:finish')
            create_event('lesson:complete')

            Report::PlayerLessonSession.write_new_records

            records = Report::PlayerLessonSession.where(user_id: user.id, lesson_id: lesson.id)
            expect(records.size).to eq(1)
            record = records[0]

            expect(record.session_id).to eq(session_id)
            expect(record.lesson_locale_pack_id).to eq(lesson.locale_pack_id)
            expect(record.lesson_stream_id).to eq(stream.id)
            expect(record.lesson_stream_locale_pack_id).to eq(stream.locale_pack_id)
            expect(record.lesson_locale).to eq(lesson.locale)
            expect(record.client_type).to eq('mobile_app')

            expect(record.started_at).to eq(lesson_play_event.estimated_time)
            expect(record.last_lesson_activity_time).to eq(lesson_finish_event.estimated_time)
            expect(record.total_lesson_time).to eq(lesson_finish_event.estimated_time - lesson_play_event.estimated_time)

            expect(record.started_in_this_session).to be(true)
            expect(record.completed_in_this_session).to be(true)
        end

        it "should record two different sessions" do
            create_page_load_event
            create_event('lesson:start')
            session_1_first_event = create_event('lesson:play')
            session_1_last_event = create_event('lesson:frame:unload')
            Report::PlayerLessonSession.write_new_records

            @session_id = SecureRandom.uuid
            @time = @time + 20.minutes
            session_2_first_event = create_event('lesson:frame:unload')
            create_event('lesson:frame:unload')
            session_2_last_event = create_event('lesson:finish')
            create_event('lesson:complete')

            Report::PlayerLessonSession.write_new_records

            records = Report::PlayerLessonSession.where(user_id: user.id, lesson_id: lesson.id).order('started_at')
            expect(records.size).to eq(2)

            expect(records[0].started_at).to eq(session_1_first_event.estimated_time)
            expect(records[0].last_lesson_activity_time).to eq(session_1_last_event.estimated_time)
            expect(records[0].total_lesson_time).to eq(session_1_last_event.estimated_time - session_1_first_event.estimated_time)
            expect(records[0].started_in_this_session).to be(true)
            expect(records[0].completed_in_this_session).to be(false)

            expect(records[1].started_at).to eq(session_2_first_event.estimated_time)
            expect(records[1].last_lesson_activity_time).to eq(session_2_last_event.estimated_time)
            expect(records[1].total_lesson_time).to eq(session_2_last_event.estimated_time - session_2_first_event.estimated_time)

            expect(records[1].started_in_this_session).to be(false)
            expect(records[1].completed_in_this_session).to be(true)
        end

        it "should create a session with no complete event" do
            create_page_load_event
            create_event('lesson:start')
            lesson_play_event = create_event('lesson:play')
            create_event('lesson:frame:unload')
            create_event('lesson:frame:unload')
            create_event('lesson:frame:unload')
            lesson_finish_event = create_event('lesson:finish')

            Report::PlayerLessonSession.write_new_records

            record = Report::PlayerLessonSession.where(user_id: user.id, lesson_id: lesson.id).first
            expect(record).not_to be_nil

            expect(record.session_id).to eq(session_id)
            expect(record.lesson_locale_pack_id).to eq(lesson.locale_pack_id)
            expect(record.lesson_stream_id).to eq(stream.id)
            expect(record.lesson_stream_locale_pack_id).to eq(stream.locale_pack_id)
            expect(record.lesson_locale).to eq(lesson.locale)
            expect(record.client_type).not_to be_nil


            expect(record.started_at).to eq(lesson_play_event.estimated_time)
            expect(record.last_lesson_activity_time).to eq(lesson_finish_event.estimated_time)
            expect(record.total_lesson_time).to eq(lesson_finish_event.estimated_time - lesson_play_event.estimated_time)

            expect(record.started_in_this_session).to be(true)
            expect(record.completed_in_this_session).to be(false)
        end

        # see https://trello.com/c/eYxDiyUB
        it "should work when the same session id exists for 2 users" do
            create_page_load_event
            create_event('lesson:start')
            create_event('lesson:play')
            create_event('lesson:frame:unload')

            user_1 = @user
            user_2 = @user = User.where.not(id: @user.id).first
            create_page_load_event
            create_event('lesson:start')
            create_event('lesson:play')
            create_event('lesson:frame:unload')

            Report::PlayerLessonSession.write_new_records
            records = Report::PlayerLessonSession.where(user_id: [user_1.id, user_2.id], lesson_id: lesson.id).order('started_at')
            expect(records.size).to eq(2)
            expect(records[0].session_id).to eq(records[1].session_id)
        end

        # see https://trello.com/c/eYxDiyUB
        it "should work when the same session id exists for 2 streams" do
            create_page_load_event
            create_event('lesson:start')
            create_event('lesson:play')
            create_event('lesson:frame:unload')

            stream_1 = @stream
            stream_2 = @stream = Lesson::Stream.all_published.where.not(id: @stream.id).first
            create_page_load_event
            create_event('lesson:start')
            create_event('lesson:play')
            create_event('lesson:frame:unload')

            Report::PlayerLessonSession.write_new_records
            records = Report::PlayerLessonSession.where(lesson_stream_id: [stream_1.id, stream_2.id], lesson_id: lesson.id).order('started_at')
            expect(records.size).to eq(2)
            expect(records[0].session_id).to eq(records[1].session_id)

        end

        def increment_time
            @time = @time + 1.second
        end

        def create_page_load_event
            @page_load_id = SecureRandom.uuid
            event = RedshiftEvent.create!({
                id: SecureRandom.uuid,
                event_type: 'page_load:load',
                page_load_id: @page_load_id,
                user_id: @user.id,
                client: 'ios',
                estimated_time: increment_time,
                created_at: increment_time
            })
            event
        end

        def create_event(event_type)
            attrs = {
                id: SecureRandom.uuid,
                user_id: user.id,
                event_type: event_type,
                lesson_id: lesson.id,
                lesson_stream_id: stream.id,
                estimated_time: increment_time,
                created_at: increment_time,
                page_load_id: @page_load_id
            }

            # start and complete events do not have session ids
            unless ['lesson:start', 'lesson:complete'].include?('lesson:frame:unload')
                attrs[:lesson_player_session_id] = session_id
            end

            event = RedshiftEvent.create!(attrs)
            event
        end

    end


end
