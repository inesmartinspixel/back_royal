require 'spec_helper'
require 'report_spec_helper'

describe Report::BaseTimeSeriesReport do
    include ReportSpecHelper

    class Report::TimeSeriesThingsReport < Report::BaseTimeSeriesReport
        REPORT_CONFIG = {
            'klass' => ReportSpecHelper::TimeSeriesThing,
            'time_column' => 'time',
            'count_column' => 'user_id'
        }
    end

    class Report::TimeSeriesThingsSumReport < Report::BaseTimeSeriesReport
        REPORT_CONFIG = {
            'klass' => ReportSpecHelper::TimeSeriesThing,
            'time_column' => 'time',
            'sum_column' => '13'
        }
    end

    before(:each) do
        @default_group = AccessGroup.find_by_name("REPORTGROUP")
    end

    describe "time_series_data" do

        describe_email_or_name_filter
        describe_sign_up_code_filter
        describe_institution_filter
        describe_role_filter
        describe_group_filter
        describe_cohort_filter
        describe_cohort_status_filter

        describe "group_bys" do

            it "should group by sign_up_code" do
                report = get_report({
                    'group_bys' => ['sign_up_code'],
                    'filters' => [{
                        'filter_type' => 'SignUpCodeFilter',
                        'value' => ['a', 'b']
                    }]
                })

                expected_keys = []

                days_in_range.map do |day|
                    expected_keys << ['a', day.to_timestamp].to_json
                    expected_keys << ['b', day.to_timestamp].to_json
                end

                key = report.time_series_data('en').keys[0]
                expect(report.time_series_data('en').keys).to match_array(expected_keys)
                expect(report.time_series_data('en').values.uniq).to eq([1])
            end

            it "should group by role_name" do
                report = get_report({
                    'group_bys' => ['role_name']
                })

                expected_keys = []

                days_in_range.map do |day|
                    expected_keys << ['learner', day.to_timestamp].to_json
                end

                key = report.time_series_data('en').keys[0]
                expect(report.time_series_data('en').keys).to match_array(expected_keys)
                expect(report.time_series_data('en').values.uniq).to eq([relevant_users.count])
            end

            it "should group by institution_name" do
                institution = Institution.first
                user = institution.users.first
                reports_user = create_things(user)

                report = get_report({
                    'group_bys' => ['institution_name']
                })

                expected_keys = []

                days_in_range.map do |day|
                    expected_keys << ['(No Institution)', day.to_timestamp].to_json
                    expected_keys << [institution.name, day.to_timestamp].to_json
                end

                key = report.time_series_data('en').keys[0]
                expect(report.time_series_data('en').keys).to match_array(expected_keys)
                report.time_series_data('en').each do |key, value|
                    key_parts = ActiveSupport::JSON.decode(key)
                    if key_parts.first.nil? || key_parts.first == '(No Institution)'
                        # the non-institutional users have activity
                        # on every day so all of those values
                        # should be the total number of users
                        # (subtract the 1 institutional user)
                        expect(value).to eq(relevant_users.count - 1)
                    elsif key_parts.first == institution.name
                        # there is one institutional user, and he has activity
                        # on every day so all of those values
                        # should be one
                        expect(value).to eq(1)
                    else
                        raise "Unexpected key #{key_parts.inspect}"
                    end
                end

            end

            it "should group by group_name" do
                institution = Institution.first
                user = institution.users.detect { |u| u.groups.empty? }
                expect(user).not_to be_nil
                institution_group_names = institution.groups.to_a.map(&:name)
                reports_user = create_things(user)

                report = get_report({
                    'group_bys' => ['group_name'],
                    'filters' => [{
                        'filter_type' => 'GroupFilter',
                        'value' => [@default_group.name] + institution_group_names
                    }]
                })

                expected_keys = []

                days_in_range.map do |day|
                    expected_keys << [@default_group.name, day.to_timestamp].to_json
                    institution_group_names.each do |group|
                        expected_keys << [group, day.to_timestamp].to_json
                    end
                end

                key = report.time_series_data('en').keys[0]
                expect(report.time_series_data('en').keys).to match_array(expected_keys)
                report.time_series_data('en').each do |key, value|
                    key_parts = ActiveSupport::JSON.decode(key)
                    if key_parts.first == @default_group.name
                        # there are two non-institutional users, and they have activity
                        # on every day so all of those values should be two
                        expect(value).to eq(2)
                    elsif institution_group_names.include?(key_parts.first)
                        # there is one institutional user, and he has activity
                        # on every day so all of those values should be one
                        expect(value).to eq(1)
                    else
                        raise "Unexpected key #{key_parts.inspect}"
                    end
                end
            end


        end

        describe 'date_zoom' do

            it "should load up some data by day" do
                report = get_report({
                    'date_zoom_id' => 'day'
                })
                expect(report.time_series_data('en').keys).to eq(days_in_range.map(&:to_i).map(&:to_s))
                expect(report.time_series_data('en').values.uniq).to eq([relevant_users.count])
            end

            it "should load up some data by week" do
                report = get_report({
                    'date_zoom_id' => 'week'
                })
                expect(report.time_series_data('en').keys).to eq(weeks_in_range.map(&:to_i).map(&:to_s))
                expect(report.time_series_data('en').values.uniq).to eq([relevant_users.count])
            end

            it "should load up some data by month" do
                report = get_report({
                    'date_zoom_id' => 'month'
                })
                expect(report.time_series_data('en').keys).to eq(months_in_range.map(&:to_i).map(&:to_s))
                expect(report.time_series_data('en').values.uniq).to eq([relevant_users.count])
            end

        end
    end

    describe 'with a sum column' do

        it 'should sum a column with a day date_zoom' do
            report = get_report(
                'report_type' => 'TimeSeriesThingsSumReport',
            )

            # see sum_column above or where the 13 comes from
            expect(report.time_series_data('en').values[0]).to be(13*relevant_users.count)
        end

        it 'should sum a column with a week date_zoom' do
            report = get_report(
                'report_type' => 'TimeSeriesThingsSumReport',
                'date_zoom_id' => 'week',
                'date_range' => {
                    'type' => 'between',
                    'start_day' => (start_time + 1.week).beginning_of_week(:monday).strftime('%Y-%m-%d'),
                    'finish_day' => finish_time.strftime('%Y-%m-%d')
                }
            )

            # see sum_column above or where the 13 comes from
            # 7 is the number of days in a week
            expect(report.time_series_data('en').values[0]).to be(13*7*relevant_users.count)
        end

    end

    def days_in_range
        if !defined? @days_in_range
            days_in_range = []
            day = start_time
            while day <= (finish_time) do
                days_in_range << day
                day = day + 1.day
            end
            @days_in_range = days_in_range.map(&:utc)
        end
        @days_in_range
    end

    def weeks_in_range
        if !defined? @weeks_in_range
            weeks_in_range = []
            week = start_time.beginning_of_week(:monday)
            while week <= (finish_time) do
                weeks_in_range << week
                week = week + 1.week
            end
            @weeks_in_range = weeks_in_range.map(&:utc)
        end
        @weeks_in_range
    end

    def months_in_range
        if !defined? @months_in_range
            months_in_range = []
            month = start_time.beginning_of_month
            while month <= (finish_time) do
                months_in_range << month
                month = month + 1.month
            end
            @months_in_range = months_in_range.map(&:utc)
        end
        @months_in_range
    end

    def count_results(report)
        results = report.time_series_data('en').values.uniq
        if results.empty?
            return 0
        elsif results.size != 1
            raise "Expected one unique result. got #{results}"
        end
        results.first
    end

    def get_report(config = {})
        Report.create_from_hash!({
            'report_type' => 'TimeSeriesThingsReport',
            'filters' => [],
            'group_bys' => [],
            'date_zoom_id' => 'day',
            'date_range' => {
                'type' => 'between',
                'start_day' => start_time.strftime('%Y-%m-%d'),
                'finish_day' => finish_time.strftime('%Y-%m-%d')
            }
        }.merge(config))
    end

    def relevant_users
        User.where("users.id IN (?)", ReportSpecHelper::TimeSeriesThing.pluck('user_id'))
    end

    def create_things(user)
        ReportSpecHelper.create_time_series_things(user)
    end

end