# == Schema Information
#
# Table name: published_playlists
#
#  id             :uuid
#  locale_pack_id :uuid
#  title          :text
#  version_id     :uuid
#  locale         :text
#  stream_entries :json             is an Array
#  stream_count   :integer
#

require 'spec_helper'

describe PublishedPlaylist do

    fixtures(:playlists)

    describe "for_pref_locale" do

        it "should return the expected locales" do
            es_playlist = playlists(:es_item)
            en_playlist = playlists(:en_item)
            locale_pack = es_playlist.locale_pack

            # sanity checks
            expect(es_playlist.locale_pack_id).to eq(en_playlist.locale_pack_id)
            expect(es_playlist.has_published_version?).to be(true)
            expect(en_playlist.has_published_version?).to be(true)

            # English user gets English stream when both English and Spanish are available
            expect(PublishedPlaylist.for_pref_locale('en', ['id']).where(locale_pack_id: locale_pack.id).first['id']).to eq(en_playlist.id)

            # Spanish user gets Spanish stream
            expect(PublishedPlaylist.for_pref_locale('es', ['id']).where(locale_pack_id: locale_pack.id).first['id']).to eq(es_playlist.id)

            es_playlist.unpublish!
            RefreshMaterializedContentViews.refresh(true)

            # When only English is available, English user still gets English stream
            expect(PublishedPlaylist.for_pref_locale('en', ['id']).where(locale_pack_id: locale_pack.id).first['id']).to eq(en_playlist.id)

            # Since Spanish is unavailable, Spanish user gets English stream too
            expect(PublishedPlaylist.for_pref_locale('es', ['id']).where(locale_pack_id: locale_pack.id).first['id']).to eq(en_playlist.id)
        end
    end
end
