# == Schema Information
#
# Table name: entity_metadata
#
#  id             :uuid             not null, primary key
#  created_at     :datetime
#  updated_at     :datetime
#  title          :string
#  description    :string
#  canonical_url  :text
#  image_id       :uuid
#  tweet_template :text
#


require 'spec_helper'

describe EntityMetadata do
    describe "update" do

        it "should update_from_hash!" do
            entity_metadata_hash = {
                "title" => "a concept",
                "description" => "the awesomeness concept",
                "canonical_url" => "awesome.com",
                "tweet_template" => "once upon a time"
            }

            entity_metadata = EntityMetadata.create!(entity_metadata_hash)

            entity_metadata_hash = {
                "title" => "another title",
                "id" => entity_metadata.id,
                "tweet_template" => "in a land far away"
            }
            entity_metadata = EntityMetadata.update_from_hash!(entity_metadata_hash)
            expect(entity_metadata.title).to eq("another title")
            expect(entity_metadata.tweet_template).to eq("in a land far away")

        end

    end

end
