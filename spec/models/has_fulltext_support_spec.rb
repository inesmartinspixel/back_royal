require 'spec_helper'

describe HasFulltextSupport do

    class FulltextItem < ActiveRecord::Base
        include HasFulltextSupport
    end

    before(:each) do
        @fulltext_item = FulltextItem.new
    end

    describe "class methods" do
        it "should add fulltext quote sanitization support" do
            expect(FulltextItem.sanitize_fulltext_sql('things\' things aren\'t the-ing')).to eq("'things'' things aren''t the-ing'")
        end

        it "should properly format vector searches" do
            expect(FulltextItem.get_vector_search_sql('things\' things aren\'t the-ing')).to eq("'things''''&things&aren''''t&the-ing'")
        end
    end

    describe "callbacks" do
        it "should call update_fulltext after save" do
            expect(@fulltext_item).to receive(:update_fulltext)
            @fulltext_item.save
        end

        it "should call update_fulltext if !should_update_fulltext?" do
            expect(@fulltext_item).to receive(:should_update_fulltext?) { false }
            expect(@fulltext_item).not_to receive(:update_fulltext)
            @fulltext_item.save
        end


        it "should call clear_fulltext before destroy" do
            expect(@fulltext_item).to receive(:clear_fulltext)
            @fulltext_item.destroy
        end

        it "should error if update_fulltext has not been overridden" do
            expect(Proc.new {
                @fulltext_item.update_fulltext
            }).to raise_error("update_fulltext must be implemented in including class")
        end
    end

end