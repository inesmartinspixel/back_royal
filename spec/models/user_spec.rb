# == Schema Information
#
# Table name: users
#
#  id                                              :uuid             not null, primary key
#  email                                           :string(255)      default("")
#  encrypted_password                              :string(255)      default(""), not null
#  reset_password_token                            :string(255)
#  reset_password_sent_at                          :datetime
#  remember_created_at                             :datetime
#  sign_in_count                                   :integer          default(0)
#  current_sign_in_at                              :datetime
#  last_sign_in_at                                 :datetime
#  current_sign_in_ip                              :string(255)
#  last_sign_in_ip                                 :string(255)
#  created_at                                      :datetime         not null
#  updated_at                                      :datetime         not null
#  failed_attempts                                 :integer          default(0)
#  unlock_token                                    :string(255)
#  locked_at                                       :datetime
#  sign_up_code                                    :string(255)
#  reset_password_redirect_url                     :string(255)
#  provider                                        :string(255)
#  uid                                             :string(255)      default(""), not null
#  tokens                                          :json
#  has_seen_welcome                                :boolean          default(FALSE), not null
#  notify_email_newsletter                         :boolean          default(TRUE), not null
#  school                                          :text
#  provider_user_info                              :json
#  free_trial_started                              :boolean          default(FALSE), not null
#  confirmed_profile_info                          :boolean          default(TRUE), not null
#  pref_decimal_delim                              :string           default("."), not null
#  pref_locale                                     :string           default("en"), not null
#  active_playlist_locale_pack_id                  :uuid
#  avatar_url                                      :text
#  additional_details                              :json
#  mba_content_lockable                            :boolean          default(FALSE), not null
#  job_title                                       :text
#  phone                                           :text
#  country                                         :text
#  has_seen_accepted                               :boolean          default(FALSE), not null
#  name                                            :text
#  nickname                                        :text
#  phone_extension                                 :text
#  can_edit_career_profile                         :boolean          default(FALSE), not null
#  professional_organization_option_id             :uuid
#  pref_show_photos_names                          :boolean          default(TRUE), not null
#  identity_verified                               :boolean          default(FALSE), not null
#  pref_keyboard_shortcuts                         :boolean          default(FALSE), not null
#  sex                                             :text
#  ethnicity                                       :text
#  race                                            :text             default([]), not null, is an Array
#  how_did_you_hear_about_us                       :text
#  anything_else_to_tell_us                        :text
#  birthdate                                       :date
#  address_line_1                                  :text
#  address_line_2                                  :text
#  city                                            :text
#  state                                           :text
#  zip                                             :text
#  last_seen_at                                    :datetime
#  fallback_program_type                           :string
#  program_type_confirmed                          :boolean          default(FALSE), not null
#  invite_code                                     :string
#  notify_hiring_updates                           :boolean          default(TRUE), not null
#  notify_hiring_spotlights                        :boolean          default(TRUE), not null
#  notify_hiring_content                           :boolean          default(TRUE), not null
#  notify_hiring_candidate_activity                :boolean          default(TRUE), not null
#  hiring_team_id                                  :uuid
#  referred_by_id                                  :uuid
#  has_seen_featured_positions_page                :boolean          default(FALSE), not null
#  pref_one_click_reach_out                        :boolean          default(FALSE), not null
#  has_seen_careers_welcome                        :boolean          default(FALSE), not null
#  can_purchase_paid_certs                         :boolean          default(FALSE), not null
#  has_seen_hide_candidate_confirm                 :boolean          default(FALSE), not null
#  pref_positions_candidate_list                   :boolean          default(TRUE), not null
#  skip_apply                                      :boolean          default(FALSE), not null
#  has_seen_student_network                        :boolean          default(FALSE), not null
#  pref_student_network_privacy                    :text             default("full"), not null
#  notify_hiring_spotlights_business               :boolean          default(TRUE), not null
#  notify_hiring_spotlights_technical              :boolean          default(TRUE), not null
#  has_drafted_open_position                       :boolean          default(FALSE), not null
#  deactivated                                     :boolean          default(FALSE), not null
#  avatar_id                                       :uuid
#  has_seen_first_position_review_modal            :boolean          default(FALSE), not null
#  timezone                                        :text
#  pref_allow_push_notifications                   :boolean          default(TRUE), not null
#  english_language_proficiency_comments           :text
#  english_language_proficiency_documents_type     :text
#  english_language_proficiency_documents_approved :boolean          default(FALSE), not null
#  transcripts_verified                            :boolean          default(FALSE), not null
#  has_logged_in                                   :boolean          default(FALSE), not null
#  has_seen_mba_submit_popup                       :boolean          default(FALSE), not null
#  has_seen_short_answer_warning                   :boolean          default(FALSE), not null
#  recommended_positions_seen_ids                  :uuid             default([]), is an Array
#  allow_password_change                           :boolean          default(FALSE), not null
#  notify_candidate_positions                      :boolean
#  notify_candidate_positions_recommended          :text
#  has_seen_hiring_tour                            :boolean          default(FALSE), not null
#  academic_hold                                   :boolean          default(FALSE), not null
#  experiment_ids                                  :string           default([]), not null, is an Array
#  enable_front_royal_store                        :boolean          default(FALSE), not null
#  pref_sound_enabled                              :boolean          default(TRUE), not null
#  student_network_email                           :string(255)
#

require 'spec_helper'

describe User do

    attr_reader :user

    include StripeHelper

    fixtures(:users, :institutions, :cohorts, :lesson_streams, :lesson_streams_progress, :playlists)

    before(:each) do
        @attr = {
            :name => "Example",
            :email => "user@example.com",
            :password => "password",
            :password_confirmation => "password",
            :provider => "email"
        }

        @default_playlist = playlists(:published_playlist)
    end

    it "should create a new instance given a valid attribute" do
        User.create!(@attr)
    end

    it "can handle preexisting user accounts with sign up codes that dont map to institutions" do
        u = User.create!(@attr)
        # mimic user records that have sign up codes that dont map to insts
        u.update_columns(sign_up_code: "somethinginvalid#{rand}")

        expect(u.valid?).to be_truthy
    end

    it "should handle phone number sanitization on assignment" do
        expected_format = '+12025555555' #E164 formatted
        u = User.create!(@attr)
        expect(u.phone).to be_nil
        u.phone = "(202) 555-5555"
        expect(u.phone).to eq(expected_format)

        u.phone = "2025555555"
        expect(u.phone).to eq(expected_format)

        u.phone = expected_format
        expect(u.phone).to eq(expected_format)
    end

    describe "email" do

        it "should accept valid email addresses" do
            addresses = %w[user@foo.com THE_USER@foo.bar.org first.last@foo.jp]
            addresses.each do |address|
                valid_email_user = User.new(@attr.merge(:email => address))
                expect(valid_email_user).to be_valid
            end
        end

        it "should reject duplicate email addresses" do
            User.create!(@attr)
            user_with_duplicate_email = User.new(@attr)
            expect(user_with_duplicate_email).not_to be_valid
        end

        it "should reject email addresses identical up to case" do
            upcased_email = @attr[:email].upcase
            User.create!(@attr.merge(:email => upcased_email))
            user_with_duplicate_email = User.new(@attr)
            expect(user_with_duplicate_email).not_to be_valid
        end

        it "should reject address not in domain if config defines email_pattern_domain" do
            allow(AppConfig).to receive(:join_config).and_return({
                "foo": {
                    "requires_email" => true,
                    "signup_code" => "FOO",
                    "email_pattern_domain" => "foo.bar"
                }
            })
            user = User.new(@attr.merge(:email => "foo@test.com", :sign_up_code => "FOO"))
            expect(user).not_to be_valid
        end

        it "should reject invalid address if config defines email_pattern" do
            allow(AppConfig).to receive(:join_config).and_return({
                "foo": {
                    "requires_email" => true,
                    "signup_code" => "FOO",
                    "email_pattern" => /^.+@foo\.bar$/
                }
            })
            user = User.new(@attr.merge(:email => "foo@test.com", :sign_up_code => "FOO"))
            expect(user).not_to be_valid
        end

        it "should validate presence of email if not using phone_no_password" do
            user = User.new(@attr.merge(:provider => "email", :email => nil))
            expect(user).not_to be_valid
        end

        it "should not validate presence of email if using phone_no_password" do
            user = User.new(@attr.merge(:provider => "phone_no_password", :email => nil, :phone => '+12025555556', :uid => '+12025555556'))
            expect(user).to be_valid
        end

        it "validate uniquness of email" do
            users = User.limit(2)
            users[0].email = users[1].email
            users[0].save
            expect(users[0].errors.full_messages).to include("Email has already been taken")
        end
    end

    describe "phone" do
        it "should require phone if using phone_no_password" do
            phone_number = '+12025555557'
            user = User.new(@attr.merge(:provider => "phone_no_password", :email => nil, :uid => phone_number))
            expect(user).not_to be_valid
            expect(User.find_by_phone(phone_number)).to be_nil
            user = User.new(@attr.merge(:provider => "phone_no_password", :phone => phone_number, :uid => phone_number))
            expect(user).to be_valid
        end

        it "should validate uniqueness only for other phone_no_password users" do
            phone_number = '+12025555557'
            expect(User.find_by_phone(phone_number)).to be_nil
            User.create!(@attr.merge(:provider => "email", :email => 'some-email-user@example.com', :phone => phone_number, :uid => 'some-email-user@example.com'))
            user = User.create!(@attr.merge(:provider => "phone_no_password", :phone => phone_number, :uid => phone_number))
            expect(user).to be_valid
            dupe_user = User.new(@attr.merge(:provider => "phone_no_password", :phone => phone_number, :uid => phone_number))
            expect(dupe_user).not_to be_valid
        end


        it "should validate using possible, not strict mode for problematic phone numbers" do
            phone_number = '+85211111111'
            expect(Phonelib.valid?(phone_number)).to be(false)
            expect(User.find_by_phone(phone_number)).to be_nil
            User.create!(@attr.merge(:provider => "email", :email => 'some-email-user@example.com', :phone => phone_number, :uid => 'some-email-user@example.com'))
            user = User.new(@attr.merge(:provider => "phone_no_password", :phone => phone_number, :uid => phone_number))
            expect(user).to be_valid
        end
    end

    describe "timezone" do
        it "should set correctly" do
            user = User.first

            user.update!(timezone: nil)
            expect(user.reload.timezone).to be_nil

            user.update!(timezone: 'America/New_York')
            expect(user.reload.timezone).to eq('America/New_York')
        end

        it "should unset and log to Sentry if present but invalid" do
            user = User.first

            expect(Raven).to receive(:capture_message).with('Unsetting an invalid timezone', extra: {
                user_id: user.id,
                timezone: 'america/new_york'
            })

            user.update!(timezone: 'america/new_york')
            expect(user.reload.timezone).to be_nil
        end
    end

    describe "career profile" do
        it "should create a career profile if user is being flagged as can_edit but does not have one" do
            u = User.create!(@attr)
            u.can_edit_career_profile = true
            expect(u.career_profile).to be(nil)
            u.save!
            expect(u.career_profile).not_to be_nil
        end

        it "should not call ensure_career_profile if there are no substantive changes" do
            user = CareerProfile.first.user
            user.sign_in_count = user.sign_in_count + 1
            expect(user).not_to receive(:ensure_career_profile)
            user.save!
        end

        it "should create a career profile when creating a consumer user" do
            user = User.create!(@attr.merge(fallback_program_type: 'mba'))
            expect(user.career_profile).not_to be_nil
        end

        it "should require one if can_edit_career_profile" do
            user = User.where(:can_edit_career_profile => false)
                        .left_outer_joins(:career_profile)
                        .where("career_profiles.id is null")
                        .first
            allow(user).to receive(:ensure_career_profile) # make sure one is not created
            user.can_edit_career_profile = true
            user.save
            expect(user.errors.full_messages).to include("Career profile can't be blank")
        end

        it "should not check for one when saving if can_edit_career_profile is not changing" do
            user = User.where(:can_edit_career_profile => false).first
            allow(user).to receive(:update_career_profile_fulltext) # needed because we expect career_profile not to be called
            user.name = "changed"
            allow(user).to receive(:consumer_user?).and_return(false)
            expect(user).not_to receive(:career_profile)
            user.save!
        end
    end

    describe "demo user" do
        before(:each) do
            @demo_attr = @attr.merge({
                :phone => '+12025555555',
                :job_title => 'Developer',
                :professional_organization_option_id => ProfessionalOrganizationOption.first.id,
                :country => 'US',
                :sign_up_code => 'FREEDEMO'
            })
        end

        it "should create a new instance given valid data" do
            valid_user = User.new(@demo_attr)
            expect(valid_user).to be_valid
        end

        it "should reject a user with missing data" do
            invalid_user = User.new(@attr.merge({ :sign_up_code => 'FREEDEMO'}))
            expect(invalid_user).not_to be_valid
        end
    end

    describe "identify" do
        it "should make an identify call after commit" do
            u = User.create!(@attr)
            expect(u).to receive(:identify)
            u.run_callbacks(:commit)
        end

        it "should not identify on changes to special fields" do
            user = User.first

            expect(user).not_to receive(:identify)
            user.tokens = {
                'O9CXdrkEaN1_Dz32k8Ud4w' => {
                    "token"=>"$2a$10$cRMV7F0u1QwpEQ/FTcszc.ms80OeXL1n9eOlgw40Lkjfm.WCrC1kK",
                    "expiry"=>1480518600
                }
            }
            user.updated_at = Time.now
            user.save!
        end

        # see https://trello.com/c/xQ0Gv2Y8/1473-bug-user-identify-called-on-every-request
        it "should not identify if json field is nil" do
            user = User.first
            User.connection.execute("update users set provider_user_info=NULL where id='#{user.id}'")
            user = User.find(user.id)
            expect(user).not_to receive(:identify)
            user.tokens = {
                'O9CXdrkEaN1_Dz32k8Ud4w' => {
                    "token"=>"$2a$10$cRMV7F0u1QwpEQ/FTcszc.ms80OeXL1n9eOlgw40Lkjfm.WCrC1kK",
                    "expiry"=>1480518600
                }
            }
            user.updated_at = Time.now

            user.save!
        end

        it "should not identify when no changes" do
            user = User.first
            expect(user).not_to receive(:identify)
            user.save!
        end

        it "should identify on other changes" do
            user = User.first
            expect(user).to receive(:identify)
            user.name = "New name"
            user.save!
        end

        it "should pass along priority argument to identify call" do
            priority = 4
            user = User.first
            expect(IdentifyUserJob).to receive(:perform_later_with_debounce).with(user.id, priority)
            user.identify(priority)
        end

        it "should work if no priority is given" do
            user = User.first
            expect(IdentifyUserJob).to receive(:perform_later_with_debounce).with(user.id, IdentifyUserJob::DEFAULT_PRIORITY)
            user.identify # no priority given, so should use DEFAULT_PRIORITY
        end
    end

    describe "passwords" do

        before(:each) do
            @user = User.new(@attr)
        end

        it "should have a password attribute" do
            expect(@user).to respond_to(:password)
        end

        it "should have a sign_up_code attribute" do
            expect(@user).to respond_to(:sign_up_code)
        end

        it "should have a password confirmation attribute" do
            expect(@user).to respond_to(:password_confirmation)
        end
    end

    describe "password validations" do

        it "should require a password" do
            expect(User.new(@attr.merge(:password => "", :password_confirmation => ""))).
                not_to be_valid
        end

        it "should require a matching password confirmation" do
            expect(User.new(@attr.merge(:password_confirmation => "invalid"))).
                not_to be_valid
        end

        it "should reject short passwords" do
            short = "a" * 5
            hash = @attr.merge(:password => short, :password_confirmation => short)
            expect(User.new(hash)).not_to be_valid
        end

    end

    describe "password encryption" do

        before(:each) do
            @user = User.create!(@attr)
        end

        it "should have an encrypted password attribute" do
            expect(@user).to respond_to(:encrypted_password)
        end

        it "should set the encrypted password attribute" do
            expect(@user.encrypted_password).not_to be_blank
        end

    end

    describe "pref_locale" do

        it "can be chinese for super_editors" do
            u = users(:super_editor)
            u.pref_locale = 'zh'
            expect(u.valid?).to be(true), u.errors.full_messages.inspect
        end

        it "can be spanish for learners" do
            u = users(:learner)
            u.pref_locale = 'es'
            expect(u.valid?).to be(true), u.errors.full_messages.inspect
        end

        it "is set to english if invalid" do
            u = users(:learner)
            u.pref_locale = 'jk'
            u.save!
            expect(u.pref_locale).to eq('en')
        end

    end

    describe "pref_one_click_reach_out" do

        it "should default to false" do
            u = users(:hiring_manager)
            u.save!
            expect(u.pref_one_click_reach_out).to be(false)
        end

        it "can be set to true" do
            u = users(:hiring_manager)
            u.pref_one_click_reach_out = true
            u.save!
            expect(u.pref_one_click_reach_out).to be(true)
        end

    end

    describe "expire" do

        before(:each) do
            @user = User.create!(@attr)
        end

    end


    describe "favorites" do
        it "should allow clearing of favorites for a given user" do
            user = User.first

            user.favorite_lesson_stream_locale_packs = Lesson::Stream::LocalePack.limit(2)
            user.save!

            # sanity check
            user.reload
            expect(user.favorite_lesson_stream_locale_packs).not_to be_empty

            User.clear_all_favorites(user.id)

            # reload user
            user = User.find(user.id)
            expect(user.favorite_lesson_stream_locale_packs).to be_empty

        end
    end


    describe "self.as_json" do
        before(:each) do
            AccessGroup.create!(name: 'CATS')
            Role.find_or_create_by(:name => "admin")
            Role.find_or_create_by(:name => "editor")
            Role.find_or_create_by(:name => "learner")
        end

        it "serializes users in batch" do
            stream_locale_packs = Lesson::Stream::LocalePack.limit(2)
            user, user2, user3 = User.includes(:access_groups).limit(3)

            user.roles = [Role.find_by_name("admin")]
            user2.roles = []
            user3.roles = [Role.find_by_name("learner")]
            user.add_to_group("SUPERVIEWER")
            user2.add_to_group("CATS")
            user2.remove_from_group("GROUP")
            user3.remove_from_group("GROUP")

            user.favorite_lesson_stream_locale_packs = stream_locale_packs
            user2.favorite_lesson_stream_locale_packs = []
            user3.favorite_lesson_stream_locale_packs = [stream_locale_packs.last]

            serialized_user_list = User.as_json([user, user2, user3], {})
            expect(serialized_user_list.size).to eq(3)
            expect(serialized_user_list[0]["id"]).to eq(user.id)
            expect(serialized_user_list[0]["roles"][0]["name"]).to eq("admin")
            expect(serialized_user_list[0]["groups"].collect{ |g| g['name']}.sort).to match_array(user.groups.map(&:name))
            expect(serialized_user_list[0]["favorite_lesson_stream_locale_packs"]).to eq(stream_locale_packs.map {|pack| {"id" => pack.id}})

            expect(serialized_user_list[1]["id"]).to eq(user2.id)
            expect(serialized_user_list[1]["roles"]).to eq([])
            expect(serialized_user_list[1]["groups"].collect{ |g| g['name']}.sort).to match_array(user2.groups.map(&:name))
            expect(serialized_user_list[1]["favorite_lesson_stream_locale_packs"]).to eq([])

            expect(serialized_user_list[2]["id"]).to eq(user3.id)
            expect(serialized_user_list[2]["roles"][0]["name"]).to eq("learner")
            expect(serialized_user_list[2]["groups"].collect{ |g| g['name']}.sort).to match_array(user3.groups.map(&:name))
            expect(serialized_user_list[2]["favorite_lesson_stream_locale_packs"]).to eq([{"id" => stream_locale_packs[1].id}])

        end

        it "gracefully handles lessons deleted out form under roles" do
            user = User.first
            lesson = Lesson.first

            user.roles = [Role.find_by_name("editor")]
            user.add_role :lesson_editor, lesson
            lesson.destroy
            user.as_json({})
        end

        it "does not do unnecessary things if for=public" do
            assert_no_unnecessary_stuff_called do |users|
                User.as_json(users, {for: :email_list})
            end
        end

        it "does not do unnecessary things if for=email_list" do
            assert_no_unnecessary_stuff_called do |users|
                User.as_json(users, {for: :email_list})
            end
        end

        def assert_no_unnecessary_stuff_called(&block)
            users = User.limit(2)
            expect(AccessGroup).to receive(:where).exactly(0).times
            expect(Role).to receive(:as_json).exactly(0).times

            result = yield(users)

            expect(result.size).to eq(2)
            expect(result[0].key?('groups')).to be(false)
            expect(result[0].key?('available_groups')).to be(false)
            expect(result[0].key?('roles')).to be(false)
        end
    end

    describe "register_content_access" do

        before(:each) do
            @user = FactoryBot.create(:user, email: "test@example.com")
        end

        it "should correctly set up a Miya Miya user" do
            @user.sign_up_code = 'MIYAMIYA'
            @user.register_content_access
            expect(user.active_institution).to eq(Institution.miya_miya)
        end

        describe "with no sign_up_code" do

            before(:each) do
                expect(@user).to receive(:raise_on_no_sign_up_code?).and_return(false)
                expect(Raven).to receive(:capture_exception).with('User "test@example.com" created with no sign_up_code.  Defaulting to FREEMBA.')
                @user.sign_up_code = nil
            end

            it "should capture an exception, set FREEMBA, and configure institution fields to Quantic" do
                expect(@user.sign_up_code).to be_nil
                allow(@user).to receive(:add_to_group)
                @user.register_content_access
                expect(@user.sign_up_code).to eq('FREEMBA')
                expect(@user.fallback_program_type).to eq('mba')
                expect(@user.institutions).to eq([Institution.quantic])
                expect(@user.active_institution).to eq(Institution.quantic)
            end

            it "should capture an exception and lock MBA content" do
                allow(@user).to receive(:add_to_group)
                @user.register_content_access
                expect(@user.mba_content_lockable).to eq(true)
            end

            it "should capture an exception and add user to OPEN COURSES access group" do
                expect(@user).to receive(:add_to_group).with('OPEN COURSES').and_return(true)
                @user.register_content_access
            end

        end

        describe "with ADMIN code" do

            before(:each) do
                @user.sign_up_code = 'ADMIN'
                @user.register_content_access
            end

            it "should not set a group" do
                expect(@user.reload.groups.map(&:name).sort).to eq([])
            end

            it "should not lock MBA content" do
                expect(@user.mba_content_lockable).to eq(false)
            end

            it "should not configure any institution fields" do
                expect(@user.institutions).to be_empty
                expect(@user.active_institution).to be_nil
            end
        end

        describe "with sign_up_code" do

            it "should add group" do
                @user.sign_up_code = 'GROUP'
                @user.register_content_access
                expect(@user.reload.groups.map(&:name).sort).to eq(["GROUP"])
            end

            it "should send a message to GetSentry and default to FREEMBA if no group" do
                expect(Raven).to receive(:capture_message)

                @user.sign_up_code = 'undefined'
                expect(@user).to receive(:add_to_group).with('undefined').and_call_original # important expectation for this test
                allow(@user).to receive(:add_to_group).with('OPEN COURSES') # stub out this method call
                @user.register_content_access

                expect(@user.sign_up_code).to eq("FREEMBA")
                expect(@user.fallback_program_type).to eq('mba')
            end

            it "should not lock MBA content for non FREEMBA users" do
                @user.sign_up_code = AccessGroup.where.not(name: ['ADMIN', 'FREEMBA', 'HIRING']).first.name
                @user.register_content_access
                expect(@user.mba_content_lockable).to eq(false)
            end

            it "should lock MBA content for FREEMBA users" do
                @user.sign_up_code = 'FREEMBA'
                allow(@user).to receive(:add_to_group)
                @user.register_content_access
                expect(@user.mba_content_lockable).to eq(true)
            end

            it "should set just_called_register_content_access" do
                # need a group that matches the code
                AccessGroup.create!(name: 'SOMETHING_ELSE')
                @user.sign_up_code = 'SOMETHING_ELSE'
                @user.register_content_access
                expect(user.just_called_register_content_access).to be(true)
            end

            it "should add user to OPEN COURSES access group for FREEMBA users" do
                @user.sign_up_code = 'FREEMBA'
                expect(@user).to receive(:add_to_group).with('OPEN COURSES').and_return(true)
                @user.register_content_access
            end
        end

        describe "with institutional sign up code" do
            it "should set things up" do
                institution = Institution.first
                @user.sign_up_code = institution.sign_up_code
                expect(@user).to receive(:institutional_sign_up_codes).and_return([institution.sign_up_code])
                @user.register_content_access
                expect(@user.institutions).to eq([institution])
                expect(@user.active_institution).to eq(institution)
                expect(@user.fallback_program_type).to eq('demo')
            end
        end

        # I think this really just tests that a user gets a group named after the sign_up_code
        describe('with TRANSFORMAFRICA code') do

            before(:each) do
                @user.sign_up_code = 'TRANSFORMAFRICA'
                @locale_packs = AccessGroup.find_by_name('TRANSFORMAFRICA').lesson_stream_locale_packs
                expect(@locale_packs.any?).to be(true)
                @user.register_content_access
            end

            it "should favorite locale_packs from group" do
                expect(@user.favorite_lesson_stream_locale_packs).to match_array(@locale_packs)
            end
        end

        describe('with HIRING code') do

            before(:each) do
                @user.sign_up_code = 'HIRING'
                @locale_packs = AccessGroup.find_by_name('HIRING').lesson_stream_locale_packs
                expect(@locale_packs.any?).to be(true)
                @user.register_content_access
            end

            it "should not configure any institution fields" do
                expect(@user.institutions).to be_empty
                expect(@user.active_institution).to be_nil
            end

            it "should favorite locale_packs from group" do
                expect(@user.favorite_lesson_stream_locale_packs).to match_array(@locale_packs)
            end

            it "should set demo program_type" do
                expect(@user.fallback_program_type).to eq('demo')
            end
        end

        describe('with BOSHIGH code') do

            before(:each) do
                @user.sign_up_code = 'BOSHIGH'
                @locale_packs = AccessGroup.find_by_name('BLUEOCEAN').lesson_stream_locale_packs
                expect(@locale_packs.any?).to be(true)
                @institution = institutions(:boshigh_institution)
                @institution_pack_ids = @institution.lesson_stream_locale_packs.map {|pack| {"id" => pack.id} }
            end

            it "should add institution to user" do
                expect(@institution.lesson_streams).not_to be_empty
                @user.register_content_access
                expect(@user.sign_up_code).to eq(@institution.sign_up_code)
                expect(@user.institutions).to eq([@institution])
                expect(@user.favorite_lesson_stream_locale_packs.map {|pack| {"id" => pack.id} }.uniq).to match_array(@institution_pack_ids.uniq)

            end
        end

        describe('with FREEMBA code') do

            before(:each) do
                @user.sign_up_code = 'FREEMBA'
                allow(@user).to receive(:relevant_cohort).and_return(cohorts(:published_mba_cohort))
                expect(@user).to receive(:add_to_group).with('OPEN COURSES').and_return(true)
                @user.register_content_access
            end

            it "should configure institution fields to Quantic" do
                expect(@user.institutions).to eq([Institution.quantic])
                expect(@user.active_institution).to eq(Institution.quantic)
            end

            it "should set program_type to mba" do
                expect(@user.fallback_program_type).to eq('mba')
            end

            it "should set a default active_playlist_locale_pack_id to the promoted cohort's first required playlist pack id" do
                expect(@user.active_playlist_locale_pack_id).not_to be_nil # sanity check
                expect(@user.active_playlist_locale_pack_id).to eq(
                    @user.relevant_cohort.required_playlist_pack_ids.first
                )
            end

            # custom_omniauth_mixin calls this on an unsaved user
            it "should work on a new, unsaved user" do
                u = User.new(sign_up_code: 'FREEMBA')
                expect(u).to receive(:add_to_group).with('OPEN COURSES').and_return(true)
                expect {
                    u.register_content_access
                }.not_to raise_error
            end
        end

        # This is a special case, where we specify -A and -B versions
        # with the intention of placing the users in different groups while keeping
        # the same institution, in this case the institution with sign_up_code EMUMATH.
        # See https://trello.com/c/wKXMSdct
        describe('with EMUMATH-A and EMUMATH-B sign up code') do
            before(:each) do
                Institution.create!({
                    sign_up_code: 'EMUMATH'
                })
                allow(AppConfig).to receive(:playlist_locale_pack_id).and_return(@default_playlist.id)
            end

            it "should add appropriate group for EMUMATH-A" do
                @user.sign_up_code = 'EMUMATH-A'
                @user.register_content_access
                expect(@user.access_groups).to eq(AccessGroup.where(name: 'EMUMATH_BLENDED'))
                expect(@user.sign_up_code).to eq('EMUMATH')
            end

            it "should add appropriate group for EMUMATH-B" do
                @user.sign_up_code = 'EMUMATH-B'
                @user.register_content_access
                expect(@user.access_groups).to eq(AccessGroup.where(name: 'EMUMATH_ALL_SMARTLY'))
                expect(@user.sign_up_code).to eq('EMUMATH')
            end
        end

        describe("with EMIRATESDB sign_up_code") do
            attr_reader :playlist

            before(:each) do
                @playlist = Playlist.joins(:content_publishers).where.not(id: @default_playlist_id).first
                access_group = AccessGroup.create!(name: 'EMIRATESDB')
                institution = Institution.create!({
                    sign_up_code: 'EMIRATESDB'
                })
                institution = Institution.update_from_hash!(id: institution.id, updated_at: institution.updated_at, playlist_pack_ids: [@playlist.locale_pack_id], access_groups: [access_group.id])
                institution.save!

                allow(AppConfig).to receive(:all).and_return(config)
            end

            it "should set up the user" do
                @user.sign_up_code = 'EMIRATESDB'
                @user.register_content_access
                expect(@user.access_groups).to eq([])
                expect(@user.sign_up_code).to eq('EMIRATESDB')
                expect(@user.active_playlist_locale_pack_id).to eq(@playlist.locale_pack_id)
            end


        end

        describe('with saml auth') do

            before(:each) do
                @institution = institutions(:institution_with_streams)
                OMNIAUTH_SAML_PROVIDERS <<  @institution.sign_up_code
                @user.provider = @institution.sign_up_code
            end

            it 'should add institution to user' do
                expect(@institution.lesson_streams).not_to be_empty
                @user.register_content_access
                expect(@user.sign_up_code).to eq(@institution.sign_up_code)
                expect(@user.institutions).to eq([@institution])
                expect(@user.active_institution).to eq(@institution)
                expect(@user.favorite_lesson_stream_locale_packs.map {|pack| {"id" => pack.id} }).to match_array(@institution.lesson_streams.map {|s| {"id" => s.locale_pack_id}}.uniq)
            end

            it 'should raise if no institution' do
                @institution.users = []
                @institution.save!
                @institution.destroy
                expect(Proc.new {
                    @user.register_content_access
                }).to raise_error(RuntimeError, "No institution exists with the sign_up_code #{@user.provider.inspect}")
            end
        end
    end

    describe "as_json" do

        describe "for public" do

            it "should just have a name and id" do
                @user = User.first
                expect(@user.as_json(for: :public)).to eq({
                    'name' => @user.name,
                    'id' => @user.id
                })
            end

        end

        describe "for roles" do
            before :each do
                Role.find_or_create_by(:name => "admin")
                Role.find_or_create_by(:name => "learner")
            end

            it "should serialize dates used on the client" do
                @user = User.first
                @user.roles = [Role.find_by_name("admin")]

                json = user.as_json
                expect(json["last_seen_at"]).to be(user.last_seen_at.to_timestamp)
                expect(json["updated_at"]).to be(user.updated_at.to_timestamp)
            end

            it "should serialize roles, groups, and available groups" do
                @user = User.first
                @user.roles = [Role.find_by_name("admin")]

                serialized_user = @user.as_json({})
                expect(serialized_user.keys.include?("roles")).to be_truthy
                expect(serialized_user["roles"][0]["name"]).to eq("admin")
                expect(serialized_user["groups"].collect{ |g| g['name']}).to match_array(@user.groups.map(&:name))
            end

            it "should serialize roles, groups, and available groups into user when passed cached" do
                @user = User.first
                @user.roles = [Role.find_by_name("admin")]

                roles = Role.joins(:users).where(:users => {:id => @user.id}).first.as_json({})

                cached_roles_map = {@user.id => [roles]}
                cached_available_group_names = AccessGroup.pluck('name').sort

                serialized_user = @user.as_json({}, cached_roles_map, cached_available_group_names)
                expect(serialized_user.keys.include?("roles")).to be_truthy
                expect(serialized_user["roles"][0]["name"]).to eq("admin")
                expect(serialized_user["groups"].collect{ |g| g['name']}).to match_array(@user.group_names)
            end

        end

        it "should have instititions" do
            user = User.joins(:institutions).first
            expect(user.as_json['institutions'].any?).to be(true)
            expect(user.as_json['institutions']).to eq(user.institutions.as_json)
        end

        it "should sort cohort applications by applied at" do
            user = User.joins("LEFT JOIN cohort_applications ON cohort_applications.user_id = users.id")
                        .where("cohort_applications.id is null").first

            # Add the ones already there by fixture builder
            applications = user.cohort_applications

            # Create a few more
            allow_any_instance_of(CohortApplication).to receive(:add_user_to_access_group)
            cohort_ids = Cohort.select(:id).joins(:published_versions).order(:id).uniq.map{|cohort| "'#{cohort.id}'"}
            cohorts = Cohort.where("id IN (#{cohort_ids.join(',')})")

            cohort_application1 = CohortApplication.create!({:cohort_id => cohorts[0].id, :applied_at => Time.new(2016, 4, 1), :user_id => user.id, :status => 'rejected'})
            cohort_application2 = CohortApplication.create!({:cohort_id => cohorts[1].id, :applied_at => Time.new(2016, 4, 10), :user_id => user.id, :status => 'rejected' })
            cohort_application3 = CohortApplication.create!({:cohort_id => cohorts[2].id, :applied_at => Time.new(2016, 4, 5), :user_id => user.id, :status => 'rejected'})

            applications.push(cohort_application1)
            applications.push(cohort_application2)
            applications.push(cohort_application3)

            user.reload

            expect(user.cohort_applications.map(&:applied_at)).to eq(applications.map(&:applied_at).sort.reverse)
        end

        it "should handle an empty id string" do
            user = User.new
            user.id = ""
            user.as_json # just testing that this calls with no exception
        end

        it "should handle a nil id string" do
            user = User.new
            user.id = nil
            user.as_json # just testing that this calls with no exception
        end

        it "should attach user-restricted fields to career_profile" do
            user = users(:user_with_career_profile)
            expect(user.career_profile).to receive(:as_json).with({view: 'editable'})
            user.as_json
        end

        it "should have user_id_verifications" do
            user = User.first
            cohort = Cohort.where(program_type: 'emba').first

            UserIdVerification.create!({
                cohort_id: cohort.id,
                user_id: user.id,
                id_verification_period_index: 0,
                verified_at: DateTime.parse('2018-09-18'),
                verification_method: 'verified_by_admin',
                verifier_id: SecureRandom.uuid,
                verifier_name: 'Mr. Foo'
            })

            new_verification = UserIdVerification.create!({
                cohort_id: cohort.id,
                user_id: user.id,
                id_verification_period_index: 1,
                verified_at: DateTime.parse('2018-09-20'),
                verification_method: 'verified_by_admin',
                verifier_id: SecureRandom.uuid,
                verifier_name: 'Mr. Foo'
            })
            user.reload
            expect(user.as_json['user_id_verifications'].any?).to be(true)
            expect(user.as_json['user_id_verifications']).to match_array(user.user_id_verifications.as_json)
        end

        describe "referred_by" do
            it "should send a subset of fields for referred_by" do
                users = User.limit(2)
                user = users[0]
                user.referred_by = users[1]
                user.save!
                expect(user.as_json["referred_by"].keys.size).to be(3)
                expect(user.as_json["referred_by"]["id"]).to eq(user.referred_by.id)
                expect(user.as_json["referred_by"]["name"]).to eq(user.referred_by.name)
                expect(user.as_json["referred_by"]["email"]).to eq(user.referred_by.email)
            end

            it "should handle no referred_by" do
                user = User.where(referred_by_id: nil).first
                expect(user.as_json.key?("referred_by")).to be(false)
            end
        end

        describe "is_institutional_reports_viewer" do
            before(:each) do
                # See note in code about checking external = true
                @user = User.joins(:institutions).where("institutions.external = true AND users.id not in (#{User.joins(:views_reports_for_institutions).select(:id).to_sql})").first
                @institution = @user.institutions.first
            end

            it "should be true if user has access to reports for some institution" do
                @institution.reports_viewers << @user
                expect(@user.as_json['is_institutional_reports_viewer']).to be(true)
            end

            it "should be false if user is in an institution but does not have access" do
                @institution.reports_viewers = []
                expect(@user.as_json['is_institutional_reports_viewer']).to be(false)
            end

            it "should not make a query if user is not in any external institution" do
                user.institutions.update_all(external: false)
                expect(user).not_to receive(:views_reports_for_institutions)
                expect(user.as_json['is_institutional_reports_viewer']).to be(false)
            end
        end

    end

    describe "missing_verification_documents?" do
        it "should be true if missing identification document" do
            user = users(:accepted_mba_cohort_user)
            allow(user).to receive(:missing_identification_document?).and_return(true)
            allow(user).to receive(:missing_transcripts?).and_return(false)
            expect(user.missing_verification_documents?).to be(true)
        end

        it "should be true if missing transcripts" do
            user = users(:accepted_mba_cohort_user)
            allow(user).to receive(:missing_identification_document?).and_return(false)
            allow(user).to receive(:missing_transcripts?).and_return(true)
            expect(user.missing_verification_documents?).to be(true)
        end

        it "should be false if not missing verification documents" do
            user = users(:accepted_mba_cohort_user)
            allow(user).to receive(:missing_identification_document?).and_return(false)
            allow(user).to receive(:missing_transcripts?).and_return(false)
            expect(user.missing_verification_documents?).to be(false)
        end
    end

    describe "missing_identification_document?" do
        attr_accessor :user

        before(:each) do
            @user = users(:accepted_mba_cohort_user)
        end

        it "should return false if no last_application" do
            allow(user).to receive(:last_application).and_return(nil)
            expect(user.missing_identification_document?).to be(false)
        end

        describe "when last_application" do
            attr_accessor :last_application

            before(:each) do
                @last_application = user.last_application
                allow(user).to receive(:last_application).and_return(@last_application)
                allow(@last_application).to receive(:indicates_user_should_upload_identification_document?).and_return(true)
            end

            it "should return false if !last_application.indicates_user_should_upload_identification_document?" do
                allow(last_application).to receive(:indicates_user_should_upload_identification_document?).and_return(false)
                expect(user.missing_identification_document?).to be(false)
            end

            describe "when last_application.indicates_user_should_upload_identification_document?" do

                before(:each) do
                    allow(last_application).to receive(:indicates_user_should_upload_identification_document?).and_return(true)
                end

                it "should return false if identity_verified" do
                    allow(user).to receive(:identity_verified).and_return(true)
                    expect(user.missing_identification_document?).to be(false)
                end

                describe "when !identity_verified" do

                    before(:each) do
                        allow(user).to receive(:identity_verified).and_return(false)
                    end

                    it "should return false if s3_identification_asset is present" do
                        allow(user).to receive(:s3_identification_asset).and_return({})
                        expect(user.missing_identification_document?).to be(false)
                    end

                    it "should return true if s3_identification_asset is nil" do
                        allow(user).to receive(:s3_identification_asset).and_return(nil)
                        expect(user.missing_identification_document?).to be(true)
                    end
                end
            end
        end
    end

    describe "missing_transcripts?" do
        attr_accessor :user

        before(:each) do
            @user = users(:accepted_mba_cohort_user)
        end

        def mock_attrs(opts = {})
            last_application = opts.key?(:last_application) ? opts[:last_application] : user.last_application
            allow(user).to receive(:last_application).and_return(last_application)
            if last_application
                allow(last_application).to receive(:indicates_user_should_upload_transcripts?).and_return(opts.key?(:indicates_user_should_upload_verification_documents) ? opts[:indicates_user_should_upload_english_language_proficiency_documents] : true)
            end
            allow(user).to receive(:transcripts_verified).and_return(opts.key?(:transcripts_verified) ? opts[:transcripts_verified] : false)
            career_profile = opts.key?(:career_profile) ? opts[:career_profile] : user.career_profile
            allow(user).to receive(:career_profile).and_return(career_profile)
            if career_profile
                allow(career_profile).to receive(:indicates_user_should_upload_transcripts?).and_return(opts.key?(:indicates_user_should_upload_transcripts) ? opts[:indicates_user_should_upload_transcripts] : true)
                allow(user.career_profile).to receive(:missing_transcripts?).and_return(opts.key?(:career_profile_missing_transcripts) ? opts[:career_profile_missing_transcripts] : true)
            end
        end

        it "should return false if no last_application" do
            mock_attrs(last_application: nil)
            expect(user.missing_transcripts?).to be(false)
        end

        it "should return false if !last_application.indicates_user_should_upload_verification_documents?" do
            mock_attrs(indicates_user_should_upload_verification_documents: false)
            expect(user.missing_transcripts?).to be(false)
        end

        it "should return false if transcripts_verified" do
            mock_attrs(transcripts_verified: true)
            expect(user.missing_transcripts?).to be(false)
        end

        it "should return false if no career_profile" do
            mock_attrs(career_profile: nil)
            expect(user.missing_transcripts?).to be(false)
        end

        it "should return false if !career_profile.indicates_user_should_upload_transcripts?" do
            mock_attrs(indicates_user_should_upload_transcripts: false)
            expect(user.missing_transcripts?).to be(false)
        end

        it "should return false if career_profile is not missing_transcripts?" do
            mock_attrs(career_profile_missing_transcripts: false)
            expect(user.missing_transcripts?).to be(false)
        end

        it "should return true if records indicate user should upload docs, !transcripts_verified, and career_profile is missing_transcripts?" do
            mock_attrs
            expect(user.missing_transcripts?).to be(true)
        end
    end

    describe "missing_english_proficiency_documents?" do
        attr_accessor :user

        before(:each) do
            @user = users(:user_with_career_profile)
        end

        def mock_attrs(opts = {})
            allow(user).to receive(:english_language_proficiency_documents_approved?).and_return(opts.key?(:english_language_proficiency_documents_approved) ? opts[:english_language_proficiency_documents_approved] : false)
            career_profile = opts.key?(:career_profile) ? opts[:career_profile] : user.career_profile
            allow(user).to receive(:career_profile).and_return(career_profile)
            if career_profile
                allow(career_profile).to receive(:indicates_user_should_upload_english_language_proficiency_documents?).and_return(opts.key?(:career_profile_indicates_user_should_upload_english_language_proficiency_documents) ? opts[:career_profile_indicates_user_should_upload_english_language_proficiency_documents] : true)
            end
            last_application = opts.key?(:last_application) ? opts[:last_application] : user.last_application
            allow(user).to receive(:last_application).and_return(last_application)
            if last_application
                allow(last_application).to receive(:indicates_user_should_upload_english_language_proficiency_documents?).and_return(opts.key?(:last_application_indicates_user_should_upload_english_language_proficiency_documents) ? opts[:last_application_indicates_user_should_upload_english_language_proficiency_documents] : true)
            end
            allow(user.s3_english_language_proficiency_documents).to receive(:empty?).and_return(opts.key?(:s3_english_language_proficiency_documents_empty) ? opts[:s3_english_language_proficiency_documents_empty] : true)
        end

        it "should be false if english_language_proficiency_documents_approved?" do
            mock_attrs({
                english_language_proficiency_documents_approved: true
            })
            expect(user.missing_english_proficiency_documents?).to be(false)
        end

        it "should be false if no career_profile" do
            mock_attrs({
                career_profile: nil
            })
            expect(user.missing_english_proficiency_documents?).to be(false)
        end

        it "should be false if !career_profile.indicates_user_should_upload_english_language_proficiency_documents?" do
            mock_attrs({
                career_profile_indicates_user_should_upload_english_language_proficiency_documents: false
            })
            expect(user.missing_english_proficiency_documents?).to be(false)
        end

        it "should be false if no last_application" do
            mock_attrs({
                last_application: nil
            })
            expect(user.missing_english_proficiency_documents?).to be(false)
        end

        it "should be false if !last_application.indicates_user_should_upload_english_language_proficiency_documents?" do
            mock_attrs({
                last_application_indicates_user_should_upload_english_language_proficiency_documents: false
            })
            expect(user.missing_english_proficiency_documents?).to be(false)
        end

        it "should be false if !s3_english_language_proficiency_documents.empty?" do
            mock_attrs({
                s3_english_language_proficiency_documents_empty: false
            })
            expect(user.missing_english_proficiency_documents?).to be(false)
        end

        it "should be true if ProgramTypeConfig requires_english_language_proficiency, !english_language_proficiency_documents_approved, career_profile.indicates_user_should_upload_english_language_proficiency_documents, and no s3_english_language_proficiency_documents" do
            mock_attrs
            expect(user.missing_english_proficiency_documents?).to be(true)
        end
    end

    describe "ability" do

        before(:each) do
            @sucker, @editor = User.limit(2)
            @editor.add_role(:editor)
        end

        it "should update if roles change" do
            expect(@sucker.ability.cannot?(:crud, Lesson.new)).to be(true)
            expect(@sucker.ability.can?(:crud, Lesson.new)).to be(false)
            @sucker.add_role(:editor)
            expect(@sucker.ability.cannot?(:crud, Lesson.new)).to be(false)
            expect(@sucker.ability.can?(:crud, Lesson.new)).to be(true)
        end


    end

    describe "editable_resources"do
        it "can list editable resources" do
            @sucker, @editor = User.limit(2)
            @editor.add_role(:editor)

            lessons = Lesson.limit(3)
            @editor.add_role :lesson_editor, lessons[0]
            @editor.add_role :previewer, lessons[1]

            editables = @editor.editable_resources("Lesson")
            expect(editables.size).to eq(2)
            expect(editables[0].resource_id).to eq(lessons[0].id)
            expect(editables[1].resource_id).to eq(lessons[1].id)
        end
    end

    describe "email validation" do

        it "should be possible to create 2 users with no email" do
            users = User.limit(2)
            users[0].update(email: nil)
            users[1].update(email: nil)
        end

        it "should be impossible to create 2 users with the same email" do
            users = User.where("email IS NOT NULL").limit(2)
            expect(Proc.new {
                users[1].email = users[0].email
                users[1].save!
            }).to raise_error(ActiveRecord::RecordInvalid)
        end
    end

    describe "notify_candidate_positions preferences validations" do
        it "should be invalid when improper notify_candidate_positions_recommended value" do
            user = users(:learner)
            user.update_column(:notify_candidate_positions_recommended, nil)
            expect(user.valid?).to be(true)
            user.update_column(:notify_candidate_positions_recommended, 'daily')
            expect(user.valid?).to be(true)
            user.update_column(:notify_candidate_positions_recommended, 'bi_weekly')
            expect(user.valid?).to be(true)
            user.update_column(:notify_candidate_positions_recommended, 'never')
            expect(user.valid?).to be(true)
            user.update_column(:notify_candidate_positions_recommended, 'bi_fortnight')
            expect(user.valid?).to be(false)
        end
    end

    describe "tell_slack_about_create" do

        before(:each) do
            @user = User.new({
                email: 'email@gmail.com',
                name: 'fred',
                provider: 'email',
                sign_up_code: 'CODE'
            })
        end

        it "should log basic info to slack" do
            expect(Raven).not_to receive(:capture_exception)
            assert_message "New user: fred / *email@gmail.com*\n    •  registered with *email and password* \n    •  signed up with code *CODE*", nil
        end

        it "should log school to slack" do
            expect(Raven).not_to receive(:capture_exception)
            user.school = 'School of Hard Knocks'
            assert_message "New user: fred / *email@gmail.com*\n    •  registered with *email and password* \n    •  signed up with code *CODE* \n    •  attends *School of Hard Knocks*", nil
        end

        it "should have special coloring for non-mainstream emails" do
            expect(Raven).not_to receive(:capture_exception)
            user.email = "email@not_gmail.com"
            assert_message "New user: fred / *email@not_gmail.com*\n    •  registered with *email and password* \n    •  signed up with code *CODE*", "F3E000"
        end

        it "should have special messaging for edu emails" do
            expect(Raven).not_to receive(:capture_exception)
            user.email = "email@prestigious.edu"
            assert_message "New user: fred / *email@prestigious.edu* 🚌\n    •  registered with *email and password* \n    •  signed up with code *CODE*", "F3E000"
        end

        it "should have special messaging for hiring manager sign-ups" do
            expect(Raven).not_to receive(:capture_exception)
            user.email = "hiring-manager@prestigious.com"
            user.sign_up_code = "HIRING"
            assert_message "New user: fred / *hiring-manager@prestigious.com* 👥\n    •  registered with *email and password* \n    •  signed up with code *HIRING*", "F3E000"
        end

        it "should have special handling for concerning emails" do
            expect(Raven).not_to receive(:capture_exception)
            user.email = "email@hired.com"
            assert_message "New user: fred / *email@hired.com*\n    •  registered with *email and password* \n    •  signed up with code *CODE*\n(cc: @channel)", "FF0000"
        end

        it "should use English when determining provider name" do
            expect(Raven).not_to receive(:capture_exception)
            expect(I18n).to receive(:provider).with("email", :en).and_call_original
            assert_message "New user: fred / *email@gmail.com*\n    •  registered with *email and password* \n    •  signed up with code *CODE*", nil
        end

        it "should catch errors" do
            err = RuntimeError.new("")
            expect(Raven).to receive(:capture_exception).with(err)
            expect(LogToSlack).to receive(:perform_later).and_raise(err)
            @user.tell_slack_about_create
        end

        def assert_message(message, color)
            expect(LogToSlack).to receive(:perform_later).with(Slack.signups_channel, nil, [{:color => color, :text => message, :mrkdwn_in=>["text"]}])
            @user.tell_slack_about_create
        end

    end


    describe "log_create_event" do

        it "should work" do
            expect_any_instance_of(Event).to receive(:log_to_external_systems).with(nil, false).at_least(1).times
            user = User.create!({
                email: 'email@example.com',
                name: 'fred',
                provider: 'email',
                sign_up_code: 'CODE',
                password: 'password',
                password_confirmation: 'password',
            })
            event = Event.where(user_id: user.id, event_type: 'user:created').first
            expect(event).not_to be_nil
            expect(event.user_id).to eq(user.id)
            expect(event.event_type).to eq('user:created')
            expect(event.payload).to eq({
                "sign_up_code"=>"CODE",
                "label"=>"CODE",
                "provider"=>"email",
                "server_event"=>true
            })
        end

    end

    describe "log_pre_application_create_event" do

        it "should happen if just called register_content_access" do
            expect_any_instance_of(User).to receive(:valid?).at_least(1).times.and_return(true)
            user = User.create!({
                email: 'email@example.com',
                name: 'fred',
                fallback_program_type: 'mba'
            })
            user.just_called_register_content_access = true

            expect_create_event_logged(user) do
                user.save!
            end
        end

        it "should happen if just called register_content_access but not on a second save" do
            expect_any_instance_of(User).to receive(:valid?).at_least(1).times.and_return(true)
            user = User.create!({
                email: 'email@example.com',
                name: 'fred',
                fallback_program_type: 'mba'
            })
            user.just_called_register_content_access = true

            expect_create_event_logged(user) do
                user.save!
            end

            # delete the last event so the next check will work...
            event = Event.where(user_id: user.id, event_type: 'pre_application:user_created').first
            event.destroy

            expect_no_create_event_logged(user) do
                user.save!
            end
        end

        it "should not happen if not a consumer user" do
            expect_any_instance_of(User).to receive(:valid?).at_least(1).times.and_return(true)
            user = User.create!({
                email: 'email@example.com',
                name: 'fred',
                fallback_program_type: 'demo'
            })
            user.just_called_register_content_access = true

            expect_no_create_event_logged(user) do
                user.save!
            end
        end

        it "should happen if the email is changing from nil to something" do
            expect_any_instance_of(User).to receive(:valid?).at_least(1).times.and_return(true)
            user = User.create!({
                email: nil,
                name: 'fred',
                fallback_program_type: 'mba'
            })

            expect_create_event_logged(user) do
                user.email = 'something@something.com'
                user.save!
            end
        end

        it "should not happen on other changes" do
            expect_any_instance_of(User).to receive(:valid?).at_least(1).times.and_return(true)
            user = User.create!({
                email: 'something@something.com',
                name: 'fred',
                fallback_program_type: 'mba'
            })

            expect_no_create_event_logged(user) do
                user.email = 'changed@something.com'
                user.save!
            end
        end

        def expect_create_event_logged(user, &block)
            expect_any_instance_of(Event).to receive(:log_to_external_systems)

            expect(AdmissionRound).to receive(:promoted_rounds_event_attributes).with(user.timezone).and_return({'round_prop' => 'YES!'})
            yield
            event = Event.where(user_id: user.id, event_type: 'pre_application:user_created').first
            expect(event).not_to be_nil
            expect(event.user_id).to eq(user.id)
            expect(event.payload['round_prop']).to eq('YES!')
        end

        def expect_no_create_event_logged(user, &block)
            yield
            event = Event.where(user_id: user.id, event_type: 'pre_application:user_created').first
            expect(event).to be_nil
        end

    end

    describe "share_career_profile" do

        it "should hiring_manager:share_career_profile event" do
            user, recipient = [User.first, User.second]
            career_profile = CareerProfile.first
            message = 'foo bar baz qux'
            mock_event = double("Event instance")
            expect(Event).to receive(:create_server_event!).with(
                anything,
                user.id,
                'hiring_manager:share_career_profile',
                {
                    recipient_id: recipient.id,
                    recipient_email: recipient.email,
                    message: message,
                    deep_link_url: career_profile.deep_link_url,
                    candidate_nickname: career_profile.user.nickname || career_profile.user.name,
                    candidate_name: career_profile.user.name,
                    candidate_avatar_url: career_profile.user.avatar_url_with_fallback,
                    candidate_position_title: career_profile.featured_work_experience&.job_title,
                    candidate_position_company_name: career_profile.featured_work_experience&.professional_organization&.text
                }
            ).and_return(mock_event)
            expect(mock_event).to receive(:log_to_external_systems)
            user.share_career_profile(career_profile, recipient, message)
        end
    end

    describe "set_provider_user_info" do

        it "should set the avatar info" do
            user = User.where(avatar_url: nil).first
            user.set_provider_user_info('google_oauth2', {
                'image' => 'http://path/to/image'
            })
            expect(user.avatar_url).to eq('http://path/to/image')
        end

        it "should not allow direct setting of provider_user_info" do
            user = User.first
            expect(Proc.new{
                user.provider_user_info = {}
            }).to raise_error(RuntimeError, "Do not set provider_user_info directly.  Use set_provider_user_info")
        end

        it "should ignore blank google image" do
            user = User.where(avatar_url: nil).first
            user.set_provider_user_info('google_oauth2', {
                'image' => 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg'
            })
            expect(user.avatar_url).to be_nil
        end

        it "should not override an avatar" do
            user = User.first
            user.avatar_url = 'http://path/to/avatar'

            expect(Proc.new {
                user.set_provider_user_info('google_oauth2', {
                    'image' => 'http://new/path/to/avatar'
                })
            }).not_to change { user.avatar_url }

        end

    end

    describe "relevant_cohort" do

        it "should return nil the user has a hiring_application" do
            user = users(:hiring_manager)
            expect(user).to receive(:hiring_application).and_return(user.hiring_application)
            expect(user.relevant_cohort).to be_nil
        end

        describe "with no applications" do

            it "should be the promoted cohort for a user with no application" do
                @user = User.joins("left join cohort_applications on users.id = user_id")
                    .where(["cohort_applications.id is null"])
                    .where(fallback_program_type: 'mba')
                    .first
                expect(Cohort).to receive(:promoted_cohort).with('mba').and_return(:promoted_cohort)
                expect(@user.relevant_cohort).to eq(:promoted_cohort)
            end

            it "should be a promoted mba cohort for a demo user" do
                @user = User.where(fallback_program_type: 'demo').first
                expect(Cohort).to receive(:promoted_cohort).with('mba').and_return(:promoted_cohort)
                expect(@user.relevant_cohort).to eq(:promoted_cohort)
            end
        end

        describe "with an application" do

            before(:each) do
                @cohort = cohorts(:published_mba_cohort)
                @published_cohort_version = @cohort.published_version
            end

            it "should be the published version of the cohort if there is one" do
                user = users(:accepted_mba_cohort_user)
                expect(user.relevant_cohort).not_to eq(@cohort)
                expect(user.relevant_cohort).to eq(@published_cohort_version)
            end

            it "should raise an error if no published version is found" do
                user = users(:accepted_mba_cohort_user)
                allow(user.application_for_relevant_cohort).to receive(:published_cohort).and_return(nil)
                expect(Proc.new {
                    user.relevant_cohort
                }).to raise_error(RuntimeError, "No published version found for relevant cohort #{user.application_for_relevant_cohort.cohort.name}")
            end

            it "should be the published version of the cohort from the application if application is pending" do
                user = users(:pending_mba_cohort_user)
                expect(user.cohort_applications.first.cohort).to eq(@cohort) # sanity check
                expect(user.relevant_cohort).to eq(@published_cohort_version)
            end

            it "should be the published version of the cohort from the application if application is accepted" do
                user = users(:accepted_mba_cohort_user)
                expect(user.cohort_applications.first.cohort).to eq(@cohort) # sanity check
                expect(user.relevant_cohort).to eq(@published_cohort_version)
            end

            it "should be the published version of the cohort from the application if application is pre_accepted" do
                user = users(:pre_accepted_mba_cohort_user)
                expect(user.cohort_applications.first.cohort).to eq(@cohort) # sanity check
                expect(user.relevant_cohort).to eq(@published_cohort_version)
            end

            it "should be the promoted cohort if application is rejected" do
                user = users(:rejected_mba_cohort_user)
                expect(Cohort).to receive(:promoted_cohort).with(user.program_type).and_return(:promoted_cohort)
                expect(user.relevant_cohort).to eq(:promoted_cohort)
            end
        end
    end

    describe "program_type setter" do
        it "should update fallback_program_type" do
            user = users(:pending_mba_cohort_user)
            expect(user.fallback_program_type).not_to eq('emba') # sanity check

            user.program_type = 'emba'

            expect(user.fallback_program_type).to eq('emba')
        end

        it "should set program_type_confirmed to true" do
            user = User.where(program_type_confirmed: false).first
            expect(user.program_type_confirmed).to be(false) # sanity check

            user.program_type = 'mba'

            expect(user.program_type_confirmed).to be(true)
        end

        describe "with a pending application" do

            before(:each) do
                @mba_cohort = cohorts(:published_mba_cohort)
                @emba_cohort = cohorts(:published_emba_cohort)
            end

            describe "with pre-existing application for new cohort" do
                attr_accessor :user, :new_cohort, :pre_existing_application, :pending_application

                before(:each) do
                    @user = User.first
                    @user.cohort_applications.delete_all
                    @new_cohort = Cohort.find_by_program_type('the_business_certificate')
                    @pre_existing_application = CohortApplication.create!(
                        user_id: @user.id,
                        cohort_id: @new_cohort.attributes['id'],
                        applied_at: Time.now,
                        status: 'rejected'
                    )

                    @pending_application = CohortApplication.create!(
                        user_id: @user.id,
                        cohort_id: @mba_cohort.attributes['id'],
                        applied_at: Time.now,
                        status: 'pending'
                    )
                    @pending_application.update_attribute(:original_program_type, 'emba') # simulate a previous change from emba

                    @user.reload
                    allow(@user).to receive(:cohort_applications).and_return([@pending_application, @pre_existing_application])
                    allow(user).to receive(:pending_application).at_least(:once).and_return(pending_application)
                    allow(@pre_existing_application).to receive(:save!)
                end

                describe "when supports_reapply?" do

                    before(:each) do
                        allow(@new_cohort.program_type_config).to receive(:supports_reapply?).and_return(true)
                    end

                    it "should destroy currently pending application" do
                        expect(pending_application).to receive(:destroy)
                        user.program_type = 'the_business_certificate'
                    end

                    it "should update pre-existing application's original_program_type, applied_at, and status to 'pending'" do
                        now = Time.now
                        allow(Time).to receive(:now).and_return(now)
                        expect(pre_existing_application).to receive(:status=).with('pending')
                        expect(pre_existing_application).to receive(:applied_at=).with(now)
                        expect(pre_existing_application).to receive(:save!)
                        expect(pre_existing_application).to receive(:original_program_type=).with('emba')
                        user.program_type = 'the_business_certificate'
                    end

                    it "should reset cohort_applications association" do
                        mock_association = double("cohort_applications association")
                        expect(user).to receive(:association).with(:cohort_applications).and_return(mock_association)
                        expect(mock_association).to receive(:reset)
                        user.program_type = 'the_business_certificate'
                    end
                end

                describe "when !supports_reapply?" do

                    before(:each) do
                        allow(@new_cohort.program_type_config).to receive(:supports_reapply?).and_return(false)
                    end

                    it "should capture_exception" do
                        expect(Raven).to receive(:capture_exception).with("Attempting to reapply to cohort that doesn't support reapply", {
                            extra: {
                                user_id: user.id,
                                new_cohort_id: new_cohort.attributes['id'],
                                pre_existing_application_id: pre_existing_application.id
                            }
                        })
                        user.program_type = 'the_business_certificate'
                    end
                end
            end

            describe "without pre-existing application for new cohort" do

                it "should not change the pending application if it has the same program_type as the promoted cohort" do
                    user = users(:pending_mba_cohort_user)
                    expect(user.cohort_applications.first.cohort).to eq(@mba_cohort) # sanity check

                    promoted_cohort = @mba_cohort.clone
                    allow(Cohort).to receive(:promoted_cohort).and_return(promoted_cohort)

                    expect(user.cohort_applications.first.cohort).to_not eq(@promoted_cohort) # sanity check

                    user.program_type = 'mba'

                    expect(user.cohort_applications.first.cohort).to eq(@mba_cohort)
                    expect(user.cohort_applications.first.cohort).to_not eq(@promoted_cohort)
                end

                it "should change the pending application if it does not have same program_type as the promoted cohort if not switching between mba and emba" do
                    user = users(:pending_emba_cohort_user)
                    expect(user.cohort_applications.first.cohort).to eq(@emba_cohort) # sanity check

                    promoted_cohort = Cohort.promoted_cohort('the_business_certificate')
                    expect(Cohort).to receive(:promoted_cohort).and_return(promoted_cohort)

                    # ensure_institution_state_for_fallback_program_type gets called on the user when we save
                    # the cohort_application and can mess with some of the assertions in these specs that are
                    # focused on testing just the relevant logic. Since it's not important for these specs,
                    # we just stub it out.
                    # NOTE: We're using `allow_any_instance_of` here because stubbing it out on the `user`
                    # and on `user.cohort_applications.first.user` wasn't working.
                    allow_any_instance_of(User).to receive(:ensure_institution_state_for_fallback_program_type)

                    expect(user.cohort_applications.first.cohort).to_not eq(promoted_cohort) # sanity check

                    user.program_type = 'the_business_certificate'

                    expect(user.cohort_applications.first.cohort.attributes['id']).to_not eq(@emba_cohort.attributes['id'])
                    expect(user.cohort_applications.first.cohort.attributes['id']).to eq(promoted_cohort.attributes['id'])
                end

                it "should change the pending application to user's relevant_cohort sister_cohort if switching between mba and emba" do
                    user = users(:pending_emba_cohort_user)
                    expect(user.cohort_applications.first.cohort).to eq(@emba_cohort) # sanity check

                    expect(user.relevant_cohort).to receive(:sister_cohort).and_return(@mba_cohort) # mock out the call to get the sister_cohort

                    user.program_type = 'mba'

                    expect(user.cohort_applications.first.cohort).to_not eq(@emba_cohort)
                    expect(user.cohort_applications.first.cohort).to eq(@mba_cohort)
                end
            end
        end
    end

    describe "fallback_program_type" do
        attr_accessor :user

        before(:each) do
            @user = users(:rejected_the_business_certificate_cohort_user)
            expect(user.active_institution).to eq(Institution.smartly) # sanity check
            user.update_columns(fallback_program_type: 'the_business_certificate')
        end

        it "should ensure correct institution state if changing" do
            expect(user).to receive(:ensure_institution_state_for_fallback_program_type).at_least(1).times.and_call_original

            # Emulates selecting mba in the program_choice_form_dir
            user.update!(fallback_program_type: 'mba')
            expect(user.reload.active_institution).to eq(Institution.quantic)
        end

        it "should not try to ensure anything if fallback_program_type is not changing" do
            expected_active_institution = user.active_institution
            expect(user).not_to receive(:ensure_institution_state_for_fallback_program_type)
            user.update!(name: 'foo')
            expect(user.reload.active_institution).to eq(expected_active_institution)
        end

        it "should not try to ensure anything if fallback_program_type is changing to nil" do
            expected_active_institution = user.active_institution
            expect(user).not_to receive(:ensure_institution_state_for_fallback_program_type)
            user.update!(fallback_program_type: nil)
            expect(user.reload.active_institution).to eq(expected_active_institution)
        end
    end

    describe "ensure_candidate_relationships" do

        it "should create some candidate relationships" do
            hiring_manager = users(:hiring_manager)
            existing_candidate = hiring_manager.candidate_relationships.first.candidate
            new_candidate = CareerProfile.where.not(user_id: hiring_manager.candidates.map(&:id)).first.user

            orig_length = hiring_manager.candidate_relationships.count
            hiring_manager.ensure_candidate_relationships([existing_candidate.id, new_candidate.id])
            expect(hiring_manager.candidate_relationships.reload.count).to eq(orig_length + 1)
            expect(hiring_manager.candidate_relationships.map(&:candidate)).to include(existing_candidate)
            expect(hiring_manager.candidate_relationships.map(&:candidate)).to include(new_candidate)
        end

        it "should work with attrs passed in" do
            hiring_manager = users(:hiring_manager)
            existing_relationship = hiring_manager.candidate_relationships.first
            existing_candidate = existing_relationship.candidate
            new_candidate = CareerProfile.where.not(user_id: hiring_manager.candidates.map(&:id)).first.user

            orig_length = hiring_manager.candidate_relationships.count

            expect_any_instance_of(HiringRelationship).to receive(:meth=).with('called').exactly(1).times

            hiring_manager.ensure_candidate_relationships({
                existing_candidate.id => {hiring_manager_priority: 1} ,
                new_candidate.id => {hiring_manager_priority: 2, meth: 'called'}
            })
            expect(hiring_manager.candidate_relationships.reload.count).to eq(orig_length + 1)

            # existing relationship is not changed
            expect(hiring_manager.candidate_relationships.detect { |r| r.candidate_id == existing_candidate.id}.hiring_manager_priority).to eq(existing_relationship.hiring_manager_priority)

            # new relationship is changed
            expect(hiring_manager.candidate_relationships.detect { |r| r.candidate_id == new_candidate.id}.hiring_manager_priority).to eq(2)
        end

    end

    describe "sync with airtable" do
        it "should not update_in_airtable on changes to special fields" do
            user = User.first

            expect(user).not_to receive(:update_in_airtable)
            user.tokens = {
                'O9CXdrkEaN1_Dz32k8Ud4w' => {
                    "token"=>"$2a$10$cRMV7F0u1QwpEQ/FTcszc.ms80OeXL1n9eOlgw40Lkjfm.WCrC1kK",
                    "expiry"=>1480518600
                }
            }
            user.updated_at = Time.now
            user.save!
        end

        it "should not update_in_airtable if json field is nil" do
            user = User.first
            User.connection.execute("update users set provider_user_info=NULL where id='#{user.id}'")
            user = User.find(user.id)
            expect(user).not_to receive(:update_in_airtable)
            user.tokens = {
                'O9CXdrkEaN1_Dz32k8Ud4w' => {
                    "token"=>"$2a$10$cRMV7F0u1QwpEQ/FTcszc.ms80OeXL1n9eOlgw40Lkjfm.WCrC1kK",
                    "expiry"=>1480518600
                }
            }
            user.updated_at = Time.now
            user.save!
        end

        it "should not update_in_airtable when no changes" do
            user = User.first
            expect(user).not_to receive(:update_in_airtable)
            user.save!
        end

        it "should update_in_airtable on other changes" do
            user = User.first
            expect(user).to receive(:update_in_airtable)
            user.name = "New name"
            user.save!
        end

        it "should not continue to update applications with should_sync_with_airtable? = false" do
            @user = User.left_outer_joins(:cohort_applications).where("cohort_applications.id is null").first
            cohort = Cohort.first
            allow_any_instance_of(CohortApplication).to receive(:add_user_to_access_group)
            create_application(cohort, "rejected")
            expect_any_instance_of(CohortApplication).to receive(:should_sync_with_airtable?).and_return false
            expect(SyncWithAirtableJob).not_to receive(:perform_later_with_debounce)
            user.name = "New name"
            user.save!
        end
    end

    describe "validate_appropriate_can_edit_career_profile_change" do
        it "should fail if can_edit_career_profile changed from true -> false and career profile is in a CareerProfileList" do
            user = User.joins(:career_profile).where(career_profiles: {id: CareerProfileList.pluck(:career_profile_ids).flatten}).first
            # valid change
            user.can_edit_career_profile = true
            expect(user.valid?).to be(true)

            user.save!
            user.reload

            # invalid change
            user.can_edit_career_profile = false
            expect(user.valid?).to be(false)
            expect(user.errors.full_messages).to include("Can edit career profile can only be false if candidate is not in a CareerProfileList")
        end
    end

    describe "cohort_percent_complete" do

        it "should work" do
            cohort = cohorts(:published_mba_cohort)
            user = cohort.accepted_users.first
            user.lesson_streams_progresses.destroy_all

            required_stream_pack_ids = cohort.get_required_stream_locale_pack_ids
            expect(user.cohort_percent_complete(cohort.id)).to eq(0)

            # required stream not in playlist is not counted
            Lesson::StreamProgress.create!(user_id: user.id, locale_pack_id: required_stream_pack_ids.first, completed_at: Time.now, started_at: Time.now)
            expect(user.cohort_percent_complete(cohort.id)).to be_within(0.0001).of(1.0/required_stream_pack_ids.size)

        end
    end

    describe "completed_lesson_locale_pack_ids" do
        it "should work" do
            user = LessonProgress.first.user

            expect(user.completed_lesson_locale_pack_ids).to match_array(
                LessonProgress.where(user_id: user.id)
                .where.not(completed_at: nil)
                .pluck(:locale_pack_id))
        end
    end

    describe "completed_specializations_count" do

        it "should return the number of completed specializations if cohort supports specializations" do
            user = users(:accepted_emba_cohort_user)

            pid = user.relevant_cohort.get_specialization_stream_locale_pack_ids.first
            expect(pid).not_to be(nil)

            Lesson::StreamProgress.create!(
                user_id: user.id,
                locale_pack_id: pid,
                started_at: Time.now,
                completed_at: Time.now
            )

            expect(user.completed_specializations_count).to eq(1)
        end

        it "should return 0 if no completed specializations and cohort supports specializations" do
            user = users(:accepted_emba_cohort_user)
            user.lesson_progresses.destroy_all
            expect(user.completed_specializations_count).to be(0)
        end

        it "should return nil if no relevant_cohort" do
            user = User.create!(@attr)
            expect(user.relevant_cohort).to be(nil)
            expect(user.completed_specializations_count).to be(nil)
        end

        it "should return nil if relevant cohort does not supports specializations" do
            user = users(:accepted_career_network_only_cohort_user)
            expect(user.completed_specializations_count).to be(nil)
        end

    end

    describe "get_completed_foundations_stream_count" do

        it "should return number of completed foundations courses" do
            u = User.first

            # return the first Cohort as the promoted cohort
            cohort = Cohort.first

            # This is called twice because the Identify will be called when the Stream Progress is made as well
            expect(u).to receive(:relevant_cohort).twice.and_return(cohort)

            # return the first lesson stream locale pack as the cohort's foundations streams id
            lesson_stream_locale_pack = Lesson::Stream::LocalePack.first
            expect(cohort).to receive(:get_foundations_lesson_stream_locale_pack_ids).and_return([lesson_stream_locale_pack.id])

            # complete that stream pack
            Lesson::StreamProgress.create(user_id: u.id, locale_pack_id: lesson_stream_locale_pack.id, started_at: Time.now - 1000, completed_at: Time.now)

            # we get should one for the completed stream count now
            expect(u.get_completed_foundations_stream_count).to be(1)
        end
    end

    describe "has_met_requirements_for_period?" do
        attr_accessor :user, :cohort

        before(:each) do
            @user = User.left_outer_joins(:cohort_applications).where("cohort_applications.id is null").first
            @cohort = cohorts(:published_mba_cohort_with_exams)

            # Add user to cohort
            CohortApplication.create({
                cohort_id: cohort.id,
                status: 'accepted',
                user_id: user.id,
                applied_at: Time.current,
            })

            # Destroy all of the user's progress
            user.lesson_streams_progresses.destroy_all
        end

        it "should return true if all required streams up to and including the passed-in period are completed" do
            sanity_check(cohort)

            locale_pack_ids = cohort.periods.first["stream_entries"].map { |hash| hash["locale_pack_id"] } +
                cohort.periods.second["stream_entries"].map { |hash| hash["locale_pack_id"] }

            # Complete courses for a different user and ensure that doesn't affect the calculation
            complete_locale_packs(User.where.not(id: user.id).first.id, locale_pack_ids)
            expect(user.has_met_requirements_for_period?(cohort, 2)).to be(false)

            complete_locale_packs(user.id, locale_pack_ids)
            # If we call this method for the third (0-based) period it should be false since we completed all required courses
            # in the first two periods for the user
            expect(user.has_met_requirements_for_period?(cohort, 2)).to be(true)
        end

        it "should return false if all required streams up to and including the passed-in period are not completed" do
            sanity_check(cohort)
            expect(user.has_met_requirements_for_period?(cohort, 2)).to be(false)
        end

        def sanity_check(cohort)
            # Sanity check. Need at least two periods to properly test this
            expect(cohort.periods.length).to be >= 1

            # Sanity check. Make sure that the two periods have at least one course
            expect(cohort.periods.first["stream_entries"].length).to be >= 1
            expect(cohort.periods.second["stream_entries"].length).to be >= 1
        end

        def complete_locale_packs(user_id, locale_pack_ids)
            return unless locale_pack_ids.present?

            locale_pack_ids.each do |locale_pack_id|
                Lesson::StreamProgress.create_or_update!({
                    :locale_pack_id => locale_pack_id,
                    :user_id => user_id,
                    :complete => true
                })
            end
        end
    end

    describe "who_receives_pre_application_communications?" do

        it "should respect program_type" do

            expected_user = User.left_outer_joins(:cohort_applications)
                        .where("cohort_applications.id": nil)
                        .where(fallback_program_type: Cohort::ProgramTypeConfig.signup_program_types).first

            expect(User.who_receives_pre_application_communications.where(id: expected_user.id).present?).to be(true)

            unexpected_user = User.left_outer_joins(:cohort_applications)
                        .where("cohort_applications.id": nil)
                        .where.not(fallback_program_type: Cohort::ProgramTypeConfig.signup_program_types).first

            expect(User.who_receives_pre_application_communications.where(id: unexpected_user.id).present?).to be(false)
        end

        it "should not return someone with an application" do

            expected_user = User.left_outer_joins(:cohort_applications)
                        .where(fallback_program_type: Cohort::ProgramTypeConfig.signup_program_types)
                        .where("cohort_applications.id": nil).first

            expect(User.who_receives_pre_application_communications.where(id: expected_user.id).present?).to be(true)

            unexpected_user = User.left_outer_joins(:cohort_applications)
                        .where(fallback_program_type: Cohort::ProgramTypeConfig.signup_program_types)
                        .where.not("cohort_applications.id": nil).first

            expect(User.who_receives_pre_application_communications.where(id: unexpected_user.id).present?).to be(false)

        end

        it "should not return a user who is too old" do
            min_created_at = create_recent_admission_rounds
            expected_user, unexpected_user = User.left_outer_joins(:cohort_applications)
                        .where(fallback_program_type: Cohort::ProgramTypeConfig.signup_program_types)
                        .where("cohort_applications.id": nil).limit(2).to_a

            expected_user.update_attribute(:created_at, min_created_at + 1.day)
            unexpected_user.update_attribute(:created_at, min_created_at - 1.day)

            expect(User.who_receives_pre_application_communications.where(id: expected_user.id).present?).to be(true)
            expect(User.who_receives_pre_application_communications.where(id: unexpected_user.id).present?).to be(false)
        end

        def create_recent_admission_rounds
            cohort = cohorts(:published_mba_cohort)
            allow_any_instance_of(Cohort).to receive(:after_publish_change).and_return(true)
            Cohort.destroy_all

            # Create 4 cohorts starting  32 days ago, 12 days ago,
            # 8 days from now, and 28 days from now.
            # Each has 2 admission rounds
            start_date = 32.days.ago
            cohorts = (1..4).map do |i|
                attrs = cohort.attributes
                attrs.delete("playlist_pack_ids")
                attrs.delete("application_deadline")
                attrs.delete("id")
                attrs.delete("enrollment_warning_days_offset")
                attrs.delete("enrollment_expulsion_days_offset")
                attrs.delete("stripe_plan_id")
                attrs.delete("stripe_plan_amount")
                cohort = Cohort.create!(attrs.merge({
                    start_date: start_date,
                    admission_rounds: [
                        {
                            application_deadline_days_offset: -11
                        },
                        {
                            application_deadline_days_offset: -1
                        }
                    ]
                }))
                start_date = start_date + 20.days
                cohort.publish!
                cohort
            end
            RefreshMaterializedContentViews.refresh

            # We are now in round 2 of the 3rd cohort.  So if we want
            # to get the last 4 cohorts, then we want
            # - cohort 3 rounds 1 and 2
            # - cohort 2 rounds 1 and 2

            # Which means the date we're looking for is the application deadline
            # of cohort 1 round 2
            admission_round = AdmissionRound.where({
                program_type: 'mba',
                index: 2, # 1-indexed
                cohort_id: cohorts[0].id
            }).first
            admission_round.application_deadline
        end
    end

    describe "available_for_relationships" do
        attr_reader :user

        before(:each) do
            @user = CareerProfile.active.joins(:work_experiences)
                        .where("user_id in (#{User.external_users.select(:id).to_sql})")
                        .first.user
            expect(User.available_for_relationships.where(id: user.id).exists?).to be(true)
        end

        it "should not return a user with no active profile" do
            expect(CareerProfile).to receive(:active).and_return(CareerProfile.active.where.not(user_id: user.id))
            expect(User.available_for_relationships.where(id: user.id).exists?).to be(false)
        end

        it "should not return a user set to do_not_create_relationships" do
            user.career_profile.update_attribute(:do_not_create_relationships, true)
            expect(User.available_for_relationships(nil, false).where(id: user.id).exists?).to be(false)
        end

        it "should return a user set to do_not_create_relationships" do
            user.career_profile.update_attribute(:do_not_create_relationships, true)
            expect(User.available_for_relationships.where(id: user.id).exists?).to be(true)
        end

        it "should not return a candidate who is has a current work experience at the same company as the hiring manager" do
            option_id = user.career_profile.work_experiences.first.professional_organization_option_id
            expect(User.available_for_relationships(option_id).where(id: user.id).exists?).to be(false)
        end

        it "should not return a candidate who is an internal Pedago user" do
            user.update_attribute(:email, 'foo@pedago.com')
            expect(User.available_for_relationships.where(id: user.id).exists?).to be(false)
        end
    end

    describe "candidate_relationships_with_accepted_hiring_manager_status" do
        it "should work" do
            hiring_manager = HiringRelationship.first.hiring_manager
            hiring_manager.candidate_relationships.destroy_all

            expected_relationships = []
            HiringRelationship.where.not(hiring_manager_id: hiring_manager.id).limit(5)
            candidate_ids = User.where.not(id: hiring_manager.id).limit(5).pluck(:id)
            candidate_ids.each do |candidate_id|
                relationship = HiringRelationship.create!(hiring_manager_id: hiring_manager.id, candidate_id: candidate_id, hiring_manager_status: "accepted")
                expected_relationships.push(relationship)
            end

            expect(hiring_manager.candidate_relationships_with_accepted_hiring_manager_status.pluck(:id)).to match_array(expected_relationships.pluck(:id))
        end
    end

    describe "hiring_team_members" do
        it "should work" do
            team = HiringTeam.create(domain: 'thebrowns.com', title: 'The Browns')
            hiring_managers = User.limit(3)
            hiring_managers.each { |hm| hm.update(hiring_team_id: team.id) }
            expect(hiring_managers.first.hiring_team_members.pluck(:id)).to match_array(hiring_managers.pluck(:id))
        end

        it "should work if no team" do
            hiring_manager = User.where(hiring_team_id: nil).first
            expect(hiring_manager.hiring_team_members).to be_empty
        end
    end

    describe "relationships_from_team_with_accepted_hiring_manager_status" do
        it "should work" do
            team = HiringTeam.create(domain: 'thebrowns.com', title: 'The Browns')
            hiring_managers = User.limit(3)
            users = User.where.not(id: hiring_managers.pluck(:id)).limit(3)
            hiring_managers.each { |hm| hm.update(hiring_team_id: team.id) }

            expected_relationships = []
            hiring_managers.each_with_index do |hiring_manager, i|
                relationship = HiringRelationship.create!(hiring_manager_id: hiring_manager.id, candidate_id: users[i].id, hiring_manager_status: "accepted")
                expected_relationships.push(relationship)
            end

            expect(hiring_managers.first.relationships_from_team_with_accepted_hiring_manager_status.pluck(:id)).to match_array(expected_relationships.pluck(:id))
        end
    end

    describe "ensure_hiring_team!" do

        it "should work with no company_name" do
            user = User.where(hiring_team_id: nil)
                .where.not(email: nil)
                .left_outer_joins(:professional_organization).where("professional_organization_options.id is null").first
            user.ensure_hiring_team!
            expect(user.hiring_team.title).to eq("Team for #{user.name}")
        end

        it "should work with a company_name" do
            user = User.where(hiring_team_id: nil)
                .where.not(email: nil)
                .joins(:professional_organization).first
            user.ensure_hiring_team!
            expect(user.hiring_team.title).to eq(user.company_name)
        end

        it "should work with a team" do
            user = User.joins(:hiring_team).first
            expect(user).not_to receive(:save!)
            orig_hiring_team = user.hiring_team
            user.ensure_hiring_team!
            expect(user.hiring_team).to eq(orig_hiring_team)
        end

    end

    describe "hide_relationships_accepted_by_team" do
        it "should hide pending and saved relationships that are accepted by the hiring manager's team when team changes" do
            hiring_manager = User.where(hiring_team_id: nil).first
            future_teammate = users(:hiring_manager_with_team)
            future_teammate.candidate_relationships.destroy_all
            candidates = User.where.not(id: [hiring_manager.id, future_teammate.id]).limit(2)

            # Create a pending relationship for the hiring_manager
            pending_relationship = HiringRelationship.create!(
                hiring_manager_id: hiring_manager.id,
                candidate_id: candidates[0].id,
                hiring_manager_status: 'pending')

            # Create a saved relationship for the hiring_manager
            saved_relationship = HiringRelationship.create!(
                hiring_manager_id: hiring_manager.id,
                candidate_id: candidates[1].id,
                hiring_manager_status: 'saved_for_later')

            # Create accepted relationships for the future teammate
            candidates.each do |candidate|
                HiringRelationship.create!(
                    hiring_manager_id: future_teammate.id,
                    candidate_id: candidate.id,
                    hiring_manager_status: 'accepted')
            end

            # Place the hiring manager in the future_teammate's team
            expect(hiring_manager).to receive(:hide_relationships_accepted_by_team).and_call_original
            hiring_manager.update!(hiring_team_id: future_teammate.hiring_team_id)

            pending_relationship.reload
            expect(pending_relationship.hiring_manager_status).to eq('hidden')

            saved_relationship.reload
            expect(saved_relationship.hiring_manager_status).to eq('hidden')
        end

        it "should not be called if hiring_team_id is not changing" do
            hiring_manager = users(:hiring_manager_with_team)
            expect(hiring_manager).not_to receive(:hide_pending_and_saved_relationships_accepted_by_team)
            hiring_manager.update!(name: "foo")
        end
    end

    describe "hide_teammates_relationships_accepted_by_you" do
        it "should hide teammates relationships that are accepted by the hiring manager when team changes" do
            hiring_manager = User.where(hiring_team_id: nil).first
            future_teammate = users(:hiring_manager_with_team)
            future_teammate.candidate_relationships.destroy_all
            candidates = User.where.not(id: [hiring_manager.id, future_teammate.id]).limit(2)

            # Create accepted relationships for the hiring_manager
            candidates.each do |candidate|
                HiringRelationship.create!(
                    hiring_manager_id: hiring_manager.id,
                    candidate_id: candidate.id,
                    hiring_manager_status: 'accepted')
            end

            # Create a pending relationship for the future_teammate
            pending_relationship = HiringRelationship.create!(
                hiring_manager_id: future_teammate.id,
                candidate_id: candidates[0].id,
                hiring_manager_status: 'pending')

            # Create a saved relationship for the future_teammate
            saved_relationship = HiringRelationship.create!(
                hiring_manager_id: future_teammate.id,
                candidate_id: candidates[1].id,
                hiring_manager_status: 'saved_for_later')

            # Place the hiring manager in the future_teammate's team
            expect(hiring_manager).to receive(:hide_teammates_relationships_accepted_by_you).and_call_original
            hiring_manager.update!(hiring_team_id: future_teammate.hiring_team_id)

            pending_relationship.reload
            expect(pending_relationship.hiring_manager_status).to eq('hidden')

            saved_relationship.reload
            expect(saved_relationship.hiring_manager_status).to eq('hidden')
        end

        it "should not be called if hiring_team_id is not changing" do
            hiring_manager = users(:hiring_manager_with_team)
            expect(hiring_manager).not_to receive(:hide_relationships_accepted_by_team)
            hiring_manager.update!(name: "foo")
        end
    end

    describe "validate_hiring_team_owner" do

        it "should be invalid if changing team of owner" do
            hiring_team = HiringTeam.create!(domain: 'newteam.com', title: 'new team')
            user = User.where(hiring_team_id: nil).first
            user.hiring_team_id = hiring_team.id
            user.save!
            hiring_team.owner = user
            hiring_team.save!

            user.hiring_team_id = nil
            expect(user.valid?).to be(false)
            expect(user.errors.full_messages).to include("Hiring team cannot be changed away from a team that the user owns")

        end

    end

    describe "validate_hiring_application_not_accepted" do
        it "should be invalid if changing to nil and hiring_application is accepted" do
            hiring_manager = users(:hiring_manager_with_team)
            expect(hiring_manager.hiring_application.status).to eq('accepted') # sanity
            expect(hiring_manager.valid?).to be(true)
            hiring_manager.hiring_team_id = nil
            expect(hiring_manager.valid?).to be(false)
            expect(hiring_manager.errors.full_messages).to include("Hiring team cannot be unset for a user with an accepted HiringApplication")
        end
    end

    describe "hiring_team_change" do
        it "should return change if in previous_changes" do
            user = User.first
            allow(user).to receive(:previous_changes).and_return({
                hiring_team_id: ['foo', 'bar']
            })
            expect(user.send(:hiring_team_change)).to eq('bar')
        end
    end

    describe "candidate_position_interest conflict handling" do

        it "THIS user changes hiring_team_id, THIS user has position, OTHER user has ACCEPTED relationship" do
            hiring_manager = users(:hiring_manager)
            hiring_manager.candidate_relationships.destroy_all

            open_position = hiring_manager.open_positions.first
            open_position.candidate_position_interests.destroy_all
            candidates = User.where("id not in (#{HiringApplication.select('user_id').to_sql})").limit(3).to_a
            destroy_candidate_position_interests(candidates)

            reviewed_position_interest = CandidatePositionInterest.create!(
                open_position_id: open_position.id,
                candidate_id: candidates.first.id,
                hiring_manager_status: CandidatePositionInterest.reviewed_hiring_manager_statuses.first,
                candidate_status: 'accepted'
            )
            unreviewed_position_interest = CandidatePositionInterest.create!(
                open_position_id: open_position.id,
                candidate_id: candidates.second.id,
                hiring_manager_status: 'unseen',
                candidate_status: 'accepted'
            )
            rejected_position_interest = CandidatePositionInterest.create!(
                open_position_id: open_position.id,
                candidate_id: candidates.third.id,
                hiring_manager_status: 'rejected',
                candidate_status: 'accepted'
            )

            future_teammate = users(:hiring_manager_with_team)
            future_teammate.ensure_candidate_relationships(candidates.map(&:id))
            future_teammate.candidate_relationships.where(candidate_id: candidates.map(&:id)).each { |r| r.update_attribute(:hiring_manager_status, 'accepted') }

            hiring_manager.update_attribute(:hiring_team_id, future_teammate.hiring_team_id)

            # reviewed record is hidden
            expect(reviewed_position_interest.reload.hiring_manager_status).to eq('hidden')

            # unseen record is hidden
            expect(unreviewed_position_interest.reload.hiring_manager_status).to eq('hidden')

            # rejected record is unchanged
            expect(rejected_position_interest.reload.hiring_manager_status).to eq('rejected')
        end

        it "THIS user changes hiring_team_id, THIS user has position, OTHER user has REJECTED relationship" do
            hiring_manager = users(:hiring_manager)
            hiring_manager.candidate_relationships.destroy_all

            open_position = hiring_manager.open_positions.first

            candidates = User.where("id not in (#{HiringApplication.select('user_id').to_sql})").limit(2)
            destroy_candidate_position_interests(candidates)
            reviewed_position_interest = CandidatePositionInterest.create!(
                open_position_id: open_position.id,
                candidate_id: candidates.first.id,
                hiring_manager_status: CandidatePositionInterest.reviewed_hiring_manager_statuses.first,
                candidate_status: 'accepted'
            )
            unreviewed_position_interest = CandidatePositionInterest.create!(
                open_position_id: open_position.id,
                candidate_id: candidates.second.id,
                hiring_manager_status: 'unseen',
                candidate_status: 'accepted'
            )

            future_teammate = users(:hiring_manager_with_team)
            future_teammate.candidate_relationships.destroy_all
            future_teammate.ensure_candidate_relationships(candidates.map(&:id))
            future_teammate.candidate_relationships.where(candidate_id: candidates.map(&:id)).each { |r| r.update_attribute(:hiring_manager_status, 'rejected') }

            hiring_manager.update_attribute(:hiring_team_id, future_teammate.hiring_team_id)

            # since the relationship is rejected, there is no conflict
            expect(reviewed_position_interest.reload.hiring_manager_status).to eq(CandidatePositionInterest.reviewed_hiring_manager_statuses.first)
            expect(unreviewed_position_interest.reload.hiring_manager_status).to eq('unseen')
        end

        it "THIS user changes hiring_team_id, THIS user has ACCEPTED relationship, OTHER user has position" do
            hiring_manager = users(:hiring_manager)
            candidates = hiring_manager.candidate_relationships.where(hiring_manager_status: 'accepted').limit(3).map(&:candidate)
            expect(candidates.size).to eq(3)
            destroy_candidate_position_interests(candidates)

            future_teammate = users(:hiring_manager_with_team)
            open_position = future_teammate.open_positions.first

            reviewed_position_interest = CandidatePositionInterest.create!(
                open_position_id: open_position.id,
                candidate_id: candidates.first.id,
                hiring_manager_status: CandidatePositionInterest.reviewed_hiring_manager_statuses.first,
                candidate_status: 'accepted'
            )
            unreviewed_position_interest = CandidatePositionInterest.create!(
                open_position_id: open_position.id,
                candidate_id: candidates.second.id,
                hiring_manager_status: 'unseen',
                candidate_status: 'accepted'
            )
            rejected_position_interest = CandidatePositionInterest.create!(
                open_position_id: open_position.id,
                candidate_id: candidates.third.id,
                hiring_manager_status: 'rejected',
                candidate_status: 'accepted'
            )

            hiring_manager.update_attribute(:hiring_team_id, future_teammate.hiring_team_id)

            # reviewed record is hidden
            expect(reviewed_position_interest.reload.hiring_manager_status).to eq('hidden')

            # unseen record is hidden
            expect(unreviewed_position_interest.reload.hiring_manager_status).to eq('hidden')

            # rejected record is unchanged
            expect(rejected_position_interest.reload.hiring_manager_status).to eq('rejected')
        end

        it "THIS user changes hiring_team_id, THIS user has REJECTED relationship, OTHER user has position" do
            hiring_manager = users(:hiring_manager)
            relationships = hiring_manager.candidate_relationships.where.not(hiring_manager_status: 'accepted').limit(2)
            relationships.each { |r| r.update_attribute(:hiring_manager_status, 'rejected')}
            candidates = relationships.map(&:candidate)
            destroy_candidate_position_interests(candidates)
            expect(candidates.size).to eq(2)

            future_teammate = users(:hiring_manager_with_team)
            open_position = future_teammate.open_positions.first

            # ensure we don't accidentally hide the interests we are about to create if the future_teammate already has
            # a relationship with them
            future_teammate.candidate_relationships.where(candidate_id: candidates.pluck(:id)).delete_all

            reviewed_position_interest = CandidatePositionInterest.create!(
                open_position_id: open_position.id,
                candidate_id: candidates.first.id,
                hiring_manager_status: CandidatePositionInterest.reviewed_hiring_manager_statuses.first,
                candidate_status: 'accepted'
            )
            unreviewed_position_interest = CandidatePositionInterest.create!(
                open_position_id: open_position.id,
                candidate_id: candidates.second.id,
                hiring_manager_status: 'unseen',
                candidate_status: 'accepted'
            )

            hiring_manager.update_attribute(:hiring_team_id, future_teammate.hiring_team_id)

            # since the relationship is rejected, there is no conflict
            expect(reviewed_position_interest.reload.hiring_manager_status).to eq(CandidatePositionInterest.reviewed_hiring_manager_statuses.first)
            expect(unreviewed_position_interest.reload.hiring_manager_status).to eq('unseen')
        end

        it "THAT user changes hiring_team_id, THIS user has ACCEPTED relationship, THIS user has position" do
            hiring_manager = users(:hiring_manager_with_team)
            candidate = User.where.not(id: hiring_manager.candidate_relationships.pluck(:candidate_id)).first
            destroy_candidate_position_interests([candidate])
            open_position = hiring_manager.open_positions.first
            candidate_position_interest = CandidatePositionInterest.create!(
                open_position_id: open_position.id,
                candidate_id: candidate.id,
                hiring_manager_status: CandidatePositionInterest.reviewed_hiring_manager_statuses.first,
                candidate_status: 'accepted'
            )
            hiring_manager.ensure_candidate_relationships([candidate.id])
            relationship = hiring_manager.candidate_relationships.find_by_candidate_id(candidate.id)
            relationship.update(
                hiring_manager_status: 'accepted',
                open_position_id: open_position.id
            )
            expect(candidate_position_interest.reload.hiring_manager_status).to eq('invited')

            future_teammate = users(:hiring_manager)
            future_teammate.candidate_relationships.destroy_all
            future_teammate.update_attribute(:hiring_team_id, hiring_manager.hiring_team_id)

            # since the new user has no association with this user, there is no conflict
            expect(candidate_position_interest.reload.hiring_manager_status).to eq('invited')
        end

        def destroy_candidate_position_interests(candidates)
            candidates.each { |ca| ca.candidate_position_interests.destroy_all }
        end
    end

    describe "deactivation/reactivation" do
        attr_accessor :user, :default_deactivated_attrs

        before(:each) do
            @default_deactivated_attrs = {
                'can_edit_career_profile' => false,
                'pref_student_network_privacy' => 'hidden',
                'notify_hiring_updates' => false,
                'notify_hiring_content' => false,
                'notify_hiring_candidate_activity' => false,
                'notify_hiring_spotlights' => false,
                'notify_hiring_spotlights_business' => false,
                'notify_hiring_spotlights_technical' => false,
                'notify_email_newsletter' => false,
                'pref_allow_push_notifications' => false,
                'notify_candidate_positions' => false,
                'notify_candidate_positions_recommended' => 'never'
            }

            @user = User.create!(@attr.merge({'can_edit_career_profile' => true}))
            ensure_user_activated
        end

        describe "deactivate_user" do

            it "should deactivate user" do
                ensure_user_deactivated
            end

        end

        describe "reactivate_user" do

            before(:each) do
                ensure_user_deactivated
            end

            it "should reactivate user by restoring from version" do
                ensure_user_activated
            end

            it "should update nothing if no version to restore from" do
                User::Version.where(id: @user.id).delete_all
                @user.deactivated = false
                @user.save!
                @user.reload
                @default_deactivated_attrs.each do |key, value|
                    expect(@user[key]).to eq(value)
                end
            end

        end

        def ensure_user_deactivated
            @user.deactivated = true
            @user.save!
            @user.reload

            expect(@user.deactivated).to eq(true)
            @default_deactivated_attrs.each do |key, value|
                expect(@user[key]).to eq(value)
            end
        end

        def ensure_user_activated
            @user.deactivated = false
            @user.save!
            @user.reload

            expect(@user.deactivated).to eq(false)
            @default_deactivated_attrs.each do |key, value|
                expect(@user[key]).not_to eq(value)
            end
        end

    end

    describe "is_first_concentration_playlist_complete?" do
        it "should work" do
            SafeCache.clear # Cohort#get_required_lesson_stream_locale_pack_ids_by_playlist
            user = users(:accepted_mba_cohort_user)
            first_concentration_playlist = Playlist.where(locale_pack_id: user.relevant_cohort.get_first_concentration_playlist_locale_pack, locale: user.locale).first
            allow(user).to receive(:completed_stream_locale_pack_ids).and_return(first_concentration_playlist.stream_locale_pack_ids) # just mock this rather than creating progresses
            expect(user.is_first_concentration_playlist_complete?).to be(true)
        end
    end

    def create_application(cohort, status)
        return CohortApplication.create!({
            user_id: @user.id,
            cohort_id: cohort.id,
            status: status,
            applied_at: Time.now,
            total_num_required_stripe_payments: (cohort.supports_recurring_payments? ? 12 : nil)
        })
    end

    describe "check_if_json_changed" do
        before(:each) do
            @user = users(:pending_mba_cohort_user)
        end

        it "should return :unchanged if the record, and associated records, did not change" do
            result = @user.check_if_json_changed do
                1 + 1
            end
            expect(result).to be(:unchanged)
        end

        describe "changed" do
            it "should return :changed if the user changed" do
                result = @user.check_if_json_changed do
                    User.find(@user.id).touch
                end
                expect(result).to be(:changed)
            end

            it "should return :changed if the last_application changed" do
                result = @user.check_if_json_changed do
                    CohortApplication.find(@user.last_application.id).touch
                end
                expect(result).to be(:changed)
            end

            it "should return :changed if the user had no last_application but does now" do
                @user.cohort_applications.delete_all
                result = @user.check_if_json_changed do
                    CohortApplication.create!(
                        :cohort_id => Cohort.first.id,
                        :applied_at => Time.now,
                        :user_id => user.id,
                        :status => 'pending'
                    )
                end
                expect(result).to be(:changed)
            end

            it "should return :changed if the career_profile changed" do
                result = @user.check_if_json_changed do
                    CareerProfile.find(@user.career_profile.id).touch
                end
                expect(result).to be(:changed)
            end

            # see https://trello.com/c/WMFtjTJU
            it "should return unchanged if the last_application is not the most recently updated" do
                @user = User.joins(:cohort_applications).first
                CohortApplication.create!(
                    user_id: user.id,
                    applied_at: Time.now,
                    cohort_id: Cohort.where.not(id: user.cohort_applications.pluck(:cohort_id)).first.id,
                    status: 'rejected'
                )
                @user.cohort_applications.reload
                not_last_application = @user.cohort_applications.detect { |ca| ca != @user.last_application}
                expect(not_last_application).not_to eq(@user.last_application)

                not_last_application.update_attribute(:updated_at, @user.last_application.updated_at + 1.day)

                result = @user.check_if_json_changed do
                    # do nothing
                end
                expect(result).to be(:unchanged)
            end
        end
    end

    describe "bump_check_in_with_inactive_user_job_run_at" do

        describe "when !saved_change_to_last_seen_at?" do

            it "should not be called after create" do
                allow_any_instance_of(User).to receive(:saved_change_to_last_seen_at?).and_return(false)
                expect_any_instance_of(User).not_to receive(:bump_check_in_with_inactive_user_job_run_at)
                User.create!(@attr)
            end

            it "should not be called after save" do
                user = User.first
                allow(user).to receive(:saved_change_to_last_seen_at?).and_return(false)
                expect(user).not_to receive(:bump_check_in_with_inactive_user_job_run_at)
                user.save!
            end
        end

        describe "when saved_change_to_last_seen_at?" do

            it "should be called after create" do
                allow_any_instance_of(User).to receive(:saved_change_to_last_seen_at?).and_return(true)
                expect_any_instance_of(User).to receive(:bump_check_in_with_inactive_user_job_run_at)
                User.create!(@attr)
            end

            it "should be called after save" do
                user = User.first
                allow(user).to receive(:saved_change_to_last_seen_at?).and_return(true)
                expect(user).to receive(:bump_check_in_with_inactive_user_job_run_at)
                user.save!
            end
        end

        describe "when !cohort_application_indicates_user_should_be_checked_in_with?" do

            it "should not enqueue a Cohort::CheckInWithInactiveUser delayed job" do
                user = User.first

                allow(user).to receive(:cohort_application_indicates_user_should_be_checked_in_with?).and_return(false)
                expect(Cohort::CheckInWithInactiveUser).not_to receive(:set)

                user.bump_check_in_with_inactive_user_job_run_at
            end
        end

        describe "when cohort_application_indicates_user_should_be_checked_in_with?" do

            before(:each) do
                @user = User.first
                allow(@user).to receive(:cohort_application_indicates_user_should_be_checked_in_with?).and_return(true)
            end

            it "should update run_at of existing pending job" do
                now = Time.now
                mock_job = double("Cohort::CheckInWithInactiveUser double")

                allow(@user).to receive(:check_in_with_inactive_user_job).and_return(mock_job)
                allow(Time).to receive(:now).and_return(now)
                expect(mock_job).to receive(:update_attribute).with(:run_at, now + 2.weeks)

                @user.bump_check_in_with_inactive_user_job_run_at
            end
        end
    end

    describe "should_be_checked_in_with?" do

        before(:each) do
            @user = User.first
        end

        it "should return false if last_seen_at was recently updated (less than 2.weeks ago)" do
            assert_should_be_checked_in_with(false, {last_seen_at: 2.weeks.ago + 1.minute})
        end

        it "should return false if !cohort_application_indicates_user_should_be_checked_in_with?" do
            assert_should_be_checked_in_with(false, {cohort_application_indicates_user_should_be_checked_in_with: false})
        end

        it "should return true if no last_seen_at and all other conditions are met" do
            assert_should_be_checked_in_with(true, {last_seen_at: nil})
        end

        it "should return true if all conditions are met" do
            assert_should_be_checked_in_with(true)
        end

        def assert_should_be_checked_in_with(expected_value, opts = {})
            opts = opts.with_indifferent_access

            allow(@user).to receive(:last_seen_at).and_return(opts.key?(:last_seen_at) ? opts['last_seen_at'] : 2.weeks.ago - 1.minute) # stub this to be recent (less than 2.weeks ago)
            allow(@user).to receive(:cohort_application_indicates_user_should_be_checked_in_with?).and_return(opts.key?(:cohort_application_indicates_user_should_be_checked_in_with) ? opts['cohort_application_indicates_user_should_be_checked_in_with'] : true)

            expect(@user.should_be_checked_in_with?).to be(expected_value)
        end
    end

    describe "cohort_application_indicates_user_should_be_checked_in_with?" do

        before(:each) do
            @user = User.first
        end

        it "should return false if no application_for_relevant_cohort" do
            assert_cohort_application_indicates_user_should_be_checked_in_with(false, {application_for_relevant_cohort: nil})
        end

        it "should return false if application_for_relevant_cohort.indicates_user_should_be_checked_in_with? is false" do
            assert_cohort_application_indicates_user_should_be_checked_in_with(false, {indicates_user_should_be_checked_in_with: false})
        end

        it "should return true if all conditions are met" do
            assert_cohort_application_indicates_user_should_be_checked_in_with(true)
        end

        def assert_cohort_application_indicates_user_should_be_checked_in_with(expected_value, opts = {})
            opts = opts.with_indifferent_access

            mock_cohort_application = double("CohortApplication for relevant_cohort")
            allow(@user).to receive(:application_for_relevant_cohort).and_return(opts.key?(:application_for_relevant_cohort) ? opts['application_for_relevant_cohort'] : mock_cohort_application) # stub this to use mock_cohort_application double by default
            allow(mock_cohort_application).to receive(:indicates_user_should_be_checked_in_with?).and_return(opts.key?(:indicates_user_should_be_checked_in_with) ? opts['indicates_user_should_be_checked_in_with'] : true) # stub this to be true by default

            expect(@user.cohort_application_indicates_user_should_be_checked_in_with?).to be(expected_value)
        end
    end

    describe "career_profile_fulltext" do

        it "should be updated if name is changing" do
            user = User.joins(:career_profile).first

            expect(user.career_profile).to receive(:update_fulltext).exactly(2).times

            user.update(avatar_url: 'changed')
            user.update(name: 'changed') # call update_fulltext
            user.update(nickname: 'changed') # call update_fulltext
        end

        it "should be updated if privacy setting is changing" do
            user = User.joins(:career_profile).first

            expect(user.career_profile).to receive(:update_fulltext).exactly(1).times

            user.update(avatar_url: 'changed')
            user.update(pref_student_network_privacy: 'changed') # call update_fulltext
        end

    end

    describe "raise_invalid_error_if_not_destroyable" do
        it "should prevent deleting the owner of a hiring team" do
            user = HiringTeam.joins(:owner).first.owner
            expect {
                user.destroy
            }.to raise_error(ActiveRecord::RecordInvalid, 'Validation failed: Hiring team has this user as the owner so the user cannot be destroyed.')
        end
    end

    describe "destruction lifecycle" do

        attr_accessor :user

        before(:each) do
            @user = users(:user_with_career_profile)
        end

        describe "before_destroy" do

            it "should delete progress records" do
                stream = lesson_streams(:published_stream)
                lesson = stream.lessons.detect(&:has_published_version?)
                Lesson::StreamProgress.create!(
                    user_id: user.id,
                    locale_pack_id: stream.locale_pack_id,
                    started_at: Time.now
                )
                LessonProgress.create!(
                    user_id: user.id,
                    locale_pack_id: lesson.locale_pack_id,
                    started_at: Time.now
                )

                expect(user).to receive(:delete_user_progresses).and_call_original
                expect(LessonProgress.where(user_id: user.id).to_a.count).not_to eq(0)
                expect(Lesson::StreamProgress.where(user_id: user.id).to_a.count).not_to eq(0)

                user.destroy

                expect(LessonProgress.where(user_id: user.id).to_a.count).to eq(0)
                expect(Lesson::StreamProgress.where(user_id: user.id).to_a.count).to eq(0)
            end

        end

        describe "after_destroy" do

            before(:each) do
                expect(user).to receive(:delete_user_data).and_call_original
            end

            it "should call destroy_check_in_with_inactive_user_job" do
                expect(user).to receive(:destroy_check_in_with_inactive_user_job)
                user.destroy
            end

            it "should enqueue a DeleteAssociatedUserDataJob" do
                expect(DeleteAssociatedUserDataJob).to receive(:perform_later).with(user.id, user.career_profile.id)
                user.destroy
            end

        end

        describe "persisted_timeline_events" do

            it "should be destroyed" do

                expect {
                expect {
                    user.persisted_timeline_events << ActivityTimeline::PersistedTimelineEvent.new(
                        time: Time.now,
                        event: 'note',
                        category: 'note'
                    )
                }.to change { ActivityTimeline::PersistedTimelineEvent.count }.by(1)
                }.to change { ActivityTimeline::PersistedTimelineEventUserJoin.count }.by(1)

                expect {
                    expect {
                    user.destroy
                }.to change { ActivityTimeline::PersistedTimelineEvent.count }.by(-1)
                }.to change { ActivityTimeline::PersistedTimelineEventUserJoin.count }.by(-1)



            end

        end

    end

    describe "handle_avatar_changes" do

        attr_accessor :user, :avatar

        before(:each) do
            self.user = User.first
            self.avatar = AvatarAsset.new

            user.avatar = avatar
            user.save!
        end

        it "should queue a job if avatar_url is set" do
            expect(CopyAvatarJob).to receive(:perform_later).with(user.id)
            user.avatar_url = 'http://path/to/new/avatar'
            user.save!
        end

        it "should not queue job if avatar is set directly" do
            expect(CopyAvatarJob).not_to receive(:perform_later).with(user.id)
            user.avatar = AvatarAsset.new
            user.save!
        end

        it "should unset avatar_url if avatar is added" do
            user.avatar_url = 'something'
            user.avatar = nil
            user.save!

            expect(CopyAvatarJob).not_to receive(:perform_later).with(user.id)
            user.avatar = AvatarAsset.new # setting avatar_url above nukes the one from the before(:each)
            user.save!
            expect(user.attributes['avatar_url']).to be_nil
        end

    end

    describe "destroy_old_avatar" do

        attr_accessor :user, :avatar

        before(:each) do
            self.user = User.first
            self.avatar = AvatarAsset.new

            user.avatar = avatar
            user.save!
        end

        it "should destroy current avatar if avatar_url is set" do
            user.avatar_url = 'http://path/to/new/avatar'
            user.save!

            # old avatar should be destroyed
            expect(AvatarAsset.find_by_id(avatar.id)).to be_nil
        end

        it "should destroy current avatar if avatar is changed" do
            user.avatar = AvatarAsset.new
            user.save!

            # old avatar should be destroyed
            expect(AvatarAsset.find_by_id(avatar.id)).to be_nil
        end

    end

    describe "avatar_url=" do

        attr_accessor :user, :avatar

        before(:each) do
            self.user = User.first
            self.avatar = AvatarAsset.new({
                :source_url => 'http://some/url/from/facebook'
            })

            user.avatar = avatar
            user.save!
        end

        it "should not override an existing avatar with the same url" do
            expect(user.avatar_url).to eq(user.avatar.file.url) # sanity check

            expect {
                user.avatar_url = user.avatar.file.url
            }.not_to change {
                {
                    avatar_url: user.avatar_url,
                    attr: user.attributes['avatar_url'],
                    avatar_id: user.avatar_id
                }
            }
        end

        it "should not override an existing avatar with a source_url that matches the one coming in" do
            expect(user.avatar_url).to eq(user.avatar.file.url) # sanity check

            expect {
                user.avatar_url = user.avatar.source_url
            }.not_to change {
                {
                    avatar_url: user.avatar_url,
                    attr: user.attributes['avatar_url'],
                    avatar_id: user.avatar_id
                }
            }
        end

        it "should allow for a new avatar_url" do
            user.avatar_url = 'http://path/to/new/avatar'
            user.save!

            expect(user.avatar_url).to eq('http://path/to/new/avatar')
            expect(user.attributes['avatar_url']).to eq('http://path/to/new/avatar')
            expect(user.avatar_id).to be_nil
            expect(AvatarAsset.exists?(avatar.id)).to be(false)
        end

        it "should prevent setting avatar_url attribute directly with brackets" do
            user = User.first
            expect(user).to receive(:avatar_url=).with('new_avatar_url')
            expect(Raven).to receive(:capture_in_production)
                .with(User::DisallowedBracketAccess)
                .exactly(1).times
            user['avatar_url'] = 'new_avatar_url'

            # no problem setting something else
            expect(user).not_to receive(:name=)
            user['name'] = 'new name'
            expect(user.name).to eq('new name')
            expect(user.attributes['name']).to eq('new name')

        end

        it "should not set the avatar_url to a smart.ly url" do
            user = User.first
            orig = user.avatar_url
            url = 'http://uploads.smart.ly/path/to/avatar'
            expect(Raven).to receive(:capture_exception).with("Trying to set avatar_url to a url in smartly\'s aws bucket", {
                extra: {
                    url: url
                }
            })
            user.avatar_url = url
            expect(user.avatar_url).to eq(orig)
        end
    end

    describe "handle_pref_allow_push_notifications_changes" do

        attr_accessor :user

        before(:each) do
            self.user = User.first
        end

        it "should queue a ReconcileMobileDeviceJob if the pref changes to false" do
            user.update_column(:pref_allow_push_notifications, true) # skip callbacks

            expect(ReconcileMobileDeviceJob).to receive(:perform_later).with(user.id).and_return({})

            user.pref_allow_push_notifications = false
            user.save!

        end

        it "should queue a ReconcileMobileDeviceJob if the pref changes to true" do
            user.update_column(:pref_allow_push_notifications, false) # skip callbacks

            expect(ReconcileMobileDeviceJob).to receive(:perform_later).with(user.id).and_return({})

            user.pref_allow_push_notifications = true
            user.save!
        end

    end

    describe "update_user_id_verifications_from_hashes" do

        it "should work" do
            cohort = cohorts(:published_mba_cohort)
            cohort.assign_attributes(
                start_date: Time.now - 25.days,
                id_verification_periods: [
                    {start_date_days_offset: 0, due_date_days_offset: 10},
                    {start_date_days_offset: 11, due_date_days_offset: 20},
                    {start_date_days_offset: 21, due_date_days_offset: 30},
                    {start_date_days_offset: 31, due_date_days_offset: 40}
                ]
            )

            user = cohort.accepted_users.first
            verifier_user = User.where.not(id: user.id).first
            expect(user.user_id_verifications).to be_empty

            user.update_user_id_verifications_from_hashes([{
                "user_id" => user.id,
                "cohort_id" => cohort.id,
                "id_verification_period_index" => 0,
                "verification_method" => 'verified_by_admin'
            }], verifier_user)

            expect(user.user_id_verifications.size).to eq(1)
            expect(user.user_id_verifications[0].attributes.slice('cohort_id', 'id_verification_period_index', 'verification_method', 'verifier_id', 'verifier_name')).to eq({
                "cohort_id" => cohort.id,
                "id_verification_period_index" => 0,
                "verification_method" => 'verified_by_admin',
                "verifier_id" => verifier_user.id,
                "verifier_name" => verifier_user.name
            })
            expect(user.user_id_verifications[0].verified_at).to be_within(1.second).of(Time.now)

            user.update_user_id_verifications_from_hashes(user.user_id_verifications.as_json + [{
                "user_id" => user.id,
                "cohort_id" => cohort.id,
                "id_verification_period_index" => 1,
                "verification_method" => 'verified_by_admin'
            }], verifier_user)

            user_id_verifications = user.user_id_verifications.reorder(:verified_at)
            expect(user_id_verifications.size).to eq(2)
            expect(user_id_verifications[1].attributes.slice('cohort_id', 'id_verification_period_index', 'verification_method', 'verifier_id', 'verifier_name')).to eq({
                "cohort_id" => cohort.id,
                "id_verification_period_index" => 1,
                "verification_method" => 'verified_by_admin',
                "verifier_id" => verifier_user.id,
                "verifier_name" => verifier_user.name
            })
            expect(user_id_verifications[1].verified_at).to be_within(1.second).of(Time.now)
        end

    end

    describe "deferred logic" do
        before(:each) do
            @random_id = SecureRandom.uuid
            @user = User.left_outer_joins(:cohort_applications).where("cohort_applications.id is null").first
            @cohorts = Cohort.limit(3)
            @user.cohort_applications = [
                CohortApplication.create({
                    id: @random_id,
                    status: 'deferred',
                    applied_at: Time.now - 2.years,
                    user: @user,
                    cohort: @cohorts.first
                }),
                CohortApplication.create({
                    status: 'rejected',
                    applied_at: Time.now - 1.years,
                    user: @user,
                    cohort: @cohorts.second
                }),
                CohortApplication.create({
                    status: 'pending',
                    applied_at: Time.now,
                    user: @user,
                    cohort: @cohorts.third
                })
            ]
            @user.save!
            @user.reload
        end

        it "should build deferred info correctly" do
            expect(@user.deferred_info.statuses_after_deferred).to match_array(['pending', 'rejected', 'deferred'])
            expect(@user.deferred_info.last_deferred_application.id).to eq(@random_id)
        end
    end

    describe "handle_network_inclusion" do

        it "should include accepted mba and emba students" do
            assert_inclusion_of_accepted_user(true, 'mba')
            assert_inclusion_of_accepted_user(true, 'emba')
            assert_inclusion_of_accepted_user(false, 'the_business_certificate')
        end

        it "should include deferred students with no later application" do
            cohort_application = get_deferred_application
            assert_inclusion_of_user(true, cohort_application.user)
        end

        it "should include deferred students with later pre_accepted application" do
            cohort_application = get_deferred_application
            cohort2 = Cohort.where(program_type: 'mba').where.not(id: cohort_application.cohort_id).all_published.first

            CohortApplication.new(
                cohort_id: cohort2.id,
                user_id: cohort_application.user_id,
                applied_at: Time.now,
                status: 'pre_accepted'
            ).save!(validate: false)

            assert_inclusion_of_user(true, cohort_application.user)
        end

        it "should not include deferred students with later expelled application" do
            cohort_application = get_deferred_application
            cohort2 = Cohort.where(program_type: 'mba').where.not(id: cohort_application.cohort_id).all_published.first

            CohortApplication.new(
                cohort_id: cohort2.id,
                user_id: cohort_application.user_id,
                applied_at: Time.now,
                status: 'expelled'
            ).save!(validate: false)

            assert_inclusion_of_user(false, cohort_application.user)
        end

        it "should not include failed users" do
            cohort = Cohort.joins(:cohort_applications => {:user => :career_profile})
                        .where(cohort_applications: {status: 'accepted'})
                        .first

            cohort_application = cohort.cohort_applications.find_by_status('accepted')
            cohort_application.update(graduation_status: 'failed')
            user = cohort_application.user
            assert_inclusion_of_user(false, user)
        end

        it "should support the first scenario that ori came up with" do
            cohort_application = get_deferred_application
            cohort2 = Cohort.where(program_type: 'mba').where.not(id: cohort_application.cohort_id).all_published.first
            emba_cohort = Cohort.where(program_type: 'emba').first

            CohortApplication.new(
                cohort_id: cohort2.id,
                user_id: cohort_application.user_id,
                applied_at: Time.now,
                status: 'expelled'
            ).save!(validate: false)

            CohortApplication.new(
                cohort_id: emba_cohort.id,
                user_id: cohort_application.user_id,
                applied_at: Time.now,
                status: 'pending'
            ).save!(validate: false)
            assert_inclusion_of_user(false, cohort_application.user)
        end

        it "should support the second scenario that ori came up with" do
            cohort_application = get_deferred_application
            cohort2 = Cohort.where(program_type: 'mba').where.not(id: cohort_application.cohort_id).all_published.first
            emba_cohort = Cohort.where(program_type: 'emba').first

            # since this uzer was expelled, being pre_accepted does not allow zir
            # back in.  Ze has lost student network priveleges until ze
            # is accepted
            CohortApplication.new(
                cohort_id: cohort2.id,
                user_id: cohort_application.user_id,
                applied_at: Time.now,
                status: 'expelled'
            ).save!(validate: false)

            CohortApplication.new(
                cohort_id: emba_cohort.id,
                user_id: cohort_application.user_id,
                applied_at: Time.now,
                status: 'pre_accepted'
            ).save!(validate: false)

            assert_inclusion_of_user(false, cohort_application.user)
        end

        it "should support the third scenario that ori came up with" do
            cohort_application = get_deferred_application
            cohort2 = Cohort.where(program_type: 'mba').where.not(id: cohort_application.cohort_id).all_published.first
            emba_cohort = Cohort.where(program_type: 'emba').first

            # This user was expelled, but has not been accepted, so ze
            # is back in
            CohortApplication.new(
                cohort_id: cohort2.id,
                user_id: cohort_application.user_id,
                applied_at: Time.now,
                status: 'expelled'
            ).save!(validate: false)

            CohortApplication.new(
                cohort_id: emba_cohort.id,
                user_id: cohort_application.user_id,
                applied_at: Time.now,
                status: 'accepted'
            ).save!(validate: false)

            assert_inclusion_of_user(true, cohort_application.user)
        end

        it "should support the fourth scenario that ori came up with" do
            cohort_application = get_deferred_application
            mba_cohort_2 = Cohort.where(program_type: 'mba').where.not(id: cohort_application.cohort_id).all_published.first
            emba_cohort = Cohort.where(program_type: 'emba').first

            # This user was expelled, but then deferred again, so ze is in again
            CohortApplication.new(
                cohort_id: mba_cohort_2.id,
                user_id: cohort_application.user_id,
                applied_at: Time.now,
                status: 'expelled'
            ).save!(validate: false)

            CohortApplication.new(
                cohort_id: emba_cohort.id,
                user_id: cohort_application.user_id,
                applied_at: Time.now,
                status: 'deferred'
            ).save!(validate: false)

            assert_inclusion_of_user(true, cohort_application.user)
        end

        def get_deferred_application
            cohort_application = CohortApplication.joins(:cohort, {:user => :career_profile})
                                    .where({
                                        cohorts: {program_type: 'mba'},
                                        status: 'deferred'
                                    }).first

            (cohort_application.user.cohort_applications - [cohort_application]).map(&:destroy)
            cohort_application
        end

        def assert_inclusion_of_accepted_user(includes, program_type)
            cohort = Cohort.joins(:cohort_applications => {:user => :career_profile})
                        .where(program_type: program_type, cohort_applications: {status: 'accepted'})
                        .first

            user = cohort.cohort_applications.find_by_status('accepted').user
            user.handle_network_inclusion
            assert_inclusion_of_user(includes, user)
        end

        def assert_inclusion_of_user(includes, user)
            user.handle_network_inclusion
            expect(user.student_network_inclusion.present?).to eq(includes)
        end

        def a_different_mba_cohort_with_an_application(cohort_application)
            another_cohort_application = CohortApplication.joins(:cohort).where(cohorts: {program_type: 'mba'})
                .where.not(id: cohort_application.cohort_id).first
            if another_cohort_application.present?
                return another_cohort_application.cohort
            else
                cohort = Cohort.where(program_type: 'mba').where.not(id: cohort_application.cohort_id)
                CohortApplication.where.not(id: cohort_application.id).update(cohort_id: cohort.id)
                return cohort
            end
        end
    end

    describe "has_full_student_network_access?" do

        it "should be true for admins" do
            user = User.first
            role = Role.find_or_create_by(:name => "admin")
            user.roles = [role]
            expect(user.send(:has_full_student_network_access?)).to equal(true)
        end

        it "should be true for interviewers" do
            user = User.first
            role = Role.find_or_create_by(:name => "interviewer")
            user.roles = [role]
            expect(user.send(:has_full_student_network_access?)).to equal(true)
        end

        it "should be true when has_application_that_provides_student_network_inclusion?" do
            user = User.first
            expect(user).to receive(:is_admin_or_interviewer?).and_return(false) # need to mock this so that we check has_application_that_provides_student_network_inclusion?
            expect(user).to receive(:has_application_that_provides_student_network_inclusion?).and_return(true)
            expect(user.send(:has_full_student_network_access?)).to equal(true)
        end

        it "should be false otherwise" do
            user = User.first
            role = Role.find_or_create_by(:name => "leaner")
            user.roles = [role]
            expect(user).to receive(:has_application_that_provides_student_network_inclusion?).and_return(false)
            expect(user.send(:has_full_student_network_access?)).to equal(false)
        end

    end

    describe "has_full_student_network_events_access?" do

        attr_accessor :user

        before(:each) do
            @user = User.first
        end

        describe "when not admin_or_interviewer?" do

            before(:each) do
                expect(user).to receive(:is_admin_or_interviewer?).at_least(1).times.and_return(false)
            end

            it "should be false if no student_network_inclusion record" do
                expect(user).to receive(:student_network_inclusion).and_return(nil)
                expect(user.has_full_student_network_events_access?).to be(false)
            end

            it "should be false if student_network_inclusions.status is not accepted" do
                expect(user).to receive(:student_network_inclusion).at_least(1).times.and_return(OpenStruct.new({}))
                ['deferred', 'pre_accepted'].each do |status|
                    expect(user.student_network_inclusion).to receive(:status).and_return(status)
                    expect(user.has_full_student_network_events_access?).to be(false)
                end
            end

            it "should be true if student_network_inclusions.status is accepted" do
                expect(user).to receive(:student_network_inclusion).at_least(1).times.and_return(OpenStruct.new({}))
                expect(user.student_network_inclusion).to receive(:status).and_return('accepted')
                expect(user.has_full_student_network_events_access?).to be(true)
            end

        end

        it "should be true if is_admin_or_interviewer?" do
            expect(user).to receive(:is_admin_or_interviewer?).and_return(true)
            expect(user.has_full_student_network_events_access?).to be(true)
        end

    end

    describe "active_institution" do
        attr_accessor :user

        before(:each) do
            @user = User.left_outer_joins(:institutions).where(institutions: {id: nil}).first
        end

        it "should work" do
            expect(user.active_institution).to be_nil
            user.institutions << Institution.quantic
            user.active_institution = Institution.quantic
            expect(user.active_institution).to eq(Institution.quantic)
            user.reload
            expect(user.active_institution).to eq(Institution.quantic)
        end
    end

    describe "active_institution=" do
        attr_accessor :user

        before(:each) do
            @user = User.left_outer_joins(:institutions).where(institutions: {id: nil}).first
        end

        it "should allow setting, changing, and unsetting the active_institution" do
            user.institutions << Institution.quantic
            user.active_institution = Institution.quantic
            expect(user.active_institution).to eq(Institution.quantic)

            user.institutions << Institution.smartly
            user.active_institution = Institution.smartly
            expect(user.active_institution).to eq(Institution.smartly)

            user.active_institution = nil
            expect(user.active_institution).to be_nil
        end

        it "should not make a query if setting to the existing active_institution" do
            smartly_institution = Institution.smartly
            user.institutions << smartly_institution
            user.active_institution = smartly_institution
            expect(user.active_institution).to eq(smartly_institution)

            expect(ActiveRecord::Base.connection).not_to receive(:exec_query).and_call_original
            user.active_institution = smartly_institution
            expect(user.active_institution).to eq(smartly_institution)
        end

        it "should raise an error if user is not in the institution" do
            user = User.left_outer_joins(:institutions).where(institutions: {id: nil}).first
            expect {
                user.active_institution = Institution.first
            }.to raise_error('User must be in the institution for it to become their active')
        end
    end

    describe "defer_into_cohort" do
        it "should defer application, create a new pre_accepted_application for target cohort, and unregister the now deferred application" do
            now = Time.now
            user = users(:pre_accepted_mba_cohort_user)
            pre_accepted_application = user.pre_accepted_application
            target_cohort = Cohort.where.not(id: pre_accepted_application.cohort_id).first
            allow(Time).to receive(:now).and_return(now)
            allow_any_instance_of(Cohort).to receive(:supports_payments?).and_return(false)
            allow(pre_accepted_application).to receive(:registered_at).and_return(now)
            payment_grace_period_end_at = Time.now + 1.week
            pre_accepted_application.update_column(:payment_grace_period_end_at, payment_grace_period_end_at)
            payment_info = pre_accepted_application.payment_info
            expect(payment_info["payment_grace_period_end_at"]).to eq(payment_grace_period_end_at)
            expect(pre_accepted_application).to receive(:update!).with(status: 'deferred').ordered.and_call_original
            expect(CohortApplication).to receive(:create!).with({user_id: user.id, cohort_id: target_cohort.id, applied_at: now, status: 'pre_accepted', admissions_decision: 'manual_admin_decision', registered_at: now}.merge(payment_info)).ordered.and_call_original
            expect(pre_accepted_application).to receive(:update!).with(registered: false).ordered.and_call_original # calling through to the original to verify that no errors are thrown
            user.defer_into_cohort(pre_accepted_application, target_cohort.id)
        end

        it "should update existing subscription with a trial period without proration" do
            now = Time.now
            user = users(:pre_accepted_emba_cohort_user)
            pre_accepted_application = user.pre_accepted_application
            target_cohort = Cohort.where.not(id: pre_accepted_application.cohort_id).first
            allow(Time).to receive(:now).and_return(now)
            allow_any_instance_of(Cohort).to receive(:supports_payments?).and_return(true)
            expect(pre_accepted_application).to receive(:update!).with(status: 'deferred').and_call_original
            allow(pre_accepted_application).to receive(:registered_at).and_return(now)
            mock_cohort_application = double('CohortApplication', {'stripe_plan' => {'frequency' => 'not_once'}})
            expect(CohortApplication).to receive(:create!).with({user_id: user.id, cohort_id: target_cohort.id, applied_at: now, status: 'pre_accepted', admissions_decision: 'manual_admin_decision', registered_at: now}.merge(pre_accepted_application.payment_info)).and_return(mock_cohort_application)
            expect(pre_accepted_application).to receive(:update!).with(registered: false).and_call_original
            mock_primary_subscription = double('Stripe::Subscription', {stripe_subscription_id: 'sub_foo'})
            expect(user).to receive(:primary_subscription).at_least(1).times.and_return(mock_primary_subscription)
            expect(Stripe::Subscription).to receive(:update).with('sub_foo', {trial_end: target_cohort.trial_end_date.to_timestamp, proration_behavior: 'none'})
            user.defer_into_cohort(pre_accepted_application, target_cohort.id)
        end

        it "should raise if new subscription would be created" do
            now = Time.now
            user = users(:pre_accepted_emba_cohort_user)
            pre_accepted_application = user.pre_accepted_application
            target_cohort = Cohort.where.not(id: pre_accepted_application.cohort_id).first

            allow(Time).to receive(:now).and_return(now)
            allow_any_instance_of(Cohort).to receive(:supports_payments?).and_return(true)
            expect(pre_accepted_application).to receive(:update!).with(status: 'deferred').and_call_original
            allow(pre_accepted_application).to receive(:registered_at).and_return(now)
            mock_cohort_application = double('CohortApplication', {'total_num_required_stripe_payments' => nil, 'stripe_plans' => [{'id' => 1, 'amount' => 1337, 'frequency' => 'monthly'}]})
            expect(mock_cohort_application).to receive(:has_full_scholarship?).at_least(1).times.and_return(false)
            expect(CohortApplication).to receive(:create!).with({user_id: user.id, cohort_id: target_cohort.id, applied_at: now, status: 'pre_accepted', admissions_decision: 'manual_admin_decision', registered_at: now}.merge(pre_accepted_application.payment_info)).and_return(mock_cohort_application)
            expect(pre_accepted_application).to receive(:update!).with(registered: false).and_call_original
            expect(user).to receive(:primary_subscription).at_least(1).times.and_return(nil)

            expect {
                user.defer_into_cohort(pre_accepted_application, target_cohort.id)
            }.to raise_error('Does not support creating a new subscription')
        end
    end

    describe "should_have_transcripts_auto_verified?" do

        attr_accessor :user, :education_experiences

        before(:each) do
            @user = users(:user_with_career_profile)
            @education_experiences = CareerProfile::EducationExperience.limit(2).to_a
        end

        it "should be true when !transcripts_verified and all required transcripts are approved or waived" do
            expect(user).to receive(:transcripts_verified).and_return(false)
            expect(user.career_profile).to receive(:education_experiences_indicating_transcript_required).at_least(1).times.and_return(education_experiences)
            allow_any_instance_of(CareerProfile::EducationExperience).to receive(:transcript_approved_or_waived?).and_return(true)
            expect(user.should_have_transcripts_auto_verified?).to be(true)
        end

        it "should be false if not all required transcripts are approved or waived" do
            expect(user).to receive(:transcripts_verified).and_return(false)
            expect(user.career_profile).to receive(:education_experiences_indicating_transcript_required).at_least(1).times.and_return(education_experiences)
            expect(education_experiences.first).to receive(:transcript_approved_or_waived?).and_return(true)
            expect(education_experiences.second).to receive(:transcript_approved_or_waived?).and_return(false)
            expect(user.should_have_transcripts_auto_verified?).to be(false)
        end

        it "should be false if transcripts_verified" do
            expect(user).to receive(:transcripts_verified).and_return(true)
            expect(user.should_have_transcripts_auto_verified?).to be(false)
        end

        it "should be false if no transcripts are required" do
            expect(user).to receive(:transcripts_verified).and_return(false)
            expect(user.career_profile).to receive(:education_experiences_indicating_transcript_required).at_least(1).times.and_return([])
            expect(user.should_have_transcripts_auto_verified?).to be(false)
        end

    end
end
