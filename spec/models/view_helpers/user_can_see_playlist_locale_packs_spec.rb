require 'spec_helper'

describe ViewHelpers::UserCanSeePlaylistLocalePacks do

    fixtures(:cohorts, :institutions)

    it "should include required and specialization playlists from a cohort" do
        cohort = cohorts(:published_mba_cohort).published_version
        user = cohort.accepted_users.first
        expect(user_can_see_playlist_pack_ids(user.id)).to match_array(cohort.required_playlist_pack_ids + cohort.specialization_playlist_pack_ids)

    end

    it "should include playlists from an institution" do
        user = User.left_outer_joins(:institutions).where(institutions: {id: nil}).first
        institution = institutions(:institution_with_playlists)
        user.institutions << institution
        expect(user_can_see_playlist_pack_ids(user.id)).to include(*institution.playlist_pack_ids)
        
        user.institutions = []
        user.save!
        expect(user_can_see_playlist_pack_ids(user.id)).not_to include(*institution.playlist_pack_ids)
    end

    def user_can_see_playlist_pack_ids(user_id)
        ActiveRecord::Base.connection.execute(%Q~
            SELECT locale_pack_id
            FROM user_can_see_playlist_locale_packs
            WHERE user_id='#{user_id}'
        ~).to_a.map { |r| r['locale_pack_id']}
    end

end