require 'spec_helper'

describe ViewHelpers::UsersRelevantCohorts do

    fixtures(:cohorts)

    it "should assign the promoted cohort as relevant for a user with no application" do
        promoted_cohort = Cohort.promoted_cohort('mba')
        user = User.left_outer_joins(:cohort_applications)
            .where("cohort_applications.id is null").first
        user.update_attribute(:fallback_program_type, 'mba')

        expect(relevant_cohort_id(user.id)).to eq(promoted_cohort.attributes['id'])
    end

    it "should assign the promoted cohort as relevant for a user with a rejected application" do
        promoted_cohort = Cohort.promoted_cohort('mba')
        user = CohortApplication.where.not(cohort_id: promoted_cohort.attributes['id']).where(status: 'rejected').first.user
        user.update_attribute(:fallback_program_type, 'mba')

        expect(relevant_cohort_id(user.id)).to eq(promoted_cohort.attributes['id'])
    end

    it "should assign the active cohort as relevant for a user with a pending application" do
        promoted_cohort = Cohort.promoted_cohort('mba')
        cohort_application = CohortApplication.where.not(cohort_id: promoted_cohort.attributes['id']).where(status: 'pending').first
        expect(relevant_cohort_id(cohort_application.user_id)).to eq(cohort_application.cohort_id)
    end

    it "should assign the active cohort as relevant for a user with an accepted application" do
        promoted_cohort = Cohort.promoted_cohort('mba')
        cohort_application = CohortApplication.where.not(cohort_id: promoted_cohort.attributes['id']).where(status: 'accepted').first
        expect(relevant_cohort_id(cohort_application.user_id)).to eq(cohort_application.cohort_id)
    end

    it "should assign no cohort for a user with no accepted application and no promoted cohort" do
        user = User.left_outer_joins(:cohort_applications).where("cohort_applications.id is null").first
        user.update_attribute(:fallback_program_type, 'no_cohort')
        expect(relevant_cohort_id(user.id)).to be_nil
    end

    def relevant_cohort_id(user_id)
        ActiveRecord::Base.connection.execute(%Q~
            SELECT
                cohort_id
            FROM users_relevant_cohorts
            WHERE user_id='#{user_id}'
        ~).to_a.first['cohort_id']
    end

end