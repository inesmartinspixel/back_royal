# == Schema Information
#
# Table name: idology_verifications
#
#  id                   :uuid             not null, primary key
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  user_id              :uuid             not null
#  idology_id_number    :text             not null
#  scan_capture_url     :text             not null
#  response_received_at :datetime
#  verified             :boolean
#  response             :json
#  email                :text
#  phone                :text
#

require 'spec_helper'

describe "IdologyVerification" do

    describe "fetch_idology_capture_token" do

        it "should fetch a token and save a record" do
            expect(IdologyVerification).to receive(:make_idology_request).with("scan-capture-token.svc").and_return({
                'id_number': '12345',
                'scan_capture_url': 'http://path/to/somewhere'
            }.stringify_keys)
            user = User.first
            IdologyVerification.fetch_idology_capture_token({user_id: user.id, phone: 'phone', email: 'email'})

            record = IdologyVerification.reorder(:created_at).last
            expect(record.user_id).to eq(user.id)
            expect(record.idology_id_number).to eq('12345')
            expect(record.email).to eq('email')
            expect(record.phone).to eq('phone')
            expect(record.scan_capture_url).to eq('http://path/to/somewhere')
        end

    end

    describe "make_idology_request" do

        def mock_http_request(endpoint, params, idology_response)
            mock_http = {}
            mock_response = {}

            expect(mock_response).to receive(:body) do
                idology_response
            end

            expect(mock_http).to receive(:request) do |request|
                expect(request.uri.to_s).to eq("https://web.idologylive.com/api/#{endpoint}")
                queryparams = Rack::Utils.parse_nested_query(request.body)
                expect(queryparams['username']).to eq(ENV['IDOLOGY_USERNAME'])
                expect(queryparams['password']).to eq(ENV['IDOLOGY_PASSWORD'])
                params.each do |key, value|
                    expect(queryparams[key.to_s]).to eq(value)
                end
                mock_response
            end

            expect(Net::HTTP).to receive(:start).with("web.idologylive.com", 443, {
                use_ssl: true,
                verify_mode: OpenSSL::SSL::VERIFY_NONE,
            }) do |http, &block|
                block.call(mock_http)
            end

        end

        it "should make a request and return the response" do
            idology_response = {"some" => "response"}
            params = {some: 'params'}
            mock_http_request('endpoint', params, idology_response.to_xml(root: 'response'))
            expect(IdologyVerification.make_idology_request('endpoint', params)).to eq(idology_response)
        end

        it "should handle an error" do
            params = {some: 'params'}
            mock_http_request('endpoint', params, {"error" => "Something went wrong."}.to_xml(root: 'response'))
            expect {
                IdologyVerification.make_idology_request('endpoint', params)
            }.to raise_error do |err|
                expect(err.message).to eq('Error response from IDology: "Something went wrong."')
                expect(err.raven_options).to eq({
                    extra: {
                        response: {"error" => "Something went wrong."},
                        endpoint: 'endpoint',
                        query_params: params
                    },
                    fingerprint: ['Idology.make_idology_request', "Something went wrong."]
                })
            end
        end

        it "should handle malformed XML response" do
            params = {some: 'params'}
            mock_http_request('endpoint', params, '<response><country><AO</country></response>')
            expect(IdologyVerification.make_idology_request('endpoint', params)).to eq({
                'capture_decision' => {'key' => 'capture.internal.error'}
            })
        end

        it "should handle blank response" do
            params = {some: 'params'}
            mock_http_request('endpoint', params, '')
            expect(IdologyVerification.make_idology_request('endpoint', params)).to eq({
                'capture_decision' => {'key' => 'capture.internal.error'}
            })
        end

    end

    describe "fetch_result_and_record_verification" do

        it "should handle 'result.scan.capture.id.approved'" do
            idology_id_number = '42'
            instance = IdologyVerification.create!(
                user_id: User.limit(1).pluck('id').first,
                idology_id_number: idology_id_number,
                scan_capture_url: 'scan_capture_url'
            )
            user = instance.user

            response = mock_idology_request(instance, {
                'capture_decision' => {'key' => 'result.scan.capture.id.approved'}
            })

            expect(UserIdVerification).to receive(:record_verification!).with(user, {
                verification_method: 'verified_by_idology',
                idology_verification_id: instance.id
            })
            expect(Raven).not_to receive(:capture_exception)

            instance.fetch_result_and_record_verification

            expect(instance.response_received_at).to be_within(1.second).of(Time.now)
            expect(instance.verified).to be(true)
            expect(instance.response).to eq(response)

        end

        it "should handle any other key" do

            idology_id_number = '42'
            instance = IdologyVerification.create!(
                user_id: User.limit(1).pluck('id').first,
                idology_id_number: idology_id_number,
                scan_capture_url: 'scan_capture_url'
            )
            user = instance.user

            response = mock_idology_request(instance, {
                'capture_decision' => {'key' => 'result.scan.capture.id.looks.a.bit.iffy'}
            })

            expect(UserIdVerification).not_to receive(:record_verification!)
            expect(Raven).to receive(:capture_exception).with("Unexpected capture_decision_key",
                level: 'warning',
                extra: {
                    user_id: instance.user_id,
                    idology_id_number: instance.idology_id_number,
                    response: response
                },
                fingerprint: ['Unexpected capture_decision_key', response['capture_decision']['key']]
            )

            instance.fetch_result_and_record_verification

            expect(instance.response_received_at).to be_within(1.second).of(Time.now)
            expect(instance.verified).to be(false)
            expect(instance.response).to eq(response)
        end

        it "should abort if response already received" do
            instance = IdologyVerification.create!(
                user_id: User.limit(1).pluck('id').first,
                idology_id_number: 1345,
                scan_capture_url: 'scan_capture_url',
                verified: false
            )
            expect(instance).not_to receive(:fetch_result)
            instance.fetch_result_and_record_verification
        end

        it "should do nothing if no capture_decision_key" do
            idology_id_number = '42'
            instance = IdologyVerification.create!(
                user_id: User.limit(1).pluck('id').first,
                idology_id_number: idology_id_number,
                scan_capture_url: 'scan_capture_url'
            )
            user = instance.user

            response = mock_idology_request(instance, {})

            expect(UserIdVerification).not_to receive(:record_verification!)
            expect(Raven).not_to receive(:capture_exception)

            instance.fetch_result_and_record_verification

            expect(instance.response_received_at).to be_nil
            expect(instance.verified).to be_nil
            expect(instance.response).to be_nil
        end

        def mock_idology_request(instance, response)
            expect(IdologyVerification).to receive(:make_idology_request).with("scan-capture-poll.svc", {
                queryId: instance.idology_id_number
            }).and_return(response)

            response
        end

    end

    describe "ensure_job_if_necessary" do
        before(:each) do
            @instance = IdologyVerification.create!(
                user_id: User.limit(1).pluck('id').first,
                idology_id_number: 12345,
                scan_capture_url: 'scan_capture_url'
            )
        end

        it "should not call ensure_job if instance is less than 30 seconds old" do
            @instance.verified = nil
            expect(Time).to receive(:now).and_return(@instance.created_at + 29.seconds)
            expect(GetIdologyResultJob).not_to receive(:ensure_job)
            @instance.ensure_job_if_necessary
        end

        it "should not call ensure_job if instance is already verified" do
            expect(GetIdologyResultJob).not_to receive(:ensure_job)
            expect(Time).to receive(:now).and_return(@instance.created_at + 31.seconds)
            @instance.verified = true
            @instance.ensure_job_if_necessary
        end

        it "should call ensure_job otherwise" do
            expect(GetIdologyResultJob).to receive(:ensure_job).with(@instance.idology_id_number, 30.seconds)
            expect(Time).to receive(:now).and_return(@instance.created_at + 31.seconds)
            @instance.verified = nil
            @instance.ensure_job_if_necessary
        end
    end


end
