require 'spec_helper'

describe Location::PlaceFormattingMixin do

    class PlaceFormattingMixinKlass
        extend Location::PlaceFormattingMixin
    end

    before(:each) do
        @place_details = {
            'formatted_address' => 'a formatted_address',
            'locality' => {
                'long' => 'Foo'
            },
            'administrative_area_level_1' => {
                'short' => 'Ba',
                'long' => 'Bar'
            },
            'country' => {
                'short' => 'US'
            }
        }
    end

    describe "location_string" do
        it "should work for US place" do
            expect(PlaceFormattingMixinKlass.location_string(@place_details)).to eq('Foo, Ba')
        end

        it "should include country code if present and not in the US/USA" do
            @place_details = @place_details.merge({
                'country' => {
                    'short' => 'AA'
                }
            })
            expect(PlaceFormattingMixinKlass.location_string(@place_details)).to eq('Foo, Ba, AA')
        end

        # Ex. 'Hong Kong', 'Singapore' (sometimes)
        it "should handle locations with only a locality" do
            @place_details = {
                'locality' => {
                    'long' => 'Foo'
                },
                'country' => {
                    'short' => 'ZZ'
                }
            }
            expect(PlaceFormattingMixinKlass.location_string(@place_details)).to eq('Foo, ZZ')
        end

        # Ex. Singapore (sometimes)
        it "should handle locations with only a country" do
            @place_details = {
                'country' => {
                    'long' => 'Singapore',
                    'short' => 'SG'
                },
                'formatted_address' => 'Singapore'
            }
            expect(PlaceFormattingMixinKlass.location_string(@place_details)).to eq('Singapore')
        end
    end

    describe "locations that require special handling" do
        it "should handle Singapore properly" do
            @place_details = {
                'formatted_address' => 'a formatted address',
                'locality' => {
                    'long' => 'Singapore'
                },
                'administrative_area_level_1' => {
                    'short' => 'Singapore'
                },
                'country' => {
                    'short' => 'SG'
                }
            }
            expect(PlaceFormattingMixinKlass.location_string(@place_details)).to eq('Singapore')
        end

        it "should handle Kong Kong properly" do
            @place_details = {
                'formatted_address' => 'a formatted address',
                'locality' => {
                    'long' => 'Hong Kong'
                },
                'country' => {
                    'short' => 'HK'
                }
            }
            expect(PlaceFormattingMixinKlass.location_string(@place_details)).to eq('Hong Kong')
        end

        it "should handle Tokyo properly" do
            @place_details = {
                'colloquial_area' => {
                    'short' => 'Tokyo',
                    'long' => 'Tokyo'
                },
                'administrative_area_level_1' => {
                    'short' => 'Tokyo',
                    'long' => 'Tokyo'
                },
                'country' => {
                    'short' => 'JP',
                    'long' => 'Japan'
                },
                'formatted_address' => 'Tokyo, Japan'
            }
            expect(PlaceFormattingMixinKlass.location_string(@place_details)).to eq('Tokyo, JP')
        end
    end

end
