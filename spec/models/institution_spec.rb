# == Schema Information
#
# Table name: institutions
#
#  id                :uuid             not null, primary key
#  created_at        :datetime
#  updated_at        :datetime
#  name              :string(255)
#  sign_up_code      :string(255)
#  scorm_token       :uuid
#  playlist_pack_ids :uuid             default([]), is an Array
#  domain            :text
#  external          :boolean          default(TRUE), not null
#

require 'spec_helper'

describe Institution do

    fixtures(:lesson_streams, :institutions)

    describe "users" do
        it "should allow deleting the institution if no users are in it" do
            institution = Institution.left_outer_joins(:user_joins).where(institutions_users: {id: nil}).first
            expect {
                institution.destroy!
            }.not_to raise_error
        end

        it "should not allow deleting the institution if users are in it" do
            institution = Institution.first
            user = User.left_outer_joins(:institutions).where(institutions: {id: nil}).first
            user.ensure_institution(institution)
            expect {
                institution.destroy!
            }.to raise_error(ActiveRecord::InvalidForeignKey)
        end
    end

    describe "update_from_hash" do
        before(:each) do
            # create the default test signup group
            @group1 = AccessGroup.find_or_create_by(name: "CATS")
            @group2 = AccessGroup.find_or_create_by(name: "DOGS")
        end

        it "updates institution and associated groups" do
            inst = Institution.create({name: 'JLL2'})

            inst.update!({name: 'JLL2', id: inst.id})
            expect(inst.name).to eq('JLL2')
            expect(inst.groups.size).to eq(0)

            # can add groups to this inst
            inst.update!({name: 'JLL2', id: inst.id, groups: [@group1]})
            expect(inst.groups.size).to eq(1)
            expect(inst.groups[0].name).to eq('CATS')

            inst.update!({name: 'JLL2', id: inst.id, groups: [@group1, @group2]})
            expect(inst.groups.size).to eq(2)
            expect(inst.groups[0].name).to eq('CATS')

            # can delete the groups from the inst
            inst.update!({name: 'JLL2', id: inst.id, groups: []})
            expect(inst.groups.size).to eq(0)

        end

        it "updates reports_viewers" do
            inst = Institution.first

            users = User.limit(3);
            inst = Institution.update_from_hash!({id: inst.id, updated_at: inst.updated_at, reports_viewer_ids: users.map(&:id)})

            reloaded = Institution.find(inst.id)
            expect(reloaded.reports_viewers).to match_array(users)
        end

        describe "update_from_hash!" do

            it "should call refresh_materialized_views" do
                inst = Institution.first
                expect(RefreshMaterializedContentViews).to receive(:refresh)

                inst = Institution.update_from_hash!({id: inst.id, updated_at: inst.updated_at, playlist_pack_ids: [SecureRandom.uuid]})
            end

        end
    end

    describe "save_from_hash" do
        attr_accessor :institution, :base_hash

        before(:each) do
            @institution = Institution.where(external: true).first
            @base_hash = {
                id: institution.id,
                updated_at: institution.updated_at
            }
        end

        describe "refresh content views" do
            it "should refresh if changing the name" do
                expect(RefreshMaterializedContentViews).to receive(:refresh)
                Institution.update_from_hash!(base_hash.merge(name: "foo #{Time.now.utc}"))
            end

            it "should refresh if changing groups" do
                expect(RefreshMaterializedContentViews).to receive(:refresh)
                new_group = AccessGroup.where.not(id: institution.access_groups.pluck(:id)).first
                Institution.update_from_hash!(base_hash.merge(groups: institution.access_groups + [new_group]))
            end

            it "should refresh if changing playlists" do
                expect(RefreshMaterializedContentViews).to receive(:refresh)
                new_playlist = Playlist.all_published.where.not(locale_pack_id: institution.playlist_pack_ids).first
                new_pack_ids = institution.playlist_pack_ids + [new_playlist.locale_pack_id]
                Institution.update_from_hash!(base_hash.merge(playlist_pack_ids: new_pack_ids))
            end

            it "should not refresh if no applicable changes" do
                expect(RefreshMaterializedContentViews).not_to receive(:perform_now)
                Institution.update_from_hash!(
                    base_hash.merge(domain: "#{Time.now.utc}.com", playlist_pack_ids: institution.playlist_pack_ids)
                )
            end
        end
    end

    describe "as_json" do

        before(:each) do
            # Preload the quantic institution to prevent an additional query from executing,
            # which would cause certain specs in this `describee block to fail that assert
            # that only one query gets run. NOTE: This is probably okay to do in our specs
            # because the quantic institution is cached.
            Institution.quantic
        end

        it "should only run one query if no fields passed in" do
            institution = Institution.first
            expect(ActiveRecord::Base.connection).not_to receive(:exec_query).and_call_original
            institution.as_json
        end

        it "should only run one query when adding reports_viewer_ids" do
            institution = Institution.includes(:reports_viewers).first
            expect(ActiveRecord::Base.connection).not_to receive(:exec_query).and_call_original
            institution.as_json
        end

        it "should only run one query when asking for users if they were preloaded" do
            institution = Institution.includes(:users).first
            expect(ActiveRecord::Base.connection).not_to receive(:exec_query).and_call_original
            institution.as_json
        end
    end

    describe "branding" do
        attr_accessor :quantic, :smartly, :other_institution

        before(:each) do
            @quantic = Institution.quantic
            @smartly = Institution.smartly
            @other_institution = Institution.where.not(id: [quantic.id, smartly.id]).first
        end

        it "should return 'quantic' for institutions with the quantic domain" do
            expect(other_institution).to receive(:domain).and_return(quantic.domain)
            expect(other_institution.branding).to eq('quantic')
            expect(quantic.branding).to eq('quantic')
            expect(smartly.branding).not_to eq('quantic')
        end

        it "should return 'smartly' for institutions that don't have the quantic domain" do
            expect(other_institution).to receive(:domain).and_return(smartly.domain)
            expect(other_institution.branding).to eq('smartly')
            expect(smartly.branding).to eq('smartly')
            expect(quantic.branding).not_to eq('smartly')
        end
    end
end
