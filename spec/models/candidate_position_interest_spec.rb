# == Schema Information
#
# Table name: candidate_position_interests
#
#  id                           :uuid             not null, primary key
#  candidate_id                 :uuid             not null
#  open_position_id             :uuid             not null
#  candidate_status             :text             not null
#  hiring_manager_status        :text             default("hidden"), not null
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  hiring_manager_priority      :integer
#  admin_status                 :text             default("unreviewed"), not null
#  hiring_manager_reviewer_id   :uuid
#  last_updated_at_by_non_admin :datetime
#  cover_letter                 :json             not null
#

require 'spec_helper'

describe CandidatePositionInterest do
    fixtures :users

    attr_reader :candidate
    attr_reader :open_position

    before(:each) do
        @candidate, @open_position = get_unrelated_candidate_and_position
    end

    describe "before_save" do
        it "should ensure career_profile on the user" do
            @candidate.career_profile = nil
            @candidate.save!
            expect(@candidate.career_profile).to be_nil
            CandidatePositionInterest.create_from_hash!(valid_attrs, {}, @candidate)
            expect(@candidate.career_profile).not_to be_nil
        end
    end

    describe "hiring_relationship conflict handling" do
        it "should update the candidate_position_interest to hidden if exists a relationship accepted by the hiring manager" do
            ensure_handles_conflict("accepted")
        end

        it "should update the candidate_position_interest to hidden if exists a relationship rejected by the hiring manager" do
            ensure_handles_conflict("rejected")
        end

        it "should not update the candidate_position_interest if already reviewed" do
            open_position = OpenPosition.first
            hiring_manager = open_position.hiring_manager
            candidate = User.where.not(id: hiring_manager.id).first

            hiring_manager.ensure_candidate_relationships([candidate.id])
            hiring_relationship = hiring_manager.candidate_relationships.find_by_candidate_id(candidate.id)
            hiring_relationship.update!(hiring_manager_status: 'accepted', open_position_id: open_position.id)

            interest = CandidatePositionInterest.create!({
                candidate_id: candidate.id,
                open_position_id: open_position.id,
                candidate_status: 'accepted',
                hiring_manager_status: CandidatePositionInterest.reviewed_hiring_manager_statuses.first
            })

            expect(interest.reload.hiring_manager_status).to eq(CandidatePositionInterest.reviewed_hiring_manager_statuses.first)
        end

        it "should update the candidate_position_interest to hidden if exists an accepted relationship on the team" do
            hiring_manager = users(:hiring_manager_with_team)
            teammate = users(:hiring_manager_teammate)

            excluded_ids = hiring_manager
                .candidate_relationships_with_accepted_hiring_manager_status
                .pluck(:candidate_id) +
                [hiring_manager.id, teammate.id]
            candidate = User.where.not(id: excluded_ids).first

            teammate.ensure_candidate_relationships([candidate.id])
            teammate_hiring_relationship = teammate.candidate_relationships.find_by_candidate_id(candidate.id)
            teammate_hiring_relationship.update!(hiring_manager_status: 'accepted')

            interest = CandidatePositionInterest.create!({
                candidate_id: candidate.id,
                open_position_id: hiring_manager.open_positions.first.id,
                candidate_status: 'accepted',
                hiring_manager_status: 'unseen'
            })

            expect(interest.reload.hiring_manager_status).to eq('hidden')
        end

        it "should not update the candidate_position_interest if there exists a relationship with an unrelated hiring manager" do
            hiring_manager = users(:hiring_manager_with_team)
            not_teammate = User.where.not(hiring_team_id: hiring_manager.hiring_team_id).first


            excluded_ids = hiring_manager
                .candidate_relationships_with_accepted_hiring_manager_status
                .pluck(:candidate_id) +
                [hiring_manager.id, not_teammate.id]
            candidate = User.where.not(id: excluded_ids).first

            not_teammate.ensure_candidate_relationships([candidate.id])
            teammate_hiring_relationship = not_teammate.candidate_relationships.find_by_candidate_id(candidate.id)
            teammate_hiring_relationship.update!(hiring_manager_status: 'accepted')

            interest = CandidatePositionInterest.create!({
                candidate_id: candidate.id,
                open_position_id: hiring_manager.open_positions.first.id,
                candidate_status: 'accepted',
                hiring_manager_status: 'unseen'
            })

            expect(interest.reload.hiring_manager_status).to eq('unseen')
        end

        def ensure_handles_conflict(hiring_manager_status)
            open_position = OpenPosition.first
            hiring_manager = open_position.hiring_manager

            # make sure not on a team as we test team logic in another test
            ensure_fresh_hiring_team(hiring_manager)

            # ensure a fresh relationship with a candidate
            user_id_not = hiring_manager.candidate_relationships.pluck(:candidate_id).push(hiring_manager.id)

            candidate = User.where.not(id: user_id_not).first
            hiring_manager.ensure_candidate_relationships([candidate.id])
            hiring_relationship = hiring_manager.candidate_relationships.find_by_candidate_id(candidate.id)
            hiring_relationship.update!(hiring_manager_status: hiring_manager_status)

            interest = CandidatePositionInterest.create!({
                candidate_id: candidate.id,
                open_position_id: open_position.id,
                candidate_status: 'accepted',
                hiring_manager_status: 'unseen'
            })

            expect(interest.reload.hiring_manager_status).to eq('hidden')
        end
    end

    describe "create_from_hash!" do

        it "should work" do
            expect_any_instance_of(CandidatePositionInterest).to receive(:merge_hash).and_call_original
            id = SecureRandom.uuid
            instance = CandidatePositionInterest.create_from_hash!(valid_attrs.merge(id: id))
            expect(instance.id).to eq(id)
            expect(instance.candidate_id).to eq(valid_attrs['candidate_id'])
        end

    end

    describe "update_from_hash" do

        it "should work" do
            instance = CandidatePositionInterest.create_from_hash!(valid_attrs)
            expect_any_instance_of(CandidatePositionInterest).to receive(:merge_hash).and_call_original
            CandidatePositionInterest.update_from_hash!({
                "id" => instance.id,
                "candidate_status" => "rejected"
            })
            expect(instance.reload.candidate_status).to eq("rejected")
        end

        it "should set last_curated_at on the open_position" do
            instance = CandidatePositionInterest.create_from_hash!(valid_attrs)
            expect_any_instance_of(CandidatePositionInterest).to receive(:merge_hash).and_call_original
            expected_ts = Time.now.to_timestamp
            CandidatePositionInterest.update_from_hash!({
                "id" => instance.id,
                "candidate_status" => "rejected"
            }, {
                "last_curated_at" => expected_ts
            })
            expect(instance.open_position.reload.last_curated_at).to eq(Time.at(expected_ts))
        end

        describe "hiring_manager_reviewer_id" do
            it "should set hiring_manager_reviewer_id when a hiring_manager and changing to a reviewed status" do
                instance = CandidatePositionInterest.first
                hiring_manager = instance.open_position.hiring_manager
                instance.update_columns({
                    hiring_manager_reviewer_id: nil,
                    hiring_manager_status: 'unseen'
                })
                CandidatePositionInterest.update_from_hash!({
                    "id" => instance.id,
                    "open_position_id" => instance.open_position_id,
                    "hiring_manager_status" => "rejected"
                }, nil, nil, hiring_manager)
                expect(instance.reload.hiring_manager_reviewer_id).to eq(hiring_manager.id)
            end

            it "should set hiring_manager_reviewer_id when a hiring_manager's teammate and changing to a reviewed status" do
                hiring_manager = users(:hiring_manager_with_team)
                teammate = users(:hiring_manager_teammate)
                instance = CandidatePositionInterest.joins(:open_position).where(open_position_id: hiring_manager.open_positions.pluck(:id)).first
                instance.update_columns({
                    hiring_manager_reviewer_id: nil,
                    hiring_manager_status: 'unseen'
                })
                CandidatePositionInterest.update_from_hash!({
                    "id" => instance.id,
                    "open_position_id" => instance.open_position_id,
                    "hiring_manager_status" => "rejected"
                }, nil, nil, teammate)
                expect(instance.reload.hiring_manager_reviewer_id).to eq(teammate.id)
            end

            it "should not set hiring_manager_reviewer_id when a hiring_manager but not changing to a reviewed status" do
                instance = CandidatePositionInterest.first
                hiring_manager = instance.open_position.hiring_manager
                instance.update_columns({
                    hiring_manager_reviewer_id: nil,
                    hiring_manager_status: 'unseen'
                })
                CandidatePositionInterest.update_from_hash!({
                    "id" => instance.id,
                    "open_position_id" => instance.open_position_id,
                    "hiring_manager_status" => "pending"
                }, nil, nil, hiring_manager)
                expect(instance.reload.hiring_manager_reviewer_id).to be_nil
            end

            it "should not set hiring_manager_reviewer_id when a hiring_manager but not changing the status" do
                instance = CandidatePositionInterest.first
                hiring_manager = instance.open_position.hiring_manager
                instance.update_columns({
                    hiring_manager_reviewer_id: nil,
                    hiring_manager_status: 'pending'
                })
                CandidatePositionInterest.update_from_hash!({
                    "id" => instance.id,
                    "open_position_id" => instance.open_position_id,
                    "hiring_manager_status" => "pending"
                }, nil, nil, hiring_manager)
                expect(instance.reload.hiring_manager_reviewer_id).to be_nil
            end

            it "should not set hiring_manager_reviewer_id when a candidate" do
                instance = CandidatePositionInterest.first
                instance.update_column(:hiring_manager_reviewer_id, nil)
                CandidatePositionInterest.update_from_hash!({
                    "id" => instance.id,
                    "open_position_id" => instance.open_position_id,
                    "candidate_status" => "rejected"
                }, nil, nil, instance.candidate)
                expect(instance.reload.hiring_manager_reviewer_id).to be_nil
            end
        end

        describe "CandidatePositionInterest::OldVersion" do
            it "should never raise if updated_at in hash is most recent" do
                instance = CandidatePositionInterest.first
                instance.update_column(:hiring_manager_status, 'saved_for_later')
                expect {
                    CandidatePositionInterest.update_from_hash!({
                        id: instance.id,
                        updated_at: instance.updated_at,
                        hiring_manager_status: 'hidden'
                    })
                }.not_to raise_error
            end

            it "should never raise if not changing hiring_manager_status" do
                instance = CandidatePositionInterest.first
                instance.update_column(:hiring_manager_status, 'saved_for_later')
                expect {
                    CandidatePositionInterest.update_from_hash!({
                        id: instance.id,
                        updated_at: instance.updated_at - 1.hour,
                    })
                }.not_to raise_error
            end

            it "should not raise when hiring_manager_status is being changed to a status that is after it in the statuses array" do
                instance = CandidatePositionInterest.first
                instance.update_column(:hiring_manager_status, 'saved_for_later')
                expect {
                    CandidatePositionInterest.update_from_hash!({
                        id: instance.id,
                        updated_at: instance.updated_at - 1.hour,
                        hiring_manager_status: 'invited'
                    })
                }.not_to raise_error
            end

            describe "as hiring manager" do
                it "should raise if hiring_manager_status is being changed to a status before it in the statuses array and updated_at is stale" do
                    instance = CandidatePositionInterest.first
                    instance.update_column(:hiring_manager_status, 'saved_for_later')
                    expect {
                        CandidatePositionInterest.update_from_hash!({
                            id: instance.id,
                            updated_at: instance.updated_at - 1.hour,
                            hiring_manager_status: 'pending'
                        }, {
                            is_admin: false
                        })
                    }.to raise_error(CandidatePositionInterest::OldVersion)
                end
            end

            describe "as admin" do
                it "should not raise if just updated_at is stale" do
                    instance = CandidatePositionInterest.first
                    instance.update_column(:hiring_manager_status, 'saved_for_later')
                    expect {
                        CandidatePositionInterest.update_from_hash!({
                            id: instance.id,
                            updated_at: instance.updated_at - 1.hour,
                            hiring_manager_status: 'pending'
                        }, {
                            is_admin: true
                        })
                    }.not_to raise_error
                end

                it "should not raise if hiring_manager_status or candidate_status are not in hash" do
                    instance = CandidatePositionInterest.first
                    instance.update_columns({
                        last_updated_at_by_non_admin: Time.now
                    })
                    expect {
                        CandidatePositionInterest.update_from_hash!({
                            id: instance.id,
                            last_updated_at_by_non_admin: instance.updated_at - 1.hour
                        }, {
                            is_admin: true
                        })
                    }.not_to raise_error
                end

                it "should raise if last_updated_at_by_non_admin is stale and hiring_manager_status is in hash" do
                    instance = CandidatePositionInterest.first
                    instance.update_columns({
                        hiring_manager_status: 'saved_for_later',
                        last_updated_at_by_non_admin: Time.now
                    })
                    expect {
                        CandidatePositionInterest.update_from_hash!({
                            id: instance.id,
                            last_updated_at_by_non_admin: instance.updated_at - 1.hour,
                            hiring_manager_status: 'pending'
                        }, {
                            is_admin: true
                        })
                    }.to raise_error(CandidatePositionInterest::OldVersion)
                end

                it "should raise if last_updated_at_by_non_admin is stale and candidate_status is in hash" do
                    instance = CandidatePositionInterest.first
                    instance.update_columns({
                        candidate_status: 'rejected',
                        last_updated_at_by_non_admin: Time.now
                    })
                    expect {
                        CandidatePositionInterest.update_from_hash!({
                            id: instance.id,
                            last_updated_at_by_non_admin: instance.updated_at - 1.hour,
                            candidate_status: 'accepted'
                        }, {
                            is_admin: true
                        })
                    }.to raise_error(CandidatePositionInterest::OldVersion)
                end
            end
        end
    end

    describe "merge_hash" do

        it "should set stuff" do
            instance = CandidatePositionInterest.create_from_hash!(valid_attrs)
            open_position_id = OpenPosition.first.id
            candidate_id = SecureRandom.uuid
            hiring_manager_reviewer_id = SecureRandom.uuid
            now = Time.now
            instance.merge_hash({
                candidate_id: candidate_id,
                open_position_id: open_position_id,
                candidate_status: 'rejected',
                hiring_manager_status: 'unseen',
                admin_status: 'outstanding',
                hiring_manager_priority: 5,
                hiring_manager_reviewer_id: hiring_manager_reviewer_id,
                last_updated_at_by_non_admin: now
            })
            expect(instance.candidate_id).to eq(candidate_id)
            expect(instance.open_position_id).to eq(open_position_id)
            expect(instance.candidate_status).to eq('rejected')
            expect(instance.hiring_manager_status).to eq('unseen')
            expect(instance.admin_status).to eq('outstanding')
            expect(instance.hiring_manager_priority).to eq(5.0)
            expect(instance.hiring_manager_reviewer_id).to eq(hiring_manager_reviewer_id)
            expect(instance.last_updated_at_by_non_admin.to_timestamp).to eq(now.to_timestamp)
        end
    end

    describe "reviewed_by_hiring_manager?" do
        it "should work" do
            interest = CandidatePositionInterest.first
            interest.update_column(:hiring_manager_status, CandidatePositionInterest.reviewed_hiring_manager_statuses.first)
            expect(interest.reload.reviewed_by_hiring_manager?).to be(true)
            interest.update_column(:hiring_manager_status, 'unseen')
            expect(interest.reload.reviewed_by_hiring_manager?).to be(false)
        end
    end

    describe "as_json" do
        it "should include fields" do
            instance = CandidatePositionInterest.create_from_hash!(valid_attrs)
            json = instance.as_json(fields: ['HIRING_MANAGER_REVIEWER_FIELDS'], career_profile_options: {fields: []})
            keys = json.keys
            expect(keys.include?('id')).to be(true)
            expect(keys.include?('hiring_manager_reviewer_name')).to be(true)
            expect(json['last_updated_at_by_non_admin']).to be(instance.last_updated_at_by_non_admin.to_timestamp)
        end

        it "should respect except" do
            instance = CandidatePositionInterest.create_from_hash!(valid_attrs)
            expect(instance.as_json(except: [:id], career_profile_options: {fields: []}).keys).not_to include("id")
        end

        it "should return hiring_manager_rejected properly" do
            instance = CandidatePositionInterest.first

            allow(instance).to receive(:hiring_manager_status).and_return('not_rejected')
            expect(instance.as_json(career_profile_options: {fields: []})['hiring_manager_rejected']).to be(false)

            allow(instance).to receive(:hiring_manager_status).and_return('rejected')
            expect(instance.as_json(career_profile_options: {fields: []})['hiring_manager_rejected']).to be(true)

            allow(instance).to receive(:admin_status).and_return('unreviewed')
            allow(instance).to receive(:hiring_manager_status).and_return('hidden')
            expect(instance.as_json(career_profile_options: {fields: []})['hiring_manager_rejected']).to be(false)

            allow(instance).to receive(:admin_status).and_return('reviewed')
            allow(instance).to receive(:hiring_manager_status).and_return('hidden')
            expect(instance.as_json(career_profile_options: {fields: []})['hiring_manager_rejected']).to be(true)
        end

        it "should pass options through to career_profile" do
            instance = CandidatePositionInterest.first
            career_profile = instance.career_profile
            expect(career_profile).to receive(:as_json).with({
                view: 'career_profiles',
                some: 'options'
            }).and_return('career_profile_json')
            expect(instance.as_json(career_profile_options: {some: 'options'})['career_profile']).to eq('career_profile_json')
        end

        it "should return HIRING_MANAGER_REVIEWER_FIELDS fields properly" do
            instance = CandidatePositionInterest.first
            reviewer = User.first
            reviewer.update_column(:avatar_url, 'http://foo.com/avatar')
            instance.update_column(:hiring_manager_reviewer_id, reviewer.id)
            json = instance.as_json({
                career_profile_options: {some: 'options'},
                fields: ['HIRING_MANAGER_REVIEWER_FIELDS']
            })
            expect(json['hiring_manager_reviewer_name']).to eq(reviewer.name)
            expect(json['hiring_manager_reviewer_avatar_url']).to eq(reviewer.avatar_url)
            expect(json['career_profile']).not_to be_nil
        end
    end

    describe "career_profile" do

        it "should expose candidate.career_profile" do
            expect(@candidate.career_profile).not_to be_nil
            instance = CandidatePositionInterest.create_from_hash!(valid_attrs, {}, @candidate)
            expect(instance.career_profile.id).not_to be_nil
            expect(instance.career_profile.id).to eq(@candidate.career_profile.id)
        end

    end

    describe "hide_from_hiring_manager!" do
        it "should set the interest to hiring_manager_status: hidden and admin_status: reviewed" do
            interest = CandidatePositionInterest.first
            interest.update_columns({
                admin_status: 'unreviewed',
                hiring_manager_status: 'unseen'
            })
            interest.hide_from_hiring_manager!
            expect(interest.admin_status).to eq('not_applicable')
            expect(interest.hiring_manager_status).to eq('hidden')
        end
    end

    describe "log_curated_event" do
        attr_accessor :interest

        before(:each) do
            @interest = CandidatePositionInterest.first
        end

        it "should call log_curated_event on update" do
            expect(interest).to receive(:log_curated_event)
            interest.save!
        end

        it "should not log an event if admin_status did not change" do
            expect(Event).not_to receive(:create_server_event!)
            interest.send(:log_curated_event)
        end

        it "should not log an event if hiring_manager_status is not 'unseen'" do
            expect(interest).to receive(:saved_change_to_admin_status?).and_return(true) # avoid false positives
            expect(interest).to receive(:admin_status).and_return('outstanding') # avoid false positives
            expect(interest).to receive(:hiring_manager_status).and_return('foobar')
            expect(Event).not_to receive(:create_server_event!)
            interest.send(:log_curated_event)
        end

        it "should log an event when an admin curates the interest" do
            expect(interest).to receive(:saved_change_to_admin_status?).and_return(true)
            expect(interest).to receive(:admin_status).and_return('outstanding')
            expect(interest).to receive(:hiring_manager_status).and_return('unseen')

            event = Event.new
            expect(Event).to receive(:create_server_event!).with(
                anything,
                interest.hiring_manager_id,
                'open_position:new_applicant',
                {
                    candidate_avatar_url: interest.candidate.avatar_url,
                    candidate_name: interest.candidate.name,
                    candidate_nickname: interest.candidate.nickname,
                    cover_letter: interest.cover_letter,
                    open_position_title: interest.open_position.title,
                    open_position_review_path: interest.open_position.review_path
                }
            ).and_return(event)
            expect(event).to receive(:log_to_external_systems).with(no_args)

            interest.send(:log_curated_event)
        end
    end

    def assert_attrs(obj, attrs)
        attrs = attrs.stringify_keys

        # find any keys that refer to associations and handle them specially
        attrs.each do |key, val|

            # if this key refers to an association instead of a vanilla attribute
            if obj.class.reflections.keys.include?(key)

                # turn it into an array even if it isn't so we can use the
                # same code for has_one and has_many relationships
                actual_list = Array.wrap(obj.send(key.to_sym))
                expected_list = Array.wrap(val)

                expect(actual_list.size).to eq(expected_list.size), "unexpected size for #{key.inspect}. #{actual_list.size} != #{expected_list.size}"

                actual_list.each_with_index do |actual_item, i|
                    assert_attrs(actual_item, expected_list[i])
                end

                # ensure we remove this key so as not to trip up the next expectation
                attrs.delete(key)
            end
        end

        actual_attrs = obj.attributes.slice(*attrs.keys.map(&:to_s)).stringify_keys
        expect(actual_attrs).to eq(attrs.stringify_keys)
    end

    def valid_attrs(attrs = {})
        {
            candidate_id: candidate.id,
            open_position_id: open_position.id,
            candidate_status: 'accepted',
            hiring_manager_status: 'unseen',
            last_updated_at_by_non_admin: Time.now
        }.with_indifferent_access.merge(attrs)
    end

    def get_unrelated_candidate_and_position
        candidate = User.joins(:cohort_applications).left_outer_joins(:candidate_position_interests).where("candidate_position_interests.id is null").first
        open_position = OpenPosition.first
        [candidate, open_position]
    end

    def ensure_fresh_hiring_team(hiring_manager)
        if hiring_manager.hiring_team
            hiring_manager.hiring_team.owner_id = nil
            hiring_manager.hiring_team.save!
        end
        hiring_manager.hiring_team = HiringTeam.create!({domain: 'freshteam.com', title: "Fresh Team"})
        hiring_manager.save!
    end

end
