# == Schema Information
#
# Table name: published_cohort_playlist_locale_packs
#
#  created_at              :datetime
#  cohort_id               :uuid
#  cohort_name             :text
#  required                :boolean
#  specialization          :boolean
#  foundations             :boolean
#  playlist_title          :text
#  playlist_locales        :text
#  playlist_locale_pack_id :uuid
#
require 'spec_helper'
require 'derived_content_table_spec_helper'

describe PublishedCohortPlaylistLocalePack do
    include DerivedContentTableSpecHelper
    fixtures(:cohorts)

    attr_accessor :cohort, :playlist

    before(:each) do
        @cohort = cohorts(:published_emba_cohort)
        @playlist = Playlist.find_by_locale_pack_id(cohort.specialization_playlist_pack_ids.first)
    end

    describe "write_new_records" do

        it "should write specialization playlist data" do
            specialization_pack_id = cohort.specialization_playlist_pack_ids[0]
            playlists = Playlist.all_published.where(locale_pack_id: specialization_pack_id)
            en_playlist = playlists.detect { |p| p.locale == 'en'}

            PublishedCohortPeriodsStreamLocalePack.delete_all
            PublishedCohortPeriodsStreamLocalePack.send(:write_new_records, :cohort_id, [cohort.id])

            record = PublishedCohortPlaylistLocalePack.where(cohort_id: cohort.id, playlist_locale_pack_id: specialization_pack_id).first
            expect(record).not_to be_nil
            expect(record.cohort_name).to eq(cohort.name)
            expect(record.required).to be(false)
            expect(record.specialization).to be(true)
            expect(record.foundations).to be(false)
            expect(record.playlist_title).to eq(en_playlist.title)
            expect(record.playlist_locales.split(',')).to match_array(playlists.map(&:locale))
        end

        it "should write foundations playlist data" do
            required_pack_id = cohort.required_playlist_pack_ids[0]
            playlists = Playlist.all_published.where(locale_pack_id: required_pack_id)
            en_playlist = playlists.detect { |p| p.locale == 'en'}

            PublishedCohortPeriodsStreamLocalePack.delete_all
            PublishedCohortPeriodsStreamLocalePack.send(:write_new_records, :cohort_id, [cohort.id])

            record = PublishedCohortPlaylistLocalePack.where(cohort_id: cohort.id, playlist_locale_pack_id: required_pack_id).first
            expect(record).not_to be_nil
            expect(record.cohort_name).to eq(cohort.name)
            expect(record.required).to be(true)
            expect(record.specialization).to be(false)
            expect(record.foundations).to be(true)
            expect(record.playlist_title).to eq(en_playlist.title)
            expect(record.playlist_locales.split(',')).to match_array(playlists.map(&:locale))
        end

        it "should write required, non-foundations playlist data" do
            required_pack_id = cohort.required_playlist_pack_ids[1]
            playlists = Playlist.all_published.where(locale_pack_id: required_pack_id)
            en_playlist = playlists.detect { |p| p.locale == 'en'}

            PublishedCohortPeriodsStreamLocalePack.delete_all
            PublishedCohortPeriodsStreamLocalePack.send(:write_new_records, :cohort_id, [cohort.id])

            record = PublishedCohortPlaylistLocalePack.where(cohort_id: cohort.id, playlist_locale_pack_id: required_pack_id).first
            expect(record).not_to be_nil
            expect(record.cohort_name).to eq(cohort.name)
            expect(record.required).to be(true)
            expect(record.specialization).to be(false)
            expect(record.foundations).to be(false)
            expect(record.playlist_title).to eq(en_playlist.title)
            expect(record.playlist_locales.split(',')).to match_array(playlists.map(&:locale))
        end

    end

    describe "update_on_content_change" do

        it "should replace rows for a cohort" do
            assert_replace_rows_for_content_item(PublishedCohortPlaylistLocalePack, cohort, :id, :cohort_id) do
                cohort.name = 'changed'
            end
        end

        it "should replace rows for a playlist" do
            assert_replace_rows_for_content_item(PublishedCohortPlaylistLocalePack, playlist, :locale_pack_id, :playlist_locale_pack_id) do
                playlist.title = 'testing title update'
            end
        end
    end
end
