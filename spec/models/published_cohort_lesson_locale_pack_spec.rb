# == Schema Information
#
# Table name: published_cohort_lesson_locale_packs
#
#  created_at            :datetime
#  cohort_id             :uuid
#  cohort_name           :text
#  foundations           :boolean
#  required              :boolean
#  specialization        :boolean
#  elective              :boolean
#  exam                  :boolean
#  test                  :boolean
#  assessment            :boolean
#  lesson_title          :text
#  lesson_locale_pack_id :uuid
#  lesson_locales        :text
#
require 'spec_helper'
require 'derived_content_table_spec_helper'

describe PublishedCohortLessonLocalePack do
    include DerivedContentTableSpecHelper

    fixtures(:cohorts, :lessons, :lesson_streams)

    attr_accessor :cohort, :stream

    before(:each) do
        allow(RefreshMaterializedContentViews).to receive(:force_refresh_views).and_return(true)
        @cohort = cohorts(:published_mba_cohort)
    end

    describe "write_new_records" do

        def stream_lpids_in_schedule
            cohort.periods.pluck('stream_entries').flatten.select {|e| e['required']}.pluck('locale_pack_id')
        end

        def stream_lpids_in_required_playlists
            Playlist.where(locale_pack_id: cohort.required_playlist_pack_ids).map(&:stream_locale_pack_ids).flatten
        end

        def get_lesson_not_in_cohort
            existing_lesson_locale_pack_ids = PublishedCohortLessonLocalePack.pluck('lesson_locale_pack_id')
            Lesson.all_published
                .where(locale: 'en')
                .where.not(locale_pack_id: existing_lesson_locale_pack_ids)
                .first
        end

        def get_lesson_only_in_schedule
            stream_lpid_only_in_schedule = (stream_lpids_in_schedule - stream_lpids_in_required_playlists).first
            stream_only_in_schedule = Lesson::Stream.where(locale: 'en', locale_pack_id: stream_lpid_only_in_schedule).first

            lesson_only_in_schedule = get_lesson_not_in_cohort
            stream_only_in_schedule.add_lesson_to_chapter!(lesson_only_in_schedule, stream_only_in_schedule.chapters[0])
            stream_only_in_schedule.publish!

            lesson_only_in_schedule
        end

        def get_lesson_only_in_required_playlist
            stream_lpid_only_in_required_playlist = (stream_lpids_in_required_playlists - stream_lpids_in_schedule).first
            stream_only_in_required_playlist = Lesson::Stream.where(locale: 'en', locale_pack_id: stream_lpid_only_in_required_playlist).first

            lesson_only_in_required_playlist = get_lesson_not_in_cohort
            stream_only_in_required_playlist.add_lesson_to_chapter!(lesson_only_in_required_playlist, stream_only_in_required_playlist.chapters[0])
            stream_only_in_required_playlist.publish!

            lesson_only_in_required_playlist
        end

        it "should write columns with info about lesson" do
            # find a lesson with multiple locales and ensure it is in the cohort
            es_lesson = lessons(:es_item)

            es_stream = lesson_streams(:es_item)

            if !es_stream.lessons.include?(es_lesson)
                es_stream.add_lesson_to_chapter!(es_lesson, es_stream.chapters[0])
                es_stream.publish!
            end

            cohort.periods[0]['stream_entries'] << {'locale_pack_id' => es_stream.locale_pack_id, 'required' => false}

            # publishing the cohort will trigger write_new_records
            expect(PublishedCohortLessonLocalePack).to receive(:write_new_records).and_call_original
            cohort.publish!

            # find the record for the lesson
            record = PublishedCohortLessonLocalePack.where(
                cohort_id: cohort.id,
                lesson_locale_pack_id: es_lesson.locale_pack_id
            ).first
            expect(record).not_to be_nil

            lessons = Lesson.where(locale_pack_id: es_lesson.locale_pack_id)
            en_lesson = lessons.detect { |str| str.locale = 'en' }

            expect(record).not_to be_nil
            expect(record.lesson_title).to eq(en_lesson.title)
            expect(record.lesson_locales.split(',')).to match_array(lessons.map(&:locale))
        end

        it "should write record for assessment lesson" do
            assert_flag_from_lesson_written(:assessment)
        end

        it "should write record for test lesson" do
            assert_flag_from_lesson_written(:test)
        end

        it "should write columns from published_cohort_stream_locale_packs" do
            assert_flag_from_stream_written(:elective)
            assert_flag_from_stream_written(:foundations)
            assert_flag_from_stream_written(:required)
            assert_flag_from_stream_written(:specialization)
            assert_flag_from_stream_written(:exam)
        end

        def assert_flag_from_lesson_written(col)
            existing_cohort_stream_lpids = PublishedCohortStreamLocalePack.pluck(:stream_locale_pack_id)
            lesson_lpid_for_col = PublishedStreamLessonLocalePack
                .where(col => true, stream_locale_pack_id: existing_cohort_stream_lpids)
                .first
                .lesson_locale_pack_id

            PublishedCohortLessonLocalePack.delete_all
            PublishedCohortLessonLocalePack.send(:write_new_records, :lesson_locale_pack_id, [lesson_lpid_for_col])

            record = PublishedCohortLessonLocalePack.where(lesson_locale_pack_id: lesson_lpid_for_col).first

            expect(record).not_to be_nil
            expect(record[col]).to be(true)
        end

        def assert_flag_from_stream_written(col)
            stream = PublishedCohortStreamLocalePack
                .where(cohort_id: cohort.id, col => true)
                .first

            lesson_lpid = Lesson::Stream
                    .published_lesson_locale_pack_ids(stream.stream_locale_pack_id)
                    .first


            PublishedCohortLessonLocalePack.delete_all
            PublishedCohortLessonLocalePack.send(:write_new_records, :lesson_locale_pack_id, [lesson_lpid])

            record = PublishedCohortLessonLocalePack.where(lesson_locale_pack_id: lesson_lpid).first

            expect(record).not_to be_nil
            expect(record[col]).to be(true), "Unexpectd value for #{col} column"
        end

    end

    describe "update_on_content_change" do

        it "should add rows for lessons in a stream when the stream is added to a playlist" do

            max_created_at = PublishedCohortLessonLocalePack.maximum('created_at')
            lesson_lpid, stream, playlist = add_stream_to_required_playlist_and_assert_row_created

            # check that only records for the added lesson have been updated
            modified_records = PublishedCohortLessonLocalePack.where('created_at > ?', max_created_at)
            expect(modified_records.map(&:lesson_locale_pack_id).uniq).to eq([lesson_lpid])

        end

        it "should remove rows for lessons in a stream when the stream is removed from a playlist" do
            lesson_lpid, stream, playlist = add_stream_to_required_playlist_and_assert_row_created

            max_created_at = PublishedCohortLessonLocalePack.maximum('created_at')

            playlist.stream_entries.reject! { |e| e['locale_pack_id'] == stream.locale_pack_id}
            playlist.publish!

            record = PublishedCohortLessonLocalePack.where(cohort_id: cohort.id, lesson_locale_pack_id: lesson_lpid).first
            expect(record).to be_nil

            # Since the only changes were to remove records, we don't expect to
            # find any modified records in the db
            modified_records = PublishedCohortLessonLocalePack.where('created_at > ?', max_created_at)
            expect(modified_records).to be_empty

        end

        def add_stream_to_required_playlist_and_assert_row_created
            playlist = Playlist.where(locale_pack_id: cohort.required_playlist_pack_ids, locale: 'en').first

            # find a stream that
            # 1. is not attached to any cohorts
            # 2. has only 1 lesson
            # 3. the lesson has no records in PublishedCohortLessonLocalePack
            existing_stream_locale_pack_ids = PublishedCohortStreamLocalePack.pluck('stream_locale_pack_id')
            existing_lesson_locale_pack_ids = PublishedCohortLessonLocalePack.pluck('lesson_locale_pack_id')
            stream = Lesson::Stream.all_published.where.not(locale_pack_id: existing_stream_locale_pack_ids).detect { |s|
                lesson_locale_pack_ids = Lesson::Stream.published_lesson_locale_pack_ids(s.locale_pack_id)
                lesson_locale_pack_ids.size == 1 &&
                !existing_lesson_locale_pack_ids.include?(lesson_locale_pack_ids.first)
            }
            lesson_lpid = Lesson::Stream.published_lesson_locale_pack_ids(stream.locale_pack_id).first

            playlist.stream_entries << {
                'locale_pack_id' => stream.locale_pack_id
            }
            playlist.publish!

            # check that a record was written for this stream in this cohort
            record =  PublishedCohortLessonLocalePack.where(cohort_id: cohort.id, lesson_locale_pack_id: lesson_lpid).first
            expect(record).not_to be_nil

            [lesson_lpid, stream, playlist]
        end

        it "should add rows when a lesson is added to a stream" do
            max_created_at = PublishedCohortLessonLocalePack.maximum('created_at')
            lesson_lpid, stream, playlist = add_stream_to_required_playlist_and_assert_row_created

            # check that only records for the added lesson have been updated
            modified_records = PublishedCohortLessonLocalePack.where('created_at > ?', max_created_at)
            expect(modified_records.map(&:lesson_locale_pack_id).uniq).to eq([lesson_lpid])

        end

        it "should remove rows when a lesson is removed from a stream" do
            lesson, stream = add_lesson_to_required_stream_and_assert_row_created

            max_created_at = PublishedCohortLessonLocalePack.maximum('created_at')

            stream.remove_lesson!(lesson)
            stream.publish!

            record = PublishedCohortLessonLocalePack.where(cohort_id: cohort.id, lesson_locale_pack_id: lesson.locale_pack_id).first
            expect(record).to be_nil

            # Since the only changes were to remove records, we don't expect to
            # find any modified records in the db
            modified_records = PublishedCohortLessonLocalePack.where('created_at > ?', max_created_at)
            expect(modified_records).to be_empty
        end

        def add_lesson_to_required_stream_and_assert_row_created
            existing_lesson_locale_pack_ids = PublishedCohortLessonLocalePack.pluck('lesson_locale_pack_id')
            lesson = Lesson.all_published
                .where(locale: 'en')
                .where.not(locale_pack_id: existing_lesson_locale_pack_ids)
                .first

            required_stream = Lesson::Stream.all_published
                .where(locale: 'en', locale_pack_id: cohort.get_required_stream_locale_pack_ids)
                .first

            required_stream.add_lesson_to_chapter!(lesson, required_stream.chapters[0])
            required_stream.publish!

            # check that a record was written for this lesson in this cohort
            record =  PublishedCohortLessonLocalePack.where(cohort_id: cohort.id, lesson_locale_pack_id: lesson.locale_pack_id).first
            expect(record).not_to be_nil

            [lesson, required_stream]
        end

        it "should replace rows for a lesson" do
            lesson = Lesson.find_by_locale_pack_id(PublishedCohortLessonLocalePack.pluck('lesson_locale_pack_id').first)
            assert_replace_rows_for_content_item(PublishedCohortLessonLocalePack, lesson, :locale_pack_id, :lesson_locale_pack_id) do
                lesson.title = 'changed'
            end
        end

        it "should replace rows for a cohort" do
            assert_replace_rows_for_content_item(PublishedCohortLessonLocalePack, cohort, :id, :cohort_id) do
                cohort.name = 'changed'
            end
        end
    end
end
