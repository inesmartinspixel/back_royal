require 'spec_helper'

describe AppConfig do

    describe "current" do

        it "should not error" do
            expect(Raven).not_to receive(:capture_exception)
            expect(Proc.new {
                AppConfig.current
            }).not_to raise_error
        end
    end
end