require 'spec_helper'

describe Ability do

    include StripeHelper
    fixtures(:lesson_streams, :users)

    describe "can load_full_content_for_lesson_id" do

        describe "with no user" do

            before(:each) do
                @ability = Ability.new(nil)
            end

            it "should be true with unrestricted lesson" do
                expect(@ability.can?(:load_full_content_for_lesson_id, {
                    stream: {},
                    lesson: {unrestricted: true}
                })).to be(true)
            end

            it "should be false with restricted lesson" do
                expect(@ability.can?(:load_full_content_for_lesson_id, {
                    stream: {},
                    lesson: {unrestricted: false}
                })).to be(false)
            end

        end

        describe "with institutional reports viewer" do

            before(:each) do
                @reports_viewer = users(:institutional_reports_viewer)
                @users_in_institution = @reports_viewer.views_reports_for_institutions.first.users
                @ability = Ability.new(@reports_viewer )
            end

            it "should be able to delete stream progress for a user in the institution" do
                stream_progress = Lesson::StreamProgress.where(user_id: @users_in_institution.map(&:id)).first
                expect(stream_progress).not_to be_nil
                expect(@ability.can?(:destroy, stream_progress)).to be(true)
            end

            it "should not be able to delete stream progress for a user not in the institution" do
                stream_progress = Lesson::StreamProgress.where.not(user_id: @users_in_institution.map(&:id)).first
                expect(stream_progress).not_to be_nil
                expect(@ability.can?(:destroy, stream_progress)).to be(false)
            end

        end

    end

    describe "can destroy cohort_application" do
        it "should only allow destroying an application if cohort.user_can_destroy_application?" do
            application = CohortApplication.first
            expect(application.published_cohort_or_cohort).to receive(:user_can_destroy_application?).and_return(true)
            @ability = Ability.new(application.user)
            expect(@ability.can?(:destroy, application)).to be(true)
        end

        it "should not allow destroying an application if it is not cohort.user_can_destroy_application?" do
            application = CohortApplication.first
            expect(application.published_cohort_or_cohort).to receive(:user_can_destroy_application?).and_return(false)
            @ability = Ability.new(application.user)
            expect(@ability.can?(:destroy, application)).to be(false)
        end

        it "should allow admins to destroy an application" do
            user = users('admin')
            expect(user.ability.can?(:destroy, CohortApplication)).to be(true)
        end
    end

    describe "index_hiring_relationships" do

        def learner
            @learner ||= users(:learner)
        end

        def hiring_manager
            @hiring_manager ||= users(:hiring_manager_with_team)
        end

        def allowed_filters_for_learner
            {
                candidate_id: learner.id,
                candidate_status_not: 'hidden'
            }
        end

        def allowed_filters_for_hiring_manager
            {
                hiring_manager_id: hiring_manager.id,
                hiring_manager_status_not: 'hidden',
                viewable_by_hiring_manager: true
            }
        end

        it "should allow allowed params for learner" do
            assert_permissions_for_filters(learner, true, allowed_filters_for_learner)
        end

        it "should allow allowed params for hiring_manager" do
            assert_permissions_for_filters(hiring_manager, true, allowed_filters_for_hiring_manager)
        end

        it "should allow a hiring_manager to request relationships for a specific candidate" do
            assert_permissions_for_filters(hiring_manager, true, allowed_filters_for_hiring_manager.merge({
                candidate_id: SecureRandom.uuid
            }))
        end

        it "should disallow requesting a candidate_id that is not the current user's" do
            assert_permissions_for_filters(learner, false, allowed_filters_for_learner.merge({
                candidate_id: SecureRandom.uuid
            }))
        end

        it "should disallow requesting a hiring_manager_id that is not the current user's" do
            assert_permissions_for_filters(hiring_manager, false, allowed_filters_for_hiring_manager.merge({
                hiring_manager_id: SecureRandom.uuid
            }))
        end

        it "should disallow requesting a hiring_team_id that is not the current user's" do
            assert_permissions_for_filters(hiring_manager, false, allowed_filters_for_hiring_manager.merge({
                hiring_team_id: SecureRandom.uuid
            }))

            assert_permissions_for_filters(hiring_manager, true, allowed_filters_for_hiring_manager.merge({
                hiring_team_id: hiring_manager.hiring_team_id
            }))
        end

        it "disallow requesting without a candidate_id, hiring_manager_id, or hiring_team_id filter" do
            assert_permissions_for_filters(learner, false, allowed_filters_for_learner.merge({
                candidate_id: nil
            }))
        end

        it "should disallow anonymous user" do
            assert_permissions_for_filters(nil, false, {})
        end

        it "should disallow requesting without filtering out hidden relationships" do
            assert_permissions_for_filters(learner, false, allowed_filters_for_learner.merge({
                candidate_status_not: nil
            }))

            assert_permissions_for_filters(hiring_manager, false, allowed_filters_for_hiring_manager.merge({
                hiring_manager_status_not: nil
            }))
        end

        it "should allow requesting with a list of statuses that does not include hidden" do
            assert_permissions_for_filters(learner, true, allowed_filters_for_learner.merge({
                candidate_status_not: nil,
                candidate_status: ['pending']
            }))

            assert_permissions_for_filters(hiring_manager, true, allowed_filters_for_hiring_manager.merge({
                hiring_manager_status_not: nil,
                hiring_manager_status: ['pending']
            }))

            assert_permissions_for_filters(learner, false, allowed_filters_for_learner.merge({
                candidate_status_not: nil,
                candidate_status: ['hidden', 'pending']
            }))

            assert_permissions_for_filters(hiring_manager, false, allowed_filters_for_hiring_manager.merge({
                hiring_manager_status_not: nil,
                hiring_manager_status: ['hidden', 'pending']
            }))
        end

        it "should allow requesting without filtering out hidden relationships if only asking for ids" do
            assert_permissions_for_filters(learner, true, allowed_filters_for_learner.merge({
                candidate_status_not: nil
            }), {
                fields: ['id', 'hiring_manager_status', 'candidate_status']
            })

            assert_permissions_for_filters(hiring_manager, true, allowed_filters_for_hiring_manager.merge({
                hiring_manager_status_not: nil
            }), {
                fields: ['id', 'hiring_manager_status', 'candidate_status']
            })
        end

        it "should disallow hiring manager filtering without viewable_by_hiring_manager" do
            assert_permissions_for_filters(hiring_manager, false, allowed_filters_for_hiring_manager.merge({
                viewable_by_hiring_manager: nil
            }))
        end

        it "should 401 if hiring manager filtering for hiring_team without viewable_by_hiring_manager" do
             assert_permissions_for_filters(hiring_manager, false, allowed_filters_for_hiring_manager.merge({
                hiring_manager_id: nil,
                hiring_team_id: hiring_manager.hiring_team_id,
                viewable_by_hiring_manager: false
            }))
        end

        def assert_permissions_for_filters(user, can, filters, other_params = {})
            ability = Ability.new(user)
            expect(ability.can?(:index_hiring_relationships, other_params.merge({
                filters: filters
            }))).to be(can)
        end
    end

    describe "update hiring_relationship" do

        it "should be false if this is not the hiring manager or the candidate" do
            user = users(:learner)
            expect(user).not_to receive(:hiring_team)
            expect(user.can?(:update, HiringRelationship.new)).to eq(false)
        end

        describe "as candidate" do
            it "should be true" do
                user = users(:learner)
                expect(user).not_to receive(:hiring_team)
                expect(user.can?(:update, HiringRelationship.new(candidate_id: user.id))).to eq(true)
            end
        end

        describe "as teammate" do
            it "should pay attention to has_full_access" do
                user = users(:hiring_manager_with_team)
                teammate = users(:hiring_manager_teammate)

                has_full_access=true
                expect(user.hiring_team).to receive(:has_full_access?).at_least(1).times do
                    has_full_access
                end

                expect(user.can?(:update, HiringRelationship.new({
                    hiring_manager_id: teammate.id,
                    hiring_manager_status: 'pending'
                }))).to eq(true)

                has_full_access=false
                expect(user.can?(:update, HiringRelationship.new({
                    hiring_manager_id: user.id,
                    hiring_manager_status: 'pending'
                }))).to eq(false)
            end

            it "should pay attention to the current hiring_manager_status" do
                user = users(:hiring_manager_with_team)
                teammate = users(:hiring_manager_teammate)
                expect(user.hiring_team).to receive(:has_full_access?).at_least(1).times.and_return(false)
                expect(user.can?(:update, HiringRelationship.new({
                    hiring_manager_id: teammate.id,
                    hiring_manager_status: 'pending'
                }))).to eq(false)
                expect(user.can?(:update, HiringRelationship.new({
                    hiring_manager_id: user.id,
                    hiring_manager_status: 'accepted'
                }))).to eq(true)
            end

            it "should pay attention to is_freemium_candidate?" do
                user = users(:hiring_manager_with_team)
                teammate = users(:hiring_manager_teammate)
                expect(user.hiring_team).to receive(:has_full_access?).at_least(1).times.and_return(false)
                candidate_id = SecureRandom.uuid
                expect(user.hiring_team).to receive(:is_freemium_candidate?) do |_candidate_id|
                    candidate_id == _candidate_id
                end.at_least(1).times

                expect(user.can?(:update, HiringRelationship.new({
                    hiring_manager_id: teammate.id,
                    hiring_manager_status: 'pending',
                    candidate_id: SecureRandom.uuid
                }))).to eq(false)
                expect(user.can?(:update, HiringRelationship.new({
                    hiring_manager_id: teammate.id,
                    hiring_manager_status: 'pending',
                    candidate_id: candidate_id
                }))).to eq(true)
            end

            it "should allow for updating one's own relationship" do

                user = users(:hiring_manager_with_team)

                has_full_access=true
                expect(user.hiring_team).to receive(:has_full_access?).and_return(true)

                expect(user.can?(:update, HiringRelationship.new({
                    hiring_manager_id: user.id,
                    hiring_manager_status: 'pending'
                }))).to eq(true)
            end
        end

    end

    describe "index_cohorts" do
        before(:each) do
            @ability = Ability.new(users('learner'))
        end

        def allowed_params
            {filters: {published: true, promoted: true}}
        end

        it "should work with allowed params" do
            expect(@ability.can?(:index_cohorts, allowed_params)).to be(true)
        end

        it "should require non-admins to pass promoted=true" do
            params = allowed_params
            params[:filters].delete(:promoted)
            expect(@ability.can?(:index_cohorts, params)).to be(false)
        end

        it "should require non-admins to pass published=true" do
            params = allowed_params
            params[:filters].delete(:published)
            expect(@ability.can?(:index_cohorts, params)).to be(false)
        end

        it "should not allow regular users to pass in ADMIN_FIELDS" do
            params = allowed_params
            params[:fields] = ['ADMIN_FIELDS']
            expect(@ability.can?(:index_cohorts, params)).to be(false)
        end

        it "should not allow regular users to ask for cohort_promotions" do
            params = allowed_params
            params[:get_cohort_promotions] = true
            expect(@ability.can?(:index_cohorts, params)).to be(false)
        end

        it "should not allow regular users to ask for stripe metadata" do
            params = allowed_params
            params[:get_stripe_metadata] = true
            expect(@ability.can?(:index_cohorts, params)).to be(false)
        end
    end

    describe "show_deferral_link" do
        attr_accessor :learner, :deferral_link

        before(:each) do
            @learner = users(:accepted_emba_cohort_user)
            @deferral_link = DeferralLink.create!(
                user_id: learner.id,
                expires_at: Time.now + 30.days,
                from_cohort_application_id: learner.accepted_application.id
            )
        end

        it "should show deferral link if it is for the user" do
            ability = Ability.new(learner)
            expect(ability.can?(:show_deferral_link, deferral_link)).to be(true)
        end

        it "should not show deferral link if it is not for the user" do
            ability = Ability.new(User.where.not(id: learner.id).joins(:roles).where("roles.name = 'learner'").first)
            expect(ability.can?(:show_deferral_link, deferral_link)).to be(false)
        end
    end

    describe "update_deferral_link" do
        attr_accessor :learner, :ability, :deferral_link, :sample_params

        before(:each) do
            @learner = users(:accepted_emba_cohort_user)
            @ability = Ability.new(learner)
        end

        def mock_deferral_state(to_cohort_id = nil)
            @deferral_link = DeferralLink.create!(
                user_id: learner.id,
                expires_at: Time.now + 30.days,
                from_cohort_application_id: learner.accepted_application.id
            )

            mock_upcoming_cohorts = Cohort.where.not(id: learner.accepted_application.cohort_id).limit(3)
            allow(deferral_link).to receive(:upcoming_cohorts).and_return(mock_upcoming_cohorts.to_a)

            @sample_params = {
                meta: {
                    to_cohort_id: to_cohort_id || mock_upcoming_cohorts.pluck(:id).first
                },
                record: {
                    id: deferral_link.id,
                    user_id: learner.id
                }
            }
        end

        it "should work when the user is trying to use an active link for themselves to defer to a cohort" do
            mock_deferral_state
            allow(deferral_link).to receive(:active?).and_return(true)
            expect(ability.can?(:update_deferral_link, sample_params, deferral_link)).to be(true)
        end

        it "should not work if the link is inactive" do
            mock_deferral_state
            allow(deferral_link).to receive(:active?).and_return(false)
            expect(ability.can?(:update_deferral_link, sample_params, deferral_link)).to be(false)
        end

        it "should not work if the link is not for the user" do
            mock_deferral_state
            allow(deferral_link).to receive(:active?).and_return(true)
            params = sample_params[:record].merge(user_id: SecureRandom.uuid)
            expect(ability.can?(:update_deferral_link, params, deferral_link)).to be(false)
        end

        it "should not work if to_cohort_id is not upcoming" do
            mock_deferral_state(to_cohort_id: SecureRandom.uuid)
            allow(deferral_link).to receive(:active?).and_return(true)
            expect(ability.can?(:update_deferral_link, sample_params, deferral_link)).to be(false)
        end

        it "should not work for admin if it is not for their user" do
            mock_deferral_state
            admin = users(:admin)
            ability = Ability.new(admin)
            expect(ability.can?(:update_deferral_link, sample_params, deferral_link)).to be(false)
        end
    end

    describe "can create_hiring_relationships" do

        before(:each) do
            @user = users('hiring_manager')
            @ability = Ability.new(@user)
        end

        it "should return false if no hiring_manager_id" do
            expect(@user.ability.can?(:create_hiring_relationships, {})).to be(false)
        end

        it "should return false if hiring_manager_id doesn't match user's id" do
            other_user = users('pending_hiring_manager')
            expect(@user.ability.can?(:create_hiring_relationships, {
                hiring_manager_id: other_user.id
            })).to be(false)
        end

        it "should return false if user doesn't have a hiring application, i.e. is not a hiring manager" do
            @user = users('user_without_hiring_application')
            expect(@user.ability.can?(:create_hiring_relationships, {
                hiring_manager_id: @user.id
            })).to be(false)
        end

        it "should return false if hiring manager does not have full access" do
            expect(@user.hiring_team).to receive(:has_full_access?).and_return(false)
            expect(@user.hiring_team).to receive(:is_freemium_candidate?).and_return(false)
            expect(@user.ability.can?(:create_hiring_relationships, {
                hiring_manager_id: @user.id
            })).to be(false)
        end

        it "should return true if no full access but the user is from one of the non-anonymized candidate position interests" do
            candidate_id = SecureRandom.uuid
            expect(@user.hiring_team).to receive(:is_freemium_candidate?).with(candidate_id).and_return(true)
            expect(@user.hiring_team).to receive(:has_full_access?).and_return(false)
            expect(@user.ability.can?(:create_hiring_relationships, {
                hiring_manager_id: @user.id,
                candidate_id: candidate_id
            })).to be(true)
        end

        it "should return false if the user's application is not accepted and not pending_pay_per_post?" do
            expect(@user).to receive(:pending_pay_per_post?).and_return(false)
            expect(@user.hiring_team).not_to receive(:has_full_access?)
            expect(@user.hiring_application).to receive(:status).and_return('pending')
            expect(@user.ability.can?(:create_hiring_relationships, {
                hiring_manager_id: @user.id
            })).to be(false)
        end

        it "should return true if the user is a hiring manager and the hiring_manager_id matches the user's" do
            expect(@user.hiring_team).to receive(:has_full_access?).and_return(true)
            expect(@user.ability.can?(:create_hiring_relationships, {
                hiring_manager_id: @user.id
            })).to be(true)
        end

    end

    describe "view_as" do
        it "should allow admin user to view_as any group" do
            user = users('admin')
            expect(user.ability.can?(:read_streams, {
                filters: {
                    view_as: 'group:foo'
                }
            })).to be(true)
        end

        it "should allow regular user to view_as for promoted cohorts" do
            user = users('learner')
            expect(user.ability.can?(:read_streams, {
                filters: {
                    view_as: 'cohort:PROMOTED_DEGREE'
                }
            })).to be(true)

            expect(user.ability.can?(:read_streams, {
                filters: {
                    view_as: 'group:foo'
                }
            })).to be(false)
        end

        it "should allow anonymous user to view_as for promoted cohorts" do
            ability = Ability.new(nil)

            expect(ability.can?(:read_streams, {
                filters: {
                    view_as: 'cohort:PROMOTED_DEGREE'
                }
            })).to be(true)
            expect(ability.can?(:read_streams, {
                filters: {
                    view_as: 'group:foo'
                }
            })).to be(false)
        end
    end

    describe "index_using_career_profiles_controller" do

        it "should defer to index_career_profiles if view='career_profiles'" do
            user = users(:learner)
            ability = Ability.new(user)

            # it is hard to mock out can_index_career_profiles? here,
            # so we set up real situations.

            good_params = {filters: {user_id: user.id}}
            expect(ability.can?(:index_career_profiles, good_params)).to be(true) # sanity check
            expect(ability.can?(:index_using_career_profiles_controller, good_params.merge(view: 'career_profiles'))).to be(true)

            bad_params = {}
            expect(ability.can?(:index_career_profiles, bad_params)).to be(false) # sanity check
            expect(ability.can?(:index_career_profiles, bad_params.merge(view: 'career_profiles'))).to be(false)
        end

        it "should defer to can_index_student_network_profiles if view='student_network_profiles'" do
            user = users(:learner)
            ability = Ability.new(user)
            can_index_student_network_profiles = nil
            expect(user).to receive(:has_student_network_access?) {
                can_index_student_network_profiles
            }.at_least(1).times

            # it is hard to mock out index_student_network_profiles? here,
            # so we set up real situations.

            can_index_student_network_profiles = true
            expect(ability.can?(:index_student_network_profiles, {})).to be(true) # sanity check
            expect(ability.can?(:index_using_career_profiles_controller, {view: 'student_network_profiles'})).to be(true)

            can_index_student_network_profiles = false
            expect(ability.can?(:index_student_network_profiles, {})).to be(false) # sanity check
            expect(ability.can?(:index_student_network_profiles,{view: 'career_profiles'})).to be(false)
        end

        it "should disallow other views" do
            user = users(:learner)
            ability = Ability.new(user)
            expect(ability.can?(:index_using_career_profiles_controller, {})).to be(false)
            expect(ability.can?(:index_using_career_profiles_controller, {view: 'unknown'})).to be(false)
            expect(ability.can?(:index_using_career_profiles_controller, {view: 'editable'})).to be(false)
        end

        it "should only allow certain hiring_plans" do
            user = User.joins(:hiring_team).first

            allow(user).to receive(:has_accepted_hiring_manager_access?).and_return(true)
            good_params = {view: 'career_profiles', filters: {available_for_relationships_with_hiring_manager: user.id}}
            ability = Ability.new(user)

            user.hiring_team.hiring_plan = HiringTeam::HIRING_PLAN_LEGACY
            expect(ability.can?(:index_using_career_profiles_controller, good_params)).to be(true)

            user.hiring_team.hiring_plan = HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING
            user.hiring_team.subscriptions.destroy_all
            expect(ability.can?(:index_using_career_profiles_controller, good_params)).to be(false)

            user.hiring_team.subscriptions = [Subscription.first]
            user.hiring_team.save!
            expect(ability.can?(:index_using_career_profiles_controller, good_params)).to be(true)

            user.hiring_team.hiring_plan = HiringTeam::HIRING_PLAN_PAY_PER_POST
            expect(ability.can?(:index_using_career_profiles_controller, good_params)).to be(false)
        end

    end

    describe "index_student_network_profiles" do
        it "should allow if has_student_network_access?" do
            user = users(:learner)
            ability = Ability.new(user)
            can_index_student_network_profiles = nil
            expect(user).to receive(:has_student_network_access?) {
                can_index_student_network_profiles
            }.at_least(1).times

            can_index_student_network_profiles = true
            expect(ability.can?(:index_student_network_profiles, {})).to be(true)

            can_index_student_network_profiles = false
            expect(ability.can?(:index_student_network_profiles, {})).to be(false) # sanity check
        end
    end

    describe "index_career_profiles" do
        it "should allow a user to only view themself" do
            user = users(:learner)
            ability = Ability.new(user)
            expect(ability.can?(:index_career_profiles, {filters: {user_id: user.id}})).to be(true)
            expect(ability.can?(:index_career_profiles, {})).to be(false)
            expect(ability.can?(:index_career_profiles, {filters: {user_id: SecureRandom.uuid}})).to be(false)
        end

        describe "available_for_relationships_with_hiring_manager" do
            attr_accessor :user, :ability

            def assert_when_using_available
                expect(ability.can?(:index_career_profiles, {filters: {available_for_relationships_with_hiring_manager: user.id}})).to be(true)
                expect(ability.can?(:index_career_profiles, {filters: {available_for_relationships_with_hiring_manager: SecureRandom.uuid}})).to be(false)
                expect(ability.can?(:index_career_profiles, {})).to be(false)
            end

            before(:each) do
                @user = HiringApplication.joins(user: :hiring_team).where(status: 'accepted').first.user
                @ability = Ability.new(user)
            end

            it "should allow an accepted subscribed unlimited-with-sourcing hiring manager to view active career profiles" do
                user.hiring_team.hiring_plan = HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING
                user.hiring_team.subscriptions = [Subscription.first]
                assert_when_using_available
            end

            it "should allow a hiring manager whose team does not require subscription" do
                user.hiring_team.update_columns({
                    hiring_plan: HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING,
                    subscription_required: false
                })
                user.hiring_team.subscriptions.destroy_all
                assert_when_using_available
            end

            it "should allow an accepted legacy hiring manager to view active career profiles" do
                user.hiring_team.hiring_plan = HiringTeam::HIRING_PLAN_LEGACY
                assert_when_using_available
            end
        end
    end

    describe "index_student_network_map" do

        it "should allow a pending student to view student network map (anonymous)" do
            user = CohortApplication.where(status: 'pending').first.user
            ability = Ability.new(user)
            expect(ability.can?(:index_student_network_maps, {})).to be(true)
        end

        it "should allow an accepted student to view student network maps" do
            user = CohortApplication.where(status: 'accepted').first.user
            ability = Ability.new(user)
            expect(ability.can?(:index_student_network_maps, {})).to be(true)
        end
    end

    describe "create_student_network_messages" do
        attr_accessor :user, :message_recipient, :ability

        before(:each) do
            @user = users(:learner)
            @message_recipient = users(:admin)
            @ability = Ability.new(user)
        end

        it "should return false if user !has_full_student_network_access?" do
            expect(user).to receive(:has_full_student_network_access?).and_return(false)
            expect(ability.can?(:create_student_network_messages, {reply_to: 'foo.bar@example.com', subject: 'Foo', message_body: 'Bar'}, message_recipient)).to be(false)
        end

        it "should return false if message_recipient !can_receive_student_network_messages?" do
            expect(user).to receive(:has_full_student_network_access?).and_return(true)
            expect(message_recipient).to receive(:can_receive_student_network_messages?).and_return(false)
            expect(ability.can?(:create_student_network_messages, {reply_to: 'foo.bar@example.com', subject: 'Foo', message_body: 'Bar'}, message_recipient)).to be(false)
        end

        it "should return false if not all message fields are present" do
            expect(user).to receive(:has_full_student_network_access?).and_return(true)
            expect(message_recipient).to receive(:can_receive_student_network_messages?).and_return(true)
            expect(ability.can?(:create_student_network_messages, {reply_to: 'foo.bar@example.com', subject: 'Foo'}, message_recipient)).to be(false)
        end

        it "should return true if all checks are met" do
            expect(user).to receive(:has_full_student_network_access?).and_return(true)
            expect(message_recipient).to receive(:can_receive_student_network_messages?).and_return(true)
            expect(ability.can?(:create_student_network_messages, {reply_to: 'foo.bar@example.com', subject: 'Foo', message_body: 'Bar'}, message_recipient)).to be(true)
        end
    end

    describe "index_student_network_events" do

        attr_accessor :user, :ability

        before(:each) do
            @user = User.joins(:roles).where("roles.name = 'learner'").first
            @ability = Ability.new(user)
        end

        it "should return false if params[:fields] includes 'ADMIN_FIELDS' and user is not an admin" do
            expect(user).to receive(:is_admin?).and_return(false)
            expect(ability.can?(:index_student_network_events, {fields: ["ADMIN_FIELDS"]})).to be(false)
        end

        it "should return true if params[:fields] includes 'ADMIN_FIELDS' and user is an admin" do
            expect(user).to receive(:is_admin?).and_return(true)
            expect(ability.can?(:index_student_network_events, {fields: ["ADMIN_FIELDS"]})).to be(true)
        end

        it "should return true otherwise" do
            expect(ability.can?(:index_student_network_events, {})).to be(true)
        end
    end

    describe "index_open_positions" do

        it "should allow a user to index his own positions" do
            user = users(:learner)
            ability = Ability.new(user)
            expect(ability.can?(:index_open_positions, {filters: {hiring_manager_id: user.id}})).to be(true)
            expect(ability.can?(:index_open_positions, {})).to be(false)
            expect(ability.can?(:index_open_positions, {filters: {hiring_manager_id: SecureRandom.uuid}})).to be(false)
        end

        it "should allow a user to index his team's positions" do
            user = users(:hiring_manager_with_team)
            ability = Ability.new(user)
            expect(ability.can?(:index_open_positions, {filters: {hiring_team_id: user.hiring_team_id}})).to be(true)
            expect(ability.can?(:index_open_positions, {})).to be(false)
            expect(ability.can?(:index_open_positions, {filters: {hiring_team_id: SecureRandom.uuid}})).to be(false)
        end

        it "should allow a user where can_edit_career_profile=true to index featured, non-archived, hiring_manager_might_be_interested_in positions" do
            user = users(:learner)
            user.can_edit_career_profile = true
            user.save!
            ability = Ability.new(user)
            expect(ability.can?(:index_open_positions, {filters: {featured: true, archived: false}})).to be(false)
            expect(ability.can?(:index_open_positions, {filters: {featured: true, archived: false, hiring_manager_might_be_interested_in: SecureRandom.uuid}})).to be(false)
            expect(ability.can?(:index_open_positions, {filters: {featured: true, archived: false, hiring_manager_might_be_interested_in: user.id, recommended_for: SecureRandom.uuid}})).to be(false)
            expect(ability.can?(:index_open_positions, {filters: {featured: true, archived: false, hiring_manager_might_be_interested_in: user.id}})).to be(true)
            expect(ability.can?(:index_open_positions, {filters: {featured: true, archived: false, hiring_manager_might_be_interested_in: user.id, recommended_for: user.id}})).to be(true)
            expect(ability.can?(:index_open_positions, {})).to be(false)
            expect(ability.can?(:index_open_positions, {filters: {featured: false, archived: false}})).to be(false)
            expect(ability.can?(:index_open_positions, {filters: {featured: true, archived: true}})).to be(false)
        end

        it "should allow a user where can_edit_career_profile=true to index featured, non-archived, recommended_for positions" do
            user = users(:learner)
            user.can_edit_career_profile = true
            user.save!
            ability = Ability.new(user)
            expect(ability.can?(:index_open_positions, {filters: {featured: true, archived: false}})).to be(false)
            expect(ability.can?(:index_open_positions, {filters: {featured: true, archived: false, recommended_for: SecureRandom.uuid}})).to be(false)
            expect(ability.can?(:index_open_positions, {filters: {featured: true, archived: false, recommended_for: user.id, hiring_manager_might_be_interested_in: SecureRandom.uuid}})).to be(false)
            expect(ability.can?(:index_open_positions, {filters: {featured: true, archived: false, recommended_for: user.id}})).to be(true)
            expect(ability.can?(:index_open_positions, {filters: {featured: true, archived: false, recommended_for: user.id, hiring_manager_might_be_interested_in: user.id}})).to be(true)
            expect(ability.can?(:index_open_positions, {})).to be(false)
            expect(ability.can?(:index_open_positions, {filters: {featured: false, archived: false}})).to be(false)
            expect(ability.can?(:index_open_positions, {filters: {featured: true, archived: true}})).to be(false)
        end

        it "should not allow a user where can_edit_career_profile=false to index featured and non-archived positions" do
            user = users(:learner)
            user.can_edit_career_profile = false
            user.save!
            ability = Ability.new(user)
            expect(ability.can?(:index_open_positions, {filters: {featured: true, archived: false}})).to be(false)
            expect(ability.can?(:index_open_positions, {})).to be(false)
            expect(ability.can?(:index_open_positions, {filters: {featured: false, archived: false}})).to be(false)
            expect(ability.can?(:index_open_positions, {filters: {featured: true, archived: true}})).to be(false)
        end

        it "should not allow regular users to pass in ADMIN_FIELDS" do
            user = users(:learner)
            ability = Ability.new(user)
            expect(ability.can?(:index_open_positions, {fields: ['ADMIN_FIELDS']})).to be(false)
        end
    end

    describe "create_or_update_open_positions" do

        it "should allow a user to update their own position" do
            user = users(:hiring_manager)
            ability = Ability.new(user)
            expect(ability.can?(:create_or_update_open_positions, {record: {hiring_manager_id: user.id}})).to be(true)
            expect(ability.can?(:create_or_update_open_positions, {})).to be(false)
            expect(ability.can?(:create_or_update_open_positions, {record: {hiring_manager_id: SecureRandom.uuid}})).to be(false)
        end

        it "should allow a user to update their teammate's position" do
            hiring_manager = users(:hiring_manager_with_team)
            teammate = users(:hiring_manager_teammate)
            ability = Ability.new(teammate)
            expect(ability.can?(:create_or_update_open_positions, {record: {hiring_manager_id: hiring_manager.id}})).to be(true)
            expect(ability.can?(:create_or_update_open_positions, {})).to be(false)
            expect(ability.can?(:create_or_update_open_positions, {record: {hiring_manager_id: SecureRandom.uuid}})).to be(false)
        end

        it "should allow a user to update their own position when passed in the meta" do
            user = users(:hiring_manager_with_team)
            ability = Ability.new(user)
            expect(ability.can?(:create_or_update_open_positions, {meta: {open_position: {hiring_manager_id: user.id}}})).to be(true)
            expect(ability.can?(:create_or_update_open_positions, {})).to be(false)
            expect(ability.can?(:create_or_update_open_positions, {meta: {open_position: {hiring_manager_id: SecureRandom.uuid}}})).to be(false)
        end

        it "should not allow a pay_per_post user to create a published a position without a subscription" do
            open_position = OpenPosition.joins(:hiring_team => :owner)
                                .where(featured: false).first
            user = open_position.hiring_manager
            user.hiring_team.hiring_plan = HiringTeam::HIRING_PLAN_PAY_PER_POST
            ability = Ability.new(user)
            expect(ability.can?(:create_or_update_open_positions, {meta: {open_position: {hiring_manager_id: user.id, featured: true}}})).to be(false)
        end

        it "should not allow a pay_per_post user to unarchive a featured a position without a subscription" do
            open_position = OpenPosition.joins(:hiring_team => :owner)
                                .where(featured: true).first
            open_position.update_column(:archived, true)
            user = open_position.hiring_manager
            user.hiring_team.hiring_plan = HiringTeam::HIRING_PLAN_PAY_PER_POST
            ability = Ability.new(user)
            json = open_position.as_json.merge('archived' => false)
            expect(ability.can?(:create_or_update_open_positions, {meta: {open_position: json}})).to be(false)
        end

        it "should allow a pay_per_post user to publish an unpublished position with a subscription" do
            open_position = OpenPosition.joins(:hiring_team => :owner)
                                .where(featured: false).first
            create_customer_with_default_source(open_position.hiring_team)
            open_position.subscription = create_subscription(open_position.hiring_team, pay_per_post_plan.id, nil, {open_position_id: open_position.id})
            open_position.save!
            user = open_position.hiring_manager
            user.hiring_team.hiring_plan = HiringTeam::HIRING_PLAN_PAY_PER_POST

            ability = Ability.new(user)
            json = open_position.as_json.merge('featured' => true)
            expect(ability.can?(:create_or_update_open_positions, {meta: {open_position: json}})).to be(true)
        end

        it "should allow a pay_per_post user to unarchive an archived featured position with a subscription" do
            open_position = OpenPosition.joins(:hiring_team => :owner)
                                .where(featured: true).first
            open_position.update_column(:archived, true)
            create_customer_with_default_source(open_position.hiring_team)
            open_position.subscription = create_subscription(open_position.hiring_team, pay_per_post_plan.id, nil, {open_position_id: open_position.id})
            open_position.save!
            user = open_position.hiring_manager
            user.hiring_team.hiring_plan = HiringTeam::HIRING_PLAN_PAY_PER_POST

            ability = Ability.new(user)
            json = open_position.as_json.merge('archived' => false)
            expect(ability.can?(:create_or_update_open_positions, {meta: {open_position: json}})).to be(true)
        end

        it "should allow a pay_per_post user to edit a published position that has a subscription" do
            open_position = OpenPosition.joins(:hiring_team)
                                .left_outer_joins(:subscription)
                                .where("subscriptions.id is null")
                                .where(featured: true).first
            open_position.subscription = create_subscription(open_position.hiring_team, pay_per_post_plan.id, nil, {open_position_id: open_position.id})
            open_position.save!
            user = open_position.hiring_manager
            user.hiring_team.hiring_plan = HiringTeam::HIRING_PLAN_PAY_PER_POST

            ability = Ability.new(user)
            expect(ability.can?(:create_or_update_open_positions, {meta: {open_position: open_position.as_json}})).to be(true)
        end
    end

    describe "create_or_update_conversation" do

        it "should return true when no candidate_position_interest_id param is present" do
            user = User.joins(:roles).where("roles.name = 'learner'").first
            ability = Ability.new(user)
            expect(ability.can?(:create_or_update_conversation, {candidate_position_interest_id: nil})).to be(true)
        end

        describe "when candidate_position_interest_id param is present" do

            it "should return false if candidate position interest can't be found for candidate_position_interest_id param" do
                user = User.joins(:roles).where("roles.name = 'learner'").first
                ability = Ability.new(user)
                expect(ability.can?(:create_or_update_conversation, {candidate_position_interest_id: SecureRandom.uuid})).to be(false)
            end

            it "should return false if the user has no hiring_team_id" do
                user = User.joins(:roles).where("roles.name = 'learner'").left_outer_joins(:hiring_team).where(hiring_teams: {id: nil}).first
                ability = Ability.new(user)
                candidate_position_interest = CandidatePositionInterest.first
                expect(ability.can?(:create_or_update_conversation, {candidate_position_interest_id: candidate_position_interest.id})).to be(false)
            end

            it "should return false if the candidate position interest's hiring_team.id doesn't match the user's hiring_team_id" do
                user = users(:hiring_manager_with_team)
                ability = Ability.new(user)
                mock_hiring_team = double('HiringTeam instance')
                candidate_position_interest = CandidatePositionInterest.first
                expect(CandidatePositionInterest).to receive(:find_by_id).with(candidate_position_interest.id).and_return(candidate_position_interest)
                expect(candidate_position_interest).to receive(:hiring_team).and_return(mock_hiring_team)
                expect(mock_hiring_team).to receive(:id).and_return(SecureRandom.uuid)
                expect(ability.can?(:create_or_update_conversation, {candidate_position_interest_id: candidate_position_interest.id})).to be(false)
            end

            it "should return true if the candidate position interest's hiring_team.id matches the user's hiring_team_id" do
                user = users(:hiring_manager_with_team)
                ability = Ability.new(user)
                candidate_position_interest = CandidatePositionInterest.first
                candidate_position_interest.open_position.hiring_manager.update_attribute(:hiring_team_id, user.hiring_team_id)
                expect(ability.can?(:create_or_update_conversation, {candidate_position_interest_id: candidate_position_interest.id})).to be(true)
            end
        end
    end

    describe "index_candidate_position_interests" do

        def learner
            @learner ||= users(:learner)
        end

        def hiring_manager
            @hiring_manager ||= users(:hiring_manager_with_team)
        end

        def allowed_filters_for_learner
            {
                candidate_id: learner.id,
                candidate_status: 'accepted',
                open_position_id: []
            }
        end

        def allowed_filters_for_hiring_manager
            {
                hiring_manager_id: hiring_manager.id,
                candidate_status: 'accepted',
                hiring_manager_status_not: 'hidden',
                hiring_manager_status: CandidatePositionInterest.hiring_manager_statuses.without('hidden').first,
                open_position_id: []
            }
        end

        it "should allow allowed params for learner" do
            assert_permissions_for_filters(learner, true, allowed_filters_for_learner, {except: ['hiring_manager_status']})
        end

        it "should allow allowed params for hiring_manager" do
            assert_permissions_for_filters(hiring_manager, true, allowed_filters_for_hiring_manager)
        end

        it "should allow a user to index his team's interests" do
            allowed_filters_when_team = allowed_filters_for_hiring_manager.except(:hiring_manager_id).merge({
                hiring_team_id: hiring_manager.hiring_team_id
            })

            assert_permissions_for_filters(hiring_manager, true, allowed_filters_when_team)
            assert_permissions_for_filters(hiring_manager, false, {})
            assert_permissions_for_filters(hiring_manager, false, allowed_filters_when_team.except(:hiring_team_id))
        end

        it "should disallow requesting a candidate_id that is not the current user's" do
            assert_permissions_for_filters(learner, false, allowed_filters_for_learner.merge({
                candidate_id: SecureRandom.uuid
            }))
        end

        it "should disallow requesting a hiring_manager_id that is not the current user's" do
            assert_permissions_for_filters(hiring_manager, false, allowed_filters_for_hiring_manager.merge({
                hiring_manager_id: SecureRandom.uuid
            }))
        end

        it "should disallow requesting a hiring_manager_id when not asking for not hidden" do
            assert_permissions_for_filters(hiring_manager, false, allowed_filters_for_hiring_manager.merge({
                hiring_manager_id: hiring_manager.id,
                hiring_manager_status_not: 'fooshouldbehidden'
            }))
        end

        it "should disallow requesting hiring_manager_status if not a hiring manager" do
            assert_permissions_for_filters(hiring_manager, true, allowed_filters_for_hiring_manager)
            assert_permissions_for_filters(learner, false, allowed_filters_for_learner)
            assert_permissions_for_filters(learner, true, allowed_filters_for_learner, { except: ['hiring_manager_status']} )
        end

        it "should require that a hiring_manager request only candidate_status=accepted records" do
            assert_permissions_for_filters(hiring_manager, false, allowed_filters_for_hiring_manager.merge({
                candidate_status: nil
            }))
        end

        it "should disallow anonymous user" do
            assert_permissions_for_filters(nil, false, {})
        end

        def assert_permissions_for_filters(user, can, filters, other_params = {})
            ability = Ability.new(user)
            expect(ability.can?(:index_candidate_position_interests, other_params.merge({
                filters: filters
            }))).to be(can)
        end
    end

    describe "create_candidate_position_interests" do
        it "should allow candidates to create" do
            candidate = users(:learner)
            ability = Ability.new(candidate)
            expect(ability.can?(:create_candidate_position_interests, {record: {candidate_id: candidate.id}})).to be(true)
        end

        it "should not allow hiring_managers to create" do
            hiring_manager = users(:hiring_manager)
            ability = Ability.new(hiring_manager)
            expect(ability.can?(:create_candidate_position_interests, {record: {hiring_manager: hiring_manager.id}})).to be(false)
        end
    end

    describe "update candidate_position_interests" do
        it "should be false if not candidate or hiring manager" do
            user = User.joins(:roles).where("roles.name = 'learner'").first
            expect(user.can?(:update, CandidatePositionInterest.new)).to be(false)
        end

        describe "as candidate" do
            it "should be true" do
                user = User.joins(:roles).where("roles.name = 'learner'").first
                expect(user.can?(:update, CandidatePositionInterest.new({
                    candidate_id: user.id
                }))).to be(true)
            end
        end

        describe "as hiring_manager" do
            attr_accessor :user, :candidate_position_interest
            before(:each) do
                self.user = User.joins(:hiring_team, :hiring_application, :open_positions).where(hiring_applications: {status: 'accepted'}).first
                open_position = user.open_positions.first
                self.candidate_position_interest = CandidatePositionInterest.new({
                    open_position: open_position
                })
            end

            it "should be true if user's hiring_team.using_pay_per_post_hiring_plan?" do
                expect(user.hiring_team).to receive(:using_pay_per_post_hiring_plan?).and_return(true)
                expect(user.can?(:update, candidate_position_interest)).to be(true)
            end

            it "should be true unless should_anonymize_interest?" do
                freemium_interest_id = SecureRandom.uuid
                allow(user.hiring_team).to receive(:using_pay_per_post_hiring_plan?).and_return(false) # ensure no false positive
                expect(user.hiring_team).to receive(:should_anonymize_interest?) do |interest|
                    interest.id != freemium_interest_id
                end.at_least(1).times

                expect(user.can?(:update, candidate_position_interest)).to be(false)

                candidate_position_interest.id = freemium_interest_id
                expect(user.can?(:update, candidate_position_interest)).to be(true)
            end

            it "should be false if no hiring_team" do
                expect(user).to receive(:hiring_team).at_least(:once).and_return(nil)
                expect(user.can?(:update, candidate_position_interest)).to be(false)
            end

            it "should be false if no accepted application" do
                user.hiring_application.status = 'pending'
                expect(user.hiring_team).not_to receive(:should_anonymize_interest?)
                expect(user.can?(:update, candidate_position_interest)).to be(false)
            end
        end
    end

    describe "show_career_profile" do

        it "should not allow showing a career profile that is not viewable_by_hiring_manager_through_deep_link?" do
            career_profile, another_career_profile = [CareerProfile.first, CareerProfile.second]
            user = career_profile.user
            ability = Ability.new(user)

            expect(another_career_profile).to receive(:viewable_by_hiring_manager_through_deep_link?).with(user).and_return(false)
            expect(ability.can?(:show_career_profile, opts_with_view(another_career_profile))).to be(false)
        end

        it "should allow a user to show zer own profile in editable view" do
            career_profile = CareerProfile.first
            user = career_profile.user
            ability = Ability.new(user)

            expect(ability.can?(:show_career_profile, opts_with_view(career_profile, 'editable'))).to be(true)
        end

        def opts_with_view(career_profile, view="career_profiles")
            {
                career_profile: career_profile,
                params: {view: view}
            }
        end

    end

    describe Lesson::StreamProgress do

        it "should not be writable by a regular user" do
            ability = Ability.new(User.new)
            expect(ability.can?(:write, Lesson::StreamProgress)).to be(false)
        end

    end

    describe 'create_hiring_team_invite_with_params' do

        it "should allow if everything is good" do
            user = users(:hiring_manager_with_team)
            expect(user.can?(:create_hiring_team_invite_with_params, valid_params(user))).to be(true)
        end

        it "should disallow if inviter_id does not match" do
            user = users(:hiring_manager_with_team)
            expect(user.can?(:create_hiring_team_invite_with_params, valid_params(user).merge({
                inviter_id: SecureRandom.uuid
            }))).to be(false)
        end

        it "should disallow if non-pay-per-post user has no accepted hiring_application" do
            user = users(:hiring_manager_with_team)
            user.hiring_team.hiring_plan = HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING
            allow(user.hiring_application).to receive(:status).and_return('pending')
            expect(user.can?(:create_hiring_team_invite_with_params, valid_params(user))).to be(false)

            allow(user).to receive(:hiring_application).and_return(nil)
            expect(user.can?(:create_hiring_team_invite_with_params, valid_params(user))).to be(false)
        end

        it "should allow if pay-per-post user has pending hiring_application" do
            user = users(:hiring_manager_with_team)
            user.hiring_team.hiring_plan = HiringTeam::HIRING_PLAN_PAY_PER_POST
            allow(user.hiring_application).to receive(:status).and_return('pending')
            expect(user.can?(:create_hiring_team_invite_with_params, valid_params(user))).to be(true)
        end

        it "should disallow if pay-per-post user has rejected hiring_application" do
            user = users(:hiring_manager_with_team)
            user.hiring_team.hiring_plan = HiringTeam::HIRING_PLAN_PAY_PER_POST
            allow(user.hiring_application).to receive(:status).and_return('rejected')
            expect(user.can?(:create_hiring_team_invite_with_params, valid_params(user))).to be(false)
        end

        def valid_params(user)
            {
                inviter_id: user.id,
                invitee_email: 'email',
                invitee_name: 'name'
            }
        end

    end

    describe "index_stream_progress" do

        describe "with zapier=true" do

            it "should allow an institutional user to view zapier reports for zer institution" do
                institution = Institution.joins(:reports_viewers).first
                user = institution.reports_viewers.first
                expect(user.can?(:index_stream_progress, {zapier: true, institution_id: institution.id})).to be(true)
            end

            it "should not allow an institutional user to view zapier reports for another institution" do
                institution = Institution.joins(:reports_viewers).first
                user = institution.reports_viewers.first
                expect(user.can?(:index_stream_progress, {zapier: true, institution_id: SecureRandom.uuid})).to be(false)
            end

            it "should not allow a non-institutional user to view zapier reports for another institution" do
                institution = Institution.joins(:reports_viewers).first
                user = (institution.users - institution.reports_viewers).first
                expect(user.can?(:index_stream_progress, {zapier: true, institution_id: institution.id})).to be(false)
            end

        end

        describe "with zapier=false" do

            it "should allow a user to view zer own progress" do
                # make sure it is not an admin
                user = User.joins(:roles).where("roles.name = 'learner'").first
                expect(user.can?(:index_stream_progress, {filters: {user_id: user.id}})).to be(true)
            end

            it "should not allow a user to view anyone else's progress" do
                # make sure it is not an admin
                user = User.joins(:roles).where("roles.name = 'learner'").first
                expect(user.can?(:index_stream_progress, {filters: {user_id: SecureRandom.uuid}})).to be(false)
            end

        end

    end

    describe "destroy Subscription" do

        describe "with user subscription" do

            it "should not allow for deleting a non-hiring team subscription" do
                subscription = Subscription.joins(:user).first
                ability = Ability.new(subscription.user)
                expect(ability.can?(:destroy, subscription)).to be(false)
            end

        end

        describe "with hiring_team subscription" do
            attr_accessor :subscription, :hiring_team, :owner

            before(:each) do
                @hiring_team = HiringTeam.joins(:owner).first
                @owner = hiring_team.owner
                @subscription = Subscription.first
                allow(@subscription).to receive(:owner).and_return(@hiring_team)
                allow(@subscription).to receive(:hiring_team).and_return(@hiring_team)
            end

            it "should allow an owner to delete a primary subscription" do
                ability = Ability.new(owner)
                expect(hiring_team).to receive(:primary_subscription).and_return(subscription)
                expect(ability.can?(:destroy, subscription)).to be(true)
            end

            it "should not allow an owner to delete a non-primary subscription" do
                ability = Ability.new(owner)
                expect(hiring_team).to receive(:primary_subscription).and_return(nil)
                expect(ability.can?(:destroy, subscription)).to be(false)
            end

            it "should not allow a non-owner to delete a subscription" do
                user = User.where.not(id: owner.id).joins(:roles).where("roles.name = 'learner'").first
                ability = Ability.new(user)
                expect(ability.can?(:destroy, subscription)).to be(false)
            end

        end
    end

    describe "create_subscription" do
        attr_accessor :hiring_team, :owner

        before(:each) do
            @hiring_team = HiringTeam.joins(:owner).first
            @owner = hiring_team.owner
        end

        it "should call can_edit_subscription" do
            ability = Ability.new(owner)
            expect(ability).to receive(:can_edit_subscription)
            ability.can?(:create_subscription, {})
        end
    end

    describe "can_edit_subscription" do
        attr_accessor :hiring_team, :owner, :non_owner

        before(:each) do
            @owner = users(:hiring_manager_with_team)
            @non_owner = users(:hiring_manager_teammate)
            @hiring_team = owner.hiring_team
        end

        it "should allow a user to create a subscription for zirself" do
            user = User.joins(:roles).where("roles.name = 'learner'").first
            expect(user.can?(:create_subscription, {user_id: user.id})).to be(true)
            expect(user.can?(:create_subscription, {user_id: SecureRandom.uuid})).to be(false)
        end

        describe "hiring_plan pay-per-post" do
            before(:each) do
                hiring_team.update_columns(hiring_plan: HiringTeam::HIRING_PLAN_PAY_PER_POST)
            end

            it "should allow a non-owner to create a pay-per-post subscription" do
                expect(owner.can?(:create_subscription, {
                    hiring_team_id: hiring_team.id,
                    stripe_plan_id: pay_per_post_plan.id
                })).to be(true)
            end

            it "should allow owner to create an unlimited subscription when converting" do
                expect(owner.can?(:create_subscription, {
                    hiring_team_id: hiring_team.id,
                    stripe_plan_id: unlimited_with_sourcing_plan.id
                })).to be(true)
            end
        end

        describe "hiring_plan unlimited" do
            before(:each) do
                hiring_team.update_columns(hiring_plan: HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING)
            end

            it "should allow owner to create a subscription if there is no existing subscription" do
                expect_any_instance_of(HiringTeam).to receive(:primary_subscription).and_return(nil)
                expect(owner.can?(:create_subscription, {
                    hiring_team_id: hiring_team.id,
                    stripe_plan_id: unlimited_with_sourcing_plan.id
                })).to be(true)
            end

            it "should not allow owner to create a subscription if there is an existing subscription" do
                expect_any_instance_of(HiringTeam).to receive(:primary_subscription).and_return(Subscription.first)
                expect(owner.can?(:create_subscription, {
                    hiring_team_id: hiring_team.id,
                    stripe_plan_id: unlimited_with_sourcing_plan.id
                })).to be(false)
            end

            it "should not allow non-owner to create a subscription even if there is no existing subscription" do
                allow_any_instance_of(HiringTeam).to receive(:primary_subscription).and_return(nil)
                expect(non_owner.can?(:create_subscription, {
                    hiring_team_id: hiring_team.id,
                    stripe_plan_id: unlimited_with_sourcing_plan.id
                })).to be(false)
            end
        end

        describe "hiring_plan legacy" do
            before(:each) do
                hiring_team.update_columns(hiring_plan: HiringTeam::HIRING_PLAN_LEGACY)
            end

            it "should allow owner to change plan back and forth" do
                expect(owner.can?(:create_subscription, {
                    hiring_team_id: hiring_team.id,
                    stripe_plan_id: unlimited_with_sourcing_plan.id
                })).to be(true)

                expect(owner.can?(:create_subscription, {
                    hiring_team_id: hiring_team.id,
                    stripe_plan_id: metered_plan.id
                })).to be(true)
            end

            it "should not allow a non-owner to change plan" do
                expect(non_owner.can?(:create_subscription, {
                    hiring_team_id: hiring_team.id,
                    stripe_plan_id: unlimited_with_sourcing_plan.id
                })).to be(false)
            end
        end

        describe "hiring_plan nil" do
            before(:each) do
                hiring_team.update_columns(hiring_plan: nil)
            end

            it "should allow a hiring team owner to create a pay-per-post subscription" do
                expect(owner.can?(:create_subscription, {
                    hiring_team_id: hiring_team.id,
                    stripe_plan_id: pay_per_post_plan.id
                })).to be(true)
            end

            it "should allow a non-owner to create a pay-per-post subscription" do
                expect(non_owner.can?(:create_subscription, {
                    hiring_team_id: hiring_team.id,
                    stripe_plan_id: pay_per_post_plan.id
                })).to be(true)
            end
        end

        it "should not allow an owner to create a subscription for another hiring team" do
            hiring_team = HiringTeam.where.not(owner_id: nil).first
            hiring_team.update_columns(hiring_plan: nil)
            expect(owner.can?(:create_subscription, {hiring_team_id: SecureRandom.uuid})).to be(false)
        end
    end

    describe "modify_payment_details_with_params" do
        it "should allow a user" do
            user = User.joins(:roles).where("roles.name = 'learner'").first
            expect(user.can?(:modify_payment_details_with_params, {owner_id: user.id, source: SecureRandom.uuid})).to be(true)
        end

        it "should not allow a non-existant user" do
            user = User.joins(:roles).where("roles.name = 'learner'").first
            expect(user.can?(:modify_payment_details_with_params, {owner_id: SecureRandom.uuid, source: SecureRandom.uuid})).to be(false)
        end

        it "should allow a hiring_team owner" do
            owner = users(:hiring_manager_with_team)
            expect(owner.can?(:modify_payment_details_with_params, {owner_id: owner.hiring_team.id, source: SecureRandom.uuid})).to be(true)
        end

        it "should not allow a hiring_team regular member" do
            teammate = users(:hiring_manager_teammate)
            expect(teammate.can?(:modify_payment_details_with_params, {owner_id: teammate.hiring_team.id, source: SecureRandom.uuid})).to be(false)
        end
    end

    describe "request_idology_link_with_params" do
        it "should allow for requesting a link for this user" do
            user = CohortApplication.where(status: 'accepted').first.user
            user.relevant_cohort.id_verification_periods = [{}]
            expect(user.can?(:request_idology_link_with_params, {user_id: SecureRandom.uuid})).to be(false)
            expect(user.can?(:request_idology_link_with_params, {user_id: user.id})).to be(true)
        end

        it "should only allow for requesting a link if the user is accepted or pre_accepted into a cohort that has verification periods" do
            cohort = Cohort.first
            user = CohortApplication.where(status: 'accepted').first.user
            allow(user).to receive(:relevant_cohort).and_return(cohort) # mock this so it does not change when we change statuses
            allow(user).to receive(:last_application).and_return(user.last_application) # mock this so it does not return a new object each time
            user.relevant_cohort.id_verification_periods = [{}]
            expect(user.can?(:request_idology_link_with_params, {user_id: user.id})).to be(true)

            user.relevant_cohort.id_verification_periods = []
            expect(user.can?(:request_idology_link_with_params, {user_id: user.id})).to be(false)
            user.relevant_cohort.id_verification_periods = [{}]
            expect(user.can?(:request_idology_link_with_params, {user_id: user.id})).to be(true) # sanity check

            user.last_application.status = 'pre_accepted'
            expect(user.can?(:request_idology_link_with_params, {user_id: user.id})).to be(true)
            user.last_application.status = 'deferred'
            expect(user.can?(:request_idology_link_with_params, {user_id: user.id})).to be(true)
            user.last_application.status = 'pending'
            expect(user.can?(:request_idology_link_with_params, {user_id: user.id})).to be(false)

        end

        it "should disallow requesting more that 5 links in a minute" do
            user, another_user = CohortApplication.where(status: 'accepted').limit(2).map(&:user)
            user.relevant_cohort.id_verification_periods = [{}]
            another_user.relevant_cohort.id_verification_periods = [{}]
            4.times do |i|
                IdologyVerification.create!(
                    user_id: user.id,
                    idology_id_number: i,
                    scan_capture_url: "some_url_#{i}"
                )
            end
            expect(user.can?(:request_idology_link_with_params, {user_id: user.id})).to be(true)
            expect(another_user.can?(:request_idology_link_with_params, {user_id: another_user.id})).to be(true)

            IdologyVerification.create!(
                user_id: user.id,
                idology_id_number: 4,
                scan_capture_url: "some_url_#{4}"
            )
            expect(user.can?(:request_idology_link_with_params, {user_id: user.id})).to be(false)
            expect(another_user.can?(:request_idology_link_with_params, {user_id: another_user.id})).to be(true)

            allow(Time).to receive(:now).and_return(Time.now + 1.minute)
            expect(user.can?(:request_idology_link_with_params, {user_id: user.id})).to be(true)
            expect(another_user.can?(:request_idology_link_with_params, {user_id: another_user.id})).to be(true)

        end

        it "should disallow requesting more than 30 links in a day" do

            user, another_user = CohortApplication.where(status: 'accepted').limit(2).map(&:user)
            user.relevant_cohort.id_verification_periods = [{}]
            another_user.relevant_cohort.id_verification_periods = [{}]
            29.times do |i|
                IdologyVerification.create!(
                    user_id: user.id,
                    idology_id_number: i,
                    scan_capture_url: "some_url_#{i}",
                    created_at: Time.now - 20.minutes
                )
            end
            expect(user.can?(:request_idology_link_with_params, {user_id: user.id})).to be(true)
            expect(another_user.can?(:request_idology_link_with_params, {user_id: another_user.id})).to be(true)

            IdologyVerification.create!(
                user_id: user.id,
                idology_id_number: 29,
                scan_capture_url: "some_url_#{29}",
                created_at: Time.now - 20.minutes
            )
            expect(user.can?(:request_idology_link_with_params, {user_id: user.id})).to be(false)
            expect(another_user.can?(:request_idology_link_with_params, {user_id: another_user.id})).to be(true)

            allow(Time).to receive(:now).and_return(Time.now + 1.day)
            expect(user.can?(:request_idology_link_with_params, {user_id: user.id})).to be(true)
            expect(another_user.can?(:request_idology_link_with_params, {user_id: another_user.id})).to be(true)
        end

    end

    describe "update_hiring_team_with_params" do
        attr_reader :hiring_team, :user

        before(:each) do
            @hiring_team = HiringTeam.first
            @hiring_team.update(hiring_plan: nil)
            @user = hiring_team.hiring_managers.first
        end


        it "should not allow a user to update a team that is not their own" do
            expect(user.can?(:update_hiring_team_with_params, {id: HiringTeam.where.not(id: hiring_team.id).first.id})).to be(false)
        end

        it "should not allow users to update anything other than hiring_plan" do
            expect(user.can?(:update_hiring_team_with_params, {id: hiring_team.id, title: 'new title'})).to be(false)

        end

        it "should not allow users to update if the hiring_plan is already set" do
            hiring_team.update(hiring_plan: HiringTeam::HIRING_PLAN_PAY_PER_POST)
            expect(user.can?(:update_hiring_team_with_params, {id: hiring_team.id})).to be(false)
        end

        it "should allow users to update if the hiring_plan is already set to unlimited and is not changing" do
            hiring_team.update(hiring_plan: HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING)
            expect(user.can?(:update_hiring_team_with_params, {id: hiring_team.id, hiring_plan: HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING})).to be(true)
        end

        it "should not allow users to update to a plan other than unlimited" do
            expect(user.can?(:update_hiring_team_with_params, {id: hiring_team.id, hiring_plan: HiringTeam::HIRING_PLAN_PAY_PER_POST})).to be(false)
        end

        it "should allow otherwise" do
            expect(user.can?(:update_hiring_team_with_params, {id: hiring_team.id, hiring_plan: HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING})).to be(true)
        end

    end
end