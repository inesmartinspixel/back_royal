# == Schema Information
#
# Table name: career_profile_lists
#
#  id                 :uuid             not null, primary key
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  name               :text             not null
#  career_profile_ids :uuid             default([]), not null, is an Array
#  role_descriptors   :text             default([]), not null, is an Array
#

require 'spec_helper'

describe CareerProfileList do

    describe "as_json" do

        it "should not populate career_profiles if career_profiles field is not present" do
            career_profile = CareerProfile.first
            career_profile_list = CareerProfileList.create(:name => "foo", :career_profile_ids => [career_profile.id])
            expect(career_profile_list.as_json["career_profiles"]).to be_nil
        end

        it "should populate career_profiles if career_profiles field is present" do
            career_profile = CareerProfile.first
            career_profile_list = CareerProfileList.create(:name => "foo", :career_profile_ids => [career_profile.id])
            expect(career_profile_list.as_json({ :fields => ["career_profiles"] })[:career_profiles]).to_not be_nil
        end

        it "should sort the order of career_profiles to match the order of the ids in career_profile_ids" do
            career_profile_1 = CareerProfile.first
            career_profile_2 = CareerProfile.second
            career_profile_list = CareerProfileList.create(:name => "foo", :career_profile_ids => [career_profile_1.id, career_profile_2.id])
            career_profiles_json = career_profile_list.as_json({ :fields => ["career_profiles"] })[:career_profiles]
            expect(career_profiles_json).to_not be_nil
            career_profiles_json.each_with_index do |career_profile, index|
                expect(career_profile["id"]).to eq(career_profile_list.career_profile_ids[index])
            end
        end
    end

end
