# == Schema Information
#
# Table name: curriculum_templates
#
#  id                                      :uuid             not null, primary key
#  created_at                              :datetime         not null
#  updated_at                              :datetime         not null
#  name                                    :text             not null
#  description                             :text
#  specialization_playlist_pack_ids        :uuid             default([]), not null, is an Array
#  periods                                 :json             not null, is an Array
#  num_required_specializations            :integer          default(0), not null
#  program_type                            :text             not null
#  admission_rounds                        :json             is an Array
#  registration_deadline_days_offset       :integer          default(0)
#  enrollment_deadline_days_offset         :integer
#  stripe_plans                            :json             is an Array
#  scholarship_levels                      :json             is an Array
#  project_submission_email                :string
#  early_registration_deadline_days_offset :integer          default(0)
#  id_verification_periods                 :json             is an Array
#  graduation_days_offset_from_end         :integer
#  diploma_generation_days_offset_from_end :integer
#  learner_project_ids                     :uuid             default([]), not null, is an Array
#  enrollment_agreement_template_id        :text
#  playlist_collections                    :json             not null, is an Array
#

require 'spec_helper'

describe CurriculumTemplate do

    fixtures(:users)

    before(:each) do
        @curriculum_template = CurriculumTemplate.create!({
            name: "test template",
            program_type: 'mba'
        })

        @user = users(:admin)
    end

    it "should raise error on empty name" do
        curriculum_template = CurriculumTemplate.first

        # save it with no name
        expect {
            CurriculumTemplate.update_from_hash!(@user, {name: "", id: curriculum_template.id, updated_at: Time.now})
        }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Name can't be blank")
    end

    describe "update_from_hash!" do

        it "updates name" do
            CurriculumTemplate.update_from_hash!(@user, {updated_at: Time.now, id: @curriculum_template.id, name: "foo schedule"})
            @curriculum_template.reload
            expect(@curriculum_template.name).to eq("foo schedule")
        end

        it "updates groups" do
            @group1 = AccessGroup.find_or_create_by(name: "CATS")
            @group2 = AccessGroup.find_or_create_by(name: "DOGS")

            # can add groups
            CurriculumTemplate.update_from_hash!(@user, {updated_at: Time.now, id: @curriculum_template.id, groups: [@group1.as_json]})
            @curriculum_template.reload
            expect(@curriculum_template.groups.size).to eq(1)
            expect(@curriculum_template.groups[0].name).to eq('CATS')

            CurriculumTemplate.update_from_hash!(@user, {updated_at: Time.now, id: @curriculum_template.id, groups: [@group1.as_json, @group2.as_json]})
            @curriculum_template.reload
            expect(@curriculum_template.groups.size).to eq(2)
            expect(@curriculum_template.groups[0].name).to eq('CATS')

            # can delete the groups
            CurriculumTemplate.update_from_hash!(@user, {updated_at: Time.now, id: @curriculum_template.id, groups: []})
            @curriculum_template.reload
            expect(@curriculum_template.groups.size).to eq(0)
        end
    end

    describe "merge hash" do
        it "should update a bunch of stuff" do
            curriculum_template = CurriculumTemplate.first

            required_playlist_pack_ids = [SecureRandom.uuid]
            specialization_playlist_pack_ids = [SecureRandom.uuid]
            learner_project_ids = [SecureRandom.uuid]
            enrollment_agreement_template_id = SecureRandom.uuid

            curriculum_template.merge_hash({
                name: 'updated',
                specialization_playlist_pack_ids: specialization_playlist_pack_ids,
                periods: [{'updated': true}],
                program_type: 'updated',
                num_required_specializations: 42,
                description: 'updated',
                admission_rounds: [{"a" => 1}, {"b" => 2}],
                registration_deadline_days_offset: 3,
                early_registration_deadline_days_offset: 8,
                enrollment_deadline_days_offset: 4,
                graduation_days_offset_from_end: 40,
                diploma_generation_days_offset_from_end: 10,
                project_submission_email: 'project_submission@pedago.com',
                learner_project_ids: learner_project_ids,
                enrollment_agreement_template_id: enrollment_agreement_template_id,
                playlist_collections: [{title: 'Some Title', required_playlist_pack_ids: required_playlist_pack_ids}]
            }.with_indifferent_access)
            expect(curriculum_template.name).to eq('updated')
            expect(curriculum_template.specialization_playlist_pack_ids).to eq(specialization_playlist_pack_ids)
            expect(curriculum_template.periods).to eq([{'updated' => true}])
            expect(curriculum_template.num_required_specializations).to eq(42)
            expect(curriculum_template.description).to eq('updated')
            expect(curriculum_template.program_type).to eq('updated')
            expect(curriculum_template.admission_rounds).to eq([{"a" => 1}, {"b" => 2}])
            expect(curriculum_template.registration_deadline_days_offset).to eq(3)
            expect(curriculum_template.early_registration_deadline_days_offset).to eq(8)
            expect(curriculum_template.enrollment_deadline_days_offset).to eq(4)
            expect(curriculum_template.graduation_days_offset_from_end).to eq(40)
            expect(curriculum_template.diploma_generation_days_offset_from_end).to eq(10)
            expect(curriculum_template.project_submission_email).to eq('project_submission@pedago.com')
            expect(curriculum_template.learner_project_ids).to eq(learner_project_ids)
            expect(curriculum_template.enrollment_agreement_template_id).to eq(enrollment_agreement_template_id)
            expect(curriculum_template.playlist_collections).to eq([{'title' => 'Some Title', 'required_playlist_pack_ids' => required_playlist_pack_ids}])
        end
    end

    describe "as_json" do
        it "should only return update fields if instructed" do
            curriculum_template = CurriculumTemplate.first
            json = curriculum_template.as_json({
                fields: ['UPDATE_FIELDS']
            })

            expected = {}
            expected["updated_at"] = curriculum_template.updated_at.to_time.to_timestamp
            expected["created_at"] = curriculum_template.created_at.to_time.to_timestamp
            expect(json).to eq(expected)
        end
    end

end
