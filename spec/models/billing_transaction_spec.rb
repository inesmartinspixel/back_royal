# == Schema Information
#
# Table name: billing_transactions
#
#  id                      :uuid             not null, primary key
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  transaction_time        :datetime         not null
#  transaction_type        :text             not null
#  provider                :text
#  provider_transaction_id :text
#  amount                  :float            not null
#  amount_refunded         :float            default(0.0), not null
#  refunded                :boolean          default(FALSE), not null
#  currency                :text             not null
#  description             :text
#  metadata                :json
#  stripe_livemode         :boolean
#

require 'spec_helper'
require 'stripe_helper'

describe BillingTransaction do

    include StripeHelper

    fixtures(:users)

    describe "create_from_hash!" do
        it "should work" do
            expect_any_instance_of(BillingTransaction).to receive(:merge_hash).and_call_original
            user = User.first
            instance = BillingTransaction.create_from_hash!(user, valid_attrs)
            expect(instance.id).not_to be_nil
            expect(user.billing_transactions.reload).to include(instance)
        end

        it "should raise if given an uneditable provider" do
            expect {
                BillingTransaction.create_from_hash!(User.first, valid_attrs.merge({
                    provider: BillingTransaction::PROVIDER_STRIPE
                }))
            }.to raise_error("Uneditable provider")
        end

    end

    describe "update_from_hash!" do
        it "should work" do
            billing_transaction = BillingTransaction.create!(valid_attrs)
            expect_any_instance_of(BillingTransaction).to receive(:merge_hash).and_call_original
            instance = BillingTransaction.update_from_hash!({
                id: billing_transaction.id,
                amount: 193829
            })
            expect(instance.amount).to eq(193829)
        end

        it "should raise if record has an uneditable provider" do
            billing_transaction = BillingTransaction.create!(valid_attrs.merge({
                provider: BillingTransaction::PROVIDER_STRIPE
            }))
            expect {
                BillingTransaction.update_from_hash!({
                    id: billing_transaction.id
                })
            }.to raise_error("Uneditable provider")
        end

        it "should raise if given an uneditable provider" do
            billing_transaction = BillingTransaction.create!(valid_attrs)
            expect {
                BillingTransaction.update_from_hash!({
                    id: billing_transaction.id,
                    provider: BillingTransaction::PROVIDER_STRIPE
                })
            }.to raise_error("Uneditable provider")
        end

    end

    describe "destroy" do

        it "should error if billing_transaction has an uneditable provider" do
            billing_transaction = BillingTransaction.create!(valid_attrs.merge({
                provider: BillingTransaction::PROVIDER_STRIPE
            }))
            expect {
                billing_transaction.destroy
            }.to raise_error("Uneditable provider")
        end

    end

    describe "merge_hash" do

        it "should work" do
            transaction_time = Time.parse('2018/01/07')

            billing_transaction = BillingTransaction.new
            billing_transaction.merge_hash({
                transaction_time: transaction_time.to_timestamp,
                provider: 'SVB - 38322',
                provider_transaction_id: '12345',
                amount: 4200,
                amount_refunded: 2100,
                description: 'blah blah',
                currency: 'cigarettes'
            })
            expect(billing_transaction.transaction_time).to eq(transaction_time)
            expect(billing_transaction.provider).to eq('SVB - 38322')
            expect(billing_transaction.provider_transaction_id).to eq('12345')
            expect(billing_transaction.amount).to eq(4200)
            expect(billing_transaction.amount_refunded).to eq(2100)
            expect(billing_transaction.description).to eq('blah blah')
            expect(billing_transaction.currency).to eq('cigarettes')
        end

    end

    describe "set_refunds_from_hash" do

        it "should add a refund" do
            time = Time.parse('2019/01/01')
            billing_transaction = BillingTransaction.create!(valid_attrs)
            billing_transaction.set_refunds_from_hash([{
                'refund_time' => time.to_timestamp,
                'amount' => 42,
                'provider_transaction_id' => SecureRandom.uuid,
                'id' => SecureRandom.uuid
            }])
            refund = billing_transaction.refunds.detect { |r| r.refund_time == time }
            expect(refund).not_to be_nil
            expect(refund.amount).to eq(42)
            expect(billing_transaction.amount_refunded).to eq(42)
        end

        it "should update a refund" do
            billing_transaction, refund = add_a_refund
            billing_transaction.set_refunds_from_hash([refund.as_json.merge({
                'amount' => 42
            })])

            updated_refund = billing_transaction.refunds.detect { |r| r.id == refund.id }
            expect(updated_refund).not_to be_nil
            expect(updated_refund.amount).to eq(42)
            expect(billing_transaction.amount_refunded).to eq(42)
        end

        it "should delete a refund" do
            billing_transaction, refund = add_a_refund
            billing_transaction.set_refunds_from_hash([])
            expect(billing_transaction.refunds).to be_empty
        end

        it "should leave an unchanged refund unchanged" do
            billing_transaction, refund = add_a_refund
            expect {
                billing_transaction.set_refunds_from_hash([refund.as_json])
            }.not_to change { refund.reload.updated_at}
        end

        describe "refunded" do
            it "should set refunded to true when refunding the full amount" do
                billing_transaction, refund = add_a_refund
                billing_transaction.set_refunds_from_hash([refund.as_json.merge({
                    'amount' => billing_transaction.amount
                })])

                refund = billing_transaction.refunds.find(refund.id)
                expect(refund).not_to be_nil
                expect(billing_transaction.refunded).to be(true)
            end

            it "should set refunded to false when not refunding the full amount" do
                billing_transaction, refund = add_a_refund
                billing_transaction.set_refunds_from_hash([refund.as_json.merge({
                    'amount' => billing_transaction.amount - 1
                })])

                refund = billing_transaction.refunds.find(refund.id)
                expect(refund).not_to be_nil
                expect(billing_transaction.refunded).to be(false)
            end
        end

        def add_a_refund
            time = Time.parse('2019/01/01')
            billing_transaction = BillingTransaction.create!(valid_attrs)
            refund = Refund.create!({
                'refund_time' => time,
                'amount' => 41,
                'billing_transaction_id' => billing_transaction.id,
                'provider' => billing_transaction.provider,
                'provider_transaction_id' => SecureRandom.uuid
            })
            billing_transaction.refunds.reset
            [billing_transaction, refund]
        end

    end

    describe "warn_on_incorrect_amount_refunded" do
        it "should warn in Sentry if the sum_of_refunds does not match amount_refunded" do
            billing_transaction = BillingTransaction.create!(valid_attrs)
            expect(billing_transaction).to receive(:sum_of_refunds).at_least(1).times.and_return(10)
            expect(Raven).to receive(:capture_exception).with("amount_refunded different from expected value", {
                extra: {
                    billing_transaction_id: billing_transaction.id,
                    expected_value: 10,
                    actual_value: 15
                }
            })
            billing_transaction.update!(amount_refunded: 15)
        end
    end

    describe "find_or_create_by_stripe_charge" do
        it "should create a new billing_transaction for a user" do
            assert_creates_billing_transaction(User.first)
        end
        it "should create a new billing_transaction for a hiring team" do
            assert_creates_billing_transaction(HiringTeam.first)
        end

        it "should create a new billing_transaction when owner does not exist" do
            transaction_time = Time.parse('2018-01-01')
            stripe_billing_transaction = OpenStruct.new({
                created: transaction_time.to_timestamp,
                id: '12345',
                amount: 4200*100,
                currency: 'cigarettes',
                description: 'a description',
                metadata: {'some' => 'metadata'},
                livemode: true,
                customer: SecureRandom.uuid
            })
            expect(Raven).to receive(:capture_exception)
            record = BillingTransaction.find_or_create_by_stripe_charge(stripe_billing_transaction)
            expect(record.attributes).to have_entries({
                transaction_time: transaction_time,
                provider: BillingTransaction::PROVIDER_STRIPE,
                provider_transaction_id: stripe_billing_transaction.id,
                amount: 4200,
                currency: stripe_billing_transaction.currency,
                description: stripe_billing_transaction.description,
                metadata: stripe_billing_transaction.metadata,
                stripe_livemode: true
            }.stringify_keys)
        end

        it "should update an existing billing_transaction" do
            user = User.first
            record = BillingTransaction.create!(valid_attrs.merge({
                provider: BillingTransaction::PROVIDER_STRIPE,
                provider_transaction_id: '12345'
            }))
            user.billing_transactions << record
            transaction_time = Time.now

            stripe_billing_transaction = OpenStruct.new({
                created: transaction_time.to_timestamp,
                id: record.provider_transaction_id,
                amount: record.amount*100,
                currency: record.currency,
                description: 'an updated description',
                metadata: {'some' => 'updated metadata'},
                customer: user.id
            })
            record = BillingTransaction.find_or_create_by_stripe_charge(stripe_billing_transaction)
            expect(record.reload.attributes).to have_entries({
                provider: BillingTransaction::PROVIDER_STRIPE,
                provider_transaction_id: '12345',
                amount: record.amount,
                description: 'an updated description',
                metadata: {'some' => 'updated metadata'}
        }.stringify_keys)
        end
    end

    describe "all_for_owner_id" do

        it "should get billing_transactions for a user" do
            user = User.first
            billing_transaction = BillingTransaction.create!(valid_attrs)
            user.billing_transactions << billing_transaction
            expect(BillingTransaction.all_for_owner_id(user.id)).to include(billing_transaction)
        end

        it "should get billing_transactions for a hiring_team" do
            hiring_team = HiringTeam.first
            billing_transaction = BillingTransaction.create!(valid_attrs)
            hiring_team.billing_transactions << billing_transaction
            expect(BillingTransaction.all_for_owner_id(hiring_team.id)).to include(billing_transaction)
        end

    end

    describe "validations" do

        it "should be invalid if amount_refunded > amount" do
            billing_transaction = BillingTransaction.new(valid_attrs)
            billing_transaction.amount = 42
            billing_transaction.amount_refunded = 43
            expect(billing_transaction.valid?).to be(false)
            expect(billing_transaction.errors.full_messages).to include("Amount cannot be less than amount_refunded")

        end

        it "should be invalid if negative amount expected but amount is positive" do
            billing_transaction = BillingTransaction.new(valid_attrs)
            billing_transaction.amount = 42

            billing_transaction.transaction_type = BillingTransaction::TYPES_WITH_NEGATIVE_AMOUNTS[0]
            expect(billing_transaction.valid?).to be(false)
            expect(billing_transaction.errors.full_messages).to include("Amount cannot be positive for this transaction type")
        end

        it "should be invalid if positive amount expected but amount is negative" do
            billing_transaction = BillingTransaction.new(valid_attrs)
            billing_transaction.amount = -42

            billing_transaction.transaction_type = BillingTransaction::TYPES_WITH_POSITIVE_AMOUNTS[0]
            expect(billing_transaction.valid?).to be(false)
            expect(billing_transaction.errors.full_messages).to include("Amount cannot be negative for this transaction type")
        end

        it "should be invalid if amount_refunded is set when not expected" do
            billing_transaction = BillingTransaction.new(valid_attrs)
            billing_transaction.amount_refunded = 1

            (BillingTransaction::TRANSACTION_TYPES - [BillingTransaction::TRANSACTION_TYPE_PAYMENT]).each do |type|
                billing_transaction.transaction_type = type
                expect(billing_transaction.valid?).to be(false)
                expect(billing_transaction.errors.full_messages).to include("Amount refunded must be 0")
            end

            billing_transaction.transaction_type = BillingTransaction::TRANSACTION_TYPE_PAYMENT
            expect(billing_transaction.valid?).to be(true)

        end
    end

    describe "handle_billing_locked" do

        it "should be called on create" do
            expect_any_instance_of(BillingTransaction).to receive(:handle_billing_locked)
            BillingTransaction.create!(valid_attrs)
        end

        it "should be called after save" do
            billing_transaction = BillingTransaction.create!(valid_attrs)
            expect(billing_transaction).to receive(:handle_billing_locked)
            billing_transaction.save!
        end

        it "should move user out of billing locked if has_paid_in_full? with EditorTracking" do
            user = users(:accepted_emba_cohort_user)
            user.last_application.update_column(:locked_due_to_past_due_payment, true)
            expect(user.last_application.locked_due_to_past_due_payment).to be(true)
            allow_any_instance_of(BillingTransaction).to receive(:user).and_return(user)
            expect_any_instance_of(CohortApplication).to receive(:has_paid_in_full?).at_least(:once).and_return(true)
            expect(EditorTracking).to receive(:transaction) do |user, &block|
                expect(user).to eq('BillingTransaction#handle_billing_locked')
                block.call
            end
            assert_creates_billing_transaction(user)
            expect(user.last_application.reload.locked_due_to_past_due_payment).to be(false)
        end
    end

    def assert_creates_billing_transaction(owner)
        transaction_time = Time.parse('2018-01-01')
        stripe_billing_transaction = OpenStruct.new({
            created: transaction_time.to_timestamp,
            id: SecureRandom.uuid,
            amount: 4200*100,
            amount_refunded: 2100*100,
            refunded: false,
            currency: 'cigarettes',
            description: 'a description',
            metadata: {'some' => 'metadata'},
            livemode: true,
            customer: owner.id
        })
        record = BillingTransaction.find_or_create_by_stripe_charge(stripe_billing_transaction)
        expect(owner.billing_transactions.reload).to include(record)
        expect(record.attributes).to have_entries({
            transaction_time: transaction_time,
            provider: BillingTransaction::PROVIDER_STRIPE,
            provider_transaction_id: stripe_billing_transaction.id,
            amount: 4200,
            currency: stripe_billing_transaction.currency,
            description: stripe_billing_transaction.description,
            metadata: stripe_billing_transaction.metadata,
            stripe_livemode: true
        }.stringify_keys)
    end

    def valid_attrs
        {
            transaction_type: 'payment',
            transaction_time: Time.parse('2017-06-09').to_timestamp,
            provider: BillingTransaction::PROVIDER_PAYPAL,
            provider_transaction_id: '875676453',
            amount: 3600,
            description: 'blah blah',
            currency: 'usd'
        }
    end
end
