# == Schema Information
#
# Table name: cohorts
#
#  id                                      :uuid             not null, primary key
#  created_at                              :datetime         not null
#  updated_at                              :datetime         not null
#  start_date                              :datetime         not null
#  end_date                                :datetime         not null
#  was_published                           :boolean          default(FALSE)
#  name                                    :text             not null
#  specialization_playlist_pack_ids        :uuid             default([]), is an Array
#  periods                                 :json             not null, is an Array
#  program_type                            :text             default("mba"), not null
#  num_required_specializations            :integer          default(0), not null
#  title                                   :text             not null
#  description                             :text
#  admission_rounds                        :json             is an Array
#  registration_deadline_days_offset       :integer          default(0)
#  external_schedule_url                   :text
#  program_guide_url                       :text
#  enrollment_deadline_days_offset         :integer          default(0), not null
#  stripe_plans                            :json             is an Array
#  scholarship_levels                      :json             is an Array
#  internal_notes                          :text
#  project_submission_email                :string
#  early_registration_deadline_days_offset :integer          default(0)
#  id_verification_periods                 :json             is an Array
#  graduation_days_offset_from_end         :integer
#  diploma_generation_days_offset_from_end :integer
#  learner_project_ids                     :uuid             default([]), not null, is an Array
#  enrollment_agreement_template_id        :text
#  playlist_collections                    :json             not null, is an Array
#  isolated_network                        :boolean          default(FALSE), not null
#

require 'spec_helper'

describe Cohort do

    include StripeHelper

    fixtures(:lesson_streams, :cohorts, :playlists, :users)

    before(:each) do
        SafeCache.clear # Cohort#get_required_lesson_stream_locale_pack_ids_by_playlist
    end

    it "should raise error on empty name" do
        # get a published cohort
        cohort = cohorts(:published_mba_cohort)

        # save it with no name
        expect {
            Cohort.update_from_hash!(@user, {name: "", id: cohort.id, updated_at: Time.now})
        }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Name can't be blank")
    end

    describe "create" do
        it "requires a start_date" do
            cohort = Cohort.create({name: 'New Cohort', title:'title'})
            expect(cohort.valid?).to be(false)
            cohort.start_date = Time.now
        end
    end

    describe "update_from_hash!" do
        before(:each) do
            # create the default test signup group
            @group1 = AccessGroup.find_or_create_by(name: "CATS")
            @group2 = AccessGroup.find_or_create_by(name: "DOGS")
            @user = users(:admin)
        end

        it "updates title and description" do
            cohort = Cohort.create({name: 'New Cohort', title:'title', start_date: Time.now, end_date: Time.now })

            Cohort.update_from_hash!(@user, {title: 'test title', description: 'test description', updated_at: Time.now, id: cohort.id})
            cohort.reload
            expect(cohort.title).to eq('test title')
            expect(cohort.description).to eq('test description')
        end

        it "updates name" do
            cohort = Cohort.create({name: 'New Cohort', title:'title', start_date: Time.now, end_date: Time.now })

            Cohort.update_from_hash!(@user, {name: 'New Cohort changed', updated_at: Time.now, id: cohort.id})
            cohort.reload
            expect(cohort.name).to eq('New Cohort changed')
            expect(cohort.groups.size).to eq(0)
        end

        it "updates groups" do
            cohort = Cohort.create({name: 'New Cohort', title:'title', start_date: Time.now, end_date: Time.now })

            # can add groups to this cohort
            expect {

                Cohort.update_from_hash!(@user, {updated_at: Time.now, id: cohort.id, groups: [@group1.as_json]})
                cohort.reload
            }.to change { cohort.updated_at }
            expect(cohort.groups.size).to eq(1)
            expect(cohort.groups[0].name).to eq('CATS')

            expect {
                Cohort.update_from_hash!(@user, {updated_at: Time.now, id: cohort.id, groups: [@group1.as_json, @group2.as_json]})
                cohort.reload
            }.to change { cohort.updated_at }
            expect(cohort.groups.size).to eq(2)
            expect(cohort.groups[0].name).to eq('CATS')

            # can delete the groups from the cohort
            expect {
                Cohort.update_from_hash!(@user, {updated_at: Time.now, id: cohort.id, groups: []})
                cohort.reload
            }.to change { cohort.updated_at }
            expect(cohort.groups.size).to eq(0)
        end

        it "updates dates" do
            cohort = Cohort.create({name: 'New Cohort', title:'title', start_date: Time.now, end_date: Time.now })

            new_date = Time.parse('2017/01/01')
            Cohort.update_from_hash!(@user, {
                start_date: new_date.to_timestamp,
                end_date: new_date.to_timestamp,
                registration_deadline_days_offset: 3,
                early_registration_deadline_days_offset: 8,
                updated_at: Time.now, id: cohort.id})
            cohort.reload
            expect(cohort.start_date).to eq(new_date)
            expect(cohort.end_date).to eq(new_date)
            expect(cohort.registration_deadline_days_offset).to eq(3)
            expect(cohort.early_registration_deadline_days_offset).to eq(8)
        end

        it "updates admission_rounds" do
            cohort = Cohort.create({name: 'New Cohort', title:'title', start_date: Time.now, end_date: Time.now })

            admission_rounds = [{"a" => 1}, {"b" => 2}]
            Cohort.update_from_hash!(@user, {admission_rounds: admission_rounds, updated_at: Time.now, id: cohort.id})
            cohort.reload
            expect(cohort.admission_rounds).to eq(admission_rounds)
        end

        it "updates enrollment_agreement_template_id" do
            cohort = Cohort.create({name: 'New Cohort', title:'title', start_date: Time.now, end_date: Time.now })

            enrollment_agreement_template_id = SecureRandom.uuid
            Cohort.update_from_hash!(@user, {enrollment_agreement_template_id: enrollment_agreement_template_id, updated_at: Time.now, id: cohort.id})
            cohort.reload
            expect(cohort.enrollment_agreement_template_id).to eq(enrollment_agreement_template_id)
        end


        it "should be possible to publish" do
            # get an unpublished cohort
            cohort = cohorts(:unpublished_cohort)
            expect(cohort.has_published_version?).to be(false)

            # publish it
            Cohort.update_from_hash!(@user, {id: cohort.id, updated_at: Time.now}, {'should_publish': true})
            published_version = Cohort.find(cohort.id).published_version
            expect(published_version).not_to be_nil
            expect(published_version.was_published).to be(true)
        end

        it "should be possible to unpublish" do
            # get a published cohort
            cohort = cohorts(:published_mba_cohort)
            expect(cohort.has_published_version?).to be(true)

            # publish it
            Cohort.update_from_hash!(@user, {id: cohort.id, updated_at: Time.now}, {'should_unpublish': true})
            expect(Cohort.find(cohort.id).has_published_version?).to be(false)
        end

        it "should not be possible to unpublish a promoted cohort" do
            # get a published cohort
            cohort = cohorts(:published_career_network_only_cohort)
            allow(cohort).to receive(:supports_manual_promotion?).and_return(true)
            CohortPromotion.delete_all
            CohortPromotion.create!(cohort_id: cohort.id)

            # publish it
            expect {
                Cohort.update_from_hash!(@user, {id: cohort.id, updated_at: Time.now}, {'should_unpublish': true})
            }.to raise_error(ActiveRecord::RecordInvalid, 'Validation failed: Promoted cannot be true for an unpublished cohort')
        end

        it "updates external_schedule_url" do
            cohort = Cohort.create({name: 'New Cohort', title:'title', start_date: Time.now, end_date: Time.now })

            Cohort.update_from_hash!(@user, {external_schedule_url: 'ok', updated_at: Time.now, id: cohort.id})
            cohort.reload
            expect(cohort.external_schedule_url).to eq('ok')
        end

        it "should call after_access_group_update after groups have changed" do
            cohort = cohorts(:published_mba_cohort)
            group = AccessGroup.create!(name: 'new_group_for_testing_refresh')
            expect(cohort.access_groups).not_to be_empty
            orig_groups = cohort.access_groups.to_a.clone

            # assert that when we call after_access_group_update, the groups
            # have already changed
            expect_any_instance_of(Cohort).to receive(:after_access_group_update) do |cohort, _orig_group_ids|
                expect(_orig_group_ids).to match_array(orig_groups.map(&:id))
                expect(cohort.access_groups).to eq([group])
            end
            Cohort.update_from_hash!(@user, {updated_at: Time.now, id: cohort.id, groups: [group.as_json]})
        end
    end

    describe "foundations_updated_at" do

        attr_reader :cohort

        before(:each) do
            @cohort = cohorts(:published_mba_cohort)
        end

        it "should return the cohort updated_at if that is last" do
            updated_at = Time.now + 3.days
            cohort.update_attribute('updated_at', updated_at)
            expect(Time.at(cohort.foundations_updated_at)).to be_within(1.second).of(updated_at)
        end

        it "should return the last updated_at from a playlist if that is last" do
            playlist = cohort.get_required_playlist_locale_packs.first.content_items.first
            updated_at = Time.now + 3.days
            playlist.update_attribute('updated_at', updated_at)
            expect(Time.at(cohort.foundations_updated_at)).to be_within(1.second).of(updated_at)
        end
    end

    describe "get_required_lesson_locale_pack_ids" do

        it "should work" do
            # this is mostly testing the materialized view
            cohort = cohorts(:published_mba_cohort)
            required_playlists = Playlist.where(locale: 'en', locale_pack_id: cohort.required_playlist_pack_ids)
            playlist_streams = Lesson::Stream.where(locale: 'en', locale_pack_id: required_playlists.map(&:stream_locale_pack_ids).flatten)

            schedule_stream_lp_ids = cohort.periods.select { |p| ['period with non-playlist courses', "exam period", "assessment period"].include?(p['title']) }
                                        .map { |p| p['stream_entries'] }.flatten
                                        .map { |e| e['required'] && e['locale_pack_id'] }.flatten.compact.uniq
            schedule_streams = Lesson::Stream.where(locale: 'en', locale_pack_id: schedule_stream_lp_ids)

            expected_playlist_lps = playlist_streams.map(&:lessons).flatten.map(&:locale_pack_id)
            expected_schedule_lps = schedule_streams.map(&:lessons).flatten.map(&:locale_pack_id)

            actual_lps = cohort.get_required_lesson_locale_pack_ids

            expect(expected_playlist_lps - actual_lps).to be_empty, "Not all lessons from required playlists included"
            expect(expected_schedule_lps - actual_lps).to be_empty, "Not all lessons from schedule streams included"

            # check for any extra lessons being in the list
            expect(actual_lps).to match_array(expected_schedule_lps + expected_playlist_lps)
        end

    end

    describe "get_required_lesson_stream_locale_pack_ids_by_playlist" do

        it "should work" do
            cohort = cohorts(:published_mba_cohort)
            first_playlist_pack_id = SecureRandom.uuid
            second_playlist_pack_id = SecureRandom.uuid

            expect(cohort).to receive(:required_playlist_pack_ids).and_return([first_playlist_pack_id, second_playlist_pack_id, SecureRandom.uuid])
            expect(Playlist).to receive(:stream_locale_pack_ids_for_playlist).exactly(2).times do |playlist_locale_pack_id, locale|
                expect(locale).to eq('locale')
                {
                    first_playlist_pack_id => ['stream_1', 'stream_2'],
                    second_playlist_pack_id => ['stream_3', 'stream_4']
                }[playlist_locale_pack_id]
            end

            expect(cohort.get_required_lesson_stream_locale_pack_ids_by_playlist(2, 'locale')).to eq([
                ['stream_1', 'stream_2'],
                ['stream_3', 'stream_4']
            ])
        end
    end

    describe "get_first_concentration_lesson_stream_locale_pack_ids" do
        it "should work" do
            user = AccessGroup.find_by_name('SUPERVIEWER').users.where(pref_locale: 'en').first
            cohort = cohorts(:published_mba_cohort)
            first_concentration_playlist = Playlist.where.not(locale_pack_id: cohort.required_playlist_pack_ids).first
            cohort.playlist_collections[0]['required_playlist_pack_ids'][1] = first_concentration_playlist.locale_pack_id
            cohort.save!
            expect(cohort.get_first_concentration_lesson_stream_locale_pack_ids(user.locale)).to match_array(first_concentration_playlist.stream_locale_pack_ids)
        end
    end

    describe "get_required_stream_locale_pack_ids" do
        it "should work" do
            cohort = cohorts(:published_mba_cohort)
            required_from_playlists = Playlist.where(locale_pack_id: cohort.required_playlist_pack_ids).pluck(:stream_entries).flatten.map{|e| e["locale_pack_id"]}
            required_from_schedule = cohort.periods.map{|p| p["stream_entries"]}.flatten.select{|se| se["required"]}.map{|se| se["locale_pack_id"]}
            expected_ids = (required_from_playlists + required_from_schedule).uniq
            expect(cohort.get_required_stream_locale_pack_ids).to match_array(expected_ids)
        end

        it "should return empty if none found" do
            cohort = Cohort.create({name: 'New Cohort', title:'title', start_date: Time.now, end_date: Time.now })
            expect(cohort.get_required_stream_locale_pack_ids).to eq([])
        end
    end

    describe "get_specialization_stream_locale_pack_ids" do
        it "should work" do
            cohort = cohorts(:published_mba_cohort)
            expected_ids = Playlist.where(locale_pack_id: cohort.specialization_playlist_pack_ids).pluck(:stream_entries).flatten.map{|e| e["locale_pack_id"]}
            expect(cohort.get_specialization_stream_locale_pack_ids).to match_array(expected_ids)
        end

        it "should return empty if none found" do
            cohort = Cohort.create({name: 'New Cohort', title:'title', start_date: Time.now, end_date: Time.now })
            expect(cohort.get_specialization_stream_locale_pack_ids).to eq([])
        end
    end

    class TestCohortJob < ApplicationJob
        include ScheduledJob

        queue_as :test_cohort_job
    end

    describe "verify_cohort_jobs" do

        it "should do nothing if nil datetime passed in" do
            expect {
                cohorts(:published_mba_cohort).verify_cohort_jobs(nil, TestCohortJob)
            }.not_to raise_error
        end

        it "should not process jobs if the date has passed" do
            cohort = cohorts(:published_mba_cohort)

            cohort.verify_cohort_jobs(Time.now - 10.days, TestCohortJob)
            expect(cohort.jobs_for_job_klass(TestCohortJob).count).to be(0)
        end

        it "should create new job if one is not already queued" do
            cohort = cohorts(:published_mba_cohort)
            Delayed::Job.delete_all

            cohort.verify_cohort_jobs(Time.now + 10.days, TestCohortJob)
            expect(cohort.jobs_for_job_klass(TestCohortJob).count).to be(1)
        end

        it "should recreate jobs if job is already queued" do
            cohort = cohorts(:published_mba_cohort)
            Delayed::Job.delete_all

            # Create a job for this cohort
            TestCohortJob.set(wait_until: Time.now + 10.days).perform_later(cohort.id)

            later = Time.now + 10.days
            cohort.verify_cohort_jobs(later, TestCohortJob)

            expect(cohort.jobs_for_job_klass(TestCohortJob).first.run_at.to_timestamp).to eq(later.to_timestamp)
        end

        it "should work with multiple datetimes passed in" do
            cohort = cohorts(:published_mba_cohort)
            Delayed::Job.delete_all

            datetimes = [Time.now - 10.days, Time.now + 10.days, Time.now + 20.days]
            cohort.verify_cohort_jobs(datetimes, TestCohortJob)
            expect(cohort.jobs_for_job_klass(TestCohortJob).count).to be(2)
        end

        it "should work with hash passed in" do
            cohort = cohorts(:published_mba_cohort)
            Delayed::Job.delete_all

            cohort.verify_cohort_jobs({
                (Time.now + 10.days) => ['arg1', 'arg2']
            }, TestCohortJob)

            jobs = cohort.jobs_for_job_klass(TestCohortJob)
            expect(jobs.size).to be(1)
            arguments = YAML.load(jobs.first.handler).job_data['arguments']
            expect(arguments).to eq([cohort.id, 'arg1', 'arg2'])
        end

        it "should remove jobs if not published" do
            cohort = cohorts(:published_mba_cohort)
            cohort.verify_cohort_jobs(Time.now + 1.day, TestCohortJob)
            expect(cohort.jobs_for_job_klass(TestCohortJob)).not_to be_empty
            cohort.was_published = false
            cohort.verify_cohort_jobs(Time.now + 1.day, TestCohortJob)
            expect(cohort.jobs_for_job_klass(TestCohortJob)).to be_empty
        end

        it "should not create a new job if skip_if_job_running is true and there is already a job" do
            cohort = cohorts(:published_mba_cohort)
            Delayed::Job.delete_all

            # Create a job for this cohort and start it running
            TestCohortJob.perform_later(cohort.id)
            Delayed::Job.first.update!(run_at: Time.now, locked_at: Time.now)

            expect {
                cohort.verify_cohort_jobs(Time.now + 1.minute, TestCohortJob, {skip_if_job_running: true})
            }.not_to change {
                Delayed::Job.pluck(:id)
            }
        end
    end

    describe "raise_if_unpublished_playlist_is_required" do

        # Ideally, we'd assert that this method gets called BEFORE the publish change,
        # but it's kind of hard to make that assertion because you can't really tell
        # exactly when you're before or after the publish change, so we'll just have
        # to settle for asserting that it gets called when published and unpublished
        it "should be called before publish change" do
            cohort = cohorts(:published_mba_cohort)
            expect(cohort).to receive(:raise_if_unpublished_playlist_is_required).exactly(:once)
            cohort.publish!
            expect(cohort).to receive(:raise_if_unpublished_playlist_is_required).exactly(:once)
            cohort.unpublish!
        end

        it "should do nothing if !was_published" do
            cohort = cohorts(:published_mba_cohort)
            expect(cohort).to receive(:was_published).and_return(false)
            expect {
                cohort.raise_if_unpublished_playlist_is_required
            }.not_to raise_error
        end

        describe "when was_published" do
            attr_accessor :cohort

            before(:each) do
                @cohort = cohorts(:published_mba_cohort)
                expect(cohort).to receive(:was_published).and_return(true)
            end

            it "should raise ActiveRecord::RecordInvalid error if an unpublished playlist is marked as required" do
                mock_playlist = double('Playlist')
                expect(cohort).to receive(:unpublished_required_playlists).and_return([mock_playlist])
                expect(mock_playlist).to receive(:title).and_return('FooBarBaz')
                expect {
                    cohort.raise_if_unpublished_playlist_is_required
                }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Cohort cannot be published when the required playlist 'FooBarBaz' has not been published")
            end

            it "should not raise error if no unpublished_required_playlists" do
                expect(cohort).to receive(:unpublished_required_playlists).and_return([])
                expect {
                    cohort.raise_if_unpublished_playlist_is_required
                }.not_to raise_error
            end
        end
    end

    describe "after_publish_change" do

        it "should verify jobs" do
            cohort = cohorts(:published_mba_cohort)
            expect(cohort).to receive(:verify_cohort_jobs).with(cohort.on_period_action_job_details, Cohort::OnPeriodActionJob)
            expect(cohort).to receive(:verify_cohort_jobs).with(cohort.on_period_exercise_job_details, Cohort::OnPeriodExerciseJob)
            expect(cohort).to receive(:verify_cohort_jobs).with(cohort.on_period_start_job_details, Cohort::OnPeriodStartJob)
            expect(cohort).to receive(:verify_cohort_jobs).with(cohort.enrollment_deadline, Cohort::UpdateCohortStatusChangesAtEnrollmentDeadlineJob)

            # need to mock this out because it changes when the published status changes
            expect(cohort).to receive(:admission_round_ends_soon_job_details).and_return("admission_round_ends_soon_job_details")
            expect(cohort).to receive(:verify_cohort_jobs).with("admission_round_ends_soon_job_details", Cohort::AdmissionRoundEndsSoonJob)
            expect(cohort).to receive(:admission_round_ends_tomorrow_job_details).and_return("admission_round_ends_tomorrow_job_details")
            expect(cohort).to receive(:verify_cohort_jobs).with("admission_round_ends_tomorrow_job_details", Cohort::AdmissionRoundEndsTomorrowJob)
            expect(cohort).to receive(:decision_day_deferral_reminder_job_details).and_return("decision_day_deferral_reminder_job_details")
            expect(cohort).to receive(:verify_cohort_jobs).with("decision_day_deferral_reminder_job_details", Cohort::DecisionDayDeferralReminderJob)
            expect(cohort).to receive(:ensure_enrollment_agreements_if_necessary).exactly(1).times
            cohort.unpublish!
        end

        describe "admission round mid-point job" do
            attr_reader :cohort

            before(:each) do
                @cohort = cohorts(:published_mba_cohort)
                expect(@cohort.admission_rounds.size > 1).to be(true)
                expect(@cohort).not_to be_nil
            end

            it "should also verify the job for the next cohort" do
                # we need to update the next cohort's jobs as well because updating this one may
                # have changed the start date for the next
                attrs = cohort.attributes
                attrs.delete("id")
                attrs.delete("playlist_pack_ids")
                attrs.delete("application_deadline")
                attrs.delete("enrollment_warning_days_offset")
                attrs.delete("enrollment_expulsion_days_offset")
                attrs.delete("stripe_plan_id")
                attrs.delete("stripe_plan_amount")
                next_cohort = Cohort.create!(attrs)
                next_cohort.start_date = cohort.start_date + 3.months
                next_cohort.publish!
                RefreshMaterializedContentViews.refresh

                CohortPromotion.destroy_all
                Cohort.where(program_type: cohort.program_type)
                    .where.not(id: [cohort.id, next_cohort.id])
                    .destroy_all

                cohort.publish!
                expect(cohort.jobs_for_job_klass(Cohort::AdmissionRoundMidpointJob).count).to eq(1)
                expect(next_cohort.jobs_for_job_klass(Cohort::AdmissionRoundMidpointJob).count).to eq(2)

            end
        end

    end

    describe "verify_setup_slack_channels_jobs" do

        it "should verify_setup_slack_channels_job for each slack room on publish if slack_channels_that_receive_automated_messages has changed" do
            cohort = cohorts(:published_mba_cohort)
            old_slack_channels = cohort.published_version.slack_channels_that_receive_automated_messages
            cohort.periods.first["exercises"] = [{
                "id" => SecureRandom.uuid,
                "hours_offset_from_end" => -48,
                "channel" => "#some_channel",
                "message" => "Some required message"
            }]
            expect(cohort).to receive(:verify_setup_slack_channels_jobs).and_call_original
            expect(cohort.slack_rooms).not_to be_empty
            cohort.slack_rooms.each { |slack_room| expect(slack_room).to receive(:verify_setup_slack_channels_job) }
            cohort.publish!
            new_slack_channels = cohort.published_version.slack_channels_that_receive_automated_messages
            expect(old_slack_channels).not_to eq(new_slack_channels)
        end

        it "should not verify_setup_slack_channels_job for any of the slack rooms on publish if slack_channels_that_receive_automated_messages has not changed" do
            cohort = cohorts(:published_mba_cohort)
            old_slack_channels = cohort.published_version.slack_channels_that_receive_automated_messages
            expect(cohort).to receive(:verify_setup_slack_channels_jobs).and_call_original
            expect(cohort.slack_rooms).not_to be_empty
            cohort.slack_rooms.each { |slack_room| expect(slack_room).not_to receive(:verify_setup_slack_channels_job) }
            cohort.publish!
            new_slack_channels = cohort.published_version.slack_channels_that_receive_automated_messages
            expect(old_slack_channels).to eq(new_slack_channels)
        end

        it "should not verify_setup_slack_channels_job for any of the slack rooms if unpublished" do
            cohort = cohorts(:published_mba_cohort)
            old_slack_channels = cohort.published_version.slack_channels_that_receive_automated_messages
            expect(cohort).to receive(:verify_setup_slack_channels_jobs).and_call_original
            expect(cohort.slack_rooms).not_to be_empty
            cohort.slack_rooms.each { |slack_room| expect(slack_room).not_to receive(:verify_setup_slack_channels_job) }
            cohort.unpublish!
        end
    end

    describe "ensure_enrollment_agreements_if_necessary" do

        it "should return if !requires_enrollment_agreement?" do
            cohort = Cohort.first
            expect(cohort).to receive(:requires_enrollment_agreement?).and_return(false)
            expect(cohort.cohort_applications).not_to receive(:select)
            expect(EnsureEnrollmentAgreementJob).not_to receive(:ensure_job)
            cohort.send(:ensure_enrollment_agreements_if_necessary)
        end

        it "should return if no enrollment_agreement_template_id" do
            cohort = Cohort.first
            expect(cohort).to receive(:requires_enrollment_agreement?).and_return(true)
            cohort.update_attribute(:enrollment_agreement_template_id, nil)
            expect(cohort.cohort_applications).not_to receive(:select)
            expect(EnsureEnrollmentAgreementJob).not_to receive(:ensure_job)
            cohort.send(:ensure_enrollment_agreements_if_necessary)
        end

        it "should call verify_cohort_jobs" do
            cohort = Cohort.first
            expect(cohort).to receive(:requires_enrollment_agreement?).and_return(true)
            cohort.update_attribute(:enrollment_agreement_template_id, SecureRandom.uuid)
            expect(cohort).to receive(:verify_cohort_jobs).with(cohort.decision_date, Cohort::DecisionDayEnrollmentAgreementsJob)
            cohort.send(:ensure_enrollment_agreements_if_necessary)
        end

    end

    describe "on_period_start_job_details" do
        it "should return an entry for each period" do
            cohort = cohorts(:published_mba_cohort)
            expect(cohort.periods).not_to be_empty
            details = cohort.on_period_start_job_details
            expect(details.size).to eq(cohort.periods.size)
            expect(details[cohort.start_time_for_period(1) + 30.minutes]).to eq([1])
            expect(details[cohort.start_time_for_period(2) + 30.minutes]).to eq([2])
        end
    end

    describe "on_period_action_job_details" do
        it "should return an entry for each period action" do
            cohort = cohorts(:published_mba_cohort)
            expect(cohort.periods).not_to be_empty
            ids = [SecureRandom.uuid, SecureRandom.uuid, SecureRandom.uuid, SecureRandom.uuid]

            cohort.periods.first["actions"] = [{
                "id" => ids[0],
                "type" => "foo type",
                "rule" => "foo rule",
                "days_offset_from_end" => -1
            }, {
                "id" => ids[1],
                "type" => "foo type 2",
                "rule" => "foo rule 2",
                "days_offset_from_end" => -2
            }]
            cohort.periods[3]["actions"] = [{
                "id" => ids[2],
                "type" => "bar type",
                "rule" => "bar rule",
                "days_offset_from_end" => -1
            }, {
                "id" => ids[3],
                "type" => "bar type 2",
                "rule" => "bar rule 2",
                "days_offset_from_end" => -2
            }]

            details = cohort.on_period_action_job_details
            expect(details.size).to eq(4)

            values = details.values
            expect(values[0]).to match_array([1, ids[0]])
            expect(values[1]).to match_array([1, ids[1]])
            expect(values[2]).to match_array([4, ids[2]])
            expect(values[3]).to match_array([4, ids[3]])
        end
    end

    describe "on_period_exercise_job_details" do
        it "should return an entry for each period exercise" do
            cohort = cohorts(:published_mba_cohort)
            expect(cohort.periods).not_to be_empty
            ids = [SecureRandom.uuid, SecureRandom.uuid, SecureRandom.uuid, SecureRandom.uuid]

            cohort.periods.first["exercises"] = [{
                "id" => ids[0],
                "hours_offset_from_end" => -24
            }, {
                "id" => ids[1],
                "hours_offset_from_end" => -48
            }]
            cohort.periods[3]["exercises"] = [{
                "id" => ids[2],
                "hours_offset_from_end" => -24
            }, {
                "id" => ids[3],
                "hours_offset_from_end" => -48
            }]

            details = cohort.on_period_exercise_job_details
            expect(details.size).to eq(4)

            values = details.values
            expect(values[0]).to match_array([1, ids[0]])
            expect(values[1]).to match_array([1, ids[1]])
            expect(values[2]).to match_array([4, ids[2]])
            expect(values[3]).to match_array([4, ids[3]])
        end
    end

    describe "admission_round_midpoint_job_details" do
        attr_reader :cohort

        before(:each) do
            @cohort = cohorts(:published_mba_cohort)
            expect(cohort.admission_rounds.size > 1).to be(true)
            expect(cohort).not_to be_nil
        end

        it "should have an entry in the middle of each round" do
            first_round_end = cohort.start_date + cohort.admission_rounds.first['application_deadline_days_offset'].days
            second_round_end = cohort.start_date + cohort.admission_rounds.second['application_deadline_days_offset'].days
            midpoint = first_round_end + (second_round_end - first_round_end)/2

            expect(cohort.admission_round_midpoint_job_details).to include(midpoint)
        end

        it "should not create entry for round in the past" do
            cohort.start_date = 1.year.ago
            cohort.publish!
            RefreshMaterializedContentViews.refresh
            expect(cohort.admission_round_midpoint_job_details).to be_empty
        end
    end

    describe "admission_round_ends_job_details" do
        attr_reader :cohort

        before(:each) do
            @cohort = cohorts(:published_mba_cohort)
            expect(cohort.admission_rounds.size > 1).to be(true)
            expect(cohort).not_to be_nil
        end

        it "should have an entry for each round relative to the passed in offset" do
            first_round_end = cohort.start_date + cohort.admission_rounds.first['application_deadline_days_offset'].days
            second_round_end = cohort.start_date + cohort.admission_rounds.second['application_deadline_days_offset'].days

            expect(Time).to receive(:now).at_least(1).times.and_return(first_round_end - 50.hours)

            expect(cohort.admission_round_ends_job_details(-2.days)).to eq([first_round_end - 2.days, second_round_end - 2.days])
        end

        it "should not have entries for rounds in the past" do
            first_round_end = cohort.start_date + cohort.admission_rounds.first['application_deadline_days_offset'].days
            second_round_end = cohort.start_date + cohort.admission_rounds.second['application_deadline_days_offset'].days

            # first round end is in the past, so it is ignored
            expect(Time).to receive(:now).at_least(1).times.and_return(first_round_end + 25.hours)

            expect(cohort.admission_round_ends_job_details(-2.days)).to eq([second_round_end - 2.days])
        end
    end

    describe "admission_round_ends_soon_job_details" do

        it "should return admission_round_ends_job_details for 5 day offset" do
            cohort = cohorts(:published_mba_cohort)
            expect(cohort).to receive(:admission_round_ends_job_details).with(-6.days).and_return('admission_round_ends_job_details')
            expect(cohort.admission_round_ends_soon_job_details).to eq('admission_round_ends_job_details')
        end
    end

    describe "admission_round_ends_tomorrow_job_details" do

        it "should return admission_round_ends_job_details for 2 day offset" do
            cohort = cohorts(:published_mba_cohort)
            expect(cohort).to receive(:admission_round_ends_job_details).with(-2.days).and_return('admission_round_ends_job_details')
            expect(cohort.admission_round_ends_tomorrow_job_details).to eq('admission_round_ends_job_details')
        end
    end

    describe "decision_day_deferral_reminder_job_details" do
        attr_accessor :cohort

        before(:each) do
            @cohort = cohorts(:published_emba_cohort)
        end

        it "should return nil if program type doesn't support deferral" do
            expect(cohort.program_type_config).to receive(:supports_deferral?).and_return(false)
            expect(cohort.decision_day_deferral_reminder_job_details).to be_nil
        end

        describe "when program type supports_deferral?" do

            before(:each) do
                allow(cohort.program_type_config).to receive(:supports_deferral?).and_return(true)
            end

            it "should return nil if cohort has no admission rounds" do
                mock_relation = double('AdmissionRound::ActiveRecord_Relation')
                expect(AdmissionRound).to receive(:where).with(cohort_id: cohort.id).and_return(mock_relation)
                expect(mock_relation).to receive(:reorder).with(:index).and_return([])
                expect(cohort.decision_day_deferral_reminder_job_details).to be_nil
            end

            describe "when cohort has admission rounds" do

                before(:each) do
                    expect(AdmissionRound.where(cohort_id: cohort.id).count).to be > 0
                end

                it "should return the decision_date for the cohort's last admission round" do
                    last_admission_round = AdmissionRound.where(cohort_id: cohort.id).reorder(:index).last
                    expect(cohort.decision_day_deferral_reminder_job_details).to eq(last_admission_round.decision_date)
                end
            end
        end
    end

    describe "registration_deadline" do

        before(:each) do
            @cohort = cohorts(:published_emba_cohort)
            allow(@cohort).to receive(:start_date).and_return(Time.now)
            allow(@cohort).to receive(:registration_deadline_days_offset).and_return(-3)
        end

        it "should work" do
            expect(@cohort.registration_deadline).to eq(@cohort.start_date.add_dst_aware_offset(-3.days))
        end

        it "should return nil if not supports_registration_deadline?" do
            allow(@cohort).to receive(:supports_registration_deadline?).and_return(false)
            expect(@cohort.registration_deadline).to be(nil)
        end

    end

    describe "early_registration_deadline" do

        before(:each) do
            @cohort = cohorts(:published_emba_cohort)
            allow(@cohort).to receive(:start_date).and_return(Time.now)
            allow(@cohort).to receive(:early_registration_deadline_days_offset).and_return(-8)
        end

        it "should work" do
            expect(@cohort.early_registration_deadline).to eq(@cohort.start_date.add_dst_aware_offset(-8.days))
        end

        it "should return nil if not supports_early_registration_deadline?" do
            allow(@cohort).to receive(:supports_early_registration_deadline?).and_return(false)
            expect(@cohort.early_registration_deadline).to be(nil)
        end

    end

    describe "as_json" do

        it "should only run one query if no fields passed in" do
            cohort = Cohort.first
            cohort.periods_as_json # warm rails cache
            expect(ActiveRecord::Base.connection).not_to receive(:exec_query).and_call_original
            cohort.as_json
        end

        it "should only return basic fields if instructed" do
            cohort = Cohort.first
            expect(Cohort.first.as_json(fields: ['BASIC_FIELDS'])).to eq({
                id: cohort.id,
                name: cohort.name,
                program_type: cohort.program_type,
            }.stringify_keys)
        end

        it "should only return update fields if instructed" do
            user = User.first
            cohort = Cohort.first
            json = cohort.as_json({
                "user_id" => user.id,
                fields: ['UPDATE_FIELDS']
            })

            expected = {}
            expected["updated_at"] = cohort.updated_at.to_time.to_timestamp
            expected["created_at"] = cohort.created_at.to_time.to_timestamp
            expected["published_at"] = nil
            expected["slack_room_assignment"] = cohort.slack_room_assignment.as_json
            expected["diff_from_published_version"] = cohort.diff_from_published_version.as_json
            expected["slack_rooms"] = cohort.slack_rooms.as_json
            expect(json).to eq(expected)
        end

        it "should return the list of stripe_plans" do
            cohort = Cohort.first
            expected = [{'id': 'plan_a', 'amount': 10000, 'frequency': 'monthly'}, {'id': 'plan_b', 'amount': 20000, 'frequency': 'bi_annual'}]
            cohort.stripe_plans = expected
            expect(cohort.as_json['stripe_plans']).to match_array(expected.as_json)
        end

        it "should not get_foundations_lesson_stream_locale_pack_ids if no user_id option" do
            cohort = Cohort.first
            expect(cohort).not_to receive(:get_foundations_lesson_stream_locale_pack_ids)
            cohort.as_json
        end

        describe "projects" do

            it "should include project-related fields in the json" do
                cohort = cohorts(:published_mba_cohort)
                projects = [
                    LearnerProject.create!(
                        title: 'project1',
                        project_documents: [{"for" => "project 1"}],
                        requirement_identifier: 'requirement_identifier'
                    ),
                    LearnerProject.create!(
                        title: 'project2',
                        project_documents: [{"for" => "project 2"}],
                        requirement_identifier: 'requirement_identifier'
                    )
                ]
                cohort.learner_project_ids = [projects[0].id]
                cohort.periods[0]['learner_project_ids'] = [projects[1].id]

                json = cohort.as_json
                expect(json['learner_projects']).to eq([projects[0].as_json])

            end

        end

        describe "cohort_level_learner_projects" do
            it "should work" do
                cohort = cohorts(:published_mba_cohort)
                project = LearnerProject.create!(
                    title: 'project1',
                    project_documents: [{"for" => "project 1"}],
                    requirement_identifier: 'requirement_identifier'
                )
                cohort.learner_project_ids = [project.id]
                expect(cohort.cohort_level_learner_projects).to eq([project.as_json])
            end
        end

        describe "with user_id option" do

            it "should not get_foundations_lesson_stream_locale_pack_ids if set to nil" do
                cohort = Cohort.first
                expect(cohort).not_to receive(:get_foundations_lesson_stream_locale_pack_ids)
                cohort.as_json(user_id: nil)
            end

            it "should get_foundations_lesson_stream_locale_pack_ids for user's locale" do
                user = User.first
                cohort = Cohort.first
                expect(cohort).to receive(:get_foundations_lesson_stream_locale_pack_ids).with(user.locale).and_return([])
                cohort.as_json(user_id: user.id)
            end

            # iIn the registration flow, if the user is not valid, we can hit this with the user id of
            # as user who has not been saved.  To repro, register from a form that presets the emba program type using an existing email
            # and no password
            it "should not blow up if user is not found" do
                cohort = Cohort.first
                expect(cohort).to receive(:get_foundations_lesson_stream_locale_pack_ids).with(I18n.default_locale.to_s).and_return([])
                cohort.as_json(user_id: SecureRandom.uuid)
            end
        end

        describe "ADMIN_FIELDS" do
            it "should include some extra stuff with ADMIN_FIELDS" do
                cohort = Cohort.first
                cohort.internal_notes = 'notes'
                cohort.enrollment_agreement_template_id = '123abc'


                # this one (and others) is always included
                expect(cohort.as_json['title']).to eq(cohort.title)
                expect(cohort.as_json(fields: ['ADMIN_FIELDS'])['title']).to eq(cohort.title)

                # these are only included when ADMIN_FIELDS are requested
                expect(cohort.as_json['internal_notes']).to be_nil
                expect(cohort.as_json(fields: ['ADMIN_FIELDS'])['internal_notes']).to eq('notes')
                expect(cohort.as_json['enrollment_agreement_template_id']).to be_nil
                expect(cohort.as_json(fields: ['ADMIN_FIELDS'])['enrollment_agreement_template_id']).to eq('123abc')
            end
        end

    end

    describe "destroy" do

        it "should be possible to destroy a published cohort" do

            cohort = Cohort.all_published.first
            expect(cohort).to receive(:refresh_content_views_and_tables_on_publish_change)
            expect {
                cohort.destroy
            }.not_to raise_error

        end

    end

    describe "merge_hash" do
        it "should update a bunch of stuff" do
            cohort = Cohort.first

            specialization_playlist_pack_ids = [SecureRandom.uuid]
            learner_project_ids = [SecureRandom.uuid]

            cohort.merge_hash({
                name: 'updated',
                specialization_playlist_pack_ids: specialization_playlist_pack_ids,
                periods: [{'updated': true}],
                program_type: 'updated',
                num_required_specializations: 42,
                start_date: Time.parse('2016/01/01').to_timestamp,
                end_date: Time.parse('2016/01/02').to_timestamp,
                internal_notes: 'notes',
                diploma_generation_days_offset_from_end: 10,
                project_submission_email: 'project_submission@pedago.com',
                learner_project_ids: learner_project_ids,
                playlist_collections: [{
                    title: 'Some Playlist Collection Title',
                    required_playlist_pack_ids: ['some', 'playlist', 'pack', 'ids']
                }]
            }.with_indifferent_access)
            expect(cohort.name).to eq('updated')
            expect(cohort.specialization_playlist_pack_ids).to eq(specialization_playlist_pack_ids)
            expect(cohort.periods).to eq([{'updated' => true}])
            expect(cohort.num_required_specializations).to eq(42)
            expect(cohort.program_type).to eq('updated')
            expect(cohort.start_date).to eq(Time.parse('2016/01/01'))
            expect(cohort.end_date).to eq(Time.parse('2016/01/02'))
            expect(cohort.internal_notes).to eq('notes')
            expect(cohort.diploma_generation_days_offset_from_end).to eq(10)
            expect(cohort.project_submission_email).to eq('project_submission@pedago.com')
            expect(cohort.learner_project_ids).to eq(learner_project_ids)
            expect(cohort.playlist_collections).to eq([{
                "title" => 'Some Playlist Collection Title',
                "required_playlist_pack_ids" => ['some', 'playlist', 'pack', 'ids']
            }])
        end

        it "should update slack rooms" do
            cohort = cohorts(:published_mba_cohort)
            slack_rooms = cohort.slack_rooms.limit(3)
            slack_rooms.each do |s|
                CohortApplication.where(cohort_slack_room_id: s.id).update_all(cohort_slack_room_id: nil)
            end
            expect(slack_rooms.size).to eq(3) # sanity check

            id = SecureRandom.uuid
            hash = {
                slack_rooms: [
                    slack_rooms[0].as_json,
                    slack_rooms[1].as_json.merge({
                        title: 'new title',
                        url: 'new url',
                        admin_token: 'new token',
                        cohort_id: cohort.id
                    }),
                    {
                        id: id,
                        title: 'new room',
                        url: 'new room url',
                        admin_token: 'new room token',
                        cohort_id: cohort.id
                    }
                ]
            }.as_json
            cohort.merge_hash(hash)
            cohort.save!

            # 1 slack room should have been deleted
            expect(CohortSlackRoom.find_by_id(slack_rooms[2].id)).to be_nil

            # the existing rooms should match what was passed in
            reloaded_slack_rooms = cohort.slack_rooms.reload
            json = reloaded_slack_rooms.as_json.map { |j| j.slice('id', 'title', 'url', 'admin_token') }
            expected_json = hash['slack_rooms'].as_json.map { |j| j.slice('id', 'title', 'url', 'admin_token') }
            expect(json).to match_array(expected_json)

            # we had a bug where unchanged slack rooms were being re-created
            orig_created_at = slack_rooms[0].created_at
            orig_updated_at = slack_rooms[0].updated_at
            slack_rooms[0].reload
            expect(slack_rooms[0].created_at).to eq(orig_created_at)
            expect(slack_rooms[0].updated_at).to eq(orig_updated_at)
        end
    end

    describe "playlist_pack_ids" do

        it "should return the required_playlist_pack_ids and specialization_playlist_pack_ids" do
            cohort = Cohort.first
            expected_ids = [SecureRandom.uuid, SecureRandom.uuid, SecureRandom.uuid, SecureRandom.uuid]
            expect(cohort).to receive(:required_playlist_pack_ids).and_return([expected_ids[0], expected_ids[1]])
            expect(cohort).to receive(:specialization_playlist_pack_ids).and_return([expected_ids[2], expected_ids[3]])
            expect(cohort.playlist_pack_ids).to match_array(expected_ids)
        end
    end

    describe "with_users_for_action" do
        it "should work" do
            cohort = Cohort.joins(:accepted_users).detect{ |c| c.accepted_users.size > 1 }
            applications_to_keep = cohort.cohort_applications.where(status: 'accepted').limit(2).pluck(:id)
            cohort.cohort_applications.where.not(id: applications_to_keep).delete_all

            period = cohort.periods.first
            period["actions"] = [{
                "id" => SecureRandom.uuid,
                "type" => "expulsion_warning",
                "rule" => "requirements_not_met",
                "days_offset_from_end" => -1
            }]
            cohort.publish!

            run_requirements_not_met(cohort)

            expect { |a| cohort.with_users_for_action(period, period["actions"][0], &a) }.to yield_control.exactly(1).times
        end

        describe "expulsion" do
            it "should skip if skip_period_expulsion is true" do
                @cohort = Cohort.first
                @cohort.cohort_applications.delete_all

                period = @cohort.periods.first
                period["actions"] = [{
                    "id" => SecureRandom.uuid,
                    "type" => "expulsion_warning",
                    "rule" => "requirements_not_met",
                    "days_offset_from_end" => -1
                }]
                @cohort.publish!

                @users = User.joins(:cohort_applications).where("cohort_applications.status = 'accepted'").limit(2).map do |user|
                    application = user.accepted_application
                    application.skip_period_expulsion = true
                    application.cohort = @cohort
                    application.cohort_slack_room_id = @cohort.slack_rooms[0]&.id
                    application.save!

                    # Add one more application for another cohort with skip_period_expulsion set to false to test that it doesn't affect
                    # anything
                    new_cohort = Cohort.where("stripe_plans IS NULL AND id <> '#{@cohort.id}'").first
                    CohortApplication.create!({
                        user_id: user.id,
                        cohort_id: new_cohort.id,
                        applied_at: Time.now,
                        status: 'deferred',
                        skip_period_expulsion: false
                    })
                end

                run_requirements_not_met(@cohort)

                @cohort.reload
                expect { |a| @cohort.with_users_for_action(period, period["actions"][0], &a) }.to yield_control.exactly(0).times
            end

            it "should skip if user is already graduated" do
                @cohort = Cohort.first
                @cohort.cohort_applications.delete_all

                period = @cohort.periods.first
                period["actions"] = [{
                    "id" => SecureRandom.uuid,
                    "type" => "expulsion_warning",
                    "rule" => "requirements_not_met",
                    "days_offset_from_end" => -1
                }]
                @cohort.publish!

                @users = User.joins(:cohort_applications).where("cohort_applications.status = 'accepted'").limit(2).map do |user|
                    application = user.accepted_application
                    application.skip_period_expulsion = false

                    # Meat of the test
                    application.graduation_status = 'graduated'

                    application.cohort = @cohort
                    application.cohort_slack_room_id = @cohort.slack_rooms[0]&.id
                    application.save!
                end

                run_requirements_not_met(@cohort)

                @cohort.reload
                expect { |a| @cohort.with_users_for_action(period, period["actions"][0], &a) }.to yield_control.exactly(0).times
            end
        end
    end

    describe "event_attributes" do

        it "should include is_paid_cert" do
            cohort = cohorts(:published_mba_cohort)
            expect(cohort.program_type_config).to receive(:is_paid_cert?).and_return(true)
            event_attributes = cohort.event_attributes(nil)
            expect(event_attributes[:is_paid_cert]).to be(true)
        end

        it "should return deadline attrs relative_to_threshold and timezone when timezone is passed in" do
            cohort = cohorts(:published_emba_cohort)
            timezone = 'Europe/Warsaw'
            now = Time.now

            mock_application_deadline = double('Time')
            mock_registration_deadline = double('Time')
            mock_early_registration_deadline = double('Time')

            expect(cohort).to receive(:application_deadline).and_return(mock_application_deadline)
            expect(cohort).to receive(:registration_deadline).and_return(mock_registration_deadline)
            expect(cohort).to receive(:early_registration_deadline).and_return(mock_early_registration_deadline)

            expect(mock_application_deadline).to receive(:relative_to_threshold).with(timezone: timezone).and_return(now)
            expect(mock_registration_deadline).to receive(:relative_to_threshold).with(timezone: timezone).and_return(now)
            expect(mock_early_registration_deadline).to receive(:relative_to_threshold).with(timezone: timezone).and_return(now)

            event_attributes = cohort.event_attributes(timezone)

            expect(event_attributes[:cohort_application_deadline]).to eq(now.to_timestamp)
            expect(event_attributes[:cohort_registration_deadline]).to eq(now.to_timestamp)
            expect(event_attributes[:cohort_early_registration_deadline]).to eq(now.to_timestamp)
        end
    end

    describe "event_attributes_for_period" do
        it "should work for standard period" do
            cohort = cohorts(:published_mba_cohort)
            period = cohort.periods.first

            expect(cohort.event_attributes_for_period(period)).to include(
                cohort_id: cohort.id,
                program_type: 'mba',
                period_title: 'period to do playlist title',
                period_index: 1,
                period_style: 'standard',
                period_start: cohort.start_time_for_period(period).to_timestamp,
                period_end: cohort.end_time_for_period(period).to_timestamp
            )

             # test title fallback logic
            period['title'] = ''
            expect(cohort.event_attributes_for_period(period)[:period_title]).to eq('Week 1')

            period['title'] = nil
            expect(cohort.event_attributes_for_period(period)[:period_title]).to eq('Week 1')
        end

        it "should add attributes for an exam period" do
            fixture_file_upload('files/test.png', 'image/png')

            cohort = Cohort.first

            period = {
                "style" => 'exam',
                "exam_style" => 'intermediate'
            }

            allow(cohort).to receive(:index_for_period).and_return(0)
            allow(cohort).to receive(:start_time_for_period).and_return(Time.now + 10.days)
            allow(cohort).to receive(:end_time_for_period).and_return(Time.now + 20.days)

            hash = cohort.event_attributes_for_period(period)
            expect(hash[:period_style]).to eq('exam')
            expect(hash[:period_exam_style]).to eq('intermediate')
        end

        it "should add attributes for a review period" do
            fixture_file_upload('files/test.png', 'image/png')

            cohort = Cohort.first

            period = {
                "style" => 'review',
                "exam_style" => 'intermediate'
            }

            allow(cohort).to receive(:index_for_period).and_return(0)
            allow(cohort).to receive(:start_time_for_period).and_return(Time.now + 10.days)
            allow(cohort).to receive(:end_time_for_period).and_return(Time.now + 20.days)

            hash = cohort.event_attributes_for_period(period)
            expect(hash[:period_style]).to eq('review')
            expect(hash[:period_exam_style]).to eq('intermediate')
        end

        it "should add attributes for a project period" do
            project = LearnerProject.first

            cohort = Cohort.first

            period = {
                "style" => 'project',
                "project_style" => 'foo_project',
                "learner_project_ids" => [project.id]
            }

            allow(cohort).to receive(:index_for_period).and_return(0)
            allow(cohort).to receive(:start_time_for_period).and_return(Time.now + 10.days)
            allow(cohort).to receive(:end_time_for_period).and_return(Time.now + 20.days)

            hash = cohort.event_attributes_for_period(period)
            expect(hash[:period_style]).to eq('project')
            expect(hash[:period_project_style]).to eq('foo_project')
            expect(hash[:period_learner_projects]).to eq([project.as_json])
        end
    end

    describe "next_exam_period" do
        it "should return the next exam period relative to the index argument passed in" do
            cohort = cohorts(:published_mba_cohort)
            exam_period_index = cohort.periods.find_index { |period| period["style"] == 'exam' }
            expect(exam_period_index).to be > 0

            # the value passed to next_exam_period should be 1-based
            expect(cohort.next_exam_period(1)).to eq(cohort.periods[exam_period_index])
        end

        it "should return nil if no exam period is found" do
            cohort = cohorts(:published_mba_cohort)
            exam_period_index = cohort.periods.find_index { |period| period["style"] == 'exam' }
            cohort.periods.delete_at(exam_period_index) # remove the exam period from the schedule

            # the value passed to next_exam_period should be 1-based
            expect(cohort.next_exam_period(1)).to be_nil
        end
    end

    describe "current_period_index" do
        it "should return the correct index if during the schedule" do
            cohort = cohorts(:published_mba_cohort)
            cohort.start_date = Time.now - 11.days
            cohort.periods[0]['days_offset'] = 0
            cohort.periods[0]['days'] = 2
            cohort.periods[1]['days_offset'] = 3
            cohort.periods[1]['days'] = 5
            cohort.periods[2]['days_offset'] = 0
            cohort.periods[2]['days'] = 2
            expect(cohort.current_period_index).to be(2)
        end

        it "should return the correct index if during the offset" do
            cohort = cohorts(:published_mba_cohort)
            cohort.start_date = Time.now - 11.days
            cohort.periods[0]['days_offset'] = 0
            cohort.periods[0]['days'] = 2
            cohort.periods[1]['days_offset'] = 3
            cohort.periods[1]['days'] = 5
            cohort.periods[2]['days_offset'] = 2
            expect(cohort.current_period_index).to be(2)
        end

        it "should return -1 if before the schedule" do
            cohort = cohorts(:published_mba_cohort)
            cohort.start_date = Time.now - 100.years
            expect(cohort.current_period_index).to be_nil
        end

        it "should return nil if after the schedule" do
            cohort = cohorts(:published_mba_cohort)
            cohort.start_date = Time.now + 100.years
            expect(cohort.current_period_index).to be(-1)
        end
    end

    describe "start_time_for_period" do
        it "should work" do
            cohort = cohorts(:published_mba_cohort)
            cohort.periods[0]['days_offset'] = 0
            cohort.periods[0]['days'] = 2
            cohort.periods[1]['days_offset'] = 3

            # check that calculation works for first period and that the
            # method accepts either a period or an index
            expect(cohort.start_time_for_period(cohort.periods[0])).to eq(cohort.start_date)

            # we pass in 1 as the period_index because this follows the way
            # published_cohort_periods treats the 0-period as all the time before
            # the start date
            expect(cohort.start_time_for_period(1)).to eq(cohort.start_date)

            # check that calculation works for second period and that the
            # method accepts either a period or an index
            start_time_for_second_period = cohort.start_date + 2.days + 3.days
            expect(cohort.start_time_for_period(cohort.periods[1])).to eq(start_time_for_second_period)
            expect(cohort.start_time_for_period(2)).to eq(start_time_for_second_period)
        end
    end

    describe "end_time_for_period" do
        it "should work" do
            cohort = cohorts(:published_mba_cohort)
            cohort.periods[0]['days_offset'] = 0
            cohort.periods[0]['days'] = 2
            cohort.periods[1]['days_offset'] = 3
            cohort.periods[1]['days'] = 6

            # check that calculation works for first period and that the
            # method accepts either a period or an index
            expect(cohort.end_time_for_period(cohort.periods[0])).to eq(cohort.start_date + 2.days)

            # see note above in start_time_for_period about 1-indexing
            expect(cohort.end_time_for_period(1)).to eq(cohort.start_date + 2.days)

            # check that calculation works for second period and that the
            # method accepts either a period or an index
            end_time_for_second_period = cohort.start_date + 2.days + 3.days + 6.days
            expect(cohort.end_time_for_period(cohort.periods[1])).to eq(end_time_for_second_period)
            expect(cohort.end_time_for_period(2)).to eq(end_time_for_second_period)
        end

        it "should agree with published_cohort_periods" do

            cohort = cohorts(:published_mba_cohort)
            cohort.start_date = Time.parse('2018/03/01 04:00:00 EST')
            cohort.periods[0]['days_offset'] = 0
            cohort.periods[0]['days'] = 7
            cohort.periods[1]['days_offset'] = 3
            cohort.periods[1]['days'] = 7
            cohort.periods[2]['days_offset'] = 0
            cohort.periods[2]['days'] = 7
            cohort.publish!
            RefreshMaterializedContentViews.refresh(true)

            published_cohort_periods = PublishedCohortPeriod.where(cohort_id: cohort.id).to_a.index_by(&:index)

            expect(cohort.end_time_for_period(1)).to eq(published_cohort_periods[1].period_end)
            expect(cohort.end_time_for_period(2)).to eq(published_cohort_periods[2].period_end)
            expect(cohort.end_time_for_period(3)).to eq(published_cohort_periods[3].period_end)
            expect(cohort.end_time_for_period(3).in_time_zone('America/New_York').hour).to eq(4) # check that we keep the correct hour across dst
        end
    end

    describe "period_for_required_stream_locale_pack_id" do
        it "should return a period if one is found" do
            cohort = cohorts(:published_mba_cohort)
            period = cohort.periods[1]
            locale_pack_id = period['stream_entries'].detect { |entry| entry['required'] }['locale_pack_id']
            expect(locale_pack_id).not_to be_nil
            expect(cohort.period_for_required_stream_locale_pack_id(locale_pack_id)).to eq(period)
        end

        it "should return nil if none is found" do
            cohort = cohorts(:published_mba_cohort)
            expect(cohort.period_for_required_stream_locale_pack_id(SecureRandom.uuid)).to be_nil
        end
    end

    describe "expected_percent_complete" do
        it "should work" do
            cohort = cohorts(:published_mba_cohort).published_version

            required_pack_count = ActiveRecord::Base.connection.execute("
                select
                    count(*) c
                from published_cohort_stream_locale_packs
                where
                    required=true
                    and cohort_id='#{cohort.attributes['id']}'
            ").to_a.first['c']

            cumulative_required_pack_ids = Set.new

            cohort.periods.each_with_index do |period, i|

                stream_locale_pack_ids = period['stream_entries'].map { |e| e['locale_pack_id'] if e['required'] }.compact
                cumulative_required_pack_ids += stream_locale_pack_ids

                expected_value = cumulative_required_pack_ids.size.to_f / required_pack_count
                actual_value = cohort.expected_percent_complete(i+1) # periods are 1-indexed

                expect(actual_value).to eq(expected_value)

            end
        end
    end

    describe "expected_stream_lpids_complete" do
        it "should work" do
            cohort = cohorts(:published_mba_cohort).published_version

            required_pack_ids = ActiveRecord::Base.connection.execute("
                select
                    unnest(cumulative_required_stream_pack_ids) as expected_stream_lpids_complete
                from published_cohort_periods
                where
                    cohort_id = '#{cohort.attributes['id']}'
                    AND index = 2
            ").to_a.map { |row| row['expected_stream_lpids_complete'] }

            expect(cohort.expected_stream_lpids_complete(2)).to match_array(required_pack_ids)
        end
    end

    def run_requirements_not_met(cohort)
        returned_one = false
        allow_any_instance_of(User).to receive(:has_met_requirements_for_period?).with(cohort, cohort.periods.first) do
            if returned_one
                false
            else
                returned_one = true
                true
            end
        end
    end

    describe "validate_supports_schedule" do

        it "should validate that periods is empty when supports_schedule? is false" do
            cohort = Cohort.first
            expect(cohort).to receive(:supports_schedule?).and_return(false).at_least(1).times
            expect(cohort.supports_schedule?).to be(false)
            expect(cohort.valid?).to be(false)
            expect(cohort.errors.full_messages).to include("Periods must be empty if cohort does not support schedule"), cohort.errors.full_messages.inspect
        end
    end

    describe "validate_supports_payments" do

        it "should validate that stripe_plans is provided when supports_payments? is true" do
            cohort = Cohort.first
            expect(cohort).to receive(:supports_payments?).and_return(true).at_least(1).times
            expect(cohort.supports_payments?).to be(true)
            expect(cohort.valid?).to be(false)
            expect(cohort.errors.full_messages).to include("Stripe plans must be provided if cohort supports payments"), cohort.errors.full_messages.inspect
        end
    end

    describe "isolated_network validations" do

        attr_accessor :cohort
        before(:each) do
            @cohort = Cohort.first
        end

        it "should validate falsiness of isolated_network unless program type config supports_isolated_network?" do
            expect(cohort.program_type_config).to receive(:supports_isolated_network?).and_return(false)
            cohort.isolated_network = true
            expect(cohort.valid?).to be(false)
            expect(cohort.errors.full_messages).to include("Isolated network must be false for cohorts that do not support isolated network flag")
        end

        it "should allow setting isolated_network to true when program type config supports_isolated_network?" do
            expect(cohort.program_type_config).to receive(:supports_isolated_network?).and_return(true)
            cohort.isolated_network = true
            expect(cohort.valid?).to be(true)
        end

    end

    describe "midterm_exam_locale_pack_id" do
        it "should work" do
            cohort = cohorts(:published_mba_cohort_with_exams)
            midterm_locale_pack_id = lesson_streams(:midterm_exam).locale_pack_id
            expect(cohort.midterm_exam_locale_pack_id).to eq(midterm_locale_pack_id)
        end
    end

    describe "final_exam_locale_pack_id" do
        it "should work" do
            cohort = cohorts(:published_mba_cohort_with_exams)
            final_locale_pack_id = lesson_streams(:final_exam).locale_pack_id
            expect(cohort.final_exam_locale_pack_id).to eq(final_locale_pack_id)
        end
    end

    describe "enrollment_deadline" do

        it "should agree with the deadline in published cohorts" do

            # make sure it works across daylight savings
            cohort = Cohort.first
            cohort.start_date = Time.parse('2018/03/08')
            cohort.enrollment_deadline_days_offset = 10
            cohort.publish!
            expect(cohort.enrollment_deadline).to eq(cohort.start_date.add_dst_aware_offset(10.days))
            RefreshMaterializedContentViews.refresh

            ActiveRecord::Base.connection.execute("select id, enrollment_deadline from published_cohorts").to_a.each do |record|
                cohort = Cohort.find(record['id']).published_version
                expect(cohort.enrollment_deadline).to eq(record['enrollment_deadline'])
            end

        end

    end


    describe Cohort::CohortRelationExtensions do
        describe "edit_and_republish" do

            it "should raise if the cohort has been edited since being published" do

                cohort = Cohort.all_published.first
                cohort.update_attribute('name', 'updated')
                expect(cohort.user_facing_change_since_last_published?).to be(true) # sanity check

                expect {
                    Cohort.where(id: cohort.id).edit_and_republish do |_cohort|
                        expect(_cohort).to receive(:save!).and_call_original
                        _cohort.name = 'updated again'
                    end
                }.to raise_error("Cannot automatically republish cohort #{cohort.name.inspect} because it has been changed since it was last published")

            end

            it "should not raise if the only differences are not user-facing" do
                cohort = Cohort.all_published.first
                cohort.update_attribute('internal_notes', 'updated')
                expect(cohort.user_facing_change_since_last_published?).to be(false) # sanity check

                expect {
                    Cohort.where(id: cohort.id).edit_and_republish do |_cohort|
                        expect(_cohort).to receive(:save!).and_call_original
                        _cohort.name = 'updated'
                    end
                }.not_to raise_error

            end

            it "should re-save unpublished cohort" do
                cohort = Cohort.where.not(id: Cohort.all_published.pluck('id')).first
                Cohort.where(id: cohort.id).edit_and_republish do |_cohort|
                    expect(_cohort).to receive(:save!).and_call_original
                    _cohort.name = 'updated'
                end
                cohort.reload
                expect(cohort.name).to eq('updated')
                expect(cohort.has_published_version?).to be(false)
            end

            it "should re-publish published cohort" do
                cohort = Cohort.all_published.first
                Cohort.where(id: cohort.id).edit_and_republish do |_cohort|
                    expect(_cohort).to receive(:publish!).with({:update_published_at=>true}).and_call_original
                    _cohort.name = 'updated'
                end

                reloaded = Cohort.find_by_id(cohort.id)
                expect(reloaded.published_version.name).to eq('updated')
            end

            it "should not raise if the cohort has been edited since being published but in dev mode" do

                cohort = Cohort.all_published.first
                cohort.update_attribute('name', 'updated')
                expect(cohort.user_facing_change_since_last_published?).to be(true) # sanity check

                original_stdout = $stdout
                $stdout = File.open(File::NULL, "w") # suppress the puts for this test

                expect(Rails.env).to receive(:development?).and_return(true)

                expect {
                    Cohort.where(id: cohort.id).edit_and_republish do |_cohort|
                        expect(_cohort).to receive(:save!).and_call_original
                        _cohort.name = 'updated again'
                    end
                }.not_to raise_error

                $stdout = original_stdout


            end

        end

        describe "promoted" do

            it "should return manually promoted cohorts" do
                promoted_cohort = CohortPromotion.first.cohort
                Cohort.create!(
                    program_type: promoted_cohort.program_type,
                    name: 'cohort',
                    title: 'cohort',
                    start_date: Time.now,
                    end_date: Time.now,
                    stripe_plans: promoted_cohort.supports_payments? ? [{id: 'default_plan', amount: 80000, frequency: 'monthly' }] : nil
                )
                expect(Cohort.where(program_type: promoted_cohort.program_type).promoted.pluck('id')).to eq([promoted_cohort['id']])
            end

            it "should return cohorts that use admission rounds" do
                mba_cohorts = Cohort.all_published.where(program_type: 'mba')
                promoted_cohort = Cohort.promoted_cohort(:mba)
                mba_cohorts.detect { |cohort| cohort['id'] != promoted_cohort['id'] }
                expect(mba_cohorts.size > 1).to be(true)

                expect(Cohort.where(program_type: 'mba').promoted.pluck('id')).to eq([promoted_cohort['id']])
            end

        end

        describe "next_exam_info" do
            it "should return info about the next exam" do
                cohort = Cohort.where("periods::text like '%exam%'").first
                exam_period = cohort.periods.detect { |p| p['style'] == 'exam' }
                required_course_entry = exam_period['stream_entries'].detect { |e| e['required'] }
                stream = Lesson::Stream.find_by_locale_pack_id(required_course_entry['locale_pack_id']).published_version
                expect(cohort.next_exam_info(1)).to eq({
                    locale_pack_id: stream.locale_pack_id,
                    start_time: cohort.start_time_for_period(exam_period)

                })

                expect(cohort.next_exam_info(99)).to be_nil
            end

            it "should return nil if there is no exam after the index passed in" do
                cohort = Cohort.where("periods::text like '%exam%'").first
                expect(cohort.next_exam_info(99)).to be_nil
            end

            it "should return nil if there is no exam at all" do
                cohort = Cohort.where("periods::text not like '%exam%'").first
                expect(cohort.next_exam_info(1)).to be_nil
            end
        end

    end

    describe "ensure_slack_room_assignment_job" do

        it "should schedule a job" do
            cohort = Cohort.joins(:slack_rooms).first
            old_record = Cohort::SlackRoomAssignment.create!(
                cohort_id: cohort.id,
                slack_room_assignments: {},
                location_assignments: {}
            )

            Delayed::Job.delete_all
            query = Delayed::Job.where(queue: Cohort::SlackRoomAssignmentJob.queue_name)
            cohort.ensure_slack_room_assignment_job
            expect(query.count).to be(1)
            expect(query.first.run_at).to be_within(1.second).of (1.minute.from_now)
            cohort.association(:slack_room_assignment).reload
            expect(Cohort::SlackRoomAssignment.find_by_id(old_record.id)).to be_nil
        end

        it "should skip if just called" do
            now = Time.at(120)
            expect(Time).to receive(:now).at_least(1).times do
                now
            end
            cohort = Cohort.first
            expect(cohort).to receive(:verify_cohort_jobs).with(anything, Cohort::SlackRoomAssignmentJob, {:skip_if_job_running=>true}).exactly(2).times

            cohort.ensure_slack_room_assignment_job
            cohort.ensure_slack_room_assignment_job
            now = now + 61.seconds
            cohort.ensure_slack_room_assignment_job
            cohort.ensure_slack_room_assignment_job
        end

        it "should skip if no rooms" do
            cohort = Cohort.left_outer_joins(:slack_rooms).where("cohort_slack_rooms.id is null").first
            old_record = Cohort::SlackRoomAssignment.create!(
                cohort_id: cohort.id,
                slack_room_assignments: {},
                location_assignments: {}
            )

            Delayed::Job.delete_all
            query = Delayed::Job.where(queue: Cohort::SlackRoomAssignmentJob.queue_name)
            cohort.slack_rooms.delete_all
            cohort.ensure_slack_room_assignment_job
            expect(query.count).to be(0)
            expect(Cohort::SlackRoomAssignment.find_by_id(old_record.id)).to be_nil
        end

        it "should skip if start date in the past, but leave existing assignment" do
            cohort = Cohort.joins(:slack_rooms).first
            cohort.start_date = Time.now - 1.week
            old_record = Cohort::SlackRoomAssignment.create!(
                cohort_id: cohort.id,
                slack_room_assignments: {},
                location_assignments: {}
            )

            Delayed::Job.delete_all
            query = Delayed::Job.where(queue: Cohort::SlackRoomAssignmentJob.queue_name)
            cohort.ensure_slack_room_assignment_job
            expect(query.count).to be(0)
            expect(Cohort::SlackRoomAssignment.find_by_id(old_record.id)).not_to be_nil
        end
    end

    describe "verification_periods" do
        attr_reader :cohort

        before(:each) do
            @cohort = Cohort.new(
                start_date: Time.now - 25.days,
                id_verification_periods: [
                    {start_date_days_offset: 0, due_date_days_offset: 10},
                    {start_date_days_offset: 11, due_date_days_offset: 20},
                    {start_date_days_offset: 21, due_date_days_offset: 30},
                    {start_date_days_offset: 31, due_date_days_offset: 40}
                ].map(&:as_json)
            )
        end

        describe "active_verification_period" do
            it "should return an ongoing period" do
                expect(cohort.active_verification_period['start_date_days_offset']).to eq(21)
            end
            it "should return a completed period" do
                cohort.start_date = Time.now - 20.days - 1.hour
                expect(cohort.active_verification_period['start_date_days_offset']).to eq(11)
            end
        end

        describe "verification_periods_before_active_one" do
            it "should work" do
                expect(cohort.verification_periods_before_active_one.map { |p| p['start_date_days_offset']}).to match_array([0, 11])
            end
        end
    end

    describe "acceptance date" do
        it "should be 5 days prior to the start date" do
            cohort = Cohort.new(start_date: Time.now)
            expect(cohort.acceptance_date).to eq(cohort.start_date.add_dst_aware_offset(-5.days))
        end
    end


    describe "ensure slack rooms for accepted users" do
        it "should automatically add a slack room to an accepted user" do

            cohort = Cohort.create!(title: 'title', start_date: Time.now, end_date: Time.now, name: 'my cohort', program_type: 'mba')
            cohort.publish!
            cohort_application = CohortApplication.create!(
                user_id: User.left_outer_joins(:cohort_applications).where("cohort_applications.id is null").first.id,
                cohort_id: cohort.id,
                status: 'accepted',
                applied_at: Time.now
            )

            id = SecureRandom.uuid
            hash = {
                slack_rooms: [
                    {
                        id: id,
                        title: 'new room',
                        url: 'new room url',
                        admin_token: 'new room token',
                        cohort_id: cohort.id
                    }
                ]
            }.as_json
            cohort.merge_hash(hash)

            expect(cohort_application.reload.cohort_slack_room_id).to eq(id)

        end
    end

    describe "slack_channels_that_receive_automated_messages" do

        it "should return an array containing the unique names of the Slack channels (without the leading #) that the cohort periods are configured to send messages to" do
            cohort = cohorts(:published_mba_cohort)
            mock_periods = [{
                'exercises' => [{'channel' => '#foo'}, {'channel' => '#bar'}]
            }, {
                'exercises' => [{'channel' => 'baz'}]
            }, {
                'exercises' => [{'channel' => '#foo'}] # another message gets sent to #foo
            }]
            expect(cohort).to receive(:periods).and_return(mock_periods)
            expect(cohort.slack_channels_that_receive_automated_messages).to eq(['foo', 'bar', 'baz']) # should not contain duplicates
        end
    end

    describe "decision_date" do

        attr_accessor :cohort, :future

        before(:each) do
            @cohort = cohorts(:published_mba_cohort)
            # some fixtures have dynamically calculated dates
            @future = Time.now + 50.years
        end

        it "should return a cached value" do
            expect(SafeCache).to receive(:fetch).with("decision_date/#{cohort.id}/#{cohort.published_at.to_timestamp}", expires_in: 1.week).and_return(future)
            expect(AdmissionRound).not_to receive(:where)
            expect(cohort.send(:decision_date)).to eq(future)
        end

        it "should set value in cache" do
            expect(SafeCache.read("decision_date/#{cohort.id}/#{cohort.published_at.to_timestamp}")).to be(nil)
            mock_relation = double('AdmissionRound')
            expect(mock_relation).to receive(:maximum).with("decision_date").and_return(future)
            expect(AdmissionRound).to receive(:where).and_return(mock_relation)
            expect(cohort.send(:decision_date)).to eq(future)
            expect(SafeCache.read("decision_date/#{cohort.id}/#{cohort.published_at.to_timestamp}")).to eq(future)
        end

    end

    describe "application_deadline" do

        attr_accessor :cohort, :future

        before(:each) do
            @cohort = cohorts(:published_mba_cohort)
            # some fixtures have dynamically calculated dates
            @future = Time.now + 50.years
        end

        it "should return a cached value" do
            expect(SafeCache).to receive(:fetch).with("application_deadline/#{cohort.id}/#{cohort.published_at.to_timestamp}", expires_in: 1.week).and_return(future)
            expect(AdmissionRound).not_to receive(:where)
            expect(cohort.send(:application_deadline)).to eq(future)
        end

        it "should set value in cache" do
            expect(SafeCache.read("application_deadline/#{cohort.id}/#{cohort.published_at.to_timestamp}")).to be(nil)
            mock_relation = double('AdmissionRound')
            expect(mock_relation).to receive(:maximum).with("application_deadline").and_return(future)
            expect(AdmissionRound).to receive(:where).and_return(mock_relation)
            expect(cohort.send(:application_deadline)).to eq(future)
            expect(SafeCache.read("application_deadline/#{cohort.id}/#{cohort.published_at.to_timestamp}")).to eq(future)
        end

    end

    describe "sister_cohort" do

        it "should work" do
            mba_cohort = cohorts(:published_mba_cohort)
            emba_cohort = mba_cohort.sister_cohort
            expect(emba_cohort).not_to be_nil
            expect(emba_cohort.class).to eq(Cohort::Version) # we should get the published version
            expect(emba_cohort.application_deadline).to eq(mba_cohort.application_deadline)
        end

        it "should never return isolated_network cohorts" do
            # NOTE: this is making an assumption that published_emba_cohort
            # would be the sister_cohort of published_mba_cohort. This should
            # be the case, given how we create these cohorts in fixtures.
            mba_cohort = cohorts(:published_mba_cohort)
            emba_cohort = cohorts(:published_emba_cohort)
            emba_cohort.update_column(:isolated_network, true)

            # This is a situation that should NEVER happen in the wild. Non-isolated
            # MBA/EMBA cohorts always have sister cohorts. This assertion is only to
            # test the logic that explicitly excludes isolated_network EMBA cohorts
            # from being considered as a sister_cohort to an MBA cohort.
            expect(mba_cohort.sister_cohort).to be_nil
        end

        it "should return nil if the current cohort is an isolated_network cohort" do
            emba_cohort = cohorts(:published_emba_cohort)
            emba_cohort.update_column(:isolated_network, true)
            # Isolated cohorts should never have `sister_cohort`s.
            expect(emba_cohort.sister_cohort).to be_nil
        end

        it "should raise if not EMBA or MBA" do
            cohort = cohorts(:published_career_network_only_cohort)
            expect {
                cohort.sister_cohort
            }.to raise_error("sister_cohort is only defined for mba and emba cohorts")
        end

    end


end
