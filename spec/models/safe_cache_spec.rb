require 'spec_helper'

describe SafeCache do

    before(:each) do
        Rails.cache.clear
        Rails.cache.write('foo', 'bar')
    end

    describe "fetch" do

        it "should return value if found" do
            expect(Rails.cache).to receive(:fetch).with('foo', nil).and_call_original
            result = SafeCache.fetch('foo')
            expect(result).to eq('bar')
        end

        it "should accept options" do
            expect(Rails.cache).to receive(:fetch).with('foo', {expires_in: 1.minute}).and_call_original
            SafeCache.fetch('foo', expires_in: 1.minute)
        end

        it "should not yield to a block if not a cache miss" do
            expect(Rails.cache).to receive(:fetch).with('foo', nil).and_call_original
            result = SafeCache.fetch('foo') do
                'block'
            end
            expect(result).to eq('bar')
        end

        it "should yield to a block if provided and a cache miss" do
            expect(Rails.cache).to receive(:fetch).with('foobar', nil).and_call_original
            result = SafeCache.fetch('foobar') do
                'block'
            end
            expect(result).to eq('block')
        end

        it "should return nil if fetch errors" do
            expect(Rails.cache).to receive(:fetch).at_least(1).times.and_raise(Errno::ENOENT.new(Rails.root.to_s))
            result = SafeCache.fetch('foobar')
            expect(result).to be_nil
        end

    end

    describe "delete" do

        it "should return true if deleted" do
            expect(Rails.cache).to receive(:delete).with('foo', nil).and_call_original
            result = SafeCache.delete('foo')
            expect(result).to eq(true)
        end

        it "should accept options" do
            expect(Rails.cache).to receive(:delete).with('foo', {option: 1}).and_call_original
            SafeCache.delete('foo', option: 1)
        end

        it "should return nil if delete errors" do
            expect(Rails.cache).to receive(:delete).at_least(1).times.and_raise(Errno::ENOENT.new(Rails.root.to_s))
            result = SafeCache.delete('foobar')
            expect(result).to eq(nil)
        end

    end

    describe "clear" do

        it "should return deleted values" do
            expect(Rails.cache).to receive(:clear).with(nil).and_call_original
            result = SafeCache.clear
            expect(result).not_to be_nil
            expect(result).not_to eq([])
        end

        it "should accept options" do
            expect(Rails.cache).to receive(:clear).with({option: 1}).and_call_original
            SafeCache.clear(option: 1)
        end

        it "should return [] if clear raises" do
            expect(Rails.cache).to receive(:clear).at_least(1).times.and_raise(Errno::ENOENT.new(Rails.root.to_s))
            SafeCache.clear
            expect(SafeCache.clear).to eq([])
        end

    end

    describe "rescue_from_errors" do

        it "should yield to a block" do
            result = 'foo'
            SafeCache.rescue_from_errors do
                result = 'bar'
            end
            expect(result).to eq('bar')
        end

        describe "when an exception is raised" do

            it "should obey return_value_on_fail" do
                result = SafeCache.rescue_from_errors('foobar') do
                    raise Errno::ENOENT
                end
                expect(result).to eq('foobar')
            end

            it "should call proc if provided" do
                result = SafeCache.rescue_from_errors(Proc.new { 'foobar' } ) do
                    raise Errno::ENOENT
                end
                expect(result).to eq('foobar')
            end

            it "should rescue Errno::ENOENT" do
                expect{
                    SafeCache.rescue_from_errors do
                        raise Errno::ENOENT
                    end
                }.not_to raise_error()
            end

            it "should rescue Errno::EACCES" do
                expect{
                    SafeCache.rescue_from_errors do
                        raise Errno::EACCES
                    end
                }.not_to raise_error()
            end


            it "should raise if the error is not ENOENT or EACCES" do
                expect{
                    SafeCache.rescue_from_errors do
                        raise Errno::ENOSPC
                    end
                }.to raise_error(Errno::ENOSPC)
            end

        end

    end

end