# == Schema Information
#
# Table name: events
#
#  id                     :uuid             not null, primary key
#  user_id                :uuid
#  event_type             :string(255)
#  payload                :json
#  created_at             :datetime
#  updated_at             :datetime
#  estimated_time         :datetime
#  client_reported_time   :datetime
#  hit_server_at          :datetime
#  total_buffered_seconds :float
#

require 'spec_helper'

describe Event do

    describe :create_batch_from_hash! do

        describe "with no previous events for page_load_id in database" do

            before(:each) do
                Event.delete_all
            end

            describe "page_load events" do

                it "should add an ip_address" do
                    bundle = [
                        event_params('event_type' => 'page_load:load'),
                        event_params
                    ]

                    ip_address = "1.2.3.4"
                    Event.create_batch_from_hash!(bundle, {
                        user_id: User.first.id,
                        hit_server_at: Time.now,
                        request_queued_for_seconds: 42,
                        client_ip_address: ip_address
                    })
                    events = Event.all
                    page_load_event = events.detect { |e| e['event_type'] == 'page_load:load' }
                    other_event = events.detect { |e| e != page_load_event }
                    expect(page_load_event.payload['ip_address']).to eq(ip_address)
                    expect(other_event.payload.key?('ip_address')).to be(false)
                end

                it "should add country code" do
                    bundle = [
                        event_params('event_type' => 'page_load:load'),
                        event_params
                    ]

                    country_code = "US"
                    Event.create_batch_from_hash!(bundle, {
                        user_id: User.first.id,
                        hit_server_at: Time.now,
                        request_queued_for_seconds: 42,
                        cloudflare_country_code: country_code
                    })
                    events = Event.all
                    page_load_event = events.detect { |e| e['event_type'] == 'page_load:load' }
                    other_event = events.detect { |e| e != page_load_event }
                    expect(page_load_event.payload['cf_country_code']).to eq(country_code)
                    expect(other_event.payload.key?('cf_country_code')).to be(false)
                end

            end

        end

        describe "with server_time in payload" do
            it "should set the estimated_time based on the server time in the payload" do
                id = SecureRandom.uuid
                bundle = [
                    event_params('id' => id, 'client_utc_timestamp' => 10, 'buffered_time' => 2, 'page_load_id' => 'with_server_time', 'server_timestamp' => 42)
                ]

                Event.create_batch_from_hash!(bundle, {user_id: User.first.id, hit_server_at: Time.now, request_queued_for_seconds: 0})
                expect(Event.find(id).estimated_time.to_timestamp).to be(42)
            end

            it "should order events so that created_at lines up with estimated_time" do

                ids = [SecureRandom.uuid, SecureRandom.uuid]
                bundle = [
                    event_params('id' => ids[0], 'client_utc_timestamp' => 10, 'buffered_time' => 2, 'page_load_id' => 'with_server_time', 'server_timestamp' => 42),
                    event_params('id' => ids[1], 'client_utc_timestamp' => 10, 'buffered_time' => 2, 'page_load_id' => 'with_server_time', 'server_timestamp' => 41)
                ]

                Event.create_batch_from_hash!(bundle, {user_id: User.first.id, hit_server_at: Time.now, request_queued_for_seconds: 0})
                expect(Event.find(ids[1]).created_at).to be < Event.find(ids[0]).created_at
            end
        end

        describe "with no server_time in payload" do
            it "should set the estimated_time based on the client timestamp if reasonable" do
                allow(Event).to receive(:fallback_on_server_timestamp_error_allowed?).and_return(true)
                id = SecureRandom.uuid

                # within 1 hour of now is allowed
                client_timestamp = (Time.now - 50.minutes).to_timestamp

                event = event_params('id' => id, 'client_utc_timestamp' => client_timestamp, 'server_timestamp' => nil, 'buffered_time' => 2, 'page_load_id' => 'with_no_server_time')
                bundle = [
                    event
                ]
                expect(Raven).to receive(:capture_exception).with("No server timestamp in event.  Falling back to client timestamp.",
                    {:extra=>{:event_type=>event['event_type']}})


                Event.create_batch_from_hash!(bundle, {user_id: User.first.id, hit_server_at: Time.now, request_queued_for_seconds: 0})
                expect(Event.find(id).estimated_time.to_timestamp).to eq(client_timestamp)
            end

            it "should set raise if client timestamp if unreasonable" do
                id = SecureRandom.uuid

                # mroe than 1 hour off of now is not allowed
                client_timestamp = Time.now.to_timestamp - 150.minutes
                event = event_params('id' => id, 'client_utc_timestamp' => client_timestamp, 'server_timestamp' => nil, 'buffered_time' => 2, 'page_load_id' => 'with_no_server_time')
                bundle = [
                    event
                ]
                expect(Raven).to receive(:capture_exception).with("No server timestamp in event.  Cannot fallback to client timestamp.",
                    {:extra=>{:event_type=>event['event_type']}})

                expect {
                    Event.create_batch_from_hash!(bundle, {user_id: User.first.id, hit_server_at: Time.now, request_queued_for_seconds: 0})
                }.to raise_error("No server timestamp in event.  Cannot fallback to client timestamp.")
            end
        end

        describe "with duplicate ids in database" do

            it "should skip existing events" do
                existing_event = Event.where('(payload->>\'page_load_id\')::uuid is not null').first
                Event.where("id != ?", existing_event.id).delete_all
                Event.create_batch_from_hash!([existing_event.payload.merge({'server_timestamp' => 12345})], {user_id: existing_event.user_id, hit_server_at: Time.now, request_queued_for_seconds: 0,  server_timestamp: 12345})
                expect(Event.count).to eq(1)
            end

        end

        describe "with unknown properties" do

            before(:each) do
                @bundle = [event_params({'client_utc_timestamp' => 10, 'buffered_time' => 2, 'page_load_id' => 'this_one', 'unknown property' => true})]
            end

            it "should raise if configured to do so" do
                expect(Event).to receive(:raise_on_unknown_properties?).and_return(true)

                expect {
                    Event.create_batch_from_hash!(@bundle, {user_id: User.first.id, hit_server_at: Time.now, request_queued_for_seconds: 0, server_timestamp: 12345 })
                }.to raise_error(Event::UnknownPropertyException)


            end

            it "should not raise if not configured to do so" do
                expect(Event).to receive(:raise_on_unknown_properties?).and_return(false)

                expect {
                    Event.create_batch_from_hash!(@bundle, {user_id: User.first.id, hit_server_at: Time.now, request_queued_for_seconds: 0, server_timestamp: 12345 })
                }.not_to raise_error
            end

        end

        describe "with 'cordova:device-registered' event_type" do

            it "should enqueue a ReconcileMobileDeviceJob" do
                user = User.first
                bundle = [
                    event_params(
                        'event_type' => 'cordova:device-registered',
                        'device_token' => 'some_fcm_registration_token',
                        'platform' => 'android'
                    ),
                    event_params
                ]

                expect(ReconcileMobileDeviceJob).to receive(:perform_later).with(
                    user.id,
                    'some_fcm_registration_token',
                    'android'
                ).exactly(1).times.and_return({})

                Event.create_batch_from_hash!(bundle, {
                    user_id: user.id,
                    hit_server_at: Time.now,
                    request_queued_for_seconds: 42,
                    server_timestamp: 12345
                })
            end

        end

        describe "with 'cordova:notification-opened' event_type" do

            it "should enqueue a TrackPushNotificationJob" do
                user = User.first

                cordova_event = event_params(
                    'event_type' => 'cordova:notification-opened',
                    'foo' => 'bar',
                    'baz' => 'qux'
                )

                bundle = [
                    cordova_event,
                    event_params
                ]

                expect(TrackPushNotificationJob).to receive(:perform_later).with(cordova_event)

                Event.create_batch_from_hash!(bundle, {
                    user_id: user.id,
                    hit_server_at: Time.now,
                    request_queued_for_seconds: 42,
                    server_timestamp: 12345
                })
            end

        end

        describe "with 'page_load:load' event_type" do

            it "should cache the event" do
                user = User.first

                page_load_id = SecureRandom.uuid
                event = event_params(
                    'event_type' => 'page_load:load',
                    'client' => 'web',
                    'os_name' => 'Windows',
                    'page_load_id' => page_load_id
                )

                bundle = [
                    event
                ]

                Event.create_batch_from_hash!(bundle, {
                    user_id: user.id,
                    hit_server_at: Time.now,
                    request_queued_for_seconds: 42,
                    server_timestamp: 12345
                })

                cached_event = SafeCache.read("PageLoadEvent/#{page_load_id}")
                expect(cached_event).to be_is_a(RedshiftEvent)
                %w(id page_load_id event_type os_name client).each do |key|
                    expect(cached_event[key]).to eq(event[key]), "Unexpected value for #{key.inspect}"
                end
            end

        end

        describe "PutEventsInFirehose" do

            it "should call Event::PutEventsInFirehose.put_and_queue_failures with new events and non persisted events from save_event_hashes" do
                bundle = [
                    event_params
                ]
                new_event = Event.new(id: SecureRandom.uuid)
                non_persisted_event = Event.new(id: SecureRandom.uuid)

                allow(Event).to receive(:save_event_hashes).and_return({
                    :new_events => [new_event],
                    :non_persisted_events => [non_persisted_event],
                    :ids_to_log_to_customer_io => []
                })
                expect(Event::PutEventsInFirehose).to receive(:put_and_queue_failures).with([new_event, non_persisted_event], :client_side_events)
                Event.create_batch_from_hash!(bundle, {
                    user_id: SecureRandom.uuid,
                    hit_server_at: Time.now,
                    request_queued_for_seconds: 42
                })
            end

            it "should work when no non_persisted_events" do
                bundle = [
                    event_params
                ]
                new_event = Event.new(id: SecureRandom.uuid)

                allow(Event).to receive(:save_event_hashes).and_return({
                    :new_events => [new_event],
                    :ids_to_log_to_customer_io => []
                })
                expect(Event::PutEventsInFirehose).to receive(:put_and_queue_failures).with([new_event], :client_side_events)
                Event.create_batch_from_hash!(bundle, {
                    user_id: SecureRandom.uuid,
                    hit_server_at: Time.now,
                    request_queued_for_seconds: 42
                })
            end

            it "should work when no new_events" do
                bundle = [
                    event_params
                ]
                non_persisted_event = Event.new(id: SecureRandom.uuid)

                allow(Event).to receive(:save_event_hashes).and_return({
                    :non_persisted_events => [non_persisted_event],
                    :ids_to_log_to_customer_io => []
                })
                expect(Event::PutEventsInFirehose).to receive(:put_and_queue_failures).with([non_persisted_event], :client_side_events)
                Event.create_batch_from_hash!(bundle, {
                    user_id: SecureRandom.uuid,
                    hit_server_at: Time.now,
                    request_queued_for_seconds: 42
                })
            end

        end

    end

    describe "log_to_external_systems" do

        before(:all) do
            @orig_env_logging_value = ENV['DISABLE_EVENT_LOGGING']
        end

        after(:all) do
            ENV['DISABLE_EVENT_LOGGING'] = @orig_env_logging_value
        end

        before(:each) do
            @bundle = [
                event_params('log_to_customerio' => true),
                event_params
            ]

            expect_any_instance_of(Event).to receive(:log_to_external_systems).exactly(1).times.and_call_original
        end

        describe "with DISABLE_EVENT_LOGGING" do

            it "should return without calling jobs when true" do
                ENV['DISABLE_EVENT_LOGGING'] = 'true'

                expect(LogToSegmentIoJob).not_to receive(:perform_later)
                expect(LogToCustomerIoJob).not_to receive(:perform_later)

                create_event
            end

        end

        describe "without DISABLE_EVENT_LOGGING" do

            it "should call LogToCustomerIoJob when false" do
                ENV['DISABLE_EVENT_LOGGING'] = 'false'

                expect(LogToSegmentIoJob).not_to receive(:perform_later)
                mock_configured_job = double('ActiveJob::ConfiguredJob instance')
                expect(LogToCustomerIoJob).to receive(:set).and_return(mock_configured_job)
                expect(mock_configured_job).to receive(:perform_later)

                create_event
            end

            it "should call LogToCustomerIoJob when nil" do
                ENV['DISABLE_EVENT_LOGGING'] = nil

                expect(LogToSegmentIoJob).not_to receive(:perform_later)
                mock_configured_job = double('ActiveJob::ConfiguredJob instance')
                expect(LogToCustomerIoJob).to receive(:set).and_return(mock_configured_job)
                expect(mock_configured_job).to receive(:perform_later)

                create_event
            end

            describe "with log_to_customerio_priority" do

                it "should set priority on enqueued LogToCustomerIoJob to log_to_customerio_priority arg" do
                    log_to_customerio_priority = LogToCustomerIoJob::MASS_LOG_PRIORITY
                    mock_configured_job = double('ActiveJob::ConfiguredJob instance')
                    expect(LogToCustomerIoJob).to receive(:set).with(priority: log_to_customerio_priority).and_return(mock_configured_job)
                    expect(mock_configured_job).to receive(:perform_later)
                    event = Event.first
                    event.log_to_external_systems(nil, true, log_to_customerio_priority)
                end
            end

            describe "without log_to_customerio_priority" do

                it "should set priority on enqueued LogToCustomerIoJob to #{LogToCustomerIoJob::DEFAULT_PRIORITY}" do
                    log_to_customerio_priority = LogToCustomerIoJob::DEFAULT_PRIORITY
                    mock_configured_job = double('ActiveJob::ConfiguredJob instance')
                    expect(LogToCustomerIoJob).to receive(:set).with(priority: log_to_customerio_priority).and_return(mock_configured_job)
                    expect(mock_configured_job).to receive(:perform_later)
                    event = Event.first
                    event.log_to_external_systems(nil, true, log_to_customerio_priority)
                end
            end

            describe "with log_to_segmentio_priority" do

                it "should set priority on enqueued LogToSegmentIoJob to log_to_segmentio_priority arg" do
                    log_to_segmentio_priority = LogToSegmentIoJob::MASS_LOG_PRIORITY
                    mock_configured_job = double('ActiveJob::ConfiguredJob instance')
                    expect(LogToSegmentIoJob).to receive(:set).with(priority: log_to_segmentio_priority).and_return(mock_configured_job)
                    expect(mock_configured_job).to receive(:perform_later)
                    event = Event.first
                    event.log_to_external_systems(nil, false, 0, log_to_segmentio_priority)
                end
            end

            describe "without log_to_segmentio_priority" do

                it "should set priority on enqueued LogToSegmentIoJob to #{LogToSegmentIoJob::DEFAULT_PRIORITY}" do
                    log_to_segmentio_priority = LogToSegmentIoJob::DEFAULT_PRIORITY
                    mock_configured_job = double('ActiveJob::ConfiguredJob instance')
                    expect(LogToSegmentIoJob).to receive(:set).with(priority: log_to_segmentio_priority).and_return(mock_configured_job)
                    expect(mock_configured_job).to receive(:perform_later)
                    event = Event.first
                    event.log_to_external_systems(nil, false, 0, log_to_segmentio_priority)
                end
            end
        end

        def create_event
            Event.create_batch_from_hash!(@bundle, {
                user_id: User.first.id,
                hit_server_at: Time.now,
                request_queued_for_seconds: 42,
                server_timestamp: 12345
            })
        end
    end

    describe "create_server_event" do
        it "should create a new Event record" do

            event_id =  SecureRandom.uuid
            user_id = User.first.id

            Event.create_server_event!(
                event_id,
                user_id,
                'server_event',
                {
                    label: 'server_event_label'
                }
            )

            e = Event.find(event_id)
            expect(e.user_id).to eq(user_id)
            expect(e.event_type).to eq('server_event')
            expect(e.payload['label']).to eq('server_event_label')

        end

        it "should create a new ArchivedEvent record if configured" do

            event_id =  SecureRandom.uuid
            user_id = User.first.id

            ENV['SAVE_TO_ARCHIVED_EVENTS'] = 'true'

            Event.create_server_event!(
                event_id,
                user_id,
                'server_event',
                {
                    label: 'server_event_label'
                }
            )

            ENV['SAVE_TO_ARCHIVED_EVENTS'] = 'false' # UNSET!

            e = Event::ArchivedEvent.find(event_id)
            expect(e.user_id).to eq(user_id)
            expect(e.event_type).to eq('server_event')
            expect(e.payload['label']).to eq('server_event_label')

        end
    end


    describe "save_event_hashes" do

        before(:each) do
            Event.delete_all
        end

        it "should work" do
            bundle = [
                event_params('event_type' => 'foo_bar')
            ]
            results = Event.save_event_hashes(bundle, {
                user_id: User.first.id,
                hit_server_at: Time.now,
                request_queued_for_seconds: 42
            })
            expect(Event.count).to eq(1)
            expect(results[:new_events][0]).to eq(Event.first)
        end

        it "should not insert CLIENT_SIDE_EVENTS_NOT_PERSISTED_TO_RDS when firehose is enabled" do
            expect(Event::PutEventsInFirehose).to receive(:is_enabled?).at_least(1).times.and_return(true)

            bundle = []
            Event::CLIENT_SIDE_EVENTS_NOT_PERSISTED_TO_RDS.each do |type|
                bundle << event_params('event_type' => type)
            end
            results = Event.save_event_hashes(bundle, {
                user_id: User.first.id,
                hit_server_at: Time.now,
                request_queued_for_seconds: 42
            })
            expect(Event.count).to eq(0)
            expect(results[:non_persisted_events].count).to eq(Event::CLIENT_SIDE_EVENTS_NOT_PERSISTED_TO_RDS.size)
        end

        it "should insert all events if firehose is not enabled" do
            expect(Event::PutEventsInFirehose).to receive(:is_enabled?).at_least(1).times.and_return(false)

            bundle = []
            Event::CLIENT_SIDE_EVENTS_NOT_PERSISTED_TO_RDS.each do |type|
                bundle << event_params('event_type' => type)
            end
            results = Event.save_event_hashes(bundle, {
                user_id: User.first.id,
                hit_server_at: Time.now,
                request_queued_for_seconds: 42
            })
            expect(Event.count).to eq(Event::CLIENT_SIDE_EVENTS_NOT_PERSISTED_TO_RDS.size)
            expect(results[:non_persisted_events]).to be(nil)
        end
    end


    def event_params(params = {})
        {
            "id" => SecureRandom.uuid,
            "event_type" => "event",
            "buffered_time" => 1.5,
            "client_utc_timestamp" => 12345.5,
            'server_timestamp' => 56789.0,
            "page_load_id" => "f1b94b0c-cb02-408a-b49c-029fa4936b1e"
        }.merge(params)
    end
end

