# == Schema Information
#
# Table name: lessons
#
#  id                 :uuid             not null, primary key
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  content_json       :json
#  author_id          :uuid
#  title              :string(255)
#  description        :text             default([]), is an Array
#  modified_at        :datetime
#  archived           :boolean          default(FALSE), not null
#  pinned             :boolean          default(FALSE), not null
#  pinned_title       :string(255)
#  pinned_description :text
#  was_published      :boolean          default(FALSE), not null
#  last_editor_id     :uuid
#  seo_title          :string
#  seo_description    :string
#  seo_canonical_url  :text
#  seo_image_id       :uuid
#  lesson_type        :string
#  frame_count        :integer
#  key_terms          :string           default([]), not null, is an Array
#  entity_metadata_id :uuid
#  assessment         :boolean          default(FALSE)
#  unrestricted       :boolean          default(FALSE)
#  tag                :text
#  locale_pack_id     :uuid
#  locale             :text             default("en"), not null
#  duplicated_from_id :uuid
#  duplicated_to_id   :uuid
#  test               :boolean          default(FALSE), not null
#

require 'spec_helper'

describe Lesson do
    fixtures(:lesson_streams, :users, :lesson_streams_progress, :lessons)

    before(:each) do
        @author = User.first
        @fake_content_klass = Class.new(Lesson::Content) do
            include EmbeddableDocument
            def self.name; 'FakeContentKlass'; end
            def self.inspect; 'FakeContentKlass'; end
        end
        allow(Lesson::Content).to receive(:get_klass_for_type) { @fake_content_klass }
    end

    describe "#create_from_hash!" do

        it "should save a lesson" do
            content = valid_attrs_hash
            lesson = Lesson.create_from_hash!(content)
            expect(lesson.content.lesson_type).to eq(valid_attrs_hash["lesson_type"])
        end

        it "should save a lesson and automatically give author editor permissions to lesson" do
            content = valid_attrs_hash
            lesson = Lesson.create_from_hash!(content)
            author = User.find(content['author']['id'])

            assert author.has_role? :editor, lesson
        end

        it "should save a lesson and dynamically create tags" do
            content = valid_attrs_hash
            lesson = Lesson.create_from_hash!(content.merge({
                "tag" => "test"
            })).reload

            expect(lesson.tag).to eq("test")
        end

        it "shouldnt create blank tags" do
            content = valid_attrs_hash
            lesson = Lesson.create_from_hash!(content.merge({
                "tag" => " "
            }))
            lesson = Lesson.create_from_hash!(content)
            expect(lesson.tag).to be_nil
        end

        it "should import an author but not stick it onto the content" do
            lesson = Lesson.create_from_hash!(valid_attrs_hash.merge('author' => @author.as_json(for: :public)).with_indifferent_access)
            expect(lesson.author).to eq(@author)
            expect(lesson.content.as_json['author']).to be(nil)
        end

        describe "is_practice_for_locale_pack_id" do
            it "should add a practice lesson if there is no practice lesson locale pack" do
                lesson = lessons(:without_practice_locale_pack)
                practice_lesson = Lesson.create_from_hash!(valid_attrs_hash, {
                    'is_practice_for_locale_pack_id' => lesson.locale_pack_id
                })
                expect(lesson.practice_lesson).to eq(practice_lesson)
            end

            it "should add a practice lesson if there is a practice lesson locale pack but no lesson for this locale" do
                lesson = lessons(:with_only_en_practice_lesson).locale_pack.content_items.where.not(locale: 'en').first
                expect(lesson).not_to be_nil
                expect(lesson.practice_lesson).to be_nil
                expect(lesson.practice_locale_pack).not_to be_nil
                practice_lesson = Lesson.create_from_hash!(
                    valid_attrs_hash.merge({
                        'locale' => lesson.locale
                    }), {
                    'is_practice_for_locale_pack_id' => lesson.locale_pack_id
                    })
                expect(lesson.practice_lesson).to eq(practice_lesson)
            end

            it "should raise if there is already a practice lesson" do
                lesson = lessons(:with_practice_lesson)
                expect(Proc.new{
                    Lesson.create_from_hash!(valid_attrs_hash, {
                        'is_practice_for_locale_pack_id' => lesson.locale_pack_id
                    })
                }).to raise_error(ActiveRecord::RecordNotUnique)
            end
        end

    end

    describe "update_from_hash" do
        before :each do
            support_property_a_in_content
            @lesson = Lesson.first
        end

        it "should support toggling assessment" do
            Lesson.update_from_hash!(@author, @lesson.as_json.merge('assessment' => true))
            expect(@lesson.reload.assessment).to be(true)
            Lesson.update_from_hash!(@author, @lesson.as_json.merge('assessment' => false))
            expect(@lesson.reload.assessment).to be(false)
        end

        it "should not override content keys that are not set" do
            @lesson.content = {'frames' => [{'id' => '1'}], 'lesson_type' => 'some_lesson_type'}
            @lesson.lesson_type = 'some_lesson_type'
            @lesson.save!
            Lesson.update_from_hash!(@author, @lesson.as_json.except('prop'))
            @lesson.reload
            expect(Lesson.find(@lesson.id).content['frames'][0]['id']).to eq('1')
        end

        it "should support archiving and unarchiving" do
            lesson = Lesson.where.not(id: Lesson.all_published.pluck(:id)).first
            expect(lesson.archived).to be_falsey

            # not passing in archived should not change anything
            Lesson.update_from_hash!(lesson.author, lesson.as_json)
            expect(lesson.reload.archived).to be_falsey

            # passing in true should change it to true
            Lesson.update_from_hash!(lesson.author, lesson.as_json.merge({
                'archived' => true
            }))
            expect(lesson.reload.archived).to be_truthy

            # not passing in archived should not change anything
            Lesson.update_from_hash!(lesson.author, lesson.as_json)
            expect(lesson.reload.archived).to be_truthy

            # passing in false should change it to false
            Lesson.update_from_hash!(lesson.author, lesson.as_json.merge({
                'archived' => false
            }))
            expect(lesson.reload.archived).to be_falsey
        end

        it "should not allow a user without publish permissions to archive a published lesson" do
            lesson = Lesson.first
            expect(lesson.archived).to be_falsey
            lesson.publish!
            expect(lesson.author.ability).to receive(:cannot?).with(:publish, anything).and_return(true)

            # passing in true should change it to true
            expect(Proc.new {
                Lesson.update_from_hash!(lesson.author, lesson.as_json.merge({
                    'archived' => true
                }))
            }).to raise_error(ContentPublisher::Publishable::UnauthorizedError)

            expect(lesson.reload.archived).to be_falsey
        end

        it "should allow for updating a specific version" do
            lesson = Lesson.first
            lesson.title = "another version"
            lesson.save!
            versions = Lesson::Version.where("id" => lesson.id).reorder('updated_at asc')
            expect(versions.size > 1).to be(true)

            result = Lesson.update_from_hash!(
                lesson.author,
                versions[0].as_json.merge({
                    'title' => 'updated old version',
                    'description' => ['updated description']
                }),
                {update_version: versions[0].version_id}
            )
            expect(result).to eq(versions[0])
            expect(result.changed?).to be(false)

            reloaded_versions = Lesson::Version.where("id" => lesson.id).reorder('updated_at asc')
            expect(reloaded_versions.size).to eq(versions.size)
            expect(lesson.reload.title).to eq("another version")
            expect(reloaded_versions[0].title).to eq('updated old version')
            expect(reloaded_versions[0].description).to eq(['updated description'])

        end

        describe "pinning" do
            it "should pin if indicated" do
                lesson = Lesson.first
                start = lesson.versions.minimum(:version_created_at)
                lesson = Lesson.update_from_hash!(lesson.author, lesson.as_json.merge('title' => 'pinned version'), {
                    'should_pin' => true,
                    'pinned_title' => 'title',
                    'pinned_description' => 'description'
                })

                Lesson.update_from_hash!(lesson.author, lesson.as_json.merge('title' => 'unpinned_version'), {
                    'should_pin' => false
                })

                pinned_versions = lesson.versions.where('pinned' => true).where("version_created_at > ?", start)
                expect(pinned_versions.count).to eq(1)
                version = pinned_versions.first
                expect(version.pinned).to be_truthy
                expect(version.pinned_title).to eq('title')
                expect(version.pinned_description).to eq('description')
                expect(version.title).to eq('pinned version')
            end

            it "should not pin if not indicated" do
                lesson = Lesson.first
                Lesson.update_from_hash!(lesson.author, lesson.as_json, {
                    'should_pin' => false
                })

                version = lesson.versions.last
                expect(version.pinned).to be_falsey
                expect(version.pinned_title).to be_nil
                expect(version.pinned_description).to be_nil
            end

            it "should unpin if not indicated" do
                lesson = Lesson.where(pinned: true).first
                Lesson.update_from_hash!(lesson.author, lesson.as_json, {
                    'should_pin' => false
                })
                expect(lesson.reload.pinned).to be_falsey
            end

            it "should set the default title and description while pinning" do
                lesson = Lesson.first
                Lesson.update_from_hash!(lesson.author, lesson.as_json, {
                    'should_pin' => true
                })

                lesson = lesson.reload
                expect(lesson.pinned).to be_truthy
                expect(lesson.pinned_title.slice(0, 8)).to eq("Pin from")
                expect(lesson.pinned_description).to be_nil
            end

            it "should pin if publishing" do
                lesson = Lesson.first
                expect(lesson.author.ability).to receive(:cannot?).with(:publish, anything).and_return(false)
                Lesson.update_from_hash!(lesson.author, lesson.as_json, {
                    'should_publish' => true
                })

                expect(lesson.reload.pinned).to be_truthy
                expect(lesson.reload.was_published).to be_truthy
            end
        end

    end

    describe "merge_hash" do

        it "should set frame_count" do
            lesson = Lesson.new
            lesson.merge_hash({
                'frame_count' => 42
            })
            expect(lesson.frame_count).to eq(42)
        end

        it "should set lesson_type" do
            lesson = Lesson.new
            lesson.merge_hash({
                'lesson_type' => 'some_type'
            })
            expect(lesson.lesson_type).to eq('some_type')
        end

        it "should set key_terms" do
            lesson = Lesson.new
            lesson.merge_hash({
                'key_terms' => ['some', 'key', 'terms']
            })
            expect(lesson.key_terms).to eq(['some', 'key', 'terms'])
        end

        it "should set test flag" do
            lesson = Lesson.new
            lesson.merge_hash({
                'test' => true
            })
            expect(lesson.test).to be(true)
        end

        describe "with frame_list lesson" do
            it "should remove usage reports" do
                lesson = lessons(:frame_list_lesson)
                lesson.merge_hash({
                    'frames' => [
                        {
                            'frame_type' => 'componentized',
                            'components' => [{
                                'component_type' => 'Componentized.Text',
                                'usage_report' => 'USAGE_REPORT'
                            }]
                        }
                    ]
                })
                expect(lesson.content.frames[0].components[0].as_json.key?('usage_report')).to be(false)
            end
        end

    end

    describe "all_published_and_in_a_stream" do

        before(:each) do
            # prevent validation errors on stream save
            allow_any_instance_of(Lesson::Stream).to receive(:raise_unless_all_playlists_including_stream_are_valid)
            Lesson::Stream.all.each(&:unpublish!)
            Lesson.all.each(&:unpublish!)
            @stream = Lesson::Stream.first
            @lesson = @stream.lessons.first
            @stream.publish!
            @lesson.publish!
            expect(Lesson.all_published_and_in_a_stream).to eq([@lesson])
        end

        it "should filter out unpublished" do
            @lesson.unpublish!
            expect(Lesson.all_published_and_in_a_stream).to eq([])
        end

        it "should filter out not in a stream" do
            @stream.lessons = []
            expect(Lesson.all_published_and_in_a_stream).to eq([])
        end

        it "should filter out not in a published stream" do
            @stream.unpublish!
            expect(Lesson.all_published_and_in_a_stream).to eq([])
        end
    end

    describe "content" do

        it "should allow access to decoded content_json" do
            content = {
                'lesson_type' => 'lesson_type',
                'frames' => [{'id' => '1'}]
            }
            lesson = Lesson.create_from_hash!(valid_attrs_hash)
            # we should reset the content to the one with the {id: 1} in it
            lesson.content = content
            lesson.save!
            # when we reload the content, it should be the one we expect
            expect(content['frames'][0]['id']).to eq('1')
        end

        it "should not exist if there is no content_json" do
            lesson = Lesson.new
            expect(lesson.content).to be(nil)
            lesson.content_json = {}
            expect(lesson.content).not_to be(nil)
        end

        it "should update when content_json is updated" do
            support_property_a_in_content
            lesson = Lesson.create_from_hash!(valid_attrs_hash.merge({'a' => 1}))
            lesson.content_json = {'a' => 2}
            expect(lesson.content['a']).to eq(2)
        end

        it "should be validated when there are no errors" do
            lesson = Lesson.create_from_hash!(valid_attrs_hash)
            lesson.valid?
        end

        it "should be validated when there are errors" do
            @content_klass_with_validation = Class.new(Lesson::Content) do
                def self.name; 'ContentKlassWithValidation'; end
                def self.inspect; 'ContentKlassWithValidation'; end

                key :required_key, :required => true

            end
            allow(Lesson::Content).to receive(:get_klass_for_type) { @content_klass_with_validation }

            lesson = Lesson.new
            lesson.content = {'lesson_type' => 'content_klass_with_validation'}
            expect(lesson.valid?).to eq(false)
            expect(lesson.errors.full_messages).to include("Required key can't be blank")
        end

    end

    describe "get_image_asset" do

        it "should create an instance of S3Asset" do
            lesson = Lesson.create_from_hash!(valid_attrs_hash)
            stub_asset = {}
            allow(stub_asset).to receive(:add_dimensions)
            allow(S3Asset).to receive(:new).with(any_args()) do |attrs|
                expect(attrs[:file]).to eq("some_file.png")
                expect(attrs[:directory]).to eq("images/")
                expect(attrs[:styles]).to eq({
                    "1400x550" => '1400x550>',
                    "700x275" => '700x275>',
                    "1020x400" => '1020x400>',
                    "510x200" => '510x200>',
                    "168x66" => '168x66>',
                    "84x33" => '84x33>',
                    "816x320" => '816x320>',
                    "408x160" => '408x160>',
                    "586x230" => '586x230>',
                    "293x115" => '293x115>',
                    "1146x450" => '1146x450>',
                    "573x225" => '573x225>',
                    "688x270" => '688x270>',
                    "344x135" => '344x135>'
                }.to_json)
            end
            expect(S3Asset).to receive(:new).and_return(stub_asset)
            image_asset = lesson.get_image_asset("some_file.png")
        end

    end

    describe "remove_from_all_streams" do

        it "should remove from streams" do
            @stream = Lesson::Stream.first
            lesson = @stream.lessons[0]
            lesson.remove_from_all_streams
            @stream.reload
            expect(@stream.lessons).not_to include(lesson)
        end

    end

    describe "do_work_if_archiving" do

        it "should unpublish" do
            lesson = Lesson.first
            lesson.publish!
            lesson.update!(:archived => true)
            expect(Lesson.find(lesson.id).has_published_version?).to be_falsey
        end

        it "should remove from streams" do
            lesson = Lesson.first
            expect(lesson).to receive(:remove_from_all_streams)
            lesson.update!(:archived => true)
            lesson.save!
        end

    end

    it "should delete stream joins when deleted" do
        stream = Lesson::Stream.first
        lesson = stream.lessons[0]
        count_of_streams_with_lesson = Lesson::Stream.joins(:lessons).where("lessons.id = ?", lesson.id).count
        join_count = Lesson::ToStreamJoin.count
        lesson.destroy
        expect(Lesson::ToStreamJoin.count).to eq(join_count - count_of_streams_with_lesson)
    end

    describe "get_version" do

        it "should return nil if version not found" do
            lesson = Lesson.first
            expect(lesson.get_version('version_id')).to be_nil
        end

        it "should return old version" do
            lesson = Lesson.first
            start = lesson.versions.minimum(:version_created_at)
            lesson.title = 'new version'
            lesson.save!
            lesson.title = 'another new version'
            lesson.save!

            versions = lesson.versions.where(operation: 'U').where("version_created_at > ?", start)
            expect(versions.size).to eq(2)
            versions.each do |version|
                result = lesson.get_version(version.version_id)
                expect(result.version_id).to eq(version.version_id)
                expect(result.attributes['id']).to eq(lesson.id)
            end
        end
    end

    describe "prepare_params_for_duplication" do

        it "should remove some things" do
            params = Lesson.prepare_params_for_duplication({'a' => 1, 'archived' => 2}, :ignore, :ignore, :ignore)
            expect(params).to eq({'a' => 1})
        end

    end

    describe "is_practice?" do

        it "should work" do
            expect(lessons(:with_practice_lesson).is_practice?).to be(false)
            expect(lessons(:with_practice_lesson).practice_lesson.is_practice?).to be(true)
        end

    end

    def valid_attrs_hash
        {
            'lesson_type' => 'lesson_type',
            'author' => {'id' => @author.id},
            'title' => 'stuff and things'
        }
    end

    def invalid_attrs_hash
        valid_attrs_hash.merge('lesson_type' => nil, 'updated_at' => Time.now.to_timestamp)
    end

    def support_property_a_in_content
        content_klass_with_a = Class.new(Lesson::Content) do
            key :a
        end
        allow(Lesson::Content).to receive(:get_klass_for_type) { content_klass_with_a }
    end

end
