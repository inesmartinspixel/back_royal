require 'spec_helper'

describe RetriableTransaction do

    fixtures(:users)

    before(:each) do
        @times_called = 0
        @errToRetry = ActiveRecord::PreparedStatementCacheExpired.new("")
        @errsToRaise = [
            ActiveRecord::RecordNotUnique.new(""),
            ActiveRecord::RecordNotFound.new(""),
            ActiveRecord::RecordNotSaved.new(""),
            ActiveRecord::DangerousAttributeError.new(""),
            ActiveRecord::StatementInvalid.new("")
        ]
    end

    describe "RetriableTransaction.transaction" do
        it "should retry a failed RetriableTransaction.transaction on ActiveRecord::PreparedStatementCacheExpired" do
            expect(Proc.new {
                RetriableTransaction.transaction do
                    @times_called += 1
                    raise @errToRetry if @times_called == 1
                end
            }).not_to raise_error
            expect(@times_called).to eq(2)
        end

        it "should recover even when nested" do
            result = RetriableTransaction.transaction do
                RetriableTransaction.transaction do
                    @times_called = @times_called + 1
                    if @times_called == 1
                        begin
                            ActiveRecord::Base.connection.execute("RAISE EXCEPTION 'kill transaction'")
                        rescue
                            raise @errToRetry
                        end
                    else
                        a = ActiveRecord::Base.connection.execute('select 1 as one').to_a[0]['one']
                    end
                end
            end
            expect(result).to eq(1)
        end

        it "should raise if retry of ActiveRecord::PreparedStatementCacheExpired error still fails" do
            expect(Proc.new {
                RetriableTransaction.transaction do
                    @times_called += 1
                    raise @errToRetry
                end
            }).to raise_error(@errToRetry)
            expect(@times_called).to eq(2)
        end

        it "should not retry a failed RetriableTransaction.transaction on other error" do
            @errsToRaise.each do |err|
                @times_called = 0;
                expect(ActiveRecord::Base).to receive('transaction').exactly(1).times do |params|
                    @times_called += 1
                    raise err if @times_called == 1
                end
                expect(Proc.new {
                    RetriableTransaction.transaction()
                }).to raise_error(err)
                expect(@times_called).to eq(1)
            end
        end

    end

    describe "ActiveRecord::Base.transaction" do
        it "should not retry a failed ActiveRecord::Base.transaction on ActiveRecord::PreparedStatementCacheExpired" do
            expect(ActiveRecord::Base).to receive('transaction').exactly(1).times do |params|
                @times_called += 1
                raise @errToRetry if @times_called == 1
            end
            expect(Proc.new {
                ActiveRecord::Base.transaction()
            }).to raise_error(@errToRetry)
            expect(@times_called).to eq(1)
        end

        it "should not retry a failed ActiveRecord::Base.transaction on other errors" do
            @errsToRaise.each do |err|
                @times_called = 0;
                expect(ActiveRecord::Base).to receive('transaction').exactly(1).times do |params|
                    @times_called += 1
                    raise err if @times_called == 1
                end
                expect(Proc.new {
                    ActiveRecord::Base.transaction()
                }).to raise_error(err)
                expect(@times_called).to eq(1)
            end
        end

    end

end
