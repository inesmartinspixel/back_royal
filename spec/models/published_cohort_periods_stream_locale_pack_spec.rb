# == Schema Information
#
# Table name: published_cohort_periods_stream_locale_packs
#
#  created_at     :datetime
#  cohort_id      :uuid
#  index          :integer
#  locale_pack_id :uuid
#  title          :text
#  required       :boolean
#  optional       :boolean
#
require 'spec_helper'
require 'derived_content_table_spec_helper'

describe PublishedCohortPeriodsStreamLocalePack do
    include DerivedContentTableSpecHelper
    fixtures(:cohorts, :users)

    attr_accessor :cohort, :stream

    before(:each) do
        @cohort = cohorts(:published_mba_cohort)
        @stream = Lesson::Stream.find_by_locale_pack_id(@cohort.periods[0]['stream_entries'][0]['locale_pack_id'])
    end

    describe "write_new_records" do
        it "should write expected data" do
            expected_records = []
            cohort.periods.each_with_index do |period, i|
                period['stream_entries'].each do |entry|
                    expected_records << {
                        locale_pack_id: entry['locale_pack_id'],
                        required: entry['required'],
                        optional: !entry['required'],
                        title: Lesson::Stream.where(locale_pack_id: entry['locale_pack_id'], locale: 'en').first.title,
                        cohort_id: cohort.id,
                        index: i+1
                    }.stringify_keys
                end
            end

            PublishedCohortPeriodsStreamLocalePack.delete_all
            PublishedCohortPeriodsStreamLocalePack.send(:write_new_records, :cohort_id, [cohort.id])

            records = PublishedCohortPeriodsStreamLocalePack.where(cohort_id: cohort.id)

            # sanity checks
            expect(records.count(&:optional)).to be > 0
            expect(records.count(&:required)).to be > 0
            expect(records.map(&:attributes).each { |a| a.delete('created_at')}).to match_array(expected_records)
        end
    end

    describe "update_on_content_change" do

        it "should replace rows for a cohort" do
            assert_replace_rows_for_content_item(PublishedCohortPeriodsStreamLocalePack, cohort, :id, :cohort_id) do
                cohort.periods[0]['trigger_a_change'] = true
            end
        end

        it "should replace rows for a stream" do
            assert_replace_rows_for_content_item(PublishedCohortPeriodsStreamLocalePack, stream, :locale_pack_id, :locale_pack_id) do
                stream.title = 'testing title update'
            end
        end

    end
end
