require 'spec_helper'


# A lot of the specs in here are a bit higher-level, testing the integration
# between IsDerivedContentTable, Publishable, and AccessGroup. So it's
# possible that changes to one of those other modules could break these
# without anything actually changing in IsDerivedContentTable
describe IsDerivedContentTable do
    attr_reader :derived_content_table_classes

    before(:each) do
        @derived_content_table_classes = []
    end

    describe "triggering changes to derived tables" do

        describe "update_when_attr_change_is_published" do

            it "should trigger update_on_content_change with the change when watched attribute change is published" do
                klass = get_derived_content_table_klass
                klass.update_when_attr_change_is_published(Playlist, :title)
                playlist = Playlist.all_published.first
                orig_published_version = playlist.published_version

                expect(klass).to receive(:wrapped_update_on_content_change) do |klass:, identifiers:, version_pairs:|
                    expect(klass).to be(Playlist)
                    expect(identifiers).to eq([playlist.id])
                    expect(version_pairs[0][0]).to eq(orig_published_version)
                    expect(version_pairs[0][1].title).to eq('changed')
                end

                playlist.title = 'changed'
                playlist.publish!
            end

            it "should not triggerupdate_on_content_change when an item is published with no relevant changes" do
                klass = get_derived_content_table_klass
                klass.update_when_attr_change_is_published(Playlist, :title)
                playlist = Playlist.all_published.first

                expect(klass).not_to receive(:wrapped_update_on_content_change)

                playlist.description = 'changed'
                playlist.publish!
            end

            it "should pass along the attribute indicated in the identifier option to update_on_content_change" do
                klass = get_derived_content_table_klass
                klass.update_when_attr_change_is_published(Playlist, :title, identifier: :locale_pack_id)
                playlist = Playlist.all_published.first

                expect(klass).to receive(:wrapped_update_on_content_change) do |klass:, identifiers:, version_pairs:|
                    expect(klass).to be(Playlist)
                    expect(identifiers).to eq([playlist.locale_pack_id])
                end

                playlist.title = 'changed'
                playlist.publish!

            end

            describe "with comparison=sort_agnostic_collection" do

                it "should not call update_on_content_change if the collection has not changed according to a sort_agnostic comparison" do
                    klass = get_derived_content_table_klass
                    klass.update_when_attr_change_is_published(Cohort, :specialization_playlist_pack_ids, comparison: :sort_agnostic_collection)
                    cohort = Cohort.all_published.first

                    list = [SecureRandom.uuid, SecureRandom.uuid ]

                    cohort.specialization_playlist_pack_ids = list
                    cohort.publish!

                    expect(klass).not_to receive(:wrapped_update_on_content_change)

                    cohort.specialization_playlist_pack_ids = list.reverse
                    cohort.publish!

                end

                it "should call update_on_content_change if the collection has changed according to a sort_agnostic comparison" do
                    klass = get_derived_content_table_klass
                    klass.update_when_attr_change_is_published(Cohort, :specialization_playlist_pack_ids, comparison: :sort_agnostic_collection)
                    cohort = Cohort.all_published.first

                    list = [SecureRandom.uuid, SecureRandom.uuid ]

                    cohort.specialization_playlist_pack_ids = list
                    cohort.publish!

                    expect(klass).to receive(:wrapped_update_on_content_change)

                    cohort.specialization_playlist_pack_ids += [SecureRandom.uuid]
                    cohort.publish!
                end
            end


            describe "with comparison=hashdiff" do

                it "should not call update_on_content_change if the collection has not changed" do
                    klass = get_derived_content_table_klass
                    klass.update_when_attr_change_is_published(Cohort, :playlist_collections, comparison: :hashdiff)
                    cohort = Cohort.all_published.first

                    hash = [{'required_playlist_pack_ids' => [SecureRandom.uuid]}]

                    cohort.playlist_collections = hash
                    cohort.publish!

                    expect(klass).not_to receive(:wrapped_update_on_content_change)

                    cohort.playlist_collections = hash.clone
                    cohort.publish!

                end

                it "should call update_on_content_change if the collection has changed" do
                    klass = get_derived_content_table_klass
                    klass.update_when_attr_change_is_published(Cohort, :playlist_collections, comparison: :hashdiff)
                    cohort = Cohort.all_published.first

                    hash = [{'required_playlist_pack_ids' => [SecureRandom.uuid]}]

                    cohort.playlist_collections = hash
                    cohort.publish!

                    expect(klass).to receive(:wrapped_update_on_content_change)

                    hash = hash.clone
                    hash[0]['required_playlist_pack_ids'] << SecureRandom.uuid
                    cohort.playlist_collections = hash
                    cohort.publish!
                end
            end
        end
    end

    describe "update_when_access_groups_change" do
        attr_reader :klass

        before(:each) do
            @klass = get_derived_content_table_klass
            klass.update_when_access_groups_change(Lesson::Stream)
        end

        it "should trigger update_on_content_change when after_access_group_update is called on a content item" do
            stream = Lesson::Stream.all_published.joins(:locale_pack => :access_groups).first
            expect_trigger_with_stream(stream)
            orig_group_ids = stream.access_groups.pluck(:id)
            stream.locale_pack.access_groups << AccessGroup.where.not(id: orig_group_ids).first
            stream.after_access_group_update(orig_group_ids)
        end

        it "should trigger update_on_content_change when an access group is updated, removing a stream from the group" do
            access_group = AccessGroup.create!(name: 'group for spec')
            stream = Lesson::Stream.all_published.first
            access_group.update_from_hash!(access_group.as_json.merge('stream_locale_pack_ids' => [stream.locale_pack_id]))
            expect_trigger_with_stream(stream)
            access_group.update_from_hash!(access_group.as_json.merge('stream_locale_pack_ids' => []))
        end

        it "should trigger update_on_content_change when an access group is updated, adding a stream to the group" do
            access_group = AccessGroup.create!(name: 'group for spec')
            stream = Lesson::Stream.all_published.first
            expect_trigger_with_stream(stream)
            access_group.update_from_hash!(access_group.as_json.merge('stream_locale_pack_ids' => [stream.locale_pack_id]))

        end

        it "should trigger update_on_content_change when an access group with stream_locale_pack_ids is destroyed" do
            access_group = AccessGroup.create!(name: 'group for spec')
            stream = Lesson::Stream.all_published.first
            access_group.update_from_hash!(access_group.as_json.merge('stream_locale_pack_ids' => [stream.locale_pack_id]))
            expect_trigger_with_stream(stream)
            access_group.destroy
        end

        it "should pass along the attribute indicated in the identifier option to update_on_content_change" do
            @klass = get_derived_content_table_klass
            klass.update_when_access_groups_change(Lesson::Stream, identifier: :locale_pack_id)
            stream = Lesson::Stream.all_published.joins(:locale_pack => :access_groups).first
            expect_trigger_with_stream(stream, :locale_pack_id)
            orig_group_ids = stream.access_groups.pluck(:id)
            stream.locale_pack.access_groups << AccessGroup.where.not(id: orig_group_ids).first
            stream.after_access_group_update(orig_group_ids)
        end

        def expect_trigger_with_stream(stream, identifier=:id)
            expect(klass).to receive(:wrapped_update_on_content_change) do |klass:, identifiers:, version_pairs:|
                expect(klass).to be(Lesson::Stream)
                expect(identifiers).to eq([stream.send(identifier)])
                expect(version_pairs[0]).to eq([stream.published_version, stream.published_version])
            end
        end

    end

    it "should work when update_on_content_change only expects identifiers" do
        klass = get_derived_content_table_klass
        klass.define_singleton_method(:update_on_content_change) do |klass:, identifiers:|
            wrapped_update_on_content_change(klass: klass, identifiers: identifiers)
        end
        klass.update_when_attr_change_is_published(Playlist, :title)
        playlist = Playlist.all_published.first
        orig_published_version = playlist.published_version

        expect(klass).to receive(:wrapped_update_on_content_change) do |klass:, identifiers:|
            expect(klass).to be(Playlist)
            expect(identifiers).to eq([playlist.id])
        end

        playlist.title = 'changed'
        playlist.publish!
    end

    it "should work when update_on_content_change only expects version_pairs" do
        klass = get_derived_content_table_klass
        klass.define_singleton_method(:update_on_content_change) do |klass:, version_pairs:|
            wrapped_update_on_content_change(klass: klass, version_pairs: version_pairs)
        end
        klass.update_when_attr_change_is_published(Playlist, :title)
        playlist = Playlist.all_published.first
        orig_published_version = playlist.published_version

        expect(klass).to receive(:wrapped_update_on_content_change) do |klass:, version_pairs:|
            expect(klass).to be(Playlist)
            expect(version_pairs[0][0]).to eq(orig_published_version)
            expect(version_pairs[0][1].title).to eq('changed')
        end

        playlist.title = 'changed'
        playlist.publish!
    end

    describe "update_after" do

        it "should control the order in which klasses are updated" do
            updated_klasses = []
            klasses = (1..4).map do
                klass = get_derived_content_table_klass
                klass.update_when_attr_change_is_published(Playlist, :title)
                expect(klass).to receive(:wrapped_update_on_content_change) do |*args|
                    updated_klasses << klass
                end
                klass
            end

            klasses[0].update_after(klasses[2])
            klasses[1].update_after(klasses[0])

            playlist = Playlist.all_published.first
            playlist.title = 'changed'
            playlist.publish!

            # assert on names to make it easier to debug failures
            expect(updated_klasses.index(klasses[0])).to be > updated_klasses.index(klasses[2])
            expect(updated_klasses.index(klasses[1])).to be > updated_klasses.index(klasses[0])

        end

        it "should raise if a circular dependency is found in update_after" do
            klasses = (1..3).map do
                klass = get_derived_content_table_klass
                klass.update_when_attr_change_is_published(Playlist, :title)
                klass
            end

            klasses[0].update_after(klasses[1])
            klasses[1].update_after(klasses[2])
            klasses[2].update_after(klasses[0])

            playlist = Playlist.all_published.first
            playlist.title = 'changed'

            expect {
                playlist.publish!
            }.to raise_error(TSort::Cyclic)
        end

        it "should not blow up if there is a dependency on a table that is not being updated" do
            updated_klasses = []
            klasses = (1..2).map do
                klass = get_derived_content_table_klass
                allow(klass).to receive(:wrapped_update_on_content_change) do |*args|
                    updated_klasses << klass
                end
                klass
            end

            klasses[0].update_when_attr_change_is_published(Playlist, :title)
            klasses[0].update_after(klasses[1])

            playlist = Playlist.all_published.first
            playlist.title = 'changed'
            playlist.publish!

            expect(updated_klasses).to eq([klasses[0]])
        end
    end

    def get_derived_content_table_klass
        klass = Class.new() do
            cattr_reader :skip_class_name_check
            include IsDerivedContentTable

            #Ssince Publishable#make_derived_content_table_updates introspects on the
            # arguments for update_on_content_change, we cannot mock it out directly. We
            # have to mock out wrapped_update_on_content_change
            def self.update_on_content_change(klass:, identifiers:, version_pairs:)
                wrapped_update_on_content_change(klass:klass, identifiers:identifiers, version_pairs:version_pairs)
            end

            def self.wrapped_update_on_content_change(*args); end
        end

        name = "DerivedContentTable#{@derived_content_table_classes.size}"
        derived_content_table_classes << klass
        klass.define_singleton_method(:name)  { name }
        allow(klass.name).to receive(:constantize).and_return(klass)
        allow(IsDerivedContentTable).to receive(:derived_content_table_class_names).and_return(derived_content_table_classes.map(&:name))
        klass
    end
end