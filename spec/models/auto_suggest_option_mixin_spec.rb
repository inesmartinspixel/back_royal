require 'spec_helper'

describe AutoSuggestOptionMixin do
    attr_reader :instance

    class SuggestOption
        include AutoSuggestOptionMixin
    end

    before(:each) do
        @instance = SuggestOption.new
    end

    describe "get_klass_from_type" do

        it "should support multiple models" do
            supported_types = {
                'skills' => SkillsOption,
                'student_network_interests' => StudentNetworkInterestsOption,
                'awards_and_interests' => CareerProfile::AwardsAndInterestsOption,
                'professional_organization' => ProfessionalOrganizationOption,
                'educational_organization' => CareerProfile::EducationalOrganizationOption
            }

            supported_types.each do |type, klass|
                expect(instance.get_klass_from_type(type)).to eq(klass)
            end
        end

        it "should return nil" do
            expect(instance.get_klass_from_type('foobar')).to be(nil)
        end

    end

end