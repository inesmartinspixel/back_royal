# == Schema Information
#
# Table name: open_positions
#
#  id                                :uuid             not null, primary key
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  hiring_manager_id                 :uuid             not null
#  title                             :text             not null
#  salary                            :float
#  available_interview_times         :text
#  last_invitation_at                :datetime
#  place_id                          :text
#  place_details                     :json             not null
#  role                              :text
#  job_post_url                      :text
#  description                       :text
#  perks                             :text             default([]), is an Array
#  desired_years_experience          :json             not null
#  featured                          :boolean          default(FALSE)
#  archived                          :boolean          default(FALSE), not null
#  position_descriptors              :text             default([]), not null, is an Array
#  drafted_from_positions            :boolean          default(FALSE)
#  posted_at                         :datetime
#  last_curated_at                   :datetime
#  curation_email_last_triggered_at  :datetime
#  last_updated_at_by_hiring_manager :datetime
#  auto_expiration_date              :date
#  manually_archived                 :boolean          default(FALSE), not null
#  location                          :geography(Point,
#  short_description                 :text
#  subscription_id                   :uuid
#  will_sponsor_visa                 :boolean
#

require 'spec_helper'
require 'stripe_helper'

describe OpenPosition do

    include StripeHelper

    attr_reader :hiring_manager

    fixtures(:users)

    before(:each) do
        @hiring_manager = HiringApplication.first.user
    end

    describe "lifecycle hooks" do
        it "should set posted_at to now in before_validation if position is being posted for the first time" do
            now = Time.now
            allow(Time).to receive(:now).and_return(now)
            instance = OpenPosition.create_from_hash!(valid_attrs.merge({
                featured: false,
                posted_at: nil
            }))
            instance.featured = true
            instance.save!
            expect(instance.posted_at).to eq(now)
        end

        it "should not set posted_at in before_validation if aleady exists" do
            expected_time = Time.now - 1.year
            open_position = OpenPosition.where(featured: false, posted_at: nil).first
            open_position.update_column(:posted_at, expected_time)
            open_position.featured = true
            open_position.save!
            expect(open_position.posted_at).to eq(expected_time)
        end
    end

    describe "validations" do
        it "should not be valid if featured but no posted_at" do
            instance = OpenPosition.create_from_hash!(valid_attrs.merge({
                featured: false,
                posted_at: nil
            }))
            instance.update_column(:featured, true)
            expect(instance.valid?).to eq(false)
        end

        describe "manually_archived" do
            it "should be valid if manually_archived and archived" do
                instance = OpenPosition.create_from_hash!(valid_attrs.merge({
                    manually_archived: true,
                    archived: true
                }))
                expect(instance.valid?).to eq(true)
            end

            it "should be valid if manually_archived false and archived false" do
                instance = OpenPosition.create_from_hash!(valid_attrs.merge({
                    manually_archived: false,
                    archived: false
                }))
                expect(instance.valid?).to eq(true)
            end

            it "should be invalid if manually_archived true and archived false" do
                instance = OpenPosition.create_from_hash!(valid_attrs.merge({
                    manually_archived: false,
                    archived: false
                }))
                instance.update_column(:manually_archived, true)
                expect(instance.valid?).to eq(false)
            end
        end

        describe "auto_expiration_date" do
            it "should be absent if manually_archived?" do
                instance = OpenPosition.first
                instance.update_columns({
                    archived: true,
                    manually_archived: true,
                    auto_expiration_date: Time.now
                })
                expect(instance.valid?).to eq(false)

                instance.update_column(:auto_expiration_date, nil)
                expect(instance.valid?).to eq(true)
            end

            it "should be absent if subscription_id" do
                instance = OpenPosition.first
                instance.auto_expiration_date = Time.now
                instance.subscription_id = SecureRandom.uuid

                # prevent before_validation filter from fixing things
                expect(instance).to receive(:reconcile_manually_archived_and_auto_expiration_date).at_least(1).times
                expect(instance.valid?).to eq(false)
                instance.subscription_id = nil
                expect(instance.valid?).to eq(true)
            end

            it "should be present if not manually_archived?" do
                instance = OpenPosition.first
                instance.update_columns({
                    archived: true,
                    manually_archived: false,
                    auto_expiration_date: nil
                })
                expect(instance.valid?).to eq(false)

                instance.update_column(:auto_expiration_date, Time.now)
                expect(instance.valid?).to eq(true)
            end
        end
    end

    describe "unreviewed_and_unrejected_candidate_position_interests" do
        it "should work" do
            open_position = OpenPosition.first
            open_position.candidate_position_interests.delete_all
            users = User.limit(2)
            CandidatePositionInterest.create(open_position_id: open_position.id, candidate_id: users[0].id, candidate_status: 'rejected', admin_status: 'unreviewed')
            CandidatePositionInterest.create(open_position_id: open_position.id, candidate_id: users[1].id, candidate_status: 'accepted', admin_status: 'unreviewed')
            expect(open_position.unreviewed_and_unrejected_candidate_position_interests.size).to be(1)
        end
    end

    describe "create_from_hash!" do

        it "should work" do
            expect_any_instance_of(OpenPosition).to receive(:merge_hash).and_call_original
            id = SecureRandom.uuid
            instance = OpenPosition.create_from_hash!(valid_attrs.merge(id: id))
            expect(instance.id).to eq(id)
            expect(instance.hiring_manager_id).to eq(valid_attrs['hiring_manager_id'])
        end

    end

    describe "update_from_hash" do

        it "should work" do
            instance = OpenPosition.create_from_hash!(valid_attrs)
            expect_any_instance_of(OpenPosition).to receive(:merge_hash).and_call_original
            OpenPosition.update_from_hash!({
                "id" => instance.id,
                "available_interview_times" => "updated",
                "updated_at" => instance.updated_at.to_timestamp
            })
            expect(instance.reload.available_interview_times).to eq("updated")
        end

        it "should log to Sentry if the position has been updated since being loaded" do
            instance = OpenPosition.first
            OpenPosition.where(id: instance.id).update(updated_at: instance.updated_at + 5.minutes)
            expect(Raven).to receive(:capture_exception)
            OpenPosition.update_from_hash!({
                "id" => instance.id,
                "updated_at" => instance.updated_at.to_timestamp
            })
        end

        it "should raise OldVersion if the position has been updated since being loaded by an admin" do
            instance = OpenPosition.first
            now = Time.now
            OpenPosition.where(id: instance.id).update(last_updated_at_by_hiring_manager: now + 5.minutes)
            expect {
                OpenPosition.update_from_hash!({
                    "id" => instance.id,
                    "last_updated_at_by_hiring_manager" => now.to_timestamp
                }, {
                    is_admin: true
                })
            }.to raise_error(OpenPosition::OldVersion)
        end
    end

    describe "merge_hash" do

        it "should set stuff" do
            open_position = OpenPosition.new
            hiring_manager_id = SecureRandom.uuid
            last_invitation_at = Time.now
            last_updated_at_by_hiring_manager = Time.now
            open_position.merge_hash({
                hiring_manager_id: hiring_manager_id,
                title: 'updated title',
                salary: 42,
                available_interview_times: 'updated times',
                last_invitation_at: last_invitation_at.to_timestamp,
                last_updated_at_by_hiring_manager: last_updated_at_by_hiring_manager.to_timestamp,
                place_id: '11',
                place_details: {
                    locality: {
                        long: 'Bartown'
                    }
                },
                role: 'some_role',
                position_descriptors: ['some_type'],
                job_post_url: 'https://some_job_post_url.com',
                short_description: 'some short description',
                description: 'Some full position description',
                perks: ['foo', 'bar', 'qux'],
                desired_years_experience: {
                    min: 2,
                    max: 5
                },
                featured: false,
                archived: true,
                will_sponsor_visa: true
            })

            expect(open_position.hiring_manager_id).to eq(hiring_manager_id)
            expect(open_position.title).to eq('updated title')
            expect(open_position.salary).to eq(42)
            expect(open_position.available_interview_times).to eq('updated times')
            expect(open_position.last_invitation_at.to_timestamp).to eq(last_invitation_at.to_timestamp)
            expect(open_position.last_updated_at_by_hiring_manager.to_timestamp).to eq(last_updated_at_by_hiring_manager.to_timestamp)
            expect(open_position.place_id).to eq('11')
            expect(open_position.place_details).to eq({
                locality: {
                    long: 'Bartown'
                }
            }.with_indifferent_access)
            expect(open_position.role).to eq('some_role')
            expect(open_position.position_descriptors).to eq(['some_type'])
            expect(open_position.job_post_url).to eq('https://some_job_post_url.com')
            expect(open_position.short_description).to eq('some short description')
            expect(open_position.description).to eq('Some full position description')
            expect(open_position.perks).to eq(['foo', 'bar', 'qux'])
            expect(open_position.desired_years_experience).to eq({
                min: 2,
                max: 5
            }.with_indifferent_access)
            expect(open_position.featured).to be(false)
            expect(open_position.archived).to be(true)
            expect(open_position.will_sponsor_visa).to be(true)
        end

        describe "skills" do
            attr_accessor :open_position

            before(:each) do
                @open_position = OpenPosition.new({
                    hiring_manager_id: SecureRandom.uuid
                })
                open_position.merge_hash(valid_attrs.merge(skills_attrs))
            end

            it "should add skills" do
                assert_attrs(open_position, skills_attrs)
                open_position.save!
                assert_attrs(open_position.reload, skills_attrs)

                # check that doing it over again doesn't screw it up
                open_position.merge_hash(skills_attrs)
                assert_attrs(open_position, skills_attrs)
                open_position.save!
                assert_attrs(open_position.reload, skills_attrs)
            end

            it "should overwrite skills after save" do
                new_attrs = skills_attrs({
                    :skills  => [{ text: "c", locale: "en"}, { text: "d", locale: "en" }]
                })
                open_position.merge_hash(new_attrs)
                assert_attrs(open_position, new_attrs)
                open_position.save!
                assert_attrs(open_position.reload, new_attrs)
            end

            it "should assign position on save" do
                open_position = OpenPosition.new({
                    hiring_manager_id: SecureRandom.uuid
                })
                attrs_with_flipped_lists = skills_attrs.merge({
                    :skills  => skills_attrs[:skills].reverse
                })
                open_position.merge_hash(valid_attrs.merge(attrs_with_flipped_lists))
                assert_attrs(open_position, attrs_with_flipped_lists)
                open_position.save!
                assert_attrs(open_position.reload, attrs_with_flipped_lists)
            end
        end

        describe "drafted_from_positions" do
            attr_accessor :open_position

            before(:each) do
                @open_position = OpenPosition.new({
                    hiring_manager_id: SecureRandom.uuid
                })
            end

            it "should default to false" do
                open_position.merge_hash(valid_attrs)
                open_position.save!
                open_position.reload

                expect(open_position.drafted_from_positions).to be(false)
            end

            it "can be set to true" do
                open_position.merge_hash(valid_attrs.merge(drafted_from_positions_attrs(true)))
                open_position.save!
                open_position.reload

                expect(open_position.drafted_from_positions).to be(true)
            end

            it "can be nil" do
                open_position.merge_hash(valid_attrs.merge(drafted_from_positions_attrs(nil)))
                open_position.save!
                open_position.reload

                expect(open_position.drafted_from_positions).to be(nil)
            end
        end
    end

    describe "reconcile_manually_archived_and_auto_expiration_date" do
        attr_accessor :position

        before(:each) do
            @position = OpenPosition.first
        end

        it "should be called before_validation on update" do
            expect(position).to receive(:reconcile_manually_archived_and_auto_expiration_date)
            position.save!
        end

        it "should be called before_validation on create" do
            expect_any_instance_of(OpenPosition).to receive(:reconcile_manually_archived_and_auto_expiration_date)
            OpenPosition.create!(title: 'foo', hiring_manager_id: User.first.id, auto_expiration_date: Date.today)
        end

        it "should call reconcile_manually_archived and then reconcile_auto_expiration_date" do
            expect(position).to receive(:reconcile_manually_archived).ordered
            expect(position).to receive(:reconcile_auto_expiration_date).ordered
            position.reconcile_manually_archived_and_auto_expiration_date
        end
    end

    describe "reconcile_manually_archived" do
        attr_accessor :position

        before(:each) do
            @position = OpenPosition.first
        end

        it "should set manually_archived to false if archived changed to false" do
            position.update_column(:manually_archived, true)
            expect(position).to receive(:archived_changed?).with(to: false).and_return(true)
            expect(position.manually_archived).not_to be(false)
            position.reconcile_manually_archived
            expect(position.manually_archived).to be(false)
        end

        describe "when archived changed to true" do

            before(:each) do
                expect(position).to receive(:archived_changed?).with(to: false).and_return(false)
                expect(position).to receive(:archived_changed?).with(to: true).and_return(true)
            end

            it "should set manually_archived to true if position was not just_auto_expired" do
                expect(position).to receive(:just_auto_expired).and_return(false)
                expect(position.manually_archived).not_to be(true)
                position.reconcile_manually_archived
                expect(position.manually_archived).to be(true)
            end
        end
    end

    describe "reconcile_auto_expiration_date" do
        attr_accessor :position

        before(:each) do
            @position = OpenPosition.first
        end

        it "should set auto_expiration_date to 60 days from today if new_record?" do
            position.auto_expiration_date = nil
            expect(position).to receive(:new_record?).and_return(true)
            expect(position.auto_expiration_date).to be_nil
            position.reconcile_auto_expiration_date
            expect(position.auto_expiration_date).to eq(Date.today + 60.days)
        end

        it "should set auto_expiration_date to 60 days from today if archived changed to false" do
            position.auto_expiration_date = nil
            expect(position).to receive(:archived_changed?).with(to: false).and_return(true)
            expect(position.auto_expiration_date).to be_nil
            position.reconcile_auto_expiration_date
            expect(position.auto_expiration_date).to eq(Date.today + 60.days)
        end

        it "should set auto_expiration_date to nil if subscription_id" do
            position.auto_expiration_date = Time.now
            position.subscription_id = SecureRandom.uuid
            position.reconcile_auto_expiration_date
            expect(position.auto_expiration_date).to be_nil
        end

        describe "when not new_record? and when archived_changed has changed to true" do

            before(:each) do
                expect(position).to receive(:new_record?).and_return(false)
                expect(position).to receive(:archived_changed?).with(to: false).and_return(false)
                expect(position).to receive(:archived_changed?).with(to: true).and_return(true)
            end

            it "should set auto_expiration_date to nil if manually_archived?" do
                position.auto_expiration_date = Date.today
                expect(position).to receive(:manually_archived?).and_return(true)
                position.reconcile_auto_expiration_date
                expect(position.auto_expiration_date).to be_nil
            end
        end
    end

    describe "trigger_curation_email" do
        it "should work" do
            open_position = OpenPosition.first
            users = User.limit(9)
            CandidatePositionInterest.where(open_position_id: open_position.id).delete_all

            CandidatePositionInterest.create!(
                candidate_id: users[0].id, open_position_id: open_position.id, candidate_status: 'accepted', hiring_manager_status: 'pending', admin_status: 'reviewed'
            )
            CandidatePositionInterest.create!(
                candidate_id: users[1].id, open_position_id: open_position.id, candidate_status: 'accepted', hiring_manager_status: 'unseen', admin_status: 'reviewed'
            )
            CandidatePositionInterest.create!(
                candidate_id: users[2].id, open_position_id: open_position.id, candidate_status: 'rejected', hiring_manager_status: 'unseen', admin_status: 'reviewed'
            )
            CandidatePositionInterest.create!(
                candidate_id: users[3].id, open_position_id: open_position.id, candidate_status: 'accepted', hiring_manager_status: 'hidden', admin_status: 'reviewed'
            )
            CandidatePositionInterest.create!(
                candidate_id: users[4].id, open_position_id: open_position.id, candidate_status: 'accepted', hiring_manager_status: 'rejected', admin_status: 'reviewed'
            )
            CandidatePositionInterest.create!(
                candidate_id: users[5].id, open_position_id: open_position.id, candidate_status: 'accepted', hiring_manager_status: 'pending', admin_status: 'outstanding'
            )
            CandidatePositionInterest.create!(
                candidate_id: users[6].id, open_position_id: open_position.id, candidate_status: 'accepted', hiring_manager_status: 'unseen', admin_status: 'outstanding'
            )
            CandidatePositionInterest.create!(
                candidate_id: users[7].id, open_position_id: open_position.id, candidate_status: 'rejected', hiring_manager_status: 'rejected', admin_status: 'outstanding'
            )
            CandidatePositionInterest.create!(
                candidate_id: users[8].id, open_position_id: open_position.id, candidate_status: 'accepted', hiring_manager_status: 'rejected', admin_status: 'outstanding'
            )

            event = Event.new
            allow(event).to receive(:log_to_external_systems)
            expect(Event).to receive(:create_server_event!).with(anything, open_position.hiring_manager_id, 'open_position:send_curation_email', open_position.event_attributes(open_position.hiring_manager_id).merge({
                num_outstanding: 2,
                num_approved: 2
            })).and_return(event)

            open_position.trigger_curation_email(open_position.hiring_manager_id)
        end

        it "should update curation_email_last_triggered_at" do
            open_position = OpenPosition.first
            open_position.update_column(:curation_email_last_triggered_at, nil)
            event = open_position.trigger_curation_email(open_position.hiring_manager_id)
            expect(open_position.curation_email_last_triggered_at).to eq(event.created_at)
        end
    end

    describe "as_json" do
        it "should respect except=hiring_application" do
            open_position = OpenPosition.first
            expect(open_position.as_json({except: ["hiring_application"]}).keys).not_to include("hiring_application")
        end

        it "should include ADMIN_FIELDS if asked for" do
            open_position = OpenPosition.first
            now = Time.now
            open_position.update_column(:curation_email_last_triggered_at, now)
            open_position.update_column(:last_curated_at, now + 5.minutes)
            open_position.update_column(:posted_at, now + 10.minutes)
            open_position.update_column(:last_updated_at_by_hiring_manager, now + 15.minutes)

            # have to get someone with no current relationships to avoid conflicts
            hiring_manager = User.left_outer_joins(:candidate_relationships).where("hiring_relationships.id is null").first
            hiring_manager.update_columns({
                last_seen_at: Time.now,
                hiring_team_id: HiringTeam.first.id
            })
            allow(open_position).to receive(:hiring_manager).and_return(hiring_manager)

            allow(open_position).to receive(:accepted_and_unhidden_candidate_position_interests)
                .and_return(CandidatePositionInterest.limit(1).to_a)

            allow(open_position).to receive(:unreviewed_and_unrejected_candidate_position_interests)
                .and_return(CandidatePositionInterest.limit(1).to_a)

            json = open_position.as_json({
                fields: ['ADMIN_FIELDS']
            })

            expect(json['hiring_manager_id']).to eq(hiring_manager.id)
            expect(json['hiring_manager_email']).to eq(hiring_manager.email)
            expect(json['hiring_manager_name']).to eq(hiring_manager.name)
            expect(json['hiring_manager_company_name']).to eq(hiring_manager.company_name)
            expect(json['hiring_manager_last_seen_at']).to eq(hiring_manager.last_seen_at.to_timestamp)
            expect(json['hiring_manager_hiring_team_id']).to eq(hiring_manager.hiring_team_id)
            expect(json['num_interested_candidates']).to eq(1)
            expect(json['num_unreviewed_and_unrejected_interests']).to eq(1)
            expect(json['curation_email_last_triggered_at']).to eq(now.to_timestamp)
            expect(json['last_curated_at']).to eq((now + 5.minutes).to_timestamp)
            expect(json['posted_at']).to eq((now + 10.minutes).to_timestamp)
            expect(json['last_updated_at_by_hiring_manager']).to eq((now + 15.minutes).to_timestamp)
        end

        it "should not include certain fields if ADMIN_FIELDS not asked for" do
            open_position = OpenPosition.first
            now = Time.now
            open_position.update_column(:curation_email_last_triggered_at, now)
            open_position.update_column(:last_curated_at, now + 5.minutes)

            expect(open_position).not_to receive(:hiring_manager)
            expect(open_position).not_to receive(:accepted_and_unhidden_candidate_position_interests)
            expect(open_position).not_to receive(:unreviewed_and_unrejected_candidate_position_interests)

            json = open_position.as_json

            expect(json['curation_email_last_triggered_at']).to be_nil
            expect(json['last_curated_at']).to be_nil
            expect(json['posted_at']).to be_nil
            expect(json['last_updated_at_by_hiring_manager']).to be_nil
        end

        it "should include default fields" do
            fields = %w(
                updated_at
                created_at
                last_invitation_at
                skills
                auto_expiration_date
                will_sponsor_visa
                external
            )
            keys = OpenPosition.first.as_json.keys
            fields.each do |field|
                expect(keys).to include(field)
            end
        end
    end

    describe "log_archived_or_renewal_canceled_event" do
        attr_reader :open_position

        before(:each) do
            # {:subscriptions => :hiring_team} is necessary (maybe) to prevent
            # an intermittent issue on staging where we get a subscription with no owner
            @open_position = OpenPosition.left_outer_joins(hiring_team: [{:subscriptions => :hiring_team}, :owner])
                .where(archived:false)
                .where("subscriptions.id is null and hiring_teams.owner_id is not null")
                .first
            open_position.subscription = create_subscription(open_position.hiring_team)
            open_position.save!
        end

        it "should log event when a subcription renewal has been canceled" do
            open_position.subscription.update_cancel_at_period_end!(true)
            start = Time.now
            open_position.send(:log_archived_or_renewal_canceled_event)
            assert event = Event.where("created_at > ?", start).where(user_id: open_position.hiring_manager_id).first
            expect(event.payload).to have_entries(open_position.event_attributes(open_position.hiring_manager_id).merge({
                position_just_archived: false,
                renewal_just_canceled: true
            }).stringify_keys)
        end

        it "should log event when a subcription renewal has been canceled and the position has been archived" do
            open_position.subscription.update_cancel_at_period_end!(true)
            open_position.update!(archived: true)
            start = Time.now
            open_position.send(:log_archived_or_renewal_canceled_event)
            assert event = Event.where("created_at > ?", start).where(user_id: open_position.hiring_manager_id).first
            expect(event.payload).to have_entries(open_position.event_attributes(open_position.hiring_manager_id).merge({
                position_just_archived: true,
                renewal_just_canceled: true
            }).stringify_keys)
        end

        it "should log event when a position has been archived with no change to subscription" do
            open_position.update!(archived: true)
            start = Time.now
            open_position.send(:log_archived_or_renewal_canceled_event)
            assert event = Event.where("created_at > ?", start).where(user_id: open_position.hiring_manager_id).first
            expect(event.payload).to have_entries(open_position.event_attributes(open_position.hiring_manager_id).merge({
                position_just_archived: true,
                renewal_just_canceled: false
            }).stringify_keys)
        end

        it "should log event to owner if different from creator" do
            another_user = User.left_outer_joins(:hiring_team).where("hiring_teams.id is null").where.not(id: open_position.hiring_manager_id).first
            another_user.update!(hiring_team: open_position.hiring_team)
            open_position.hiring_team.update!(owner_id: another_user.id)
            open_position.update!(archived: true)

            start = Time.now
            open_position.send(:log_archived_or_renewal_canceled_event)

            [open_position.hiring_manager,another_user].each do |user|
                assert event = Event.where("created_at > ?", start).where(user_id: user.id).first
                expect(event.payload).to have_entries(open_position.event_attributes(user.id).stringify_keys)

            end
        end

    end

    describe "review_path" do

        it "should work" do
            position = OpenPosition.first
            expect(position.send(:review_path)).to eq("/hiring/positions?positionId=#{position.id}&action=review")
        end

    end

    def assert_attrs(obj, attrs)
        attrs = attrs.stringify_keys

        # find any keys that refer to associations and handle them specially
        attrs.each do |key, val|

            # if this key refers to an association instead of a vanilla attribute
            if obj.class.reflections.keys.include?(key)

                # turn it into an array even if it isn't so we can use the
                # same code for has_one and has_many relationships
                actual_list = Array.wrap(obj.send(key.to_sym))
                expected_list = Array.wrap(val)

                expect(actual_list.size).to eq(expected_list.size), "unexpected size for #{key.inspect}. #{actual_list.size} != #{expected_list.size}"

                actual_list.each_with_index do |actual_item, i|
                    assert_attrs(actual_item, expected_list[i])
                end

                # ensure we remove this key so as not to trip up the next expectation
                attrs.delete(key)
            end
        end

        actual_attrs = obj.attributes.slice(*attrs.keys.map(&:to_s)).stringify_keys
        expect(actual_attrs).to eq(attrs.stringify_keys)
    end

    def skills_attrs(attrs = {})
        {
            :skills => [{ text: "a", locale: "en"}, { text: "b", locale: "en" }]
        }.with_indifferent_access.merge(attrs)
    end

    def drafted_from_positions_attrs(value, attrs = {})
        {
            :drafted_from_positions => value
        }.with_indifferent_access.merge(attrs)
    end

    def valid_attrs(attrs = {})
        {
            hiring_manager_id: hiring_manager.id,
            title: 'title',
            salary: 42,
            available_interview_times: 'times',
            place_id: '1',
            place_details: {
                locality: {
                    long: 'Footown'
                }
            },
            role: 'valid_role',
            position_descriptors: ['valid_position_descriptors'],
            job_post_url: 'https://valid_job_post_url.com',
            description: 'Valid description',
            perks: ['free_food'],
            desired_years_experience: {
                min: 1,
                max: 3
            },
            featured: true,
            archived: false
        }.with_indifferent_access.merge(attrs)
    end
end
