require 'spec_helper'

describe StudentDashboard::ToJsonFromApiParams do
    fixtures(:users, :playlists, :cohorts)

    before(:each) do
        @user = users(:user_with_active_playlist)
    end

    describe "join_and_select_streams" do

        before(:each) do
            # build the test user from scratch so that each test is run without having to worry
            # about extraneous data outside the scope of the test
            @user = User.create!(
                name: 'Testuser',
                email: 'johnny.appleseed@orchard.com',
                uid: SecureRandom.uuid,
                password: 'password'
            )
            SafeCache.clear
        end

        it "should include streams the user has completed" do
            completed_streams = Lesson::Stream.where(was_published: true).limit(3).to_a

            # create completed stream progress records for each stream
            completed_streams.each do |stream|
                Lesson::StreamProgress.create!(
                    user_id: @user.id,
                    locale_pack_id: stream.locale_pack_id,
                    started_at: Time.now,
                    completed_at: Time.now
                )
            end

            expected_stream_locale_pack_ids = completed_streams.pluck(:locale_pack_id)
            assert_student_dashboard_includes_streams(expected_stream_locale_pack_ids)
        end

        it "should include streams the user has bookmarked" do
            favorite_streams = Lesson::Stream.where(was_published: true).limit(3).to_a

            # favorite each of the streams
            @user.favorite_lesson_stream_locale_pack_ids = favorite_streams.pluck(:locale_pack_id)

            expected_stream_locale_pack_ids = favorite_streams.pluck(:locale_pack_id)
            assert_student_dashboard_includes_streams(expected_stream_locale_pack_ids)
        end

        it "should include required streams attached to user's relevant_cohort" do
            # ensure user has a relevant_cohort
            @user.fallback_program_type = 'mba'
            @user.save!

            expect(@user.relevant_cohort).not_to be_nil
            required_stream_locale_pack_ids = @user.relevant_cohort.get_required_stream_locale_pack_ids
            expect(required_stream_locale_pack_ids).not_to be_empty

            assert_student_dashboard_includes_streams(required_stream_locale_pack_ids)
        end

        it "should include streams in user's active playlist" do
            active_playlist = playlists(:published_playlist)

            @user.active_playlist_locale_pack_id = active_playlist.locale_pack_id
            @user.save!

            # Our main concern is the data that gets passed along when creating the Playlist::ToJsonFromApiParams instance; NOT the result of
            # its computation, since that's not unit-testy. Since we don't really care about the result, we can return a mock result.
            expect(Playlist).to receive(:stream_locale_pack_ids_for_playlist).with(@user.active_playlist_locale_pack_id, @user.locale).and_return(active_playlist.stream_locale_pack_ids)

            # We've mocked the result of Playlist::ToJsonFromApiParams, but we still expect the active playlist's stream_locale_pack_ids
            # to be included in the call to Lesson::Stream::ToJsonFromApiParams, so it's important that we assert that.
            assert_student_dashboard_includes_streams(active_playlist.stream_locale_pack_ids)
        end

        it "should include required streams from the user's institution" do
            # ensure user has a relevant_cohort
            institution = Institution.all.detect do |inst|
                if inst.playlist_pack_ids.any?
                    @user = inst.users.first
                    !!@user
                end
            end
            @user.update(active_playlist_locale_pack_id: nil)

            stream_locale_pack_ids = ActiveRecord::Base.connection.execute("
                select stream_locale_pack_id from institution_stream_locale_packs where institution_id='#{institution.id}'
            ").to_a.map { |r| r['stream_locale_pack_id'] }
            expect(stream_locale_pack_ids).not_to be_empty

            assert_student_dashboard_includes_streams(stream_locale_pack_ids)
        end

        it "should include optional streams" do
            # ensure user has a relevant_cohort
            @user.fallback_program_type = 'mba'
            @user.save!

            expect(@user.relevant_cohort).not_to be_nil
            optional_stream_locale_pack_ids = @user.relevant_cohort.get_optional_stream_locale_pack_ids
            expect(optional_stream_locale_pack_ids).not_to be_empty

            assert_student_dashboard_includes_streams(optional_stream_locale_pack_ids)

        end

        # An overarching spec to test everything together. It's kind of annoying, but since
        # StudentDashboard::ToJsonFromApiParams bypasses ActiveRecord and interacts with the
        # database directly, we build the user from scratch and attach data to the user as needed.
        it "should include streams that are favorited, completed, attached to user's access groups, attached to user's relevant_cohort, and attached to user's active_playlist" do
            # gather an active playlist and some streams
            active_playlist = playlists(:published_playlist)
            streams = Lesson::Stream.where(was_published: true).limit(9).to_a
            favorite_streams = streams[0..2]
            completed_streams = streams[3..5]

            @user.fallback_program_type = 'mba' # ensure user has relevant_cohort
            @user.active_playlist_locale_pack_id = active_playlist.locale_pack_id # ensure user has an active playlist
            @user.save!

            expect(@user.relevant_cohort).not_to be_nil # sanity check
            expect(@user.relevant_cohort.get_required_stream_locale_pack_ids).not_to be_empty # sanity check

            # favorites
            @user.favorite_lesson_stream_locale_pack_ids = favorite_streams.pluck(:locale_pack_id)

            # completed streams
            completed_streams.each do |stream|
                Lesson::StreamProgress.create!(
                    user_id: @user.id,
                    locale_pack_id: stream.locale_pack_id,
                    started_at: Time.now,
                    completed_at: Time.now
                )
            end

            # favorite streams, completed streams, access group streams, streams required by relevant_cohort,
            # optional streams on relevant_cohort, and streams attached to user's active playlist
            expected_stream_locale_pack_ids = (
                favorite_streams.map(&:locale_pack_id) +
                completed_streams.map(&:locale_pack_id) +
                @user.relevant_cohort.get_required_stream_locale_pack_ids +
                @user.relevant_cohort.get_optional_stream_locale_pack_ids +
                active_playlist.stream_locale_pack_ids
            ).uniq

            assert_student_dashboard_includes_streams(expected_stream_locale_pack_ids)
        end

        it 'should not include the stream locale pack ids for the user’s active_playlist_locale_pack_id if it’s for an unpublished playlist' do
            # this playlist isn't published in their locale
            active_playlist = playlists(:published_only_in_es)

            @user.fallback_program_type = 'mba' # ensure user has relevant_cohort
            @user.active_playlist_locale_pack_id = active_playlist.locale_pack_id # ensure user has an active playlist that isn't published
            @user.save!

            expect(@user.relevant_cohort).not_to be_nil # sanity check
            expect(@user.relevant_cohort.get_required_stream_locale_pack_ids).not_to be_empty # sanity check

            # streams required by relevant_cohort, optional streams on relevant_cohort, but NOT streams attached to user's active playlist (since it's unpublished)
            expected_stream_locale_pack_ids = (
                @user.relevant_cohort.get_required_stream_locale_pack_ids +
                @user.relevant_cohort.get_optional_stream_locale_pack_ids
            ).uniq

            assert_student_dashboard_includes_streams(expected_stream_locale_pack_ids)
        end


        def assert_student_dashboard_includes_streams(expected_stream_locale_pack_ids = [])

            @user.access_groups << AccessGroup.find_by_name('SUPERVIEWER')

            result = get_student_dashboard_json({
                filters: {
                    user_can_see: true,
                    in_users_locale_or_en: true
                }
            })
            expect(expected_stream_locale_pack_ids - result['lesson_streams'].map { |s| s['locale_pack']['id']}).to be_empty
        end
    end

    describe "available_playlists" do

        before(:each) do
            @group = AccessGroup.find_by_name('SUPERVIEWER')
            @user = @group.users.where(pref_locale: 'en').first
        end

        it "should return playlists from relevant cohort" do
            allow_any_instance_of(User).to receive(:relevant_cohort).and_return(cohorts(:published_mba_cohort).published_version)

            result = get_student_dashboard_json
            expected_playlist_ids = get_playlist_ids({locale_pack_id: @user.relevant_cohort.playlist_pack_ids})
            expect(result['available_playlists'].map { |p| p['id'] }).to match_array(expected_playlist_ids)
        end

        it "should return playlists from institutions" do
            playlists = [playlists(:published_playlist)]
            institution = Institution.first
            institution = Institution.update_from_hash!({id: institution.id, updated_at: institution.updated_at, "playlist_pack_ids": playlists.map(&:locale_pack_id), "groups" => []})

            @user.institutions = [institution]
            @user.save!

            result = get_student_dashboard_json
            expect(result['available_playlists'].map { |p| p['id'] }).to match_array(playlists.map(&:id))
        end

        def get_playlist_ids(extra_filters = {})
             Playlist::ToJsonFromApiParams.new({
                user: @user,
                filters: {
                    published: true,
                    in_users_locale_or_en: true
                }.merge(extra_filters),
                fields: ['id']
            }).to_a.map { |p| p['id'] }
        end

    end

    describe "view_as" do
        it "should still return an empty array instead of nil for lesson_streams when viewing by group" do
            access_group = AccessGroup.first
            student_dashboard = get_student_dashboard_json({
                filters: {
                    view_as: "group:#{access_group.id}",
                }
            })
            expect(student_dashboard["lesson_streams"]).to be_empty
        end

        it "should filter by group:name" do
            access_group = AccessGroup.all.detect { |g| g.lesson_stream_locale_packs.any? }
            student_dashboard = get_student_dashboard_json({
                filters: {
                    view_as: "group:#{access_group.name}",
                    in_locale_or_en: @user.locale
                }
            })
            expect(student_dashboard["available_playlists"]).to be_empty
            expect(student_dashboard["lesson_streams"]).to be_empty
        end

        it "should filter by institution:id" do
            institution = Institution.first

            # Ensure some playlists
            playlist_query = Playlist.where("locale": @user.locale)
            first_playlist = playlist_query.first
            second_playlist = playlist_query.second
            playlist_ids = [first_playlist.id, second_playlist.id]
            playlist_pack_ids = [first_playlist.locale_pack_id, second_playlist.locale_pack_id]
            institution.playlist_pack_ids = playlist_pack_ids
            institution.save!

            student_dashboard = get_student_dashboard_json({
                filters: {
                    view_as: "institution:#{institution.id}",
                    in_locale_or_en: @user.locale
                }
            })

            expect(student_dashboard["available_playlists"].pluck("id")).to match_array(playlist_ids)
        end

        it "should filter by cohort:id" do
            cohort = Cohort.first

            # Ensure some playlists
            playlist_query = Playlist.where("locale": @user.locale)
            first_playlist = playlist_query.first
            second_playlist = playlist_query.second
            fourth_playlist = playlist_query.fourth

            playlist_ids = [first_playlist.id, second_playlist.id, fourth_playlist.id]
            cohort.playlist_collections = [{title: '', required_playlist_pack_ids: [first_playlist.locale_pack_id, second_playlist.locale_pack_id]}]
            cohort.specialization_playlist_pack_ids = [fourth_playlist.locale_pack_id]
            cohort.publish!

            student_dashboard = get_student_dashboard_json({
                filters: {
                    view_as: "cohort:#{cohort.id}",
                    in_locale_or_en: @user.locale
                }
            })

            expect(student_dashboard["available_playlists"].pluck("id")).to match_array(playlist_ids)
        end

        it "should use the published cohort version for the specified id if there is one" do
            cohort = Cohort.first

            # Ensure it has a published version
            playlist_query = Playlist.where("locale": @user.locale)
            first_playlist = playlist_query.first
            second_playlist = playlist_query.second
            fourth_playlist = playlist_query.fourth

            playlist_ids = [first_playlist.id, second_playlist.id, fourth_playlist.id]
            cohort.playlist_collections = [{title: '', required_playlist_pack_ids: [first_playlist.locale_pack_id, second_playlist.locale_pack_id]}]
            cohort.specialization_playlist_pack_ids = [fourth_playlist.locale_pack_id]
            cohort.publish!

            # Save a new version of the cohort, but don't publish
            third_playlist = playlist_query.third
            cohort.playlist_collections = [{title: '', required_playlist_pack_ids: [first_playlist.locale_pack_id, second_playlist.locale_pack_id, third_playlist.locale_pack_id]}]
            cohort.save! # save!, don't publish!

            student_dashboard = get_student_dashboard_json({
                filters: {
                    view_as: "cohort:#{cohort.id}",
                    in_locale_or_en: @user.locale
                }
            })

            expect(student_dashboard["available_playlists"].pluck("id")).to match_array(playlist_ids)
        end

        it "should not add any playlist locale pack ids if no published cohort version is found" do
            # stub this out, ensuring that the cohort found from the
            # specified id returns nil when published_version is called
            allow_any_instance_of(Cohort).to receive(:published_version).and_return(nil)
            cohort = Cohort.first

            # Ensure some playlists
            playlist_query = Playlist.where("locale": @user.locale)
            first_playlist = playlist_query.first
            second_playlist = playlist_query.second
            cohort.playlist_collections = [{title: '', required_playlist_pack_ids: [first_playlist.locale_pack_id, second_playlist.locale_pack_id]}]
            cohort.specialization_playlist_pack_ids = [playlist_query.third.locale_pack_id]
            cohort.save!

            student_dashboard = get_student_dashboard_json({
                filters: {
                    view_as: "cohort:#{cohort.id}",
                    in_locale_or_en: @user.locale
                }
            })

            expect(student_dashboard["available_playlists"].pluck("id")).to match_array([])
        end
    end

    def get_student_dashboard_json(params = {})
        params = params.with_indifferent_access
        params[:user] = @user
        params = {
            filters: {
                user_can_see: true,
                in_users_locale_or_en: true
            }
        }.deep_merge(params)
        results = get_json(params)
        results.first
    end

    def get_json(params = {})
        json = StudentDashboard::ToJsonFromApiParams.new(params).json
        ActiveSupport::JSON.decode(json)
    end

    def get_student_dashboard(params = {})
        params = params.with_indifferent_access
        params[:user] = @user
        params = {}.deep_merge(params)
        StudentDashboard::ToJsonFromApiParams.new(params)
    end

end
