require 'spec_helper'

describe User::BatchUpdater do

    fixtures(:users, :cohorts)
    attr_reader :cohort, :user, :cohort_application, :current_user

    before(:each) do
        @current_user = User.new(id: SecureRandom.uuid, name: 'current_user')
    end

    describe "exec" do

        it "should raise if user not found" do
            expect(Proc.new {
                User::BatchUpdater.new(current_user, [SecureRandom.uuid], {}).exec
            }).to raise_error(RuntimeError, "Cannot find all users")
        end

        describe "refresh progress views" do
            it "should refresh upon completion if any users updated" do
                expect(RefreshMaterializedInternalReportsViews).to receive(:perform_later_with_debounce)
                batch_updater = User::BatchUpdater.new(current_user, [User.first.id], {can_edit_career_profile: true})
                batch_updater.exec
            end

            it "should not refresh if all users errored" do
                expect(RefreshMaterializedInternalReportsViews).not_to receive(:perform_later_with_debounce)
                batch_updater = User::BatchUpdater.new(current_user, [User.first.id], {can_edit_career_profile: true})
                allow_any_instance_of(User).to receive(:save!) do
                    raise ActiveRecord::RecordInvalid
                end
                batch_updater.exec
            end
        end

        describe "can_edit_career_profile update" do
            attr_reader :has_enabled, :not_enabled

            before(:each) do
                @has_enabled = User.joins(:career_profile).where.not(career_profiles: {id: CareerProfileList.pluck(:career_profile_ids).flatten}).where(can_edit_career_profile: true).first
                @not_enabled = User.where(can_edit_career_profile: false).first
            end

            it "should update can_edit_career_profile=true" do
                expect {
                    User::BatchUpdater.new(current_user, [has_enabled.id, not_enabled.id], {can_edit_career_profile: true}).exec
                }.not_to change{ has_enabled.reload.updated_at }
                expect(not_enabled.reload.can_edit_career_profile).to eq(true)

            end

            it "should update can_edit_career_profile=false" do
                expect {
                    User::BatchUpdater.new(current_user, [has_enabled.id, not_enabled.id], {can_edit_career_profile: false}).exec
                }.not_to change{ not_enabled.reload.updated_at }
                expect(has_enabled.reload.can_edit_career_profile).to eq(false)
            end

        end

        describe "cohort_status update" do

            describe "with no existing application" do
                before(:each) do
                    # this is put inside of its own before each hook for optimization purposes
                    @user = User.where.not(id: CohortApplication.pluck('user_id')).first
                end

                ['mba', 'career_network_only', 'the_business_certificate'].each do |cohort_type|
                    before(:each) do
                        @cohort = cohorts("published_#{cohort_type}_cohort")
                    end
                    ['pending', 'accepted', 'pre_accepted'].each do |status|
                        describe "and cohort_status=#{status}" do
                            it "should create a new application" do
                                expect_new_application(status) {
                                    updater = update_cohort_status(status)
                                }
                            end
                        end
                    end
                end
                ['mba', 'emba', 'career_network_only', 'the_business_certificate'].each do |cohort_type|
                    ['rejected', 'expelled', 'deferred'].each do |status|
                        describe "and cohort_status=#{status}" do
                            it "should error" do
                                expect_error("#{user.account_id} does not have an application for cohort #{cohort.name}") {
                                    update_cohort_status(status)
                                }
                            end
                        end
                    end
                end

                it "should set manual_admin_decision" do
                    status = 'pre_accepted'
                    application = expect_new_application(status) {
                        updater = update_cohort_status(status)
                    }
                    expect(application.admissions_decision).to eq('manual_admin_decision')
                end
            end

            ['mba', 'emba', 'career_network_only', 'the_business_certificate'].each do |cohort_type|
                ['pending', 'accepted', 'pre_accepted'].each do |status|
                    describe "and attempting to update an existing #{status} #{cohort_type} application for a different cohort" do
                        it "should add errors for user" do
                            @user = users("#{status}_#{cohort_type}_cohort_user")
                            cohort_application = user.cohort_applications.first
                            @cohort = Cohort.where.not(id: cohort_application.cohort_id).first

                            cohort_application.status = status
                            cohort_application.save!
                            expect_error("#{user.account_id} has an existing #{status} application for #{cohort_application.cohort.name}") {
                                update_cohort_status('accepted')
                            }
                        end
                    end
                end
            end

            describe "with an existing application" do
                it "should set manual_admin_decision" do
                    @user = users("pending_mba_cohort_user")
                    @cohort_application = @user.cohort_applications.first
                    @cohort = @cohort_application.cohort
                    update_cohort_status('pre_accepted')
                    expect(@user.last_application.admissions_decision).to eq('manual_admin_decision')
                end
            end

            describe "with an existing pending application" do
                ['mba', 'emba', 'career_network_only', 'the_business_certificate'].each do |cohort_type|
                    describe "for #{cohort_type} cohort" do
                        describe "and cohort_status=pending" do
                            it "should do nothing" do
                                @user = users("pending_#{cohort_type}_cohort_user")
                                @cohort_application = @user.cohort_applications.first
                                @cohort = @cohort_application.cohort
                                expect_no_application_changes {
                                    update_cohort_status('pending')
                                }
                            end
                        end

                        ['rejected', 'deferred', 'pre_accepted'].each do |status|
                            describe "and cohort_status=#{status}" do
                                it "should update status on existing application" do
                                    @user = users("pending_#{cohort_type}_cohort_user")
                                    @cohort_application = @user.cohort_applications.first
                                    @cohort = @cohort_application.cohort

                                    if status == 'rejected'
                                        expect_any_instance_of(CohortApplication).to receive(:rejected_or_expelled).and_return('rejected')
                                    end

                                    expect_application_update(cohort_application, status) {
                                        update_cohort_status(status)
                                    }
                                end
                            end
                        end
                        describe "and cohort_status=expelled" do
                            it "should error" do
                                @user = users("pending_#{cohort_type}_cohort_user")
                                @cohort_application = @user.cohort_applications.first
                                @cohort = @cohort_application.cohort
                                expect_error("Cannot expel #{user.account_id} from cohort #{cohort.name} because he/she is not yet accepted.") {
                                    update_cohort_status('expelled')
                                }
                            end
                        end
                    end
                end
            end

            describe "with an existing rejected application" do
                ['mba', 'emba', 'career_network_only', 'the_business_certificate'].each do |cohort_type|
                    before(:each) do
                        @user = users("rejected_#{cohort_type}_cohort_user")
                        @cohort_application = @user.cohort_applications.first
                        @cohort = @cohort_application.cohort
                    end
                    describe "for #{cohort_type} cohort" do
                        ['pending', 'accepted', 'deferred', 'pre_accepted'].each do |status|
                            describe "and cohort_status=#{status}" do
                                it "should update status on existing application" do
                                    expect_application_update(cohort_application, status) {
                                        update_cohort_status(status)
                                    }
                                end
                            end
                        end
                        describe "and cohort_status=rejected" do
                            it "should do nothing" do
                                expect_no_application_changes {
                                    update_cohort_status('rejected')
                                }
                            end
                        end
                        describe "and cohort_status=expelled" do
                            it "should error" do
                                expect_error("Cannot expel #{user.account_id} from cohort #{cohort.name} because he/she is not yet accepted.") {
                                    update_cohort_status('expelled')
                                }
                            end
                        end
                    end
                end
            end

            describe "with an existing accepted application" do
                ['mba', 'emba', 'career_network_only', 'the_business_certificate'].each do |cohort_type|
                    before(:each) do
                        @user = users("accepted_#{cohort_type}_cohort_user")
                        @cohort_application = @user.cohort_applications.first
                        @cohort = @cohort_application.cohort
                    end
                    describe "for #{cohort_type} cohort" do
                        ['pending', 'rejected', 'expelled', 'deferred', 'pre_accepted'].each do |status|
                            describe "and cohort_status=#{status}" do
                                it "should update status on existing application" do
                                    expect_application_update(cohort_application, status) {
                                        update_cohort_status(status)
                                    }
                                end
                            end
                        end
                        describe "and cohort_status=accepted" do
                            it "should do nothing" do
                                expect_no_application_changes {
                                    update_cohort_status('accepted')
                                }
                            end
                        end
                    end
                end
            end

            describe "with an existing deferred application" do
                ['mba', 'emba', 'career_network_only', 'the_business_certificate'].each do |cohort_type|
                    before(:each) do
                        @user = users("deferred_#{cohort_type}_cohort_user")
                        @cohort_application = @user.cohort_applications.first
                        @cohort = @cohort_application.cohort
                    end
                    describe "for #{cohort_type} cohort" do
                        ['pending', 'rejected', 'expelled', 'accepted', 'pre_accepted'].each do |status|
                            describe "and cohort_status=#{status}" do
                                it "should update status on existing application" do
                                    expect_application_update(cohort_application, status) {
                                        update_cohort_status(status)
                                    }
                                end
                            end
                        end
                        describe "and cohort_status=deferred" do
                            it "should do nothing" do
                                expect_no_application_changes {
                                    update_cohort_status('deferred')
                                }
                            end
                        end
                    end
                end
            end

            describe "with an existing expelled application" do
                ['mba', 'emba', 'career_network_only', 'the_business_certificate'].each do |cohort_type|
                    before(:each) do
                        @user = users("expelled_#{cohort_type}_cohort_user")
                        @cohort_application = @user.cohort_applications.first
                        @cohort = @cohort_application.cohort
                    end
                    describe "for #{cohort_type} cohort" do
                        ['pending', 'rejected', 'deferred', 'accepted', 'pre_accepted'].each do |status|
                            describe "and cohort_status=#{status}" do
                                it "should error" do
                                    expect_error("Cannot update status to #{status} because #{user.account_id} has been expelled.") {
                                        update_cohort_status(status)
                                    }
                                end
                            end
                        end
                        describe "and cohort_status=expelled" do
                            it "should do nothing" do
                                expect_no_application_changes {
                                    update_cohort_status('expelled')
                                }
                            end
                        end
                    end
                end
            end

            describe "with an existing pre_accepted application" do
                ['mba', 'emba', 'career_network_only', 'the_business_certificate'].each do |cohort_type|
                    before(:each) do
                        @user = users("pre_accepted_#{cohort_type}_cohort_user")
                        @cohort_application = user.cohort_applications.first
                        @cohort = cohorts("published_#{cohort_type}_cohort")
                    end
                    describe "for #{cohort_type} cohort" do
                        ['pending', 'deferred'].each do |status|
                            describe "and cohort_status=#{status}" do
                                it "should error" do
                                    expect_error("#{user.account_id} must have their status updated to accepted or rejected; not #{status}") {
                                        update_cohort_status(status)
                                    }
                                end
                            end
                        end
                        ['accepted', 'rejected'].each do |status|
                            describe "and cohort_status=#{status}" do
                                it "should update status on existing application" do
                                    expect_application_update(cohort_application, status) {
                                        update_cohort_status(status)
                                    }
                                end
                            end
                        end
                    end
                    describe "and cohort_status=pre_accepted" do
                        it "should do nothing" do
                            expect_no_application_changes {
                                update_cohort_status('pre_accepted')
                            }
                        end
                    end
                end
            end
        end

        describe "graduation_status update" do
            describe "with no existing application" do
                before(:each) do
                    @user = User.where.not(id: CohortApplication.pluck('user_id')).first
                    @cohort = Cohort.first
                end

                ['pending', 'graduated'].each do |status|
                    describe "and graduation_status=#{status}" do
                        it "should do nothing" do
                            expect(@user.cohort_applications.size).to be(0)
                        end
                    end
                end
            end

            describe "with an existing application" do

                before(:each) do
                    @cohort_application = CohortApplication.where(status: 'accepted').first
                    @user = @cohort_application.user
                    @cohort = cohort_application.cohort
                end
                describe "and graduation_status=pending" do
                    it "should update status on existing application" do
                        expect_application_graduation_status_update(cohort_application, 'pending') {
                            update_graduation_status('pending')
                        }
                    end
                end
                describe "and graduation_status=graduated" do
                    it "should update status on existing application" do
                        expect_application_graduation_status_update(cohort_application, 'graduated') {
                            update_graduation_status('graduated')
                        }
                    end
                end
                it "should error trying to mark a graduation status on non-accepted application" do
                    @cohort_application = CohortApplication.where(status: 'pending').first
                    @user = @cohort_application.user
                    @cohort = cohort_application.cohort
                    expect_error("Validation failed: Graduation status must be pending if status is not accepted (unless biz cert)") {
                        update_graduation_status('failed')
                    }
                end
            end
        end

        def expect_application_update(application, status)
            apps_for_user_and_cohort = CohortApplication.where(user_id: @user.id, cohort_id: cohort.id)
            expect {
                updater = yield
                expect(updater.errors).to be_empty
            }.not_to change { apps_for_user_and_cohort.count }
            expect(application.reload.status).to eq(status)
        end

        def expect_application_graduation_status_update(application, graduation_status)
            apps_for_user_and_cohort = CohortApplication.where(user_id: @user.id, cohort_id: cohort.id)
            expect {
                updater = yield
                expect(updater.errors).to be_empty
            }.not_to change { apps_for_user_and_cohort.count }
            expect(application.reload.graduation_status).to eq(graduation_status)
        end

        def expect_new_application(status)
            apps_for_user_and_cohort = CohortApplication.where(user_id: user.id, cohort_id: cohort.id)
            expect {
                updater = yield
                expect(updater.errors).to be_empty
            }.to change { apps_for_user_and_cohort.count }.from(apps_for_user_and_cohort.count).to(apps_for_user_and_cohort.count + 1)
            new_application = apps_for_user_and_cohort.reorder('created_at desc').first
            expect(new_application.status).to eq(status)
            return new_application
        end

        def expect_no_application_changes
            expect{
                updater = yield
                expect(updater.errors).to be_empty
            }.not_to change{ CohortApplication.where(user_id: user.id).map(&:updated_at) }
        end

        def expect_error(error)
            expect{
                updater = yield
                expect(updater.errors).to eq({user.id => [error]})
            }.not_to change{ CohortApplication.where(user_id: user.id).map(&:updated_at) }
        end

        def update_cohort_status(cohort_status)
            User::BatchUpdater.new(current_user, [user.id], {cohort_id: cohort.id, cohort_status: cohort_status}).exec
        end

        def update_graduation_status(graduation_status)
            User::BatchUpdater.new(current_user, [user.id], {cohort_id: cohort.id, graduation_status: graduation_status}).exec
        end
    end

end