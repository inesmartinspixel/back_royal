require 'spec_helper'

describe User::SubscriptionConcern do
    attr_reader :user

    include StripeHelper

    fixtures(:cohorts, :users)

    before(:each) do
        @user = users(:user_with_subscriptions_enabled)
    end

   describe "after_subscription_reconciled_with_stripe" do

        it "should update registered and stripe_plan_id on the application" do
            # update amounts, etc as indicated by stripe
            application = user.application_for_stripe_plan_id(default_plan.id)
            application.registered = false
            application.num_charged_payments = 1
            application.num_refunded_payments = 1
            application.total_num_required_stripe_payments = 12
            application.stripe_plans << {"id" => 'something_wrong'}
            application.stripe_plan_id = 'something_wrong'
            application.save!

            expect(user).to receive(:set_total_num_default_value_if_changing_plans).with(application, default_plan.id)

            create_subscription(user, default_plan.id, default_coupon.id)
            application.reload

            expect(application.registered).to be(true)
            expect(application.stripe_plan_id).to eq(default_plan.id)
        end

        describe "total_num_required_stripe_payments" do
            attr_reader :application

            before(:each) do
                @application = user.application_for_stripe_plan_id(default_plan.id)
                application.num_charged_payments = 3
                application.num_refunded_payments = 3
                application.total_num_required_stripe_payments = 12
                application.stripe_plan_id = default_plan.id
            end

            it "should raise if trying to change plans when all charges are not fully refunded" do
                application.num_refunded_payments = 1
                expect(application).not_to receive(:set_total_num_default_value)
                expect {
                    user.send(:set_total_num_default_value_if_changing_plans, application, 'new_plan')
                }.to raise_error("Unable to change plans without being fully refunded!")
            end

            it "should not reset total_num_required_stripe_payments if not changing plans" do
                expect(application).not_to receive(:set_total_num_default_value)
                user.send(:set_total_num_default_value_if_changing_plans, application, default_plan.id)
            end

            it "should reset total_num_required_stripe_payments if it is currently nil" do
                application.total_num_required_stripe_payments = nil
                expect(application).to receive(:set_total_num_default_value).with(default_plan.id)
                user.send(:set_total_num_default_value_if_changing_plans, application, default_plan.id)
            end

            it "should reset total_num_required_stripe_payments if changing plans" do
                expect(application).to receive(:set_total_num_default_value).with('new_plan')
                user.send(:set_total_num_default_value_if_changing_plans, application, 'new_plan')
            end
        end
    end

    describe "identify" do
        it "should reidentify user if subscription is created" do
            expect_any_instance_of(User).to receive(:identify)
            Subscription.create(stripe_plan_id: default_plan.id, stripe_product_id: 'some_stripe_product_id', user: user, stripe_subscription_id: 'abcd1234')
        end

        it "should reidentify user if subscription is updated" do
            subscription = Subscription.create(stripe_plan_id: default_plan.id, stripe_product_id: 'some_stripe_product_id', user: user, stripe_subscription_id: 'abcd1234')
            expect_any_instance_of(User).to receive(:identify)
            subscription.stripe_product_id = 'another_product'
            subscription.save!
        end

        it "should identify user if subscription is destroyed" do
            user.ensure_stripe_customer
            subscription = Subscription.create(stripe_plan_id: default_plan.id, stripe_product_id: 'some_stripe_product_id', user: user, stripe_subscription_id: 'abcd1234')
            expect_any_instance_of(User).to receive(:identify)

            # skip this since it might call identify in some
            # cases but not others if the appication ends up getting saved
            expect_any_instance_of(User).to receive(:before_subscription_destroy)
            subscription.destroy!
        end
    end

    describe "handle_past_due_changes" do

        it "should ensure_payment_grace_period on application and save it if payment_past_due?" do
            subscription = create_subscription(user, default_plan.id)
            application = user.send(:cohort_application_for_subscription, subscription)
            application.update_column(:payment_grace_period_end_at, nil)

            expect(application).to receive(:payment_past_due?).and_return(true)
            expect(application).to receive(:ensure_payment_grace_period).and_call_original
            expect(user).to receive(:cohort_application_for_subscription).with(subscription).and_return(application)
            user.send(:handle_past_due_changes, subscription)

            # What this timestamp actually gets set to is tested elsewhere
            expect(application.reload.payment_grace_period_end_at).not_to be_nil
        end

        it "should update locked_due_to_past_due_payment to false and unset payment_grace_period_end_at if payment is not past due" do
            subscription = create_subscription(user, default_plan.id)
            application = user.send(:cohort_application_for_subscription, subscription)

            # The application would never be locked_due_to_past_due_payment and also have a payment_grace_period_end_at set,
            # but for the purposes of this spec and verifying that these things get unset, we put the applicatoin in this state.
            application.update_columns(locked_due_to_past_due_payment: true, payment_grace_period_end_at: Time.now)

            expect(application).to receive(:payment_past_due?).and_return(false)
            expect(user).to receive(:cohort_application_for_subscription).with(subscription).and_return(application)
            user.send(:handle_past_due_changes, subscription)

            application.reload
            expect(application.locked_due_to_past_due_payment).to eq(false)
            expect(application.payment_grace_period_end_at).to be_nil
        end
    end

    describe "handle_create_subscription_request" do

        before(:each) do
            # the controller will ensure that this user has a valid card
            # before we get here
            create_customer_with_default_source(user)
        end

        it "should call create_for_owner and create an internal subscription" do
            application = user.application_for_stripe_plan_id(default_plan.id)

            # since we're mocking the subscription create, this will end up with an invalid application
            expect(application).to receive(:validate_some_other_stripe_details_before_commit)

            # since no coupon_id in meta, the last argumet is nil
            expect(user).to receive(:get_coupon_id_for_request)
                    .with(application,  default_plan.id, nil)
                    .and_return(default_coupon.id)

            # the coupon id returned by get_coupon_id_for_request is
            # passed into the create_for_owner call
            expect(Subscription).to receive(:create_for_owner)
                .with(owner: user, stripe_plan_id: default_plan.id, coupon_id: default_coupon.id, trial_end_ts: nil)
                .and_call_original

            subscription = user.handle_create_subscription_request({}, {}, default_plan.id)

            reloaded = User.find(user.id)
            application.reload
            expect(subscription).not_to be_nil
            expect(subscription).to eq(reloaded.primary_subscription)
            expect(application.reload.registered).to eq(true)
            expect(application.shareable_with_classmates).to be_falsey
        end

        it "should pass coupon from meta to get_coupon_id_for_request" do
            application = user.application_for_stripe_plan_id(default_plan.id)

            # in the wild, get_coupon_id_for_request would never return a different
            # coupon_id than what was passed in the meta, but the way the code is
            # written, if get_coupon_id_for_request was refactored such that it
            # could return a different coupon_id, then whatever is returned should
            # be passed to get_coupon_id_for_request
            expect(user).to receive(:get_coupon_id_for_request)
                    .with(application,  default_plan.id, 'some_coupon_id')
                    .and_return('selected_coupon_id')


            expect(Subscription).to receive(:create_for_owner)
                .with(owner: user, stripe_plan_id: default_plan.id, coupon_id: 'selected_coupon_id', trial_end_ts: nil)

            user.handle_create_subscription_request({}, {coupon_id: 'some_coupon_id'}, default_plan.id)
        end

        # see https://trello.com/c/SJ1wFoFF
        it "should correctly set registered_early" do
            application = user.application_for_stripe_plan_id(default_plan.id)
            expect(application.published_cohort.early_registration_deadline).to be > Time.now

            # since we're mocking the subscription create, this will end up with an invalid application
            expect(application).to receive(:validate_some_other_stripe_details_before_commit)

            # the coupon id returned by get_coupon_id_for_request is
            # passed into the create_for_owner call
            expect(Subscription).to receive(:create_for_owner)
                .and_call_original

            subscription = user.handle_create_subscription_request({}, {}, default_plan.id)

            reloaded = User.find(user.id)
            application.reload
            expect(application.reload.registered_early).to eq(true)
        end

        it "should not call create_for_owner or create a subscription with a full-scholarship user" do
            expect(Subscription).not_to receive(:create_for_owner)

            application = user.application_for_stripe_plan_id(default_plan.id)
            application.total_num_required_stripe_payments = nil
            application.stripe_plan_id = nil
            application.save!
            expect_any_instance_of(CohortApplication).to receive(:has_full_scholarship?).at_least(1).times.and_return(true)

            subscription = user.handle_create_subscription_request({}, {}, default_plan.id)
            application.reload

            expect(subscription).to be_nil
            expect(user.primary_subscription).to be_nil
            expect(application.stripe_plan_id).to eq(default_plan.id)
            expect(application.registered).to be(true)
            expect(application.total_num_required_stripe_payments).to eq(0)
        end

        it "should ensure registration is confirmed if no payment is required" do
            expect(Subscription).not_to receive(:create_for_owner)

            application = user.application_for_stripe_plan_id(default_plan.id)
            application.total_num_required_stripe_payments = 0
            application.registered = false
            application.stripe_plan_id = nil
            application.save!

            subscription = user.handle_create_subscription_request({}, {}, default_plan.id)
            application.reload

            expect(subscription).to be_nil
            expect(user.primary_subscription).to be_nil
            expect(application.stripe_plan_id).to eq(default_plan.id)
            expect(application.registered).to be(true)
            expect(application.total_num_required_stripe_payments).to eq(0)
        end

        it "should set shareable_with_classmates" do
            expect(Subscription).to receive(:create_for_owner)
                    .with(owner: user, stripe_plan_id: default_plan.id, coupon_id: nil, trial_end_ts: nil)

                application = user.application_for_stripe_plan_id(default_plan.id)

                user.handle_create_subscription_request({},
                    {
                        cohort_application: {
                            id: application.id,
                            shareable_with_classmates: true
                        }
                    },
                    default_plan.id)
                application.reload
                expect(application.shareable_with_classmates).to be(true)
        end

        describe "re-registration" do

            def create_previous_charge(charged_at)
                charge = Stripe::Charge.create(id: '12345', refunded: false, amount: 1000, currency: 'usd', created: charged_at.to_timestamp, customer: user.id)
                stub_invoices(user, [
                    { date: Time.at(0), total: 1000, id: 'paid_subscription_invoice', paid: true, charge: charge  }
                ])
                application = user.application_for_stripe_plan_id(default_plan.id)
                application.num_charged_payments = 1
                user.subscriptions.destroy_all
                application
            end

            it "should create a stripe subscription with a trial_period when prior payment was made < 1 interval ago" do

                charged_at = Time.now - 15.days
                trial_end = Time.at(charged_at.to_timestamp) + 1.month

                create_previous_charge(charged_at)

                expect(Subscription).to receive(:create_for_owner)
                    .with(owner: user, stripe_plan_id: default_plan.id, coupon_id: nil, trial_end_ts: trial_end.to_timestamp)
                    .and_call_original

                user.handle_create_subscription_request({}, {}, default_plan.id)
            end

            it "should create a stripe subscription without a trial_period when prior payment was made > 1 interval ago" do

                charged_at = Time.now - 45.days
                create_previous_charge(charged_at)

                expect(Subscription).to receive(:create_for_owner)
                    .with(owner: user, stripe_plan_id: default_plan.id, coupon_id: nil, trial_end_ts: nil)
                    .and_call_original

                user.handle_create_subscription_request({}, {}, default_plan.id)
            end


            it "should create a stripe subscription without a trial_period when all previous charges have been refunded" do
                charged_at = Time.now - 15.days

                application = create_previous_charge(charged_at)
                application.num_refunded_payments = application.num_charged_payments

                expect(Subscription).to receive(:create_for_owner)
                    .with(owner: user, stripe_plan_id: default_plan.id, coupon_id: nil, trial_end_ts: nil)
                    .and_call_original

                user.handle_create_subscription_request({}, {}, default_plan.id)
            end

        end
    end

    describe "get_coupon_id_for_request" do

        it "should work with scholarship levels and a scholarship" do
            cohort = cohorts(:published_emba_cohort)
            scholarship_level = cohort.scholarship_levels.detect { |sl| sl.to_s.match("discount_250")}
            application = CohortApplication.new(
                cohort_id: cohort.id,
                scholarship_level: scholarship_level,
                stripe_plans: [{id: default_plan.id, amount: default_plan.amount, frequency: 'monthly'}]
            )
            expect(user.send(:get_coupon_id_for_request, application, default_plan.id, nil)).to eq("discount_250")
        end

        it "should work with scholarship levels and no scholarship" do
            cohort = cohorts(:published_emba_cohort)
            scholarship_level = cohort.scholarship_levels.detect { |sl| sl.to_s.match("No Scholarship")}
            application = CohortApplication.new(
                cohort_id: cohort.id,
                scholarship_level: scholarship_level,
                stripe_plans: [{id: default_plan.id, amount: default_plan.amount, frequency: 'monthly'}]
            )
            expect(user.send(:get_coupon_id_for_request, application, default_plan.id, nil)).to be_nil
        end

        it "should work with valid coupon passed in" do
            cohort = Cohort.all_published.first
            application = CohortApplication.new(
                stripe_plans: [{id: default_plan.id, amount: default_plan.amount, frequency: 'monthly'}],
                cohort_id: cohort.id
            )
            expect(cohort.program_type_config).to receive(:supports_scholarship_levels?).and_return(false)
            expect(cohort.program_type_config).to receive(:valid_coupon_id?).with('some_coupon_id').and_return(true)
            expect(user.send(:get_coupon_id_for_request, application, default_plan.id, 'some_coupon_id')).to eq('some_coupon_id')
        end

        it "should render error with invalid coupon passed in" do
            cohort = Cohort.all_published.first
            application = CohortApplication.new(
                stripe_plans: [{id: default_plan.id, amount: default_plan.amount, frequency: 'monthly'}],
                cohort_id: cohort.id
            )
            expect(cohort.program_type_config).to receive(:supports_scholarship_levels?).and_return(false)
            expect(cohort.program_type_config).to receive(:valid_coupon_id?).with('some_coupon_id').and_return(false)
            expect {
                user.send(:get_coupon_id_for_request, application, default_plan.id, 'some_coupon_id')
            }.to raise_error(User::SubscriptionConcern::InvalidCoupon)
        end

        it "should work with no coupon" do
            cohort = Cohort.all_published.first
            application = CohortApplication.new(
                stripe_plans: [{id: default_plan.id, amount: default_plan.amount, frequency: 'monthly'}],
                cohort_id: cohort.id
            )
            expect(cohort.program_type_config).to receive(:supports_scholarship_levels?).and_return(false)
            expect(cohort.program_type_config).not_to receive(:valid_coupon_id?)
            expect(user.send(:get_coupon_id_for_request, application, default_plan.id, nil)).to be_nil
        end

    end

    describe "before_subscription_destroy" do
        attr_reader :subscription, :application
        before(:each) do
            @subscription = create_subscription(user, default_plan.id)
            @application = user.application_for_stripe_plan_id(subscription.stripe_plan_id)
            @application.total_num_required_stripe_payments = 12
            @application.registered = true
            @application.save!
        end

        it "should set registered to true if there are no required payments" do
            application.total_num_required_stripe_payments = 0
            application.save!
            expect(Subscription.find_by_id(subscription.id)).to be_nil # ensure that subscription was destroyed by the application save call
            expect(application.reload.registered).to be(true)
        end

        it "should set registered to false if there are required stripe payments left" do
            application.total_num_required_stripe_payments = 12
            application.save!
            subscription.destroy
            expect(application.reload.registered).to be(false)
        end

        it "should set the relevant_cohort_id in subscription metadata" do
            subscription.owner.before_subscription_destroy(subscription)
            expect(Subscription.find(subscription.id).stripe_subscription.metadata['relevant_cohort_id']).to eq(application.cohort_id)
        end
    end

    describe "record_invoice_payment" do
        attr_reader :subscription, :application
        before(:each) do
            @subscription = create_subscription(user, default_plan.id)
            @application = user.application_for_stripe_plan_id(subscription.stripe_plan_id)
            @application.total_num_required_stripe_payments = 12
            @application.save!
        end

        it "should log an event and set num_charged_payments when the first payment is made" do
            mock_event = "mock_event"
            expect(mock_event).to receive(:log_to_external_systems).with(no_args)

            projects = [LearnerProject.first, LearnerProject.second]
            allow(application.published_cohort.program_type_config).to receive(:supports_cohort_level_projects?).and_return(true)
            allow(application.published_cohort).to receive(:cohort_level_learner_projects).and_return(projects)

            expect(Event).to receive(:create_server_event!).with(anything, user.id, 'cohort:first_payment_succeeded', {
                is_paid_cert: application.program_type_config.is_paid_cert?,
                cohort_title: application.published_cohort.title,
                learner_projects: projects.as_json
            }).and_return(mock_event)

            subscription.record_invoice_payment(:mock_invoice)
            application.reload
            expect(application.num_charged_payments).to eq(1)
            expect(application.num_refunded_payments).to eq(0)
        end

        it "should set the num_charged_payments when a subsequent payment is made" do
            subscription.record_invoice_payment(:mock_invoice) # record 1st payment
            expect(Event).not_to receive(:create_server_event!)

            subscription.record_invoice_payment(:mock_invoice) # record 2nd payment
            application.reload
            expect(application.num_charged_payments).to eq(2)
            expect(application.num_refunded_payments).to eq(0)
        end

        it "should set the total_num_required_stripe_payments to 0 if all payments have been made, triggering a destroy" do
            application.num_charged_payments = 14
            application.num_refunded_payments = 3
            application.save!

            subscription.record_invoice_payment(:mock_invoice)
            application.reload
            expect(Subscription.find_by_id(subscription.id)).to be_nil
        end
    end

    describe "all_payments_complete?" do
        it "should be true if all payments have been made" do
            create_subscription(user, default_plan.id)
            subscription = user.primary_subscription
            # subscription.user is not the same object as user.  I tried for a long time
            # messing with inverse_of_to make this work, but it seems like it can't work
            # in this situation
            application = subscription.user.application_for_stripe_plan_id(default_plan.id)
            application.num_charged_payments = 13
            application.num_refunded_payments = 1
            application.total_num_required_stripe_payments = 12
            expect(subscription.all_payments_complete?).to eq(true)
        end

        it "should be false if total_num_required_stripe_payments != 0" do
            create_subscription(user, default_plan.id)
            subscription = user.primary_subscription
            application = user.application_for_stripe_plan_id(default_plan.id)
            application.num_charged_payments = 13
            application.num_refunded_payments = 2
            application.total_num_required_stripe_payments = 12
            expect(subscription.all_payments_complete?).to eq(false)
        end
    end

    describe "subscription_identifier_for_slack" do

        it "should handle a subscription" do
            subscription = create_subscription(user, default_plan.id)
            application = user.application_for_stripe_plan_id(default_plan.id)
            identifier = user.subscription_identifier_for_slack(subscription, nil)
            expect(identifier).to eq(application.published_cohort.name)
        end

        it "should handle a stripe_subscription with relevant_cohort_id metadata" do
            subscription = create_subscription(user, default_plan.id)
            stripe_subscription = subscription.stripe_subscription
            cohort = Cohort.first
            stripe_subscription.metadata['relevant_cohort_id'] = cohort.id
            identifier = user.subscription_identifier_for_slack(nil, stripe_subscription)
            expect(identifier).to eq(cohort.name)
        end

        it "should handle nil subscription and stripe_subscriptions" do
            identifier = user.subscription_identifier_for_slack(nil, nil)
            expect(identifier).to eq("UNKNOWN")
        end

    end

    describe "record_refund" do

        it "should handle increment num_refunded_payments" do
            create_subscription(user, default_plan.id)
            application = user.application_for_stripe_plan_id(default_plan.id)
            application.num_charged_payments = 1
            application.save
            expect(application.num_refunded_payments).to be_nil
            user.record_refund(default_plan.id)
            application.reload
            expect(application.num_refunded_payments).to eq(1)
        end

        describe "with cancelled subscription" do

            attr_reader :application

            before(:each) do
                subscription = create_subscription(user, default_plan.id)
                @application = user.application_for_stripe_plan_id(default_plan.id)
                application.registered = true
                application.locked_due_to_past_due_payment = false
                application.total_num_required_stripe_payments = 1
                application.save!
                allow(user).to receive(:primary_subscription).and_return(nil)
            end

            describe "with no outside charges" do

                it "should unregister, lock, and reset payments" do
                    user.record_refund(default_plan.id)
                    application.reload
                    expect(application.registered).to be(false)
                    expect(application.locked_due_to_past_due_payment).to be(true)
                    expect(application.total_num_required_stripe_payments).to eq(12)
                end

                it "should log to slack" do
                    expect(LogToSlack).to receive(:perform_later).with(
                        Slack.cohort_payments_channel,
                        nil,
                        [
                            {
                                color: '#FF9300',
                                text: "*#{user.email}* has been *unregistered* and *locked_due_to_past_due_payment* after issuing refund. :warning:",
                                mrkdwn_in: ['text']
                            }
                        ]
                    )
                    user.record_refund(default_plan.id)
                end

            end

            it "should not unregister, lock, and reset payments if outside charges present" do
                BillingTransaction.create_from_hash!(user, {
                    transaction_time: Time.parse('2017-06-09').to_timestamp,
                    transaction_type: 'payment',
                    provider: BillingTransaction::PROVIDER_PAYPAL,
                    provider_transaction_id: '875676453',
                    amount: 3600,
                    amount_refunded: 0,
                    description: 'description',
                    currency: 'usd'
                })

                expect(application).not_to receive(:registered=)
                expect(application).not_to receive(:locked_due_to_past_due_payment=)
                expect(application).not_to receive(:total_num_required_stripe_payments=)
                user.record_refund(default_plan.id)
                application.reload
                expect(application.registered).to be(true)
                expect(application.locked_due_to_past_due_payment).to be(false)
                expect(application.total_num_required_stripe_payments).to eq(1)
            end

        end

    end

    describe "raise_if_duplicate_subscription!" do
        it "should always call raise_if_duplicate_primary_subscription!" do
            user = User.first
            expect(user).to receive(:raise_if_duplicate_primary_subscription!)
            user.raise_if_duplicate_subscription!
        end
    end
end