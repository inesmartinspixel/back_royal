# == Schema Information
#
# Table name: work_experiences
#
#  id                                  :uuid             not null, primary key
#  created_at                          :datetime
#  updated_at                          :datetime
#  job_title                           :text             not null
#  start_date                          :date             not null
#  end_date                            :date
#  responsibilities                    :text             default([]), not null, is an Array
#  career_profile_id                   :uuid             not null
#  professional_organization_option_id :uuid             not null
#  featured                            :boolean          default(FALSE), not null
#  role                                :text             default("other")
#  industry                            :text             default("other")
#  employment_type                     :text             default("full_time"), not null
#

require 'spec_helper'

describe CareerProfile::WorkExperience do

    it "should set length string correctly" do
        work_experience = CareerProfile::WorkExperience.new({
            start_date: Time.now,
            end_date: Time.now + 5.years + 20.days
        })

        expect(work_experience.length_string).to eq("about 5 years")
    end

    it "should set length string to current if end_date.nil?" do
        work_experience = CareerProfile::WorkExperience.new({
            start_date: Time.now - 5.years,
            end_date: nil
        })

        expect(work_experience.length_string).to eq("current")
    end

    describe "should_update_career_profile_fulltext?" do
        it "should be based on the saved attributes" do
            work_experience = CareerProfile::WorkExperience.first
            target_attributes = [
                :professional_organization_option_id,
                :job_title,
                :industry,
                :responsibilities
            ]

            CareerProfile::WorkExperience.new.attributes.keys.map(&:to_sym).each do |attribute|
                is_target_attr = target_attributes.include?(attribute)
                allow(work_experience).to receive(:saved_changes) { Hash[attribute, true] }
                expect(work_experience.should_update_career_profile_fulltext?).to be(is_target_attr)
            end
        end
    end

    describe "after_save" do
        it "should call update_fulltext on the career_profile if should_update_career_profile_fulltext?" do
            work_experience = CareerProfile::WorkExperience.first
            allow(work_experience).to receive(:should_update_career_profile_fulltext?) { true }
            expect(work_experience.career_profile).to receive(:update_fulltext)
            work_experience.update(job_title: 'changed')
        end

        it "should not call update_fulltext if not should_update_career_profile_fulltext?" do
            work_experience = CareerProfile::WorkExperience.first
            allow(work_experience).to receive(:should_update_career_profile_fulltext?) { false }
            expect(work_experience.career_profile).not_to receive(:update_fulltext)
            work_experience.update(job_title: 'changed')
        end
    end

    describe "after_destroy" do
        it "should call update_fulltext on the career_profile" do
            work_experience = CareerProfile::WorkExperience.first
            expect(work_experience.career_profile).to receive(:update_fulltext)
            work_experience.destroy!
        end
    end

end
