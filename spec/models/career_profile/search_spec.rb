# == Schema Information
#
# Table name: career_profile_searches
#
#  id                                  :uuid             not null, primary key
#  created_at                          :datetime         not null
#  updated_at                          :datetime         not null
#  user_id                             :uuid             not null
#  only_local                          :boolean          default(FALSE), not null
#  years_experience                    :text             default([]), not null, is an Array
#  last_search_at                      :datetime
#  search_count                        :integer
#  keyword_search                      :text
#  preferred_primary_areas_of_interest :text             default([]), not null, is an Array
#  industries                          :text             default([]), not null, is an Array
#  skills                              :text             default([]), not null, is an Array
#  employment_types_of_interest        :text             default([]), not null, is an Array
#  school_name                         :text
#  in_school                           :boolean
#  company_name                        :text
#  levels_of_interest                  :text             default([]), not null, is an Array
#  places                              :json             not null, is an Array
#

require 'spec_helper'

describe CareerProfile::Search do

    describe "save_search" do

        attr_reader :filters, :user, :now_i

        before(:each) do
            @user = User.where.not(id: CareerProfile::Search.distinct.pluck(:user_id)).first
            @places = {
                miami: ActiveSupport::JSON.decode("{\"lat\":25.7617,\"lng\":-80.1918,\"place_details\":{\"locality\":{\"short\":\"Miami\",\"long\":\"Miami\"},\"administrative_area_level_2\":{\"short\":\"Miami-Dade County\",\"long\":\"Miami-Dade County\"},\"administrative_area_level_1\":{\"short\":\"FL\",\"long\":\"Florida\"},\"country\":{\"short\":\"US\",\"long\":\"United States\"},\"formatted_address\":\"Miami, FL, USA\",\"utc_offset\":-240,\"lat\":25.7616798,\"lng\":-80.1917902}}"),
                houston: ActiveSupport::JSON.decode("{\"lat\":29.7604,\"lng\":-95.3698,\"place_details\":{\"locality\":{\"short\":\"Houston\",\"long\":\"Houston\"},\"administrative_area_level_2\":{\"short\":\"Harris County\",\"long\":\"Harris County\"},\"administrative_area_level_1\":{\"short\":\"TX\",\"long\":\"Texas\"},\"country\":{\"short\":\"US\",\"long\":\"United States\"},\"formatted_address\":\"Houston, TX, USA\",\"utc_offset\":-300,\"lat\":29.7604267,\"lng\":-95.3698028}}"),
                boston: ActiveSupport::JSON.decode("{\"lat\":42.3601,\"lng\":-71.0589,\"place_details\":{\"locality\":{\"short\":\"Boston\",\"long\":\"Boston\"},\"administrative_area_level_2\":{\"short\":\"Suffolk County\",\"long\":\"Suffolk County\"},\"administrative_area_level_1\":{\"short\":\"MA\",\"long\":\"Massachusetts\"},\"country\":{\"short\":\"US\",\"long\":\"United States\"},\"formatted_address\":\"Boston, MA, USA\",\"utc_offset\":-240,\"lat\":42.3600825,\"lng\":-71.0588801}}")
            }
            @filters = {
                places: [@places[:miami], @places[:houston]],
                only_local: true,
                # preferred_primary_areas_of_interest: ['a', 'b'], # deprecated
                roles: {
                    preferred_primary_areas_of_interest: ['a', 'b']
                },
                industries: ['a', 'b'],
                years_experience: ['a', 'b'],
                skills: ['a', 'b'],
                employment_types_of_interest: ['a', 'b'],
                school_name: 'Haaaaaarvard',
                in_school: true,
                company_name: 'Acme'
            }
            @now_i = 1
            assert_new_search(filters)
        end

        it "should create a new search if places is different" do
            assert_new_search(filters.merge({
                places: [@places[:miami]]
            }))

            assert_new_search(filters.merge({
                places: [@places[:houston], @places['boston']]
            }))

            assert_new_search(filters.merge({
                places: [@places[:miami], @places[:houston], @places['boston']]
            }))
        end

        it "should create a new search if only_local is different" do
            assert_new_search(filters.merge({
                only_local: !filters[:only_local]
            }))
        end

        it "should create a new search if preferred_primary_areas_of_interest is different" do
            assert_new_search(filters.merge({
                roles: {preferred_primary_areas_of_interest: ['a']}
            }))

            assert_new_search(filters.merge({
                roles: {preferred_primary_areas_of_interest: ['b', 'c']}
            }))

            assert_new_search(filters.merge({
                roles: {preferred_primary_areas_of_interest: ['a', 'b', 'c']}
            }))
        end

        it "should create a new search if industries is different" do
            assert_new_search(filters.merge({
                industries: ['a']
            }))

            assert_new_search(filters.merge({
                industries: ['b', 'c']
            }))

            assert_new_search(filters.merge({
                industries: ['a', 'b', 'c']
            }))
        end

        it "should create a new search if years_experience is different" do
            assert_new_search(filters.merge({
                years_experience: ['a']
            }))

            assert_new_search(filters.merge({
                years_experience: ['b', 'c']
            }))

            assert_new_search(filters.merge({
                years_experience: ['a', 'b', 'c']
            }))
        end

        it "should create a new search if skills array is different" do
            assert_new_search(filters.merge({
                skills: ['a']
            }))

            assert_new_search(filters.merge({
                skills: ['b', 'c']
            }))

            assert_new_search(filters.merge({
                skills: ['a', 'b', 'c']
            }))
        end

        it "should create a new search if employment_types_of_interest is different" do
            assert_new_search(filters.merge({
                employment_types_of_interest: ['a']
            }))

            assert_new_search(filters.merge({
                employment_types_of_interest: ['b', 'c']
            }))

            assert_new_search(filters.merge({
                employment_types_of_interest: ['a', 'b', 'c']
            }))
        end

        it "should create a new search if in_school is different" do
            assert_new_search(filters.merge({
                in_school: !filters[:in_school]
            }))
        end

        it "should create a new search if keyword_search is different" do
            assert_new_search(filters.merge({
                keyword_search: 'changed'
            }))
        end

        it "should create a new search if company_name is different" do
            assert_new_search(filters.merge({
                company_name: 'changed'
            }))
        end

        it "should create a new search if school_name is different" do
            assert_new_search(filters.merge({
                school_name: 'changed'
            }))
        end

        it "should find an existing record if everything is the same" do
            assert_incremented_existing_search(filters)
        end

        it "should find an existing record if just the orders are different" do
            assert_incremented_existing_search(filters.merge(
                places: filters[:places].reverse,
                only_local: true,
                :roles => {
                    :preferred_primary_areas_of_interest => filters[:roles][:preferred_primary_areas_of_interest].reverse,
                },
                years_experience: filters[:years_experience].reverse,
                skills: filters[:skills].reverse
            ))
        end

        it "should find an existing record if some of the filters are empty" do
            CareerProfile::Search.where(user_id: user.id).first.update_attribute('places', [])
            assert_incremented_existing_search(filters.merge(
                places: []
            ))
        end

        it "should find an existing record if all of the filters are empty (default search)" do
            CareerProfile::Search.where(user_id: user.id).first.update({
                :places => [],
                :only_local => false,
                :preferred_primary_areas_of_interest => [],
                :years_experience => [],
                :skills => [],
                :keyword_search => nil
            })

            assert_incremented_existing_search(filters.merge(
                places: [],
                only_local: false,
                :roles => {
                    :preferred_primary_areas_of_interest => []
                },
                years_experience: [],
                skills: [],
                keyword_search: nil
            ))
        end

        def assert_new_search(filters)
            allow(Time).to receive(:now).and_return(Time.at(@now_i = @now_i + 1))

            expect {
                search = CareerProfile::Search.save_search(user.id, filters)
                expect(search.search_count).to eq(1)
                expect(search.last_search_at).to eq(Time.at(now_i))
                expect(search.user_id).to eq(user.id)
                expect(search.places).to match_array(filters[:places])
                expect(search.only_local).to eq(filters[:only_local])

                preferred_primary_areas_of_interest = (filters[:roles] && filters[:roles][:preferred_primary_areas_of_interest]) || filters[:preferred_primary_areas_of_interest]
                expect(search.preferred_primary_areas_of_interest).to match_array(preferred_primary_areas_of_interest)
                expect(search.years_experience).to match_array(filters[:years_experience])
                expect(search.skills).to match_array(filters[:skills])
            }.to change { CareerProfile::Search.count}.by(1)
        end

        def assert_incremented_existing_search(filters)
            now = Time.now
            allow(Time).to receive(:now).and_return(now)
            expect {
                search = CareerProfile::Search.save_search(user.id, filters)
                expect(search.search_count).to eq(2)
                expect(search.last_search_at).to eq(now)
                expect(search.user_id).to eq(user.id)
                expect(search.places).to match_array(filters[:places])
                expect(search.only_local).to eq(filters[:only_local])

                preferred_primary_areas_of_interest = (filters[:roles] && filters[:roles][:preferred_primary_areas_of_interest]) || filters[:preferred_primary_areas_of_interest]
                expect(search.preferred_primary_areas_of_interest).to match_array(preferred_primary_areas_of_interest)
                expect(search.years_experience).to match_array(filters[:years_experience])
                expect(search.skills).to match_array(filters[:skills])
            }.not_to change { CareerProfile::Search.count}
        end

    end

end
