require 'spec_helper'

describe CareerProfile::TranscriptRequirementHelper do

    attr_reader :instance, :career_profile, :education_experiences, :educational_organizations

    before(:each) do
        @instance = CareerProfile::TranscriptRequirementHelper
        @career_profile = CareerProfile.first
        @education_experiences = CareerProfile::EducationExperience.order(:id).limit(4)
        @educational_organizations = CareerProfile::EducationalOrganizationOption.order(:id).limit(4)
        allow(@career_profile).to receive(:education_experiences_indicating_transcript_required).and_return(@education_experiences)
    end

    describe "determine_transcript_requirements" do

        attr_accessor :education_experiences, :educational_organizations

        before(:each) do
            @education_experiences.first.update_columns(degree: 'doctor_of_education', graduation_year: 2019, educational_organization_option_id: @educational_organizations.first.id)
            @education_experiences.second.update_columns(degree: 'master_of_advanced_studies', graduation_year: 2016, educational_organization_option_id: @educational_organizations.second.id)
            @education_experiences.third.update_columns(degree: 'bachelor_of_architecture', graduation_year: 2013, educational_organization_option_id: @educational_organizations.third.id)
            @education_experiences.fourth.update_columns(degree: 'associate_of_applied_arts', graduation_year: 2010, educational_organization_option_id: @educational_organizations.fourth.id)
        end

        it "should require official transcript for education with an override if present" do
            @education_experiences.third.update_columns(official_transcript_required_override: true)
            instance.determine_transcript_requirements(career_profile)
            assert_official_transcript_required_for_experience(education_experiences.third.id)
        end

        it "should require transcripts for highest level, most recent degree" do
            instance.determine_transcript_requirements(career_profile)
            assert_official_transcript_required_for_experience(education_experiences.first.id)
        end

        it "should require transcripts for highest level, most recent, non-blacklisted degree" do
            educational_organizations.first.update_column(:degree_blacklist, true)

            instance.determine_transcript_requirements(career_profile)
            assert_official_transcript_required_for_experience(education_experiences.second.id)
        end

        it "should require transcripts for highest level, most recent degree if all degrees are blacklisted" do
            educational_organizations.first.update_column(:degree_blacklist, true)
            educational_organizations.second.update_column(:degree_blacklist, true)
            educational_organizations.third.update_column(:degree_blacklist, true)
            educational_organizations.fourth.update_column(:degree_blacklist, true)

            instance.determine_transcript_requirements(career_profile)
            assert_official_transcript_required_for_experience(education_experiences.first.id)
        end

        describe "unrecognized degrees" do

            it "should treat unrecognized degrees the same as the highest known degree level" do
                education_experiences.first.update_column(:degree, 'foo_degree')

                instance.determine_transcript_requirements(career_profile)
                assert_official_transcript_required_for_experience(education_experiences.first.id)
            end

            it "should require transcripts for the most recent unrecognized degree" do
                education_experiences.first.update_columns(degree: 'foo_degree', graduation_year: 2019)
                education_experiences.second.update_columns(degree: 'bar_degree', graduation_year: 2020)

                instance.determine_transcript_requirements(career_profile)
                assert_official_transcript_required_for_experience(education_experiences.second.id)
            end

            it "should still work if all degrees are unrecognized" do
                education_experiences.each_with_index do |e, i|
                    e.update_column(:degree, "#{i}_degree")
                end

                instance.determine_transcript_requirements(career_profile)
                assert_official_transcript_required_for_experience(education_experiences.first.id)
            end

        end

        describe "with passed in experience" do

            attr_accessor :experience

            before(:each) do
                @experience = education_experiences.first
            end

            it "should not call \'update\' on passed in experience" do
                allow(experience).to receive(:transcript_required?).and_return(true)
                expect(experience).not_to receive(:update)
                expect(experience).to receive(:official_transcript_required=).exactly(1).times
                instance.determine_transcript_requirements(career_profile, experience)
            end
    
            it "should work when passed in experience is only experience that requires transcripts" do
                allow(career_profile).to receive(:education_experiences_indicating_transcript_required).and_return([experience])

                allow(experience).to receive(:transcript_required?).and_return(true)
                expect(experience).not_to receive(:update)
                expect(experience).to receive(:official_transcript_required=).exactly(1).times
                instance.determine_transcript_requirements(career_profile, experience)
            end

            it "should work when passed in experience has not yet been saved" do
                experience = CareerProfile::EducationExperience.new
                experience.educational_organization = educational_organizations.first
                allow(career_profile).to receive(:education_experiences_indicating_transcript_required).and_return([experience])

                allow(experience).to receive(:transcript_required?).and_return(true)
                expect(experience).not_to receive(:update)
                expect(experience).to receive(:official_transcript_required=).exactly(1).times
                instance.determine_transcript_requirements(career_profile, experience)
            end
        
        end

        it "should do nothing when no experiences require transcripts" do
            allow(career_profile).to receive(:education_experiences_indicating_transcript_required).and_return([])
            
            experience = education_experiences.first
            allow(experience).to receive(:transcript_required?).and_return(false)

            expect_any_instance_of(CareerProfile::EducationExperience).not_to receive(:update)
            expect_any_instance_of(CareerProfile::EducationExperience).not_to receive(:official_transcript_required=)
            expect {
                instance.determine_transcript_requirements(career_profile, experience)
            }.not_to raise_error
        end

    end

    def assert_official_transcript_required_for_experience(id)
        education_experiences.each do |experience|
            expect(experience.reload.official_transcript_required).to be(id == experience.id ? true : false)
        end
    end

end