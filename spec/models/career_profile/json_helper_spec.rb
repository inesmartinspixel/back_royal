require 'spec_helper'

describe CareerProfile::JsonHelper do

    fixtures(:users)

    describe "specific fields" do
        it "should not blow up if a work_experience has no end_date" do

            career_profile = CareerProfile.first
            career_profile.work_experiences = []
            career_profile.save!
            work_experiences = CareerProfile::WorkExperience.limit(5)
            work_experiences.each do |work_experience|
                work_experience.update({'end_date': Time.now, career_profile_id: career_profile.id})
            end

            work_experience_with_no_end_date = work_experiences[2]
            work_experience_with_no_end_date.update({'end_date': nil, career_profile_id: career_profile.id})
            expect(CareerProfile.find(career_profile.id).as_json(view: 'editable')['work_experiences'][0]['id']).to eq(work_experience_with_no_end_date.id)

        end

        it "should include cohort name" do
            career_profile = CohortApplication.where.not(accepted_at: nil).first.user.career_profile
            expect(career_profile.as_json(view: 'editable')["candidate_cohort_name"]).not_to eq(nil)
        end

        it "should properly serialize dates to timestamps" do
            career_profile = CareerProfile.first
            career_profile.update_column(:feedback_last_sent_at, Time.now)
            json = career_profile.as_json(view: 'editable')
            expect(json['created_at']).to eq(career_profile.created_at.to_timestamp)
            expect(json['updated_at']).to eq(career_profile.updated_at.to_timestamp)
            expect(json['feedback_last_sent_at']).to eq(career_profile.feedback_last_sent_at.to_timestamp)
            expect(json['last_confirmed_at_by_student']).to eq(career_profile.last_confirmed_at_by_student.to_timestamp)
            expect(json['last_confirmed_at_by_internal']).to eq(career_profile.last_confirmed_at_by_internal.to_timestamp)
            expect(json['last_confirmed_internally_by']).to eq(career_profile.last_confirmed_internally_by.to_timestamp)
        end
    end

    describe "as_json_for_student_network" do

        attr_accessor :accepted_user, :career_profile, :student_network_restricted_json_fields, :student_network_unrestricted_json_fields

        before(:each) do
            @accepted_user = User.joins(:cohort_applications => :cohort).
                where("cohorts.program_type" => 'mba',
                    "cohort_applications.status" => 'accepted',
                    "users.can_edit_career_profile" => true).first

            @career_profile = @accepted_user.career_profile
            ensure_work_experiences
            @accepted_user.reload

            @accepted_user.name = @accepted_user.nickname = "Paddington Bear"

            @student_network_restricted_json_fields = *%w|
                avatar_url
                personal_website_url
                blog_url
                tw_profile_url
                li_profile_url
                github_profile_url
            |

            @student_network_unrestricted_json_fields = *%w|
                id
                user_id
                personal_fact
                place_id
                place_details
                work_experiences
                education_experiences
                created_at
                updated_at
                name
                nickname
                city
                state
                country
                target_graduation
                candidate_cohort_name
                candidate_cohort_title
                student_network_interests
                student_network_looking_for
                pref_student_network_privacy
                anonymized
            |
        end

        # these are annoying specs to keep up to date, but again I wanted to be super
        # explicit so we don't ever accidentally leak private information to strangers in the network
        it "should include restricted fields if anonymize=false" do
            allow(accepted_user).to receive(:pref_student_network_privacy).and_return('full')
            json = career_profile.as_json_for_student_network({anonymize: false})
            expect(json.keys).to match_array(student_network_unrestricted_json_fields.push(*student_network_restricted_json_fields))
            expect(json["name"]).to eq(accepted_user.name)
            expect(json["nickname"]).to eq(accepted_user.nickname)
            expect(json["anonymized"]).to be(false)
            expect(json["work_experiences"].size).to be > 1
        end

        it "should remove restricted fields if anonymize != false" do
            json = career_profile.as_json_for_student_network
            expect(json.keys).not_to include(student_network_restricted_json_fields)
            expect(json.keys).to match_array(student_network_unrestricted_json_fields)
            expect(json["name"]).to eq("P. B.")
            expect(json["nickname"]).to eq("P. B.")
            expect(json["anonymized"]).to be(true)
            expect(json["work_experiences"].size).to eq(1)
        end


        it "should remove restricted fields if user.pref_student_network_privacy is not \'full\'" do
            allow(accepted_user).to receive(:pref_student_network_privacy).and_return('not_full')
            json = career_profile.as_json_for_student_network({anonymize: true})
            expect(json.keys).not_to include(student_network_restricted_json_fields)
            expect(json.keys).to match_array(student_network_unrestricted_json_fields)
            expect(json["name"]).to eq("P. B.")
            expect(json["nickname"]).to eq("P. B.")
            expect(json["anonymized"]).to be(true)
            expect(json["work_experiences"].size).to eq(1)
        end

    end

    describe "as_json_for_career_network" do

        attr_accessor :accepted_user, :career_profile, :career_network_restricted_json_fields, :career_network_unrestricted_json_fields

        before(:each) do
            @accepted_user = User.joins(:cohort_applications => :cohort).
                where("cohorts.program_type" => 'mba',
                    "cohort_applications.status" => 'accepted',
                    "users.can_edit_career_profile" => true).first

            @career_profile = @accepted_user.career_profile
            ensure_work_experiences
            @accepted_user.reload

            @accepted_user.name = @accepted_user.nickname = "Paddington Bear"

            @career_network_restricted_json_fields = *%w|
                avatar_url
                personal_website_url
                blog_url
                tw_profile_url
                li_profile_url
                github_profile_url
                resume
            |

            @career_network_unrestricted_json_fields = *%w|
                id
                user_id
                personal_fact
                place_id
                place_details
                work_experiences
                education_experiences
                created_at
                updated_at
                name
                nickname
                city
                state
                country
                target_graduation
                candidate_cohort_name
                candidate_cohort_title
                pref_student_network_privacy
                authorized_to_work_in_us
                awards_and_interests
                bio
                company_sizes_of_interest
                primary_areas_of_interest
                salary_range
                interested_in_joining_new_company
                locations_of_interest
                skills
                sat_max_score
                score_on_gre_verbal
                score_on_gre_quantitative
                score_on_gre_analytical
                top_motivations
                top_personal_descriptors
                top_workplace_strengths
                willing_to_relocate
                score_on_gmat
                score_on_sat
                score_on_act
                open_to_remote_work
                top_mba_subjects
                preferred_company_culture_descriptors
                job_sectors_of_interest
                employment_types_of_interest
                anonymized
            |


        end

        # these are annoying specs to keep up to date, but again I wanted to be super
        # explicit so we don't ever accidentally leak private information to strangers in the network
        it "should include restricted fields if anonymize=false" do
            json = career_profile.as_json_for_career_network({anonymize: false})
            expect(json.keys).to match_array(career_network_unrestricted_json_fields.push(*career_network_restricted_json_fields))
            expect(json["name"]).to eq(accepted_user.name)
            expect(json["nickname"]).to eq(accepted_user.nickname)
            expect(json["anonymized"]).to eq(false)
            expect(json["work_experiences"].size).to be > 1
        end

        it "should remove restricted fields if anonymize != false" do
            json = career_profile.as_json_for_career_network
            expect(json.keys).not_to include(career_network_restricted_json_fields)
            expect(json.keys).to match_array(career_network_unrestricted_json_fields)
            expect(json["name"]).to eq("P. B.")
            expect(json["nickname"]).to eq("P. B.")
            expect(json["anonymized"]).to eq(true)
            expect(json["work_experiences"].size).to be > 1
        end

    end

    describe "as_json_for_edit" do

        attr_accessor :accepted_user, :career_profile, :career_network_editable_json_fields

        before(:each) do
            @accepted_user = User.joins(:cohort_applications => :cohort).
                where("cohorts.program_type" => 'mba',
                    "cohort_applications.status" => 'accepted',
                    "users.can_edit_career_profile" => true).first

            @career_profile = @accepted_user.career_profile
            ensure_work_experiences
            @accepted_user.reload

            @accepted_user.name = @accepted_user.nickname = "Paddington Bear"

            @career_network_editable_json_fields = *%w|
                avatar_url
                personal_website_url
                blog_url
                tw_profile_url
                li_profile_url
                github_profile_url
                resume
                id
                user_id
                personal_fact
                place_id
                place_details
                work_experiences
                education_experiences
                created_at
                updated_at
                name
                nickname
                city
                state
                country
                program_type
                target_graduation
                candidate_cohort_name
                candidate_cohort_title
                pref_student_network_privacy
                authorized_to_work_in_us
                awards_and_interests
                bio
                company_sizes_of_interest
                primary_areas_of_interest
                salary_range
                interested_in_joining_new_company
                locations_of_interest
                skills
                sat_max_score
                score_on_gre_verbal
                score_on_gre_quantitative
                score_on_gre_analytical
                top_motivations
                top_personal_descriptors
                top_workplace_strengths
                willing_to_relocate
                score_on_gmat
                score_on_sat
                score_on_act
                open_to_remote_work
                top_mba_subjects
                preferred_company_culture_descriptors
                job_sectors_of_interest
                employment_types_of_interest
                peer_recommendations

                native_english_speaker
                earned_accredited_degree_in_english
                sufficient_english_work_experience
                english_work_experience_description

                survey_highest_level_completed_education_description
                survey_years_full_time_experience
                survey_most_recent_role_description

                short_answers
                fb_profile_url
                last_calculated_complete_percentage
                short_answer_greatest_achievement
                short_answer_why_pursuing
                short_answer_use_skills_to_advance
                short_answer_ask_professional_advice
                has_no_formal_education
                primary_reason_for_applying
                do_not_create_relationships
                short_answer_leadership_challenge
                profile_feedback
                feedback_last_sent_at
                long_term_goal
                last_confirmed_at_by_student
                last_confirmed_at_by_internal
                last_updated_at_by_student
                last_confirmed_internally_by
                short_answer_scholarship
                consider_early_decision
                phone
                sex
                ethnicity
                race
                how_did_you_hear_about_us
                address_line_1
                address_line_2
                zip
                birthdate
                anything_else_to_tell_us
                student_network_email
                resume_id
                student_network_interests
                student_network_looking_for

                cohort_slack_room_id
                cohort_status
                last_application_id
                has_full_scholarship
            |
        end

        # these are annoying specs to keep up to date, but again I wanted to be super
        # explicit so we don't ever accidentally leak private information to strangers in the network
        it "should include expected fields'" do
            json = career_profile.as_json_for_edit({})
            expect(json.keys).to match_array(career_network_editable_json_fields)
            expect(json["name"]).to eq(accepted_user.name)
            expect(json["nickname"]).to eq(accepted_user.nickname)
            expect(json["work_experiences"].size).to be > 1
        end
    end

    def ensure_work_experiences
        return if career_profile.work_experiences&.size > 1
        experiences = CareerProfile::WorkExperience.limit(2)
        experiences.update_all(career_profile_id: @career_profile.id)
    end
end
