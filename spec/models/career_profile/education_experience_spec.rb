# == Schema Information
#
# Table name: education_experiences
#
#  id                                    :uuid             not null, primary key
#  created_at                            :datetime
#  updated_at                            :datetime
#  graduation_year                       :integer          not null
#  degree                                :text
#  major                                 :text             not null
#  minor                                 :text
#  gpa                                   :text
#  career_profile_id                     :uuid             not null
#  educational_organization_option_id    :uuid             not null
#  gpa_description                       :text
#  degree_program                        :boolean          default(TRUE), not null
#  will_not_complete                     :boolean
#  transcript_waiver                     :text
#  transcript_approved                   :boolean          default(FALSE), not null
#  official_transcript_required          :boolean          default(FALSE), not null
#  official_transcript_required_override :boolean          default(FALSE), not null
#


require 'spec_helper'

describe CareerProfile::EducationExperience do

    fixtures(:users, :education_experiences)

    it "should not delete transcripts when deleted" do
        education_experience = education_experiences(:education_experience_with_two_transcripts)
        transcripts = education_experience.transcripts.to_a
        education_experience.destroy!
        expect(S3TranscriptAsset.where(id: transcripts.pluck(:id)).size).to be(2)
    end

    describe "validations" do
        it "should not be valid with an empty string transcript_waiver" do
            education_experience = education_experiences(:education_experience_with_no_transcripts)
            education_experience.transcript_waiver = ''
            expect(education_experience.valid?).to be(false)
            education_experience.transcript_waiver = nil
            expect(education_experience.valid?).to be(true)
            education_experience.transcript_waiver = 'test'
            expect(education_experience.valid?).to be(true)
        end

        it "should not be valid if both waived and a transcript" do
            education_experience = education_experiences(:education_experience_with_two_transcripts)
            expect(education_experience.valid?).to be(true)
            education_experience.transcript_waiver = 'test'
            expect(education_experience.valid?).to be(false)
            education_experience.transcripts.delete_all
            expect(education_experience.valid?).to be(true)
        end

        it "should not be valid if trying to approve a transcript that has not been waived or transcript uploaded" do
            education_experience = education_experiences(:education_experience_with_no_transcripts)
            education_experience.transcript_approved = true
            expect(education_experience.valid?).to be(false)
            education_experience.transcript_waiver = 'test waiver'
            expect(education_experience.valid?).to be(true)
        end
    end

    describe "should_update_career_profile_fulltext?" do
        it "should be based on the saved attributes" do
            education_experience = CareerProfile::EducationExperience.first
            target_attributes = [
                :educational_organization_option_id,
                :major,
                :minor
            ]

            CareerProfile::EducationExperience.new.attributes.keys.map(&:to_sym).each do |attribute|
                is_target_attr = target_attributes.include?(attribute)
                allow(education_experience).to receive(:saved_changes) { Hash[attribute, true] }
                expect(education_experience.should_update_career_profile_fulltext?).to be(is_target_attr)
            end
        end
    end

    describe "identify_user" do
        it "should identify user if transcript_waiver changes" do
            education_experience = education_experiences(:education_experience_with_no_transcripts)
            expect(education_experience).to receive(:identify_user).and_call_original
            expect_any_instance_of(User).to receive(:identify)
            education_experience.update!(transcript_waiver: 'test')
        end

        it "should identify user if transcript_approved changes" do
            education_experience = education_experiences(:education_experience_with_two_transcripts)
            education_experience.update_columns(transcript_approved: false)
            expect(education_experience).to receive(:identify_user).and_call_original
            expect_any_instance_of(User).to receive(:identify)
            education_experience.update!(transcript_approved: true)
        end
    end

    describe "after_save" do
        describe "career_profile_fulltext" do
            it "should call update_fulltext on the career_profile if should_update_career_profile_fulltext?" do
                education_experience = CareerProfile::EducationExperience.first
                allow(education_experience).to receive(:should_update_career_profile_fulltext?) { true }
                expect(education_experience.career_profile).to receive(:update_fulltext)
                education_experience.update!(major: 'changed')
            end

            it "should not call update_fulltext if not should_update_career_profile_fulltext?" do
                education_experience = CareerProfile::EducationExperience.first
                allow(education_experience).to receive(:should_update_career_profile_fulltext?) { false }
                expect(education_experience.career_profile).not_to receive(:update_fulltext)
                education_experience.update!(major: 'changed')
            end
        end
    end

    describe "after_destroy" do
        it "should call update_fulltext on the career_profile" do
            education_experience = CareerProfile::EducationExperience.first
            expect(education_experience.career_profile).to receive(:update_fulltext)
            education_experience.destroy!
        end
    end

    describe "transcript_required?" do
        it "should work" do
            education_experience = CareerProfile::EducationExperience.first

            education_experience.update_columns(degree_program: false, will_not_complete: true, graduation_year: Date.current.year + 1)
            expect(education_experience.transcript_required?).to be(false)

            education_experience.update_columns(degree_program: true)
            expect(education_experience.transcript_required?).to be(false)

            education_experience.update_columns(will_not_complete: false)
            expect(education_experience.transcript_required?).to be(false)

            education_experience.update_columns(graduation_year: Date.current.year)
            expect(education_experience.transcript_required?).to be(true)

            education_experience.update_columns(graduation_year: Date.current.year - 1)
            expect(education_experience.transcript_required?).to be(true)
        end
    end

    describe "as_json" do
        it "should work" do
            education_experience = education_experiences(:education_experience_with_two_transcripts)
            education_experience.update!(transcript_approved: true)
            json = education_experience.as_json

            expect(json["transcripts"]).to eq(education_experience.transcripts.as_json)
            expect(json["transcript_approved"]).to be(true)
        end
    end

    describe "determine_transcript_requirements" do

        it "should be called in a before_save on create when has_changes_that_could_affect_transcript_requirements?" do
            career_profile = CareerProfile.first
            experience = career_profile.education_experiences.build({
                career_profile_id: career_profile.id,
                degree_program: false,
                graduation_year: 2019,
                major: 'Some Program',
                will_not_complete: false,
                educational_organization_option_id: CareerProfile::EducationalOrganizationOption.first.id
            })
            expect(experience).to receive(:has_changes_that_could_affect_transcript_requirements?).exactly(1).time.and_return(true)
            expect(experience).to receive(:determine_transcript_requirements).exactly(1).time
            experience.save!
        end

        it "should be called in a before_save on update when has_changes_that_could_affect_transcript_requirements?" do
            experience = CareerProfile.includes(:education_experiences).joins(:education_experiences).first.education_experiences.first
            expect(experience).to receive(:has_changes_that_could_affect_transcript_requirements?).exactly(1).times.and_return(true)
            expect(experience).to receive(:determine_transcript_requirements).exactly(1).time
            experience.save!
        end

        it "should be called on an after_commit on destroy" do
            experience = CareerProfile::EducationExperience.first
            expect(experience).to receive(:determine_transcript_requirements).exactly(1).time
            experience.destroy
        end

    end

    describe "has_changes_that_could_affect_transcript_requirements?" do

        it "should be based on the changes" do
            experience = CareerProfile::EducationExperience.first
            target_attributes = [
                :degree,
                :educational_organization_option_id,
                :graduation_year,
                :will_not_complete,
                :official_transcript_required_override
            ]

            experience.attributes.keys.map(&:to_sym).each do |attribute|
                is_target_attr = target_attributes.include?(attribute)
                allow(experience).to receive(:changes) { Hash["#{attribute}", true] }
                expect(experience.has_changes_that_could_affect_transcript_requirements?).to be(is_target_attr)
            end
        end

    end

    describe "should_determine_transcript_requirements?" do

        attr_accessor :experience, :another_experience

        before(:each) do
            @experience = education_experiences(:education_experience_with_two_transcripts)
            @another_experience = education_experiences(:education_experience_with_no_transcripts)
        end

        describe "when has_changes_that_could_affect_transcript_requirements?" do

            before(:each) do
                allow(experience).to receive(:has_changes_that_could_affect_transcript_requirements?).and_return(true)
                allow(another_experience).to receive(:has_changes_that_could_affect_transcript_requirements?).and_return(true)
            end

            it "should return true if experience is in career profile association and has_changes_that_could_affect_transcript_requirements?" do
                allow(experience.career_profile).to receive(:education_experiences).and_return([experience])
                expect(experience.should_determine_transcript_requirements?).to be(true)
            end

            it "should return false if experience is not last in relationship that has_changes_that_could_affect_transcript_requirements?" do
                allow(another_experience.career_profile).to receive(:education_experiences).and_return([another_experience, experience])
                expect(another_experience.should_determine_transcript_requirements?).to be(false)
            end

            it "should capture_in_production if experience is NOT in career profile association and has_changes_that_could_affect_transcript_requirements?" do
                allow(experience.career_profile).to receive(:education_experiences).and_return([])
                expect(Raven).to receive(:capture_in_production).exactly(1).times
                expect(experience.should_determine_transcript_requirements?).to be(false)
            end

        end

        it "should return false" do
            allow(experience).to receive(:has_changes_that_could_affect_transcript_requirements?).and_return(false)
            expect(experience.should_determine_transcript_requirements?).to be(false)
        end

    end

end
