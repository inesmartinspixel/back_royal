# == Schema Information
#
# Table name: educational_organization_options
#
#  id               :uuid             not null, primary key
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  text             :text             not null
#  locale           :text             default("en"), not null
#  suggest          :boolean          default(FALSE)
#  source_id        :text
#  image_url        :text
#  degree_blacklist :boolean          default(FALSE), not null
#

require 'spec_helper'

describe CareerProfile::EducationalOrganizationOption do

    describe 'degree_blacklist' do

        it 'should get set to true for blacklisted institution' do
            institution = CareerProfile::EducationalOrganizationOption.create!({text: "University of Kabul"})
            expect(institution.degree_blacklist).to be(true)
        end

        it 'should be false for non-blacklisted institution' do
            institution = CareerProfile::EducationalOrganizationOption.create!({text: "Some Non-Blacklisted Institution"})
            expect(institution.degree_blacklist).to be(false)
        end

    end

end
