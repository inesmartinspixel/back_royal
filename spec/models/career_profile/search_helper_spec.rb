# == Schema Information
#
# Table name: career_profile_search_helpers_2
#
#  id                                 :uuid             not null, primary key
#  career_profile_id                  :uuid             not null
#  current_up_to                      :datetime         not null
#  work_experience_roles              :text             not null, is an Array
#  work_experience_industries         :text             not null, is an Array
#  past_work_interval                 :interval
#  work_experience_company_names      :text
#  education_experience_school_names  :text
#  current_work_experience_roles      :text             not null, is an Array
#  current_work_experience_industries :text             not null, is an Array
#  has_current_work_experience        :boolean          not null
#  in_school                          :boolean          not null
#  skills_texts                       :text             not null, is an Array
#  student_network_interests_texts    :text             not null, is an Array
#  accepted_by_hiring_managers_count  :integer          not null
#  default_prioritization             :float            not null
#  years_of_experience                :float
#  years_of_experience_reference_date :datetime
#  skills_vector                      :tsvector
#  student_network_interests_vector   :tsvector
#

require 'spec_helper'

describe CareerProfile::SearchHelper do

    it "should update when the career profile is updated" do
        start = Time.now
        career_profile = CareerProfile.joins(:career_profile_search_helper).first
        expect(career_profile.career_profile_search_helper.current_up_to).to be < start
        career_profile.touch
        CareerProfile::SearchHelper.write_new_records
        expect(career_profile.career_profile_search_helper.reload.current_up_to).to be > start
    end

    it "should update when a hiring relationship is updated" do
        start = Time.now
        hiring_relationship = HiringRelationship.joins(:candidate => {:career_profile => :career_profile_search_helper}).first
        career_profile = hiring_relationship.candidate.career_profile
        expect(career_profile.career_profile_search_helper.current_up_to).to be < start
        hiring_relationship.touch
        CareerProfile::SearchHelper.write_new_records
        expect(career_profile.career_profile_search_helper.reload.current_up_to).to be > start
    end

    it "should update if a work experience has started since the last update" do
        # set up a career profile with a work experience in the future and
        # search helpers all written
        start_time = Time.now + 1.month
        work_experience = CareerProfile::WorkExperience.first
        work_experience.start_date = start_time
        work_experience.end_date = start_time + 11.months
        work_experience.save!
        career_profile = work_experience.career_profile
        CareerProfile::SearchHelper.write_new_records

        # mock time passing such that the start_date of the work experience
        # is now in the past
        orig = career_profile.career_profile_search_helper.current_up_to
        allow(CareerProfile::SearchHelper).to receive(:now).and_return(start_time + 1.day)
        CareerProfile::SearchHelper.write_new_records
        expect(career_profile.career_profile_search_helper.reload.current_up_to).to be > orig
    end

    it "should update if a work experience has ended since the last update" do
        # set up a career profile with a work experience that has a start_date in the
        # past and an end_date in the future and make sure that
        # search helpers all written
        start_time = Time.now - 1.month
        end_time = start_time + 2.months
        work_experience = CareerProfile::WorkExperience.first
        work_experience.start_date = start_time
        work_experience.end_date = end_time
        work_experience.save!
        career_profile = work_experience.career_profile
        CareerProfile::SearchHelper.write_new_records

        # mock time passing such that the start_date of the work experience
        # is now in the past
        orig = career_profile.career_profile_search_helper.current_up_to
        allow(CareerProfile::SearchHelper).to receive(:now).and_return(end_time + 1.day)
        CareerProfile::SearchHelper.write_new_records
        expect(career_profile.career_profile_search_helper.reload.current_up_to).to be > orig
    end

    describe "years_of_experience" do

        it "should be nil if there are no work experiences" do
            career_profile = CareerProfile.where(interested_in_joining_new_company: CareerProfile.active_interest_levels.first).first
            career_profile.work_experiences.destroy_all
            career_profile.touch # trigger update in write_new_records
            CareerProfile::SearchHelper.write_new_records
            expect(career_profile.career_profile_search_helper.has_current_work_experience).to be(false)
            expect(career_profile.career_profile_search_helper.years_of_experience).to eq(0)
        end

        it "should be calculated from work experiences if there is no current work experience" do
            career_profile = CareerProfile.joins(:career_profile_search_helper).first
            career_profile.education_experiences.delete_all
            career_profile.work_experiences.delete_all

            work_experiences = CareerProfile::WorkExperience.where.not(start_date: nil).where.not(career_profile_id: career_profile.id).limit(2)
            work_experiences[0].update({
                :start_date => '2008-12-31',
                :end_date => '2010-12-31',
                :career_profile_id => career_profile.id,
                :employment_type => 'full_time'
            })
            work_experiences[1].update({
                :start_date => '2012-01-01',
                :end_date => '2014-01-01',
                :career_profile_id => career_profile.id,
                :employment_type => 'full_time'
            })
            work_experiences[0].career_profile.touch # trigger update in write_new_records
            CareerProfile::SearchHelper.write_new_records

            career_profile.reload

            expected_years = 4
            expect(career_profile.career_profile_search_helper.years_of_experience).to be_within(0.1).of(expected_years)
            expect(career_profile.career_profile_search_helper.years_of_experience_reference_date).to be_nil
        end

        it "should have a reference date if there is a current work experience (no end_date)" do
            career_profile = CareerProfile.joins(:career_profile_search_helper).first
            career_profile.education_experiences.delete_all
            career_profile.work_experiences.delete_all

            work_experiences = CareerProfile::WorkExperience.where.not(start_date: nil).where.not(career_profile_id: career_profile.id).limit(2)
            work_experiences[0].update({
                :start_date => '2008-12-31',
                :end_date => '2010-12-31',
                :career_profile_id => career_profile.id,
                :employment_type => 'full_time'
            })
            work_experiences[1].update({
                :start_date => '2018-01-01',
                :career_profile_id => career_profile.id,
                :end_date => nil,
                :employment_type => 'full_time'
            })
            work_experiences[0].career_profile.touch # trigger update in write_new_records
            CareerProfile::SearchHelper.write_new_records

            career_profile.reload

            expect(career_profile.career_profile_search_helper.years_of_experience).to be_nil

            years_at_previous_job = 2
            years_at_current_job = (Time.now - Time.parse('2018/01/01')) / 1.year
            calculated_years_of_experience = (Time.now - career_profile.career_profile_search_helper.years_of_experience_reference_date)
            expect(calculated_years_of_experience).to be_within(1.day).of((years_at_previous_job + years_at_current_job)*1.year)
        end

        it "should have a reference date if there is a current work experience (end_date > now())" do
            career_profile = CareerProfile.joins(:career_profile_search_helper).first
            career_profile.education_experiences.delete_all
            career_profile.work_experiences.delete_all

            work_experiences = CareerProfile::WorkExperience.where.not(start_date: nil).where.not(career_profile_id: career_profile.id).limit(2)
            work_experiences[0].update({
                :start_date => '2008-12-31',
                :end_date => '2010-12-31',
                :career_profile_id => career_profile.id,
                :employment_type => 'full_time'
            })
            work_experiences[1].update({
                :start_date => '2018-01-01',
                :career_profile_id => career_profile.id,
                :end_date => Time.now + 3.months,
                :employment_type => 'full_time'
            })
            work_experiences[0].career_profile.touch # trigger update in write_new_records
            CareerProfile::SearchHelper.write_new_records

            career_profile.reload

            expect(career_profile.career_profile_search_helper.years_of_experience).to be_nil

            years_at_previous_job = 2
            years_at_current_job = (Time.now - Time.parse('2018/01/01')) / 1.year
            calculated_years_of_experience = (Time.now - career_profile.career_profile_search_helper.years_of_experience_reference_date)
            expect(calculated_years_of_experience).to be_within(1.day).of((years_at_previous_job + years_at_current_job)*1.year)
        end

        it "should handle multiple current work experiences" do
            career_profile = CareerProfile.joins(:career_profile_search_helper).first
            career_profile.education_experiences.delete_all
            career_profile.work_experiences.delete_all

            work_experiences = CareerProfile::WorkExperience.where.not(start_date: nil).where.not(career_profile_id: career_profile.id).limit(2)
            work_experiences[0].update({
                :start_date => '2018-01-01',
                :end_date => nil,
                :career_profile_id => career_profile.id,
                :employment_type => 'full_time'
            })
            work_experiences[1].update({
                :start_date => '2012-06-10',
                :career_profile_id => career_profile.id,
                :end_date => nil,
                :employment_type => 'full_time'
            })
            work_experiences[0].career_profile.touch # trigger update in write_new_records
            CareerProfile::SearchHelper.write_new_records

            career_profile.reload

            expect(career_profile.career_profile_search_helper.years_of_experience).to be_nil

            years_at_current_job = (Time.now - Time.parse('2012/06/10')) / 1.year
            calculated_years_of_experience = (Time.now - career_profile.career_profile_search_helper.years_of_experience_reference_date)
            expect(calculated_years_of_experience).to be_within(1.day).of(years_at_current_job*1.year)
        end

        it "should handle past work experiences overlapping the current one" do
            career_profile = CareerProfile.joins(:career_profile_search_helper).first
            career_profile.education_experiences.delete_all
            career_profile.work_experiences.delete_all

            work_experiences = CareerProfile::WorkExperience.where.not(start_date: nil).where.not(career_profile_id: career_profile.id).limit(2)
            work_experiences[0].update({
                :start_date => '2012-12-31',
                :end_date => '2018-02-28',
                :career_profile_id => career_profile.id,
                :employment_type => 'full_time'
            })
            work_experiences[1].update({
                :start_date => '2016-01-01',
                :career_profile_id => career_profile.id,
                :end_date => nil,
                :employment_type => 'full_time'
            })
            work_experiences[0].career_profile.touch # trigger update in write_new_records
            CareerProfile::SearchHelper.write_new_records

            career_profile.reload

            expect(career_profile.career_profile_search_helper.years_of_experience).to be_nil

            years_at_job = (Time.now - Time.parse('2012-12-31')) / 1.year
            calculated_years_of_experience = (Time.now - career_profile.career_profile_search_helper.years_of_experience_reference_date)
            expect(calculated_years_of_experience).to be_within(1.day).of(years_at_job*1.year)
        end


        it "should ignore a part-time work experience" do
            career_profile = CareerProfile.joins(:career_profile_search_helper).first
            career_profile.education_experiences.delete_all
            career_profile.work_experiences.delete_all

            work_experiences = CareerProfile::WorkExperience.where.not(start_date: nil).where.not(career_profile_id: career_profile.id).limit(2)
            work_experiences[0].update({
                :start_date => '2008-12-31',
                :end_date => '2010-12-31',
                :career_profile_id => career_profile.id,
                :employment_type => 'not_full_time'
            })
            work_experiences[0].career_profile.touch # trigger update in write_new_records
            CareerProfile::SearchHelper.write_new_records

            career_profile.reload

            expect(career_profile.career_profile_search_helper.has_current_work_experience).to be(false)
            expect(career_profile.career_profile_search_helper.years_of_experience).to eq(0)
        end

        it "should ignore a work experience that starts in the future" do
            career_profile = CareerProfile.joins(:career_profile_search_helper).first
            career_profile.education_experiences.delete_all
            career_profile.work_experiences.delete_all

            work_experiences = CareerProfile::WorkExperience.where.not(start_date: nil).where.not(career_profile_id: career_profile.id).limit(1)
            work_experiences[0].update({
                :start_date => Time.now + 1.year,
                :end_date => nil,
                :career_profile_id => career_profile.id,
                :employment_type => 'full_time'
            })
            work_experiences[0].career_profile.touch # trigger update in write_new_records
            CareerProfile::SearchHelper.write_new_records

            career_profile.reload

            expect(career_profile.career_profile_search_helper.has_current_work_experience).to be(false)
            expect(career_profile.career_profile_search_helper.years_of_experience).to eq(0)
        end

        # we shouldn't have these, and in the future we will prevent it, but they exist in the db
        it "should ignore a work experience with end_time before start_time" do
            career_profile = CareerProfile.joins(:career_profile_search_helper).first
            career_profile.education_experiences.delete_all
            career_profile.work_experiences.delete_all

            work_experiences = CareerProfile::WorkExperience.where.not(start_date: nil).where.not(career_profile_id: career_profile.id).limit(2)
            work_experiences[0].update({
                :start_date => '2008-12-31',
                :end_date => '2010-12-31',
                :career_profile_id => career_profile.id,
                :employment_type => 'full_time'
            })
            work_experiences[1].update({
                :start_date => '2010-12-31',
                :end_date => '208-12-31',
                :career_profile_id => career_profile.id,
                :employment_type => 'full_time'
            })
            work_experiences[0].career_profile.touch # trigger update in write_new_records
            CareerProfile::SearchHelper.write_new_records

            career_profile.reload

            expected_years = 2
            expect(career_profile.career_profile_search_helper.years_of_experience).to be_within(0.1).of(expected_years)
            expect(career_profile.career_profile_search_helper.years_of_experience_reference_date).to be_nil
        end
    end

    describe "get_years_of_experience" do
        it "should compute years_of_experience based on years_of_experience_reference_date if present" do
            now = Time.now
            search_helper = CareerProfile::SearchHelper.first
            expect(search_helper).to receive(:years_of_experience_reference_date).twice.and_return(now - 2.years)
            expect(search_helper.get_years_of_experience).to be_within(0.01).of(2) # float rounding
        end

        it "should return years_of_experience if no years_of_experience_reference_date" do
            search_helper = CareerProfile::SearchHelper.first
            expect(search_helper).to receive(:years_of_experience_reference_date).and_return(nil)
            expect(search_helper).to receive(:years_of_experience).and_return(1337.7)
            expect(search_helper.get_years_of_experience).to be(1337.7)
        end
    end

end
