# == Schema Information
#
# Table name: published_cohort_stream_locale_packs
#
#  created_at            :datetime
#  cohort_id             :uuid
#  cohort_name           :text
#  foundations           :boolean
#  required              :boolean
#  specialization        :boolean
#  elective              :boolean
#  exam                  :boolean
#  stream_title          :text
#  stream_locale_pack_id :uuid
#  stream_locales        :text
#  playlist_count        :integer
#
require 'spec_helper'
require 'derived_content_table_spec_helper'

describe PublishedCohortStreamLocalePack do
    include DerivedContentTableSpecHelper

    fixtures(:cohorts, :users, :lesson_streams)

    attr_accessor :cohort, :stream

    before(:each) do
        allow(RefreshMaterializedContentViews).to receive(:force_refresh_views).and_return(true)
        @cohort = cohorts(:published_emba_cohort)
        @stream = Lesson::Stream.find_by_locale_pack_id(@cohort.periods[0]['stream_entries'][0]['locale_pack_id'])
    end

    describe "write_new_records" do

        it "should write columns with info about stream" do
            # find a stream with multiple locales and ensure it is in the cohort
            stream_locale_pack_id = lesson_streams(:es_item).locale_pack_id
            cohort.periods[0]['stream_entries'] << {'locale_pack_id' => stream_locale_pack_id, 'required' => false}

            # publishing the cohort will trigger write_new_records
            expect(PublishedCohortStreamLocalePack).to receive(:write_new_records).and_call_original
            cohort.publish!

            # find the record for the stream
            record = PublishedCohortStreamLocalePack.where(
                cohort_id: cohort.id,
                stream_locale_pack_id: stream_locale_pack_id
            ).first
            expect(record).not_to be_nil
            streams = Lesson::Stream.where(locale_pack_id:stream_locale_pack_id)
            en_stream = streams.detect { |str| str.locale == 'en' }


            expect(record).not_to be_nil
            expect(record.stream_title).to eq(en_stream.title)
            expect(record.stream_locales.split(',')).to match_array(streams.map(&:locale))
        end

        it "should write playlist_count" do
            # make sure there is a stream that is in 2 playlists (there are
            # no such streams in the fixtures)
            playlists = Playlist.where(locale:'en', locale_pack_id: cohort.required_playlist_pack_ids).limit(2)
            stream = playlists[0].streams.where(locale: 'en').first
            playlists[1]['stream_entries'] << {'locale_pack_id' => stream.locale_pack_id}

            # publishing the playlist will trigger write_new_records
            expect(PublishedCohortStreamLocalePack).to receive(:write_new_records).and_call_original
            cohort.publish!

            playlists[1].publish! # triggers write_new_records

            record = PublishedCohortStreamLocalePack.where(
                cohort_id: cohort.id,
                stream_locale_pack_id: stream.locale_pack_id
            ).first
            expect(record.playlist_count).to eq(2)
        end

        it "should write record for foundations stream" do
            locale_pack_id = cohort.get_foundations_lesson_stream_locale_pack_ids('en').first

            PublishedCohortStreamLocalePack.delete_all
            PublishedCohortStreamLocalePack.send(:write_new_records, :stream_locale_pack_id, [locale_pack_id])

            record = PublishedCohortStreamLocalePack.where(
                cohort_id: cohort.id,
                stream_locale_pack_id: locale_pack_id
            ).first

            expect(record).not_to be_nil
            expect(record.foundations).to be(true)
        end

        it "should write record for stream that is required because it is in the schedule" do
            locale_pack_ids_in_schedule = cohort.periods.pluck('stream_entries').flatten.select {|e| e['required']}.pluck('locale_pack_id')
            locale_pack_ids_in_required_playlists = Playlist.where(locale_pack_id: cohort.required_playlist_pack_ids).map(&:stream_locale_pack_ids).flatten
            locale_pack_id_only_in_schedule = (locale_pack_ids_in_schedule - locale_pack_ids_in_required_playlists).first
            expect(locale_pack_id_only_in_schedule).not_to be_nil

            PublishedCohortStreamLocalePack.delete_all
            PublishedCohortStreamLocalePack.send(:write_new_records, :stream_locale_pack_id, [locale_pack_id_only_in_schedule])

            record = PublishedCohortStreamLocalePack.where(
                cohort_id: cohort.id,
                stream_locale_pack_id: locale_pack_id_only_in_schedule
            ).first

            expect(record).not_to be_nil
            expect(record.required).to be(true)
            expect(record.playlist_count).to be(0)
        end

        it "should write record for stream that is required because it is in a required playlist" do
            locale_pack_ids_in_schedule = cohort.periods.pluck('stream_entries').flatten.select {|e| e['required']}.pluck('locale_pack_id')
            locale_pack_ids_in_required_playlists = Playlist.where(locale_pack_id: cohort.required_playlist_pack_ids).map(&:stream_locale_pack_ids).flatten
            locale_pack_id_only_in_playlist = (locale_pack_ids_in_required_playlists - locale_pack_ids_in_schedule).first
            expect(locale_pack_id_only_in_playlist).not_to be_nil

            PublishedCohortStreamLocalePack.delete_all
            PublishedCohortStreamLocalePack.send(:write_new_records, :stream_locale_pack_id, [locale_pack_id_only_in_playlist])

            record = PublishedCohortStreamLocalePack.where(
                cohort_id: cohort.id,
                stream_locale_pack_id: locale_pack_id_only_in_playlist
            ).first

            expect(record).not_to be_nil
            expect(record.required).to be(true)
            expect(record.playlist_count).to be > 0
        end

        it "should write record for specialization stream" do
            locale_pack_ids_in_specialization_playlists = Playlist.where(locale_pack_id: cohort.specialization_playlist_pack_ids).map(&:stream_locale_pack_ids).flatten

            PublishedCohortStreamLocalePack.delete_all
            PublishedCohortStreamLocalePack.send(:write_new_records, :stream_locale_pack_id, locale_pack_ids_in_specialization_playlists)

            record = PublishedCohortStreamLocalePack.where(
                cohort_id: cohort.id,
                stream_locale_pack_id: locale_pack_ids_in_specialization_playlists
            ).first

            expect(record).not_to be_nil
            expect(record.specialization).to be(true)
        end

        it "should write record for elective stream that is included because it is in an access group" do
            locale_pack_ids_in_schedule = cohort.periods.pluck('stream_entries').flatten.pluck('locale_pack_id')
            locale_pack_ids_in_required_playlists = Playlist.where(locale_pack_id: cohort.required_playlist_pack_ids).map(&:stream_locale_pack_ids).flatten

            # fixtures do not set up any of these streams
            another_stream = Lesson::Stream.all_published.joins(:locale_pack => :access_groups).where.not(locale_pack_id: PublishedCohortStreamLocalePack.where(cohort_id: cohort.id).pluck('stream_locale_pack_id')).first

            # since this is not the real way we add access groups to a stream
            # in the wild, it will not trigger a rebuild of the table, so
            # we have to do it manually
            cohort.access_groups << another_stream.access_groups.first
            PublishedCohortStreamLocalePack.delete_all
            PublishedCohortStreamLocalePack.send(:write_new_records, :stream_locale_pack_id, [another_stream.locale_pack_id])

            record = PublishedCohortStreamLocalePack.where(
                cohort_id: cohort.id,
                stream_locale_pack_id: another_stream.locale_pack_id
            ).first

            expect(record).not_to be_nil
            expect(record.elective).to be(true)
        end


        it "should write record for elective stream that is included because it is in the schedule" do
            locale_pack_ids_in_required_playlists = Playlist.where(locale_pack_id: cohort.required_playlist_pack_ids).map(&:stream_locale_pack_ids).flatten.to_set
            locale_pack_id = cohort.periods.pluck('stream_entries').flatten.detect do |e|
                !e['required'] &&
                !locale_pack_ids_in_required_playlists.include?(e['locale_pack_id'])
            end['locale_pack_id']

            PublishedCohortStreamLocalePack.delete_all
            PublishedCohortStreamLocalePack.send(:write_new_records, :stream_locale_pack_id, [locale_pack_id])

            record = PublishedCohortStreamLocalePack.where(
                cohort_id: cohort.id,
                stream_locale_pack_id: locale_pack_id
            ).first

            expect(record).not_to be_nil
            expect(record.elective).to be(true)
        end

        it "should write record for exam stream" do
            locale_pack_ids_in_schedule = cohort.periods.pluck('stream_entries').flatten.select {|e| e['required']}.pluck('locale_pack_id')
            exam_stream = Lesson::Stream.where(exam: true, locale_pack_id: locale_pack_ids_in_schedule).first
            expect(exam_stream).not_to be_nil

            PublishedCohortStreamLocalePack.delete_all
            PublishedCohortStreamLocalePack.send(:write_new_records, :stream_locale_pack_id, [exam_stream.locale_pack_id])

            record = PublishedCohortStreamLocalePack.where(
                cohort_id: cohort.id,
                stream_locale_pack_id: exam_stream.locale_pack_id
            ).first

            expect(record).not_to be_nil
            expect(record.exam).to be(true)
        end

    end

    describe "update_on_content_change" do

        it "should add rows for a stream when a stream is added to a required playlist" do

            max_created_at = PublishedCohortStreamLocalePack.maximum('created_at')
            stream, playlist = add_stream_to_required_playlist_and_assert_row_created

            # check that only records for the added stream have been updated
            modified_records = PublishedCohortStreamLocalePack.where('created_at > ?', max_created_at)
            expect(modified_records.map(&:stream_locale_pack_id).uniq).to eq([stream.locale_pack_id])

        end

        it "should remove rows for a stream when a stream is removed from a required playlist" do
            stream, playlist = add_stream_to_required_playlist_and_assert_row_created

            max_created_at = PublishedCohortStreamLocalePack.maximum('created_at')

            playlist.stream_entries.reject! { |e| e['locale_pack_id'] == stream.locale_pack_id}
            playlist.publish!

            record =  PublishedCohortStreamLocalePack.where(cohort_id: cohort.id, stream_locale_pack_id: stream.locale_pack_id).first
            expect(record).to be_nil

            # Since the only changes were to remove records, we don't expect to
            # find any modified records in the db
            modified_records = PublishedCohortStreamLocalePack.where('created_at > ?', max_created_at)
            expect(modified_records).to be_empty

        end

        def add_stream_to_required_playlist_and_assert_row_created
            playlist = Playlist.where(locale_pack_id: cohort.required_playlist_pack_ids, locale: 'en').first
            existing_locale_pack_ids = PublishedCohortStreamLocalePack.pluck('stream_locale_pack_id')
            stream = Lesson::Stream.all_published.where.not(locale_pack_id: existing_locale_pack_ids).first

            playlist.stream_entries << {
                'locale_pack_id' => stream.locale_pack_id
            }
            playlist.publish!

            # check that a record was written for this stream in this cohort
            record =  PublishedCohortStreamLocalePack.where(cohort_id: cohort.id, stream_locale_pack_id: stream.locale_pack_id).first
            expect(record).not_to be_nil

            [stream, playlist]
        end

        it "should replace rows for a stream" do
            assert_replace_rows_for_content_item(PublishedCohortStreamLocalePack, stream, :locale_pack_id, :stream_locale_pack_id) do
                stream.title = 'changed'
            end
        end

        it "should replace rows for a cohort" do
            assert_replace_rows_for_content_item(PublishedCohortStreamLocalePack, cohort, :id, :cohort_id) do
                cohort.name = 'changed'
            end
        end
    end

end
