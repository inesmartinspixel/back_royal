# == Schema Information
#
# Table name: lesson_streams_progress
#
#  id                   :uuid             not null, primary key
#  created_at           :datetime
#  updated_at           :datetime
#  lesson_bookmark_id   :uuid
#  started_at           :datetime
#  completed_at         :datetime
#  user_id              :uuid
#  certificate_image_id :uuid
#  locale_pack_id       :uuid             not null
#  time_runs_out_at     :datetime
#  official_test_score  :float
#  waiver               :text
#

require 'spec_helper'

describe Lesson::StreamProgress do

    fixtures(:lesson_streams_progress, :lesson_streams, :users, :cohorts)

    before(:each) do
        @user = users(:learner)
        @streams = Lesson::Stream.all_published.limit(3)
        @lessons = @streams[0].lessons
    end

    describe "create_or_update!" do

        it "should create a stream progress event" do
            Lesson::StreamProgress.delete_all
            Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => @streams[0].locale_pack_id,
                :lesson_bookmark_id => @lessons[0].id,
                :user_id => @user.id
            })

            lp_events = Lesson::StreamProgress.where({:locale_pack_id => @streams[0].locale_pack_id, :user_id => @user.id})
            expect(lp_events.length).to eq(1)
            expect(lp_events[0].locale_pack_id).to eq(@streams[0].locale_pack_id)
            expect(lp_events[0].user_id).to eq(@user.id)
            expect(lp_events[0].started_at).not_to be_nil
            expect(lp_events[0].completed_at).to be_nil
        end

        it "should create a single record for a user doing progress in two locales" do
            en_stream, es_stream = [lesson_streams(:en_item), lesson_streams(:es_item)]
            Lesson::StreamProgress.delete_all
            Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => en_stream.locale_pack_id,
                :user_id => @user.id
            })
            Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => es_stream.locale_pack_id,
                :user_id => @user.id
            })
            records = Lesson::StreamProgress.all
            expect(records.length).to eq(1)
            expect(records[0].locale_pack_id).to eq(en_stream.locale_pack_id)
        end

        it "should use a user that is passed in" do
            record = Lesson::StreamProgress.first
            attrs = {locale_pack_id: record.locale_pack_id, user_id: record.user_id}
            user = record.user

            # it should work when updating
            expect(Lesson::StreamProgress.create_or_update!(attrs, user).user).to be(user)

            # it should work when creating
            record.destroy
            expect(Lesson::StreamProgress.create_or_update!(attrs, user).user).to be(user)

        end

        it "should raise if the passed-in user does not match the user_id" do
            user = User.first
            expect {
                Lesson::StreamProgress.create_or_update!({locale_pack_id: SecureRandom.uuid, user_id: SecureRandom.uuid}, user)
            }.to raise_error("Unexpected user")
        end

        it "should not call merge_hash if nothing is changing" do
            record = Lesson::StreamProgress.where(completed_at: nil).first
            expect_any_instance_of(Lesson::StreamProgress).not_to receive(:merge_hash)
            allow_any_instance_of(Lesson::StreamProgress).to receive(:raise_if_exam_closed)
            Lesson::StreamProgress.create_or_update!(
                locale_pack_id: record.locale_pack_id,
                user_id: record.user_id,
                complete: false
            )
        end

        describe "times_run_out_at" do
             it "should be set only on create if the stream has a time limit" do
                expect_any_instance_of(Lesson::StreamProgress).to receive(:stream_within_launch_window_and_time_limit?).and_return(true)
                stream = lesson_streams(:exam_stream)
                Lesson::StreamProgress.where(user_id: @user.id, locale_pack_id: stream.locale_pack_id).destroy_all

                now = Time.now
                expect(Time).to receive(:now).and_return(now).at_least(1).times
                stream_progress = Lesson::StreamProgress.create_or_update!({
                    :locale_pack_id => stream.locale_pack_id,
                    :user_id => @user.id
                })
                expect(stream_progress.time_runs_out_at).to eq(now + stream.time_limit_hours.hours)

                # should not change the second time, but I could not find a reasonable
                # way to test for it.
            end

            it "should not be updated if it is already set " do
                stream = Lesson::Stream.all_published.where(time_limit_hours: nil).first
                stream_progress = Lesson::StreamProgress.create_or_update!({
                    :locale_pack_id => stream.locale_pack_id,
                    :user_id => @user.id
                })
                expect(stream_progress.time_runs_out_at).to be_nil

            end
        end

        it "should auto-favorite a newly started stream" do

            @user.favorite_lesson_stream_locale_packs = [];
            @user.save!

            Lesson::StreamProgress.delete_all
            Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => @streams[0].locale_pack_id,
                :lesson_bookmark_id => @lessons[0].id,
                :user_id => @user.id
            })

            user = User.find(@user.id)

            expect(user.favorite_lesson_stream_locale_packs).to include(@streams[0].locale_pack)
        end

        it "should update a stream progress event if one already exists" do
            # create the first progress event
            Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => @streams[0].locale_pack_id,
                :lesson_bookmark_id => @lessons[0].id,
                :user_id => @user.id
            })
            orig_count = Lesson::StreamProgress.count

            # now this should update the existing event
            Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => @streams[0].locale_pack_id,
                :lesson_bookmark_id => @lessons[0].id,
                :user_id => @user.id,
                :complete => true
            })

            expect(Lesson::StreamProgress.all.length).to eq(orig_count)
            lsp_events = Lesson::StreamProgress.where({:locale_pack_id => @streams[0].locale_pack_id, :user_id => @user.id})
            expect(lsp_events.length).to eq(1)
            expect(lsp_events[0].locale_pack_id).to eq(@streams[0].locale_pack_id)
            expect(lsp_events[0].user_id).to eq(@user.id)
            expect(lsp_events[0].started_at).not_to be_nil
            expect(lsp_events[0].completed_at).not_to be_nil
        end


        it "should raise on a validation failure" do
            expect{Lesson::StreamProgress.create_or_update!({
                :lesson_bookmark_id => SecureRandom.uuid,
                :user_id => @user.id
            })}.to raise_error(ActiveRecord::RecordNotSaved)
        end

        it "should raise on attempt to create duplicate" do
            lp = Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => @streams[0].locale_pack_id,
                :user_id => @user.id
            })

            expect{Lesson::StreamProgress.create!({
                :user_id => @user.id,
                :started_at => Time.now(),
                :locale_pack_id => @streams[0].locale_pack_id
            })}.to raise_error(ActiveRecord::RecordNotUnique)

        end

        # in the wild, this should not ever happen, since the certificate
        # should be pre-generated, but just to be sure
        it "should update certificate image if complete and there is not one already" do
            sp = Lesson::StreamProgress.where('completed_at' => nil)

            sp = Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => @streams[0].locale_pack_id,
                :user_id => @user.id,
                :complete => true
            })

            expect(sp.certificate_image_id).not_to be_nil
        end

        it "should update last_progress_at" do
            sp = Lesson::StreamProgress.first
            original_last_progress_at = sp.last_progress_at
            Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => sp.lesson_stream.locale_pack_id,
                :user_id => sp.user_id
            })
            expect(sp.reload.last_progress_at > original_last_progress_at).to be(true)
        end

        it "should only set the completed_at the first time a stream is completed" do
            stream = Lesson::Stream.all_published.first
            Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => stream.locale_pack_id,
                :user_id => @user.id,
                :complete => true
            })

            stream_progress = Lesson::StreamProgress.where(:locale_pack_id => stream.locale_pack_id, :user_id => @user.id).first
            orig_completed_at = stream_progress.completed_at
            Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => stream.locale_pack_id,
                :user_id => @user.id,
                :complete => true
            })

            expect(stream_progress.reload.completed_at).to eq(orig_completed_at)
        end

        it "should raise an invalid error if stream_progress.time_has_run_out?" do
            now = Time.now
            allow(Time).to receive(:now).and_return(now)
            sp = Lesson::StreamProgress.first
            allow_any_instance_of(User).to receive(:relevant_cohort).and_return(Cohort.all_published.first)
            sp.update_attribute(:time_runs_out_at, Time.now - 1.hour)
            expect(Raven).to receive(:capture_exception).with("Trying to save stream_progress for closed stream.", {
                level: 'warning',
                extra: {
                    user_id: sp.user_id,
                    locale_pack_id: sp.locale_pack_id,
                    time: now.to_s,
                    time_has_run_out: sp.time_has_run_out?,
                    time_runs_out_at: sp.time_runs_out_at.to_s,
                    exam_open_time: sp.published_stream_for_exam_info.exam_open_time(sp.user),
                    cohort: sp.user.relevant_cohort && sp.user.relevant_cohort.attributes['id']
                }
            })

            expect {
                Lesson::StreamProgress.create_or_update!({
                    :locale_pack_id => sp.locale_pack_id,
                    :user_id => sp.user_id
                })
            }.to raise_error(ActiveRecord::RecordInvalid, 'Validation failed: Course is closed')
        end

        it "should save progress if no exam_open_time" do
            user = User.first
            expect_any_instance_of(Lesson::Stream).to receive(:exam_open_time).at_least(1).times.and_return(nil)
            stream = lesson_streams(:exam_stream)

            expect {
                Lesson::StreamProgress.create_or_update!({
                    :locale_pack_id => stream.locale_pack_id,
                    :user_id => user.id
                })
            }.not_to raise_error
        end

        it "should raise an invalid error before the exam_open_time" do
            user = users(:learner)
            allow_any_instance_of(Lesson::Stream).to receive(:exam_open_time).and_return(Time.now + 1.day)
            stream = lesson_streams(:exam_stream)

            expect {
                Lesson::StreamProgress.create_or_update!({
                    :locale_pack_id => stream.locale_pack_id,
                    :user_id => user.id
                })
            }.to raise_error(ActiveRecord::RecordInvalid, 'Validation failed: Course is closed')
        end

        # do not allow updating lesson progress for lessons in exam streams when
        # outside of the launch window
        it "should error if stream_progress.stream_within_launch_window_and_time_limit? is false" do
            user = users(:learner)
            stream = Lesson::Stream.all_published.first.published_version
            expect_any_instance_of(Lesson::StreamProgress).to receive(:stream_within_launch_window_and_time_limit?).and_return(false)

            expect {
                Lesson::StreamProgress.create_or_update!(
                    locale_pack_id: stream.locale_pack_id,
                    user_id: user.id
                )
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Course is closed")
        end
    end


    describe "handle_started_foundations_playlist_event" do
        attr_reader :foundations_playlist, :stream, :another_stream_from_playlist, :stream_progress, :user
        before(:each) do
            @user.lesson_streams_progresses.delete_all
            @foundations_playlist = Playlist.all_published.where("array_length(playlists_versions.stream_entries, 1) > 1").first
            @stream, @another_stream_from_playlist = foundations_playlist.streams.limit(2)
            @stream_progress = Lesson::StreamProgress.new(
                user_id: user.id,
                locale_pack_id: stream.locale_pack_id,
                started_at: Time.now,
                created_at: Time.now
            )
            stream_progress.user = user
        end

        it "should do nothing if this is not a foundations stream" do
            expect(user).to receive(:get_foundations_playlist).and_return(nil)
            expect(Lesson::StreamProgress).not_to receive(:where)
            expect(Event).not_to receive(:create_server_event!)
            stream_progress.save!
        end

        it "should do nothing if this is not the first completed foundations stream" do
            expect(user).to receive(:get_foundations_playlist).and_return(foundations_playlist)
            Lesson::StreamProgress.create!(
                user_id: user.id,
                locale_pack_id: another_stream_from_playlist.locale_pack_id,
                started_at: Time.now,
                created_at: Time.now
            )
            expect(Event).not_to receive(:create_server_event!)
            stream_progress.save!
        end

        it "should log event if this is the first foundations stream" do
            expect(user).to receive(:relevant_cohort).at_least(1).times.and_return(Cohort.all_published.first)
            expect(user).to receive(:get_foundations_playlist).and_return(foundations_playlist)
            mock_event = double("Event instance")
            expect(Event).to receive(:create_server_event!).with(anything, user.id, 'cohort:started_foundations_playlist', {
                label: user.relevant_cohort.name,
                lesson_stream_locale_pack_id: stream.locale_pack_id,
                cohort_id: user.relevant_cohort.id,
                cohort_title: user.relevant_cohort.title
            }).and_return(mock_event)
            expect(mock_event).to receive(:log_to_external_systems).with(no_args)

            stream_progress.save!
        end
    end

    describe "handle_completed_foundations_events" do
        attr_reader :foundations_playlist, :stream, :another_stream_from_playlist, :stream_progress, :user,
                        :logged_events
        before(:each) do
            @user = CohortApplication.find_by_status('accepted').user
            @user.lesson_streams_progresses.delete_all
            @foundations_playlist = Playlist.all_published.where("array_length(playlists_versions.stream_entries, 1) > 1").first
            @stream, @another_stream_from_playlist = foundations_playlist.streams.limit(2)
            @stream_progress = Lesson::StreamProgress.new(
                user_id: user.id,
                locale_pack_id: stream.locale_pack_id,
                started_at: Time.now,
                created_at: Time.now,
                completed_at: Time.now
            )
            stream_progress.user = user
            @logged_events = {}
            allow(Event).to receive(:create_server_event!) do |id, user_id, event_type, payload|
                expect(user_id).to eq(user.id)
                logged_events[event_type] = payload
                mock_event = double(event_type)
                expect(mock_event).to receive(:log_to_external_systems)
                mock_event
            end
            allow(@user).to receive(:relevant_cohort).and_return(Cohort.all_published.first)
            allow(AdmissionRound).to receive(:promoted_rounds_event_attributes).with(@user.timezone).and_return({promoted_rounds_event_attributes: "yes"})
            allow(user.relevant_cohort).to receive(:event_attributes).with(@user.timezone).and_return({cohort_event_attributes: "yes"})
        end

        it "should do nothing if this is not a foundations stream" do
            expect(user).to receive(:get_foundations_playlist).and_return(nil)
            expect(Lesson::StreamProgress).not_to receive(:where)
            expect(user).not_to receive(:identify)
            stream_progress.save!
        end

        it "should re-identify the user" do
            expect(user).to receive(:get_foundations_playlist).and_return(foundations_playlist)
            expect(user).to receive(:identify)
            stream_progress.save!
        end

        it "should not log completed_foundations_course if log_foundations_completed_events? is false" do
            expect(user).to receive(:get_foundations_playlist).and_return(foundations_playlist)
            expect(user.application_for_relevant_cohort).to receive(:log_foundations_completed_events?).and_return(false)
            stream_progress.save!
            expect(logged_events.keys).not_to include('cohort:completed_foundations_course')
        end

        it "should log completed_foundations_course if log_foundations_completed_events? is true" do
            expect(user).to receive(:get_foundations_playlist).and_return(foundations_playlist)
            expect(user.application_for_relevant_cohort).to receive(:log_foundations_completed_events?).and_return(true)
            stream_progress.save!
            expect(logged_events.keys).to include('cohort:completed_foundations_course')
            attrs = logged_events['cohort:completed_foundations_course']
            expect(attrs[:foundations_percent_complete]).to eq(1.0/foundations_playlist.stream_entries.size)
            expect(attrs[:foundations_courses_complete_count]).to eq(1)
            expect(attrs[:promoted_rounds_event_attributes]).to eq("yes")
            expect(attrs[:cohort_event_attributes]).to eq("yes")
        end

        it "should not log completed_foundations_playlist if foundations is not complete" do
            expect(user).to receive(:get_foundations_playlist).and_return(foundations_playlist)
            stream_progress.save!
            expect(logged_events.keys).not_to include('cohort:completed_foundations_playlist')
        end

        it "should log completed_foundations_playlist if foundations is complete" do
            expect(user).to receive(:get_foundations_playlist).and_return(foundations_playlist)

            # remove all other streams, so that just finishing this one will be enough to complete it
            foundations_playlist.stream_entries.reject! { |e| e['locale_pack_id'] != stream.locale_pack_id }
            foundations_playlist.publish!
            stream_progress.save!
            expect(logged_events.keys).to include('cohort:completed_foundations_playlist')
            attrs = logged_events['cohort:completed_foundations_playlist']
            expect(attrs[:label]).to eq(user.relevant_cohort.name)
            expect(attrs[:cohort_id]).to eq(user.relevant_cohort.id)
            expect(attrs[:cohort_title]).to eq(user.relevant_cohort.title)
            expect(attrs[:lesson_stream_locale_pack_id]).to eq(stream.locale_pack_id)
        end
    end

    describe "mark_application_completed" do
        attr_reader :user, :cohort_application, :stream_progress

        before(:each) do
            @cohort_application = CohortApplication.where(status: 'accepted', graduation_status: 'pending').first
            @user = cohort_application.user
            @user.lesson_streams_progresses.delete_all
            @stream_progress = Lesson::StreamProgress.new(
                id: SecureRandom.uuid,
                user_id: user.id,
                locale_pack_id: Lesson::Stream::LocalePack.first.id,
                started_at: Time.now,
                created_at: Time.now,
                completed_at: Time.now
            )
            stream_progress.user = user
            allow(user).to receive(:application_for_relevant_cohort).and_return(cohort_application)
            allow(cohort_application).to receive(:set_final_score) # don't let anything change the score
            allow(stream_progress).to receive(:just_completed_curriculum?).and_return(true)
        end

        it "should set completed_at" do
            start = Time.now
            stream_progress.save!
            expect(cohort_application.reload.completed_at).to be > start
        end

        it "should mark application as graduated if final_score > 0.7 and supports_graduation_on_complete?" do
            cohort_application.final_score = 0.71
            expect(user.relevant_cohort).to receive(:supports_graduation_on_complete?).and_return(true)
            stream_progress.save!
            expect(cohort_application.reload.graduation_status).to eq('graduated')
        end

        it "should mark application as failed if final_score <= 0.7 and supports_graduation_on_complete?" do
            cohort_application.final_score = 0.70
            expect(user.relevant_cohort).to receive(:supports_graduation_on_complete?).exactly(1).times.and_return(true)
            stream_progress.save!
            expect(cohort_application.reload.graduation_status).to eq('failed')
        end

        it "should not change graduation_status if !supports_graduation_on_complete?" do
            cohort_application.final_score = 0.71
            expect(user.relevant_cohort).to receive(:supports_graduation_on_complete?).exactly(1).times.and_return(false)
            stream_progress.save!
            expect(cohort_application.reload.graduation_status).to eq('pending')
        end

        it "should call capture_in_production if no final_score and supports_graduation_on_complete" do
            cohort_application.final_score = nil
            expect(user.relevant_cohort).to receive(:supports_graduation_on_complete?).exactly(1).times.and_return(true)
            expect(Raven).to receive(:capture_in_production).exactly(1).times.with(Lesson::StreamProgress::NoFinalScoreOnCompleteApplication,
                extra: {
                    stream_progress_id: stream_progress.id
                }
            )
            stream_progress.save!
        end

        it "should not call capture_in_production if no final_score and !supports_graduation_on_complete" do
            cohort_application.final_score = nil
            expect(user.relevant_cohort).to receive(:supports_graduation_on_complete?).exactly(1).times.and_return(false)
            expect(Raven).not_to receive(:capture_in_production)
            stream_progress.save!
        end

    end

    describe "set_relevant_cohort_completion_percentage" do

        before(:each) do
            @stream_progress = Lesson::StreamProgress.first
        end

        it "should set @relevant_cohort_completion_percentage to value of updated_relevant_cohort_completion_percentage" do
            expect(@stream_progress).to receive(:updated_relevant_cohort_completion_percentage).and_return('something_random')
            @stream_progress.set_relevant_cohort_completion_percentage
            expect(@stream_progress.relevant_cohort_completion_percentage).to eq('something_random')
        end

        describe "before create" do

            it "should be called" do
                stream = lesson_streams(:exam_stream)
                user = User.left_outer_joins(:lesson_streams_progresses).where("lesson_streams_progress.id is null").first
                expect_any_instance_of(Lesson::StreamProgress).to receive(:set_relevant_cohort_completion_percentage)
                Lesson::StreamProgress.create!(
                    user_id: user.id,
                    locale_pack_id: stream.locale_pack_id,
                    started_at: Time.now - 1000
                )
            end
        end

        describe "before update" do

            it "should be called" do
                expect(@stream_progress).to receive(:set_relevant_cohort_completion_percentage)
                @stream_progress.save!
            end
        end
    end

    describe "handle_exam_events" do

        # these tests are kind of strangely organized how it checks for all of these "nots",
        # but I'm trying to be defensive against NoMethodErrors and things with as few tests
        # as possible.
        describe "not raise" do
            it "should not raise if not an exam stream" do
                progress = Lesson::StreamProgress.where(completed_at: nil).first
                progress.completed_at = Time.now
                expect(progress).to receive(:for_exam_stream?).at_least(1).times.and_return(false)
                expect(Cohort::ReportExamResultJob).not_to receive(:wait_until)
                expect {
                    progress.save!
                }.not_to raise_error
            end

            it "should not raise if no relevant cohort" do
                progress = Lesson::StreamProgress.where(completed_at: nil).first
                progress.completed_at = Time.now
                expect(progress).to receive(:for_exam_stream?).at_least(1).times.and_return(true)
                expect(progress.user).to receive(:relevant_cohort).and_return(nil).at_least(1).times
                expect(Cohort::ReportExamResultJob).not_to receive(:wait_until)
                expect {
                    progress.save!
                }.not_to raise_error
            end

            it "should not raise if no relevant period" do
                progress = Lesson::StreamProgress.where(completed_at: nil).first
                progress.completed_at = Time.now
                expect(progress).to receive(:for_exam_stream?).at_least(1).times.and_return(true)
                cohort = Cohort.first
                expect(progress.user).to receive(:relevant_cohort).and_return(cohort).at_least(1).times
                expect(cohort).to receive(:period_for_required_stream_locale_pack_id).with(progress.locale_pack_id).and_return(nil)
                expect(Cohort::ReportExamResultJob).not_to receive(:wait_until)
                expect {
                    progress.save!
                }.not_to raise_error
            end

        end

        it "should not do anything if not completing" do
            progress = Lesson::StreamProgress.where(completed_at: nil).first
            expect(Cohort::ReportExamResultJob).not_to receive(:set)
            progress.save!
        end

        it "should not do anything if the stream is not an exam" do
            progress = Lesson::StreamProgress.where(completed_at: nil).first
            progress.completed_at = Time.now
            allow(progress).to receive(:for_exam_stream?).and_return(false)
            expect(Cohort::ReportExamResultJob).not_to receive(:set)
            progress.save!
        end

        it "should work when in the schedule" do
            progress = Lesson::StreamProgress.where(completed_at: nil).first
            progress.completed_at = Time.now
            allow(progress).to receive(:for_exam_stream?).and_return(true)
            cohort = Cohort.first
            expect(progress.user).to receive(:relevant_cohort).and_return(cohort).at_least(1).times
            if progress.user.application_for_relevant_cohort.blank?
                CohortApplication.where(cohort_id: cohort.id, status: 'accepted').first.update(user_id: progress.user_id)
                progress.user.cohort_applications.reload
            end
            period = cohort.periods.first
            expect(cohort).to receive(:period_for_required_stream_locale_pack_id).with(progress.locale_pack_id).and_return(period)

            expected_target_time = cohort.end_time_for_period(period) + 1.day
            job_scheduler = "job_scheduler"
            expect(job_scheduler).to receive(:perform_later).with(progress.user_id, progress.locale_pack_id, {
                curriculum_context: "schedule"
            }.merge(cohort.event_attributes_for_period(period)))
            expect(Cohort::ReportExamResultJob).to receive(:set).with(wait_until: expected_target_time).and_return(job_scheduler)

            progress.save!
        end

        it "should work for specialization playlists" do
            progress = Lesson::StreamProgress.where(completed_at: nil).first
            progress.completed_at = Time.now
            allow(progress).to receive(:for_exam_stream?).and_return(true)
            cohort = Cohort.first
            expect(progress.user).to receive(:relevant_cohort).and_return(cohort).at_least(1).times
            expect(cohort).to receive(:period_for_required_stream_locale_pack_id).with(progress.locale_pack_id).and_return(nil)
            expect(cohort).to receive(:get_specialization_stream_locale_pack_ids).and_return([progress.locale_pack_id])

            expected_target_time = Time.now
            allow(Time).to receive(:now).and_return(expected_target_time)
            job_scheduler = "job_scheduler"
            expect(job_scheduler).to receive(:perform_later).with(progress.user_id, progress.locale_pack_id, {
                curriculum_context: "specialization_playlist"
            })
            expect(Cohort::ReportExamResultJob).to receive(:set).with(wait_until: expected_target_time).and_return(job_scheduler)

            progress.save!
        end

        it "should work for required playlists" do
            progress = Lesson::StreamProgress.where(completed_at: nil).first
            progress.completed_at = Time.now
            allow(progress).to receive(:for_exam_stream?).and_return(true)
            cohort = Cohort.first
            expect(progress.user).to receive(:relevant_cohort).and_return(cohort).at_least(1).times
            expect(cohort).to receive(:period_for_required_stream_locale_pack_id).with(progress.locale_pack_id).and_return(nil)
            expect(cohort).to receive(:get_specialization_stream_locale_pack_ids).and_return([])
            expect(cohort).to receive(:get_required_stream_locale_pack_ids).and_return([progress.locale_pack_id])

            expected_target_time = Time.now
            allow(Time).to receive(:now).and_return(expected_target_time)
            job_scheduler = "job_scheduler"
            expect(job_scheduler).to receive(:perform_later).with(progress.user_id, progress.locale_pack_id, {
                curriculum_context: "required_playlist"
            })
            expect(Cohort::ReportExamResultJob).to receive(:set).with(wait_until: expected_target_time).and_return(job_scheduler)

            progress.save!
        end

        it "should work for legacy users for the library" do
            progress = Lesson::StreamProgress.where(completed_at: nil).first
            progress.completed_at = Time.now
            allow(progress).to receive(:for_exam_stream?).and_return(true)
            cohort = Cohort.first
            expect(progress.user).to receive(:relevant_cohort).and_return(cohort).at_least(1).times
            expect(cohort).to receive(:period_for_required_stream_locale_pack_id).with(progress.locale_pack_id).and_return(nil)
            expect(cohort).to receive(:get_specialization_stream_locale_pack_ids).and_return([])
            expect(cohort).to receive(:get_required_stream_locale_pack_ids).and_return([])

            expected_target_time = Time.now
            allow(Time).to receive(:now).and_return(expected_target_time)
            job_scheduler = "job_scheduler"
            expect(job_scheduler).to receive(:perform_later).with(progress.user_id, progress.locale_pack_id, {
                curriculum_context: "library"
            })
            expect(Cohort::ReportExamResultJob).to receive(:set).with(wait_until: expected_target_time).and_return(job_scheduler)

            progress.save!
        end
    end

    describe "generate_certificate_image!" do

        it "should not replace image if the new one is the same as the old, and should delete the newly created one" do
            allow_any_instance_of(S3Asset).to receive(:formats).and_return({'original' => {'url' => 'something'}})
            sp = Lesson::StreamProgress.first
            s3_assets = S3Asset.limit(2).to_a
            sp.certificate_image = s3_assets[0]
            expect(Lesson::StreamProgress).to receive(:generate_certificate_image).and_return(s3_assets[1])

            expect(s3_assets[1]).to receive(:delete)
            expect(sp).not_to receive(:certificate_image=)
            expect(sp.generate_certificate_image!).to eq(false)
        end

        it "should replace image if the new one is not the same as the old" do
            sp = Lesson::StreamProgress.first
            s3_assets = S3Asset.limit(2).to_a
            sp.certificate_image = s3_assets[0]
            expect(Lesson::StreamProgress).to receive(:generate_certificate_image).and_return(s3_assets[1])

            expect(s3_assets[1]).not_to receive(:delete)
            expect(s3_assets[0]).to receive(:destroy)
            expect(sp).to receive(:certificate_image=).with(s3_assets[1]).and_call_original
            expect(sp.generate_certificate_image!).to eq(true)
        end

        it "should pass the exam flag accordingly" do
            expect_any_instance_of(Lesson::StreamProgress).to receive(:stream_within_launch_window_and_time_limit?).and_return(true)
            stream = lesson_streams(:exam_stream)
            stream.publish!  # ensure it is published
            expect(CertificateGenerator).to receive(:generate_asset).with(@user, hash_including(:exam => true))
            sp = Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => stream.locale_pack_id,
                :user_id => @user.id
            })
            expect(sp.generate_certificate_image!).to eq(true)
        end

        it "should ignore access denied error in dev mode" do

            sp = Lesson::StreamProgress.first
            s3_assets = S3Asset.limit(2).to_a
            sp.certificate_image = s3_assets[0]

            expect_any_instance_of(Lesson::StreamProgress).to receive(:ignore_access_denied_errors_while_creating_cert?)
                .and_return(true)
            expect(Lesson::StreamProgress).to receive(:generate_certificate_image) do |*args|
                raise Aws::S3::Errors::AccessDenied.new("msg", nil)
            end
            expect {
                sp = Lesson::StreamProgress.create_or_update!({
                    :locale_pack_id => sp.locale_pack_id,
                    :user_id => @user.id
                })
                }.not_to raise_error

            expect(sp).not_to receive(:certificate_image=)
            expect(sp.generate_certificate_image!).to eq(true)
        end

    end


    describe "get_published_stream_json" do

        before(:each) do
            LessonProgress.delete_all
            Lesson::StreamProgress.delete_all
            @es_stream = Lesson::Stream.all_published.where(locale: 'es').detect do |es_stream|
                @en_stream = Lesson::Stream.all_published.where(locale: 'en', locale_pack_id: es_stream.locale_pack_id).first
            end
            @locale_pack_id = @es_stream.locale_pack_id

            @stream_progress = Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => @locale_pack_id,
                :lesson_bookmark_id => SecureRandom.uuid,
                :user_id => @user.id
            })
        end

        it "should return english stream for an english user" do
            expect(Lesson::StreamProgress.get_published_stream_json(@stream_progress)['id']).to eq(@en_stream.id)
        end

        it "should return english stream for a spanish user when no spanish stream is available" do
            @stream_progress.user.pref_locale = 'es'
            @es_stream.unpublish!
            RefreshMaterializedContentViews.refresh(true)
            expect(Lesson::StreamProgress.get_published_stream_json(@stream_progress)['id']).to eq(@en_stream.id)
        end

        it "should return spanish stream for a spanish user when spanish stream is available" do
            @stream_progress.user.pref_locale = 'es'
            expect(Lesson::StreamProgress.get_published_stream_json(@stream_progress)['id']).to eq(@es_stream.id)
        end

    end

    describe "on_last_lesson / all_lessons_complete?" do
        before(:each) do
            LessonProgress.delete_all
            Lesson::StreamProgress.delete_all
            @stream = lesson_streams(:published_stream)
            lessons = @stream.lessons

            # sanity checks.
            # the first lesson is not published. all the others are.
            # the first one should be ignored when calculating on_last_lesson
            expect(lessons[0].has_published_version?).to be(false)
            expect(lessons.to_a.slice(1, 9999).map(&:has_published_version?).uniq).to eq([true])
            # Note: the first lesson in
            @lessons = @stream.lessons.to_a.slice(1, 9999)
        end

        it "should work" do
            # note: were assuming lesson progress gets logged before stream progress
            # which happens at the controller level
            LessonProgress.create_or_update!({
                :locale_pack_id => @lessons[0].locale_pack_id,
                :user_id => @user.id,
                :complete => true
            })
            stream_progress = Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => @stream.locale_pack_id,
                :lesson_bookmark_id => @lessons[0].id,
                :user_id => @user.id
            })
            expect(stream_progress.on_last_lesson).to be(false)
            expect(stream_progress.all_lessons_complete?).to be(false)
            @lessons.to_a.slice(0, @lessons.size - 1).each do |lesson|
                LessonProgress.create_or_update!({
                    :locale_pack_id => lesson.locale_pack_id,
                    :user_id => @user.id,
                    :complete => true
                })
            end
            expect(stream_progress.on_last_lesson).to be(true)
            expect(stream_progress.all_lessons_complete?).to be(false)

            LessonProgress.create_or_update!({
                :locale_pack_id => @lessons.last.locale_pack_id,
                :user_id => @user.id,
                :complete => true
            })
            expect(stream_progress.all_lessons_complete?).to be(true)
        end
    end

    describe "clear_all_progress" do

        it "clears progress for user" do

            # progress for this stream
            Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => @streams[0].locale_pack_id,
                :lesson_bookmark_id => @lessons[0].id,
                :user_id => @user.id
            })

            # progress for another stream
            Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => @streams[1].locale_pack_id,
                :lesson_bookmark_id => @lessons[0].id,
                :user_id => @user.id
            })

            expect(Lesson::StreamProgress.where(user_id: @user.id).length).not_to eq(0)
            Lesson::StreamProgress.clear_all_progress(@user.id)
            expect(Lesson::StreamProgress.where(user_id: @user.id).length).to eq(0)
        end
    end

    describe "as_json" do
        before(:each) do
            Lesson::StreamProgress.delete_all
            @stream_progress_1 = Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => @streams[0].locale_pack_id,
                :lesson_bookmark_id => @lessons[0].id,
                :user_id => @user.id
            })

            # progress for another stream
            Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => @streams[1].locale_pack_id,
                :lesson_bookmark_id => @lessons[0].id,
                :user_id => @user.id
            })

            # progress for another user
            another_user = User.where("id != '#{@user.id}'").first
            @completed_stream_progress = Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => @streams[0].locale_pack_id,
                :lesson_bookmark_id => @lessons[0].id,
                :user_id => another_user.id,
                :complete => true
            })

        end

        it "should serialize a single event" do
            stream_progress = @completed_stream_progress
            stream_progress.update_columns(waiver: 'foo waiver')
            json = stream_progress.as_json
            expect(json[:user_id]).to eq(stream_progress.user_id)
            expect(json[:locale_pack_id]).to eq(stream_progress.locale_pack_id)
            expect(json[:started_at]).to be(stream_progress.started_at.to_timestamp)
            expect(json[:completed_at]).to be(stream_progress.completed_at.to_timestamp)
            expect(json[:complete]).to be(true)
            expect(json[:last_progress_at]).to be(stream_progress.last_progress_at.to_timestamp)
            expect(json[:waiver]).to eq('foo waiver')
        end

        it "should match the results from ToJsonForApiParams" do
            stream_progress = @completed_stream_progress

            actual = stream_progress.as_json.deep_stringify_keys
            expected = Lesson::Stream::ToJsonFromApiParams.new({
                user: stream_progress.user,
                filters: {locale_pack_id: stream_progress.locale_pack_id},
                include_progress: true
            }).to_a.first['lesson_streams_progress']

            # there is some extra stuff in the certificate json generated in ruby. This
            # probably isn't important, but I don't want to risk messing with it right now
            expect(actual['certificate_image']['dimensions']).to eq(expected['certificate_image']['dimensions'])
            expect(actual['certificate_image']['formats']).to eq(expected['certificate_image']['formats'])
            actual.delete('certificate_image')
            expected.delete('certificate_image')
            expect(actual).to eq(expected)

        end

    end

    describe "destroy" do

        it "should log a reset event" do
            stream_progress = Lesson::StreamProgress.joins(:user).where('users.pref_locale' => 'en').first
            stream = stream_progress.lesson_streams.where(locale: 'en').first.published_version
            uuid = SecureRandom.uuid
            expect(SecureRandom).to receive(:uuid).and_return(uuid).at_least(1).times
            expect(Event).to receive(:create_server_event!).with(uuid, stream_progress.user_id, 'lesson:stream:reset', {
                lesson_stream_id: stream['id'],
                label: stream['id'],
                lesson_stream_title: stream.title,
                lesson_stream_complete: !!stream_progress.completed_at,
                lesson_stream_version_id: stream.version_id
            }).and_call_original
            stream_progress.destroy!
        end

    end

    describe "official_test_score" do
        it "should be set when completing an exam stream" do
            stream = lesson_streams(:exam_stream)
            expect_any_instance_of(Lesson::StreamProgress).to receive(:raise_if_exam_closed)
            stream_progress = Lesson::StreamProgress.create_or_update!(
                locale_pack_id: stream.locale_pack_id,
                user_id: User.left_outer_joins(:lesson_streams_progresses).where("lesson_streams_progress.id is null").first.id,
                lesson_bookmark_id: stream.lessons[0].id,
            )

            expect(stream_progress).to receive(:calculate_official_test_score_from_lesson_progress).and_return(0.42)
            stream_progress.update_attribute(:completed_at, Time.now)
            expect(stream_progress.reload.official_test_score).to eq(0.42)
        end
    end

    describe "calculate_official_test_score_from_lesson_progress" do

        it "should be nil if not completed" do
            stream = lesson_streams(:exam_stream)
            expect_any_instance_of(Lesson::StreamProgress).to receive(:raise_if_exam_closed)
            stream_progress = Lesson::StreamProgress.create_or_update!(
                locale_pack_id: stream.locale_pack_id,
                user_id: User.left_outer_joins(:lesson_streams_progresses).where("lesson_streams_progress.id is null").first.id,
                lesson_bookmark_id: stream.lessons[0].id,
            )
            stream_progress.completed_at = nil
            expect(stream_progress).not_to receive(:get_lesson_progresses)
            expect(stream_progress.calculate_official_test_score_from_lesson_progress).to be_nil
        end

        it "should be nil if not an exam stream" do
            stream = lesson_streams(:exam_stream)
            stream_progress = Lesson::StreamProgress.where.not(completed_at: nil).first
            expect(stream_progress).to receive(:for_exam_stream?).and_return(false)
            expect(stream_progress).not_to receive(:get_lesson_progresses)
            expect(stream_progress.calculate_official_test_score_from_lesson_progress).to be_nil
        end

        it "should work" do
            stream = lesson_streams(:exam_stream)
            user = User.left_outer_joins(:lesson_streams_progresses).where("lesson_streams_progress.id is null").first
            expect_any_instance_of(Lesson::StreamProgress).to receive(:raise_if_exam_closed)
            stream_progress = Lesson::StreamProgress.create_or_update!(
                locale_pack_id: stream.locale_pack_id,
                user_id: user.id,
                lesson_bookmark_id: stream.lessons[0].id,
                complete: true
            )

            lesson_locale_pack_ids = Lesson::Stream.published_lesson_locale_pack_ids(stream_progress.locale_pack_id)
            expect(lesson_locale_pack_ids).not_to be_empty

            lesson_locale_pack_ids.each do |lesson_locale_pack_id|
                LessonProgress.create_or_update!(
                    locale_pack_id: lesson_locale_pack_id,
                    user_id: user.id,
                    best_score: 0.42
                )
            end

            expect(stream_progress.calculate_official_test_score_from_lesson_progress).to eq(0.42)
        end

    end

    describe "curriculum_event_attributes" do
        it "should include learner_projects" do
            user = users(:accepted_mba_cohort_user)
            allow(user.relevant_cohort.published_version.program_type_config).to receive(:supports_cohort_level_projects?).and_return(true)
            project = LearnerProject.first
            user.relevant_cohort.published_version.update_columns(learner_project_ids: [project.id])
            # Normally the cache for published_version would have been busted by republishing the above change; fake it here
            SafeCache.clear
            stream = lesson_streams(:en_item)
            stream_progress = Lesson::StreamProgress.create_or_update!(
                locale_pack_id: stream.locale_pack_id,
                user_id: user.id,
                lesson_bookmark_id: stream.lessons[0].id,
                complete: true
            )
            hash = stream_progress.curriculum_event_attributes
            expect(hash[:learner_projects]).to eq([project.as_json])
        end
    end

end
