require 'spec_helper'

describe Lesson::Content::FrameList do

    def valid_attrs(overrides = {})
        {
            lesson_type: 'frame_list',
            frames: [{
                frame_type: 'componentized'
            }]
        }.merge(overrides).with_indifferent_access
    end

    it 'should include frames' do
        content = Lesson::Content::FrameList.load(valid_attrs)
        raise if content.frames[0].class != Lesson::Content::FrameList::Frame::Componentized
        expect(content.frames[0].class).to be(Lesson::Content::FrameList::Frame::Componentized)
    end

    it 'should validate frames if pinned is true' do
        content = Lesson::Content::FrameList.load(valid_attrs)
        expect(content).to receive(:pinned?).and_return(true)
        frame = content.frames[0]
        allow(frame).to receive(:valid?) do
            frame.errors.add(:property, "has some error")
            false
        end
        expect(content.valid?).to be(false)
        expect(content.errors.full_messages.include?("Frame 1 property has some error")).to be(true), content.errors.full_messages.inspect
    end

    it 'should not validate frames if pinned is false' do
        content = Lesson::Content::FrameList.load(valid_attrs)
        expect(content).not_to receive(:validate_frames)
        expect(content).to receive(:pinned?).and_return(false)
        content.valid?
    end

    it 'should validate frames ids' do
        invalid_attrs = valid_attrs.merge({
            frames: [{
                        id: 'duplicate',
                        frame_type: 'componentized'
                    },
                    {
                        id: 'duplicate',
                        frame_type: 'componentized'
                    }]
        })
        content = Lesson::Content::FrameList.load(invalid_attrs)
        expect(content).to receive(:pinned?).and_return(false)
        expect(content.valid?).to be(false)
        expect(content.errors.full_messages.include?("Frames Duplicate frame IDs found in lesson: duplicate")).to be(true), content.errors.full_messages.inspect
    end

    describe 'save_warnings' do

        it 'should show warnings if there are validation failures' do
            content = Lesson::Content::FrameList.load(valid_attrs)
            frame = content.frames[0]
            allow(frame).to receive(:valid?) do
                frame.errors.add(:property, "has some error")
                false
            end
            expect(content.save_warnings.include?('Frame 1 property has some error')).to be(true), content.save_warnings.inspect
        end

        it 'should be empty if there are no validation failures' do
            content = Lesson::Content::FrameList.load(valid_attrs)
            expect(content).to receive(:validate_frames)
            expect(content.save_warnings).to eq([])
        end

    end

end