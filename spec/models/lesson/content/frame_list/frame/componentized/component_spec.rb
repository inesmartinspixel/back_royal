require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component do

	before(:each) do
		@frame = Lesson::Content::FrameList::Frame::Componentized.load
		@frame.components = []
	end

	describe 'references' do

		class Lesson::Content::FrameList::Frame::Componentized::Component::ReferencesOne < Lesson::Content::FrameList::Frame::Componentized::Component
			references('something').through('something_id')
		end

		class Lesson::Content::FrameList::Frame::Componentized::Component::ReferencesOneSubklass < Lesson::Content::FrameList::Frame::Componentized::Component::ReferencesOne
			references('something_else').through('something_else_id')
		end

		class Lesson::Content::FrameList::Frame::Componentized::Component::ReferencesMany < Lesson::Content::FrameList::Frame::Componentized::Component
			references('somethings').through('something_ids')
		end

		it 'should support references to single components' do

			# for some reason push works but regular assignment does not
			@frame.components.push Lesson::Content::FrameList::Frame::Componentized::Component::ReferencesOne.load({
				id: '0', something_id: '1'
			})
			@frame.components.push Lesson::Content::FrameList::Frame::Componentized::Component.load({
				id: '1'
			})
			expect(@frame.components[0].something).to be(@frame.components[1])
		end

		it 'should support references to lists of  components' do
			@frame.components.push Lesson::Content::FrameList::Frame::Componentized::Component::ReferencesMany.load({id: '0', something_ids: ['1', '2']})
			@frame.components.push Lesson::Content::FrameList::Frame::Componentized::Component.load({id: '1'})
			@frame.components.push Lesson::Content::FrameList::Frame::Componentized::Component.load({id: '2'})

			expect(@frame.components[0].somethings).to eq([
				@frame.components[1],
				@frame.components[2]
			])
		end

		it 'should support inheriting references' do

			@frame.components.push Lesson::Content::FrameList::Frame::Componentized::Component::ReferencesOne.load({id: '0', component_type: 'my_component_klass'})
			@frame.components.push Lesson::Content::FrameList::Frame::Componentized::Component::ReferencesOneSubklass.load({id: '1', something_id: '0', something_else_id: '0', component_type: 'subclass'})

			expect(@frame.components[1].something).to be(@frame.components[0])
			expect(@frame.components[1].something_else).to be(@frame.components[0])

			# the something_else reference should only be on the subclass
			expect{
				@frame.components.push Lesson::Content::FrameList::Frame::Componentized::Component::ReferencesOne.load({id: '0', something_else_id: '1', component_type: 'my_component_klass'})
			}.to raise_error(NoMethodError)
		end

	end

    it 'should be invalid if a reference cannot be resolved' do
    	component = Lesson::Content::FrameList::Frame::Componentized::Component::ReferencesOne.load({
			id: '0', something_id: '1'
		})
		@frame.components.push(component)
		expect(component.valid?).to be(false)
		expect(component.errors.full_messages.include?("Something cannot be dereferenced (no component found for \"1\")")).to be(true), component.errors.full_messages.inspect
    end

    it 'should be invalid if a reference in a list cannot be resolved' do
    	component = Lesson::Content::FrameList::Frame::Componentized::Component::ReferencesMany.load({
			id: '0', something_ids: ['1']
		})
		@frame.components.push(component)
		expect(component.valid?).to be(false)
		expect(component.errors.full_messages.include?("Something ids 1 cannot be dereferenced  (no component found for \"1\")")).to be(true), component.errors.full_messages.inspect
    end

    it 'should treat the id key normally' do
    	component = Lesson::Content::FrameList::Frame::Componentized::Component.load({
			id: '0'
		})
		expect(component['id']).to eq('0')
		expect(component.id).to eq('0')
    end

end