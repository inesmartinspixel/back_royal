require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Challenges do



    describe 'with compose_blanks template' do

    	before(:each) do
	        @frame = Lesson::Content::FrameList::Frame::Componentized.load
	        @frame.components = []
	        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})
	        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'layout', component_type: 'ComponentizedFrame.Layout'})

	        @challenge1 = Lesson::Content::FrameList::Frame::Componentized::Component.load({
        		id: 'challenge1',
        		component_type: 'ComponentizedFrame.UserInputChallenge',
        		editor_template: 'basic_user_input'
        	})
	        @frame.components << @challenge1

	        @challenge2 = Lesson::Content::FrameList::Frame::Componentized::Component.load({
        		id: 'challenge2',
        		component_type: 'ComponentizedFrame.UserInputChallenge',
        		editor_template: 'basic_user_input'
        	})
	        @frame.components << @challenge2

	        @text  =  Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'text',
	        	component_type: 'ComponentizedFrame.Text',
	        	text: 'a [blank] and [another]'})
	        @frame.components << @text

	        # mock out validations for references that are tested elsewhere
	        @frame.components.each do |component|
	            allow(component).to receive(:valid?).and_return(true)
	        end
	    end

	    it 'should be valid if everything is awesome' do
	    	component = load
	    	expect(component.valid?).to be(true), component.errors.full_messages.inspect
	    end

	    it 'should be invalid unless it has expected behaviors' do
	    	component = load({
            	behaviors: {}
        	})
	        expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Goto next on challenge complete behavior must be included"), component.errors.full_messages.inspect
	        expect(component.errors.full_messages).to include("Complete on all challenges complete behavior must be included"), component.errors.full_messages.inspect
	    end

	    # we can assume that this covers both nil and invalid cases, since
	    # that's how validates_reference works
	    it 'should be invalid with a non-text component in sharedContentForText' do
	    	component = load({
            	shared_content_for_text_id: 'image'
        	})
        	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Shared content for text must be an instance of Text but is Image"), component.errors.full_messages.inspect
	    end

	    it 'should be invalid unless all challenges have basic_user_input editor template' do
	    	@challenge1.editor_template = nil
	    	component = load
	    	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Challenge 1 must have editor template \"basic_user_input\" but has nil"), component.errors.full_messages.inspect
	    end

    	# we can assume that this covers both nil and invalid cases, since
	    # that's how validates_reference works
	    it 'should be invalid with non-image component in sharedContentForFirstImage' do
	    	component = load({
            	shared_content_for_image_id: 'text'
        	})
        	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Shared content for first image must be an instance of Image but is Text"), component.errors.full_messages.inspect
	    end

        # we can assume that this covers both nil and invalid cases, since
        # that's how validates_reference works
        it 'should be invalid with non-image component in sharedContentForSecondImage' do
            component = load({
                shared_content_for_image_2_id: 'text'
            })
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Shared content for second image must be an instance of Image but is Text"), component.errors.full_messages.inspect
        end

    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.Challenges",
            editor_template: "compose_blanks",
            layout_id: 'layout',
            behaviors: {
            	GotoNextOnChallengeComplete: {},
            	CompleteOnAllChallengesComplete: {}
            },
            challenge_ids: ['challenge1', 'challenge2'],
            shared_content_for_text_id: 'text',
            shared_content_for_image_id: 'image'
        }.deep_merge(overrides)
    end

end