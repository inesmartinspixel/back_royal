require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::ContinueButton::ChallengesContinueButton do
    
    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'vanilla'})
        @frame.components << (@challenge = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'challenges', 
            component_type: 'ComponentizedFrame.Challenges'
        }) )
        # mock out validations for references that are tested elsewhere
        allow(@challenge) .to receive(:valid?).and_return(true)
    end

    it 'should be valid if everything is awesome' do
        component = load
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::ContinueButton::ChallengesContinueButton)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-Challenges component as target component' do
        component = load({
            target_component_id: 'vanilla'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Target component must be an instance of Challenges but is Component"), component.errors.full_messages.inspect
    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    
    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.ChallengesContinueButton",
            target_component_id: "challenges"
        }.deep_merge(overrides)
    end 
    

end