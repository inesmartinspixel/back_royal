require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::AnswerMatcher::MatchesExpectedText do

    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []

        # mock out validations for references that are tested elsewhere
        allow_message_expectations_on_nil
        allow(@answer).to receive(:valid?).and_return(true);
    end

    it 'should be valid if everything is awesome' do
        component = load
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::AnswerMatcher::MatchesExpectedText)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    it 'should be invalid without expectedText' do
        component = load({
            expectedText: ''
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Expectedtext can't be blank"), component.errors.full_messages.inspect
    end


    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            id: '1',
            component_type: "ComponentizedFrame.MatchesExpectedText",
            expectedText: 'answer'
        }.deep_merge(overrides)
    end


end