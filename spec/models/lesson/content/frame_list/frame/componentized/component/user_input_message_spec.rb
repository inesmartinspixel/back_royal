require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::UserInputMessage do

    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'text', component_type: 'ComponentizedFrame.Text'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'challenge', component_type: 'ComponentizedFrame.UserInputChallenge'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})

        # mock out validations for references that are tested elsewhere
        @frame.components.each do |component|
            allow(component).to receive(:valid?).and_return(true)
        end
    end

    it 'should be valid if everything is awesome' do
        component = load(valid_attrs)
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::UserInputMessage)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-UserInputChallenge component as challenge' do
        component = load({
            challenge_id: 'image'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Challenge must be an instance of UserInputChallenge but is Image"), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-Text component as message text' do
        component = load({
            message_text_id: 'image'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Message text must be an instance of Text but is Image"), component.errors.full_messages.inspect
    end

    it 'should be invalid with show_on_correct_answer != true' do
        component = load({
            show_on_correct_answer: false
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Show on correct answer is not included in the list"), component.errors.full_messages.inspect
    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.UserInputMessage",
            message_text_id: 'text',
            challenge_id: 'challenge',
            show_on_correct_answer: true
        }.deep_merge(overrides)
    end


end