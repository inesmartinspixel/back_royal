require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::InteractiveCards do

    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []

        @frame.components << (@overlay = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'overlay',
            component_type: 'ComponentizedFrame.ComponentOverlay',
            overlay_component_ids: ['challenge_overlay_blank']
        }) )

         @frame.components << (@challenge_overlay_blank = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'challenge_overlay_blank',
            component_type: 'ComponentizedFrame.ChallengeOverlayBlank'
        }) )

        @frame.components << (@challenges_component = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'challenges',
            component_type: 'ComponentizedFrame.Challenges'
        }) )

        # mock out validations for references that are tested elsewhere
        @frame.components.each do |component|
            allow(component).to receive(:valid?).and_return(true)
        end
    end

    it 'should be valid if everything is awesome' do
        component = load(valid_attrs)
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::InteractiveCards)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with no overlays' do
        component = load({
            overlay_ids: []
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Overlays must have at least one element"), component.errors.full_messages.inspect
    end

    it 'should be invalid with non-ComponentOverlay as overlay' do
        component = load({
            overlay_ids: ['challenges']
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Overlay 1 must be an instance of ComponentOverlay but is Challenges"), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-Challenges component as challenge_component as overlay' do
        component = load({
            challenges_component_id: ['overlay']
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Challenges component 1 must be an instance of Challenges but is ComponentOverlay"), component.errors.full_messages.inspect
    end

    it 'should be invalid without challenge overlay blanks as the overlayComponents' do
        component = load
        @overlay.overlay_components = [@challenges_component]
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Overlay component 1 must be an instance of ChallengeOverlayBlank but is Challenges"), component.errors.full_messages.inspect
    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.InteractiveCards",
            overlay_ids: ['overlay'],
            challenges_component_id: 'challenges',
            force_card_height: true,
            wide: nil
        }.deep_merge(overrides)
    end


end