require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Challenge::MultipleChoiceChallenge do
    
    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})
        @frame.components << (@answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'answer_list', 
            component_type: 'ComponentizedFrame.AnswerList',
            answer_ids: ['answer1', 'answer2']
        }) )
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'answer1', 
            component_type: 'ComponentizedFrame.SelectableAnswer'
        })
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'answer2', 
            component_type: 'ComponentizedFrame.SelectableAnswer'
        })
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'similar_to_selectable_answer', 
            component_type: 'ComponentizedFrame.SimilarToSelectableAnswer',
            answer_id: 'answer1'
        })
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'similar_to_selectable_answer', 
            component_type: 'ComponentizedFrame.SimilarToSelectableAnswer',
            answer_id: 'answer2'
        })
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'vanilla_answer_matcher', component_type: 'ComponentizedFrame.AnswerMatcher'})
        @frame.components << (@validator = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'validator', 
            component_type: 'ComponentizedFrame.ChallengeValidator',
            behaviors: {
                HasAllExpectedAnswers: true,
                HasNoUnexpectedAnswers: true
            },
            expected_answer_matcher_ids: ['similar_to_selectable_answer']
        }) )

        # mock out validations for references that are tested elsewhere
        @frame.components.each do |component|
            allow(component).to receive(:valid?).and_return(true)
        end
    end

    describe 'with check_many editor_template' do

        it 'should be valid if everything is awesome' do
            component = load(valid_attrs)
            expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Challenge::MultipleChoiceChallenge)
            expect(component.valid?).to eq(true), component.errors.full_messages.inspect
        end

        it 'should be invalid unless it has expected behaviors' do
            component = load({
                behaviors: {}
            })
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Immediate validation behavior must be included"), component.errors.full_messages.inspect
            expect(component.errors.full_messages).to include("Complete on correct behavior must be included"), component.errors.full_messages.inspect
            expect(component.errors.full_messages).to include("Reset answers on activated behavior must be included"), component.errors.full_messages.inspect
            expect(component.errors.full_messages).to include("Show correct styling behavior must be included"), component.errors.full_messages.inspect
            expect(component.errors.full_messages).to include("Flash incorrect styling behavior must be included"), component.errors.full_messages.inspect
            expect(component.errors.full_messages).to include("Clear messages on answer select behavior must be included"), component.errors.full_messages.inspect

            # expect it to not be present
            expect(component.errors.full_messages).not_to include("Disallow multiple select behavior cannot be included"), component.errors.full_messages.inspect
        end


        # there are actually a bunch of ways to not have a single correct answer.  They 
        # are all wrapped up in validates_single_correct_answer, so we just need to check 
        # one of them here, and then we can assume the rest all work
        it 'should be invalid unless there is at least one correct answer' do
            @validator.expected_answer_matcher_ids = []
            component = load
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Validator must have at least one expected answer matcher"), component.errors.full_messages.inspect
        end

    end



    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end
    
    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.MultipleChoiceChallenge",
            editor_template: 'check_many',
            answer_list_id: 'answer_list',
            validator_id: 'validator',
            behaviors: {
                ImmediateValidation: {},
                CompleteOnCorrect: {},
                ResetAnswersOnActivated: {},
                ShowCorrectStyling: {},
                FlashIncorrectStyling: {},
                ClearMessagesOnAnswerSelect: {}
            }
        }.deep_merge(overrides)
    end 
    

end