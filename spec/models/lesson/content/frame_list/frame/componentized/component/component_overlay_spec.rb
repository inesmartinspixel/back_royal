require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::ComponentOverlay do

    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        @frame.components << (@text1 = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'text1',
            component_type: 'ComponentizedFrame.Text',
            text: 'text'
        }))
        @frame.components << (@text2 = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'text2',
            component_type: 'ComponentizedFrame.Text',
            text: 'text'
        }))
        @frame.components << (@text3 = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'text3',
            component_type: 'ComponentizedFrame.Text',
            text: 'text'
        }))


        # mock out validations for references that are tested elsewhere
        @frame.components.each do |component|
            allow(component).to receive(:valid?).and_return(true)
        end
    end

    it 'should be valid if everything is awesome' do
        component = load
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::ComponentOverlay)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    # Continue to support the legacy main_component for when duplicating old content
    # FIXME: https://trello.com/c/e5vFpRcK
    it 'should still work with a main_component instead of a text or image component' do
        component = load({
            text_id: nil,
            image_id: nil,
            main_component_id: 'text1'
        })
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::ComponentOverlay)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    it 'should be invalid without either a text compoment or image component' do
        component = load({
            text_id: nil,
            image_id: nil
        })
        expect(component.valid?).to eq(false)
        expect(component.errors.full_messages).to include("Text or image must be set"), component.errors.full_messages.inspect
    end

    it 'should be invalid without any overlays' do
        component = load({
            overlay_component_ids: [],
            overlayOptions: {}
        })
        expect(component.valid?).to eq(false)
        expect(component.errors.full_messages).to include("Overlay components must not be empty"), component.errors.full_messages.inspect
    end

    it 'should be invalid if no overlayOptions entry for an overlay_component' do
        component = load({
            overlayOptions: {}
        })
        expect(component.valid?).to eq(false)
        expect(component.errors.full_messages).to include("Overlayoptions must have one entry for each overlay component ([] != [\"text2\", \"text3\"])"), component.errors.full_messages.inspect
    end

    it 'should be invalid if no x or y value in overlayOptions' do
        component = load({
            overlayOptions: {
                text2: {
                }
            }
        })
        expect(component.valid?).to eq(false)
        expect(component.errors.full_messages).to include("Overlayoptions text2 x cannot be blank"), component.errors.full_messages.inspect
        expect(component.errors.full_messages).to include("Overlayoptions text2 y cannot be blank"), component.errors.full_messages.inspect
    end

    it 'should be invalid if non-number x or y value in overlayOptions' do
        component = load({
            overlayOptions: {
                text2: {
                    x: 'NAN'
                }
            }
        })
        expect(component.valid?).to eq(false)
        expect(component.errors.full_messages).to include("Overlayoptions text2 x must be a number but was \"NAN\""), component.errors.full_messages.inspect
    end

    it 'should be invalid if no units value in overlayOptions' do
        component = load({
            overlayOptions: {
                text2: {
                }
            }
        })
        expect(component.valid?).to eq(false)
        expect(component.errors.full_messages).to include("Overlayoptions text2 units must be one of [\"px\", \"%\"] but is nil"), component.errors.full_messages.inspect
    end

    it 'should be invalid if invalid units value in overlayOptions' do
        component = load({
            overlayOptions: {
                text2: {
                    units: 'NOPE'
                }
            }
        })
        expect(component.valid?).to eq(false)
        expect(component.errors.full_messages).to include("Overlayoptions text2 units must be one of [\"px\", \"%\"] but is \"NOPE\""), component.errors.full_messages.inspect
    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.ComponentOverlay",
            text_id: 'text1',
            background_color: 'beige',
            overlay_component_ids: ['text2', 'text3'],
            overlayOptions: {
                text2: {
                    x: 0,
                    y: 0,
                    units: 'px'
                },
                text3: {
                    x: 0,
                    y: 0,
                    units: 'px'
                }
            }
        }.deep_merge(overrides).with_indifferent_access
    end


end