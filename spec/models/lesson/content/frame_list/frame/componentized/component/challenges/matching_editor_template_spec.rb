require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Challenges do



    describe 'with matching template' do

    	before(:each) do
	        @frame = Lesson::Content::FrameList::Frame::Componentized.load
	        @frame.components = []
	        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})
	        @frame.components << (@matching_board = Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'matching_board', 
	        	component_type: 'ComponentizedFrame.MatchingBoard',
	        	challenges_component_id: 'challenges',
	        	answer_list_id: 'shared_answer_list',
	        	matching_challenge_button_ids: ['matching_challenge_button']
	        }))
	        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'layout', component_type: 'ComponentizedFrame.Layout'})
	        @frame.components << (@shared_answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'shared_answer_list', 
	        	component_type: 'ComponentizedFrame.AnswerList'
	        }) )
	        @frame.components << (@distinct_answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'distinct_answer_list', 
	        	component_type: 'ComponentizedFrame.AnswerList'
	        }) )
	        @frame.components << (@another_answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'another_answer_list', 
	        	component_type: 'ComponentizedFrame.AnswerList'
	        }) )

	        @challenge = Lesson::Content::FrameList::Frame::Componentized::Component.load({
        		id: 'challenge', 
        		component_type: 'ComponentizedFrame.MultipleChoiceChallenge', 
        		editor_template: 'matching',
        		answer_list_id: 'shared_answer_list'
        	})
	        @frame.components << @challenge

	        @challenge_copy = Lesson::Content::FrameList::Frame::Componentized::Component.load({
        		id: 'challenge_copy', 
        		component_type: 'ComponentizedFrame.MultipleChoiceChallenge', 
        		editor_template: 'matching',
        		answer_list_id: 'shared_answer_list'
        	})
	        @frame.components << @challenge_copy


	        @text  =  Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'text', 
	        	component_type: 'ComponentizedFrame.Text', 
	        	text: 'main text'})
	        @frame.components << @text

	        @matching_challenge_button  =  Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'matching_challenge_button', 
	        	component_type: 'ComponentizedFrame.MatchingChallengeButton'})
	        @frame.components << @matching_challenge_button

	        @matching_challenge_button_2  =  Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'matching_challenge_button_2', 
	        	component_type: 'ComponentizedFrame.MatchingChallengeButton'})
	        @frame.components << @matching_challenge_button_2

	        # mock out validations for references that are tested elsewhere
	        @frame.components.each do |component|
	            allow(component).to receive(:valid?).and_return(true)
	        end
	    end

	    it 'should be valid if everything is awesome' do
	        component = load
	        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Challenges)
	        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
	    end

	    it 'should be invalid unless it has expected behaviors' do
	    	component = load({
            	behaviors: {}
        	})
	        expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Goto next on challenge complete behavior must be included"), component.errors.full_messages.inspect
	        expect(component.errors.full_messages).to include("Complete on all challenges complete behavior must be included"), component.errors.full_messages.inspect
	    end

	    # we can assume that this covers both nil and invalid cases, since
	    # that's how validates_reference works
	    it 'should be invalid with a non-text component in sharedContentForText' do
	    	component = load({
            	shared_content_for_text_id: 'image'
        	})
        	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Shared content for text must be an instance of Text but is Image"), component.errors.full_messages.inspect
	    end

	    it 'should be invalid unless all challenges have matching editor template' do
	    	@challenge.editor_template = nil
	    	component = load
	    	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Challenge 1 must have editor template \"matching\" but has nil"), component.errors.full_messages.inspect
	    end

    	# we can assume that this covers both nil and invalid cases, since
	    # that's how validates_reference works
	    it 'should be invalid with non-MatchingBoard component in sharedContentForInteractiveImage' do
	    	component = load({
            	shared_content_for_interactive_image_id: 'text'
        	})
        	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Shared content for interactive image must be an instance of MatchingBoard but is Text"), component.errors.full_messages.inspect
	    end

	    # some of these validations could have been moved down into 
	    # MatchingBoard, since it is always supposed to work this way,
	    # but it is more similar to blanks_on_image to leave them here,
	    # and it allows us to make the MatchingBoard test be more encapsulated,
	    # whereas this one already needs to know about a number of different
	    # components.
	    it 'should be invalid if there are not the same number of matching challenge buttons and challenges' do
	    	@matching_board.matching_challenge_buttons = []
	    	component = load
        	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Matching challenge button count must be equal to the number of challenges (0 != 1)"), component.errors.full_messages.inspect
	    end

    	it 'should be invalid if a challenge has ResetAnswersOnActivated behavior' do
    		@challenge.behaviors['ResetAnswersOnActivated'] = {}
    		component = load
    		expect(component.valid?).to be(false)
        	expect(component.errors.full_messages).to include("Challenge 1 must not have behavior \"ResetAnswersOnActivated\""), component.errors.full_messages.inspect
    	end

	    it 'should be invalid unless all challenges have the answer list' do
	    	@challenge.answer_list_id = @another_answer_list.id
	    	component = load
	    	expect(component.valid?).to be(false)           
	        expect(component.errors.full_messages).to include("Challenge 1 answer list must equal value of matching_board_answer_list"), component.errors.full_messages.inspect
	    end

	    it 'should be invalid unless there is a distinct answer for each challenge' do
	    	allow(@challenge).to receive(:correct_answer).and_return('answer')
	    	allow(@challenge_copy).to receive(:correct_answer).and_return('answer')
	    	@matching_board.matching_challenge_buttons = [
	    		@matching_challenge_button,
	    		@matching_challenge_button_2
	    	]
	    	component = load({
	    		challenge_ids: ['challenge', 'challenge_copy']
	    	})
	    	expect(component.valid?).to be(false)           
	        expect(component.errors.full_messages).to include("Consumable answers require a distinct answer for each challenge."), component.errors.full_messages.inspect
	    end

    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            id: 'challenges',
            component_type: "ComponentizedFrame.Challenges",
            editor_template: "matching",
            layout_id: 'layout',
            behaviors: {
            	GotoNextOnChallengeComplete: {},
            	CompleteOnAllChallengesComplete: {}
            },
            challenge_ids: ['challenge'],
            shared_content_for_text_id: 'text',
            shared_content_for_interactive_image_id: 'matching_board'
        }.deep_merge(overrides)
    end 
    

end