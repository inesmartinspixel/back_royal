require 'spec_helper'

# reference Component in order to load up all validations
Lesson::Content::FrameList::Frame::Componentized::Component

describe Lesson::Content::FrameList::Frame::Componentized::Component::Validations::ValidatesReference do

	before(:each) do
		@frame = Lesson::Content::FrameList::Frame::Componentized.load
		@frame.components = []
	end

	class Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesReference < Lesson::Content::FrameList::Frame::Componentized::Component
		references('text').through('text_id')
		references('texts').through('text_ids')
		references('with_editor_template').through('with_editor_template_id')
        references('with_one_of_editor_templates').through('with_one_of_editor_template_ids')
        references('with_expected_behavior').through('with_expected_behavior_id')
        references('with_unexpected_behavior').through('with_unexpected_behavior_id')

		validates_reference :text, Text
		validates_reference :texts, Text, :allow_nil => true
		validates_reference :with_editor_template, Text, :editor_template => 'expected_template', :allow_nil => true
        validates_reference :with_one_of_editor_templates, Text, :editor_template => ['expected_template_1', 'expected_template_2'], :allow_nil => true
        validates_reference :with_expected_behavior, Text, :has_behaviors => ['ExpectedBehavior'], :allow_nil => true
        validates_reference :with_unexpected_behavior, Text, :does_not_have_behaviors => ['UnexpectedBehavior'], :allow_nil => true
	end

	before(:each) do

		@frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})
        @text = Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'text', component_type: 'ComponentizedFrame.Text', text: 'Text', formatted_text: "<p>text</p>"})
        @text2 = Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'text2', component_type: 'ComponentizedFrame.Text', text: 'Text', formatted_text: "<p>text</p>"})
        @frame.components << @text
        @frame.components << @text2
	end

	it 'should be invalid with nothing in reference' do
    	component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesReference.load({
        	text_id: nil,
        	component_type: 'ComponentizedFrame.WithValidatesReference'
    	})
    	@frame.components.push component
    	expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Text must not be nil"), component.errors.full_messages.inspect
    end

    it 'should be invalid unexpected component type in reference' do
    	component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesReference.load({
        	text_id: 'image',
        	component_type: 'ComponentizedFrame.WithValidatesReference'
    	})
    	@frame.components.push component
    	expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Text must be an instance of Text but is Image"), component.errors.full_messages.inspect
    end

    it 'should be invalid with invalid reference' do
    	component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesReference.load({
        	text_id: 'text',
        	component_type: 'ComponentizedFrame.WithValidatesReference'
    	})
    	@frame.components.push component
    	@text.text = nil
    	expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Text Text can't be blank"), component.errors.full_messages.inspect
    end

    it 'should allow nil' do
    	component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesReference.load({
        	text_id: 'text',
        	component_type: 'ComponentizedFrame.WithValidatesReference'
    	})
    	@frame.components.push component
    	expect(component.valid?).to be(true), component.errors.full_messages.inspect
    end

    it 'should be invalid with unexpected editor_template' do
    	component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesReference.load({
        	text_id: 'text',
        	with_editor_template_id: 'text',
        	component_type: 'ComponentizedFrame.WithValidatesReference'
    	})
    	@frame.components.push component
    	@text.editor_template = 'unexpected'
    	expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("With editor template must have editor template \"expected_template\" but has \"unexpected\""), component.errors.full_messages.inspect
    end

    it 'should be valid with expected editor_template' do
    	component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesReference.load({
        	text_id: 'text',
        	with_editor_template_id: 'text',
        	component_type: 'ComponentizedFrame.WithValidatesReference'
    	})
    	@frame.components.push component
    	@text.editor_template = 'expected_template'
    	expect(component.valid?).to be(true), component.errors.full_messages.inspect
    end

    it 'should be valid with one of many expected editor_templates' do
        component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesReference.load({
            text_id: 'text',
            with_one_of_editor_template_ids: 'text',
            component_type: 'ComponentizedFrame.WithValidatesReference'
        })
        @frame.components.push component
        @text.editor_template = 'expected_template_1'
        expect(component.valid?).to be(true), component.errors.full_messages.inspect
    end

    it 'should be invalid without one of many expected editor_templates' do
        component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesReference.load({
            text_id: 'text',
            with_one_of_editor_template_ids: 'text',
            component_type: 'ComponentizedFrame.WithValidatesReference'
        })
        @frame.components.push component
        @text.editor_template = 'unexpected_template'
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("With one of editor templates must have editor template [\"expected_template_1\", \"expected_template_2\"] but has \"unexpected_template\""), component.errors.full_messages.inspect
    end

    it 'should be invalid without expected behavior' do
        component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesReference.load({
            text_id: 'text',
            with_expected_behavior_id: 'text',
            component_type: 'ComponentizedFrame.WithValidatesReference'
        })
        @frame.components.push component
        @text.behaviors = {}
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("With expected behavior must have behavior \"ExpectedBehavior\""), component.errors.full_messages.inspect
    end

    it 'should be valid with expected behavior' do
        component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesReference.load({
            text_id: 'text',
            with_expected_behavior_id: 'text',
            component_type: 'ComponentizedFrame.WithValidatesReference'
        })
        @frame.components.push component
        @text.behaviors = {ExpectedBehavior: {}}
        expect(component.valid?).to be(true), component.errors.full_messages.inspect
    end

    it 'should be invalid with unexpected behavior' do
        component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesReference.load({
            text_id: 'text',
            with_unexpected_behavior_id: 'text',
            component_type: 'ComponentizedFrame.WithValidatesReference'
        })
        @frame.components.push component
        @text.behaviors = {UnexpectedBehavior: {}}
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("With unexpected behavior must not have behavior \"UnexpectedBehavior\""), component.errors.full_messages.inspect
    end

    it 'should be valid without unexpected behavior' do
        component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesReference.load({
            text_id: 'text',
            with_unexpected_behavior_id: 'text',
            component_type: 'ComponentizedFrame.WithValidatesReference'
        })
        @frame.components.push component
        @text.behaviors = {}
        expect(component.valid?).to be(true), component.errors.full_messages.inspect
    end

    it 'should work on lists' do
    	component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesReference.load({
        	text_ids: ['text', 'text2'],
        	component_type: 'ComponentizedFrame.WithValidatesReference'
    	})
    	@frame.components.push component
    	@text.text = nil
        @text2.text = nil
    	expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Text 1 Text can't be blank"), component.errors.full_messages.inspect
        expect(component.errors.full_messages).to include("Text 2 Text can't be blank"), component.errors.full_messages.inspect
    end

    it 'should be valid with valid reference' do
    	component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesReference.load({
        	text_id: 'text',
        	component_type: 'ComponentizedFrame.WithValidatesReference'
    	})
    	@frame.components.push component
    	expect(component.valid?).to be(true), component.errors.full_messages.inspect
    end

end