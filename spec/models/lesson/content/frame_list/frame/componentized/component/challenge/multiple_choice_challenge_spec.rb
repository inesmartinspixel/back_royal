require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Challenge::MultipleChoiceChallenge do

    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'answer_list',
            component_type: 'ComponentizedFrame.AnswerList',
            answer_ids: ['answer']
        })
        @frame.components << (@validator = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'validator',
            component_type: 'ComponentizedFrame.ChallengeValidator',
            expected_answer_matcher_ids: ['answer_matcher'],
            behaviors: {
                HasAllExpectedAnswers: {},
                HasNoUnexpectedAnswers: {}
            }
        }))
        @frame.components << (@answer_matcher = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'answer_matcher',
            component_type: 'ComponentizedFrame.SimilarToSelectableAnswer',
            answer_id: 'answer'
        }))
        @frame.components << (@answer = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'answer', component_type: 'ComponentizedFrame.SelectableAnswer'
        }))
        @frame.components << (@message = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'message',
            component_type: 'ComponentizedFrame.MultipleChoiceMessage'
        }) )

        # mock out validations for references that are tested elsewhere
        @frame.components.each do |component|
            allow(component).to receive(:valid?).and_return(true)
        end
    end


    it 'should be valid if everything is awesome' do
        component = load(valid_attrs)
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Challenge::MultipleChoiceChallenge)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-AnswerList component as answer list' do
        component = load({
            answer_list_id: 'image'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Answer list must be an instance of AnswerList but is Image"), component.errors.full_messages.inspect
    end

    it 'should be invalid without a SimilarToSelectableAnswer matcher in the validator' do
        @validator.expected_answer_matcher_ids = ['image']
        component = load(valid_attrs)
        expect(component.valid?).to eq(false)
        expect(component.errors.full_messages).to include("Expected answer matcher 1 must be an instance of SimilarToSelectableAnswer but is Image"), component.errors.full_messages.inspect
    end

    it 'should be invalid if messages are invalid' do
        @message.stub_invalid(self)
        component = load
        expect(component.valid?).to eq(false)
        expect(component.errors.full_messages).to include("Message 1 Stubbed out to be invalid"), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-ChallengeValidator component as validator' do
        component = load({
            validator_id: 'image'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Validator must be an instance of ChallengeValidator but is Image"), component.errors.full_messages.inspect
    end

    describe 'correct_answer' do

        it 'should return the correct answer' do
            component = load
            expect(component.correct_answer).to be(@answer)
        end

    end

    describe 'correct_answers' do

        it 'should return the correct answers' do
            component = load
            expect(component.correct_answers).to eq([@answer])
        end

    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.MultipleChoiceChallenge",
            answer_list_id: 'answer_list',
            validator_id: 'validator',
            message_ids: ['message']
        }.deep_merge(overrides)
    end


end