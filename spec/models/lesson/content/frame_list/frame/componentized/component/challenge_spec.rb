require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Challenge do
    
    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'validator', component_type: 'ComponentizedFrame.ChallengeValidator'})

    end

    # only subclasses can be created
    it 'should be invalid' do
        component = load
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Challenge)
        expect(component.valid?).to eq(false), component.errors.full_messages.inspect
        expect(component.errors.full_messages).to include("Is challenge subclass? must be true"), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-ChallengeValidator component as validator' do
        component = load({
            validator_id: 'image'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Validator must be an instance of ChallengeValidator but is Image"), component.errors.full_messages.inspect
    end
    
    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.Challenge",
            validator_id: 'validator'
        }.deep_merge(overrides)
    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end
    

end