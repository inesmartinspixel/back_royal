require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::MatchingChallengeButton do
    
    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'text', component_type: 'ComponentizedFrame.Text'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})
        
        @challenge = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'challenge', 
            component_type: 'ComponentizedFrame.Challenge'
        })
        @frame.components << @challenge

        # mock out validations for references that are tested elsewhere
        @frame.components.each do |component|
            allow(component).to receive(:valid?).and_return(true)
        end
    end

    it 'should be valid with a text component' do
        component = load(valid_attrs)
        @frame.components << component
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::MatchingChallengeButton)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    it 'should be valid with an image component' do
        component = load({
            text_id: nil,
            image_id: 'image'
        })
        @frame.components << component
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::MatchingChallengeButton)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-Text component as text' do
        component = load({
            text_id: 'image'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Text must be an instance of Text but is Image"), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-Image component as image' do
        component = load({
            image_id: 'text'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Image must be an instance of Image but is Text"), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-Challenge component as challenge' do
        component = load({
            challenge_id: 'text'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Challenge must be an instance of Challenge but is Text"), component.errors.full_messages.inspect
    end

    it 'should be invalid with neither image nor text' do
        component = load({
            text_id: nil,
            image_id: nil
        })
        @frame.components << component
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Text or image must be set"), component.errors.full_messages.inspect
    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.MatchingChallengeButton",
            text_id: 'text',
            challenge_id: 'challenge'
        }.deep_merge(overrides)
    end 
    

end