require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::ChallengeValidator do
    
    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'challenge', component_type: 'ComponentizedFrame.Challenge'})

        # mock out validations for references that are tested elsewhere
        @frame.components.each do |component|
            allow(component).to receive(:valid?).and_return(true)
        end

    end

    # only subclasses can be created
    it 'should valid if everything is awesome' do
        component = load
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::ChallengeValidator)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-Challenge component as challenge' do
        component = load({
            challenge_id: 'image'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Challenge must be an instance of Challenge but is Image"), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-AnswerMatcher component as expected answer matcher' do
        component = load({
            expected_answer_matcher_ids: ['image']
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Expected answer matcher 1 must be an instance of AnswerMatcher but is Image"), component.errors.full_messages.inspect
    end

    it 'should allow nil answer matchers' do
        component = load({
            expected_answer_matcher_ids: nil
        })
        expect(component.valid?).to be(true), component.errors.full_messages.inspect
    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end
    
    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.ChallengeValidator",
            challenge_id: 'challenge'
        }.deep_merge(overrides)
    end 
    

end