require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::TilePromptBoard do
    
    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        
        @frame.components << (@answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'answer_list', 
            component_type: 'ComponentizedFrame.AnswerList'
        }) )
        @frame.components << (@another_answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'another_answer_list', 
            component_type: 'ComponentizedFrame.AnswerList'
        }) )

        @challenge = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'challenge', 
            component_type: 'ComponentizedFrame.MultipleChoiceChallenge', 
            answer_list_id: 'answer_list'
        })
        @frame.components << @challenge


        @challenges = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'challenges',
            component_type: 'ComponentizedFrame.Challenges'
        });
        @frame.components << @challenges


        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'tile_prompt_1', component_type: 'ComponentizedFrame.TilePrompt'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'tile_prompt_2', component_type: 'ComponentizedFrame.TilePrompt'})
        
        # mock out validations for references that are tested elsewhere
        @frame.components.each do |component|
            allow(component).to receive(:valid?).and_return(true)
        end
    end

    it 'should be valid if everything is awesome' do
        component = load(valid_attrs)
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::TilePromptBoard)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-AnswerList as answer list' do
        component = load({
            answer_list_id: 'tile_prompt_1'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Answer list must be an instance of AnswerList but is TilePrompt"), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-TilePrompt as matching_challenge_button' do
        component = load({
            tile_prompt_ids: 'answer_list'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Tile prompts must be an instance of TilePrompt but is AnswerList"), component.errors.full_messages.inspect
    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.TilePromptBoard",
            tile_prompt_ids: ['tile_prompt_1', 'tile_prompt_2'],
            answer_list_id: 'answer_list',
            challenges_component_id: 'challenges'
        }.deep_merge(overrides)
    end 
    

end