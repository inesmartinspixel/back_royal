require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Image do
    
    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
    end

    it 'should be valid if everything is awesome' do
        component = load(valid_attrs)
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Image)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    it 'should be invalid without an image' do
        component = load({image: nil})
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Image can't be blank"), component.errors.full_messages.inspect
    end

    it 'should be invalid without a label' do
        component = load({label: nil})
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Label can't be blank"), component.errors.full_messages.inspect
    end

    it 'should be invalid without a formats hash' do
        component = load
        component.image['formats'] = nil
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Formats can't be blank"), component.errors.full_messages.inspect
    end

    it 'should be invalid without an original format url' do
        component = load
        component.image['formats']['original'] = nil
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Original format url can't be blank"), component.errors.full_messages.inspect
    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end
    
    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.Image",
            label: 'label',
            image: {
                file_file_name: "three_cluster_2_3_6.png",
                formats: {
                    original: {
                        url: "https://s3.amazonaws.com/pedago/front_royal_assets/lessons/bf9e8941-d9a3-4593-b32b-2730071bef39/images/Fire1/original/Fire1.png?1390925319"
                    }
                }
            }
        }.deep_merge(overrides)
    end 
    

end