require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Challenges do

    describe 'with venn_diagram template' do

        before(:each) do
            @frame = Lesson::Content::FrameList::Frame::Componentized.load
            @frame.components = []
            @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})
            @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'layout', component_type: 'ComponentizedFrame.Layout'})
            @frame.components << (@shared_answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
                id: 'shared_answer_list', 
                component_type: 'ComponentizedFrame.AnswerList'
            }) )

            @challenge = Lesson::Content::FrameList::Frame::Componentized::Component.load({
                id: 'challenge', 
                component_type: 'ComponentizedFrame.MultipleChoiceChallenge', 
                editor_template: 'venn_diagram',
                answer_list_id: 'shared_answer_list'
            })
            @frame.components << @challenge
            @challenge2 = Lesson::Content::FrameList::Frame::Componentized::Component.load({
                id: 'challenge2', 
                component_type: 'ComponentizedFrame.MultipleChoiceChallenge', 
                editor_template: 'venn_diagram',
                answer_list_id: 'shared_answer_list'
            })
            @frame.components << @challenge2

            @text  =  Lesson::Content::FrameList::Frame::Componentized::Component.load({
                id: 'text', 
                component_type: 'ComponentizedFrame.Text', 
                text: 'here is a [blank]'})
            @frame.components << @text


            @header_text1  =  Lesson::Content::FrameList::Frame::Componentized::Component.load({
                id: 'header_text1', 
                component_type: 'ComponentizedFrame.Text', 
                text: 'header 1'})
            @frame.components << @header_text1

            @header_text2  =  Lesson::Content::FrameList::Frame::Componentized::Component.load({
                id: 'header_text2', 
                component_type: 'ComponentizedFrame.Text', 
                text: 'header 2'})
            @frame.components << @header_text2


            # mock out validations for references that are tested elsewhere
            @frame.components.each do |component|
                allow(component).to receive(:valid?).and_return(true)
            end
        end

        it 'should be valid if everything is awesome' do
            component = load_venn_diagram_component
            expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Challenges)
            expect(component.valid?).to eq(true), component.errors.full_messages.inspect
        end

        it 'should be invalid with a non-text component in sharedContentForText' do
            component = load_venn_diagram_component({
                shared_content_for_text_id: 'image'
            })
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Shared content for text must be an instance of Text but is Image"), component.errors.full_messages.inspect
        end

        it 'should be invalid unless it has exactly one challenge with the venn_diagram editor template' do
            component = load_venn_diagram_component({
                challenge_ids: ['challenge', 'challenge2']
            })
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Challenges must have exactly one element"), component.errors.full_messages.inspect

        end

        it 'should be invalid unless all challenges have venn_diagram editor template' do
            @challenge.editor_template = nil
            component = load_venn_diagram_component
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Challenge 1 must have editor template \"venn_diagram\" but has nil"), component.errors.full_messages.inspect
        end

    end

    def load_venn_diagram_component(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_venn_diagram_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_venn_diagram_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.Challenges",
            editor_template: "venn_diagram",
            layout_id: 'layout',
            behaviors: {
                GotoNextOnChallengeComplete: {},
                CompleteOnAllChallengesComplete: {}
            },
            challenge_ids: ['challenge'],
            shared_content_for_text_id: 'text',
            venn_diagram_header_ids: ['header_text1', 'header_text2']
        }.deep_merge(overrides)
    end 

end