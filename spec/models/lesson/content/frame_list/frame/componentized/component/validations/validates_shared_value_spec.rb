require 'spec_helper'

# reference Component in order to load up all validations
Lesson::Content::FrameList::Frame::Componentized::Component

describe Lesson::Content::FrameList::Frame::Componentized::Component::Validations::ValidatesSharedValue do

	before(:each) do
		@frame = Lesson::Content::FrameList::Frame::Componentized.load
		@frame.components = []
	end

	class Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesSharedValue < Lesson::Content::FrameList::Frame::Componentized::Component
		references('something').through('something_id')
		references('somethings').through('something_ids')

		validates_shared_value :something, :text, :expected_text
		validates_shared_value :somethings, :text, :expected_text

		def expected_text
			'expected_text'
		end
	end

	before(:each) do

		@frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        @something = Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'something', component_type: 'ComponentizedFrame.Text'})
		@frame.components << @something

        allow(@something).to receive(:valid?).and_return(true)
	end

	it 'should be valid with nothing in reference' do
    	component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesSharedValue.load({
        	something_id: nil,
        	component_type: 'ComponentizedFrame.WithValidatesSharedValue'
    	})
    	@frame.components.push component
    	expect(component.valid?).to be(true), component.errors.full_messages.inspect
    end

    it 'should be valid with expected value' do
    	component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesSharedValue.load({
        	something_id: 'something',
        	component_type: 'ComponentizedFrame.WithValidatesSharedValue'
    	})
    	@frame.components.push component
    	@something.text = component.expected_text
    	expect(component.valid?).to be(true), component.errors.full_messages.inspect
    end

    it 'should be invalid with unexpected value in reference' do
    	component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesSharedValue.load({
        	something_id: 'something',
        	component_type: 'ComponentizedFrame.WithValidatesSharedValue'
    	})
    	@something.text = 'unexpected'
    	@frame.components.push component
    	expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Something text must equal value of expected_text"), component.errors.full_messages.inspect
    end

    it 'should be invalid with unexpected value in list' do
    	component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesSharedValue.load({
        	something_ids: ['something'],
        	component_type: 'ComponentizedFrame.WithValidatesSharedValue'
    	})
    	@something.text = 'unexpected'
    	@frame.components.push component
    	expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Something 1 text must equal value of expected_text"), component.errors.full_messages.inspect
    end

    it 'should be invalid if record does not support property' do
    	component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesSharedValue.load({
        	something_ids: ['something'],
        	component_type: 'ComponentizedFrame.WithValidatesSharedValue'
    	})
    	allow(component).to receive(:expected_text) { raise NoMethodError }
    	@frame.components.push component
    	expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Expected text is not a method"), component.errors.full_messages.inspect
    end

    it 'should be invalid if association does not support property' do
    	component = Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesSharedValue.load({
        	something_ids: ['something'],
        	component_type: 'ComponentizedFrame.WithValidatesSharedValue'
    	})
    	allow(@something).to receive(:text) { raise NoMethodError }
    	@frame.components.push component
    	expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Something 1 text is not a method"), component.errors.full_messages.inspect
    end

end