require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Challenges do

    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'challenge', component_type: 'ComponentizedFrame.Challenge'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'layout', component_type: 'ComponentizedFrame.Layout'})
        @text  =  Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'text',
            component_type: 'ComponentizedFrame.Text',
            text: 'main text'})
        @frame.components << @text

        @frame.components << (@continue_button = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'continue_button',
            component_type: 'ComponentizedFrame.ChallengesContinueButton'
        }) )

        @frame.continue_button_id = @continue_button.id

        # mock out validations for references that are tested elsewhere
        @frame.components.each do |component|
            allow(component).to receive(:valid?).and_return(true)
        end

    end

    it 'should be valid if everything is awesome' do
        component = load(valid_attrs)
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Challenges)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    it 'should be invalid without at least one challenge' do
    	component = load({
            challenge_ids: []
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Challenges is too short (min is 1)"), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-Challenge component as challenge' do
        component = load({
            challenge_ids: ['image']
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Challenge 1 must be an instance of Challenge but is Image"), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-Layout component as layout' do
        component = load({
            layout_id: 'image'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Layout must be an instance of Layout but is Image"), component.errors.full_messages.inspect
    end

    it 'should be invalid if frame does not have a challenges continue button component' do
        @frame.continue_button_id = 'image'
        component = load
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Continue button must be instance of ChallengesContinueButton but is Image"), component.errors.full_messages.inspect
    end

    it 'should be invalid unless there is at least one challenge' do
        component = load({
            challenge_ids: []
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Challenges is too short (min is 1)"), component.errors.full_messages.inspect
    end

    it 'should not extract a blank from a link' do
        @text.text = '[url](http://www.google.com)'
        component = load({
            editor_template: 'fill_in_the_blanks',
            shared_content_for_text_id: 'text',
        })
        expect(component.inline_blank_texts).to eq([])
    end

    it 'should extract blanks on either side of a link' do
        @text.text = '[blank] [url](http://www.google.com) [blank2]'
        component = load({
            editor_template: 'fill_in_the_blanks',
            shared_content_for_text_id: 'text',
        })
        expect(component.inline_blank_texts).to eq(['blank', 'blank2'])
    end

    it 'should extract two blanks in a row' do
        @text.text = '[blank] [blank2]'
        component = load({
            editor_template: 'fill_in_the_blanks',
            shared_content_for_text_id: 'text',
        })
        expect(component.inline_blank_texts).to eq(['blank', 'blank2'])
    end

    it 'should extract a blank with a parenthetical in MathJax' do
        @text.text = '%%E(X+Y)=Blank[E]​(X)+E(Y)=Blank[16]%%'
        component = load({
            editor_template: 'fill_in_the_blanks',
            shared_content_for_text_id: 'text'
        })
        expect(component.inline_blank_texts.size).to eq(2);
    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.Challenges",
            challenge_ids: ['challenge'],
            layout_id: 'layout'
        }.deep_merge(overrides)
    end


end