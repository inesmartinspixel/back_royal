require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Layout::TextImageInteractive do
    
    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
    end

    it 'should be valid' do
        component = load(valid_attrs)
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Layout::TextImageInteractive)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end
    
    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.TextImageInteractive"
        }.deep_merge(overrides)
    end 

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end
    

end