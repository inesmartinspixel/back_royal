require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Challenges do

    describe 'with image_hotspot template' do

    	before(:each) do
	        @frame = Lesson::Content::FrameList::Frame::Componentized.load
	        @frame.components = []
	        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})
	        # @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'challenges_ready_intro', component_type: 'ComponentizedFrame.ChallengesReadyIntro'})
	        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'layout', component_type: 'ComponentizedFrame.Layout'})
	        @frame.components << (@component_overlay = Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'component_overlay',
	        	component_type: 'ComponentizedFrame.ComponentOverlay',
	        	image_id: 'image',
	        	overlay_component_ids: ['shared_answer_list']
	        }))
	        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'layout', component_type: 'ComponentizedFrame.Layout'})
	        @frame.components << (@shared_answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'shared_answer_list',
	        	component_type: 'ComponentizedFrame.AnswerList'
	        }) )

	        @challenge = Lesson::Content::FrameList::Frame::Componentized::Component.load({
        		id: 'challenge',
        		component_type: 'ComponentizedFrame.MultipleChoiceChallenge',
        		editor_template: 'basic_multiple_choice',
        		answer_list_id: 'shared_answer_list'
        	})
	        @frame.components << @challenge


	        @text  =  Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'text',
	        	component_type: 'ComponentizedFrame.Text',
	        	text: 'here is a [blank]'})
	        @frame.components << @text

	        # mock out validations for references that are tested elsewhere
	        @frame.components.each do |component|
	            allow(component).to receive(:valid?).and_return(true)
	        end
	    end

	    it 'should be valid if everything is awesome' do
            component = load
            expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Challenges)
            expect(component.valid?).to eq(true), component.errors.full_messages.inspect
        end

	    it 'should be invalid unless it has expected behaviors' do
	    	component = load({
            	behaviors: {}
        	})
	        expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Goto next on challenge complete behavior must be included"), component.errors.full_messages.inspect
	        expect(component.errors.full_messages).to include("Complete on all challenges complete behavior must be included"), component.errors.full_messages.inspect
	    end

	    # we can assume that this covers both nil and invalid cases, since
	    # that's how validates_reference works
	    it 'should be invalid non-text component in sharedContentForText' do
	    	component = load({
            	shared_content_for_text_id: 'image'
        	})
        	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Shared content for text must be an instance of Text but is Image"), component.errors.full_messages.inspect
	    end

	    # we can assume that this covers both nil and invalid cases, since
	    # that's how validates_reference works
	    it 'should be invalid non-ComponentOverlay component in sharedContentForInteractiveImage' do
	    	component = load({
            	shared_content_for_interactive_image_id: 'text'
        	})
        	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Shared content for interactive image must be an instance of ComponentOverlay but is Text"), component.errors.full_messages.inspect
	    end

	    # we can assume that this covers both nil and invalid cases, since
	    # that's how validates_reference works
	    it 'should be invalid without exactly one overlay component' do
	    	@component_overlay.overlay_component_ids = ['shared_answer_list', 'text']
	    	component = load
        	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Shared content for image overlay components must have exactly one element"), component.errors.full_messages.inspect
	    end

	    # we can assume that this covers both nil and invalid cases, since
	    # that's how validates_reference works
	    it 'should be invalid if overlay component is not an answer list' do
	    	@component_overlay.overlay_component_ids = ['text']
	    	component = load
        	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Shared content for image first overlay component must be an instance of AnswerList but is Text"), component.errors.full_messages.inspect
	    end

	    it 'should be invalid unless all challenges have the answer list' do
            @challenge.answer_list_id = 'text'
            component = load
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Challenge 1 answer list must equal value of shared_content_for_image_first_overlay_component"), component.errors.full_messages.inspect
        end

        it 'should be invalid with more than one challenge' do
            component = load({
            	challenge_ids: ['challenge', 'challenge']
            })
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Challenges must have exactly one element"), component.errors.full_messages.inspect
        end

        it 'should be invalid unless all challenges have basic_multiple_choice or checl_many editor template' do
	    	@challenge.editor_template = nil
	    	component = load
	    	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Challenge 1 must have editor template [\"basic_multiple_choice\", \"check_many\"] but has nil"), component.errors.full_messages.inspect
	    end

	end


    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.Challenges",
            editor_template: "image_hotspot",
            layout_id: 'layout',
            behaviors: {
            	GotoNextOnChallengeComplete: {},
            	CompleteOnAllChallengesComplete: {}
            },
            challenge_ids: ['challenge'],
            shared_content_for_text_id: 'text',
            shared_content_for_interactive_image_id: 'component_overlay'
        }.deep_merge(overrides)
    end

end