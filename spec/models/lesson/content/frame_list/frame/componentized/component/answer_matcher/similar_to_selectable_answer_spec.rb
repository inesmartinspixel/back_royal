require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::AnswerMatcher::SimilarToSelectableAnswer do

    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        @frame.components << (@answer = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'answer',
            component_type: 'ComponentizedFrame.SelectableAnswer'
        }) )
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})

        # mock out validations for references that are tested elsewhere
        allow(@answer).to receive(:valid?).and_return(true);
    end

    it 'should be valid if everything is awesome' do
        component = load
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::AnswerMatcher::SimilarToSelectableAnswer)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-SelectableAnswer  component as answer' do
        component = load({
            answer_id: 'image'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Answer must be an instance of SelectableAnswer but is Image"), component.errors.full_messages.inspect
    end


    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            id: '1',
            component_type: "ComponentizedFrame.SimilarToSelectableAnswer",
            answer_id: 'answer'
        }.deep_merge(overrides)
    end


end