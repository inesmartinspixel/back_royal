require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::MultipleChoiceMessage do
    
    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'answer_matcher', component_type: 'ComponentizedFrame.SimilarToSelectableAnswer'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'text', component_type: 'ComponentizedFrame.Text'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'challenge', component_type: 'ComponentizedFrame.MultipleChoiceChallenge'})
        

        # mock out validations for references that are tested elsewhere
        @frame.components.each do |component|
            allow(component).to receive(:valid?).and_return(true)
        end
    end

    it 'should be valid if everything is awesome' do
        component = load(valid_attrs)
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::MultipleChoiceMessage)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-MultipleChoiceChallenge component as challenge' do
        component = load({
            challenge_id: 'image'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Challenge must be an instance of MultipleChoiceChallenge but is Image"), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-SimilarToSelectableAnswer component as answer matcher' do
        component = load({
            answer_matcher_id: 'image'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Answer matcher must be an instance of SimilarToSelectableAnswer but is Image"), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-Text component as message text' do
        component = load({
            message_text_id: 'image'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Message text must be an instance of Text but is Image"), component.errors.full_messages.inspect
    end

    it 'should be valid with selected event' do
        component = load({
            event: 'selected'
        })
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    it 'should be invalid without a supported event' do
        component = load({
            event: 'unsupported'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Event is not included in the list"), component.errors.full_messages.inspect
    end
    
    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.MultipleChoiceMessage",
            message_text_id: 'text',
            challenge_id: 'challenge',
            answer_matcher_id: 'answer_matcher',
            event: 'validated'
        }.deep_merge(overrides)
    end 
    

end