require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Challenges do

    describe 'with fill_in_the_blanks template' do

    	before(:each) do
	        @frame = Lesson::Content::FrameList::Frame::Componentized.load
	        @frame.components = []
	        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})
	        # @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'challenges_ready_intro', component_type: 'ComponentizedFrame.ChallengesReadyIntro'})
	        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'layout', component_type: 'ComponentizedFrame.Layout'})
	        @frame.components << (@shared_answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'shared_answer_list',
	        	component_type: 'ComponentizedFrame.AnswerList'
	        }) )
	        @frame.components << (@distinct_answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'distinct_answer_list',
	        	component_type: 'ComponentizedFrame.AnswerList'
	        }) )
	        @frame.components << (@another_answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'another_answer_list',
	        	component_type: 'ComponentizedFrame.AnswerList'
	        }) )

	        @consumable_challenge = Lesson::Content::FrameList::Frame::Componentized::Component.load({
        		id: 'consumable_challenge',
        		component_type: 'ComponentizedFrame.MultipleChoiceChallenge',
        		editor_template: 'fill_in_the_blanks',
        		answer_list_id: 'shared_answer_list'
        	})
	        @frame.components << @consumable_challenge

	        @consumable_challenge_copy = Lesson::Content::FrameList::Frame::Componentized::Component.load({
        		id: 'consumable_challenge_copy',
        		component_type: 'ComponentizedFrame.MultipleChoiceChallenge',
        		editor_template: 'fill_in_the_blanks',
        		answer_list_id: 'shared_answer_list'
        	})
	        @frame.components << @consumable_challenge_copy

	        @sequential_challenge = Lesson::Content::FrameList::Frame::Componentized::Component.load({
        		id: 'sequential_challenge',
        		component_type: 'ComponentizedFrame.MultipleChoiceChallenge',
        		editor_template: 'fill_in_the_blanks',
        		answer_list_id: 'distinct_answer_list',
        		behaviors: {
        			ResetAnswersOnActivated: {}
        		}
        	})
	        @frame.components << @sequential_challenge



	        @text  =  Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'text',
	        	component_type: 'ComponentizedFrame.Text',
	        	text: 'here is a [blank]'})
	        @frame.components << @text

	        # mock out validations for references that are tested elsewhere
	        @frame.components.each do |component|
	            allow(component).to receive(:valid?).and_return(true)
	        end
	    end

	    it 'should be invalid unless it has expected behaviors' do
	    	component = load_sequential({
            	behaviors: {}
        	})
	        expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Goto next on challenge complete behavior must be included"), component.errors.full_messages.inspect
	        expect(component.errors.full_messages).to include("Complete on all challenges complete behavior must be included"), component.errors.full_messages.inspect
	    end

	    # we can assume that this covers both nil and invalid cases, since
	    # that's how validates_reference works
	    it 'should be invalid with a non-text component in sharedContentForText' do
	    	component = load_sequential({
            	shared_content_for_text_id: 'image'
        	})
        	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Shared content for text must be an instance of Text but is Image"), component.errors.full_messages.inspect
	    end

	    it 'should be invalid unless all challenges have fill_in_the_blanks editor template' do
	    	@sequential_challenge.editor_template = nil
	    	component = load_sequential
	    	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Challenge 1 must have editor template \"fill_in_the_blanks\" but has nil"), component.errors.full_messages.inspect
	    end

    	# we can assume that this covers both nil and invalid cases, since
	    # that's how validates_reference works
	    it 'should be invalid non-image component in sharedContentForFirstImage' do
	    	component = load_sequential({
            	shared_content_for_image_id: 'text'
        	})
        	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Shared content for first image must be an instance of Image but is Text"), component.errors.full_messages.inspect
	    end

        # we can assume that this covers both nil and invalid cases, since
        # that's how validates_reference works
        it 'should be invalid non-image component in sharedContentForSecondImage' do
            component = load_sequential({
                shared_content_for_image_2_id: 'text'
            })
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Shared content for second image must be an instance of Image but is Text"), component.errors.full_messages.inspect
        end

        it 'should be valid with blanks in mathjax' do
            @text.text = 'a %%Blank[mathjax blank]%%'
            component = load_sequential
            expect(component.valid?).to be(true), component.errors.full_messages.inspect
        end

        it 'should be valid with mathjax in blanks' do
            @text.text = 'a mathjax in a [%%blank%%]'
            component = load_sequential
            expect(component.valid?).to be(true), component.errors.full_messages.inspect
        end

	    describe 'sequential mode' do

	    	it 'should be valid if everything is awesome' do
		        component = load_sequential
                expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Challenges)
		        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
		    end

	    	it 'should be invalid if a challenge does not have ResetAnswersOnActivated behavior' do
	    		@sequential_challenge.behaviors['ResetAnswersOnActivated'] = nil
	    		component = load_sequential
	    		expect(component.valid?).to be(false)
	        	expect(component.errors.full_messages).to include("Challenge 1 must have behavior \"ResetAnswersOnActivated\""), component.errors.full_messages.inspect
	    	end

	    end


	    describe 'consumable mode' do

	    	it 'should be valid if everything is awesome' do
		        component = load_consumable
		        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Challenges)
		        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
		    end

	    	it 'should be invalid if a challenge has ResetAnswersOnActivated behavior' do
	    		@consumable_challenge.behaviors['ResetAnswersOnActivated'] = {}
	    		component = load_consumable
	    		expect(component.valid?).to be(false)
	        	expect(component.errors.full_messages).to include("Challenge 1 must not have behavior \"ResetAnswersOnActivated\""), component.errors.full_messages.inspect
	    	end

	    	# we can assume that this covers both nil and invalid cases, since
		    # that's how validates_reference works
		    it 'should be invalid without an answer list in sharedContentForInteractive' do
		    	component = load_consumable({
	            	shared_content_for_interactive_id: 'image'
	        	})
	        	expect(component.valid?).to be(false)
		        expect(component.errors.full_messages).to include("Shared content for interactive must be an instance of AnswerList but is Image"), component.errors.full_messages.inspect
		    end

		    it 'should be invalid unless all challenges have the answer list' do
		    	@consumable_challenge.answer_list_id = @another_answer_list.id
		    	component = load_consumable
		    	expect(component.valid?).to be(false)
		        expect(component.errors.full_messages).to include("Challenge 1 answer list must equal value of shared_content_for_interactive"), component.errors.full_messages.inspect
		    end

		    it 'should be invalid unless there is a distinct answer for each challenge' do
		    	allow(@consumable_challenge).to receive(:correct_answer).and_return('answer')
		    	allow(@consumable_challenge_copy).to receive(:correct_answer).and_return('answer')
		    	@text.text = "a [blank] and [another]"
		    	component = load_consumable({
		    		challenge_ids: ['consumable_challenge', 'consumable_challenge_copy']
		    	})
		    	expect(component.valid?).to be(false)
		        expect(component.errors.full_messages).to include("Consumable answers require a distinct answer for each challenge."), component.errors.full_messages.inspect
		    end


	    end

    end

    def load_sequential(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_sequential_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def load_consumable(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_consumable_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_sequential_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.Challenges",
            editor_template: "fill_in_the_blanks",
            layout_id: 'layout',
            behaviors: {
            	GotoNextOnChallengeComplete: {},
            	CompleteOnAllChallengesComplete: {}
            },
            challenge_ids: ['sequential_challenge'],
            shared_content_for_text_id: 'text'
        }.deep_merge(overrides)
    end

    def valid_consumable_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.Challenges",
            editor_template: "fill_in_the_blanks",
            layout_id: 'layout',
            behaviors: {
            	GotoNextOnChallengeComplete: {},
            	CompleteOnAllChallengesComplete: {}
            },
            challenge_ids: ['consumable_challenge'],
            shared_content_for_text_id: 'text',
            shared_content_for_interactive_id: 'shared_answer_list'
        }.deep_merge(overrides)
    end


end