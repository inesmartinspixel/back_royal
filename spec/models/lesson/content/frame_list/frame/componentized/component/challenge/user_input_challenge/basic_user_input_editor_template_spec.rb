require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Challenge::UserInputChallenge do

    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})
        @frame.components << (@answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'answer_list',
            component_type: 'ComponentizedFrame.AnswerList',
            answer_ids: ['answer']
        }) )
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'answer',
            component_type: 'ComponentizedFrame.SelectableAnswer'
        })
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'answer_matcher',
            component_type: 'ComponentizedFrame.MatchesExpectedText'
        })
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'vanilla_answer_matcher', component_type: 'ComponentizedFrame.AnswerMatcher'})
        @frame.components << (@validator = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'validator',
            component_type: 'ComponentizedFrame.ChallengeValidator',
            behaviors: {
                HasAllExpectedAnswers: true,
                HasNoUnexpectedAnswers: true
            },
            expected_answer_matcher_ids: ['answer_matcher']
        }) )
        # mock out validations for references that are tested elsewhere
        @frame.components.each do |component|
            allow(component).to receive(:valid?).and_return(true)
        end
    end

    describe 'with basic_user_input editor_template' do

        it 'should be valid if everything is awesome' do
            component = load(valid_attrs)
            expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Challenge::UserInputChallenge)
            expect(component.valid?).to eq(true), component.errors.full_messages.inspect
        end

        it 'should be invalid unless it has expected behaviors' do
            component = load({
                behaviors: {}
            })
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Complete on correct behavior must be included"), component.errors.full_messages.inspect
        end

        it 'should be invalid if validator does no have expected behaviors' do
            @validator.behaviors = {}
            component = load
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Validator must have behavior \"HasAllExpectedAnswers\""), component.errors.full_messages.inspect
            expect(component.errors.full_messages).to include("Validator must have behavior \"HasNoUnexpectedAnswers\""), component.errors.full_messages.inspect
        end

    end



    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.UserInputChallenge",
            editor_template: 'basic_user_input',
            validator_id: 'validator',
            behaviors: {
                CompleteOnCorrect: {}
            }
        }.deep_merge(overrides)
    end


end