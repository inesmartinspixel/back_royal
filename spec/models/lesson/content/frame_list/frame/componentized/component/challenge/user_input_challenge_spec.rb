require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Challenge::UserInputChallenge do

    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'answer_list', component_type: 'ComponentizedFrame.AnswerList'})
        @frame.components << (@validator = Lesson::Content::FrameList::Frame::Componentized::Component.load({
        	id: 'validator',
        	component_type: 'ComponentizedFrame.ChallengeValidator',
        	expected_answer_matcher_ids: ['answer_matcher']
        }))
        @frame.components << (@answer_matcher = Lesson::Content::FrameList::Frame::Componentized::Component.load({
        	id: 'answer_matcher', component_type: 'ComponentizedFrame.MatchesExpectedText'
        }))
        @frame.components << (@message = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'message',
            component_type: 'ComponentizedFrame.UserInputMessage'
        }) )

        # mock out validations for references that are tested elsewhere
        @frame.components.each do |component|
            allow(component).to receive(:valid?).and_return(true)
        end
    end


    it 'should be valid if everything is awesome' do
        component = load(valid_attrs)
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Challenge::UserInputChallenge)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    it 'should be invalid without a MatchesExpectedText matcher in the validator' do
        @validator.expected_answer_matcher_ids = ['image']
        component = load(valid_attrs)
        expect(component.valid?).to eq(false)
        expect(component.errors.full_messages).to include("Expected answer matcher 1 must be an instance of MatchesExpectedText but is Image"), component.errors.full_messages.inspect
    end

    it 'should be invalid if messages are invalid' do
        @message.stub_invalid(self)
        component = load
        expect(component.valid?).to eq(false)
        expect(component.errors.full_messages).to include("Message 1 Stubbed out to be invalid"), component.errors.full_messages.inspect
    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.UserInputChallenge",
            validator_id: 'validator',
            message_ids: ['message']
        }.deep_merge(overrides)
    end


end