require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Layout::TextImageInteractive do
    
    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        @frame.components << (@text = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'text', 
            component_type: 'ComponentizedFrame.Text'
        }) )
        @frame.components << (@image = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'image', 
            component_type: 'ComponentizedFrame.Image'
        }) )
        @frame.components << (@continue_button = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'continue_button', 
            component_type: 'ComponentizedFrame.AlwaysReadyContinueButton'
        }) )

        @frame.continue_button_id = @continue_button.id

        # mock out validations for references that are tested elsewhere
        allow(@text).to receive(:valid?).and_return(true)
        allow(@image).to receive(:valid?).and_return(true)
    end

    describe 'with no_interaction editor_template' do

    	it 'should be valid if everything is awesome' do
    		component = load
	        expect(component.valid?).to be(true), component.errors.full_messages.inspect
    	end

        # we can assume that this covers both nil and invalid cases, since
	    # that's how validates_reference works
	    it 'should be invalid with non-Text component as static_content_for_text' do
	        component = load({
	            content_for_text_id: 'image'
	        })
	        expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Static content for text must be an instance of Text but is Image"), component.errors.full_messages.inspect
	    end

	    # we can assume that this covers both nil and invalid cases, since
	    # that's how validates_reference works
	    it 'should be invalid with non-Image component as static_content_for_first_image' do
	        component = load({
	            content_for_image_id: 'text'
	        })
	        expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Static content for first image must be an instance of Image but is Text"), component.errors.full_messages.inspect
	    end

        # we can assume that this covers both nil and invalid cases, since
        # that's how validates_reference works
        it 'should be invalid with non-Image component as static_content_for_second_image' do
            component = load({
                content_for_image_2_id: 'text'
            })
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Static content for second image must be an instance of Image but is Text"), component.errors.full_messages.inspect
        end

	    it 'should be valid with no main image' do
			component = load({
	            content_for_image_id: nil
	        })
	        expect(component.valid?).to be(true), component.errors.full_messages.inspect
	    end

        it 'should be invalid with anything in the contentForInteractive section' do
            component = load({
	            content_for_interactive_id: 'text'
	        })
	        expect(component.valid?).to be(false)
	        expect(component.errors.full_messages).to include("Static content for interactive must be blank"), component.errors.full_messages.inspect
        end

        it 'should be invalid unless the frame has an AlwaysReadyContinueButton' do
            @frame.continue_button_id = 'image'
            component = load
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Continue button must be instance of AlwaysReadyContinueButton but is Image"), component.errors.full_messages.inspect
        end

    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end
    
    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.TextImageInteractive",
            editor_template: 'no_interaction',
            content_for_image_id: 'image',
            content_for_text_id: 'text',
            content_for_interactive_id: nil
        }.deep_merge(overrides)
    end 
    

end