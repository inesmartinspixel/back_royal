require 'spec_helper'

# reference Component in order to load up all validations
Lesson::Content::FrameList::Frame::Componentized::Component

describe Lesson::Content::FrameList::Frame::Componentized::Component::Validations::ValidatesCorrectAnswerExists do

    class Lesson::Content::FrameList::Frame::Componentized::Component::WithValidatesCorrectAnswerExists < Lesson::Content::FrameList::Frame::Componentized::Component::Challenge::MultipleChoiceChallenge

        validates_correct_answer_exists

    end

	before(:each) do
		@frame = Lesson::Content::FrameList::Frame::Componentized.load

        @frame.components = [];

        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'vanilla_answer_matcher',
            component_type: 'ComponentizedFrame.AnswerMatcher'
        })

        @frame.components << (@answer_matcher = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'similar_to_selectable_answer',
            component_type: 'ComponentizedFrame.SimilarToSelectableAnswer',
            answer_id: 'answer'
        }) )
        @frame.components << (@answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'answer_list',
            component_type: 'ComponentizedFrame.AnswerList',
            answer_ids: ['answer']
        }) )
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'answer',
            component_type: 'ComponentizedFrame.SelectableAnswer'
        })
        @frame.components << (@validator = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'validator',
            component_type: 'ComponentizedFrame.ChallengeValidator',
            behaviors: {
                HasAllExpectedAnswers: true,
                HasNoUnexpectedAnswers: true
            },
            expected_answer_matcher_ids: ['similar_to_selectable_answer']
        }) )

        # mock out validations for references that are tested elsewhere
        @frame.components.each do |component|
            allow(component).to receive(:valid?).and_return(true)
        end

        @component = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'component',
            component_type: 'ComponentizedFrame.WithValidatesCorrectAnswerExists',
            answer_list_id: 'answer_list',
            validator_id: 'validator'
        })

        @frame.components << @component

	end

    it 'should be valid if everything is awesome' do
        expect(@component.valid?).to be(true), @component.errors.full_messages.inspect
    end

	it 'should have expected behaviors on validator' do
        @validator.behaviors = {}
        expect(@component.valid?).to be(false)
        expect(@component.errors.full_messages).to include("Validator must have behavior \"HasAllExpectedAnswers\""), @component.errors.full_messages.inspect
        expect(@component.errors.full_messages).to include("Validator must have behavior \"HasNoUnexpectedAnswers\""), @component.errors.full_messages.inspect
    end

    it 'should be invalid unless it has at least one correct answer' do
        @validator.expected_answer_matcher_ids = []
        expect(@component.valid?).to be(false)
        expect(@component.errors.full_messages).to include("Validator must have at least one expected answer matcher"), @component.errors.full_messages.inspect
    end

    it 'should have SimilarToSelectableAnswer answer matcher in the validator' do
        @validator.expected_answer_matcher_ids = ['vanilla_answer_matcher']
        expect(@component.valid?).to be(false)
        expect(@component.errors.full_messages).to include("Validator answer matcher must be a SimilarToSelectableAnswer but is AnswerMatcher"), @component.errors.full_messages.inspect
    end

    it 'should be invalid if answer matcher does not refer to an answer in the answer list' do
        @answer_matcher.answer_id = nil
        expect(@component.valid?).to be(false)
        expect(@component.errors.full_messages).to include("Validator correct answer must refer to an answer in the answer list"), @component.errors.full_messages.inspect
    end

    it 'should be invalid if there is no answer list' do
        @component.answer_list_id = nil
        @answer_matcher.answer_id = nil
        expect(@component.valid?).to be(false)
        expect(@component.errors.full_messages).to include("Answers must be defined"), @component.errors.full_messages.inspect
    end

    it 'should be invalid if there is no answer list answers is nil' do
        @answer_list.answer_ids = nil
        expect(@component.valid?).to be(false)
        expect(@component.errors.full_messages).to include("Answers must be defined"), @component.errors.full_messages.inspect
    end

end