require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Challenges do

    describe 'with multiple_choice_poll template' do

        before(:each) do
            @frame = Lesson::Content::FrameList::Frame::Componentized.load
            @frame.components = []
            @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})

            @frame.components << (@layout = Lesson::Content::FrameList::Frame::Componentized::Component.load({
                id: 'layout',
                component_type: 'ComponentizedFrame.TextImageInteractive',
                content_for_header_id: 'poll_logo'
            }))
            @frame.components << (@answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
                id: 'answer_list',
                component_type: 'ComponentizedFrame.AnswerList'
            }) )
            @another_answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'another_answer_list', component_type: 'ComponentizedFrame.AnswerList'})
            @frame.components << @another_answer_list

            @standard_challenge = Lesson::Content::FrameList::Frame::Componentized::Component.load({
                id: 'challenge',
                component_type: 'ComponentizedFrame.MultipleChoiceChallenge',
                editor_template: 'basic_multiple_choice',
                answer_list_id: 'answer_list'
            })

            @check_many_challenge = Lesson::Content::FrameList::Frame::Componentized::Component.load({
                id: 'challenge',
                component_type: 'ComponentizedFrame.MultipleChoiceChallenge',
                editor_template: 'check_many',
                answer_list_id: 'answer_list'
            })


            @text  =  Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'text', component_type: 'ComponentizedFrame.Text', text: 'Text'})
            @frame.components << @text

        end

        describe 'check_many=false' do
            before(:each) do
                @challenge = @standard_challenge
                @frame.components << @challenge
                # mock out validations for references that are tested elsewhere
                @frame.components.each do |component|
                    allow(component).to receive(:valid?).and_return(true)
                end
            end

            it 'should be valid if everything is awesome' do
                component = load(valid_attrs)
                expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Challenges)
                expect(component.valid?).to eq(true), component.errors.full_messages.inspect
            end

            it 'should be invalid unless it has expected behaviors' do
                component = load({
                    behaviors: {}
                })
                expect(component.valid?).to be(false)
                expect(component.errors.full_messages).to include("Goto next on challenge complete behavior must be included"), component.errors.full_messages.inspect
                expect(component.errors.full_messages).to include("Complete on all challenges complete behavior must be included"), component.errors.full_messages.inspect
            end

            # we can assume that this covers both nil and invalid cases, since
            # that's how validates_reference works
            it 'should be invalid non-text component in sharedContentForText' do
                component = load({
                    shared_content_for_text_id: 'image'
                })
                expect(component.valid?).to be(false)
                expect(component.errors.full_messages).to include("Shared content for text must be an instance of Text but is Image"), component.errors.full_messages.inspect
            end

            # we can assume that this covers both nil and invalid cases, since
            # that's how validates_reference works
            it 'should be invalid non-image component in sharedContentForFirstImage' do
                component = load({
                    shared_content_for_image_id: 'text'
                })
                expect(component.valid?).to be(false)
                expect(component.errors.full_messages).to include("Shared content for first image must be an instance of Image but is Text"), component.errors.full_messages.inspect
            end

            # we can assume that this covers both nil and invalid cases, since
            # that's how validates_reference works
            it 'should be invalid non-image component in sharedContentForSecondImage' do
                component = load({
                    shared_content_for_image_2_id: 'text'
                })
                expect(component.valid?).to be(false)
                expect(component.errors.full_messages).to include("Shared content for second image must be an instance of Image but is Text"), component.errors.full_messages.inspect
            end

            # we can assume that this covers both nil and invalid cases, since
            # that's how validates_reference works
            it 'should be invalid without an answer list in sharedContentForInteractive' do
                component = load({
                    shared_content_for_interactive_id: 'image'
                })
                expect(component.valid?).to be(false)
                expect(component.errors.full_messages).to include("Shared content for interactive must be an instance of AnswerList but is Image"), component.errors.full_messages.inspect
            end

            it 'should be invalid unless all challenges have basic_multiple_choice or check_many editor templates' do
                @challenge.editor_template = nil
                component = load
                expect(component.valid?).to be(false)
                expect(component.errors.full_messages).to include("Challenge 1 must have editor template [\"basic_multiple_choice\"] but has nil"), component.errors.full_messages.inspect
            end

            it 'should be invalid unless all challenges have the answer list' do
                @challenge.answer_list_id = @another_answer_list.id
                component = load
                expect(component.valid?).to be(false)
                expect(component.errors.full_messages).to include("Challenge 1 answer list must equal value of shared_content_for_interactive"), component.errors.full_messages.inspect
            end

        end

        describe 'check_many=true' do

            before(:each) do
                @challenge = @check_many_challenge
                @frame.components << @challenge
                # mock out validations for references that are tested elsewhere
                @frame.components.each do |component|
                    allow(component).to receive(:valid?).and_return(true)
                end
            end

            it 'should be invalid' do
                component = load(valid_attrs)
                expect(component.valid?).to be(false)
                expect(component.errors.full_messages).to include("Challenge 1 must have editor template [\"basic_multiple_choice\"] but has \"check_many\""), component.errors.full_messages.inspect
            end

        end

        # since we don't decorate_frame_json in practice lessons, we disallow polling.  We could
        # bring it back, but seemed like we wouldn't want it
        describe 'in practice lesson' do

            it 'should be invalid' do
                allow(@frame).to receive(:lesson).and_return(Lesson.new)
                expect(@frame.lesson).to receive(:is_practice?).and_return(true)
                component = load(valid_attrs)
                expect(component.valid?).to be(false)
                expect(component.errors.full_messages).to include("Lesson for poll frame must not be a practice lesson"), component.errors.full_messages.inspect
            end

        end

    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.Challenges",
            editor_template: "multiple_choice_poll",
            layout_id: 'layout',
            behaviors: {
                GotoNextOnChallengeComplete: {},
                CompleteOnAllChallengesComplete: {}
            },
            challenge_ids: ['challenge'],
            shared_content_for_text_id: 'text',
            shared_content_for_image_id: 'image',
            shared_content_for_interactive_id: 'answer_list'
        }.deep_merge(overrides)
    end


end