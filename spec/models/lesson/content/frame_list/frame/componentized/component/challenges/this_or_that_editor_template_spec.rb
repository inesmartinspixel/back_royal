require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Challenges do



    describe 'with this_or_that template' do

    	before(:each) do
	        @frame = Lesson::Content::FrameList::Frame::Componentized.load
	        @frame.components = []
	        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})
	        @frame.components << (@tile_prompt_board = Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'tile_prompt_board', 
	        	component_type: 'ComponentizedFrame.TilePromptBoard',
	        	challenges_component_id: 'challenges',
	        	answer_list_id: 'shared_answer_list',
	        	tile_prompt_ids: ['tile_prompt']
	        }))
	        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'layout', component_type: 'ComponentizedFrame.Layout'})
	        @frame.components << (@shared_answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'shared_answer_list', 
	        	component_type: 'ComponentizedFrame.AnswerList'
	        }) )
	        @frame.components << (@distinct_answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'distinct_answer_list', 
	        	component_type: 'ComponentizedFrame.AnswerList'
	        }) )
	        @frame.components << (@another_answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'another_answer_list', 
	        	component_type: 'ComponentizedFrame.AnswerList'
	        }) )

	        @challenge = Lesson::Content::FrameList::Frame::Componentized::Component.load({
        		id: 'challenge', 
        		component_type: 'ComponentizedFrame.MultipleChoiceChallenge', 
        		editor_template: 'this_or_that',
        		answer_list_id: 'shared_answer_list',
                behaviors: {
                    ResetAnswersOnActivated: {},
                }
        	})
	        @frame.components << @challenge

	        @challenge_copy = Lesson::Content::FrameList::Frame::Componentized::Component.load({
        		id: 'challenge_copy', 
        		component_type: 'ComponentizedFrame.MultipleChoiceChallenge', 
        		editor_template: 'this_or_that',
        		answer_list_id: 'shared_answer_list',
                behaviors: {
                    ResetAnswersOnActivated: {},
                }
        	})
	        @frame.components << @challenge_copy


	        @text  =  Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'text', 
	        	component_type: 'ComponentizedFrame.Text', 
	        	text: 'main text'})
	        @frame.components << @text

	        @tile_prompt  =  Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'tile_prompt', 
	        	component_type: 'ComponentizedFrame.MatchingChallengeButton'})
	        @frame.components << @tile_prompt

	        @tile_prompt_2  =  Lesson::Content::FrameList::Frame::Componentized::Component.load({
	        	id: 'tile_prompt_2', 
	        	component_type: 'ComponentizedFrame.MatchingChallengeButton'})
	        @frame.components << @tile_prompt_2

	        # mock out validations for references that are tested elsewhere
	        @frame.components.each do |component|
	            allow(component).to receive(:valid?).and_return(true)
	        end
	    end

	    it 'should be valid if everything is awesome' do
	        component = load
	        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Challenges)
	        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
	    end

	    it 'should be invalid unless it has expected behaviors' do
	    	component = load({
            	behaviors: {}
        	})
	        expect(component.valid?).to be(false)
	        expect(component.errors.full_messages.include?("Goto next on challenge complete behavior must be included")).to be(true), component.errors.full_messages.inspect
	        expect(component.errors.full_messages.include?("Complete on all challenges complete behavior must be included")).to be(true), component.errors.full_messages.inspect
	    end

	    # we can assume that this covers both nil and invalid cases, since
	    # that's how validates_reference works
	    it 'should be invalid with a non-text component in sharedContentForText' do
	    	component = load({
            	shared_content_for_text_id: 'image'
        	})
        	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages.include?("Shared content for text must be an instance of Text but is Image")).to be(true), component.errors.full_messages.inspect
	    end

	    it 'should be invalid unless all challenges have this_or_that editor template' do
	    	@challenge.editor_template = nil
	    	component = load
	    	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages.include?("Challenge 1 must have editor template \"this_or_that\" but has nil")).to be(true), component.errors.full_messages.inspect
	    end

    	# we can assume that this covers both nil and invalid cases, since
	    # that's how validates_reference works
	    it 'should be invalid with non-TilePromptBoard component in sharedContentForInteractiveImage' do
	    	component = load({
            	shared_content_for_interactive_image_id: 'text'
        	})
        	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages.include?("Shared content for interactive image must be an instance of TilePromptBoard but is Text")).to be(true), component.errors.full_messages.inspect
	    end


	    it 'should be invalid if there are not the same number of tile prompts as challenges' do
	    	@tile_prompt_board.tile_prompts = []
	    	component = load
        	expect(component.valid?).to be(false)
	        expect(component.errors.full_messages.include?("Tile prompt count must be equal to the number of challenges (0 != 1)")).to be(true), component.errors.full_messages.inspect
	    end

    	it 'should be invalid if a challenge does not have ResetAnswersOnActivated behavior' do
    		@challenge.behaviors = {}
    		component = load
    		expect(component.valid?).to be(false)
        	expect(component.errors.full_messages.include?("Challenge 1 must have behavior \"ResetAnswersOnActivated\"")).to be(true), component.errors.full_messages.inspect
    	end

	    it 'should be invalid unless all challenges have the answer list' do
	    	@challenge.answer_list_id = @another_answer_list.id
	    	component = load
	    	expect(component.valid?).to be(false)           
	        expect(component.errors.full_messages.include?("Challenge 1 answer list must equal value of tile_prompt_board_answer_list")).to be(true), component.errors.full_messages.inspect
	    end

    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            id: 'challenges',
            component_type: "ComponentizedFrame.Challenges",
            editor_template: "this_or_that",
            layout_id: 'layout',
            behaviors: {
            	GotoNextOnChallengeComplete: {},
            	CompleteOnAllChallengesComplete: {}
            },
            challenge_ids: ['challenge'],
            shared_content_for_text_id: 'text',
            shared_content_for_interactive_image_id: 'tile_prompt_board'
        }.deep_merge(overrides)
    end 
    

end