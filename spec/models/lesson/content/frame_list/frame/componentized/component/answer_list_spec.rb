require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::AnswerList do

    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'answer1', component_type: 'ComponentizedFrame.SelectableAnswer'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'answer2', component_type: 'ComponentizedFrame.SelectableAnswer'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image_hotspot_answer', x: 42, y: 42, component_type: 'ComponentizedFrame.SelectableAnswer'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'invalid_image_hotspot_answer', component_type: 'ComponentizedFrame.SelectableAnswer'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'image', component_type: 'ComponentizedFrame.Image'})

        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'header1', component_type: 'ComponentizedFrame.Text'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'header2', component_type: 'ComponentizedFrame.Text'})

        # mock out validations for references that are tested elsewhere
        @frame.components.each do |component|
            allow(component).to receive(:valid?).and_return(true)
        end
    end

    it 'should be valid if everything is awesome' do
        component = load(valid_attrs)
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::AnswerList)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-SelectableAnswer component as answer' do
        component = load({
            answer_ids: ['image']
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Answer 1 must be an instance of SelectableAnswer but is Image"), component.errors.full_messages.inspect
    end

    describe 'with venn_diagram skin' do
        it 'should be valid if everything is awesome' do
            component = load_venn_diagram
            expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::AnswerList)
            expect(component.valid?).to eq(true), component.errors.full_messages.inspect
        end

        it 'should be invalid for venn diagram with non-text header components' do
            component = load_venn_diagram({
                venn_diagram_header_ids: ['image', 'image']
            })
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Venn diagram header 1 must be an instance of Text but is Image"), component.errors.full_messages.inspect
            expect(component.errors.full_messages).to include("Venn diagram header 2 must be an instance of Text but is Image"), component.errors.full_messages.inspect
        end

        it 'should be invalid for venn diagram without exactly 2 headers' do
            component = load_venn_diagram({
                venn_diagram_header_ids: ['header1']
            })
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Venn diagram headers must have exactly two elements"), component.errors.full_messages.inspect
        end

    end

    describe 'with image_hotspot skin' do

        it 'should be valid with less than 2 answers' do
            component = load({
                answer_ids: ['image_hotspot_answer'],
                skin: 'image_hotspot'
            })
            expect(component.valid?).to eq(true), component.errors.full_messages.inspect
        end

        it 'should be invalid with less than 1 answer' do
            component = load({
                answer_ids: [],
                skin: 'image_hotspot'
            })
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Answers is too short (min is 1)"), component.errors.full_messages.inspect
        end

        it 'should be invalid with no x or y values' do
            component = load({
                answer_ids: ['invalid_image_hotspot_answer'],
                skin: 'image_hotspot'
            })
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Answer 1 must have a value for 'y'"), component.errors.full_messages.inspect
            expect(component.errors.full_messages).to include("Answer 1 must have a value for 'x'"), component.errors.full_messages.inspect
        end

    end


    it 'should be invalid with less than 2 answers' do
    	component = load({
            answer_ids: ['answer1']
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Answers is too short (min is 2)"), component.errors.full_messages.inspect
    end

    it 'should be invalid with an invalid skin' do
        component = load({
            skin: 'invalid'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Skin is not included in the list"), component.errors.full_messages.inspect
    end

    describe 'RandomizeAnswerOrder' do
        it 'should be valid with answer ids in ensureLast' do
            component = load({
                behaviors: {
                    RandomizeAnswerOrder: {
                        ensureLast: ['answer1']
                    }
                }
            })
            expect(component.valid?).to be(true), component.errors.full_messages.inspect
        end

        it 'should be invalid if ensureLast references a non-existent answer' do
            component = load({
                behaviors: {
                    RandomizeAnswerOrder: {
                        ensureLast: ['nonexistent']
                    }
                }
            })
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Randomize answer order options ensurelast must include ids of answer in the list. [\"nonexistent\"] are not in the list"), component.errors.full_messages.inspect
        end

        it 'should be invalid if ensureLast is empty' do
            component = load({
                behaviors: {
                    RandomizeAnswerOrder: {
                        ensureLast: []
                    }
                }
            })
            expect(component.valid?).to be(false)
            expect(component.errors.full_messages).to include("Randomize answer order options ensurelast cannot be empty"), component.errors.full_messages.inspect
        end
    end


    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def load_venn_diagram(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_venn_diagram_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.AnswerList",
            answer_ids: ['answer1', 'answer2'],
            skin: 'buttons'
        }.deep_merge(overrides)
    end

    def valid_venn_diagram_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.AnswerList",
            answer_ids: ['answer1', 'answer2'],
            venn_diagram_header_ids: ['header1', 'header2'],
            skin: 'venn_diagram'
        }.deep_merge(overrides)
    end

end