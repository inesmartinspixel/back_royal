require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Text do

    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        @frame.components << (@modal = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'modal',
            component_type: 'ComponentizedFrame.Text',
            text: 'text',
            formatted_text: '<p>text</p>'
        }))
    end

    it 'should be valid if everything is awesome' do
        component = load(valid_attrs)
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Text)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    it 'should not allow blank text' do
        component = load(valid_attrs.merge({
            text: ""
        }))
        expect(component.valid?).to eq(false)
        expect(component.errors.full_messages).to include("Text can't be blank"), component.errors.full_messages.inspect
    end

    it 'should allow blank text if allowBlank is true' do
        component = load(valid_attrs.merge({
            text: "",
            allowBlank: true
        }))
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    it 'should be invalid if modals are invalid' do
        @modal.text = nil
        component = load
        expect(component.valid?).to eq(false)
        expect(component.errors.full_messages).to include("Modal 1 Text can't be blank"), component.errors.full_messages.inspect
    end

    it 'should be invalid if MathJax hasn\'t processed correctly' do
        component = load({
            component_type: "ComponentizedFrame.Text",
            text: "some text",
            formatted_text: '<p>[Math Processing Error]</p>',
            modal_ids: ['modal']
        })
        expect(component.valid?).to eq(false)
        expect(component.errors.full_messages).to include("Formatted text formatted_text appears to contain improperly processed MathJax. This may be caused by invalid whitespaces or extended unicode. Make a change to the math expression to force reformatting."), component.errors.full_messages.inspect
    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.Text",
            text: "some text",
            formatted_text: '<p>some text</p>',
            modal_ids: ['modal']
        }.deep_merge(overrides)
    end


end