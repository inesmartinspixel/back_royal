require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::Layout do
    
    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
    end

    # only subclasses can be created
    it 'should be invalid' do
        component = load
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Layout)
        expect(component.valid?).to eq(false), component.errors.full_messages.inspect
        expect(component.errors.full_messages).to include("Is layout subclass? must be true"), component.errors.full_messages.inspect
    end
    
    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.Layout"
        }.deep_merge(overrides)
    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end
    

end