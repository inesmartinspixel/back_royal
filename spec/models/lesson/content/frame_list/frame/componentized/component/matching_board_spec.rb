require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized::Component::MatchingBoard do
    
    before(:each) do
        @frame = Lesson::Content::FrameList::Frame::Componentized.load
        @frame.components = []
        
        @frame.components << (@answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'answer_list', 
            component_type: 'ComponentizedFrame.AnswerList'
        }) )
        @frame.components << (@another_answer_list = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'another_answer_list', 
            component_type: 'ComponentizedFrame.AnswerList'
        }) )

        @challenge = Lesson::Content::FrameList::Frame::Componentized::Component.load({
            id: 'challenge', 
            component_type: 'ComponentizedFrame.MultipleChoiceChallenge', 
            editor_template: 'matching',
            answer_list_id: 'answer_list'
        })
        @frame.components << @challenge

        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'challenge_button_1', component_type: 'ComponentizedFrame.MatchingChallengeButton'})
        @frame.components << Lesson::Content::FrameList::Frame::Componentized::Component.load({id: 'challenge_button_2', component_type: 'ComponentizedFrame.MatchingChallengeButton'})
        
        # mock out validations for references that are tested elsewhere
        @frame.components.each do |component|
            allow(component).to receive(:valid?).and_return(true)
        end
    end

    it 'should be valid if everything is awesome' do
        component = load(valid_attrs)
        expect(component.class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::MatchingBoard)
        expect(component.valid?).to eq(true), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-AnswerList as answer list' do
        component = load({
            answer_list_id: 'challenge_button_1'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Answer list must be an instance of AnswerList but is MatchingChallengeButton"), component.errors.full_messages.inspect
    end

    # we can assume that this covers both nil and invalid cases, since
    # that's how validates_reference works
    it 'should be invalid with non-MatchingChallengeButton as matching_challenge_button' do
        component = load({
            matching_challenge_button_ids: 'answer_list'
        })
        expect(component.valid?).to be(false)
        expect(component.errors.full_messages).to include("Matching challenge buttons must be an instance of MatchingChallengeButton but is AnswerList"), component.errors.full_messages.inspect
    end

    def load(attrs = {})
        component = Lesson::Content::FrameList::Frame::Componentized::Component.load(valid_attrs.merge(attrs))
        @frame.components << component
        component
    end

    def valid_attrs(overrides = {})
        {
            component_type: "ComponentizedFrame.MatchingBoard",
            matching_challenge_button_ids: ['challenge_button_1', 'challenge_button_2'],
            answer_list_id: 'answer_list'
        }.deep_merge(overrides)
    end 
    

end