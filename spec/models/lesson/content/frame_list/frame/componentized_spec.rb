require 'spec_helper'

describe Lesson::Content::FrameList::Frame::Componentized do

    it 'should instantiate components' do
        frame = load
        expect(frame.components[0].class).to be(Lesson::Content::FrameList::Frame::Componentized::Component::Text)
    end

    it 'should be valid if everything is awesome' do
        frame = load
        allow(@unreferenced_component).to receive(:allow_unreferenced?).and_return(true)
        expect(frame.valid?).to be(true), frame.errors.full_messages.inspect
    end

    it 'should be invalid without a main ui component' do
        frame = load({main_ui_component_id: nil})
        expect(frame.valid?).to be(false)
        expect(frame.errors.full_messages).to include("Main ui component can't be blank"), frame.errors.full_messages.inspect
    end

    it 'should be invalid without a continue button' do
        frame = load({continue_button_id: nil})
        expect(frame.valid?).to be(false)
        expect(frame.errors.full_messages).to include("Continue button can't be blank"), frame.errors.full_messages.inspect
    end

    it 'should be invalid without a frame navigator' do
        frame = load({frame_navigator_id: nil})
        expect(frame.valid?).to be(false)
        expect(frame.errors.full_messages).to include("Frame navigator can't be blank"), frame.errors.full_messages.inspect
    end

    it 'should be invalid if main ui component is invalid' do
        frame = load
        frame.main_ui_component.text = nil

        expect(frame.valid?).to be(false)
        expect(frame.errors.full_messages).to include("Text can't be blank"), frame.errors.full_messages.inspect
    end

    it 'should be invalid if continue button is invalid' do
        frame = load
        frame.continue_button.text = nil

        expect(frame.valid?).to be(false)
        expect(frame.errors.full_messages).to include("Continue button text can't be blank"), frame.errors.full_messages.inspect
    end

    it 'should be invalid if frame navigator is invalid' do
        frame = load
        frame.frame_navigator.text = nil

        expect(frame.valid?).to be(false)
        expect(frame.errors.full_messages).to include("Frame navigator text can't be blank"), frame.errors.full_messages.inspect
    end

    it 'should be invalid if unreferenced component is invalid' do
        frame = load
        allow(@unreferenced_component).to receive(:allow_unreferenced?).and_return(true)
        @unreferenced_component.text = nil
        expect(frame.valid?).to be(false)
        expect(frame.errors.full_messages).to include("Text component 2 text can't be blank"), frame.errors.full_messages.inspect
    end

    it 'should be invalid if unreferenced component is not allowed to be unreferenced' do
        frame = load
        allow(@unreferenced_component).to receive(:allow_unreferenced?).and_return(false)
        expect(frame.valid?).to be(false)
        expect(frame.errors.full_messages).to include("Text component 2 is not referenced (or at least it was not validated. id=\"#{@unreferenced_component_id}\")"), frame.errors.full_messages.inspect
    end

    it 'should be invalid if unreferenced component is allowed to be unreferenced' do
        frame = load
        allow(@unreferenced_component).to receive(:allow_unreferenced?).and_return(true)
        expect(frame.valid?).to be(true), frame.errors.full_messages.inspect
    end

    describe 'decorate_frame_json' do

        before(:each) do
            @frames = [load, load]
            @frames_json = @frames.map(&:as_json)
            @lesson_id = SecureRandom.uuid
            @answer_ids = [SecureRandom.uuid, SecureRandom.uuid]

            @usage_reports = [
                FactoryBot.create(:multiple_choice_challenge_usage_report, {
                    lesson_id: @lesson_id,
                    frame_id: @frames[0].id,
                    challenge_id: @frames[0].components[0].id,
                    answer_id: @answer_ids[0],
                    count: 3,
                }),
                FactoryBot.create(:multiple_choice_challenge_usage_report, {
                    lesson_id: @lesson_id,
                    frame_id: @frames[0].id,
                    challenge_id: @frames[0].components[0].id,
                    answer_id: @answer_ids[1],
                    count: 42,
                }),
                FactoryBot.create(:multiple_choice_challenge_usage_report, {
                    lesson_id: @lesson_id,
                    frame_id: @frames[1].id,
                    challenge_id: @frames[1].components[0].id,
                    answer_id: @answer_ids[0],
                    count: 79,
                })
            ]
        end

        it 'should add information from usage reports into the json' do
            @decorated_frame_hashes = Lesson::Content::FrameList::Frame::Componentized.decorate_frame_json(
                @lesson_id, @frames_json)

            assert_answer_counts(0, 0, {
                @answer_ids[0] => 3,
                @answer_ids[1] => 42
            })
            assert_answer_counts(0, 1, nil)
            assert_answer_counts(1, 0, {
                @answer_ids[0] => 79
            })
        end

        def assert_answer_counts(frame_index, component_index, expected_answer_counts)
            expect(frame_hash = @decorated_frame_hashes[frame_index]).not_to be(nil)
            expect(component_hash = frame_hash['components'][component_index]).not_to be(nil)

            usage_report = component_hash['usage_report']
            if expected_answer_counts.nil?
                expect(usage_report).to be(nil)
            else
                expect(usage_report).not_to be(nil)

                expect(usage_report['first_answer_counts']).to eq(expected_answer_counts)
            end
        end

    end

    def load(attrs = {})
        @main_ui_component_id = SecureRandom.uuid
        @unreferenced_component_id = SecureRandom.uuid
        frame = Lesson::Content::FrameList::Frame::Componentized.load({
            id: SecureRandom.uuid,
            frame_type: "componentized",
            components: [
                {
                    component_type: "ComponentizedFrame.Text",
                    id: @main_ui_component_id,
                    text: 'text',
                    formatted_text: '<p>text</p>'
                },
                {
                    component_type: "ComponentizedFrame.Text",
                    id: @unreferenced_component_id,
                    text: 'text',
                    formatted_text: '<p>text</p>'
                }],
            main_ui_component_id: @main_ui_component_id,
            continue_button_id: @main_ui_component_id,
            frame_navigator_id: @main_ui_component_id
        }.merge(attrs))

        @unreferenced_component = frame.components.last
        frame
    end

end