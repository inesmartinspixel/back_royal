require 'spec_helper'

describe Lesson::ToJsonFromApiParams do

    fixtures(:lesson_streams, :lessons, :users, :lesson_streams_progress)


    before(:each) do
        @user = User.first
    end

    it "should have expected keys" do
        lesson = lessons(:frame_list_lesson).versions.last
        lesson_json = get_json({filters: {id: lesson.attributes['id'], published: false}}).first

        expect(lesson_json.keys).to match_array(%w|
            id title description frame_count entity_metadata
            client_requirements lesson_type
            author updated_at modified_at archived
            old_version pinned_title pinned_description
            was_published pinned tag stream_titles version_id
            last_editor published_at published_version_id version_history
            frames key_terms assessment unrestricted locale locale_pack
            practice_frames test
        |)

        {
            "id" => lesson.attributes['id'],
            "title" => lesson.title,
            "description" => lesson.description,
            "frame_count" => lesson.frame_count,
            "entity_metadata" => lesson.entity_metadata.attributes.slice("id", "title", "description", "canonical_url", "tweet_template").merge("image" => image_json(lesson.entity_metadata.image)),
            "client_requirements" => {
                "min_allowed_version" => 0,
                "supported_in_latest_available_version" => true
            },
            "lesson_type" => lesson.content.lesson_type,
            "author" => lesson.author.as_json(for: :public),
            "updated_at" => lesson.updated_at.to_timestamp,
            "modified_at" => lesson.modified_at.to_timestamp,
            "archived" => lesson.archived,
            "tag" => lesson.tag,
            "stream_titles" => lesson.lesson_streams_with_just_titles.map(&:title).sort,
            "version_id" => lesson.version_id,
            "last_editor" => lesson.last_editor.as_json(for: :public),
            "frames" => lesson.content_json['frames'],
            "key_terms" => lesson.key_terms,
            "test" => lesson.test

        }.each do |key, value|
            expect(value).not_to be_nil, "Expecting all simple values to be set. #{key.inspect} is not"

            if key == 'stream_titles'
                expect(lesson_json[key]).to match_array(value), "Unexpected value for #{key.inspect}. expected #{value.inspect} **** GOT *** #{lesson_json[key].inspect}"
            else
                expect(lesson_json[key]).to eq(value), "Unexpected value for #{key.inspect}. expected #{value.inspect} **** GOT *** #{lesson_json[key].inspect}"
            end

        end
    end

    it "should filter by multiple ids" do
        lesson_ids = Lesson.limit(2).pluck('id')
        json = get_json({filters: {id: lesson_ids, published: false}})
        expect(json.size).to be(2)
    end

    it "should return nothing when id is an empty array" do
        json = get_json({filters: {id: [], published: false}})
        expect(json.size).to be(0)
    end

    it "should return empty array when key_terms is blank" do
        lesson = Lesson.first
        lesson.update(:key_terms => [])
        json = get_json({filters: {id: lesson.id, published: false}})
        expect(json[0]['key_terms']).to eq([])
    end

    it "should filter by is_practice" do
        lesson = lessons(:with_practice_lesson)
        practice_lesson = lesson.practice_lesson
        lesson_ids = [lesson, practice_lesson].map(&:id)

        result = get_json({filters: {id: lesson_ids, published: false}})
        expect(result.size).to be(2) # sanity check

        result = get_json({filters: {id: lesson_ids, published: false, is_practice: false}})
        expect(result.size).to be(1)
        expect(result[0]['id']).to eq(lesson.id)
    end

    describe "locale_pack" do

        it "should include the locale_pack" do
            lesson = lessons(:en_item)
            lessons_in_pack = [lessons(:en_item), lessons(:es_item), lessons(:zh_item)]

            json = get_json({fields: ['id', 'locale_pack'], filters: {id: lesson.id, published: false}})[0]
            expect(json['locale_pack']).not_to be_nil
            expect(json['locale_pack']['id']).to eq(lesson.locale_pack_id)
            expect(json['locale_pack']['content_items']).to match_array(lessons_in_pack.map { |lesson|
                {
                    'id' => lesson.id,
                    'title' => lesson.title,
                    'locale' => lesson.locale
                }
            })
        end

        it "should include practice_locale_pack on the locale_pack" do
            lesson = lessons(:with_practice_lesson)
            json = get_json({fields: ['id', 'locale_pack'], filters: {id: lesson.id, published: false}})[0]

            expect(json['locale_pack']['practice_locale_pack_id']).to eq(lesson.locale_pack.practice_locale_pack_id)
            expect(json['locale_pack']['practice_content_items'].map { |c| c['id']}).to match_array(lesson.practice_locale_pack.content_items.ids)
        end

        it "should include is_practice_for_locale_pack on the locale_pack" do
            lesson = lessons(:with_practice_lesson)
            json = get_json({fields: ['id', 'locale_pack'], filters: {id: lesson.practice_lesson.id, published: false}})[0]

            expect(json['locale_pack']['is_practice_for_locale_pack_id']).to eq(lesson.locale_pack_id)
        end

        it "should respect the published filter" do
            lesson = lessons(:en_item)
            lessons(:es_item).unpublish!
            lessons_in_pack = [lessons(:en_item), lessons(:zh_item)]
            lessons_in_pack.map(&:publish!)

            json = get_json({fields: ['id', 'locale_pack'], filters: {id: lesson.id, published: true}})[0]
            expect(json['locale_pack']).not_to be_nil
            expect(json['locale_pack']['id']).to eq(lesson.locale_pack_id)
            expect(json['locale_pack']['content_items']).to match_array(lessons_in_pack.map { |lesson|
                {
                    'id' => lesson.id,
                    'title' => lesson.title,
                    'locale' => lesson.locale
                }
            })
        end

    end

    it "should include lesson_progress if indicated" do
        completed_stream_progress = lesson_streams_progress(:completed_stream_progress)
        user_id = completed_stream_progress.user_id
        lesson = completed_stream_progress.lesson_stream.lessons.detect(&:has_published_version?)
        completed_lesson_progress = LessonProgress.where(user_id: user_id, locale_pack_id: lesson.locale_pack_id).first

        lesson_json = get_json({
            filters: {id: lesson.id, published: false},
            user_id: user_id
        }).first

        expect(lesson_json.key?('lesson_progress')).to be(true)
        expect(lesson_json['lesson_progress']).to eq({
            :locale_pack_id => lesson.locale_pack_id,
            :frame_bookmark_id => completed_lesson_progress.frame_bookmark_id,
            :frame_history => completed_lesson_progress.frame_history,
            :completed_frames => completed_lesson_progress.completed_frames,
            :challenge_scores => completed_lesson_progress.challenge_scores,
            :frame_durations => completed_lesson_progress.frame_durations,
            :started_at => completed_lesson_progress.started_at.to_timestamp,
            :completed_at => completed_lesson_progress.completed_at.to_timestamp,
            :complete => true,
            :last_progress_at => completed_lesson_progress.last_progress_at.to_timestamp,
            :best_score => completed_lesson_progress.best_score,
            :id => completed_lesson_progress.id,
            :user_id => completed_lesson_progress.user_id,
            :for_assessment_lesson => completed_lesson_progress.for_assessment_lesson?,
            :for_test_lesson => completed_lesson_progress.for_test_lesson?,
            :updated_at => completed_lesson_progress.updated_at.to_timestamp,
            :created_at => completed_lesson_progress.created_at.to_timestamp
        }.as_json)
    end

    it "should include publishing information and version history" do
        published_stream = lesson_streams(:published_stream)
        published_lesson = published_stream.lessons.detect(&:has_published_version?)
        working_version = published_lesson.versions.last
        published_lesson_version = published_lesson.published_version

        expected_version_history = published_lesson.versions.reorder('updated_at desc').map do |version|
            {
                last_editor: version.last_editor.as_json(for: :public),
                updated_at: version.updated_at.to_timestamp,
                version_id: version.version_id,
                pinned: version.pinned,
                pinned_title: version.pinned_title,
                pinned_description: version.pinned_description,
                was_published: version.was_published
            }.as_json
        end

        working_version_json = get_json({filters: {id: published_lesson.id, published: false}}).first
        expect(working_version_json['published_at']).to eq(published_lesson.content_publisher.published_at.to_timestamp)
        expect(working_version_json['published_version_id']).to eq(published_lesson_version.version_id)
        expect(working_version_json['version_history']).to eq(expected_version_history)


        published_version_json = get_json({filters: {id: published_lesson.id, published: true}}).first
        expect(published_version_json['published_at']).to eq(published_lesson.content_publisher.published_at.to_timestamp)
        expect(published_version_json['published_version_id']).to eq(published_lesson_version.version_id)
        expect(published_version_json['version_history']).to eq(expected_version_history)
    end

    it "should respect archived filter" do
        # make sure there is an archived lesson
        lesson = Lesson.first
        lesson.archived = true
        lesson.save!
        expect(get_json({filters: {archived: true, published: false}}).map { |l| l['id']}).to match_array(Lesson.where(archived: true).pluck('id'))
        expect(get_json({filters: {archived: false, published: false}}).map { |l| l['id']}).to match_array(Lesson.where(archived: false).pluck('id'))
    end

    it "should not return a deleted lesson" do
        stream = lesson_streams(:stream_with_many_versions)
        lesson = stream.lessons.first
        lesson.destroy
        expect(get_json({filters: {published: false, id: lesson.id}})).to eq([])
    end

    it "should decorate frames" do
        lesson = lessons(:frame_list_lesson).versions.last
        expect(lesson.frames).not_to be_nil
        expect(Lesson::Content::FrameList).to receive(:decorate_frame_json) do |id, frames|
            expect(id).to eq(lesson.attributes['id'])
            frames[0]['decorated'] = true
        end
        lesson_json = get_json({filters: {id: lesson.attributes['id'], published: false}}).first

        expect(lesson_json).not_to be_nil

        expected_frames = lesson.content_json['frames']
        expected_frames[0]['decorated'] = true
        expect(lesson_json['frames']).to eq(expected_frames)


    end

    describe "updated filters" do
        it "should respect updated_since filter" do
            lesson = Lesson.first
            updated_at = lesson.updated_at
            expect(get_json({fields: ['id'], filters: {published: false, updated_since: (updated_at + 1.minute).to_timestamp}}).map { |l| l['id'] }).not_to include(lesson.id)
            expect(get_json({fields: ['id'], filters: {published: false, updated_since: (updated_at - 1.minute).to_timestamp}}).map { |l| l['id'] }).to include(lesson.id)
        end

        it "should consider updated_at on locale pack when processing updated_since filter" do
            lesson = Lesson.first
            updated_at = lesson.locale_pack.updated_at
            lesson.update_attribute(:updated_at,  updated_at - 3.days)
            expect(get_json({fields: ['id'], filters: {published: false, updated_since: (updated_at + 1.minute).to_timestamp}}).map { |l| l['id'] }).not_to include(lesson.id)
            expect(get_json({fields: ['id'], filters: {published: false, updated_since: (updated_at - 1.minute).to_timestamp}}).map { |l| l['id'] }).to include(lesson.id)
        end

        it "should respect updated_before filter" do
            lesson = Lesson.first
            updated_at = lesson.updated_at
            expect(get_json({filters: {published: false, updated_before: (updated_at + 1.minute).to_timestamp}}).map { |l| l['id'] }).to include(lesson.id)
            expect(get_json({filters: {published: false, updated_before: (updated_at - 1.minutes).to_timestamp}}).map { |l| l['id'] }).not_to include(lesson.id)
        end
    end

    describe "filter_editable_resources" do

        it "should return the lessons that an editor has created or has permissions for" do
            editor = users(:editor)
            lesson_ids = get_json({filters: {published: false, filter_editable_resources: editor.id}}).map { |l| l['id'] }

            expected_lesson_ids = [
                lessons(:editor_has_lesson_editor),
                lessons(:editor_has_previewer),
                lessons(:editor_has_reviewer),
                lessons(:editor_created)
            ].map(&:id)
            expect(lesson_ids).to match_array(expected_lesson_ids)
        end

    end

    it "should work without an author entirely" do
        lesson = lessons(:frame_list_lesson).versions.last
        ActiveRecord::Base.connection.execute "UPDATE lessons_versions SET author_id = NULL"
        lesson_json = get_json({filters: {id: lesson.attributes['id'], published: false}}).first

        expect(lesson_json['id']).to eq(lesson.attributes['id'])
        expect(lesson_json['author']).to be_nil
    end


    it "should not blowup on invalid tsvector searches" do
        expect{
            get_json({filters: {content_search: '%%P\left('}})
        }.not_to raise_error()
    end

    describe "practice frames" do

        it "should return practice frames" do
            lesson = lessons(:with_published_practice_frames)
            lesson_json = get_json({
                filters: {id: lesson.id, published: true},
                fields: ['practice_frames']
            }).first
            expect(lesson_json['practice_frames']).to eq(lesson.practice_lesson.content.frames.as_json.map(&:deep_stringify_keys))
        end

        it "should not show unpublished practice frames" do
            lesson = lessons(:with_unpublished_practice_frames)
            lesson_json = get_json({
                filters: {id: lesson.id, published: true},
                fields: ['practice_frames']
            }).first
            expect(lesson_json['practice_frames']).to eq([])
        end

    end

    def get_json(params)
        json = Lesson::ToJsonFromApiParams.new(params).json
        ActiveSupport::JSON.decode(json)
    end

    def image_json(image)
        {
            'id' => image.id,
            'formats' => image.formats.deep_stringify_keys,
            'dimensions' => image.dimensions.deep_stringify_keys
        }
    end


end
