require 'spec_helper'

describe Lesson::Content do
    
    def valid_attrs(overrides = {})
        {
            lesson_type: 'frame_list'
        }.merge(overrides)
    end
    
    def get_content(attrs = {})
        Lesson::Content.load(valid_attrs(attrs))
    end
    
    def expect_invalid(attrs)
        expect(get_content(attrs).valid?).to be(false)
    end

    # if there is no lesson_type or an invalid one, we don't want errors, 
    # we just want things to be invalid
    it 'should return superclass if invalid lesson_type passed in' do
        content = get_content(lesson_type: 'unknown')
        expect(content.class).to be(Lesson::Content)
        content.valid?
        expect(content.valid?).to be(false)
    end
    
    # in the creation of lessons, it is easiest if we allow content to be
    # instantiated without a lesson_type.  These should not be saved, however,
    # so should be invalid
    it 'should return the superclass if no lesson_type is passed in' do
        content = get_content(lesson_type: '')
        expect(content.class).to be(Lesson::Content)
        expect(content.valid?).to be(false)
    end

    
end