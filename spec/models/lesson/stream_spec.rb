# == Schema Information
#
# Table name: lesson_streams
#
#  id                     :uuid             not null, primary key
#  created_at             :datetime
#  updated_at             :datetime
#  title                  :string(255)
#  description            :text
#  author_id              :uuid
#  image_id               :uuid
#  chapters               :json
#  modified_at            :datetime
#  pinned                 :boolean          default(FALSE), not null
#  pinned_title           :string(255)
#  pinned_description     :text
#  was_published          :boolean          default(FALSE), not null
#  last_editor_id         :uuid
#  entity_metadata_id     :uuid
#  credits                :text
#  what_you_will_learn    :string           default([]), is an Array
#  resource_downloads     :json
#  resource_links         :json
#  recommended_stream_ids :uuid             default([]), is an Array
#  related_stream_ids     :uuid             default([]), is an Array
#  coming_soon            :boolean          default(FALSE), not null
#  just_added             :boolean          default(FALSE), not null
#  summaries              :json
#  beta                   :boolean          default(FALSE), not null
#  just_updated           :boolean          default(FALSE), not null
#  locale_pack_id         :uuid
#  locale                 :text             default("en"), not null
#  duplicated_from_id     :uuid
#  duplicated_to_id       :uuid
#  unlocked_for_mba_users :boolean          default(FALSE)
#  exam                   :boolean          default(FALSE), not null
#  time_limit_hours       :float
#  exam_open_time         :datetime
#  exam_close_time        :datetime
#  tag                    :text
#

require 'spec_helper'

describe Lesson::Stream do

    fixtures(:lesson_streams, :users, :lesson_streams_progress, :playlists, :lessons, :cohorts)

    describe "chapters" do
        before(:each) do
            @lessons = Lesson.limit(4)
            @stream = Lesson::Stream.where.not(locale_pack_id: nil).first
            @stream.lessons = [@lessons[1], @lessons[2], @lessons[0]]
            @stream.locale_pack.add_to_group("GROUP")
        end

        it 'should error on save if no title' do
            @stream.chapters = [
                {"lesson_ids"=>[@lessons[0].id, @lessons[1].id]}
            ]
            expect(@stream.valid?).to be(false)
            expect(@stream.errors.full_messages).to include("Chapters 1 must have a title"), @stream.errors.full_messages.inspect
        end

        it 'should error on save if lesson list contains invalid lesson_ids' do
            @stream.chapters = [
                {"title"=>"Chapter 1", "lesson_ids"=>["invalid lesson guid", @lessons[1].id]}
            ]
            expect(@stream.valid?).to be(false)
            expect(@stream.errors.full_messages).to include("Chapters 1 lesson id invalid lesson guid must be the id of a lesson contained in this course"), @stream.errors.full_messages.inspect

        end

        it 'should error on save if lesson list contains no lesson_hashes' do
            @stream.chapters = [
                {"title"=>"Chapter 1", "lesson_ids"=>["invalid lesson guid", @lessons[1].id]}
            ]
            expect(@stream.valid?).to be(false)
            expect(@stream.errors.full_messages).to include("Chapters 1 must have a list of lesson_hashes"), @stream.errors.full_messages.inspect

        end

        it 'should error on save if lesson list contains lesson_ids that do not match lesson_hashes' do
            @stream.chapters = [
                {"title"=>"Chapter 1", "lesson_ids"=>[@lessons[1].id], "lesson_hashes" => [{"lesson_id" => SecureRandom.uuid}]}
            ]
            expect(@stream.valid?).to be(false)
            expect(@stream.errors.full_messages).to include("Chapters 1 lesson_hashes must match lesson_ids"), @stream.errors.full_messages.inspect

        end

    end

    describe "destroy" do


        # I just wanted to make sure that I understood how dependent => destroy worked
        # with belongs_to
        it "should not destroy the associated s3 asset when destroyed" do
            stream = Lesson::Stream.where("image_id is not null").first
            image = stream.image
            expect(image).not_to be(nil)

            # ignore any playlists that don't want us to mess with this stream
            allow_any_instance_of(Playlist).to receive(:valid?).and_return(true)
            stream.destroy
            expect(S3Asset.find_by_id(image.id)).not_to be(nil)
        end

        it "should error if in a playlist" do
            playlist = Playlist.first
            stream = playlist.streams.first
            allow(stream).to receive(:destroy_locale_pack_if_en) # stub this out so that we don't hit this error
            expect(Proc.new {
                stream.destroy
            }).to raise_error(ActiveRecord::RecordInvalid)
        end

    end

    describe "create_from_hash" do

        it "should create an instance from a hash" do
            # I was running into a crazy mongo mapper error when these lessons
            # had frames in them.  Hoping that ignoring it is not hiding
            # a real issue ....
            lessons = Lesson.where("lesson_type != 'frame_list'").limit(3)
            user = User.first
            image = S3Asset.first

            stream = Lesson::Stream.create_from_hash!({
                :tag => "TAG",
                :title => "My Title",
                :description => "A description",
                :lessons => lessons,
                :author => user.as_json(for: :public),
                :image => image,
                :chapters => nil
            }.as_json)

            # check that it was saved successfully
            expect(stream.new_record?).to be(false)

        end

    end

    describe "update_from_hash!" do

        before(:each) do
            @stream = Lesson::Stream.first
            @lessons = @stream.lessons
            @author = @stream.author
        end

        it 'should add a lesson' do
            lesson = Lesson.where("id NOT IN (?)", @lessons.map(&:id)).first
            hash = @stream.as_json
            hash['lessons'] << lesson.as_json
            hash['chapters'][0]['lesson_ids'] << lesson.id
            hash['chapters'][0]['lesson_hashes'] << {'lesson_id' => lesson.id}
            expect(@stream.reload.lessons.include?(lesson)).to be(false) #sanity check
            Lesson::Stream.update_from_hash!(@author, hash)
            expect(@stream.reload.lessons.include?(lesson)).to be(true)
        end

        it 'should remove a lesson' do
            lesson_id = @stream.chapters[0]['lesson_ids'].first
            hash = @stream.as_json
            hash['lessons'].delete_if { |l| l['id'] == lesson_id }
            hash['chapters'].each { |c|
                c['lesson_ids'].delete(lesson_id)
                c['lesson_hashes'].delete_if { |l| l['lesson_id'] == lesson_id }
            }
            expect(@stream.reload.lessons.map(&:id).include?(lesson_id)).to be(true) #sanity check
            Lesson::Stream.update_from_hash!(@author, hash)
            expect(@stream.reload.lessons.map(&:id).include?(lesson_id)).to be(false)
        end

        it 'should update image' do
            image = @stream.get_image_asset('another_image.png')
            image.save!
            hash = @stream.as_json
            hash['image'] = image.as_json
            expect(@stream.reload.image).not_to eq(image) #sanity check
            Lesson::Stream.update_from_hash!(@author, hash)
            expect(@stream.reload.image).to eq(image)
        end

        it 'should handle a nil image value' do
            stream = Lesson::Stream.first
            expect(stream.image).not_to be_nil
            hash = {
                    'id' => @stream.id,
                    'image' => nil,
                    'updated_at' => Time.now
                }
            result = Lesson::Stream.update_from_hash!(@author, hash)
            expect(stream.reload.image).to be_nil
        end

        it 'should update credits' do
            stream = Lesson::Stream.first
            hash = {
                    'id' => @stream.id,
                    'credits' => 'some new credits',
                    'updated_at' => Time.now
                }
            result = Lesson::Stream.update_from_hash!(@author, hash)
            expect(stream.reload.credits).to eq('some new credits')
        end

        it 'should update coming soon' do
            stream = Lesson::Stream.first
            hash = {
                    'id' => @stream.id,
                    'coming_soon' => true,
                    'updated_at' => Time.now
                }
            result = Lesson::Stream.update_from_hash!(@author, hash)
            expect(stream.reload.coming_soon).to eq(true)
        end

        it 'should update just added' do
            stream = Lesson::Stream.first
            hash = {
                    'id' => @stream.id,
                    'just_added' => true,
                    'updated_at' => Time.now
                }
            result = Lesson::Stream.update_from_hash!(@author, hash)
            expect(stream.reload.just_added).to eq(true)
        end

        it 'should update beta' do
            stream = Lesson::Stream.first
            hash = {
                    'id' => @stream.id,
                    'beta' => true,
                    'updated_at' => Time.now
                }
            result = Lesson::Stream.update_from_hash!(@author, hash)
            expect(stream.reload.beta).to eq(true)
        end

        it 'should update just updated' do
            stream = Lesson::Stream.first
            hash = {
                    'id' => @stream.id,
                    'just_updated' => true,
                    'updated_at' => Time.now
                }
            result = Lesson::Stream.update_from_hash!(@author, hash)
            expect(stream.reload.just_updated).to eq(true)
        end

        it 'should update what you will learn' do
            stream = Lesson::Stream.first
            learnings = ['learning 1', 'learning 2']
            hash = {
                    'id' => @stream.id,
                    'what_you_will_learn' => learnings,
                    'updated_at' => Time.now
                }
            result = Lesson::Stream.update_from_hash!(@author, hash)
            expect(stream.reload.what_you_will_learn).to eq(learnings)
        end

        it 'should update recommended_stream_ids' do
            stream = Lesson::Stream.first
            streams = [stream.id, stream.id]
            hash = {
                    'id' => @stream.id,
                    'recommended_stream_ids' => streams,
                    'updated_at' => Time.now
                }
            result = Lesson::Stream.update_from_hash!(@author, hash)
            expect(stream.reload.recommended_stream_ids).to eq(streams)
        end

        it 'should update related_stream_ids' do
            stream = Lesson::Stream.first
            streams = [stream.id, stream.id]
            hash = {
                    'id' => @stream.id,
                    'related_stream_ids' => streams,
                    'updated_at' => Time.now
                }
            result = Lesson::Stream.update_from_hash!(@author, hash)
            expect(stream.reload.related_stream_ids).to eq(streams)
        end

        it 'should update resource downloads' do
            stream = Lesson::Stream.first
            resources = [{
                "url" => "https://www.google.com",
                "title" => "Google's homepage"
            }]
            hash = {
                    'id' => @stream.id,
                    'resource_downloads' => resources,
                    'updated_at' => Time.now
                }
            result = Lesson::Stream.update_from_hash!(@author, hash)
            expect(stream.reload.resource_downloads).to eq(resources)
        end

        it 'should update resource links' do
            stream = Lesson::Stream.first
            resources = [{
                "url" => "https://www.google.com",
                "title" => "Google's homepage"
            }]
            hash = {
                    'id' => @stream.id,
                    'resource_links' => resources,
                    'updated_at' => Time.now
                }
            result = Lesson::Stream.update_from_hash!(@author, hash)
            expect(stream.reload.resource_links).to eq(resources)
        end

        describe "content topics" do

            before(:each) do
                stream = Lesson::Stream.first
                stream.save!

                @existing_topic = ContentTopic.create!(locales: { en: "existing topic", es: "existing spanish topic" })
                @existing_topic_ids = ContentTopic.pluck('id')
                stream.locale_pack.content_topics = [@existing_topic]

                @stream = stream
            end

            it "should save content topics" do
                locale_pack_json = @stream.locale_pack.as_json
                locale_pack_json['content_topics'] = [
                    @existing_topic.as_json,
                    {
                        'id': SecureRandom.uuid,
                        'locales': {
                            en: 'new topic'
                        }
                    }
                ]

                hash = {
                    'id' => @stream.id,
                    'updated_at' => Time.now,
                    'locale_pack' => locale_pack_json
                }

                result = Lesson::Stream.update_from_hash!(@author, hash)

                new_topic = ContentTopic.where('id not in (?)', @existing_topic_ids).first
                expect(new_topic).not_to be_nil
                expect(@stream.reload.locale_pack.content_topics).to match_array([@existing_topic, new_topic])
            end

            it "should treat nil as an empty array" do
                locale_pack_json = @stream.locale_pack.as_json
                locale_pack_json['content_topics'] = nil

                hash = {
                    'id' => @stream.id,
                    'updated_at' => Time.now,
                    'locale_pack' => locale_pack_json
                }
                result = Lesson::Stream.update_from_hash!(@author, hash)

                expect(@stream.reload.locale_pack.content_topics).to eq([])
            end

        end

        it "should set time_limit_hours" do
             hash = {
                'id' => @stream.id,
                'updated_at' => Time.now,
                'time_limit_hours' => 42
            }
            Lesson::Stream.update_from_hash!(@author, hash)
            expect(@stream.reload.time_limit_hours).to eq(42)
        end

    end

    describe 'remove_lesson' do

        it 'should remove a lesson' do
            @stream = Lesson::Stream.first
            lesson_id = @stream.chapters[0]['lesson_hashes'][0]['lesson_id']
            lesson = Lesson.find(lesson_id)
            expect(@stream.chapters[0]['lesson_hashes']).not_to be_nil # sanity check
            @stream.remove_lesson!(lesson)
            @stream = Lesson::Stream.find(@stream.id)
            expect(@stream.chapters[0]['lesson_ids']).not_to include(lesson.id)
            expect(@stream.lessons.map(&:id)).not_to include(lesson.id)
        end

    end

    describe "validate_all_playlists_including_stream" do

        it "should add error if a playlist is invalid" do
            playlist = Playlist.first
            stream = playlist.streams.first
            errors = Playlist.new.errors
            errors.add(:things, "are not ok")

            allow_any_instance_of(Playlist).to receive(:valid?).and_return(false)
            allow_any_instance_of(Playlist).to receive(:errors).and_return(errors)

            expect(Proc.new {
                stream.save!
            }).to raise_error(ActiveRecord::RecordInvalid)
        end

    end

    it "should delete stream joins when deleted" do
        stream = Lesson::Stream.first
        orig_count = Lesson::ToStreamJoin.count

        # ignore any playlists that don't want us to mess with this stream
        allow_any_instance_of(Playlist).to receive(:valid?).and_return(true)
        stream.destroy
        expect(Lesson::ToStreamJoin.count < orig_count).to be_truthy
    end

    describe "duplicate!" do
        attr_accessor :user, :stream, :stream_lessons

        before(:each) do
            @user = User.first
            @stream = lesson_streams(:en_only_item)

            # one lesson already has a translation
            # one lesson does not have a translation
            @stream_lessons = [lessons(:en_item), lessons(:en_only_item)]
            Lesson::Stream.update_from_hash!(user, {
                id: stream.id,
                updated_at: stream.updated_at,
                chapters: [{
                    title: "Chapter 1",
                    lesson_hashes: [{
                        lesson_id: stream_lessons[0].id
                    },{
                        lesson_id: stream_lessons[1].id
                    }],
                    lesson_ids: stream_lessons.map(&:id)
                }],
                lessons: stream_lessons.map(&:as_json)
            })

            stream.reload
        end

        it "should duplicate lessons when in_same_locale_pack=true" do
            # seems like this is some complicated thing about deleting empty locale packs.
            # since we can't remove the english anymore, maybe we don't need that anymore
            duplicate = Lesson::Stream.duplicate!(user, {
                title: 'Spanish',
                locale: 'es'
            }, {
                in_same_locale_pack: true,
                duplicate_from_id: stream.id,
                duplicate_from_updated_at: stream.updated_at
            })
            stream.reload
            stream_lessons[1].reload

            expect(duplicate.locale_pack).not_to be_nil
            expect(duplicate.locale_pack).to eq(stream.locale_pack)

            duplicate_lessons = Lesson.where(id: duplicate.chapters[0]['lesson_ids']).sort_by { |l| duplicate.chapters[0]['lesson_ids'].index(l.id) }
            expect(duplicate_lessons[0].id).to eq(lessons(:es_item).id)
            expect(duplicate_lessons[1].locale_pack_id).to eq(stream_lessons[1].locale_pack_id)
            expect(duplicate_lessons[1]['locale']).to eq('es')


            expect(duplicate_lessons[1].versions.last.title).to eq("Duplicated from '#{stream_lessons[1].title}'")

        end

        it "should not duplicate lessons when in_same_locale_pack=false" do
            expect(Lesson).not_to receive(:create_from_hash!)
            duplicate = Lesson::Stream.duplicate!(user, {
                title: 'Duplicate'
            }, {
                in_same_locale_pack: false,
                duplicate_from_id: stream.id,
                duplicate_from_updated_at: stream.updated_at
            })
            stream.reload
            stream_lessons[1].reload

            expect(duplicate.locale_pack).not_to eq(stream.locale_pack)
            expect(duplicate.chapters).to eq(stream.chapters)
        end

        # see https://trello.com/c/Sgj6HPHY/1005-bug-usage-report-included-in-frame-data-when-duplicating-a-lesson
        it "should not duplicate usage_reports" do
            stream = lesson_streams(:en_only_item)
            lesson = lessons(:frame_list_lesson)
            allow_any_instance_of(Lesson::Content::FrameList).to receive(:validate_frames).and_return(true)
            lesson.content_json['frames'][0]['components'] << {
                'component_type' => 'ComponentizedFrame.MultipleChoiceChallenge',
                'usage_report' => {'a' => 1}
            }
            lesson.save!
            stream.add_lesson_to_chapter!(lesson, stream.chapters[0])

            Lesson::Stream.duplicate!(user, {
                title: 'Spanish',
                locale: 'es'
            }, {
                in_same_locale_pack: true,
                duplicate_from_id: stream.id,
                duplicate_from_updated_at: stream.updated_at
            })

            lesson_copy = lesson.locale_pack.content_items.where(locale: 'es').first
            expect(lesson_copy.content_json['frames'][0]['components'].last['usage_report']).to be_nil
        end

    end

    describe "event_attributes" do
        it "should work for an english stream" do
            en_stream = Lesson::Stream.where(locale: 'en').first

            expect(Lesson::Stream.event_attributes(en_stream.locale_pack_id, 'en')).to eq({
                english_title: en_stream.title,
                title_in_users_locale: en_stream.title,
                english_url: en_stream.entity_metadata.canonical_url,
                url_in_users_locale: en_stream.entity_metadata.canonical_url,
                exam: en_stream.exam
            })
        end

        it "should work for a non-english stream" do
            es_stream = Lesson::Stream.where(locale: 'es').first
            en_stream = Lesson::Stream.where(locale: 'en', locale_pack_id: es_stream.locale_pack_id).first

            expect(Lesson::Stream.event_attributes(es_stream.locale_pack_id, 'es')).to eq({
                english_title: en_stream.title,
                title_in_users_locale: es_stream.title,
                english_url: en_stream.entity_metadata.canonical_url,
                url_in_users_locale: es_stream.entity_metadata.canonical_url,
                exam: en_stream.exam
            })
        end
    end

    describe "published_lesson_locale_pack_ids" do

        it "should work" do
            locale_pack_id = Lesson::Stream.all_published.where(locale: 'es').first.locale_pack_id
            streams = Lesson::Stream.all_published.where(locale_pack_id: locale_pack_id)
            lesson_locale_pack_ids = streams.map(&:lessons).flatten.select(&:has_published_version?).map(&:locale_pack_id).uniq
            published_lesson_locale_pack_ids = Lesson.all_published.where(locale_pack_id: lesson_locale_pack_ids).map(&:locale_pack_id).uniq
            expect(Lesson::Stream.published_lesson_locale_pack_ids(locale_pack_id)).to match_array(lesson_locale_pack_ids)

        end
    end

    describe "exam_open_times" do
        it "should return times for a user with a relevant cohort" do
            cohort = cohorts(:published_mba_cohort)
            user = cohort.accepted_users.first
            stream = lesson_streams(:exam_stream)
            period = cohort.period_for_required_stream_locale_pack_id(stream.locale_pack_id)
            expect(stream.exam_open_time(user)).to eq(cohort.start_time_for_period(period))
        end

        it "should be empty if disable_exam_locking=true" do
            cohort = cohorts(:published_mba_cohort)
            user = cohort.accepted_users.first
            user.accepted_application.update_attribute(:disable_exam_locking, true)
            stream = lesson_streams(:exam_stream)
            expect(stream.exam_open_time(user)).to be_nil
        end
    end

    describe "validate_summaries_have_lessons" do
        attr_accessor :stream

        before(:each) do
            @stream = Lesson::Stream.first
        end

        it "should be called on save" do
            expect(stream).to receive(:validate_summaries_have_lessons)
            stream.save!
        end

        describe "when stream summaries is nil" do

            before(:each) do
                allow(stream).to receive(:summaries).and_return(nil)
            end

            it "should not add any messages to errors" do
                expect(stream.errors).not_to receive(:add)
                stream.validate_summaries_have_lessons
                expect(stream.errors.messages).to eq({})
            end
        end

        describe "when stream summaries is an empty array" do

            before(:each) do
                allow(stream).to receive(:summaries).and_return([])
            end

            it "should not add any messages to errors" do
                expect(stream.errors).not_to receive(:add)
                stream.validate_summaries_have_lessons
                expect(stream.errors.messages).to eq({})
            end
        end

        describe "when stream has summaries" do

            before(:each) do
                allow(stream).to receive(:summaries).and_return([{
                    "title" => 'Foo',
                    "lessons" => ['some_lesson_id']
                }, {
                    "title" => 'Bar',
                    "lessons" => []
                }, {
                    "title" => 'Foo',
                    "lessons" => ['some_lesson_id']
                }, {
                    "title" => 'Qux',
                    "lessons" => []
                }])
            end

            it "should add message to errors for each summary with no lessons" do
                expect(stream.errors).to receive(:add).with(:summary, "'Bar' must have lessons added to it.")
                expect(stream.errors).to receive(:add).with(:summary, "'Qux' must have lessons added to it.")
                stream.validate_summaries_have_lessons
            end
        end
    end

    def valid_attrs
       stream = Lesson::Stream.first
       stream.as_json.except("id")
    end

    def invalid_attrs
        valid_attrs.merge('title' => nil)
    end
end
