require 'spec_helper'

describe Lesson::Stream::JsonCache do

    fixtures(:lesson_streams)


    before(:each) do
        SafeCache.clear
    end

    describe "get_attrs_for_locale_pack_ids" do

        it "should work" do
            stream = Lesson::Stream.all_published.first.published_version

            result_version_ids = Lesson::Stream::JsonCache.get_attrs_for_locale_pack_ids([stream.locale_pack_id]).map do |attrs|
                attrs['version_id']
            end
            expect(result_version_ids).to include(stream.version_id)
        end

    end

    describe "get_jsons_for_locale_pack_ids" do

        it "should work" do
            stream = Lesson::Stream.all_published.first.published_version

            result_version_ids = Lesson::Stream::JsonCache.get_jsons_for_locale_pack_ids([stream.locale_pack_id]).map do |json|
                ActiveSupport::JSON.decode(json)['version_id']
            end
            expect(result_version_ids).to include(stream.version_id)
        end

    end

    describe "get_stream_jsons_for_version_ids" do
        it "should work when nothing is cached" do
            stream = Lesson::Stream.all_published.first.published_version
            result = get_and_decode([stream.version_id])
            expect(result.size).to eq(1)
            expect(result[0]['version_id']).to eq(stream.version_id)
            expect(result[0]['title']).to eq(stream.title)
        end

        it "should grab things from the cache that it can and load things that it cannot" do
            streams = Lesson::Stream.all_published.limit(2).map(&:published_version).to_a
            # get one of them into the cache
            result = get_and_decode([streams[0].version_id])

            # we have to do the at_least(1).times because ToJsonFromApiParams calls itself internally
            expect(Lesson::Stream::ToJsonFromApiParams).to receive(:new).at_least(1).times.and_call_original
            # make sure that only one was loaded up from the db
            expect(SafeCache).to receive(:write).exactly(1).times

            result = get_and_decode(streams.map(&:version_id))

            expect(result.size).to eq(2)
            expect(result.map { |s| s['version_id'] } ).to match_array(streams.map(&:version_id))
            expect(result.map { |s| s['title'] } ).to match_array(streams.map(&:title))
        end

        it "should make no request if everthing is cached" do
            streams = Lesson::Stream.all_published.limit(2).map(&:published_version)
            # get it cached
            result = get_and_decode(streams.map(&:version_id))

            expect(Lesson::Stream::ToJsonFromApiParams).not_to receive(:new)
            expect(SafeCache).not_to receive(:write)
            result = get_and_decode(streams.map(&:version_id))

            expect(result.size).to eq(2)
            expect(result.map { |s| s['version_id'] } ).to match_array(streams.map(&:version_id))
            expect(result.map { |s| s['title'] } ).to match_array(streams.map(&:title))
        end

        it "should bust the cache when something is published" do
            stream = Lesson::Stream.all_published.first.published_version
            # get it cached
            get_and_decode([stream.version_id])
            # reset content_views_refresh_updated_at
            RefreshMaterializedContentViews.refresh(true)

            # we have to do the at_least(1).times because ToJsonFromApiParams calls itself internally
            expect(Lesson::Stream::ToJsonFromApiParams).to receive(:new).at_least(1).times.and_call_original
            get_and_decode([stream.version_id])
        end

        def get_and_decode(version_ids)
            result = Lesson::Stream::JsonCache.get_stream_jsons_for_version_ids(version_ids)
            result.map do |r|
                ActiveSupport::JSON.decode(r);
            end
        end
    end

end