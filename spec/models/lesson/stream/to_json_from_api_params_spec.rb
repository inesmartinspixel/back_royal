require 'spec_helper'

describe Lesson::Stream::ToJsonFromApiParams do

    fixtures(:lesson_streams, :users, :lesson_streams_progress, :cohorts, :playlists, :institutions)


    before(:each) do
        @user = User.first
    end

    it "should have expected keys" do

        stream = lesson_streams(:stream_with_many_versions)
        result = get_single_stream_json(stream, {})
        expect(result.keys).to match_array([
            "id", "version_id", "tag", "title", "description",
            "modified_at", "updated_at", "author", "image", "was_published",
            "lessons", "entity_metadata", "chapters",
            "credits", "what_you_will_learn",
            "recommended_stream_ids", "related_stream_ids",
            "resource_downloads", "resource_links", "summaries",
            "coming_soon", "just_added", "beta", "just_updated",
            "locale", "locale_pack",
            "published_at", "published_version_id",
            "unlocked_for_mba_users", "exam", "time_limit_hours"
        ])
    end

    it "should respect fields" do
        stream = lesson_streams(:stream_with_many_versions)
        result = get_single_stream_json(stream, {fields: ['id']})
        expect(result.keys).to match_array([
            "id"
        ])
    end

    it "should respect lesson_fields" do
        stream = lesson_streams(:stream_with_frame_list_lesson)
        result = get_single_stream_json(stream, least_restrictive_params.merge({lesson_fields: ['id']}))
        expect(result['lessons'][0].keys).to match_array([
            "id"
        ])
    end

    it "should respect except" do
        stream = lesson_streams(:stream_with_many_versions)
        result = get_single_stream_json(stream, {except: ['title']})
        expect(result.keys.include?('title')).to be(false)
    end

    describe "keys with simple values" do

        it "should have expected values" do
            stream = Lesson::Stream.where('chapters is not null').where(exam: false).first

            result = get_single_stream_json(stream, least_restrictive_params)
            {
                "id" => stream.id,
                "version_id" => stream.versions.last.version_id,
                "title" => stream.title,
                "description" => stream.description,
                "modified_at" => stream.modified_at.to_timestamp,
                "updated_at" => stream.updated_at.to_timestamp,
                "chapters" => stream.chapters,
                "credits" => stream.credits,
                "what_you_will_learn" => stream.what_you_will_learn,
                "recommended_stream_ids" => stream.recommended_stream_ids,
                "related_stream_ids" => stream.related_stream_ids,
                "coming_soon" => stream.coming_soon,
                "just_added" => stream.just_added,
                "beta" => stream.beta,
                "just_updated" => stream.just_updated,
                "exam" => stream.exam
            }.each do |key, value|
                expect(value).not_to be_nil, "Expecting all simple values to be set. #{key.inspect} is not"
                expect(result[key]).to eq(value), "Unexpected value for #{key.inspect}. expected #{value.inspect} got #{result[key].inspect}"
            end
        end
    end

    describe "exam stuff" do
        attr_reader :user, :cohort, :period, :stream, :period_details, :fields, :orig_min_allowed_ios_version
        before(:each) do
            @user = User.first
            @cohort = cohorts(:published_mba_cohort)
            @period = cohort.periods.detect { |p| p['style'] == 'exam' }
            expect(period).not_to be_nil
            period_index = cohort.periods.index(period) + 1
            required_stream_pack_ids = period['stream_entries'].map { |entry| entry['required'] && entry['locale_pack_id'] }.compact
            @stream = Lesson::Stream.all_published.where(locale_pack_id: required_stream_pack_ids).first
            expect(stream).not_to be_nil
            expect(stream.exam).to be(true)

            @fields = [:id, :exam, :time_limit_hours]

            orig_min_allowed_ios_version = ENV['MIN_ALLOWED_IOS_VERSION']
        end

        after(:each) do
            ENV['MIN_ALLOWED_IOS_VERSION'] = orig_min_allowed_ios_version
        end

        it "should show special exam keys" do
            allow(@user).to receive(:relevant_cohort).and_return(@cohort)

            exam_times = stream.get_exam_timestamps(@user)
            result = get_single_stream_json(stream, least_restrictive_params.merge(user: user, fields: fields))
            {
                "exam" => stream.exam,
                "time_limit_hours" => stream.time_limit_hours
            }.each do |key, value|
                expect(value).not_to be_nil, "Expecting all simple values to be set. #{key.inspect} is not"
                expect(result[key]).to eq(value), "Unexpected value for #{key.inspect}. expected #{value.inspect} got #{result[key].inspect}"
            end
        end

        it "should not have time_limit_hours if no relevant cohort" do
            allow(@user).to receive(:relevant_cohort).and_return(nil)
            result = get_single_stream_json(stream, least_restrictive_params.merge(user: user, fields: fields))
            {
                "exam" => stream.exam,
                "time_limit_hours" => stream.time_limit_hours
            }.each do |key, value|
                expect(result[key]).to eq(value), "Unexpected value for #{key.inspect}. expected #{value.inspect} got #{result[key].inspect}"
            end
        end

        it "should not show time_limit_hours if not an exam" do
            allow(@user).to receive(:relevant_cohort).and_return(@cohort)
            stream.update_attribute(:exam, false)
            result = get_single_stream_json(stream, least_restrictive_params.merge(user: user, fields: fields))
            {
                "exam" => stream.exam,
                "time_limit_hours" => stream.time_limit_hours
            }.each do |key, value|
                expect(result[key]).to eq(value), "Unexpected value for #{key.inspect}. expected #{value.inspect} got #{result[key].inspect}"
            end
        end
    end

    describe "author" do

        it "should have expected value" do
            stream = Lesson::Stream.first
            result = get_single_stream_json(stream, least_restrictive_params)
            expect(result['author']).to eq(stream.author.as_json(for: :public))
        end

    end

    describe "content topics" do

        it "should have expected values" do
            stream = Lesson::Stream.first
            expect(ContentTopic.count).to be > 1
            stream.locale_pack.content_topics = [ContentTopic.first]
            stream.save!
            result = get_single_stream_json(stream, least_restrictive_params)
            expect(result).not_to be_nil
            expect(result['locale_pack']['content_topics']).to eq(stream.locale_pack.content_topics.map(&:as_json))
        end

        it "should be an empty array if there are none" do
            stream = Lesson::Stream.first
            stream.locale_pack.content_topics = []
            stream.save!
            result = get_single_stream_json(stream, least_restrictive_params)
            expect(result).not_to be_nil
            expect(result['locale_pack']['content_topics']).to eq([])
        end
    end

    describe "favorite" do

        before(:each) do
            @user = User.joins(:favorite_lesson_stream_locale_packs).first
            @stream = @user.favorite_lesson_stream_locale_packs[0].content_items[0]
            expect(@stream).not_to be_nil
        end

        it "should be true for favorite stream" do
            result = get_single_stream_json(@stream, least_restrictive_params(user: @user))
            expect(result).not_to be_nil
            expect(result['favorite']).to be(true)
        end

        it "should be false for not favorite stream" do
            @user.favorite_lesson_stream_locale_packs = []
            @user.save!
            result = get_single_stream_json(@stream, least_restrictive_params(user: @user))
            expect(result).not_to be_nil
            expect(result['favorite']).to be(false)
        end

    end

    describe "image" do
        it "should have expected value" do
            stream = Lesson::Stream.where('image_id is not null').first
            result = get_single_stream_json(stream, least_restrictive_params)
            expect(result).not_to be_nil
            expect(result['image']).to eq(image_json(stream.image))
        end
        it "should be nil if there is no image" do
            stream = Lesson::Stream.first
            stream.image = nil
            stream.save!
            result = get_single_stream_json(stream, least_restrictive_params)
            expect(result).not_to be_nil
            expect(result['image']).to be_nil
        end
    end

    describe "lessons" do

        it "should include all the expected values in the lesson json" do
            stream = lesson_streams(:stream_with_frame_list_lesson)
            another_stream = Lesson::Stream.where('id != ?', stream.id).first
            lesson = Lesson.find(stream.chapters[0]['lesson_ids'][0])

            # make sure this lesson is in two streams so we can
            # test stream_titles
            unless another_stream.lessons.include?(lesson)
                another_stream.add_lesson_to_chapter!(lesson, another_stream.chapters[0])
            end

            result = get_single_stream_json(stream, least_restrictive_params)
            expect(result).not_to be_nil
            expect(result['lessons']).not_to be_nil
            lesson_json = result['lessons'].detect { |lj| lj['id'] == lesson.id }

            expect(lesson_json.keys).to match_array(%w|
                id title description entity_metadata frame_count
                client_requirements lesson_type
                key_terms tag assessment version_id unrestricted
                locale locale_pack updated_at test
            |)

            {
                "id" => lesson.id,
                "lesson_type" => lesson.content.lesson_type,
                "title" => lesson.title,
                "description" => lesson.description,
                "entity_metadata" => lesson.entity_metadata.attributes.slice("id", "title", "description", "canonical_url", "tweet_template").merge("image" => image_json(lesson.entity_metadata.image)),
                "frame_count" => lesson.content.frames.size,
                "key_terms" => lesson.key_terms,
                "tag" => lesson.tag,
                "client_requirements" => {
                    "min_allowed_version" => 0,
                    "supported_in_latest_available_version" => true
                }

            }.each do |key, value|
                expect(value).not_to be_nil, "Expecting all simple values to be set. #{key.inspect} is not"
                expect(lesson_json[key]).to eq(value), "Unexpected value for #{key.inspect}. expected #{value.inspect} got #{result[key].inspect}"
            end

        end

        it "should add published_at if requesting published lessons" do
            stream = lesson_streams(:stream_with_frame_list_lesson)
            result = get_single_stream_json(stream, least_restrictive_params.merge(filters: {published: true}))
            expect(result).not_to be_nil
            expect(result['lessons']).not_to be_nil
            lesson_json = result['lessons'].first

            expect(lesson_json.keys).to include('published_at')

        end

        module HasTruePublishedFilter
            class HasTruePublishedFilter
                def initialize
                end

                def ==(actual_options)
                    actual_options[:filters][:published] == true
                end

                def inspect
                    "options with filters[:published] equal to true"
                end
            end

            # Matches arguments that contain the same elements as the given Array
            def has_true_published_filter
                HasTruePublishedFilter.new
            end
        end

        RSpec.configure do |config|
            config.include HasTruePublishedFilter
        end

        it "should default to loading published lessons" do
            # see https://trello.com/c/BrFMs57E/246-bug-unpublished-but-saved-change-to-a-lesson-is-showing-up-in-the-player
            stream = lesson_streams(:stream_with_frame_list_lesson)
            expect(Lesson::ToJsonFromApiParams).to receive(:new).with(has_true_published_filter).at_least(1).times.and_call_original
            result = get_single_stream_json(stream, {})
        end

        it "should be an empty array if there are no lessons" do
            stream = Lesson::Stream.first
            stream.lessons = []
            stream.chapters = []
            stream.save!
            result = get_single_stream_json(stream, least_restrictive_params)
            expect(result).not_to be_nil
            expect(result['lessons']).to eq([])
        end

        describe('with published param') do

            before(:each) do
                @stream = lesson_streams(:published_stream)
                @published_lesson_titles = @stream.lessons.select(&:has_published_version?).map do |lesson|
                    expect(lesson.title).not_to eq(lesson.published_version.title)
                    lesson.published_version.title
                end

                @working_lesson_titles = @stream.lessons.map(&:title)

                # make sure there is at least one unpublished lesson,
                # so we know it has been filtered out
                expect(@published_lesson_titles.size).not_to be(@working_lesson_titles.size)
            end

            it "should include published versions of lessons when published=true" do
                result = get_single_stream_json(@stream, least_restrictive_params(:filters => {:published => true}))
                expect(result).not_to be_nil
                expect(result['lessons']).not_to be_nil
                expect(result['lessons'].map { |l| l['title'] }).to match_array(@published_lesson_titles)
            end

            it "should include working versions of lessons when published=false" do
                result = get_single_stream_json(@stream, least_restrictive_params(:filters => {:published => false}))
                expect(result).not_to be_nil
                expect(result['lessons']).not_to be_nil
                expect(result['lessons'].map { |l| l['title'] }).to match_array(@working_lesson_titles)
            end
        end

        it "should include lesson_progress when include_progress=true" do
            stream_progress = lesson_streams_progress('completed_stream_progress')
            @user = stream_progress.user
            stream = stream_progress.lesson_stream
            lesson = stream.lessons.detect(&:has_published_version?)
            result = get_single_stream_json(stream, least_restrictive_params({
                include_progress: true,
                user: @user,
                filters: {published: true}
            }))

            expect(result).not_to be_nil

            expect(result['lessons']).not_to be_nil
            lesson_entry = result['lessons'].detect { |e| e['id'] == lesson.id }
            expect(lesson_entry).not_to be_nil

            # sanity check: check that there is progress for this lesson
            lesson_progress = LessonProgress.where({
                locale_pack_id: lesson_entry['locale_pack']['id'],
                user: @user
            }).first
            expect(lesson_progress).not_to be_nil

            expect(lesson_entry['lesson_progress']).not_to be_nil
            expect(lesson_entry['lesson_progress']['locale_pack_id']).to eq(lesson_progress.locale_pack_id)
        end

        it "should have lesson_progress=nil when include_progress=true but there is none" do
            stream = Lesson::Stream.first
            LessonProgress.delete_all

            result = get_single_stream_json(stream, least_restrictive_params({
                include_progress: true,
                user: @user
            }))
            expect(result).not_to be_nil
            expect(result['lessons']).not_to be_nil
            expect(result['lessons'][0]).not_to be_nil
            expect(result['lessons'][0]['lesson_progress']).to be_nil
        end

        describe "with load_full_content_for_lesson_id" do

            before(:each) do
                @stream = lesson_streams(:stream_with_frame_list_lesson)
                @lesson = Lesson.find(@stream.chapters[0]['lesson_ids'][0])
                @stub_ability = {}
                @expectation = expect(@stub_ability).to receive(:can?) do |meth, obj|
                    obj[:stream]['id'] == @stream.id && obj[:lesson]['id'] = @lesson.id
                end
            end

            it "should work when user has access" do
                @expectation.and_return(true)
                assert_load_full_content_works(user: @user)
            end

            it "should raise when user has no access" do
                @expectation.and_return(false)
                assert_load_full_content_raises(user: @user)
            end

            it "should work when ability has access" do
                @expectation.and_return(true)
                assert_load_full_content_works
            end

            it "should raise when ability has no access" do
                @expectation.and_return(false)
                assert_load_full_content_raises
            end

            def assert_load_full_content_works(params = {})
                stream = @stream
                lesson = @lesson
                expect(lesson.frames).not_to be_nil
                expect(Lesson::Content::FrameList).to receive(:decorate_frame_json) do |id, frames|
                    expect(id).to eq(lesson.id)
                    frames[0]['decorated'] = true
                end
                result = get_single_stream_json(stream, least_restrictive_params({
                    load_full_content_for_lesson_id: lesson.id
                }.merge(params)), @stub_ability)

                expect(result).not_to be_nil
                expect(result['lessons']).not_to be_nil
                lesson_entry = result['lessons'].detect { |l| l['id'] == lesson.id }
                expect(lesson_entry).not_to be_nil

                expected_frames = lesson.content_json['frames']
                expected_frames[0]['decorated'] = true
                expect(lesson_entry['frames']).to eq(expected_frames)
            end

            def assert_load_full_content_raises(params = {})
                stream = lesson_streams(:stream_with_frame_list_lesson)
                lesson = Lesson.find(stream.chapters[0]['lesson_ids'][0])
                expect(Lesson::Content::FrameList).not_to receive(:decorate_frame_json)

                expect(Proc.new{
                    get_single_stream_json(stream, least_restrictive_params({
                        user: @user,
                        load_full_content_for_lesson_id: lesson.id
                    }), @stub_ability)
                }).to raise_error(Lesson::Stream::ToJsonFromApiParams::UnauthorizedError, "Cannot load full content for lessons in stream #{stream.id}")

            end



        end

        describe "with load_full_content_for_all_lessons" do

            before(:each) do
                @stream = lesson_streams(:stream_with_frame_list_lesson)
                @lesson = Lesson.find(@stream.chapters[0]['lesson_ids'][0])
                @stub_ability = {}
                @expectation = expect(@stub_ability).to receive(:can?).at_least(1).times do |meth, obj|
                    obj[:stream]['id'] == @stream.id && obj[:lesson]['id'] = @lesson.id
                end
            end

            it "should work when ability has access" do
                @expectation.and_return(true)
                assert_load_full_content_works
            end

            it "should raise when ability has no access" do
                @expectation.and_return(false)
                assert_load_full_content_raises
            end

            def assert_load_full_content_works(params = {})
                stream = @stream
                expect(Lesson::Content::FrameList).to receive(:decorate_frame_json).exactly(stream.lessons.size).times do |id, frames|
                    frames[0]['decorated'] = true
                end
                result = get_single_stream_json(stream, least_restrictive_params({
                    load_full_content_for_all_lessons: true
                }.merge(params)), @stub_ability)

                expect(result).not_to be_nil
                expect(result['lessons']).not_to be_nil
                result['lessons'].each do |lesson_entry|
                    expect(lesson_entry['frames'][0]['decorated']).to eq(true)
                end
            end

            def assert_load_full_content_raises(params = {})
                stream = lesson_streams(:stream_with_frame_list_lesson)
                lesson = Lesson.find(stream.chapters[0]['lesson_ids'][0])
                expect(Lesson::Content::FrameList).not_to receive(:decorate_frame_json)

                expect(Proc.new{
                    get_single_stream_json(stream, least_restrictive_params({
                        user: @user,
                        load_full_content_for_all_lessons: true
                    }), @stub_ability)
                }).to raise_error(Lesson::Stream::ToJsonFromApiParams::UnauthorizedError, "Cannot load full content for lessons in stream #{stream.id}")

            end



        end

        describe "with load_full_content_for_stream_ids" do

            before(:each) do
                @stream = lesson_streams(:stream_with_frame_list_lesson)
                @lesson = Lesson.find(@stream.chapters[0]['lesson_ids'][0])
                @stub_ability = {}

            end

            it "should raise when ability has no access" do
                expect_ability_check(false)
                assert_load_full_content_raises
            end

            it "should return full content for a stream in the list" do
                expect_ability_check(true)
                result = load_with_full_content_requested(
                    stream_to_load: @stream,
                    load_full_content_for_stream_ids: [@stream.id]
                )
                assert_frames_loaded_and_decorated(result)
            end

            it "should not return full content for a stream not in the list" do
                result = load_with_full_content_requested(
                    stream_to_load: @stream,
                    load_full_content_for_stream_ids: [SecureRandom.uuid]
                )
                assert_frames_not_loaded(result)
            end

            it "should not error when the provided list is empty" do
                result = load_with_full_content_requested(
                    stream_to_load: @stream,
                    load_full_content_for_stream_ids: []
                )
                assert_frames_not_loaded(result)
            end

            def expect_ability_check(result)
                can_expectation = expect(@stub_ability).to receive(:can?).at_least(1).times do |meth, obj|
                    obj[:stream]['id'] == @stream.id && obj[:lesson]['id'] = @lesson.id
                end
                can_expectation.and_return(result)
            end

            def assert_frames_loaded_and_decorated(result)
                result['lessons'].each do |lesson_entry|
                    expect(lesson_entry['frames'][0]['decorated']).to eq(true)
                end
            end

            def assert_frames_not_loaded(result)
                result['lessons'].each do |lesson_entry|
                    expect(lesson_entry['frames']).to be_nil
                end
            end

            def load_with_full_content_requested(stream_to_load:, load_full_content_for_stream_ids:)
                stream = stream_to_load
                allow(Lesson::Content::FrameList).to receive(:decorate_frame_json) do |id, frames|
                    frames[0]['decorated'] = true
                end
                result = get_single_stream_json(stream, least_restrictive_params({
                    load_full_content_for_stream_ids: load_full_content_for_stream_ids
                }), @stub_ability)

                expect(result).not_to be_nil
                expect(result['lessons']).not_to be_nil
                result
            end

            def assert_load_full_content_raises(params = {})
                stream = lesson_streams(:stream_with_frame_list_lesson)
                lesson = Lesson.find(stream.chapters[0]['lesson_ids'][0])
                expect(Lesson::Content::FrameList).not_to receive(:decorate_frame_json)

                expect(Proc.new{
                    get_single_stream_json(stream, least_restrictive_params({
                        user: @user,
                        load_full_content_for_stream_ids: [stream.id]
                    }), @stub_ability)
                }).to raise_error(Lesson::Stream::ToJsonFromApiParams::UnauthorizedError, "Cannot load full content for lessons in stream #{stream.id}")

            end



        end




    end


    describe "with published param" do

        it "should return working versions if published=false" do
            stream = lesson_streams(:stream_with_many_versions)
            result = get_single_stream_json(stream, least_restrictive_params({filters: {published: false}}))
            expect(result).not_to be_nil
            expect(result['version_id']).not_to eq(stream.published_version.version_id)
            expect(result['title']).to eq(stream.title) # in the fixture, the title changes with each version
        end

        it "should return published versions if published=true" do
            stream = lesson_streams(:stream_with_many_versions)
            result = get_single_stream_json(stream, least_restrictive_params({filters: {published: true}}))
            expect(result).not_to be_nil
            expect(result['version_id']).to eq(stream.published_version.version_id)
            expect(result['title']).to eq(stream.published_version.title) # in the fixture, the title changes with each version
        end
    end

    describe "locale_pack" do

        it "should include the locale_pack" do
            stream = lesson_streams(:en_item)
            streams_in_pack = [lesson_streams(:en_item), lesson_streams(:es_item), lesson_streams(:zh_item)]
            expect(stream.locale_pack.access_groups).not_to be_empty # sanity check

            json = get_json({fields: ['id', 'locale_pack'], filters: {id: stream.id, published: false}})[0]
            expect(json['locale_pack']).not_to be_nil
            expect(json['locale_pack']['id']).to eq(stream.locale_pack_id)
            expect(json['locale_pack']['groups']).not_to be_nil
            expect(json['locale_pack']['groups'].map { |g| g['name'] }).to match_array(stream.locale_pack.access_groups.map(&:name))
            expect(json['locale_pack']['content_items']).to match_array(streams_in_pack.map { |stream|
                {
                    'id' => stream.id,
                    'title' => stream.title,
                    'locale' => stream.locale
                }
            })
        end

        it "should respect the published filter" do
            stream = lesson_streams(:en_item)
            lesson_streams(:es_item).unpublish!
            streams_in_pack = [lesson_streams(:en_item), lesson_streams(:zh_item)]
            streams_in_pack.map(&:publish!)

            json = get_json({fields: ['id', 'locale_pack'], filters: {id: stream.id, published: true}})[0]
            expect(json['locale_pack']).not_to be_nil
            expect(json['locale_pack']['id']).to eq(stream.locale_pack_id)
            expect(json['locale_pack']['content_items']).to match_array(streams_in_pack.map { |stream|
                {
                    'id' => stream.id,
                    'title' => stream.title,
                    'locale' => stream.locale
                }
            })
        end

    end


    describe "with publishing info" do

        it "should have correct json when loading working version of a published stream" do
            stream = lesson_streams(:stream_with_many_versions)

            result = get_single_stream_json(stream, {filters: {published: false}})
            expect(result).not_to be_nil
            expect(result['was_published']).to be(false) # this refers to whether this version was published
            expect(result['published_at']).to eq(stream.content_publisher.published_at.to_timestamp) # this refers the the published_at date of the currently published version
            expect(result['published_version_id']).to eq(stream.content_publisher.lesson_stream_version_id)

        end

        it "should have the correct json when loading published version" do
            stream = lesson_streams(:stream_with_many_versions)
            result = get_single_stream_json(stream, {filters: {published: true}})
            expect(result).not_to be_nil
            expect(result['was_published']).to be(true) # this refers to whether this version was published
            expect(result['published_at']).to eq(stream.content_publisher.published_at.to_timestamp) # this refers the the published_at date of the currently published version
            expect(result['published_version_id']).to eq(stream.content_publisher.lesson_stream_version_id)

        end

        it "should have correct json when loading working version of an unpublished stream" do
            stream = Lesson::Stream.where.not(id: Lesson::Stream.all_published.ids).first
            result = get_single_stream_json(stream, {filters: {published: false}})
            expect(result).not_to be_nil
            expect(result['was_published']).to be(false) # this refers to whether this version was published
            expect(result['published_at']).to be_nil # this refers the the published_at date of the currently published version
            expect(result['published_version_id']).to be_nil
        end

    end

    describe "lesson_streams_progress" do

        it "should include progress when include_progress=true" do
            stream_progress = lesson_streams_progress('completed_stream_progress')
            expect(stream_progress.time_runs_out_at).to be_nil
            @user = stream_progress.user
            stream = Lesson::Stream.where(locale_pack_id: stream_progress.locale_pack_id).first
            result = get_single_stream_json(stream, least_restrictive_params({
                include_progress: true,
                user: @user,
                fields: ['id', 'lesson_streams_progress']
            }))
            expect(result).not_to be_nil

            expect(result['lesson_streams_progress']).not_to be_nil
            expect(result['lesson_streams_progress']).to eq({
                "user_id" => stream_progress.user_id,
                "updated_at" => stream_progress.updated_at.to_f.round,
                "created_at" => stream_progress.created_at.to_timestamp,
                "locale_pack_id" => stream_progress.locale_pack_id,
                "lesson_bookmark_id" => stream_progress.lesson_bookmark_id,
                "started_at" => stream_progress.started_at.to_timestamp,
                "completed_at" => stream_progress.completed_at.to_timestamp,
                "complete" => true,
                "certificate_image" => image_json(stream_progress.certificate_image),
                "last_progress_at" => stream_progress.last_progress_at.to_timestamp,
                "id" => stream_progress.id,
                "time_runs_out_at" => nil,
                "official_test_score" => nil,
                "waiver" => nil
            })
        end

        it "should include progress when include_progress=true and not including fields param" do
            stream_progress = lesson_streams_progress('completed_stream_progress')
            expect(stream_progress.time_runs_out_at).to be_nil
            @user = stream_progress.user
            stream = Lesson::Stream.where(locale_pack_id: stream_progress.locale_pack_id).first
            result = get_single_stream_json(stream, least_restrictive_params({
                include_progress: true,
                user: @user,
                filters: {
                    locale_pack_id: [stream_progress.locale_pack_id]
                }
            }))
            expect(result).not_to be_nil

            expect(result['lesson_streams_progress']).not_to be_nil
            expect(result['lesson_streams_progress']).to eq({
                "locale_pack_id" => stream_progress.locale_pack_id,
                "lesson_bookmark_id" => stream_progress.lesson_bookmark_id,
                "updated_at" => stream_progress.updated_at.to_timestamp,
                "created_at" => stream_progress.created_at.to_timestamp,
                "started_at" => stream_progress.started_at.to_timestamp,
                "completed_at" => stream_progress.completed_at.to_timestamp,
                "complete" => true,
                "certificate_image" => image_json(stream_progress.certificate_image),
                "last_progress_at" => stream_progress.last_progress_at.to_timestamp,
                "id" => stream_progress.id,
                "time_runs_out_at" => nil,
                "official_test_score" => nil,
                "waiver" => nil,
                "user_id" => stream_progress.user_id
            })
        end

        it "should handle time_runs_out_at" do
            stream_progress = lesson_streams_progress('completed_stream_progress')
            stream_progress.update_attribute('time_runs_out_at', Time.now)
            @user = stream_progress.user
            stream = Lesson::Stream.where(locale_pack_id: stream_progress.locale_pack_id).first
            result = get_single_stream_json(stream, least_restrictive_params({
                include_progress: true,
                user: @user,
                fields: ['id', 'lesson_streams_progress']
            }))
            expect(result['lesson_streams_progress']).not_to be_nil
            expect(result['lesson_streams_progress']['time_runs_out_at']).to eq(stream_progress.time_runs_out_at.to_timestamp)
        end

        it "should not load up progress for the wrong user" do
            stream = Lesson::StreamProgress.first.lesson_stream
            stream_progresses = Lesson::StreamProgress.where(locale_pack_id: stream.locale_pack_id)
            user = User.where("id not in (#{stream_progresses.select(:user_id).to_sql})").first
            result = get_single_stream_json(stream, least_restrictive_params({
                include_progress: true,
                user: user
            }))
            expect(result).not_to be_nil
            expect(result['lesson_streams_progress']).to be_nil
        end

        it "should raise as UnauthorizedError if no user is provided" do
            stream = Lesson::StreamProgress.first.lesson_stream
            expect {
                result = get_single_stream_json(stream, least_restrictive_params({
                    include_progress: true,
                    user: nil
                }))
            }.to raise_error(Lesson::Stream::ToJsonFromApiParams::UnauthorizedError, "user must be provided in order to include progress")
        end

    end

    describe "with id param" do

        it "should load up a single stream" do
            stream = Lesson::Stream.first
            json = Lesson::Stream::ToJsonFromApiParams.new(least_restrictive_params(id: stream.id)).json
            results = ActiveSupport::JSON.decode(json)
            expect(results.size).to be(1)
            expect(results[0]['id']).to eq(stream.id)
        end

        it "should work if id is passed in filters hash" do
            stream = Lesson::Stream.first
            json = Lesson::Stream::ToJsonFromApiParams.new(least_restrictive_params(filters:
                {id: stream.id}
            )).json
            results = ActiveSupport::JSON.decode(json)
            expect(results.size).to be(1)
            expect(results[0]['id']).to eq(stream.id)
        end

        it "should load up multiple ids if array is passed in" do
            stream_ids = Lesson::Stream.limit(2).pluck('id')
            json = Lesson::Stream::ToJsonFromApiParams.new(least_restrictive_params(filters:
                {id: stream_ids}
            )).json
            results = ActiveSupport::JSON.decode(json)
            expect(results.size).to be(2)
            expect(results.map { |e| e['id'] }).to match_array(stream_ids)
        end

    end

    describe "filters" do

        it "should filter by favorite=true" do
            @user = User.joins(:favorite_lesson_stream_locale_packs).first
            favorite_stream_ids = @user.favorite_lesson_stream_locale_packs.map(&:content_items).flatten.select {|item| item.locale == 'en'}.map(&:id)
            expect(favorite_stream_ids).not_to be_empty
            results = get_json(least_restrictive_params({
                :user => @user,
                :filters => {
                    :favorite => true,
                    :locale => 'en'
                }
            }))
            expect(results.map { |stream| stream['id'] }).to match_array(favorite_stream_ids)
        end

        it "should raise as ArgumentError if filtering by favorite=true and no user" do
            expect {
                get_json(least_restrictive_params({
                    user: nil,
                    filters: { favorite: true }
                }))
            }.to raise_error(ArgumentError, "user must be provided in order to filter by 'favorite'")
        end


        it "should filter by locale_pack_id" do
            locale_pack_id = lesson_streams(:en_item).locale_pack_id
            results = get_json(least_restrictive_params({
                :filters => {:locale_pack_id => locale_pack_id}
            }))
            expect(results.map { |stream| stream['id'] }).to match_array([
                lesson_streams(:en_item).id,
                lesson_streams(:es_item).id,
                lesson_streams(:zh_item).id,
            ])
        end

        it "should filter by version_id" do
            stream = Lesson::Stream.all_published.first.published_version
            version_id = stream.version_id
            results = get_json(least_restrictive_params({
                :filters => {:version_id => version_id}
            }))
            expect(results.map { |s| s['id'] }).to match_array([
                stream['id']
            ])
        end

        it "should filter by locale_pack_id with a nil value" do
            locale_pack_id = lesson_streams(:en_item).locale_pack_id
            results = get_json(least_restrictive_params({
                :filters => {:locale_pack_id => [locale_pack_id, nil]}
            }))

            items_with_locale_pack = [
                lesson_streams(:en_item).id,
                lesson_streams(:es_item).id,
                lesson_streams(:zh_item).id,
            ]

            items_with_nil_locale_pack = Lesson::Stream.where(locale_pack_id: nil).ids
            expect(items_with_nil_locale_pack.any?).to be(true)

            expected_ids = (items_with_locale_pack + items_with_nil_locale_pack)
            expect(results.map { |stream| stream['id'] }).to match_array(expected_ids)
        end

        it "should filter by locale" do
            results = get_json(least_restrictive_params({
                :filters => {
                    :locale => 'en'
                }
            }))
            expect(results.map { |stream| stream['locale'] }.uniq).to match_array(['en'])
        end

        it "should filter by updated at" do
            before = Time.now + 1.second
            stream = Lesson::Stream.first
            stream.update_attribute(:updated_at, Time.now + 10.seconds)
            results = get_json(least_restrictive_params({
                :filters => {
                    :updated_since => before.to_timestamp
                }
            }))
            expect(results.map { |stream| stream['id'] }).to eq([stream.id])
        end

        it "should filter by exam=true" do
            results = get_json(least_restrictive_params({
                :filters => {
                    :exam => true
                }
            }))
            expect(results.map { |stream| stream['id'] }).to include(lesson_streams(:exam_stream).id)
        end

        it "should raise if exam=true" do
            expect {
                results = get_json(least_restrictive_params({
                    :filters => {
                        :exam => false
                    }
                }))
            }.to raise_error(ArgumentError, 'true is the only value supported for filters[:exam]')
        end

        # FIXME: See if we can do these without having to force RefreshMaterializedContentViews
        describe "user_can_see" do

            attr_accessor :stream, :cohort

            before(:each) do
                @stream = lesson_streams(:en_item)
                @user = get_user_with_nothing
                @cohort = cohorts(:published_mba_cohort_with_no_courses)
            end

            it "should show streams in user's groups" do
                # Ensure a stream has a group
                group = AccessGroup.create!({
                    name: 'doesnotexistyet'
                })
                stream.locale_pack.add_to_group(group.name)

                @user.groups = [group]
                @user.save!

                RefreshMaterializedContentViews.refresh_and_rebuild_derived_content_tables
                expect_streams([stream.id])
            end

            it "should show streams in institution's groups" do
                # Ensure a stream has a group
                group = AccessGroup.create!({
                    name: 'doesnotexistyet'
                })
                stream.locale_pack.add_to_group(group.name)

                # Ensure an institution has only that group
                institution = Institution.first
                institution = Institution.update_from_hash!({id: institution.id, updated_at: institution.updated_at, "playlist_pack_ids": [], "groups" => [{"id" => group.id}]})

                # Add the institution to the user
                @user.institutions = [institution]
                @user.save!

                # Don't get any courses from playlists or user's groups
                expect(@user.groups).to be_empty
                expect(institution.playlist_pack_ids).to be_empty

                RefreshMaterializedContentViews.refresh_and_rebuild_derived_content_tables
                expect_streams([stream.id])
            end

            it "should show streams in institution's playlists" do
                # Add a playlist with streams to an institution
                institution = Institution.first
                playlist = playlists(:published_playlist)
                institution = Institution.update_from_hash!({id: institution.id, updated_at: institution.updated_at, "playlist_pack_ids": [playlist.locale_pack_id], "groups" => []})

                # Add that institution to the user
                @user.institutions = [institution]
                @user.groups = []
                @user.save!

                # Don't get any courses from groups
                expect(@user.groups).to be_empty
                expect(institution.access_groups).to be_empty

                RefreshMaterializedContentViews.refresh_and_rebuild_derived_content_tables
                expect_streams(playlist.streams.map(&:id))
            end

            # FIXME: Make checks more explicit by using expect_streams
            it "should show streams in cohort's groups" do
                allow(RefreshMaterializedContentViews).to receive(:force_refresh_views).and_return(true)

                # Ensure a stream has a group
                group = AccessGroup.create!({
                    name: 'doesnotexistyet'
                })
                stream.locale_pack.add_to_group(group.name)

                # Add the group to the cohort and add a playlist_pack_id with no stream_entries since it can't be null
                cohort.update!({"groups" => [group]})
                cohort.publish!
                allow(@user).to receive(:relevant_cohort).and_return(cohort)

                # Ensure no other streams for @user
                @user.groups = []
                @user.institutions = []
                User.clear_all_favorites(@user.id)
                @user.save!

                # since we're using add_to_group here, which is not the
                # same how things are done in the wild, published_cohort_stream_locale_packs
                # is not being updated.  published_cohort_stream_locale_packs is used in
                # StreamsAccessMixin#filter_by_user_can_see
                PublishedCohortStreamLocalePack.rebuild

                expect_streams([stream.id])
            end

            it "should show streams in cohort's required playlists" do
                # Add a playlist with streams to a cohort
                cohort.update!({
                    "playlist_collections": [{
                        title: '',
                        required_playlist_pack_ids: [playlists(:published_playlist).locale_pack_id]
                    }]
                })
                cohort.publish!

                # Allow that cohort to be used for the user
                allow(@user).to receive(:relevant_cohort).and_return(cohort)

                # No false-positives
                expect(@user.access_groups).to be_empty
                expect(cohort.access_groups).to be_empty

                RefreshMaterializedContentViews.refresh_and_rebuild_derived_content_tables
                expect_streams(playlists(:published_playlist).streams.map(&:id))
            end

            it "should show streams in cohort's specialization playlists" do
                # Add a playlist with streams to a cohort
                cohort.update!({"specialization_playlist_pack_ids": [playlists(:published_playlist).locale_pack_id]})
                cohort.publish!

                # Allow that cohort to be used for the user
                allow(@user).to receive(:relevant_cohort).and_return(cohort)

                # No false-positives
                expect(@user.access_groups).to be_empty
                expect(cohort.access_groups).to be_empty

                RefreshMaterializedContentViews.refresh_and_rebuild_derived_content_tables
                expect_streams(playlists(:published_playlist).streams.map(&:id))
            end

            it "should show streams in cohort's schedule's required courses" do
                cohort.update!({
                    "periods": [{
                        project_style: "standard",
                        stream_entries: [{locale_pack_id: stream.locale_pack_id, required: true}],
                        days: 7,
                        days_offset: 0
                    }]
                })
                cohort.publish!

                # Allow that cohort to be used for the user
                allow(@user).to receive(:relevant_cohort).and_return(cohort)

                # No false-positives
                expect(@user.access_groups).to be_empty

                RefreshMaterializedContentViews.refresh_and_rebuild_derived_content_tables
                expect_streams([stream.id])
            end

            it "should show streams in cohort's schedule's optional courses" do
                cohort.update!({
                    "playlist_collections": [{
                        title: '',
                        required_playlist_pack_ids: []
                    }],
                    "specialization_playlist_pack_ids": [],
                    "periods": [{
                        project_style: "standard",
                        stream_entries: [{locale_pack_id: stream.locale_pack_id, required: false}],
                        days: 7,
                        days_offset: 0
                    }]
                })
                cohort.publish!
                # Allow that cohort to be used for the user
                allow(@user).to receive(:relevant_cohort).and_return(cohort)

                # No false-positives
                expect(@user.access_groups).to be_empty

                RefreshMaterializedContentViews.refresh_and_rebuild_derived_content_tables
                expect_streams([stream.id])
            end

            def get_user_with_nothing
                user = User.where(pref_locale: 'en').first
                user.groups = []
                user.institutions = []
                user.cohort_applications = []
                user.fallback_program_type = 'nothing'
                user.save!

                json = Lesson::Stream::ToJsonFromApiParams.new(least_restrictive_params({
                    user: user,
                    filters: {
                        user_can_see: true
                    }
                })).json
                result = ActiveSupport::JSON.decode(json)
                expect(result).to be_empty

                return user
            end


            def expect_streams(stream_ids)
                json = Lesson::Stream::ToJsonFromApiParams.new(least_restrictive_params({
                    user: @user,
                    filters: {
                        user_can_see: true,
                        in_users_locale_or_en: true
                    }
                })).json
                result = ActiveSupport::JSON.decode(json)
                expect(result.map {|h| h["id"]}).to match_array(stream_ids)
            end
        end

        describe "in_users_locale" do
            attr_accessor :en_only_item, :en_item, :es_item, :es_only_item

            before(:each) do
                @en_only_item, @en_item, @es_item, @es_only_item = [
                    lesson_streams(:en_only_item),
                    lesson_streams(:en_item),
                    lesson_streams(:es_item),
                    lesson_streams(:published_only_in_es)
                ]
            end


            describe "in_users_locale_or_en" do

                it "should only show english content to an english user" do
                    @user = users(:en_superviewer)

                    result = get_json(least_restrictive_params({
                        user: @user,
                        filters: {
                            in_users_locale_or_en: true,
                            user_can_see: true
                        }
                    }))

                    ids = result.map { |e| e['id'] }
                    expect(ids).to include(en_item.id)
                    expect(ids).to include(en_only_item.id)
                    expect(ids).not_to include(es_item.id)
                    expect(ids).not_to include(es_only_item.id)
                end

                it "should show spanish content to a spanish user but fallback to english if necessary" do
                    @user = users(:en_superviewer)
                    @user.update_columns(pref_locale: 'es')

                    result = get_json(least_restrictive_params({
                        user: @user,
                        filters: {
                            in_users_locale_or_en: true,
                            user_can_see: true
                        }
                    }))

                    ids = result.map { |e| e['id'] }
                    expect(ids).not_to include(en_item.id)
                    expect(ids).to include(en_only_item.id)
                    expect(ids).to include(es_item.id)
                    expect(ids).to include(es_only_item.id)
                end
            end

        end

        describe "in_locale_or_en" do
            attr_accessor :en_only_item, :en_item, :es_item, :es_only_item

            before(:each) do
                @en_only_item, @en_item, @es_item, @es_only_item = [
                    lesson_streams(:en_only_item),
                    lesson_streams(:en_item),
                    lesson_streams(:es_item),
                    lesson_streams(:published_only_in_es)
                ]
            end

            it "should only show english content if 'en'" do
                @user = users(:en_superviewer)

                result = get_json(least_restrictive_params({
                    filters: {
                        in_locale_or_en: 'en'
                    }
                }))

                ids = result.map { |e| e['id'] }
                expect(ids).to include(en_item.id)
                expect(ids).to include(en_only_item.id)
                expect(ids).not_to include(es_item.id)
                expect(ids).not_to include(es_only_item.id)
            end

            it "should show spanish content if 'es' but fallback to english if necessary" do
                @user = users(:en_superviewer)
                @user.update_columns(pref_locale: 'es')

                result = get_json(least_restrictive_params({
                    filters: {
                        in_locale_or_en: 'es'
                    }
                }))

                ids = result.map { |e| e['id'] }
                expect(ids).not_to include(en_item.id)
                expect(ids).to include(en_only_item.id)
                expect(ids).to include(es_item.id)
                expect(ids).to include(es_only_item.id)
            end

        end

        describe "view_as" do
            it "should work with group:id" do
                access_group = AccessGroup.all.detect { |g| g.lesson_stream_locale_packs.any? }
                streams = access_group.lesson_stream_locale_packs.map(&:content_items).flatten.uniq

                result = get_json(least_restrictive_params({
                    user: nil,
                    filters: {
                        view_as: "group:#{access_group.name}",
                        in_locale_or_en: @user.locale
                    }
                }))
                expect(result.map { |entry| entry['id']}).to match_array(streams.map(&:id))
            end

            it "should work with institution:id" do
                institution = institutions(:institution_with_streams)

                streams = institution.lesson_stream_locale_packs.map(&:content_items).flatten.uniq

                result = get_json(least_restrictive_params({
                    user: nil,
                    filters: {
                        view_as: "institution:#{institution.id}",
                        in_locale_or_en: @user.locale
                    }
                }))
                expect(result.map { |entry| entry['id']}).to match_array(streams.map(&:id))
            end

            it "should work with cohort:id" do
                access_group = AccessGroup.all.detect { |g| g.lesson_stream_locale_packs.any? }
                cohort = cohorts(:published_mba_cohort)
                cohort.access_groups = [access_group]
                cohort.save!
                content_items = Lesson::Stream.joins("
                        JOIN published_cohort_stream_locale_packs j
                            ON j.stream_locale_pack_id = lesson_streams.locale_pack_id
                            AND j.cohort_id = '#{cohort.id}'")

                result = get_json(least_restrictive_params({
                    user: nil,
                    filters: {
                        view_as: "cohort:#{cohort.id}",
                        in_locale_or_en: @user.locale
                    }
                }))

                expect(result.map { |entry| entry['id']}).to match_array(content_items.pluck("id"))
            end

            it "should prevent against sql injection in view_as" do
                expect {
                    filter_by_group_name('not%%allowed')
                }.to raise_error(ArgumentError, 'Unsupported value for filter "view_as" of type regexp: "group:not%%allowed"')

                expect {
                    filter_by_group_name('SMARTER')
                }.not_to raise_error
            end
        end

        def filter_by_group_name(group_name)
            get_json(least_restrictive_params({
                user: nil,
                filters: {
                    view_as: "group:#{group_name}"
                }
            }))
        end

        it "should filter by complete=>false" do
            stream_progress = lesson_streams_progress(:completed_stream_progress)
            result = get_json(least_restrictive_params({
                user: stream_progress.user,
                filters: {
                    complete: false
                }
            }))
            ids = result.map { |entry| entry['id'] }
            expect(ids.include?(stream_progress.lesson_stream.id)).to be(false)
        end

    end

    describe "with limit" do

        it "should limit results" do
            result = get_json(least_restrictive_params({
                limit: 1
            }))
            expect(result.size).to eq(1)
        end

    end

    def least_restrictive_params(overrides = {})
        {
            filters: {published: false}
        }.deep_merge(overrides);
    end

    def get_json(params, ability = nil)
        json = Lesson::Stream::ToJsonFromApiParams.new(params, ability).json
        ActiveSupport::JSON.decode(json)
    end

    def get_single_stream_json(stream, params, ability = nil)
        expect(stream).not_to be_nil
        results = get_json(params.deep_merge({filters: {id: stream.id}}), ability)
        results_for_stream = results.select { |r| r['id'] == stream.attributes['id'] }
        results_not_for_stream = results.select { |r| r['id'] && r['id'] != stream.attributes['id'] }
        results_with_no_id = results.select { |r| r['id'].nil? }
        expect(results_for_stream.size).to be <=1

        if results_for_stream.empty? && results_not_for_stream.any?
            raise "There were some results, but not for the stream you were expecting"
        end
        if results_for_stream.empty? && results_with_no_id.any?
            raise "There were some results, but they had no id.  Perhaps you forgot to include 'id' in fields?"
        end
        results_for_stream.first
    end

    def image_json(image)
        {
            'id' => image.id,
            'formats' => image.formats.deep_stringify_keys,
            'dimensions' => image.dimensions.deep_stringify_keys
        }
    end


end
