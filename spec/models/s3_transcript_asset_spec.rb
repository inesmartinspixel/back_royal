# == Schema Information
#
# Table name: s3_transcript_assets
#
#  id                      :uuid             not null, primary key
#  created_at              :datetime
#  updated_at              :datetime
#  file_file_name          :string
#  file_content_type       :string
#  file_file_size          :integer
#  file_updated_at         :datetime
#  file_fingerprint        :string
#  user_id                 :uuid
#  education_experience_id :uuid
#  transcript_type         :string
#  persisted_path          :string
#

require 'spec_helper'

describe S3TranscriptAsset do
    it "should re-identify user after create and delete" do
        expect_any_instance_of(User).to receive(:identify).twice
        asset = S3TranscriptAsset.create!(education_experience_id: CareerProfile::EducationExperience.first.id)
        asset.destroy
    end
end
