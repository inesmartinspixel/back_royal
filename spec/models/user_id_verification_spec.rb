# == Schema Information
#
# Table name: user_id_verifications
#
#  id                           :uuid             not null, primary key
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  cohort_id                    :uuid             not null
#  user_id                      :uuid             not null
#  id_verification_period_index :integer          not null
#  verified_at                  :datetime         not null
#  verification_method          :text             not null
#  idology_verification_id      :uuid
#  verifier_id                  :uuid
#  verifier_name                :text
#

require 'spec_helper'

describe "UserIdVerification" do
    it "should validate allowable verification_method values" do
        instance = UserIdVerification.new(user_id: User.first.id)

        instance.verification_method = "invalid"
        expect(instance.valid?).to be(false)

        instance.verification_method = "verified_by_admin"
        instance.verifier_id = SecureRandom.uuid
        instance.verifier_name = "Mr. Foo"
        expect(instance.valid?).to be(true)

        instance.verification_method = "verified_by_idology"
        instance.idology_verification_id = SecureRandom.uuid
        expect(instance.valid?).to be(true), instance.errors.full_messages.inspect

        instance.verification_method = "waived_due_to_late_enrollment"
        expect(instance.valid?).to be(true)
    end

    it "should validate verifier_id if verified_by_admin" do
        instance = UserIdVerification.new({
            user_id: User.first.id,
            verification_method: 'waived_due_to_late_enrollment',
            verifier_name: 'Mr. Foo'
        })
        expect(instance.valid?).to be(true)

        instance.verification_method = 'verified_by_admin'
        expect(instance.valid?).to be(false)

        instance.verifier_id = SecureRandom.uuid
        expect(instance.valid?).to be(true)
    end

    it "should validate verifier_name if verified_by_admin" do
        instance = UserIdVerification.new({
            user_id: User.first.id,
            verification_method: 'waived_due_to_late_enrollment',
            verifier_id: SecureRandom.uuid
        })
        expect(instance.valid?).to be(true)

        instance.verification_method = 'verified_by_admin'
        expect(instance.valid?).to be(false)

        instance.verifier_name = 'Mr. Foo'
        expect(instance.valid?).to be(true)
    end

    describe "record_verification" do


        it "should create a record" do

            cohort = Cohort.joins(:accepted_users).first
            user = cohort.accepted_users.first
            expect(user.relevant_cohort).to receive(:index_for_active_verification_period).and_return(42)

            now = Time.at(42)
            expect(Time).to receive(:now).at_least(1).times.and_return(now)
            idology_verification_id = SecureRandom.uuid
            expect(Raven).not_to receive(:capture_exception)

            expect(UserIdVerification).to receive(:create!).with(
                user_id: user.id,
                cohort_id: cohort.id,
                id_verification_period_index: 42,
                verified_at: now,
                verification_method: 'verified_by_idology',
                idology_verification_id: idology_verification_id,
                verifier_id: nil,
                verifier_name: nil
            )

            UserIdVerification.record_verification!(user,
                verification_method: 'verified_by_idology',
                idology_verification_id: idology_verification_id
            )
        end

        it "should log a warning if the user is already verified" do
            cohort = Cohort.joins(:accepted_users).first
            user = cohort.accepted_users.first
            now = Time.at(42)
            expect(Time).to receive(:now).at_least(1).times.and_return(now)
            expect(user.relevant_cohort).to receive(:index_for_active_verification_period).at_least(1).times.and_return(42)

            verifier_id = SecureRandom.uuid
            UserIdVerification.record_verification!(user,
                {
                    verification_method: 'verified_by_admin',
                    verifier_id: verifier_id,
                    verifier_name: 'Mr. Foo'
                }
            )

            expect(Raven).to receive(:capture_exception).with("Trying to verify a user for a period, but they are already verified.", {
                extra: {
                    user_id: user.id,
                    cohort_id: cohort.id,
                    id_verification_period_index: 42,
                    verification_method: 'verified_by_admin',
                    verifier_id: verifier_id,
                    verifier_name: 'Mr. Foo'
                }
            })

            UserIdVerification.record_verification!(user,
                {
                    verification_method: 'verified_by_admin',
                    verifier_id: verifier_id,
                    verifier_name: 'Mr. Foo'
                }
            )
        end

    end
end
