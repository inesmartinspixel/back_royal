# == Schema Information
#
# Table name: content_topics
#
#  id         :uuid             not null, primary key
#  created_at :datetime
#  updated_at :datetime
#  locales    :json
#

require 'spec_helper'

describe ContentTopic do

    describe "destroy" do

        it "should not destroy if attached to locale_packs" do
            stream = Lesson::Stream.first
            ContentTopic.update_topics(stream, ContentTopic.all.as_json)
            content_topic = ContentTopic.first
            expect{content_topic.destroy}.to raise_error(ActiveRecord::RecordInvalid)
        end
    end
end
