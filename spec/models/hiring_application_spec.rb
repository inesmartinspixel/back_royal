# == Schema Information
#
# Table name: hiring_applications
#
#  id                                  :uuid             not null, primary key
#  created_at                          :datetime         not null
#  updated_at                          :datetime         not null
#  applied_at                          :datetime         not null
#  accepted_at                         :datetime
#  user_id                             :uuid             not null
#  status                              :text             default("pending"), not null
#  job_role                            :text
#  company_year                        :integer
#  company_employee_count              :text
#  company_annual_revenue              :text
#  company_sells_recruiting_services   :boolean
#  place_id                            :text
#  place_details                       :json             not null
#  logo_url                            :text
#  website_url                         :text
#  hiring_for                          :text
#  team_name                           :text
#  team_mission                        :text
#  role_descriptors                    :text             default([]), is an Array
#  fun_fact                            :text
#  where_descriptors                   :text             default([]), is an Array
#  position_descriptors                :text             default([]), is an Array
#  funding                             :text
#  industry                            :text
#  last_calculated_complete_percentage :float
#  has_seen_welcome                    :boolean          default(FALSE), not null
#  salary_ranges                       :text             default([]), is an Array
#

require 'spec_helper'

describe HiringApplication do

    fixtures :users

    it "should reidentify user if application changes" do
        instance = users(:hiring_manager).hiring_application

        expect_any_instance_of(User).to receive(:identify)
        instance.team_name = "foo"
        instance.save!
    end

    describe "create" do
        before(:each) do
            @user = users("user_without_hiring_application")
            @user.professional_organization = ProfessionalOrganizationOption.reorder(:text).first
            @user.save!
            @user.reload
        end

        it "should set defaults" do
            hiring_application = HiringApplication.create!({
                user_id: @user.id
            })
            expect(hiring_application.applied_at).to be_within(10.seconds).of(Time.now)
            expect(hiring_application.status).to eq('pending')
        end

        it "should validate presence of professional_organization" do
            # valid
            hiring_application = HiringApplication.new(
                user_id: @user.id
            )
            expect(hiring_application).to be_valid

            # not valid
            @user.professional_organization = nil
            @user.save!
            @user.reload
            hiring_application = HiringApplication.new(
                user_id: @user.id
            )
            expect(hiring_application).not_to be_valid
        end

    end

    describe "update_from_hash" do

        it "should work" do
            instance = users(:hiring_manager).hiring_application
            expect_any_instance_of(HiringApplication).to receive(:merge_hash).and_call_original
            HiringApplication.update_from_hash!({
                "id" => instance.id,
                "job_role" => "bossman"
            })
            expect(instance.reload.job_role).to eq("bossman")
        end

    end

    describe "merge_hash" do
        it "should set company fields" do
            hiring_application = HiringApplication.new
            hiring_application.merge_hash(company_attrs)
            assert_attrs(hiring_application, company_attrs.except("user_id"))
        end

        it "should set team field" do
            hiring_application = HiringApplication.new
            hiring_application.merge_hash(team_attrs)
            assert_attrs(hiring_application, team_attrs.except("user_id"))
        end

        it "should set you field" do
            hiring_application = HiringApplication.new
            hiring_application.merge_hash(you_attrs)
            assert_attrs(hiring_application, you_attrs.except("user_id"))
        end

        it "should set candidates field" do
            hiring_application = HiringApplication.new
            hiring_application.merge_hash(candidates_attrs)
            assert_attrs(hiring_application, candidates_attrs.except("user_id"))
        end

        it "should should normalize and URL fields" do
            hiring_application = HiringApplication.new
            hiring_application.merge_hash({
                :website_url => "zombo.com"
            })
            expect(hiring_application.website_url).to eq("https://zombo.com")
        end

        it "should set last_calculated_complete_percentage" do
            hiring_application = HiringApplication.new
            expect(hiring_application.last_calculated_complete_percentage).not_to equal(0.42)
            hiring_application.merge_hash({:last_calculated_complete_percentage => 0.42})
            expect(hiring_application.last_calculated_complete_percentage).to equal(0.42)
        end

        it "should set company_sells_recruiting_services" do
            hiring_application = HiringApplication.new
            expect(hiring_application.company_sells_recruiting_services).not_to equal(true)
            hiring_application.merge_hash({:company_sells_recruiting_services => true})
            expect(hiring_application.company_sells_recruiting_services).to equal(true)
        end
    end

    describe "update" do
        before(:each) do
            @application = HiringApplication.where(status: 'pending').first
            @user = @application.user
        end

        it "should require user hiring_team if accepting" do
            @user.update_columns(hiring_team_id: nil)
            @application.status = 'accepted'

            expect {
                @application.save!
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: User hiring_team must be specified if accepted")

            @user.ensure_hiring_team!
            expect {
                @application.save!
            }.not_to raise_error

        end

        it "should set accepted_at to now if accepting" do
            @application.status = 'accepted'
            @application.save!
            expect(@application.accepted_at).to be_within(10.seconds).of(Time.now)
        end

        it "should set accepted_at to nil if unaccepting" do
            @user.ensure_hiring_team!
            @application.status = 'accepted'
            @application.save!
            expect(@application.accepted_at).not_to be_nil

            @application.status = 'pending'
            @application.save!
            expect(@application.accepted_at).to be_nil
        end

    end

    describe "as_json" do
        before(:each) do
            @application = HiringApplication.where(status: 'accepted').first
            @user = @application.user
        end

        describe "with empty options" do

            it "should work" do
                @application.last_calculated_complete_percentage = 0.42
                json = @application.as_json

                expect(json['status']).to eq(@application.status) # sampling one attruibute
                expect(json['company_name']).to eq(@application.company_name) # an custom property
                expect(json['applied_at'].is_a?(Integer)).to be(true) # a date
                expect(json['accepted_at'].is_a?(Integer)).to be(true)# a date
                expect(json['last_calculated_complete_percentage']).to eq(@application.last_calculated_complete_percentage)
                expect(json['name']).to eq(@user.name) # sampling one property copied from user
            end

        end

        describe "with zapier=true" do

            it "should work" do
                @application.place_details = {'formatted_address' => '1600 Pennsylvania Ave'}
                json = @application.as_json(zapier: true)

                expect(json['status']).to eq(@application.status) # sampling one attruibute
                expect(json['company_name']).to eq(@application.company_name) # an custom property
                expect(json['applied_at'].is_a?(String)).to be(true) # a date
                expect(json['accepted_at'].is_a?(String)).to be(true)# a date
                expect(json['name']).to eq(@user.name) # sampling one property copied from user
                expect(json['location']).to eq(@application.place_details['formatted_address'])
                expect(json['place_details']).to be_nil
            end

        end

    end

    def assert_attrs(obj, attrs)
        attrs = attrs.stringify_keys
        actual_attrs = obj.attributes.slice(*attrs.keys.map(&:to_s)).stringify_keys
        expect(actual_attrs).to eq(attrs.stringify_keys)
    end

    def company_attrs(attrs = {})
        {
            :logo_url => "https://my.logo",
            :company_year => 2016,
            :company_employee_count => "count_identifier",
            :company_annual_revenue => "million_billion",
            :company_sells_recruiting_services => true,
            :website_url => "https://zombo.com", # I <3 THIS
            :funding => "funding_identifier",
            :industry => "industry_identifier"
        }.with_indifferent_access.merge(attrs)
    end

    def team_attrs(attrs = {})
        {
            :hiring_for => "everyone",
            :team_name => "Zombo Inc.",
            :team_mission => "You can do anything",
            :role_descriptors => ["manager"],
        }.with_indifferent_access.merge(attrs)
    end

    def you_attrs(attrs = {})
        {
            :job_role => "DA BAWSE",
            :fun_fact => "I write tests LIKE A BAWSE",
            :team_mission => "You can do anything at zombo com"
        }.with_indifferent_access.merge(attrs)
    end

    def candidates_attrs(attrs = {})
        {
            :where_descriptors => ['timbuktoo'],
            :position_descriptors => ['bawse']
        }.with_indifferent_access.merge(attrs)
    end

    describe "check_professional_organization" do

        before(:each) do
            @unsuggestable_org = ProfessionalOrganizationOption.create!({text: "Zombo Inc.", suggest: false})
            @application = HiringApplication.where(status: 'pending').first
            @application.user.professional_organization = @unsuggestable_org
            @application.user.ensure_hiring_team!
            @application.user.save!
            expect(@application.user.professional_organization.suggest).to be(false)
            @application.status = "accepted"
            @application.save!
        end

        it "should set the corresponding professional_organization to 'suggest = true' if false and hiring application is being accepted" do
            expect(@application.user.professional_organization.suggest).to be(true)
        end

        it "should also ensure all other locales are generated" do
            options = ProfessionalOrganizationOption.where(text: @unsuggestable_org.text, suggest: true).to_a
            expect(options.map(&:locale).to_set).to eq(I18n.available_locales.map(&:to_s).to_set)
        end
    end

    describe "update_hidden_relationships" do
        it "should update relationships when application is accepted" do
            application = HiringApplication.where(status: "pending").first
            application.user.ensure_hiring_team!
            users = CareerProfile.active.limit(2).map(&:user)
            accepted_hidden_relationship = HiringRelationship.create!({
                hiring_manager_id: application.user.id,
                candidate_id: users.first.id,
                hiring_manager_status: "accepted",
                candidate_status: "hidden"
            })
            pending_hidden_relationship = HiringRelationship.create!({
                hiring_manager_id: application.user.id,
                candidate_id: users.second.id,
                hiring_manager_status: "pending",
                candidate_status: "hidden"
            })
            application.status = "accepted"
            application.save!

            expect(accepted_hidden_relationship.reload.candidate_status).to eq("pending")
            expect(pending_hidden_relationship.reload.candidate_status).to eq("hidden")
        end

        it "should not update relationships when rejected" do
            application = HiringApplication.where(status: "pending").first
            relationship = HiringRelationship.create!({
                hiring_manager_id: application.user.id,
                candidate_id: users(:learner).id,
                candidate_status: "hidden"
            })
            application.status = "rejected"
            application.save!

            expect(relationship.reload.candidate_status).to eq("hidden")

        end

        it "should not update relationships when updating something other than status" do
            application = HiringApplication.where(status: "pending").first
            relationship = HiringRelationship.create!({
                hiring_manager_id: application.user.id,
                candidate_id: User.first.id,
                candidate_status: "hidden"
            })
            application.team_name = "foo"
            application.save!

            expect(relationship.reload.candidate_status).to eq("hidden")
        end
    end

    describe "auto_create_hiring_team" do

        it "should generate a team if there is none for the user's domain" do
            user = User.left_outer_joins(:hiring_application)
                    .left_outer_joins(:hiring_team)
                    .where("hiring_applications.id is null and hiring_teams.id is null").first
            user.professional_organization ||= ProfessionalOrganizationOption.first
            user.email = "someone@somewhere.com"
            user.save!

            hiring_application = HiringApplication.create!({
                user_id: user.id
            })
            expect(user.reload.hiring_team).not_to be_nil
        end

        it "should not generate a team if there is one for the user's domain" do
            user = User.left_outer_joins(:hiring_application)
                    .left_outer_joins(:hiring_team)
                    .where("hiring_applications.id is null and hiring_teams.id is null").first
            user.professional_organization ||= ProfessionalOrganizationOption.first
            user.email = "someone@somewhere.com"
            user.save!

            HiringTeam.first.update(domain: 'somewhere.com')

            hiring_application = HiringApplication.create!({
                user_id: user.id
            })
            expect(user.reload.hiring_team).to be_nil
        end

    end

    describe "log_status_event / log_created_event" do

        it "should log created event" do
            allow_any_instance_of(HiringTeam).to receive(:create_hiring_plan_changed_event)
            user = User.left_outer_joins(:hiring_application)
                    .where("hiring_applications.id is null").first
            user.professional_organization ||= ProfessionalOrganizationOption.first
            user.email = "someone@somewhere.com"
            user.save!

            expect(Event).to receive(:create_server_event!).with(
                anything,
                user.id,
                'hiring_application:created',
                anything
            ).and_call_original

            hiring_application = HiringApplication.create!({
                user_id: user.id
            })

        end

        it "should log rejected event" do
            application = HiringApplication.where(status: 'pending').first

            expect(Event).to receive(:create_server_event!).with(
                anything,
                application.user_id,
                'hiring_application:rejected',
                anything
            ).and_call_original

            application.update!(status: 'rejected')

        end

        it "should log accepted event" do
            application = HiringApplication.where(status: 'pending').first

            expect(Event).to receive(:create_server_event!).with(
                anything,
                application.user_id,
                'hiring_application:accepted',
                anything
            ).and_call_original

            application.update!(status: 'accepted')

        end

        it "should include expected stuff in payload and call log_to_external_systems" do

            application = HiringApplication.where(status: 'pending').first
            mock_event = "mock_event"
            expect(mock_event).to receive(:log_to_external_systems)

            expect(Event).to receive(:create_server_event!) {|id, user_id, event_type, payload|

                if event_type == 'hiring_application:accepted'
                    expect(payload).to eq({
                        :hiring_plan => application.user.hiring_team&.hiring_plan,
                        :hiring_team_id => application.user.hiring_team_id,
                        :status => "accepted",
                        :old_status => "pending",
                        :is_hiring_team_owner => application.user.hiring_team&.owner_id == application.user.id,
                        :has_subscription => application.user.hiring_team&.subscriptions&.any?,
                        :has_primary_subscription => application.user.hiring_team&.primary_subscription.present?,
                        :primary_subscription_stripe_plan_id => application.user.hiring_team&.primary_subscription&.stripe_plan_id
                    })
                end
            }.and_return(mock_event)

            application.update!(status: 'accepted')
        end

        it "should log two events when accepting an invited user on creation" do
            user = User.left_outer_joins(:hiring_application)
                    .where("hiring_applications.id is null").first
            user.professional_organization ||= ProfessionalOrganizationOption.first
            user.email = "someone@somewhere.com"
            user.save!

            start = Time.now

            hiring_application = HiringApplication.new({
                user_id: user.id,
                status: 'accepted'
            })
            hiring_application.save!(validate: false)

            new_events = Event.where("created_at > ?", start).where("event_type like 'hiring_application%'")

            expect(new_events.size).to eq(2)

            created_event = new_events.detect { |ev| ev.event_type == 'hiring_application:created' }
            expect(created_event.payload['status']).to eq('accepted')

            created_event = new_events.detect { |ev| ev.event_type == 'hiring_application:accepted' }
            expect(created_event.payload['status']).to eq('accepted')
            expect(created_event.payload['old_status']).to eq(nil)
        end

        describe "close_hiring_relationships_if_becoming_rejected" do
            attr_accessor :hiring_application, :open_accepted_hiring_relationships

            before(:each) do
                @hiring_application = users(:hiring_manager_with_team).hiring_application
                @open_accepted_hiring_relationships = hiring_application.user.candidate_relationships_with_accepted_hiring_manager_status
                    .where(hiring_manager_closed: [nil, false])
                expect(hiring_application.status).to eq('accepted')
                expect(open_accepted_hiring_relationships.size).to be > 0 # sanity check
            end

            it "should automatically close accepted hiring relationships when becoming rejected" do
                closed_by_candidate_relationship = HiringRelationship.create!(
                    hiring_manager_id: hiring_application.user.id,
                    candidate_id: User.where.not(id: open_accepted_hiring_relationships.pluck(:candidate_id)).first.id,
                    hiring_manager_status: 'accepted',
                    candidate_status: 'accepted',
                    candidate_closed: true
                )
                hiring_application.update!(status: 'rejected')
                open_accepted_hiring_relationships.each do |hr|
                    if hr.id == closed_by_candidate_relationship.id
                        expect(hr.hiring_manager_closed).to be_nil
                        expect(hr.candidate_closed).to be(true)
                    else
                        expect(hr.hiring_manager_closed).to be(true)
                    end
                end
            end

            it "should not close accepted hiring relationships if not changing to rejected" do
                hiring_application.update!(status: 'pending')
                open_accepted_hiring_relationships.each do |hr|
                    expect(hr.hiring_manager_closed).to be_nil
                end
            end
        end
    end
end
