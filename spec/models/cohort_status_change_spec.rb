# == Schema Information
#
# Table name: cohort_status_changes
#
#  user_id                   :uuid
#  cohort_id                 :uuid
#  from_time                 :datetime
#  until_time                :datetime
#  status                    :text
#  cohort_application_status :text
#  graduation_status         :text
#  id                        :uuid             not null, primary key
#  was_enrolled              :boolean          not null
#  cohort_application_id     :uuid             not null
#
require 'spec_helper'

describe CohortStatusChange do
    attr_reader :cohort, :user

    fixtures(:cohorts)

    before(:each) do
        @cohort = cohorts(:published_mba_cohort)
        @user = User.left_outer_joins(:cohort_applications).where("cohort_applications.id is null").first
        CareerProfileList.delete_all # prevent validation errors
    end

    describe "update_cohort_status_changes_for_application" do

        describe "simple status changes" do

            it "should write no record for pre_accepted version" do
                cohort_application = create_application('pre_accepted')
                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)
                expect(get_cohort_status_changes.size).to eq(0)
            end

            it "should write deferred record for deferred version" do
                cohort_status_change = create_application_and_assert_one_cohort_status_change('deferred')
                expect(cohort_status_change.status).to eq('deferred')
            end
        end

        describe "enrolled changes" do

            it "should handle enrolled record and other records in a batch for enrolled user" do
                set_enrollment_deadline(Time.now - 4.day)

                # create an application that was marked as accepted 7 days ago with
                # an enrollment deadline 4 days ago and a gradution 1 day ago
                cohort_application = create_application('accepted')
                version1 = CohortApplication::Version.where(id: cohort_application.id).first
                version1.version_created_at = Time.now - 7.days
                version1.commit_changes


                cohort_application.update!(graduation_status: 'graduated')
                version2 = CohortApplication::Version.where(id: cohort_application.id).reorder(:updated_at).last
                version2.version_created_at = Time.now - 1.days
                version2.commit_changes

                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

                assert_cohort_status_changes_for_enrolled_user(version1, version2)
            end

            it "should handle enrolled record and other records saved one-by-one for enrolled user" do
                seven_days_ago = 7.days.ago
                four_days_ago = 4.days.ago
                one_day_agp = 1.day.ago
                set_enrollment_deadline(four_days_ago)
                allow(Time).to receive(:now) { @now }
                @now = seven_days_ago

                # create an application that was marked as accepted 7 days ago with
                # an enrollment deadline 4 days ago and a gradution 1 day ago. Update cohort_status_changes
                # between each
                cohort_application = create_application('accepted')
                version1 = CohortApplication::Version.where(id: cohort_application.id).first
                version1.version_created_at = seven_days_ago
                version1.commit_changes
                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

                @now = cohort.enrollment_deadline + 1.minute
                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

                cohort_application.update!(graduation_status: 'graduated')
                version2 = CohortApplication::Version.where(id: cohort_application.id).reorder(:updated_at).last
                version2.version_created_at = one_day_agp
                version2.commit_changes
                @now = version2.version_created_at + 1.minute
                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

                assert_cohort_status_changes_for_enrolled_user(version1, version2)

            end

            it "should handle user who is accepted after the enrollment deadline" do
                set_enrollment_deadline(Time.now - 4.day)

                # create an application for a user who was expelled at the
                # enrollment deadline but then let back in later
                cohort_application = create_application('accepted')
                version1 = CohortApplication::Version.where(id: cohort_application.id).first
                version1.version_created_at = Time.now - 7.days
                version1.commit_changes

                cohort_application.update!(status: 'expelled')
                version2 = CohortApplication::Version.where(id: cohort_application.id).reorder(:updated_at).last
                version2.version_created_at = Time.now - 5.days
                version2.commit_changes

                cohort_application.update!(status: 'accepted')
                version3 = CohortApplication::Version.where(id: cohort_application.id).reorder(:updated_at).last
                version3.version_created_at = Time.now - 3.days
                version3.commit_changes

                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

                cohort_status_changes = get_cohort_status_changes
                expect(cohort_status_changes.count).to eq(3)

                expect_attrs(cohort_status_changes.first, {
                    from_time: version1.version_created_at,
                    until_time: version2.version_created_at,
                    status: 'pending_enrollment'
                })
                expect_attrs(cohort_status_changes.second, {
                    from_time: version2.version_created_at,
                    until_time: version3.version_created_at,
                    status: 'did_not_enroll'
                })
                expect_attrs(cohort_status_changes.third, {
                    from_time: version3.version_created_at,
                    until_time: Time.parse('2099/01/01 00:00:00 UTC'),
                    status: 'enrolled'
                })
            end

        end

        describe "graduation changes" do

            it "should write failed record for failed version" do
                cohort_status_change = create_application_and_assert_one_cohort_status_change('accepted', 'failed')
                expect(cohort_status_change.status).to eq('failed')
            end

            it "should write graduated record for graduated version" do
                cohort_status_change = create_application_and_assert_one_cohort_status_change('accepted', 'graduated')
                expect(cohort_status_change.status).to eq('graduated')
            end

            it "should write graduated record for honors version" do
                cohort_status_change = create_application_and_assert_one_cohort_status_change('accepted', 'honors')
                expect(cohort_status_change.status).to eq('graduated')
            end

        end

        describe "rejected changes" do

            it "should write no records when an application goes from pending to rejected" do
                cohort_application = create_application('pending')
                cohort_application.update!(status: 'rejected')
                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)
                expect(get_cohort_status_changes).to be_empty
            end

            it "should write a rejected record when an application goes from accepted to rejected" do
                cohort_application = create_application('accepted')
                cohort_application.update!(status: 'rejected')
                fix_version_created_ats(cohort_application.id)

                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

                cohort_status_changes = get_cohort_status_changes
                expect(cohort_status_changes.map(&:status)).to eq(['pending_enrollment', 'rejected'])
            end

        end

        describe "expelled changes" do

            it "should write an expelled record when an application goes from enrolled to expelled" do
                set_enrollment_deadline(Time.now - 1.day)
                cohort_application = create_application('accepted')
                cohort_application.update!(status: 'expelled')
                fix_version_created_ats(cohort_application.id)

                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

                cohort_status_changes = get_cohort_status_changes
                expect(cohort_status_changes.map(&:status)).to eq(['enrolled', 'expelled'])
            end

            it "should write a did_not_enroll record when an application goes from pending_enrollment to expelled" do
                set_enrollment_deadline(Time.now + 1.day)
                cohort_application = create_application('accepted')
                cohort_application.update!(status: 'expelled')
                fix_version_created_ats(cohort_application.id)

                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

                cohort_status_changes = get_cohort_status_changes
                expect(cohort_status_changes.map(&:status)).to eq(['pending_enrollment', 'did_not_enroll'])
            end

        end

        describe "until_time updates" do

            it "should update until_time on previously written record when a new one is written" do
                cohort_application = create_application('accepted')
                fix_version_created_ats(cohort_application.id)

                # write the first record
                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

                cohort_application.update!(status: 'expelled')
                versions = fix_version_created_ats(cohort_application.id)
                # write the second record
                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

                assert_from_and_until_times_on_consecutive_records(versions)
            end

            it "should update until_time on first record when two records are being written at once" do
                cohort_application = create_application('accepted')
                cohort_application.update!(status: 'expelled')
                versions = fix_version_created_ats(cohort_application.id)

                # write both records at once
                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)
                assert_from_and_until_times_on_consecutive_records(versions)
            end


            def assert_from_and_until_times_on_consecutive_records(versions)

                cohort_status_changes = get_cohort_status_changes
                expect(cohort_status_changes[0].from_time).to eq(versions[0].version_created_at)
                expect(cohort_status_changes[0].until_time).to eq(versions[1].version_created_at)

                expect(cohort_status_changes[1].from_time).to eq(versions[1].version_created_at)
                expect(cohort_status_changes[1].until_time).to eq(Time.parse('2099/01/01 00:00:00 UTC'))

            end

        end

        describe "deferred/expelled situations" do

            it "deferred before enrollment then expelled shoudl work" do
                set_enrollment_deadline(Time.now - 4.days)

                # accepted before the enrollment deadline
                cohort_application = create_application('accepted')
                version1 = CohortApplication::Version.where(id: cohort_application.id).first
                version1.version_created_at = Time.now - 7.days
                version1.commit_changes

                # deferred before the enrollment deadline
                cohort_application.update!(status: 'deferred')
                version2 = CohortApplication::Version.where(id: cohort_application.id).reorder(:updated_at).last
                version2.version_created_at = Time.now - 5.days
                version2.commit_changes

                # expelled after the enrollment deadline
                cohort_application.update!(status: 'expelled')
                version3 = CohortApplication::Version.where(id: cohort_application.id).reorder(:updated_at).last
                version3.version_created_at = Time.now - 1.days
                version3.commit_changes

                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

                cohort_status_changes = get_cohort_status_changes

                expect(cohort_status_changes.map(&:status)).to eq(['pending_enrollment', 'deferred', 'did_not_enroll'])

            end

            it "deferred after enrollment then expelled should work" do
                set_enrollment_deadline(Time.now - 4.days)

                # accepted before the enrollment deadline
                cohort_application = create_application('accepted')
                version1 = CohortApplication::Version.where(id: cohort_application.id).first
                version1.version_created_at = Time.now - 7.days
                version1.commit_changes

                # deferred after the enrollment deadline
                cohort_application.update!(status: 'deferred')
                version2 = CohortApplication::Version.where(id: cohort_application.id).reorder(:updated_at).last
                version2.version_created_at = Time.now - 3.days
                version2.commit_changes

                # expelled after the enrollment deadline
                cohort_application.update!(status: 'expelled')
                version3 = CohortApplication::Version.where(id: cohort_application.id).reorder(:updated_at).last
                version3.version_created_at = Time.now - 1.days
                version3.commit_changes

                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

                cohort_status_changes = get_cohort_status_changes

                expect(cohort_status_changes.map(&:status)).to eq(['pending_enrollment','enrolled', 'deferred', 'expelled'])
            end

        end

        describe "changing cohort ids" do

            it "should handle an application for which the cohort_id was changed" do
                # simulating the situation for 88effcbe-2b2e-4f75-9341-ef60c2853ec3
                cohorts = Cohort.all_published.where(program_type: 'mba').limit(2)

                # set the enrollment deadline so we know whether the
                # statuses will be enrolled or pending_enrollment
                set_enrollment_deadline(4.days.ago, cohorts[0])
                set_enrollment_deadline(4.days.ago, cohorts[1])

                cohort_application = CohortApplication.create!(
                    user_id: user.id,
                    cohort_id: cohorts[0].id,
                    status: 'accepted',
                    applied_at: Time.now,
                    cohort_slack_room_id: cohort.slack_rooms.first.id,
                    graduation_status: 'pending'
                )

                # when deferring the user, the cohort on the application is changed,
                # and then immediately changed back (presumably due to a mistake)
                cohort_application.update!(status: 'deferred', cohort_id: cohorts[1].id)
                cohort_application.update!(cohort_id: cohorts[0].id)
                fix_version_created_ats(cohort_application)

                cohort_application2 = CohortApplication.create!(
                    user_id: user.id,
                    cohort_id: cohorts[1].id,
                    status: 'accepted',
                    applied_at: Time.now,
                    cohort_slack_room_id: cohort.slack_rooms.first.id,
                    graduation_status: 'pending'
                )
                fix_version_created_ats(cohort_application2)

                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)
                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application2.id)

                changes_for_cohort_0 = CohortStatusChange.where(user_id: user.id, cohort_id: cohorts[0].id).reorder(:from_time)
                expect(changes_for_cohort_0.map(&:status)).to eq([
                    'enrolled',
                    'deferred'
                ])

                # there should be a hole in the cohort_status_changes for this
                # cohort when the application was switched away to another cohort
                expect(changes_for_cohort_0[0].until_time).to be < changes_for_cohort_0[1].from_time

                changes_for_cohort_1 = CohortStatusChange.where(user_id: user.id, cohort_id: cohorts[1].id).reorder(:from_time)
                expect(changes_for_cohort_1.map(&:status)).to eq([
                    'deferred',
                    'enrolled'
                ])
                expect(changes_for_cohort_1[0].until_time).to be < changes_for_cohort_1[1].from_time

            end

            # This spec sets up a different situation than the one above, because I was
            # simulating a user that I got into a bad state by making updates in the browser.
            # In fact, as indicated by the title of the spec, the substantive difference is
            # just whether we run update_cohort_status_changes_for_application after all the versions
            # are created, or whether we run it after each version
            it "should handle a situation where the cohort_id for a cohort was changed and the changes were processed separately" do
                cohorts = Cohort.all_published.where(program_type: 'mba').limit(2)

                # set the enrollment deadline so we know whether the
                # statuses will be enrolled or pending_enrollment
                set_enrollment_deadline(4.days.ago, cohorts[0])
                set_enrollment_deadline(4.days.ago, cohorts[1])

                cohort_application = CohortApplication.create!(
                    user_id: user.id,
                    cohort_id: cohorts[0].id,
                    status: 'accepted',
                    applied_at: Time.now,
                    cohort_slack_room_id: cohort.slack_rooms.first.id,
                    graduation_status: 'pending'
                )

                fix_version_created_ats(cohort_application)
                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

                cohort_application.update!(cohort_id: cohorts[1].id)

                versions = fix_version_created_ats(cohort_application)
                CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

                cohort_status_changes = CohortStatusChange.where(cohort_application_id: cohort_application.id).reorder(:from_time)

                expect(cohort_status_changes.size).to eq(2)
                expect(cohort_status_changes[0].status).to eq('enrolled')
                expect(cohort_status_changes[0].cohort_id).to eq(cohorts[0].id)
                expect(cohort_status_changes[0].from_time).to eq(versions[0].version_created_at)
                expect(cohort_status_changes[0].until_time).to eq(versions[1].version_created_at)

                expect(cohort_status_changes[1].status).to eq('enrolled')
                expect(cohort_status_changes[1].cohort_id).to eq(cohorts[1].id)
                expect(cohort_status_changes[1].from_time).to eq(versions[1].version_created_at)
                expect(cohort_status_changes[1].until_time).to eq(Time.parse('2099-01-01 00:00:00 UTC'))
            end

        end

        # See comment near `last_cohort_status_change&.from_time == attrs[:from_time]`
        # in append_cohort_status_change
        it "should handle multiple versions saved in the same transaction" do
            cohort_application = create_application('accepted')
            cohort_application.update!(status: 'expelled')

            # write both records at once
            CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)
            expect(get_cohort_status_changes.map(&:status)).to eq(['did_not_enroll'])
        end

        it "should not write a record when nothing has changed" do
            cohort_application = create_application('accepted')
            cohort_application.update!(updated_at: Time.now)
            fix_version_created_ats(cohort_application.id)
            CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)
            expect(get_cohort_status_changes.size).to eq(1)

        end

        it "should default to a pending graduation_status for old versions where it is nil" do
            cohort_application = create_application('accepted')
            cohort_application.update!(updated_at: Time.now)
            versions = fix_version_created_ats(cohort_application.id)
            versions[0].graduation_status = nil
            versions[0].commit_changes
            expect(versions[1].graduation_status).to eq('pending') # sanity check
            CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

            cohort_status_changes = get_cohort_status_changes
            expect(cohort_status_changes.size).to eq(1)
            expect(cohort_status_changes[0].graduation_status).to eq('pending')
        end



        it "should handle deletion of an application" do
            cohort_application = create_application('pre_accepted')
            CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

            cohort_application.destroy

            # If the status on the application had been changed just before it
            # was deleted, then this could be called in a job after the application
            # is gone (we've seen this happen in the wild)
            CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

            expect(get_cohort_status_changes).to be_empty
        end

        # Since we're in a spec, everything is being created within a single
        # transaction.  That means that all the versions will have the same
        # version_created_at, since, according to the database, time stands
        # still during a transaction (magical).  This helper updates
        # all the version_created_ats so they will be treated as
        # having happened at different times inside of update_cohort_status_changes_for_application
        def fix_version_created_ats(cohort_application_id)
            CohortApplication::Version.where(id: cohort_application_id).reorder(:updated_at).each do |version|
                version.version_created_at = version.updated_at
                version.commit_changes
            end
        end

        def set_enrollment_deadline(time, cohort=nil)
            cohort ||= self.cohort
            diff = (time - cohort.start_date) / 1.day
            cohort.enrollment_deadline_days_offset = diff
            cohort.publish!
            RefreshMaterializedContentViews.refresh(true)

            # needed because cached_published is using ContentViewsRefresh.debounced_value
            SafeCache.clear
        end

        def create_application(status, graduation_status='pending')
            cohort_application = CohortApplication.create!(
                user_id: user.id,
                cohort_id: cohort.id,
                status: status,
                applied_at: Time.now,
                cohort_slack_room_id: cohort.slack_rooms.first.id,
                graduation_status: graduation_status
            )
        end

        def create_application_and_assert_one_cohort_status_change(status, graduation_status='pending')
            cohort_application = create_application(status, graduation_status)

            CohortStatusChange.update_cohort_status_changes_for_application(cohort_application.id)

            cohort_status_changes = get_cohort_status_changes
            expect(cohort_status_changes.count).to eq(1)
            first_version_created_at = max_version_created_at(cohort_application)

            cohort_status_change = cohort_status_changes.first
            expect_attrs(cohort_status_change, {
                from_time: first_version_created_at,
                until_time: Time.parse('2099-01-01 00:00:00 UTC'),
                cohort_application_status: status,
                graduation_status: graduation_status
            })
            cohort_status_change
        end

        def get_cohort_status_changes
            CohortStatusChange.where(user_id: user.id, cohort_id: cohort.id).reorder(:from_time)
        end

        def max_version_created_at(cohort_application)
            CohortApplication::Version.where(id: cohort_application.id).maximum(:version_created_at)
        end

        def expect_attrs(cohort_status_change, attrs)
            record_attrs = cohort_status_change.attributes.symbolize_keys

            # hacking around rounding issues on ci
            if attrs[:from_time] && (attrs[:from_time] - record_attrs[:from_time]).abs < 0.000001
                attrs[:from_time] = record_attrs[:from_time]
            end
            if attrs[:until_time] && (attrs[:until_time] - record_attrs[:until_time]).abs < 0.000001
                attrs[:until_time] = record_attrs[:until_time]
            end
            expect(record_attrs).to have_entries(attrs)
        end

        def assert_cohort_status_changes_for_enrolled_user(version1, version2)

            cohort_status_changes = get_cohort_status_changes
            expect(cohort_status_changes.count).to eq(3)

            expect_attrs(cohort_status_changes.first, {
                from_time: version1.version_created_at,
                until_time: cohort.enrollment_deadline,
                status: 'pending_enrollment',
                graduation_status: 'pending'
            })
            expect_attrs(cohort_status_changes.second, {
                from_time: cohort.enrollment_deadline,
                until_time: version2.version_created_at,
                status: 'enrolled',
                graduation_status: 'pending'
            })
            expect_attrs(cohort_status_changes.third, {
                from_time: version2.version_created_at,
                until_time: Time.parse('2099/01/01 00:00:00 UTC'),
                status: 'graduated',
                graduation_status: 'graduated'
            })
        end
    end

end
