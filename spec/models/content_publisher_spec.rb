# == Schema Information
#
# Table name: content_publishers
#
#  id                       :uuid             not null, primary key
#  created_at               :datetime
#  updated_at               :datetime
#  published_at             :datetime
#  object_updated_at        :datetime
#  lesson_version_id        :uuid
#  lesson_stream_version_id :uuid
#  playlist_version_id      :uuid
#  cohort_version_id        :uuid
#

require 'spec_helper'

describe ContentPublisher do

    before(:each) do
        @lesson = Lesson.first
        @lesson.title = "Version 1"
        @lesson.save!
        @lesson.title = "Version 2"
        @lesson.save!

        @last_version = @lesson.versions.last
    end

    it "should work when reloading a live, published item" do
        @lesson.publish!

        reloaded = Lesson.find(@lesson.id)

        # note: provide allowances due to microsecond (postgres) / and nanosecond (rails) precisions
        expect(reloaded.published_version.updated_at).to be_within(0.001.seconds).of(@lesson.updated_at)
    end

    it "should allow for unpublishing" do
        @lesson.publish!
        expect(@lesson.published_version).not_to be(nil)
        @lesson.unpublish!
        expect(@lesson.published_version).to be(nil)

        @lesson.reload
        expect(@lesson.published_version).to be(nil)
    end

    describe "published_at" do

        it "should be updated on publish! only if update_published_at is true" do
            @lesson.publish!
            expect(@lesson.content_publisher.published_at).to be(nil)
            @lesson.publish!({update_published_at: true})
            expect(@lesson.content_publisher.published_at).not_to be(nil)
        end

        it "should be removed on unpublish" do
            @lesson.publish!({update_published_at: true})
            expect(@lesson.content_publisher.published_at).not_to be(nil)
            @lesson.unpublish!
            expect(@lesson.content_publisher).to be(nil)
        end

    end

    describe 'Publishable' do

        it 'should add an all_published method' do
            Lesson.all.map(&:unpublish!)
            @lesson = Lesson.first
            expect(Lesson.all_published).to eq([])
            @lesson.publish!
            expect(Lesson.all_published).to eq([@lesson])
            @lesson.unpublish!
            expect(Lesson.all_published).to eq([])
        end

    end

end
