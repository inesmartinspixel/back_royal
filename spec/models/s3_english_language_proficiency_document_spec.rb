# == Schema Information
#
# Table name: s3_english_language_proficiency_documents
#
#  id                :uuid             not null, primary key
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  file_file_name    :string
#  file_content_type :string
#  file_file_size    :bigint
#  file_updated_at   :datetime
#  file_fingerprint  :string
#  user_id           :uuid
#  persisted_path    :string
#

require 'spec_helper'

describe S3EnglishLanguageProficiencyDocument do
    it "should re-identify user after create and delete" do
        expect_any_instance_of(User).to receive(:identify).twice
        asset = S3EnglishLanguageProficiencyDocument.create!(user_id: User.first.id)
        asset.destroy
    end
end
