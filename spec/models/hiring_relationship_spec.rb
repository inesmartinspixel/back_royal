# == Schema Information
#
# Table name: hiring_relationships
#
#  id                         :uuid             not null, primary key
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  hiring_manager_id          :uuid             not null
#  candidate_id               :uuid             not null
#  hiring_manager_status      :text             default("pending"), not null
#  candidate_status           :text             default("hidden"), not null
#  hiring_manager_priority    :float            default(100.0), not null
#  hiring_manager_closed      :boolean
#  candidate_closed           :boolean
#  open_position_id           :uuid
#  matched_at                 :datetime
#  hiring_manager_closed_info :json             not null
#  conversation_id            :integer
#  matched_by_hiring_manager  :boolean
#  stripe_usage_record_info   :json
#

require 'spec_helper'
require 'stripe_helper'

describe HiringRelationship do

    include StripeHelper

    fixtures :hiring_relationships, :users

    describe "validates" do
        it "should be invalid if not in hiring_manager_statuses" do
            relationship = HiringRelationship.where(hiring_manager_status: 'pending').first
            relationship.update_column(:hiring_manager_status, 'foo')
            expect(relationship.reload.valid?).to be(false)
            relationship.update_column(:hiring_manager_status, 'pending')
            expect(relationship.reload.valid?).to be(true), relationship.errors.full_messages.inspect
        end

        it "should be invalid if not in candidate_statuses" do
            relationship = HiringRelationship.where(candidate_status: 'pending').first
            relationship.update_column(:candidate_status, 'foo')
            expect(relationship.reload.valid?).to be(false)
            relationship.update_column(:candidate_status,  'pending')
            expect(relationship.reload.valid?).to be(true), relationship.errors.full_messages.inspect
        end
    end

    describe "candidate_position_interest conflict handling" do

        it "should update an existing candidate_position_interest to hidden when the relationship is accepted by hiring manager" do
            ensure_handles_conflict("accepted")
        end

        it "should update an existing candidate_position_interest to hidden when the relationship is rejected by hiring manager" do
            ensure_handles_conflict("rejected")
        end

        it "should not update a candidate_position_interest that is already marked as hidden or rejected" do
            candidate_position_interest = CandidatePositionInterest.where(candidate_status: 'accepted').first
            HiringRelationship.where(candidate_id: candidate_position_interest.candidate_id, hiring_manager_id: candidate_position_interest.hiring_manager.id).destroy_all

            CandidatePositionInterest.hidden_or_rejected_hiring_manager_statuses.each do |status|
                candidate_position_interest.update_attribute(:hiring_manager_status, status)
                hiring_manager = candidate_position_interest.hiring_manager
                hiring_manager.ensure_candidate_relationships([candidate_position_interest.candidate_id])
                hiring_relationship = hiring_manager.candidate_relationships.find_by_candidate_id(candidate_position_interest.candidate_id)
                hiring_relationship.update_attribute(:hiring_manager_status, 'accepted')
                expect(candidate_position_interest.reload.hiring_manager_status).to eq(status)
                hiring_relationship.update_attribute(:hiring_manager_status, 'pending') # reset for the next loop
            end

        end

        it "should update a candidate_position_interest that is already marked as reviewed" do
            candidate_position_interest = CandidatePositionInterest.first
            HiringRelationship.where(candidate_id: candidate_position_interest.candidate_id, hiring_manager_id: candidate_position_interest.hiring_manager.id).destroy_all

            statuses = CandidatePositionInterest.reviewed_hiring_manager_statuses.reject { |s| s == 'rejected' }
            statuses.each do |status|
                candidate_position_interest.update_attribute(:hiring_manager_status, status)
                hiring_manager = candidate_position_interest.hiring_manager
                hiring_manager.ensure_candidate_relationships([candidate_position_interest.candidate_id])
                hiring_relationship = hiring_manager.candidate_relationships.find_by_candidate_id(candidate_position_interest.candidate_id)
                hiring_relationship.update_attribute(:hiring_manager_status, 'accepted')
                expect(candidate_position_interest.reload.hiring_manager_status).to eq('hidden')
                hiring_relationship.update_attribute(:hiring_manager_status, 'pending') # reset for the next loop
            end

        end

        it "should update candidate_position_interest to hidden when teammate accepts relationship" do
            hiring_manager = users(:hiring_manager_with_team)
            teammate = users(:hiring_manager_teammate)

            excluded_ids = hiring_manager.candidate_relationships_with_accepted_hiring_manager_status.pluck(:candidate_id)
            candidate_position_interest = CandidatePositionInterest.where.not(candidate_id: excluded_ids).where(hiring_manager_status: 'pending').first
            candidate_position_interest.open_position.update_attribute(:hiring_manager_id, hiring_manager.id)

            teammate.candidate_relationships.destroy_all
            teammate.ensure_candidate_relationships([candidate_position_interest.candidate_id])
            teammate_hiring_relationship = teammate.candidate_relationships.find_by_candidate_id(candidate_position_interest.candidate_id)
            teammate_hiring_relationship.update_attribute(:hiring_manager_status, 'accepted')

            expect(candidate_position_interest.reload.hiring_manager_status).to eq('hidden')
        end

        it "should not update candidate_position_interest to hidden when teammate rejects relationship" do
            hiring_manager = users(:hiring_manager_with_team)
            teammate = users(:hiring_manager_teammate)

            excluded_ids = hiring_manager.candidate_relationships_with_accepted_hiring_manager_status.pluck(:candidate_id)
            candidate_position_interest = CandidatePositionInterest.where.not(candidate_id: excluded_ids).where.not(hiring_manager_status: ['reviewed', 'hidden']).first
            candidate_position_interest.open_position.update!(hiring_manager_id: hiring_manager.id)
            original_status = candidate_position_interest.hiring_manager_status

            teammate.ensure_candidate_relationships([candidate_position_interest.candidate_id])
            teammate_hiring_relationship = teammate.candidate_relationships.find_by_candidate_id(candidate_position_interest.candidate_id)

            teammate_hiring_relationship.update_attribute(:hiring_manager_status, 'rejected')

            expect(candidate_position_interest.reload.hiring_manager_status).to eq(original_status)

        end

        it "should not mark an interest hidden when it is for an unrelated hiring manager than the one accepting" do
            hiring_manager = users(:hiring_manager)
            hiring_manager.ensure_hiring_team!
            candidate = users(:learner)

            hiring_manager.candidate_relationships.destroy_all
            position = OpenPosition.create!(hiring_manager_id: hiring_manager.id, title: "foo", available_interview_times: 'times')
            interest = CandidatePositionInterest.create!({
                candidate_id: candidate.id,
                open_position_id: position.id,
                candidate_status: 'accepted',
                hiring_manager_status: 'unseen'
            })

            second_hiring_manager = HiringApplication.joins(:user).joins("INNER JOIN hiring_teams ON users.hiring_team_id = hiring_teams.id")
                .where.not(user_id: hiring_manager.id).where.not("hiring_teams.id = '#{hiring_manager.hiring_team_id}'").where(status: 'accepted')
                .first.user

            HiringRelationship.create!(hiring_manager_id: second_hiring_manager.id,
                candidate_id: candidate.id,
                hiring_manager_status: "accepted")

            expect(interest.hiring_manager_status).to eq('unseen')
        end

        def ensure_handles_conflict(hiring_manager_status)
            candidate_position_interest = CandidatePositionInterest.find_by_hiring_manager_status('unseen')
            hiring_manager = candidate_position_interest.hiring_manager

            ensure_fresh_hiring_team(hiring_manager)

            hiring_relationship = nil;
            # The following needs to be wrapped in its own transaction, so the
            # after_commit hooks in hiring_relationship.rb will fire appropriately.
            ActiveRecord::Base.transaction do
                hiring_relationship = HiringRelationship.create!(hiring_manager_id: hiring_manager.id, candidate_id: candidate_position_interest.candidate_id, hiring_manager_status: hiring_manager_status)
            end

            expect(candidate_position_interest.reload.hiring_manager_status).to eq('hidden')
            expect(hiring_relationship.side_effects[:candidate_position_interests][candidate_position_interest.id]).to eq(candidate_position_interest)
        end
    end

    describe "candidate_position_interest_for_any_related_open_positions?" do
        attr_accessor :hiring_relationship, :candidate_position_interest

        before(:each) do
            self.hiring_relationship = hiring_relationships(:pending_hidden)

            position = OpenPosition.create!(
                hiring_manager_id: hiring_relationship.hiring_manager_id,
                title: "foo"
            )

            self.hiring_relationship.open_position_id = position.id
            self.candidate_position_interest = CandidatePositionInterest.create!({
                candidate_id: hiring_relationship.candidate_id,
                open_position_id: position.id,
                candidate_status: 'accepted',
                hiring_manager_status: 'accepted'
            })
        end

        it "should return true if associated candidate_position_interest" do
            expect(hiring_relationship.candidate_position_interest_for_any_related_open_positions?).to be(true)
        end

        it "should return true if interest for ANY open position created by this hiring manager" do
            candidate_position_interest.destroy

            another_position = OpenPosition.create!(
                hiring_manager_id: hiring_relationship.hiring_manager_id,
                title: "foo"
            )
            CandidatePositionInterest.create!({
                candidate_id: hiring_relationship.candidate_id,
                open_position_id: another_position.id,
                candidate_status: 'accepted',
                hiring_manager_status: 'accepted'
            })

            expect(hiring_relationship.candidate_position_interest_for_any_related_open_positions?).to be(true)
        end

        it "should return true if interest for ANY open position created by this hiring manager's team" do
            candidate_position_interest.destroy

            hiring_manager = users(:hiring_manager_with_team)
            teammate = users(:hiring_manager_teammate)

            teammates_position = OpenPosition.create!(
                hiring_manager_id: teammate.id,
                title: "foo"
            )
            CandidatePositionInterest.create!({
                candidate_id: hiring_relationship.candidate_id,
                open_position_id: teammates_position.id,
                candidate_status: 'accepted',
                hiring_manager_status: 'accepted'
            })

            hiring_relationship.update_column(:hiring_manager_id, hiring_manager.id)
            expect(hiring_relationship.candidate_position_interest_for_any_related_open_positions?).to be(true)
        end

        it "should return false if candidate_status != \'accepted\'" do
            candidate_position_interest.candidate_status = 'rejected'
            candidate_position_interest.save!
            expect(hiring_relationship.candidate_position_interest_for_any_related_open_positions?).to be(false)
        end

        it "should return false if no associated candidate_position_interest" do
            candidate_position_interest.destroy
            expect(hiring_relationship.candidate_position_interest_for_any_related_open_positions?).to be(false)
        end

    end

    describe "handle_matched_by_hiring_manager" do

        attr_accessor :hiring_relationship

        before(:each) do
            self.hiring_relationship = hiring_relationships(:pending_hidden)
        end

        it "should be called on before_save" do
            expect(hiring_relationship).to receive(:handle_matched_by_hiring_manager).exactly(1).times
            hiring_relationship.update(hiring_manager_status: 'accepted')
        end

        it "should set candidate_status to \'accepted\' and matched_by_hiring_manager to true" do
            allow(hiring_relationship).to receive(:candidate_position_interest_for_any_related_open_positions?).and_return(true)
            allow(hiring_relationship).to receive(:hiring_manager_status).and_return('accepted')
            hiring_relationship.handle_matched_by_hiring_manager
            expect(hiring_relationship.candidate_status).to eq('accepted')
            expect(hiring_relationship.matched_by_hiring_manager).to eq(true)
        end

        it "should not alter candidate_status or set matched_by_hiring_manager if hiring_manager_status != \'accepted\'" do
            allow(hiring_relationship).to receive(:hiring_manager_status).and_return('saved_for_later')
            hiring_relationship.handle_matched_by_hiring_manager
            expect(hiring_relationship.candidate_status).to eq('hidden')
            expect(hiring_relationship.matched_by_hiring_manager).to be(nil)
        end

        it "should not alter candidate_status or set matched_by_hiring_manager when !candidate_position_interest_for_any_related_open_positions?" do
            allow(hiring_relationship).to receive(:candidate_position_interest_for_any_related_open_positions?).and_return(false)
            allow(hiring_relationship).to receive(:hiring_manager_status).and_return('accepted')
            hiring_relationship.handle_matched_by_hiring_manager
            expect(hiring_relationship.candidate_status).to eq('hidden')
            expect(hiring_relationship.matched_by_hiring_manager).to be(nil)
        end

    end

    describe "hide_teammates_relationships" do
        it "should call hide_teammates_relationships before_save and after_commit" do
            hiring_relationship = hiring_relationships(:pending_hidden)
            expect(hiring_relationship).to receive(:hide_teammates_relationships).exactly(2).times
            hiring_relationship.update(hiring_manager_status: 'accepted')
        end

        it "should hide relationship" do
            hiring_manager = users(:hiring_manager_with_team)
            teammate = users(:hiring_manager_teammate)
            teammate.candidate_relationships.destroy_all

            hiring_manager_relationship = nil
            teammate_relationship = nil
            # The following needs to be wrapped in its own transaction, so the
            # after_commit hooks in hiring_relationship.rb will fire appropriately.
            ActiveRecord::Base.transaction do
                hiring_manager_relationship = HiringRelationship.create!(hiring_manager_id: hiring_manager.id, candidate_id: users(:learner).id, hiring_manager_status: 'pending')
                teammate_relationship = HiringRelationship.create!(hiring_manager_id: teammate.id, candidate_id: users(:learner).id, hiring_manager_status: 'pending')

                hiring_manager_relationship.update_attribute(:hiring_manager_status, 'accepted')
            end

            expect(hiring_manager_relationship.side_effects[:hiring_relationships][teammate_relationship.id]).to eq(teammate_relationship)
            expect(teammate.candidate_relationships.count).to eq(1)
            expect(teammate_relationship.hiring_manager_status).to eq('pending')
            expect(teammate_relationship.reload.hiring_manager_status).to eq('hidden')
        end
    end

    describe "ensure_associated_candidate_position_interest_invited_on_accepted" do
        attr_accessor :candidate_position_interest, :hiring_relationship

        before(:each) do
            @candidate_position_interest = CandidatePositionInterest.find_by_hiring_manager_status('unseen')
            @hiring_manager = candidate_position_interest.hiring_manager
            ensure_fresh_hiring_team(@hiring_manager)

            @hiring_relationship = HiringRelationship.create!(
                hiring_manager_id: @hiring_manager.id,
                candidate_id: @candidate_position_interest.candidate_id,
                hiring_manager_status: 'pending'
            )
        end

        it "should update hiring_manager_status associated_candidate_position_interest to invited" do
            hiring_relationship.update!(
                hiring_manager_status: 'accepted',
                open_position_id: candidate_position_interest.open_position_id
            );

            expect(candidate_position_interest.reload.hiring_manager_status).to eq('invited')
            expect(hiring_relationship.side_effects[:candidate_position_interests][candidate_position_interest.id]).to eq(candidate_position_interest)
        end

        it "should not update hiring_manager_status to invited for non-associated interests" do
            hiring_relationship.update!(
                hiring_manager_status: 'accepted'
                # intentionally leaving out open_position_id here
            );

            # see hide_candidate_position_interests_on_accepted_or_rejected for why this
            # should be hidden, and in side_effects
            expect(candidate_position_interest.reload.hiring_manager_status).to eq('hidden')
            expect(hiring_relationship.side_effects[:candidate_position_interests][candidate_position_interest.id]).to eq(candidate_position_interest)
        end

    end

    describe "hide_candidate_position_interests_on_accepted_or_rejected" do
        attr_accessor :candidate_position_interest, :hiring_relationship

        before(:each) do
            @candidate_position_interest = CandidatePositionInterest.find_by_hiring_manager_status('unseen')
            @hiring_manager = candidate_position_interest.hiring_manager
            ensure_fresh_hiring_team(@hiring_manager)

            @hiring_relationship = HiringRelationship.create!(
                hiring_manager_id: @hiring_manager.id,
                candidate_id: @candidate_position_interest.candidate_id,
                hiring_manager_status: 'pending'
            )
        end

        it "should hide non-associated candidate_position_interests when a new accepted relationship is created" do
            hiring_relationship.update!(
                hiring_manager_status: 'accepted'
                # intentionally leaving out open_position_id here
            );

            expect(candidate_position_interest.reload.hiring_manager_status).to eq('hidden')
            expect(hiring_relationship.side_effects[:candidate_position_interests][candidate_position_interest.id]).to eq(candidate_position_interest)
        end

        it "should not hide associated_candidate_position_interest" do
            hiring_relationship.update!(
                hiring_manager_status: 'accepted',
                open_position_id: candidate_position_interest.open_position_id
            );

            # see ensure_associated_candidate_position_interest_invited_on_accepted for why this
            # should be invited, and in side_effects
            expect(candidate_position_interest.reload.hiring_manager_status).to eq('invited')
            expect(hiring_relationship.side_effects[:candidate_position_interests][candidate_position_interest.id]).to eq(candidate_position_interest)
        end

    end

    describe "update_candidate_status" do
        before(:each) do
            CandidatePositionInterest.delete_all
        end

        it "should change candidate_status to pending if hiring_manager_status is changing to accepted from pending" do
            relationship = HiringRelationship.where(hiring_manager_status: "pending", candidate_status: "hidden").first
            expect(relationship).to receive(:update_candidate_status).and_call_original
            relationship.update!(hiring_manager_status: "accepted")
            expect(relationship.candidate_status).to eq("pending")
        end

        it "should change candidate_status to pending if hiring_manager_status is changing to accepted from rejected" do
            relationship = HiringRelationship.where(hiring_manager_status: "rejected", candidate_status: "hidden").first
            expect(relationship).to receive(:update_candidate_status).and_call_original
            relationship.update!(hiring_manager_status: "accepted")
            expect(relationship.candidate_status).to eq("pending")
        end

        it "should change candidate_status to pending if hiring_manager_status is changing to accepted from saved_for_later" do
            relationship = HiringRelationship.where(hiring_manager_status: "saved_for_later", candidate_status: "hidden").first
            expect(relationship).to receive(:update_candidate_status).and_call_original
            relationship.update!(hiring_manager_status: "accepted")
            expect(relationship.candidate_status).to eq("pending")
        end
    end

    describe "update" do
        before(:each) do
        end

        it "should unhide for user when set to accepted" do
            hiring_relationship = hiring_relationships(:pending_hidden)
            hiring_relationship.hiring_manager_status = 'accepted'
            hiring_relationship.save!
            expect(hiring_relationship.candidate_status).to eq('pending')
        end

        it "should not unhide for user when set to rejected" do
            hiring_relationship = hiring_relationships(:pending_hidden)
            hiring_relationship.hiring_manager_status = 'rejected'
            hiring_relationship.save!
            expect(hiring_relationship.candidate_status).to eq('hidden')
        end

        it "should unhide for user when set from rejected to accepted" do
            hiring_relationship = hiring_relationships(:pending_hidden)
            hiring_relationship.hiring_manager_status = 'rejected'
            hiring_relationship.save!
            expect(hiring_relationship.candidate_status).to eq('hidden')
            hiring_relationship.hiring_manager_status = 'accepted'
            hiring_relationship.save!
            expect(hiring_relationship.candidate_status).to eq('pending')
        end

        it "should set candidate side to hidden if hiring manager not yet accepted" do
            hiring_relationship = hiring_relationships(:pending_hidden)

            # Simulate not yet accepted
            hiring_relationship.hiring_manager.hiring_application.status = "pending"
            hiring_relationship.hiring_manager.hiring_application.save!

            hiring_relationship.hiring_manager_status = 'accepted'
            hiring_relationship.save!
            expect(hiring_relationship.reload.candidate_status).to eq('hidden')

            # If somehow we try to set the candidate side to pending while hiring manager is not accepted into careers
            hiring_relationship.hiring_manager_status = 'accepted'
            hiring_relationship.candidate_status = "pending"
            hiring_relationship.save!
            expect(hiring_relationship.reload.candidate_status).to eq('hidden')
        end

        # since the candidate may have already seen this, there is no going back
        it "should not be possible to set hiring_manager_status from accepted to pending" do
            hiring_relationship = hiring_relationships(:accepted_pending)
            hiring_relationship.hiring_manager_status = 'pending'
            expect(Proc.new {
                hiring_relationship.save!
            }).to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Hiring manager status Cannot be changed from accepted to pending")
        end

        # since the candidate may have already seen this, there is no going back
        it "should not be possible to set hiring_manager_status from accepted to rejected" do
            hiring_relationship = hiring_relationships(:accepted_pending)
            hiring_relationship.hiring_manager_status = 'rejected'
            expect(Proc.new {
                hiring_relationship.save!
            }).to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Hiring manager status Cannot be changed from accepted to rejected")
        end

        describe "closing relationship" do
            it "should log_to_slack when a hiring manager first closes a relationship if there is feedback" do
                hiring_relationship = hiring_relationships(:accepted_accepted)

                title = "#{hiring_relationship.hiring_manager.name} (#{hiring_relationship.hiring_manager.email}) left feedback for a closed connection"
                expect(LogToSlack).to receive(:perform_later).with(Slack.talent_notify_channel, title, anything)
                hiring_relationship.update({
                    'hiring_manager_closed' => true,
                    'hiring_manager_closed_info' => {
                        'reasons' => ['foo', 'bar'],
                        'feedback' => 'some feedback'
                    }
                })
            end

            it "should not log_to_slack on subsequent updates after the relationship is closed" do
                hiring_relationship = hiring_relationships(:accepted_accepted)

                hiring_relationship.update({
                    'hiring_manager_closed' => true,
                    'hiring_manager_closed_info' => {
                        'reasons' => ['foo', 'bar'],
                        'feedback' => 'some feedback'
                    }
                })

                expect(LogToSlack).not_to receive(:perform_later)
                hiring_relationship.update({
                    'hiring_manager_closed' => true,
                    'hiring_manager_closed_info' => {
                        'reasons' => ['foo', 'bar'],
                        'feedback' => 'some feedback'
                    }
                })
            end

            it "should not log_to_slack if there is no feedback" do
                hiring_relationship = hiring_relationships(:accepted_accepted)

                expect(LogToSlack).not_to receive(:perform_later)
                hiring_relationship.update({
                    'hiring_manager_closed' => true,
                    'hiring_manager_closed_info' => {
                        'reasons' => ['foo', 'bar']
                    }
                })
            end
        end
    end

    describe "notify_participants_if_just_accepted" do

        describe "when just_accepted_by_hiring_manager?" do
            attr_accessor :hiring_relationship

            before(:each) do
                @hiring_relationship = HiringRelationship.first
                allow(@hiring_relationship).to receive(:just_accepted_by_hiring_manager?).and_return(true)
            end

            it "should enqueue two RemindCandidateOfPendingHiringRelationship jobs; one to run 24 hours later and the other to run 72 hours later" do
                now = Time.now
                allow(Time).to receive(:now).and_return(now)

                expect(RemindCandidateOfPendingHiringRelationship).to receive(:set).with(wait_until: now + 24.hours).and_return(RemindCandidateOfPendingHiringRelationship)
                expect(RemindCandidateOfPendingHiringRelationship).to receive(:perform_later).with(hiring_relationship.id, 24) # should set num_hours_in_delay to 24

                expect(RemindCandidateOfPendingHiringRelationship).to receive(:set).with(wait_until: now + 72.hours).and_return(RemindCandidateOfPendingHiringRelationship)
                expect(RemindCandidateOfPendingHiringRelationship).to receive(:perform_later).with(hiring_relationship.id, 72) # should set num_hours_in_delay to 72

                hiring_relationship.notify_participants_if_just_accepted
            end
        end
    end

    describe "events" do
        attr_accessor :hiring_manager, :candidate, :hiring_relationship, :start, :num_log_to_external_systems_calls, :num_expected_log_to_external_systems_calls

        before(:each) do
            self.start = Time.now
            self.hiring_manager = users(:hiring_manager)
            self.candidate = User.where.not(id: hiring_manager.candidate_ids).first

            self.hiring_relationship = HiringRelationship.new(
                hiring_manager_id: hiring_manager.id,
                candidate_id: candidate.id
            )

            new_message = Mailboxer::Notification.new
            hiring_relationship.new_message = new_message
            hiring_relationship.save!

            self.num_log_to_external_systems_calls = 0

            allow_any_instance_of(Event).to receive(:log_to_external_systems) do
                self.num_log_to_external_systems_calls += 1
            end
        end

        it "should log events when created and updated" do
            new_message = Mailboxer::Notification.new
            hiring_relationship.new_message = new_message
            hiring_relationship.update('hiring_manager_status' => 'accepted')
            hiring_relationship.notify_participants_if_just_accepted

            # candidate updates a different object, so as not to confuse
            # the just_accepted stuff
            reloaded = HiringRelationship.find(hiring_relationship.id)
            reloaded.update('candidate_status' => 'accepted')
            reloaded.notify_participants_if_just_accepted

            created_events = Event.where("estimated_time > ?", start)
                                .reorder("estimated_time asc")
                                .to_a

            expect_events(created_events, "created", {
                "hiring_manager_status" => "pending",
                "candidate_status" => "hidden"
            })

            expect_candidate_event(created_events, "accepted", {
                "hiring_manager_status" => "accepted",
                "candidate_status" => "pending"}, true)
            expect_hiring_manager_event(created_events, "accepted", {
                "hiring_manager_status" => "accepted",
                "candidate_status" => "accepted",
                "new_message" => nil}, true)

            # check that there are no events we haven't asserted on above
            expect(created_events).to be_empty
            expect(num_log_to_external_systems_calls).to eq(2)
        end

        it "should log closed event when just updated to hiring_manager_closed" do
            hiring_relationship.update({
                'hiring_manager_status' => 'accepted',
                'candidate_status' => 'accepted'
            })

            closed_info = {
                'reasons' => ['foo', 'bar'],
                'feedback' => 'some feedback'
            }

            expect(hiring_relationship).to receive(:log_events).with("closed", {
                closed_by: "hiring_manager",
                hiring_manager_closed_info: closed_info,
                hiring_manager_company_name: hiring_relationship.hiring_manager.company_name,
                open_position_id: hiring_relationship.open_position_id
            }).and_call_original

            hiring_relationship.update({
                'hiring_manager_closed' => true,
                'hiring_manager_closed_info' => closed_info
            })

            expect(hiring_relationship).not_to receive(:log_events)

            hiring_relationship.update({
                'hiring_manager_closed' => true,
                'hiring_manager_closed_info' => closed_info
            })

            expect(num_log_to_external_systems_calls).to eq(2)
        end

        it "should include selection_details in created events" do
            hiring_relationship.destroy
            Event.destroy_all
            hiring_relationship = HiringRelationship.new(
                hiring_manager_id: hiring_manager.id,
                candidate_id: candidate.id
            )
            hiring_relationship.selection_details = {
                some: 'stuff',
                policy: 'some_policy',
                found_with_search_id: 'some_search_id'
            }
            hiring_relationship.save!

            created_events = Event.where("estimated_time > ?", start)
                                .reorder("estimated_time asc")
                                .to_a

            expect(created_events.map(&:payload).map { |p| p['selection_details'] }.uniq).to eq([hiring_relationship.selection_details.as_json])
            expect(created_events.map(&:payload).map { |p| p['selected_by_policy'] }.uniq).to eq(['some_policy'])
            expect(created_events.map(&:payload).map { |p| p['found_with_search_id'] }.uniq).to eq(['some_search_id'])

            expect(num_log_to_external_systems_calls).to eq(0)
        end

        def expect_hiring_manager_event(created_events, event_type, payload, logged_to_external_systems = false)
            event = created_events.shift
            expect(event.user_id).to eq(hiring_manager.id)
            expect(event.event_type).to eq("hiring_relationship:#{event_type}")
            expect(event.payload).to eq(
                {
                    'candidate_id' => candidate.id,
                    "hiring_relationship_id" => hiring_relationship.id,
                    "hiring_relationship_role" => "hiring_manager",
                    "hiring_manager_status" => hiring_relationship.hiring_manager_status,
                    "candidate_status" => hiring_relationship.candidate_status,
                    "matched_by_hiring_manager" => hiring_relationship.matched_by_hiring_manager,
                    "server_event" => true,
                    "hiring_manager_nickname" => hiring_manager.preferred_name,
                    "candidate_nickname" => candidate.preferred_name,
                    "new_message" => hiring_relationship.new_message.as_json
                }.merge(payload)
            )
        end

        def expect_candidate_event(created_events, event_type, payload, logged_to_external_systems = false)
            event = created_events.shift
            expect(event.user_id).to eq(candidate.id)
            expect(event.event_type).to eq("hiring_relationship:#{event_type}")
            expect(event.payload).to eq(
                {
                    'hiring_manager_id' => hiring_manager.id,
                    "hiring_manager_company_name" => hiring_manager.company_name,
                    "hiring_relationship_id" => hiring_relationship.id,
                    "hiring_relationship_role" => "candidate",
                    "matched_by_hiring_manager" => hiring_relationship.matched_by_hiring_manager,
                    "server_event" => true,
                    "hiring_manager_status" => hiring_relationship.hiring_manager_status,
                    "candidate_status" => hiring_relationship.candidate_status,
                    "hiring_manager_nickname" => hiring_manager.preferred_name,
                    "candidate_nickname" => candidate.preferred_name,
                    "new_message" => hiring_relationship.new_message.as_json
                }.merge(payload)
            )
        end

        def expect_events(created_events, event_type, payload)
            expect_hiring_manager_event(created_events, event_type, payload)
            expect_candidate_event(created_events, event_type, payload)
        end
    end

    describe "matching_employment_type_count" do

        it "should work" do
            hiring_relationship = get_relationship_with_profiles

            hiring_relationship.hiring_application.position_descriptors = []
            hiring_relationship.career_profile.employment_types_of_interest = []
            expect(hiring_relationship.matching_employment_type_count).to eq(0)

            hiring_relationship.hiring_application.position_descriptors = ['a']
            hiring_relationship.career_profile.employment_types_of_interest = ['b']
            expect(hiring_relationship.matching_employment_type_count).to eq(0)

            hiring_relationship.hiring_application.position_descriptors = ['a']
            hiring_relationship.career_profile.employment_types_of_interest = ['a', 'b']
            expect(hiring_relationship.matching_employment_type_count).to eq(1)

            hiring_relationship.hiring_application.position_descriptors = ['a', 'b', 'c']
            hiring_relationship.career_profile.employment_types_of_interest = ['b', 'c', 'd']
            expect(hiring_relationship.matching_employment_type_count).to eq(2)
        end

    end

    describe "matching_industries_count" do

        it "should work" do
            hiring_relationship = get_relationship_with_profiles

            hiring_relationship.hiring_application.industry = 'a'
            hiring_relationship.career_profile.job_sectors_of_interest = ['b']
            expect(hiring_relationship.matching_industries_count).to eq(0)

            hiring_relationship.hiring_application.industry = 'a'
            hiring_relationship.career_profile.job_sectors_of_interest = ['a', 'b']
            expect(hiring_relationship.matching_industries_count).to eq(1)
        end

    end

    describe "matching_roles_count" do

        it "should work" do
            hiring_relationship = get_relationship_with_profiles

            hiring_relationship.hiring_application.role_descriptors = []
            hiring_relationship.career_profile.primary_areas_of_interest = []
            expect(hiring_relationship.matching_roles_count).to eq(0)

            hiring_relationship.hiring_application.role_descriptors = ['a']
            hiring_relationship.career_profile.primary_areas_of_interest = ['b']
            expect(hiring_relationship.matching_roles_count).to eq(0)

            hiring_relationship.hiring_application.role_descriptors = ['a']
            hiring_relationship.career_profile.primary_areas_of_interest = ['a', 'b']
            expect(hiring_relationship.matching_roles_count).to eq(1)

            hiring_relationship.hiring_application.role_descriptors = ['a', 'b', 'c']
            hiring_relationship.career_profile.primary_areas_of_interest = ['b', 'c', 'd']
            expect(hiring_relationship.matching_roles_count).to eq(2)
        end

    end

    describe "location_state_match_descriptor" do

        it "should work" do
            hiring_relationship = get_relationship_with_profiles

            hiring_relationship.hiring_application.place_details = {'administrative_area_level_1' => 'Virginia'}
            hiring_relationship.career_profile.place_details = {'administrative_area_level_1' => 'Ohio'}
            hiring_relationship.career_profile.willing_to_relocate = false
            hiring_relationship.career_profile.open_to_remote_work = false

            expect(hiring_relationship.location_state_match_descriptor).to eq('incompatible')

            hiring_relationship.career_profile.open_to_remote_work = true
            expect(hiring_relationship.location_state_match_descriptor).to eq('open_to_remote_work')

            hiring_relationship.career_profile.open_to_remote_work = false
            hiring_relationship.career_profile.willing_to_relocate = true
            expect(hiring_relationship.location_state_match_descriptor).to eq('willing_to_relocate')

            hiring_relationship.career_profile.place_details = {'administrative_area_level_1' => 'Virginia'}
            expect(hiring_relationship.location_state_match_descriptor).to eq('same_location')

        end

    end

    describe "just_accepted?" do

        it "should work when the hiring_manager accepts" do
            hiring_relationship = hiring_relationships(:pending_hidden)
            expect(hiring_relationship.just_accepted?).to be(false)
            hiring_relationship.hiring_manager_status = 'accepted'
            expect(hiring_relationship.just_accepted?).to be(true)
            hiring_relationship.save!
            expect(hiring_relationship.just_accepted?).to be(true)
        end

        it "should work when the candidate accepts" do
            hiring_relationship = hiring_relationships(:accepted_pending)
            expect(hiring_relationship.just_accepted?).to be(false)
            hiring_relationship.candidate_status = 'accepted'
            expect(hiring_relationship.just_accepted?).to be(true)
            hiring_relationship.save!
            expect(hiring_relationship.just_accepted?).to be(true)
        end

    end

    describe "as_json" do
        it "should send back the hiring_manager_closed_info when is_admin" do
            hiring_relationship = hiring_relationships(:accepted_accepted)
            hiring_relationship.hiring_manager_closed = true
            hiring_relationship.hiring_manager_closed_info = {
                "reasons" => ["foo", "bar"],
                "feedback" => "sample feedback"
            }
            expect(hiring_relationship.as_json(is_admin: true)["hiring_manager_closed_info"]).to eq({
                "reasons" => ["foo", "bar"],
                "feedback" => "sample feedback"
            })
        end

        it "should respect fields" do
            hiring_relationship = hiring_relationships(:accepted_accepted)
            expect(hiring_relationship.as_json(fields: [:id]).keys).to eq(["id"])
        end

        it "should respect except" do
            hiring_relationship = hiring_relationships(:accepted_accepted)
            expect(hiring_relationship.as_json(except: [:id]).keys).not_to include("id")
        end

        it "should not as_json the career_profile if not needed" do
            hiring_relationship = hiring_relationships(:accepted_accepted)
            expect(hiring_relationship).not_to receive(:career_profile)
            expect(hiring_relationship.as_json(except: [:career_profile]).keys).not_to include("career_profile")
        end

        it "should not as_json the hiring_application if not needed" do
            hiring_relationship = hiring_relationships(:accepted_accepted)
            expect(hiring_relationship).not_to receive(:hiring_application)
            expect(hiring_relationship.as_json(except: [:hiring_application]).keys).not_to include("hiring_application")
        end

        it "should not as_json the conversation if not needed" do
            hiring_relationship = hiring_relationships(:accepted_accepted)
            expect(hiring_relationship).not_to receive(:conversation)
            expect(hiring_relationship.as_json(except: [:conversation]).keys).not_to include("conversation")
        end

        it "should include conversation and pass through options" do
            hiring_relationship = hiring_relationships(:accepted_accepted)
            options = {a: 'b'}
            expect(hiring_relationship.conversation).to receive(:as_json).with(options).and_return('conversation_json')
            expect(hiring_relationship.as_json(options)['conversation']).to eq('conversation_json')
        end

    end

    describe "matched_at" do

        it "should be updated on save" do
            hiring_relationship = hiring_relationships(:pending_hidden)
            hiring_relationship.update_attribute(:hiring_manager_status, 'accepted')
            expect(hiring_relationship.matched_at).to be_nil

            allow(Time).to receive(:now).and_return(Time.now)
            hiring_relationship.update_attribute(:candidate_status, 'accepted')
            expect(hiring_relationship.matched_at).to eq(Time.now)

            # this never happens, but if they get unmatched somehow, then change it back
            hiring_relationship.update_attribute(:candidate_status, 'pending')
            expect(hiring_relationship.matched_at).to be_nil

        end

    end

    describe "HiringRelationshipRelationExtensions" do
        describe "matched" do
            it "should work" do
                expect(HiringRelationship.where(id: hiring_relationships(:pending_hidden).id).matched).to be_empty
                expect(HiringRelationship.where(id: hiring_relationships(:accepted_pending).id).matched).to be_empty
                expect(HiringRelationship.where(id: hiring_relationships(:accepted_accepted).id).matched).not_to be_empty
            end
        end

        describe "not_closed" do
            it "should work" do
                # check with one that is not closed
                hiring_relationship = hiring_relationships(:accepted_accepted)
                expect(HiringRelationship.where(id: hiring_relationship.id).not_closed).not_to be_empty

                # check closed by hiring manager
                hiring_relationship.update(:hiring_manager_closed => true)
                expect(HiringRelationship.where(id: hiring_relationship.id).not_closed).to be_empty

                # check closed by candidate
                hiring_relationship.update(:hiring_manager_closed => false, :candidate_closed => true)
                expect(HiringRelationship.where(id: hiring_relationship.id).not_closed).to be_empty

                # check null
                hiring_relationship.update(:hiring_manager_closed => nil, :candidate_closed => nil)
                expect(HiringRelationship.where(id: hiring_relationship.id).not_closed).not_to be_empty
            end
        end
    end

    describe "matched?" do
        it "should work" do
            expect(hiring_relationships(:pending_hidden).matched?).to be(false)
            expect(hiring_relationships(:accepted_pending).matched?).to be(false)
            expect(hiring_relationships(:accepted_accepted).matched?).to be(true)
        end
    end

    describe "viewable_by_hiring_manager" do
        it "should work" do
            result = HiringRelationship.viewable_by_hiring_manager
            expect(result).not_to be_empty
        end
    end

    describe "last_activity_at" do
        it "should be the max of updated_at and last_message_at if there is a conversation" do
            hiring_relationship = hiring_relationships(:accepted_accepted)
            expect(hiring_relationship.conversation).to receive(:last_message_at).and_return(Time.now).at_least(1).times
            expect(hiring_relationship.last_activity_at).to eq(hiring_relationship.conversation.last_message_at)
            hiring_relationship.updated_at = Time.now
            expect(hiring_relationship.last_activity_at).to eq(hiring_relationship.updated_at)
        end

        it "should be updated_at if there is no conversation" do
            hiring_relationship = hiring_relationships(:pending_hidden)
            expect(hiring_relationship.conversation).to be_nil
            expect(hiring_relationship.last_activity_at).to eq(hiring_relationship.updated_at)
        end
    end

    describe "days_since_matched" do
        it "should work" do
            time_zone = 'Eastern Time (US & Canada)'
            allow(Time).to receive(:now).and_return(ActiveSupport::TimeZone[time_zone].parse('2017/08/31 11:00').utc)
            hiring_relationship = HiringRelationship.first

            hiring_relationship.matched_at = ActiveSupport::TimeZone[time_zone].parse('2017/08/31 10:00')
            expect(hiring_relationship.days_since_matched(time_zone)).to eq(0)

            hiring_relationship.matched_at = ActiveSupport::TimeZone[time_zone].parse('2017/08/30 23:59')
            expect(hiring_relationship.days_since_matched(time_zone)).to eq(1)

            hiring_relationship.matched_at = ActiveSupport::TimeZone[time_zone].parse('2017/08/30 00:01')
            expect(hiring_relationship.days_since_matched(time_zone)).to eq(1)

            hiring_relationship.matched_at = ActiveSupport::TimeZone[time_zone].parse('2017/08/29 00:01')
            expect(hiring_relationship.days_since_matched(time_zone)).to eq(2)

        end
    end

    describe "hiring_team" do
        it "should delegate to hiring_manager" do
            hiring_relationship = HiringRelationship.first
            hiring_manager = hiring_relationship.hiring_manager
            hiring_manager.ensure_hiring_team!
            expect(hiring_relationship.hiring_team).to be(hiring_manager.hiring_team)
        end
    end

    describe "just_accepted_and_now_matched?" do
        it "should be true when both just_accepted? and matched?" do
            hiring_relationship = HiringRelationship.first

            allow(hiring_relationship).to receive(:just_accepted?).and_return(true)
            allow(hiring_relationship).to receive(:matched?).and_return(true)

            expect(hiring_relationship.just_accepted_and_now_matched?).to be(true)
        end

        it "should be true false when just_accepted? is false" do
            hiring_relationship = HiringRelationship.first

            allow(hiring_relationship).to receive(:just_accepted?).and_return(false)
            allow(hiring_relationship).to receive(:matched?).and_return(true)
            expect(hiring_relationship.just_accepted_and_now_matched?).to be(false)

            allow(hiring_relationship).to receive(:just_accepted?).and_return(true)
            allow(hiring_relationship).to receive(:matched?).and_return(false)
            expect(hiring_relationship.just_accepted_and_now_matched?).to be(false)
        end
    end

    describe "after_match" do

        attr_accessor :hiring_relationship

        before(:each) do
            @hiring_relationship = hiring_relationships(:pending_hidden)
            hiring_relationship.hiring_manager.ensure_hiring_team!
            hiring_relationship.hiring_team.update(subscription_required: true)
            hiring_relationship.hiring_team.update(owner_id: hiring_relationship.hiring_manager.id)
            create_subscription(hiring_relationship.hiring_team)
        end

        it "should queue an IncrementMatchUsageJob if stripe_usage_record_info is nil" do
            expect(IncrementMatchUsageJob).to receive(:perform_later).with(hiring_relationship.id)
            hiring_relationship.send(:after_match)
        end

        it "should not queue an IncrementMatchUsageJob if stripe_usage_record_info is already set" do
            hiring_relationship.update(stripe_usage_record_info: { id: "some_id" })
            expect(IncrementMatchUsageJob).not_to receive(:perform_later)
            hiring_relationship.send(:after_match)
        end

        it "should not queue an IncrementMatchUsageJob if subscription_required is false" do
            hiring_relationship.hiring_team.update(subscription_required: false)
            expect(IncrementMatchUsageJob).not_to receive(:perform_later)
            hiring_relationship.send(:after_match)
        end

        it "should not queue an IncrementMatchUsageJob if there is no primary_subscription" do
            hiring_relationship.hiring_team.update(subscription_required: true)
            hiring_relationship.hiring_team.subscriptions = []
            expect(IncrementMatchUsageJob).not_to receive(:perform_later)
            hiring_relationship.send(:after_match)
        end

        it "should be called on before_commit if just_accepted_and_now_matched?" do
            expect(hiring_relationship.stripe_usage_record_info).to be_nil
            expect(hiring_relationship).to receive(:after_match).once
            hiring_relationship.update_attribute(:hiring_manager_status, 'accepted')
            hiring_relationship.update_attribute(:candidate_status, 'accepted')
        end
    end


    def get_relationship_with_profiles
        users(:hiring_manager).candidate_relationships.detect { |r| r.candidate.career_profile.present? }
    end

    def ensure_fresh_hiring_team(hiring_manager)
        if hiring_manager.hiring_team
            hiring_manager.hiring_team.owner_id = nil
            hiring_manager.hiring_team.save!
        end
        hiring_manager.hiring_team = HiringTeam.create!({domain: 'freshteam.com', title: "Fresh Team"})
        hiring_manager.save!
    end
end
