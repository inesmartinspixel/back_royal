require 'spec_helper'

describe RefreshMaterializedInternalReportsViews do

    it "should include ScheduledJob" do
        expect(RefreshMaterializedInternalReportsViews.new).to be_a(ScheduledJob)
    end

    describe "::perform_later_with_debounce" do

        before(:each) do
            Delayed::Job.delete_all
        end

        describe "when a job is currently running and no pending jobs exists" do

            before(:each) do
                RefreshMaterializedInternalReportsViews.perform_later
                jobs = Delayed::Job.where(queue: 'refresh_materialized_internal_reports_views')
                expect(jobs.size).to eq(1) # sanity check
                jobs.first.update(locked_at: Time.now)
            end

            it "should enqueue a new job to run twenty minutes from now even if run_at is passed in" do
                now = Time.now
                allow(Time).to receive(:now).and_return(now)
                expect(RefreshMaterializedInternalReportsViews).to receive(:set).with(wait_until: now + 20.minutes).and_call_original

                RefreshMaterializedInternalReportsViews.perform_later_with_debounce(now + 5.minutes)

                jobs = Delayed::Job.where(queue: 'refresh_materialized_internal_reports_views').to_a
                expect(jobs.size).to eq(2) # 1 locked and now 1 pending

                pending_job = jobs.find { |job| job.locked_at.nil? }
                expect(pending_job.run_at.to_timestamp).to eq((now + 20.minutes).to_timestamp)
            end
        end

        describe "when pending job exists" do
            attr_accessor :now, :pending_job

            before(:each) do
                @now = Time.now
                allow(Time).to receive(:now).and_return(now)
                RefreshMaterializedInternalReportsViews.set(wait_until: now + 8.hours).perform_later
                jobs = Delayed::Job.where(queue: 'refresh_materialized_internal_reports_views')
                expect(jobs.size).to eq(1) # sanity check
                @pending_job = jobs.first
                expect(pending_job.locked_at).to be_nil
            end

            it "should update run_at on pending job to be five minutes from now if no job is currently running and no run_at is passed in" do
                currently_running_jobs = Delayed::Job.where(queue: 'refresh_materialized_internal_reports_views').where.not(locked_at: nil)
                expect(currently_running_jobs.size).to eq(0)
                expect(pending_job.run_at).to be > now + 5.minutes

                RefreshMaterializedInternalReportsViews.perform_later_with_debounce

                expect(pending_job.reload.run_at.to_timestamp).to eq((now + 5.minutes).to_timestamp)
            end

            it "should update run_at on pending job to the passed in run_at if no job is currently running" do
                currently_running_jobs = Delayed::Job.where(queue: 'refresh_materialized_internal_reports_views').where.not(locked_at: nil)
                expect(currently_running_jobs.size).to eq(0)
                expect(pending_job.run_at).to be > now + 10.minutes

                RefreshMaterializedInternalReportsViews.perform_later_with_debounce(now + 10.minutes)

                expect(pending_job.reload.run_at.to_timestamp).to eq((now + 10.minutes).to_timestamp)
            end

            it "should update run_at on pending job to be twenty minutes from now if job is currently running even if run_at is passed in" do
                # create a job and mock it to be currently running
                RefreshMaterializedInternalReportsViews.perform_later
                existing_jobs = Delayed::Job.where(queue: 'refresh_materialized_internal_reports_views').where.not(id: pending_job.id)
                expect(existing_jobs.size).to eq(1)
                currently_running_job = existing_jobs.first
                currently_running_job.update_column(:locked_at, now)

                expect(pending_job.run_at).to be > now + 20.minutes

                RefreshMaterializedInternalReportsViews.perform_later_with_debounce(now + 5.minutes)

                expect(pending_job.reload.run_at.to_timestamp).to eq((now + 20.minutes).to_timestamp)
            end

            it "should destroy any extra pending jobs" do
                RefreshMaterializedInternalReportsViews.set(wait_until: now + 9.hours).perform_later # scheduled to run after other pending job
                jobs = Delayed::Job.where(queue: 'refresh_materialized_internal_reports_views').reorder(:run_at)

                expect(jobs.size).to eq(2)
                jobs.each { |job| expect(job.locked_at).to be_nil }

                RefreshMaterializedInternalReportsViews.perform_later_with_debounce

                expect(jobs.reload.size).to eq(1)
            end
        end
    end
end
