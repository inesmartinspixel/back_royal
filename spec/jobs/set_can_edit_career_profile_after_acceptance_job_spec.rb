require 'spec_helper'

describe SetCanEditCareerProfileAfterAcceptanceJob do
    attr_accessor :instance

    fixtures :users, :cohorts

    before(:each) do
        @accepted_user = User.joins(:cohort_applications => :cohort).where("cohorts.program_type" => 'mba', "cohort_applications.status" => 'accepted', "users.can_edit_career_profile" => false).first
        @pending_user = User.joins(:cohort_applications => :cohort).where("cohorts.program_type" => 'mba', "cohort_applications.status" => 'pending', "users.can_edit_career_profile" => false).first
        @instance = SetCanEditCareerProfileAfterAcceptanceJob.new
    end

    describe "perform" do

        it "should update can_edit_career_profile for mba/emba user if application has been accepted" do
            expect_any_instance_of(User).to receive(:save!).and_call_original
            @instance.perform(@accepted_user.id)

            @accepted_user.reload
            expect(@accepted_user.can_edit_career_profile).to be(true)
        end

        it "should not update can_edit_career_profile for mba/emba user if application has not been accepted" do
            expect_any_instance_of(User).not_to receive(:save!)
            @instance.perform(@pending_user.id)

            @pending_user.reload
            expect(@pending_user.can_edit_career_profile).to be(false)
        end

    end

end
