require 'spec_helper'

describe DeleteFromCustomerIoJob do
    attr_accessor :user, :instance

    fixtures(:users)

    before(:each) do
        self.user = users(:learner)
        self.instance = DeleteFromCustomerIoJob.new
    end

    describe "perform" do

        it "should call customerio delete" do
            expect($customerio).to receive(:delete).with(user.id)
            instance.perform(user.id)
        end

    end

end
