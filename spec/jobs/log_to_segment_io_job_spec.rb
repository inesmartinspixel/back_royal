require 'spec_helper'

describe LogToSegmentIoJob do

    fixtures(:users)

    describe "track" do

        before(:each) do
            @user = users(:learner)

            @event = Event.new({
                id: SecureRandom.uuid,
                event_type: 'type',
                user_id: @user.id,
                payload: {
                    a: {b: 1},
                    c: 2
                }
            })
            expect(Event).to receive(:find).with(@event.id).and_return(@event)
        end

        it "should log to segment with flattening" do
            expect(Analytics).to receive(:track).with({
                :user_id=>@event.user_id,
                :event=>@event.event_type,
                :timestamp=>@event.estimated_time,
                :properties=>{
                    'a.b' => 1,
                    'c' => 2
                }
            }.with_indifferent_access)
            LogToSegmentIoJob.perform_now(@event.id)
        end

        it "should log specific keys to segment if provided" do
            expect(Analytics).to receive(:track).with({
                :user_id=>@event.user_id,
                :event=>@event.event_type,
                :timestamp=>@event.estimated_time,
                :properties=>{'c' => 2}
            }.with_indifferent_access)
            LogToSegmentIoJob.perform_now(@event.id, ['c'])
        end

    end

    it "should do nothing if no user is found from event.user_id" do
        event = Event.new({
            event_type: 'type',
            user_id: SecureRandom.uuid,
            payload: {
                a: {b: 1},
                c: 2
            }
        })
        expect(Event).to receive(:find).and_return(event)
        expect(Analytics).not_to receive(:track)
        LogToSegmentIoJob.perform_now(event.id)
    end

end