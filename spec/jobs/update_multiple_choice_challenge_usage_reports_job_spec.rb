require 'spec_helper'

describe UpdateMultipleChoiceChallengeUsageReportsJob do

    describe "perform" do
        attr_reader :instance, :frame_id, :lesson_id, :challenge_id, :answer_id, :identifiers,
        :original_last_answer_at

        before(:each) do
            # setup an instance of the job
            @instance = UpdateMultipleChoiceChallengeUsageReportsJob.new

            # create a hash of identifiers that represent possible values
            # for the unique index on MultipleChoiceChallengeUsageReport
            @frame_id = SecureRandom.uuid
            @challenge_id = SecureRandom.uuid
            @lesson_id = SecureRandom.uuid
            @answer_id = SecureRandom.uuid
            @identifiers = {
                lesson_id: self.lesson_id,
                frame_id: self.frame_id,
                challenge_id: self.challenge_id,
                answer_id: self.answer_id,
                editor_template:'editor_template'
            }.stringify_keys

            # Set things up in the DB as though we have already processed
            # events up to 2017/06/01
            @original_last_answer_at = Time.parse('2017/06/01 12:00 UTC')
            Lesson::MultipleChoiceChallengeUsageReport.delete_all
            Lesson::MultipleChoiceChallengeUsageReport.create!(
                lesson_id: SecureRandom.uuid,
                frame_id: SecureRandom.uuid,
                challenge_id: SecureRandom.uuid,
                answer_id: SecureRandom.uuid,
                editor_template:'editor_template',
                count: 1,
                last_answer_at: original_last_answer_at
            )

            # Mock out now so that it is now 23 hours later, and we need to process
            # new events for those last 23 hours
            allow(@instance).to receive(:now).and_return(original_last_answer_at + 23.hours)
        end

        it "should create a new record if there is none for this answer" do

            # Mock out the redshift query to return data for a new answer
            # (See comment near get_increments in code for info about why we mock this out)
            last_answer_at = expect_one_call_to_get_increments_returns_count(identifiers, 3)

            # Since there is no record in MultipleChoiceChallengeUsageReport for the specific
            # answer that comes back, we expect a new one to be created
            expect {
                instance.perform
            }.to change {
                Lesson::MultipleChoiceChallengeUsageReport.count
            }.by(1)

            new_record = Lesson::MultipleChoiceChallengeUsageReport.where(identifiers).first
            expect(new_record.created_at).to be_within(1.second).of(Time.now)
            expect(new_record.updated_at).to be_within(1.second).of(Time.now)
            expect(new_record.count).to eq(3)
            expect(new_record.last_answer_at).to eq(last_answer_at)

        end

        it "should increment an existing record" do
            existing_record = Lesson::MultipleChoiceChallengeUsageReport.first


            # Mock out the redshift query to return data for the existing answer
            # (See comment near get_increments in code for info about why we mock this out)
            existing_record_identifiers = existing_record.attributes.slice('lesson_id', 'frame_id', 'challenge_id', 'answer_id', 'editor_template')
            last_answer_at = expect_one_call_to_get_increments_returns_count(existing_record_identifiers, 3)

            # Since the record is already in the db, no new records should
            # be created
            expect {
                instance.perform
            }.not_to change {
                Lesson::MultipleChoiceChallengeUsageReport.count
            }

            reloaded = Lesson::MultipleChoiceChallengeUsageReport.find(existing_record.id)
            expect(reloaded.created_at).to eq(existing_record.created_at)
            expect(reloaded.updated_at).to be_within(1.second).of(Time.now)
            expect(reloaded.count).to eq(existing_record.count + 3)
            expect(reloaded.last_answer_at).to eq(last_answer_at)
        end

        it "should load up batches of updates one day at a time" do
            # we have counted up to original_last_answer_at, and
            # now it is 3 days later,
            # so we will need 3 batches to bring us up to current
            allow(@instance).to receive(:now).and_return(original_last_answer_at + 72.hours)

            calls = []
            expect(@instance).to receive(:get_increments).exactly(3).times do |start_time, finish_time|
                calls << [start_time, finish_time]
                []
            end

            instance.perform

            int = UpdateMultipleChoiceChallengeUsageReportsJob::BATCH_INTERVAL
            expect(calls).to eq([
                [original_last_answer_at, original_last_answer_at + 1 * int],
                [original_last_answer_at + 1 * int, original_last_answer_at + 2 * int],
                [original_last_answer_at + 2 * int, original_last_answer_at + 3 * int],
            ])
        end

        def expect_one_call_to_get_increments_returns_count(identifiers, count)
            # When we query redshift, we get back information that `count` users
            # gave the same answer to a particular challenge the first time
            # they saw it.  The last answer we find is from 4 hours after the previous
            # last_answer_at. (See note near #get_increments about why we have to mock out this call)
            last_answer_at = original_last_answer_at + 4.hours
            expect(@instance).to receive(:get_increments).exactly(1)
                .times
                .with(original_last_answer_at, original_last_answer_at + UpdateMultipleChoiceChallengeUsageReportsJob::BATCH_INTERVAL)
                .and_return([identifiers.merge(
                    'ct' => 3,
                    'last_answer_at' => last_answer_at
                )])

                last_answer_at
        end
    end
end