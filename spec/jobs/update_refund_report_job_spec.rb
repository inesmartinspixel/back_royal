require 'spec_helper'

describe UpdateRefundReportJob do

    describe "perform" do
        attr_reader :registered_application

        before(:each) do
            @registered_application = CohortApplication.joins(:cohort).where(
                cohorts: {
                    program_type: Cohort::ProgramTypeConfig.configs.select(&:supports_payments?).map(&:program_type),
                },
                status: 'accepted'
            ).first
            allow(@registered_application).to receive(:validate_stripe_details)
            @registered_application.registered = true
            @registered_application.save!
        end

        it "should save a record when one does not exist" do
            expect(UserRefundEntitlement.count).to be(0)
            allow_any_instance_of(CohortApplication).to receive(:refund_details).and_return({refund_amount: 42})
            UpdateRefundReportJob.perform_now
            record = UserRefundEntitlement.find_by_user_id(registered_application.user_id)
            expect(record).not_to be_nil
            expect(record.refund_amount).to eq(42)
        end

        it "should work when refund_details is nil" do
            expect(UserRefundEntitlement.count).to be(0)
            allow_any_instance_of(CohortApplication).to receive(:refund_details).and_return(nil)
            UpdateRefundReportJob.perform_now
            record = UserRefundEntitlement.find_by_user_id(registered_application.user_id)
            expect(record).not_to be_nil
            expect(record.refund_amount).to eq(nil)
        end

        it "should update an existing record if the cohort_application has been updated" do
            UpdateRefundReportJob.perform_now
            record = UserRefundEntitlement.find_by_user_id(registered_application.user_id)
            allow_any_instance_of(CohortApplication).to receive(:refund_details).and_return({refund_amount: 42})
            registered_application.update(updated_at: Time.now)
            expect {
                UpdateRefundReportJob.perform_now
            }.to change { record.reload.refund_amount }
        end

        it "should update an existing record if the user has made progress" do
            UpdateRefundReportJob.perform_now
            record = UserRefundEntitlement.find_by_user_id(registered_application.user_id)
            @refund_amount = 42
            allow_any_instance_of(CohortApplication).to receive(:refund_details) do
                {refund_amount: @refund_amount}
            end

            # it should get updated when the user_progress_record is created
            user_lesson_progress_record = Report::UserLessonProgressRecord.create!(
                user_id: registered_application.user_id,
                locale_pack_id: SecureRandom.uuid,
                started_at: Time.now,
                last_lesson_activity_time: Time.now - 10.minutes  )
            RefreshMaterializedInternalReportsViews.perform_now
            expect {
                UpdateRefundReportJob.perform_now
            }.to change { record.reload.refund_amount }

            # it should get updated when the user_progress_record is updated
            @refund_amount = 84
            ActiveRecord::Base.connection.execute("update user_lesson_progress_records set last_lesson_activity_time = now() where user_id='#{user_lesson_progress_record.user_id}' and locale_pack_id = '#{user_lesson_progress_record.locale_pack_id}'")
            RefreshMaterializedInternalReportsViews.perform_now
            expect {
                UpdateRefundReportJob.perform_now
            }.to change { record.reload.refund_amount }
        end

        it "should delete a record that is no longer relevant" do
            UpdateRefundReportJob.perform_now
            record = UserRefundEntitlement.find_by_user_id(registered_application.user_id)

            # since mba does not support payments, this application is no longer
            # relevant
            registered_application.cohort = Cohort.find_by_program_type('mba')
            registered_application.save!
            UpdateRefundReportJob.perform_now
            expect(UserRefundEntitlement.find_by_id(record.id)).to be_nil
        end

    end

end
