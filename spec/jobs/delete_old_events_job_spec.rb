require 'spec_helper'

describe DeleteOldEventsJob do
    attr_accessor :now

    before(:each) do
        @now = Time.now
        allow(Time).to receive(:now).and_return(now)
    end

    describe "delete_old_events" do

        before(:each) do
            FactoryBot.create(:event, {
                created_at: Time.now - 5.days
            })

            FactoryBot.create(:redshift_event, {
                event_type: 'click',
                created_at: Time.now - 5.days
            })

            FactoryBot.create(:redshift_event, {
                event_type: 'error',
                created_at: Time.now - 5.days
            })
        end

        it "should delete old events from postgres" do
            instance = DeleteOldEventsJob.new
            expect(instance).to receive(:days_to_keep_events_in_postgres).and_return(1).at_least(1).times
            expect(Event.where('created_at < ?', now - 1.day)).not_to be_empty
            instance.send(:perform)
            expect(Event.where('created_at < ?', now - 1.day)).to be_empty
        end

    end

end