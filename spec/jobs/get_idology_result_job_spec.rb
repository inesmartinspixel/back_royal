require 'spec_helper'

describe GetIdologyResultJob do

    describe "ensure_job" do

        before(:each) do
            @idology_id_number = 12345
            @delay = 30.seconds
        end

        it "should not create a new job if one exists" do
            GetIdologyResultJob.perform_later(@idology_id_number)
            job = call_ensure_job_and_return_new_job
            expect(job).to be_nil
        end

        it "should create a new job if none exists" do
            GetIdologyResultJob.perform_later('some_other_idology_id_number')
            job = call_ensure_job_and_return_new_job
            expect(job).not_to be_nil
            expect(job.payload_object.job_data['arguments']).to eq([@idology_id_number])
            expect(job.run_at).to be_within(1.second).of(Time.now + 30.seconds)
        end

        def call_ensure_job_and_return_new_job
            start = Time.now
            GetIdologyResultJob.ensure_job(@idology_id_number, @delay)
            Delayed::Job.where(queue: GetIdologyResultJob.queue_name).where("created_at > ?", start).first
        end


    end
end