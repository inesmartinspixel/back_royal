require 'spec_helper'

describe DeleteFromSegmentJob do

    attr_accessor :user, :instance

    fixtures(:users)

    before(:each) do
        ENV['SEGMENT_DELETION_ACCESS_TOKEN'] = '1234567890'
        ENV['SEGMENT_WORKSPACE_SLUG'] = 'workspace-slug'
        ENV['SEGMENT_SOURCE_SLUG'] = 'source-slug'

        self.user = users(:learner)
        self.instance = DeleteFromSegmentJob.new
    end

    after(:each) do
        ENV['SEGMENT_DELETION_ACCESS_TOKEN'] = nil
        ENV['SEGMENT_WORKSPACE_SLUG'] = nil
        ENV['SEGMENT_SOURCE_SLUG'] = nil
    end

    describe "perform" do

        it "should set instance.user_id" do
            instance.perform(user.id)
            expect(instance.user_id).to eq(user.id)
        end

        it "should call suppress_and_delete_user" do
            expect(instance).to receive(:suppress_and_delete_user).exactly(1).times
            instance.perform(user.id)
        end

    end

    describe "suppress_and_delete_user" do

        attr_accessor :endpoint, :payload

        before(:each) do
            instance.user_id = user.id
            self.endpoint = "https://platform.segmentapis.com/v1beta/workspaces/workspace-slug/sources/source-slug/regulations"
            self.payload = {
                headers: {
                    'Authorization' => "Bearer 1234567890",
                    'Content-Type' => 'application/json'
                },
                body: {
                    "regulation_type" => "Suppress_With_Delete",
                    "attributes" => {
                        "name" => "userId",
                        "values" => [
                            user.id
                        ]
                    }
                }.to_json
            }
        end

        it "should issue POST request to Segment REST API" do
            mock_response = double('HTTParty')
            expect(mock_response).to receive(:success?).and_return(true)
            expect(HTTParty).to receive(:post).with(endpoint, payload).and_return(mock_response)
            instance.suppress_and_delete_user
        end

        it "should raise DeleteFromSegmentJob::SuppressionError if rate limited " do
            mock_response = {"error"=>"Too many concurrent requests. Please try again later"}
            expect(mock_response).to receive(:success?).and_return(false)
            expect(HTTParty).to receive(:post).with(endpoint, payload).and_return(mock_response)
            expect {
                instance.suppress_and_delete_user
            }.to raise_error(DeleteFromSegmentJob::SuppressionError)
        end

        it "should raise  DeleteFromSegmentJob::SuppressionError if upstream timeout" do
            mock_response = {"error"=>"upstream request timeout"}
            expect(mock_response).to receive(:success?).and_return(false)
            expect(HTTParty).to receive(:post).with(endpoint, payload).and_return(mock_response)
            expect {
                instance.suppress_and_delete_user
            }.to raise_error(DeleteFromSegmentJob::SuppressionError)
        end

        it "should capture_exception if not due to rate limiting" do
            mock_response = {"error"=>"foobar"}
            expect(mock_response).to receive(:success?).and_return(false)
            expect(HTTParty).to receive(:post).with(endpoint, payload).and_return(mock_response)
            expect(Raven).to receive(:capture_exception).with(
                "Unable to suppress and delete user!",
                {
                    extra: {
                        user_id: user.id,
                        errors: "\"foobar\""
                    }
                }
            )
            expect {
                instance.suppress_and_delete_user
            }.not_to raise_error
        end

    end

end
