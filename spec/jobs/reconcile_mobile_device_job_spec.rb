require 'spec_helper'

describe ReconcileMobileDeviceJob do

    fixtures(:users)

    before(:each) do
        @user = User.first
    end

    describe "perform" do

        before(:each) do
            @instance = ReconcileMobileDeviceJob.new
            expect(@instance).to receive(:perform).and_call_original
            allow(@instance).to receive(:create_or_update_mobile_device).and_return({})
            allow(@instance).to receive(:add_all_mobile_devices).and_return({})
            allow(@instance).to receive(:delete_all_mobile_devices).and_return({})
        end

        describe "with device_token and platform" do

            it "should call create_or_update_mobile_device" do
                expect(@instance).to receive(:create_or_update_mobile_device)
                @instance.perform(@user.id, 'some_token', 'android')
            end

        end

        describe "when user.pref_allow_push_notifications is true" do

            it "should call add_all_mobile_devices" do
                @user.update_column(:pref_allow_push_notifications, true)
                expect(@instance).to receive(:add_all_mobile_devices)
                expect(@instance).not_to receive(:delete_all_mobile_devices)
                @instance.perform(@user.id)
            end

        end

        describe "when user.pref_allow_push_notifications is fasle" do

            it "should call delete_all_mobile_devices" do
                @user.update_column(:pref_allow_push_notifications, false)
                expect(@instance).not_to receive(:add_all_mobile_devices)
                expect(@instance).to receive(:delete_all_mobile_devices)
                @instance.perform(@user.id)
            end

        end

        it "should do nothing if no user is found from event.user_id" do
            expect(@instance).not_to receive(:create_or_update_mobile_device)
            expect(@instance).not_to receive(:add_all_mobile_devices)
            expect(@instance).not_to receive(:delete_all_mobile_devices)
            @instance.perform(SecureRandom.uuid)
        end

    end

    describe "create_or_update_mobile_device" do

        before(:each) do
            @instance = ReconcileMobileDeviceJob.new
            @instance.user_id = @user.id
            @instance.device_token = 'some_token'
            @instance.platform = 'android'

            MobileDevice.delete_all # sanity check
        end

        it "should create a mobile_devices record in back_royal" do
            expect(MobileDevice.where(user_id: @user.id, device_token: 'some_token').to_a.first).to be(nil)
            @instance.create_or_update_mobile_device
            expect(MobileDevice.where(user_id: @user.id, device_token: 'some_token').to_a.first).not_to be(nil)
        end

        it "should update a mobile_devices record in back_royal" do
            ts = Time.now
            allow(Time).to receive(:now).and_return(ts)

            MobileDevice.create!({
                user_id: @user.id,
                device_token: 'some_token',
                platform: 'android',
                last_used_at: ts - 1.day
            })

            @instance.create_or_update_mobile_device

            mobile_device = MobileDevice.where(user_id: @user.id, device_token: 'some_token').to_a.first
            expect(mobile_device).not_to be(nil)
            expect(mobile_device.last_used_at).to be_within(1.second).of(ts)
        end

    end

    describe "add_all_mobile_devices" do

        before(:each) do
            @instance = ReconcileMobileDeviceJob.new
            @instance.user_id = @user.id
            @user.update_column(:pref_allow_push_notifications, true)

            allow($customerio).to receive(:add_device).and_return({})

            # just to make sure we don't have fixtures or anything that could mess with
            # the below tests
            MobileDevice.delete_all
            (1..3).map do |i|
                MobileDevice.create!({
                    user_id: @user.id,
                    device_token: "some_token_#{i}",
                    platform: 'android',
                    last_used_at: Time.now
                })
            end
        end

        it "should add devices to customer.io" do
            expect($customerio).to receive(:add_device).exactly(3).times
            @instance.add_all_mobile_devices
        end

    end

    describe "delete_all_mobile_devices" do

        before(:each) do
            @instance = ReconcileMobileDeviceJob.new
            @instance.user_id = @user.id
            @user.update_column(:pref_allow_push_notifications, false)

            allow($customerio).to receive(:delete_device).and_return({})
            allow(HTTParty).to receive(:delete).and_return({})

            # just to make sure we don't have fixtures or anything that could mess with
            # the below tests
            MobileDevice.delete_all
            (1..3).map do |i|
                MobileDevice.create!({
                    user_id: @user.id,
                    device_token: SecureRandom.uuid,
                    platform: 'android',
                    last_used_at: Time.now
                })
            end
        end

        describe "$customerio" do

            it "should delete devices from customer.io" do
                expect($customerio).to receive(:delete_device).exactly(3).times
                @instance.delete_all_mobile_devices
            end

        end

    end

end