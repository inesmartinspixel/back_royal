require 'spec_helper'

describe DeleteAssociatedUserDataJob do
    attr_accessor :user, :instance

    fixtures(:users)

    before(:each) do
        self.user = users(:user_with_career_profile)
        self.instance = DeleteAssociatedUserDataJob.new

        mock_job = Class.new(Delayed::Job) do
            def self.perform_later(user_id, career_profile_id); return 'foobar'; end
        end
        allow_any_instance_of(Delayed::Job).to receive(:new).and_return(mock_job)
    end

    describe "perform" do

        it "should queue other deletion jobs" do
            # Just to mock out the Airtable data aggregation
            allow(CohortApplication::Version).to receive(:where).and_return([])

            expect(DeleteFromAirtableJob).to receive(:perform_later).with([]).exactly(1).times

            now = Time.now
            allow(Time).to receive(:now).and_return(now)
            expect(DeleteFromRedshiftJob).to receive(:set).with(wait_until: now + 6.hours).exactly(1).times.and_return(DeleteFromRedshiftJob)
            expect(DeleteFromRedshiftJob).to receive(:perform_later).with(user.id).exactly(1).times

            expect(DeleteFromCustomerIoJob).to receive(:perform_later).with(user.id).exactly(1).times
            expect(DeleteFromSegmentJob).to receive(:perform_later).with(user.id).exactly(1).times
            expect(DeleteFromBackRoyalJob).to receive(:perform_later).with(user.id, user.career_profile.id).exactly(1).times
            instance.perform(user.id, user.career_profile.id)
        end

    end

    describe "Airtable" do
        attr_accessor :cohort_application

        before(:each) do
            CohortApplication.delete_all

            # Imitate an application being created and synced with our live Airtable base
            cohort_application = CohortApplication.new(
                id: SecureRandom.uuid,
                user_id: user.id,
                cohort_id: Cohort.first.id,
                applied_at: Time.now,
                status: 'pending',
                airtable_record_id: '12345',
                airtable_base_key: 'AIRTABLE_BASE_KEY_LIVE'
            )
            cohort_application.save!

            # Imitate that application being archived
            cohort_application.airtable_base_key = 'AIRTABLE_BASE_KEY_ARCHIVE'
            cohort_application.save!
            self.cohort_application = cohort_application
        end

        describe "With staging and local environment variables" do

            before(:each) do
                ENV['AIRTABLE_BASE_KEY_STAGING'] = 'airtable_key_staging'
                ENV['AIRTABLE_BASE_KEY_LOCAL'] = 'airtable_key_local'
            end

            it "should send live, archive, staging, and local values to DeleteFromAirtableJob" do
                attrs = [
                    cohort_application.attributes.slice('id', 'airtable_record_id').merge({'airtable_base_key' => 'AIRTABLE_BASE_KEY_LIVE'}),
                    cohort_application.attributes.slice('id', 'airtable_record_id').merge({'airtable_base_key' => 'AIRTABLE_BASE_KEY_ARCHIVE'}),
                    cohort_application.attributes.slice('id', 'airtable_record_id').merge({'airtable_base_key' => 'AIRTABLE_BASE_KEY_STAGING'}),
                    cohort_application.attributes.slice('id', 'airtable_record_id').merge({'airtable_base_key' => 'AIRTABLE_BASE_KEY_LOCAL'})
                ]

                args = nil
                allow(DeleteFromAirtableJob).to receive(:perform_later) { |arg| args = arg }

                instance.perform(user.id)

                expect(args).to match_array(attrs)
            end

            after(:each) do
                ENV['AIRTABLE_BASE_KEY_STAGING'] = nil
                ENV['AIRTABLE_BASE_KEY_LOCAL'] = nil
            end

        end

        describe "Without staging and local environment variables" do

            before(:each) do
                ENV['AIRTABLE_BASE_KEY_STAGING'] = nil
                ENV['AIRTABLE_BASE_KEY_LOCAL'] = nil
            end

            it "should not include staging and local values" do
                attrs = [
                    cohort_application.attributes.slice('id', 'airtable_record_id').merge({'airtable_base_key' => 'AIRTABLE_BASE_KEY_LIVE'}),
                    cohort_application.attributes.slice('id', 'airtable_record_id').merge({'airtable_base_key' => 'AIRTABLE_BASE_KEY_ARCHIVE'})
                ]

                args = nil
                allow(DeleteFromAirtableJob).to receive(:perform_later) { |arg| args = arg }

                instance.perform(user.id)

                expect(args).to match_array(attrs)
            end

        end

    end

end
