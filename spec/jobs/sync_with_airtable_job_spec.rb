require 'spec_helper'

describe SyncWithAirtableJob do
    attr_accessor :application, :instance

    fixtures :users

    before(:each) do
        @application = CohortApplication.first
        @instance = SyncWithAirtableJob.new
    end

    describe "self.generate_fix_decision_action" do
        it "should work" do
            expect(SyncWithAirtableJob.generate_fix_decision_action("Foo Decision")).to eq("fix_decision::Foo Decision")
        end

        it "should work with nil" do
            expect(SyncWithAirtableJob.generate_fix_decision_action(nil)).to eq("fix_decision")
        end
    end

    describe "::DEFAULT_PRIORITY" do
        it "should be 3" do
            expect(SyncWithAirtableJob::DEFAULT_PRIORITY).to eq(3)
        end
    end

    describe "::MASS_SYNC_PRIORITY" do
        it "should be numerically greater than DEFAULT_PRIORITY" do
            expect(SyncWithAirtableJob::MASS_SYNC_PRIORITY).to be > SyncWithAirtableJob::DEFAULT_PRIORITY
        end
    end

    describe "self.sanitize_airtable_mentions" do

        it "should remove any <airtable:mention></airtable:mention> tags and the associated @ symbol" do
            test_string_1 = "<airtable:mention id=\"men2xck840Gaz7ePl\">@Johnny Appleseed</airtable:mention> foo bar baz."
            expected_sanitized_test_string_1 = "Johnny Appleseed foo bar baz."
            test_string_2 = "<airtable:mention id=\"men2xck840Gaz7ePl\">@Johnny Appleseed</airtable:mention> foo bar baz <airtable:mention id=\"men137xjey1D87QP\">@John Smith</airtable:mention>."
            expected_sanitized_test_string_2 = "Johnny Appleseed foo bar baz John Smith."
            test_string_3 = "foo bar <airtable:mention id=\"men2xck840Gaz7ePl\">@Johnny Appleseed</airtable:mention> baz <airtable:mention id=\"men137xjey1D87QP\">@John Smith</airtable:mention>."
            expected_sanitized_test_string_3 = "foo bar Johnny Appleseed baz John Smith."
            test_string_4 = "foo bar <airtable:mention id=\"men2xck840Gaz7ePl\">@Johnny Appleseed</airtable:mention> baz <airtable:mention id=\"men137xjey1D87QP\">@John Smith</airtable:mention> qux."
            expected_sanitized_test_string_4 = "foo bar Johnny Appleseed baz John Smith qux."
            expect(SyncWithAirtableJob.sanitize_airtable_mentions(test_string_1)).to eq(expected_sanitized_test_string_1)
            expect(SyncWithAirtableJob.sanitize_airtable_mentions(test_string_2)).to eq(expected_sanitized_test_string_2)
            expect(SyncWithAirtableJob.sanitize_airtable_mentions(test_string_3)).to eq(expected_sanitized_test_string_3)
            expect(SyncWithAirtableJob.sanitize_airtable_mentions(test_string_4)).to eq(expected_sanitized_test_string_4)
        end

        it "should handle nil" do
            expect(SyncWithAirtableJob.sanitize_airtable_mentions(nil)).to be_nil
        end
    end

    describe "self.perform_later_with_debounce" do
        before(:each) do
            ActiveRecord::Base.connection.execute("TRUNCATE delayed_jobs RESTART IDENTITY")
            @application_job_query = Delayed::Job
                .where(queue: "sync_with_airtable")
                .where("handler ILIKE '\%arguments\%#{@application.id}\%#{SyncWithAirtableJob::UPDATE}\%'")
        end

        it "should update the run_at for a pending job and set the priority to DEFAULT_PRIORITY (even if new priority is given) instead of creating a new job" do
            SyncWithAirtableJob.perform_later(@application.id, SyncWithAirtableJob::UPDATE)

            job = @application_job_query.first
            job.update_attribute(:priority, SyncWithAirtableJob::DEFAULT_PRIORITY + 1) # ensure priority is not DEFAULT_PRIORITY

            mock_time = Time.now
            allow(Time).to receive(:now).and_return(mock_time)
            SyncWithAirtableJob.perform_later_with_debounce(@application.id, SyncWithAirtableJob::UPDATE, SyncWithAirtableJob::DEFAULT_PRIORITY + 2)

            expect(@application_job_query.count).to be(1)
            expect(job.reload.run_at).to be_within(1.second).of(mock_time + 1.minute)
            expect(job.reload.priority).to eq(SyncWithAirtableJob::DEFAULT_PRIORITY)
        end

        it "should create a new job for the future if no pending job due to being locked" do
            SyncWithAirtableJob.perform_later(@application.id, SyncWithAirtableJob::UPDATE)

            job = @application_job_query.first
            job.update(locked_at: Time.now) # job is not pending if it is currently running

            SyncWithAirtableJob.perform_later_with_debounce(@application.id, SyncWithAirtableJob::UPDATE)
            expect(@application_job_query.count).to eq(2) # expect a new job to be created since the prior one is now running
        end

        it "should create a new job for the future if no pending job due to being failed" do
            SyncWithAirtableJob.perform_later(@application.id, SyncWithAirtableJob::UPDATE)

            job = @application_job_query.first
            job.update(failed_at: Time.now) # job is not pending if it is currently running

            SyncWithAirtableJob.perform_later_with_debounce(@application.id, SyncWithAirtableJob::UPDATE)
            expect(@application_job_query.count).to eq(2) # expect a new job to be created since the prior one is now running
        end

        it "should create a new job for the future if no pending job due to different arguments" do
            different_application = CohortApplication.where.not(id: @application.id).first
            SyncWithAirtableJob.perform_later(different_application.id, SyncWithAirtableJob::UPDATE)

            SyncWithAirtableJob.perform_later_with_debounce(@application.id, SyncWithAirtableJob::UPDATE)
            expect(@application_job_query.count).to eq(1)
            expect(Delayed::Job.where(queue: "sync_with_airtable").count).to eq(2)
        end

        it "should respect priority param (when there is no pending job)" do
            SyncWithAirtableJob.perform_later_with_debounce(@application.id, SyncWithAirtableJob::UPDATE, 4) # priority given
            job = @application_job_query.last # new job is created, so it's the last one
            expect(job.priority).to eq(4)
        end

        it "should set the priority to DEFAULT_PRIORITY if no priority is given" do
            SyncWithAirtableJob.perform_later_with_debounce(@application.id, SyncWithAirtableJob::UPDATE) # no priority given
            job = @application_job_query.last # new job is created, so it's the last one
            expect(job.priority).to eq(SyncWithAirtableJob::DEFAULT_PRIORITY)
        end
    end

    describe "perform" do

        describe "destroy" do

            it "should destroy an application in Airtable" do
                application_id = application.id
                application.update_column(:airtable_record_id, "foo-record-id")
                application.destroy!
                expect(Airtable::Application).to receive(:destroy).with(CohortApplication::Version.where(id: application_id).order(version_created_at: :desc).first)
                instance.perform(application_id, SyncWithAirtableJob::DESTROY)
            end

            it "should short-circuit if a version is not found" do
                application_id = application.id
                application.update_column(:airtable_record_id, "foo-record-id")
                application.destroy!
                CohortApplication::Version.where(id: application_id).delete_all
                expect(Airtable::Application).not_to receive(:destroy)
                instance.perform(application_id, SyncWithAirtableJob::DESTROY)
            end

        end

        describe "create" do
            it "should create an application in Airtable" do
                expect(Airtable::Application).to receive(:find_or_create).with(application, 'SyncWithAirtableJob').and_return(OpenStruct.new({:id => "123"}))
                instance.perform(application.id, SyncWithAirtableJob::CREATE)
            end

            it "should set airtable_base_key to live with EditorTracking" do
                expect(Airtable::Application).to receive(:find_or_create).with(application, 'SyncWithAirtableJob').and_return(OpenStruct.new({:id => "123"}))
                application.update_column(:airtable_base_key, "foo")
                expect(EditorTracking).to receive(:transaction) do |user, &block|
                    expect(user).to eq('SyncWithAirtableJob')
                    expect(application.airtable_base_key).to eq("foo")
                    block.call
                    expect(application.reload.airtable_base_key).to eq("AIRTABLE_BASE_KEY_LIVE")
                end
                instance.perform(application.id, SyncWithAirtableJob::CREATE)
            end

            it "should short-circuit if the record is not found on our side" do
                expect(CohortApplication).to receive(:find_by_id).and_return(nil)
                expect(Airtable::Application).not_to receive(:find_or_create)
                expect_any_instance_of(CohortApplication).not_to receive(:update_column)
                instance.perform(application.id, SyncWithAirtableJob::CREATE)
            end
        end

        describe "update" do
            it "should update an application in Airtable" do
                # This is cool: https://relishapp.com/rspec/rspec-mocks/v/3-2/docs/configuring-responses/block-implementation#yield-to-the-caller's-block
                expect_any_instance_of(CohortApplication).to receive(:check_if_json_changed) { |&block| block.call }.and_return(:unchanged)
                expect(Airtable::Application).to receive(:update).with(an_instance_of(CohortApplication)).and_return({}) # fake a non-nil Airtable response
                instance.perform(application.id, SyncWithAirtableJob::UPDATE)
            end

            it "should throw a ForceRetry if record is not in Airtable and the record is still present in the database" do
                expect_any_instance_of(CohortApplication).to receive(:check_if_json_changed) { |&block| block.call }.and_return(:unchanged)
                expect(Airtable::Application).to receive(:update).with(an_instance_of(CohortApplication)).and_return(nil)

                expect{
                    instance.perform(application.id, SyncWithAirtableJob::UPDATE)
                }.to raise_error(Delayed::ForceRetry)
            end

            it "should NOT throw a ForceRetry if record is not in Airtable and the record is not in the database (it was destroyed so another job will handle that)" do
                expect_any_instance_of(CohortApplication).to receive(:check_if_json_changed) { |&block| block.call }.and_return(:destroyed)
                expect(Airtable::Application).to receive(:update).with(an_instance_of(CohortApplication)).and_return(nil)

                expect{
                    instance.perform(application.id, SyncWithAirtableJob::UPDATE)
                }.not_to raise_error
            end

            it "should throw a ForceRetry if record has been changed in the database after the job updated Airtable" do
                expect_any_instance_of(CohortApplication).to receive(:check_if_json_changed) { |&block| block.call }.and_return(:changed)
                expect(Airtable::Application).to receive(:update).with(an_instance_of(CohortApplication)).and_return({}) # fake a non-nil Airtable response

                expect{
                    instance.perform(application.id, SyncWithAirtableJob::UPDATE)
                }.to raise_error(Delayed::ForceRetry)
            end

            it "should short-circuit if the record is not found on our side" do
                expect(CohortApplication).to receive(:find_by_id).and_return(nil)
                expect_any_instance_of(CohortApplication).not_to receive(:check_if_json_changed)
                expect(Airtable::Application).not_to receive(:update)
                instance.perform(application.id, SyncWithAirtableJob::UPDATE)
            end

            it "should short-circuit if the application has been archived" do
                expect_any_instance_of(CohortApplication).to receive(:is_archived_in_airtable?).and_return(true)
                expect(Airtable::Application).not_to receive(:update)
                instance.perform(application.id, SyncWithAirtableJob::UPDATE)
            end
        end

        describe "fix_decision action" do
            it "should call fix_decision" do
                expect(Airtable::Application).to receive(:fix_decision).with(application, "Foo Decision")
                instance.perform(application.id, SyncWithAirtableJob.generate_fix_decision_action("Foo Decision"))
            end

            it "should handle decision reset" do
                expect(Airtable::Application).to receive(:fix_decision).with(application, nil)
                instance.perform(application.id, SyncWithAirtableJob.generate_fix_decision_action(nil))
            end

            it "should short-circuit if the record is not found on our side" do
                expect(CohortApplication).to receive(:find_by_id).and_return(nil)
                expect(Airtable::Application).not_to receive(:fix_decision).with(application)
                instance.perform(application.id, SyncWithAirtableJob.generate_fix_decision_action("Foo Decision"))
            end
        end
    end

    describe "maybe_transfer_prior_airtable_fields" do
        attr_accessor :a2a_rubric_fields, :db_synced_rubric_fields, :all_rubric_fields,
            :user, :emba_cohorts, :prior_application, :current_application

        def setup_two_applications(prior_applied_at: Time.now - 5.months)
            @prior_application = CohortApplication.create!({
                user_id: user.id,
                cohort_id: emba_cohorts.first.id,
                status: 'deferred',
                applied_at: prior_applied_at,
                rubric_interview: 'Conducted',
                airtable_base_key: 'AIRTABLE_BASE_KEY_FOO'
            })

            @current_application = CohortApplication.create!({
                user_id: user.id,
                cohort_id: emba_cohorts.second.id,
                status: 'pending',
                applied_at: Time.now,
                airtable_record_id: 1337
            })
        end

        before(:each) do
            @a2a_rubric_fields = [
                'Interview Date',
                'Motivation Score',
                'Motivation Score Reason',
                'Contribution Score',
                'Contribution Score Reason',
                'Insightfulness Score',
                'Insightfulness Score Reason',
                'Interviewer-Director Notes',
                'High Potential Applicant [emba/mba]',
                'Tagger-Interviewer Notes'
            ]
            @db_synced_rubric_fields = Airtable::Application.from_airtable_column_map.values - [
                "Round 1 Tag",
                "Round 1 Reason - MBA Invite",
                "Round 1 Reason - MBA Reject",
                "Round 1 Reason - EMBA Invite",
                "Round 1 Reason - EMBA Reject",
                "Final Decision",
                "Final Decision Reason"
            ]

            @all_rubric_fields = a2a_rubric_fields + db_synced_rubric_fields

            @user = User.joins(:career_profile).first
            user.cohort_applications.delete_all
            user.career_profile.peer_recommendations.delete_all

            @emba_cohorts = Cohort.all_published.where(program_type: 'emba').limit(2)
            expect(@emba_cohorts.size).to be(2) # sanity check
        end

        # This is just for staging. We'd always expect there to be an airtable record,
        # whether in live or an archive base, for a data application in prod.
        it "should not transfer fields if the prior application lookup fails" do
            setup_two_applications
            expect(instance).to receive(:transfer_airtable_fields).with(current_application, prior_application, anything, all_rubric_fields, true).and_return(false)
            instance.send(:maybe_transfer_prior_airtable_fields, current_application, OpenStruct.new)
            expect(current_application.rubric_inherited).to be(false)
        end

        it "should not transfer fields and set inherited flag when only one application" do
            current_application = CohortApplication.create!({
                user_id: user.id,
                cohort_id: emba_cohorts.second.id,
                status: 'pending',
                applied_at: Time.now,
                airtable_record_id: 1337
            })
            expect(instance).not_to receive(:transfer_airtable_fields)
            rubric_inherited = instance.send(:maybe_transfer_prior_airtable_fields, current_application, OpenStruct.new)
            expect(current_application.rubric_inherited).to be(false)
        end

        it "should not transfer fields and set inherited flag when reapplying after six months" do
            setup_two_applications(prior_applied_at: Time.now - 7.months)
            expect(instance).not_to receive(:transfer_airtable_fields)
            rubric_inherited = instance.send(:maybe_transfer_prior_airtable_fields, current_application, OpenStruct.new)
            expect(current_application.rubric_inherited).to be(false)
        end

        it "should transfer fields and set inherited flag when reapplying within six months" do
            setup_two_applications
            expect(instance).to receive(:transfer_airtable_fields).with(current_application, prior_application, anything, all_rubric_fields, true).and_return(true)
            instance.send(:maybe_transfer_prior_airtable_fields, current_application, OpenStruct.new)
            expect(current_application.rubric_inherited).to be(true)
        end

        it "should transfer fields and set inherited flag when reapplying within six months to mba or emba when previously the other" do
            setup_two_applications
            current_application.update_columns(cohort_id: Cohort.promoted_cohort('mba')['id'])
            current_application.reload
            expect(current_application.cohort.program_type).to eq('mba') # sanity check
            expect(instance).to receive(:transfer_airtable_fields).with(current_application, prior_application, anything, all_rubric_fields, true).and_return(true)
            instance.send(:maybe_transfer_prior_airtable_fields, current_application, OpenStruct.new)
            expect(current_application.rubric_inherited).to be(true)
        end

        it "should not transfer fields and set inherited flag if only inherited applications within six months" do
            setup_two_applications
            another_emba_cohort = prior_application.cohort.dup
            another_emba_cohort.start_date = another_emba_cohort.start_date - 1.months
            another_emba_cohort.publish!

            even_more_prior_application = CohortApplication.create!({
                user_id: user.id,
                cohort_id: another_emba_cohort.id,
                status: 'deferred',
                applied_at: Time.now - 7.months,
                rubric_interview: 'Conducted',
                airtable_base_key: 'AIRTABLE_BASE_KEY_FOO'
            })

            prior_application.update_columns(rubric_inherited: true)

            # Should not inherit in this case because while there is a prior application
            # with rubric_interview = 'Conducted', it was inherited from another prior application
            # that is now outside of the six month window
            expect(instance).not_to receive(:transfer_airtable_fields)
            instance.send(:maybe_transfer_prior_airtable_fields, current_application, OpenStruct.new)
            expect(current_application.rubric_inherited).to be(false)
        end

        it "should transfer Peer Recommendation field too if database shows some were requested" do
            setup_two_applications
            PeerRecommendation.create!(
                career_profile_id: user.career_profile.id,
                email: 'john.doe@gmail.com',
                contacted: true
            )

            expect(instance).to receive(:transfer_airtable_fields).with(current_application, prior_application, anything, all_rubric_fields + ['Peer Recommendations'], true).and_return(true)
            instance.send(:maybe_transfer_prior_airtable_fields, current_application, OpenStruct.new)
            expect(current_application.rubric_inherited).to be(true)
        end

        it "should transfer Peer Recommendation field even if interview not conducted on the prior application" do
            setup_two_applications

            prior_application.update_columns(rubric_interview: nil)
            PeerRecommendation.create!(
                career_profile_id: user.career_profile.id,
                email: 'john.doe@gmail.com',
                contacted: true
            )

            expect(instance).to receive(:transfer_airtable_fields).with(current_application, prior_application, anything, ['Peer Recommendations'], false).and_return(true)
            instance.send(:maybe_transfer_prior_airtable_fields, current_application, OpenStruct.new)
        end

        it "should also transfer the database fields if rubric is being inherited" do
            setup_two_applications

            # Some arbitrary database rubric fields to test later for being transferred
            prior_application.update_columns(rubric_interview_scheduled: true, rubric_interview_recommendation: 'foo', rubric_reason_for_declining: ['bar', 'baz'])

            expect(instance).to receive(:transfer_airtable_fields).with(current_application, prior_application, anything, all_rubric_fields, true).and_return(true)
            instance.send(:maybe_transfer_prior_airtable_fields, current_application, OpenStruct.new)
            expect(current_application.rubric_inherited).to be(true)
            expect(current_application.rubric_interview_scheduled).to be(true)
            expect(current_application.rubric_interview_recommendation).to eq('foo')
            expect(current_application.rubric_reason_for_declining).to match_array(['bar', 'baz'])
        end

        it "should transfer the correct fields when redeciding on a biz cert" do
            now = Time.now
            @prior_application = CohortApplication.create!({
                user_id: user.id,
                cohort_id: Cohort.promoted_cohort('the_business_certificate')['id'],
                status: 'rejected',
                applied_at: now - 1.months,
                rubric_interview: 'Conducted'
            })

            @current_application = CohortApplication.create!({
                user_id: user.id,
                cohort_id: emba_cohorts.first.id,
                status: 'pending',
                applied_at: now,
            })

            expected_fields_to_transfer = all_rubric_fields + [
                'Rubric - Employment',
                'Rubric - Role / Title Score',
                'Rubric Company Score Override',
                'Rubric - Years of Experience Override'
            ]

            expect(instance).to receive(:transfer_airtable_fields).with(current_application, prior_application, anything, expected_fields_to_transfer, true).and_return(true)
            instance.send(:maybe_transfer_prior_airtable_fields, current_application, OpenStruct.new)
        end
    end

    describe "transfer_airtable_fields" do

        it "should return false if prior application has no airtable_base_key" do
            mock_current_airtable_record = double('Airtable::Application')
            mock_prior_application = double('CohortApplication')
            expect(mock_prior_application).to receive(:airtable_base_key).and_return(nil)
            expect(instance.send(:transfer_airtable_fields, application, mock_prior_application, mock_current_airtable_record, [], false)).to be(false)
        end

        it "should sanitize_airtable_mentions for String fields" do
            mock_current_airtable_record = {}
            mock_airtable_prior_application_record = double('Airtable::Application')
            mock_prior_application = double('CohortApplication')
            expect(mock_prior_application).to receive(:airtable_base_key).at_least(:once).and_return('some_airtable_base_key')
            expect(Airtable::Application).to receive(:klass_for_base_key).with('some_airtable_base_key').and_return(Airtable::Application)
            expect(Airtable::Application).to receive(:find_by_id_with_fallback).with(mock_prior_application).and_return(mock_airtable_prior_application_record)

            unsanitized_string = "Some unsanitized string"
            sanitized_string = "Some sanitized string"
            another_unsanitized_string = "Another unsanitized string"
            another_sanitized_string = "Another sanitized string"

            expect(mock_airtable_prior_application_record).to receive(:[]).with('Motivation Score').and_return(100)
            expect(SyncWithAirtableJob).not_to receive(:sanitize_airtable_mentions).with(100)

            expect(mock_airtable_prior_application_record).to receive(:[]).with('Interviewer-Director Notes').and_return(unsanitized_string)
            expect(SyncWithAirtableJob).to receive(:sanitize_airtable_mentions).with(unsanitized_string).and_return(sanitized_string)

            expect(mock_airtable_prior_application_record).to receive(:[]).with('Tagger-Interviewer Notes').and_return(another_unsanitized_string)
            expect(SyncWithAirtableJob).to receive(:sanitize_airtable_mentions).with(another_unsanitized_string).and_return(another_sanitized_string)

            expect(mock_current_airtable_record).to receive(:save)

            instance.send(:transfer_airtable_fields, application, mock_prior_application, mock_current_airtable_record, ['Motivation Score', 'Interviewer-Director Notes', 'Tagger-Interviewer Notes'], false)

            expect(mock_current_airtable_record['Motivation Score']).to eq(100)
            expect(mock_current_airtable_record['Interviewer-Director Notes']).to eq(sanitized_string)
            expect(mock_current_airtable_record['Tagger-Interviewer Notes']).to eq(another_sanitized_string)
        end
    end
end