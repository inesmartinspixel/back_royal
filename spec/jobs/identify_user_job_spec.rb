require 'spec_helper'

describe IdentifyUserJob do
    attr_accessor :user, :instance

    fixtures :users

    before(:each) do
        self.user = User.first
        self.instance = IdentifyUserJob.new
    end

    describe "::DEFAULT_PRIORITY" do

        it "should be 1" do
            expect(IdentifyUserJob::DEFAULT_PRIORITY).to eq(1)
        end
    end

    describe "::MASS_IDENTIFY_PRIORITY" do

        it "should be numerically greater than DEFAULT_PRIORITY" do
            expect(IdentifyUserJob::MASS_IDENTIFY_PRIORITY).to be > IdentifyUserJob::DEFAULT_PRIORITY
        end
    end

    describe "self.perform_later_with_debounce" do
        before(:each) do
            ActiveRecord::Base.connection.execute("TRUNCATE delayed_jobs RESTART IDENTITY")
            @job_query = Delayed::Job
                .where(queue: "identify_user")
                .where("handler ILIKE '\%arguments\%#{user.id}\%'")
        end

        describe "when pending_job" do

            it "should update the run_at for a pending job and set the priority to the min prioirity instead of creating a new job" do
                IdentifyUserJob.perform_later(user.id)

                job = @job_query.first
                job.update_attribute(:priority, IdentifyUserJob::DEFAULT_PRIORITY + 1) # ensure priority is not DEFAULT_PRIORITY

                mock_time = Time.now
                allow(Time).to receive(:now).and_return(mock_time)

                # even if a new priority is given, it should still set priority to DEFAULT_PRIORITY
                IdentifyUserJob.perform_later_with_debounce(user.id, IdentifyUserJob::DEFAULT_PRIORITY + 2)

                expect(@job_query.count).to be(1)
                expect(job.reload.run_at).to be_within(1.second).of(mock_time + 1.minute)
                expect(job.reload.priority).to eq(IdentifyUserJob::DEFAULT_PRIORITY + 1)
            end
        end

        describe "when no pending_job" do

            it "should create a new job for the future if no pending job due to being locked" do
                IdentifyUserJob.perform_later(user.id)

                job = @job_query.first
                job.update(locked_at: Time.now) # job is not pending if it is currently running

                IdentifyUserJob.perform_later_with_debounce(user.id)
                expect(@job_query.count).to eq(2) # expect a new job to be created since the prior one is now running
            end

            it "should create a new job for the future if no pending job due to being failed" do
                IdentifyUserJob.perform_later(user.id)

                job = @job_query.first
                job.update(failed_at: Time.now) # job is not pending if it is currently running

                IdentifyUserJob.perform_later_with_debounce(user.id)
                expect(@job_query.count).to eq(2) # expect a new job to be created since the prior one is now running
            end

            it "should respect priority param" do
                priority = 4
                IdentifyUserJob.perform_later(user.id)

                job = @job_query.first
                job.update(failed_at: Time.now) # job is not pending if it is currently running

                IdentifyUserJob.perform_later_with_debounce(user, priority) # priority given
                job = @job_query.last # new job is created, so it's the last one
                expect(job.priority).to eq(priority)
            end

            it "should set the priority to DEFAULT_PRIORITY if no priority is given" do
                IdentifyUserJob.perform_later(user.id)

                job = @job_query.first
                job.update(failed_at: Time.now) # job is not pending if it is currently running

                IdentifyUserJob.perform_later_with_debounce(user.id) # no priority given
                job = @job_query.last # new job is created, so it's the last one
                expect(job.priority).to eq(IdentifyUserJob::DEFAULT_PRIORITY)
            end
        end
    end

    describe "perform" do

        it "should include all expected info in user traits" do
            expect(instance).to receive(:basic_user_info).and_return({basic_user_info: true})
            expect(instance).to receive(:progress_info).and_return({progress_info: true})
            expect(instance).to receive(:institution_info).and_return({institution_info: true})
            expect(instance).to receive(:cohort_info).and_return({cohort_info: true})
            expect(instance).to receive(:subscription_info).and_return({subscription_info: true})
            expect(instance).to receive(:career_profile_info).and_return({career_profile_info: true})
            expect(instance).to receive(:hiring_application_info).and_return({hiring_application_info: true})

            expect(Analytics).to receive(:identify).with({
                user_id: user.id,
                traits: {
                    basic_user_info: true,
                    progress_info: true,
                    institution_info: true,
                    cohort_info: true,
                    subscription_info: true,
                    career_profile_info: true,
                    hiring_application_info: true,
                }
            })

            expect($customerio).to receive(:identify).with(
                basic_user_info: true,
                progress_info: true,
                institution_info: true,
                cohort_info: true,
                subscription_info: true,
                career_profile_info: true,
                hiring_application_info: true
            )

            instance.perform(user.id)
            expect(instance.user).to eq(user) # this is needed in the _info methods, so make sure it is set
        end

        it "should not error if user does not exist" do
            expect {
                instance.perform(SecureRandom.uuid)
            }.not_to raise_error
        end

        it "should not identify a user that does not exist" do
            expect(instance).not_to receive(:basic_user_info)
            expect(Analytics).not_to receive(:identify)
            expect($customerio).not_to receive(:identify)
            instance.perform(SecureRandom.uuid)
        end

    end

    describe "_info methods" do

        before(:each) do
            instance.user = user # normally this would happen in perform, but we're trying to isolate things here
        end

        describe "basic_user_info" do

            it "should not raise any errors" do
                expect(Proc.new { instance.basic_user_info }).not_to raise_error
            end

            # See https://learn.customer.io/documentation/timezone-match.html#the-timezone-attribute.
            it "should include the specially named timezone attribute" do
                timezone = 'America/Los_Angeles'
                expect(instance.user).to receive(:timezone_with_fallback).and_return(timezone)
                expect(instance.basic_user_info).to have_entries({
                    timezone: timezone
                })
            end
        end

        describe "progress_info" do

            it "should add lessons_complete_count" do
                self.user = LessonProgress.where.not(completed_at: nil).first.user
                instance.user = user
                lessons_complete_count = user.lesson_progresses
                                            .where.not({completed_at: nil})
                                            .count

                expect(instance.progress_info).to have_entries({
                    lessons_complete_count: lessons_complete_count
                })
            end

        end

        describe "institution_info" do

            it "should work for quantic" do
                quantic = Institution.quantic
                allow(user).to receive(:active_institution).and_return(quantic)
                institution_info = instance.institution_info
                expect(institution_info).to eq({
                    institution_id: quantic.id,
                    institution_domain: quantic.domain,
                    institution_branding: quantic.branding,
                    institution_name: quantic.name,
                    institution_brand_name_short: quantic.brand_name_short,
                    institution_brand_name_standard: quantic.brand_name_standard,
                    institution_brand_name_long: quantic.brand_name_long
                })
            end

            it "should work for miya miya" do
                miya_miya = Institution.miya_miya
                allow(user).to receive(:active_institution).and_return(miya_miya)
                institution_info = instance.institution_info
                expect(institution_info).to eq({
                    institution_id: miya_miya.id,
                    institution_domain: miya_miya.domain,
                    institution_branding: miya_miya.branding,
                    institution_name: miya_miya.name,
                    institution_brand_name_short: miya_miya.brand_name_short,
                    institution_brand_name_standard: miya_miya.brand_name_standard,
                    institution_brand_name_long: miya_miya.brand_name_long
                })
            end

            it "should work for smartly" do
                smartly = Institution.smartly
                allow(user).to receive(:active_institution).and_return(smartly)
                institution_info = instance.institution_info
                expect(institution_info).to eq({
                    institution_id: smartly.id,
                    institution_domain: smartly.domain,
                    institution_branding: smartly.branding,
                    institution_name: smartly.name,
                    institution_brand_name_short: smartly.brand_name_short,
                    institution_brand_name_standard: smartly.brand_name_standard,
                    institution_brand_name_long: smartly.brand_name_long
                })
            end

            it "should work for external institution" do
                gtown = Institution.create({name: 'GEORGETOWNMSB', sign_up_code: 'GEORGETOWNMSB', external: true})
                allow(user).to receive(:active_institution).and_return(gtown)
                institution_info = instance.institution_info
                expect(institution_info).to eq({
                    institution_id: gtown.id,
                    institution_domain: 'smart.ly',
                    institution_branding: gtown.branding,
                    institution_name: gtown.name,
                    institution_brand_name_short: 'Smartly',
                    institution_brand_name_standard: 'Smartly Institute',
                    institution_brand_name_long: 'Smartly Institute'
                })
            end

        end

        describe "cohort_info" do

            describe "with career_network_only user" do

                before(:each) do
                    self.user = users(:pending_career_network_only_cohort_user)
                    instance.user = user
                    allow(user).to receive(:fallback_program_type).and_return('career_network_only')
                end

                after(:all) do
                    ENV['FREE_MBA_GROUPS'] = @orig_free_mba_groups_value
                end

                describe "without application" do

                    before(:each) do
                        allow(user).to receive(:get_completed_foundations_stream_count).and_return(0)
                    end

                    it "should include basic cohort_info" do
                        user.cohort_applications.destroy_all
                        cohort_info = instance.cohort_info
                        expect(cohort_info).to have_entries({
                            cohort_id: nil,
                            cohort_application_status: nil
                        })
                    end
                end

                describe "with application" do

                    it "should include basic cohort_info" do
                        cohort_info = instance.cohort_info
                        expect(cohort_info).to have_entries({
                            cohort_id: user.last_application.cohort_id,
                            cohort_application_status: 'pending',
                            cohort_title: user.last_application.cohort.title,
                            cohort_application_graduation_status: 'pending',
                            cohort_application_skip_period_expulsion: false
                        })
                    end

                    it "should set date timestamp information if user is accepted" do
                        allow_any_instance_of(Cohort).to receive(:decision_date).and_return(Time.now)
                        cohort_application = CohortApplication.find_by_user_id(user.id)
                        cohort_application.cohort.publish!
                        cohort_application.update(status: 'accepted')
                        cohort_info = instance.cohort_info
                        expect(cohort_info).to have_entries({
                            cohort_start_date: user.last_application.cohort.start_date && user.last_application.cohort.start_date.to_timestamp,
                            cohort_end_date: user.last_application.cohort.end_date && user.last_application.cohort.end_date.to_timestamp
                        })
                    end

                end

                describe "with mba_or_emba user" do

                    before(:each) do
                        self.user = CohortApplication.joins(:cohort).where(
                                        {cohorts: {program_type: ['mba', 'emba']}}
                                    ).first.user
                        instance.user = user
                    end

                    it "should set date timestamp information if user is mba_or_emba and pending" do
                        mba_cohort = Cohort.promoted_cohort('mba')
                        emba_cohort = Cohort.promoted_cohort('emba')
                        self.user.last_application.update_attribute(:status, 'pending')
                        allow_any_instance_of(CohortApplication).to receive(:published_cohort).and_return(mba_cohort)
                        allow(mba_cohort).to receive(:sister_cohort).and_return(emba_cohort)

                        expect(mba_cohort).to receive(:event_attributes).with(self.user.timezone).and_return({})
                        expect(emba_cohort).to receive(:event_attributes).with(self.user.timezone).and_return({})
                        instance.cohort_info
                    end

                end

            end

            describe "with mba_enabled user" do

                before(:each) do
                    self.user = users(:pending_mba_cohort_user)
                    allow(user).to receive(:mba_enabled?).and_return(true)
                    instance.user = user
                end

                after(:all) do
                    ENV['FREE_MBA_GROUPS'] = @orig_free_mba_groups_value
                end

                describe "without application" do
                    it "should include basic cohort_info" do
                        user.cohort_applications.destroy_all
                        user.fallback_program_type = 'mba'
                        user.save!
                        cohort_info = instance.cohort_info
                        expect(cohort_info).to have_entries({
                            cohort_id: nil,
                            cohort_application_status: nil
                        })
                    end
                end

                describe "with application" do
                    it "should include basic cohort_info" do
                        cohort_info = instance.cohort_info
                        expect(cohort_info).to have_entries({
                            cohort_id: user.last_application.cohort_id,
                            cohort_application_status: 'pending',
                            cohort_application_graduation_status: 'pending'
                        })
                    end

                    it "should set date timestamp information if user is accepted" do
                        cohort_application = CohortApplication.find_by_user_id(user.id)
                        cohort_application.cohort.publish!
                        cohort_application.update(status: 'accepted', cohort_slack_room_id: cohort_application.cohort.slack_rooms[0]&.id)
                        user.fallback_program_type = cohort_application.program_type
                        user.save!
                        cohort_info = instance.cohort_info
                        expect(cohort_info).to have_entries({
                            cohort_start_date: user.last_application.cohort.start_date.to_timestamp,
                            cohort_end_date: user.last_application.cohort.end_date.to_timestamp
                        })
                    end

                    describe "cohort_missing_documents" do
                        it "should be nil if no last_application" do
                            allow(user).to receive(:last_application).and_return(nil)
                            allow(user).to receive(:missing_verification_documents?).and_return(false)
                            allow(user).to receive(:missing_english_proficiency_documents?).and_return(false)
                            cohort_info = instance.cohort_info
                            expect(cohort_info[:cohort_missing_documents]).to be_nil
                        end

                        describe "with last_application" do
                            it "should be false if not missing documents" do
                                allow(user).to receive(:missing_verification_documents?).and_return(false)
                                allow(user).to receive(:missing_english_proficiency_documents?).and_return(false)
                                cohort_info = instance.cohort_info
                                expect(cohort_info[:cohort_missing_documents]).to be(false)
                            end

                            it "should be true if missing verification documents" do
                                allow(user).to receive(:missing_verification_documents?).and_return(true)
                                allow(user).to receive(:missing_english_proficiency_documents?).and_return(false)
                                cohort_info = instance.cohort_info
                                expect(cohort_info[:cohort_missing_documents]).to be(true)
                            end

                            it "should be true if missing english proficiency documents" do
                                allow(user).to receive(:missing_verification_documents?).and_return(false)
                                allow(user).to receive(:missing_english_proficiency_documents?).and_return(true)
                                cohort_info = instance.cohort_info
                                expect(cohort_info[:cohort_missing_documents]).to be(true)
                            end
                        end
                    end

                    describe "cohort_missing_english_proficiency_documents" do
                        it "should be nil if no last_application" do
                            allow(user).to receive(:last_application).and_return(nil)
                            allow(user).to receive(:missing_english_proficiency_documents?).and_return(false)
                            cohort_info = instance.cohort_info
                            expect(cohort_info[:cohort_missing_documents]).to be_nil
                        end

                        describe "with last_application" do
                            it "should be nil if not missing english proficiency documents" do
                                allow(user).to receive(:missing_english_proficiency_documents?).and_return(false)
                                cohort_info = instance.cohort_info
                                expect(cohort_info[:cohort_missing_documents]).to be(false)
                            end

                            it "should be true if missing english proficiency documents" do
                                allow(user).to receive(:missing_english_proficiency_documents?).and_return(true)
                                cohort_info = instance.cohort_info
                                expect(cohort_info[:cohort_missing_documents]).to be(true)
                            end
                        end
                    end

                    describe "cohort_interview_scheduled" do
                        it "should be nil if no last_application" do
                            allow(user).to receive(:last_application).and_return(nil)
                            cohort_info = instance.cohort_info
                            expect(cohort_info[:cohort_interview_scheduled]).to be_nil
                        end

                        describe "with last_application" do
                            it "should be nil if not scheduled" do
                                cohort_info = instance.cohort_info
                                expect(cohort_info[:cohort_interview_scheduled]).to be_nil
                            end

                            it "should be true if scheduled" do
                                ca = user.last_application
                                ca.rubric_interview_scheduled = true
                                ca.save!
                                cohort_info = instance.cohort_info
                                expect(cohort_info[:cohort_interview_scheduled]).to be(true)
                            end
                        end
                    end

                    describe "cohort_interview_conducted" do
                        it "should be nil if no last_application" do
                            allow(user).to receive(:last_application).and_return(nil)
                            cohort_info = instance.cohort_info
                            expect(cohort_info[:cohort_interview_conducted]).to be_nil
                        end

                        describe "with last_application" do
                            it "should be nil if not conducted" do
                                cohort_info = instance.cohort_info
                                expect(cohort_info[:cohort_interview_conducted]).to be_nil
                            end

                            it "should be true if conducted" do
                                ca = user.last_application
                                ca.rubric_interview = 'Conducted'
                                ca.save!
                                cohort_info = instance.cohort_info
                                expect(cohort_info[:cohort_interview_conducted]).to be(true)
                            end
                        end
                    end
                end
            end

            describe "pending_user_cohort_info" do

                ['pending', 'pre_accepted'].each do |status|

                    attr_accessor :user, :mba_cohort, :emba_cohort

                    before(:each) do
                        @user = users(:"#{status}_emba_cohort_user")
                        instance.user = @user

                        @mba_cohort = Cohort.promoted_cohort('mba')
                        @emba_cohort = Cohort.promoted_cohort('emba')
                        allow_any_instance_of(CohortApplication).to receive(:published_cohort).and_return(@emba_cohort)
                    end

                    it "should include info for pending cohort and sister_cohort" do
                        info = instance.cohort_info
                        expect(info[:pending_user_cohort_info][:mba]).to eq(mba_cohort.event_attributes(user.timezone))
                        expect(info[:pending_user_cohort_info][:emba]).to eq(emba_cohort.event_attributes(user.timezone))
                    end

                    it "should work for isolated_network cohorts" do
                        allow(emba_cohort).to receive(:isolated_network).and_return(true)
                        expect(Cohort).to receive(:promoted_cohort).with('mba').exactly(1).times.and_return(mba_cohort)
                        info = instance.cohort_info
                        expect(info[:pending_user_cohort_info][:mba]).to eq(mba_cohort.event_attributes(user.timezone))
                        expect(info[:pending_user_cohort_info][:emba]).to eq(emba_cohort.event_attributes(user.timezone))
                    end

                    it "should raise if no sister_cohort for non-isolated cohort" do
                        allow(emba_cohort).to receive(:sister_cohort).and_return(nil)
                        expect {
                            instance.cohort_info
                        }.to raise_error("I cannot identify a user with a pending application for a cohort that has no sister cohort")
                    end

                end

            end

            describe "cohort_isolated_network" do

                attr_accessor :emba_cohort

                before(:each) do
                    user = users(:accepted_emba_cohort_user)
                    instance.user = user
                    @emba_cohort = Cohort.promoted_cohort('emba')
                    allow_any_instance_of(CohortApplication).to receive(:published_cohort).and_return(@emba_cohort)
                end

                it "should include cohort_isolated_network attribute for isolated network cohorts" do
                    allow(emba_cohort).to receive(:isolated_network).and_return(true)
                    expect(instance.cohort_info[:cohort_isolated_network]).to be(true)
                end

                it "should be nil for non-isolated cohorts" do
                    expect(instance.cohort_info[:cohort_isolated_network]).to be(nil)
                end

            end

            describe "cohort_specializations_complete_count" do

                it "should add cohort_specializations_complete_count if cohort supports specializations and accepted" do
                    user = users(:accepted_emba_cohort_user)
                    instance.user = user
                    allow(user).to receive(:completed_specializations_count).and_return(1)

                    cohort_info = instance.cohort_info
                    expect(cohort_info).to have_entries({
                        cohort_specializations_complete_count: 1
                    })
                end

                it "should set cohort_specializations_complete_count to nil if cohort does not support specializations" do
                    user = users(:accepted_career_network_only_cohort_user)
                    instance.user = user

                    cohort_info = instance.cohort_info
                    expect(cohort_info).to have_entries({
                        cohort_specializations_complete_count: nil
                    })
                end

                it "should set cohort_specializations_complete_count to nil if not accepted" do
                    user = users(:accepted_emba_cohort_user)
                    user.last_application.update_attribute(:status, 'pending')
                    instance.user = user

                    cohort_info = instance.cohort_info
                    expect(cohort_info).to have_entries({
                        cohort_specializations_complete_count: nil
                    })
                end

            end

            describe "cohort_coursework_complete" do

                attr_accessor :last_application
                before(:each) do
                    self.last_application = CohortApplication.first
                    allow(user).to receive(:last_application).and_return(self.last_application)
                end

                describe "when accepted and cohort supports_specializations?" do

                    before(:each) do
                        allow(user).to receive(:program_type).and_return('emba')
                        allow(last_application).to receive(:status).and_return('accepted')
                        allow(Cohort::ProgramTypeConfig[user.program_type]).to receive(:supports_specializations?).and_return(true)
                    end

                    it "should be true if curriculum_complete? and specializations complete" do
                        allow(last_application).to receive(:curriculum_complete?).and_return(true)
                        allow(last_application).to receive(:required_specializations_complete?).and_return(true)
                        cohort_info = instance.cohort_info
                        expect(cohort_info).to have_entries({
                            cohort_coursework_complete: true
                        })
                    end

                    it "should be false if not curriculum_complete?" do
                        allow(last_application).to receive(:curriculum_complete?).and_return(false)
                        cohort_info = instance.cohort_info
                        expect(cohort_info).to have_entries({
                            cohort_coursework_complete: false
                        })
                    end

                    it "should be false if not specializations complete" do
                        allow(last_application).to receive(:curriculum_complete?).and_return(true)
                        allow(last_application).to receive(:required_specializations_complete?).and_return(false)
                        cohort_info = instance.cohort_info
                        expect(cohort_info).to have_entries({
                            cohort_coursework_complete: false
                        })
                    end

                end

                it "should be nil if cohort does not supports_specializations?" do
                    allow(user).to receive(:program_type).and_return('the_business_certificate')
                    allow(Cohort::ProgramTypeConfig[user.program_type]).to receive(:supports_specializations?).and_return(false)
                    cohort_info = instance.cohort_info
                    expect(cohort_info).to have_entries({
                        cohort_coursework_complete: nil
                    })
                end

                it "should be nil if not accepted" do
                    allow(last_application).to receive(:status).and_return('not_accepted')
                    cohort_info = instance.cohort_info
                    expect(cohort_info).to have_entries({
                        cohort_coursework_complete: nil
                    })
                end

            end

        end

        describe "career_profile_info" do
            it "should work for a user with can_edit_career_profile=true" do
                self.user = User.where(can_edit_career_profile: true).joins(:career_profile).first
                instance.user = user

                career_profile = user.career_profile

                expect(instance.career_profile_info).to have_entries({
                    can_edit_career_profile: true,
                    career_profile_complete_percentage: career_profile ? career_profile.last_calculated_complete_percentage.to_i : '',
                    career_profile_active: career_profile ? career_profile.career_profile_active? : ''
                })
            end

            it "should work for a user with can_edit_career_profile=false" do
                self.user = User.where(can_edit_career_profile: false).left_outer_joins(:career_profile).where("career_profiles.id is null").first
                instance.user = user

                expect(instance.career_profile_info).to have_entries({
                    can_edit_career_profile: false,
                    career_profile_complete_percentage: '',
                    career_profile_active: ''
                })
            end

            describe "career_profile_status" do

                it "should be '' if user has no career_profile" do
                    self.user = User.first
                    instance.user = user
                    allow(instance.user).to receive(:career_profile).and_return(nil) # stub the user's career_profile to be nil
                    expect(instance.career_profile_info).to have_entries({
                        career_profile_status: ''
                    })
                end

                it "should be interested_in_joining_new_company if user has career_profile" do
                    self.user = users(:user_with_very_interested_career_profile) # get a user with a career profile
                    instance.user = user
                    expect(user.career_profile.interested_in_joining_new_company).to eq('very_interested')
                    expect(instance.career_profile_info).to have_entries({
                        career_profile_status: 'very_interested'
                    })
                end
            end
        end

        describe "hiring_application_info" do
            it "should work for a user with a hiring_application" do
                self.user = User.joins(:hiring_application).first
                instance.user = user

                hiring_application = user.hiring_application
                expected_identify_value = hiring_application.last_calculated_complete_percentage.nil? ? 0 : hiring_application.last_calculated_complete_percentage
                expect(instance.hiring_application_info).to have_entries({
                    hiring_application_status: hiring_application.status,
                    hiring_profile_complete_percentage: expected_identify_value
                })
            end

            it "should work for a user without a hiring_application" do
                self.user = User.where.not(id: HiringApplication.pluck('user_id')).first
                instance.user = user

                expect(instance.hiring_application_info).to have_entries({
                    hiring_application_status: '',
                    hiring_profile_complete_percentage: ''
                })
            end
        end

    end

end