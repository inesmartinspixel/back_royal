require 'spec_helper'

describe SignNowCompletionPollingJob do

    attr_accessor :instance

    before(:each) do
        @instance = SignNowCompletionPollingJob.new
        @original_stdout = $stdout
        $stdout = File.open(File::NULL, "w") # suppress the puts for this test (/dev/null)
    end

    after(:each) do
        $stdout = @original_stdout
    end

    describe "perform" do

        it "should query for unsigned documents and check_and_queue_download_if_necessary" do
            unsigned_documents = [SignableDocument.create!(valid_atts), SignableDocument.create!(valid_atts)]
            mock_relation = double('SignableDocument::ActiveRecord_Relation')

            allow(mock_relation).to receive(:count)
            expect(SignableDocument).to receive_message_chain(:where, :where) { mock_relation }

            unsigned_documents.each do |document|
                expect(document).to receive(:check_and_queue_download_if_necessary)
            end

            allow(mock_relation).to receive(:find_each).and_yield(unsigned_documents[0]).and_yield(unsigned_documents[1])
            instance.perform
        end
    end

    describe "unsigned_signable_documents" do

        before(:each) do
            SignableDocument.destroy_all
        end

        it "should filter out documents with a file fingerprint" do
            SignableDocument.create!(valid_atts.merge({ file_fingerprint: 'something'}))
            expect(instance.unsigned_signable_documents).to eq([])
        end

        it "should filter out documents that have already expired fingerprint" do
            SignableDocument.create!(valid_atts.merge({ file_fingerprint: nil, sign_now_signing_link_expiry: Time.now - 1.day }))
            expect(instance.unsigned_signable_documents).to eq([])
        end

        it "should return documents that have no file details and have not expired" do
            document = SignableDocument.create!(valid_atts.merge({ file_fingerprint: nil, sign_now_signing_link_expiry: Time.now + 1.day }))
            expect(instance.unsigned_signable_documents).to eq([document])
        end

    end

    def valid_atts
        {
            user_id: User.first.id,
            document_type: 'enrollment_agreement',
            metadata: {}
        }
    end

end
