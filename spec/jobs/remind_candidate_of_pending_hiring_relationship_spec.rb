require 'spec_helper'

describe RemindCandidateOfPendingHiringRelationship do
    attr_accessor :instance

    before(:each) do
        @instance = RemindCandidateOfPendingHiringRelationship.new
    end

    describe "perform" do

        it "should do nothing if no hiring relationship" do
            expect_any_instance_of(HiringRelationship).not_to receive(:log_event)
            instance.perform(SecureRandom.uuid, 24) # pass in a uuid that's not going to return an actual hiring relationship
        end

        it "should do nothing if hiring relationship candidate_status is 'rejected'" do
            assert_perform(false, {candidate_status: 'rejected'})
        end

        it "should do nothing if hiring relationship candidate_status is 'accepted'" do
            assert_perform(false, {candidate_status: 'accepted'})
        end

        it "should do nothing if hiring relationship hiring_manager_status is not 'accepted'" do
            assert_perform(false, {hiring_manager_status: 'rejected'})
        end

        it "should do nothing if hiring relationship is closed?" do
            assert_perform(false, {closed: true})
        end

        it "should do nothing if hiring_manager is not accepted" do
            assert_perform(false, {hiring_application_status: 'rejected'})
        end

        it "should do nothing if conversation contains message for candidate" do
            assert_perform(false, {contains_message_from_user: true})
        end

        describe "when not returning early" do

            it "should log hiring_relationship:pending_request_reminder event if no conversation" do
                assert_perform(true, {conversation: nil})
            end

            it "should log hiring_relationship:pending_request_reminder event if conversation does not contain messages from candidate" do
                assert_perform(true)
            end
        end
    end

    def assert_perform(expect_log_event, opts = {})
        opts = opts.with_indifferent_access
        hiring_relationship = HiringRelationship.first

        allow_any_instance_of(HiringRelationship).to receive(:candidate_status).and_return((opts['candidate_status'] || 'pending')) # stub this to be 'pending' by default
        allow_any_instance_of(HiringRelationship).to receive(:hiring_manager_status).and_return((opts['hiring_manager_status'] || 'accepted')) # stub this to be 'accepted' by default
        allow_any_instance_of(HiringRelationship).to receive(:closed?).and_return((opts['closed'] || false)) # stub this to be false by default
        allow_any_instance_of(HiringApplication).to receive(:status).and_return((opts['hiring_application_status'] || 'accepted')) # stub this to be 'accepted' by default
        conversation_double = double("Mailboxer::Conversation instance double", contains_message_from_user?: opts['contains_message_from_user'] || false) # stub this to be false by default
        allow_any_instance_of(HiringRelationship).to receive(:conversation).and_return(opts.key?('conversation') ? opts['conversation'] : conversation_double) # stub this to use the conversation_double by default

        if expect_log_event
            event_double = double("Event instance double")
            expect_any_instance_of(HiringRelationship).to receive(:log_event).with(
                'pending_request_reminder',
                'candidate', {
                num_hours_in_delay: 24
            }).and_return(event_double)
            expect(event_double).to receive(:log_to_external_systems)
        else
            expect_any_instance_of(HiringRelationship).not_to receive(:log_event)
        end

        num_hours_in_delay = 24
        instance.perform(hiring_relationship.id, num_hours_in_delay)
    end
end
