require 'spec_helper'

describe ImportAndPruneRedshiftEventsJob do
    attr_accessor :now

    before(:each) do
        @now = Time.now
        allow(Time).to receive(:now).and_return(now)
    end

    describe "build_manifest" do

        attr_accessor :mock_bucket, :mock_bucket_objs, :instance

        before(:each) do
            @instance = ImportAndPruneRedshiftEventsJob.new
            @mock_bucket_objs = []
            @mock_bucket = OpenStruct.new(objects: mock_bucket_objs)
            mock_put_obj = OpenStruct.new()

            expect(mock_put_obj).to receive(:put).exactly(1).times
            expect(mock_bucket).to receive(:object).exactly(1).times { mock_put_obj }
            allow(instance).to receive(:bucket) { mock_bucket }
            allow(instance).to receive(:prefix) { 'events_'}
        end

        it "should filter out stream data that is not sufficiently old" do
            mock_bucket_objs.push(*[
                OpenStruct.new(key: 'events_2019/06/19/14/some_events-2019-06-19', last_modified: now - 1.day),
                OpenStruct.new(key: 'events_2019/06/20/13/some_events-2019-06-20', last_modified: now - 1.hour),
                OpenStruct.new(key: 'events_2019/06/20/14/some_events-2019-06-20', last_modified: now - 4.minutes)
            ])
            manifest_contents, manifest_key = instance.build_manifest
            expect(manifest_key).to eq("events_ImportAndPruneRedshiftEventsJob/manifests/#{now.strftime('%Y-%m-%d_%H-%M-%S-%12N')}")
            expect(manifest_contents[:entries].map { |entry| entry[:key] }).to match_array([mock_bucket_objs[0].key, mock_bucket_objs[1].key])
        end

        it "should filter out objects that don't match the prefix / pattern" do
            mock_bucket_objs.push(*[
                OpenStruct.new(key: 'fake_events_2019/06/19/14/some_events-2019-06-19', last_modified: now - 1.day),
                OpenStruct.new(key: 'events_aaaa/bb/cc/14/some_events-2019-06-19', last_modified: now - 1.day),
                OpenStruct.new(key: 'events_2019/06/20/13/some_events-2019-06-20', last_modified: now - 1.hour),
                OpenStruct.new(key: 'events_2019/06/20/14/some_events-2019-06-20', last_modified: now - 10.minutes)
            ])
            manifest_contents, manifest_key = instance.build_manifest
            expect(manifest_key).to eq("events_ImportAndPruneRedshiftEventsJob/manifests/#{now.strftime('%Y-%m-%d_%H-%M-%S-%12N')}")
            expect(manifest_contents[:entries].map { |entry| entry[:key] }).to match_array([mock_bucket_objs[2].key, mock_bucket_objs[3].key])
        end
    end

    describe "delete_old_events" do

        before(:each) do
            FactoryBot.create(:event, {
                created_at: Time.now - 5.days
            })

            FactoryBot.create(:redshift_event, {
                event_type: 'click',
                created_at: Time.now - 5.days
            })

            FactoryBot.create(:redshift_event, {
                event_type: 'error',
                created_at: Time.now - 5.days
            })
        end

        it "should delete old click events from redshift" do
            instance = ImportAndPruneRedshiftEventsJob.new
            expect(instance).to receive(:days_to_keep_click_events_in_redshift).and_return(1).at_least(1).times
            expect(RedshiftEvent.where(event_type: 'click').where('created_at < ?', now - 1.day)).not_to be_empty
            allow(instance).to receive(:dump) # just mock this out to prevent errors.  We're not interested in this right now
            instance.send(:delete_old_events)
            expect(RedshiftEvent.where(event_type: 'click').where('created_at < ?', now - 1.day)).to be_empty
        end

        it "should delete old click events from redshift" do
            instance = ImportAndPruneRedshiftEventsJob.new
            expect(instance).to receive(:days_to_keep_error_events_in_redshift).and_return(1).at_least(1).times
            expect(RedshiftEvent.where(event_type: 'error').where('created_at < ?', now - 1.day)).not_to be_empty
            allow(instance).to receive(:dump) # just mock this out to prevent errors.  We're not interested in this right now
            instance.send(:delete_old_events)
            expect(RedshiftEvent.where(event_type: 'error').where('created_at < ?', now - 1.day)).to be_empty
        end

    end

end