require 'spec_helper'

describe Cohort::AdmissionRoundEndsSoonJob do
    attr_reader :instance

    before(:each) do
        @instance = Cohort::AdmissionRoundEndsSoonJob.new
    end

    it "should include Cohort::AdmissionRoundBatchJobMixin" do
        expect(instance).to be_a(Cohort::AdmissionRoundBatchJobMixin)
    end

    describe "::batch_event_type" do

        it "should return 'pre_application:admission_round_ends_soon'" do
            event_type = nil
            expect {
                event_type = Cohort::AdmissionRoundEndsSoonJob.batch_event_type
            }.not_to raise_error
            expect(event_type).to eq('pre_application:admission_round_ends_soon')
        end
    end
end
