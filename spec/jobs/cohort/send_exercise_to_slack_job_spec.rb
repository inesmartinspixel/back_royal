require 'spec_helper'
require 'tempfile'

describe Cohort::SendExerciseToSlackJob do

    fixtures :users, :cohorts
    attr_reader :cohort, :period_index, :period

    before(:each) do
        stub_const('ENV', {'ENABLE_SLACK_COHORT_MESSAGES' => 'true'})

        @user = users(:accepted_mba_cohort_user)
        @cohort = cohorts(:published_mba_cohort)
        @cohort.slack_rooms.to_a.slice(1, 99).each do |s|
            CohortApplication.where(cohort_slack_room_id: s.id).update_all(cohort_slack_room_id: nil)
            s.destroy
        end
        @slack_room = @cohort.slack_rooms.first
        expect(cohort.accepted_users).not_to be_empty

        @period_index = 2 # 1-based
        @period = cohort.periods[period_index-1] # see note in code about 1-indexing of period indexes
        @cohort.publish!
    end

    it "should only run if ENABLE_SLACK_COHORT_MESSAGES is set to 'true'" do
        stub_const('ENV', {'ENABLE_SLACK_COHORT_MESSAGES' => ''})
        @job = Cohort::SendExerciseToSlackJob.new
        setup_exercise_with_file

        expect_any_instance_of(Cohort).not_to receive(:relative_time_from_end_of_period)
        expect(Slack::Web::Client).not_to receive(:new)
        expect_any_instance_of(Slack::Web::Client).not_to receive(:chat_postMessage)

        @job.perform(@cohort.id, @period_index, @exercise["id"], @slack_room.id)
    end

    it "should short-circuit if more than 24 hours after job should have run" do
        @job = Cohort::SendExerciseToSlackJob.new
        setup_exercise_with_file
        allow(Time).to receive(:now).and_return(@cohort.relative_time_from_end_of_period(-24, 2, 'hours') + 25.hours)
        expect(@job.perform(@cohort.id, @period_index, @exercise["id"], @slack_room.id)).to be(nil)
    end

    it "should work when just message" do
        @job = Cohort::SendExerciseToSlackJob.new

        @exercise = {
            "id" => SecureRandom.uuid,
            "hours_offset_from_end" => -24,
            "message" => "@channel Test message. Test email mba@smart.ly. Test message.",
            "channel" => "#foo"
        }
        @period["exercises"] = [@exercise]
        @cohort.publish!

        setup_client

        timestamp = Time.now.utc
        mock_response = OpenStruct.new({
            channel: "slackChannelId",
            ts: timestamp
        })
        expect(@mock_client).to receive(:chat_postMessage).with(
            channel: @exercise["channel"],
            text: "<!channel> Test message. Test email mba@smart.ly. Test message."
        ).and_return(mock_response)

        expect(@mock_client).to receive(:pins_add).with({
            channel: "slackChannelId",
            timestamp: timestamp
        })

        @job.perform(@cohort.id, @period_index, @exercise["id"], @slack_room.id)
    end

    it "should support overriding message and document for a particular slack room" do
        setup_exercise_with_file(false)
        @job = Cohort::SendExerciseToSlackJob.new

        @exercise["slack_room_overrides"] = {
            @slack_room.id => {
                "message" => "You guys are the best room",
                "document"=> @exercise['document'].merge('title' => "For the best slack room")
            }
        }
        (@cohort.slack_rooms - [@slack_room]).each do |other_slack_room|
            @exercise["slack_room_overrides"][other_slack_room.id] = {
                "message" => "not this one",
                "document"=> @exercise['document'].merge('title' => "Not this one")
            }
        end
        @cohort.publish!
        expect(S3ExerciseDocument).to receive(:find).with(@s3_document.id).and_return(@s3_document)

        expect(@job).to receive(:log_to_slack_room) do |opts|
            expect(opts[:message_with_escaped_mentions]).to eq('You guys are the best room')
            expect(opts[:document_title]).to eq("For the best slack room")
        end
        expect(@job).to receive(:add_pins)

        @job.perform(@cohort.id, @period_index, @exercise["id"], @slack_room.id)

    end

    describe "without bot_user_access_token" do

        before(:each) do
            @slack_room.update_column(:bot_user_access_token, nil)
        end

        it "should use admin_token instead and set as_user to true when we log_to_slack_room" do
            @slack_room.update_column(:admin_token, 'some_admin_access_token')
            @job = Cohort::SendExerciseToSlackJob.new

            @exercise = {
                "id" => SecureRandom.uuid,
                "hours_offset_from_end" => -24,
                "message" => "@channel Test message. Test email mba@smart.ly. Test message.",
                "channel" => "#foo"
            }
            @period["exercises"] = [@exercise]
            @cohort.publish!

            setup_client(true)

            timestamp = Time.now.utc
            mock_response = OpenStruct.new({
                channel: "slackChannelId",
                ts: timestamp
            })
            expect(@mock_client).to receive(:chat_postMessage).with(
                channel: @exercise["channel"],
                text: "<!channel> Test message. Test email mba@smart.ly. Test message.",

                # true because the admin_token belongs to an actual user.
                # See https://api.slack.com/methods/chat.postMessage#arg_as_user.
                as_user: true
            ).and_return(mock_response)

            expect(@mock_client).to receive(:pins_add).with({
                channel: "slackChannelId",
                timestamp: timestamp
            })

            @job.perform(@cohort.id, @period_index, @exercise["id"], @slack_room.id)
        end
    end

    describe "with file" do
        it "should work when file upload" do
            @job = Cohort::SendExerciseToSlackJob.new
            setup_exercise_with_file
            setup_client
            expect_file_upload

            expect(@mock_client).to receive(:pins_add).with({
                channel: "slackChannelId",
                timestamp: "slackThreadTimeStamp"
            })

            expect(@mock_temp_file).to receive(:unlink)
            @job.perform(@cohort.id, @period_index, @exercise["id"], @slack_room.id)
        end

        it "should just capture exception if error when deleting the temp file" do
            @job = Cohort::SendExerciseToSlackJob.new
            setup_exercise_with_file
            setup_client
            expect_file_upload

            expect(@mock_client).to receive(:pins_add).with({
                channel: "slackChannelId",
                timestamp: "slackThreadTimeStamp"
            })

            expect(@mock_temp_file).to receive(:unlink).exactly(3).times.and_raise("foo error")
            expect(Raven).to receive(:capture_exception).with("Error deleting /tmp/mock.png after sending to Slack")
            @job.perform(@cohort.id, @period_index, @exercise["id"], @slack_room.id)
        end
    end

    it "should just capture exception if error when pinning" do
        @job = Cohort::SendExerciseToSlackJob.new
        setup_exercise_with_file
        setup_client
        expect_file_upload
        err = RuntimeError.new('foo error')
        expect(@mock_client).to receive(:pins_add).exactly(3).times.and_raise(err)

        expect(Raven).to receive(:capture_exception).with(err, {
            extra: {
                title: @exercise['document']['title'],
                channel: @exercise['channel'],
                cohort: @cohort.name,
                period_index: @period_index
            }
        })
        expect(@mock_temp_file).to receive(:unlink)
        @job.perform(@cohort.id, @period_index, @exercise["id"], @slack_room.id)
    end

    def setup_exercise_with_file(save = true)
        @s3_document = S3ExerciseDocument.new(
            file: fixture_file_upload('files/test.png', 'image/png'),
            file_file_name: "foo.png",
            file_content_type: "Image/Foo")
        @exercise = {
            "id" => SecureRandom.uuid,
            "hours_offset_from_end" => -24,
            "message" => "@channel Test message. Test email mba@smart.ly. Test message.",
            "channel" => "#foo",
            "document" => {
                "id" => @s3_document.id,
                "file_file_name" => @s3_document.file_file_name,
                "url" => "https://foo.com",
                "title" => "Foo File"
            }
        }
        @period["exercises"] = [@exercise]
        @cohort.publish! if save
    end

    def setup_client(with_admin_token = false)
        @mock_client = OpenStruct.new({files_upload: "mock", chat_postMessage: "mock", pins_add: "mock"})
        expect(Slack::Web::Client).to receive(:new).with(token: with_admin_token ? @slack_room.admin_token : @slack_room.bot_user_access_token).and_return(@mock_client)
    end

    def expect_file_upload
        expect(S3ExerciseDocument).to receive(:find).with(@s3_document.id).and_return(@s3_document)

        @mock_temp_file = OpenStruct.new({
            write: "mock",
            close: "mock",
            path: "/tmp/mock.png"
        })
        expect(Tempfile).to receive(:new).with(@s3_document.file_file_name).and_return(@mock_temp_file)
        expect(@job).to receive(:read_paperclip_file).with(@s3_document.file)
        expect(@mock_temp_file).to receive(:write)
        expect(@mock_temp_file).to receive(:close)

        mock_response = OpenStruct.new({
            file: OpenStruct.new({
                channels: ["slackChannelId"],
                id: "slackFileId",
                shares: OpenStruct.new({
                    "public": OpenStruct.new({
                        "slackChannelId": [
                            OpenStruct.new({
                                "reply_users": [],
                                "reply_users_count": 0,
                                "reply_count": 0,
                                "ts": "slackThreadTimeStamp"
                            })
                        ]
                    })
                })
            })
        })

        mock_file = "a file"
        expect(Faraday::UploadIO).to receive(:new).with(@mock_temp_file.path, @s3_document.file_content_type).and_return(mock_file)
        expect(@mock_client).to receive(:files_upload).and_return(mock_response)
            .with(channels: @exercise["channel"],
                file: mock_file,
                title: @exercise["document"]["title"],
                filename: @s3_document.file_file_name,
                initial_comment: "<!channel> Test message. Test email mba@smart.ly. Test message.")
    end
end