require 'spec_helper'

describe Cohort::ReportExamResultJob do
    attr_reader :stream_progress

    before(:each) do
        @stream_progress = Lesson::StreamProgress.where.not(completed_at: nil).first
        allow_any_instance_of(User).to receive(:average_assessment_score_best).and_return(13.37)
    end

    describe "basic functionality" do
        it "should work" do
            assert_event_logged do |id, user_id, event_type, attrs|

                expect(user_id).to eq(stream_progress.user_id)
                expect(event_type).to eq('exam:report_score')

                expect(attrs[:stream_locale_pack_id]).to eq(stream_progress.locale_pack_id)
                expect(attrs[:score]).to eq(42)
                expect(attrs[:passed]).to eq(false)
                expect(attrs[:average_assessment_score_best]).to eq(13.37)
            end
        end

        it "should round up properly" do
            assert_event_logged(0.705) do |id, user_id, event_type, attrs|
                expect(attrs[:score]).to eq(71)
            end
        end

        it "should round down properly" do
            assert_event_logged(0.704) do |id, user_id, event_type, attrs|
                expect(attrs[:score]).to eq(70)
            end
        end
    end

    describe "passed" do
        it "should return true if == 0.70" do
            assert_event_logged(0.70) do |id, user_id, event_type, attrs|
                expect(attrs[:passed]).to eq(true)
            end
        end

        it "should return true if > 0.70" do
            assert_event_logged(0.7000001) do |id, user_id, event_type, attrs|
                expect(attrs[:passed]).to eq(true)
            end
        end

        it "should return false if < 0.70" do
            assert_event_logged(0.69999) do |id, user_id, event_type, attrs|
                expect(attrs[:passed]).to eq(false)
            end
        end
    end

    describe "course details" do

        it "should include english stream info if user has english locale" do
            user = stream_progress.user
            user.update_attribute(:pref_locale, 'en')

            stream = Lesson::Stream.all_published.where(locale: 'en', locale_pack_id: stream_progress.locale_pack_id).first

            stream.entity_metadata.tweet_template = 'amazing tweet'
            stream.entity_metadata.save!

            assert_event_logged do |id, user_id, event_type, attrs|
                expect(attrs[:english_title]).to eq(stream.title)
                expect(attrs[:title_in_users_locale]).to eq(stream.title)
                expect(attrs[:english_url]).to eq(stream.entity_metadata.canonical_url)
                expect(attrs[:url_in_users_locale]).to eq(stream.entity_metadata.canonical_url)
                expect(attrs[:tweet_template]).to eq(stream.entity_metadata.tweet_template)
            end
        end

        it "should include localized stream info if user has non-english locale and localized stream is available" do
            user = stream_progress.user

            # ensure there is a spanish stream
            locale_pack_id = stream_progress.locale_pack_id
            english_stream = Lesson::Stream.where(locale: 'en', locale_pack_id: locale_pack_id).first
            spanish_stream = Lesson::Stream.where(locale: 'es', locale_pack_id: locale_pack_id).first


            if spanish_stream.nil?
                spanish_stream = Lesson::Stream.duplicate!(
                    User.first,
                    {locale: 'es', title: 'title'},
                    {
                        in_same_locale_pack: true,
                        duplicate_from_id: english_stream.id,
                        duplicate_from_updated_at: english_stream.updated_at
                    })
                spanish_stream.publish!
            end

            user.update_attribute(:pref_locale, 'es')

            english_stream.entity_metadata.tweet_template = 'amazing tweet'
            english_stream.entity_metadata.save!
            spanish_stream.entity_metadata.tweet_template = 'tweet increíble'
            spanish_stream.entity_metadata.save!

            assert_event_logged do |id, user_id, event_type, attrs|
                expect(attrs[:english_title]).to eq(english_stream.title)
                expect(attrs[:title_in_users_locale]).to eq(spanish_stream.title)
                expect(attrs[:english_url]).to eq(english_stream.entity_metadata.canonical_url)
                expect(attrs[:url_in_users_locale]).to eq(spanish_stream.entity_metadata.canonical_url)
                expect(attrs[:tweet_template]).to eq(english_stream.entity_metadata.tweet_template)
            end
        end

        it "should include english stream info if user has non-english locale and localized stream is not available" do
            user = stream_progress.user

            # ensure there is a spanish stream
            locale_pack_id = stream_progress.locale_pack_id
            english_stream = Lesson::Stream.where(locale: 'en', locale_pack_id: locale_pack_id).first
            spanish_stream = Lesson::Stream.where(locale: 'es', locale_pack_id: locale_pack_id).first

            if spanish_stream.present?
                spanish_stream.unpublish!
            end

            user.update_attribute(:pref_locale, 'es')

            english_stream.entity_metadata.tweet_template = 'amazing tweet'
            english_stream.entity_metadata.save!

            assert_event_logged do |id, user_id, event_type, attrs|
                expect(attrs[:english_title]).to eq(english_stream.title)
                expect(attrs[:title_in_users_locale]).to eq(english_stream.title)
                expect(attrs[:english_url]).to eq(english_stream.entity_metadata.canonical_url)
                expect(attrs[:url_in_users_locale]).to eq(english_stream.entity_metadata.canonical_url)
                expect(attrs[:tweet_template]).to eq(english_stream.entity_metadata.tweet_template)
            end
        end
    end

    def assert_event_logged(score = 0.42)

        allow_any_instance_of(Lesson::StreamProgress).to receive(:official_test_score).and_return(score)

        expect(Event).to receive(:create_server_event!) do |id, user_id, event_type, attrs|

            yield(id, user_id, event_type, attrs)

            mock_event = "mock_event"
            expect(mock_event).to receive(:log_to_external_systems)
            mock_event
        end

        Cohort::ReportExamResultJob.perform_now(stream_progress.user_id, stream_progress.locale_pack_id, {
            foo: true
        })
    end
end