require 'spec_helper'

describe Cohort::AdmissionRoundMidpointJob do

    fixtures :cohorts
    attr_reader :cohort, :period_index, :period, :users, :user

    before(:each) do
        @cohort = cohorts(:published_mba_cohort)
        @users = User.limit(1)
        @user = users.first
    end

    it "should include Cohort::AdmissionRoundBatchJobMixin" do
        expect(Cohort::AdmissionRoundMidpointJob.new).to be_a(Cohort::AdmissionRoundBatchJobMixin)
    end

    describe "after_final_batch_success" do

        it "should log_invite_to_reapply_events" do
            instance = Cohort::AdmissionRoundMidpointJob.new
            expect(instance).to receive(:log_invite_to_reapply_events)
            instance.after_final_batch_success
        end
    end

    describe "log_invite_to_reapply_events" do
        it "should be sent to a user whose last application is marked as should_invite_to_reapply" do
            application = CohortApplication.where(status: 'rejected').first

            # ensure that this is the last application for the user
            application.user.cohort_applications.where.not(id: application.id).destroy_all
            application.update_attribute(:should_invite_to_reapply, true)
            mock_promoted_admission_rounds = [double('AdmissionRound instance', {index: 1, program_type: 'mba'}), double('AdmissionRound instance', {index: 1, program_type: 'emba'})]
            allow_any_instance_of(Cohort::AdmissionRoundMidpointJob).to receive(:promoted_admission_rounds).and_return(mock_promoted_admission_rounds)
            expect(AdmissionRound).to receive(:promoted_rounds_event_attributes).with(application.user.timezone, mock_promoted_admission_rounds).and_return({})
            expect_any_instance_of(Cohort::Version).to receive(:event_attributes).with(application.user.timezone).and_return({})

            assert_event_logged

            expect(application.reload.should_invite_to_reapply).to be(false)
        end

        it "should be not sent to a user whose previous application is marked as should_invite_to_reapply" do
            application = CohortApplication.where(status: 'rejected').first
            application.update_attribute(:should_invite_to_reapply, true)

            another_cohort_id = Cohort.all_published.where.not(id: application.user.cohort_applications.pluck(:cohort_id)).first.id

            # ensure that the user has an application after this one
            CohortApplication.create!(
                user_id: application.user_id,
                applied_at: application.applied_at + 1.day,
                cohort_id: another_cohort_id
            )
            expect(Event).not_to receive(:create_server_event!)

            Cohort::AdmissionRoundMidpointJob.new(cohort.id).log_invite_to_reapply_events
            expect(application.reload.should_invite_to_reapply).to be(false)
        end

        it "should not be sent in the 2nd round of a cycle" do
            application = CohortApplication.where(status: 'rejected').first

            # ensure that this is the last application for the user
            application.user.cohort_applications.where.not(id: application.id).destroy_all
            application.update_attribute(:should_invite_to_reapply, true)

            allow_any_instance_of(AdmissionRound).to receive(:index).and_return(2)

            expect(Event).not_to receive(:create_server_event!)

            Cohort::AdmissionRoundMidpointJob.new(cohort.id).log_invite_to_reapply_events
            expect(application.reload.should_invite_to_reapply).to be(true)
        end

        def assert_event_logged(&block)
            expect(Event).to receive(:create_server_event!) do |id, user_id, event_type, attrs|
                expect(event_type).to eq('cohort:invite_to_reapply')
                mock_event = "mock_event_for_#{user_id}"
                expect(mock_event).to receive(:log_to_external_systems).with(no_args)
                mock_event
            end

            Cohort::AdmissionRoundMidpointJob.new(cohort.id).log_invite_to_reapply_events
        end
    end

end
