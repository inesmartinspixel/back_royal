require 'spec_helper'

describe Cohort::AdmissionRoundEndsTomorrowJob do

    attr_reader :instance

    before(:each) do
        @instance = Cohort::AdmissionRoundEndsTomorrowJob.new
    end

    it "should include Cohort::AdmissionRoundBatchJobMixin" do
        expect(instance).to be_a(Cohort::AdmissionRoundBatchJobMixin)
    end

    describe "::batch_event_type" do

        it "should return 'pre_application:admission_round_ending_tomorrow'" do
            event_type = nil
            expect {
                event_type = Cohort::AdmissionRoundEndsTomorrowJob.batch_event_type
            }.not_to raise_error
            expect(event_type).to eq('pre_application:admission_round_ending_tomorrow')
        end
    end
end
