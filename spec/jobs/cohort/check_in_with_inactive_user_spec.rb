require 'spec_helper'

describe Cohort::CheckInWithInactiveUser do
    attr_accessor :instance

    before(:each) do
        @instance = Cohort::CheckInWithInactiveUser.new
    end

    describe "perform" do

        it "should return early if no user is found" do
            user_id = nil
            expect(User).to receive(:find_by_id).with(user_id).and_return(nil)
            expect(Event).not_to receive(:create_server_event!)
            @instance.perform(user_id)
        end

        it "should return early if user !should_be_checked_in_with?" do
            mock_user = double("User")
            allow(User).to receive(:find_by_id).with('foo').and_return(mock_user)
            expect(mock_user).to receive(:should_be_checked_in_with?).and_return(false)
            expect(Event).not_to receive(:create_server_event!)
            @instance.perform('foo')
        end

        it "should create_server_event! for user" do
            mock_published_cohort = double("Cohort", title: 'THE BEST COHORT EVER!')
            mock_program_type_config = double("ProgramTypeConfig", is_paid_cert?: true)
            mock_cohort_application = double("CohortApplication", published_cohort: mock_published_cohort, program_type_config: mock_program_type_config)
            mock_user = double("User", id: 'foo', application_for_relevant_cohort: mock_cohort_application)
            allow(User).to receive(:find_by_id).with('foo').and_return(mock_user)
            allow(mock_user).to receive(:should_be_checked_in_with?).and_return(true)
            mock_event = double("Event instance")
            expect(Event).to receive(:create_server_event!).with(
                anything,
                'foo',
                'cohort:check_in_with_inactive_user',
                {
                    cohort_title: 'THE BEST COHORT EVER!',
                    is_paid_cert: true
                }
            ).and_return(mock_event)
            expect(mock_event).to receive(:log_to_external_systems).with(no_args)
            @instance.perform('foo')
        end
    end
end
