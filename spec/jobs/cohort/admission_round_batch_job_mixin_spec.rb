require 'spec_helper'

describe Cohort::AdmissionRoundBatchJobMixin do

    fixtures :cohorts
    attr_reader :cohort, :user

    class Cohort::AdmissionRoundJobThatIncludesRelevantEmail < ApplicationJob
        include Cohort::AdmissionRoundBatchJobMixin

        def self.batch_event_type
            'some_event_type'
        end

        def self.include_relevant_email_in_event_payload?
            true
        end

        def after_final_batch_success
        end
    end

    class Cohort::AdmissionRoundJobThatDoesNotIncludeRelevantEmail < ApplicationJob
        include Cohort::AdmissionRoundBatchJobMixin

        def self.batch_event_type
            'some_event_type'
        end

        def self.include_relevant_email_in_event_payload?
            false
        end
    end

    class Cohort::BadAdmissionRoundJob < ApplicationJob
        include Cohort::AdmissionRoundBatchJobMixin
    end


    before(:each) do
        @cohort = cohorts(:published_mba_cohort)
        @user = User.first
    end

    describe "ClassMethods" do

        describe "batch_event_type" do

            it "should raise error if batch_event_type method is not overridden in including class" do
                expect {
                    Cohort::BadAdmissionRoundJob.batch_event_type
                }.to raise_error('batch_event_type must be implemented in including class as a class method')
            end

            it "should not raise error if batch_event_type method is overridden in including class" do
                expect {
                    Cohort::AdmissionRoundJobThatIncludesRelevantEmail.batch_event_type
                }.not_to raise_error
            end
        end

        describe "include_relevant_email_in_event_payload?" do

            it "should raise error if include_relevant_email_in_event_payload? method is not overridden in including class" do
                expect {
                    Cohort::BadAdmissionRoundJob.include_relevant_email_in_event_payload?
                }.to raise_error('include_relevant_email_in_event_payload? must be implemented in including class as a class method')
            end

            it "should not raise error if include_relevant_email_in_event_payload? method is overridden in including class" do
                expect {
                    Cohort::AdmissionRoundJobThatIncludesRelevantEmail.include_relevant_email_in_event_payload?
                }.not_to raise_error
                expect {
                    Cohort::AdmissionRoundJobThatDoesNotIncludeRelevantEmail.include_relevant_email_in_event_payload?
                }.not_to raise_error
            end
        end
    end

    describe "#perform" do

        it "should call after_final_batch_success if users are found but the number of users found is less than #{Cohort::AdmissionRoundBatchJobMixin::BATCH_SIZE}" do
            instance = Cohort::AdmissionRoundJobThatDoesNotIncludeRelevantEmail.new
            relation = User.where(id: User.first.id)
            expect(User).to receive(:who_receives_pre_application_communications).and_return(relation)
            expect(instance).to receive(:after_final_batch_success)
            instance.perform(cohort.id)
        end

        it "should create an event and log it to external systems" do
            allow_any_instance_of(AdmissionRound).to receive(:start_of_cycle).and_return(user.created_at - 1.days)
            mock_promoted_admission_rounds = [double('AdmissionRound instance', {cohort_id: cohort.id, program_type: 'mba'}), double('AdmissionRound instance', {cohort_id: cohort.id, program_type: 'emba'})]
            mock_promoted_admission_rounds_event_attrs = {}
            allow(AdmissionRound).to receive(:promoted_admission_rounds).and_return(mock_promoted_admission_rounds)
            expect(AdmissionRound).to receive(:promoted_rounds_event_attributes).with(user.timezone, mock_promoted_admission_rounds).and_return(mock_promoted_admission_rounds_event_attrs)
            assert_event_logged(Cohort::AdmissionRoundJobThatDoesNotIncludeRelevantEmail) do |id, user_id, event_type, attrs|
                expect(event_type).to eq('some_event_type')
                expect(attrs).to eq(mock_promoted_admission_rounds_event_attrs)
            end
        end

        it "should not enqueue another job if users are found but the number of users found is less than #{Cohort::AdmissionRoundBatchJobMixin::BATCH_SIZE}" do
            instance = Cohort::AdmissionRoundJobThatDoesNotIncludeRelevantEmail.new
            relation = User.where(id: User.first.id)
            expect(relation.size).to be < Cohort::AdmissionRoundBatchJobMixin::BATCH_SIZE
            expect(User).to receive(:who_receives_pre_application_communications).and_return(relation)
            expect(Cohort::AdmissionRoundJobThatDoesNotIncludeRelevantEmail).not_to receive(:perform_later)
            instance.perform(cohort.id)
        end

        it "should enqueue another job if more may need to be processed" do
            instance = Cohort::AdmissionRoundJobThatDoesNotIncludeRelevantEmail.new
            user = User.first
            relation = User.where(id: user.id)
            # mock the size of the relation to make it seem like there are more users to need to be processed
            allow(relation).to receive(:size).and_return(Cohort::AdmissionRoundBatchJobMixin::BATCH_SIZE)
            expect(User).to receive(:who_receives_pre_application_communications).and_return(relation)
            expect(Cohort::AdmissionRoundJobThatDoesNotIncludeRelevantEmail).to receive(:perform_later).with(cohort.id, user.id, user.created_at.to_timestamp, false)
            instance.perform(cohort.id)
        end
    end

    describe "relevant_email" do

        it "should be signed_up_this_cycle_end_of_round when expected" do
            admission_round = promote_round(1)
            mock_signed_up_this_cycle(admission_round)
            assert_event_logged do |id, user_id, event_type, attrs|
                expect(attrs[:relevant_email]).to eq('signed_up_this_cycle_end_of_round')
            end
        end

        it "should be signed_up_this_cycle_end_of_cycle when expected" do
            admission_round = promote_round(2)
            mock_signed_up_this_cycle(admission_round)
            assert_event_logged do |id, user_id, event_type, attrs|
                expect(attrs[:relevant_email]).to eq('signed_up_this_cycle_end_of_cycle')
            end
        end

        it "should be signed_up_prev_cycle_end_of_round when expected" do
            admission_round = promote_round(1)
            mock_signed_up_last_cycle(admission_round)
            assert_event_logged do |id, user_id, event_type, attrs|
                expect(attrs[:relevant_email]).to eq('signed_up_prev_cycle_end_of_round')
            end
        end

        it "should be signed_up_prev_cycle_end_of_cycle when expected" do
            admission_round = promote_round(2)
            mock_signed_up_last_cycle(admission_round)
            assert_event_logged do |id, user_id, event_type, attrs|
                expect(attrs[:relevant_email]).to eq('signed_up_prev_cycle_end_of_cycle')
            end

        end

        def promote_round(index)
            admission_round = AdmissionRound.where(cohort_id: cohort.id, index: index).first
            expect(AdmissionRound).to receive(:promoted_admission_rounds).exactly(:once).and_return([admission_round])
            expect(AdmissionRound).to receive(:promoted_rounds_event_attributes).with(user.timezone, [admission_round]).and_return({})
            admission_round
        end

        def mock_signed_up_this_cycle(admission_round)
            expect(admission_round).to receive(:start_of_cycle).and_return(user.created_at - 1.days)
        end

        def mock_signed_up_last_cycle(admission_round)
            expect(admission_round).to receive(:start_of_cycle).and_return(user.created_at + 1.days)
        end
    end

    def assert_event_logged(klass = Cohort::AdmissionRoundJobThatIncludesRelevantEmail, &block)
        expect(User).to receive(:who_receives_pre_application_communications).and_return(User.where(id: user.id))
        expect(Event).to receive(:create_server_event!).once do |id, user_id, event_type, attrs|
            yield(id, user_id, event_type, attrs)
            mock_event = "mock_event_for_#{user_id}"
            expect(mock_event).to receive(:log_to_external_systems).with(nil, true, LogToCustomerIoJob::MASS_LOG_PRIORITY)
            mock_event
        end
        klass.perform_now(cohort.id, nil, nil)
    end
end
