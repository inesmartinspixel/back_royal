require 'spec_helper'

describe Cohort::OnPeriodActionJob do

    fixtures :users, :cohorts
    attr_reader :cohort, :period_index, :period

    before(:each) do
        @user = users(:accepted_mba_cohort_user)
        @cohort = cohorts(:published_mba_cohort)
        expect(cohort.accepted_users).not_to be_empty
        @period_index = 2 # 1-based
        @period = cohort.periods[period_index-1] # see note in code about 1-indexing of period indexes
    end

    it "should short-circuit if more than 24 hours after job should have run" do
        id = SecureRandom.uuid

        @period["actions"] = [{
            id: id,
            type: "expulsion_warning",
            rule: "requirements_not_met",
            days_offset_from_end: -1
        }, {
            id: SecureRandom.uuid,
            type: "expulsion",
            rule: "requirements_not_met",
            days_offset_from_end: 0
        }]
        @cohort.publish!

        allow(Time).to receive(:now).and_return(@cohort.relative_time_from_end_of_period(-1, 2) + 25.hours)

        expect(SecureRandom).to receive(:uuid).and_return("uuid").at_least(1).times
        event = "mock_event"
        expect(Event).not_to receive(:create_server_event!).with(
            "uuid",
            @user.id,
            'period_action:expulsion_warning',
            {
                cohort_id: @cohort.id,
                action_rule: "requirements_not_met",
                action_id: id,
                period_index: @period_index,
                period_style: @period["style"],
                period_title: @period["title"],
                program_type: @cohort.program_type,
                auto_expulsion_will_occur_at: @cohort.end_time_for_period(@period_index).to_timestamp
            }
        )
        expect(event).not_to receive(:log_to_external_systems)
        Cohort::OnPeriodActionJob.perform_now(@cohort.id, @period_index, id)
    end

    describe "expulsion_warning action" do
        it "should log expulsion warning event" do
            allow_any_instance_of(Cohort).to receive(:with_users_for_action).and_yield(@user)
            id = SecureRandom.uuid

            @period["actions"] = [{
                id: id,
                type: "expulsion_warning",
                rule: "requirements_not_met",
                days_offset_from_end: -1
            }, {
                id: SecureRandom.uuid,
                type: "expulsion",
                rule: "requirements_not_met",
                days_offset_from_end: 0
            }]
            @cohort.publish!

            allow_any_instance_of(Cohort::Version).to receive(:enrollment_deadline).and_return(Time.parse('2099/01/01'))
            expect(SecureRandom).to receive(:uuid).and_return("uuid").at_least(1).times
            event = "mock_event"
            next_exam_period_index = @cohort.periods.find_index { |period| period["style"] == 'exam' }
            next_exam_period = @cohort.periods[next_exam_period_index]
            allow_any_instance_of(Cohort::Version).to receive(:next_exam_period).and_return(next_exam_period)
            expect(Event).to receive(:create_server_event!).with(
                "uuid",
                @user.id,
                'period_action:expulsion_warning',
                {
                    cohort_id: @cohort.id,
                    program_type: @cohort.program_type,
                    period_index: @period_index,
                    period_style: @period["style"],
                    period_title: @period["title"],
                    period_description: @period["description"],
                    period_start: @cohort.start_time_for_period(@period_index).to_timestamp,
                    period_end: @cohort.end_time_for_period(@period_index).to_timestamp,
                    action_rule: "requirements_not_met",
                    action_id: id,
                    auto_expulsion_will_occur_at: @cohort.end_time_for_period(@period_index).to_timestamp,
                    course_entries: Cohort.get_course_entries_event_array(@period, @user),
                    before_enrollment_deadline: true, # this is true because we mocked the enrollment deadline in the future
                    num_periods_until_next_exam: ((next_exam_period_index + 1) - @period_index), # 1-based
                    next_exam_style: next_exam_period["exam_style"],
                    next_exam: Cohort.get_course_entries_event_array(next_exam_period, @user)[0]
                }
            ).and_return(event)
            expect(event).to receive(:log_to_external_systems)
            Cohort::OnPeriodActionJob.perform_now(@cohort.id, @period_index, id)
        end
    end

    describe "expulsion action" do
        it "should do expulsion" do
            allow_any_instance_of(Cohort).to receive(:with_users_for_action).and_yield(@user)
            id = SecureRandom.uuid

            @period["actions"] = [{
                id: id,
                type: "expulsion",
                rule: "requirements_not_met",
                days_offset_from_end: -1
            }]
            @cohort.publish!
            Cohort::OnPeriodActionJob.perform_now(@cohort.id, @period_index, id)
            expect(@user.reload.cohort_applications.first.status).to eq('expelled')
        end

        it "should log expulsion event" do
            allow_any_instance_of(Cohort).to receive(:with_users_for_action).and_yield(@user)
            allow(CohortApplication).to receive(:update_from_hash!)
            id = SecureRandom.uuid

            @period["actions"] = [{
                id: SecureRandom.uuid,
                type: "expulsion_warning",
                rule: "requirements_not_met",
                days_offset_from_end: -1
            }, {
                id: id,
                type: "expulsion",
                rule: "requirements_not_met",
                days_offset_from_end: 0
            }]
            @period["style"] = "exam"
            @period["exam_style"] = "final"
            @cohort.publish!

            allow_any_instance_of(Cohort::Version).to receive(:enrollment_deadline).and_return(Time.parse('2099/01/01'))
            expect(SecureRandom).to receive(:uuid).and_return("uuid").at_least(1).times
            event = "mock_event"
            expect(Event).to receive(:create_server_event!).with(
                "uuid",
                @user.id,
                'period_action:expulsion',
                {
                    cohort_id: @cohort.id,
                    program_type: @cohort.program_type,
                    period_index: @period_index,
                    period_style: @period["style"],
                    period_exam_style: "final",
                    period_title: @period["title"],
                    period_description: @period["description"],
                    course_entries: Cohort.get_course_entries_event_array(@period, @user),
                    before_enrollment_deadline: true, # this is true because we mocked the enrollment deadline in the future
                    period_start: @cohort.start_time_for_period(@period_index).to_timestamp,
                    period_end: @cohort.end_time_for_period(@period_index).to_timestamp,
                    action_rule: "requirements_not_met",
                    action_id: id
                }
            ).and_return(event)
            expect(event).to receive(:log_to_external_systems)
            Cohort::OnPeriodActionJob.perform_now(@cohort.id, @period_index, id)
        end
    end
end