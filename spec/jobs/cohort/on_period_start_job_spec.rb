require 'spec_helper'

describe Cohort::OnPeriodStartJob do

    fixtures :cohorts
    attr_reader :cohort, :period_index, :period

    before(:each) do
        @cohort = cohorts(:published_mba_cohort)
        expect(cohort.accepted_users).not_to be_empty
        @period_index = 2
        @period = cohort.periods[period_index-1] # see note in code about 1-indexing of period indexes

        @exercises = [{
            "id" => SecureRandom.uuid,
            "hours_offset_from_end" => -24,
            "message" => "@channel Test message. Test email mba@smart.ly. Test message.",
            "channel" => "#foo"
        }]
        @period["exercises"] = @exercises
        @cohort.publish!
    end

    it "should work" do
        allow_any_instance_of(User).to receive(:cohort_percent_complete).with(cohort.id).and_return(0.42324)
        allow_any_instance_of(Cohort).to receive(:expected_percent_complete).with(period_index-1).and_return(0.53474)

        assert_event_logged do |id, user_id, event_type, attrs|

            user = User.find(user_id)

            expect(event_type).to eq('cohort:period_started')
            expect(attrs[:cohort_id]).to eq(cohort.id)
            expect(attrs[:program_type]).to eq(cohort.program_type)
            expect(attrs[:period_index]).to eq(period_index)
            expect(attrs[:period_style]).to eq(period['style'])
            expect(attrs[:period_title]).to eq(period['title'])
            expect(attrs[:period_start]).to eq(cohort.start_time_for_period(period).to_timestamp)
            expect(attrs[:period_end]).to eq(cohort.end_time_for_period(period).to_timestamp)
            expect(attrs[:curriculum_percent_complete]).to eq(42)
            expect(attrs[:curriculum_expected_complete]).to eq(53)

            expect(attrs[:course_entries].size).to eq(period['stream_entries'].size)

            expect(attrs[:exercises]).to eq(@exercises)

            # in event
            course_entry = attrs[:course_entries].first

            # in period
            stream_entry = period['stream_entries'].first

            expect(course_entry[:required]).to eq(stream_entry['required'])
            expect(course_entry[:caption]).to eq(stream_entry['caption'])

            # sanity check
            expected_required_courses_completed_target = cohort.periods[0]['stream_entries'].select { |e| e['required']}.size
            expect(expected_required_courses_completed_target > 0).to be(true)

            expect(attrs[:required_courses_completed_actual]).to eq(user.required_courses_completed_overall(cohort.id))
            expect(attrs[:required_courses_completed_target]).to eq(expected_required_courses_completed_target) # see fixture builder
            expect(attrs[:required_courses_entire_schedule]).to eq(cohort.get_required_stream_count)

            # stream-specific info is tested in separate specs below, since it relies on locale
        end
    end

    describe "next exam info" do
        it "should be set if there is one" do
            next_exam_start = Time.now
            next_exam = Lesson::Stream.all_published.where(locale: 'en').first
            allow_any_instance_of(Cohort).to receive(:next_exam_info).with(period).and_return({
                locale_pack_id: next_exam.locale_pack_id,
                start_time: next_exam_start.to_timestamp
            })
            assert_event_logged do |id, user_id, event_type, attrs|
                expect(attrs[:next_exam_info][:english_title]).to eq(next_exam.title)
                expect(attrs[:next_exam_info][:start_time]).to eq(next_exam_start.to_timestamp)
            end
        end

        it "should be set if there ain't one" do
            allow_any_instance_of(Cohort).to receive(:next_exam_info).with(period).and_return(nil)
            assert_event_logged do |id, user_id, event_type, attrs|
                expect(attrs[:next_exam_info]).to be_nil
            end
        end
    end

    describe "relevant email" do

        it "should include the style and program type" do
            assert_event_logged do |id, user_id, event_type, attrs|
                expect(attrs[:relevant_email]).to eq("#{period['style']}_period_#{cohort.program_type}")
            end
        end

        it "should indicate the first period specially" do
            @period_index = 1
            assert_event_logged do |id, user_id, event_type, attrs|
                expect(attrs[:relevant_email]).to eq("first_period_#{cohort.program_type}")
            end
        end

    end

    it "should abort if running more than 1 day after the period start" do

        instance = Cohort::OnPeriodStartJob.new
        allow(Time).to receive(:now).and_return(cohort.start_time_for_period(period_index) + 25.hours)
        expect(instance).not_to receive(:log_weekly_update_events)
        instance.perform(cohort.id, period_index)

    end

    # FIXME: Should ideally be moved to cohort_spec
    describe "course entries" do

        it "should include english stream info if user has english locale" do
            user = remove_all_but_one_user
            user.update_attribute(:pref_locale, 'en')

            assert_course_entry_logged do |locale_pack_id, course_entry|

                stream = Lesson::Stream.all_published.where(locale: 'en', locale_pack_id: locale_pack_id).first

                expect(course_entry[:english_title]).to eq(stream.title)
                expect(course_entry[:title_in_users_locale]).to eq(stream.title)
                expect(course_entry[:english_url]).to eq(stream.entity_metadata.canonical_url)
                expect(course_entry[:url_in_users_locale]).to eq(stream.entity_metadata.canonical_url)
            end
        end

        it "should include localized stream info if user has non-english locale and localized stream is available" do
            user = remove_all_but_one_user

            # ensure there is a spanish stream
            locale_pack_id = cohort.periods[1]['stream_entries'][0]['locale_pack_id']
            english_stream = Lesson::Stream.where(locale: 'en', locale_pack_id: locale_pack_id).first
            spanish_stream = Lesson::Stream.where(locale: 'es', locale_pack_id: locale_pack_id).first
            if spanish_stream.nil?
                spanish_stream = Lesson::Stream.duplicate!(
                    User.first,
                    {locale: 'es', title: 'title'},
                    {
                        in_same_locale_pack: true,
                        duplicate_from_id: english_stream.id,
                        duplicate_from_updated_at: english_stream.updated_at
                    })
                spanish_stream.publish!
            end

            user.update_attribute(:pref_locale, 'es')

            assert_course_entry_logged do |locale_pack_id, course_entry|

                # sanity check. make sure we're dealing with the stream we set up above
                expect(english_stream.locale_pack_id).to eq(locale_pack_id)

                expect(course_entry[:english_title]).to eq(english_stream.title)
                expect(course_entry[:title_in_users_locale]).to eq(spanish_stream.title)
                expect(course_entry[:english_url]).to eq(english_stream.entity_metadata.canonical_url)
                expect(course_entry[:url_in_users_locale]).to eq(spanish_stream.entity_metadata.canonical_url)
            end
        end

        it "should include english stream info if user has non-english locale and localized stream is not available" do
            user = remove_all_but_one_user

            # ensure there is a spanish stream
            locale_pack_id = cohort.periods[1]['stream_entries'][0]['locale_pack_id']
            english_stream = Lesson::Stream.where(locale: 'en', locale_pack_id: locale_pack_id).first
            spanish_stream = Lesson::Stream.where(locale: 'es', locale_pack_id: locale_pack_id).first
            if spanish_stream.present?
                spanish_stream.unpublish!
            end

            user.update_attribute(:pref_locale, 'es')

            assert_course_entry_logged do |locale_pack_id, course_entry|

                # sanity check. make sure we're dealing with the stream we set up above
                expect(english_stream.locale_pack_id).to eq(locale_pack_id)

                expect(course_entry[:english_title]).to eq(english_stream.title)
                expect(course_entry[:title_in_users_locale]).to eq(english_stream.title)
                expect(course_entry[:english_url]).to eq(english_stream.entity_metadata.canonical_url)
                expect(course_entry[:url_in_users_locale]).to eq(english_stream.entity_metadata.canonical_url)
            end
        end
    end

    def remove_all_but_one_user
        users = @cohort.accepted_users.to_a
        user = users.pop

        # remove all but one user
        users.map(&:cohort_applications).flatten.map(&:destroy)
        @cohort.accepted_users.reload

        user
    end

    def assert_event_logged(&block)
        allow(Time).to receive(:now).and_return(cohort.start_time_for_period(period_index))

        expect(Event).to receive(:create_server_event!).exactly(cohort.accepted_users.count).times do |id, user_id, event_type, attrs|

            yield(id, user_id, event_type, attrs)
            mock_event = "mock_event_for_#{user_id}"
            expect(mock_event).to receive(:log_to_external_systems).with(no_args)
            mock_event
        end

        Cohort::OnPeriodStartJob.perform_now(cohort.id, period_index)
    end

    def assert_course_entry_logged(&block)
        assert_event_logged do |id, user_id, event_type, attrs|

            # in event
            course_entry = attrs[:course_entries].first

            # in period
            stream_entry = period['stream_entries'].first

            yield(stream_entry['locale_pack_id'], course_entry)

        end

    end
end