require 'spec_helper'

describe Cohort::UpdateFinalScoresJob do

    fixtures :cohorts

    it "should only save users who have their final_score changed" do
        cohort = cohorts(:published_mba_cohort)
        cohort_applications = cohort.cohort_applications.where(status: 'accepted')
        expect(cohort_applications.size).to be > 1

        allow_any_instance_of(CohortApplication).to receive(:set_final_score) do |cohort_application|
            if cohort_application.id == cohort_applications.first.id
                cohort_application.final_score = cohort_application.final_score.nil? ? 0.42 : cohort_application.final_score + 0.1
            end
        end

        expect_any_instance_of(CohortApplication).to receive(:save!).exactly(1).times

        Cohort::UpdateFinalScoresJob.perform_now(cohort.id)

    end

end