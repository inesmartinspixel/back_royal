require 'spec_helper'

describe Cohort::DecisionDayEnrollmentAgreementsJob do

    fixtures :cohorts

    before(:each) do
        @instance_class = Cohort::DecisionDayEnrollmentAgreementsJob
        @cohort = Cohort.joins(:accepted_users).detect{ |c| c.accepted_users.size > 1 }
        allow(Cohort).to receive(:find_by_id).and_return(@cohort)
    end

    it "should call queue_ensure_enrollment_agreement_job_if_necessary for every accepted or pre_accepted application" do
        expect(@cohort).to receive(:requires_enrollment_agreement?).and_return(true)
        expect(@cohort).to receive(:enrollment_agreement_template_id).and_return(SecureRandom.uuid)

        accepted_applications = @cohort.cohort_applications.where(status: 'accepted')
        pre_accepted_applications = @cohort.cohort_applications.where(status: 'pre_accepted')
        application_ids = (accepted_applications + pre_accepted_applications).pluck(:id)

        calls = []
        allow_any_instance_of(CohortApplication).to receive(:queue_ensure_enrollment_agreement_job_if_necessary) do |application|
            calls << application.id
        end

        @instance_class.perform_now(@cohort.id)
        expect(calls.size).to eq(application_ids.size)
        expect(calls).to match_array(application_ids)
    end

    it "should return if cohort does not require enrollment agreement" do
        expect(@cohort).to receive(:requires_enrollment_agreement?).and_return(false)
        expect(@cohort).not_to receive(:cohort_applications)
        @instance_class.perform_now(@cohort.id)
    end

    it "should return if cohort has to enrollment_agreement_template_id" do
        expect(@cohort).to receive(:requires_enrollment_agreement?).and_return(true)
        expect(@cohort).to receive(:enrollment_agreement_template_id).and_return(nil)
        expect(@cohort).not_to receive(:cohort_applications)
        @instance_class.perform_now(@cohort.id)
    end


end