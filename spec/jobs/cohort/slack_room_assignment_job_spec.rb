require 'spec_helper'

describe Cohort::SlackRoomAssignmentJob do

    fixtures(:cohorts)

    # These end-to-end specs test the whole thing, but they are too slow
    # to run all the time.  I don't want to remove them though, because
    # they are useful when making changes here.  So I added the env
    # var so you can choose to run them
    if ENV['RUN_SLACK_ROOM_ASSIGNMENT_INTEGRATION_SPECS']
        describe "end-to-end" do

            it "should work with 2 rooms" do
                cohort = cohorts(:cohort_for_slack_room_assignment_spec)
                expect(cohort.slack_rooms.count).to be(2)
                search = Cohort::SlackRoomAssignmentJob::TabuSearch.new(cohort.slack_rooms.order('title').map(&:id),
                    cohort.cohort_applications.where(status: ['pre_accepted','accepted'])
                ).exec

                expect(search.best_state[:rooms][0][:cohort_applications].size).to be_within(1).of (search.best_state[:rooms][0][:cohort_applications].size)

                [0, 0.5, 1].each do |val|
                    expect(search.stats[:sex][0][val]).to be_within(2).of(search.stats[:sex][1][val])
                end
                [0, 1].each do |val|
                    expect(search.stats[:full_scholarship][0][val]).to be_within(1).of(search.stats[:full_scholarship][1][val])
                end

                search.stats[:locations].each do |entry|
                    counts = entry[1]
                    total = counts[0] + counts[1]
                    # when there are 5 or fewer people in a location, they
                    # should end up in 1 group
                    if total <= 5
                        expect(counts.sort).to eq([0, total])

                    # when there are more than 5, they should be split up
                    else
                        expect(counts[0]).to be_within(1).of(counts[1])
                    end
                end

            end

            it "should work with 3 rooms" do

                cohort = cohorts(:cohort_for_slack_room_assignment_spec)
                i = cohort.slack_rooms.size
                while cohort.slack_rooms.size < 3
                    i+=1
                    CohortSlackRoom.create!(
                        title: "Room #{i}",
                        url: "http://path/to/slack/room/#{i}/for/#{cohort.id}",
                        admin_token: "token_for_slack_room_#{i}_#{cohort.id}",
                        cohort_id: cohort.id
                    )
                end
                expect(cohort.slack_rooms.count).to be(3)
                search = Cohort::SlackRoomAssignmentJob::TabuSearch.new(cohort.slack_rooms.order('title').map(&:id),
                    cohort.cohort_applications.where(status: ['pre_accepted','accepted'])
                ).exec

                expect(search.best_state[:rooms][0][:cohort_applications].size).to be_within(1).of (search.best_state[:rooms][0][:cohort_applications].size)


                [0, 0.5, 1].each do |val|
                    expect(search.stats[:sex][0][val]).to be_within(2).of(search.stats[:sex][1][val])
                    expect(search.stats[:sex][0][val]).to be_within(2).of(search.stats[:sex][2][val])
                    expect(search.stats[:sex][1][val]).to be_within(2).of(search.stats[:sex][2][val])
                end
                [0, 1].each do |val|
                    expect(search.stats[:full_scholarship][0][val]).to be_within(1).of(search.stats[:full_scholarship][1][val])
                    expect(search.stats[:full_scholarship][0][val]).to be_within(1).of(search.stats[:full_scholarship][2][val])
                    expect(search.stats[:full_scholarship][1][val]).to be_within(1).of(search.stats[:full_scholarship][2][val])
                end

                search.stats[:locations].each do |entry|
                    counts = entry[1]
                    total = counts[0] + counts[1] + counts[2]

                    if total < 3
                        expect(counts.sort).to eq([0,0,total])
                    else

                        # This doesn't at all assert that we're maximizing variety of
                        # locations among the different groups.  It only checks that
                        # no rooms have fewer than 3 people from the location
                        expect(counts.reject { |c| c == 0 }.min).to be >= 3
                    end
                end
            end

            describe "assign_one_application" do

                it "should assign a new user to the same room with other people from the same location" do
                    cohort = cohorts(:cohort_for_slack_room_assignment_spec)
                    cohort.cohort_applications.update_all(cohort_slack_room_id: nil)
                    applications_from_switzerland = cohort.cohort_applications.select do |cohort_application|
                        next unless cohort_application&.user&.career_profile&.place_details
                        next unless cohort_application.user.career_profile.place_details['country']
                        cohort_application.user.career_profile.place_details['country']['long'] == "Switzerland"
                    end
                    expect(applications_from_switzerland.size).to eq(3) # sanity check

                    # register all applications except for one.  We will auto_assign that
                    # one later and make sure the right thing happens
                    allow_any_instance_of(CohortApplication).to receive(:has_full_scholarship?).and_return(true)
                    application = applications_from_switzerland.first
                    cohort.cohort_applications.where(status: ['pre_accepted', 'accepted']).where.not(id: application.id).each do |app|
                        app.update!(
                            total_num_required_stripe_payments: 0,
                            registered: true)
                    end
                    expect(application.registered).to be(false)

                    # create an auto-assignment and assign each application to
                    # the room that the auto-assignment chose
                    Cohort::SlackRoomAssignmentJob.perform_now(cohort.id)
                    cohort.reload.slack_room_assignment.slack_room_assignments.each do |entry|
                        CohortApplication.where(id: entry['cohort_application_ids']).each do |app|
                            app.update!(cohort_slack_room_id: entry['slack_room_id'])
                        end
                    end

                    # sanity check.  2 of the 3 applications_from_switzerland should
                    # now have slack rooms assigned.  the third one (which is the `application`)
                    applications_from_switzerland.map(&:reload)
                    expect(applications_from_switzerland[0].cohort_slack_room_id).to be_nil
                    expect(applications_from_switzerland[1].cohort_slack_room_id).not_to be_nil
                    expect(applications_from_switzerland[2].cohort_slack_room_id).not_to be_nil

                    # Simulate what happens when a user is deferred into an existing cohort.  In that
                    # case, all the other users in the cohort have slack rooms assigned, and we just
                    # pick a room for this user.
                    # We could test this here without running update_from_hash!, but this is
                    # and end_to_end test, so let's cover as much as possible
                    CohortApplication.update_from_hash!({
                        id: application.id,
                        cohort_slack_room_id: 'AUTO_ASSIGN'
                    })

                    rooms_for_swiss_users = applications_from_switzerland.map(&:reload).map(&:cohort_slack_room_id).uniq.compact
                    expect(rooms_for_swiss_users.size).to eq(1)

                end

            end

        end
    end

    describe "perform" do

        it "should run assignment and save a record" do
            cohort = cohorts(:published_mba_cohort)
            expect(cohort.slack_rooms.size).to eq(3)
            expect(cohort.slack_room_assignment).to be_nil

            expected_applications = cohort.cohort_applications.where(status: ['accepted', 'pre_accepted']).to_a
            expect(expected_applications.size).to be >= 6
            expected_lists = [
                expected_applications.slice(0, 2),
                expected_applications.slice(2, 2),
                expected_applications.slice(4, 2)
            ]

            expected_assignments = []
            expected_lists.each_with_index do |cohort_applications, i|
                expected_assignments << {
                    slack_room_id: cohort.slack_rooms[i].id,
                    cohort_application_ids: cohort_applications.map(&:id)
                }.as_json
            end

            locations_by_user_id = {'a' => 'b'}

            expect_any_instance_of(Cohort::SlackRoomAssignmentJob::TabuSearch).to receive(:exec) do |instance|
                expect(instance.cohort_applications).to match_array(expected_applications)
                expect(instance.slack_room_ids).to match_array(cohort.slack_rooms.pluck(:id))
                i = -1
                instance.best_state = {
                    rooms: expected_lists.map { |cohort_applications|
                        {
                            cohort_applications: cohort_applications,
                            slack_room_id: instance.slack_room_ids[i += 1]
                        }
                    }
                }
                instance.locations_by_user_id = locations_by_user_id
                instance
            end

            Cohort::SlackRoomAssignmentJob.perform_now(cohort.id)

            expect(cohort.association(:slack_room_assignment).reload).not_to be_nil
            expect(cohort.slack_room_assignment.slack_room_assignments).to match_array(expected_assignments)
            expect(cohort.slack_room_assignment.location_assignments).to eq(locations_by_user_id)

        end

        it "should filter out non-registered if supports_registration_deadline?" do
            cohort = cohorts(:published_mba_cohort)
            allow_any_instance_of(Cohort).to receive(:supports_registration_deadline?).and_return(true)

            applications = cohort.cohort_applications.where(status: ['accepted', 'pre_accepted']).to_a
            expected_applications = applications.select(&:registered)
            while expected_applications.size < 6
                app = applications.pop
                app.update(registered: true)
                expected_applications << app
            end

            expect_any_instance_of(Cohort::SlackRoomAssignmentJob::TabuSearch).to receive(:exec) do |instance|
                expect(instance.cohort_applications).to match_array(expected_applications)

                # We just need to assert that the search object was set up appropriately.  We
                # don't actually care what gets set up by the exec method here
                instance.best_state = {rooms: []}
                instance
            end

            Cohort::SlackRoomAssignmentJob.perform_now(cohort.id)
        end

        it "should raise if a cohort application changes while running" do
            assert_needs_to_run_again do |cohort, applications|
                applications.first.destroy
            end
        end

        it "should raise if slack rooms change while running" do
            assert_needs_to_run_again do |cohort, applications|
                CohortSlackRoom.create!(cohort: cohort, title: SecureRandom.uuid, url: SecureRandom.uuid, admin_token:SecureRandom.uuid)
            end
        end

        def assert_needs_to_run_again(&block)

            cohort = cohorts(:published_mba_cohort)
            expect(cohort.slack_rooms.size).to eq(3)

            applications = cohort.cohort_applications.where(status: ['accepted', 'pre_accepted']).to_a
            expect(applications.size).to be >= 2

            expect_any_instance_of(Cohort::SlackRoomAssignmentJob).to receive(:do_search) do
                yield(cohort, applications)

                [:slack_room_assignments, :locations_by_user_id]
            end

            expect {
                Cohort::SlackRoomAssignmentJob.perform_now(cohort.id)
            }.to raise_error("Need to re-run slack room assignment because something changed while it was running.")

            expect(cohort.association(:slack_room_assignment).reload).to be_nil
        end

        describe "assign_one_application" do
            attr_accessor :cohort, :job, :slack_room_ids

            before(:each) do
                @cohort = cohorts(:cohort_for_slack_room_assignment_spec)
                @slack_room_ids = cohort.slack_rooms.map(&:id)
                @job = Cohort::SlackRoomAssignmentJob.new
            end

            it "should not duplicate the application in the list provided to the search if it is already there" do
                cohort_applications = cohort.cohort_applications
                cohort_application = cohort_applications.first

                # setup
                expect(job).to receive(:get_cohort_and_applications).with(cohort.id).and_return([cohort, slack_room_ids, cohort_applications])
                expect(job).to receive(:raise_if_too_many_unassigned_applications)
                    .with(cohort_applications, cohort_application, slack_room_ids)

                # assert
                expect_do_search(cohort_applications, cohort_application)
                job.assign_one_application(cohort_application)
            end

            it "should add the application to the list provided to the search if it is not already there" do
                cohort_applications = cohort.cohort_applications
                cohort_application = cohort_applications.first
                cohort_applications_without = cohort_applications.where.not(id: cohort_application.id)
                cohort_applications_list = cohort_applications_without.to_a + [cohort_application]

                # setup
                expect(job).to receive(:get_cohort_and_applications).with(cohort.id).and_return([cohort, slack_room_ids, cohort_applications_without])
                expect(job).to receive(:raise_if_too_many_unassigned_applications)
                    .with(cohort_applications_list, cohort_application, slack_room_ids)

                # assert
                expect_do_search(cohort_applications_list, cohort_application)
                job.assign_one_application(cohort_application)
            end

            it "should get the result from the search and set it on the application" do
                cohort_applications = cohort.cohort_applications
                cohort_application = cohort_applications.first

                # setup
                expect(job).to receive(:get_cohort_and_applications).with(cohort.id).and_return([cohort, slack_room_ids, cohort_applications])
                expect(job).to receive(:raise_if_too_many_unassigned_applications)
                slack_room_id = slack_room_ids[1]

                # assert
                expect_do_search(cohort_applications, cohort_application, slack_room_id)
                job.assign_one_application(cohort_application)
                expect(cohort_application.cohort_slack_room_id).to eq(slack_room_id)
            end

            def expect_do_search(cohort_applications, cohort_application, slack_room_id=nil)
                slack_room_id ||= slack_room_ids.first
                slack_room_assignments = [{slack_room_id: slack_room_id, cohort_application_ids: [cohort_application.id]}]
                expect(job).to receive(:do_search).with(
                    cohort,
                    cohort.slack_room_ids,
                    cohort_applications.to_a
                ).and_return([slack_room_assignments, :ignore])
            end

            # this block is inside the assign_one_application block since it is a private
            # method that is only called from inside of assign_one_application
            describe 'raise_if_too_many_unassigned_applications' do

                attr_accessor :job, :cohort_applications, :cohort_application, :slack_room_ids

                before(:each) do
                    @job = Cohort::SlackRoomAssignmentJob.new
                    @cohort_applications = CohortApplication.limit(30).to_a
                    expect(cohort_applications.size).to eq(30)
                    @cohort_application = cohort_applications.first
                    @slack_room_ids = [SecureRandom.uuid, SecureRandom.uuid]
                end

                it "should raise if too may unassigned" do
                    cohort_applications.slice(0, 5).each { |ca| ca.cohort_slack_room_id = SecureRandom.uuid }
                    cohort_applications.slice(5, 99999).each { |ca| ca.cohort_slack_room_id = nil }

                    error = expect {
                        result = job.raise_if_too_many_unassigned_applications(cohort_applications, cohort_application, slack_room_ids)
                    }.to raise_error { |error|
                        expect(error.message).to eq("Cannot assign a single application to a slack room when there are many unassigned applications")
                        expect(error.raven_options).to eq({
                            extra: {
                                unassigned_count: cohort_applications.size - 5,
                                application: cohort_application.id,
                                cohort_id: cohort_application.cohort_id,
                                cohort: cohort_application.cohort.name
                            }
                        })
                    }
                end

                it "should not raise without too many unassigned" do
                    cohort_applications.slice(0, 5).each { |ca| ca.cohort_slack_room_id = nil }
                    cohort_applications.slice(5, 99999).each { |ca| ca.cohort_slack_room_id = SecureRandom.uuid }
                    expect(cohort_application.cohort_slack_room_id).to be_nil

                    expect(Raven).not_to receive(:capture_exception)

                    expect {
                        job.raise_if_too_many_unassigned_applications(cohort_applications, cohort_application, slack_room_ids)
                    }.not_to raise_error

                end

            end
        end

        describe TabuSearch do

            describe "map_applications_to_locations" do
                it "should work" do
                    cohort = cohorts(:cohort_for_slack_room_assignment_spec)
                    applications_around_san_jose = get_applications_around_san_jose(cohort)
                    applications_in_america = get_applications_in_america(cohort)
                    applications_in_the_boondocks = get_applications_in_the_boondocks(cohort)
                    applications_in_canada = get_applications_in_canada(cohort).limit(2)
                    instance = Cohort::SlackRoomAssignmentJob::TabuSearch.new(['room1', 'room2'],
                        applications_around_san_jose +
                        applications_in_america +
                        applications_in_the_boondocks +
                        applications_in_canada
                    )

                    # When there are more than 5 people in a city, they should get grouped in the city
                    applications_around_san_jose.each do |application|
                        expect(instance.locations_by_user_id[application.user_id]).to eq(["United States", "San Jose, CA"])
                    end

                    # When there are people in a country, but in different cities and there
                    # are not enough in any one city, then should get grouped by country and
                    # labeled 'other cities'
                    applications_in_america.each do |application|
                        expect(instance.locations_by_user_id[application.user_id]).to eq(["United States", '(other cities)'])
                    end

                    # When someone is in a country that does not have any cities,
                    # they should be grouped by country and labeld 'all cities'
                    applications_in_canada.each do |application|
                        expect(instance.locations_by_user_id[application.user_id]).to eq(["Canada", '(all cities)'])
                    end

                    # When someone is in a country all by zirself, ze should be
                    # put in the "Other Countries group"
                    applications_in_the_boondocks.each do |application|
                        expect(instance.locations_by_user_id[application.user_id]).to eq(["Other Countries", ''])
                    end
                end

                it "should group based on country code, not country name" do
                    cohort = cohorts(:cohort_for_slack_room_assignment_spec)
                    applications = get_applications_in_america(cohort).limit(3)
                    english = {"locality"=>{"short"=>"Reinach", "long"=>"Reinach"},
                        "administrative_area_level_2"=>{"short"=>"Kulm District", "long"=>"Kulm District"},
                        "administrative_area_level_1"=>{"short"=>"AG", "long"=>"Aargau"},
                        "country"=>{"short"=>"CH", "long"=>"Switzerland"},
                        "lat"=>47.2580272,
                        "lng"=>8.179928000000018,
                        "formatted_address"=>"Reinach, Switzerland",
                        "utc_offset"=>120}
                    some_other_language = {"locality"=>{"short"=>"Ženeva", "long"=>"Ženeva"},
                        "administrative_area_level_2"=>{"short"=>"Genève", "long"=>"Genève"},
                        "administrative_area_level_1"=>{"short"=>"GE", "long"=>"Ženeva"},
                        "country"=>{"short"=>"CH", "long"=>"Švica"},
                        "lat"=>46.2043907,
                        "lng"=>6.143157699999961,
                        "formatted_address"=>"Ženeva, Švica",
                        "utc_offset"=>120}
                    applications[0].user.career_profile.update(place_details: english)
                    applications[1].user.career_profile.update(place_details: english)
                    applications[2].user.career_profile.update(place_details: some_other_language)
                    instance = Cohort::SlackRoomAssignmentJob::TabuSearch.new(['room1', 'room2'], applications)

                    applications.each do |application|
                        expect(instance.locations_by_user_id[application.user_id]).to eq(["Switzerland", '(all cities)'])
                    end
                end
            end

            describe "generate_initial_state" do

                it "should copy ids onto rooms" do
                    cohort = cohorts(:cohort_for_slack_room_assignment_spec)
                    instance = Cohort::SlackRoomAssignmentJob::TabuSearch.new(['room1', 'room2'],
                        get_applications_in_canada(cohort).limit(2) +
                        get_applications_in_america(cohort).limit(2) +
                        get_applications_in_the_boondocks(cohort).limit(2)
                    )
                    initial_state = instance.generate_initial_state
                    expect(initial_state[:rooms][0][:slack_room_id]).to eq('room1')
                    expect(initial_state[:rooms][1][:slack_room_id]).to eq('room2')
                end

                it "should assign locations with small numbers to different slack rooms when there are 2 rooms" do
                    cohort = cohorts(:cohort_for_slack_room_assignment_spec)
                    instance = Cohort::SlackRoomAssignmentJob::TabuSearch.new(['room1', 'room2'],
                        get_applications_in_canada(cohort).limit(2) +
                        get_applications_in_america(cohort).limit(2) +
                        get_applications_in_the_boondocks(cohort).limit(2)
                    )
                    initial_state = instance.generate_initial_state

                    lists = initial_state[:rooms].map { |g| g[:cohort_applications] }

                    locations_by_room = lists.map { |list|
                        list.map { |cohort_application| instance.locations_by_user_id[cohort_application.user_id] }.uniq
                    }

                    # we should have one location assigned to one room and the
                    # other two locations assigned to the other
                    expect(lists.map(&:size).sort).to eq([2,4])
                    expect(locations_by_room.map(&:size).sort).to eq([1,2])

                    # none of these people from small cities can be moved
                    expect(instance.movable_vectors).to be_empty
                end

                it "should split 6 people up into two rooms" do
                    cohort = cohorts(:cohort_for_slack_room_assignment_spec)
                    instance = Cohort::SlackRoomAssignmentJob::TabuSearch.new(['room1', 'room2'],
                        get_applications_in_canada(cohort).limit(2) +
                        get_applications_in_america(cohort).limit(6)
                    )
                    initial_state = instance.generate_initial_state

                    lists = initial_state[:rooms].map { |g| g[:cohort_applications] }

                    locations_by_room = lists.map { |list|
                        list.map { |cohort_application| instance.locations_by_user_id[cohort_application.user_id] }.uniq
                    }

                    # we should 3 americans and 2 candians in 1 room and
                    # 3 americans in the other
                    expect(lists.map(&:size).sort).to eq([3,5])
                    expect(locations_by_room.map(&:size).sort).to eq([1,2])

                    # none of these people from small cities can be moved
                    expect(instance.movable_vectors).to be_empty
                end

                it "should assign locations with small numbers to different slack rooms when there are 3 rooms" do
                    cohort = cohorts(:cohort_for_slack_room_assignment_spec)
                    instance = Cohort::SlackRoomAssignmentJob::TabuSearch.new(['room1', 'room2', 'room3'],
                        get_applications_in_canada(cohort).limit(2) +
                        get_applications_in_america(cohort).limit(7) +
                        get_applications_in_the_boondocks(cohort).limit(3)
                    )
                    initial_state = instance.generate_initial_state
                    lists = initial_state[:rooms].map { |g| g[:cohort_applications] }

                    # First the 2 people from Canada and the 3 from
                    # Other countries are each placed in separate rooms.
                    # The 3 people from America are placed in the empty room,
                    # and the remaining 4 are placed with the 2 from Canada
                    list_1 = lists.detect do |list|
                        locations = list.map { |cohort_application| instance.locations_by_user_id[cohort_application.user_id] }.uniq
                        list.size == 6 && locations.sort_by(&:first) == [['Canada', '(all cities)'], ['United States', '(all cities)']]
                    end
                    expect(list_1).not_to be_nil, "No list found with 4 people from America and 2 from Canada"

                    list_2 = lists.detect do |list|
                        locations = list.map { |cohort_application| instance.locations_by_user_id[cohort_application.user_id] }.uniq
                        list.size == 3 && locations == [["Other Countries", ""]]
                    end
                    expect(list_2).not_to be_nil, "No list found with 3 people from Other Countries"

                    list_3 = lists.detect do |list|
                        locations = list.map { |cohort_application| instance.locations_by_user_id[cohort_application.user_id] }.uniq
                        list.size == 3 && locations == [['United States', '(all cities)']]
                    end
                    expect(list_3).not_to be_nil, "No list found with 3 people from America"
                    # none of these people from small cities can be moved
                    expect(instance.movable_vectors).to be_empty
                end

                it "should split up users from locations with many users" do
                    cohort = cohorts(:cohort_for_slack_room_assignment_spec)
                    instance = Cohort::SlackRoomAssignmentJob::TabuSearch.new(['room1', 'room2'],
                        get_applications_around_san_jose(cohort).limit(8) +
                        get_applications_in_canada(cohort).limit(2)
                    )
                    initial_state = instance.generate_initial_state

                    lists = initial_state[:rooms].map { |g| g[:cohort_applications] }

                    # The number of users in each room should be balanced
                    expect(lists.map(&:size).sort).to eq([5,5])

                    locations_by_room = lists.map { |list|
                        list.map { |cohort_application| instance.locations_by_user_id[cohort_application.user_id] }.uniq.sort
                    }

                    # one of the rooms should have 2 people from canada and
                    # 2 from San Jose.  The other should have the other 4 people
                    # from San Jose
                    expect(locations_by_room).to match_array([
                        [['United States', 'San Jose, CA']],
                        [['Canada', '(all cities)'], ['United States', 'San Jose, CA']]
                    ])

                    # the San Jose people can be moved, but not the Canadians
                    expect(instance.movable_vectors.map { |v| v[:location]}.uniq).to eq([['United States', 'San Jose, CA']])
                end

                it "should fix accepted candidates into the rooms they have been assigned" do
                    cohort = cohorts(:cohort_for_slack_room_assignment_spec)
                    cohort_applications = get_applications_around_san_jose(cohort).limit(4).to_a
                    cohort_applications.each_with_index do |ca, i|
                        ca.cohort_slack_room_id = cohort.slack_rooms[i % 2].id
                        if i < 2
                            ca.status = 'accepted'
                        end
                    end
                    instance = Cohort::SlackRoomAssignmentJob::TabuSearch.new(cohort.slack_rooms.pluck(:id), cohort_applications)
                    state = instance.generate_initial_state

                    expeted_room_for_app_0 = state[:rooms].detect { |r| r[:slack_room_id] == cohort_applications[0].cohort_slack_room_id}
                    expect(expeted_room_for_app_0[:cohort_applications]).to include(cohort_applications[0])
                    expeted_room_for_app_0 = state[:rooms].detect { |r| r[:slack_room_id] == cohort_applications[1].cohort_slack_room_id}
                    expect(expeted_room_for_app_0[:cohort_applications]).to include(cohort_applications[1])
                end
            end

            describe "possible_moves" do
                it "should work" do
                    cohort_applications = CohortApplication.limit(6).to_a
                    expect(cohort_applications.size).to eq(6)

                    # mock out vectorize so we can manually room these cohort applications
                    # into different vectors
                    expect_any_instance_of(Cohort::SlackRoomAssignmentJob::TabuSearch).to receive(:vectorize_application).at_least(1).times do |instance, cohort_application|
                        index = cohort_applications.index(cohort_application)
                        {
                            0 => 'vector1',
                            1 => 'vector1',
                            2 => 'vector2',
                            3 => 'vector2',
                            4 => 'vector3',
                            5 => 'vector6'
                        }[index]
                    end
                    instance = Cohort::SlackRoomAssignmentJob::TabuSearch.new(['room1', 'room2'], cohort_applications)

                    # mock out the stuff that would be set up by generate_initial_state,
                    # putting the first 3 applications in one room and the
                    # next 3 in another room.  Mark vector2 as unmovable
                    state = create_state(instance, [
                        cohort_applications.slice(0, 3),
                        cohort_applications.slice(3, 3)
                    ])
                    instance.movable_vectors = Set.new(['vector1', 'vector3', 'vector6'])

                    # Get the moves and check that all the expected swaps are
                    # included
                    moves = instance.possible_moves(state)
                    swaps = moves.map do |move|
                        i = 0
                        move[:changes].map do |change|
                            expect(change[:current_room_index]).to eq(i)
                            expect(change[:new_room_index]).to eq(i == 1 ? 0 : 1)
                            i += 1
                            swap = instance.vectorize_application(change[:cohort_application])
                            swap
                        end
                    end

                    expect(swaps).to match_array([
                        # vector1 in room1 should be swappable with
                        # vector3 or vector6 in room2.  vector2 is not
                        # movable, so it is not included
                        ['vector1', 'vector3'],
                        ['vector1', 'vector6'],
                    ])

                end

                it "should not move accepted applications" do
                    cohort = cohorts(:cohort_for_slack_room_assignment_spec)

                    # get an application which is the only one of its vector to
                    # remove the randomness of it being selected
                    application = get_applications_in_canada(cohort).first
                    cohort_applications = get_applications_in_the_boondocks(cohort).limit(6) + [application]

                    instance = Cohort::SlackRoomAssignmentJob::TabuSearch.new(cohort.slack_rooms.pluck(:id), cohort_applications)
                    state = instance.generate_initial_state

                    # sanity check.  If the application is not accepted, it is movable
                    movable_applications = instance.possible_moves(state).map { |move| move[:changes] }.flatten.map { |change| change[:cohort_application] }.uniq
                    expect(movable_applications).to include(application)

                    # once we mark it as accepted it should no longer be movable
                    application.status = 'accepted'
                    movable_applications = instance.possible_moves(state).map { |move| move[:changes] }.flatten.map { |change| change[:cohort_application] }.uniq
                    expect(movable_applications).not_to include(application)

                end
            end

            describe "result_of_move" do
            it "should move applications" do
                    cohort_applications = CohortApplication.limit(2)
                    instance = Cohort::SlackRoomAssignmentJob::TabuSearch.new(['room1', 'room2'], cohort_applications)
                    state = create_state(instance, [
                        [cohort_applications[0]],
                        [cohort_applications[1]]
                    ])
                    result_state = instance.result_of_move(state, {
                        :changes => [
                            {
                                current_room_index: 0,
                                new_room_index: 1,
                                cohort_application: cohort_applications[0]
                            },
                            {
                                current_room_index: 1,
                                new_room_index: 0,
                                cohort_application: cohort_applications[1]
                            }
                        ]
                    })

                    # id should be copied over
                    expect(state[:rooms][0][:slack_room_id]).to eq('room1')
                    expect(state[:rooms][1][:slack_room_id]).to eq('room2')

                    # original state should be unchanged
                    expect(state[:rooms][0][:cohort_applications]).to eq([cohort_applications[0]])
                    expect(state[:rooms][1][:cohort_applications]).to eq([cohort_applications[1]])

                    # result state should have applications swapped
                    expect(result_state[:rooms][0][:cohort_applications]).to eq([cohort_applications[1]])
                    expect(result_state[:rooms][1][:cohort_applications]).to eq([cohort_applications[0]])

                end
            end

            # testing these two methods together since they need to agree on how the
            # recent_moves array works
            describe "after_move/move_is_tabu?" do
                it "should work" do
                    instance = Cohort::SlackRoomAssignmentJob::TabuSearch.new(['room1', 'room2'], [])
                    expect(instance).to receive(:vectorize_application).at_least(1).times do |cohort_application|
                        cohort_application
                    end

                    move_application_from_0_to_1 = {
                                                        changes: [
                                                            {
                                                                current_room_index: 0,
                                                                new_room_index: 1,
                                                                cohort_application: :moved_cohort_application
                                                            }
                                                        ]
                                                    }

                    move_application_from_1_to_0 = {
                                                        changes: [
                                                            {
                                                                current_room_index: 1,
                                                                new_room_index: 0,
                                                                cohort_application: :moved_cohort_application
                                                            }
                                                        ]
                                                    }

                    move_another_application_from_1_to_0 = {
                                                        changes: [
                                                            {
                                                                current_room_index: 1,
                                                                new_room_index: 0,
                                                                cohort_application: :another_cohort_application
                                                            }
                                                        ]
                                                    }

                    # move an application from room 0 to room 1
                    instance.after_move(:some_state, {move: move_application_from_0_to_1})

                    # it should now be tabu to move that application right out of room 1 again
                    expect(instance.move_is_tabu?(:some_state, move_application_from_1_to_0)).to be(true)

                    # it should not be tabu to move another application with the same vector representation
                    # from room 0 to room 1
                    expect(instance.move_is_tabu?(:some_state, move_application_from_0_to_1)).to be(false)

                    # it should not be tabu to move another application out of room 1
                    expect(instance.move_is_tabu?(:some_state, move_another_application_from_1_to_0)).to be(false)

                    # for four more moves, it should still be tabu to move this application out of
                    # room 1
                    4.times do
                        instance.after_move(:some_state, {move: {changes: []}})
                        expect(instance.move_is_tabu?(:some_state, move_application_from_1_to_0)).to be(true)
                    end

                    # but after 5 moves it is ok again
                    instance.after_move(:some_state, {move: {changes: []}})
                    expect(instance.move_is_tabu?(:some_state, move_application_from_1_to_0)).to be(false)
                end
            end

            describe "value_of_state" do

                # FIXME: this really should be tested, but I'm confident it works
                # right now from testing on real data, and I've got to get this out

            end

        end

        def create_state(instance, lists)
            i = 0
            state = {rooms: lists.map { |cohort_applications|
                slack_room_id = instance.slack_room_ids[i]
                i += 1
                {
                    cohort_applications: cohort_applications,
                    slack_room_id: slack_room_id
                }
            }}
            state[:rooms].each do |room|
                room[:cohort_applications_by_vector] = instance.room_applications_by_vector(room[:cohort_applications])
            end
            state
        end

        def get_applications_for_locations(cohort, locations)
            cohort.cohort_applications.joins(:user => :career_profile).where("career_profiles.location" => locations)
        end

        def get_applications_around_san_jose(cohort)
            get_applications_for_locations(cohort, [
                "0101000020E61000009CA0979BB9785EC0981E03684AAB4240",
                "0101000020E6100000647E7F94227D5EC0AAE05C6853AD4240",
                "0101000020E6100000F40E01D15D855EC0590861246AB14240",
                "0101000020E61000009BA0979BB9785EC0981E03684AAB4240",
                "0101000020E61000002C10E26F208F5EC0D6C21B881BBE4240",
                "0101000020E610000040CE458D53825EC050E449D235AF4240",
                "0101000020E61000001CC17C68D5945EC0B0B5AD1C10C84240",
                "0101000020E6100000A48059460F825EC009D7EDFD57A94240"
            ])
        end

        def get_applications_in_america(cohort)
            get_applications_for_locations(cohort, [
                "0101000020E6100000A8F809B1C4C351C053AEF02E172E4540",
                "0101000020E6100000A8C0C936F0D955C09D3A45FDE4F84240",
                "0101000020E6100000983C9E961F5558C0E00BEE62F5D74240",
                "0101000020E61000004098DBBDDC8052C04F35C4C25A8B4440",
                "0101000020E6100000D04C9C81367252C075649B0AA7804440",
                "0101000020E61000002461DF4EA2DA51C0861DC6A4BF234540",
                "0101000020E6100000963A6D324F4A5DC09240834D9D5B4040",
                "0101000020E6100000E8F120E28BE252C06B7121EA99DE4340"
            ])
        end

        def get_applications_in_canada(cohort)
            get_applications_for_locations(cohort, [
                "0101000020E61000005CC5E23785DD53C098AC8A7093074640",
                "0101000020E6100000C4C4D5B702EC53C07AA9D898D7BB4540",
                "0101000020E6100000B85F9912A4C94FC0E78C28ED0D534640",
                "0101000020E6100000CC35711786D853C0CD72D9E89CD34540",
                "0101000020E610000010D99B73CBF053C08FFA905CA3DD4540",
                "0101000020E6100000B0F171B7355B4AC0ED31EC8BDFC74740",
                "0101000020E610000090E33DBD88845CC0BF4B040539864940"
            ])
        end

        def get_applications_in_the_boondocks(cohort)
            get_applications_for_locations(cohort, [
                "0101000020E6100000B09F200C97BE5F400A85083884C84240",
                "0101000020E61000008095E2F43F1A3A40DEFF6C50A0364640",
                "0101000020E6100000D099ED0A7D0C5840FC1065B9B6DD3040",
                "0101000020E6100000F0A1444B1EB65A4035689E12C6D518C0",
                "0101000020E6100000F024C74219395A400FF4AB94F9162740",
                "0101000020E610000000D820DD19871840FF45D09849CE4840",
                "0101000020E610000080AAB0BE26681140323F92EDD76C4940",
                "0101000020E610000000221F4F26812540DA6A20A7F9F44D40",
                "0101000020E6100000007FF8F9EFC9184068476062E2314740",
                "0101000020E61000004022E5828E113240F026BF4527AA4D40",
                "0101000020E610000000DA82EF47C31240A7A498CD2D274A40",
                "0101000020E6100000A0FF46F1E0AF3640382806ED7A3A4E40",
                "0101000020E610000000255CC823981D40BCB1A03028272240",
                "0101000020E6100000206FCA260ADE37405D948F9378954E40",
                "0101000020E610000080206E98B25F3040112F9974A51A4840",
                "0101000020E6100000382F4E7C357953C00B9CC7BCE9793240",
                "0101000020E6100000D0B3379D099A414083CC84155DE73F40"
            ])
        end

    end
end