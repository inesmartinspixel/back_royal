require 'spec_helper'
require 'tempfile'

describe Cohort::OnPeriodExerciseJob do

    fixtures :users, :cohorts
    attr_reader :cohort, :period_index, :period

    before(:each) do
        stub_const('ENV', {'ENABLE_SLACK_COHORT_MESSAGES' => 'true'})

        @user = users(:accepted_mba_cohort_user)
        @cohort = cohorts(:published_mba_cohort)
        expect(@cohort.slack_rooms.size).to be > 1
    end

    it "should queue send jobs for each room" do
        setup_exercise_with_file
        # expect_events

        start = Time.now
        Cohort::OnPeriodExerciseJob.perform_now(@cohort.id, @period_index, @exercise["id"])
        jobs = Delayed::Job.where("created_at > ?", start)
        args_for_jobs = jobs.map { |j| j.payload_object.job_data['arguments'] }
        expected_args_for_jobs = @cohort.slack_rooms.map do |slack_room|
            [@cohort.id, @period_index, @exercise["id"], slack_room.id ]
        end
        expect( args_for_jobs ).to match_array(expected_args_for_jobs)
    end

    def setup_exercise_with_file
        @period_index = 2 # 1-based
        @period = cohort.periods[period_index-1] # see note in code about 1-indexing of period indexes

        @s3_document = S3ExerciseDocument.new(
            file: fixture_file_upload('files/test.png', 'image/png'),
            file_file_name: "foo.png",
            file_content_type: "Image/Foo")
        @exercise = {
            "id" => SecureRandom.uuid,
            "hours_offset_from_end" => -24,
            "message" => "@channel Test message. Test email mba@smart.ly. Test message.",
            "channel" => "#foo",
            "document" => {
                "id" => @s3_document.id,
                "file_file_name" => @s3_document.file_file_name,
                "url" => "https://foo.com",
                "title" => "Foo File"
            }
        }
        @period["exercises"] = [@exercise]
        @cohort.publish!
    end

    def expect_events
        allow(SecureRandom).to receive(:uuid).and_return('uuid-foo')
        allow_any_instance_of(Event).to receive(:log_to_external_systems)
        @cohort.accepted_users.each do |accepted_user|
            expect(Event).to receive(:create_server_event!).with('uuid-foo', accepted_user.id, 'period_exercise', hash_including(exercise: @exercise)).and_return(Event.new)
        end
    end
end