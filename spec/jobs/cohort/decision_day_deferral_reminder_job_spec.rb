require 'spec_helper'

describe Cohort::DecisionDayDeferralReminderJob do
    attr_accessor :instance, :cohort
    fixtures :cohorts

    before(:each) do
        @instance = Cohort::DecisionDayDeferralReminderJob.new
    end

    describe "perform" do

        it "should filter out users who have no penultimate cohort application" do
            cohort = cohorts(:published_emba_cohort)
            pre_accepted_user = cohort.pre_accepted_users.first
            cohort.cohort_applications.where.not(user_id: pre_accepted_user.id).delete_all
            expect(cohort.cohort_applications.count).to eq(1)
            expect(pre_accepted_user.cohort_applications.count).to eq(1)
            expect(instance).to receive(:create_server_events).with(cohort.published_version, [])
            instance.perform(cohort.attributes['id'])
        end

        {
            'emba' => ['deferred'],
            'mba' => ['deferred', 'expelled']
        }.each do |program_type, statuses|
            describe "when #{program_type} cohort" do

                before(:each) do
                    @cohort = cohorts("published_#{program_type}_cohort")
                end

                statuses.each do |status|

                    it "should create_server_events for pre_accepted_users who haven't registered and their penultimate cohort application is #{status}" do
                        pre_accepted_user = cohort.pre_accepted_users.first
                        expect(pre_accepted_user.cohort_applications.count).to eq(1)

                        # create another cohort application for the pre_accepted_user for a different
                        # published cohort with an applied_at before their currently pre-accepted application
                        other_cohort = Cohort.where(program_type: program_type, was_published: true).where.not(id: cohort.attributes['id']).first
                        CohortApplication.create!(
                            user_id: pre_accepted_user.id,
                            cohort_id: other_cohort.attributes['id'],
                            status: status,
                            applied_at: pre_accepted_user.pre_accepted_application.applied_at - 1.minute
                        )
                        pre_accepted_user.reload

                        expect(pre_accepted_user.cohort_applications.count).to eq(2)
                        expect(pre_accepted_user.pre_accepted_application.registered).not_to be(true)
                        expect(instance).to receive(:create_server_events) do |cohort, users|
                            expect(users.pluck(:id)).to include(pre_accepted_user.id)
                        end
                        instance.perform(cohort.attributes['id'])
                    end
                end
            end
        end
    end

    describe "create_server_events" do

        it "should create a cohort:deferral_registration_reminder server event for each user and log_to_external_systems" do
            cohort = cohorts(:published_emba_cohort)
            users = [cohort.pre_accepted_users.first]
            mock_event = double('Event')
            expect(Event).to receive(:create_server_event!).with(
                anything,
                users.first.id,
                'cohort:deferral_registration_reminder',
                {
                    program_type: cohort.program_type
                }
            ).and_return(mock_event)
            expect(mock_event).to receive(:log_to_external_systems)
            instance.create_server_events(cohort, users)
        end
    end
end
