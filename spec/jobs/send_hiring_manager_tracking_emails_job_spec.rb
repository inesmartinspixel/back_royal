require 'spec_helper'

describe SendHiringManagerTrackingEmailsJob do
    attr_accessor :instance

    fixtures :users, :hiring_relationships

    before(:each) do
        self.instance = SendHiringManagerTrackingEmailsJob.new
    end

    describe "perform" do

        it "should create a server event for each hiring manager with unreviewed interests" do
            hiring_manager = users(:hiring_manager_with_team)
            teammate = users(:hiring_manager_teammate)

            another_teammate = User.joins(:hiring_application).where.not(id: [hiring_manager.id, teammate.id]).first
            another_teammate.candidate_relationships.destroy_all # prevent conflicts
            another_teammate.update_column(:hiring_team_id, hiring_manager.hiring_team_id)

            # Destroy open_positions for other users so we can expect only three times below
            HiringRelationship.update_all(open_position_id: nil) # prevent fk constraint error
            User.joins(:hiring_application).where.not(hiring_team_id: hiring_manager.hiring_team_id).each do |user|
                user.open_positions.destroy_all
            end

            # Destroy open_positions for one of the teammates
            another_teammate.open_positions.destroy_all

            hiring_managers = [hiring_manager, teammate, another_teammate]

            expect(hiring_manager.open_positions.size).to be > 0
            expect(teammate.open_positions.size).to be > 0
            expect(another_teammate.open_positions.size).to be 0

            # Make sure there is one archived open position
            OpenPosition.create!(hiring_manager_id: hiring_manager.id, title: 'Foo Testing', archived: true)
            hiring_manager.reload

            # mock this out so we can ensure it was called with the correct list
            allow(instance).to receive(:hiring_relationships_json) do |relationships|
                relationships.pluck('id').sort
            end
            expect(Event).to receive(:create_server_event!).exactly(hiring_manager.hiring_team.hiring_managers.size).times do |uuid, hiring_manager_id, event_type, payload|

                # assert that there is a hiring manager and it is one from the
                # list of hiring_managers created above
                hiring_manager = hiring_managers.detect { |hm| hm.id == hiring_manager_id }
                expect(hiring_manager).not_to be_nil
                expect(hiring_managers.pluck(:id).include?(hiring_manager_id)).to be(true)

                # initialize counts and assert that this hiring manager has some open
                # posisitions
                num_total_candidates_interested = 0
                num_new_candidates_interested = 0
                unarchived_open_positions = hiring_manager.hiring_team.unarchived_open_positions
                expect(unarchived_open_positions.size).to be > 0 # sanity check

                # iterate through open positions, incrementing the counts of interested candidates
                unarchived_open_positions.each do |open_position|
                    num_new_candidates_interested += open_position.candidate_position_interests_needing_review.size
                    num_total_candidates_interested += open_position.accepted_and_unhidden_candidate_position_interests.size
                end

                # write out the expected attributes to be logged
                expected_positions_attributes = unarchived_open_positions
                    .select { |position| position.candidate_position_interests_needing_review.size > 0 }
                    .sort_by { |position| [position.candidate_position_interests_needing_review.size, position.title] }
                    .reverse
                    .map do |position|
                        expect(position.archived).to be(false)
                        expect(position.candidate_position_interests_needing_review.size).to be > 0

                        {
                            id: position.id,
                            title: position.title,
                            hiring_manager_name: position.hiring_manager.name,
                            num_interests_needing_review: position.candidate_position_interests_needing_review.size
                        }
                end

                expect(uuid.looks_like_uuid?).to be(true)
                expect(event_type).to eq('hiring_manager:send_tracker_email')
                expect(payload[:positions]).to eq(expected_positions_attributes)
                expect(payload[:has_created_an_open_position]).to eq(hiring_manager.open_positions.any?)
                expect(payload[:num_unarchived_open_positions]).to eq(hiring_manager.hiring_team.unarchived_open_positions.count)
                expect(payload[:num_new_candidates_interested]).to eq(num_new_candidates_interested)
                expect(payload[:num_total_candidates_interested]).to eq(num_total_candidates_interested)
                expect(payload[:num_connections]).to eq(hiring_manager.connected_candidate_relationships.count)
                expect(payload[:num_saved_for_later]).to eq(hiring_manager.candidate_relationships_saved_for_later.count)
                expect(payload[:connected_relationships]).to eq(instance.send(:hiring_relationships_json, hiring_manager.connected_candidate_relationships))
                expect(payload[:relationships_saved_for_later]).to eq(instance.send(:hiring_relationships_json, hiring_manager.candidate_relationships_saved_for_later))

                mock_event = "mock_event"
                expect(mock_event).to receive(:log_to_external_systems)
                mock_event
            end

            instance.perform
        end

    end

    describe "hiring_relationships_json (plooral https://www.youtube.com/watch?v=jbpP888OJyU)" do

        it "should sort and slice the hiring relationships" do
            relationships = []
            expect(instance).to receive(:hiring_relationship_json).exactly(8).times do |relationship|
                "json #{relationship.last_activity_at}"
            end

            0.upto(10) do |i|
                relationship = "mock_relationship"
                allow(relationship).to receive(:last_activity_at).and_return(i)
                relationships << relationship
            end

            expect(instance.send(:hiring_relationships_json, relationships.shuffle)).to eq([
                "json 10", "json 9", "json 8", "json 7", "json 6", "json 5", "json 4", "json 3"
            ])
        end

    end

    describe "hiring_relationships_json" do

        describe "basic info" do

            it "should be set" do
                hiring_relationship = hiring_relationships(:accepted_accepted)
                json = hiring_relationship.as_json({
                    career_profile_options: {
                        anonymize: false
                    }
                })

                expect(instance.send(:hiring_relationship_json, hiring_relationship)).to have_entries({
                    "avatar_url" => hiring_relationship.candidate.avatar_url_with_fallback,
                    "name" => json['career_profile']['name'],
                    "matched_at" => json['matched_at'],
                    "days_since_matched" => hiring_relationship.days_since_matched,
                    "conversation_url" => "https://smart.ly/hiring/tracker?connectionId=#{hiring_relationship.candidate_id}"
                })
            end

            it "should set conversation_url to nil if saved_for_later" do
                hiring_relationship = HiringRelationship.where(hiring_manager_status: 'saved_for_later').first
                json = hiring_relationship.as_json({
                    career_profile_options: {
                        anonymize: false
                    }
                })

                expect(instance.send(:hiring_relationship_json, hiring_relationship)).to have_entries({
                    "avatar_url" => hiring_relationship.candidate.avatar_url_with_fallback,
                    "name" => json['career_profile']['name'],
                    "matched_at" => json['matched_at'],
                    "days_since_matched" => hiring_relationship.days_since_matched,
                    "conversation_url" => nil
                })
            end

        end

        describe "current_position info" do

            it "should work when there is one" do
                featured_work_experience = CareerProfile::WorkExperience.where(featured: true).first
                candidate_with_featured_work_exp = featured_work_experience.career_profile.user
                hiring_manager = users(:hiring_manager)
                hiring_manager.candidate_relationships.destroy_all
                hiring_relationship = HiringRelationship.create!(
                    hiring_manager_id: hiring_manager.id,
                    candidate_id: candidate_with_featured_work_exp.id,
                    hiring_manager_status: 'accepted',
                    candidate_status: 'accepted'
                )
                expect(instance.send(:hiring_relationship_json, hiring_relationship)).to have_entries({
                    "current_position_title" => featured_work_experience.job_title,
                    "current_position_company_name" => featured_work_experience.professional_organization.text
                })
            end

            it "should work whent there is none" do
                candidate_with_no_featured_work_exp = CareerProfile.where.not(id: CareerProfile::WorkExperience.pluck(:career_profile_id)).first.user
                hiring_manager = users(:hiring_manager)
                hiring_manager.candidate_relationships.destroy_all
                hiring_relationship = HiringRelationship.create!(
                    hiring_manager_id: hiring_manager.id,
                    candidate_id: candidate_with_no_featured_work_exp.id,
                    hiring_manager_status: 'accepted',
                    candidate_status: 'accepted'
                )
                expect(instance.send(:hiring_relationship_json, hiring_relationship)).to have_entries({
                    "current_position_title" => nil,
                    "current_position_company_name" => nil
                })
            end

        end

        describe "position_title" do

            it "should work when there is one" do
                hiring_relationship = hiring_relationships(:with_open_position)
                expect(instance.send(:hiring_relationship_json, hiring_relationship)).to have_entries({
                    "position_title" => hiring_relationship.open_position.title
                })
            end

            it "should work whent there is none" do
                hiring_relationship = HiringRelationship.where(:open_position_id => nil).first
                expect(instance.send(:hiring_relationship_json, hiring_relationship)).to have_entries({
                    "position_title" => nil
                })
            end

        end

        describe "has_unread_messages" do
            attr_reader :hiring_relationship, :receipts
            before(:each) do
                @hiring_relationship = hiring_relationships(:accepted_accepted)
                expect(hiring_relationship.conversation).not_to be_nil

                @receipts = hiring_relationship.conversation.messages.map(&:receipts).flatten.select { |r| r.receiver_id == hiring_relationship.hiring_manager_id }
            end

            it "should work when there is a conversation with unread messages" do
                receipts.each { |r| r.update_attribute(:is_read, false) }
                expect(instance.send(:hiring_relationship_json, hiring_relationship)['has_unread_messages']).to eq(true)
            end

            it "should work when there is a conversation with no unread messages" do
                receipts.each { |r| r.update_attribute(:is_read, true) }
                expect(instance.send(:hiring_relationship_json, hiring_relationship)['has_unread_messages']).to eq(false)
            end

            it "should work when conversation messages are missing receipts" do
                hiring_relationship.conversation.messages.to_a.first.receipts.delete_all
                expect(instance.send(:hiring_relationship_json, hiring_relationship)['has_unread_messages']).to eq(false)
            end

            it "should work when there is no conversation" do
                hiring_relationship.update_attribute(:conversation_id, nil)
                expect(instance.send(:hiring_relationship_json, hiring_relationship)['has_unread_messages']).to eq(false)

            end

        end

    end

end