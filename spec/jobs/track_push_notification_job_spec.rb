require 'spec_helper'

describe TrackPushNotificationJob do

    fixtures(:users)

    attr_accessor :delivery_id, :device_id, :event

    before(:each) do
        @instance = TrackPushNotificationJob.new

        @delivery_id = SecureRandom.uuid
        @device_id = SecureRandom.uuid
        @event = OpenStruct.new

        @orig_customer_io_site_id = ENV['CUSTOMER_IO_SITE_ID']
        @orig_customer_io_api_key = ENV['CUSTOMER_IO_API_KEY']
        ENV['CUSTOMER_IO_SITE_ID'] = '12345'
        ENV['CUSTOMER_IO_API_KEY'] = '54321'
    end

    after(:each) do
        ENV['CUSTOMER_IO_SITE_ID'] = @orig_customer_io_site_id
        ENV['CUSTOMER_IO_API_KEY'] = @orig_customer_io_api_key
    end

    describe "perform" do

        it "should make a request to customer.io push notification tracking endpoint with related event properties" do
            now = Time.now
            allow(Time).to receive(:now).and_return(now)

            mock_session = {}
            expect(Net::HTTP).to receive(:new).with('track.customer.io', 443).and_return(mock_session)
            expect(mock_session).to receive(:use_ssl=).with(true)

            mock_http_request = {}
            expect(Net::HTTP::Post).to receive(:new).with(URI.parse('https://track.customer.io/push/events')).and_return(mock_http_request)
            expect(mock_http_request).to receive(:basic_auth).with('12345', '54321')
            expect(mock_http_request).to receive(:body=).with({
                delivery_id: delivery_id,
                device_id: device_id,
                event: event,
                timestamp: now.to_timestamp
            }.to_json)

            mock_http = {}
            expect(mock_session).to receive(:start).and_yield mock_http
            expect(mock_http).to receive(:request).with(mock_http_request)

            @instance.perform({
                delivery_id: delivery_id,
                device_id: device_id,
                event: event
            })
        end

        it "should return early if necessary properties not present on hash" do
            expect(Net::HTTP).not_to receive(:new)
            expect(Net::HTTP::Post).not_to receive(:new)

            @instance.perform({
                delivery_id: delivery_id,
                device_id: device_id,
                event: nil
            })
        end

    end

end