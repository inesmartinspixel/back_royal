require 'spec_helper'

describe SyncFromAirtableBatchJob do
    attr_accessor :instance, :cohort_application, :bucket

    before(:each) do
        self.cohort_application = CohortApplication.where(status: 'pending', admissions_decision: nil).first
        self.instance = SyncFromAirtableBatchJob.new
        self.bucket = 0
    end

    describe "#perform" do
        it "should perform correctly" do
            expect(instance).to receive(:perform_airtable_to_database_sync).with(bucket).ordered
            expect(instance).to receive(:perform_decision_sync).with(bucket).ordered
            instance.perform(bucket: bucket)
        end

        it "should not run database_sync if default value overriden" do
            expect(instance).not_to receive(:perform_airtable_to_database_sync)
            expect(instance).to receive(:perform_decision_sync).with(bucket)
            instance.perform(database_sync: false, bucket: bucket)
        end

        it "should not run decision_sync if default value overriden" do
            expect(instance).to receive(:perform_airtable_to_database_sync).with(bucket)
            expect(instance).not_to receive(:perform_decision_sync)
            instance.perform(decision_sync: false, bucket: bucket)
        end
    end

    describe "#perform_airtable_to_database_sync" do
        before(:each) do
            # A real-world hash sent back from the test Airtable
            @airtable_hash = {
                "Application ID"=> self.cohort_application.id,
                "Interview"=>"Conducted",
                "Interviewer"=>"Matt",
                "Interview Scheduled"=>true,
                "Round 1 Tag"=>"Reject for Restricted Location",
                "Interview Recommendation"=>"Do Not Recommend",
                "Final Decision"=>"Reject",
                "Risks to Yield"=>["Finances", "Finances - Tuition Reimbursement", "Finances - Full Scholarship Only", "Brand Perception", "Accreditation", "Value of Offering"],
                "Likelihood to Yield"=>"Sitting on the Fence",
                "Final Decision Reason"=>"Testing a final decision reason textTesting a final decision reason textTesting a final decision reason textTesting a final decision reason textTesting a final decision reason text\n",
                "Round 1 Reason - MBA Reject"=>["test1", "test2"],
                "Round 1 Reason - MBA Invite"=>["test1", "test2"],
                "Round 1 Reason - EMBA Invite"=>["test1", "test2"],
                "Round 1 Reason - EMBA Reject"=>["test1"],
                "Reason for Declining"=>["test2", "test1"],
                "Interview Date"=>"2019-02-21T20:45:30.000Z",
                "Contribution Score"=>"Poor",
                "Insightfulness Score"=>"Average",
                "Motivation Score"=>"Great",
                "Dev - Last Modified At For Sync Logic"=>"2019-02-21T20:45:30.000Z",
                "Dev - Last Modified At When Synced"=>"2019-02-21T20:38:38.000Z"
            }
            @database_applications_by_id = {"#{self.cohort_application.id}" => self.cohort_application}
        end

        it "should fetch_and_process_records" do
            expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                expect(view).to eq('Dev Usage Only - Sync')
                expect(fields).to eq([Airtable::Application.column_map["id"], 'Dev - Last Modified At For Sync Logic', 'Dev - Last Modified At When Synced'] + Airtable::Application.from_airtable_column_map.values)
                expect(bucket).to eq(self.bucket)
                expect(base_filter).to be_nil
            end
            instance.perform_airtable_to_database_sync(bucket)
        end

        it "should update rubric columns" do
            expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                expect(self.cohort_application).to receive(:update).with({
                    "rubric_round_1_tag"=>"Reject for Restricted Location",
                    "rubric_round_1_reason_mba_invite"=>["test1", "test2"],
                    "rubric_round_1_reason_mba_reject"=>["test1", "test2"],
                    "rubric_round_1_reason_emba_invite"=>["test1", "test2"],
                    "rubric_round_1_reason_emba_reject"=>["test1"],
                    "rubric_final_decision"=>"Reject",
                    "rubric_final_decision_reason"=>"Testing a final decision reason textTesting a final decision reason textTesting a final decision reason textTesting a final decision reason textTesting a final decision reason text\n",
                    "rubric_interviewer"=>"Matt",
                    "rubric_interview"=>"Conducted",
                    "rubric_interview_scheduled"=>true,
                    "rubric_interview_recommendation"=>"Do Not Recommend",
                    "rubric_reason_for_declining"=>["test2", "test1"],
                    "rubric_likelihood_to_yield"=>"Sitting on the Fence",
                    "rubric_risks_to_yield"=>["Finances", "Finances - Tuition Reimbursement", "Finances - Full Scholarship Only", "Brand Perception", "Accreditation", "Value of Offering"],
                    "rubric_interview_date"=>"2019-02-21T20:45:30.000Z",
                    "rubric_contribution_score"=>"Poor",
                    "rubric_insightfulness_score"=>"Average",
                    "rubric_motivation_score"=>"Great"
                })
                block.call(@airtable_hash, @database_applications_by_id)
            end
            instance.perform_airtable_to_database_sync(bucket)
        end

        it "should log_orphaned_airtable_applications, passing along the application ids of any records in Airtable that don't have a corresponding cohort application in our database" do
            @airtable_hash['Application ID'] = SecureRandom.uuid

            expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                expect {
                    block.call(@airtable_hash, @database_applications_by_id)
                }.not_to raise_error
            end
            expect(instance).to receive(:log_orphaned_airtable_applications).with([@airtable_hash['Application ID']], 'database')

            instance.perform_airtable_to_database_sync(bucket)
        end

        it "should log error rather than raising if error occurs while updating cohort application in our database" do
            expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                expect_any_instance_of(CohortApplication).to receive(:update).and_raise('Foo Error')
                expect(Raven).to receive(:capture_exception).with('Foo Error', {
                    extra: {
                        application_id: self.cohort_application.id
                    }
                })
                block.call(@airtable_hash, @database_applications_by_id)
            end

            expect {
                instance.perform_airtable_to_database_sync(bucket)
            }.not_to raise_error
        end

        it "should handle several network failures when saving airtable record" do
            expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                expect(Raven).not_to receive(:capture_exception)
                block.call(@airtable_hash, @database_applications_by_id)
            end

            n = 0
            allow(@airtable_hash).to receive(:save) do
                n += 1
                raise 'The internet boogeyman is coming for your data packets' if n < 3
            end
            instance.perform_airtable_to_database_sync(bucket)
        end

        it "should eventually log if too many retries when saving airtable record" do
            expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                allow(@airtable_hash).to receive(:save).exactly(3).times.and_raise('The internet boogeyman is coming for your data packets')
                expect(Raven).to receive(:capture_exception).with('The internet boogeyman is coming for your data packets', {
                    extra: {
                        application_id: self.cohort_application.id
                    }
                })
                block.call(@airtable_hash, @database_applications_by_id)
            end
            instance.perform_airtable_to_database_sync(bucket)
        end
    end

    describe "#perform_decision_sync" do

        before(:each) do
            @mock_airtable_application = {
                "Application ID" => self.cohort_application.id,
                "Decision" => nil # specs can override this value as necessary
            }
            @database_applications_by_id = {"#{self.cohort_application.id}" => self.cohort_application}
        end

        it "should fetch_and_process_records" do
            expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                expect(view).to eq('Dev Usage Only - Decidable')
                expect(fields).to eq([Airtable::Application.column_map["id"], 'Decision'])
                expect(bucket).to eq(self.bucket)
                expect(base_filter).to eq("{Decision} != {Dev - Synced Decision}")
            end
            instance.perform_decision_sync(bucket)
        end

        it "should log_orphaned_airtable_applications, passing along the application ids of any records in Airtable that don't have a corresponding cohort application in our database" do
            @mock_airtable_application['Application ID'] = SecureRandom.uuid

            expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                expect {
                    block.call(@mock_airtable_application, @database_applications_by_id)
                }.not_to raise_error
            end
            expect(instance).to receive(:log_orphaned_airtable_applications).with([@mock_airtable_application['Application ID']], 'decision')

            instance.perform_decision_sync(bucket)
        end

        it "should update admissions_decision if airtable 'Decision' has changed" do
            self.cohort_application.update_columns(admissions_decision: "TBC")

            expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                @mock_airtable_application["Decision"] = "Reject"
                expect(self.cohort_application).to receive(:update).with(admissions_decision: "Reject")
                block.call(@mock_airtable_application, @database_applications_by_id)
            end

            instance.perform_decision_sync(bucket)
        end

        it "should not update admissions_decision if the airtable 'Decision' has not changed" do
            self.cohort_application.update_columns(admissions_decision: "TBC")

            expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                @mock_airtable_application["Decision"] = "TBC"
                expect(self.cohort_application).not_to receive(:update)
                block.call(@mock_airtable_application, @database_applications_by_id)
            end

            instance.perform_decision_sync(bucket)
        end

        describe "special case handling" do

            it "should raise an error if we detect manual_admin_decision being used in Airtable" do
                self.cohort_application.update_columns(admissions_decision: "TBC")

                expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                    @mock_airtable_application["Decision"] = "manual_admin_decision"
                    expect(Raven).to receive(:capture_exception).with("Detected manual_admin_decision being set in Airtable", {
                        extra: {
                            application_id: self.cohort_application.id
                        }
                    })
                    expect_any_instance_of(CohortApplication).not_to receive(:update)
                    block.call(@mock_airtable_application, @database_applications_by_id)
                end
                instance.perform_decision_sync(bucket)
            end

            describe "redecidable" do

                it "should raise an error if we detect a second decision on a processed application that is not in the whitelist" do
                    self.cohort_application.update_columns({
                        status: 'accepted',
                        admissions_decision: "Accept to Business Certificate"
                    })

                    expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                        @mock_airtable_application["Decision"] = "Reject with something special"
                        expect(Raven).to receive(:capture_exception).with("Cannot process redecision for application", {
                            extra: {
                                application_id: self.cohort_application.id,
                                admissions_decision: "Accept to Business Certificate",
                                redecision: "Reject with something special"
                            }
                        })
                        expect_any_instance_of(CohortApplication).not_to receive(:update)
                        block.call(@mock_airtable_application, @database_applications_by_id)
                    end
                    instance.perform_decision_sync(bucket)
                end

                it "should not raise an error if we detect a second decision on a processed application that is 'Invite to Interview'" do
                    self.cohort_application.update_columns({
                        status: 'accepted',
                        admissions_decision: "Accept to Business Certificate"
                    })

                    expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                        @mock_airtable_application["Decision"] = "Invite to Interview"
                        expect(Raven).not_to receive(:capture_exception)
                        block.call(@mock_airtable_application, @database_applications_by_id)
                    end
                    instance.perform_decision_sync(bucket)
                end

                it "should not raise an error if we detect a second decision on a processed application that is 'Accept w/ Career Network'" do
                    allow(Cohort::ProgramTypeConfig[cohort_application.program_type]).to receive(:supports_career_network_access_on_airtable_decision?).and_return(true)

                    self.cohort_application.update_columns({
                        status: 'accepted',
                        admissions_decision: "Accept to Business Certificate",
                    })

                    expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                        @mock_airtable_application["Decision"] = "Accept w/ Career Network"
                        expect(Raven).not_to receive(:capture_exception)
                        block.call(@mock_airtable_application, @database_applications_by_id)
                    end
                    instance.perform_decision_sync(bucket)
                end

                it "should not raise an error if we detect a second decision on a processed application that is 'EMBA w/'" do
                    self.cohort_application.update_columns({
                        status: 'accepted',
                        admissions_decision: "Accept to Business Certificate"
                    })

                    expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                        @mock_airtable_application["Decision"] = "EMBA w/ X something something Scholarship"
                        expect(Raven).not_to receive(:capture_exception)
                        block.call(@mock_airtable_application, @database_applications_by_id)
                    end
                    instance.perform_decision_sync(bucket)
                end

                it "should not raise an error if we detect a second decision on a processed application that is in NORMAL_REJECTIONS" do
                    self.cohort_application.update_columns({
                        status: 'accepted',
                        admissions_decision: "Accept to Business Certificate"
                    })

                    expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                        @mock_airtable_application["Decision"] = CohortApplication::NORMAL_REJECTIONS.sample
                        expect(Raven).not_to receive(:capture_exception)
                        block.call(@mock_airtable_application, @database_applications_by_id)
                    end
                    instance.perform_decision_sync(bucket)
                end
            end

            it "should raise an error if 'Reject and Convert to EMBA' for non-certificate learner" do
                self.cohort_application.update_columns(cohort_id: Cohort.find_by_program_type('mba').id)

                expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                    @mock_airtable_application["Decision"] = "Reject and Convert to EMBA"
                    expect(Raven).to receive(:capture_exception).with("Can't change decision to 'Reject and Convert to EMBA' unless it supports_reject_with_conversion_to_emba?", {
                        extra: {
                            application_id: self.cohort_application.id
                        }
                    })
                    expect_any_instance_of(CohortApplication).not_to receive(:update)
                    block.call(@mock_airtable_application, @database_applications_by_id)
                end
                instance.perform_decision_sync(bucket)
            end

            it "should raise an error if 'Accept w/ Career Network' for inappropriate program_type" do
                self.cohort_application.update_columns(cohort_id: Cohort.find_by_program_type('mba').id)

                expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                    @mock_airtable_application["Decision"] = "Accept w/ Career Network"
                    expect(Raven).to receive(:capture_exception).with("Can't change decision to 'Accept w/ Career Network' unless it supports_career_network_access_on_airtable_decision?", {
                        extra: {
                            application_id: self.cohort_application.id
                        }
                    })
                    expect_any_instance_of(User).not_to receive(:update)
                    expect_any_instance_of(CohortApplication).not_to receive(:update)
                    block.call(@mock_airtable_application, @database_applications_by_id)
                end
                instance.perform_decision_sync(bucket)
            end

            it "should raise an error if 'Pre-Accept to MBA' for a non-MBA program_type" do
                self.cohort_application.update_columns(cohort_id: Cohort.find_by_program_type('emba').id)
                expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                    @mock_airtable_application["Decision"] = "Pre-Accept to MBA"
                    expect(Raven).to receive(:capture_exception).with("Can't set decision for non-MBA application to 'Pre-Accept to MBA'", {
                        extra: {
                            application_id: self.cohort_application.id
                        }
                    })
                    expect_any_instance_of(User).not_to receive(:update)
                    expect_any_instance_of(CohortApplication).not_to receive(:update)
                    block.call(@mock_airtable_application, @database_applications_by_id)
                end
                instance.perform_decision_sync(bucket)
            end

            it "should raise an error if 'Waitlist' for a non-pending status" do
                self.cohort_application.update_columns({
                    status: 'accepted'
                })
                expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                    @mock_airtable_application["Decision"] = "Waitlist"
                    expect(Raven).to receive(:capture_exception).with("Can't set decision for non-pending application to 'Waitlist'", {
                        extra: {
                            application_id: self.cohort_application.id
                        }
                    })
                    expect_any_instance_of(User).not_to receive(:update)
                    expect_any_instance_of(CohortApplication).not_to receive(:update)
                    block.call(@mock_airtable_application, @database_applications_by_id)
                end
                instance.perform_decision_sync(bucket)
            end

            describe "set to nil" do
                it "should raise an error if we detect a non-TBC decision being set to nil" do
                    self.cohort_application.update_columns(admissions_decision: "Foo")
                    expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                        @mock_airtable_application["Decision"] = nil
                        expect(Raven).to receive(:capture_exception).with("Can't change decision to nil unless it was TBC", {
                            extra: {
                                application_id: self.cohort_application.id
                            }
                        })
                        expect_any_instance_of(CohortApplication).not_to receive(:update)
                        block.call(@mock_airtable_application, @database_applications_by_id)
                    end
                    instance.perform_decision_sync(bucket)
                end

                it "should not raise an error if we detect a TBC decision being set to nil" do
                    self.cohort_application.update_columns(admissions_decision: "TBC - Manager")
                    expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                        @mock_airtable_application["Decision"] = nil
                        expect(Raven).not_to receive(:capture_exception)
                        expect(self.cohort_application).to receive(:update).with(admissions_decision: nil)
                        block.call(@mock_airtable_application, @database_applications_by_id)
                    end
                    instance.perform_decision_sync(bucket)
                end
            end

            it "should raise an error if we detect a non-syncable application being decided on" do
                expect(Cohort::ProgramTypeConfig[cohort_application.program_type]).to receive(:supports_airtable_sync?).and_return(false)
                expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                    @mock_airtable_application["Decision"] = "Waitlist"
                    expect(Raven).to receive(:capture_exception).with("Trying to decide on an application that does not support airtable sync", {
                        extra: {
                            application_id: self.cohort_application.id,
                            airtable_decision: 'Waitlist'
                        }
                    })
                    expect_any_instance_of(CohortApplication).not_to receive(:update)
                    block.call(@mock_airtable_application, @database_applications_by_id)
                end
                instance.perform_decision_sync(bucket)
            end

            it "should raise an error if we detect an interview decision on a rejected and converted application" do
                cohort_application.update_columns(admissions_decision: 'Reject and Convert to EMBA')
                expect(instance).to receive(:fetch_and_process_records) do |view, fields, bucket, base_filter, &block|
                    @mock_airtable_application['Decision'] = 'Invite to Interview'
                    expect(Raven).to receive(:capture_exception).with("Can't Invite to Interview an application that was rejected and converted", {
                        extra: {
                            application_id: self.cohort_application.id
                        }
                    })
                    expect_any_instance_of(CohortApplication).not_to receive(:update)
                    block.call(@mock_airtable_application, @database_applications_by_id)
                end
                instance.perform_decision_sync(bucket)
            end
        end
    end

    describe "#log_orphaned_airtable_applications" do

        it "should send error to Sentry if not staging and there are any orphaned_airtable_application_ids" do
            application_id = SecureRandom.uuid
            expect(instance).to receive(:is_staging?).and_return(false)
            expect(Raven).to receive(:capture_exception).with("SyncFromAirtableBatchJob: Could not find corresponding cohort application in our database for record(s) in Airtable during decision sync", {
                extra: {
                    orphaned_airtable_application_ids: [application_id]
                }
            })

            instance.log_orphaned_airtable_applications([application_id], 'decision')
        end

        it "should not send error to Sentry if we're on staging" do
            application_id = SecureRandom.uuid
            expect(instance).to receive(:is_staging?).and_return(true)
            expect(Raven).not_to receive(:capture_exception)

            instance.log_orphaned_airtable_applications([application_id], 'decision')
        end
    end
end