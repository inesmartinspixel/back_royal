require 'spec_helper'

describe DeleteFromBackRoyalJob do
    attr_accessor :user, :instance

    fixtures(:users)

    before(:each) do
        self.user = users(:user_with_career_profile)
        self.instance = DeleteFromBackRoyalJob.new
    end

    describe "perform" do

        it "should call delete methods" do
            expect(instance).to receive(:delete_from_user_id_tables).exactly(1).times
            expect(instance).to receive(:delete_from_candidate_id_tables).exactly(1).times
            expect(instance).to receive(:delete_from_hiring_manager_id_tables).exactly(1).times
            expect(instance).to receive(:delete_from_career_profile_id_tables).exactly(1).times
            expect(instance).to receive(:delete_from_mailboxer_receipts_and_notifications).exactly(1).times
            expect(instance).to receive(:delete_from_users_versions).exactly(1).times
            instance.perform(user.id)
        end

    end

    describe "delete_from_user_id_tables" do

        it "should delete from tables with \'user_id\' column" do
            expect(CareerProfile::Version.where(user_id: user.id).count).not_to eq(0)
            expect(CohortApplication::Version.where(user_id: user.id).count).not_to eq(0)
            expect(ActiveRecord::Base.connection.execute(%Q~ SELECT * FROM distinct_user_ids WHERE user_id = '#{user.id}' ~).to_a.count).not_to eq(0)

            instance.user_id = user.id
            instance.delete_from_user_id_tables

            expect(CareerProfile::Version.where(user_id: user.id).count).to eq(0)
            expect(CohortApplication::Version.where(user_id: user.id).count).to eq(0)
            expect(ActiveRecord::Base.connection.execute(%Q~ SELECT * FROM distinct_user_ids WHERE user_id = '#{user.id}' ~).to_a.count).to eq(0)
        end

    end

    describe "delete_from_candidate_id_tables" do

        it "should delete from tables with \'candidate_id\' column" do
            HiringRelationship.create!(
                candidate_id: user.id,
                hiring_manager_id: users(:hiring_manager_with_team).id,
                hiring_manager_status: "pending"
            )
            CandidatePositionInterest.create!(
                candidate_id: user.id,
                open_position_id: users(:hiring_manager_with_team).open_positions.first.id,
                candidate_status: 'accepted'
            )

            instance.user_id = user.id
            instance.delete_from_candidate_id_tables

            expect(HiringRelationship.where(candidate_id: user.id).count).to eq(0)
            expect(CandidatePositionInterest.where(candidate_id: user.id).count).to eq(0)
        end

    end

    describe "delete_from_hiring_manager_id_tables" do

        attr_accessor :hiring_manager

        before(:each) do
            self.hiring_manager = users(:hiring_manager_with_team)
        end

        it "should delete candidate_position_interests records for open_positions where \'hiring_manager_id\' = user_id" do
            sql = "SELECT * FROM candidate_position_interests WHERE open_position_id IN (
                    SELECT id FROM open_positions WHERE hiring_manager_id = '#{hiring_manager.id}'
                );"

            expect(ActiveRecord::Base.connection.execute(sql).to_a.count).not_to eq(0)

            instance.user_id = instance.user_id = hiring_manager.id
            instance.delete_from_hiring_manager_id_tables

            expect(ActiveRecord::Base.connection.execute(sql).to_a.count).to eq(0)
        end

        it "should delete from tables with \'hiring_manager_id\' column" do
            HiringRelationship.create!(
                candidate_id: user.id,
                hiring_manager_id: hiring_manager.id,
                hiring_manager_status: "pending"
            )
            OpenPosition.create!(hiring_manager_id: hiring_manager.id, title: "foo")

            instance.user_id = hiring_manager.id
            instance.delete_from_hiring_manager_id_tables

            expect(HiringRelationship.where(hiring_manager_id: hiring_manager.id).count).to eq(0)
            expect(OpenPosition.where(hiring_manager_id: hiring_manager.id).count).to eq(0)
        end

    end

    describe "delete_from_career_profile_id_tables" do

        it "should delete from tables with \'career_profile_id\' column" do
            CareerProfile::WorkExperience.create(
                career_profile_id: user.career_profile.id,
                job_title: 'NA',
                start_date: Time.now,
                professional_organization_option_id: ProfessionalOrganizationOption.first.id,
                industry: 'b'
            )
            PeerRecommendation.create!(
                career_profile_id: user.career_profile.id,
                email: 'john.doe@gmail.com',
                contacted: true
            )

            instance.career_profile_id = user.career_profile.id
            instance.delete_from_career_profile_id_tables

            expect(CareerProfile::WorkExperience.where(career_profile_id: user.career_profile.id).to_a.count).to eq(0)
            expect(PeerRecommendation.where(career_profile_id: user.career_profile.id).to_a.count).to eq(0)
        end

        describe "transcripts" do

            it "should delete transcripts for education_experiences where \'career_profile_id\' = career_profile_id" do
                instance.career_profile_id = user.career_profile.id
                sql = "SELECT * FROM s3_transcript_assets WHERE education_experience_id IN (
                    select id from education_experiences where career_profile_id = '#{user.career_profile.id}'
                )"

                expect(ActiveRecord::Base.connection.execute(sql).to_a.count).not_to eq(0)
                instance.delete_transcripts_for_education_experiences
                expect(ActiveRecord::Base.connection.execute(sql).to_a.count).to eq(0)
            end

            it "should delete transcripts for education_experiences that were deleted where \'career_profile_id\' = career_profile_id" do
                instance.career_profile_id = user.career_profile.id
                sql = "SELECT * FROM s3_transcript_assets WHERE education_experience_id IN (
                    select distinct id from education_experiences_versions where career_profile_id = '#{user.career_profile.id}'
                )"

                expect(ActiveRecord::Base.connection.execute(sql).to_a.count).not_to eq(0)
                user.career_profile.education_experiences.destroy_all
                instance.delete_transcripts_for_education_experiences
                expect(ActiveRecord::Base.connection.execute(sql).to_a.count).to eq(0)
            end
        end
    end

    describe "delete_from_mailboxer_receipts_and_notifications" do

        it "should delete mailboxer receipts and notifications" do
            user = users(:user_with_multiple_conversations)

            notifications = Mailboxer::Notification.where(sender_id: user.id)
            expect(notifications.count).not_to eq(0) # sanity

            notification_ids = notifications.map(&:id)
            expect(Mailboxer::Receipt.where(notification_id: notification_ids).to_a.count).not_to eq(0) # sanity

            instance.user_id = user.id
            instance.delete_from_mailboxer_receipts_and_notifications

            expect(Mailboxer::Notification.where(sender_id: user.id).count).to eq(0)
            expect(Mailboxer::Receipt.where(notification_id: notification_ids).to_a.count).to eq(0)
        end

    end

    describe "delete_from_users_versions" do

        it "should delete users_versions records" do
            expect(User::Version.where(id: user.id).count).not_to eq(0)

            instance.user_id = user.id
            instance.delete_from_users_versions

            expect(User::Version.where(id: user.id).count).to eq(0)
        end

    end

     describe "delete_records_in_batches" do

        it "should use \'id\' as identifier for non-versions tables" do
            expect(ActiveRecord::Base.connection).to receive(:execute).with(
                "SELECT id FROM some_table WHERE column = 'value' LIMIT 1;"
            ).and_return([])
            instance.delete_records_in_batches('some_table', 'column', 'value')
        end

        it "should use \'version_id\' as identifier for versions tables" do
            expect(ActiveRecord::Base.connection).to receive(:execute).with(
                "SELECT version_id FROM some_table_versions WHERE column = 'value' LIMIT 1;"
            ).and_return([])
            instance.delete_records_in_batches('some_table_versions', 'column', 'value')
        end

     end

end
