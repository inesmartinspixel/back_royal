require 'spec_helper'

describe EnsureEnrollmentAgreementJob do

    describe "ensure_job" do

        before(:each) do
            @cohort_application_id = 12345
        end

        it "should not create a new job if one exists" do
            EnsureEnrollmentAgreementJob.perform_later(@cohort_application_id)
            job = call_ensure_job_and_return_new_job
            expect(job).to be_nil
        end

        it "should create a new job if none exists" do
            EnsureEnrollmentAgreementJob.perform_later('some_other_cohort_application_id')
            job = call_ensure_job_and_return_new_job
            expect(job).not_to be_nil
            expect(job.payload_object.job_data['arguments']).to eq([@cohort_application_id])
            expect(job.run_at).to be_within(1.second).of(Time.now)
        end

        def call_ensure_job_and_return_new_job
            start = Time.now
            EnsureEnrollmentAgreementJob.ensure_job(@cohort_application_id)
            Delayed::Job.where(queue: EnsureEnrollmentAgreementJob.queue_name).where("created_at > ?", start).first
        end

    end

    describe "perform" do

        it "should return no application found" do
            expect {
                EnsureEnrollmentAgreementJob.perform_now(SecureRandom.uuid)
            }.not_to raise_error
        end

        it "should not call create_enrollment_agreement if it !need_to_generate_new_enrollment_agreement?" do
            cohort_application = CohortApplication.first
            expect(CohortApplication).to receive(:find_by_id) { cohort_application }
            expect(cohort_application).to receive(:need_to_generate_new_enrollment_agreement?) { false }
            expect(cohort_application).not_to receive(:enrollment_agreement)
            expect(cohort_application).not_to receive(:create_enrollment_agreement).with(false)
            EnsureEnrollmentAgreementJob.perform_now(SecureRandom.uuid)
        end

        it "should call create_enrollment_agreement if applicable" do
            cohort_application = CohortApplication.first
            expect(CohortApplication).to receive(:find_by_id) { cohort_application }
            expect(cohort_application).to receive(:need_to_generate_new_enrollment_agreement?) { true }
            expect(cohort_application).to receive(:enrollment_agreement) { nil }.at_least(1).times
            expect(cohort_application).to receive(:create_enrollment_agreement).with(false)
            EnsureEnrollmentAgreementJob.perform_now(SecureRandom.uuid)
        end

        it "should delete any prior enrollment_agreements prior to creation" do

            document = SignNow::Document.new(
                id: SecureRandom.uuid
            )
            allow(document).to receive(:generate_signing_link).and_return(OpenStruct.new({
                url: "http://path/to_doc",
                expiry: Time.now + 1.week
            }))
            allow(document).to receive(:update_smart_fields)
            allow(SignNow::Document).to receive(:create).and_return(document)
            allow_any_instance_of(Cohort::Version).to receive(:enrollment_agreement_template_id).and_return('template_id')

            cohort_application = CohortApplication.first
            enrollment_agreement = cohort_application.create_enrollment_agreement(false)
            expect(enrollment_agreement).not_to be_nil

            expect(CohortApplication).to receive(:find_by_id) { cohort_application }
            expect(cohort_application).to receive(:need_to_generate_new_enrollment_agreement?) { true }
            expect(cohort_application).to receive(:create_enrollment_agreement).with(true)

            EnsureEnrollmentAgreementJob.perform_now(SecureRandom.uuid)

            # old enrollment_agreement should be deleted
            expect(SignableDocument.find_by_id(enrollment_agreement.id)).to be_nil
        end


    end

end