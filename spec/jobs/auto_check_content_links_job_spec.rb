require 'spec_helper'
require 'addressable/uri'

describe AutoCheckContentLinksJob do

    attr_reader :instance, :stream

    fixtures :lesson_streams

    before(:each) do
        @instance = AutoCheckContentLinksJob.new
        @stream = lesson_streams(:published_stream)
        @original_stdout = $stdout
        $stdout = File.open(File::NULL, "w") # suppress the puts for this test (/dev/null)
    end

    after(:each) do
        $stdout = @original_stdout
    end

    describe "perform" do

        it "should verify_links_for_streams all_published streams in_batches and create_and_log_event_to_external_systems!" do
            all_published_streams = [stream]
            mock_relation = double('Lesson::Stream::ActiveRecord_Relation')
            allow(mock_relation).to receive(:count)
            expect(Lesson::Stream).to receive(:includes).with(:lessons).and_return(mock_relation)
            expect(mock_relation).to receive(:all_published).and_return(mock_relation)
            expect(mock_relation).to receive(:in_batches) do |opts, &block|
                expect(opts).to eq(of: 2)
                expect(instance).to receive(:verify_links_for_streams).with(all_published_streams)
                block.call(all_published_streams)
            end
            expect(instance).to receive(:create_and_log_event_to_external_systems!)
            instance.perform
        end
    end

    describe "verify_links_for_streams" do

        it "should verify links for stream summaries, resource_links, resource_downloads, and lessons" do
            expect(instance).to receive(:verify_links_for_stream_attr).with(stream, 'summaries').ordered
            expect(instance).to receive(:verify_links_for_stream_attr).with(stream, 'resource_links').ordered
            expect(instance).to receive(:verify_links_for_stream_attr).with(stream, 'resource_downloads').ordered
            expect(instance).to receive(:verify_links_for_stream_lessons)
            instance.verify_links_for_streams([stream])
        end
    end

    describe "verify_links_for_stream_attr" do

        it "should iterate over every entry in stream[attr] and add the url attached to every entry to bad_links if not found" do
            expect(stream).to receive(:[]).with('foo').and_return([{"url" => 'https://bar.baz'}])
            expect(instance).to receive(:verify_link).with('https://bar.baz', stream)
            instance.verify_links_for_stream_attr(stream, 'foo')
        end
    end

    describe "verify_links_for_stream_lessons" do

        it "should iterate over every lesson in the stream and verify_links_for_stream_lesson" do
            expect(stream.lessons.size).to be > 0 # sanity check
            stream.lessons.each { |lesson| expect(instance).to receive(:verify_links_for_stream_lesson).with(lesson).ordered }
            instance.verify_links_for_stream_lessons(stream)
        end
    end

    describe "verify_links_for_stream_lesson" do

        it "should iterate over every frame in the lesson and add each content link to bad_links if not found" do
            lesson = stream.lessons.first
            mock_content = double('Lesson::Content::FrameList')
            expect(lesson).to receive(:content).and_return(mock_content)
            mock_frames = [double('Lesson::Content::FrameList::Frame::Componentized')]
            expect(mock_content).to receive(:frames).and_return(mock_frames)
            mock_frames.each do |frame|
                expect(frame).to receive(:content_links).and_return(['https://foo.bar', 'https://baz.qux'])
                expect(instance).to receive(:verify_link).with('https://foo.bar', lesson, frame).ordered
                expect(instance).to receive(:verify_link).with('https://baz.qux', lesson, frame).ordered
            end
            instance.verify_links_for_stream_lesson(lesson)
        end
    end

    describe "verify_link" do
        attr_accessor :url, :normalized_url

        before(:each) do
            @url = 'https://foo.bar/baz/qux'
            @normalized_url = Addressable::URI.parse(url).normalize
        end

        it "should skip the URL if the URL is known to be a passing link" do
            url = 'https://smart.ly/course/<course_id>'
            instance.passing_links[url] = true
            expect_any_instance_of(Net::HTTP).not_to receive(:request_head)
            instance.verify_link(url, stream)
        end

        it "should skip the URL if the URL is for a Smartly course" do
            url = 'https://smart.ly/course/<course_id>'
            expect_any_instance_of(Net::HTTP).not_to receive(:request_head)
            instance.verify_link(url, stream)
        end

        it "should add_content_item_info_to_bad_links_map if the URL has already been processed as a bad link" do
            instance.bad_links[url] = [] # make it appear as if it's already been processed as a bad link
            expect(instance).to receive(:add_content_item_info_to_bad_links_map).with(normalized_url, stream, nil)
            expect_any_instance_of(Net::HTTP).not_to receive(:request_head)
            instance.verify_link(url, stream)
        end

        it "should make a head request to the passed in URL" do
            expect(Net::HTTP).to receive(:new).with(normalized_url.host, normalized_url.port).and_call_original
            expect_any_instance_of(Net::HTTP).to receive(:request_head).with(normalized_url.path)
            instance.verify_link('https://foo.bar/baz/qux', stream)
        end

        it "should issue_get_request if head request errored and return early if it returns nil" do
            mock_response = double('Net::HTTPResponse')
            expect_any_instance_of(Net::HTTP).to receive(:request_head).and_raise('An unexpected error occurred')
            expect(instance).to receive(:issue_get_request).with(normalized_url, stream, nil).and_return(nil)
            expect(instance).not_to receive(:add_content_item_info_to_bad_links_map)
            instance.verify_link(url, stream)
            expect(instance.passing_links[url].present?).to be(false)
        end

        describe "when head request response is not a 404" do

            before(:each) do
                mock_response = double('Net::HTTPResponse')
                allow_any_instance_of(Net::HTTP).to receive(:request_head).and_return(mock_response)
                allow(mock_response).to receive(:code).and_return('not_404')
            end

            it "should add normalized version of URL to passing_links" do
                expect(instance.passing_links[normalized_url]).not_to be(true)
                instance.verify_link(url, stream)
                expect(instance.passing_links[normalized_url]).to be(true)
            end
       end

        describe "when head request response is a 404" do

            before(:each) do
                mock_response = double('Net::HTTPResponse')
                allow_any_instance_of(Net::HTTP).to receive(:request_head).and_return(mock_response)
                allow(mock_response).to receive(:code).and_return('404')
            end

            it "should follow-up with a full GET request" do
                expect(instance).to receive(:issue_get_request).with(normalized_url, stream, nil)
                instance.verify_link(url, stream)
            end

            describe "when follow-up GET request is not a 404" do

                before(:each) do
                    mock_httparty_response = double('HTTParty')
                    allow(mock_httparty_response).to receive(:code).and_return('not_404')
                    allow(instance).to receive(:issue_get_request).with(normalized_url, stream, nil).and_return(mock_httparty_response)
                end

                it "should add normalized version of URL to passing_links" do
                    expect(instance.passing_links[normalized_url]).not_to be(true)
                    instance.verify_link(url, stream)
                    expect(instance.passing_links[normalized_url]).to be(true)
                end
            end

            describe "when follow-up GET request is a 404" do

                before(:each) do
                    mock_httparty_response = double('HTTParty')
                    allow(instance).to receive(:issue_get_request).with(normalized_url, stream, nil).and_return(mock_httparty_response)
                    allow(mock_httparty_response).to receive(:code).and_return('404')
                end

                it "should add_content_item_info_to_bad_links_map" do
                    expect(instance).to receive(:add_content_item_info_to_bad_links_map).with(normalized_url, stream, nil)
                    instance.verify_link(url, stream)
                end
            end
        end
    end

    describe "issue_get_request" do

        it "should rescue from any exception during GET request, add_content_item_info_to_bad_links_map, and return nil" do
            normalized_url = Addressable::URI.parse('https://foo.bar/baz/qux').normalize
            expect(HTTParty).to receive(:get).with(normalized_url).and_raise('An error occurred')
            expect(instance).to receive(:add_content_item_info_to_bad_links_map).with(normalized_url, stream, nil)
            expect(instance.issue_get_request(normalized_url, stream, nil)).to be_nil # should not have errored
        end
    end

    describe "add_content_item_info_to_bad_links_map" do

        it "should return early and not add the content item info to the bad_links map again if content item's editor_link has already been added for the passed in url" do
            url = Addressable::URI.parse('https://foo.bar/baz/qux').normalize
            lesson = stream.lessons.first
            instance.bad_links[url.to_s] = [{editor_link: lesson.editor_link}]
            instance.add_content_item_info_to_bad_links_map(url, lesson)
            expect(instance.bad_links[url.to_s]).to eq([{editor_link: lesson.editor_link}])
        end

        it "should add the to_s URL value and content item editor_link and title to the bad_links map if URL is not already present" do
            url = Addressable::URI.parse('https://foo.bar/baz/qux').normalize
            expect(stream).to receive(:editor_link).and_return('some_editor_link')
            instance.add_content_item_info_to_bad_links_map(url, stream)
            expect(instance.bad_links).to eq({ "#{url.to_s}" => [{ editor_link: 'some_editor_link', title: stream.title }] })
        end

        it "should add the content item editor_link and title to the bad_links map if URL is already present" do
            url = Addressable::URI.parse('https://foo.bar/baz/qux').normalize
            instance.bad_links['https://foo.bar/baz/qux'] = [{ editor_link: 'some_editor_link', title: 'some_title'}]
            expect(stream).to receive(:editor_link).and_return('some_other_editor_link')
            instance.add_content_item_info_to_bad_links_map(url, stream)
            expect(instance.bad_links).to eq({ 'https://foo.bar/baz/qux' => [{ editor_link: 'some_editor_link', title: 'some_title'}, { editor_link: 'some_other_editor_link', title: stream.title }] })
        end

        it "should use frame edior_link if passed in" do
            url = Addressable::URI.parse('https://foo.bar/baz/qux').normalize
            mock_frame = double('Lesson::Content::FrameList::Frame::Componentized')
            expect(mock_frame).to receive(:editor_link).and_return('some_editor_link')
            expect(stream).not_to receive(:editor_link)
            instance.add_content_item_info_to_bad_links_map(url, stream, mock_frame)
            expect(instance.bad_links).to eq({ 'https://foo.bar/baz/qux' => [{ editor_link: 'some_editor_link', title: stream.title }] })
        end
    end

    describe "create_and_log_event_to_external_systems!" do

        it "should create_server_event! containing bad_links and unchecked_editor_links and log_to_external_systems" do
            mock_event = double("Event")
            expect(Event).to receive(:create_server_event!).with(
                anything,
                'a576a6fc-175f-4013-8637-0005b2f64374', # Alexie
                'auto_check_content_links:bad_links',
                {
                    bad_links: {}
                }
            ).and_return(mock_event)
            expect(mock_event).to receive(:log_to_external_systems)
            instance.create_and_log_event_to_external_systems!
        end
    end
end
