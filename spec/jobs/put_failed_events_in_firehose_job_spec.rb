require 'spec_helper'

describe PutFailedEventsInFirehoseJob do
    attr_accessor :career_profile, :instance

    fixtures :users

    describe :perform do
        it 'should raise if there is an error_code in the response' do
            event = Event.first
            expect(Event::PutEventsInFirehose).to receive(:put).with([event]).and_return(
                OpenStruct.new({
                    request_responses: [
                        OpenStruct.new({
                            error_code: 42,
                            error_message: 'All messed up'
                        })
                    ]
                })
            )

            expect {
                PutFailedEventsInFirehoseJob.perform_now(event.id)
            }.to raise_error('Firehose error: All messed up')
        end

        it "should not raise if the response is good" do
            event = Event.first
            expect(Event::PutEventsInFirehose).to receive(:put).with([event]).and_return(
                OpenStruct.new({
                    request_responses: [
                        OpenStruct.new({})
                    ]
                })
            )

            PutFailedEventsInFirehoseJob.perform_now(event.id)

        end
    end
end