require 'spec_helper'

describe SyncFromAirtableJob do
    attr_accessor :instance

    before(:each) do
        @instance = SyncFromAirtableJob.new
    end

    it "should include SyncFromAirtableBatchHelper::MasterJobHelper" do
        expect(instance).to be_a(SyncFromAirtableBatchHelper::MasterJobHelper)
    end

    describe "#perform" do

        it "should enqueue_batch_jobs" do
            expect(instance).to receive(:enqueue_batch_jobs).with(SyncFromAirtableBatchJob, database_sync: true, decision_sync: true)
            instance.perform
        end
    end
end