require 'spec_helper'

describe IncrementMatchUsageJob do

    attr_accessor :instance
    attr_accessor :hiring_relationship

    include StripeHelper

    before(:each) do
        self.instance = IncrementMatchUsageJob.new

        hiring_teams_with_subscriptions = HiringTeam.joins(:subscriptions).pluck(:id)

        self.hiring_relationship = HiringRelationship.joins(:hiring_manager)
            .joins("INNER JOIN hiring_teams on hiring_teams.id = users.hiring_team_id")
            .where("users.hiring_team_id IS NOT NULL AND hiring_teams.owner_id IS NOT NULL")
            .where.not("users.hiring_team_id" => hiring_teams_with_subscriptions)
            .first
    end


    describe "with metered plan" do

        before(:each) do
            create_subscription(hiring_relationship.hiring_team, metered_plan.id)
        end

        describe "perform" do
            it "should call increment_usage on a hiring relationship's associated subscription if stripe_usage_record_info is nil" do
                expect(hiring_relationship.stripe_usage_record_info).to be_nil
                expect(HiringRelationship).to receive(:find).with(hiring_relationship.id).and_call_original
                expect_any_instance_of(Subscription).to receive(:increment_usage).and_call_original
                expect_any_instance_of(HiringTeam).to receive(:increment_usage).and_call_original

                instance.perform(hiring_relationship.id)

                expect(hiring_relationship.reload.stripe_usage_record_info).not_to be_nil
            end

            it "should not call increment_usage on a hiring relationship's associated subscription if stripe_usage_record_info is set" do
                hiring_relationship.update(stripe_usage_record_info: {id: "pre_existing_usage_record_id"})
                expect(HiringRelationship).to receive(:find).at_least(1).times.with(hiring_relationship.id).and_call_original
                expect_any_instance_of(Subscription).not_to receive(:increment_usage)
                expect_any_instance_of(HiringTeam).not_to receive(:increment_usage)

                instance.perform(hiring_relationship.id)

                expect(hiring_relationship.reload.stripe_usage_record_info).not_to be_nil
                expect(hiring_relationship.reload.stripe_usage_record_info["id"]).to eq("pre_existing_usage_record_id")
            end
        end

    end

    describe "with non-metered plan" do

        before(:each) do
            create_subscription(hiring_relationship.hiring_team, default_plan.id)
        end

        describe "perform" do
            it "should do nothing and not raise" do
                expect(hiring_relationship.stripe_usage_record_info).to be_nil
                expect(HiringRelationship).to receive(:find).with(hiring_relationship.id).and_call_original
                expect_any_instance_of(Subscription).not_to receive(:increment_usage)
                expect_any_instance_of(HiringTeam).not_to receive(:increment_usage)

                expect {
                    instance.perform(hiring_relationship.id)
                }.not_to raise_error

                expect(hiring_relationship.reload.stripe_usage_record_info).to be_nil
            end
        end

    end

    describe "subscription error handling" do

        it "should raise if no subscription is found" do
            expect(hiring_relationship.stripe_usage_record_info).to be_nil
            expect(HiringRelationship).to receive(:find).with(hiring_relationship.id).and_call_original
            expect_any_instance_of(Subscription).not_to receive(:increment_usage)
            expect_any_instance_of(HiringTeam).not_to receive(:increment_usage)

            expect {
                instance.perform(hiring_relationship.id)
            }.to raise_error("No active subscription for hiring team found!")

            expect(hiring_relationship.reload.stripe_usage_record_info).to be_nil
        end

    end


    describe "API error handling" do

        before(:each) do
            create_subscription(hiring_relationship.hiring_team, metered_plan.id)
        end

        it "should log to Sentry but proceed if the Subscription's increment_usage fails" do
            expect(hiring_relationship.stripe_usage_record_info).to be_nil
            expect(HiringRelationship).to receive(:find).with(hiring_relationship.id).and_call_original
            expect_any_instance_of(Subscription).to receive(:increment_usage) {
                raise "Some increment_usage"
            }

            expect(Raven).to receive(:capture_exception).once

            expect {
                instance.perform(hiring_relationship.id)
            }.not_to raise_error

            expect(hiring_relationship.reload.stripe_usage_record_info).not_to be_nil
        end

    end


end