require 'spec_helper'

describe CopyAvatarJob do
    attr_accessor :user, :instance

    fixtures :users

    before(:each) do
        self.user = User.where.not(avatar_url: nil).first
        self.instance = CopyAvatarJob.new
    end

    it "should return if there is no user" do
        expect_any_instance_of(User).not_to receive(:attributes)
        instance.perform(SecureRandom.uuid)
    end

    it "should return if the user has no avatar url" do
        user = User.where(avatar_url: nil).first
        expect_any_instance_of(User).not_to receive(:avatar=)
        instance.perform(user.id)
    end

    it "should copy the file and save" do
        expect(user.avatar).to be_nil
        expect_any_instance_of(AvatarAsset).to receive(:fetch_file_from_url).with(user.avatar_url)
        instance.perform(user.id)
        expect(user.reload.avatar).not_to be_nil
    end

    it "should rescue certain status codes and log to sentry" do
        err = OpenURI::HTTPError.new('message', nil)
        expect(err).to receive(:io).and_return(OpenStruct.new(status: ['403', 'Not authorized']))
        expect(Raven).to receive(:capture_exception).with(err, {
            extra: {
                user_id: user.id,
                url: user.avatar_url,
                source: 'source'
            },
            :fingerprint => ["source", "403"]
        })
        expect(instance).to receive(:url_source).and_return('source')
        expect_any_instance_of(AvatarAsset).to receive(:fetch_file_from_url).and_raise(err)

        expect {
            instance.perform(user.id)
        }.not_to raise_error
    end

    it "should rescue AvatarAsset::DisallowedScheme" do
        err = AvatarAsset::DisallowedScheme.new
        expect(Raven).to receive(:capture_exception).with(err, {
            extra: {
                user_id: user.id,
                url: user.avatar_url,
                source: 'source'
            },
            :fingerprint => ["source", "DisallowedScheme"]
        })
        expect(instance).to receive(:url_source).and_return('source')
        expect_any_instance_of(AvatarAsset).to receive(:fetch_file_from_url).and_raise(err)

        expect {
            instance.perform(user.id)
        }.not_to raise_error
    end

    it "should rescue ActiveRecord::RecordInvalid" do
        err = ActiveRecord::RecordInvalid.new
        expect(Raven).to receive(:capture_exception).with(err, {
            extra: {
                user_id: user.id,
                url: user.avatar_url,
                source: 'source'
            },
            :fingerprint => ["source", "RecordInvalid"]
        })
        expect(instance).to receive(:url_source).and_return('source')
        expect_any_instance_of(AvatarAsset).to receive(:fetch_file_from_url).and_return({})
        expect_any_instance_of(User).to receive(:save!).and_raise(err)

        expect {
            instance.perform(user.id)
        }.not_to raise_error
    end

    it "should not log to sentry if log_http_errors_to_sentry=false" do
        err = OpenURI::HTTPError.new('message', nil)
        expect(err).to receive(:io).and_return(OpenStruct.new(status: ['403', 'Not authorized']))
        expect(Raven).not_to receive(:capture_exception)
        expect_any_instance_of(AvatarAsset).to receive(:fetch_file_from_url).and_raise(err)

        expect {
            instance.perform(user.id, false)
        }.not_to raise_error
    end

    it "should not rescue other status codes" do
        err = OpenURI::HTTPError.new('message', nil)
        expect(err).to receive(:io).and_return(OpenStruct.new(status: ['xxx', 'Who knows']))
        expect(Raven).not_to receive(:capture_exception)
        expect_any_instance_of(AvatarAsset).to receive(:fetch_file_from_url).and_raise(err)

        expect {
            instance.perform(user.id, false)
        }.to raise_error(err)
    end

    describe "url_source" do

        it "should map urls to sources" do
            expect(instance.url_source('https://lh6.googleusercontent.com/...')).to eq('google')
            expect(instance.url_source('http://graph.facebook.com/v2.6/...')).to eq('facebook')
            expect(instance.url_source('http://fbsbx/...')).to eq('facebook')
            expect(instance.url_source('http://fbcdn/...')).to eq('facebook')
            expect(instance.url_source('http://licdn/...')).to eq('linkedin')
            expect(instance.url_source('http://gravatar/...')).to eq('gravatar')
            expect(instance.url_source('http://sumnelse/...')).to eq('unknown')
        end
    end

end