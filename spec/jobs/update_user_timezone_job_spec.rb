require 'spec_helper'

describe UpdateUserTimezoneJob do
    attr_accessor :instance, :user, :lat, :lng

    fixtures :users

    before(:each) do
        @instance = UpdateUserTimezoneJob.new
        @user = users(:user_with_career_profile)
    end

    describe "perform" do

        describe "when record not found" do

            it "should do nothing if no record is found for passed in id" do
                expect(Timezone).not_to receive(:lookup)
                expect(user).not_to receive(:update!)
                instance.perform('CareerProfile', SecureRandom.uuid)
            end
        end

        describe "when record is found" do

            before(:each) do
                allow(CareerProfile).to receive(:find_by_id).and_return(@user.career_profile)
                ensure_timezone_cannot_be_derived_from_another_record
            end

            describe "when derived from another record" do

                it "should check career_profiles for matching place_id and not lookup_timezone" do
                    # ensure the timezone can be derived from another record of its type
                    timezone_name = 'America/Foo'
                    another_career_profile = CareerProfile.where.not(id: user.career_profile.id).first
                    another_career_profile.update_column(:place_id, user.career_profile.place_id)
                    another_career_profile.user.update_column(:timezone, timezone_name)

                    expect(user.career_profile).not_to receive(:lookup_timezone)
                    expect(user).to receive(:update!).with(timezone: timezone_name)
                    instance.perform('CareerProfile', user.career_profile.id)
                end

                it "should check hiring_applications for matching place_id and not lookup_timezone" do
                    # ensure the timezone can be derived from another record not of its type
                    timezone_name = 'America/Foo'
                    hiring_application = HiringApplication.first
                    hiring_application.update_column(:place_id, user.career_profile.place_id)
                    hiring_application.user.update_column(:timezone, timezone_name)

                    expect(user.career_profile).not_to receive(:lookup_timezone)
                    expect(user).to receive(:update!).with(timezone: timezone_name)
                    instance.perform('CareerProfile', user.career_profile.id)
                end

            end

            describe "when not derived from another record" do

                it "should do nothing if in development env" do
                    expect(Rails.env).to receive(:development?).and_return(true)
                    expect(user.career_profile).not_to receive(:lookup_timezone)
                    expect(user).not_to receive(:update!)
                    instance.perform('CareerProfile', user.career_profile.id)
                end

                it "should not update user's timezone if lookup_timezone returned nil" do
                    expect(user.career_profile).to receive(:lookup_timezone).and_return(nil)
                    expect(user).not_to receive(:update!)
                    instance.perform('CareerProfile', user.career_profile.id)
                end

                it "should capture_exception for Timezone::Error::InvalidZone error that occurred during the timezone lookup and not update user's timezone" do
                    error = Timezone::Error::InvalidZone.new
                    expect(user).not_to receive(:update!)
                    expect(user.career_profile).to receive(:lookup_timezone).and_raise(error)
                    expect(Raven).to receive(:capture_exception).with(error, {
                        extra: {
                            "career_profile_id": user.career_profile.id,
                            "user_id": user.id,
                            "place_id": user.career_profile.place_id,
                            "lat": user.career_profile.place_details['lat'],
                            "lng": user.career_profile.place_details['lng']
                        }
                    }).and_return('foo')
                    instance.perform('CareerProfile', user.career_profile.id)
                end

                it "should update user's timezone if lookup_timezone returned a timezone" do
                    timezone_name = 'America/New_York'
                    expect(user.career_profile).to receive(:lookup_timezone).and_return(timezone_name)
                    expect(user).to receive(:update!).with(timezone: timezone_name)
                    instance.perform('CareerProfile', user.career_profile.id)
                end
            end
        end
    end

    def ensure_timezone_cannot_be_derived_from_another_record
        CareerProfile.where(place_id: user.career_profile.place_id).destroy_all
        HiringApplication.where(place_id: user.career_profile.place_id).destroy_all
    end
end