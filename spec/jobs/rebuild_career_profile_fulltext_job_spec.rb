require 'spec_helper'

describe RebuildCareerProfileFulltextJob do
    attr_accessor :career_profile, :instance

    fixtures :users

    before(:each) do
        @career_profile_id = CareerProfile.first.id
        @instance = RebuildCareerProfileFulltextJob.new
    end

    describe "self.perform_later_with_debounce" do
        before(:each) do
            ActiveRecord::Base.connection.execute("TRUNCATE delayed_jobs RESTART IDENTITY")
            @career_profile_job_query = Delayed::Job
                .where(queue: "rebuild_career_profile_fulltext")
                .where("handler ILIKE '\%arguments\%#{@career_profile_id}\%'")
        end

        it "should update the run_at for a pending job instead of creating a new job" do
            RebuildCareerProfileFulltextJob.perform_later(@career_profile_id)

            job = @career_profile_job_query.first

            mock_time = Time.now
            allow(Time).to receive(:now).and_return(mock_time)
            RebuildCareerProfileFulltextJob.perform_later_with_debounce(@career_profile_id)

            expect(@career_profile_job_query.count).to be(1)
            expect(job.reload.run_at).to be_within(1.second).of(mock_time + 1.minute)
        end

        it "should create a new job for the future if no pending job due to being locked" do
            RebuildCareerProfileFulltextJob.perform_later(@career_profile_id)

            job = @career_profile_job_query.first
            job.update(locked_at: Time.now) # job is not pending if it is currently running

            RebuildCareerProfileFulltextJob.perform_later_with_debounce(@career_profile_id)
            expect(@career_profile_job_query.count).to eq(2) # expect a new job to be created since the prior one is now running
        end

        it "should create a new job for the future if no pending job due to being failed" do
            RebuildCareerProfileFulltextJob.perform_later(@career_profile_id)

            job = @career_profile_job_query.first
            job.update(failed_at: Time.now) # job is not pending if it is currently running

            RebuildCareerProfileFulltextJob.perform_later_with_debounce(@career_profile_id)
            expect(@career_profile_job_query.count).to eq(2) # expect a new job to be created since the prior one is now running
        end

        it "should create a new job for the future if no pending job due to different arguments" do
            different_career_profile = CareerProfile.where.not(id: @career_profile_id).first
            RebuildCareerProfileFulltextJob.perform_later(different_career_profile.id)

            RebuildCareerProfileFulltextJob.perform_later_with_debounce(@career_profile_id)
            expect(@career_profile_job_query.count).to eq(1)
            expect(Delayed::Job.where(queue: "rebuild_career_profile_fulltext").count).to eq(2)
        end
    end

    describe "perform" do

        describe "with privacy settings set to 'full'" do

            before(:each) do
                allow_any_instance_of(User).to receive(:pref_student_network_privacy).and_return('full')
            end

            it "should include student_network_looking_for" do
                career_profile = CareerProfile
                                    .where(last_calculated_complete_percentage: 100)
                                    .first
                career_profile.student_network_looking_for = ['some_key']
                career_profile.save!
                RebuildCareerProfileFulltextJob.perform_now(career_profile.id)
                fulltext = CareerProfile.connection.execute("select * from career_profile_fulltext where career_profile_id='#{career_profile.id}'").to_a[0]
                {
                    'keyword_text' => :not_to,
                    'student_network_full_keyword_text' => :to,
                    'student_network_anonymous_keyword_text' => :to
                }.each do |field, meth|
                    expect(fulltext[field]).send(meth, match(career_profile.student_network_looking_for[0]))
                end
            end

            it "should include name and nickname" do
                career_profile = CareerProfile.joins(:user)
                                    .where(last_calculated_complete_percentage: 100)
                                    .where.not(users: {name: nil})
                                    .first
                career_profile.user.nickname = 'nickname'
                career_profile.user.save!
                RebuildCareerProfileFulltextJob.perform_now(career_profile.id)
                fulltext = CareerProfile.connection.execute("select * from career_profile_fulltext where career_profile_id='#{career_profile.id}'").to_a[0]
                ['name', 'nickname'].each do |attr|
                    expect(fulltext['keyword_text']).not_to match(career_profile.user[attr])
                    expect(fulltext['student_network_full_keyword_text']).to match(career_profile.user[attr])
                    expect(fulltext['student_network_anonymous_keyword_text']).not_to match(career_profile.user[attr])
                end
            end

            it "should include student_network_interests" do
                career_profile = CareerProfile
                                    .where(last_calculated_complete_percentage: 100)
                                    .first
                career_profile = CareerProfile.update_from_hash!(career_profile.as_json(view: 'editable').merge(
                    student_network_interests: [{
                        locale: 'en',
                        text: 'some_interest'
                    }]
                ))
                RebuildCareerProfileFulltextJob.perform_now(career_profile.id)
                fulltext = CareerProfile.connection.execute("select * from career_profile_fulltext where career_profile_id='#{career_profile.id}'").to_a[0]
                {
                    'keyword_text' => :not_to,
                    'student_network_full_keyword_text' => :to,
                    'student_network_anonymous_keyword_text' => :to
                }.each do |field, meth|
                    expect(fulltext[field]).send(meth, match(career_profile.student_network_interests.first.text))
                end
            end

            it "should include accepted cohort title" do
                career_profile = CareerProfile.joins(:user => :cohort_applications)
                                    .where(last_calculated_complete_percentage: 100)
                                    .where("cohort_applications.status = 'accepted'")
                                    .first

                RebuildCareerProfileFulltextJob.perform_now(career_profile.id)
                fulltext = CareerProfile.connection.execute("select * from career_profile_fulltext where career_profile_id='#{career_profile.id}'").to_a[0]
                {
                    'keyword_text' => :not_to,
                    'student_network_full_keyword_text' => :to,
                    'student_network_anonymous_keyword_text' => :to
                }.each do |field, meth|
                    expect(fulltext[field]).send(meth, match(career_profile.user.accepted_application.published_cohort.title))
                end
            end

            it "should save education experience info" do
                career_profile = CareerProfile.joins(:education_experiences => :educational_organization)
                                    .where(last_calculated_complete_percentage: 100)
                                    .where.not("educational_organization_options.text" => nil)
                                    .first
                education_experience = career_profile.education_experiences.first
                education_experience.major = "some major"
                education_experience.minor = "some minor"
                education_experience.save! # i do not know why this has to be saved
                RebuildCareerProfileFulltextJob.perform_now(career_profile.id)
                fulltext = CareerProfile.connection.execute("select * from career_profile_fulltext where career_profile_id='#{career_profile.id}'").to_a[0]

                [
                    'keyword_text',
                    'student_network_full_keyword_text',
                    'student_network_anonymous_keyword_text'
                ].each do |field|
                    expect(fulltext[field]).to match(education_experience.educational_organization.text)
                    expect(fulltext[field]).to match(education_experience.major)
                end

                expect(fulltext['keyword_text']).to match(education_experience.minor)
                expect(fulltext['student_network_full_keyword_text']).not_to match(education_experience.minor)
                expect(fulltext['student_network_anonymous_keyword_text']).not_to match(education_experience.minor)
            end

            it "should save work experience info" do
                career_profile = CareerProfile.joins(:work_experiences => :professional_organization)
                                    .where(last_calculated_complete_percentage: 100)
                                    .where.not("professional_organization_options.text" => nil)
                                    .where.not("work_experiences.job_title" => nil)
                                    .first
                work_experience = career_profile.work_experiences.first
                expect(work_experience.responsibilities).not_to be_empty
                expect(work_experience.industry).not_to be_nil

                RebuildCareerProfileFulltextJob.perform_now(career_profile.id)
                fulltext = CareerProfile.connection.execute("select * from career_profile_fulltext where career_profile_id='#{career_profile.id}'").to_a[0]

                [
                    'keyword_text',
                    'student_network_full_keyword_text',
                    'student_network_anonymous_keyword_text'
                ].each do |field|
                    expect(fulltext[field]).to match(work_experience.professional_organization.text)
                    expect(fulltext[field]).to match(work_experience.job_title)
                    expect(fulltext[field]).to match(work_experience.industry)
                end

                expect(fulltext['keyword_text']).to match(work_experience.responsibilities[0])
                expect(fulltext['student_network_full_keyword_text']).not_to match(work_experience.responsibilities[0])
                expect(fulltext['student_network_anonymous_keyword_text']).not_to match(work_experience.responsibilities[0])
            end

            it "should save location info" do
                career_profile = CareerProfile.where.not(location: nil)
                                    .where(last_calculated_complete_percentage: 100).first

                RebuildCareerProfileFulltextJob.perform_now(career_profile.id)
                fulltext = CareerProfile.connection.execute("select * from career_profile_fulltext where career_profile_id='#{career_profile.id}'").to_a[0]

                [
                    'keyword_text',
                    'student_network_full_keyword_text',
                    'student_network_anonymous_keyword_text'
                ].each do |field|
                    expect(fulltext[field]).to match(career_profile.city_name)
                end
            end

            it "should save more career network stuff" do
                career_profile = CareerProfile.where.not(personal_fact: nil)
                                    .where.not(top_personal_descriptors: nil)
                                    .where.not(top_workplace_strengths: nil)
                                    .where.not(job_sectors_of_interest: nil)
                                    .where(last_calculated_complete_percentage: 100).first

                RebuildCareerProfileFulltextJob.perform_now(career_profile.id)
                fulltext = CareerProfile.connection.execute("select * from career_profile_fulltext where career_profile_id='#{career_profile.id}'").to_a[0]
                {
                    'keyword_text' => :to,
                    'student_network_full_keyword_text' => :not_to,
                    'student_network_anonymous_keyword_text' => :not_to
                }.each do |field, meth|
                    expect(fulltext[field]).send(meth, match(career_profile.personal_fact))
                    expect(fulltext[field]).send(meth, match(career_profile.top_personal_descriptors[0]))
                    expect(fulltext[field]).send(meth, match(career_profile.top_workplace_strengths[0]))
                    expect(fulltext[field]).send(meth, match(career_profile.job_sectors_of_interest[0]))
                end
            end

            it "should include skills" do
                career_profile = CareerProfile
                                    .joins(:skills)
                                    .where(last_calculated_complete_percentage: 100)
                                    .first
                RebuildCareerProfileFulltextJob.perform_now(career_profile.id)
                fulltext = CareerProfile.connection.execute("select * from career_profile_fulltext where career_profile_id='#{career_profile.id}'").to_a[0]
                {
                    'keyword_text' => :to,
                    'student_network_full_keyword_text' => :not_to,
                    'student_network_anonymous_keyword_text' => :not_to
                }.each do |field, meth|
                    expect(fulltext[field]).send(meth, match(career_profile.skills.first.text))
                end
            end

            it "should include awards and interests" do
                career_profile = CareerProfile
                                    .where(last_calculated_complete_percentage: 100)
                                    .joins(:awards_and_interests).first
                RebuildCareerProfileFulltextJob.perform_now(career_profile.id)
                fulltext = CareerProfile.connection.execute("select * from career_profile_fulltext where career_profile_id='#{career_profile.id}'").to_a[0]
                {
                    'keyword_text' => :to,
                    'student_network_full_keyword_text' => :not_to,
                    'student_network_anonymous_keyword_text' => :not_to
                }.each do |field, meth|
                    expect(fulltext[field]).send(meth, match(career_profile.awards_and_interests.first.text))
                end
            end
        end

        describe "with privacy settings not set to 'full'" do

            before(:each) do
                allow_any_instance_of(User).to receive(:pref_student_network_privacy).and_return('anything_but_full')
            end

            it "should not include private stuff in the full keyword search" do
                career_profile = CareerProfile.joins(:user)
                    .where.not(users: {name: nil})
                    .where(last_calculated_complete_percentage: 100)
                    .first
                career_profile.user.nickname = 'nickname'

                RebuildCareerProfileFulltextJob.perform_now(career_profile.id)
                fulltext = CareerProfile.connection.execute("select * from career_profile_fulltext where career_profile_id='#{career_profile.id}'").to_a[0]
                ['name', 'nickname'].each do |attr|
                    expect(fulltext['student_network_full_keyword_text']).not_to match(career_profile.user[attr])
                end

                expect(fulltext['student_network_full_keyword_text']).to eq(fulltext['student_network_anonymous_keyword_text'])
            end

        end

        it "should not fail when no matching record exists" do
            expect {
                RebuildCareerProfileFulltextJob.perform_now('non-existent-id')
            }.not_to raise_error
        end

    end
end