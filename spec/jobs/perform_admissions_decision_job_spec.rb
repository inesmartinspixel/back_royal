require 'spec_helper'

describe PerformAdmissionsDecisionJob do
    attr_accessor :instance

    before(:each) do
        self.instance = PerformAdmissionsDecisionJob.new
    end

    describe "perform" do
        it "should call perform_admissions_decision on the application" do
            id = CohortApplication.first.id
            expect(CohortApplication).to receive(:find).with(id).and_call_original
            expect_any_instance_of(CohortApplication).to receive(:perform_admissions_decision)
            instance.perform(CohortApplication.first.id)
        end
    end
end