require 'spec_helper'

describe DailyMarketingUpdate do
    attr_reader :instance

    before(:each) do
        @instance = DailyMarketingUpdate.new
        @instance.ensure_refreshed
    end

    describe "attachments" do

        it "should not raise even with no new visitors" do
            expect(instance).to receive(:new_visitor_count).at_least(1).times.and_return(0)
            expect(Proc.new {
                instance.attachments
            }).not_to raise_error
        end

        it "should not raise when there are some visitors" do
            expect(instance).to receive(:new_visitor_count).at_least(1).times.and_return(42)
            expect(Proc.new {
                instance.attachments
            }).not_to raise_error
        end

    end

    describe "perform" do

        it "should log message to slack" do

            start = Time.now.utc.beginning_of_day - 1.day
            expect(instance).to receive(:yesterday_start).and_return(start)

            text = "*Daily Smart.ly Update - #{start.strftime('%B %d')}*"

            expect(LogToSlack).to receive(:perform_later).with(Slack.marketing_notify_channel, text,
                [{
                    :pretext=> "_Except where indicated, all numbers include all external, consumer users. (i.e. all non-institutional, non-editor users with email addresses other than @pedago.com, @workaround.com, etc.)  Visitors who never login are assumed to be external consumer users and so are also included._",
                    :color=>"#36A64F",
                    :fields=> [{:title=>"metric1", :value=>"value1", :short=>true},
                                {:title=>"metric2", :value=>"value2", :short=>true}],
                    :mrkdwn_in=>["pretext"]}]

            )
            expect(instance).to receive(:metrics).and_return([['metric1', 'value1'], ['metric2', 'value2']])
            instance.perform
        end

    end

end