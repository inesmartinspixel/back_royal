require 'spec_helper'

describe ProcessCompletedSignNowDocumentJob do

    attr_accessor :instance, :doc, :signable_document

    fixtures(:users)

    before(:each) do
        @instance = ProcessCompletedSignNowDocumentJob.new

        @doc = SignNow::Document.new(
            id: SecureRandom.uuid
        )

        expect(doc).to receive(:generate_signing_link).and_return(OpenStruct.new({
            url: "http://path/to_doc",
            expiry: Time.now
        }))
        expect(SignNow::Document).to receive(:create).with(template_id: 'template_id').and_return(doc)
        expect_any_instance_of(SignNow::Document).to receive(:update_smart_fields).with({ some: 'values' })

        @signable_document = SignableDocument.create_with_sign_now(
            template_id: 'template_id',
            document_type: SignableDocument::TYPE_ENROLLMENT_AGREEMENT,
            user_id: User.first.id,
            smart_field_values: { some: 'values' },
            metadata: {
                'some' => 'metadata'
            }
        )
    end

    describe "ensure_job" do

        it "should not create a new job if one exists" do
            ProcessCompletedSignNowDocumentJob.perform_later(doc.id)
            job = call_ensure_job_and_return_new_job
            expect(job).to be_nil
        end

        it "should create a new job if none exists" do
            ProcessCompletedSignNowDocumentJob.perform_later('some_other_doc.id')
            job = call_ensure_job_and_return_new_job
            expect(job).not_to be_nil
            expect(job.payload_object.job_data['arguments']).to eq([doc.id])
            expect(job.run_at).to be_within(1.second).of(Time.now)
        end

        def call_ensure_job_and_return_new_job
            start = Time.now
            ProcessCompletedSignNowDocumentJob.ensure_job(doc.id)
            Delayed::Job.where(queue: ProcessCompletedSignNowDocumentJob.queue_name).where("created_at > ?", start).first
        end

    end

    describe "perform" do
        it "should retrieve and download a document" do
            expect(SignableDocument).to receive(:find_by_sign_now_document_id).with(doc.id) { signable_document }
            expect(signable_document).to receive(:download_completed_document_from_sign_now)
            instance.perform(doc.id)
        end

        it "should log if it doesn't exist" do
            expect(SignableDocument).to receive(:find_by_sign_now_document_id).with(doc.id) { nil }
            expect_any_instance_of(SignableDocument).not_to receive(:download_completed_document_from_sign_now)
            expect(Raven).to receive(:capture_exception).with("No signable_document found", {
                extra: {
                    sign_now_document_id: doc.id
                }
            })
            instance.perform(doc.id)
        end
    end
end