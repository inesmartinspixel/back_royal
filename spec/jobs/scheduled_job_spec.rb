require 'spec_helper'

describe ScheduledJob do

    class JobWithScheduleInterval < ApplicationJob
        include ScheduledJob

        queue_as :job_with_schedule_interval
        self.schedule_interval 10.minutes

        def perform
            track
        end

        def track
        end
    end

    class JobWithRunAtHour < ApplicationJob
        include ScheduledJob

        queue_as :job_with_run_at_hour
        run_at_hour 4

        def perform
            track
        end

        def track
        end
    end

    class AnotherJobWithRunAtHour < ApplicationJob
        include ScheduledJob

        queue_as :another_job_with_run_at_hour
        run_at_hour 4 # same run_at_hour as JobWithRunAtHour

        def perform
            track
        end

        def track
        end
    end

    class YetAnotherJobWithRunAtHour < ApplicationJob
        include ScheduledJob

        queue_as :yet_another_job_with_run_at_hour
        run_at_hour 4 # same run_at_hour as JobWithRunAtHour and AnotherJobWithRunAtHour

        def perform
            track
        end

        def track
        end
    end

    class YupYetAnotherJobWithRunAtHour < ApplicationJob
        include ScheduledJob

        queue_as :yup_yet_another_job_with_run_at_hour
        run_at_hour 4 # same run_at_hour as JobWithRunAtHour, AnotherJobWithRunAtHour, and YetAnotherJobWithRunAtHour

        def perform
            track
        end

        def track
        end
    end

    before(:each) do
        [
            JobWithRunAtHour,
            AnotherJobWithRunAtHour,
            YetAnotherJobWithRunAtHour,
            YupYetAnotherJobWithRunAtHour
        ].each do |klass|
            klass.run_on_day nil
            klass.run_at_minute nil
        end
        Delayed::Job.where(queue: [
            'job_scheduler',
            'job_with_schedule_interval',
            'job_with_run_at_hour',
            'another_job_with_run_at_hour',
            'yet_another_job_with_run_at_hour',
            'yup_yet_another_job_with_run_at_hour'
        ]).delete_all
    end

    # https://trello.com/c/GaJrOohk/1877-bug-fix-scheduledjob-to-not-have-the-possibility-of-failing-to-run-if-related-job-isn-t-complete-when-its-locked
    # this one users job_with_schedule_interval, but should be the same regardless of how
    # it is scheduled
    it "should still schedule a new job even if this one takes a few moments to get destroyed" do
        # we schedule a job manually, check it is in the db, then invoke it manually
        JobWithScheduleInterval.perform_later
        expect(initial_job = Delayed::Job.find_by_queue('job_with_schedule_interval')).not_to be_nil

        # we stub out the destroy so that we can simulate the case where the job_scheduler_job
        # runs before the initial job is destroyed
        lock_and_run_job(initial_job, {destroy: false})

        # Since the initial_job is still locked, the job_scheduler_job will not create a new
        # job_with_schedule_interval job
        expect(job_scheduler_job = Delayed::Job.find_by_queue('job_scheduler')).not_to be_nil
        expect {
            lock_and_run_job(job_scheduler_job)
        }.not_to change {Delayed::Job.where(queue: 'job_with_schedule_interval').count }


        # so once the initial job is completed and destroyed, there is no job_with_schedule_interval
        # job scheduled.
        (initial_job).destroy
        expect(next_job = Delayed::Job.find_by_queue('job_with_schedule_interval')).to be_nil

        # Before we fixed the ticket referenced above, this would be the final state,
        # and we would stop running the scheduled job until the next app restart or deployment.
        # The fix is to have the job_scheduler_job recognize that there is a locked job,
        # and try again in 10 seconds
        expect(job_scheduler_job = Delayed::Job.find_by_queue('job_scheduler')).not_to be_nil
        expect(job_scheduler_job.run_at).to be_within(5.seconds).of(Time.now + 10.seconds)
        expect {
            lock_and_run_job(job_scheduler_job)
        }.to change {Delayed::Job.where(queue: 'job_with_schedule_interval').count }

    end

    describe "with schedule_interval" do

        it "should schedule a new job when one is complete" do
            next_job = assert_runs_and_schedules_a_new_job(JobWithScheduleInterval)

            # check that it was scheduled 10 minutes from now, since that is the schedule interval
            expect(next_job.run_at).to be_within(5.seconds).of(Time.now + JobWithScheduleInterval.schedule_interval)
        end

    end

    describe "with run_at_hour" do

        it "should schedule a new job today if the hour has not yet happened today" do
            time_zone = 'Eastern Time (US & Canada)'
            # now 1 hour before the run_at_hour
            now = ActiveSupport::TimeZone[time_zone].parse("2017/08/28 #{JobWithRunAtHour.run_at_hour-1}:00")
            allow(Time).to receive(:now).and_return(now)

            next_job = assert_runs_and_schedules_a_new_job(JobWithRunAtHour)

            expected_time = Time.now.in_time_zone('Eastern Time (US & Canada)').change({
                hour: JobWithRunAtHour.run_at_hour,
                min: 0,
                sec: 0
            })
            expect(next_job.run_at).to eq(expected_time)
        end

        it "should schedule a new job today if the hour has already yet happened today" do
            time_zone = 'Eastern Time (US & Canada)'
            # now is 1 hour after the run_at_hour
            now = ActiveSupport::TimeZone[time_zone].parse("2017/08/28 #{JobWithRunAtHour.run_at_hour+1}:00")
            allow(Time).to receive(:now).and_return(now)

            next_job = assert_runs_and_schedules_a_new_job(JobWithRunAtHour)

            expected_time = Time.now.in_time_zone('Eastern Time (US & Canada)').change({
                hour: JobWithRunAtHour.run_at_hour,
                min: 0,
                sec: 0
            }) + 1.day
            expect(next_job.run_at).to eq(expected_time)
        end

        describe "with run_at_minute" do

            before(:each) do
                JobWithRunAtHour.run_at_minute nil # just run_at_hour without run_at_minute
                AnotherJobWithRunAtHour.run_at_minute 15 # unique run_at_minute
                YetAnotherJobWithRunAtHour.run_at_minute 30 # same run_at_minute as YupYetAnotherJobWithRunAtHour
                YupYetAnotherJobWithRunAtHour.run_at_minute 30 # same run_at_minute as YetAnotherJobWithRunAtHour
                allow(ScheduledJob).to receive(:registered_klasses).and_return([JobWithRunAtHour, AnotherJobWithRunAtHour, YetAnotherJobWithRunAtHour, YupYetAnotherJobWithRunAtHour])
            end

            it "should schedule job to run at the minute of the run_at_hour even if another job is scheduled for that time slot" do
                time_zone = 'Eastern Time (US & Canada)'
                # now is 1 hour before the run_at_hour
                now = ActiveSupport::TimeZone[time_zone].parse("2017/08/28 #{JobWithRunAtHour.run_at_hour-1}:00")
                allow(Time).to receive(:now).and_return(now)

                # Even though YetAnotherJobWithRunAtHour and YupYetAnotherJobWithRunAtHour have the same run_at_minute,
                # they should still be scheduled for that time. We'll get a Sentry warning about the jobs being scheduled
                # for the same time slot so we can fix it (tested in a separate spec).
                expect(YetAnotherJobWithRunAtHour.run_at_minute).to eq(YupYetAnotherJobWithRunAtHour.run_at_minute)
                next_job = assert_runs_and_schedules_a_new_job(YetAnotherJobWithRunAtHour)

                expected_time = Time.now.in_time_zone('Eastern Time (US & Canada)').change({
                    hour: JobWithRunAtHour.run_at_hour,
                    min: 30,
                    sec: 0
                })
                expect(next_job.run_at).to eq(expected_time)
            end

            it "should send a warning to Sentry if jobs have been configured with the same run_at_hour and run_at_minute" do
                time_zone = 'Eastern Time (US & Canada)'
                # now is 1 hour before the run_at_hour
                now = ActiveSupport::TimeZone[time_zone].parse("2017/08/28 #{JobWithRunAtHour.run_at_hour-1}:00")
                allow(Time).to receive(:now).and_return(now)

                # verify that the run_at_hour values are all the same for these jobs
                expect(JobWithRunAtHour.run_at_hour).to eq(AnotherJobWithRunAtHour.run_at_hour)
                expect(AnotherJobWithRunAtHour.run_at_hour).to eq(YetAnotherJobWithRunAtHour.run_at_hour)
                expect(YetAnotherJobWithRunAtHour.run_at_hour).to eq(YupYetAnotherJobWithRunAtHour.run_at_hour)

                # JobWithRunAtHour and AnotherJobWithRunAtHour don't have conflicting run_at_minute values,
                # so they shouldn't trigger the warning
                allow(Raven).to receive(:capture_exception)
                next_job_with_run_at_hour = assert_runs_and_schedules_a_new_job(JobWithRunAtHour)
                next_another_job_with_run_at_hour = assert_runs_and_schedules_a_new_job(AnotherJobWithRunAtHour)
                expect(Raven).not_to have_received(:capture_exception)

                # YetAnotherJobWithRunAtHour and YupYetAnotherJobWithRunAtHour have the same run_at_minute value though,
                # so when one of those jobs is scheduled, we should get a warning.
                next_yet_another_job_with_run_at_hour = assert_runs_and_schedules_a_new_job(YetAnotherJobWithRunAtHour)
                expect(Raven).to have_received(:capture_exception).with('Jobs have been configured with the same run_at_hour and run_at_minute. Reconfigure the following jobs to run at different times.', {
                    level: 'warning',
                    extra: {
                        jobs_with_same_run_at: ['YupYetAnotherJobWithRunAtHour', 'YetAnotherJobWithRunAtHour']
                    }
                })
            end
        end
    end

    describe "with run_on_day" do

        before(:each) do
            JobWithRunAtHour.run_on_day :monday
        end

        it "should schedule a new job today if the hour has not yet happened today" do
            time_zone = 'Eastern Time (US & Canada)'
            # now monday, 1 hour before the run_at_hour
            now = ActiveSupport::TimeZone[time_zone].parse("2017/08/28 #{JobWithRunAtHour.run_at_hour-1}:00")
            allow(Time).to receive(:now).and_return(now)

            next_job = assert_runs_and_schedules_a_new_job(JobWithRunAtHour)

            expected_time = Time.now.in_time_zone('Eastern Time (US & Canada)').change({
                hour: JobWithRunAtHour.run_at_hour,
                min: 0,
                sec: 0
            })
            expect(next_job.run_at).to eq(expected_time)
        end

        it "should schedule a new job next week if the hour has already yet happened today" do
            time_zone = 'Eastern Time (US & Canada)'
            # now is monday, 1 hour after the run_at_hour
            now = ActiveSupport::TimeZone[time_zone].parse("2017/08/28 #{JobWithRunAtHour.run_at_hour+1}:00")
            allow(Time).to receive(:now).and_return(now)

            next_job = assert_runs_and_schedules_a_new_job(JobWithRunAtHour)

           # job should be scheduled for next monday at the run_at_hour
            expect(next_job.run_at).to eq(ActiveSupport::TimeZone[time_zone].parse("2017/09/04 #{JobWithRunAtHour.run_at_hour}:00"))
        end

        it "should schedule a new job on the appropriate day if that day is not today" do
            JobWithRunAtHour.run_on_day :wednesday
            time_zone = 'Eastern Time (US & Canada)'

            # now is monday
            now = ActiveSupport::TimeZone[time_zone].parse("2017/08/28")
            allow(Time).to receive(:now).and_return(now)

            next_job = assert_runs_and_schedules_a_new_job(JobWithRunAtHour)

            # job should be scheduled for wednesday at the run_at_hour
            expect(next_job.run_at.in_time_zone(time_zone)).to eq(ActiveSupport::TimeZone[time_zone].parse("2017/08/30 #{JobWithRunAtHour.run_at_hour}:00"))
        end

    end

    def assert_runs_and_schedules_a_new_job(klass)
        queue = klass.queue_name
        # we schedule a job manually, check it is in the db, then invoke it manually
        klass.perform_later
        expect(initial_job = Delayed::Job.find_by_queue(queue)).not_to be_nil
        lock_and_run_job(initial_job)

        # when the first job finished, a new job in the job_scheduler queue should
        # have been created.  This job will scheduled the next job
        expect(job_scheduler_job = Delayed::Job.find_by_queue('job_scheduler')).not_to be_nil
        expect(job_scheduler_job.payload_object.job_data['arguments'][0]).to eq(klass.name)

        # invoking the job_scheduler_job should create a new job
        expect(Delayed::Job.find_by_queue(queue)).to be_nil
        lock_and_run_job(job_scheduler_job)
        expect(next_job = Delayed::Job.find_by_queue(queue)).not_to be_nil
        next_job
    end

    def lock_and_run_job(job, options = {})
        options[:destroy] = true unless options.key?(:destroy)
        job.invoke_job
        if options[:destroy]
            job.destroy
        else
            job.update(locked_at: Time.now)
        end
    end



end
