require 'spec_helper'

describe DeleteFromAirtableJob do
    attr_accessor :user, :instance, :mock_cohort_application_version_attrs, :mock_airtable_application

    fixtures(:users)

    before(:each) do
        self.user = users(:learner)
        self.instance = DeleteFromAirtableJob.new

        self.mock_cohort_application_version_attrs = {:airtable_base_key => "AIRTABLE_BASE_KEY_LIVE"}
        self.mock_airtable_application = Class.new(Airtable::Application)
    end

    describe "perform" do

        before(:each) do
            # expect it to be called with AIRTABLE_BASE_KEY_LIVE because we've mocked it above (see mock_cohort_application_version_attrs)
            # and we're passing mock_cohort_application_version_attrs into the perform each time
            expect(Airtable::Application).to receive(:klass_for_base_key).with("AIRTABLE_BASE_KEY_LIVE").and_return(Airtable::Application)
        end

        # It's VERY important that we use the `find_by_id_with_fallback` method rather than `find_by_id` for a couple of reasons:
        # 1) The `find_by_id_with_fallback` method first searches by the `airtable_record_id` and then falls back to searching by
        #   the `Application ID`. This fallback to searching by the `Application ID` will throw an error if it can't find the Airtable
        #   base. We rely on this error to make the job fail. Otherwise, if the job succeeds without having successfully destroyed
        #   the associated records in Airtable, we'd probably never know about it and we'd be in violation of GDPR.
        # 2) The `find_by_id` method uses the `airtable_record_id` to find the associated record in Airtable. However, this column
        #   value is changed when an Airtable base is duplicated, so we can't rely only on the `airtable_record_id`; we need to fallback
        #   to the `Application ID`.
        it "should find_by_id_with_fallback" do
            expect(Airtable::Application).to receive(:find_by_id_with_fallback).with(OpenStruct.new(mock_cohort_application_version_attrs))
            instance.perform([mock_cohort_application_version_attrs])
        end

        it "should destroy an application if found" do
            allow(Airtable::Application).to receive(:find_by_id_with_fallback).and_return(mock_airtable_application)
            expect(mock_airtable_application).to receive(:destroy)
            instance.perform([mock_cohort_application_version_attrs])
        end

        it "should not destroy if no application is found" do
            allow(Airtable::Application).to receive(:find_by_id_with_fallback).and_return(nil)
            expect_any_instance_of(Airtable::Application).not_to receive(:destroy)
            instance.perform([mock_cohort_application_version_attrs])
        end
    end
end
