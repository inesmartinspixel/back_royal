require 'spec_helper'

describe SyncFromAirtableBatchHelper do

    class SomeMasterJob < ApplicationJob
        include SyncFromAirtableBatchHelper::MasterJobHelper
    end

    class SomeBatchJob < ApplicationJob
        include SyncFromAirtableBatchHelper::BatchJobHelper
    end

    describe "::BUCKETS" do

        it "should be an array of characters that can be used as the first character in a valid uuid" do
            expect(SyncFromAirtableBatchHelper::BUCKETS).to eq(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'])
        end
    end

    describe "::MasterJobHelper" do
        attr_accessor :master_job

        before(:each) do
            @master_job = SomeMasterJob.new
        end

        describe "#enqueue_batch_jobs" do

            it "should enqueue a job for each character that can be used as the first character in a valid uuid (0-9 and a-f)" do
                SyncFromAirtableBatchHelper::BUCKETS.each do |bucket|
                    expect(SomeBatchJob).to receive(:perform_later).with(bucket: bucket)
                end
                master_job.enqueue_batch_jobs(SomeBatchJob)
            end

            it "should pass along any variable and keyword args to batch jobs" do
                SyncFromAirtableBatchHelper::BUCKETS.each do |bucket|
                    expect(SomeBatchJob).to receive(:perform_later).with('foo', 'bar', baz: 'baz', qux: 'qux', bucket: bucket)
                end
                master_job.enqueue_batch_jobs(SomeBatchJob, 'foo', 'bar', baz: 'baz', qux: 'qux')
            end
        end
    end

    describe "::BatchJobHelper" do
        attr_accessor :batch_job, :bucket

        before(:each) do
            @batch_job = SomeBatchJob.new
            @bucket = SyncFromAirtableBatchHelper::BUCKETS.sample
        end

        describe "fetch_and_process_records" do

            it "should get all Airtable and ActiveRecord objects, yielding to the passed in block to process each individual Airtable record" do
                view = 'Some View'
                fields = ['Some field', 'Another field']
                base_filter = nil
                application_id_column = Airtable::Application.column_map["id"]
                database_application = CohortApplication.first
                mock_airtable_application = {"#{application_id_column}" => database_application.id}
                block_called = false

                expect(Airtable::Application).to receive(:all).with(
                    view: view,
                    fields: fields,
                    filter: "LEFT({#{application_id_column}}, 1) = '#{bucket}'"
                ).and_return([mock_airtable_application])
                expect(CohortApplication).to receive(:where).with(id: [database_application.id]).and_return([database_application])
                mock_block = Proc.new do |airtable_application, database_applications_by_id|
                    expect(airtable_application).to be(mock_airtable_application)
                    expect(database_applications_by_id).to eq({"#{database_application.id}" => database_application})
                    block_called = true
                end

                batch_job.fetch_and_process_records(view, fields, bucket, base_filter, &mock_block)
                expect(block_called).to be(true)
            end

            it "should support a base_filter" do
                view = 'Some View'
                fields = ['Some field', 'Some other field']
                base_filter = "{Some field} != {Some other field}"
                application_id_column = Airtable::Application.column_map["id"]
                database_application = CohortApplication.first
                mock_airtable_application = {"#{application_id_column}" => database_application.id}
                mock_block = Proc.new { }

                expect(Airtable::Application).to receive(:all).with(
                    view: view,
                    fields: fields,
                    filter: "AND(#{base_filter}, LEFT({#{application_id_column}}, 1) = '#{bucket}')"
                ).and_return([mock_airtable_application])

                batch_job.fetch_and_process_records(view, fields, bucket, base_filter, &mock_block)
            end
        end
    end
end
