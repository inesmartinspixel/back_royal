require 'spec_helper'

describe AutoExpireOpenPositionsJob do

    before(:each) do
        @instance = AutoExpireOpenPositionsJob.new
        @position_to_expire_today = OpenPosition.first
        @position_to_expire_in_2_weeks = OpenPosition.second
    end

    describe "perform" do

        before(:each) do
            today = Date.today
            expect(OpenPosition).to receive(:where).with("archived = FALSE AND auto_expiration_date <= '#{today.to_s}'").and_return([@position_to_expire_today])
            expect(OpenPosition).to receive(:where).with("archived = FALSE AND auto_expiration_date = '#{(today + 2.weeks).to_s}'").and_return([@position_to_expire_in_2_weeks])
        end

        it "should auto_expire_positions! for positions that expire today" do
            expect(@instance).to receive(:auto_expire_positions!).with(@position_to_expire_today.hiring_manager_id, [@position_to_expire_today])
            @instance.perform
        end

        it "should log_event_to_external_systems! for positions to expire in 2 weeks" do
            allow(@instance).to receive(:auto_expire_positions!)
            expect(@instance).to receive(:log_event_to_external_systems!).with('auto_expiring_positions_warning', @position_to_expire_in_2_weeks.hiring_manager_id, [@position_to_expire_in_2_weeks])
            @instance.perform
        end
    end

    describe "auto_expire_positions!" do

        it "should set just_auto_expired instance attribute on position" do
            expect(@position_to_expire_today.just_auto_expired).not_to be(true)
            @instance.auto_expire_positions!(@position_to_expire_today.hiring_manager_id, [@position_to_expire_today])
            expect(@position_to_expire_today.just_auto_expired).to be(true)
        end

        it "should archive the position and ensure manually_archived is set to false" do
            expect(@position_to_expire_today).to receive(:update!).with(archived: true, manually_archived: false)
            @instance.auto_expire_positions!(@position_to_expire_today.hiring_manager_id, [@position_to_expire_today])
        end

        it "should reject all candidate_position_interests_needing_review for hiring manager" do
            candidate_position_interest = CandidatePositionInterest.first
            expect(@position_to_expire_today).to receive(:candidate_position_interests_needing_review).and_return([candidate_position_interest])
            expect(candidate_position_interest).to receive(:update!).with(hiring_manager_status: 'rejected')
            @instance.auto_expire_positions!(@position_to_expire_today.hiring_manager_id, [@position_to_expire_today])
        end

        it "should log_event_to_external_systems!" do
            expect(@instance).to receive(:log_event_to_external_systems!).with('auto_expired_positions', @position_to_expire_today.hiring_manager_id, [@position_to_expire_today])
            @instance.auto_expire_positions!(@position_to_expire_today.hiring_manager_id, [@position_to_expire_today])
        end
    end

    describe "log_event_to_external_systems!" do

        it "should log an event to external systems" do
            mock_event = double("Event instance")
            expect(Event).to receive(:create_server_event!).with(
                anything,
                @position_to_expire_today.hiring_manager_id,
                "hiring_manager:foo",
                {
                    position_titles: [@position_to_expire_today.title]
                }
            ).and_return(mock_event)
            expect(mock_event).to receive(:log_to_external_systems)
            @instance.log_event_to_external_systems!('foo', @position_to_expire_today.hiring_manager_id, [@position_to_expire_today])
        end

        it "should not log an event to external systems when user is not accepted" do
            hiring_manager = User.find(@position_to_expire_today.hiring_manager_id)
            hiring_manager.hiring_application.status = "pending"
            hiring_manager.hiring_application.save!

            expect(Event).not_to receive(:create_server_event!)
            @instance.log_event_to_external_systems!('foo', @position_to_expire_today.hiring_manager_id, [@position_to_expire_today])
        end
    end
end
