require 'spec_helper'

describe DeleteFromRedshiftJob do
    attr_accessor :user, :instance

    fixtures(:users)

    before(:each) do
        self.user = users(:learner)
        self.instance = DeleteFromRedshiftJob.new
    end

    describe "perform" do

        it "should delete redshift events" do
            RedshiftEvent.create!(
                id: SecureRandom.uuid,
                user_id: user.id,
                event_type: 'dropped the bomb',
                estimated_time: Time.parse('2016-01-01 00:00:00 UTC'),
                created_at: Time.parse('2016-01-01 00:00:00 UTC'),
                updated_at: Time.parse('2016-01-01 00:00:00 UTC')
            )

            # There does not seem to be a great way to get this number to be consistent here.
            # If we create an event for this user in another spec, or fixtures, or whatever,
            # and do not delete that event, it will impact this count. So I'm just making sure it
            # is not 0.
            expect(redshift_events_for_user.count).not_to eq(0)
            instance.perform(user.id)
            expect(redshift_events_for_user.count).to eq(0)
        end

    end

    def redshift_events_for_user

        return RedshiftEvent.where(user_id: user.id).to_a

    end

end
