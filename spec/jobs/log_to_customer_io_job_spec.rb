require 'spec_helper'

describe LogToCustomerIoJob do

    fixtures(:users)

    before(:each) do
        @instance = LogToCustomerIoJob.new
        @user = users(:learner)
        @event = Event.create_server_event!(
            SecureRandom.uuid,
            @user.id,
            'type',
            {
                a: {
                    b: 1
                },
                c: 2
            }
        )
        @instance.event_id = @event.id
        expect(Event).to receive(:find).with(@event.id).and_return(@event)
    end

    describe "perform" do

        it "should call track" do
            expect(@instance).to receive(:track)
            @instance.perform(@event.id)
        end

        it "should do nothing if no user is found from event.user_id" do
            @event.user_id = SecureRandom.uuid
            @event.save!
            expect(@instance).not_to receive(:track)
            @instance.perform(@event.id)
        end

    end

    describe "track" do

        it "should log specific properties to customer.io" do
            expect($customerio).to receive(:track).with(
                @event.user_id,
                @event.event_type,
                @event.payload.with_indifferent_access
            )
            @instance.perform(@event.id, nil)
        end

    end

    describe "get_payload_properties" do

        it "should work without specified keys" do
            @instance.keys = nil
            payload_properties = @instance.get_payload_properties
            expect(payload_properties).to eq({
                "a" => {
                    "b" => 1
                },
                "c" => 2,
                "server_event" => true
            })
        end

        it "should work with specified keys" do
            @instance.keys = ['c']
            payload_properties = @instance.get_payload_properties
            expect(payload_properties).to eq({
                "c" => 2
            })
        end

    end

end