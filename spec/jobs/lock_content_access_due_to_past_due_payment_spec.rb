require 'spec_helper'

describe LockContentAccessDueToPastDuePayment do
    fixtures :users

    before(:each) do
        @cohort_application = users(:accepted_emba_cohort_user).cohort_applications.first
        @cohort_application.update_column(:payment_grace_period_end_at, Time.now)
    end

    describe "perform" do

        describe "when !payment_past_due?" do

            it "should update payment_grace_period_end_at to nil and not update locked_due_to_past_due_payment to true" do
                expect(@cohort_application.payment_grace_period_end_at).not_to be_nil
                expect(@cohort_application.locked_due_to_past_due_payment).to be(false)
                expect_any_instance_of(CohortApplication).to receive(:payment_past_due?).and_return(false)

                LockContentAccessDueToPastDuePayment.perform_now(@cohort_application.id)

                @cohort_application.reload
                expect(@cohort_application.payment_grace_period_end_at).to be_nil
                expect(@cohort_application.locked_due_to_past_due_payment).to be(false)
            end
        end

        describe "when payment_past_due?" do

            it "should update payment_grace_period_end_at to nil and locked_due_to_past_due_payment to true" do
                expect(@cohort_application.payment_grace_period_end_at).not_to be_nil
                expect(@cohort_application.locked_due_to_past_due_payment).to be(false)
                allow_any_instance_of(CohortApplication).to receive(:payment_past_due?).and_return(true)

                LockContentAccessDueToPastDuePayment.perform_now(@cohort_application.id)

                @cohort_application.reload
                expect(@cohort_application.payment_grace_period_end_at).to be_nil
                expect(@cohort_application.locked_due_to_past_due_payment).to be(true)
            end

            # this should already be tested over in user_spec.rb, but it's an important
            # side effect of this delayed job, so some extra spec coverage is provided.
            it "should log_cohort_content_locked_event" do
                expect(@cohort_application.locked_due_to_past_due_payment).to be(false)
                allow_any_instance_of(CohortApplication).to receive(:in_good_standing?).and_return(false)
                expect_any_instance_of(CohortApplication).to receive(:log_cohort_content_locked_event)

                LockContentAccessDueToPastDuePayment.perform_now(@cohort_application.id)
            end
        end
    end
end
