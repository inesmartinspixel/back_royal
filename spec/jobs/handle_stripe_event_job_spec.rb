require 'spec_helper'

describe HandleStripeEventJob do
    attr_accessor :user, :instance, :stripe_event

    fixtures :users

    before(:each) do
        self.user = User.first
        self.instance = HandleStripeEventJob.new
        self.stripe_event = OpenStruct.new(id: 'foo_id', type: 'foo_type')
    end

    describe "reschedule_at" do
        it "should not apply exponential delay up to five attempts" do
            allow(instance).to receive(:rand).and_return(3.0)
            now = Time.now
            new_run_at = instance.reschedule_at(now, 5)
            expect(new_run_at).to be_within(1.second).of(now + 3.0)
        end

        it "should apply exponential delay after five attempts without factoring the initial five" do
            now = Time.now
            new_run_at = instance.reschedule_at(now, 6)
            expect(new_run_at).to be_within(1.second).of(now + 1 + 5)
        end
    end

    describe "stripe_jobs_for_user" do
        it "should find existing jobs for this user" do
            HandleStripeEventJob.perform_later(user.id, stripe_event.id, Marshal::dump(stripe_event))
            HandleStripeEventJob.perform_later(user.id, stripe_event.id, Marshal::dump(stripe_event))
            HandleStripeEventJob.perform_later(user.id, stripe_event.id, Marshal::dump(stripe_event))
            expect(HandleStripeEventJob.stripe_jobs_for_user(user.id).size).to be(3)
        end
    end

    describe "perform_later_with_delay" do
        it "should queue a handle_stripe_event job correctly" do
            now = Time.now
            allow(Time).to receive(:now).and_return(now)

            another_job = HandleStripeEventJob.perform_later(user.id, SecureRandom.uuid, Marshal::dump(OpenStruct.new(id: 'bar_id')))
            Delayed::Job.where(queue: HandleStripeEventJob.queue_name).first.update_columns(locked_at: Time.now)

            HandleStripeEventJob.perform_later_with_delay(user.id, stripe_event.id, Marshal::dump(stripe_event))
            job = Delayed::Job.where("handler ILIKE '\%arguments\%#{user.id}\%#{stripe_event.id}\%'").first
            expect(job.run_at).to be_within(1.second).of(Time.now + 10.seconds) # one job already queued
        end
    end

    describe "perform" do
        it "should work" do
            expect(StripeEventHandler).to receive(:new).with(self.stripe_event)
            instance.perform(user.id, stripe_event.id, Marshal::dump(stripe_event))
        end

        it "should throw retry error if an existing job for the customer exists" do
            another_job = HandleStripeEventJob.perform_later(user.id, SecureRandom.uuid, Marshal::dump(OpenStruct.new(id: 'bar_id')))
            Delayed::Job.where(queue: HandleStripeEventJob.queue_name).first.update_columns(locked_at: Time.now)
            expect{
                instance.perform(user.id, stripe_event.id, Marshal::dump(stripe_event))
            }.to raise_error(Delayed::ForceRetry)
        end
    end
end