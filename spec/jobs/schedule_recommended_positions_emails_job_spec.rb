require 'spec_helper'

describe ScheduleRecommendedPositionsEmailsJob do
    fixtures :users

    before(:each) do
        @instance = ScheduleRecommendedPositionsEmailsJob.new
        @users = [users(:learner), users(:user_with_career_profile), users(:admin)]
        User.where.not(id: @users.pluck(:id)).update_all(can_edit_career_profile: false, notify_candidate_positions_recommended: nil)

        @users.first.update_columns(can_edit_career_profile: true, notify_candidate_positions_recommended: 'daily')
        @users.second.update_columns(can_edit_career_profile: true, notify_candidate_positions_recommended: 'bi_weekly')
        @users.third.update_columns(can_edit_career_profile: false, notify_candidate_positions_recommended: 'daily')
    end

    describe "perform" do
        describe "daily preference" do
            before(:each) do
                allow(Date).to receive(:today).and_return Date.new(2018,12,31) # Monday
            end

            it "should schedule a recommended email job for applicable daily users" do
                expect(SendRecommendedPositionsEmailJob).to receive(:perform_later).once.with(@users.first.id)
                @instance.perform_now
            end

            it "should not schedule if there already exists a recommended email job for the user" do
                expect(SendRecommendedPositionsEmailJob).to receive(:perform_later).once.with(@users.first.id).and_call_original
                @instance.perform_now
                another_instance = ScheduleRecommendedPositionsEmailsJob.new
                another_instance.perform_now
            end
        end

        describe "bi_weekly preference" do
            before(:each) do
                @users.first.update_columns(can_edit_career_profile: false)
            end

            it "should schedule a recommended email job for applicable bi_weekly users on even-week Wednesdays" do
                allow(Date).to receive(:today).and_return Date.new(2019,1,9) # second Wednesday of 2019
                expect(SendRecommendedPositionsEmailJob).to receive(:perform_later).once.with(@users.second.id)
                @instance.perform_now
            end

            it "should not schedule a recommended email job if not Wednesday" do
                allow(Date).to receive(:today).and_return Date.new(2019,1,10) # Thursday
                expect(SendRecommendedPositionsEmailJob).not_to receive(:perform_later)
                @instance.perform_now
            end

            it "should not schedule a recommended email job if not an even week" do
                allow(Date).to receive(:today).and_return Date.new(2019,1,2) # first Wednesday of 2019
                expect(SendRecommendedPositionsEmailJob).not_to receive(:perform_later)
                @instance.perform_now
            end

            it "should not schedule if there already exists a recommended email job for the user" do
                allow(Date).to receive(:today).and_return Date.new(2019,1,9) # second Wednesday of 2019
                expect(SendRecommendedPositionsEmailJob).to receive(:perform_later).once.with(@users.second.id).and_call_original
                @instance.perform_now
                another_instance = ScheduleRecommendedPositionsEmailsJob.new
                another_instance.perform_now
            end
        end
    end

end