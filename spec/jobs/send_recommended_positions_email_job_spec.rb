require 'spec_helper'

describe SendRecommendedPositionsEmailJob do
    fixtures :users

    before(:each) do
        @user = users(:user_with_career_profile)
        @instance = SendRecommendedPositionsEmailJob.new(@user.id)

        hiring_managers = User.joins(:open_positions).group(:id).limit(3)
        professional_organizations = ProfessionalOrganizationOption.limit(3)

        @positions = hiring_managers.map { |hm| hm.open_positions.first }

        @positions.each_with_index do |position, i|
            i_1_based = i + 1
            position.professional_organization = professional_organizations[i]
            position.title = "Title #{i_1_based}"
            position.save!
            position.professional_organization.update_columns(image_url: "foo.com/logo/#{i_1_based}", text: "Foo Inc. #{i_1_based}")
        end
    end

    it "should perform correctly" do
        NotifiedRecommendedOpenPosition.where(user_id: @user.id).delete_all
        NotifiedRecommendedOpenPosition.create!(user_id: @user.id, open_position_id: @positions.first.id)
        NotifiedRecommendedOpenPosition.create!(user_id: @user.id, open_position_id: @positions.second.id)

        expect_any_instance_of(OpenPosition.const_get(:ActiveRecord_Relation))
            .to receive(:recommended_and_viewable)
            .and_return(OpenPosition.where(id: @positions.pluck(:id)))

        expect_any_instance_of(SendRecommendedPositionsEmailJob).to receive(:build_subject).and_return('Foo subject')
        expect(Location).to receive(:location_string).and_return('Bar, ZZ')

        expect(Event).to receive(:create_server_event!).with(anything, @user.id, 'candidate:send_recommended_positions_email', {
            subject: 'Foo subject',
            positions: [{
                title: 'Title 3',
                company_logo_url: 'foo.com/logo/3',
                company_name: 'Foo Inc. 3',
                location_string: 'Bar, ZZ'
            }]
        }).and_return(Event.new)

        expect {
            @instance.perform_now
        }.to change { NotifiedRecommendedOpenPosition.count }.by(1)

        expect(NotifiedRecommendedOpenPosition.where(user_id: @user.id, open_position_id: @positions.third.id).size).to be(1)
    end

    it "should not send customer.io event if there are no new recommended positions" do
        NotifiedRecommendedOpenPosition.where(user_id: @user.id).delete_all
        NotifiedRecommendedOpenPosition.create!(user_id: @user.id, open_position_id: @positions.first.id)
        NotifiedRecommendedOpenPosition.create!(user_id: @user.id, open_position_id: @positions.second.id)
        NotifiedRecommendedOpenPosition.create!(user_id: @user.id, open_position_id: @positions.third.id)

        expect_any_instance_of(OpenPosition.const_get(:ActiveRecord_Relation))
            .to receive(:recommended_and_viewable)
            .and_return(OpenPosition.where(id: @positions.pluck(:id)))

        expect {
            @instance.perform_now
        }.not_to change { NotifiedRecommendedOpenPosition.count }

        expect(Event).not_to receive(:create_server_event!)
    end

    describe "build_subject" do
        it "should just show the one title if only one position" do
            expect(@instance.build_subject([@positions.first])).to eq('Title 1 at Foo Inc. 1')
        end

        it "should show the title with the text 'and more' if two positions" do
            expect(@instance.build_subject(@positions[0..1])).to eq('Title 1 at Foo Inc. 1 and more')
        end

        it "should show the title with a count if more than two positions" do
            expect(@instance.build_subject(@positions)).to eq('Title 1 at Foo Inc. 1 + 2 jobs for you')
        end
    end

end