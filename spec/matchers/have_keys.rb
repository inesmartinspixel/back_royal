RSpec::Matchers.define :have_keys do |*expected|
    match do |actual|
        @actual = actual.keys
        (@actual & expected).sort == expected.sort
    end

    diffable
end