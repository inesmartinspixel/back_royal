RSpec::Matchers.define :have_entries do |expected|
    match do |actual|
        @actual = (actual || {}).slice(*expected.keys)
        @actual == expected
    end

    diffable
end