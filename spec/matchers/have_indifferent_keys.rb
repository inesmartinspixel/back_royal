RSpec::Matchers.define :have_keys do |*expected|
    match do |actual|
        @actual = actual.keys.map(&:to_s)
        expected = expected.map(&:to_s)
        (@actual & expected).sort == expected.sort
    end

    diffable
end