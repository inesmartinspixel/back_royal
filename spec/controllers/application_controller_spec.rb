require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe ApplicationController do

    include ControllerSpecHelper

    describe "update_tracked_user_fields" do
        before(:each) do
            stub_current_user(controller, [:learner])
            @user = controller.current_user
        end

        it "should update when should_update_tracked_user_fields?" do
            allow(controller).to receive(:should_update_tracked_user_fields?).and_return(true)
            expect(@user).to receive(:update_tracked_fields!).and_call_original
            controller.send :update_tracked_user_fields
        end

        it "should not update when !should_update_tracked_user_fields?" do
            allow(controller).to receive(:should_update_tracked_user_fields?).and_return(false)
            expect(@user).not_to receive(:update_tracked_fields!).and_call_original
            controller.send :update_tracked_user_fields
        end

        describe "should_update_tracked_user_fields?" do

            it "should be truthy on registration" do
                @user.current_sign_in_at = nil
                @user.sign_in_count = 0
                @user.save!
                @user.reload
                expect(controller.should_update_tracked_user_fields?).to be(true)
            end

            it "should be truthy when returning after 24 hours time" do
                @user.current_sign_in_at = 25.hours.ago
                @user.sign_in_count = 1
                @user.save!
                @user.reload
                expect(controller.should_update_tracked_user_fields?).to be(true)
            end

            it "should be falsy when returning before 24 hours time" do
                @user.current_sign_in_at = 23.hours.ago
                @user.sign_in_count = 1
                @user.save!
                @user.reload
                expect(controller.should_update_tracked_user_fields?).to be(false)
            end

            it "should be falsy if in ghost_mode" do
                allow(request).to receive(:params).and_return({
                    ghost_mode: "true"
                })
                expect(controller.should_update_tracked_user_fields?).to be(false)
            end

        end

    end

    describe "cordova_client?" do

        before(:each) do
            stub_current_user(controller, [:learner])
            @user = controller.current_user
        end

        it "should be true if not specifying fr-client as web" do
            expect(controller).to receive(:client_params).and_return({"fr-client" => "web"})
            expect(controller.cordova_client?).to be(false)
            expect(controller).to receive(:client_params).and_return({"fr-client": "android"})
            expect(controller.cordova_client?).to be(true)
        end
    end

    describe "user_from_auid_deleted?" do

        it "should return true if there is a corresponding users_deletions record" do
            auid = SecureRandom.uuid
            ActiveRecord::Base.connection.execute(%Q~
                insert into users_deletions (
                    id
                    , created_at
                    , updated_at
                    , user_id
                    , psuedo_anonymized_user_email
                )
                values(
                    '#{SecureRandom.uuid}'
                    , '#{Time.now}'
                    , '#{Time.now}'
                    , '#{auid}'
                    , 'c***y@p*****.com'
                )
            ~)
            expect(controller).to receive(:auid).exactly(2).times.and_return(auid)
            expect(controller.user_from_auid_deleted?).to eq(true)
            expect(controller.instance_variable_get(:@user_from_auid_deleted)).not_to be(nil)
        end

        it "should return false if there is a current_user" do
            stub_current_user(controller, [:learner])
            expect(controller.user_from_auid_deleted?).to eq(false)
        end

        it "should return false if there is no auid" do
            expect(controller).to receive(:auid).and_return(nil)
            expect(controller.user_from_auid_deleted?).to eq(false)
        end

        it "should return false if there is no corresponding users_deletions record" do
            expect(controller).to receive(:auid).exactly(2).times.and_return(SecureRandom.uuid)
            expect(controller.user_from_auid_deleted?).to eq(false)
        end

    end

end