require 'spec_helper'

describe ControllerMixins::StudentNetworkEventsFiltersMixin do

    attr_reader :instance, :query, :cupertino_past_meetup, :cupertino_old_conference, :cupertino_recent_conference,
        :cupertino_future_meetup, :cupertino_present_meetup, :cupertino_indeterminate_meetup, :toronto_future_book_club, :all_events, :all_event_ids

    fixtures(:student_network_events)

    class FilterableForStudentNetworkEventsFiltersMixinSpec
        include ControllerMixins::StudentNetworkEventsFiltersMixin
    end

    before(:each) do
        @instance = FilterableForStudentNetworkEventsFiltersMixinSpec.new
        @cupertino_past_meetup = student_network_events(:cupertino_past_meetup)
        @cupertino_old_conference = student_network_events(:cupertino_old_conference)
        @cupertino_recent_conference = student_network_events(:cupertino_recent_conference)
        @cupertino_future_meetup = student_network_events(:cupertino_future_meetup)
        @cupertino_present_meetup = student_network_events(:cupertino_present_meetup)
        @cupertino_indeterminate_meetup = student_network_events(:cupertino_indeterminate_meetup)
        @toronto_future_book_club = student_network_events(:toronto_future_book_club)
        @all_events = [cupertino_past_meetup, cupertino_old_conference, cupertino_recent_conference, cupertino_future_meetup, cupertino_present_meetup, cupertino_indeterminate_meetup, toronto_future_book_club]
        @all_event_ids = all_events.pluck(:id)

        allow(instance).to receive(:current_user).and_return(User.first)
    end

    describe "filter_student_network_events_by_user_access" do

        before(:each) do
            reset_query
        end

        it "should return all events for admins and interviewers" do
            expect(instance.current_user).to receive(:is_admin_or_interviewer?).and_return(true)
            actual_ids = instance.filter_student_network_events_by_user_access(@query).pluck(:id)
            expect(actual_ids).to match_array(all_event_ids)
        end

        describe "when current_user.has_full_student_network_events_access?" do

            before(:each) do
                expect(instance.current_user).to receive(:is_admin_or_interviewer?).at_least(:once).and_return(false)
                expect(instance.current_user).to receive(:has_full_student_network_events_access?).at_least(:once).and_return(true)
                expect(instance.current_user).to receive(:student_network_inclusion).at_least(:once).and_return(OpenStruct.new({}))

                # For sanity and to prevent false-positives
                all_events.each do |event|
                    event.update_column(:visible_to_current_degree_students, false)
                    event.update_column(:visible_to_graduated_degree_students, false)
                    event.update_column(:visible_to_non_degree_users, false)
                    event.update_column(:visible_to_accepted_degree_students_in_cohorts, [])
                end
            end

            it "should filter based on visible_to_current_degree_students" do
                cupertino_past_meetup.update_column(:visible_to_current_degree_students, true)
                expect(instance.current_user.student_network_inclusion).to receive(:provides_access_to_student_network_events_visible_to_current_degree_students?).and_return(true)
                actual_ids = instance.filter_student_network_events_by_user_access(@query).pluck(:id)
                expect(actual_ids).to match_array([cupertino_past_meetup.id])
            end

            it "should filter based on visible_to_graduated_degree_students" do
                cupertino_past_meetup.update_column(:visible_to_graduated_degree_students, true)
                expect(instance.current_user.student_network_inclusion).to receive(:provides_access_to_student_network_events_visible_to_graduated_degree_students?).and_return(true)
                actual_ids = instance.filter_student_network_events_by_user_access(@query).pluck(:id)
                expect(actual_ids).to match_array([cupertino_past_meetup.id])
            end

            it "should filter based on visible_to_non_degree_users" do
                cupertino_past_meetup.update_column(:visible_to_non_degree_users, true)
                expect(instance.current_user.student_network_inclusion).to receive(:provides_access_to_student_network_events_visible_to_non_degree_users?).and_return(true)
                actual_ids = instance.filter_student_network_events_by_user_access(@query).pluck(:id)
                expect(actual_ids).to match_array([cupertino_past_meetup.id])
            end

            it "should filter based on visible_to_accepted_degree_students_in_cohorts" do
                cohort_id = SecureRandom.uuid
                cupertino_past_meetup.update_column(:visible_to_accepted_degree_students_in_cohorts, [cohort_id])
                expect(instance.current_user.student_network_inclusion).to receive(:provides_access_to_student_network_events_visible_to_accepted_degree_students_in_cohorts?).and_return(true)
                expect(instance.current_user.student_network_inclusion).to receive(:cohort_id).and_return(cohort_id)
                actual_ids = instance.filter_student_network_events_by_user_access(@query).pluck(:id)
                expect(actual_ids).to match_array([cupertino_past_meetup.id])
            end

            describe "when student_network_inclusion.provides_access_to_old_conference_events?" do

                before(:each) do
                    expect(instance.current_user.student_network_inclusion).to receive(:provides_access_to_old_conference_events?).at_least(:once).and_return(true)
                end

                it "should include conference events that ended more than a week before now (even if they weren't visible to the user before)" do
                    actual_ids = instance.filter_student_network_events_by_user_access(@query).pluck(:id)
                    expect(actual_ids).to match_array([cupertino_old_conference.id])
                end
            end

            describe "multiple visibility levels" do

                it "should filter based on multiple visible_to_* boolean columns" do
                    cupertino_past_meetup.update_column(:visible_to_current_degree_students, true)
                    toronto_future_book_club.update_column(:visible_to_non_degree_users, true)
                    expect(instance.current_user.student_network_inclusion).to receive(:provides_access_to_student_network_events_visible_to_current_degree_students?).and_return(true)
                    expect(instance.current_user.student_network_inclusion).to receive(:provides_access_to_student_network_events_visible_to_non_degree_users?).and_return(true)
                    actual_ids = instance.filter_student_network_events_by_user_access(@query).pluck(:id)
                    expect(actual_ids).to match_array([cupertino_past_meetup.id, toronto_future_book_club.id])
                end

                it "should filter based on visible_to_* boolean columns and visible_to_accepted_degree_students_in_cohorts " do
                    cohort_id = SecureRandom.uuid
                    cupertino_past_meetup.update_column(:visible_to_current_degree_students, true)
                    cupertino_future_meetup.update_column(:visible_to_accepted_degree_students_in_cohorts, [cohort_id])
                    expect(instance.current_user.student_network_inclusion).to receive(:provides_access_to_student_network_events_visible_to_current_degree_students?).and_return(true)
                    expect(instance.current_user.student_network_inclusion).to receive(:provides_access_to_student_network_events_visible_to_accepted_degree_students_in_cohorts?).and_return(true)
                    expect(instance.current_user.student_network_inclusion).to receive(:cohort_id).and_return(cohort_id)
                    actual_ids = instance.filter_student_network_events_by_user_access(@query).pluck(:id)
                    expect(actual_ids).to match_array([cupertino_past_meetup.id, cupertino_future_meetup.id])
                end

                it "should filter out events if student_network_inclusions record does not provide access to visibility level" do
                    cupertino_past_meetup.update_column(:visible_to_current_degree_students, true)
                    cupertino_future_meetup.update_column(:visible_to_graduated_degree_students, true)
                    expect(instance.current_user.student_network_inclusion).to receive(:provides_access_to_student_network_events_visible_to_current_degree_students?).and_return(true)
                    expect(instance.current_user.student_network_inclusion).to receive(:provides_access_to_student_network_events_visible_to_graduated_degree_students?).and_return(false)
                    actual_ids = instance.filter_student_network_events_by_user_access(@query).pluck(:id)
                    expect(actual_ids).to match_array([cupertino_past_meetup.id])
                end

                it "should include old conference events when student_network_inclusion.provides_access_to_old_conference_events? (even if they weren't visible to the user before)" do
                    cohort_id = SecureRandom.uuid
                    cupertino_past_meetup.update_column(:visible_to_current_degree_students, true)
                    cupertino_future_meetup.update_column(:visible_to_accepted_degree_students_in_cohorts, [cohort_id])
                    allow(instance.current_user.student_network_inclusion).to receive(:provides_access_to_student_network_events_visible_to_current_degree_students?).and_return(true)
                    allow(instance.current_user.student_network_inclusion).to receive(:provides_access_to_student_network_events_visible_to_accepted_degree_students_in_cohorts?).and_return(true)
                    allow(instance.current_user.student_network_inclusion).to receive(:cohort_id).and_return(cohort_id)
                    allow(instance.current_user.student_network_inclusion).to receive(:provides_access_to_old_conference_events?).and_return(true)
                    actual_ids = instance.filter_student_network_events_by_user_access(@query).pluck(:id)
                    expect(actual_ids).to match_array([cupertino_old_conference.id, cupertino_past_meetup.id, cupertino_future_meetup.id])
                end
            end
        end

        describe "when limited access" do

            before(:each) do
                expect(instance.current_user).to receive(:is_admin_or_interviewer?).at_least(:once).and_return(false)
                expect(instance.current_user).to receive(:has_full_student_network_events_access?).at_least(:once).and_return(false)

                # For sanity and to prevent false-positives
                all_events.each do |event|
                    event.update_column(:visible_to_current_degree_students, false)
                    event.update_column(:visible_to_graduated_degree_students, false)
                    event.update_column(:visible_to_non_degree_users, false)
                    event.update_column(:visible_to_accepted_degree_students_in_cohorts, [])
                end
            end

            def assert_event_id_is_contained_in_actual_ids(event_id, actual_ids)
                # The cupertino_old_conference is included a lot of time because we
                # make sure that old conference events are made visible to the user.
                # See https://trello.com/c/ueGVLnTF.
                expect(actual_ids).to match_array([cupertino_old_conference.id].push(event_id))
            end

            it "should filter based on visible_to_current_degree_students" do
                cupertino_past_meetup.update_column(:visible_to_current_degree_students, true)
                actual_ids = instance.filter_student_network_events_by_user_access(@query).pluck(:id)
                assert_event_id_is_contained_in_actual_ids(cupertino_past_meetup.id, actual_ids)
            end

            it "should filter based on visible_to_non_degree_users" do
                cupertino_past_meetup.update_column(:visible_to_non_degree_users, true)
                actual_ids = instance.filter_student_network_events_by_user_access(@query).pluck(:id)
                assert_event_id_is_contained_in_actual_ids(cupertino_past_meetup.id, actual_ids)
            end

            it "should filter based on visible_to_accepted_degree_students_in_cohorts using the currently promoted EMBA cohort" do
                cohort_id = SecureRandom.uuid
                expect(Cohort).to receive(:promoted_cohort).with('emba').and_return(OpenStruct.new({id: cohort_id}))
                cupertino_past_meetup.update_column(:visible_to_accepted_degree_students_in_cohorts, [cohort_id])
                actual_ids = instance.filter_student_network_events_by_user_access(@query).pluck(:id)
                assert_event_id_is_contained_in_actual_ids(cupertino_past_meetup.id, actual_ids)
            end

            it "should include conference events that ended more than a week before now (even if they weren't visible to the user before)" do
                mock_now = cupertino_recent_conference.end_time + 1.week
                allow(Time).to receive(:now).and_return(mock_now)
                actual_ids = instance.filter_student_network_events_by_user_access(@query).pluck(:id)
                expect(actual_ids).to match_array([cupertino_old_conference.id])
            end
        end
    end

    describe "filter_student_network_events_by_published" do
        attr_accessor :published_event_ids, :non_published_event

        before(:each) do
            reset_query
            @published_event_ids = all_events.reject { |event| event.id === cupertino_future_meetup.id }.pluck(:id)

            # ensure we have some non-published student_network_events
            @non_published_event = cupertino_future_meetup
            non_published_event.update_column(:published, false)
        end

        it "should filter out non-published events if editor filter not present" do
            actual_ids = instance.filter_student_network_events_by_published({}, @query).pluck(:id)
            expect(actual_ids).to match_array(published_event_ids)
            expect(actual_ids).not_to include(non_published_event.id)
        end

        it "should filter out non-published events if user is not an admin" do
            expect(instance.current_user).to receive(:is_admin?).and_return(false)
            actual_ids = instance.filter_student_network_events_by_published({:editor => true}, @query).pluck(:id)
            expect(actual_ids).to match_array(published_event_ids)
            expect(actual_ids).not_to include(non_published_event.id)
        end

        it "should not filter events by published column if editor filter is present and user is an admin" do
            allow(instance).to receive(:current_user).and_return(User.first)
            expect(instance.current_user).to receive(:is_admin?).and_return(true)
            actual_ids = instance.filter_student_network_events_by_published({:editor => true}, @query).pluck(:id)
            expect(actual_ids).to match_array(published_event_ids.push(non_published_event.id))
        end

    end

    describe "filter_student_network_events_by_date" do

        before(:each) do
            reset_query
        end

        it "should filter for events with a start_time greater than or equal to the filter" do
            actual_ids = instance.filter_student_network_events_by_date({:start_time => Time.now}, @query).pluck(:id)
            expect(actual_ids).not_to include(cupertino_past_meetup.id)
            expect(actual_ids).to include(cupertino_future_meetup.id)
            expect(actual_ids).not_to include(cupertino_indeterminate_meetup.id)
            expect(actual_ids).to include(toronto_future_book_club.id)
        end

        it "should filter for events with an end_time less than or equal to the filter" do
            actual_ids = instance.filter_student_network_events_by_date({:end_time => Time.now + 2.months}, @query).pluck(:id)
            expect(actual_ids).to include(cupertino_past_meetup.id)
            expect(actual_ids).to include(cupertino_future_meetup.id)
            expect(actual_ids).not_to include(cupertino_indeterminate_meetup.id)
            expect(actual_ids).not_to include(toronto_future_book_club.id)
        end

        it "should filter for events starting after now or events that are currently happening if no start_time nor end_time explicitly given" do
            actual_ids = instance.filter_student_network_events_by_date({}, @query).pluck(:id)
            expect(actual_ids).not_to include(cupertino_past_meetup.id)
            expect(actual_ids).to include(cupertino_future_meetup.id)
            expect(actual_ids).to include(cupertino_present_meetup.id)
            expect(actual_ids).not_to include(cupertino_indeterminate_meetup.id)
        end

        it "should include indeterminate date events based on the include_tbd flag" do
            actual_ids = instance.filter_student_network_events_by_date({:include_tbd => true}, @query).pluck(:id)
            expect(actual_ids).not_to include(cupertino_past_meetup.id)
            expect(actual_ids).to include(cupertino_future_meetup.id)
            expect(actual_ids).to include(cupertino_present_meetup.id)
            expect(actual_ids).to include(cupertino_indeterminate_meetup.id)
        end

        it "should only return indeterminate date events if providing the only_tbd flag" do
            actual_ids = instance.filter_student_network_events_by_date({:only_tbd => true}, @query).pluck(:id)
            expect(actual_ids).not_to include(cupertino_past_meetup.id)
            expect(actual_ids).not_to include(cupertino_future_meetup.id)
            expect(actual_ids).not_to include(cupertino_present_meetup.id)
            expect(actual_ids).to include(cupertino_indeterminate_meetup.id)
        end


    end

    describe "filter_student_network_events_by_keywords" do

        before(:each) do
            reset_query
        end

        it "should use anonymous_keyword_vector column for filtering when !user.has_full_student_network_events_access?" do
            expect(instance.current_user).to receive(:has_full_student_network_events_access?).and_return(false)
            expect(query).to receive(:where).with("student_network_events.id IN (SELECT student_network_event_id FROM student_network_event_fulltext WHERE anonymous_keyword_vector @@ to_tsquery('english', 'foobar'))")
            instance.filter_student_network_events_by_keywords({:keyword_search => 'foobar'}, @query)
        end

        describe "when user.has_full_student_network_events_access?" do

            before(:each) do
                expect(instance.current_user).to receive(:has_full_student_network_events_access?).and_return(true)
            end

            it "should use full_keyword_vector column for filtering" do
                expect(query).to receive(:where).with("student_network_events.id IN (SELECT student_network_event_id FROM student_network_event_fulltext WHERE full_keyword_vector @@ to_tsquery('english', 'foobar'))")
                instance.filter_student_network_events_by_keywords({:keyword_search => 'foobar'}, @query)
            end

            it "should filter for events by full_keyword_vector" do
                expect(instance.current_user).to receive(:has_full_student_network_events_access?).at_least(1).times.and_return(true)

                actual_ids = instance.filter_student_network_events_by_keywords({:keyword_search => 'super cool'}, @query).pluck(:id)
                expect(actual_ids).to include(cupertino_past_meetup.id)
                expect(actual_ids).to include(cupertino_future_meetup.id)
                expect(actual_ids).not_to include(toronto_future_book_club.id)

                reset_query
                actual_ids = instance.filter_student_network_events_by_keywords({:keyword_search => 'toronto'}, @query).pluck(:id)
                expect(actual_ids).not_to include(cupertino_past_meetup.id)
                expect(actual_ids).not_to include(cupertino_future_meetup.id)
                expect(actual_ids).to include(toronto_future_book_club.id)
            end

            describe "program_types visible_to" do
                attr_accessor :event

                before(:each) do
                    @event = StudentNetworkEvent.create!({
                        id: SecureRandom.uuid,
                        title: 'EMBA19 Q&A Chat',
                        description: 'An event that has an ampersand in it',
                        event_type: 'book_club',
                        start_time: Time.now + 3.months,
                        end_time: Time.now + 3.months,
                        author_id: User.first.id,
                        published: true,
                        published_at: Time.now
                    })
                end

                it "should be fine when there are no program types" do
                    event.update!(visible_to_accepted_degree_students_in_cohorts: [])
                    ids = instance.filter_student_network_events_by_keywords({:keyword_search => 'emba19 chat'}, @query).pluck(:id)
                    expect(ids).to include(event.id)
                end

                it "should find degree program_types visible_to" do
                    allow(event).to receive(:degree_program_types_visible_to).and_return(['foomba', 'barmba'])
                    event.update_fulltext
                    ids = instance.filter_student_network_events_by_keywords({:keyword_search => 'foomba'}, @query).pluck(:id)
                    expect(ids).to include(event.id)
                end
            end

        end

    end

    describe "filter_student_network_events_by_location" do

        attr_accessor :toronto, :mountain_view

        before(:each) do
            reset_query

            toronto = Location.new("43.6532° N, 79.3832° W")
            @toronto = {
                lat: toronto.lat,
                lng: toronto.lng
            }

            mountain_view = Location.new("37.3861° N, 122.0839° W")
            @mountain_view = {
                lat: mountain_view.lat,
                lng: mountain_view.lng
            }
        end

        it "should work when the places filter is just a single place (i.e. not a list)" do
            expect(instance.current_user).to receive(:has_full_student_network_events_access?).and_return(false)
            expect(Location).to receive(:at_location_sql_string).with(@toronto, 'location_anonymized', anything)
            instance.filter_student_network_events_by_location({:places => @toronto}, @query)
        end

        it "should use location_anonymized column for filtering when !user.has_full_student_network_events_access?" do
            expect(instance.current_user).to receive(:has_full_student_network_events_access?).and_return(false)
            expect(Location).to receive(:at_location_sql_string).with(anything, 'location_anonymized', anything)
            instance.filter_student_network_events_by_location({:places => [@toronto]}, @query)
        end

        describe "when user.has_full_student_network_events_access?" do

            before(:each) do
                expect(instance.current_user).to receive(:has_full_student_network_events_access?).and_return(true)
            end

            it "should use location column for filtering" do
                expect(Location).to receive(:at_location_sql_string).with(anything, 'location', anything)
                instance.filter_student_network_events_by_location({:places => [@toronto]}, @query)
            end

            it "should filter for events by location" do
                actual_ids = instance.filter_student_network_events_by_location({:places => [@toronto]}, @query).pluck(:id)
                expect(actual_ids).not_to include(cupertino_past_meetup.id)
                expect(actual_ids).not_to include(cupertino_future_meetup.id)
                expect(actual_ids).to include(toronto_future_book_club.id)
            end

            it "should return nearby events" do
                actual_ids = instance.filter_student_network_events_by_location({:places => [@mountain_view]}, @query).pluck(:id)
                expect(actual_ids).to include(cupertino_past_meetup.id)
                expect(actual_ids).to include(cupertino_future_meetup.id)
                expect(actual_ids).not_to include(toronto_future_book_club.id)
            end

            it "should return events in multiple locales" do
                actual_ids = instance.filter_student_network_events_by_location({:places => [@mountain_view, @toronto]}, @query).pluck(:id)
                expect(actual_ids).to match_array(all_event_ids)
            end

        end

    end

    describe "filter_student_network_events_by_event_type" do

        before(:each) do
            reset_query
        end

        it "should filter for events by event_type" do
            actual_ids = instance.filter_student_network_events_by_event_type({:event_type => ['meetup']}, @query).pluck(:id)
            expect(actual_ids).to include(cupertino_past_meetup.id)
            expect(actual_ids).to include(cupertino_future_meetup.id)
            expect(actual_ids).not_to include(toronto_future_book_club.id)

            reset_query
            actual_ids = instance.filter_student_network_events_by_event_type({:event_type => ['book_club']}, @query).pluck(:id)
            expect(actual_ids).not_to include(cupertino_past_meetup.id)
            expect(actual_ids).not_to include(cupertino_future_meetup.id)
            expect(actual_ids).to include(toronto_future_book_club.id)
        end

        it "should return events for multiple event_types" do
            event_types = ['book_club', 'meetup']
            actual_ids = instance.filter_student_network_events_by_event_type({:event_type => event_types}, @query).pluck(:id)
            expected_ids = all_events.select { |event| event_types.include?(event.event_type) }.pluck(:id)
            expect(actual_ids).to match_array(expected_ids)
        end
    end

    describe "filter_student_network_events_by_id" do


        before(:each) do
            reset_query
        end

        it "should filter for events by id" do
            event = StudentNetworkEvent.first
            id = StudentNetworkEvent.first.id
            actual_ids = instance.filter_student_network_events_by_id({:id => event}, @query).pluck(:id)
            expect(actual_ids).to eq([id])
        end

    end

    def reset_query
        @query = StudentNetworkEvent
    end

end