require 'spec_helper'

describe ControllerMixins::HiringManagerFiltersMixin do
    attr_reader :instance

    class FilterableForHiringManagerFiltersMixinSpec
        include ControllerMixins::HiringManagerFiltersMixin
    end

    before(:each) do
        @instance = FilterableForHiringManagerFiltersMixinSpec.new
    end

    describe "keyword_search" do

        it "should not blowup on invalid tsvector searches" do
            expect{
                instance.process_hiring_manager_filters({:keyword_search => '%%P\left('})
            }.not_to raise_error()
        end

        # Coverage for https://trello.com/c/0J2PMdoB
        it "should work if keyword_search filter contains a dot (.)" do
            career_profile = CareerProfile.first
            career_profile.update_attribute(:personal_fact, 'Node.js')
            expect_any_instance_of(CareerProfile).to receive(:career_profile_active?) { true }
            RebuildCareerProfileFulltextJob.perform_now(career_profile.id) # ensure that the career profile has its fulltext updated in the database
            actual_user_ids = instance.process_hiring_manager_filters({:keyword_search => 'Node.js'}).pluck(:user_id)
            expect(actual_user_ids).to include(career_profile.user_id)
        end

        # Coverage for https://trello.com/c/eKqDI6NO
        it "should work with other filters" do
            expect{
                instance.process_hiring_manager_filters({:keyword_search => 'test', :industries => ['b', 'd']})
            }.not_to raise_error()
        end
    end

    describe "skills" do

        before(:each) do
            add_skills_to_profiles
        end

        it "should find profiles that match single skills" do
            actual_ids = instance.process_hiring_manager_filters({:skills => ['Ruby']}).pluck(:id)
            expect(actual_ids).to include(@ruby_profile.id)
            expect(actual_ids).not_to include(@javascript_profile.id)
            expect(actual_ids).not_to include(@custom_profile.id)

            actual_ids = instance.process_hiring_manager_filters({:skills => ['JavaScript']}).pluck(:id)
            expect(actual_ids).not_to include(@ruby_profile.id)
            expect(actual_ids).to include(@javascript_profile.id)
            expect(actual_ids).not_to include(@custom_profile.id)
        end

        it "should find profiles that match multiple skills" do
            actual_ids = instance.process_hiring_manager_filters({:skills => ['JavaScript', 'Ruby']}).pluck(:id)
            expect(actual_ids).to include(@ruby_profile.id)
            expect(actual_ids).to include(@javascript_profile.id)
            expect(actual_ids).not_to include(@custom_profile.id)

            actual_ids = instance.process_hiring_manager_filters({:skills => ['JavaScript', 'Custom Skill']}).pluck(:id)
            expect(actual_ids).not_to include(@ruby_profile.id)
            expect(actual_ids).to include(@javascript_profile.id)
            expect(actual_ids).to include(@custom_profile.id)

            actual_ids = instance.process_hiring_manager_filters({:skills => ['JavaScript', 'Custom Skill', 'Ruby']}).pluck(:id)
            expect(actual_ids).to include(@ruby_profile.id)
            expect(actual_ids).to include(@javascript_profile.id)
            expect(actual_ids).to include(@custom_profile.id)
        end

        it "should filter out profiles that do not match skills" do
            actual_ids = instance.process_hiring_manager_filters({:skills => ['AngularJS']}).pluck(:id)
            expect(actual_ids.any?).to be(false)
        end

        it "should provide tsvector searches for case inclusive case-insensitive fulltext" do
            actual_ids = instance.process_hiring_manager_filters({:skills => ['javascript', 'custom skill', 'ruby']}).pluck(:id)
            expect(actual_ids).to include(@ruby_profile.id)
            expect(actual_ids).to include(@javascript_profile.id)
            expect(actual_ids).to include(@custom_profile.id)
        end

        it "should not blowup on invalid tsvector searches" do
            expect{
                instance.process_hiring_manager_filters({:skills => ['%%P\left(']})
            }.not_to raise_error()
        end

    end

    describe "roles filtering" do

        it "should find profiles that match primary_areas_of_interest or work experience roles" do
            career_profiles = CareerProfile.select('distinct on (career_profiles.id) career_profiles.*').joins(:work_experiences).limit(8)
            career_profiles[0].update_attribute(:primary_areas_of_interest, ['a', 'b'])
            career_profiles[1].update_attribute(:primary_areas_of_interest, ['b', 'c'])
            career_profiles[2].update_attribute(:primary_areas_of_interest, [''])
            career_profiles[3].update_attribute(:primary_areas_of_interest, ['d'])
            career_profiles[4].update_attribute(:primary_areas_of_interest, ['e'])

            career_profiles[5].work_experiences[0].update_attribute(:role, 'b')
            career_profiles[6].work_experiences[0].update_attribute(:role, 'd')
            career_profiles[7].work_experiences[0].update_attribute(:role, 'a')

            update_career_profile_search_helpers(career_profiles)

            actual_ids = instance.process_hiring_manager_filters({
                :roles => {
                    :primary_areas_of_interest => ['b', 'd'],
                    :work_experience_roles => ['b', 'd']
                }
            }).pluck(:id)

            expect(actual_ids).to include(career_profiles[0].id)
            expect(actual_ids).to include(career_profiles[1].id)
            expect(actual_ids).not_to include(career_profiles[2].id)
            expect(actual_ids).to include(career_profiles[3].id)
            expect(actual_ids).not_to include(career_profiles[4].id)

            expect(actual_ids).to include(career_profiles[5].id)
            expect(actual_ids).to include(career_profiles[6].id)
            expect(actual_ids).not_to include(career_profiles[7].id)
        end
    end

    describe "industry" do
        it "should find profiles that match job_sectors_of_interest" do
            career_profiles = CareerProfile.limit(5)
            career_profiles[0].update_attribute(:job_sectors_of_interest, ['a', 'b'])
            career_profiles[1].update_attribute(:job_sectors_of_interest, ['b', 'c'])
            career_profiles[2].update_attribute(:job_sectors_of_interest, [''])
            career_profiles[3].update_attribute(:job_sectors_of_interest, ['d'])
            career_profiles[4].update_attribute(:job_sectors_of_interest, ['e'])
            update_career_profile_search_helpers

            actual_ids = instance.process_hiring_manager_filters({:industries => ['b', 'd']}).pluck(:id)
            expect(actual_ids).to include(career_profiles[0].id)
            expect(actual_ids).to include(career_profiles[1].id)
            expect(actual_ids).not_to include(career_profiles[2].id)
            expect(actual_ids).to include(career_profiles[3].id)
            expect(actual_ids).not_to include(career_profiles[4].id)
        end

        it "should find profiles that match industries in work experiences" do
            career_profiles = CareerProfile.select('distinct on (career_profiles.id) career_profiles.*').joins(:work_experiences).limit(3)
            career_profiles[0].work_experiences[0].update_attribute(:industry, 'a')
            career_profiles[1].work_experiences[0].update_attribute(:industry, 'b')
            career_profiles[2].work_experiences[0].update_attribute(:industry, 'c')
            update_career_profile_search_helpers(career_profiles)

            actual_ids = instance.process_hiring_manager_filters({:industries => ['a', 'b']}).pluck(:id)
            expect(actual_ids).to include(career_profiles[0].id)
            expect(actual_ids).to include(career_profiles[1].id)
            expect(actual_ids).not_to include(career_profiles[2].id)
        end
    end

    describe "years_experience" do

        before(:each) do

            career_profile_ids = CareerProfile::WorkExperience.distinct.reorder(nil).limit(8).pluck('career_profile_id')
            @career_profiles = CareerProfile.find(career_profile_ids)
            expect(@career_profiles.size).to eq(8)

            @without_current_experience = {}
            [6, 18, 24, 7*12].each_with_index do |months_experience, i|
                career_profile = @career_profiles[i]
                exp = career_profile.work_experiences[0]
                exp.start_date = Time.parse('2000/06/01').to_date
                exp.end_date = (exp.start_date + months_experience.months).to_date
                exp.save!
                (career_profile.work_experiences.to_a - [exp]).each(&:destroy)
                career_profile.education_experiences.destroy_all
                @without_current_experience[months_experience] = career_profile
            end

            @with_current_experience = {}
            [-72, 6, 18, 7*12].each_with_index do |months_experience, i|
                career_profile = @career_profiles[i+@without_current_experience.size]
                exp = career_profile.work_experiences[0]
                exp.start_date = Time.now - months_experience.months
                exp.end_date = nil
                exp.save!
                (career_profile.work_experiences.to_a - [exp]).each(&:destroy)
                career_profile.education_experiences.destroy_all
                @with_current_experience[months_experience] = career_profile
            end

            update_career_profile_search_helpers(@career_profiles)
        end

        it "should work with n+ years" do
            actual_ids = instance.process_hiring_manager_filters({:years_experience => ['6_plus_years']}).pluck(:id)
            expect(actual_ids).to include(@without_current_experience[7*12].id)
            expect(actual_ids).not_to include(@without_current_experience[6].id)
            expect(actual_ids).not_to include(@without_current_experience[18].id)
            expect(actual_ids).not_to include(@without_current_experience[24].id)


            expect(actual_ids).to include(@with_current_experience[7*12].id)
            expect(actual_ids).not_to include(@with_current_experience[6].id)
            expect(actual_ids).not_to include(@with_current_experience[18].id)
            expect(actual_ids).not_to include(@with_current_experience[-72].id)
        end

        it "should work with ranges" do
            actual_ids = instance.process_hiring_manager_filters({:years_experience => ['0_1_years', '2_4_years']}).pluck(:id)
            expect(actual_ids).not_to include(@without_current_experience[7*12].id)
            expect(actual_ids).to include(@without_current_experience[6].id)
            expect(actual_ids).not_to include(@without_current_experience[18].id)
            expect(actual_ids).to include(@without_current_experience[24].id)

            expect(actual_ids).not_to include(@with_current_experience[7*12].id)
            expect(actual_ids).to include(@with_current_experience[6].id)
            expect(actual_ids).not_to include(@with_current_experience[18].id)

            # someone who has one experience starting in the future should be included when filtering by 0_1
            expect(actual_ids).to include(@with_current_experience[-72].id)
        end
    end

    describe "company_name" do
        it "should work" do
            work_experience = CareerProfile::WorkExperience.first
            career_profile = work_experience.career_profile

            actual_ids = instance.process_hiring_manager_filters({:company_name => 'nobody ever worked here'}).pluck(:id)
            expect(actual_ids).to be_empty

            actual_ids = instance.process_hiring_manager_filters({:company_name => work_experience.professional_organization.text.slice(0, 3)}).pluck(:id)
            expect(actual_ids).to include(career_profile.id)
        end
    end

    describe "school_name" do
        it "should work" do
            education_experience = CareerProfile::EducationExperience.first
            career_profile = education_experience.career_profile

            actual_ids = instance.process_hiring_manager_filters({:school_name => 'nobody ever went here'}).pluck(:id)
            expect(actual_ids).to be_empty

            actual_ids = instance.process_hiring_manager_filters({:school_name => education_experience.educational_organization.text.slice(0, 3)}).pluck(:id)
            expect(actual_ids).to include(career_profile.id)
        end
    end

    describe "employment_types_of_interest" do
        it "should work" do
            career_profiles = CareerProfile.limit(5)
            career_profiles[0].update_attribute(:employment_types_of_interest, ['a', 'b'])
            career_profiles[1].update_attribute(:employment_types_of_interest, ['b', 'c'])
            career_profiles[2].update_attribute(:employment_types_of_interest, [''])
            career_profiles[3].update_attribute(:employment_types_of_interest, ['d'])
            career_profiles[4].update_attribute(:employment_types_of_interest, ['e'])

            actual_ids = instance.process_hiring_manager_filters({:employment_types_of_interest => ['b', 'd']}).pluck(:id)
            expect(actual_ids).to include(career_profiles[0].id)
            expect(actual_ids).to include(career_profiles[1].id)
            expect(actual_ids).not_to include(career_profiles[2].id)
            expect(actual_ids).to include(career_profiles[3].id)
            expect(actual_ids).not_to include(career_profiles[4].id)
        end
    end

    describe "in_school" do

        it "should work for a user with no education experiences" do
            career_profile = CareerProfile.left_outer_joins(:education_experiences).joins(:career_profile_search_helper).where("education_experiences.id is null").first

            actual_ids = instance.process_hiring_manager_filters({:in_school => true}).pluck(:id)
            expect(actual_ids).not_to include(career_profile.id)

            actual_ids = instance.process_hiring_manager_filters({:in_school => false}).pluck(:id)
            expect(actual_ids).to include(career_profile.id)

            actual_ids = instance.process_hiring_manager_filters({:in_school => nil}).pluck(:id)
            expect(actual_ids).to include(career_profile.id)
        end

        it "should work for a user with only education in the past" do
            career_profile = CareerProfile.joins(:education_experiences).first
            career_profile.education_experiences.each do |e|
                e.update({
                    graduation_year: (Time.now - 3.years).year,
                    degree_program: true
                })
            end
            update_career_profile_search_helpers([career_profile])

            actual_ids = instance.process_hiring_manager_filters({:in_school => true}).pluck(:id)
            expect(actual_ids).not_to include(career_profile.id)

            actual_ids = instance.process_hiring_manager_filters({:in_school => false}).pluck(:id)
            expect(actual_ids).to include(career_profile.id)

            actual_ids = instance.process_hiring_manager_filters({:in_school => nil}).pluck(:id)
            expect(actual_ids).to include(career_profile.id)

        end

        it "should work for a user with only education in the future" do
            career_profile = CareerProfile.joins(:education_experiences).first
            career_profile.education_experiences.each do |e|
                e.update({
                    graduation_year: (Time.now + 1.years).year,
                    degree_program: true
                })
            end
            update_career_profile_search_helpers([career_profile])

            actual_ids = instance.process_hiring_manager_filters({:in_school => true}).pluck(:id)
            expect(actual_ids).to include(career_profile.id)

            actual_ids = instance.process_hiring_manager_filters({:in_school => false}).pluck(:id)
            expect(actual_ids).not_to include(career_profile.id)

            actual_ids = instance.process_hiring_manager_filters({:in_school => nil}).pluck(:id)
            expect(actual_ids).to include(career_profile.id)

        end


        it "should work for a user with education in the past and future" do
            educational_organization = CareerProfile::EducationalOrganizationOption.first
            career_profile = CareerProfile.left_outer_joins(:education_experiences).joins(:career_profile_search_helper).where("education_experiences.id is null").first
            career_profile.education_experiences.build(
                graduation_year: (Time.now - 2.years).year,
                degree: 'a',
                major: 'a',
                educational_organization_option_id: educational_organization.id,
                will_not_complete: false,
                degree_program: true
            )
            career_profile.education_experiences.build(
                graduation_year: (Time.now + 2.years).year,
                degree: 'a',
                major: 'a',
                educational_organization_option_id: educational_organization.id,
                will_not_complete: false,
                degree_program: true
            )
            career_profile.save!
            update_career_profile_search_helpers([career_profile])

            actual_ids = instance.process_hiring_manager_filters({:in_school => true}).pluck(:id)
            expect(actual_ids).to include(career_profile.id)

            actual_ids = instance.process_hiring_manager_filters({:in_school => false}).pluck(:id)
            expect(actual_ids).not_to include(career_profile.id)

            actual_ids = instance.process_hiring_manager_filters({:in_school => nil}).pluck(:id)
            expect(actual_ids).to include(career_profile.id)

        end

        it "should ignore a non-degree program" do
            career_profile = CareerProfile.joins(:education_experiences).first
            career_profile.education_experiences.each do |e|
                e.update({
                    graduation_year: (Time.now + 1.years).year,
                    degree_program: false
                })
            end
            update_career_profile_search_helpers([career_profile])

            actual_ids = instance.process_hiring_manager_filters({:in_school => true}).pluck(:id)
            expect(actual_ids).not_to include(career_profile.id)

            actual_ids = instance.process_hiring_manager_filters({:in_school => false}).pluck(:id)
            expect(actual_ids).to include(career_profile.id)

            actual_ids = instance.process_hiring_manager_filters({:in_school => nil}).pluck(:id)
            expect(actual_ids).to include(career_profile.id)

        end

        it "should work for someone with an education experience with graduation_year set to this year" do
            # This is not really a sufficient test. We're basically testing the one case
            # if you run this test from January - May and then the other case if you
            # run it from June - December.  But I don't know any way to mock out now()
            # in the database, so this is the best I could come up with.
            expected_in_school = Time.now.month < 6
            career_profile = CareerProfile.joins(:education_experiences).first
            career_profile.education_experiences.each do |e|
                e.update({
                    graduation_year: Time.now.year,
                    degree_program: true
                })
            end
            update_career_profile_search_helpers([career_profile])

            actual_ids = instance.process_hiring_manager_filters({:in_school => true}).pluck(:id)
            if expected_in_school
                expect(actual_ids).to include(career_profile.id)
            else
                expect(actual_ids).not_to include(career_profile.id)
            end

            actual_ids = instance.process_hiring_manager_filters({:in_school => false}).pluck(:id)
            if expected_in_school
                expect(actual_ids).not_to include(career_profile.id)
            else
                expect(actual_ids).to include(career_profile.id)
            end

            actual_ids = instance.process_hiring_manager_filters({:in_school => nil}).pluck(:id)
            expect(actual_ids).to include(career_profile.id)
        end

    end

    def add_skills_to_profiles
        career_profiles = CareerProfile.active.limit(3)

        # Make sure there are corresponding skills options
        @ruby_option = SkillsOption.create!({text: "Ruby", suggest: true})
        @javascript_option = SkillsOption.create!({text: "JavaScript", suggest: true})
        @custom_option = SkillsOption.create!({text: "Custom Skill", suggest: false})

        @ruby_profile = career_profiles[0]
        @ruby_profile.update_attribute(:skills, [@ruby_option])

        @javascript_profile = career_profiles[1]
        @javascript_profile.update_attribute(:skills, [@javascript_option])

        @custom_profile = career_profiles[2]
        @custom_profile.update_attribute(:skills, [@custom_option])

        update_career_profile_search_helpers(career_profiles)
    end

    def update_career_profile_search_helpers(career_profiles = [])
        # if you have not changed anything about the career profile that would
        # touch its updated at, then you have to pass it in here.  Otherwise
        # you can save a couple milliseconds by not passing it in
        career_profiles.map(&:touch)
        CareerProfile::SearchHelper.write_new_records
    end

end