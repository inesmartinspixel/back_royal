require 'spec_helper'

describe ControllerMixins::HandlesEnrollmentDocuments do
    attr_reader :instance, :asset, :current_user

    class HandlesEnrollmentDocumentsKlass
        include ControllerMixins::HandlesEnrollmentDocuments
    end

    before(:each) do
        @instance = HandlesEnrollmentDocumentsKlass.new

        file = fixture_file_upload('files/test.pdf', 'application/pdf')
        @asset = S3IdentificationAsset.create!({
            file: @file,
            user_id: User.first.id
        })
        @asset.update_columns(file_file_name: 'test_with_a_long_name_in_case_of_necessary_truncation.pdf')
        @current_user = User.where.not(id: @asset.user.id).first
    end

    it "should create_upload_timeline_event" do
        instance.create_upload_timeline_event(asset, 'foo', current_user)
        event = asset.user.persisted_timeline_events.reorder(:created_at).last
        expect(event.created_at).to be_within(5.seconds).of(Time.now)
        expect(event.attributes.symbolize_keys).to include(
            event: 'performed_action',
            labels: ['foo', 'upload'],
            text: 'Uploaded test_with_a_long_name_...',
            editor_id: current_user.id,
            editor_name: current_user.name,
            category: 'enrollment'
        )
    end

    it "should create_deletion_timeline_event" do
        instance.create_deletion_timeline_event(asset, 'foo', current_user)
        event = asset.user.persisted_timeline_events.reorder(:created_at).last
        expect(event.created_at).to be_within(5.seconds).of(Time.now)
        expect(event.attributes.symbolize_keys).to include(
            event: 'performed_action',
            labels: ['foo', 'deletion'],
            text: 'Deleted test_with_a_long_name_...',
            editor_id: current_user.id,
            editor_name: current_user.name,
            category: 'enrollment'
        )
    end
end