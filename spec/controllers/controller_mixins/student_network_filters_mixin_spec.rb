require 'spec_helper'

describe ControllerMixins::StudentNetworkFiltersMixin do
    attr_reader :instance

    fixtures :cohorts

    class FilterableForStudentNetworkFiltersMixinSpec
        include ControllerMixins::StudentNetworkFiltersMixin
    end

    def turn_off_filters(symbols)
        symbols.each do |symbol|
            allow(instance).to receive(symbol) do |query|
                query
            end
        end
    end

    before(:each) do
        @instance = FilterableForStudentNetworkFiltersMixinSpec.new
    end

    describe "process_student_network_filters" do

        it "should call specific filter_for_* methods" do
            expect(instance).to receive(:filter_for_access_to_network).exactly(1).times.with({}, CareerProfile).and_return(CareerProfile)
            expect(instance).to receive(:filter_for_isolated_network_cohorts).exactly(1).times.with(CareerProfile).and_return(CareerProfile)
            instance.process_student_network_filters
        end

    end

    describe "with within_geometry filter" do
        # FIXME
    end

    describe "filter_for_access_to_network" do

        before(:each) do
            turn_off_filters([:filter_for_isolated_network_cohorts])
        end

        it "should filter out any profile that does not have student network inclusion" do
            turn_off_filters([:filter_for_has_location])
            user = get_user_with_network_access

            user.student_network_inclusion.destroy
            expect(instance.process_student_network_filters.where(user_id: user.id)).to be_empty
        end

        it "should filter out hidden profiles" do
            turn_off_filters([:filter_for_has_location])
            user = get_user_with_network_access

            user.update!(pref_student_network_privacy: 'hidden')
            expect(instance.process_student_network_filters.where(user_id: user.id)).to be_empty
        end

        it "should call filter_for_has_location" do
            expect(instance).to receive(:filter_for_has_location) { |query| query }
            get_user_with_network_access
        end

        def get_user_with_network_access
            user = User.joins(:student_network_inclusion).first

            # process_student_network_filters always calls filter_for_access_to_network
            expect(instance).to receive(:filter_for_access_to_network).at_least(1).times.and_call_original
            expect(instance.process_student_network_filters.where(user_id: user.id).exists?).to be(true)
            user
        end
    end

    describe "filter_for_isolated_network_cohorts" do

        attr_accessor :query, :user, :cohort

        before(:each) do
            @query = CareerProfile
            @cohort = cohorts(:published_emba_cohort)
            @user = 'mock_user'
            allow(instance).to receive(:current_user).and_return(@user)
        end

        it "should restrict results to relevant_cohort's id if current_user.in_isolated_network_cohort?" do
            expect(user).to receive(:in_isolated_network_cohort?).and_return(true)
            expect(user).to receive(:relevant_cohort).at_least(1).times.and_return(cohort)
            cohort_ids = instance.filter_for_isolated_network_cohorts(query).pluck(:cohort_id).uniq
            expect(cohort_ids).to match_array([cohort.id])
        end

        it "should restrict results to non-isolated cohorts if !current_user.in_isolated_network_cohort?" do
            expect(user).to receive(:in_isolated_network_cohort?).exactly(2).times.and_return(false)

            mba_cohort = cohorts(:published_mba_cohort)

            cohort_ids = instance.filter_for_isolated_network_cohorts(query).pluck(:cohort_id)
            expect(cohort_ids).to include(cohort.id)
            expect(cohort_ids).to include(mba_cohort.id)

            mba_cohort.update_column(:isolated_network, true)
            cohort_ids = instance.filter_for_isolated_network_cohorts(query).pluck(:cohort_id).uniq
            expect(cohort_ids).to match_array([cohort.id])
        end

    end

    describe "filter_for_has_location" do


        it "should work" do
            turn_off_filters([:filter_for_non_hidden_network_inclusion, :filter_for_isolated_network_cohorts])

            with_has_location = CareerProfile.where.not(location: nil).first
            with_no_location = CareerProfile.where(location: nil).first
            actual_user_ids = instance.process_student_network_filters
                                .where(user_id: [with_has_location.user_id, with_no_location.user_id])
                                .pluck(:user_id)
            expect(actual_user_ids).to eq([with_has_location.user_id])
        end
    end

    describe "with viewports filter" do
        it "should work" do
            disable_default_filters
            san_jose = CareerProfile.where(location: "0101000020E61000009CA0979BB9785EC0981E03684AAB4240").first
            canada = CareerProfile.where(location: "0101000020E61000005CC5E23785DD53C098AC8A7093074640").first

            # this viewports value has us zoomed in on the bay area
            actual_user_ids = instance.process_student_network_filters(viewports: ["-133.18995360344064,27.300259340388536,-111.83863524406564,48.45895002087232"])
                                .where(user_id: [san_jose.user_id, canada.user_id])
                                .pluck(:user_id)
            expect(actual_user_ids).to eq([san_jose.user_id])
        end
    end

    describe "with cohort_id filter" do

        it "should work" do
            disable_default_filters
            cohort_application = CohortApplication.where(status: ['accepted']).first
            cohort_id = cohort_application.cohort_id

            expect(instance.process_student_network_filters(cohort_id: cohort_id).where(user_id: cohort_application.user_id)).not_to be_empty
            cohort_application.update!(cohort_id: Cohort.where.not(id: cohort_id).first.id)
            expect(instance.process_student_network_filters(cohort_id: cohort_id).where(user_id: cohort_application.user_id)).to be_empty

        end

    end

    describe "with program_type filter" do

        it "should work" do
            disable_default_filters
            cohort_application = CohortApplication.where(status: ['accepted']).first
            cohort = cohort_application.cohort

            expect(instance.process_student_network_filters(program_type: cohort.program_type).where(user_id: cohort_application.user_id)).not_to be_empty
            cohort_application.update!(cohort_id: Cohort.where.not(program_type: cohort.program_type).first.id)
            expect(instance.process_student_network_filters(program_type: cohort.program_type).where(user_id: cohort_application.user_id)).to be_empty
        end

    end

    describe "with active_in_last filter" do

        it "should work" do
            disable_default_filters

            users =  User.find(instance.process_student_network_filters.limit(2).pluck(:user_id))
            users[0].update_attribute(:last_seen_at, Time.now - 25.hours)
            users[1].update_attribute(:last_seen_at, Time.now - 23.hours)

            actual_user_ids = instance.process_student_network_filters(active_in_last: 1.day.seconds).pluck(:user_id)
            expect(actual_user_ids).to include(users[1].id)
            expect(actual_user_ids).not_to include(users[0].id)
        end

    end

    describe "with student_network_looking_for filter" do

        it "should work" do
            disable_default_filters

            career_profiles = CareerProfile.limit(2)
            career_profiles[0].update(student_network_looking_for: [])
            career_profiles[1].update(student_network_looking_for: ['love in all the wrong places'])

            actual_user_ids = instance.process_student_network_filters().pluck(:user_id)
            expect(actual_user_ids).to include(career_profiles[0].user_id)
            expect(actual_user_ids).to include(career_profiles[1].user_id)

            actual_user_ids = instance.process_student_network_filters(student_network_looking_for: ['something else']).pluck(:user_id)
            expect(actual_user_ids).to match_array([])

            actual_user_ids = instance.process_student_network_filters(student_network_looking_for: ['love in all the wrong places']).pluck(:user_id)
            expect(actual_user_ids).to include(career_profiles[1].user_id)
            expect(actual_user_ids).not_to include(career_profiles[0].user_id)
        end

    end

    describe "with student_network_interests_joins filter" do

        before(:each) do
            disable_default_filters
        end

        it "should work" do

            career_profiles = CareerProfile.limit(2)
            CareerProfile.update_from_hash!(career_profiles[0].as_json(view: 'editable').merge(
                student_network_interests: [{
                    'text' => 'long walks on the beach',
                    'locale' => 'en'
                }]
            ))
            CareerProfile.update_from_hash!(career_profiles[1].as_json(view: 'editable').merge(
                student_network_interests: []
            ))
            update_career_profile_search_helpers

            actual_user_ids = instance.process_student_network_filters().pluck(:user_id)
            expect(actual_user_ids).to include(career_profiles[0].user_id)
            expect(actual_user_ids).to include(career_profiles[1].user_id)

            actual_user_ids = instance.process_student_network_filters(student_network_interests: ['something else']).pluck(:user_id)
            expect(actual_user_ids).to match_array([])

            actual_user_ids = instance.process_student_network_filters(student_network_interests: ['long walks on the beach']).pluck(:user_id)
            expect(actual_user_ids).to include(career_profiles[0].user_id)
            expect(actual_user_ids).not_to include(career_profiles[1].user_id)
        end

        it "should provide tsvector searches for case inclusive case-insensitive fulltext" do
            career_profile = CareerProfile.first
            CareerProfile.update_from_hash!(career_profile.as_json(view: 'editable').merge(
                student_network_interests: [{
                    'text' => 'long walks on the beach',
                    'locale' => 'en'
                }]
            ))
            update_career_profile_search_helpers
            actual_user_ids = instance.process_student_network_filters({:student_network_interests => ['long', 'WALKS', 'bEAcH']}).pluck(:user_id)
            expect(actual_user_ids).to include(career_profile.user_id)
        end

        it "should not blowup on invalid tsvector searches" do
            expect{
                instance.process_student_network_filters({:student_network_interests => ['%%P\left(']})
            }.not_to raise_error()
        end

    end

    describe "with alumni filter" do

        it "should work" do
            turn_off_filters([:filter_for_isolated_network_cohorts])

            # do not disabled the default filters here.  We need the join
            # with cohort applications

            career_profiles = CareerProfile.joins(:user => {:cohort_applications => :cohort})
                                .where("cohort_applications.status = ?", 'accepted')
                                .where("cohorts.program_type = 'mba'")
                                .where.not(location: nil)
                                .limit(4).to_a
            career_profiles.first.user.cohort_applications.detect { |ca| ca.status == 'accepted' }.update_attribute(:graduation_status, 'pending')
            career_profiles.second.user.cohort_applications.detect { |ca| ca.status == 'accepted' }.update_attribute(:graduation_status, 'graduated')
            career_profiles.third.user.cohort_applications.detect { |ca| ca.status == 'accepted' }.update_attribute(:graduation_status, 'failed')
            career_profiles.fourth.user.cohort_applications.detect { |ca| ca.status == 'accepted' }.update_attribute(:graduation_status, 'honors')

            actual_user_ids = instance.process_student_network_filters().pluck(:user_id)
            expect(actual_user_ids).to include(career_profiles[0].user_id)
            expect(actual_user_ids).to include(career_profiles[1].user_id)
            expect(actual_user_ids).not_to include(career_profiles[2].user_id)
            expect(actual_user_ids).to include(career_profiles[3].user_id)

            actual_user_ids = instance.process_student_network_filters(alumni: true).pluck(:user_id)
            expect(actual_user_ids).not_to include(career_profiles[0].user_id)
            expect(actual_user_ids).to include(career_profiles[1].user_id)
            expect(actual_user_ids).not_to include(career_profiles[2].user_id)
            expect(actual_user_ids).to include(career_profiles[3].user_id)

            actual_user_ids = instance.process_student_network_filters(alumni: false).pluck(:user_id)
            expect(actual_user_ids).to include(career_profiles[0].user_id)
            expect(actual_user_ids).not_to include(career_profiles[1].user_id)
            expect(actual_user_ids).not_to include(career_profiles[2].user_id)
            expect(actual_user_ids).not_to include(career_profiles[3].user_id)
        end

    end

    describe "with industries filter" do

        it "should ignore job_sectors_of_interest" do
            disable_default_filters
            career_profiles = CareerProfile.limit(5)
            career_profiles[0].update_attribute(:job_sectors_of_interest, ['a', 'b'])
            career_profiles[1].update_attribute(:job_sectors_of_interest, ['b', 'c'])
            career_profiles[2].update_attribute(:job_sectors_of_interest, [''])
            career_profiles[3].update_attribute(:job_sectors_of_interest, ['d'])
            career_profiles[4].update_attribute(:job_sectors_of_interest, ['e'])

            actual_user_ids = instance.process_student_network_filters(:industries => ['b', 'd']).pluck(:user_id)
            expect(actual_user_ids).not_to include(career_profiles[0].user_id)
            expect(actual_user_ids).not_to include(career_profiles[1].user_id)
            expect(actual_user_ids).not_to include(career_profiles[2].user_id)
            expect(actual_user_ids).not_to include(career_profiles[3].user_id)
            expect(actual_user_ids).not_to include(career_profiles[4].user_id)
        end

        it "should find profiles that match industries in work experiences" do
            disable_default_filters
            # add the filter to make sure included in CareerProfileSearchHelpers
            career_profiles = CareerProfile.select('distinct on (career_profiles.id) career_profiles.*')
                                .joins(:work_experiences)
                                .where("career_profiles.updated_at > career_profiles.created_at").limit(3)
            career_profiles[0].work_experiences[0].update_attribute(:industry, 'a')
            career_profiles[1].work_experiences[0].update_attribute(:industry, 'b')
            career_profiles[2].work_experiences[0].update_attribute(:industry, 'c')
            update_career_profile_search_helpers(career_profiles)

            actual_user_ids = instance.process_student_network_filters(:industries => ['a', 'b']).pluck(:user_id)
            expect(actual_user_ids).to include(career_profiles[0].user_id)
            expect(actual_user_ids).to include(career_profiles[1].user_id)
            expect(actual_user_ids).not_to include(career_profiles[2].user_id)
        end
    end

    describe "with keyword search" do

        attr_accessor :career_profile

        before(:each) do
            disable_default_filters

            user = 'mock_user'
            expect(user).to receive(:has_full_student_network_access?) do
                @has_full_student_network_access
            end.at_least(1).times
            allow(instance).to receive(:current_user).and_return(user)

            @career_profile = CareerProfile.joins(:work_experiences)
                                .where(last_calculated_complete_percentage: 100)
                                .first
            career_profile.user.name = "Bartholomeu"
            career_profile.user.nickname = "Bartleby"
            career_profile.user.pref_student_network_privacy = 'full'
            career_profile.user.save!
            RebuildCareerProfileFulltextJob.perform_now(career_profile.id) # perform immediately
        end


        it "should search student_network_full_keyword_text if user is allowed" do
            @has_full_student_network_access = true

            actual_user_ids = instance.process_student_network_filters(:keyword_search => 'Bartholomeu').pluck(:user_id)
            expect(actual_user_ids).to include(career_profile.user_id)

            actual_user_ids = instance.process_student_network_filters(:keyword_search => 'Bartleby').pluck(:user_id)
            expect(actual_user_ids).to include(career_profile.user_id)

            actual_user_ids = instance.process_student_network_filters(:keyword_search => career_profile.work_experiences[0].industry).pluck(:user_id)
            expect(actual_user_ids).to include(career_profile.user_id)

            actual_user_ids = instance.process_student_network_filters(:keyword_search => 'xxxxxxxxxxxxxx').pluck(:user_id)
            expect(actual_user_ids).not_to include(career_profile.user_id)
        end

        it "should search student_network_anonymous_keyword_text if user is restricted" do
            @has_full_student_network_access = false

            actual_user_ids = instance.process_student_network_filters(:keyword_search => 'Bartholomeu').pluck(:user_id)
            expect(actual_user_ids).not_to include(career_profile.user_id)

            actual_user_ids = instance.process_student_network_filters(:keyword_search => 'Bartleby').pluck(:user_id)
            expect(actual_user_ids).not_to include(career_profile.user_id)

            actual_user_ids = instance.process_student_network_filters(:keyword_search => career_profile.work_experiences[0].industry).pluck(:user_id)
            expect(actual_user_ids).to include(career_profile.user_id)

            actual_user_ids = instance.process_student_network_filters(:keyword_search => 'xxxxxxxxxxxxxx').pluck(:user_id)
            expect(actual_user_ids).not_to include(career_profile.user_id)
        end

    end

    describe "ordering priority" do

        before(:each) do
            turn_off_filters([:filter_for_isolated_network_cohorts])

            user = 'mock_user'
            expect(user).to receive(:has_full_student_network_access?) do
                @has_full_student_network_access
            end.at_least(1).times
            allow(instance).to receive(:current_user).and_return(user)

            @has_full_student_network_access = true
        end

        it "should order non-anonymized profiles first" do

            # add the filter to make sure included in CareerProfileSearchHelpers
            career_profiles = instance.process_student_network_filters.limit(5)
            career_profiles[0].user.update_attribute(:pref_student_network_privacy, 'full')
            career_profiles[1].user.update_attribute(:pref_student_network_privacy, 'hidden')
            career_profiles[2].user.update_attribute(:pref_student_network_privacy, 'anonymous')
            career_profiles[3].user.update_attribute(:pref_student_network_privacy, 'full')
            career_profiles[4].user.update_attribute(:pref_student_network_privacy, 'hidden')

            limiting_val = "Some User #{SecureRandom.uuid}"
            career_profiles.each do |career_profile|
                career_profile.user.update_attribute(:name, limiting_val)
                RebuildCareerProfileFulltextJob.perform_now(career_profile.id) # perform immediately
            end

            actual_user_ids = instance.process_student_network_filters(:keyword_search => limiting_val).pluck(:user_id)

            expect(actual_user_ids[0..1]).to match_array([career_profiles[0].user_id, career_profiles[3].user_id])
        end
    end

    def disable_default_filters
        expect(instance).to receive(:filter_for_access_to_network).at_least(1).times { |filters, query| query }
        expect(instance).to receive(:filter_for_isolated_network_cohorts).at_least(1).times { |query| query }
    end

    def update_career_profile_search_helpers(career_profiles = [])
        # if you have not changed anything about the career profile that would
        # touch its updated at, then you have to pass it in here.  Otherwise
        # you can save a couple milliseconds by not passing it in
        career_profiles.map(&:touch)
        CareerProfile::SearchHelper.write_new_records
    end

end