require 'spec_helper'

describe ControllerMixins::CareerProfilesFiltersMixin do
    attr_reader :instance

    class FilterableForCareerProfilesFiltersMixinSpec
        include ControllerMixins::CareerProfilesFiltersMixin
    end

    before(:each) do
        @instance = FilterableForCareerProfilesFiltersMixinSpec.new
    end

    describe "user_id" do
        before(:each) do
            @career_profile = CareerProfile.first
        end

        it "should return the profile for a particular user" do
            actual_ids = instance.process_career_profiles_filters({:user_id => @career_profile.user_id}).pluck(:id)
            expect(actual_ids).to eq([@career_profile.id])
        end
    end

    describe "places" do

        before(:each) do
            add_locations_to_profiles

            # a location close to dc should find
            # dc candidates
            bethesda = Location.new("38.9847° N, 77.0947° W")
            @bethesda = {
                lat: bethesda.lat,
                lng: bethesda.lng
            }

            # a location close to austin
            dripping_springs = Location.new("30.1902° N, 98.0867° W")
            @dripping_springs = {
                lat: dripping_springs.lat,
                lng: dripping_springs.lng
            }
        end

        it "should find only local candidates if only_local = true" do
            actual_ids = instance.process_career_profiles_filters({:only_local => true, :places => [@bethesda]}).pluck(:id)
            expect(actual_ids).not_to include(@irrelevant_profile.id)
            expect(actual_ids).not_to include(@relocatable_profile.id)
            expect(actual_ids).not_to include(@flexible_us_profile.id)
            expect(actual_ids).to include(@dc_profile.id)
        end

        it "should find only local candidates and ones willing to relocate if only_local != true" do
            actual_ids = instance.process_career_profiles_filters({:places => [@bethesda]}).pluck(:id)
            expect(actual_ids).not_to include(@irrelevant_profile.id)
            expect(actual_ids).to include(@relocatable_profile.id)
            expect(actual_ids).to include(@flexible_us_profile.id)
            expect(actual_ids).to include(@dc_profile.id)
        end

        it "should only show a willing_to_relocate=false user if they are local" do
            @dc_profile.willing_to_relocate = false
            @dc_profile.locations_of_interest.push("flexible") # the main cause of the bug that this test was written for

            actual_ids = instance.process_career_profiles_filters({:places => [@dripping_springs]}).pluck(:id)
            expect(actual_ids).not_to include(@dc_profile.id)

            actual_ids = instance.process_career_profiles_filters({:places => [@bethesda]}).pluck(:id)
            expect(actual_ids).to include(@dc_profile.id)
        end
    end

    def add_locations_to_profiles
        career_profiles = CareerProfile.active.limit(7)

        country_us = {'short' => 'US'}
        country_de = {'short' => 'DE'}

        @flexible_non_us_profile = career_profiles[0]
        @flexible_non_us_profile.update({:locations_of_interest => ['flexible'], :place_details => {'country' => country_de}})

        @flexible_us_profile = career_profiles[1]
        @flexible_us_profile.update({:locations_of_interest => ['flexible'], :place_details => {'country' => country_us}})

        @dc_profile = career_profiles[2]
        @dc_profile.update({:locations_of_interest => [], :place_details => {'lng' => -77.0369, 'lat' => 38.9072, 'country' => country_us}})

        @boston_profile = career_profiles[3]
        @boston_profile.update({:locations_of_interest => [], :place_details => {'lng' => -71.0589, 'lat' => 42.3601, 'country' => country_us}})

        @relocatable_profile = career_profiles[4]
        @relocatable_profile.update_attribute(:locations_of_interest, ['washington_dc'])

        @boston_relocatable_profile = career_profiles[5]
        @boston_relocatable_profile.update_attribute(:locations_of_interest, ['boston'])

        @irrelevant_profile = career_profiles[6]
        @irrelevant_profile.update({:locations_of_interest => ['cleveland'], :place_details => {'lng' => 0, 'lat' => 0}})

        update_career_profile_search_helpers
    end

    def update_career_profile_search_helpers(career_profiles = [])
        # if you have not changed anything about the career profile that would
        # touch its updated at, then you have to pass it in here.  Otherwise
        # you can save a couple milliseconds by not passing it in
        career_profiles.map(&:touch)
        CareerProfile::SearchHelper.write_new_records
    end

end