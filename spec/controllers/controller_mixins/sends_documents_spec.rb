require 'spec_helper'

describe ControllerMixins::SendsDocuments do
    attr_reader :controller, :record, :file

    class MyController < Api::ApiCrudControllerBase
        include ControllerMixins::SendsDocuments

        def current_user_id
            @current_user_id ||= User.first.id
        end
    end

    class MyKlass
        attr_accessor :id, :file_file_name, :file_content_type

        def initialize(attrs)
            self.id = attrs[:id]
            self.file_file_name = attrs[:file_file_name]
            self.file_content_type = attrs[:file_content_type]
        end

        def file
            @file ||= "mock_file"
        end
    end

    before(:each) do
        @file = fixture_file_upload('files/test.pdf', 'application/pdf')
        @controller = MyController.new

        @record = MyKlass.new({
            id: SecureRandom.uuid,
            file_file_name: 'filename',
            file_content_type: 'type'
         })
        allow(MyKlass).to receive(:find_by_id).with(record.id).and_return(record)
        allow(controller).to receive(:current_ability).and_return({})
        allow(controller).to receive(:params).and_return({id: record.id})
        allow(record.file).to receive(:expiring_url).with(20).and_return('expiring_url')
        allow(controller).to receive(:open).with(record.file.expiring_url(20)).and_return(open(file))
    end

    describe "send_document" do

        it "should 401 if not authorized" do
            expect(controller.current_ability).to receive(:can?).with(:download, MyKlass).and_return(false)
            expect(Event).not_to receive(:create_server_event!)
            expect(controller).not_to receive(:send_data)
            expect(controller).to receive(:render_unauthorized_error)
            controller.send_document(MyKlass, 'event_type')
        end

        it "should render the document" do
            expect(controller.current_ability).to receive(:can?).with(:download, MyKlass).and_return(true)
            expect(controller).to receive(:send_data).with(
                file.read,
                :filename => record.file_file_name,
                :type => record.file_content_type,
                :disposition => "attachment"
            )
            file.rewind # make it so it can be read again

            controller.send_document(MyKlass, 'event_type')

        end

        it "should log an event" do
            expect(controller).to receive(:send_data)
            expect(controller.current_ability).to receive(:can?).with(:download, MyKlass).and_return(true)
            event = Event.new
            expect(event).to receive(:log_to_external_systems)

            expect(Event).to receive(:create_server_event!).with(
                anything,
                controller.current_user_id,
                'event_type',
                {
                    label: record.id
                }).and_return(event)

            controller.send_document(MyKlass, 'event_type')
        end

    end

end