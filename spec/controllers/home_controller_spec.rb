require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe HomeController do
    include ControllerSpecHelper
    render_views

    describe "GET 'index'" do
        before :all do
            asset = S3Asset.first
            @valid_attrs = {
                site_name: "Smartly",
                default_title: "Smartly - Get Smarter at Work in Fun, 5-Minute Lessons",
                default_description: "Master marketing, finance, business strategy, & more with Smartly's fun, 5-minute lessons and courses. Sign up completely free to start learning now!",
                default_image: asset
            }
        end

        before(:each) do
            # if we disable the webpack helpers, then we can run rails specs without a
            # manifest.json, which is nice on circle, as it allows us to avoid the JS build task
            allow(controller).to receive(:disable_webpack_helpers?).and_return(true)
            expect(GlobalMetadata).to receive(:find_by_action).and_return(GlobalMetadata.new(@valid_attrs))
        end

        it "should be successful" do
            get 'index'
            expect(response).to be_successful
        end

        it "should set default title and description for seo" do
            get 'index'
            expect(assigns(:action_metadata).title).to eq(@valid_attrs[:default_title])
            expect(assigns(:action_metadata).description).to eq(@valid_attrs[:default_description])
        end

        it "should set an alternative title for institutional" do
            allow(controller).to receive(:institutional_subdomain).and_return("some-institution")
            get 'index'
            expect(assigns(:action_metadata).title).to eq("Smartly - Learn Online, Everywhere")
        end

        it "should set default image on homepage based on global metadata default_image" do
            get 'index'
            expect(assigns(:action_metadata).image_url).to eq(@valid_attrs[:default_image].formats["original"]["url"])
        end

        it "should load lesson and populate seo friendly fields if possible" do
            l = Lesson.first
            l.entity_metadata.title = "SEO title"
            l.entity_metadata.description = "SEO description"
            l.entity_metadata.canonical_url = "/home/lesson_index"
            l.entity_metadata.image = S3Asset.first
            l.entity_metadata.save!
            l.save!

            assert_routing("/lesson/stuff-and-things/show/#{l.id}", :controller => "home", :action => "lesson_index", :title => "stuff-and-things", :lesson_id => l.id)
            allow(controller).to receive(:params).and_return({lesson_id: l.id})

            get "lesson_index"
            expect(assigns(:action_metadata).title).to eq(l.entity_metadata.title)
            expect(assigns(:action_metadata).description).to eq(l.entity_metadata.description)
            expect(assigns(:action_metadata).canonical_url).to eq("https://test.host#{l.entity_metadata.canonical_url}")
            expect(assigns(:action_metadata).image_url).to eq(l.entity_metadata.image.as_json[:formats]["original"]["url"])
        end

        it "should gracefully handle malformed urls" do

            assert_routing("/lesson/stuff-and-things/unexpected/show/d0369244-700b-4dea-9c45-0a542c155da9", :controller => "home", :action => "index", :path => "lesson/stuff-and-things/unexpected/show/d0369244-700b-4dea-9c45-0a542c155da9")
            get "index", params: { "path"=>"lesson/stuff-and-things/unexpected/show/d0369244-700b-4dea-9c45-0a542c155da9" }

            expect(assigns(:action_metadata).title).to eq(@valid_attrs[:default_title])
            expect(assigns(:action_metadata).description).to eq(@valid_attrs[:default_description])
        end

        it "should load stream and populate seo friendly fields if requested" do
            stream = Lesson::Stream.joins(:entity_metadata).joins(:image)
                        .where("entity_metadata.title is not null")
                        .where("entity_metadata.description is not null")
                        .where("lesson_streams.image_id is not null")
                        .first
            expect(stream).not_to be_nil

            assert_routing("/course/#{stream.id}", :controller => "home", :action => "course_context", :stream_id => stream.id)
            allow(controller).to receive(:params).and_return({stream_id: stream.id})

            get "course_context"
            expect(assigns(:action_metadata).title).to eq("#{stream.entity_metadata.title}")
            expect(assigns(:action_metadata).description).to eq("#{stream.entity_metadata.description}")
            expect(assigns(:action_metadata).image_url).to eq(stream.image.formats["original"]["url"])
        end

    end

    it "should handle resume urls" do
        l = Lesson.first
        l.entity_metadata.title = "SEO title"
        l.entity_metadata.description = "SEO description"
        l.entity_metadata.canonical_url = "/home/lesson_index"
        l.entity_metadata.save!
        l.save!

        assert_routing("/lesson/stuff-and-things/show/#{l.id}",
            {:controller => "home", :action => "lesson_index", :title => "stuff-and-things", :lesson_id => l.id, :frame => 'cdbf2aa4-9cd5-4266-83c8-95c85cd1720e'},
            {},
            {:frame => 'cdbf2aa4-9cd5-4266-83c8-95c85cd1720e'}
        )
    end

    it "should support seo course link" do
        stream = Lesson::Stream.first
        assert_routing("/course/#{stream.title.parameterize}/#{stream.id}", :controller => "home", :action => "course_context", :stream_id => stream.id, :title => stream.title.parameterize)
    end
end
