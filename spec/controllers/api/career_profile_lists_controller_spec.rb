require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::CareerProfileListsController do
    include ControllerSpecHelper
    json_endpoint

    fixtures(:users)

    before(:each) do
        stub_client_params(controller)
    end

    describe "GET index" do
        attr_accessor :user

        describe "as non-admin" do

            before(:each) do
                @user = users(:user_with_career_profile)
                allow(controller).to receive(:current_user).and_return(@user)
            end

            # This is open to all... for now
            # it "should 401" do
            #     process :index, params: { :format => :json }
            #     assert_error_response(401)
            # end

            it "should filter by name if passed" do
                CareerProfileList.create(:name => "foo")
                CareerProfileList.create(:name => "bar")

                get :index, params: { :filters => {:name => 'foo'}, :format => :json }
                assert_200
                expect(body_json['contents']['career_profile_lists'].size).to eq(1)
                expect(body_json['contents']['career_profile_lists'].first['name']).to eq('foo')
            end

            it "should ask for specific fields if specified" do
                CareerProfileList.create(:name => "foo")
                CareerProfileList.create(:name => "bar")

                expect(controller).to receive(:render_records).with(CareerProfileList.all, {fields: ['name'], filters: {}}).and_call_original
                get :index, params: { :fields => ['name'], :format => :json }
                assert_200
            end

            it "should limit number of profiles if passed" do
                list = CareerProfileList.create(:name => "long_list")
                career_profile_ids = CareerProfile.limit(3).pluck('id')
                list.career_profile_ids = career_profile_ids
                list.save!

                expect(controller).to receive(:render_records).with([list],
                    {
                        fields: ['id', 'name', 'career_profile_ids', 'career_profiles'],
                        filters: {:limit => 2}
                    }).and_call_original
                get :index, params: { :filters => {:name => 'long_list', :career_profiles_limit => 2}, :format => :json }
                assert_200
                expect(body_json['contents']['career_profile_lists'].size).to eq(1)
                expect(body_json['contents']['career_profile_lists'].first['name']).to eq('long_list')
                expect(body_json['contents']['career_profile_lists'].first['career_profiles'].size).to eq(2)
            end
        end

        describe "as admin" do

            before(:each) do
                stub_current_user(controller, [:admin])
            end

            it "should work" do
                get :index, params: { :format => :json }
                assert_200
                expect(body_json['contents']['career_profile_lists'].size).to be(CareerProfileList.count)
            end

        end

    end

    describe "POST create" do

        before(:each) do
            @user = User.joins("LEFT JOIN career_profiles on user_id = users.id")
                    .where("career_profiles.id is null")
                    .first
            allow(controller).to receive(:current_user).and_return(@user)
            @career_profile_prarams = {
                user_id: @user.id
            }
        end

       describe "as non-admin" do

            before(:each) do
                @user = users(:user_with_career_profile)
                allow(controller).to receive(:current_user).and_return(@user)
            end

            it "should 401" do
                process :create, params: { :format => :json }
                assert_error_response(401)
            end
        end

        describe "as admin" do

            before(:each) do
                stub_current_user(controller, [:admin])
            end

            it "should work" do
                expect {
                    get :create, params: { :format => :json, :record => {name: 'list'} }
                    assert_200
                }.to change { CareerProfileList.count}.by(1)

                list = CareerProfileList.reorder('created_at desc').first
                expect(list.name).to eq('list')
                expect(body_json['contents']['career_profile_lists'][0]['id']).to eq(list.id)
            end

        end

    end

    describe "PUT update" do

        describe "as non-admin" do

            before(:each) do
                @user = users(:user_with_career_profile)
                allow(controller).to receive(:current_user).and_return(@user)
            end

            it "should 401" do
                process :update, params: { :format => :json }
                assert_error_response(401)
            end
        end

        describe "as admin" do

            before(:each) do
                stub_current_user(controller, [:admin])
            end

            it "should work" do
                list = CareerProfileList.first
                career_profile_ids = CareerProfile.limit(3).pluck('id')
                expect {
                    get :update, params: { :format => :json, :record => {
                        id: list.id,
                        name: 'new name',
                        career_profile_ids: career_profile_ids

                    } }
                    assert_200
                }.to change { list.reload.updated_at }

                expect(list.name).to eq('new name')
                expect(list.career_profile_ids).to eq(career_profile_ids)
                expect(body_json['contents']['career_profile_lists'][0]['id']).to eq(list.id)
            end

        end
    end

    describe "DELETE destroy" do

        before(:each) do
            @user = User.joins("LEFT JOIN career_profiles on user_id = users.id")
                    .where("career_profiles.id is null")
                    .first
            allow(controller).to receive(:current_user).and_return(@user)
            @career_profile_prarams = {
                user_id: @user.id
            }
            @list = CareerProfileList.first
        end

       describe "as non-admin" do

            before(:each) do
                @user = users(:user_with_career_profile)
                allow(controller).to receive(:current_user).and_return(@user)
            end

            it "should 401" do
                process :destroy, params: { :format => :json, :id => @list.id }
                assert_error_response(401)
            end
        end

        describe "as admin" do

            before(:each) do
                stub_current_user(controller, [:admin])
            end

            it "should work" do
                expect {
                    process :destroy, params: { :format => :json, :id => @list.id }
                    assert_200
                }.to change { CareerProfileList.count}.by(-1)

                expect(CareerProfileList.find_by_id(@list.id)).to be_nil
            end

        end

    end
end