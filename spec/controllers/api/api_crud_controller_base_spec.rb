require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::ApiCrudControllerBase do

    include ControllerSpecHelper

    json_endpoint

    before(:each) do
        # prevent us from intermittently getting error messages in different languages
        I18n.locale = :en
    end

    # see also: controller_spec_helper.rb:stub_client_params

    describe "set_client_config" do

        it "should assign the appropriate ClientConfig instance to client_config" do
            stub_client_params(@controller)
            expect(controller).to receive(:abort_if_using_deprecated_version)
            @controller.set_client_config
            expect(@controller.client_config).not_to be_nil
            expect(@controller.client_config.class).to be(ClientConfig::WebClientConfig)
            expect(@controller.client_config.version_number).to be(42)
        end

        it "should do nothing if client info not provided" do
            @controller.set_client_config
            expect(@controller.meta).to eq({})
        end

        it "should set add info to meta" do
            stub_client_params(@controller)
            expect(controller).to receive(:abort_if_using_deprecated_version)
            expect_any_instance_of(ClientConfig).to receive(:min_allowed_version).at_least(1).times.and_return(1)
            expect_any_instance_of(ClientConfig).to receive(:min_suggested_version).and_return(2)
            expect_any_instance_of(ClientConfig).to receive(:app_store_name).and_return('app_store_name')
            expect_any_instance_of(ClientConfig).to receive(:app_store_name_miyamiya).and_return('app_store_name_miyamiya')
            @controller.set_client_config
            expect(@controller.meta).to eq({
                "min_allowed_client_version"=>1,
                "min_suggested_client_version"=>2,
                "client_app_store_name" => 'app_store_name',
                "client_app_store_name_miyamiya" => 'app_store_name_miyamiya'})
        end

        it "should abort if the client is using a deprecated version" do
            expect(controller).to receive(:client_params).at_least(1).times.and_return({
                'fr-client-version' => 42,
                'fr-client' => 'ios'
            })
            expect_any_instance_of(ClientConfig::IosClientConfig).to receive(:min_allowed_version).at_least(1).times.and_return(43)
            expect(controller).to receive(:render_successful_response).with({})
            @controller.set_client_config
        end

        it "should allow for coding special rules to indicate that someone is using a deprecated client for a particular request" do
            expect(controller).to receive(:using_deprecated_client?).at_least(1).times.and_return(true)
            expect(controller).to receive(:client_params).at_least(1).times.and_return({
                'fr-client-version' => 42,
                'fr-client' => 'web'
            })
            expect_any_instance_of(ClientConfig::WebClientConfig).not_to receive(:min_allowed_version)
            expect(controller).to receive(:render_successful_response).with({})
            @controller.set_client_config
            expect(@controller.meta).to have_entries({
                "min_allowed_client_version"=>43,
                "min_suggested_client_version"=>43})
        end

        it "should raise if special rules indicate this is a deprecated client and it is not a web client" do
            expect(controller).to receive(:using_deprecated_client?).at_least(1).times.and_return(true)
            expect(controller).to receive(:client_params).at_least(1).times.and_return({
                'fr-client-version' => 42,
                'fr-client' => 'ios'
            })
            expect {
                @controller.set_client_config
            }.to raise_error("We can only force reloads on deprecated web clients")
        end

    end

    describe "with an invalid id" do
        it "should render a 404" do
            @controller.params[:id] = 'not_a_guid'
            expect(@controller).to receive(:render_error_response).with("We cannot find what you are looking for", :not_found)
            @controller.require_guid_id
        end
    end

    describe "save_event_bundle_from_meta" do

        before(:each) do
            stub_current_user(@controller, [:learner])
        end

        it "should save a provided bundle" do
            events = event_bundle
            @controller.params[:meta] = {
                event_bundle: {"events" => events}
            }
            @controller.params[:http_queue] = {:queued_for_seconds => 42}
            expect(@controller).to receive(:now).and_return(:now)
            expect(Event).to receive(:create_batch_from_hash!).with(events, {
                user_id: @controller.current_user_id,
                hit_server_at: :now,
                request_queued_for_seconds: 42
            })
            @controller.save_event_bundle_from_meta
        end

        it "should include ip address if available" do
            @controller.request.env["HTTP_X_FORWARDED_FOR"] = "1.2.3.4,6.7.8.9"
            @controller.params[:meta] = {
                event_bundle: {"events" => event_bundle}
            }
            @controller.params[:http_queue] = {:queued_for_seconds => 42}
            expect(@controller).to receive(:now).and_return(:now)

            # we should have been able to do this more cleanly with
            # hash_including, but it did not seem to work for me
            observed_ip_address = nil
            expect(Event).to receive(:create_batch_from_hash!) do |events, opts|
                observed_ip_address =  opts[:client_ip_address]
            end
            @controller.save_event_bundle_from_meta
            expect(observed_ip_address).to eq("1.2.3.4")
        end

        it "should include cloudflare_country_code" do
            @controller.request.env["HTTP_CF_IPCOUNTRY"] = "US"
            @controller.params[:meta] = {
                event_bundle: {"events" => event_bundle}
            }
            expect(@controller).to receive(:now).and_return(:now)

            # we should have been able to do this more cleanly with
            # hash_including, but it did not seem to work for me
            observed_country_code = nil
            expect(Event).to receive(:create_batch_from_hash!) do |events, opts|
                observed_country_code =  opts[:cloudflare_country_code]
            end
            @controller.save_event_bundle_from_meta
            expect(observed_country_code).to eq("US")
        end

        it "should do nothing if no bundle provided" do
            @controller.params[:meta] = {}
            expect(Event).not_to receive(:create_batch_from_hash!)
            @controller.save_event_bundle_from_meta
        end

        it "should not catch errors" do
            @controller.params[:meta] = {
                event_bundle: {"events" => event_bundle}
            }
            err = RuntimeError.new("")
            expect(Raven).not_to receive(:capture_exception).with(err)
            expect(Event).to receive(:create_batch_from_hash!)
            @controller.save_event_bundle_from_meta
        end

    end

    describe "save_event_bundle" do
        it "should respect force_auid" do
            auid = SecureRandom.uuid
            @controller.params[:meta] = {
                force_auid: auid
            }
            @controller.params[:http_queue] = {:queued_for_seconds => 42}
            expect(@controller).to receive(:now).and_return(:now)
            events = event_bundle
            expect(Event).to receive(:create_batch_from_hash!).with(events, {
                user_id: auid,
                hit_server_at: :now,
                request_queued_for_seconds: 42
            })
            @controller.save_event_bundle(events)
        end

        it "should raise if the bundle is too big" do

            expect(Event).not_to receive(:create_batch_from_hash!)

            events = [:event] * 2001
            expect {
                @controller.save_event_bundle(events)
            }.to raise_error(Api::ApiCrudControllerBase::TooManyEventsInBundle)

        end

        it "should raise if user_from_auid_deleted?" do
            expect(@controller).to receive(:user_from_auid_deleted?).and_return(true)
            expect {
                @controller.save_event_bundle(:events)
            }.to raise_error(Api::ApiCrudControllerBase::UserFromAuidDeleted)
        end

    end

    describe "update_last_seen_at" do

        it "should update last_seen_at and set_user_push_messages if it is old enough" do
            allow(@controller).to receive(:params).and_return({
                ghost_mode: "false"
            })

            expect(@controller).to receive(:current_user).at_least(1).times.and_return(User.first)
            expect(@controller).to receive(:set_user_push_messages)
            @controller.current_user.last_seen_at = Time.now - 4.hours
            @controller.update_last_seen_at
            expect(@controller.current_user.reload.last_seen_at).to be_within(5.seconds).of(Time.now)
        end

        it "should not update last_seen_at if it is recent" do
            allow(@controller).to receive(:params).and_return({
                ghost_mode: "false"
            })

            user = User.first
            user.last_seen_at = 3.hours.ago
            user.save!
            expect(@controller).to receive(:current_user).at_least(1).times.and_return(user)

            expect {
                @controller.update_last_seen_at
            }.not_to change { user.reload.last_seen_at }
        end

        it "should work with no user" do
            allow(@controller).to receive(:params).and_return({
                ghost_mode: "false"
            })

            expect(@controller).to receive(:current_user).at_least(1).times.and_return(nil)
            expect {
                @controller.update_last_seen_at
            }.not_to raise_error
        end

        it "should not update last_seen_at if request is in ghost mode" do
            allow(@controller).to receive(:params).and_return({
                ghost_mode: "true"
            })
        end
    end

    describe "add_num_recommended_positions_to_push_messages" do
        it "should not run if current_user's career_profile is not editable_and_complete" do
            current_user = users(:user_with_career_profile)
            expect(controller).to receive(:current_user).at_least(1).times.and_return(current_user)
            expect(controller.current_user.career_profile).to receive(:career_profile_editable_and_complete?).and_return(false)

            expect_any_instance_of(OpenPosition.const_get(:ActiveRecord_Relation))
                .not_to receive(:recommended_and_viewable)

            controller.add_num_recommended_positions_to_push_messages
        end

        it "should run if current_user's career_profile is editable_and_complete" do
            current_user = users(:user_with_career_profile)
            expect(controller).to receive(:current_user).at_least(1).times.and_return(current_user)
            expect(controller.current_user.career_profile).to receive(:career_profile_editable_and_complete?).and_return(true)

            expect_any_instance_of(OpenPosition.const_get(:ActiveRecord_Relation))
                .to receive(:recommended_and_viewable)
                .and_call_original

            controller.add_num_recommended_positions_to_push_messages
        end
    end

    describe "set_user_push_messages" do

        it "should have expected attrs" do
            user = User.first
            allow(controller).to receive(:current_user).and_return(user)
            controller.set_user_push_messages
            expect(controller.meta['push_messages']['current_user']).to eq({
                'active_playlist_locale_pack_id' => user.active_playlist_locale_pack_id,
                'can_purchase_paid_certs' => user.can_purchase_paid_certs,
                'mba_content_lockable' => user.mba_content_lockable,
                'curriculum_status' => user.last_application&.curriculum_status,
                'deactivated' => user.deactivated,
                'avatar_url' => user.avatar_url,
                'identity_verified' => user.identity_verified,
                'transcripts_verified' => user.transcripts_verified,
                's3_identification_asset' => user.s3_identification_asset.as_json,
                's3_transcript_assets' => user.s3_transcript_assets.as_json,
                's3_english_language_proficiency_documents' => user.s3_english_language_proficiency_documents.as_json,
                'english_language_proficiency_documents_approved' => user.english_language_proficiency_documents_approved,
                'english_language_proficiency_documents_type' => user.english_language_proficiency_documents_type,
                'english_language_proficiency_comments' => user.english_language_proficiency_comments,
                'notify_candidate_positions' => user.notify_candidate_positions,
                'notify_candidate_positions_recommended' => user.notify_candidate_positions_recommended,
                'signable_documents' => user.signable_documents.as_json,
                'updated_at' => user.updated_at.to_timestamp,
                'academic_hold' => user.academic_hold,
                'enable_front_royal_store' => user.enable_front_royal_store,
                'net_amount_paid' => user.net_amount_paid
            })
        end

        it "should add any extra that's been passed in onto current_user push message" do
            user = User.first
            allow(controller).to receive(:current_user).and_return(user)

            controller.meta['push_messages'] ||= {}
            expect(controller.meta['push_messages']['current_user']).not_to have_entries({foo: 'foo'})
            controller.set_user_push_messages(extra: {foo: 'foo'})
            expect(controller.meta['push_messages']['current_user']).to have_entries({foo: 'foo'})
        end
    end

    describe "set_careers_push_messages" do
        it "should set education_experiences when not verified and in an enrollment status" do
            user = users(:pre_accepted_mba_cohort_user)
            allow(controller).to receive(:current_user).and_return(user)
            user.update_columns(transcripts_verified: false)
            user.career_profile.education_experiences = CareerProfile::EducationExperience.limit(3)
            user.career_profile.save!
            controller.set_careers_push_messages
            expect(controller.meta['push_messages']['career_profile']['education_experiences'].map { |e| e['id'] })
                .to match_array(user.career_profile.education_experiences.pluck(:id))
        end

        it "should not set education_experiences if verified" do
            user = users(:pre_accepted_mba_cohort_user)
            allow(controller).to receive(:current_user).and_return(user)
            user.update_columns(transcripts_verified: true)
            user.career_profile.education_experiences = CareerProfile::EducationExperience.limit(3)
            user.career_profile.save!
            controller.set_careers_push_messages
            expect(controller.meta['push_messages']['career_profile']['education_experiences']).to be_nil
        end

        it "should not set education_experiences if not an enrollment status" do
            user = users(:pending_mba_cohort_user)
            allow(controller).to receive(:current_user).and_return(user)
            user.update_columns(transcripts_verified: false)
            user.career_profile.education_experiences = CareerProfile::EducationExperience.limit(3)
            user.career_profile.save!
            controller.set_careers_push_messages
            expect(controller.meta['push_messages']['career_profile']['education_experiences']).to be_nil
        end
    end

    describe "sanitize_params" do
        it "should strip null-bytes from Strings and ActionController::Parameters" do
            unsanitized_params = ActionController::Parameters.new(param1: "null\u0000bytes1")
            sanitized_params = controller.sanitize_params(unsanitized_params)
            expect(sanitized_params.is_a?(ActionController::Parameters)).to be(true)
            expect(sanitized_params[:param1]).to eq('nullbytes1')
        end

        it "should strip null-bytes from ActionController::Parameters" do
            unsanitized_params = ActionController::Parameters.new(param1: "something", param2: { inner_param: "null\u0000bytes1"})
            sanitized_params = controller.sanitize_params(unsanitized_params)
            expect(sanitized_params.is_a?(ActionController::Parameters)).to be(true)
            expect(sanitized_params[:param2][:inner_param]).to eq('nullbytes1')
        end

        it "should strip null-bytes from Arrays" do
            unsanitized_params = ActionController::Parameters.new(param1: ["null\u0000bytes1"] )
            sanitized_params = controller.sanitize_params(unsanitized_params)
            expect(sanitized_params.is_a?(ActionController::Parameters)).to be(true)
            expect(sanitized_params[:param1]).to eq(['nullbytes1'])
        end

        it "should handle all scenarios" do
            unsanitized_params = ActionController::Parameters.new(param1: "null\u0000bytes1", param2: { inner_param1: "null\u0000bytes2", inner_param2: ["null\u0000bytes3"]})
            sanitized_params = controller.sanitize_params(unsanitized_params)
            expect(sanitized_params.is_a?(ActionController::Parameters)).to be(true)
            expect(sanitized_params[:param1]).to eq('nullbytes1')
            expect(sanitized_params[:param2][:inner_param1]).to eq('nullbytes2')
            expect(sanitized_params[:param2][:inner_param2]).to eq(['nullbytes3'])
        end

        it "should be called exactly once via overrided params" do
            expect(controller).to receive(:sanitize_params).exactly(1).times
            controller.params
            controller.params
        end
    end

    def event_bundle
        [{'page_load_id' => SecureRandom.uuid}]
    end
end