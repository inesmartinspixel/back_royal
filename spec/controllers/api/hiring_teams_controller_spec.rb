require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::HiringTeamsController do
    include ControllerSpecHelper
    include StripeHelper
    json_endpoint

    fixtures(:users)

    before(:each) do
        stub_client_params(controller)
    end

    describe "GET index" do

        it "should 401 for non-admin" do
            stub_current_user(controller, [:learner])
            get :index, params: { format: :json }
            assert_error_response(401)
        end

        it "should return hiring teams" do
            stub_current_user(controller, [:admin])
            get :index, params: { format: :json }
            assert_200
            expect(body_json['contents']['hiring_teams'].map { |e| e['id']}).to match_array(HiringTeam.pluck('id'))
        end

        it "should use eager loading" do
            hiring_teams = controller.preload_hiring_teams_for_index

            # once the records are preloaded, there should be no more
            # queries required to convert them to json
            expect(ActiveRecord::Base.connection).not_to receive(:exec_query).and_call_original
            json = hiring_teams.as_json

            # sanity checks. make sure that we actually generated a full json tree
            hiring_team = json[0]
            expect(hiring_team).not_to be_nil
            expect(hiring_team['id']).not_to be_nil
        end

    end

    describe "GET show" do

        it "should 401 for non-admin" do
            stub_current_user(controller, [:learner])
            hiring_team = HiringTeam.first
            get :show, params: { format: :json, id: hiring_team.id }
            assert_error_response(401)
        end

        it "should render a hiring team" do
            stub_current_user(controller, [:admin])
            hiring_team = HiringTeam.first
            get :show, params: {format: :json, id: hiring_team.id}
            assert_200
            expect(body_json["contents"]["hiring_teams"].length).to eq(1)
            expect(body_json["contents"]["hiring_teams"][0]["id"]).to eq(hiring_team.id)
            expect(body_json["contents"]["hiring_teams"][0]["hiring_manager_ids"]).not_to be_nil
            expect(body_json["contents"]["hiring_teams"][0]["hiring_manager_ids"]).not_to eq(hiring_team.hiring_manager_ids.length)
        end
    end

    describe "POST create" do

        it "should 401 for non-admin" do
            stub_current_user(controller, [:learner])
            post :create, params: { format: :json }
            assert_error_response(401)
        end

        it "should work in normal case" do
            allow(controller).to receive(:current_user).and_return(users(:admin))

            post :create,
                params: {
                    :format => :json,
                    :record => {
                        title: 'Team Foo',
                        domain: 'foo.com'
                    }
                }
            assert_200
        end

    end

    describe "PUT update" do

        it "should 401 if !update_hiring_team_with_params" do
            user = users(:hiring_manager_with_team)
            allow(controller).to receive(:current_user).and_return(user)
            allow(controller.current_ability).to receive(:cannot?).and_return(false) # defaults return value to false
            expect(controller.current_ability).to receive(:cannot?).with(:update_hiring_team_with_params, ActionController::Parameters.new({
                id: user.hiring_team.id,
                hiring_plan: 'hiring_plan'
            })).and_return(true) # verifies that `cannot?` is called with the appropriate values and mocks the return value in that case to true

            put :update,
                params: {
                    :format => :json,
                    :record => user.hiring_team.as_json.merge({
                        id: user.hiring_team.id,
                        hiring_plan: 'hiring_plan'
                    })
                }
            assert_401
        end

        # since there is the odd interaction where the controller filters out certain
        # params that go into ability.rb, I'm not mocking out the can? call.  I'm just
        # letting it run as an integration spec
        it "should work for non-admin" do
            user = users(:hiring_manager_with_team)
            user.hiring_team.update!(hiring_plan: nil)
            expect(user.hiring_team.hiring_plan).to be_nil # sanity check.  make sure callbacks didn't mess this up
            allow(controller).to receive(:current_user).and_return(user)

            orig_title = user.hiring_team.title
            put :update,
                params: {
                    :format => :json,
                    :record => user.hiring_team.as_json.merge({
                        id: user.hiring_team.id,
                        hiring_plan: HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING,
                        title: 'some title that will be ignored'
                    })
                }
            assert_200

            user.hiring_team.reload
            expect(user.hiring_team.hiring_plan).to eq(HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING)
            expect(user.hiring_team.title).to eq(orig_title)
        end

        it "should work in normal case" do
            hiring_team = HiringTeam.first
            allow(controller).to receive(:current_user).and_return(users(:admin))

            put :update,
                params: {
                    :format => :json,
                    :record => hiring_team.as_json.merge({
                        id: hiring_team.id,
                        title: 'A New Title'
                    })
                }
            assert_200
            expect(hiring_team.reload.title).to eq('A New Title')
        end
    end

    describe "DELETE destroy" do

        attr_accessor :hiring_team

        before(:each) do
            self.hiring_team = HiringTeam.joins(:hiring_managers).left_outer_joins(:subscriptions)
                                .where(subscription_required: true)
                                .where("subscriptions.id is null").first
        end

        it "should 401 for non-admin" do
            stub_current_user(controller, [:learner])
            delete :destroy, params: { id: hiring_team.id, format: :json }
            assert_error_response(401)
        end

        it "should work if there are no accepted users" do
            stub_current_user(controller, [:admin])
            hiring_team.hiring_managers.each do |manager|
                manager.hiring_application.update(status: 'pending')
            end
            expect {
                delete :destroy, params: { id: hiring_team.id, format: :json }
            }.to change(HiringTeam, :count).by(-1)
            assert_200
            hiring_managers = User.where(hiring_team_id: hiring_team.id).to_a
            expect(hiring_managers).to eq([])
        end

        it "should fail if there are accepted users" do
            stub_current_user(controller, [:admin])
            hiring_team.hiring_managers.each do |manager|
                manager.hiring_application.update(status: 'accepted')
            end
            delete :destroy, params: { id: hiring_team.id, format: :json }
            assert_error_response(406)
        end

    end

end
