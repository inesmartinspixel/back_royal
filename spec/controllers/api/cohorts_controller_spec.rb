require 'spec_helper'
require 'controllers/api/helpers/api_content_controller_spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::CohortsController do

    fixtures :cohorts

    include ControllerSpecHelper
    include StripeHelper
    json_endpoint

    let(:valid_attributes) {
        {
            "name" => "Organization 1",
            "title" => "title",
            "start_date" => Time.now,
            "end_date" => Time.now,
            "program_type" => "mba",
            "periods" => [{
                "project_style" => "standard",
                "foo" => "bar",
                "days_offset" => 0,
                "days" => 5
            }]
        }
    }

    def new_cohort_params
        {
            "name" => "Organization 1",
            "title" => "title",
            "start_date" => Time.now.to_timestamp,
            "end_date" => Time.now.to_timestamp,
            "program_type" => "mba",
            "periods" => [{
                "project_style" => "standard",
                "foo" => "bar",
                "days_offset" => 0,
                "days" => 5
            }]
        }
    end

    before(:each) do
        SafeCache.delete("stripe/plans_list")
        stub_client_params(controller)
        stub_current_user(controller, [:admin])
        create_default_plan
    end

    describe "GET index" do

        before(:each) do
             @coupon = Stripe::Coupon.create(
                :amount_off => 15000,
                :duration => 'forever',
                :currency => 'usd',
                :id => 'discount_150'
            )
        end

        it "should work" do
            get :index, params: {format: :json}
            expect(body_json["contents"]["cohorts"].size).to eq(Cohort.count)
            expect(body_json["contents"]["cohorts"][0]["name"]).not_to be_nil
        end

        it "should 401 if ability says cannot" do
            allow(controller.current_ability).to receive(:can?) do |key, obj|
                if key == :index_cohorts
                    expect(obj).to eq(controller.params)
                    false
                else
                    true
                end
            end
            get :index, params: { :format => :json }
            assert_error_response(401)
        end

        it "should use eager loading when loading working cohorts for admin" do
            ContentPublisher.update_all(published_at: Time.now) # this is not generally set in fixtures, so set it now
            all_cohorts = Cohort.all
            published_ats = all_cohorts.map(&:published_at).map(&:to_timestamp)
            preloaded_cohorts = controller.send(:preload_working_cohorts_for_index).to_a
            learner_projects = LearnerProject.all_by_id # warm the learner project cache

            # once the records are preloaded, there should be no more
            # queries required to convert them to json
            expect(ActiveRecord::Base.connection).not_to receive(:exec_query).and_call_original
            json = preloaded_cohorts.as_json(fields: ['ADMIN_FIELDS'])

            # setting up a spec for a bug we ran into when refactoring the preloading here.
            # With the bug, only one cohort would have a published_at
            published_ats_from_json = cohort_json = json.map { |entry| entry['published_at'] }
            expect(published_ats_from_json).to match_array(published_ats)
        end

        describe "caching" do
            it "should use cache when loading results for admin-facing endpoints" do
                controller.send(:get_cached_results) # warm cache with no json_opts
                expect(ActiveRecord::Base.connection).not_to receive(:exec_query).and_call_original
                controller.send(:get_cached_results)
            end

            it "should not use cache if something causes updated_at to change" do
                controller.send(:get_cached_results) # warm cache with no json_opts
                allow(Api::CohortsController).to receive(:max_updated_at_for_cache_key).and_return(Time.now)
                expect(ActiveRecord::Base.connection).to receive(:exec_query).at_least(1).times.and_call_original
                controller.send(:get_cached_results)
            end

            it "should not use cache if using different json_opts" do
                controller.send(:get_cached_results) # warm cache with no json_opts
                expect(ActiveRecord::Base.connection).to receive(:exec_query).at_least(1).times.and_call_original
                controller.send(:get_cached_results, {fields: ['ADMIN_FIELDS']})
            end
        end

        describe "cohort_promotions" do
            it "should include promoted cohorts in meta if indicated" do
                get :index, params: {get_cohort_promotions: true, format: :json}
                expect(body_json["meta"]["cohort_promotions"]).to match_array(CohortPromotion.includes(:cohort).all.map(&:as_json))
            end

            it "should not include promoted cohorts in meta if not indicated" do
                get :index, params: {format: :json}
                expect(body_json["meta"]["cohort_promotions"]).to be_nil
            end
        end

        describe "learner_projects" do

            it "should include learner_projects when get_learner_projects" do
                LearnerProject.create!(title: 'title', requirement_identifier: 'requirement_identifier')
                get :index, params: {get_learner_projects: true, format: :json}
                expect(body_json["meta"]["learner_projects"].map { |e| e['id']}).to match_array(LearnerProject.pluck(:id))
            end

        end

        describe "stripe data" do
            it "should include promoted cohorts in meta if indicated" do
                get :index, params: {get_stripe_metadata: true, format: :json}
                expect(body_json["meta"]["stripe_plans"]).to match_array([@default_plan.to_h.slice(:id, :amount).merge({ name: @default_plan[:nickname], frequency: "monthly" }).as_json])
                expect(body_json["meta"]["stripe_coupons"]).to match_array([{"id"=>"none", "amount_off"=>0, "percent_off"=>0}.as_json, @coupon.to_h.slice(:id, :name, :amount_off, :percent_off).as_json])
            end

            it "should not include promoted cohorts in meta if not indicated" do
                get :index, params: {format: :json}
                expect(body_json["meta"]["stripe_plans"]).to be_nil
                expect(body_json["meta"]["stripe_coupons"]).to be_nil
            end
        end

        describe "filters" do
            it "should filter by promoted = true" do
                mba_cohorts = Cohort.all_published.where(program_type: 'mba')
                promoted_cohort = Cohort.promoted_cohort(:mba)
                non_promoted_cohort = mba_cohorts.detect { |cohort| cohort['id'] != promoted_cohort['id'] }
                expect(mba_cohorts.size > 1).to be(true)

                get :index, params: { format: :json , filters: { published: true, promoted: true }.to_json }

                promoted_cohort_response = body_json["contents"]["cohorts"].detect { |c| c["id"] == promoted_cohort['id'] }
                non_promoted_cohort_response = body_json["contents"]["cohorts"].detect { |c| c["id"] == non_promoted_cohort['id'] }
                expect(promoted_cohort_response).not_to be_nil
                expect(non_promoted_cohort_response).to be_nil
            end

            it "should filter by published = true" do
                # get the working version of the current promoted cohort so we can change it's title
                published_cohort = Cohort.find(Cohort.promoted_cohort(:mba)['id'])
                published_cohort.update_attribute(:title, 'unpublished_change')
                unpublished_cohort = cohorts(:unpublished_cohort)
                get :index, params: { format: :json , filters: { published: true, promoted: true }.to_json }

                published_cohort_response = body_json["contents"]["cohorts"].detect { |c| c["id"] == published_cohort.id }
                unpublished_cohort_response = body_json["contents"]["cohorts"].detect { |c| c["id"] == unpublished_cohort.id }
                expect(published_cohort_response['title']).not_to eq('unpublished_change')
                expect(unpublished_cohort_response).to be_nil
            end

            it "should return working versions if not filtering by published and promoted" do
                cohort = Cohort.all_published.first
                cohort.update_attribute(:title, 'unpublished_change')
                get :index, params: { format: :json , filters: {}.to_json }

                cohort_response = body_json["contents"]["cohorts"].detect { |c| c["id"] == cohort.id }
                expect(cohort_response['title']).to eq('unpublished_change')

            end
        end
    end

    describe "POST create" do
        describe "with valid params" do
            it "creates a new Cohort" do
                expect {
                    post :create, params: {record: new_cohort_params, format: :json, meta: {}}
                }.to change(Cohort.all, :count).by(1)

                expect(response.status).to eq(200) # ok

                expect(body_json["contents"]["cohorts"].size).to eq(1)
                expect(body_json["contents"]["cohorts"][0]["name"]).to eq(valid_attributes["name"])
                expect(body_json["contents"]["cohorts"][0]["periods"].size).to eq(valid_attributes["periods"].size)
            end

            it "creates a new Cohort with groups" do
                groups = [AccessGroup.first, AccessGroup.second]
                create_attributes = new_cohort_params.merge({
                    groups: groups.as_json
                })

                expect {
                    post :create, params: {record: create_attributes, format: :json, meta: {}}
                }.to change(Cohort.all, :count).by(1)

                expect(response.status).to eq(200) # ok

                expect(body_json["contents"]["cohorts"].size).to eq(1)
                expect(body_json["contents"]["cohorts"][0]["name"]).to eq(valid_attributes["name"])
                expect(body_json["contents"]["cohorts"][0]["groups"]).to eq(groups.as_json)
            end

            it "prevents you from creating a new Cohort with a duplicate English name" do
                Cohort.create!(valid_attributes)
                expect {
                    post :create, params: {record: valid_attributes, format: :json}
                }.to change(Cohort.all, :count).by(0)

                expect(response.status).to eq(406) # not_acceptable
                expect(body_json["message"]).to eq("Validation failed: Cohort with this English name already exists.")

            end

            it "prevents you from creating a new Cohort if passed an existing ID" do
                existing_cohort = Cohort.create!(valid_attributes)

                expect {
                    post :create, params: {record: valid_attributes.merge(id: existing_cohort.id), format: :json}
                }.to change(Cohort.all, :count).by(0)

                expect(response.status).to eq(406) # not_acceptable
                expect(body_json["message"]).to eq("Validation failed: Cohort with this English name already exists.")
            end

        end
    end

    describe "PUT update" do
        before :each do
            @group1 = AccessGroup.create!(name: "CATS")
            @group2 = AccessGroup.create!(name: "DOGS")
        end

        describe "with valid params" do
            it "updates the requested cohort" do
                now = Time.now
                an_hour_ago = now - 1.hour
                allow(Time).to receive(:now).and_return(an_hour_ago)
                cohort = Cohort.create! valid_attributes

                allow(Time).to receive(:now).and_return(now)
                new_name = "Organization #{rand}"
                put :update, params: {record: {id: cohort.id, updated_at: cohort.updated_at.to_timestamp, "name" => new_name}, format: :json}
                expect(response.status).to eq(200), response.body # ok
                expect(body_json["contents"]["cohorts"].size).to eq(1)
                expect(body_json["contents"]["cohorts"][0]["created_at"]).to eq(cohort.created_at.to_timestamp)
                expect(body_json["contents"]["cohorts"][0]["updated_at"]).to eq(now.to_timestamp)

            end

            it "can add groups on cohort update" do
                now = Time.now
                an_hour_ago = now - 1.hour
                allow(Time).to receive(:now).and_return(an_hour_ago)
                cohort = Cohort.create! valid_attributes

                allow(Time).to receive(:now).and_return(now)
                new_name = "Organization #{rand}"
                put :update, params: {record: { id: cohort.to_param, updated_at: cohort.updated_at.to_timestamp, "name" => new_name, "groups" =>  [@group1.as_json]}, format: :json}
                expect(response.status).to eq(200), response.body # ok
                expect(body_json["contents"]["cohorts"].size).to eq(1)
                expect(body_json["contents"]["cohorts"][0]["created_at"]).to eq(cohort.created_at.to_timestamp)
                expect(body_json["contents"]["cohorts"][0]["updated_at"]).to eq(now.to_timestamp)

            end

        end

    end

    describe "DELETE destroy" do
        it "destroys the requested cohort" do
            cohort = Cohort.create! valid_attributes
            expect {
                delete :destroy, params: {id: cohort.to_param, format: :json}
            }.to change(Cohort, :count).by(-1)
            # response.status.should == 204 # no_content
            expect(response.status).to eq(200) # ok
        end

    end

    describe "POST upload_project_document" do

        it "should 401 if not admin" do
            stub_current_user(controller, [:learner])
            post :upload_project_document, params: {file: {}, format: :json, meta: {}}
            assert_error_response(401)
        end

        it "creates a new project document" do
            file = fixture_file_upload('files/test.png', 'image/png')
            new_document = S3ProjectDocument.new(file: file)

            expect(S3ProjectDocument).to receive(:new).and_return(new_document)

            post :upload_project_document, params: {file: {}, format: :json, meta: {}}
            expect(response.status).to eq(200) # ok
            expect(body_json["contents"]["project_document"]["id"]).to eq(new_document.id)
            expect(body_json["contents"]["project_document"]["file_file_name"]).to eq(new_document.file_file_name)
            expect(body_json["contents"]["project_document"]["url"]).to eq(new_document.as_json[:url])
        end

        it "uses an existing project document if it exists" do
            file = fixture_file_upload('files/test.png', 'image/png')
            existing_document = S3ProjectDocument.create!(file: file)

            expect(S3ProjectDocument).to receive(:new).and_return(existing_document)
            expect(S3ProjectDocument).not_to receive(:save!)

            post :upload_project_document, params: {file: {}, format: :json, meta: {}}
            expect(response.status).to eq(200) # ok
            expect(body_json["contents"]["project_document"]["id"]).to eq(existing_document.id)
            expect(body_json["contents"]["project_document"]["file_file_name"]).to eq(existing_document.file_file_name)
            expect(body_json["contents"]["project_document"]["url"]).to eq(existing_document.as_json[:url])
        end
    end

    describe "POST upload_exercise_document" do
        it "should 401 if not admin" do
            stub_current_user(controller, [:learner])
            post :upload_exercise_document, params: {file: {}, format: :json, meta: {}}
            assert_error_response(401)
        end

        it "creates a new exercise document" do
            file = fixture_file_upload('files/test.png', 'image/png')
            new_document = S3ExerciseDocument.new(file: file)

            expect(S3ExerciseDocument).to receive(:new).and_return(new_document)
            expect(new_document).to receive(:save!)

            post :upload_exercise_document, params: {file: {}, format: :json, meta: {}}
            expect(response.status).to eq(200) # ok
            expect(body_json["contents"]["exercise_document"]["id"]).to eq(new_document.id)
            expect(body_json["contents"]["exercise_document"]["file_file_name"]).to eq(new_document.file_file_name)
            expect(body_json["contents"]["exercise_document"]["url"]).to eq(new_document.as_json[:url])
        end

        it "uses an existing exercise document if it exists" do
            file = fixture_file_upload('files/test.png', 'image/png')
            new_document = S3ExerciseDocument.new(file: file)
            existing_document = S3ExerciseDocument.new(file: file)

            expect(S3ExerciseDocument).to receive(:new).and_return(new_document)
            expect(S3ExerciseDocument).to receive(:find_by_file_fingerprint).and_return(existing_document)
            expect(new_document).not_to receive(:save!)
            expect(existing_document).to receive(:save!)

            post :upload_exercise_document, params: {file: {}, format: :json, meta: {}}
            expect(response.status).to eq(200) # ok
            expect(body_json["contents"]["exercise_document"]["id"]).to eq(existing_document.id)
            expect(body_json["contents"]["exercise_document"]["file_file_name"]).to eq(existing_document.file_file_name)
            expect(body_json["contents"]["exercise_document"]["url"]).to eq(existing_document.as_json[:url])
        end
    end

end
