require 'spec_helper'
require 'controllers/api/helpers/api_content_controller_spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::LearnerProjectsController do

    include ControllerSpecHelper
    json_endpoint

    fixtures :cohorts

    before(:each) do
        @user = User.first
    end

    describe "GET index" do

        it "should check permissions and work" do
            cohort = cohorts(:published_mba_cohort)
            project = LearnerProject.find(cohort.all_learner_project_ids).first
            LearnerProject.create!(title: 'some project', requirement_identifier: 'requirement_identifier')
            allow(controller).to receive(:current_user).and_return(@user)
            expect(controller.current_ability).to receive(:can?).with(:index, LearnerProject).and_return(true)
            expect(controller.current_ability).to receive(:can?).with(:manage, LearnerProject).and_return(true)
            get :index, params: {format: :json}
            expect(body_json["contents"]["learner_projects"].map { |e| e['id']}).to eq(LearnerProject.pluck(:id))
            expect(body_json["contents"]["learner_projects"][0]["title"]).not_to be_nil

            # since the related_cohort stuff requires that the cohorts be passed into the json options,
            # we test it here
            expect(body_json["contents"]["learner_projects"].detect { |e| e['id'] == project.id}['related_cohorts']).not_to be_empty
        end
    end

    describe "POST create" do
        it "creates a new LearnerProject" do
            allow(controller).to receive(:current_user).and_return(@user)
            expect(controller.current_ability).to receive(:can?).with(:create, LearnerProject).and_return(true)
            expect {
                post :create, params: {record: valid_attrs, format: :json, meta: {}}
            }.to change(LearnerProject, :count).by(1)

            expect(response.status).to eq(200) # ok

            expect(body_json["contents"]["learner_projects"].size).to eq(1)
            expect(body_json["contents"]["learner_projects"][0]["title"]).to eq(valid_attrs["title"])
        end
    end

    describe "PUT update" do

        it "updates the requested project" do
            allow(controller).to receive(:current_user).and_return(@user)
            expect(controller.current_ability).to receive(:can?).with(:update, LearnerProject).and_return(true)

            project = LearnerProject.create! valid_attrs
            new_title = "changed"
            put :update, params: {record: {id: project.id, updated_at: project.updated_at.to_timestamp, "title" => new_title}, format: :json}
            expect(response.status).to eq(200), response.body # ok
            expect(body_json["contents"]["learner_projects"].size).to eq(1)
            expect(body_json["contents"]["learner_projects"][0]["id"]).to eq(project.id)
            expect(body_json["contents"]["learner_projects"][0]["title"]).to eq(new_title)
            expect(project.reload.updated_at).to be > project.created_at

        end

    end

    describe "DELETE destroy" do
        it "destroys the requested cohort" do
            allow(controller).to receive(:current_user).and_return(@user)
            expect(controller.current_ability).to receive(:can?).with(:destroy, LearnerProject).at_least(1).times.and_return(true)

            project = LearnerProject.create! valid_attrs
            expect {
                delete :destroy, params: {id: project.to_param, format: :json}
            }.to change(LearnerProject, :count).by(-1)
            expect(response.status).to eq(200) # ok
        end

    end

    def valid_attrs(attrs = {})
        {
            title: 'some project',
            internal_notes: 'some notes',
            requirement_identifier: 'requirement_identifier'
        }.with_indifferent_access.merge(attrs)
    end
end