require 'controllers/api/helpers/api_content_controller_spec_helper'


describe Api::LessonController do

    include ApiContentControllerSpecHelper

    describe "with stubbed content" do

        before(:each) do
            fake_content_klass = Class.new(Lesson::Content) do
                def self.name; 'FakeContentKlass'; end
            end
            allow(Lesson::Content).to receive(:get_klass_for_type) { fake_content_klass }

            @lessons = Lesson.all
        end

        add_destroy_specs
        add_images_specs

        add_create_specs({
            :valid => { 'lesson_type' => 'a', 'title' => 'b'},
            :invalid => {'lesson_type' => nil}  # invalid because no lesson_type
        })

        add_update_specs({
            :valid => Proc.new { |instance_json|
                instance_json.merge({
                    'lesson_type' => 'Some other type',
                    'seo_title' => 'b',
                    'title' => 'b'
                })
            },
            :invalid => Proc.new { |instance_json|
                instance_json.merge('lesson_type' => nil) # invalid because no lesson_type
            }
        })


    end

    describe "index" do

        before(:each) do
            @lessons = Lesson.all
            stub_current_user(controller, [:super_editor])
            @lessons[0].update!(:archived => true)
        end

        it "should raise if not editor" do
            stub_current_user(controller, [:learner])
            get :index, params: { :format => :json }
            assert_error_response(401, {'message' => "You are not authorized to do that."})
        end

        it "should respect archived=false" do
            get :index, params: { :filters => {:published => false, :archived => false, }.to_json, :format => :json }
            assert_200
            ids = body_json['contents']['lessons'].map {|l| l['id']}
            expect(ids).to match_array(Lesson.where(archived: false).pluck('id'))
        end

        it "should respect archived=true" do
            get :index, params: { :filters => {:published => false, :archived => true, }.to_json, :format => :json }
            assert_200
            ids = body_json['contents']['lessons'].map {|l| l['id']}
            expect(ids).to match_array(Lesson.where(archived: true).pluck('id'))
        end

        def expected_lesson_json_list_for(lessons)
            lessons.sort_by { |i| i.title.downcase }.map do |lesson|
                lesson.as_json(user_id: controller.current_user.id).except("published_at")
            end
        end
    end

    describe "show" do

        it "should load up a specific version" do
            stub_current_user(controller, [:super_editor])
            lesson = Lesson.first
            lesson.title = "version 1"
            lesson.save!
            lesson.title = "version 2"
            lesson.save!

            version_1 = lesson.versions.find_by_title('version 1')

            get :show, params: { :id => lesson.id, :filters => {:version_id => version_1.version_id, :published => false}.to_json, :format => :json }
            assert_200
            result_json = body_json['contents']['lessons'][0]

            # there are other things we expect in here, but it is tested in more
            # detail in lesson_spec.rb.  This is enopugh to be sure that in general
            # the right stuff is passed through
            expect(result_json['title']).to eq('version 1')
            expect(result_json['old_version']).to be_truthy

            expect(result_json['entity_metadata']['title']).to eq(lesson.entity_metadata.title)
            expect(result_json['entity_metadata']['description']).to eq(lesson.entity_metadata.description)
            expect(result_json['entity_metadata']['canonical_url']).to eq(lesson.entity_metadata.canonical_url)
        end

        it "should raise if non-editor trying to load up a specific version" do
            stub_current_user(controller, [:learner])

            get :show, params: { :id => SecureRandom.uuid, :filters => {:version_id => SecureRandom.uuid }.to_json, :format => :json }
            assert_error_response(401, {'message' => "You are not authorized to do that."})
        end

        it "should call as_json only once" do
            stub_current_user(controller, [:super_editor])
            lesson = Lesson.first
            allow(Lesson).to receive(:as_json).and_call_original.exactly(1).times
            get :show, params: { :id => lesson.id, :filters => {:published => false}.to_json, :format => :json }
            assert_200
        end

        it "should be possible to load a standalone lesson" do
            lesson = Lesson.first
            lesson.publish! # only published versions can be viewed standalone
            stub_no_current_user(controller)
            allow_any_instance_of(Lesson).to receive(:allow_unrestricted_access?).and_return(true)
            get :show, params: { :id => lesson.id, :format => :json }
            assert_200
        end

        it "should be possible to load a standalone lesson with a user" do
            lesson_progress = LessonProgress.first
            lesson = lesson_progress.lesson
            lesson.publish! # only published versions can be viewed standalone
            allow(controller).to receive(:current_user).and_return(lesson_progress.user)

            allow_any_instance_of(Lesson).to receive(:allow_unrestricted_access?).and_return(true)
            get :show, params: { :id => lesson.id, :format => :json }
            assert_200
            expect(body_json['contents']['lessons'][0]['lesson_progress']['id']).to eq(lesson_progress.id)
        end

        it "should 404 if instance cannot be found" do
            id  = SecureRandom.uuid
            get :show, params: {:id => id, :format => :json}
            assert_error_response(404, {'message' => "No #{controller.class.model.table_name.singularize} found for id=\"#{id}\""})
        end

        it "should 404 if instance cannot be found with include_progress=true" do
            id = SecureRandom.uuid
            get :show, params: {:id => id, :format => :json}
            assert_error_response(404, {'message' => "No #{controller.class.model.table_name.singularize} found for id=\"#{id}\""})
        end

        it "should raise if learner trying to view unpublished content" do
            instance = Lesson.first
            allow_any_instance_of(Lesson).to receive(:allow_unrestricted_access?).and_return(true)
            stub_current_user(controller, [:learner])
            get :show, params: { :id => instance.id, :filters => {:published => false}.to_json, :format => :json }
            assert_error_response(401, {'message' => "You are not authorized to do that."})
        end

    end

    # we had a crazy bug that only applied to the to_json
    # method in frame.rb.  Adding this test for verification
    describe "with frame list" do

        describe "PUT create" do

            before(:each) do
                lesson = Lesson.where(lesson_type: 'frame_list').first
                @valid_attrs = lesson.content_json.merge({
                    'title' => 'New Lesson',
                    'lesson_type' => 'frame_list',
                    'frame_count' => lesson.frame_count
                })
                @lesson_ids = Lesson.pluck('id')
            end

            it "should create a lesson" do
                params = @valid_attrs
                stub_current_user(controller, [:editor])
                post :create, params: {'record' => params, :format => :json}
                assert_200
                reloaded = Lesson.where("id NOT IN (?)", @lesson_ids).first
                expect(reloaded).not_to be_nil
            end

            it "should create a pinned lesson" do
                params = @valid_attrs
                stub_current_user(controller, [:editor])
                post :create, params: {
                    'record' => params,
                    'meta' => {'should_pin' => true, 'pinned_title' => 'title'},
                    :format => :json}
                assert_200

                reloaded = Lesson.where("id NOT IN (?)", @lesson_ids).first
                expect(reloaded).not_to be_nil
                expect(reloaded.pinned).to be(true)
                expect(reloaded.pinned_title).to eq('title')
            end

        end

    end

    describe "update" do
        it "should support pinning" do
            stub_current_user(controller, [:admin], ["GROUP"])
            instance = Lesson.first

            put :update, params: {
                'record' => instance.as_json,
                'meta' => {
                    'should_pin' => true,
                    'pinned_title' => 'title',
                    'pinned_description' => 'description'
                },
                :format => :json}
            assert_200
            instance = instance.reload
            expect(instance.pinned).to be(true)
            expect(instance.pinned_title).to eq('title')
            expect(instance.pinned_description).to eq('description')
        end

        it "should show save warnings" do
            stub_current_user(controller, [:admin], ["GROUP"])
            instance = Lesson.first
            warnings = ['A warning']
            expect_any_instance_of(Lesson).to receive(:save_warnings).and_return(warnings)

            put :update, params: {
                'record' => instance.as_json,
                :format => :json}

            assert_200
            expect(body_json['meta']['save_warnings']).to eq(warnings)
        end

        # In the wild, only automated scripts try to save old versions.  There
        # is no way to do it from the editor UI
        it "should 401 if non-admin trying to save old version" do
            stub_current_user(controller, [:super_editor])
            instance = Lesson.first

            put :update, params: {
                'record' => instance.as_json,
                'meta' => {
                    update_version: SecureRandom.uuid
                },
                :format => :json}

            assert_error_response(401, {'message' => "You are not authorized to do that."})
        end

        it "should allow admin to save old version" do
            stub_current_user(controller, [:admin])
            instance = Lesson.first

            meta = {
                update_version: SecureRandom.uuid
            }
            expect(Lesson).to receive(:update_from_hash!).with(controller.current_user, ActionController::Parameters.new(instance.as_json), ActionController::Parameters.new(meta)).and_return(instance)

            put :update, params: {
                'record' => instance.as_json,
                'meta' => meta,
                :format => :json}

            assert_200
        end
    end

    describe "with cannot_create override role" do

        describe "PUT create" do

            it "should not allow creating if lesson creation permissions have been removed" do
                stub_current_user(controller, [:editor])

                # add the cannot_create role and check that create sends back a permissions error
                controller.current_user.add_role "cannot_create"
                allow_any_instance_of(Lesson::Content::FrameList::Frame).to receive(:valid?).and_return(true)
                params = {
                    'title' => 'New Lesson',
                    'lesson_type' => 'frame_list',
                    'frames' => [{}]
                }
                post :create, params: {'record' => params, :format => :json}
                expect(response.code).to eq("401") # not authorized

            end


        end

    end

    describe "lesson editing overrides" do

        describe "editors need specific access to lessons" do
            before(:each) do
                @instance = lessons(:unpublished_lesson)
                expect(@instance.has_published_version?).to be(false) # sanity check
            end

            [:lesson_editor, :previewer, :reviewer].each do |role|
                it "editor can show unpublished content if published=false only if granted" do
                    stub_current_user(controller, [:editor], ["GROUP"])

                    get :show, params: {:format => :json, :filters => {published: false}.to_json, :id => @instance.id}
                    expect(response.status).to eq(401)

                    controller.current_user.add_role role, @instance
                    get :show, params: {:format => :json, :filters => {published: false}.to_json, :id => @instance.id}
                    assert_returned_instance
                end
            end

            it "editor can index unpublished content without error even if no lessons granted" do
                stub_current_user(controller, [:editor], ["GROUP"])
                get :index, params: {:format => :json, :filters => {published: false}.to_json}
                expect(response.status).to eq(200)
            end

            it "lesson_editor can view lessons he/she authored" do
                @instance = lesson = Lesson.first
                author = lesson.author
                author.roles = []
                author.save!
                author.add_role(:editor)
                author.save!
                allow(controller).to receive(:current_user).and_return(author)
                get :index, params: {:format => :json, :filters => {published: false}.to_json}
                expect(response.status).to eq(200)
                assert_returned_instance(false)
            end

            # reviewer can save lessons but will only be presented with the option to edit the comments field in the frontend
            it "reviewer can save lessons" do
                stub_current_user(controller, [:editor], ["GROUP"])
                controller.current_user.add_role :reviewer, @instance

                params =  @instance.as_json(:user_id => controller.current_user.id)
                put :update, params: {'record' => params, :format => :json}
                expect(response.status).to eq(200)
            end

            it "previewer cannot save lessons" do
                stub_current_user(controller, [:editor], ["GROUP"])
                controller.current_user.roles = []
                controller.current_user.add_role :previewer, @instance
                params =  @instance.as_json(:user_id => controller.current_user.id)
                put :update, params: {'record' => params, :format => :json}
                expect(response.status).to eq(401)
            end
        end
        def assert_returned_instance(ensure_just_one = true)
            expect(response.status).to eq(200), response.body
            returned_instances = body_json["contents"][controller.class.model.table_name]
            expect(returned_instances).not_to be(nil), "No lessons returned from api call"
            expect(returned_instances.size).to be(1), "Expected one lesson, got #{returned_instances.size}" if ensure_just_one
            returned_instance = returned_instances.map(&:with_indifferent_access).detect { |e| e['id'] == @instance[:id] }
            expect(returned_instance).not_to be_nil
            returned_instance
        end
    end

end