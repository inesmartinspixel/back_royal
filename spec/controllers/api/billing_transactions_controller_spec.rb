require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::BillingTransactionsController do
    include ControllerSpecHelper
    json_endpoint

    fixtures(:users)

    before(:each) do
        stub_client_params(controller)
    end

    describe "GET index" do

        it "should 401 for non-admin" do
            stub_current_user(controller, [:learner])
            get :index, params: { format: :json }
            assert_error_response(401)
        end

        it "should return billing_transactions" do
            owner = ensure_billing_transaction.user
            stub_current_user(controller, [:admin])
            get :index, params: { format: :json, owner_id: owner.id }
            assert_200
            expect(body_json['contents']['billing_transactions'].map { |e| e['id']}).to match_array(BillingTransaction.pluck('id'))
        end

    end

    describe "GET show" do

        it "should 401 for non-admin" do
            stub_current_user(controller, [:learner])
            get :show, params: { format: :json, id: SecureRandom.uuid }
            assert_error_response(401)
        end

        it "should return a billing_transaction" do
            billing_transaction = ensure_billing_transaction
            stub_current_user(controller, [:admin])
            get :show, params: { format: :json, id: billing_transaction.id }
            assert_200
            expect(body_json['contents']['billing_transactions'].map { |e| e['id']}).to eq([billing_transaction.id])
        end

    end

    describe "POST create" do

        it "should 401 for non-admin" do
            stub_current_user(controller, [:learner])
            post :create, params: { format: :json }
            assert_error_response(401)
        end

        it "should 406 if the owner is not found" do

            allow(controller).to receive(:current_user).and_return(users(:admin))
            expect(BillingTransaction).not_to receive(:create_from_hash!)

            post :create,
                params: {
                    :format => :json,
                    :record => valid_attrs,
                    :meta => {owner_id: SecureRandom.uuid}
                }
            assert_error_response(406)
        end

        it "should work" do
            allow(controller).to receive(:current_user).and_return(users(:admin))
            owner = User.where.not(id: controller.current_user.id).first
            expect(BillingTransaction).to receive(:create_from_hash!).with(owner, hash_including(valid_attrs)).and_return(BillingTransaction.new)

            post :create,
                params: {
                    :format => :json,
                    :record => valid_attrs,
                    :meta => {owner_id: owner.id}
                }
            assert_200
        end

    end

    describe "PUT update" do

        it "should 401 for non-admin" do
            stub_current_user(controller, [:learner])
            put :update, params: { format: :json }
            assert_error_response(401)
        end

        it "should work in normal case" do
            allow(controller).to receive(:current_user).and_return(users(:admin))
            expect(BillingTransaction).to receive(:update_from_hash!).with(hash_including(valid_attrs)).and_return(BillingTransaction.new)

            put :update,
                params: {
                    :format => :json,
                    :record => valid_attrs
                }
            assert_200
        end

    end

    describe "DELETE destroy" do

        attr_accessor :billing_transaction

        before(:each) do
            self.billing_transaction = BillingTransaction.create!(valid_attrs)
        end

        it "should 401 for non-admin" do
            stub_current_user(controller, [:learner])
            delete :destroy, params: { id: billing_transaction.id, format: :json }
            assert_error_response(401)
        end

        it "should work" do
            stub_current_user(controller, [:admin])
            expect {
                delete :destroy, params: { id: billing_transaction.id, format: :json }
            }.to change(BillingTransaction, :count).by(-1)
            assert_200
        end

    end

    def valid_attrs
        {
            transaction_type: 'payment',
            transaction_time: Time.parse('2017-06-09').to_timestamp,
            provider: BillingTransaction::PROVIDER_PAYPAL,
            provider_transaction_id: '875676453',
            amount: 3600,
            amount_refunded: 1700,
            description: 'blah blah',
            currency: 'usd'
        }
    end

    def ensure_billing_transaction
        owner = User.first
        billing_transaction = BillingTransaction.create!(valid_attrs)
        owner.billing_transactions << billing_transaction
        billing_transaction.reload
    end

end
