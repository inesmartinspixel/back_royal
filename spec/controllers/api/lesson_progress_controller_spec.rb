require 'controllers/api/helpers/api_content_controller_spec_helper'

describe Api::LessonProgressController do
    attr_reader :user, :lesson, :stream, :stream_lesson
    include ControllerSpecHelper
    include ApiContentControllerSpecHelper

    fixtures(:lesson_streams, :lessons)

    before(:each) do
        stub_client_params(controller)
        @user = User.left_outer_joins(:lesson_streams_progresses).where("lesson_streams_progress.id is null").first
        @stream = Lesson::Stream.all_published.first
        @lesson = lessons(:published_lesson_without_stream)
        @stream_lesson = @stream.lessons.detect(&:has_published_version?)
    end

    describe "basic progress tracking" do

        describe "create_or_update" do

            # all the other specs mock out all the create_or_update methods.
            # This is one spec that actually calls all the way through to see
            # that the saves actually happen
            it "should work when integrated with live create_or_update calls" do
                user.lesson_progresses.delete_all

                Lesson::StreamProgress.create!(
                    :created_at => Time.now - 10.minutes,
                    :updated_at => Time.now - 10.minutes,
                    :started_at => Time.now - 10.minutes,
                    :user_id => user.id,
                    :locale_pack_id => stream.locale_pack_id
                )
                expect {
                expect {
                    call_create_api(stream_lesson, stream)
                }.to change { LessonProgress.maximum(:updated_at)}
                }.to change { Lesson::StreamProgress.maximum(:updated_at)}

                progress_max_updated_at_before_changes = body_json['meta']['push_messages']['progress_max_updated_at_before_changes']
                progress_max_updated_at = body_json['meta']['push_messages']['progress_max_updated_at']
                expect(progress_max_updated_at > progress_max_updated_at_before_changes).to be(true)

            end

            it "should update lesson progress" do
                mock_create_or_update_and_call_create_api(lesson)
                assert_200
            end

            # FIXME: cleanup. See https://trello.com/c/mf1NPdyO
            it "should update stream progress using deprecated lesson_streams_progress property" do
                mock_create_or_update_and_call_create_api(stream_lesson, stream, use_deprecated_metadata:true)
                assert_200
            end

            it "should update stream progress" do
                mock_create_or_update_and_call_create_api(stream_lesson, stream)
                assert_200
            end

            it "should update multiple stream progress records" do
                allow(controller).to receive(:raise_unless_lesson_is_in_stream) # ignore this for this spec to avoid complex setup
                another_stream = Lesson::Stream.all_published.where.not(locale_pack_id: stream.locale_pack_id).first
                mock_create_or_update_and_call_create_api(stream_lesson, [another_stream, stream])
                assert_200
            end

            it "should send back favorite lesson stream locale pack info" do
                @user = User.joins(:favorite_lesson_stream_locale_packs).first

                # we only return favorite_lesson_stream_locale_packs when the
                # stream progress is first created, so we need the returned
                # stream_progress to have a created_at after now so it looks new
                stream_progress = Lesson::StreamProgress.new(
                    :created_at => Time.now + 10.minutes,
                    :user_id => user.id,
                    :locale_pack_id => stream.locale_pack_id
                )
                mock_create_or_update_and_call_create_api(stream_lesson, stream, stream_progress:stream_progress)
                assert_200

                locale_packs = controller.current_user.favorite_lesson_stream_locale_packs
                expect(locale_packs).not_to be_empty
                expected_locale_packs_json = locale_packs.map { |pack| {'id' => pack.id} }
                expect(body_json['meta']['favorite_lesson_stream_locale_packs']).not_to be_nil
                expect(body_json['meta']['favorite_lesson_stream_locale_packs']).to match_array(expected_locale_packs_json)
            end

            it "should not load favorite_lesson_stream_locale_packs if not creating a new stream progress" do
                # return an existing stream_progress from Lesson::StreamProgress.create_or_update!
                stream_progress = Lesson::StreamProgress.new(
                    :created_at => Time.now - 10.minutes,
                    :user_id => user.id,
                    :locale_pack_id => stream.locale_pack_id
                )

                expect(user).not_to receive(:favorite_lesson_stream_locale_packs)
                mock_create_or_update_and_call_create_api(lesson, stream, stream_progress:stream_progress)
            end

            # this is necessary to make sure that we're checking the launch window.  Otherwise a user
            # could update a lesson in an exam outside of the allowed launch window
            it "should error if trying to save a lesson that is in an exam stream without also including the expected stream_progress" do
                stream = lesson_streams(:exam_stream).published_version
                lesson = stream.lessons.detect(&:has_published_version?)

                # we want to log this, since it is not expected
                expect(Raven).to receive(:capture_exception)
                mock_create_or_update_and_call_create_api(lesson)
                assert_error_response(401)
            end

            # this is important because of the checking of the launch window on exam streams
            it "should error if the stream progress is for a stream that does not include this lesson" do
                stream = Lesson::Stream.all_published.first
                lesson = Lesson.all_published.where.not(id: stream.lessons.map(&:id)).first

                # we want to log this, since it is not expected
                expect(Raven).to receive(:capture_exception)
                mock_create_or_update_and_call_create_api(lesson, stream)
                assert_error_response(401)
            end

            # just a check that we're only doing this check on lessons in exam streams.  Other lessons should
            # be savable when launched in the standalone lesson player
            it "should not error if trying to save a lesson that is not in an exam stream without also including a stream_progress" do
                lesson = lesson_streams(:non_exam_stream).lessons.detect(&:has_published_version?)
                mock_create_or_update_and_call_create_api(lesson)
                assert_response(200)
            end

            it "should unset waiver if all lessons are completed, the stream is completed, and there is a waiver" do
                expect(LessonProgress).to receive(:create_or_update!).with(hash_including({
                    :user_id => user.id,
                    :locale_pack_id => stream_lesson.locale_pack_id
                })).and_return(LessonProgress.new(
                    :user_id => user.id,
                    :locale_pack_id => stream_lesson.locale_pack_id
                ))

                stream_progress ||= Lesson::StreamProgress.new(
                    :created_at => Time.now,
                    :completed_at => Time.now,
                    :user_id => user.id,
                    :locale_pack_id => stream.locale_pack_id,
                    :waiver => Lesson::StreamProgress::WAIVER_EXAM_ALREADY_COMPLETED
                )
                expect(Lesson::StreamProgress).to receive(:create_or_update!)
                    .with(hash_including(
                        :lesson_bookmark_id => stream_lesson.id,
                        :locale_pack_id => stream.locale_pack_id
                    ), user)
                    .and_return(stream_progress)
                expect_any_instance_of(Lesson::StreamProgress).to receive(:all_lessons_complete?).and_return(true)

                allow(controller).to receive(:current_user).and_return(user)
                lesson_progress_params = {
                    :locale_pack_id => stream_lesson.locale_pack_id,
                }
                lesson_progress_params[:complete] = true

                meta = {
                    :lesson_streams_progress => {
                        :lesson_bookmark_id => stream_lesson.id,
                        :locale_pack_id => stream.locale_pack_id
                    }
                }

                expect(stream_progress.waiver).to eq(Lesson::StreamProgress::WAIVER_EXAM_ALREADY_COMPLETED)
                post :create, params: {
                    'record' => lesson_progress_params,
                    'meta' => meta,
                    :format => :json}
                expect(stream_progress.waiver).to be_nil
            end
        end

        describe "record_params" do
            it "should handle frame_history" do
                controller.params = {
                    record: {
                        frame_history: [
                            'valid',
                            {invalid: 'value'} # invalid value should be filtered out
                        ]
                    }
                }
                expect(controller.record_params[:frame_history]).to eq(['valid'])
            end

            it "should handle completed_frames" do
                controller.params = {
                    record: {
                        completed_frames: {
                            valid: 'invalid', # invalid value should be changed to true
                            also_valid: true
                        }
                    }
                }
                expect(controller.record_params[:completed_frames]).to eq({
                    'also_valid' => true,
                    'valid' => true
                })
            end

            it "should handle challenge_scores" do
                controller.params = {
                    record: {
                        challenge_scores: {
                            'valid' => 0.4, # this is valid
                            'invalid1' => -0.01, # invalid value should be filterd out
                            'invalid2' => 1.01, # invalid value should be filterd out
                            'invalid3' => 'asdfdaf', # invalid value should be filterd out
                            'invalid4' => {a: 1} # invalid value should be filterd out
                        }
                    }
                }
                expect(controller.record_params[:challenge_scores]).to eq({
                    'valid' => 0.4
                })
            end

            it "should handle frame_durations" do
                controller.params = {
                    record: {
                        frame_durations: {
                            'valid' => 0.4, # this is valid
                            'invalid' => 'asdfdaf' # invalid value should be filterd out
                        }
                    }
                }
                expect(controller.record_params[:frame_durations]).to eq({
                    'valid' => 0.4
                })
            end
        end

        # see https://trello.com/c/3B31JBXA
        describe "stream progress auto-completion" do
            attr_accessor :lesson, :stream, :other_lessons, :stream_progress

            before(:each) do
                self.stream = lesson_streams(:published_stream)
                published_lessons = stream.lessons.all_published
                expect(published_lessons.size).to be > 1

                self.lesson =published_lessons.first
                self.other_lessons = published_lessons - [lesson]

                self.stream_progress = Lesson::StreamProgress.create!(
                    locale_pack_id: stream.locale_pack_id,
                    user_id: user.id,
                    started_at: Time.now
                )
            end

            it "should mark stream progress as complete if all lessons are complete" do
                # complete all the other lessons except for _lesson.  Once we've done
                # that, when we complete the lesson, the stream should get marked as
                # complete
                lesson_progresses = stream.lessons.map do |_lesson|
                    LessonProgress.create!(
                        user_id: user.id,
                        completed_at: other_lessons.include?(_lesson) ? Time.now : nil,
                        locale_pack_id: _lesson.locale_pack_id,
                        started_at: Time.now
                    )
                end
                expect(self.stream_progress.incomplete_lesson_count).to eq(1) # sanity check

                lesson_progress = lesson_progresses.detect { |d| d.locale_pack_id == lesson.locale_pack_id}

                call_create_api(lesson, stream, true)
                assert_200

                expect(user.lesson_streams_progresses.where(locale_pack_id: stream.locale_pack_id).first&.completed_at).not_to be_nil
            end

            it "should not mark stream progress as complete if not all lessons are complete" do
                lesson_progresses = stream.lessons.map do |_lesson|
                    LessonProgress.create!(
                        user_id: user.id,
                        completed_at: nil,
                        locale_pack_id: _lesson.locale_pack_id,
                        started_at: Time.now
                    )
                end

                lesson_progress = lesson_progresses.detect { |d| d.locale_pack_id == lesson.locale_pack_id}

                expect(Raven).not_to receive(:capture_exception)
                call_create_api(lesson, stream)
                assert_200

                expect(user.lesson_streams_progresses.where(locale_pack_id: stream.locale_pack_id).first&.completed_at).to be_nil
            end
        end

        def mock_create_or_update_and_call_create_api(lesson, stream_or_streams = nil, stream_progress: nil, use_deprecated_metadata: false)
            expect(LessonProgress).to receive(:create_or_update!).with(hash_including({
                :user_id => user.id,
                :locale_pack_id => lesson.locale_pack_id
            })).and_return(LessonProgress.new(
                :user_id => user.id,
                :locale_pack_id => lesson.locale_pack_id
            ))

            if stream_or_streams
                streams = stream_or_streams.is_a?(Array) ? stream_or_streams : [stream_or_streams]
                streams.each do |stream|
                    stream_progress ||= Lesson::StreamProgress.new(
                        :created_at => Time.now,
                        :user_id => user.id,
                        :locale_pack_id => stream.locale_pack_id
                    )
                    expect(Lesson::StreamProgress).to receive(:create_or_update!)
                        .with(hash_including(
                            :lesson_bookmark_id => lesson.id,
                            :locale_pack_id => stream.locale_pack_id
                        ), user)
                        .and_return(stream_progress)
                end
            end

            call_create_api(lesson, stream_or_streams, false, use_deprecated_metadata)
        end

        def call_create_api(lesson, stream_or_streams = nil, complete = false, use_deprecated_metadata = false)
            allow(controller).to receive(:current_user).and_return(user)
            lesson_progress_params = {
                :locale_pack_id => lesson.locale_pack_id,
            }
            lesson_progress_params[:complete] = true if complete

            if stream_or_streams && use_deprecated_metadata
                meta = {
                    :lesson_streams_progress => {
                        :lesson_bookmark_id => lesson.id,
                        :locale_pack_id => stream_or_streams.locale_pack_id
                    }
                }
            elsif stream_or_streams
                streams = stream_or_streams.is_a?(Array) ? stream_or_streams : [stream_or_streams]
                meta = {
                    :stream_progress_records => streams.map { |stream|
                        {
                            :lesson_bookmark_id => lesson.id,
                            :locale_pack_id => stream.locale_pack_id
                        }
                    }
                }
            else
                meta = {}
                expect(Lesson::StreamProgress).not_to receive(:create_or_update!)
            end


            post :create, params: {
                'record' => lesson_progress_params,
                'meta' => meta,
                :format => :json}
        end
    end

end
