require 'spec_helper'

describe Api::UnloadedChangeDetector do
    attr_reader :controller

    fixtures(:users)

    class MockController
        include Api::UnloadedChangeDetector
        attr_accessor :meta

        def initialize
            self.meta = {}
        end

    end

    before(:each) do
        self.controller = MockController.new
    end

    describe "hiring_relationship" do

        # FIXME: removed as part of https://trello.com/c/oyQYlWv6
        # it "should work for candidate" do
        #     candidate = HiringRelationship.first.candidate
        #     allow(controller).to receive(:current_user).and_return(candidate)

        #     assert_last_updated_at(:hiring_relationship, candidate.hiring_manager_relationships.maximum(:updated_at).to_f)
        # end

        it "should work for hiring_manager on a team" do
            hiring_manager = users(:hiring_manager_with_team)
            teammate = users(:hiring_manager_teammate)
            candidate = users(:learner)
            hiring_manager.ensure_candidate_relationships([candidate.id])
            teammate.ensure_candidate_relationships([candidate.id])

            my_relationship = hiring_manager.candidate_relationships.first
            teammate_relationship = teammate.candidate_relationships.first

            my_relationship.update_attribute(:updated_at,  Time.now + 1.minute)
            allow(controller).to receive(:current_user).and_return(hiring_manager)

            assert_last_updated_at(:hiring_relationship, my_relationship.reload.updated_at.to_f)

            teammate_relationship.update_attribute(:updated_at,  Time.now + 2.minutes)
            assert_last_updated_at(:hiring_relationship, teammate_relationship.reload.updated_at.to_f)
        end

    end

    describe "open_position" do

        it "should work" do
            hiring_manager = users(:hiring_manager_with_team)

            allow(controller).to receive(:current_user).and_return(hiring_manager)

            while hiring_manager.open_positions.size < 2
                OpenPosition.create!(hiring_manager_id: hiring_manager.id, title: "title #{hiring_manager.open_positions.size}", available_interview_times: 'times')
                hiring_manager.open_positions.reload
            end

            first_position, second_position = hiring_manager.open_positions.to_a.slice(0, 2)

            first_position.updated_at = Time.now + 1
            second_position.updated_at = Time.now + 2
            first_position.save!
            second_position.save!

            assert_last_updated_at(:open_position, second_position.updated_at.to_f)

            first_position.updated_at = Time.now + 4
            second_position.updated_at = Time.now + 3
            first_position.save!
            second_position.save!

            assert_last_updated_at(:open_position, first_position.updated_at.to_f)

            teammate_open_position = hiring_manager.hiring_team_open_positions.where.not(hiring_manager_id: hiring_manager.id).first
            teammate_open_position.updated_at =  Time.now + 5
            teammate_open_position.save!

            assert_last_updated_at(:open_position, teammate_open_position.updated_at.to_f)
        end

    end

    describe "candidate_position_interest" do

        it "should work" do
            hiring_manager = users(:hiring_manager_with_team)
            allow(controller).to receive(:current_user).and_return(hiring_manager)
            open_position = hiring_manager.open_positions.first

            while open_position.candidate_position_interests.size < 2
                candidate_id = CareerProfile.active.where.not(user_id: CandidatePositionInterest.pluck(:candidate_id)).first.user_id

                CandidatePositionInterest.create!(
                    open_position_id: open_position.id,
                    candidate_status: 'accepted',
                    candidate_id: candidate_id)
                open_position.candidate_position_interests.reload
            end

            first_interest, second_interest = open_position.candidate_position_interests.to_a.slice(0, 2)

            first_interest.updated_at = Time.now + 1
            second_interest.updated_at = Time.now + 2
            first_interest.save!
            second_interest.save!

            assert_last_updated_at(:candidate_position_interest, second_interest.updated_at.to_f)

            first_interest.updated_at = Time.now + 4
            second_interest.updated_at = Time.now + 3
            first_interest.save!
            second_interest.save!

            assert_last_updated_at(:candidate_position_interest, first_interest.updated_at.to_f)

            teammate_position = hiring_manager.hiring_team_open_positions.where.not(hiring_manager_id: hiring_manager.id).first
            if teammate_position.candidate_position_interests.empty?
                candidate_id = CareerProfile.active.where.not(user_id: CandidatePositionInterest.pluck(:candidate_id)).first.user_id

                CandidatePositionInterest.create!(
                    open_position_id: teammate_position.id,
                    candidate_status: 'accepted',
                    candidate_id: candidate_id)
                open_position.candidate_position_interests.reload
            end
            teammate_interest = teammate_position.candidate_position_interests.first
            teammate_interest.updated_at =  Time.now + 5
            teammate_interest.save!

            assert_last_updated_at(:candidate_position_interest, teammate_interest.updated_at.to_f)
        end

    end

    def assert_last_updated_at(key, val)
        result = controller.add_last_updated_to_push_messages(key, "current")

        # not sure why be_within is necessary, but otherwise we get slight differences on ci
        expect(result).to be_within(0.0001).of(val)
        expect(controller.meta['push_messages']['unloaded_change_detector'][key.to_s]).to be_within(0.0001).of(val)
    end

end