require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'
require 'stripe_helper'

describe Api::HiringApplicationsController do
    include ControllerSpecHelper
    include StripeHelper
    json_endpoint

    fixtures(:users)

    before(:each) do
        stub_client_params(controller)
    end

    describe "GET index" do

        it "should 401 for non-admin" do
            stub_current_user(controller, [:learner])
            get :index, params: { format: :json }
            assert_error_response(401)
        end

        it "should return hiring applications" do
            stub_current_user(controller, [:admin])
            get :index, params: { format: :json }
            assert_200
            expect(body_json['contents']['hiring_applications'].map { |e| e['id']}).to match_array(HiringApplication.pluck('id'))
        end

        it "should use eager loading" do
            # see should take advantage of active record preloading to optimize data loading in conversations_controller_spec for reference
            hiring_applications = controller.preload_hiring_applications_for_index

            # once the records are preloaded, there should be no more
            # queries required to convert them to json
            expect(ActiveRecord::Base.connection).not_to receive(:exec_query).and_call_original
            json = hiring_applications.as_json(user_id: controller.current_user_id)

            # sanity checks. make sure that we actually generated a full json tree
            expect(hiring_application = json[0]).not_to be_nil
            expect(hiring_application['user_id']).not_to be_nil
        end

        describe "limit" do
            before(:each) do
                stub_current_user(controller, [:admin])
            end

            it "should not limit if nothing passed in" do
                get :index, params: { format: :json }
                assert_200
                expect(body_json['contents']['hiring_applications'].length).to eq(HiringApplication.count)
            end

            it "should obey the limit if passed in" do
                get :index, params: { format: :json, limit: 1 }
                assert_200
                expect(body_json['contents']['hiring_applications'].length).to eq(1)
            end

            it "should prevent negative numbers" do
                get :index, params: { format: :json, limit: -1 }
                assert_200
                expect(body_json['contents']['hiring_applications']).to be_nil
            end
        end

        describe "sorting" do

            before(:each) do
                stub_current_user(controller, [:admin])

                @hiring_applications = HiringApplication.limit(2)
            end

            it "should sort by created_at" do
                @hiring_applications[0].update(created_at: Time.now - 10.minutes)
                @hiring_applications[1].update(created_at: Time.now - 5.minutes)

                json = get_json_for_two_applications({sort: 'created_at', direction: 'asc', format: :json})
                expect(json.size).to eq(2)
                expect(json[0]["id"]).to eq(@hiring_applications[0].id)
                expect(json[1]["id"]).to eq(@hiring_applications[1].id)

                json = get_json_for_two_applications({sort: 'created_at', direction: 'desc', format: :json})
                expect(json.size).to eq(2)
                expect(json[0]["id"]).to eq(@hiring_applications[1].id)
                expect(json[1]["id"]).to eq(@hiring_applications[0].id)
            end

            def get_json_for_two_applications(params)
                get :index, params: params

                application_ids = [@hiring_applications[0], @hiring_applications[1]].map(&:id)
                body_json["contents"]["hiring_applications"].select do |hiring_application|
                    application_ids.include?(hiring_application['id'])
                end
            end
        end

    end

    describe "GET show" do

        describe "as non-admin" do
            it "should 401" do
                stub_current_user
                hiring_manager = users(:hiring_manager)
                get :show, params: {format: :json, id: hiring_manager.hiring_application.id}
                assert_error_response(401)
            end
        end

        describe "as admin" do

            before(:each) do
                stub_current_user(controller, [:admin])
            end

            it "should render a hiring application" do
                hiring_manager = users(:hiring_manager)
                get :show, params: {format: :json, id: hiring_manager.hiring_application.id}
                assert_200
                expect(body_json["contents"]["hiring_applications"].length).to eq(1)
                expect(body_json["contents"]["hiring_applications"][0]["id"]).to eq(hiring_manager.hiring_application.id)
                expect(body_json["contents"]["hiring_applications"][0]["user_id"]).to eq(hiring_manager.id)
            end
        end

    end

    describe "PUT update" do

        before(:each) do
            @hiring_manager = users(:hiring_manager)
            allow(controller).to receive(:current_user).and_return(@hiring_manager)
        end

        it "should 401 if trying to update application for another user" do
            allow(controller).to receive(:current_user).and_return(@hiring_manager)
            another_user = users(:pending_hiring_manager)

            expect(HiringApplication).not_to receive(:update_from_hash!)
            put :update,
                params: {
                    format: :json,
                    record: {
                        id: another_user.hiring_application.id
                    }
                }
            assert_error_response(401)
        end

        it "should allow admin to update application, including status" do
            hiring_application = HiringApplication.where.not(status: 'accepted').first

            # sanity check
            hiring_application.reload
            expect(hiring_application.status).not_to eq('accepted')
            hiring_application.user.ensure_hiring_team!

            allow(controller).to receive(:current_user).and_return(users(:admin))

            put :update,
                params: {
                    format: :json,
                    record: {
                        id: hiring_application.id,
                        status: "accepted",
                    }
                }
            assert_200
            hiring_application.reload
            expect(hiring_application.status).to eq('accepted')
        end

        it "should work in normal case" do
            allow(controller).to receive(:current_user).and_return(@hiring_manager)
            put :update,
                params: {
                    format: :json,
                    record: {
                        id: @hiring_manager.hiring_application.id,
                        website_url: "1337.com"
                    }
                }
            assert_200
        end

        it "should not allow non admin to update status" do
            hiring_application = HiringApplication.where.not(status: 'accepted').first
            @hiring_manager = hiring_application.user
            allow(controller).to receive(:current_user).and_return(@hiring_manager)
            expect(@hiring_manager.global_role.to_s).not_to eq("admin")
            put :update,
                params: {
                    format: :json,
                    record: {
                        id: hiring_application.id,
                        status: "accepted"
                    }
                }
            assert_200
            expect(hiring_application.reload.status).not_to eq("accepted")
        end

        it "should not error if rejecting a user without a hiring_team nor any new hiring_team info in the meta" do
            hiring_application = HiringApplication.where(status: 'pending').first
            hiring_application.user.update_column(:hiring_team_id, nil)

            expect(hiring_application.reload.user.hiring_team).to be_nil # sanity check
            allow(controller).to receive(:current_user).and_return(users(:admin))

            put :update,
                params: {
                    format: :json,
                    record: {
                        id: hiring_application.id,
                        status: "rejected"
                    }
                }
            assert_200
            hiring_application.reload
            expect(hiring_application.status).to eq('rejected')
        end

        describe "user properties" do
            it "should update user if its properties changed" do
                put :update,
                    params: {
                        format: :json,
                            record: {
                            id: @hiring_manager.hiring_application.id,
                            website_url: "1337.com",
                            name: "Mister Anderson",
                            nickname: "Bossmane",
                            avatar_url: "changed"
                        }
                    }
                assert_200
                expect(body_json["contents"]["hiring_applications"].first["website_url"]).to eq("https://1337.com")
                expect(body_json["contents"]["hiring_applications"].first["name"]).to eq("Mister Anderson")
                expect(body_json["contents"]["hiring_applications"].first["nickname"]).to eq("Bossmane")
                expect(body_json["contents"]["hiring_applications"].first["avatar_url"]).to eq("changed")

                @hiring_manager.reload
                @hiring_manager.hiring_application.reload

                expect(@hiring_manager.hiring_application.website_url).to eq("https://1337.com")
                expect(@hiring_manager.name).to eq("Mister Anderson")
                expect(@hiring_manager.nickname).to eq("Bossmane")
            end

            it "should not update user if its properties did not change" do
                allow(@controller).to receive(:update_last_seen_at)
                @hiring_manager.name = "Mister Anderson"
                # if this is nil, it will trigger a update_tracked_fields! call which saves the user
                # see ApplicationController#should_update_tracked_user_fields?
                @hiring_manager.current_sign_in_at = Time.now
                @hiring_manager.save!

                allow(@hiring_manager).to receive(:save)
                put :update,
                    params: {
                        format: :json,
                        record: {
                            id: @hiring_manager.hiring_application.id,
                            website_url: "1337.com",
                            name: "Mister Anderson"
                        }
                    }
                assert_200

                @hiring_manager.reload
                @hiring_manager.hiring_application.reload

                expect(@hiring_manager).not_to have_received(:save)
            end

            it "should set meta properties if user properties are changed" do
                allow(controller).to receive(:add_hiring_application_user_fields_to_push_messages)

                put :update,
                    params: {
                        format: :json,
                        record: {
                            id: @hiring_manager.hiring_application.id,
                            website_url: "1337.com",
                            name: "Mister Anderson"
                        }
                    }
                assert_200

                expect(controller).to have_received(:add_hiring_application_user_fields_to_push_messages)
            end

            it "should not allow non admins to update hiring team associations" do
                hiring_application = HiringApplication.where.not(status: 'accepted').first
                @hiring_manager = hiring_application.user
                allow(controller).to receive(:current_user).and_return(@hiring_manager)
                expect(@hiring_manager.global_role.to_s).not_to eq("admin")
                expected_hiring_team_id = HiringTeam.joins(:hiring_managers).where.not("users.id" => @hiring_manager.id).first.id
                put :update,
                    params: {
                        format: :json,
                        record: {
                            id: hiring_application.id,
                            status: "accepted"
                        },
                        meta: {
                            hiring_team_id: expected_hiring_team_id
                        }
                    }
                assert_200
                expect(@hiring_manager.reload.hiring_team_id).not_to eq(expected_hiring_team_id)
            end

            it "should allow admins to create new hiring teams and associations" do
                owner_ids = HiringTeam.pluck(:owner_id)
                hiring_application = HiringApplication.where.not(user_id: owner_ids).where.not(status: 'accepted').first
                @hiring_manager = hiring_application.user
                allow(controller).to receive(:current_user).and_return(users(:admin))
                hiring_team_title = SecureRandom.uuid
                put :update,
                    params: {
                        format: :json,
                        record: {
                            id: hiring_application.id,
                            status: "accepted"
                        },
                        meta: {
                            hiring_team_title: hiring_team_title
                        }
                    }
                assert_200
                new_team = HiringTeam.find_by_title(hiring_team_title)
                expect(new_team).not_to be_nil
                expect(new_team.owner_id).to eq(@hiring_manager.id)
                expect(@hiring_manager.reload.hiring_team_id).to eq(new_team.id)
            end

            it "should not allow non admins to create new hiring teams and associations" do
                hiring_application = HiringApplication.where.not(status: 'accepted').first
                @hiring_manager = hiring_application.user
                allow(controller).to receive(:current_user).and_return(@hiring_manager)
                expect(@hiring_manager.global_role.to_s).not_to eq("admin")
                hiring_team_title = SecureRandom.uuid
                put :update,
                    params: {
                        format: :json,
                        record: {
                            id: hiring_application.id,
                            status: "accepted"
                        },
                        meta: {
                            hiring_team_title: hiring_team_title
                        }
                    }
                assert_200
                expect(HiringTeam.find_by_title(hiring_team_title)).to be_nil
            end

            it "should be possible to create a new hiring team for an accepted user with a team" do
                assert_creates_new_team('accepted', HiringTeam.first)
            end

            it "should be possible to create a new hiring team for a pending user with a team" do
                assert_creates_new_team('pending', HiringTeam.first)
            end

            it "should be possible to create a new hiring team for an accepted user with no team" do
                assert_creates_new_team('accepted', nil)
            end

            it "should be possible to create a new hiring team for a pending user with no team" do
                assert_creates_new_team('pending', nil)
            end

            it "should be possible to change the team for a pending user" do
                asserts_changes_team('pending')
            end

            it "should be possible to change the team for an accepted user" do
                asserts_changes_team('accepted')
            end

            def asserts_changes_team(status)
                initial_hiring_team, new_team = HiringTeam.limit(2).to_a

                # make sure that this hiring manager is not the owner so we can change the team
                owner_ids = HiringTeam.pluck(:owner_id)
                hiring_application = HiringApplication.where.not(user_id: owner_ids).first
                hiring_application.update(status: status)
                @hiring_manager = hiring_application.user
                @hiring_manager.update(hiring_team_id: initial_hiring_team&.id)

                allow(controller).to receive(:current_user).and_return(users(:admin))
                hiring_team_title = SecureRandom.uuid
                put :update,
                    params: {
                        format: :json,
                        record: {
                            id: hiring_application.id,
                            status: 'pending'
                        },
                        meta: {
                            hiring_team_id: new_team.id
                        }
                    }
                assert_200
                expect(new_team).not_to be_nil
                expect(@hiring_manager.reload.hiring_team_id).to eq(new_team.id)
            end

            def assert_creates_new_team(status, initial_hiring_team)

                # make sure that this hiring manager is not the owner so we can change the team
                owner_ids = HiringTeam.pluck(:owner_id)
                hiring_application = HiringApplication.where.not(user_id: owner_ids).first
                hiring_application.update(status: status)
                @hiring_manager = hiring_application.user
                @hiring_manager.update(hiring_team_id: initial_hiring_team&.id)

                allow(controller).to receive(:current_user).and_return(users(:admin))
                hiring_team_title = SecureRandom.uuid
                put :update,
                    params: {
                        format: :json,
                        record: {
                            id: hiring_application.id,
                            status: 'pending'
                        },
                        meta: {
                            hiring_team_title: hiring_team_title
                        }
                    }
                assert_200
                new_team = HiringTeam.find_by_title(hiring_team_title)
                expect(new_team).not_to be_nil
                expect(@hiring_manager.reload.hiring_team_id).to eq(new_team.id)
            end

        end
    end

    describe "update_hiring_team_if_specified" do
        before(:each) do
            stub_current_user(controller, [:admin])
        end

        it "should allow setting the hiring_team to nil" do
            user = HiringTeam.where.not(owner_id: nil).first.owner
            allow(controller).to receive(:params).and_return(ActionController::Parameters.new(meta: {
                hiring_team_id: nil
            }))
            controller.update_hiring_team_if_specified(user)
            expect(user.hiring_team).to be_nil
        end

        # For example, when we are rejecting a hiring application we don't care if the
        # hiring manager is on a team, nor do we need to set one.
        it "should handle no hiring_team and empty meta" do
            user = User.where(hiring_team_id: nil).first
            allow(controller).to receive(:params).and_return(ActionController::Parameters.new(meta: {}))
            expect {
                controller.update_hiring_team_if_specified(user)
            }.not_to raise_error
        end
    end
end