require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::HiringRelationshipsController do
    include ControllerSpecHelper
    json_endpoint

    fixtures(:hiring_relationships)

    before(:each) do
    end

    describe "GET index" do

        it "should 401 if ability says cannot" do
            allow(controller.current_ability).to receive(:can?).and_return(false)
            get :index, params: { :format => :json, candidate_id: SecureRandom.uuid }
            assert_error_response(401)
        end

        describe "with_valid_auth" do
            before(:each) do
                allow(controller.current_ability).to receive(:can?).and_return(true)
            end

            # FIXME: removed as part of https://trello.com/c/oyQYlWv6
            # it "should set last_relationship_updated_at in push messages" do
            #     hr = HiringRelationship.where.not('candidate_status': 'hidden').first
            #     candidate = hr.candidate
            #     expect(candidate).not_to be_nil
            #     allow(controller).to receive(:current_user).and_return(candidate)
            #     get :index, params: { :format => :json, filters: { candidate_id: candidate.id }.to_json }
            #     assert_200
            #     expect(body_json['meta']['push_messages']['unloaded_change_detector']['hiring_relationship']).not_to be_nil
            # end

            it "should return relationships for a hiring manager" do
                hiring_manager = HiringRelationship.where.not(hiring_manager_status: 'hidden').first.hiring_manager
                expect(hiring_manager).not_to be_nil
                allow(controller).to receive(:current_user).and_return(hiring_manager)
                get :index, params: { :format => :json, filters: { hiring_manager_id: hiring_manager.id, :viewable_by_hiring_manager => true }.to_json }
                assert_200
            end

            it "should not show relationships hidden from a candidate" do
                hidden_relationship = HiringRelationship.where(candidate_status: 'hidden').first
                candidate = hidden_relationship.candidate
                allow(controller).to receive(:current_user).and_return(candidate)
                get :index, params: { :format => :json, filters: { candidate_id: candidate.id, candidate_status_not: 'hidden' }.to_json }
                assert_200
                entries = body_json['contents']['hiring_relationships'] || []
                hidden_relationship_entry = entries.detect { |r| r['id'] == hidden_relationship.id }
                expect(hidden_relationship_entry).to be_nil
            end

            it "should put hiring_applications into the metadata" do
                hiring_manager = users(:hiring_manager)
                allow(controller).to receive(:current_user).and_return(hiring_manager)
                get :index, params: { :format => :json, filters: {}.to_json, hiring_applications_in_meta: true }
                assert_200
                entries = body_json['contents']['hiring_relationships'] || []
                first_hiring_manager_id = entries[0]['hiring_manager_id']

                hiring_applications = body_json['meta']['hiring_applications'] || []
                expect(hiring_applications.map { |e| e['user_id']}).to include(first_hiring_manager_id)
            end

            it "should filter relationships for a hiring manager by status" do
                expected_relationships = nil
                # find a hiring manager who has some pending relationships and some
                # non-pending relationships
                hiring_manager_ids_with_pending_relationships = HiringRelationship.where(hiring_manager_status: 'pending').distinct.pluck('hiring_manager_id')
                hiring_manager_ids_with_accepted_relationships = HiringRelationship.where(hiring_manager_status: 'accepted').distinct.pluck('hiring_manager_id')
                hiring_manager = User.find((hiring_manager_ids_with_pending_relationships & hiring_manager_ids_with_accepted_relationships).first)
                expect(hiring_manager).not_to be_nil
                expected_relationships = hiring_manager.candidate_relationships.where(hiring_manager_status: 'pending')

                # I do not want to filter by viewable_by_hiring_manager here, because it would
                # require more mocking, but this user is not allowed to not do that, so
                # I'm mocking out that permission check
                expect(controller).to receive(:verify_allowed_index_params)

                allow(controller).to receive(:current_user).and_return(hiring_manager)
                get :index, params: { :format => :json, filters: { hiring_manager_id: hiring_manager.id, hiring_manager_status: 'pending' }.to_json }
                assert_200

                returned_relationships = body_json['contents']['hiring_relationships']
                expect(returned_relationships[0]['hiring_manager_id']).to eq(hiring_manager.id)
                expect(returned_relationships.map { |r| r['id'] }).to match_array(expected_relationships.map(&:id))
            end

            it "should filter on hiring_team_id" do
                hiring_manager = users(:hiring_manager_with_team)
                teammate = users(:hiring_manager_teammate)

                users = User.left_outer_joins(:hiring_application, :hiring_manager_relationships).where('hiring_applications.id is null and hiring_relationships.id is null').limit(4)
                HiringRelationship.create!(hiring_manager_id: hiring_manager.id, candidate_id: users[0].id, hiring_manager_status: "pending")
                HiringRelationship.create!(hiring_manager_id: hiring_manager.id, candidate_id: users[1].id, hiring_manager_status: "pending")
                HiringRelationship.create!(hiring_manager_id: teammate.id, candidate_id: users[2].id, hiring_manager_status: "pending")
                HiringRelationship.create!(hiring_manager_id: teammate.id, candidate_id: users[3].id, hiring_manager_status: "pending")

                allow(controller).to receive(:current_user).and_return(hiring_manager)
                get :index, params: { :format => :json, filters: { hiring_team_id: hiring_manager.hiring_team_id }.to_json }
                assert_200

                returned_relationships = body_json['contents']['hiring_relationships']
                expected_relationships = hiring_manager.candidate_relationships + teammate.candidate_relationships
                expect(returned_relationships.map { |r| r['id'] }).to match_array(expected_relationships.map(&:id))
            end

            it "should filter on hiring_manager_not" do
                hiring_manager = users(:hiring_manager_with_team)
                teammate = users(:hiring_manager_teammate)

                users = User.left_outer_joins(:hiring_application, :hiring_manager_relationships).where('hiring_applications.id is null and hiring_relationships.id is null').limit(4)
                HiringRelationship.create!(hiring_manager_id: hiring_manager.id, candidate_id: users[0].id, hiring_manager_status: "pending")
                HiringRelationship.create!(hiring_manager_id: hiring_manager.id, candidate_id: users[1].id, hiring_manager_status: "pending")
                HiringRelationship.create!(hiring_manager_id: teammate.id, candidate_id: users[2].id, hiring_manager_status: "pending")
                HiringRelationship.create!(hiring_manager_id: teammate.id, candidate_id: users[3].id, hiring_manager_status: "pending")

                allow(controller).to receive(:current_user).and_return(hiring_manager)
                get :index, params: { :format => :json, filters: { hiring_team_id: hiring_manager.hiring_team_id, hiring_manager_not: teammate.id }.to_json }
                assert_200
                returned_relationships = body_json['contents']['hiring_relationships']
                expected_relationships = hiring_manager.candidate_relationships
                expect(returned_relationships.map { |r| r['id'] }).to match_array(expected_relationships.map(&:id))
            end

            it "should filter for closed relationships" do
                hiring_manager = users(:hiring_manager)
                allow(controller).to receive(:current_user).and_return(hiring_manager)
                get :index, params: { :format => :json, filters: { closed: true }.to_json }
                assert_200
                returned_relationships = body_json['contents']['hiring_relationships']
                expect(returned_relationships.map { |r| r['id'] }).to match_array(HiringRelationship.closed.pluck(:id))
            end

            it "should filter out closed relationships" do
                hiring_manager = users(:hiring_manager)
                allow(controller).to receive(:current_user).and_return(hiring_manager)
                get :index, params: { :format => :json, filters: { closed: false }.to_json }
                assert_200
                returned_relationships = body_json['contents']['hiring_relationships']
                expect(returned_relationships.map { |r| r['id'] }).to match_array(HiringRelationship.not_closed.pluck(:id))
            end

            it "should filter by candidate_status" do
                expected_relationships = nil
                # find a hiring manager who has some pending relationships and some
                # non-pending relationships
                hiring_manager_ids_with_pending_relationships = HiringRelationship.where(candidate_status: 'pending').distinct.pluck('hiring_manager_id')
                hiring_manager_ids_with_accepted_relationships = HiringRelationship.where(candidate_status: 'accepted').distinct.pluck('hiring_manager_id')
                hiring_manager = User.find((hiring_manager_ids_with_pending_relationships & hiring_manager_ids_with_accepted_relationships).first)
                expect(hiring_manager).not_to be_nil
                expected_relationships = hiring_manager.candidate_relationships.where(candidate_status: 'pending')

                # I do not want to filter by viewable_by_hiring_manager here, because it would
                # require more mocking, but this user is not allowed to not do that, so
                # I'm mocking out that permission check
                expect(controller).to receive(:verify_allowed_index_params)

                allow(controller).to receive(:current_user).and_return(hiring_manager)
                get :index, params: { :format => :json, filters: { hiring_manager_id: hiring_manager.id, candidate_status: 'pending' }.to_json }
                assert_200

                returned_relationships = body_json['contents']['hiring_relationships']
                expect(returned_relationships[0]['hiring_manager_id']).to eq(hiring_manager.id)
                expect(returned_relationships.map { |r| r['id'] }).to match_array(expected_relationships.map(&:id))
            end

            it "should filter by updated_since" do
                hiring_manager = users(:hiring_manager)
                now = Time.now
                relationship = hiring_manager.candidate_relationships.first
                relationship.update_attribute(:updated_at, now + 1.second)

                allow(controller).to receive(:current_user).and_return(hiring_manager)
                get :index, params: { :format => :json, filters: { updated_since: now.to_timestamp }.to_json }
                assert_200
                returned_relationships = body_json['contents']['hiring_relationships']
                expect(returned_relationships.map { |r| r['id'] }).to match_array([relationship.id])
            end

            it "should filter by candidate_status_not" do
                hiring_manager = users(:hiring_manager)
                allow(controller).to receive(:current_user).and_return(hiring_manager)
                pending_relationship = hiring_manager.candidate_relationships.detect { |hr| hr.candidate_status == 'pending' }
                accepted_relationship = hiring_manager.candidate_relationships.detect { |hr| hr.candidate_status == 'accepted' }
                expect(pending_relationship).not_to be_nil
                expect(accepted_relationship).not_to be_nil

                get :index, params: { :format => :json, filters: {
                    hiring_manager_id: hiring_manager.id,
                    candidate_status_not: 'pending'
                }.to_json }
                assert_200
                returned_relationships = body_json['contents']['hiring_relationships']
                expect(returned_relationships.map { |r| r['id'] }).to include(accepted_relationship.id)
                expect(returned_relationships.map { |r| r['id'] }).not_to include(pending_relationship.id)
            end

            it "should filter by open_position_id" do
                hiring_relationship = HiringRelationship.where.not(open_position_id: nil).first
                allow(controller).to receive(:current_user).and_return(hiring_relationship.hiring_manager)
                expected_relationships = hiring_relationship.hiring_manager.candidate_relationships.where(open_position_id: hiring_relationship.open_position_id)
                get :index, params: { :format => :json, filters: {  hiring_manager_id: hiring_relationship.hiring_manager.id, open_position_id: hiring_relationship.open_position_id }.to_json }
                assert_200
                returned_relationships = body_json['contents']['hiring_relationships']
                expect(returned_relationships.map { |r| r['id'] }).to match_array(expected_relationships.pluck(:id).uniq)
            end

            it "should filter by rejected_by_hiring_manager_or_closed" do
                hiring_manager = users(:hiring_manager)
                allow(controller).to receive(:current_user).and_return(hiring_manager)
                expected_relationships = hiring_manager.candidate_relationships.select(&:closed?) + hiring_manager.candidate_relationships.where(hiring_manager_status: 'rejected')
                get :index, params: { :format => :json, filters: {  hiring_manager_id: hiring_manager.id, rejected_by_hiring_manager_or_closed: true }.to_json }
                assert_200
                returned_relationships = body_json['contents']['hiring_relationships']
                expect(returned_relationships.map { |r| r['id'] }).to match_array(expected_relationships.pluck(:id).uniq)
            end

            it "should filter by rejected_by_candidate_or_closed" do
                allow(controller).to receive(:verify_signed_in)
                expected_relationships = HiringRelationship.closed + HiringRelationship.where(candidate_status: 'rejected')
                get :index, params: { :format => :json, filters: { rejected_by_candidate_or_closed: true }.to_json }
                assert_200
                returned_relationships = body_json['contents']['hiring_relationships']
                expect(returned_relationships.map { |r| r['id'] }).to match_array(expected_relationships.pluck(:id).uniq)
            end

            it "should limit and anonymize profiles on pending relationships when not has_full_access" do
                allow(controller).to receive(:verify_signed_in)
                expect(controller.current_ability).to receive(:cannot?).with(:view_full_hiring_manager_experience, nil).at_least(1).times.and_return(true)
                expect(HiringRelationship.where(hiring_manager_status: 'pending').count > 3).to be(true)

                # we ask for 4, but we should only get 3
                get :index, params: { :format => :json, limit: 4, filters: { hiring_manager_status: 'pending' }.to_json }
                assert_200
                returned_relationships = body_json['contents']['hiring_relationships']
                expect(returned_relationships.size).to eq(3)
                expect(returned_relationships[0]['career_profile']['anonymized']).to be(true)
            end

            it "should not limit or anonymize profiles on pending relationships when has_full_access" do
                allow(controller).to receive(:verify_signed_in)
                expect(controller.current_ability).to receive(:cannot?).with(:view_full_hiring_manager_experience, nil).at_least(1).times.and_return(false)
                expect(HiringRelationship.where(hiring_manager_status: 'pending').count > 3).to be(true)

                # we ask for 4, and we should get 4 since the limit is not being overridden
                get :index, params: { :format => :json, limit: 4, filters: { hiring_manager_status: 'pending' }.to_json }
                assert_200
                returned_relationships = body_json['contents']['hiring_relationships']
                expect(returned_relationships.size).to eq(4)
                expect(returned_relationships[0]['career_profile']['anonymized']).to be(false)
            end

            describe "viewable_by_hiring_manager" do
                it "should filter by viewable_by_hiring_manager (hidden hiring manager)" do
                    hidden_relationship = HiringRelationship.where(hiring_manager_status: 'hidden').first
                    hiring_manager = hidden_relationship.hiring_manager
                    allow(controller).to receive(:current_user).and_return(hiring_manager)
                    get :index, params: { :format => :json, filters: { hiring_manager_id: hiring_manager.id, viewable_by_hiring_manager: true, hiring_manager_status_not: 'hidden'  }.to_json }
                    assert_200
                    entries = body_json['contents']['hiring_relationships'] || []
                    hidden_relationship_entry = entries.detect { |r| r['id'] == hidden_relationship.id }
                    expect(hidden_relationship_entry).to be_nil
                end

                it "should filter by viewable_by_hiring_manager (pending hiring manager)" do
                    hiring_relationship = HiringRelationship.where(hiring_manager_status: 'pending').first
                    hiring_manager = hiring_relationship.hiring_manager
                    expect(hiring_manager).not_to be_nil
                    candidate = hiring_relationship.candidate
                    candidate.career_profile.update_attribute('interested_in_joining_new_company', 'not_interested')
                    allow(controller).to receive(:current_user).and_return(hiring_manager)
                    get :index, params: { :format => :json, filters: { hiring_manager_id: hiring_manager.id, :viewable_by_hiring_manager => true }.to_json }
                    assert_200
                    expect(body_json['contents']['hiring_relationships'].map { |r| r['candidate_id']}).not_to include(candidate.id)
                end

                it "should filter by viewable_by_hiring_manager ('not_interested' accepted-pending candidate)" do
                    hiring_relationship = HiringRelationship.where(hiring_manager_status: 'accepted', candidate_status: 'pending').first
                    hiring_manager = hiring_relationship.hiring_manager
                    expect(hiring_manager).not_to be_nil
                    candidate = hiring_relationship.candidate
                    expect(candidate).not_to be_nil
                    candidate.career_profile.update_attribute('interested_in_joining_new_company', 'not_interested')
                    allow(controller).to receive(:current_user).and_return(hiring_manager)
                    get :index, params: { :format => :json, filters: { hiring_manager_id: hiring_manager.id, :viewable_by_hiring_manager => true }.to_json }
                    assert_200
                    expect(body_json['contents']['hiring_relationships'].map { |r| r['candidate_id']}).to include(candidate.id)
                end
            end
        end

        describe 'has_liked_candidate' do
            before(:each) do
                allow(controller.current_ability).to receive(:can?).and_return(true)
            end

            it "should set to true" do
                hiring_manager = HiringRelationship.where(hiring_manager_status: 'accepted').first.hiring_manager
                expect(hiring_manager).not_to be_nil

                allow(controller).to receive(:current_user).and_return(hiring_manager)
                get :index, params: { :format => :json, filters: { hiring_manager_id: hiring_manager.id, :viewable_by_hiring_manager => true }.to_json }
                assert_200
                expect(controller.meta['has_liked_candidate']).to be(true)
            end

            it "should set to false" do
                HiringRelationship.where(hiring_manager_status: 'accepted').delete_all
                hiring_manager = users(:hiring_manager)
                expect(hiring_manager).not_to be_nil

                allow(controller).to receive(:current_user).and_return(hiring_manager)
                get :index, params: { :format => :json, filters: { hiring_manager_id: hiring_manager.id, :viewable_by_hiring_manager => true }.to_json }
                assert_200
                expect(controller.meta['has_liked_candidate']).to be(false)
            end

            it "should set to true even if we are not returning the one that was liked" do
                # get a hiring manager with accepted and pending relationships
                hiring_manager = users(:hiring_manager)

                pending_relationships = hiring_manager.candidate_relationships.select { |r| r.hiring_manager_status == 'pending' }
                accepted_relationships = hiring_manager.candidate_relationships.select { |r| r.hiring_manager_status == 'accepted' }
                expect(pending_relationships).not_to be_empty
                expect(accepted_relationships).not_to be_empty
                allow(controller).to receive(:preload_hiring_relationships_for_index).and_return(pending_relationships)

                # even though only pending relationships were loaded and returned to the client, has_liked_candidate
                # is true because the user has an accepted relationship in the db somewhere
                allow(controller).to receive(:current_user).and_return(hiring_manager)
                get :index, params: { :format => :json, filters: { hiring_manager_id: hiring_manager.id, :viewable_by_hiring_manager => true }.to_json }
                assert_200
                expect(controller.meta['has_liked_candidate']).to be(true)
            end

            it "should not set to true because one of my teammates liked someone" do
                hiring_manager = users(:hiring_manager_with_team)
                hiring_manager.candidate_relationships.where(hiring_manager_status: 'accepted').destroy_all
                hiring_manager_teamate = users(:hiring_manager_teammate)
                hiring_manager_teamate.ensure_candidate_relationships([users(:learner).id])
                hiring_manager_teamate.candidate_relationships.first.update(hiring_manager_status: 'accepted')

                allow(controller).to receive(:current_user).and_return(hiring_manager)
                get :index, params: { :format => :json, filters: { hiring_team_id: hiring_manager.hiring_team_id }.to_json }
                assert_200
                entries = body_json['contents']['hiring_relationships'] || []
                expect(entries).not_to be_empty
                expect(controller.meta['has_liked_candidate']).to be(false)
            end
        end

        describe "preloading" do

            it "should use eager loading" do
                # see should take advantage of active record preloading to optimize data loading in conversations_controller_spec for reference
                hiring_relationships = controller.preload_hiring_relationships_for_index(HiringRelationship.where({}))
                assert_as_json_makes_no_queries(hiring_relationships)
            end

            it "should not eager load career_profile if it is in except" do
                controller.params["except"] = ["career_profile"]
                hiring_relationships = controller.preload_hiring_relationships_for_index(HiringRelationship.where({}))
                expect(hiring_relationships.first.association(:career_profile).loaded?).to be(false)
                expect(hiring_relationships.first.association(:candidate).loaded?).to be(true)
                assert_as_json_makes_no_queries(hiring_relationships)
            end

            it "should not eager load career_profile if it is not in fields" do
                controller.params["fields"] = ["id", "candidate_status"]
                hiring_relationships = controller.preload_hiring_relationships_for_index(HiringRelationship.where({}))
                expect(hiring_relationships.first.association(:career_profile).loaded?).to be(false)
                expect(hiring_relationships.first.association(:candidate).loaded?).to be(true)
                assert_as_json_makes_no_queries(hiring_relationships)
            end

            it "should not eager load hiring_application if it is not going to be used" do
                controller.params["except"] = ["hiring_application"]
                hiring_relationships = controller.preload_hiring_relationships_for_index(HiringRelationship.where({}))
                expect(hiring_relationships.first.association(:hiring_application).loaded?).to be(false)
                expect(hiring_relationships.first.association(:hiring_manager).loaded?).to be(true)
                assert_as_json_makes_no_queries(hiring_relationships)
            end

            it "should not eager load hiring_application if it is not in fields" do
                controller.params["fields"] = ["id", "candidate_status"]
                hiring_relationships = controller.preload_hiring_relationships_for_index(HiringRelationship.where({}))
                expect(hiring_relationships.first.association(:hiring_application).loaded?).to be(false)
                expect(hiring_relationships.first.association(:hiring_manager).loaded?).to be(true)
                assert_as_json_makes_no_queries(hiring_relationships)
            end

            it "should eager load hiring_application hiring_applications_in_meta" do
                controller.params["except"] = ["hiring_application"]
                controller.params['hiring_applications_in_meta'] = true
                hiring_relationships = controller.preload_hiring_relationships_for_index(HiringRelationship.where({}))
                expect(hiring_relationships.first.association(:hiring_application).loaded?).to be(true)
                expect(hiring_relationships.first.association(:hiring_manager).loaded?).to be(true)
                assert_as_json_makes_no_queries(hiring_relationships)
            end

            it "should not eager load conversation if it is in except" do
                controller.params["except"] = ["conversation"]
                hiring_relationships = controller.preload_hiring_relationships_for_index(HiringRelationship.where({}))
                expect(hiring_relationships.first.association(:conversation).loaded?).to be(false)
                expect(hiring_relationships.first.association(:candidate).loaded?).to be(true)
                assert_as_json_makes_no_queries(hiring_relationships)
            end

            it "should not eager load conversation if it is not in fields" do
                controller.params["fields"] = ["id", "candidate_status"]
                hiring_relationships = controller.preload_hiring_relationships_for_index(HiringRelationship.where({}))
                expect(hiring_relationships.first.association(:conversation).loaded?).to be(false)
                expect(hiring_relationships.first.association(:candidate).loaded?).to be(true)
                assert_as_json_makes_no_queries(hiring_relationships)
            end

            def assert_as_json_makes_no_queries(hiring_relationships)
                # Mock this out, since in prod it will almost always be cached
                allow(Cohort).to receive(:cached_published).and_return(
                    OpenStruct.new(
                        name: 'MBA1',
                        id: '1234',
                        start_date: Time.now + 10.days,
                        end_date: Time.now + 1.year
                    )
                )
                # once the records are preloaded, there should be no more
                # queries required to convert them to json
                expect(ActiveRecord::Base.connection).not_to receive(:exec_query).and_call_original
                json = hiring_relationships.as_json({user_id: controller.current_user_id}.merge(controller.index_as_json_options))

                # sanity checks. make sure that we actually generated a full json tree
                expect(hiring_relationship = json[0]).not_to be_nil
                expect(hiring_relationship['candidate_status']).not_to be_nil
            end

        end

    end

    describe "PUT update" do

        describe "auth" do

            it "should 401 if hiring_relationship is unrelated to current user" do
                user = users(:learner)

                expect(controller).to receive(:current_user).at_least(1).times.and_return(user)
                hiring_relationship = HiringRelationship.where.not(candidate_id: controller.current_user.id)
                                                    .where.not(hiring_manager_id: controller.current_user.id)
                                                    .first

                # get better error messages when this fails
                allow(controller).to receive(:update)

                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            :id => hiring_relationship.id
                        }
                    }

                assert_error_response(401)
            end

            it "should 401 with no current user" do
                stub_no_current_user
                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            :id => HiringRelationship.first.id
                        }
                    }
                assert_error_response(401)
            end

            it "should 401 if user is not allowed to update" do
                permission_checked = false
                expect(controller).to receive(:current_user).and_return(User.first).at_least(1).times
                expect(controller.current_ability).to receive(:can?) do |meth, obj|
                    if meth == :update && obj.is_a?(HiringRelationship)
                        permission_checked = true
                        false
                    else
                        true
                    end
                end

                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            :id => HiringRelationship.first.id
                        }
                    }
                assert_error_response(401)
                expect(permission_checked).to be(true)
            end

        end

        describe "with_valid_auth" do

            before(:each) do
                allow(controller.current_ability).to receive(:can?) do |meth, object|
                    meth != :manage
                end
            end

            it "should set last_relationship_updated_at after the update" do
                hiring_relationship = HiringRelationship.first
                allow(controller).to receive(:current_user).and_return(hiring_relationship.hiring_manager)

                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: hiring_relationship.id,
                            hiring_manager_status: hiring_relationship.hiring_manager_status
                        }
                    }

                before = body_json['meta']['push_messages']['unloaded_change_detector']['hiring_relationship_before_changes']
                after = body_json['meta']['push_messages']['unloaded_change_detector']['hiring_relationship']
                expect(before).not_to be_nil
                expect(after).not_to be_nil
                expect(after > before).to be(true)

            end

            it "should return the relationship with the full, unrestricted career profile json" do
                hiring_relationship = HiringRelationship.first
                allow(controller).to receive(:current_user).and_return(hiring_relationship.hiring_manager)
                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: hiring_relationship.id,
                            hiring_manager_status: hiring_relationship.hiring_manager_status
                        }
                    }
                expect(body_json['contents']['hiring_relationships'][0]['career_profile']['anonymized']).to be(false)
            end

            it "should only update the hiring_manager_relationship for the hiring manager" do
                hiring_relationship = HiringRelationship.where('hiring_manager_status': 'pending').first
                allow(controller).to receive(:current_user).and_return(hiring_relationship.hiring_manager)

                updated_candidate_status = HiringRelationship.candidate_statuses.without(hiring_relationship.candidate_status).first
                updated_hiring_manager_status = HiringRelationship.hiring_manager_statuses.without(hiring_relationship.hiring_manager_status).first

                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: hiring_relationship.id,
                            candidate_status: updated_candidate_status,
                            hiring_manager_status: updated_hiring_manager_status
                        }
                    }
                assert_200
                reloaded = HiringRelationship.find(hiring_relationship.id)
                expect(reloaded.hiring_manager_status).to eq(updated_hiring_manager_status)
                expect(reloaded.candidate_status).to eq(hiring_relationship.candidate_status)
                expect(reloaded.candidate_status).not_to eq(updated_candidate_status)
            end

            it "should only update the candidate relationship for the candidate" do
                hiring_relationship = HiringRelationship.where('hiring_manager_status': 'pending').first

                hiring_relationship.hiring_manager.hiring_application.status = "accepted"
                hiring_relationship.hiring_manager.hiring_application.save!

                allow(controller).to receive(:current_user).and_return(hiring_relationship.candidate)

                updated_candidate_status = HiringRelationship.candidate_statuses.without(hiring_relationship.candidate_status).first
                updated_hiring_manager_status = HiringRelationship.hiring_manager_statuses.without(hiring_relationship.hiring_manager_status).first

                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: hiring_relationship.id,
                            hiring_manager_status: updated_hiring_manager_status,
                            hiring_manager_closed_info: {
                                "reason" => "Should not be set in this case"
                            },
                            candidate_status: updated_candidate_status
                        }
                    }
                assert_200
                reloaded = HiringRelationship.find(hiring_relationship.id)
                expect(reloaded.candidate_status).to eq(updated_candidate_status)
                expect(reloaded.hiring_manager_status).to eq(hiring_relationship.hiring_manager_status)
                expect(reloaded.hiring_manager_status).not_to eq(updated_hiring_manager_status)
                expect(reloaded.hiring_manager_closed_info).to be_empty
            end

            it "should only update the conversation for a teammate" do
                hiring_manager = users(:hiring_manager_with_team)
                teammate = users(:hiring_manager_teammate)
                hiring_manager = users(:hiring_manager_with_team)
                hiring_relationship = hiring_manager.candidate_relationships.joins(:conversation).first
                expect(hiring_relationship).not_to be_nil
                expect(Mailboxer::Conversation).to receive(:create_or_update_from_hash!).and_call_original

                allow(controller).to receive(:current_user).and_return(teammate)

                updated_candidate_status = HiringRelationship.candidate_statuses.without(hiring_relationship.candidate_status).first
                updated_hiring_manager_status = HiringRelationship.hiring_manager_statuses.without(hiring_relationship.hiring_manager_status).first

                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: hiring_relationship.id,
                            hiring_manager_status: updated_hiring_manager_status,
                            hiring_manager_closed_info: {
                                "reason" => "Should not be set in this case"
                            },
                            candidate_status: updated_candidate_status,
                            conversation: hiring_relationship.conversation.as_json
                        },
                        :meta => {
                            :using_new_receipts_syntax => true
                        }
                    }
                assert_200
                reloaded = HiringRelationship.find(hiring_relationship.id)
                expect(reloaded.candidate_status).to eq(hiring_relationship.candidate_status)
                expect(reloaded.hiring_manager_status).to eq(hiring_relationship.hiring_manager_status)
                expect(reloaded.hiring_manager_closed_info).to eq(hiring_relationship.hiring_manager_closed_info)
                expect(reloaded.updated_at > hiring_relationship.updated_at).to be(true)
            end

            it "should call create_or_update_from_hash! on a conversation embedded in hiring_relationship" do
                hiring_relationship = HiringRelationship.joins(:conversation).first
                candidate_position_interest = CandidatePositionInterest.first

                conversation = hiring_relationship.conversation
                expect(conversation).to receive(:as_json).at_least(1).times.and_return({'conversation' => 'updated_json'})

                expect(controller).to receive(:current_user).and_return(hiring_relationship.candidate).at_least(1).times
                expect(Mailboxer::Conversation).to receive(:create_or_update_from_hash!)
                    .with({'conversation' => 'json'}, hiring_relationship.candidate, hiring_relationship)
                    .and_return(conversation)
                expect_any_instance_of(HiringRelationship).to receive(:new_message=)

                # we need to bump the updated_at on relationships when a conversation
                # changes so that the change can be pushed down to other participants
                expect {
                    put :update,
                        params: {
                            :format => :json,
                            :record => hiring_relationship.as_json.merge("conversation" => {'conversation' => 'json'}),
                            :meta => {
                                :candidate_position_interest => candidate_position_interest.as_json(career_profile_options: {})
                            }
                        }
                }.to change {
                    hiring_relationship.reload.updated_at
                }

                assert_200
                expect(body_json['meta']['conversation']).to be_nil
                expect(hiring_relationship.reload.conversation.id).to eq(conversation.id)
            end

            it "should save_open_position" do
                hiring_manager, candidate = get_hiring_manager_and_unrelated_candidate
                expect(controller).to receive(:current_user).and_return(hiring_manager).at_least(1).times
                expect(controller.current_ability).to receive(:cannot?).and_return(false).at_least(1).times
                expect(controller).to receive(:save_open_position).and_call_original
                open_position_id = SecureRandom.uuid

                start = Time.now.to_f
                post :create,
                    params: {
                        :format => :json,
                        :record => {
                            hiring_manager_id: hiring_manager.id,
                            candidate_id: candidate.id,
                            open_position_id: open_position_id
                        },
                        :meta => {
                            "open_position" => {
                                'id' => open_position_id,
                                'hiring_manager_id' => hiring_manager.id,
                                'title' => 'title',
                                'available_interview_times' => 'tuesday'
                            }
                        }
                    }
                assert_200
                expect(body_json['meta']['push_messages']['unloaded_change_detector']['open_position_before_changes'] > start).to be(false)
                expect(body_json['meta']['push_messages']['unloaded_change_detector']['open_position'] > start).to be(true)
            end

            it "should update hiring_manager_closed and related info for the hiring_manager" do
                hiring_relationship = HiringRelationship.first
                allow(controller).to receive(:current_user).and_return(hiring_relationship.hiring_manager)
                closed_info = {
                    "reasons" => ["foo", "bar"],
                    "feedback" => "Here is some helpful feedback"
                }

                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: hiring_relationship.id,
                            hiring_manager_status: 'accepted',
                            hiring_manager_closed: true,
                            hiring_manager_closed_info: closed_info
                        }
                    }
                assert_200
                reloaded = HiringRelationship.find(hiring_relationship.id)
                expect(reloaded.hiring_manager_status).to eq('accepted')
                expect(reloaded.hiring_manager_closed).to eq(true)
                expect(reloaded.hiring_manager_closed_info).to eq(closed_info)
            end

            it "should allow updating hiring_manager_closed if admin" do
                stub_current_user(controller, [:admin])
                allow(controller.current_user).to receive(:hiring_team_id).and_return(nil) # make sure we don't treat this as a teammate
                hiring_relationship = HiringRelationship.where(hiring_manager_status: 'accepted', candidate_status: 'accepted', hiring_manager_closed: nil).first

                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: hiring_relationship.id,
                            hiring_manager_closed: true,
                            hiring_manager_priority: hiring_relationship.hiring_manager_priority
                        },
                    }
                assert_200

                hiring_relationship.reload
                expect(hiring_relationship.hiring_manager_closed).to be(true)
            end

            it "should allow updating candidate_closed if admin" do
                stub_current_user(controller, [:admin])
                allow(controller.current_user).to receive(:hiring_team_id).and_return(nil) # make sure we don't treat this as a teammate
                hiring_relationship = HiringRelationship.where(hiring_manager_status: 'accepted', candidate_status: 'accepted').first

                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: hiring_relationship.id,
                            candidate_closed: true,
                            hiring_manager_priority: hiring_relationship.hiring_manager_priority
                        },
                    }
                assert_200

                hiring_relationship.reload
                expect(hiring_relationship.candidate_closed).to be(true)
            end

            it "should send down hiring_relationships side_effects" do
                stub_current_user(controller, [:admin])
                allow(controller.current_user).to receive(:hiring_team_id).and_return(nil) # make sure we don't treat this as a teammate
                hiring_relationship = HiringRelationship.where(hiring_manager_status: 'accepted', candidate_status: 'accepted', hiring_manager_closed: nil).first

                allow_any_instance_of(HiringRelationship).to receive(:side_effects).and_return({
                    hiring_relationships: {'id' => 'hiring_relationship'}
                })

                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: hiring_relationship.id,
                            hiring_manager_closed: true,
                            hiring_manager_priority: hiring_relationship.hiring_manager_priority
                        },
                    }
                assert_200

                expect(controller.meta["hiring_relationships"]).to eq(['hiring_relationship'])

            end

            it "should send down candidate_position_interests side_effects" do
                stub_current_user(controller, [:admin])
                allow(controller.current_user).to receive(:hiring_team_id).and_return(nil) # make sure we don't treat this as a teammate
                hiring_relationship = HiringRelationship.where(hiring_manager_status: 'accepted', candidate_status: 'accepted', hiring_manager_closed: nil).first

                allow_any_instance_of(HiringRelationship).to receive(:side_effects).and_return({
                    candidate_position_interests: {'1' => 'interest_1', '2' => 'interest_2'}
                })

                post :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: hiring_relationship.id,
                            hiring_manager_closed: true,
                            hiring_manager_priority: hiring_relationship.hiring_manager_priority
                        }
                    }
                assert_200
                expect(controller.meta["candidate_position_interests"]).to eq(['interest_1', 'interest_2'])
                expect(body_json['meta']['push_messages']['unloaded_change_detector']['candidate_position_interest_before_changes']).not_to be_nil
                expect(body_json['meta']['push_messages']['unloaded_change_detector']['candidate_position_interest']).not_to be_nil
            end

            it "should render 406 when hiring_relationship has been hidden due to having been accepted by the team" do
                hiring_manager = users(:hiring_manager_with_team)
                teammate = users(:hiring_manager_teammate)
                candidate = users(:learner)
                allow(controller).to receive(:current_user).and_return(hiring_manager)

                hiring_manager_relationship = HiringRelationship.create!({
                    hiring_manager_id: hiring_manager.id,
                    candidate_id: candidate.id,
                    hiring_manager_status: 'pending',
                    candidate_status: 'pending'
                })
                teammate_relationship = HiringRelationship.create!({
                    hiring_manager_id: teammate.id,
                    candidate_id: candidate.id,
                    hiring_manager_status: 'pending',
                    candidate_status: 'pending'
                })

                teammate_relationship.update_attribute(:hiring_manager_status, "accepted")

                post :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: hiring_manager_relationship.id,
                            hiring_manager_status: 'accepted'
                        }
                    }
                expect(response.status).to eq(406) # not_acceptable
                expect(body_json["message"]).to eq("Validation failed: Candidate has already been accepted by your team")
                expect(@controller.meta["error_type"]).to eq("multiple_team_connections_to_candidate")
                expect(@controller.meta["teammate_name"]).to eq(teammate_relationship.hiring_manager.name)
                expect(@controller.meta["hiring_relationships"]).to eq([teammate_relationship.as_json])
                expect(@controller.meta["hiring_relationship"]).to eq(hiring_manager_relationship.reload.as_json(except: ['career_profile', 'hiring_application']))
            end

            it "should save_open_position" do
                hiring_manager, candidate = get_hiring_manager_and_unrelated_candidate
                open_position_id = SecureRandom.uuid

                hiring_manager_relationship = HiringRelationship.create!({
                    hiring_manager_id: hiring_manager.id,
                    candidate_id: candidate.id,
                    hiring_manager_status: 'pending',
                    candidate_status: 'pending'
                })

                expect(controller).to receive(:current_user).and_return(hiring_manager).at_least(1).times
                expect(controller.current_ability).to receive(:cannot?).and_return(false).at_least(1).times
                expect(controller).to receive(:save_open_position).and_call_original

                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: hiring_manager_relationship.id,
                            hiring_manager_id: hiring_manager.id,
                            candidate_id: candidate.id,
                            open_position_id: open_position_id,
                            hiring_manager_status: 'accepted'
                        },
                        :meta => {
                            "open_position" => {
                                'id' => open_position_id,
                                'hiring_manager_id' => hiring_manager.id,
                                'title' => 'title',
                                'available_interview_times' => 'tuesday'
                            }
                        }
                    }
                assert_200
            end

            it "should save a candidate's position interest" do
                hiring_manager, candidate = get_hiring_manager_and_unrelated_candidate

                expect(controller).to receive(:current_user).and_return(hiring_manager).at_least(1).times
                open_position = hiring_manager.open_positions.first

                interest = CandidatePositionInterest.create!({
                    open_position_id: open_position.id,
                    candidate_id: candidate.id,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'unseen'
                })

                hiring_manager_relationship = HiringRelationship.create!({
                    hiring_manager_id: hiring_manager.id,
                    candidate_id: candidate.id,
                    hiring_manager_status: 'pending',
                    candidate_status: 'pending'
                })

                # If we ever do send meta to update_from_hash! we need to ensure that last_curated_at is not permitted
                expect(CandidatePositionInterest).to receive(:update_from_hash!).with(an_instance_of(ActionController::Parameters), nil, nil, hiring_manager).and_call_original

                post :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: hiring_manager_relationship.id,
                            open_position_id: open_position.id,
                            hiring_manager_status: 'rejected'
                        },
                        :meta => {
                            "candidate_position_interest" => {
                                'id' => interest.id,
                                'hiring_manager_status' => CandidatePositionInterest.reviewed_hiring_manager_statuses.first
                            }
                        }
                    }

                assert_200
                expect(interest.reload.hiring_manager_status).to eq(CandidatePositionInterest.reviewed_hiring_manager_statuses.first)
                expect(body_json['meta']['candidate_position_interest']['hiring_manager_status']).to eq(CandidatePositionInterest.reviewed_hiring_manager_statuses.first)
                expect(body_json['meta']['candidate_position_interest']['id']).to eq(interest.id)
            end

            it "should handle conflict if interest has been saved since being loaded" do
                hiring_manager = users(:hiring_manager_with_team)
                teammate = users(:hiring_manager_teammate)
                candidate_id_not = hiring_manager.candidates.pluck('id') + teammate.candidates.pluck('id')
                candidate = User.where(can_edit_career_profile: true).where.not(id: candidate_id_not).first
                open_position = hiring_manager.open_positions.first

                interest = CandidatePositionInterest.create!({
                    open_position_id: open_position.id,
                    candidate_id: candidate.id,
                    candidate_status: 'accepted',
                    hiring_manager_status: CandidatePositionInterest.reviewed_hiring_manager_statuses.first
                })

                hiring_manager_relationship = HiringRelationship.create!({
                    hiring_manager_id: teammate.id,
                    candidate_id: candidate.id,
                    hiring_manager_status: 'pending',
                    candidate_status: 'pending'
                })

                expect(CandidatePositionInterest).to receive(:update_from_hash!).and_raise(CandidatePositionInterest::OldVersion)
                expect(controller).to receive(:current_user).and_return(teammate).at_least(1).times
                expect(controller.current_ability).to receive(:cannot?).and_return(false).at_least(1).times
                post :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: hiring_manager_relationship.id,
                            open_position_id: open_position.id,
                            hiring_manager_status: 'rejected'
                        },
                        :meta => {
                            "candidate_position_interest" => {
                                'id' => interest.id,
                                'hiring_manager_status' => 'unseen',
                                'updated_at' => (interest.updated_at - 1.minute).to_timestamp
                            }
                        }
                    }

                assert_error_response(409)
                expect(body_json['message']).to eq('Interest has been saved more recently')
                expect(body_json['meta']['error_type']).to eq('interest_conflict')
                expect(body_json['meta']['candidate_position_interest']['id']).to eq(interest.id)
                expect(body_json['meta']['candidate_position_interest']['career_profile']['anonymized']).to eq(false);
            end

            # https://trello.com/c/W5HDtTYw
            # https://trello.com/c/zZbE7Nmc
            it "should set new_message in the update_status event when hiring manager accepts a pending relationship" do
                # make sure these two do not auto-match, creating two accepted events
                # and causing a failure below in `expect(accepted_events.size).to be(1)`
                CandidatePositionInterest.destroy_all
                relationship = HiringRelationship.where({
                    hiring_manager_status: 'pending',
                    hiring_manager_closed: nil
                }).first

                hiring_manager = relationship.hiring_manager
                candidate = relationship.candidate

                allow(controller).to receive(:current_user).and_return(hiring_manager)
                start = Time.now
                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: relationship.id,
                            hiring_manager_status: 'accepted',
                            conversation: {
                                subject: "new subject",
                                messages: [{
                                    "body" => "new message body",
                                    "sender_id" => hiring_manager.id,
                                    "sender_type" => 'user',
                                    "metadata" => {"some" => "metadata"},
                                    "receipts" => [{
                                        "receiver_id" => hiring_manager.id
                                    }]
                                }],
                                recipients: [
                                    {
                                        'type' => 'user',
                                        'id' => candidate.id
                                    },
                                    {
                                        'type' => 'user',
                                        'id' => hiring_manager.id
                                    }]
                            }
                        },
                        :meta => {'using_new_receipts_syntax' => true}
                    }

                assert_200
                accepted_events = Event.where("created_at > ?", start).where(event_type: 'hiring_relationship:accepted')
                expect(accepted_events.size).to be(1)
                event = accepted_events.first
                expect(event.user).to eq(candidate)
                expect(event.payload['new_message'] && event.payload['new_message']['body']).to eq("new message body")

                # since two events happened at the same time which would normally trigger an email
                # to the candidate, we want to make sure we only send one of them.  We use just_accepted_relationship
                # to turn off the message_received email in customer.io and only send the "a hiring manager liked you" email
                message_received_event = Event.where("created_at > ?", start).where(event_type: 'mailboxer:message_received').first
                expect(message_received_event.payload['just_accepted_relationship']).to be(true)
            end

            # https://trello.com/c/KGtZokpN
            it "should set new_message in the update_status event when candidate accepts a pending relationship" do
                relationship = hiring_relationships(:accepted_pending)

                hiring_manager = relationship.hiring_manager
                candidate = relationship.candidate

                allow(controller).to receive(:current_user).and_return(candidate)
                start = Time.now
                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: relationship.id,
                            candidate_status: 'accepted',
                            conversation: {
                                subject: "new subject",
                                messages: [{
                                    "body" => "new message body",
                                    "sender_id" => candidate.id,
                                    "sender_type" => 'user',
                                    "metadata" => {"some" => "metadata"},
                                    "receipts" => [{
                                        "receiver_id" => candidate.id
                                    }]
                                }],
                                recipients: [
                                    {
                                        'type' => 'user',
                                        'id' => candidate.id
                                    },
                                    {
                                        'type' => 'user',
                                        'id' => hiring_manager.id
                                    }]
                            }
                        },
                        :meta => {'using_new_receipts_syntax' => true}
                    }

                assert_200
                accepted_events = Event.where("created_at > ?", start).where(event_type: 'hiring_relationship:accepted')
                expect(accepted_events.size).to be(1)
                event = accepted_events.first
                expect(event.user).to eq(hiring_manager)
                expect(event.payload['new_message'] && event.payload['new_message']['body']).to eq("new message body")

                # since two events happened at the same time which would normally trigger an email
                # to the hiring manager, we want to make sure we only send one of them.  We use just_accepted_relationship
                # to turn off the message_received email in customer.io and only send the "it's a match" email
                message_received_event = Event.where("created_at > ?", start).where(event_type: 'mailboxer:message_received').first
                expect(message_received_event.payload['just_accepted_relationship']).to be(true)
            end
        end
    end

    # look in cohort_controller_spec
    describe "POST create" do

        before(:each) do
            allow(controller.current_ability).to receive(:can?) do |meth, object|
                meth != :manage
            end
        end

        it "should allow admin to create hiring relationship" do
            hiring_manager = users(:hiring_manager)
            candidate = User.where(can_edit_career_profile: true).where.not(id: hiring_manager.candidates.pluck('id')).first
            stub_current_user(controller, [:admin])
            post :create,
                params: {
                    :format => :json,
                    :record => {
                        hiring_manager_id: hiring_manager.id,
                        candidate_id: candidate.id
                    }
                }
            assert_200
            expect(HiringRelationship.where(hiring_manager_id: hiring_manager.id, candidate_id: candidate.id).first).not_to be(nil)
        end

        it "should render unauthorized error if user is not an admin or a hiring manager" do
            stub_current_user
            expect(controller.current_user.hiring_application).to be_nil
            expect(controller.current_ability).to receive(:can?).and_return(false)
            post :create,
                params: {
                    format: :json,
                    record: {
                        candidate_id: User.first.id, # just some random id
                        hiring_manager_id: controller.current_user.id,
                        # this is what's expected if the hiring manager "likes" or chooses
                        # to "connect" with a candidate from the marketing hiring pages
                        hiring_manager_status: "accepted"
                    }
                }
            expect(response.status).to eq(401)
        end

        it "should render unauthorized if trying to create relationship for another user" do
            first_hiring_relationship = HiringRelationship.where(hiring_manager_status: "pending").first
            second_hiring_relationship = HiringRelationship.where.not(hiring_manager_id: first_hiring_relationship.hiring_manager_id).first
            allow(controller).to receive(:current_user).and_return(first_hiring_relationship.hiring_manager)
            expect(controller.current_ability).to receive(:can?).and_return(false)
            post :create,
                params: {
                    format: :json,
                    record: {
                        candidate_id: User.first.id, # just some random id
                        hiring_manager_id: second_hiring_relationship.hiring_manager.id,
                        # this is what's expected if the hiring manager "likes" or chooses
                        # to "connect" with a candidate from the marketing hiring pages
                        hiring_manager_status: "accepted"
                    }
                }
            expect(response.status).to eq(401)
        end

        it "should render 406 with appropriate message if a the relationship has already been liked by the team" do
            hiring_manager = users(:hiring_manager_with_team)
            teammate = users(:hiring_manager_teammate)
            candidate = users(:learner)

            assert_406_on_multiple_connections_error(hiring_manager, teammate, candidate, {
                hiring_manager_status: 'accepted'
            })
        end

        it "should render 406 with appropriate message if the relationship has already been liked by the team and we are inviting to connect" do
            hiring_manager = users(:hiring_manager_with_team)
            teammate = users(:hiring_manager_teammate)
            candidate = users(:learner)

            # in order to catch the bug we were seeing, this cannot mock out
            # Conversation::create_from_hash!.  There was an issue caused by the
            # fact that the relationship ends up getting saved by active record
            # when the relationship is setup within create_from_hash!
            assert_406_on_multiple_connections_error(hiring_manager, teammate, candidate, {
                hiring_manager_status: 'accepted',
                conversation: {
                    subject: "new subject",
                    messages: [{
                        "body" => "new message body",
                        "sender_id" => hiring_manager.id,
                        "sender_type" => 'user',
                        "metadata" => {"some" => "metadata"},
                        "receipts" => [{
                            "receiver_id" => hiring_manager.id
                        }]
                    }],
                    recipients: [
                        {
                            'type' => 'user',
                            'id' => hiring_manager.id
                        },
                        {
                            'type' => 'user',
                            'id' => candidate.id
                        }]
                }
            })
        end

        it "should send down hiring_relationships side_effects" do

            hiring_manager = users(:hiring_manager)
            candidate = User.where(can_edit_career_profile: true).where.not(id: hiring_manager.candidates.pluck('id')).first
            stub_current_user(controller, [:admin])

            allow_any_instance_of(HiringRelationship).to receive(:side_effects).and_return({
                hiring_relationships: {'id' => 'hiring_relationship'}
            })

            post :create,
                params: {
                    :format => :json,
                    :record => {
                        hiring_manager_id: hiring_manager.id,
                        candidate_id: candidate.id
                    }
                }
            assert_200
            expect(controller.meta["hiring_relationships"]).to eq(['hiring_relationship'])
        end

        it "should send down candidate_position_interests side_effects" do

            hiring_manager = users(:hiring_manager)
            candidate = User.where(can_edit_career_profile: true).where.not(id: hiring_manager.candidates.pluck('id')).first
            stub_current_user(controller, [:admin])

            allow_any_instance_of(HiringRelationship).to receive(:side_effects).and_return({
                candidate_position_interests: {'1' => 'interest_1', '2' => 'interest_2'}
            })

            post :create,
                params: {
                    :format => :json,
                    :record => {
                        hiring_manager_id: hiring_manager.id,
                        candidate_id: candidate.id
                    }
                }
            assert_200
            expect(controller.meta["candidate_position_interests"]).to eq(['interest_1', 'interest_2'])
        end

        it "should return the relationship with the full, unrestricted career profile json" do
            hiring_manager = users(:hiring_manager)
            candidate = User.where(can_edit_career_profile: true).where.not(id: hiring_manager.candidates.pluck('id')).first
            stub_current_user(controller, [:admin])

            post :create,
                params: {
                    :format => :json,
                    :record => {
                        hiring_manager_id: hiring_manager.id,
                        candidate_id: candidate.id
                    }
                }
            assert_200
            expect(body_json['contents']['hiring_relationships'][0]['career_profile']['anonymized']).to be(false)
        end

        def cause_multiple_connections_error(hiring_manager, teammate, candidate, extra_params, extra_meta = {})
            allow(controller).to receive(:current_user).and_return(hiring_manager)

            HiringRelationship.create!({
                hiring_manager_id: teammate.id,
                candidate_id: candidate.id,
                hiring_manager_status: 'accepted',
                candidate_status: 'pending'
            })

            expect(controller.current_ability).to receive(:can?).and_return(true).at_least(1).times
            post :create,
                params: {
                    :format => :json,
                    :record => {
                        hiring_manager_id: hiring_manager.id,
                        candidate_id: candidate.id
                    }.merge(extra_params),
                    :meta => {
                        using_new_receipts_syntax: true
                    }.merge(extra_meta)
                }
        end

        def assert_406_on_multiple_connections_error(hiring_manager, teammate, candidate, extra_params, extra_meta = {})
            cause_multiple_connections_error(hiring_manager, teammate, candidate, extra_params, extra_meta)
            expect(response.status).to eq(406) # not_acceptable
            expect(body_json["message"]).to eq("Validation failed: Candidate has already been accepted by your team")
            expect(@controller.meta["error_type"]).to eq("multiple_team_connections_to_candidate")
        end

        it "should work and set the hiring_manager_status as 'accepted' if record not unique" do
            hiring_relationship = HiringRelationship.where(hiring_manager_status: "pending").first
            allow(controller).to receive(:current_user).and_return(hiring_relationship.hiring_manager)
            expect(controller.current_ability).to receive(:can?).and_return(true)
            post :create,
                params: {
                    :format => :json,
                    :record => {
                        hiring_manager_id: hiring_relationship.hiring_manager_id,
                        candidate_id: hiring_relationship.candidate_id,
                        # this is what's expected if the hiring manager "likes" or chooses
                        # to "connect" with a candidate from the marketing hiring pages
                        hiring_manager_status: "accepted"
                    }
                }
            assert_200
            expect(HiringRelationship.where(hiring_manager_id: hiring_relationship.hiring_manager_id, candidate_id: hiring_relationship.candidate.id).first.hiring_manager_status).to eq("accepted")
        end

        it "should not override hiring_manager_status if record not unique and hiring_manager_status not supplied" do
            hiring_relationship = HiringRelationship.where(hiring_manager_status: "pending").first
            allow(controller).to receive(:current_user).and_return(hiring_relationship.hiring_manager)
            expect(controller.current_ability).to receive(:can?).and_return(true)
            post :create,
                params: {
                    :format => :json,
                    :record => {
                        hiring_manager_id: hiring_relationship.hiring_manager_id,
                        candidate_id: hiring_relationship.candidate_id
                    }
                }
            assert_200
            expect(HiringRelationship.where(hiring_manager_id: hiring_relationship.hiring_manager_id, candidate_id: hiring_relationship.candidate.id).first.hiring_manager_status).to eq("pending")
        end

        it "should override hiring_manager_status if record not unique and hiring_manager_status supplied" do
            hiring_relationship = HiringRelationship.where(hiring_manager_status: "pending").first
            allow(controller).to receive(:current_user).and_return(hiring_relationship.hiring_manager)
            expect(controller.current_ability).to receive(:can?).and_return(true)
            post :create,
                params: {
                    :format => :json,
                    :record => {
                        hiring_manager_id: hiring_relationship.hiring_manager_id,
                        candidate_id: hiring_relationship.candidate_id,
                        hiring_manager_status: "saved_for_later"
                    }
                }
            assert_200
            expect(HiringRelationship.where(hiring_manager_id: hiring_relationship.hiring_manager_id, candidate_id: hiring_relationship.candidate.id).first.hiring_manager_status).to eq("saved_for_later")
        end

        it "should not override open_position_id if record not unique and open_position_id not supplied" do
            hiring_relationship = HiringRelationship.where.not(open_position_id: nil).first
            allow(controller).to receive(:current_user).and_return(hiring_relationship.hiring_manager)
            expect(controller.current_ability).to receive(:can?).and_return(true)
            post :create,
                params: {
                    :format => :json,
                    :record => {
                        hiring_manager_id: hiring_relationship.hiring_manager_id,
                        candidate_id: hiring_relationship.candidate_id
                    }
                }
            assert_200
            expect(hiring_relationship.reload.open_position_id).not_to be_nil
        end

        it "should override open_position_id if record not unique and open_position_id supplied" do
            hiring_relationship = HiringRelationship.where(open_position_id: nil).first
            open_position = OpenPosition.first
            allow(controller).to receive(:current_user).and_return(hiring_relationship.hiring_manager)
            expect(controller.current_ability).to receive(:can?).and_return(true)
            post :create,
                params: {
                    :format => :json,
                    :record => {
                        hiring_manager_id: hiring_relationship.hiring_manager_id,
                        candidate_id: hiring_relationship.candidate_id,
                        open_position_id: open_position.id
                    }
                }
            assert_200
            expect(hiring_relationship.reload.open_position_id).to eq(open_position.id)
        end

        it "should set found_with_search_id" do
            hiring_manager = HiringApplication.where(status: 'accepted').first.user
            candidate_id = User.where("id not in (#{hiring_manager.candidates.select('id').to_sql})").where("id <> '#{hiring_manager.id}'").first.id
            allow(controller).to receive(:current_user).and_return(hiring_manager)
            expect(controller.current_ability).to receive(:can?).and_return(true)

            expect {
                post :create,
                    params: {
                        :format => :json,
                        :record => {
                            hiring_manager_id: hiring_manager.id,
                            candidate_id: candidate_id,
                            hiring_manager_status: "accepted"
                        },
                        :meta => {
                            :found_with_search_id => 'search_id'
                        }
                    }
                    }.to change {HiringRelationship.count}.by(1)

            assert_200
            expect(controller.hiring_relationship.selection_details).to eq({:found_with_search_id=>"search_id"})
        end

        it "should call create_from_hash! on a conversation embedded in the hiring_relationship" do
            hiring_relationship = HiringRelationship.first
            hiring_manager = hiring_relationship.hiring_manager
            candidate = hiring_relationship.candidate
            candidate_position_interest = CandidatePositionInterest.first

            stub_conversation = Mailboxer::Conversation.first
            expect(stub_conversation).to receive(:as_json).at_least(1).times.and_return({'conversation' => 'updated_json'})

            expect(controller).to receive(:current_user).and_return(hiring_manager).at_least(1).times
            expect(Mailboxer::Conversation).to receive(:create_or_update_from_hash!)
                .with({'conversation' => 'json'}, hiring_manager, hiring_relationship)
                .and_return(stub_conversation)
            expect_any_instance_of(HiringRelationship).to receive(:new_message=)

            post :create,
                params: {
                    :format => :json,
                    :record => {
                        hiring_manager_id: hiring_manager.id,
                        candidate_id: candidate.id,
                        conversation: {'conversation' => 'json'}
                    },
                    meta: {
                        candidate_position_interest: candidate_position_interest.as_json(career_profile_options: {})
                    }
                }
            assert_200
            expect(HiringRelationship.where(hiring_manager_id: hiring_manager.id, candidate_id: candidate.id).first).not_to be(nil)
            expect(body_json['meta']['conversation']).to be_nil
            expect(hiring_relationship.reload.conversation.id).to eq(stub_conversation.id)
        end

        it "should save_open_position" do
            hiring_manager = users(:hiring_manager)
            candidate = User.where(can_edit_career_profile: true).where.not(id: hiring_manager.candidates.pluck('id')).first
            expect(controller).to receive(:current_user).and_return(hiring_manager).at_least(1).times
            expect(controller.current_ability).to receive(:cannot?).and_return(false).at_least(1).times
            expect(controller).to receive(:save_open_position).and_call_original
            open_position_id = SecureRandom.uuid


            post :create,
                params: {
                    :format => :json,
                    :record => {
                        hiring_manager_id: hiring_manager.id,
                        candidate_id: candidate.id,
                        open_position_id: open_position_id
                    },
                    :meta => {
                        "open_position" => {
                            'id' => open_position_id,
                            'hiring_manager_id' => hiring_manager.id,
                            'title' => 'title',
                            'available_interview_times' => 'tuesday'
                        }
                    }
                }
            assert_200
        end

        it "should save a candidate's position interest" do
            hiring_manager = users(:hiring_manager)
            candidate = User.where(can_edit_career_profile: true).where.not(id: hiring_manager.candidates.pluck('id')).first

            expect(controller).to receive(:current_user).and_return(hiring_manager).at_least(1).times
            expect(controller.current_ability).to receive(:cannot?).and_return(false).at_least(1).times
            open_position = hiring_manager.open_positions.first
            interest = CandidatePositionInterest.create!({
                open_position_id: open_position.id,
                candidate_id: candidate.id,
                candidate_status: 'accepted',
                hiring_manager_status: 'unseen'
            })

            # If we ever do send meta to update_from_hash! we need to ensure that last_curated_at is not permitted
            expect(CandidatePositionInterest).to receive(:update_from_hash!).with(an_instance_of(ActionController::Parameters), nil, nil, hiring_manager).and_call_original

            post :create,
                params: {
                    :format => :json,
                    :record => {
                        hiring_manager_id: hiring_manager.id,
                        candidate_id: candidate.id,
                        open_position_id: open_position.id,
                        hiring_manager_status: 'rejected'
                    },
                    :meta => {
                        "candidate_position_interest" => {
                            'id' => interest.id,
                            'hiring_manager_status' => CandidatePositionInterest.reviewed_hiring_manager_statuses.first
                        }
                    }
                }

            assert_200
            expect(interest.reload.hiring_manager_status).to eq(CandidatePositionInterest.reviewed_hiring_manager_statuses.first)
            expect(body_json['meta']['candidate_position_interest']['hiring_manager_status']).to eq(CandidatePositionInterest.reviewed_hiring_manager_statuses.first)
            expect(body_json['meta']['candidate_position_interest']['id']).to eq(interest.id)
        end

        it "should save a candidate's position interest even when the relationship has already been liked by the team" do
            hiring_manager = users(:hiring_manager_with_team)
            teammate = users(:hiring_manager_teammate)
            candidate = User.where(can_edit_career_profile: true).where.not(id: hiring_manager.candidates.pluck('id')).first

            open_position = hiring_manager.open_positions.first
            interest = CandidatePositionInterest.create!({
                open_position_id: open_position.id,
                candidate_id: candidate.id,
                candidate_status: 'accepted',
                hiring_manager_status: 'unseen'
            })

            assert_406_on_multiple_connections_error(hiring_manager, teammate, candidate, {
                hiring_manager_status: 'accepted'
            }, {
                "candidate_position_interest" => {
                    'id' => interest.id,
                    'hiring_manager_status' => CandidatePositionInterest.reviewed_hiring_manager_statuses.first
                }
            })

            expect(interest.reload.hiring_manager_status).to eq(CandidatePositionInterest.reviewed_hiring_manager_statuses.first)
            expect(body_json['meta']['candidate_position_interest']['hiring_manager_status']).to eq(CandidatePositionInterest.reviewed_hiring_manager_statuses.first)
            expect(body_json['meta']['candidate_position_interest']['id']).to eq(interest.id)
        end

        it "should handle conflict if interest has been saved since being loaded" do
            hiring_manager = users(:hiring_manager_with_team)
            teammate = users(:hiring_manager_teammate)
            open_position = hiring_manager.open_positions.first
            candidate = User.where(can_edit_career_profile: true).where.not(id: hiring_manager.candidates.pluck('id')).first
            interest = CandidatePositionInterest.create!({
                open_position_id: open_position.id,
                candidate_id: candidate.id,
                candidate_status: 'accepted',
                hiring_manager_status: CandidatePositionInterest.reviewed_hiring_manager_statuses.first
            })

            expect(CandidatePositionInterest).to receive(:update_from_hash!).and_raise(CandidatePositionInterest::OldVersion)
            expect(controller).to receive(:current_user).and_return(teammate).at_least(1).times
            allow(controller.current_ability).to receive(:cannot?).and_call_original
            allow(controller.current_ability).to receive(:cannot?).with(:update, an_instance_of(CandidatePositionInterest)).and_return(false)
            post :create,
                params: {
                    :format => :json,
                    :record => {
                        hiring_manager_id: teammate.id,
                        candidate_id: candidate.id,
                        open_position_id: open_position.id,
                        hiring_manager_status: 'rejected'
                    },
                    :meta => {
                        "candidate_position_interest" => {
                            'id' => interest.id,
                            'hiring_manager_status' => 'unseen',
                            'updated_at' => (interest.updated_at - 1.minute).to_timestamp
                        }
                    }
                }
            assert_error_response(409)
            expect(body_json['message']).to eq('Interest has been saved more recently')
            expect(body_json['meta']['error_type']).to eq('interest_conflict')
            expect(body_json['meta']['candidate_position_interest']['id']).to eq(interest.id)
            expect(body_json['meta']['candidate_position_interest']['career_profile']['anonymized']).to eq(false);
        end

        # https://trello.com/c/W5HDtTYw
        # https://trello.com/c/zZbE7Nmc
        it "should appropriately set up new message logging when creating a new relationship in the accepted state" do
            hiring_manager = users(:hiring_manager)
            candidate = users(:learner)
            expect(hiring_manager.candidate_relationships.find_by_candidate_id(candidate.id)).to be(nil) # sanity check

            allow(controller).to receive(:current_user).and_return(hiring_manager)
            start = Time.now
            post :create,
                params: {
                    :format => :json,
                    :record => {
                        hiring_manager_id: hiring_manager.id,
                        candidate_id: candidate.id,
                        hiring_manager_status: 'accepted',
                        candidate_status: 'pending',
                        conversation: {
                            subject: "new subject",
                            messages: [{
                                "body" => "new message body",
                                "sender_id" => hiring_manager.id,
                                "sender_type" => 'user',
                                "metadata" => {"some" => "metadata"},
                                "receipts" => [{
                                    "receiver_id" => hiring_manager.id
                                }]
                            }],
                            recipients: [
                                {
                                    'type' => 'user',
                                    'id' => candidate.id
                                },
                                {
                                    'type' => 'user',
                                    'id' => hiring_manager.id
                                }]
                        }
                    },
                    :meta => {'using_new_receipts_syntax' => true}
                }

            assert_200
            accepted_events = Event.where("created_at > ?", start).where(event_type: 'hiring_relationship:accepted')
            expect(accepted_events.size).to be(1)
            event = accepted_events.first
            expect(event.user).to eq(candidate)
            expect(event.payload['new_message'] && event.payload['new_message']['body']).to eq("new message body")

            # since two events happened at the same time which would normally trigger an email
            # to the candidate, we want to make sure we only send one of them.  We use just_accepted_relationship
            # to turn off the message_received email in customer.io and only send the "a hiring manager liked you" email
            message_received_event = Event.where("created_at > ?", start).where(event_type: 'mailboxer:message_received').first
            expect(message_received_event.payload['just_accepted_relationship']).to be(true)
        end

        # ideally, we would be able to test these things in unit tests and rely on stuff
        # working out here as well, but we've seen so many ways for it to be broken here even
        # if the simple case tested in unit specs works that we need to add integration specs
        # here.
        describe "side_effects" do

            it "should support candidate_position_interest side effect on update" do
                candidate_position_interest = CandidatePositionInterest.find_by_hiring_manager_status('unseen')
                hiring_manager = candidate_position_interest.hiring_manager

                ensure_fresh_hiring_team(hiring_manager)

                hiring_relationship = HiringRelationship.create!(
                    hiring_manager_id: hiring_manager.id,
                    candidate_id: candidate_position_interest.candidate_id,
                    hiring_manager_status: 'saved_for_later'
                )

                expect(controller).to receive(:current_user).at_least(1).times.and_return(hiring_manager)
                put :update,
                    params: {
                        :format => :json,
                        :record => hiring_relationship.as_json.merge(hiring_manager_status: 'accepted')
                    }
                assert_200

                expect(candidate_position_interest.reload.hiring_manager_status).to eq('hidden')
                expect(body_json['meta']['candidate_position_interests']).not_to be_nil
                expect(body_json['meta']['candidate_position_interests'].map { |e| e['id']}).to include(candidate_position_interest.id)
            end

            it "should support candidate_position_interest side effect on create" do
                candidate_position_interest = CandidatePositionInterest.find_by_hiring_manager_status('unseen')
                hiring_manager = candidate_position_interest.hiring_manager

                ensure_fresh_hiring_team(hiring_manager)

                hiring_relationship = HiringRelationship.new(
                    hiring_manager_id: hiring_manager.id,
                    candidate_id: candidate_position_interest.candidate_id,
                    hiring_manager_status: 'accepted'
                )

                expect(controller).to receive(:current_user).at_least(1).times.and_return(hiring_manager)
                post :create,
                    params: {
                        :format => :json,
                        :record => hiring_relationship.as_json
                    }
                assert_200

                expect(candidate_position_interest.reload.hiring_manager_status).to eq('hidden')
                expect(body_json['meta']['candidate_position_interests']).not_to be_nil
                expect(body_json['meta']['candidate_position_interests'].map { |e| e['id']}).to include(candidate_position_interest.id)
            end

            it "should support candidate_position_interest side effect on create with a conversation" do
                candidate_position_interest = CandidatePositionInterest.find_by_hiring_manager_status('unseen')
                hiring_manager = candidate_position_interest.hiring_manager

                ensure_fresh_hiring_team(hiring_manager)

                hiring_relationship = HiringRelationship.new(
                    hiring_manager_id: hiring_manager.id,
                    candidate_id: candidate_position_interest.candidate_id,
                    hiring_manager_status: 'accepted'
                )

                expect(controller).to receive(:current_user).at_least(1).times.and_return(hiring_manager)
                post :create,
                    params: {
                        :format => :json,
                        :record => hiring_relationship.as_json.merge(
                            conversation: new_conversation_json(hiring_relationship.hiring_manager_id, hiring_relationship.candidate_id)
                        )
                    }
                assert_200

                expect(candidate_position_interest.reload.hiring_manager_status).to eq('hidden')
                expect(body_json['meta']['candidate_position_interests']).not_to be_nil
                expect(body_json['meta']['candidate_position_interests'].map { |e| e['id']}).to include(candidate_position_interest.id)
            end

            it "should support hiring_relationship side effect on update" do
                hiring_manager = users(:hiring_manager_with_team)
                teammate = users(:hiring_manager_teammate)
                teammate.candidate_relationships.destroy_all

                hiring_manager_relationship = nil
                teammate_relationship = nil

                teammate_relationship = HiringRelationship.create!(hiring_manager_id: teammate.id, candidate_id: users(:learner).id, hiring_manager_status: 'pending')
                hiring_manager_relationship = HiringRelationship.create!(hiring_manager_id: hiring_manager.id, candidate_id: users(:learner).id, hiring_manager_status: 'saved_for_later')
                expect(controller).to receive(:current_user).at_least(1).times.and_return(hiring_manager)
                put :update,
                    params: {
                        :format => :json,
                        :record => hiring_manager_relationship.as_json.merge(hiring_manager_status: 'accepted')
                    }
                assert_200

                expect(teammate_relationship.reload.hiring_manager_status).to eq('hidden')
                expect(body_json['meta']['hiring_relationships']).not_to be_nil
                expect(body_json['meta']['hiring_relationships'].map { |e| e['id']}).to include(teammate_relationship.id)
            end

            it "should support hiring_relationship side effect on create" do
                hiring_manager = users(:hiring_manager_with_team)
                teammate = users(:hiring_manager_teammate)
                teammate.candidate_relationships.destroy_all

                hiring_manager_relationship = nil
                teammate_relationship = nil

                teammate_relationship = HiringRelationship.create!(hiring_manager_id: teammate.id, candidate_id: users(:learner).id, hiring_manager_status: 'pending')
                hiring_manager_relationship = HiringRelationship.new(hiring_manager_id: hiring_manager.id, candidate_id: users(:learner).id, hiring_manager_status: 'accepted')
                expect(controller).to receive(:current_user).at_least(1).times.and_return(hiring_manager)
                post :create,
                    params: {
                        :format => :json,
                        :record => hiring_manager_relationship.as_json
                    }
                assert_200

                expect(teammate_relationship.reload.hiring_manager_status).to eq('hidden')
                expect(body_json['meta']['hiring_relationships']).not_to be_nil
                expect(body_json['meta']['hiring_relationships'].map { |e| e['id']}).to include(teammate_relationship.id)

            end
        end
    end

    describe "save_open_position" do

        before(:each) do
            open_position_id = SecureRandom.uuid
            allow(controller).to receive(:current_user).and_return(users(:admin))
            controller.params = {
                record: {
                    open_position_id: open_position_id
                },
                meta: {
                    open_position: {
                        id: open_position_id,
                        hiring_manager_id: SecureRandom.uuid
                    }
                }
            }
            controller.verify_open_position_params
        end

        it "should add_last_updated_to_push_messages before create_or_update_from_hash! and after" do
            expect(controller).to receive(:add_last_updated_to_push_messages).with(:open_position, "before").ordered
            expect(OpenPosition).to receive(:create_or_update_from_hash!).with(controller.open_position_params).ordered
            expect(controller).to receive(:add_last_updated_to_push_messages).with(:open_position, "current").ordered
            controller.save_open_position
        end

        it "should add open_position to response meta" do
            mock_open_position = double("OpenPosition instance")
            allow(OpenPosition).to receive(:create_or_update_from_hash!).with(controller.open_position_params).and_return(mock_open_position)
            expected_json = {
                foo: 'bar'
            }
            expect(mock_open_position).to receive(:as_json).and_return(expected_json)
            controller.save_open_position
            expect(controller.meta['open_position']).to be(expected_json)
        end
    end

    describe "verify_open_position_params" do

        it "should raise if user trying to update someone else's position" do
            open_position_id = SecureRandom.uuid
            allow(controller).to receive(:current_user).and_return(users(:hiring_manager_with_team))
            controller.params = {
                record: {
                    open_position_id: open_position_id
                },
                meta: {
                    open_position: {
                        id: open_position_id,
                        hiring_manager_id: SecureRandom.uuid
                    }
                }
            }
            expect(controller).to receive(:render_unauthorized_error)
            controller.verify_open_position_params
        end

        it "should allow admin to update someone else's position" do
            open_position_id = SecureRandom.uuid
            allow(controller).to receive(:current_user).and_return(users(:admin))
            controller.params = {
                record: {
                    open_position_id: open_position_id
                },
                meta: {
                    open_position: {
                        id: open_position_id,
                        hiring_manager_id: SecureRandom.uuid
                    }
                }
            }
            expect(controller).not_to receive(:render_unauthorized_error)
            controller.verify_open_position_params
        end

        it "should raise if the open position is not attached to the hiring relationship" do
            user = users(:hiring_manager_with_team)
            allow(controller).to receive(:current_user).and_return(user)
            expect(controller.current_ability).to receive(:cannot?).and_return(false).at_least(1).times
            controller.params = {
                record: {
                    open_position_id: SecureRandom.uuid
                },
                meta: {
                    open_position: {
                        hiring_manager_id: user.id,
                        id: SecureRandom.uuid
                    }
                }
            }
            expect(controller).to receive(:render_error_response).with("open_position in meta does not match id in relationship", :not_acceptable)
            controller.verify_open_position_params
        end
    end

    describe "POST batch_create" do

        it "should 401 if non-admin" do
            stub_current_user
            career_profile_list = CareerProfileList.first
            hiring_relationship = HiringRelationship.first
            post :batch_create,
                params: {
                    :career_profile_list_id => career_profile_list.id,
                    :hiring_manager_id => hiring_relationship.hiring_manager_id,
                    :career_profile_ids => career_profile_list.career_profile_ids,
                    :format => :json
                }
            assert_error_response(401)
        end

        it "should create hiring relationships for all candidates in a career profile list" do
            hiring_manager = HiringRelationship.first.hiring_manager
            hiring_manager.candidate_relationships.destroy_all
            hiring_manager.reload
            expect(hiring_manager.candidate_relationships.empty?).to be(true)

            career_profile_list = CareerProfileList.first
            stub_current_user(controller, [:admin])
            post :batch_create,
                params: {
                    :career_profile_list_id => career_profile_list.id,
                    :hiring_manager_id => hiring_manager.id,
                    :career_profile_ids => career_profile_list.career_profile_ids,
                    :format => :json
                }
            assert_200

            hiring_relationships = hiring_manager.candidate_relationships
            expect(hiring_relationships.empty?).not_to be(true)
            career_profiles = CareerProfile.select(:user_id).where(id: career_profile_list.career_profile_ids)
            candidate_ids = hiring_relationships.pluck(:candidate_id)
            career_profiles.each do |career_profile|
                expect(candidate_ids).to include(career_profile.user_id)
            end
        end

        it "should gracefully handle creating a duplicate hiring relationship for a candidate" do
            hiring_manager = HiringRelationship.first.hiring_manager
            hiring_manager.candidate_relationships.destroy_all

            career_profile_list = CareerProfileList.first
            career_profile = CareerProfile.find(career_profile_list.career_profile_ids[0])

            HiringRelationship.create!({
                hiring_manager_id: hiring_manager.id,
                candidate_id: career_profile.user_id
            })

            stub_current_user(controller, [:admin])
            post :batch_create,
                params: {
                    :career_profile_list_id => career_profile_list.id,
                    :hiring_manager_id => hiring_manager.id,
                    :career_profile_ids => career_profile_list.career_profile_ids,
                    :format => :json
                }
            assert_200
        end

    end

    def new_conversation_json(sender_id, recipient_id)
        {
            subject: "new subject",
            messages: [{
                "body" => "new message body",
                "sender_id" => sender_id,
                "sender_type" => 'user'
            }],
            recipients: [
                {
                    'type' => 'user',
                    'id' => recipient_id
                },
                {
                    'type' => 'user',
                    'id' => sender_id
                }]
        }
    end

    def ensure_fresh_hiring_team(hiring_manager)
        if hiring_manager.hiring_team
            hiring_manager.hiring_team.owner_id = nil
            hiring_manager.hiring_team.save!
        end
        hiring_manager.hiring_team = HiringTeam.create!({domain: 'freshteam.com', title: "Fresh Team"})
        hiring_manager.save!
    end

    def get_hiring_manager_and_unrelated_candidate
        hiring_manager = users(:hiring_manager)
        candidate = User.joins(:career_profile).where.not(id: hiring_manager.candidates.pluck('id')).first
        candidate.update(can_edit_career_profile: true)
        [hiring_manager, candidate]
    end

end