require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::StudentDashboardsController do
    include ControllerSpecHelper
    json_endpoint

    describe "index" do

        before(:each) do
            stub_current_user(controller, [:learner])
            stub_client_params(controller)
            SafeCache.clear # refresh Playlist.stream_locale_pack_ids_for_playlist
        end

        it "should return instances" do
            stub_to_json_from_api_params_call do |params|
                expect(params[:user]).to be(controller.current_user)
            end
            allow(controller).to receive(:update_last_seen_at)
            expect(controller).to receive(:set_user_push_messages)

            get :index, params: { :format => :json }

            assert_successful_response({
                "student_dashboards" => []
            })
        end

        it "should 401 if no current_user" do
            stub_no_current_user(controller)
            get :index, params: { :format => :json }
            assert_error_response(401)
        end

        it "should set user_can_see filter if no view_as param" do
            stub_current_user(controller, [:learner])

            stub_to_json_from_api_params_call do |params|
                expect(params[:filters]).to eq({
                    "user_can_see" => true,
                    "in_users_locale_or_en" => true
                })
            end

            get :index, params: { :format => :json }
        end

        it "should call add_num_recommended_positions_to_push_messages" do
            expect(controller).to receive(:add_num_recommended_positions_to_push_messages)
            get :index, params: { :format => :json }
        end

        describe "with get_has_available_incomplete_streams param" do

            it "should call get_has_available_incomplete_streams if requested and return false if there are none" do
                call_index_with_get_has_available_incomplete_streams("[]")

                # since we mocked out ToJsonFromApiParams to return an empty array,
                # this should be false
                expect(body_json['meta']['has_available_incomplete_streams']).to eq(false)
            end


            it "should call get_has_available_incomplete_streams if requested and return true if there are some" do
                call_index_with_get_has_available_incomplete_streams("[{}]")

                # since we mocked out ToJsonFromApiParams to return a result,
                # this should be true
                expect(body_json['meta']['has_available_incomplete_streams']).to eq(true)
            end

            def call_index_with_get_has_available_incomplete_streams(response)
                stub_current_user(controller, [:learner])

                expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                    expect(to_json_instance.params[:user]).to be(controller.current_user)
                    expect(to_json_instance.params[:filters]).to eq({
                        published: true,
                        user_can_see: true,
                        in_users_locale_or_en: true,
                        complete:  false
                    }.stringify_keys)
                    expect(to_json_instance.params[:fields]).to eq([:id])
                    expect(to_json_instance.params[:limit]).to eq(1)
                    response
                end

                get :index, params: { :get_has_available_incomplete_streams => true, :format => :json }
            end
        end

        describe "with view_as param" do

            it "should not allow a non-admin to view_as" do
                stub_current_user(controller, [:learner])
                get :index, params: { :filters => {:view_as => 'group:RESTRICTED'}, :format => :json }
                assert_error_response(401)
            end

           it "should allow admin to view_as anything" do
                stub_current_user(controller, [:admin])


                stub_to_json_from_api_params_call do |params|
                    expect(params[:filters]).to eq({
                        'view_as' => 'group:RESTRICTED',
                        'in_locale_or_en' => controller.current_user.locale
                    })
                end
                get :index, params: { :filters => {:view_as => 'group:RESTRICTED'}, :format => :json }

                assert_successful_response({
                    "student_dashboards" => []
                })
            end
        end

        def stub_to_json_from_api_params_call(&block)

            stub_to_json_from_api_params = {}
            allow(stub_to_json_from_api_params).to receive(:json) do
                "[]"
            end
             expect(StudentDashboard::ToJsonFromApiParams).to receive(:new) do |params|
                yield(params)
                stub_to_json_from_api_params
             end
        end


    end
end