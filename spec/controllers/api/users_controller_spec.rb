require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::UsersController do
    # TODO: extract what we need out of ApiContentControllerSpecHelper into another shared helper
    include ControllerSpecHelper
    json_endpoint

    include StripeHelper

    fixtures(:users, :roles, :lesson_streams, :education_experiences)

    before (:each) do
        Institution.find_or_create_by(name: "JLL")
        stub_client_params(controller)
        stub_current_user(controller, [:admin])
        create_default_plan
    end

    describe "GET 'index'" do

        it 'should 401 if not admin' do
            stub_current_user(controller, [:learner], ['SMARTER'])
            get :index, params: {format: :json }
            assert_error_response(401)
        end

        it 'should work as a POST too' do
            post :index, params: {format: :json }
            assert_200
        end

        it "should be successful and send back interleaved roles and roles and institutions" do
            ensure_less_than_100_users

            get :index, params: {format: :json }
            expect(response).to be_successful, response.body
            expect(body_json["contents"]["users"].size).to eq(User.count)

            user_json =  body_json["contents"]["users"].detect { |entry| entry['id'] == controller.current_user.id }
            expect(user_json).not_to be_nil
            groups = user_json["groups"].map { |group| group['name'] }
            expect(groups.sort).to match_array(controller.current_user.groups.map(&:name))
            expect(user_json["roles"][0]["name"]).to eq("admin")

            expect(body_json["meta"]).not_to be_nil
            expect(body_json["meta"]["global_roles"]).not_to be_nil
            expect(body_json["meta"]["scoped_roles"]).not_to be_nil
            expect(body_json["meta"]["available_groups"]).not_to be_nil
            expect(body_json["meta"]["institutions"]).not_to be_nil
        end

        it "should exclude favorite_lesson_stream_locale_packs" do
            get :index, params: {format: :json }
            expect(response).to be_successful, response.body
            expect(body_json["contents"]["users"][0].key?('favorite_lesson_stream_locale_packs')).to be(false)
        end

        it "should exclude cannot_create from global roles" do
            Role.find_or_create_by(name: "cannot_create")
            get :index, params: {format: :json }
            expect(response).to be_successful, response.body
            expect(body_json["meta"]).not_to be_nil
            expect(body_json["meta"]["global_roles"]).not_to be_nil
            expect(body_json["meta"]["global_roles"].map { |e| e['name'] }.include?('cannot_create')).to be(false)
        end

        it "should include cohorts in the meta without using as_json (for optimization)" do
            cohort = Cohort.where(program_type: 'mba').where.not(start_date:nil).where.not(was_published: nil).first
            expect_any_instance_of(Cohort).not_to receive(:as_json)
            get :index, params: {format: :json }
            expect(response).to be_successful, response.body
            cohort_meta_json = body_json["meta"]["cohorts"].detect { |json| json["name"] == cohort.name }
            expect(cohort_meta_json["name"]).to eq(cohort.name)
            expect(cohort_meta_json["start_date"]).to eq(cohort.start_date.to_timestamp)
        end

        it "should filter by roles" do

            ensure_less_than_100_users

            expect(roles(:admin).users.size > 0).to be(true)
            get :index, params: {role_id: roles(:admin).id, format: :json}
            expect(response).to be_successful, response.body
            expect(body_json["contents"]["users"].size).to eq(roles(:admin).users.size)

            get :index, params: {role_id: roles(:learner).id, format: :json}
            expect(roles(:learner).users.size > 0).to be(true)
            expect(response).to be_successful, response.body
            expect(body_json["contents"]["users"].size).to eq(roles(:learner).users.size)
        end

        it "should filter by groups" do
            AccessGroup.create!(name: 'group_1')
            AccessGroup.create!(name: 'group_2')

            @group_1_user = FactoryBot.create(:user)
            @group_1_user.add_to_group('group_1')
            @group_1_user.add_role('learner') # users must have roles or they will be ignored

            @group_2_user = FactoryBot.create(:user)
            @group_2_user.add_to_group('group_2')
            @group_2_user.add_role('learner')

            get :index, params: {group_name: 'group_1', format: :json}
            expect(response).to be_successful, response.body
            expect(body_json["contents"]["users"].size).to eq(1)
            expect(body_json["contents"]["users"][0]["id"]).to eq(@group_1_user.id)

            get :index, params: {group_name: 'group_2', format: :json}
            expect(response).to be_successful, response.body
            expect(body_json["contents"]["users"].size).to eq(1)
            expect(body_json["contents"]["users"][0]["id"]).to eq(@group_2_user.id)
        end

        it "should filter by hiring_application_status" do
            ensure_less_than_100_users

            pending_user = User.left_outer_joins(:hiring_application)
                            .where("hiring_applications.id is null")
                            .where.not(email: nil) # need an email address so that domain can be set when auto-creating the hiring team
                            .first

            pending_user.professional_organization = ProfessionalOrganizationOption.reorder(:text).first
            pending_user.save!

            HiringApplication.create!({
                user_id: pending_user.id,
                applied_at: Time.now,
                status: 'pending'
            })

            get :index, params: {hiring_application_status: 'pending', format: :json}
            expect(response).to be_successful, response.body
            user_entry = body_json["contents"]["users"].detect { |u| u['id'] == pending_user.id }
            expect(user_entry).not_to be_nil
            expect(user_entry['hiring_application']['id']).to eq(pending_user.hiring_application.id)
            entry_with_no_application = body_json["contents"]["users"].detect { |u| u['hiring_application'].nil? }
            expect(entry_with_no_application).to be_nil
        end


        it "should filter by cohort and cohort status" do
            ensure_less_than_100_users

            cohort = Cohort.first
            first_application = CohortApplication.first
            second_application = CohortApplication.second

            first_application.cohort = cohort
            first_application.status = 'accepted'
            first_application.cohort_slack_room_id = cohort.slack_rooms[0]&.id
            first_application.graduation_status = 'graduated'
            first_application.save!

            second_application.cohort = cohort
            second_application.status = 'accepted'
            second_application.cohort_slack_room_id = cohort.slack_rooms[0]&.id
            second_application.graduation_status = 'graduated'
            second_application.save!

            expected_user_ids = CohortApplication.where(cohort_id: cohort.id, status: 'accepted').pluck('user_id')

            # legacy endpoint (e.g.: batch editor, admin users)
            get :index, params: {cohort_id: cohort.id, cohort_status: 'accepted', format: :json}
            expect(response).to be_successful, response.body
            ids = body_json["contents"]["users"].map { |u| u['id'] }
            expect(ids).to match_array(expected_user_ids)

            # new endpoint; when the params are passed together into cohort_status
            get :index, params: { filters: {cohort_id: [cohort.id], cohort_status: ['accepted']}, format: :json}
            expect(response).to be_successful, response.body
            ids = body_json["contents"]["users"].map { |u| u['id'] }
            expect(ids).to match_array(expected_user_ids)

        end

        it "should filter by cohort and graduation status" do
            ensure_less_than_100_users

            cohort = Cohort.first
            first_application = CohortApplication.first
            second_application = CohortApplication.second

            first_application.cohort = cohort
            first_application.status = 'accepted'
            first_application.cohort_slack_room_id = cohort.slack_rooms[0]&.id
            first_application.graduation_status = 'graduated'
            first_application.save!

            second_application.cohort = cohort
            second_application.status = 'accepted'
            second_application.cohort_slack_room_id = cohort.slack_rooms[0]&.id
            second_application.graduation_status = 'graduated'
            second_application.save!

            expected_user_ids = CohortApplication.where(cohort_id: cohort.id, graduation_status: 'graduated').pluck('user_id')

            # legacy endpoint (e.g.: batch editor, admin users)
            get :index, params: {cohort_id: cohort.id, graduation_status: 'graduated', format: :json}
            expect(response).to be_successful, response.body
            ids = body_json["contents"]["users"].map { |u| u['id'] }
            expect(ids).to match_array(expected_user_ids)

            # new endpoint; when the params are passed together into cohort_status
            get :index, params: { filters: {cohort_id: [cohort.id], cohort_status: ['graduated']}, format: :json}
            expect(response).to be_successful, response.body
            ids = body_json["contents"]["users"].map { |u| u['id'] }
            expect(ids).to match_array(expected_user_ids)

        end

        it "should filter by cohort and status and graduation status" do
            ensure_less_than_100_users

            cohort = Cohort.first
            first_application = CohortApplication.first
            second_application = CohortApplication.second

            first_application.cohort = cohort
            first_application.status = 'accepted'
            first_application.cohort_slack_room_id = cohort.slack_rooms[0]&.id
            first_application.graduation_status = 'graduated'
            first_application.save!

            second_application.cohort = cohort
            second_application.status = 'accepted'
            second_application.cohort_slack_room_id = cohort.slack_rooms[0]&.id
            second_application.graduation_status = 'graduated'
            second_application.save!

            # legacy endpoint (e.g.: batch editor, admin users); when passed separately, we expect the AND of the two conditions
            expected_user_ids = CohortApplication.where(cohort_id: cohort.id, status: 'accepted', graduation_status: 'graduated').pluck('user_id')

            get :index, params: {cohort_id: cohort.id, cohort_status: 'accepted', graduation_status: 'graduated', format: :json}
            expect(response).to be_successful, response.body
            ids = body_json["contents"]["users"].map { |u| u['id'] }
            expect(ids).to match_array(expected_user_ids)

            # new endpoint; when the params are passed together into cohort_status, we expect the OR of the conditions
            expected_user_ids = CohortApplication.where(cohort_id: cohort.id).where("cohort_applications.status = 'accepted' or cohort_applications.graduation_status = 'graduated'").pluck('user_id')

            get :index, params: { filters: {cohort_id: [cohort.id], cohort_status: ['accepted', 'graduated']}, format: :json}
            expect(response).to be_successful, response.body
            ids = body_json["contents"]["users"].map { |u| u['id'] }
            expect(ids).to match_array(expected_user_ids)
        end

        it "should filter for accepted_or_pre_accepted_registered" do
            ensure_less_than_100_users

            cohort = Cohort.first
            first_application = CohortApplication.first
            second_application = CohortApplication.second
            third_application = CohortApplication.third

            first_application.update_columns(cohort_id: cohort.id, status: 'accepted')
            second_application.update_columns(cohort_id: cohort.id, status: 'pre_accepted', registered: true)
            third_application.update_columns(cohort_id: cohort.id, status: 'pre_accepted', registered: false)

            expected_user_ids = CohortApplication.where(cohort_id: cohort.id).where("status = 'accepted' or (status = 'pre_accepted' and registered = true)").pluck('user_id')

            get :index, params: { filters: { cohort_id: cohort.id, cohort_status: ['accepted_or_pre_accepted_registered'] }, format: :json}
            expect(response).to be_successful, response.body
            ids = body_json["contents"]["users"].map { |u| u['id'] }
            expect(ids).to match_array(expected_user_ids)
            expect(ids).to include(first_application.user_id)
            expect(ids).to include(second_application.user_id)
            expect(ids).not_to include(third_application.user_id)
        end

        it "should filter out users by available_for_relationships_with_hiring_manager" do
            hiring_manager = HiringApplication.first.user
            professional_organization_option_id = hiring_manager.professional_organization_option_id

            some_users = User.limit(2)
            expect(User).to receive(:available_for_relationships).with(professional_organization_option_id).and_return(some_users)

            get :index, params: {
                    format: :json,
                    available_for_relationships_with_hiring_manager: hiring_manager.id
                }
            expect(response).to be_successful, response.body
            ids = body_json["contents"]["users"].map { |u| u['id'] }

            # we should get all the users that don't work for this company right now
            expect(ids).to match_array(some_users.pluck(:id))
        end

        it "should filter by interested_in_open_position" do
            position_interest = CandidatePositionInterest.first
            open_position = position_interest.open_position

            expected_users = open_position.interested_candidates
            expect(expected_users.size).to be > 0 # sanity check

            get :index, params: {
                format: :json,
                filters: {
                    interested_in_open_position: open_position.id
                }
            }
            expect(response).to be_successful, response.body
            ids = body_json["contents"]["users"].map { |u| u['id'] }
            expect(ids).to match_array(expected_users.pluck(:id))
        end

        describe "identifiers filter" do
            it "should filter by identifiers" do
                users = User.where.not(email: nil).limit(2)
                get :index, params: {identifiers: [users[0].id, users[1].email].to_json, format: :json}
                expect(body_json["contents"]["users"].size).to eq(2)
                expect(body_json["contents"]["users"].map { |e| e['id']}).to match_array(users.map(&:id))
            end

            it "should filter by identifiers when all are emails" do
                users = User.where.not(email: nil).limit(2)
                get :index, params: {identifiers: [users[0].email, users[1].email].to_json, format: :json}
                expect(body_json["contents"]["users"].size).to eq(2)
                expect(body_json["contents"]["users"].map { |e| e['id']}).to match_array(users.map(&:id))
            end

            it "should filter by identifiers when identifiers is empty" do
                get :index, params: {identifiers: [].to_json, format: :json}
                expect(body_json["contents"]["users"].size).to eq(0)
            end
        end

        describe "pagination" do

            before(:each) do
                stub_current_user(controller, [:admin], ["GROUP"])
                @users = FactoryBot.create_list(:user, 4)
                @users.each { |u| u.add_role('learner') }
            end

            it "should do basic pagination" do
                user_count = User.count
                if user_count.even?
                        destroyable_users.first.destroy
                        user_count = user_count - 1
                end
                get :index, params: {per_page: 2, page: 1, format: :json}
                expect(body_json["contents"]["users"].size).to eq(2)
                expect(body_json["meta"]["total_count"]).to eq(user_count)
                expect(body_json["meta"]["per_page"]).to eq(2)
                expect(body_json["meta"]["page"]).to eq(1)

                page = (user_count/2)+1
                get :index, params: {per_page: 2, page: page, format: :json}
                expect(body_json["contents"]["users"].size).to eq(1)
                expect(body_json["meta"]["total_count"]).to eq(user_count)
                expect(body_json["meta"]["per_page"]).to eq(2)
                expect(body_json["meta"]["page"]).to eq(page)
            end

            it "can filter by name" do
                random = 'blahblahblahblah'
                @users[0].name = "#{random} plus some more"
                @users[0].save
                @users[1].name = "#{random} something different"
                @users[1].save

                get :index, params: {per_page: 2, page: 1, freetext_search_param: random, format: :json}
                expect(body_json["contents"]["users"].size).to eq(2)
                expect(body_json["contents"]["users"].map { |e| e['id'] }.sort).to eq([@users[0], @users[1]].map { |e| e.id }.sort)

                expect(body_json["meta"]["total_count"]).to eq(2)
                expect(body_json["meta"]["per_page"]).to eq(2)
                expect(body_json["meta"]["page"]).to eq(1)

            end


            it "can filter by email" do
                email_part = 'blahblahblahblah'
                @users[1].email = "#{email_part}@pedago.com"
                @users[1].save

                get :index, params: {per_page: 2, page: 1, freetext_search_param: email_part, format: :json}
                expect(body_json["contents"]["users"].size).to eq(1)
                expect(body_json["contents"]["users"][0]['id']).to eq(@users[1].id)
                expect(body_json["meta"]["total_count"]).to eq(1)
                expect(body_json["meta"]["per_page"]).to eq(2)
                expect(body_json["meta"]["page"]).to eq(1)

            end

            it "can filter by group name" do
                group_name = "group#{rand}"
                AccessGroup.find_or_create_by(name: group_name)
                @users[0].add_to_group(group_name)
                @users[0].save

                get :index, params: {per_page: 5, page: 1, group_name: group_name, format: :json}
                expect(body_json["contents"]["users"].size).to eq(1)
                expect(body_json["contents"]["users"][0]['id']).to eq(@users[0].id)
                expect(body_json["contents"]["users"][0]['groups'][0]['name']).to eq(group_name)

            end

            it "can filter by role ID" do
                role_name = "role#{rand}"
                role = Role.find_or_create_by(name: role_name)
                @users[0].roles = [role]
                @users[0].save

                get :index, params: {per_page: 5, page: 1, role_id: role.id, format: :json}
                expect(body_json["contents"]["users"].size).to eq(1)
                expect(body_json["contents"]["users"][0]['id']).to eq(@users[0].id)
                expect(body_json["contents"]["users"][0]['roles'][0]['name']).to eq(role_name)

            end

            it "can filter by institution ID" do
                inst = Institution.joins(:users).first
                @users = inst.users

                get :index, params: {per_page: 999, page: 1, institution_id: inst.id, format: :json}

                expect(body_json["contents"]["users"].map { |e| e['id']}).to match_array(@users.map(&:id))
                expect(body_json["contents"]["users"][0]['institutions'][0]['id']).to eq(inst.id)

            end

            it "can filter by hiring_team_id" do
                hiring_team_id = HiringTeam.first.id

                user = User.where(hiring_team_id: nil).first
                user.hiring_team_id = hiring_team_id
                user.save!

                @users = User.where(hiring_team_id: hiring_team_id).to_a
                expect(@users.map(&:id)).to include(user.id) # sanity

                get :index, params: {per_page: 999, page: 1, hiring_team_id: hiring_team_id, format: :json}

                expect(body_json["contents"]["users"].map { |e| e['id']}).to match_array(@users.map(&:id))

            end

            it "should filter by available_for_hiring_team" do
                hiring_team_id = HiringTeam.first.id

                user = User.where(hiring_team_id: nil).first
                user.hiring_team_id = hiring_team_id
                user.save!

                @users = User.where("hiring_team_id IS NULL OR hiring_team_id = '#{hiring_team_id}'").to_a

                get :index, params: {per_page: 999, page: 1, available_for_hiring_team: hiring_team_id, format: :json}

                ids = body_json["contents"]["users"].map { |e| e['id']}
                expect(ids).to match_array(@users.map(&:id))
                expect(ids).to include(user.id)

            end

            it "can filter freetext_search_param on multiple params" do
                @users[0].email = "dogs@pedago.com"
                @users[0].save
                @users[1].nickname = "dogsandcats"
                @users[1].save

                get :index, params: {per_page: 3, page: 1, freetext_search_param: "dogs", format: :json}
                expect(body_json["contents"]["users"].size).to eq(2)
                expect(body_json["meta"]["total_count"]).to eq(2)
                expect(body_json["meta"]["per_page"]).to eq(3)
                expect(body_json["meta"]["page"]).to eq(1)
            end

            it "can filter by freetext_search_param within a specific group" do
                AccessGroup.find_or_create_by(name: "CATS")
                @users[0].add_to_group("CATS")
                @users[0].save
                @users[1].add_to_group("CATS")
                @users[1].email = "dogs@pedago.com"
                @users[1].save
                @users[2].email = "dogsandcats@pedago.com"
                @users[2].save

                get :index, params: {per_page: 3, page: 1, freetext_search_param: "dogs", group_name: "CATS", format: :json}
                expect(body_json["contents"]["users"].size).to eq(1)
                expect(body_json["contents"]["users"][0]["email"]).to eq("dogs@pedago.com")
                expect(body_json["meta"]["total_count"]).to eq(1)
                expect(body_json["meta"]["per_page"]).to eq(3)
                expect(body_json["meta"]["page"]).to eq(1)

            end

            it "can filter by freetext_search_param within a last_cohort_status" do
                cohort = Cohort.first
                @users[1].email = "dogs@pedago.com"
                @users[1].cohort_applications.destroy_all
                CohortApplication.create!({
                    cohort_id: cohort.id,
                    status: 'accepted',
                    cohort_slack_room_id: cohort.slack_rooms[0]&.id,
                    user_id: @users[1].id,
                    applied_at: Time.now - 5.year
                })
                @users[1].save

                @users[2].email = "dogsandcats@pedago.com"
                CohortApplication.create!({
                    cohort_id: cohort.id,
                    status: 'accepted',
                    cohort_slack_room_id: cohort.slack_rooms[0]&.id,
                    user_id: @users[2].id,
                    applied_at: Time.now - 5.year
                })
                @users[2].save

                get :index, params: {per_page: 3, page: 1, freetext_search_param: "dogs", last_cohort_status: "accepted", format: :json}
                expect(body_json["contents"]["users"].size).to eq(2)
                expect(body_json["contents"]["users"].map {|u| u["email"]}).to match_array(['dogsandcats@pedago.com', 'dogs@pedago.com'])
                expect(body_json["meta"]["total_count"]).to eq(2)
                expect(body_json["meta"]["per_page"]).to eq(3)
                expect(body_json["meta"]["page"]).to eq(1)

            end

            it "can filter freetext_search_param, email, and name on values containing aopstrophes" do
                expect {
                    get :index, params: {per_page: 999, page: 1, freetext_search_param: "THIS SHOULDN'T BREAK", format: :json}
                }.not_to raise_error()
                expect(body_json["contents"]["users"].size).to eq(0)

                expect {
                    get :index, params: {per_page: 999, page: 1, filters: { email: "THIS SHOULDN'T BREAK" }, format: :json}
                }.not_to raise_error()
                expect(body_json["contents"]["users"].size).to eq(0)

                expect {
                    get :index, params: {per_page: 999, page: 1, filters: { name: "THIS SHOULDN'T BREAK" }, format: :json}
                }.not_to raise_error()
                expect(body_json["contents"]["users"].size).to eq(0)
            end

            it "can gracefully handle no results" do
                AccessGroup.find_or_create_by(name: "CATS")
                @users[0].add_to_group("CATS")
                @users[0].save
                @users[1].add_to_group("CATS")
                @users[1].email = "dogs@pedago.com"
                @users[1].save
                @users[2].email = "dogsandcats@pedago.com"
                @users[2].save

                get :index, params: {per_page: 3, page: 1, freetext_search_param: "elephants", group_name: "CATS", format: :json}
                expect(body_json["contents"]["users"].size).to eq(0)
                expect(body_json["meta"]["total_count"]).to eq(0)
                expect(body_json["meta"]["per_page"]).to eq(3)
                expect(body_json["meta"]["page"]).to eq(1)

            end

        end

        describe "filters" do
            it "should filter by name" do
                ensure_less_than_100_users

                first_user = User.first
                second_user = User.second

                first_user.name = "Benjamin Bob"
                first_user.save!

                second_user.name = "Benjamin Smith"
                second_user.save!

                expected_user_ids = User.where("name ILIKE '\%benjamin\%'").pluck("id")

                get :index, params: {filters: {name: "benjamin"}, format: :json}
                expect(response).to be_successful, response.body
                ids = body_json["contents"]["users"].map { |u| u["id"] }
                expect(ids).to match_array(expected_user_ids)
            end

            it "should filter by email" do
                ensure_less_than_100_users

                first_user = User.first
                second_user = User.second

                first_user.email = "test@foo.com"
                first_user.save!

                second_user.name = "test2@foo.com"
                second_user.save!

                expected_user_ids = User.where("email ILIKE '\%@foo.com\%'").pluck("id")

                get :index, params: {filters: {email: "@foo.com"}, format: :json}
                expect(response).to be_successful, response.body
                ids = body_json["contents"]["users"].map { |u| u["id"] }
                expect(ids).to match_array(expected_user_ids)
            end

            it "should work if cohort_id is an array of ids" do
                ensure_less_than_100_users

                first_cohort = Cohort.first
                first_application = CohortApplication.first
                second_application = CohortApplication.second

                first_application.cohort = first_cohort
                first_application.status = 'accepted'
                first_application.cohort_slack_room_id = first_cohort.slack_rooms[0]&.id
                first_application.save!

                second_cohort = Cohort.second
                second_application.cohort = second_cohort
                second_application.status = 'accepted'
                second_application.cohort_slack_room_id = second_cohort.slack_rooms[0]&.id
                second_application.save!

                expected_user_ids = CohortApplication.where(cohort_id: [first_cohort.id, second_cohort.id], status: 'accepted').pluck('user_id')

                get :index, params: {filters: {cohort_id: [first_cohort.id, second_cohort.id], cohort_status: ['accepted']}, format: :json}
                expect(response).to be_successful, response.body
                ids = body_json["contents"]["users"].map { |u| u['id'] }
                expect(ids).to match_array(expected_user_ids)
                expect(ids).to include(first_application.user_id)
                expect(ids).to include(second_application.user_id)
            end

            it "should filter by cohort_id and cohort_status if cohort_status filter is not a graduation status" do
                ensure_less_than_100_users

                cohort = Cohort.first
                first_application = CohortApplication.first
                second_application = CohortApplication.second

                first_application.cohort = cohort
                first_application.status = 'accepted'
                first_application.cohort_slack_room_id = cohort.slack_rooms[0]&.id
                first_application.save!

                second_application.cohort = cohort
                second_application.status = 'accepted'
                second_application.cohort_slack_room_id = cohort.slack_rooms[0]&.id
                second_application.save!

                expected_user_ids = CohortApplication.where(cohort_id: cohort.id, status: 'accepted').pluck('user_id')

                get :index, params: {filters: {cohort_id: [cohort.id], cohort_status: ['accepted']}, format: :json}
                expect(response).to be_successful, response.body
                ids = body_json["contents"]["users"].map { |u| u['id'] }
                expect(ids).to match_array(expected_user_ids)
            end

            it "should filter by cohort_id and cohort_status if cohort_status filter equals 'pending'" do
                ensure_less_than_100_users

                cohort = Cohort.first
                first_application = CohortApplication.first
                second_application = CohortApplication.second

                first_application.cohort = cohort
                first_application.status = 'pending'
                first_application.save!

                second_application.cohort = cohort
                second_application.status = 'pending'
                second_application.save!

                expected_user_ids = CohortApplication.where(cohort_id: cohort.id, status: 'pending').pluck('user_id')

                get :index, params: {filters: {cohort_id: [cohort.id], cohort_status: ['pending']}, format: :json}
                expect(response).to be_successful, response.body
                ids = body_json["contents"]["users"].map { |u| u['id'] }
                expect(ids).to match_array(expected_user_ids)
            end

            it "should filter by cohort_id and graduation_status if cohort_status filter is actually a graduation status" do
                ensure_less_than_100_users

                cohort = Cohort.first
                first_application = CohortApplication.first
                second_application = CohortApplication.second

                first_application.cohort = cohort
                first_application.status = 'accepted'
                first_application.cohort_slack_room_id = cohort.slack_rooms[0]&.id
                first_application.graduation_status = 'graduated'
                first_application.save!

                second_application.cohort = cohort
                second_application.status = 'accepted'
                second_application.cohort_slack_room_id = cohort.slack_rooms[0]&.id
                second_application.graduation_status = 'graduated'
                second_application.save!

                expected_user_ids = CohortApplication.where(cohort_id: cohort.id, graduation_status: 'graduated').pluck('user_id')

                get :index, params: {filters: {cohort_id: [cohort.id], cohort_status: ['graduated']}, format: :json}
                expect(response).to be_successful, response.body
                ids = body_json["contents"]["users"].map { |u| u['id'] }
                expect(ids).to match_array(expected_user_ids)
            end

            it "should filter by last_cohort_status" do
                users = User.limit(3)
                cohorts = Cohort.limit(3)

                # Makes it easier to verify results
                CohortApplication.where(status: 'deferred').delete_all

                # Create three applications for the three users
                users.each do |user|
                    user.cohort_applications.destroy_all
                    CohortApplication.create!({
                        cohort_id: cohorts[0].id,
                        status: 'pending',
                        user_id: user.id,
                        applied_at: Time.now - 5.year
                    })
                    CohortApplication.create!({
                        cohort_id: cohorts[1].id,
                        status: 'rejected',
                        user_id: user.id,
                        applied_at: Time.now - 4.year
                    })
                    CohortApplication.create!({
                        cohort_id: cohorts[2].id,
                        status: 'deferred',
                        user_id: user.id,
                        applied_at: Time.now - 3.year
                    })
                    user.reload
                end

                # non-array style outside of filters block
                get :index, params: {last_cohort_status: 'deferred', format: :json}
                ids = body_json["contents"]["users"].map { |u| u['id'] }
                expect(ids).to match_array(users.pluck(:id))

                get :index, params: {filters: {last_cohort_status: ['deferred']}, format: :json}
                ids = body_json["contents"]["users"].map { |u| u['id'] }
                expect(ids).to match_array(users.pluck(:id))

                users[2].cohort_applications.where(status: ['pending']).first.update(applied_at: Time.now) # pending is now the latest
                get :index, params: {filters: {last_cohort_status: ['deferred']}, format: :json}
                ids = body_json["contents"]["users"].map { |u| u['id'] }
                expect(ids).to match_array([users.first.id, users.second.id])

                users[2].cohort_applications.delete_all
                get :index, params: {filters: {last_cohort_status: ['deferred']}, format: :json}
                ids = body_json["contents"]["users"].map { |u| u['id'] }
                expect(ids).to match_array([users.first.id, users.second.id])

                # test using the cohort_id filter as well
                get :index, params: {filters: {last_cohort_status: ['deferred'], cohort_id: [cohorts[2].id]}, format: :json}
                ids = body_json["contents"]["users"].map { |u| u['id'] }
                expect(ids).to match_array([users.first.id, users.second.id])
            end

            describe "application_registered" do
                it "should work when true" do
                    cohort = Cohort.first
                    CohortApplication.where(cohort_id: cohort.id).limit(5).update_all(registered: true)
                    registered_count = CohortApplication.select(:user_id).distinct.where(cohort_id: cohort.id, registered: true).reorder(nil).size
                    expect(registered_count).to be > 0 # sanity check
                    get :index, params: {filters: {cohort_id: [cohort.id], application_registered: true}, format: :json}
                    expect(response).to be_successful, response.body
                    ids = body_json["contents"]["users"].map { |u| u['id'] }
                    expect(ids.size).to eq(registered_count)
                end

                it "should work when false" do
                    cohort = Cohort.first
                    CohortApplication.where(cohort_id: cohort.id).limit(5).update_all(registered: false)
                    not_registered_count = CohortApplication.select(:user_id).distinct.where(cohort_id: cohort.id, registered: false).reorder(nil).size
                    expect(not_registered_count).to be > 0 # sanity check
                    get :index, params: {filters: {cohort_id: [cohort.id], application_registered: false}, format: :json}
                    expect(response).to be_successful, response.body
                    ids = body_json["contents"]["users"].map { |u| u['id'] }
                    expect(ids.size).to eq(not_registered_count)
                end
            end

            it "should filter by city_state" do
                ensure_less_than_100_users

                cohort = Cohort.first
                first_application = CohortApplication.first
                second_application = CohortApplication.second

                first_application.user.city = "alpha"
                first_application.user.state = "zulu"

                second_application.user.city = "alpha"
                first_application.user.state = "yankee"

                first_application.user.save!
                second_application.user.save!

                first_application.reload
                second_application.reload

                expected_user_ids = User.where("city ILIKE '\%alpha\%'").pluck("id")

                get :index, params: {cohort_id: cohort.id, filters: {city_state: "alpha"}, format: :json}
                expect(response).to be_successful, response.body
                ids = body_json["contents"]["users"].map { |u| u["id"] }
                expect(ids).to match_array(expected_user_ids)
            end

            it "should filter by multiple hiring_relationship_statuses" do
                hm_accepted_hiring_relationship = HiringRelationship.where(hiring_manager_status: "accepted", candidate_status: "pending").first
                c_accepted_hiring_relationship = HiringRelationship.where(hiring_manager_status: "accepted", candidate_status: "accepted").first

                get :index, params: {filters: {
                        hiring_relationship_statuses: ["accepted,pending"]
                    }, format: :json}
                expect(response).to be_successful, response.body
                ids = body_json["contents"]["users"].map { |u| u["id"] }
                expect(ids).to include(hm_accepted_hiring_relationship.candidate_id)
                expect(ids).not_to include(c_accepted_hiring_relationship.candidate_id)

                get :index, params: {filters: {
                        hiring_relationship_statuses: ["accepted,pending", "accepted,accepted"]
                    }, format: :json}
                expect(response).to be_successful, response.body
                ids = body_json["contents"]["users"].map { |u| u["id"] }
                expect(ids).to include(hm_accepted_hiring_relationship.candidate_id)
                expect(ids).to include(c_accepted_hiring_relationship.candidate_id)
            end

            it "should filter by hiring_relationship_statuses and available_for_relationships_with_hiring_manager" do
                hiring_relationship = HiringRelationship.where(hiring_manager_status: "accepted", candidate_status: "pending").first
                hiring_relationship_statuses = ["accepted,pending"]

                get :index, params: {filters: {
                        available_for_relationships_with_hiring_manager: hiring_relationship.hiring_manager_id,
                        hiring_relationship_statuses: hiring_relationship_statuses
                    }, format: :json}
                expect(response).to be_successful, response.body
                ids = body_json["contents"]["users"].map { |u| u["id"] }
                expect(ids).to include(hiring_relationship.candidate_id)

                hiring_relationship.candidate_status = "accepted"
                hiring_relationship.save!

                get :index, params: {filters: {
                        available_for_relationships_with_hiring_manager: hiring_relationship.hiring_manager_id,
                        hiring_relationship_statuses: hiring_relationship_statuses
                    }, format: :json}
                expect(response).to be_successful, response.body
                ids = body_json["contents"]["users"].map { |u| u["id"] }
                expect(ids).not_to include(hiring_relationship.candidate_id)
            end

            describe "career_profile" do

                it "should filter by complete" do
                    first_career_profile = CareerProfile.first
                    second_career_profile = CareerProfile.second

                    CareerProfile.where.not(id: [first_career_profile.id, second_career_profile.id]).update_all(last_calculated_complete_percentage: 0)

                    first_career_profile.last_calculated_complete_percentage = 100
                    first_career_profile.save!

                    second_career_profile.last_calculated_complete_percentage = 100
                    second_career_profile.save!

                    expected_user_ids = CareerProfile.where("last_calculated_complete_percentage = 100").pluck("user_id")

                    get :index, params: {filters: {career_profile_complete: "true"}, format: :json}
                    expect(response).to be_successful, response.body
                    ids = body_json["contents"]["users"].map { |u| u["id"] }
                    expect(ids).to match_array(expected_user_ids)

                    first_career_profile.last_calculated_complete_percentage = 99
                    first_career_profile.save!

                    second_career_profile.last_calculated_complete_percentage = 0
                    second_career_profile.save!

                    CareerProfile.where.not(id: [first_career_profile.id, second_career_profile.id]).update_all(last_calculated_complete_percentage: 100)

                    expected_user_ids = CareerProfile.where("last_calculated_complete_percentage < 100").pluck("user_id")

                    get :index, params: {filters: {career_profile_complete: "false"}, format: :json}
                    expect(response).to be_successful, response.body
                    ids = body_json["contents"]["users"].map { |u| u["id"] }
                    expect(ids).to match_array(expected_user_ids)
                end

                it "should filter by disabled" do
                    first_career_profile = CareerProfile.first
                    second_career_profile = CareerProfile.second

                    CareerProfile.where.not(id: [first_career_profile.id, second_career_profile.id]).update_all(do_not_create_relationships: false)

                    first_career_profile.do_not_create_relationships = true
                    first_career_profile.save!(validate: false) # see career_profile.rb#validate_appropriate_do_not_create_relationships_change

                    second_career_profile.do_not_create_relationships = true
                    second_career_profile.save!(validate: false)

                    expected_user_ids = CareerProfile.where("do_not_create_relationships = true").pluck("user_id")

                    get :index, params: {filters: {career_profile_disabled: "true"}, format: :json}
                    expect(response).to be_successful, response.body
                    ids = body_json["contents"]["users"].map { |u| u["id"] }
                    expect(ids).to match_array(expected_user_ids)

                    first_career_profile = CareerProfile.first
                    second_career_profile = CareerProfile.second

                    CareerProfile.where.not(id: [first_career_profile.id, second_career_profile.id]).update_all(do_not_create_relationships: true)

                    first_career_profile.do_not_create_relationships = false
                    first_career_profile.save!

                    second_career_profile.do_not_create_relationships = false
                    second_career_profile.save!

                    expected_user_ids = CareerProfile.where("do_not_create_relationships = false").pluck("user_id")

                    get :index, params: {filters: {career_profile_disabled: "false"}, format: :json}
                    expect(response).to be_successful, response.body
                    ids = body_json["contents"]["users"].map { |u| u["id"] }
                    expect(ids).to match_array(expected_user_ids)
                end

                it "should filter by employment_types_of_interest" do
                    first_career_profile = CareerProfile.first
                    second_career_profile = CareerProfile.second

                    first_career_profile.employment_types_of_interest = ["foo", "bar"]
                    first_career_profile.save!

                    second_career_profile.employment_types_of_interest = ["bar", "baz"]
                    second_career_profile.save!

                    expected_user_ids = [first_career_profile.user_id, second_career_profile.user_id]

                    get :index, params: {filters: {career_profile_employment_types_of_interest: ["bar"]}, format: :json}
                    expect(response).to be_successful, response.body
                    ids = body_json["contents"]["users"].map { |u| u["id"] }
                    expect(ids).to match_array(expected_user_ids)
                end

                it "should filter by has_profile_feedback" do
                    ensure_less_than_100_users
                    # ensure some career profiles have feedback
                    first_career_profile = CareerProfile.first
                    second_career_profile = CareerProfile.where.not(user_id: first_career_profile.user_id).first

                    first_career_profile.profile_feedback = 'This is some profile feedback'
                    second_career_profile.profile_feedback = 'This is some profile feedback'
                    first_career_profile.save!
                    second_career_profile.save!

                    # when has_profile_feedback is 'true'
                    get :index, params: {
                        filters: {
                            has_profile_feedback: 'true'
                        },
                        format: :json
                    }
                    expect(response).to be_successful, response.body
                    ids = body_json["contents"]["users"].map { |u| u["id"] }
                    expected_user_ids = [first_career_profile.user_id, second_career_profile.user_id]
                    expect(ids).to match_array(expected_user_ids)

                    # when has_profile_feedback is 'false'
                    first_career_profile.profile_feedback = nil # clear the profile feedback on one of the profiles
                    first_career_profile.save!
                    get :index, params: {
                        filters: {
                            has_profile_feedback: 'false'
                        },
                        format: :json
                    }
                    expect(response).to be_successful, response.body
                    ids = body_json["contents"]["users"].map { |u| u["id"] }
                    expect(ids).to include(first_career_profile.user_id)
                    expect(ids).not_to include(second_career_profile.user_id)
                end

                it "should filter by profile_updated_after_feedback_sent" do
                    # ensure some career profiles have profile feedback and feedback_last_sent_at is not null
                    first_career_profile = CareerProfile.first
                    second_career_profile = CareerProfile.second

                    # mock this out since it's not important to this unit test
                    allow_any_instance_of(CareerProfile).to receive(:send_feedback_email)

                    feedback_last_sent_at = Time.now
                    updated_at = feedback_last_sent_at
                    first_career_profile.profile_feedback = 'This is some profile feedback for profile #1'
                    first_career_profile.feedback_last_sent_at = feedback_last_sent_at
                    first_career_profile.updated_at = updated_at
                    first_career_profile.save!

                    second_career_profile.profile_feedback = 'This is some profile feedback for profile #2'
                    second_career_profile.feedback_last_sent_at = feedback_last_sent_at
                    first_career_profile.updated_at = updated_at
                    second_career_profile.save!

                    # when profile_updated_after_feedback_sent is 'false'
                    get :index, params: {
                        filters: {
                            profile_updated_after_feedback_sent: 'false'
                        },
                        format: :json
                    }
                    expect(response).to be_successful, response.body
                    ids = body_json["contents"]["users"].map { |u| u["id"] }
                    expected_user_ids = [first_career_profile.user_id, second_career_profile.user_id]
                    expect(ids).to match_array(expected_user_ids)

                    # when profile_updated_after_feedback_sent is 'true'
                    first_career_profile.updated_at = first_career_profile.feedback_last_sent_at + 3.seconds # update one of the profiles
                    first_career_profile.save!
                    get :index, params: {
                        filters: {
                            profile_updated_after_feedback_sent: 'true'
                        },
                        format: :json
                    }
                    expect(response).to be_successful, response.body
                    ids = body_json["contents"]["users"].map { |u| u["id"] }
                    expected_user_ids = [first_career_profile.user_id]
                    expect(ids).to match_array(expected_user_ids)
                end

                it "should filter by career_profile_active_status" do
                    user_with_uneditable_career_profile = User.joins(:career_profile).where(:can_edit_career_profile => false).first
                    user_with_very_interested_career_profile = users(:user_with_very_interested_career_profile)
                    user_with_interested_career_profile = users(:user_with_interested_career_profile)
                    user_with_neutral_career_profile = users(:user_with_neutral_career_profile)
                    user_with_not_interested_career_profile = users(:user_with_not_interested_career_profile)

                    filtered_out_ids = [
                        user_with_uneditable_career_profile,
                        user_with_very_interested_career_profile,
                        user_with_interested_career_profile,
                        user_with_neutral_career_profile,
                        user_with_not_interested_career_profile
                    ].map { |u| "'#{u.id}'" }.join(',')
                    incomplete_career_profile = CareerProfile.where("user_id NOT IN (#{filtered_out_ids})").first
                    incomplete_career_profile.update_attribute(:last_calculated_complete_percentage, 15)

                    get :index, params: {filters: {career_profile_active_status: ["interested", "neutral"]}, format: :json}
                    expect(response).to be_successful, response.body
                    ids = body_json["contents"]["users"].map { |u| u["id"] }
                    expect(ids).not_to include(user_with_uneditable_career_profile.id)
                    expect(ids).not_to include(incomplete_career_profile.user_id)
                    expect(ids).not_to include(user_with_not_interested_career_profile.id)
                    expect(ids).not_to include(user_with_very_interested_career_profile.id)
                    expect(ids).to include(user_with_interested_career_profile.id)
                    expect(ids).to include(user_with_neutral_career_profile.id)
                end

                it "should delegate some things to the career profile filter mixins" do

                    orig_filters = {
                        career_profile_years_experience: ['6_plus_years'],
                        career_profile_places: [{'some' => 'place'}],
                        career_profile_only_local: 'true',
                        career_profile_roles: {
                            'primary_areas_of_interest' => ['role'],
                            'work_experience_roles' => ['role']
                        },
                        career_profile_industries: ['industry'],
                        career_profile_skills: ['skill'],
                        career_profile_keyword_search: 'keyword',
                    }

                    delegated_filters = {}
                    orig_filters.each do |key, value|
                        delegated_filters[key.to_s.gsub('career_profile_', '').to_sym] = value
                    end

                    expect(controller).to receive(:filter_career_profiles_by_years_experience).with(delegated_filters.with_indifferent_access, anything).and_call_original
                    expect(controller).to receive(:filter_career_profiles_by_places).with(delegated_filters.with_indifferent_access, anything).and_call_original
                    expect(controller).to receive(:filter_career_profiles_by_roles).with(delegated_filters.with_indifferent_access, anything).and_call_original
                    expect(controller).to receive(:filter_career_profiles_by_industry).with(delegated_filters.with_indifferent_access, anything).and_call_original
                    expect(controller).to receive(:filter_career_profiles_by_skills).with(delegated_filters.with_indifferent_access, anything).and_call_original
                    expect(controller).to receive(:filter_career_profiles_by_keyword_search).with(delegated_filters.with_indifferent_access, anything).and_call_original
                    get :index, params: {filters: orig_filters, format: :json}
                    expect(response).to be_successful, response.body


                end

                it "should filter by career_profile_location" do
                    first_career_profile = CareerProfile.first
                    second_career_profile = CareerProfile.second
                    third_career_profile = CareerProfile.third

                    first_career_profile.place_details = {formatted_address: 'Harrisonburg, VA'}
                    first_career_profile.locations_of_interest = ['washington_dc']
                    first_career_profile.save!

                    second_career_profile.place_details = {formatted_address: 'Washington, D.C.'}
                    second_career_profile.locations_of_interest = ['houston']
                    second_career_profile.save!

                    third_career_profile.place_details = {formatted_address: 'Somewhere, NA'}
                    third_career_profile.locations_of_interest = ['flexible']
                    third_career_profile.save!

                    expected_user_ids = [first_career_profile.user_id, second_career_profile.user_id]

                    get :index, params: {filters: {career_profile_location: "washington"}, format: :json}
                    expect(response).to be_successful, response.body
                    ids = body_json["contents"]["users"].map { |u| u["id"] }
                    expect(ids).to match_array(expected_user_ids)
                    expect(ids).not_to include(third_career_profile.user_id)
                end

                describe "career_profile_last_confirmed_at_by_student" do

                    before(:each) do
                        ensure_less_than_100_users
                        @first_career_profile = CareerProfile.first
                        @second_career_profile = CareerProfile.second
                        @third_career_profile = CareerProfile.third

                        @first_career_profile.last_confirmed_at_by_student = Time.now - 4.weeks
                        @first_career_profile.save!

                        @second_career_profile.last_confirmed_at_by_student = Time.now - 2.weeks
                        @second_career_profile.save!

                        @third_career_profile.last_confirmed_at_by_student = Time.now - 1.day
                        @third_career_profile.save!
                    end

                    it "should filter by date" do
                        expected_user_ids = [@second_career_profile.user_id, @third_career_profile.user_id]

                        three_weeks_ago = (Time.now - 3.weeks).to_s
                        get :index, params: {filters: {career_profile_last_confirmed_at_by_student: three_weeks_ago}, format: :json}
                        expect(response).to be_successful, response.body
                        ids = body_json["contents"]["users"].map { |u| u["id"] }
                        expect(ids).to match_array(expected_user_ids)
                    end

                    it "should filter by NOT NULL and NULL" do
                        expected_user_ids = [@first_career_profile.user_id, @second_career_profile.user_id, @third_career_profile.user_id]

                        get :index, params: {filters: {career_profile_last_confirmed_at_by_student: 'NOT NULL'}, format: :json}
                        expect(response).to be_successful, response.body
                        ids = body_json["contents"]["users"].map { |u| u["id"] }
                        expect(ids).to match_array(expected_user_ids)

                        @first_career_profile.last_confirmed_at_by_student = nil
                        @first_career_profile.save!

                        @second_career_profile.last_confirmed_at_by_student = nil
                        @second_career_profile.save!

                        expected_user_ids = [@first_career_profile.user_id, @second_career_profile.user_id]
                        get :index, params: {filters: {career_profile_last_confirmed_at_by_student: 'NULL'}, format: :json}
                        expect(response).to be_successful, response.body
                        ids = body_json["contents"]["users"].map { |u| u["id"] }
                        expected_user_ids.each do |expected_user_id|
                            expect(ids).to include(expected_user_id)
                        end
                        expect(ids).not_to include(@third_career_profile.user_id)
                    end
                end

                describe "career_profile_last_confirmed_at_by_internal" do

                    before(:each) do
                        ensure_less_than_100_users
                        @first_career_profile = CareerProfile.first
                        @second_career_profile = CareerProfile.second
                        @third_career_profile = CareerProfile.third

                        @first_career_profile.last_confirmed_at_by_internal = Time.now - 4.weeks
                        @first_career_profile.save!

                        @second_career_profile.last_confirmed_at_by_internal = Time.now - 2.weeks
                        @second_career_profile.save!

                        @third_career_profile.last_confirmed_at_by_internal = Time.now - 1.day
                        @third_career_profile.save!
                    end

                    it "should filter by date" do
                        expected_user_ids = [@second_career_profile.user_id, @third_career_profile.user_id]

                        three_weeks_ago = (Time.now - 3.weeks).to_s
                        get :index, params: {filters: {career_profile_last_confirmed_at_by_internal: three_weeks_ago}, format: :json}
                        expect(response).to be_successful, response.body
                        ids = body_json["contents"]["users"].map { |u| u["id"] }
                        expect(ids).to match_array(expected_user_ids)
                    end

                    it "should filter by NOT NULL and NULL" do
                        expected_user_ids = [@first_career_profile.user_id, @second_career_profile.user_id, @third_career_profile.user_id]

                        get :index, params: {filters: {career_profile_last_confirmed_at_by_internal: 'NOT NULL'}, format: :json}
                        expect(response).to be_successful, response.body
                        ids = body_json["contents"]["users"].map { |u| u["id"] }
                        expect(ids).to match_array(expected_user_ids)

                        @first_career_profile.last_confirmed_at_by_internal = nil
                        @first_career_profile.save!

                        @second_career_profile.last_confirmed_at_by_internal = nil
                        @second_career_profile.save!

                        expected_user_ids = [@first_career_profile.user_id, @second_career_profile.user_id]
                        get :index, params: {filters: {career_profile_last_confirmed_at_by_internal: 'NULL'}, format: :json}
                        expect(response).to be_successful, response.body
                        ids = body_json["contents"]["users"].map { |u| u["id"] }
                        expected_user_ids.each do |expected_user_id|
                            expect(ids).to include(expected_user_id)
                        end
                        expect(ids).not_to include(@third_career_profile.user_id)
                    end
                end
            end
        end

        describe "sorting" do

            before(:each) do
                stub_current_user(controller, [:admin], ["GROUP"])

                @users = roles(:learner).users.limit(2)
            end

            def get_json_for_three_users(params)
                get :index, params: params.merge(:per_page => 9999999)

                user_ids = [@users[0], @users[1], controller.current_user].map(&:id)
                body_json["contents"]["users"].select do |user|
                    user_ids.include?(user['id'])
                end
            end

            it "should sort by name" do
                @users[0].update!(name: 'Anderson')
                controller.current_user.update!(name: 'User')
                @users[1].update!(name: 'Zamboni')

                json = get_json_for_three_users({sort: 'name', direction: 'asc', format: :json})
                expect(json.size).to eq(3)
                expect(json[0]["id"]).to eq(@users[0].id)
                expect(json[1]["id"]).to eq(controller.current_user.id)
                expect(json[2]["id"]).to eq(@users[1].id)

                json = get_json_for_three_users({sort: 'name', direction: 'desc', format: :json})
                expect(json.size).to eq(3)
                expect(json[0]["id"]).to eq(@users[1].id)
                expect(json[1]["id"]).to eq(controller.current_user.id)
                expect(json[2]["id"]).to eq(@users[0].id)
            end

            it "should sort by email" do
                @users[0].update!(email: 'Anderson@email.com')
                controller.current_user.update!(email: 'User@email.com')
                @users[1].update!(email: 'Zamboni@email.com')

                json = get_json_for_three_users({sort: 'email', direction: 'asc', format: :json})
                expect(json.size).to eq(3)
                expect(json[0]["id"]).to eq(@users[0].id)
                expect(json[1]["id"]).to eq(controller.current_user.id)
                expect(json[2]["id"]).to eq(@users[1].id)

                json = get_json_for_three_users({sort: 'email', direction: 'desc', format: :json})
                expect(json.size).to eq(3)
                expect(json[0]["id"]).to eq(@users[1].id)
                expect(json[1]["id"]).to eq(controller.current_user.id)
                expect(json[2]["id"]).to eq(@users[0].id)
            end

            it "should sort by created_at" do
                @users[0].update!(created_at: Time.now - 10.minutes)
                controller.current_user.update!(created_at: Time.now - 7.minutes)
                @users[1].update!(created_at: Time.now - 5.minutes)

                json = get_json_for_three_users({sort: 'users.created_at', direction: 'asc', format: :json})
                expect(json.size).to eq(3)
                expect(json[0]["id"]).to eq(@users[0].id)
                expect(json[1]["id"]).to eq(controller.current_user.id)
                expect(json[2]["id"]).to eq(@users[1].id)

                json = get_json_for_three_users({sort: 'users.created_at', direction: 'desc', format: :json})
                expect(json.size).to eq(3)
                expect(json[0]["id"]).to eq(@users[1].id)
                expect(json[1]["id"]).to eq(controller.current_user.id)
                expect(json[2]["id"]).to eq(@users[0].id)
            end

            it "should sort by role" do
                @users[0].add_role('editor')
                @users[0].remove_role('learner')

                @users[1].remove_role('editor')
                @users[1].add_role('learner')

                controller.current_user.remove_role('editor')
                controller.current_user.remove_role('learner')
                controller.current_user.add_role('admin')

                json = get_json_for_three_users({sort: 'roles.name', direction: 'asc', format: :json})
                expect(json.size).to eq(3)
                expect(json[0]["id"]).to eq(controller.current_user.id) # admin
                expect(json[1]["id"]).to eq(@users[0].id) # editor
                expect(json[2]["id"]).to eq(@users[1].id) # learner

                json = get_json_for_three_users({sort: 'roles.name', direction: 'desc', format: :json})
                expect(json.size).to eq(3)
                expect(json[0]["id"]).to eq(@users[1].id) # learner
                expect(json[1]["id"]).to eq(@users[0].id) # editor
                expect(json[2]["id"]).to eq(controller.current_user.id) # admin
            end

            it "should sort by city_state" do
                @users[0].update!(city: 'Alpha', state: 'Zulu')
                controller.current_user.update!(city: 'Alpha', state: 'Bravo')
                @users[1].update!(city: 'Alpha', state: 'Yankee')

                json = get_json_for_three_users({sort: 'formatted_location.city_state', direction: 'asc', format: :json})
                expect(json.size).to eq(3)
                expect(json[0]["id"]).to eq(controller.current_user.id)
                expect(json[1]["id"]).to eq(@users[1].id)
                expect(json[2]["id"]).to eq(@users[0].id)

                json = get_json_for_three_users({sort: 'formatted_location.city_state', direction: 'desc', format: :json})
                expect(json.size).to eq(3)
                expect(json[0]["id"]).to eq(@users[0].id)
                expect(json[1]["id"]).to eq(@users[1].id)
                expect(json[2]["id"]).to eq(controller.current_user.id)
            end

            it "should sort by city_state when there are nulls" do
                @users[0].update!(city: 'Alpha', state: 'Zulu')
                controller.current_user.update!(city: nil, state: 'Bravo')
                @users[1].update!(city: nil, state: nil)

                json = get_json_for_three_users({sort: 'formatted_location.city_state', direction: 'asc', format: :json})
                expect(json.size).to eq(3)
                expect(json[0]["id"]).to eq(@users[0].id)
                expect(json[1]["id"]).to eq(controller.current_user.id)
                expect(json[2]["id"]).to eq(@users[1].id)

                json = get_json_for_three_users({sort: 'formatted_location.city_state', direction: 'desc', format: :json})
                expect(json.size).to eq(3)
                expect(json[0]["id"]).to eq(controller.current_user.id)
                expect(json[1]["id"]).to eq(@users[0].id)
                expect(json[2]["id"]).to eq(@users[1].id)
            end

            it "should sort by hiring_manager_priority" do
                hiring_manager = users(:hiring_manager)
                hiring_manager.candidate_relationships.destroy_all
                users = [users(:user_with_very_interested_career_profile), users(:user_with_interested_career_profile)] # these users should be available for relationships

                HiringRelationship.create!(hiring_manager_id: hiring_manager.id, candidate_id: users[0].id, hiring_manager_priority: 98)
                HiringRelationship.create!(hiring_manager_id: hiring_manager.id, candidate_id: users[1].id, hiring_manager_priority: 90)

                get :index, params: {
                    sort: 'hiring_relationships.hiring_manager_priority',
                    available_for_relationships_with_hiring_manager: hiring_manager.id,
                    direction: 'desc',
                    per_page: 9999999,
                    format: :json
                }
                user_ids = users.pluck(:id)
                json = body_json["contents"]["users"].select do |user|
                    user_ids.include?(user['id'])
                end
                expect(json.size).to eq(2)
                expect(json[0]["id"]).to eq(users[0].id)
                expect(json[1]["id"]).to eq(users[1].id)

                get :index, params: {
                    sort: 'hiring_relationships.hiring_manager_priority',
                    available_for_relationships_with_hiring_manager: hiring_manager.id,
                    direction: 'asc',
                    per_page: 9999999,
                    format: :json
                }
                json = body_json["contents"]["users"].select do |user|
                    user_ids.include?(user['id'])
                end
                expect(json.size).to eq(2)
                expect(json[0]["id"]).to eq(users[1].id)
                expect(json[1]["id"]).to eq(users[0].id)
            end

            describe "career_profiles" do
                before(:each) do
                    ensure_less_than_100_users
                    @users = User.joins(:career_profile).includes(:career_profile).limit(3)
                end

                # Redefining this
                def get_json_for_three_users(params)
                    get :index, params: params.merge(:per_page => 9999999)

                    user_ids = [@users[0], @users[1], @users[2]].map(&:id)
                    body_json["contents"]["users"].select do |user|
                        user_ids.include?(user['id'])
                    end
                end

                it "should sort by created_at" do
                    now = Time.now
                    @users[0].career_profile.update!(created_at: now - 50)
                    @users[1].career_profile.update!(created_at: now + 50)
                    @users[2].career_profile.update!(created_at: now)

                    json = get_json_for_three_users({sort: 'career_profiles.created_at', direction: 'asc', format: :json})
                    expect(json.size).to eq(3)
                    expect(json[0]["id"]).to eq(@users[0].id)
                    expect(json[1]["id"]).to eq(@users[2].id)
                    expect(json[2]["id"]).to eq(@users[1].id)

                    json = get_json_for_three_users({sort: 'career_profiles.created_at', direction: 'desc', format: :json})
                    expect(json.size).to eq(3)
                    expect(json[0]["id"]).to eq(@users[1].id)
                    expect(json[1]["id"]).to eq(@users[2].id)
                    expect(json[2]["id"]).to eq(@users[0].id)
                end

                it "should sort by updated_at" do
                    now = Time.now
                    @users[0].career_profile.update!(updated_at: now - 50)
                    @users[1].career_profile.update!(updated_at: now + 50)
                    @users[2].career_profile.update!(updated_at: now)

                    json = get_json_for_three_users({sort: 'career_profiles.updated_at', direction: 'asc', format: :json})
                    expect(json.size).to eq(3)
                    expect(json[0]["id"]).to eq(@users[0].id)
                    expect(json[1]["id"]).to eq(@users[2].id)
                    expect(json[2]["id"]).to eq(@users[1].id)

                    json = get_json_for_three_users({sort: 'career_profiles.updated_at', direction: 'desc', format: :json})
                    expect(json.size).to eq(3)
                    expect(json[0]["id"]).to eq(@users[1].id)
                    expect(json[1]["id"]).to eq(@users[2].id)
                    expect(json[2]["id"]).to eq(@users[0].id)
                end

                it "should sort by last_confirmed_at_by_student" do
                    now = Time.now
                    @users[0].career_profile.update!(last_confirmed_at_by_student: now - 50)
                    @users[1].career_profile.update!(last_confirmed_at_by_student: now + 50)
                    @users[2].career_profile.update!(last_confirmed_at_by_student: now)

                    json = get_json_for_three_users({sort: 'career_profiles.last_confirmed_at_by_student', direction: 'asc', format: :json})
                    expect(json.size).to eq(3)
                    expect(json[0]["id"]).to eq(@users[0].id)
                    expect(json[1]["id"]).to eq(@users[2].id)
                    expect(json[2]["id"]).to eq(@users[1].id)

                    json = get_json_for_three_users({sort: 'career_profiles.last_confirmed_at_by_student', direction: 'desc', format: :json})
                    expect(json.size).to eq(3)
                    expect(json[0]["id"]).to eq(@users[1].id)
                    expect(json[1]["id"]).to eq(@users[2].id)
                    expect(json[2]["id"]).to eq(@users[0].id)
                end

                it "should sort by do_not_create_relationships" do
                    allow_any_instance_of(CareerProfile).to receive(:valid?).and_return(true)
                    @users[0].career_profile.update(do_not_create_relationships: true)
                    @users[1].career_profile.update(do_not_create_relationships: false)

                    get :index, params: {sort: 'career_profiles.do_not_create_relationships', direction: 'asc', format: :json}
                    user_ids = [@users[0].id, @users[1].id]

                    json = body_json["contents"]["users"].select do |user|
                        user_ids.include?(user['id'])
                    end

                    expect(json.size).to eq(2)

                    expect(json[0]["id"]).to eq(@users[1].id)

                    get :index, params: {sort: 'career_profiles.do_not_create_relationships', direction: 'desc', format: :json}
                    user_ids = [@users[0].id, @users[1].id]
                    json = body_json["contents"]["users"].select do |user|
                        user_ids.include?(user['id'])
                    end

                    expect(json.size).to eq(2)
                    expect(json[0]["id"]).to eq(@users[0].id)
                end

                it "should sort by last_calculated_complete_percentage" do
                    @users[0].career_profile.update!(last_calculated_complete_percentage: 0)
                    @users[1].career_profile.update!(last_calculated_complete_percentage: 100)
                    @users[2].career_profile.update!(last_calculated_complete_percentage: 50)

                    json = get_json_for_three_users({sort: 'career_profiles.last_calculated_complete_percentage', direction: 'asc', format: :json})
                    expect(json.size).to eq(3)
                    expect(json[0]["id"]).to eq(@users[0].id)
                    expect(json[1]["id"]).to eq(@users[2].id)
                    expect(json[2]["id"]).to eq(@users[1].id)

                    json = get_json_for_three_users({sort: 'career_profiles.last_calculated_complete_percentage', direction: 'desc', format: :json})
                    expect(json.size).to eq(3)
                    expect(json[0]["id"]).to eq(@users[1].id)
                    expect(json[1]["id"]).to eq(@users[2].id)
                    expect(json[2]["id"]).to eq(@users[0].id)
                end

                it "should sort by interested_in_joining_new_company" do
                    @users[0].career_profile.update!(interested_in_joining_new_company: 'very_interested')
                    @users[1].career_profile.update!(interested_in_joining_new_company: 'interested')
                    @users[2].career_profile.update!(interested_in_joining_new_company: 'neutral')

                    json = get_json_for_three_users({sort: 'career_profiles.interested_in_joining_new_company', direction: 'asc', format: :json})
                    expect(json.size).to eq(3)
                    expect(json[0]["id"]).to eq(@users[1].id)
                    expect(json[1]["id"]).to eq(@users[2].id)
                    expect(json[2]["id"]).to eq(@users[0].id)

                    json = get_json_for_three_users({sort: 'career_profiles.interested_in_joining_new_company', direction: 'desc', format: :json})
                    expect(json.size).to eq(3)
                    expect(json[0]["id"]).to eq(@users[0].id)
                    expect(json[1]["id"]).to eq(@users[2].id)
                    expect(json[2]["id"]).to eq(@users[1].id)
                end
            end
        end

        it "should include subscriptions" do
            @user = users(:user_with_subscriptions_enabled)
            create_subscription(@user)
            get :index, params: {identifiers: [@user.email], format: :json, select_subscriptions: true}
            expect(body_json["contents"]["users"][0]['subscriptions'][0]['id']).to eq(@user.primary_subscription.id)
            expect(body_json["contents"]["users"][0]['subscriptions'][0]['user_id']).to eq(@user.id)
        end

        it "should include cohort_user_progress_record" do
            @user = User.find(CohortUserProgressRecord.first.user_id)
            get :index, params: {identifiers: [@user.email], format: :json, select_cohort_user_progress_record: true}
            expect(body_json["contents"]["users"][0]['cohort_user_progress_record']['user_id']).to eq(@user.id)
        end

        it "should include project_progresses" do
            @users = User.limit(2)
            project_progress = ProjectProgress.create!(
                user_id: @users[0].id,
                requirement_identifier: 'requirement_identifier'
            )
            ActiveRecord::Base.connection.execute(%Q~
                insert into user_project_scores (user_id, score) values('#{@users[0].id}', 0.42)
            ~)
            get :index, params: {identifiers: @users.map(&:email), format: :json, select_project_progress: true}

            user_1_row = body_json["contents"]["users"].detect { |r| r['id'] == @users[0].id }
            expect(user_1_row['project_progress'][0]['id']).to eq(project_progress.id)
            expect(user_1_row['project_score_override']).to eq(0.42)
            user_2_row = body_json["contents"]["users"].detect { |r| r['id'] == @users[1].id }
            expect(user_2_row['project_progress']).to eq([])
            expect(user_2_row['project_score_override']).to be_nil
        end

        it "should include exam_progress" do
            # start with a user with no progress
            @user = User.left_outer_joins(:lesson_streams_progresses).where("lesson_streams_progress.id is null").first

            # add one progress from an exam stream and one other
            exam_stream_progress = Lesson::StreamProgress.create!(
                :user_id => @user.id,
                :locale_pack_id => Lesson::Stream.where(locale:'en', exam: true).first.locale_pack_id,
                :started_at => Time.now
            )
            Lesson::StreamProgress.create!(
                :user_id => @user.id,
                :locale_pack_id => Lesson::Stream.where(locale:'en', exam: false).first.locale_pack_id,
                :started_at => Time.now
            )

            # check that only the one from the exam stream shows up
            get :index, params: {identifiers: [@user.email], format: :json, select_exam_progress: true}
            expect(body_json["contents"]["users"][0]['exam_progress'].map { |r| r['locale_pack_id']}).to eq([exam_stream_progress.locale_pack_id])
        end

        it "should include signable_documents" do

            # start with a user with no documents
            @user = User.left_outer_joins(:signable_documents).where("signable_documents.id is null").first

            # add onedocument
            document = SignableDocument.create!(
                :user_id => @user.id,
                document_type: 'enrollment_agreement'
            )

            get :index, params: {identifiers: [@user.email], format: :json, select_signable_documents: true}
            expect(body_json["contents"]["users"][0]['signable_documents'].map { |r| r['id']}).to eq([document.id])
        end

        # Used in the Enrollment tab in Admin Cohorts
        it "should include transcripts" do
            education_experience = education_experiences(:education_experience_with_two_transcripts)
            user_with_transcripts = education_experience.career_profile.user
            user_with_transcripts.career_profile.education_experiences.where.not(id: education_experience.id).destroy_all
            get :index, params: { identifiers: [user_with_transcripts.email], format: :json, select_career_profile: true, select_education_experiences: true, select_transcripts: true}
            expect(body_json["contents"]["users"][0]['career_profile']['education_experiences'][0]['transcripts']
                .map { |t| t['id']}).to match_array(education_experience.transcripts.pluck(:id))
        end

        it "should include active_institution" do
            user = users(:accepted_mba_cohort_user)
            get :index, params: {identifiers: [user.email], format: :json}
            expect(body_json["contents"]["users"][0]["active_institution"]["id"]).to eq(user.active_institution.id)
        end

        it "should include active_deferral_link" do
            user = users(:accepted_emba_cohort_user)
            deferral_link = DeferralLink.create!(
                user: user,
                expires_at: Time.now + 30.days,
                from_cohort_application_id: user.accepted_application.id
            )
            get :index, params: {identifiers: [user.email], format: :json, select_active_deferral_link: true}
            expect(body_json["contents"]["users"][0]["active_deferral_link"]["id"]).to eq(deferral_link.id)
        end
    end

    describe "GET 'show'" do
        before (:each) do
            stub_current_user(controller, [:admin], ["GROUP"])
            @some_user = FactoryBot.create(:user)
            sign_in @some_user
        end

        it "should be successful" do
            user = User.joins(:career_profile).first
            user.update_columns({
                last_seen_at: Time.now
            })
            user.career_profile.update_columns({
                last_confirmed_at_by_student: Time.now
            })
            user.reload

            get :show, params: {id: user.id, format: :json}
            expect(response).to be_successful, response.body

            # Just spot checking a few fields in the response
            expect(body_json["contents"]["users"][0]["id"]).to eq(user.id)
            expect(body_json["contents"]["users"][0]["last_seen_at"]).to be(user.last_seen_at.to_timestamp)
            expect(body_json["contents"]["users"][0]["updated_at"]).to be(user.updated_at.to_timestamp)
            expect(body_json["contents"]["users"][0]["career_profile"]["last_confirmed_at_by_student"]).to be(user.career_profile.last_confirmed_at_by_student.to_timestamp)
        end

        it "should find the right user" do
            get :show, params: {id: @some_user.id, format: :json}
            expect(body_json["contents"]["users"][0]["id"]).to eq(@some_user.id)
        end

    end

    describe "PUT update" do
        before (:each) do
            stub_current_user(controller, [:admin], ["GROUP"])
            @some_user = FactoryBot.create(:user)
            @some_user.add_role("learner")
            @some_user.save!
            sign_in @some_user
        end

        it "updates a users roles" do
            admin_role = Role.find_by_name("admin")
            put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, roles: [{"name" => "admin"}]}, format: :json}
            expect(response.status).to eq(200)
            expect(User.find_by_id(@some_user.id).roles.to_a).to eq([admin_role])
        end

        it "replaces a users roles with the specified set" do
            lesson = Lesson.first
            global_editor_role = Role.where(name: "editor", resource_id: nil).first
            global_learner_role = Role.find_or_create_by(name: "learner")
            cannot_create_override_role = Role.find_or_create_by(name: "cannot_create")
            scoped_previewer_role = Role.find_or_create_by(name: "previewer", resource_id: lesson.id, resource_type: "Lesson")
            @some_user.roles = [global_learner_role]

            updated_roles = [{"name" => "editor"}, {"name" => "cannot_create"}, {"name" => "previewer", "resource_id"=> lesson.id, "resource_type"=> "Lesson"}]
            put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, roles: updated_roles}, format: :json}
            expect(response.status).to eq(200)
            updated_roles = User.find_by_id(@some_user.id).roles.to_a.sort
            expect(updated_roles).to match_array([global_editor_role, cannot_create_override_role, scoped_previewer_role])
            # the roles table needs to fk to the id, not the guid, so we map this in users_controller#update
            expect(updated_roles.map(&:resource_id).compact.first).to eq(lesson.id)
        end

        it "updates a users groups" do
            AccessGroup.create!(name: "OTHER")
            AccessGroup.create!(name: "CATS")
            AccessGroup.create!(name: "DOGS")

            put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, groups: [{name: "CATS"},{name: "DOGS"}]}, format: :json}
            expect(response.status).to eq(200)
            expect(User.find(@some_user.id).group_names).to eq(["CATS", "DOGS"])
        end


        it "can update institutions" do
            inst = Institution.first
            @some_user.institutions = []
            @some_user.save!
            put :update, params: {record: {id: @some_user.id, institutions: [id: inst.id], updated_at: @some_user.updated_at.to_timestamp}, format: :json}
            expect(response.status).to eq(200)
            expect(@some_user.reload.institutions.first.name).to eq(inst.name)
        end

        it "can update active_institution" do
            @some_user.institutions = [Institution.quantic, Institution.smartly]
            @some_user.active_institution = Institution.quantic
            @some_user.save!
            put :update, params: {record: {id: @some_user.id, active_institution: Institution.smartly, updated_at: @some_user.updated_at.to_timestamp}, format: :json}
            expect(response.status).to eq(200)
            expect(@some_user.reload.active_institution).to eq(Institution.smartly)
        end

        it "can remove all groups" do
            @some_user.add_to_group('GROUP')

            put :update, params: { record: { id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp,  groups: [] }, format: :json }

            # FIXME: this works ...  =/
            # put :update, params: { record: { id: @some_user.id, groups: [nil] }, format: :json }

            expect(response.status).to eq(200)

            expect(User.find(@some_user.id).group_names).to eq([])
        end

        it "can update professional organization" do
            organization = ProfessionalOrganizationOption.first
            put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, professional_organization: {locale: organization.locale, text: organization.text}}, format: :json}
            expect(response.status).to eq(200)
            expect(@some_user.reload.professional_organization_option_id).to eq(organization.id)
        end

        it "can remove professional organization if blank text supplied" do
            # set it up first
            organization = ProfessionalOrganizationOption.first
            @some_user.professional_organization = organization
            @some_user.save!

            # remove it!
            put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, professional_organization: {locale: organization.locale, text: ''}}, format: :json}
            expect(response.status).to eq(200)
            expect(@some_user.reload.professional_organization_option_id).to be_nil
        end

        it "can update phone number" do
            put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, phone: '+16015551337'}, format: :json}
            expect(response.status).to eq(200)
            expect(@some_user.reload.phone).to eq('+16015551337')
        end

        it "can update name" do
            put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, name: 'Testy McTester'}, format: :json}
            expect(response.status).to eq(200)
            expect(@some_user.reload.name).to eq('Testy McTester')
        end

        it "should update fallback_program_type" do
            put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, fallback_program_type: 'testing'}, format: :json}
            expect(response.status).to eq(200)
            expect(@some_user.reload.program_type).to eq('testing')
        end

        it "can update english_language_proficiency_comments" do
            expect(@some_user.english_language_proficiency_comments).not_to eq('Some comments!')
            put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, english_language_proficiency_comments: 'Some comments!'}, format: :json}
            expect(response.status).to eq(200)
            expect(@some_user.reload.english_language_proficiency_comments).to eq('Some comments!')
        end

        it "can update english_language_proficiency_documents_type" do
            expect(@some_user.english_language_proficiency_documents_type).not_to eq('Some documentation type!')
            put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, english_language_proficiency_documents_type: 'Some documentation type!'}, format: :json}
            expect(response.status).to eq(200)
            expect(@some_user.reload.english_language_proficiency_documents_type).to eq('Some documentation type!')
        end

        it "can update english_language_proficiency_documents_approved" do
            expect(@some_user.english_language_proficiency_documents_approved).not_to be(true)
            put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, english_language_proficiency_documents_approved: true}, format: :json}
            expect(response.status).to eq(200)
            expect(@some_user.reload.english_language_proficiency_documents_approved).to be(true)
        end

        it "can update academic_hold" do
            expect(@some_user.academic_hold).to be(false)
            put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, academic_hold: true}, format: :json}
            expect(response.status).to eq(200)
            expect(@some_user.reload.academic_hold).to be(true)
        end

        it "can update mailing address fields" do
            expect(@some_user.address_line_1).not_to be('41 Court Square')
            expect(@some_user.address_line_2).not_to be('Suite D3')
            expect(@some_user.city).not_to be('Harrisonburg')
            expect(@some_user.state).not_to be('VA')
            expect(@some_user.zip).not_to be('22802')
            expect(@some_user.country).not_to be('US')
            expect(@some_user.phone).not_to be('+15405555555')
            put :update, params: {
                    record: {
                        id: @some_user.id,
                        updated_at: @some_user.updated_at.to_timestamp,
                        address_line_1: '41 Court Square',
                        address_line_2: 'Suite D3',
                        city: 'Harrisonburg',
                        state: 'VA',
                        zip: '22802',
                        country: 'US',
                        phone: '+15405555555'
                    },
                format: :json
            }
            expect(response.status).to eq(200)
            expect(@some_user.reload.address_line_1).to eq('41 Court Square')
            expect(@some_user.reload.address_line_2).to eq('Suite D3')
            expect(@some_user.reload.city).to eq('Harrisonburg')
            expect(@some_user.reload.state).to eq('VA')
            expect(@some_user.reload.zip).to eq('22802')
            expect(@some_user.reload.country).to eq('US')
            expect(@some_user.reload.phone).to eq('+15405555555')
        end

        it "should not update some_user.favorite_lesson_stream_locale_packs if param is not provided" do
            orig_favorites = @some_user.favorite_lesson_stream_locale_packs

            streams = Lesson::Stream.where.not(locale_pack_id: nil).limit(3)
            expected_locale_packs = streams.map(&:locale_pack)
            @some_user.favorite_lesson_stream_locale_packs = expected_locale_packs
            @some_user.save!

            put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp}, format: :json}
            expect(response.status).to eq(200)
            expect(User.find_by_id(@some_user.id).favorite_lesson_stream_locale_packs.to_a).to match_array(expected_locale_packs)

            @some_user.favorite_lesson_stream_locale_packs = orig_favorites
            @some_user.save!
        end

        it "can remove all favorite_lesson_stream_locale_packs" do
            locale_packs = Lesson::Stream::LocalePack.limit(3)
            @some_user.favorite_lesson_stream_locale_packs = locale_packs
            @some_user.save!

            put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, favorite_lesson_stream_locale_packs: []}, format: :json}
            expect(response.status).to eq(200)
            expect(User.find_by_id(@some_user.id).favorite_lesson_stream_locale_packs.to_a).to eq([])
        end

        it "does not allow a non-admin to update another user" do
            stub_current_user(controller, [:learner])
            another_user = User.where('id != ?', controller.current_user.id).first
            put :update, params: {record: {id: another_user.id}, format: :json}
            assert_error_response(401, {'message' => "You are not authorized to do that."})
        end

        it "only allows a non-admin to update certain things" do
            stub_current_user(controller, [:learner])
            u = controller.current_user
            u.save!
            expect(u.has_seen_welcome).to be(false)
            expect(u.has_logged_in).to be(false)
            expect(u.has_seen_mba_submit_popup).to be(false)
            expect(u.has_seen_short_answer_warning).to be(false)
            expect(u.has_seen_featured_positions_page).to be(false)
            expect(u.has_seen_hide_candidate_confirm).to be(false)
            expect(u.has_seen_student_network).to be(false)
            expect(u.has_seen_hiring_tour).to be(false)
            expect(u.english_language_proficiency_comments).not_to eq('Some comments!')

            # mailing address fields
            expect(u.address_line_1).not_to be('41 Court Square')
            expect(u.address_line_2).not_to be('Suite D3')
            expect(u.city).not_to be('Harrisonburg')
            expect(u.state).not_to be('VA')
            expect(u.zip).not_to be('22802')
            expect(u.country).not_to be('US')
            expect(u.phone).not_to be('+15405555555')

            streams = Lesson::Stream.all_published.where.not(locale_pack_id: nil).limit(3)
            expected_locale_packs = streams.map(&:locale_pack)

            expect_any_instance_of(User).not_to receive(:replace_groups)
            expect_any_instance_of(User).not_to receive(:institutions=)
            expect_any_instance_of(User).not_to receive(:roles=)

            put :update, params: {record: {
                id: u.id,
                groups: [{}],
                institutions: [{}],
                roles: [{}],
                has_seen_welcome: true,
                has_logged_in: true,
                has_seen_mba_submit_popup: true,
                has_seen_short_answer_warning: true,
                has_seen_featured_positions_page: true,
                has_seen_hide_candidate_confirm: true,
                has_seen_student_network: true,
                has_seen_hiring_tour: true,
                english_language_proficiency_comments: 'Some comments!',
                favorite_lesson_stream_locale_packs: expected_locale_packs.map { |pack| {id: pack.id} },

                # mailing address fields
                address_line_1: '41 Court Square',
                address_line_2: 'Suite D3',
                city: 'Harrisonburg',
                state: 'VA',
                zip: '22802',
                country: 'US',
                phone: '+15405555555'
            }, format: :json}

            u.reload
            expect(u.has_seen_welcome).to be(true)
            expect(u.has_logged_in).to be(true)
            expect(u.has_seen_mba_submit_popup).to be(true)
            expect(u.has_seen_short_answer_warning).to be(true)
            expect(u.has_seen_featured_positions_page).to be(true)
            expect(u.has_seen_hide_candidate_confirm).to be(true)
            expect(u.has_seen_student_network).to be(true)
            expect(u.has_seen_hiring_tour).to be(true)
            expect(u.english_language_proficiency_comments).to eq('Some comments!')
            expect(u.favorite_lesson_stream_locale_packs).to match_array(expected_locale_packs)

            # mailing address fields
            expect(u.reload.address_line_1).to eq('41 Court Square')
            expect(u.reload.address_line_2).to eq('Suite D3')
            expect(u.reload.city).to eq('Harrisonburg')
            expect(u.reload.state).to eq('VA')
            expect(u.reload.zip).to eq('22802')
            expect(u.reload.country).to eq('US')
            expect(u.reload.phone).to eq('+15405555555')
        end

        it "can update recommended_positions_seen_ids" do
            expect(@some_user.recommended_positions_seen_ids).to be_empty
            ids = [SecureRandom.uuid, SecureRandom.uuid]
            put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, recommended_positions_seen_ids: ids}, format: :json}
            assert_200
            expect(@some_user.reload.recommended_positions_seen_ids).to eq(ids)
        end

        it "should error if this user has been changed since the user was loaded in the client" do
            user = User.joins(:roles).where("roles.name = 'learner'").first
            user_json = user.as_json.merge(
                name: 'updated'
            )
            user.update!(updated_at: Time.now + 1.second)

            put :update, params: {format: :json, record: user_json}.with_indifferent_access
            assert_error_response(406, {message: "Changes have been made to this user since you last loaded them.  Please refresh and try again."})
            expect(user.reload.name).not_to eq('updated')

        end

        describe "process identity_verified" do
            it "removes s3_identification_asset if identity_verified is set to true" do
                S3IdentificationAsset.create!({
                    :user_id => @some_user.id
                })
                expect(@some_user.s3_identification_asset).not_to be(nil)
                expect(@some_user.identity_verified).not_to be(true)

                put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, identity_verified: true}, format: :json}
                expect(response.status).to eq(200)

                expect(User.find(@some_user.id).s3_identification_asset).to be(nil)
            end

            it "should log an event when identity_verified is being set to true" do
                @some_user.identity_verified = false
                @some_user.save!

                admin_user = users(:admin)
                allow(controller).to receive(:current_user).and_return(admin_user)

                event = Event.new
                allow(event).to receive(:log_to_external_systems)

                expect(Event).to receive(:create_server_event!).with(anything, admin_user.id, 'private_user_documents:identity_verified', {
                    label: @some_user.id,
                    verified_by: admin_user.id
                }).and_return(event)
                put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, identity_verified: true}, format: :json}

                # Should not log if calling update again with identity_verified already set to true
                expect(Event).not_to receive(:create_server_event!)
                put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, identity_verified: true}, format: :json}
            end
        end

        describe "process_education_experience_updates" do
            it "should update education_experiences, transcript types, and return a recalculated transcripts_verified field" do
                first_experience = education_experiences(:education_experience_with_no_transcripts)
                second_experience = education_experiences(:education_experience_with_two_transcripts)
                education_experiences = [first_experience, second_experience]

                first_experience.update_columns(will_not_complete: false, graduation_year: Date.current.year - 1)
                second_experience.update_columns(will_not_complete: false, graduation_year: Date.current.year - 1)

                second_experience.transcripts.update_all(transcript_type: nil)

                @some_user = User.joins(:career_profile).where(transcripts_verified: false).first
                @some_user.career_profile.education_experiences = education_experiences
                @some_user.career_profile.save!

                expect(@some_user.career_profile.education_experiences_indicating_transcript_required.size).to be(2) # sanity check

                admin_user = users(:admin)
                allow(controller).to receive(:current_user).and_return(admin_user)

                update_hash = education_experiences.as_json
                update_hash = update_hash.map(&:deep_stringify_keys)

                update_hash[0]['transcript_waiver'] = 'test waiver'
                update_hash[1]['transcript_approved'] = true
                update_hash[1]['transcripts'][1]['transcript_type'] = 'official'

                put :update, params: {
                    record: {
                        id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp
                    },
                    meta: {
                        education_experiences: update_hash
                    },
                    format: :json
                }

                @some_user.reload

                expect(CareerProfile::EducationExperience.find(update_hash[0]['id']).transcript_waiver).to eq('test waiver')
                expect(CareerProfile::EducationExperience.find(update_hash[1]['id']).transcript_approved).to be(true)
                expect(S3TranscriptAsset.find(update_hash[1]['transcripts'][1]['id']).transcript_type).to eq('official')
                expect(@some_user.transcripts_verified).to be(true)
                expect(body_json["contents"]["users"][0]['transcripts_verified']).to be(true)
            end
        end

        describe "career_profile" do

            before(:each) do
                @user = users(:user_with_career_profile)
            end

            it "should edit a career_profile" do
                put :update, params: {record: {id: @user.id, updated_at: @user.updated_at.to_timestamp, career_profile: {
                    id: @user.career_profile.id,
                    profile_feedback: 'profile_feedback',
                    interested_in_joining_new_company: 'interested_in_joining_new_company'
                }}, format: :json}
                expect(response.status).to eq(200)

                career_profile = User.find(@user.id).career_profile
                expect(career_profile).not_to be_nil
                expect(career_profile.profile_feedback).to eq('profile_feedback')
                expect(career_profile.interested_in_joining_new_company).to eq('interested_in_joining_new_company')
            end
        end

        describe "hiring_application" do

            before(:each) do
                @some_user = User.where.not(id: HiringApplication.pluck('user_id')).first
                @some_user.professional_organization = ProfessionalOrganizationOption.reorder(:text).first
                @some_user.ensure_hiring_team!
                @some_user.save!
            end

            it "should create a hiring_application" do
                put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, hiring_application: {
                    status: 'accepted'
                }}, format: :json}
                expect(response.status).to eq(200)

                expect(User.find(@some_user.id).hiring_application).not_to be_nil
                expect(User.find(@some_user.id).hiring_application.status).to eq('accepted')
            end

            it "should edit a hiring_application" do
                HiringApplication.create!({
                    user_id: @some_user.id,
                    status: 'pending',
                    applied_at: Time.now
                })
                put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, hiring_application: {
                    status: 'accepted'
                }}, format: :json}
                expect(response.status).to eq(200)

                expect(User.find(@some_user.id).hiring_application).not_to be_nil
                expect(User.find(@some_user.id).hiring_application.status).to eq('accepted')
                expect(User.find(@some_user.id).hiring_application.accepted_at).to be_within(10.seconds).of(Time.now)
            end

            it "should remove a hiring_application" do
                @request.env["CONTENT_TYPE"] = "application/json"
                HiringApplication.create!({
                    user_id: @some_user.id,
                    status: 'pending',
                    applied_at: Time.now
                })
                put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, hiring_application: nil}, format: :json}
                expect(response.status).to eq(200)

                expect(User.find(@some_user.id).hiring_application).to be_nil
            end
        end

        describe "as learner" do
            before(:each) do
                stub_current_user(:learner)
                @user = controller.current_user
            end

            it "updates name for a learner" do
                put :update, params: {record: {id: @user.id, updated_at: @user.updated_at.to_timestamp, name: 'new name'}, format: :json}
                expect(@user.reload.name).to eq('new name')
            end
        end

        describe "cohort_applications" do

            it "should add a cohort_application" do
                expect(@some_user.cohort_applications).to be_empty
                cohort = Cohort.first

                put :update, params: {format: :json, record: {
                    id: @some_user.id,
                    updated_at: @some_user.updated_at.to_timestamp,
                    cohort_applications: [{
                        cohort_id: cohort.id,
                        status: "pending"
                    }]
                }}.with_indifferent_access
                assert_200
                application = @some_user.reload.cohort_applications.first
                expect(@some_user.can_edit_career_profile).to be(false)
                expect(application).not_to be_nil
                expect(application.cohort_id).to eq(cohort.id)
                expect(application.applied_at).to be_within(10.seconds).of(Time.now)
            end

            it "should update an existing cohort_application" do
                @some_user = users(:pending_mba_cohort_user)
                cohort_application = @some_user.cohort_applications[0]
                expect(cohort_application.status).not_to eq('accepted')
                expect(@some_user.can_edit_career_profile).to be(false)

                put :update, params: {format: :json, record: {
                    id: @some_user.id,
                    updated_at: @some_user.updated_at.to_timestamp,
                    cohort_applications: [cohort_application.as_json.merge(
                        status: 'accepted',
                        cohort_slack_room_id: cohort_application.cohort.slack_rooms[0]&.id,
                        graduation_status: 'graduated'
                    )]
                }}.with_indifferent_access
                assert_200
                @some_user.reload
                expect(@some_user.can_edit_career_profile).to be(false)
                expect(cohort_application.reload.status).to eq('accepted')
                expect(cohort_application.reload.graduation_status).to eq('graduated')
            end

            it "should remove existing cohort_applications as desired" do
                @some_user = users(:pending_mba_cohort_user)
                cohort_application = @some_user.cohort_applications[0]

                put :update, params: {format: :json, record: {
                    id: @some_user.id,
                    updated_at: @some_user.updated_at.to_timestamp,
                    cohort_applications: []
                }}.with_indifferent_access
                assert_200
                @some_user.reload
                expect(@some_user.can_edit_career_profile).to be(false)
                expect(Proc.new {
                    cohort_application.reload
                }).to raise_error(ActiveRecord::RecordNotFound)
            end

            it "should error if a cohort_application has been changed since the user was loaded in the client" do
                cohort_application = CohortApplication.where(status: 'accepted', graduation_status: 'pending').first
                cohort_application_json = cohort_application.as_json.merge(
                    graduation_status: 'graduated'
                )
                cohort_application.update(updated_at: Time.now)

                put :update, params: {format: :json, record: {
                    id: cohort_application.user_id,
                    updated_at: cohort_application.user.updated_at.to_timestamp,
                    cohort_applications: [cohort_application_json]
                }}.with_indifferent_access
                assert_error_response(406, {message: "Changes have been made to this user since you last loaded them.  Please refresh and try again."})
                expect(cohort_application.reload.graduation_status).not_to eq('graduated')

            end

            describe "notes" do
                it "should respond with notes if an admin" do
                    @some_user = users(:pending_mba_cohort_user)
                    cohort_application = @some_user.cohort_applications[0]

                    put :update, params: {format: :json, record: {
                        id: @some_user.id,
                        updated_at: @some_user.updated_at.to_timestamp,
                        cohort_applications: [cohort_application.as_json.merge(
                            status: 'pre_accepted'
                        )]
                    }}.with_indifferent_access
                    assert_200

                    expect(body_json["contents"]["users"][0]["cohort_applications"][0]).to have_key("notes")
                end

                it "should respond without notes if not an admin" do
                    @some_user = users(:pending_mba_cohort_user)
                    stub_current_user(controller, @some_user)

                    put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, name: 'new name'}, format: :json}
                    assert_200

                    expect(body_json["contents"]["users"][0]["cohort_applications"][0]).not_to have_key("notes")
                end
            end

        end

        it "should record a timeline event for the enrollment action" do
            @user = User.left_outer_joins(:hiring_application, :subscriptions)
                .where("hiring_applications.id is null and subscriptions.id is null").first
            put :update,
                params: {record: {id: @user.id, updated_at: @user.updated_at.to_timestamp },
                meta: {
                    enrollment_action_label: 'some_label',
                    subscription_cancellation_reason: 'some reason'
                },
                format: :json}
            assert_200

            event = @user.persisted_timeline_events.reorder(:created_at).last
            expect(event.created_at).to be_within(5.seconds).of(Time.now)
            expect(event.attributes.symbolize_keys).to include(
                event: 'performed_action',
                text: 'some_label',
                subtext: "with reason: some reason",
                editor_id: controller.current_user.id,
                editor_name: controller.current_user.name,
                category: 'enrollment'
            )
        end

        it "should record a billing_transaction" do
            @user = User.first
            transaction_time = Time.parse('2017-01-01')
            put :update,
                params: {record: {id: @user.id, updated_at: @user.updated_at.to_timestamp },
                meta: {
                    billing_transaction_to_record: {
                        amount: 4200,
                        provider: 'SVB - 38322',
                        provider_transaction_id: '12345',
                        transaction_time: transaction_time.to_timestamp,
                        transaction_type: 'payment'
                    }
                },
                format: :json}
            assert_200

            billing_transaction = BillingTransaction.reorder(:created_at).last
            expect(billing_transaction.user).to eq(@user)
            expect(billing_transaction.amount).to eq(4200)
            expect(billing_transaction.provider).to eq('SVB - 38322')
            expect(billing_transaction.provider_transaction_id).to eq('12345')
            expect(billing_transaction.transaction_time).to eq(transaction_time)
        end

        describe "subscriptions" do
            before(:each) do
                @user = users(:user_with_subscriptions_enabled)
                create_subscription(@user)
            end

            it "should allow admins to cancel a user's primary_subscription if specifying subscription_cancellation_reason in meta" do
                expect(@user.primary_subscription).not_to be_nil
                expect_any_instance_of(Subscription).to receive(:cancel_and_destroy).and_call_original
                put :update, params: {record: {id: @user.id, updated_at: @user.updated_at.to_timestamp }, meta: { subscription_cancellation_reason: 'some_reason' }, format: :json}

                @user.subscriptions.reload
                expect(@user.primary_subscription).to be_nil
                expect(body_json["contents"]["users"][0]["primary_subscription"]).to be_nil
            end

            # see https://trello.com/c/AQx1no5e
            # we had an issue where registered was being toggled off and back on when using
            # the "cancelFullyPaid" option in the admin
            it "should not toggle registered when updating cohort application and subscription" do
                expect(@user.primary_subscription).not_to be_nil
                expect(@user.accepted_application.registered).to be(true)
                expect(@user.accepted_application.total_num_required_stripe_payments).to be > 0
                expect_any_instance_of(CohortApplication).not_to receive(:log_toggle_registered_event)

                start = Time.now
                put :update, params: {
                    record: {
                        id: @user.id,
                        updated_at: @user.updated_at.to_timestamp,
                        cohort_applications: [
                            @user.accepted_application.as_json.merge({
                                "total_num_required_stripe_payments" => 0
                            })
                        ]
                    },
                    meta: {
                        subscription_cancellation_reason: 'some_reason'
                    },
                format: :json}

                @user.subscriptions.reload
                expect(@user.primary_subscription).to be_nil


            end

            it "should not cancel a user's primary_subscription without subscription_cancellation_reason in the meta" do
                expect(@user.primary_subscription).not_to be_nil
                put :update, params: {record: {id: @user.id, updated_at: @user.updated_at.to_timestamp }, meta: {}, format: :json}

                @user.subscriptions.reload
                expect(@user.primary_subscription).not_to be_nil
                expect(body_json["contents"]["users"][0]["subscriptions"][0]['id']).to eq(@user.primary_subscription.id)
            end

        end

        describe "refunds" do
            before(:each) do
                @user = CohortApplication.first.user
            end

            it "should allow admins to issue a refund" do
                expect_any_instance_of(CohortApplication).to receive(:issue_refund!)
                put :update, params: {record: {id: @user.id, updated_at: @user.updated_at.to_timestamp },
                    meta: { issue_refund: true },
                    format: :json}

                assert_200
            end

            it "should handle errors in issue_refund!" do

                allow_any_instance_of(CohortApplication).to receive(:reset_payment_information_if_needed).and_return(nil)

                expect_any_instance_of(CohortApplication).to receive(:issue_refund!) do |cohort_application|
                    # If there is an error then refund_attempt_failed will have been set to true.  I would
                    # like to check that this actually gets saved, that the error does not
                    # bubble up and kill the transaction. I'm not sure this actually works as
                    # a test though, since the whole test is wrapped in a transaction.  But,
                    # doesn't hurt to leave it here.
                    cohort_application.update(refund_attempt_failed: true)
                    raise Exception.new("An error message")
                end

                put :update, params: {record: {id: @user.id, updated_at: @user.updated_at.to_timestamp },
                    meta: { issue_refund: true },
                    format: :json}

                assert_200
                expect(@user.last_application.reload.refund_attempt_failed).to eq(true)
                expect(body_json["meta"]["error_issuing_refund"]).to eq("An error message")
            end

        end

        describe('favorite_lesson_stream_locale_packs') do
            attr_reader :en_only_stream, :en_stream, :es_stream, :user

            before(:each) do
                @en_only_stream = lesson_streams(:en_only_item)
                @en_stream = lesson_streams(:en_item)
                @es_stream = lesson_streams(:es_item)
            end

            it "adds to favorite_lesson_stream_locale_packs" do
                @some_user.favorite_lesson_stream_locale_packs = [en_only_stream.locale_pack]

                # add a stream
                put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, favorite_lesson_stream_locale_packs: pack_entries(en_only_stream.locale_pack, en_stream.locale_pack)}, format: :json}
                expect(response.status).to eq(200)
                expect(@some_user.favorite_lesson_stream_locale_packs.reload.to_a).to match_array([
                    en_only_stream.locale_pack,
                    en_stream.locale_pack
                ])
            end

            it "makes no changes to favorite_lesson_stream_locale_packs" do
                @some_user.favorite_lesson_stream_locale_packs = [en_only_stream.locale_pack, en_stream.locale_pack]

                # change nothing
                put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, favorite_lesson_stream_locale_packs: pack_entries(en_only_stream.locale_pack, en_stream.locale_pack)}, format: :json}
                expect(response.status).to eq(200)
                expect(@some_user.favorite_lesson_stream_locale_packs.reload.to_a).to match_array([
                    en_only_stream.locale_pack,
                    en_stream.locale_pack
                ])
            end

            it "removes a stream from favorite_lesson_stream_locale_packs" do
                @some_user.favorite_lesson_stream_locale_packs = [en_only_stream.locale_pack, en_stream.locale_pack]
                put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, favorite_lesson_stream_locale_packs: pack_entries(en_only_stream.locale_pack)}, format: :json}
                expect(response.status).to eq(200)
                expect(@some_user.favorite_lesson_stream_locale_packs.reload.to_a).to match_array([
                    en_only_stream.locale_pack
                ])
            end

            it "does when the incoming favorite_lesson_stream_locale_pack is a translation of an existing one" do
                @some_user.favorite_lesson_stream_locale_packs = [en_stream.locale_pack]

                # es_stream and en_stream have the same locale pack, so nothing should change
                put :update, params: {record: {id: @some_user.id, updated_at: @some_user.updated_at.to_timestamp, favorite_lesson_stream_locale_packs: pack_entries(es_stream.locale_pack)}, format: :json}
                expect(response.status).to eq(200)
                expect(@some_user.favorite_lesson_stream_locale_packs.reload.to_a).to match_array([
                    en_stream.locale_pack
                ])
            end

            def pack_entries(*packs)
                packs.map do |pack|
                    {
                        id: pack.id
                    }.with_indifferent_access
                end
            end

        end

    end

    describe "POST create" do
        before (:each) do
            @new_user_params = {
                email: "#{rand}@pedago.com",
                name: "Joe User",
                password: "password",
                sign_up_code: 'ADMIN'
            }
        end

        it "can create a user with valid params" do
            post :create, params: {record: @new_user_params, format: :json}

            expect(response.status).to eq(200)
            expect(body_json["contents"]["users"].count).to eq(1)
            expect(body_json["contents"]["users"][0]["email"]).to eq(@new_user_params[:email])
        end

        it "can handle varying case in user email if configured to do so" do
            User.case_insensitive_keys = [:email]
            email = "KatelynkinG@gmail.com"
            post :create, params: {record: @new_user_params.merge({email: email}), format: :json}

            expect(response.status).to eq(200)
            expect(body_json["contents"]["users"].count).to eq(1)
            expect(body_json["contents"]["users"][0]["email"]).to eq(email.downcase)
        end

        it "errors on creating a user with a duplicate email" do
            User.create!(@new_user_params.merge(uid: @new_user_params[:email]))

            post :create, params: {record: @new_user_params, format: :json}

            expect(response.status).to eq(406)
            expect(body_json["message"].include?("Validation failed: Email has already been taken")).to be(true)
        end

        it "can create a user with groups, roles, external institutions, and active_institution" do
            AccessGroup.find_or_create_by(name: "SOME_GROUP")
            AccessGroup.find_or_create_by(name: "SOME_OTHER_GROUP")
            inst = Institution.find_or_create_by(name: "DEMO", sign_up_code: "Code", external: true)
            @new_user_params[:groups] = [{name: "SOME_GROUP"}, {name: "SOME_OTHER_GROUP"}]
            learner_role = Role.find_or_create_by(name: "learner")
            @new_user_params[:roles] =  [learner_role]
            @new_user_params[:institutions] =  [{id: inst.id}]
            @new_user_params[:active_institution] = {id: inst.id}

            post :create, params: {record: @new_user_params, format: :json}

            expect(response.status).to eq(200), body_json.to_s
            expect(body_json["contents"]["users"].count).to eq(1)
            expect(body_json["contents"]["users"][0]["roles"].count).to eq(1)
            expect(body_json["contents"]["users"][0]["roles"][0]["name"]).to eq("learner")
            expect(body_json["contents"]["users"][0]["groups"].count).to eq(2)
            expect(body_json["contents"]["users"][0]["groups"].map { |e| e['name'] }.sort).to eq(['SOME_GROUP', 'SOME_OTHER_GROUP'])

            user = User.find_by_email(@new_user_params[:email])
            expect(user.institutions.count).to eq(1)
            expect(user.institutions[0].name).to eq("DEMO")
            expect(user.active_institution.name).to eq("DEMO")
        end

        it "can create a consumer user" do
            @new_user_params[:groups] = []
            learner_role = Role.find_or_create_by(name: "learner")
            @new_user_params[:roles] =  [learner_role]

            post :create, params: {record: @new_user_params, format: :json}

            expect(response.status).to eq(200)
            expect(body_json["contents"]["users"].count).to eq(1)
            expect(body_json["contents"]["users"][0]["roles"].count).to eq(1)
            expect(body_json["contents"]["users"][0]["roles"][0]["name"]).to eq("learner")
        end

        it "can create an institutional user" do
            inst = Institution.find_or_create_by(name: "DEMO", sign_up_code: "Code")

            learner_role = Role.find_or_create_by(name: "learner")
            @new_user_params[:roles] =  [learner_role]
            @new_user_params[:institutions] =  [{id: inst.id}]

            post :create, params: {record: @new_user_params, format: :json}

            expect(response.status).to eq(200), body_json.to_s
            expect(body_json["contents"]["users"].count).to eq(1)
            expect(body_json["contents"]["users"][0]["roles"].count).to eq(1)
            expect(body_json["contents"]["users"][0]["roles"][0]["name"]).to eq("learner")
            expect(body_json["contents"]["users"][0]["groups"].count).to eq(0)

            expect(body_json["contents"]["users"][0]["institutions"].count).to eq(1)
            expect(body_json["contents"]["users"][0]["institutions"][0]["name"]).to eq("DEMO")
        end

    end

    describe "log_in_as" do
        before (:each) do
            stub_current_user(controller, [:admin], ["GROUP"])
            @some_user = FactoryBot.create(:user)
            @some_user.add_role("learner")
            @some_user.save!
            sign_in @some_user
        end

        it "lets admins log in as some arbitrary user" do
            post :log_in_as, params: {record: {logged_in_as_user_id: @some_user.id}, format: :json}
            expect(response.status).to eq(200)
        end

        it "handles bad ids" do
            post :log_in_as, params: {record: {logged_in_as_user_id: "asdasdasd"}, format: :json}
            expect(response.status).to eq(404)
        end

        it "does not let learners log in as some arbitrary user" do
            stub_current_user(controller, [:learner], ["GROUP"])

            post :log_in_as, params: {record: {logged_in_as_user_id: @some_user.id}, format: :json}
            expect(response.status).to eq(401)
        end

    end

    describe "DELETE " do

        describe "with valid params" do
            it "deletes a user" do
                user = @some_user = FactoryBot.create(:user)
                expect {
                    delete :destroy, params: {:id => user.id, format: :json}
                }.to change(User, :count).by(-1)
                expect(response.status).to eq(200)
            end

            it "inserts a user_deletions record into the audit table" do
                user = @some_user = FactoryBot.create(:user)
                delete :destroy, params: {:id => user.id, format: :json}
                expect(response.status).to eq(200)
                user_deletion = ActiveRecord::Base.connection.execute("
                    SELECT *
                    FROM users_deletions
                    WHERE user_id='#{user.id}'
                     AND admin_id='#{controller.current_user.id}'
                     AND admin_name='#{controller.current_user.name}'
                ").to_a.first
                expect(user_deletion).not_to be(nil)
            end

            it "renders 406 if user cannot be destroyed" do
                user = FactoryBot.create(:user)
                err = ActiveRecord::RecordInvalid.new(user);
                expect_any_instance_of(User).to receive(:destroy).and_raise(err)

                expect {
                    delete :destroy, params: {:id => user.id, format: :json}
                }.not_to change(User, :count)
                expect(response.status).to eq(406)
            end
        end

    end

    describe "PUT 'batch_update'" do

        it "should 401 on non-admin" do
            stub_current_user(:super_editor)
            put :batch_update, params: {format: :json}
            assert_error_response(401, {'message' => "You are not authorized to do that."})
        end

        it "should update" do
            user, updater = expect_updater_exec
            expect(updater).to receive(:errors).and_return({})

            put :batch_update, params: {
                :user_ids => [user.id],
                :cohort_id => "cohort_id",
                :cohort_status => "cohort_status",
                :graduation_status => "graduation_status",
                format: :json}
            assert_200
            expect(body_json['contents']['users'].map { |e| e['id']}).to eq([user.id])
        end

        it "should return errors" do
            user, updater = expect_updater_exec
            expect(updater).to receive(:errors).and_return({"a" => 1})
            put :batch_update, params: {
                    user_ids: [user.id],
                    cohort_id: "cohort_id",
                    cohort_status: "cohort_status",
                    graduation_status: "graduation_status",
                    format: :json
                }
            assert_error_response(406, {"message" => "", "errors" => {"a" => 1}})
        end

        def expect_updater_exec
            user = FactoryBot.create(:user)
            updater = double(:batch_updater)
            expect(User::BatchUpdater).to receive(:new).with(controller.current_user, [user.id], ActionController::Parameters.new({
                :cohort_id => "cohort_id",
                :cohort_status => "cohort_status",
                :graduation_status => "graduation_status"
            })).and_return(updater)
            expect(updater).to receive(:exec)
            [user, updater]
        end
    end

    describe "PUT batch_update_grades" do

        it "should render_unauthorized_error if current_ability cannot manage User" do
            stub_current_user(:super_editor)
            put :batch_update_grades, params: {format: :json}
            assert_error_response(401, {'message' => "You are not authorized to do that."})
        end

        describe "when authorized" do

            it "should call ProjectProgress.create_or_update!" do
                user = User.left_outer_joins(:project_progresses).where(project_progress: {id: nil}).first
                allow(Time).to receive(:now).and_return(Time.now)
                user = User.left_outer_joins(:project_progresses).where(project_progress: {id: nil}).first
                attrs = {
                    score: 2,
                    requirement_identifier: 'requirement_identifier',
                    waived: false,
                    marked_as_passed: false
                }
                expect(ProjectProgress).to receive(:create_or_update!).with(user, controller.current_user, hash_including(attrs)).and_call_original

                put :batch_update_grades, params: {
                    records: [{
                        id: user.id,
                        project_progress: [attrs]
                    }],
                    format: :json
                }
                assert_200

                expect(body_json["contents"]["users"][0]['id']).to eq(user.id)
                expect(body_json["contents"]["users"][0]['project_progress'].size).to eq(user.project_progresses.size)
            end
        end
    end

    describe "PUT batch_update_cohort_enrollment_status" do

        it "should render_unauthorized_error if current_ability cannot manage User" do
            stub_current_user(:super_editor)
            put :batch_update_cohort_enrollment_status, params: {format: :json}
            assert_error_response(401, {'message' => "You are not authorized to do that."})
        end

        describe "when authorized" do

            it "should render_error_response if no cohort_id meta param is present" do
                put :batch_update_cohort_enrollment_status, params: {meta: {}, format: :json}
                assert_error_response(400, {'message' => "Must specify a cohort_id in the meta params"})
            end

            describe "when cohort_id meta param is present" do

                it "should update_user_id_verifications_from_hashes and update transcripts_verified and english_language_proficiency_documents_approved flags on user" do
                    user = users(:accepted_mba_cohort_user)
                    cohort_application = user.cohort_applications.first

                    original_english_language_proficiency_documents_approved_value = user.english_language_proficiency_documents_approved

                    user_id_verification_id = SecureRandom.uuid
                    user_id_verification_attrs = {
                        id: user_id_verification_id,
                        cohort_id: cohort_application.cohort_id,
                        id_verification_period_index: 0,
                        user_id: user.id,
                        verification_method: 'verified_by_admin'
                    }

                    expect_any_instance_of(User).to receive(:update_user_id_verifications_from_hashes) do |user_instance, user_id_verification_hashes, _current_user|
                        user_id_verification_hash = user_id_verification_hashes.first
                        expect(user_instance).to eq(user)
                        expect(user_instance.id).to eq(user.id)
                        expect(user_id_verification_hashes.length).to eq(1)
                        expect(user_id_verification_hash['id']).to eq(user_id_verification_id)
                        expect(user_id_verification_hash['cohort_id']).to eq(cohort_application.cohort_id)
                        expect(user_id_verification_hash['id_verification_period_index']).to eq(0)
                        expect(user_id_verification_hash['user_id']).to eq(user.id)
                        expect(user_id_verification_hash['verification_method']).to eq('verified_by_admin')
                        expect(_current_user).to be(controller.current_user)
                    end

                    put :batch_update_cohort_enrollment_status, params: {
                        records: [{
                            id: user.id,
                            english_language_proficiency_documents_approved: !original_english_language_proficiency_documents_approved_value,
                            user_id_verifications: [user_id_verification_attrs]
                        }],
                        meta: {
                            cohort_id: cohort_application.cohort_id
                        },
                        format: :json
                    }
                    assert_200

                    user.reload
                    expect(user.english_language_proficiency_documents_approved).to be(!original_english_language_proficiency_documents_approved_value)
                end

                it "should render_records for users, making sure to get the id and user_id_verifications fields" do
                    user = users(:accepted_mba_cohort_user)
                    cohort_application = user.cohort_applications.first

                    allow(User).to receive(:find).with([user.id]).and_return([user])
                    expect(controller).to receive(:render_records).with([user], {fields: ['id', 'user_id_verifications']}).and_call_original

                    put :batch_update_cohort_enrollment_status, params: {
                        records: [{
                            id: user.id
                        }],
                        meta: {
                            cohort_id: cohort_application.cohort_id
                        },
                        format: :json
                    }
                    assert_200
                end
            end
        end
    end

    describe "upload_avatar" do
        it "should set a new avatar" do
            user = User.first
            allow(controller).to receive(:current_user).and_return(user)
            avatar_asset = AvatarAsset.new

            expect(AvatarAsset).to receive(:new).with({
                :file => 'file'
            }).and_return(avatar_asset)

            put :upload_avatar, params: {
                    avatar_image: 'file',
                    id: user['id'],
                    format: :json
                }

            expect(user.reload.avatar).to eq(avatar_asset)
        end

        it "should handle invalid avatar" do

            user = User.first
            allow(controller).to receive(:current_user).and_return(user)
            avatar_asset = AvatarAsset.new
            allow(avatar_asset).to receive(:valid?) do
                avatar_asset.errors.add(:this_avatar, "is crap")
                false
            end

            expect(AvatarAsset).to receive(:new).with({
                :file => 'file'
            }).and_return(avatar_asset)

            put :upload_avatar, params: {
                avatar_image: 'file',
                id: user['id'],
                format: :json
            }

            assert_error_response(406, {"message"=>"Validation failed: Avatar is invalid"})
        end
    end

    describe "show_transcript_data" do
        it "should send back transcript data for oneself" do
            user = users(:accepted_mba_cohort_user)
            allow(controller).to receive(:current_user).and_return(user)

            expect(CohortUserProgressRecord).to receive(:where).and_return([OpenStruct.new({
                user_id: user.id,
                avg_test_score: 89.9
            })])
            expect_any_instance_of(Cohort::Version).to receive(:get_required_stream_locale_pack_ids).and_return(['bar'])
            expect_any_instance_of(Cohort::Version).to receive(:get_optional_stream_locale_pack_ids).and_return(['baz'])

            get :show_transcript_data, params: {id: user.id, format: :json}
            assert_200
            expect(body_json["required_stream_locale_pack_ids"]).to match_array(['bar'])
            expect(body_json["optional_stream_locale_pack_ids"]).to match_array(['baz'])
            expect(body_json["avg_test_score"]).to be(89.9)
        end

        it "should send back transcript data for another user if admin" do
            user = users(:admin)
            allow(controller).to receive(:current_user).and_return(user)
            another_user = users(:pre_accepted_mba_cohort_user)

            expect(CohortUserProgressRecord).to receive(:where).and_return([OpenStruct.new({
                user_id: another_user.id,
                avg_test_score: 89.9
            })])
            expect_any_instance_of(Cohort::Version).to receive(:get_required_stream_locale_pack_ids).and_return(['bar'])
            expect_any_instance_of(Cohort::Version).to receive(:get_optional_stream_locale_pack_ids).and_return(['baz'])

            get :show_transcript_data, params: {id: another_user.id, format: :json}
            assert_200
            expect(body_json["required_stream_locale_pack_ids"]).to match_array(['bar'])
            expect(body_json["optional_stream_locale_pack_ids"]).to match_array(['baz'])
            expect(body_json["avg_test_score"]).to be(89.9)
        end

        it "should render unauthorized error if asking for another user and not an admin" do
            user = users(:accepted_mba_cohort_user)
            allow(controller).to receive(:current_user).and_return(user)
            another_user = users(:pre_accepted_mba_cohort_user)
            get :show_transcript_data, params: {id: another_user.id, format: :json}
            assert_error_response(401, {'message' => "You are not authorized to do that."})
        end
    end

    def ensure_less_than_100_users
        if (count = User.count) > 100
            destroyable_users.limit(count + 1 - 100).destroy_all
        end
    end

    def destroyable_users
        ids_we_cannot_delete = Lesson.reorder(nil).distinct.pluck(:author_id) + [controller.current_user.id]
        ids_we_cannot_delete += HiringApplication.pluck(:user_id)
        ids_we_cannot_delete += SubscriptionUserJoin.pluck(:user_id)
        ids_we_cannot_delete += StudentNetworkEvent.pluck(:author_id)
        User.where("id not in (?) and email is not null", ids_we_cannot_delete)
    end

end
