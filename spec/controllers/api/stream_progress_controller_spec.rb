require 'spec_helper'
require 'controllers/api/helpers/api_content_controller_spec_helper'


describe Api::StreamProgressController do
    include ControllerSpecHelper
    json_endpoint

    fixtures(:lesson_streams_progress, :lesson_streams, :users, :institutions)

    before(:each) do
        stub_client_params(controller)
        stub_current_user(controller, [:learner], ["GROUP"])

        @institutional_reports_user = users(:institutional_reports_viewer)
        @institution = @institutional_reports_user.views_reports_for_institutions.first

        @zapier_params = { format: :json, institution_id: @institution.id, zapier: true }
    end

    describe "index" do

        it "should 401 if disallowed" do
            expect(controller.current_ability).to receive(:can?).at_least(1).times do |meth, *args|
                if meth == :index_stream_progress
                    false
                else
                    true
                end
            end
            get :index, params: { format: :json}
            assert_error_response(401)
        end

        describe "with zapier=true" do
            describe "recent completions" do

                describe "zapier" do
                    before(:each) do
                        stub_current_user(controller, @institutional_reports_user)
                    end

                    it "should include extra params and remove unnecessary ones" do
                        get :index, params: @zapier_params
                        assert_200
                        stream_progress = body_json['contents']['lesson_streams_progress'][0]

                        expect(stream_progress['complete']).not_to be_nil
                        expect(stream_progress['user_name']).not_to be_nil
                        expect(stream_progress['email']).not_to be_nil
                        expect(stream_progress['phone']).not_to be_nil
                        expect(stream_progress['english_course_title']).not_to be_nil

                        # expect missing
                        expect(stream_progress['locale_pack_id']).to be_nil
                        expect(stream_progress['lesson_bookmark_id']).to be_nil
                        expect(stream_progress['time_runs_out_at']).to be_nil
                    end
                end

                describe "limit" do
                    before(:each) do
                        stub_current_user(controller, @institutional_reports_user)
                    end

                    it "should not limit if nothing passed in" do
                        get :index, params: @zapier_params
                        assert_200
                        # See FixtureBuilder.rb#build(Institution) block for where we set up this progress data
                        expect(body_json['contents']['lesson_streams_progress'].length).to eq(2)
                    end

                    it "should obey the limit if passed in" do
                        get :index, params: @zapier_params.merge({ limit: 1 })
                        assert_200
                        expect(body_json['contents']['lesson_streams_progress'].length).to eq(1)
                    end

                    it "should prevent negative numbers" do
                        get :index, params: @zapier_params.merge({ limit: -1 })
                        assert_200
                        expect(body_json['contents']['lesson_streams_progress']).to be_nil
                    end
                end

                describe "sorting" do
                    before(:each) do
                        stub_current_user(controller, @institutional_reports_user)
                    end

                    it "should sort by completed_at" do
                        # See FixtureBuilder.rb#build(Institution) block for where we set up this progress data
                        # Note: use Zapier params because they return the user info; makes it easier to test
                        get :index, params: @zapier_params.merge({ sort: 'completed_at', direction: 'asc' })
                        assert_200
                        stream_progress = body_json['contents']['lesson_streams_progress']
                        expect(stream_progress.length).to eq(2)
                        expect(stream_progress[0]['completed_at'] < stream_progress[1]['completed_at']).to be(true)

                        get :index, params: @zapier_params.merge({ sort: 'completed_at', direction: 'desc' })
                        assert_200
                        stream_progress = body_json['contents']['lesson_streams_progress']
                        expect(stream_progress.length).to eq(2)
                        expect(stream_progress[0]['completed_at'] > stream_progress[1]['completed_at']).to be(true)
                    end
                end
            end
        end

        describe "with user_id" do

            before(:each) do
                expect(controller).to receive(:verify_allowed_index_params)
            end

            it "should return all progress" do
                user = Lesson::StreamProgress.first.user
                get :index, params: {
                    filters: {
                        user_id: user.id
                    },
                    format: :json
                }
                assert_200
                expected_locale_pack_ids = Lesson::StreamProgress.where(user_id: user.id).pluck(:locale_pack_id)
                actual_locale_pack_ids = body_json['contents']['lesson_streams_progress'].map { |e| e['locale_pack_id']}
                expect(actual_locale_pack_ids).to match_array(expected_locale_pack_ids)
            end

            it "should include lesson progress when returning all progress" do
                user = LessonProgress.first.user
                get :index, params: {
                    filters: {
                        user_id: user.id
                    },
                    include_lesson_progress: true,
                    format: :json
                }
                assert_200
                expected_locale_pack_ids = LessonProgress.where(user_id: user.id).pluck(:locale_pack_id)
                actual_locale_pack_ids = body_json['meta']['lesson_progress'].map { |e| e['locale_pack_id']}
                expect(actual_locale_pack_ids).to match_array(expected_locale_pack_ids)
            end

            it "should support filtering by updated_since" do
                user_id = ActiveRecord::Base.connection.execute(%Q~
                    select
                        user_id
                    from
                        lesson_progress
                        join lesson_streams_progress
                            using (user_id)
                    group by user_id
                    having count(distinct lesson_progress.locale_pack_id) > 1
                        and count(distinct lesson_streams_progress.locale_pack_id) > 1
                    limit 1
                ~).to_a[0]['user_id']

                user = User.find(user_id)
                updated_since = Time.now
                last_lesson_progress = user.lesson_progresses.first
                last_lesson_progress.update!(updated_at: Time.now)
                last_stream_progress = user.lesson_streams_progresses.first
                last_stream_progress.update!(updated_at: Time.now)

                get :index, params: {
                    filters: {
                        user_id: user.id,
                        updated_since: updated_since.to_f
                    },
                    include_lesson_progress: true,
                    format: :json
                }
                assert_200

                actual_lesson_locale_pack_ids = body_json['meta']['lesson_progress'].map { |e| e['locale_pack_id']}
                expect(actual_lesson_locale_pack_ids).to match_array([last_lesson_progress.locale_pack_id])

                actual_stream_locale_pack_ids = body_json['contents']['lesson_streams_progress'].map { |e| e['locale_pack_id']}
                expect(actual_stream_locale_pack_ids).to match_array([last_stream_progress.locale_pack_id])
            end
        end
    end

    describe "basic progress tracking" do

        describe "destroy" do

            it "should 401 if disallowed" do
                stream_progress = Lesson::StreamProgress.first
                disallow_destroy(stream_progress)
                delete :destroy, params: {'id' => stream_progress.id, :format => :json }
                assert_error_response(401)
                expect(Lesson::StreamProgress.where(id: stream_progress.id).first).not_to be_nil
            end

            it "should destroy stream_progress and lesson_progress" do
                stream_progress = lesson_streams_progress(:completed_stream_progress)
                lesson_locale_pack_ids_in_stream = stream_progress.lesson_streams.where(locale: 'en').first.lessons.map(&:locale_pack_id)
                lesson_progress = LessonProgress.where(user_id: stream_progress.user_id, locale_pack_id: lesson_locale_pack_ids_in_stream).first
                expect(lesson_progress).not_to be_nil
                allow_destroy(stream_progress)
                delete :destroy, params: {'id' => stream_progress.id, :format => :json}
                assert_200
                expect(Lesson::StreamProgress.where(id: stream_progress.id).first).to be_nil
                expect(LessonProgress.where(id: lesson_progress.id).first).to be_nil
            end

            def allow_destroy(stream_progress)
                expect(Lesson::StreamProgress).to receive(:find_by_id).and_return(stream_progress)
                expect_any_instance_of(Ability).to receive(:can?).with(:destroy, stream_progress).at_least(1).times.and_return(true)
            end

            def disallow_destroy(stream_progress)
                expect(Lesson::StreamProgress).to receive(:find_by_id).and_return(stream_progress)
                expect_any_instance_of(Ability).to receive(:can?).with(:destroy, stream_progress).at_least(1).times.and_return(false)
            end

        end

        describe "update" do

            it "should reset time_runs_out_at" do

                stub_current_user(controller, [:admin])
                stream_progress = Lesson::StreamProgress.first
                stream_progress.update_attribute(:time_runs_out_at, Time.now)

                delete :update, params: {record: {:id => stream_progress.id, :time_runs_out_at => nil}, :format => :json}

                assert_200
                expect(stream_progress.reload.time_runs_out_at).to be_nil

            end

        end

        describe "destroy_all_progress" do

            def create_progress(user)
                Lesson::StreamProgress.delete_all
                # mark the stream as in-progress
                now = Time.now
                allow(DateTime).to receive(:now).and_return(now)
                Lesson::StreamProgress.create!({
                    :locale_pack_id => @stream.locale_pack_id,
                    :lesson_bookmark_id => @stream.lessons[0].id,
                    :started_at => now,
                    :user_id => user.id
                })
            end


            before(:each) do
                @stream = Lesson::Stream.all_published.first
                @lessons = @stream.lessons
            end


            it "should clear user progress for all streams" do

                user = users(:editor)
                allow(controller).to receive(:current_user).and_return(user)

                create_progress(user)

                # check that the stream progress has been recorded
                stream_progress = Lesson::StreamProgress.first
                expect(stream_progress).not_to be_nil

                # destroy progress for this lesson
                delete :destroy_all_progress, :format => :json
                assert_200

                # make sure this lesson is back to "not started"
                expect(Lesson::StreamProgress.find_by_id(stream_progress.id)).to be_nil

            end


            it "should not allow clearing for learner users" do

                user = users(:learner)
                allow(controller).to receive(:current_user).and_return(user)

                create_progress(user)

                # check that the stream progress has been recorded
                stream_progress = Lesson::StreamProgress.first
                expect(stream_progress).not_to be_nil

                # destroy progress for this lesson
                delete :destroy_all_progress, :format => :json
                assert_error_response(401)

                # make sure this lesson is back to "not started"
                expect(Lesson::StreamProgress.find_by_id(stream_progress.id)).not_to be_nil

            end

        end

    end

end
