require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::OpenPositionsController do
    include ControllerSpecHelper
    include StripeHelper
    json_endpoint

    fixtures(:users)

    before(:each) do
        stub_client_params(controller)
    end

    describe "GET index" do
        attr_accessor :user

        describe "authorize_index_with_params" do

            it "should raise unauthorized error if disallowed" do
                expect(controller).to receive(:render_unauthorized_error)
                expect(controller.current_ability).to receive(:cannot?).with(:index_open_positions, controller.params).and_return(true)
                controller.authorize_index_with_params
            end
        end

        describe "with permission" do

            before(:each) do
                # skip auth checks.  authorize_index_with_params is tested on it's own below,
                # and specific rules are tested in ability_spec.rb
                allow(controller).to receive(:authorize_index_with_params)
                @user = users(:hiring_manager_with_team)
                expect(user.open_positions).not_to be_empty
                allow(controller).to receive(:current_user).and_return(user)
            end

            it 'should work as a POST too' do
                post :index, params: {format: :json }
                assert_200
            end

            it "should call add_num_recommended_positions_to_push_messages" do
                expect(controller).to receive(:add_num_recommended_positions_to_push_messages)
                get :index, params: { :format => :json }
            end

            describe "filters" do

                it "should work with id" do
                    open_positions = [
                        OpenPosition.create!(hiring_manager_id: users.first.id, title: "foo"),
                        OpenPosition.create!(hiring_manager_id: users.third.id, title: "foo something"),
                        OpenPosition.create!(hiring_manager_id: users.third.id, title: "something foo"),
                        OpenPosition.create!(hiring_manager_id: users.second.id, title: "something foo something")
                    ]
                    expected_ids = open_positions.map(&:id)
                    process :index, :params => {:format => :json, :filters => {
                        id: expected_ids
                    }.to_json}
                    assert_200
                    expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(expected_ids)
                end

                describe "status" do
                    it "should work when live" do
                        live_positions = OpenPosition.where(featured: true, archived: false).to_a
                        process :index, :params => {:format => :json, :filters => {:status => 'live'}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(live_positions.pluck(:id))
                    end

                    it "should work when drafted" do
                        live_positions = OpenPosition.where(featured: false, archived: false).to_a
                        process :index, :params => {:format => :json, :filters => {:status => 'drafted'}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(live_positions.pluck(:id))
                    end

                    it "should work when archived" do
                        OpenPosition.limit(2).update_all(archived: true) # ensure we have some
                        live_positions = OpenPosition.where(archived: true).to_a
                        process :index, :params => {:format => :json, :filters => {:status => 'archived'}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(live_positions.pluck(:id))
                    end
                end

                describe "hiring_manager_id" do

                    it "should return the open positions for a particular user" do
                        process :index, :params => {:format => :json, :filters => {:hiring_manager_id => user.id}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(user.open_position_ids)
                    end

                    it "should return the open positions for a particular team" do
                        teammate = users(:hiring_manager_teammate)
                        expect(teammate.open_positions).not_to be_empty
                        process :index, :params => {:format => :json, :filters => {:hiring_team_id => user.hiring_team_id}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(user.open_position_ids + teammate.open_position_ids)
                    end
                end

                it "should return open positions for hiring_manager_email" do
                    process :index, :params => {:format => :json, :filters => {:hiring_manager_email => @user.email}.to_json}
                    assert_200
                    expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(@user.open_position_ids)
                end

                it "should return open positions for hiring_manager_company_name" do
                    test_company = ProfessionalOrganizationOption.create!({text: "foo-#{Time.now.to_timestamp}", suggest: true})
                    @user.professional_organization = test_company
                    @user.save!
                    process :index, :params => {:format => :json, :filters => {
                        hiring_manager_company_name: test_company.text
                    }.to_json}
                    assert_200
                    expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(@user.open_positions.pluck(:id))
                end

                it "should return open positions for position title" do
                    users = User.limit(3)
                    OpenPosition.create!(hiring_manager_id: users.first.id, title: "foo")
                    OpenPosition.create!(hiring_manager_id: users.third.id, title: "foo something")
                    OpenPosition.create!(hiring_manager_id: users.third.id, title: "something foo")
                    OpenPosition.create!(hiring_manager_id: users.second.id, title: "something foo something")
                    expected_ids = OpenPosition.where("open_positions.title ilike ?", "%foo%").pluck(:id)
                    process :index, :params => {:format => :json, :filters => {
                        title: 'foo'
                    }.to_json}
                    assert_200
                    expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(expected_ids)
                end

                describe "featured" do

                    it "should return featured open positions when true" do
                        # ensure at least one open position record is featured
                        position = OpenPosition.first
                        position.featured = true
                        position.save!

                        featured_open_position_ids = OpenPosition.where(featured: true).pluck(:id)
                        process :index, :params => {:format => :json, :filters => {:featured => true}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(featured_open_position_ids)
                        expect(featured_open_position_ids.include?(position.id)).to be(true)
                    end

                    it "should return non-featured open positions when false" do
                        # ensure at least one open position record is not featured
                        position = OpenPosition.first
                        position.featured = false
                        position.save!

                        non_featured_open_position_ids = OpenPosition.where(featured: false).pluck(:id)
                        process :index, :params => {:format => :json, :filters => {:featured => false}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(non_featured_open_position_ids)
                        expect(non_featured_open_position_ids.include?(position.id)).to be(true)
                    end
                end

                describe "archived" do

                    it "should return archived open positions when true" do
                        # ensure at least one open position record is archived
                        position = OpenPosition.first
                        position.archived = true
                        position.save!

                        archived_open_position_ids = OpenPosition.where(archived: true).pluck(:id)
                        process :index, :params => {:format => :json, :filters => {:archived => true}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(archived_open_position_ids)
                        expect(archived_open_position_ids.include?(position.id)).to be(true)
                    end

                    it "should return non-archived open positions when false" do
                        # ensure at least one open position record is not archived
                        position = OpenPosition.first
                        position.archived = false
                        position.save!

                        non_archived_open_position_ids = OpenPosition.where(archived: false).pluck(:id)
                        process :index, :params => {:format => :json, :filters => {:archived => false}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(non_archived_open_position_ids)
                        expect(non_archived_open_position_ids.include?(position.id)).to be(true)
                    end
                end

                describe "hiring_manager_might_be_interested_in" do
                    it "should call viewable_when_considering_relationships_and_interests correctly" do
                        candidate = users(:user_with_career_profile)
                        expect_any_instance_of(OpenPosition.const_get(:ActiveRecord_Relation))
                            .to receive(:viewable_when_considering_relationships_and_interests)
                            .with(candidate_or_id: candidate.id, candidate_has_acted_on: nil).and_call_original
                        process :index, :params => {:format => :json, :filters => {:hiring_manager_might_be_interested_in => candidate.id}.to_json}
                        assert_200
                    end

                    it "should call viewable_when_considering_relationships_and_interests correctly when using candidate_has_acted_on" do
                        candidate = users(:user_with_career_profile)
                        expect_any_instance_of(OpenPosition.const_get(:ActiveRecord_Relation))
                            .to receive(:viewable_when_considering_relationships_and_interests)
                            .with(candidate_or_id: candidate.id, candidate_has_acted_on: false).and_call_original
                        process :index, :params => {:format => :json, :filters => {:hiring_manager_might_be_interested_in => candidate.id, :candidate_has_acted_on => false}.to_json}
                        assert_200
                    end
                end

                describe "role" do
                    it "should work" do
                        position = OpenPosition.first
                        position.update_attribute(:role, "foo")
                        process :index, :params => {:format => :json, :filters => {:role => ["foo"]}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array([position.id])
                    end

                    it "should work with multiple values" do
                        positions = OpenPosition.limit(2)
                        positions[0].update_attribute(:role, "foo")
                        positions[1].update_attribute(:role, "bar")
                        process :index, :params => {:format => :json, :filters => {:role => ["foo", "bar"]}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(positions.pluck(:id))
                    end
                end

                describe "industry" do
                    it "should work" do
                        position = OpenPosition.first
                        position.hiring_application.update_attribute(:industry, 'foo')
                        process :index, :params => {:format => :json, :filters => {:industry => ["foo"]}.to_json}
                        assert_200
                        expected_position_ids = position.hiring_manager.open_positions.pluck(:id)
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(expected_position_ids)
                    end

                    it "should work with multiple values" do
                        hiring_manager_with_team = users(:hiring_manager_with_team)
                        hiring_manager_with_team.hiring_application.update_attribute(:industry, 'foo')
                        expect(hiring_manager_with_team.open_positions.size).to be > 0

                        hiring_manager_teammate = users(:hiring_manager_teammate)
                        hiring_manager_teammate.hiring_application.update_attribute(:industry, 'bar')
                        expect(hiring_manager_teammate.open_positions.size).to be > 0

                        process :index, :params => {:format => :json, :filters => {:industry => ["foo", "bar"]}.to_json}
                        assert_200
                        expected_position_ids = hiring_manager_with_team.open_positions.pluck(:id) + hiring_manager_teammate.open_positions.pluck(:id)
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(expected_position_ids)
                    end
                end

                describe "position_descriptors" do
                    it "should work" do
                        position = OpenPosition.first
                        position.update_attribute(:position_descriptors, ["foo"])
                        process :index, :params => {:format => :json, :filters => {:position_descriptors => ["foo"]}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array([position.id])
                    end

                    it "should work with multiple values" do
                        positions = OpenPosition.limit(2)
                        positions[0].update_attribute(:position_descriptors, ["foo"])
                        positions[1].update_attribute(:position_descriptors, ["bar"])
                        process :index, :params => {:format => :json, :filters => {:position_descriptors => ["foo", "bar"]}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(positions.pluck(:id))
                    end

                    it "should work when has one of multiple descriptors" do
                        position = OpenPosition.first
                        position.update_attribute(:position_descriptors, ["foo", "bar"])
                        process :index, :params => {:format => :json, :filters => {:position_descriptors => ["foo"]}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array([position.id])
                    end
                end

                describe "years_experience" do
                    before(:each) do
                        OpenPosition.update_all({
                            desired_years_experience: {"min"=>100, "max"=>101}
                        })
                    end

                    it "should work with a single value" do
                        positions = OpenPosition.limit(5)
                        positions[0].update_column(:desired_years_experience, {"min"=>1, "max"=>nil})
                        positions[1].update_column(:desired_years_experience, {"min"=>nil, "max"=>5})
                        positions[2].update_column(:desired_years_experience, {"min"=>nil, "max"=>nil})
                        positions[3].update_column(:desired_years_experience, {})

                        positions[4].update_column(:desired_years_experience, {"min"=>2, "max"=>nil})

                        process :index, :params => {:format => :json, :filters => {:years_experience => ["0_1_years"]}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(positions[0, 4].pluck(:id))
                    end

                    it "should work with multiple values" do
                        positions = OpenPosition.limit(4)
                        positions[0].update_column(:desired_years_experience, {"min"=>1, "max"=>nil})
                        positions[1].update_column(:desired_years_experience, {"min"=>nil, "max"=>2})
                        positions[2].update_column(:desired_years_experience, {"min"=>5, "max"=>10})

                        positions[3].update_column(:desired_years_experience, {"min"=>7, "max"=>10})

                        process :index, :params => {:format => :json, :filters => {:years_experience => ["0_1_years", "4_6_years"]}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(positions[0, 3].pluck(:id))
                    end

                    it "should work when 6_plus" do
                        positions = OpenPosition.limit(3)
                        positions[0].update_column(:desired_years_experience, {"min"=>nil, "max"=>10})
                        positions[1].update_column(:desired_years_experience, {"min"=>6, "max"=>nil})
                        positions[2].update_column(:desired_years_experience, {"min"=>1, "max"=>5})

                        process :index, :params => {:format => :json, :filters => {:years_experience => ["6_plus"]}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array(positions[0, 2].pluck(:id))
                    end
                end

                describe "places" do
                    before(:each) do
                        OpenPosition.update_all({
                            place_id: nil,
                            place_details: {}
                        })

                        positions = OpenPosition.limit(2)
                        @dc_position = positions.first
                        @dc_position.update!({:place_details => {'lat' => 38.9072, 'lng' => -77.0369}})

                        @boston_position = positions.second
                        @boston_position.update!({:place_details => {'lat' => 42.3601, 'lng' => -71.0589}})

                        # a location close to dc should find
                        # dc positions
                        bethesda = Location.new("38.9847° N, 77.0947° W")
                        @bethesda = {
                            lat: bethesda.lat,
                            lng: bethesda.lng
                        }

                        boston = Location.new("42.3601° N, 71.0589° W")
                        @boston = {
                            lat: boston.lat,
                            lng: boston.lng
                        }

                        memphis = Location.new("35.1495° N, 90.0490° W")
                        @memphis = {
                            lat: memphis.lat,
                            lng: memphis.lng
                        }
                    end

                    it "should work with one value" do
                        process :index, :params => {:format => :json, :filters => {:places => [@bethesda]}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array([@dc_position.id])
                    end

                    it "should work with multiple values" do
                        process :index, :params => {:format => :json, :filters => {:places => [@bethesda, @boston]}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions'].map { |e| e['id']}).to match_array([@dc_position.id, @boston_position.id])
                    end

                    it "should work when no results expected" do
                        process :index, :params => {:format => :json, :filters => {:places => [@memphis]}.to_json}
                        assert_200
                        expect(body_json['contents']['open_positions']).to be_nil
                    end
                end

                describe "recommended_for" do
                    before(:each) do
                        @candidate = users(:user_with_career_profile)
                    end

                    it "should call recommended correctly" do
                        expect_any_instance_of(OpenPosition.const_get(:ActiveRecord_Relation))
                            .to receive(:recommended)
                            .with(candidate_or_id: @candidate)
                            .and_call_original
                        process :index, :params => {:format => :json, :filters => {:recommended_for => @candidate.id}.to_json}
                        assert_200
                    end
                end
            end

            describe "select_recommended" do
                it "should call select_recommended" do
                    # get a user that meets all the requirements in is_recommended_sql
                    @candidate = CareerProfile.joins(:career_profile_search_helper)
                                    .where.not('employment_types_of_interest[1] is null')
                                    .where.not('primary_areas_of_interest[1] is null')
                                    .where.not(place_details: nil)
                                    .where.not("array_length(locations_of_interest, 1) > 2") # ensures real_locations_of_interest is set
                                    .first.user
                    allow(controller).to receive(:current_user).and_return(@candidate)
                    # expect_any_instance_of(OpenPosition.const_get(:ActiveRecord_Relation))
                    #     .to receive(:select_recommended)
                    #     .with(candidate_or_id: @candidate)
                    #     .and_call_original
                    process :index, :params => {
                        :candidate_id => @candidate.id,
                        :format => :json
                    }
                    assert_200
                    expect(body_json['contents']['open_positions'].first['recommended']).not_to be_nil
                end
            end

            it "should return ADMIN_FIELDS" do
                post :index, :params => {
                    :format => :json,
                    :fields => ['ADMIN_FIELDS'],
                    :filters => {
                        :hiring_manager_id => user.id
                    }.to_json
                }
                assert_200
                expect(body_json['contents']['open_positions'].first['created_at']).not_to be_nil # sanity check
                expect(body_json['contents']['open_positions'].first['hiring_manager_email']).not_to be_nil
            end
        end

        describe "preloading" do

            it "should use eager loading" do
                # see should take advantage of active record preloading to optimize data loading in conversations_controller_spec for reference
                open_positions = controller.preload_open_positions_for_index(OpenPosition.all)

                expect(ActiveRecord::Base.connection).not_to receive(:exec_query)

                json = open_positions.as_json({user_id: controller.current_user_id}.merge(controller.api_options))
                expect(open_position = json[0]).not_to be_nil
                expect(open_position['hiring_manager_id']).not_to be_nil
                expect(open_position.include?('hiring_application')).not_to be_nil
            end

            it "should not eager load hiring_application if it is in except" do
                controller.params[:except] = ["hiring_application"]
                open_positions = controller.preload_open_positions_for_index(OpenPosition.all)
                expect(open_positions.first.association(:hiring_manager).loaded?).to be(false)

                expect(ActiveRecord::Base.connection).not_to receive(:exec_query)
                json = open_positions.as_json({user_id: controller.current_user_id}.merge(controller.api_options))
                expect(json[0]).not_to be_nil
            end

            it "should use eager loading when admin" do
                stub_current_user(controller, [:admin])
                open_positions = controller.preload_open_positions_for_index(OpenPosition.all)

                expect(ActiveRecord::Base.connection).not_to receive(:exec_query)
                json = open_positions.as_json({user_id: controller.current_user_id}.merge(controller.api_options))
                expect(open_position = json[0]).not_to be_nil
                expect(open_position.include?('num_interested_candidates')).not_to be_nil
            end
        end

        describe "order" do
            before(:each) do
                allow(controller).to receive(:authorize_index_with_params)
                allow(controller).to receive(:current_user).and_return(users(:admin))
                @default_orders = ["open_positions.updated_at DESC", :title, :id]
            end

            it "should work when just sort is specified" do
                expected_ids = OpenPosition.order(:title).pluck(:id)
                process :index, :params => {
                    :format => :json,
                    :sort => 'title'
                }
                assert_200
                expect(body_json['meta']['total_count']).to eq(OpenPosition.count)
                expect(body_json['contents']['open_positions'].map { |e| e['id']}).to eq(expected_ids)
            end

            it "should work when sort and direction are specified" do
                expected_ids = OpenPosition.order('title DESC').pluck(:id)
                process :index, :params => {
                    :format => :json,
                    :sort => 'title',
                    :direction => 'desc'
                }
                assert_200
                expect(body_json['meta']['total_count']).to eq(OpenPosition.count)
                expect(body_json['contents']['open_positions'].map { |e| e['id']}).to eq(expected_ids)
            end

            it "should work when created_at" do
                positions = OpenPosition.limit(3).to_a

                OpenPosition.where.not(id: positions.pluck(:id)).update_all({
                    created_at: Time.now + 1.year
                })

                positions.first.update_column(:created_at, Time.now - 1.day)
                positions.second.update_column(:created_at, Time.now)
                positions.third.update_column(:created_at, Time.now + 1.day)

                process :index, :params => {
                    :format => :json,
                    :sort => 'created_at'
                }
                assert_200

                ids = body_json['contents']['open_positions'].map { |e| e['id']}
                expect(ids.first(3)).to eq(positions.pluck(:id))
            end

            it "should work when updated_at" do
                positions = OpenPosition.limit(3).to_a

                OpenPosition.where.not(id: positions.pluck(:id)).update_all({
                    updated_at: Time.now + 1.year
                })

                positions.first.update_column(:updated_at, Time.now - 1.day)
                positions.second.update_column(:updated_at, Time.now)
                positions.third.update_column(:updated_at, Time.now + 1.day)

                process :index, :params => {
                    :format => :json,
                    :sort => 'updated_at'
                }
                assert_200

                ids = body_json['contents']['open_positions'].map { |e| e['id']}
                expect(ids.first(3)).to eq(positions.pluck(:id))
            end

            it "should work when hiring_manager_email" do
                expected_ids = OpenPosition.joins(:hiring_manager).order('users.email').order(*@default_orders).pluck(:id)
                process :index, :params => {
                    :format => :json,
                    :sort => 'hiring_manager_email'
                }
                assert_200
                expect(body_json['meta']['total_count']).to eq(OpenPosition.count)
                expect(body_json['contents']['open_positions'].map { |e| e['id']}).to eq(expected_ids)
            end

            it "should work when hiring_manager_company_name" do
                expected_order = OpenPosition.joins(:hiring_manager => [:professional_organization])
                    .order('professional_organization_options.text NULLS LAST').order(*@default_orders)

                process :index, :params => {
                    :format => :json,
                    :sort => 'hiring_manager_company_name'
                }

                expect(body_json['contents']['open_positions'].map { |e| e['id']}).to eq(expected_order.pluck(:id))
            end

            it "should work when status" do
                OpenPosition.limit(2).update_all(archived: true) # ensure some archived
                expected_ids = OpenPosition.where(archived: true).order(:archived).order(*@default_orders).limit(3).pluck(:id)
                process :index, :params => {
                    :format => :json,
                    :sort => 'status',
                    :limit => '2'
                }
                assert_200
                expect(body_json['meta']['total_count']).to eq(OpenPosition.count)
                expect(body_json['contents']['open_positions'].map { |e| e['id']}).to eq(expected_ids)
            end

            describe "num_unreviewed_and_unrejected_interests" do
                open_positions = nil

                before(:each) do
                    CandidatePositionInterest.delete_all
                    open_positions = OpenPosition.limit(4).to_a
                    candidates = User.where.not(id: open_positions.pluck(:hiring_manager_id)).limit(4)

                    2.times do |i|
                        CandidatePositionInterest.create!(
                            candidate_id: candidates[i].id,
                            open_position_id: open_positions.first.id,
                            admin_status:  'unreviewed',
                            candidate_status: 'accepted'
                        )
                    end

                    3.times do |i|
                        CandidatePositionInterest.create!(
                            candidate_id: candidates[i].id,
                            open_position_id: open_positions.second.id,
                            admin_status:  'unreviewed',
                            candidate_status: 'accepted'
                        )
                    end

                    1.times do |i|
                        CandidatePositionInterest.create!(
                            candidate_id: candidates[i].id,
                            open_position_id: open_positions.third.id,
                            admin_status:  'unreviewed',
                            candidate_status: 'accepted'
                        )
                    end

                    4.times do |i|
                        CandidatePositionInterest.create!(
                            candidate_id: candidates[i].id,
                            open_position_id: open_positions.fourth.id,
                            admin_status:  'unreviewed',
                            candidate_status: 'rejected'
                        )
                    end
                end

                it "should work when num_unreviewed_and_unrejected_interests asc" do
                    process :index, :params => {
                        :format => :json,
                        :sort => 'num_unreviewed_and_unrejected_interests',
                        :direction => 'asc'
                    }
                    assert_200
                    expect(body_json['meta']['total_count']).to eq(OpenPosition.count)
                    expect(body_json['contents']['open_positions'].first['id']).to eq(open_positions.third.id)
                    expect(body_json['contents']['open_positions'].second['id']).to eq(open_positions.first.id)
                    expect(body_json['contents']['open_positions'].third['id']).to eq(open_positions.second.id)
                end

                it "should work when num_unreviewed_and_unrejected_interests desc" do
                    process :index, :params => {
                        :format => :json,
                        :sort => 'num_unreviewed_and_unrejected_interests',
                        :direction => 'desc'
                    }
                    assert_200
                    expect(body_json['meta']['total_count']).to eq(OpenPosition.count)
                    expect(body_json['contents']['open_positions'].first['id']).to eq(open_positions.second.id)
                    expect(body_json['contents']['open_positions'].second['id']).to eq(open_positions.first.id)
                    expect(body_json['contents']['open_positions'].third['id']).to eq(open_positions.third.id)
                end
            end

            describe "RECOMMENDED_FOR_CANDIDATE" do
                before(:each) do
                    @candidate = users(:user_with_career_profile)
                end

                it "should call order_by_recommended correctly" do
                    expect_any_instance_of(OpenPosition.const_get(:ActiveRecord_Relation))
                        .to receive(:order_by_recommended)
                        .with(candidate_or_id: @candidate).and_call_original
                    expect_any_instance_of(OpenPosition.const_get(:ActiveRecord_Relation))
                        .to receive(:select_recommended)
                        .with(candidate_or_id: @candidate).and_call_original
                    process :index, :params => {
                        :format => :json,
                        :sort => 'RECOMMENDED_FOR_CANDIDATE',
                        :candidate_id => @candidate.id
                    }
                    assert_200
                end

                it "should call select_recommended when using a limit" do
                    expect_any_instance_of(OpenPosition.const_get(:ActiveRecord_Relation))
                        .to receive(:select_recommended)
                        .with(candidate_or_id: @candidate).and_call_original
                    process :index, :params => {
                        :format => :json,
                        :sort => 'RECOMMENDED_FOR_CANDIDATE',
                        :candidate_id => @candidate.id,
                        :limit => "3", # get params come in as strings
                        :max_total_count => "4"
                    }
                    assert_200
                end

                it "should include other fields in addition to recommended" do
                    expect_any_instance_of(OpenPosition.const_get(:ActiveRecord_Relation))
                        .to receive(:select_recommended)
                        .with(candidate_or_id: @candidate).and_call_original
                    process :index, :params => {
                        :format => :json,
                        :sort => 'RECOMMENDED_FOR_CANDIDATE',
                        :candidate_id => @candidate.id
                    }
                    assert_200

                    # Tests that we did not accidentally only select recommended
                    expect(body_json['contents']['open_positions'].first.keys).to include("title")
                    expect(body_json['contents']['open_positions'].second.keys).to include("title")
                end
            end
        end

        describe "limit" do
            before(:each) do
                allow(controller).to receive(:authorize_index_with_params)
                allow(controller).to receive(:current_user).and_return(users(:admin))
                @position_ids = OpenPosition.limit(5).pluck(:id)
            end

            it "should limit and return the total count if max_total_count and there are fewer results than the max_total_count" do
                process :index, :params => {
                    :format => :json,
                    :filters => {
                        id: @position_ids
                    }.as_json,
                    :limit => "3", # get params come in as strings
                    :max_total_count => "10"
                }
                assert_200
                expect(body_json['meta']['total_count']).to eq(5)
                expect(body_json['contents']['open_positions'].size).to eq(3)

            end

            it "should limit and return the max_total_count if max_total_count and there are more results than the max_total_count" do
                process :index, :params => {
                    :format => :json,
                    :filters => {
                        id: @position_ids
                    }.as_json,
                    :limit => "3", # get params come in as strings
                    :max_total_count => "4"
                }
                assert_200
                expect(body_json['meta']['total_count']).to eq(4)
                expect(body_json['contents']['open_positions'].size).to eq(3)

            end
        end
    end

    describe "POST create" do
        attr_accessor :user

        before(:each) do
            # skip auth checks.  authorize_create_or_update_with_params is tested on it's own below,
            # and specific rules are tested in ability_spec.rb
            allow(controller).to receive(:authorize_create_or_update_with_params)
            @user = users(:hiring_manager_with_team)
            expect(user.open_positions).not_to be_empty
            allow(controller).to receive(:current_user).and_return(user)
        end

        it "should work in normal case" do
            start = Time.now.to_f
            post :create,
                params: {
                    :format => :json,
                    :record => {
                        hiring_manager_id: user.id,
                        title: 'Some job title'
                    }
                }
            assert_200
            expect(body_json['meta']['push_messages']['unloaded_change_detector']['open_position'] > start).to be(true)
        end
    end

    describe "PUT update" do
        attr_accessor :user

        before(:each) do
            # skip auth checks.  authorize_create_or_update_with_params is tested on it's own below,
            # and specific rules are tested in ability_spec.rb
            allow(controller).to receive(:authorize_create_or_update_with_params)
            @user = users(:hiring_manager_with_team)
            expect(user.open_positions).not_to be_empty
            allow(controller).to receive(:current_user).and_return(user)
        end

        it "should add last_updated_at to push messages before open position is updated from hash and then add current last_updated_at to push messages" do
            open_position = user.open_positions.first
            expect(controller).to receive(:add_last_updated_to_push_messages).with(:open_position, 'before').ordered
            expect(OpenPosition).to receive(:update_from_hash!).with(controller.params['record'], { is_admin: false }).ordered.and_return(open_position)
            expect(controller).to receive(:add_last_updated_to_push_messages).with(:open_position, 'current').ordered
            put :update, :params => { :format => :json }
            assert_200
        end

        it "should render 406 if OpenPosition::OldVersion is raised" do
            open_position = user.open_positions.first
            expect(OpenPosition).to receive(:update_from_hash!).and_raise(OpenPosition::OldVersion)
            put :update, :params => {
                :format => :json,
                :record => {
                    id: open_position.id,
                    updated_at: open_position.updated_at.to_timestamp,
                    hiring_manager_id: user.id,
                    title: 'Some other title'
                }
            }
            assert_error_response(406)
            expect(body_json['message']).to eq('Position has been saved more recently. Please reload.')
            expect(body_json['meta']['open_position']['id']).to eq(open_position.id)
            expect(body_json['meta']['push_messages']['unloaded_change_detector']['open_position']).not_to be_nil
        end

        it "should update cancel_at_period_end if necessary" do
            open_position = user.open_positions.first
            user.hiring_team.update(hiring_plan: HiringTeam::HIRING_PLAN_PAY_PER_POST)
            if open_position.subscription.nil?
                create_customer_with_default_source(user.hiring_team)
                user.hiring_team.handle_create_subscription_request({}, {open_position_id: open_position.id}, pay_per_post_plan.id)
            end
            open_position.reload

            expect_any_instance_of(Subscription).to receive(:update_cancel_at_period_end!).with(true).and_call_original

            put :update, :params => {
                :format => :json,
                :record => {
                    id: open_position.id,
                    updated_at: open_position.updated_at.to_timestamp
                },
                :meta => {
                    new_value_for_cancel_at_period_end: true
                }
            }

            subscription = Subscription.find(open_position.subscription.id)
            expect(subscription.cancel_at_period_end).to be(true)
            # seems like stripe ruby mocks do not actually work to update cancel_at_period_end on the stripe sub

        end
    end

    describe "DELETE destroy" do
        it "should work for admin" do
            stub_current_user(controller, [:admin])
            open_position = OpenPosition.create!(hiring_manager_id: User.first.id, title: "foo", available_interview_times: 'times')
            expect {
                delete :destroy, params: {:id => open_position.id, format: :json}
            }.to change(OpenPosition.all, :count).by(-1)
            expect(response.status).to eq(200)
        end

        it "should 401 for normal users" do
            stub_current_user(controller, [:learner])
            expect {
                delete :destroy, params: { format: :json }
            }.to raise_error(ActionController::UrlGenerationError)
        end

        it "should render error message if attached to a hiring relationship" do
            stub_current_user(controller, [:admin])
            hiring_relationship = HiringRelationship.where.not(open_position_id: nil).first
            expect(hiring_relationship).not_to be_nil # sanity check
            delete :destroy, params: {:id => hiring_relationship.open_position.id, format: :json}
            assert_error_response(406, {
                'message' => "Positions linked to hiring relationships cannot be deleted"
            })
        end
    end

    describe "GET send_curation_email" do
        it "should 401 if not admin" do
            stub_current_user(controller, [:learner])
            get :send_curation_email, params: {format: :json, id: OpenPosition.first.id}
            assert_error_response(401)
        end

        it "should log an event to customer.io if admin and no email has ever been triggered" do
            admin_user = users(:admin)
            allow(controller).to receive(:current_user).and_return(admin_user)
            open_position = OpenPosition.first

            expect_any_instance_of(OpenPosition).to receive(:trigger_curation_email).with(open_position.hiring_manager_id).and_return(Event.first)

            get :send_curation_email, params: {format: :json, id: open_position.id, hiring_manager_id: open_position.hiring_manager_id}
            assert_200
        end

        it "should log an event to customer.io if admin and email has been triggered over 24 hours ago" do
            admin_user = users(:admin)
            hiring_manager = users(:hiring_manager_with_team)
            teammate = users(:hiring_manager_teammate)
            allow(controller).to receive(:current_user).and_return(admin_user)
            open_position = hiring_manager.open_positions.first
            Event.delete_all

            event = open_position.trigger_curation_email(teammate.id)
            event.update_column(:created_at, Time.now - 25.hours)

            expect_any_instance_of(OpenPosition).to receive(:trigger_curation_email).and_return(Event.first)

            get :send_curation_email, params: {format: :json, id: open_position.id, hiring_manager_id: teammate.id}
            assert_200
        end

        it "should not log an event to customer.io if admin and email has been within the last 24 hours" do
            admin_user = users(:admin)
            hiring_manager = users(:hiring_manager_with_team)
            allow(controller).to receive(:current_user).and_return(admin_user)
            open_position = hiring_manager.open_positions.first
            Event.delete_all

            event = open_position.trigger_curation_email(hiring_manager.id)
            event.update_column(:created_at, Time.now - 23.hours)

            expect_any_instance_of(OpenPosition).not_to receive(:trigger_curation_email)

            get :send_curation_email, params: {format: :json, id: open_position.id, hiring_manager_id: hiring_manager.id}
            assert_error_response(406, {'message' => "Curation email has been triggered within the last 24 hours"})
        end
    end

    describe "authorize_create_or_update_with_params" do

        it "should raise unauthorized error if disallowed" do
            expect(controller).to receive(:render_unauthorized_error)
            user = users(:hiring_manager_with_team)
            controller.params['record'] = {
                'hiring_manager_id': user.id
            }
            expect(controller.current_ability).to receive(:cannot?).with(:create_or_update_open_positions, controller.params).and_return(true)
            controller.authorize_create_or_update_with_params
        end
    end
end