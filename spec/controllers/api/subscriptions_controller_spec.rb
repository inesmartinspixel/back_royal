require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'
require 'stripe_helper'

describe Api::SubscriptionsController do

    attr_reader :user

    include ControllerSpecHelper
    include StripeHelper
    json_endpoint

    fixtures :cohorts

    before(:each) do
        stub_client_params(controller)
        @user = controller.instance_variable_set(:@current_user, users(:user_with_subscriptions_enabled))
    end



    describe "error handling" do

        before(:each) do
            @err = RuntimeError.new("message")
            @stripe_error = Stripe::StripeError.new("message")
            @basic_card_error = Stripe::CardError.new("message", 500)
        end

        describe "card_data_saved?" do

            it "should be nil if no card data sent up" do
                expect(controller).to receive(:params).at_least(1).times.and_return({})
                expect(controller.card_data_saved?).to be(nil)
            end

            it "should be true if instance var set" do
                expect(controller).to receive(:params).at_least(1).times.and_return({:meta => {:source => 'source'}})
                controller.instance_variable_set(:@card_data_saved, true)
                expect(controller.card_data_saved?).to be(true)
            end

            it "should be false if instance var not set" do
                expect(controller).to receive(:params).at_least(1).times.and_return({:meta => {:source => 'source'}})
                expect(controller.card_data_saved?).to be(false)
            end

        end

        describe "rescue_froms" do

            it "should handle Stripe::CardError with render_card_error" do
                expect(controller).to receive(:render_card_error).with(@basic_card_error)
                expect(controller).to receive(:create).and_raise(@basic_card_error)
                post :create, params: { :format => :json }
            end

            it "should handle Subscription::IncompleteSubscription with render_card_error" do
                err = Subscription::IncompleteSubscription.new
                expect(controller).to receive(:render_card_error).with(err)
                expect(controller).to receive(:create).and_raise(err)
                post :create, params: { :format => :json }
            end

            it "should handle Stripe::StripeError with render_stripe_error" do
                expect(controller).to receive(:render_stripe_error).with(@stripe_error)
                expect(controller).to receive(:create).and_raise(@stripe_error)
                post :create, params: { :format => :json }
            end

            # 404 error handling is tested in the create specs below

            it "should handle Exception with render_error" do
                expect(controller).to receive(:render_error).with(@err)
                expect(controller).to receive(:create).and_raise(@err)
                post :create, params: { :format => :json }
            end

        end

        describe "render_error" do

            it "should include card_data_saved?" do
                expect(controller).to receive(:card_data_saved?).and_return(:value)
                expect(controller).to receive(:render_error_response).with(
                    @err.message,
                    500,
                    {card_data_saved: :value}
                )
                controller.render_error(@err)
            end

            it "should log to raven if specified" do
                expect(Raven).to receive(:capture_exception).exactly(1).times
                expect(controller).to receive(:render_error_response).exactly(1).times
                controller.render_error(@err, 500, true)
            end

            it "should not log to raven if not specified" do
                expect(Raven).not_to receive(:capture_exception)
                expect(controller).to receive(:render_error_response).exactly(1).times
                controller.render_error(@err, 500, false)
            end

        end

        describe "render_card_error" do

            before(:each) do
                @failed_charge_card_error = Stripe::CardError.new("message", 500)
            end

            it "should properly respond with an error" do
                allow(@failed_charge_card_error).to receive(:json_body).and_return({code: 'expired_card', decline_code: 'too old', charge: '12345' })
                Stripe::Charge.create(customer: @user.id, livemode: false, amount: 10000, currency: 'usd')

                expect(controller).to receive(:render_error).with(
                    @failed_charge_card_error, 402, false, {
                        error: @failed_charge_card_error.message
                    }
                )

                controller.render_card_error(@failed_charge_card_error)
            end

            it "should not log unknown non-charged card_errors to slack" do
                expect(controller).to receive(:render_error).with(
                    @basic_card_error, 402, false, {
                        error: @basic_card_error.message
                    }
                )
                expect(LogToSlack).not_to receive(:perform_later)
                controller.render_card_error(@basic_card_error)
            end

            it "should set the message_key for IncompleteSubscription errors when there is a card" do
                err = Subscription::IncompleteSubscription.new("Error message that will be ignored by the client")

                mock_owner = "mock_owner"
                mock_customer = "mock_customer"
                expect(controller).to receive(:owner).and_return(mock_owner)
                expect(mock_owner).to receive(:stripe_customer).and_return(mock_customer)
                expect(mock_customer).to receive(:default_source).and_return(:a_card)

                expect(controller).to receive(:render_error).with(
                    err, 402, false, {
                        error: err.message,
                        message_key: 'there_was_an_issue_charging_to_card',
                        has_default_source: true
                    }
                )

                controller.render_card_error(err)
            end

            it "should set the message_key for IncompleteSubscription errors when there is no card" do
                err = Subscription::IncompleteSubscription.new("Error message that will be ignored by the client")

                mock_owner = "mock_owner"
                mock_customer = "mock_customer"
                expect(controller).to receive(:owner).and_return(mock_owner)
                expect(mock_owner).to receive(:stripe_customer).and_return(mock_customer)
                expect(mock_customer).to receive(:default_source).and_return(nil)

                expect(controller).to receive(:render_error).with(
                    err, 402, false, {
                        error: err.message,
                        message_key: 'there_is_no_payment_source',
                        has_default_source: false
                    }
                )

                controller.render_card_error(err)
            end
        end

        describe "render_stripe_error" do

            it "should add error to payload" do
                expect(controller).to receive(:render_error_response).with(
                    @stripe_error.message,
                    500,
                    {
                        error: @stripe_error.message,
                        card_data_saved: nil
                    }
                )
                controller.render_stripe_error(@stripe_error)
            end

            it "should handle 'no such coupon' error" do
                coupon_error = Stripe::InvalidRequestError.new("No such coupon: coupon_id", :param)
                expect(controller).to receive(:render_error).with(
                    coupon_error, 406, true, {
                        error: coupon_error.message
                    }
                )
                controller.render_stripe_error(coupon_error)
            end

            it "should handle 'Coupon expired' error" do
                coupon_error = Stripe::InvalidRequestError.new("Coupon expired: coupon_id", :param)
                expect(controller).to receive(:render_error).with(
                    coupon_error, 406, true, {
                        error: coupon_error.message
                    }
                )
                controller.render_stripe_error(coupon_error)
            end

        end

    end

    describe "POST create" do

        it "should 401 if disallowed" do
            expect(controller.current_ability).to receive(:can?) do |meth, object|
                if meth == :create_subscription && object[:user_id] = controller.current_user.id
                    false
                else
                    true
                end
            end

            post :create, params: {
                :record => {
                    user_id: controller.current_user.id
                },
                :format => :json
            }
            assert_401
        end

        describe "with access" do

            before(:each) do
                expect(controller.current_ability).to receive(:can?).at_least(1).times.and_return(true)
            end

            it "should work with a user as the owner" do
                expect(controller).to receive(:update_stripe_card_if_specified)
                expect(controller.current_user).to receive(:default_stripe_card).and_return('default_card')
                expect(controller.current_user).to receive(:handle_create_subscription_request).and_return('subscription')

                post :create, params: {
                    :record => {
                        user_id: controller.current_user.id
                    },
                    :format => :json
                }

                assert_200
                controller.current_user.reload
                expect(body_json['contents']['subscriptions'][0]).to eq('subscription')
                expect(body_json['meta']['push_messages']['current_user']['cohort_applications'].size).to eq(user.cohort_applications.size)
                expect(body_json['meta']['default_card']).to eq('default_card')
            end

            it "should work with a hiring_team as the owner" do
                hiring_team, subscription = setup_hiring_team_request
                post :create, params: {
                    :record => {
                        hiring_team_id: hiring_team.id,
                        stripe_plan_id: default_plan.id
                    },
                    :format => :json
                }

                assert_200
                controller.current_user.reload
                expect(body_json['contents']['subscriptions'][0]).to eq(subscription.as_json)
                expect(body_json['meta']['default_card']).to eq('default_card')
                expect(body_json['meta']['push_messages']['current_user']['hiring_team']).not_to be_nil
            end

            # https://trello.com/c/BFBEcWg3
            it "should push down a changed open position" do

                # all this setup is mostly the same as setup_hiring_team_request,
                # but we need to mock out handle_create_subscription_request so it updates
                # the subscription
                subscription = OpenStruct.new(id: SecureRandom.uuid)

                hiring_team = HiringTeam.left_outer_joins(:subscriptions).joins(:owner).where("subscriptions.id is null").first
                expect(HiringTeam).to receive(:find_by_id).with(hiring_team.id).and_return(hiring_team)

                expect(controller).to receive(:update_stripe_card_if_specified)
                expect(hiring_team).to receive(:default_stripe_card).and_return('default_card')

                open_position = OpenPosition.where.not(hiring_manager_id: hiring_team.owner_id).first
                open_position.update!(hiring_manager_id: hiring_team.owner.id)

                # Simulate the way an existing position can be updated when switching to
                # an unlimited subscription.  There is also another way for a position to
                # be changed here (see comment in code), but this test should cover any such case
                expect(hiring_team).to receive(:handle_create_subscription_request) do
                    open_position.touch
                    subscription
                end
                allow(controller).to receive(:current_user).and_return(hiring_team.owner)

                # the expect.to_change is a sanity check to make sure that our mocking worked
                expect {
                    post :create, params: {
                        :record => {
                            hiring_team_id: hiring_team.id,
                            stripe_plan_id: default_plan.id
                        },
                        :format => :json
                    }
                }.to change { open_position.reload.updated_at }

                assert_200
                expect(body_json['meta']['open_positions'][0]).to eq(open_position.as_json)
            end

            def setup_hiring_team_request
                subscription = OpenStruct.new(id: SecureRandom.uuid)

                hiring_team = HiringTeam.left_outer_joins(:subscriptions).joins(:owner).where("subscriptions.id is null").first
                expect(HiringTeam).to receive(:find_by_id).with(hiring_team.id).and_return(hiring_team)

                expect(controller).to receive(:update_stripe_card_if_specified)
                expect(hiring_team).to receive(:default_stripe_card).and_return('default_card')
                expect(hiring_team).to receive(:handle_create_subscription_request).and_return(subscription)
                allow(controller).to receive(:current_user).and_return(hiring_team.owner)

                [hiring_team, subscription]
            end

            it "should render error response on InvalidCoupon error" do
                expect(controller.current_user).to receive(:handle_create_subscription_request).and_raise(User::SubscriptionConcern::InvalidCoupon.new)
                post :create, params: {
                    :record => {
                        user_id: controller.current_user.id
                    },
                    :format => :json
                }
                assert_error_response(406)
                expect(body_json['message']).to eq("You must supply a valid coupon")
            end
        end

    end

    describe "update_stripe_card_if_specified_and_pay_unpaid_invoices" do

        describe "modifying payment details" do
            attr_reader :application

            it "should update stripe card" do
                # setup a user with a subscription
                create_customer_with_default_source(user)
                subscription = create_subscription(user)

                orig_card = controller.current_user.default_stripe_card

                controller.params = {
                    :record => {
                        id: subscription.id,
                        user_id: controller.current_user.id
                    },
                    :meta => {
                        :source => stripe_helper.generate_card_token
                    },
                    :format => :json
                }
                expect(controller).to receive(:set_push_messages_after_subscription_update)
                controller.send(:update_stripe_card_if_specified_and_pay_unpaid_invoices)

                controller.current_user.reload
                expect(controller.current_user.default_stripe_card).not_to eq(orig_card)
            end

        end

        describe "unpaid invoices" do
            attr_reader :subscriptions

            before(:each) do
                @user = User.left_outer_joins(:subscriptions).where("subscriptions.id is null").first
                @user.create_stripe_customer

                @subscriptions = [
                    Subscription.new(subscription_attrs),
                    Subscription.new(subscription_attrs),
                    Subscription.new(subscription_attrs)
                ]
                allow(controller).to receive(:current_user).and_return(@user)
                allow(controller).to receive(:owner).and_return(@user)
                allow(@user).to receive(:subscriptions).and_return(subscriptions)
            end

            it "should attempt to charge any unpaid invoices" do

                subscriptions.each do |subscription|
                    expect(subscription).to receive(:charge_unpaid_invoices)
                end

                controller.params = {
                    :meta => {
                        :source => stripe_helper.generate_card_token
                    }
                }
                expect(controller.send(:update_stripe_card_if_specified_and_pay_unpaid_invoices)).to eq(true)
            end

            it "should error with invoice details if failing to fully re-charge any unpaid invoices" do

                failed_charge_card_error_1 = Stripe::CardError.new("we will not see this message", 500)
                failed_charge_card_error_2 = Stripe::CardError.new("insufficient_funds", 500)

                expect(subscriptions[0]).to receive(:num_unpaid_invoices).exactly(2).times.and_return(3, 1) # two were paid, one failed
                expect(subscriptions[0]).to receive(:charge_unpaid_invoices).and_raise(failed_charge_card_error_1)
                expect(subscriptions[1]).to receive(:num_unpaid_invoices).exactly(2).times.and_return(1, 1) # one failed
                expect(subscriptions[1]).to receive(:charge_unpaid_invoices).and_raise(failed_charge_card_error_2)
                expect(subscriptions[2]).to receive(:num_unpaid_invoices).and_return(0)
                expect(subscriptions[2]).to receive(:charge_unpaid_invoices)

                controller.params = {
                    :meta => {
                        :source => stripe_helper.generate_card_token
                    }
                }

                expect(controller).to receive(:render_error_response).with("2 of 4 overdue invoices were paid. Last payment failed with error: insufficient_funds", 402)
                expect(controller.send(:update_stripe_card_if_specified_and_pay_unpaid_invoices)).to eq(false)

            end

            def subscription_attrs
                {
                    stripe_subscription_id: SecureRandom.uuid,
                    stripe_product_id: default_plan.product.id,
                    stripe_plan_id: default_plan.id
                }
            end
        end
    end

    describe "DELETE destroy" do

        attr_accessor :subscription

        before(:each) do
            self.subscription = Subscription.first
        end

        it "should 401 if disallowed" do
            expect(controller.current_ability).to receive(:can?).with(:destroy, subscription).and_return(false)
            delete :destroy, params: { id: subscription.id, format: :json }
            assert_error_response(401)
        end

        it "should work" do
            expect(controller.current_ability).to receive(:can?).with(:destroy, subscription).and_return(true)
            expect(controller).to receive(:set_primary_subscription_push_messages)
            expect(controller).to receive(:set_careers_push_messages)
            expect_any_instance_of(Subscription).to receive(:cancel_and_destroy) do |instance|
                expect(instance).to eq(subscription)
            end
            delete :destroy, params: { id: subscription.id, format: :json }
            assert_200
        end

        # This is not the sort of thing we would normally test in a controller spec.  Seems like
        # it should be handled farther down in unit specs.  But there was some craziness related
        # to the caching of activerecord associations that was causing this to not work as expected
        # when everything fit together, so I added a spec to make sure it works.  See the comment in
        # OwnsPayments#setup_joins_on_payment_related_thing to learn more
        it "should work to destroy an unlimited subscription" do
            allow(controller.current_ability).to receive(:can?).and_return(true)
            hiring_team = HiringTeam.joins(:owner).first
            hiring_team.update!(hiring_plan: HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING)
            create_customer_with_default_source(hiring_team)
            subscription = create_subscription(hiring_team, unlimited_with_sourcing_plan.id)

            delete :destroy, params: { id: subscription.id, format: :json }
            assert_200
            expect(Subscription.find_by_id(subscription.id)).to be_nil
            hiring_team.reload

            # When an unlimited_with_sourcing_plan is canceled, the hiring_plan should
            # be set back to nil.  See HiringTeam::SubscriptionConcern#before_subscription_destroy
            expect(hiring_team.hiring_plan).to be_nil
            expect(hiring_team.subscriptions).to be_empty
        end

    end

    describe "PUT modify_payment_details" do
        it "should 406 if no source provided" do
            user = User.first
            allow(controller).to receive(:current_user).and_return(user)
            put :modify_payment_details, params: {
                format: :json,
                owner_id: user.id,
                source: nil
            }
            assert_error_response(406, {message: 'No source token'})
        end

        it "should 401 if disallowed" do
            expect(controller.current_ability).to receive(:can?).with(:modify_payment_details_with_params, anything).and_return(false)
            put :modify_payment_details, params: {
                format: :json,
                owner_id: SecureRandom.uuid,
                source: SecureRandom.uuid
            }
            assert_error_response(401)
        end

        it "should call update_stripe_card_if_specified_and_pay_unpaid_invoices" do
            expect(controller.current_ability).to receive(:can?).with(:modify_payment_details_with_params, anything).and_return(true)
            expect(controller).to receive(:update_stripe_card_if_specified_and_pay_unpaid_invoices).and_return(true)
            put :modify_payment_details, params: {
                format: :json,
                owner_id: SecureRandom.uuid,
                source: SecureRandom.uuid
            }
            assert_200
        end

        it "should handle error from update_stripe_card_if_specified_and_pay_unpaid_invoices" do
            expect(controller.current_ability).to receive(:can?).with(:modify_payment_details_with_params, anything).and_return(true)
            expect(controller).to receive(:update_stripe_card_if_specified_and_pay_unpaid_invoices).and_return(false)
            expect(controller).not_to receive(:render_successful_response)
            put :modify_payment_details, params: {
                format: :json,
                owner_id: SecureRandom.uuid,
                source: SecureRandom.uuid
            }
        end
    end
end
