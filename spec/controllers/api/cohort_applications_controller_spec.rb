require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::CohortApplicationsController do
    include ControllerSpecHelper
    json_endpoint

    before(:each) do
        stub_client_params(controller)
    end

    describe "GET index" do

        it "should 401 for non-admin" do
            stub_current_user(controller, [:learner])
            get :index, params: { format: :json }
            assert_error_response(401)
        end

        it "should return cohort applications" do
            stub_current_user(controller, [:admin])
            get :index, params: { format: :json }
            assert_200
            expect(body_json['contents']['cohort_applications'].map { |e| e['id']}).to match_array(CohortApplication.pluck('id'))
        end

        it "should use eager loading" do
            # Mock this out, since in prod it will almost always be cached
            allow(Cohort).to receive(:cached_published).and_return(
                OpenStruct.new(
                    name: 'MBA1',
                    id: '1234',

                    # make sure has_paid_in_full? has to load billing_transactions
                    "supports_payments?": true
                )
            )

            # see should take advantage of active record preloading to optimize data loading in conversations_controller_spec for reference
            cohort_applications = controller.preload_cohort_applications_for_index


            # sanity check. json generation will call payment_past_due?  We want to make sure that
            # we have at least one user for whom payment_past_due? will call billing_transactions.  That
            # way we can be sure they are eager loaded
            app_that_needs_billing_transactions = cohort_applications.detect(&:required_to_make_payments?)
            expect(app_that_needs_billing_transactions.user).to receive(:billing_transactions).at_least(1).times.and_call_original

            # Under the hood, the quantic institution is referenced when the `as_json` method is called
            # on the cohort applications. Normally this is cached by the server, so we simulate this by
            # preparing the cache before making our assertions on no additional queries being made.
            Institution.quantic

            # once the records are preloaded, there should be no more
            # queries required to convert them to json
            expect(ActiveRecord::Base.connection).not_to receive(:exec_query).and_call_original
            json = cohort_applications.as_json

            # sanity checks. make sure that we actually generated a full json tree
            expect(cohort_application = json[0]).not_to be_nil
            expect(cohort_application['cohort_id']).not_to be_nil
        end

        it "should return 1 day of information when zapier is true" do
            stub_current_user(controller, [:admin])
            # delete all applications that have no associated career profile, to simplify
            CohortApplication.where.not(user_id: CareerProfile.pluck('user_id')).delete_all
            CohortApplication.update_all(status: 'pending')
            cohort_applications = CohortApplication.includes(*CohortApplication.includes_needed_for_json_eager_loading).where(status: 'pending').limit(2)

            # before we mock it out, the threshold should be 1 day ago
            expect(controller.zapier_updated_at_threshold).to be_within(1.second).of(1.day.ago)

            # mock out the threshold to now, and we should only expect applications that we update
            # after this (except for the one guaranteed one)
            min_updated_at = Time.now
            expect(controller).to receive(:zapier_updated_at_threshold).at_least(1).times.and_return(Time.at(min_updated_at))

            # update one application and one career profile.  These should
            # be thw only two that get returned
            cohort_applications[0].update(updated_at: Time.now)
            cohort_applications[1].career_profile.update(updated_at: Time.now)

            get :index, params: { zapier: true, include_updates: true, format: :json }
            assert_200

            # all the expected applications should be there
            expect(cohort_applications.map(&:id) - body_json['contents']['cohort_applications'].map { |e| e['id']}).to be_empty
        end

        it "should always return at least one profile with zapier is true" do
             stub_current_user(controller, [:admin])
            # delete all applications that have no associated career profile, to simplify
            CohortApplication.where.not(user_id: CareerProfile.pluck('user_id')).delete_all
            cohort_applications = CohortApplication.includes(*CohortApplication.includes_needed_for_json_eager_loading).limit(2)

            # before we mock it out, the threshold should be 1 day ago
            expect(controller.zapier_updated_at_threshold).to be_within(1.second).of(1.day.ago)

            # mock out the threshold to now, and we should only expect applications that we update
            # after this (except for the one guaranteed one)
            min_updated_at = Time.now
            expect(controller).to receive(:zapier_updated_at_threshold).at_least(1).times.and_return(Time.at(min_updated_at))

            get :index, params: { zapier: true, include_updates: true, format: :json }
            assert_200

            # all the expected applications should be there
            expect(body_json['contents']['cohort_applications'].size).to eq(1)
        end

        it "should only return pending applications when zapier is true" do
             stub_current_user(controller, [:admin])
            # delete all applications that have no associated career profile, to simplify
            CohortApplication.where.not(user_id: CareerProfile.pluck('user_id')).delete_all
            cohort_applications = CohortApplication.includes(*CohortApplication.includes_needed_for_json_eager_loading).limit(2)

            # before we mock it out, the threshold should be 1 day ago
            expect(controller.zapier_updated_at_threshold).to be_within(1.second).of(1.day.ago)

            # mock out the threshold to the beginnig of time, so we should get all pending ones
            min_updated_at = Time.at(0)
            expect(controller).to receive(:zapier_updated_at_threshold).at_least(1).times.and_return(Time.at(min_updated_at))

            get :index, params: { zapier: true, include_updates: true, format: :json }
            assert_200

            # all the expected applications should be there
            expected_application_ids = CohortApplication.where(status: 'pending').pluck('id')
            expected_application_ids.each do |id|
                expect(body_json['contents']['cohort_applications'].map { |e| e['id']}).to include(id)
            end
        end
    end


    describe "POST create" do

        before(:each) do
            @cohort_params = {
                cohort_id: Cohort.ids.first,
                status: 'pending'
            }
        end

        it "should set appropriate push messages" do
            stub_current_user(controller, [:learner])
            expect(controller).to receive(:set_relevant_cohort_push_messages).and_call_original
            allow(controller).to receive(:update_last_seen_at)
            expect(controller).to receive(:set_user_push_messages).with(reload: true).and_call_original
            expect(controller).to receive(:set_institution_push_messages).and_call_original
            post :create, params: { :format => :json, :record => @cohort_params }
            assert_200
        end

        describe "as non-admin" do
            it "should 401 if not user_can_create_application?" do
                stub_current_user(controller, [:learner])
                user = controller.current_user
                expect_any_instance_of(Cohort).to receive(:user_can_create_application?).with(user).and_return(false)
                expect(CohortApplication).not_to receive(:create_from_hash!)
                post :create,
                    params: {
                        :format => :json,
                        :record => @cohort_params.merge({
                            status: 'accepted'
                        })
                    }
                assert_error_response(401)
            end

            it "should not 401 if user_can_create_application?" do
                stub_current_user(controller, [:learner])
                user = controller.current_user
                expect_any_instance_of(Cohort).to receive(:user_can_create_application?).with(user).and_return(true)
                expect(CohortApplication).to receive(:create_from_hash!).and_return(CohortApplication.first)
                post :create,
                    params: {
                        :format => :json,
                        :record => @cohort_params.merge({
                            status: 'pre_accepted'
                        })
                    }
                assert_200
            end

            it "should 401 if trying to create application for another user" do
                stub_current_user(controller, [:learner])
                expect(CohortApplication).not_to receive(:create_from_hash!)
                post :create,
                    params: {
                        :format => :json,
                        :record => @cohort_params.merge({
                            user_id: 'That ain\t you'
                        })
                    }
                assert_error_response(401)
            end

            it "should work in normal case" do
                stub_current_user(controller, [:learner])
                post :create, params: { :format => :json, :record => @cohort_params }
                assert_200
                expect(CohortApplication.last.user_id).to eq(controller.current_user.id)
            end
        end

        describe "as admin" do
            before(:each) do
                stub_current_user(controller, [:admin])
            end

            it "should allow setting non-pending status" do
                post :create,
                    params: {
                        :format => :json,
                        :record => @cohort_params.merge({
                            status: 'pre_accepted'
                        })
                    }
                assert_200
                expect(CohortApplication.last.status).to eq('pre_accepted')
            end

            it "should allow creating application for another user" do
                another_user_id = User.left_outer_joins(:cohort_applications).where("cohort_applications.id" =>  nil).where('users.id != ?', controller.current_user.id).ids.first
                post :create,
                    params: {
                        :format => :json,
                        :record => @cohort_params.merge({
                            user_id: another_user_id
                        })
                    }
                assert_200
                expect(CohortApplication.last.user_id).to eq(another_user_id)
            end
        end

    end

    describe "PUT update" do

        describe "as non-admin" do

            it "should 401 if trying to set non-pending status" do
                user = users(:pending_career_network_only_cohort_user)
                stub_current_user(controller, user)
                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: user.cohort_applications.last.id,
                            status: 'accepted'
                        }
                    }
                assert_error_response(401)
            end

            it "should 401 if trying to set pending status on non-rejected" do
                user = users(:accepted_career_network_only_cohort_user)
                stub_current_user(controller, user)
                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: user.cohort_applications.last.id,
                            status: 'pending'
                        }
                    }
                assert_error_response(401)
            end

            it "should 401 if trying to update rejected to pending when cohort does not allow re-applying" do
                user = users(:rejected_mba_cohort_user)
                stub_current_user(controller, user)
                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: user.cohort_applications.last.id,
                            status: 'pending'
                        }
                    }
                assert_error_response(401)
            end

            it "should 401 if trying to update another user's profile" do
                user = users(:pending_career_network_only_cohort_user)
                stub_current_user(controller, user)
                other_application = CohortApplication.where(status: 'rejected').first.id
                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: other_application,
                            status: 'pending'
                        }
                    }
                assert_error_response(401)
            end

            it "should allow updating rejected to pending for career_network_only cohort that allows re-applying" do
                user = users(:rejected_career_network_only_cohort_user)
                stub_current_user(controller, user)
                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: user.cohort_applications.last.id,
                            status: 'pending'
                        }
                    }
                assert_200
            end

            it "should allow updating rejected to pending for the_business_certificate cohort that allows re-applying" do
                user = users(:rejected_the_business_certificate_cohort_user)
                stub_current_user(controller, user)
                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: user.cohort_applications.last.id,
                            status: 'pending'
                        }
                    }
                assert_200
            end

            it "should allow update to status if it doesn't actually change it" do
                user = users(:rejected_mba_cohort_user)
                stub_current_user(controller, user)
                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: user.cohort_applications.last.id,
                            status: 'rejected'
                        }
                    }
                assert_200
            end

            it "should set appropriate push messages" do
                user = users(:rejected_mba_cohort_user)
                stub_current_user(controller, user)
                expect(controller).to receive(:set_institution_push_messages).and_call_original
                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: user.cohort_applications.last.id,
                            status: 'rejected'
                        }
                    }
                assert_200
            end
        end
    end

    describe "PUT batch_update" do

        it "should raise if cannot manage" do
            allow(controller).to receive(:current_user).and_return(User.first)
            expect(controller.current_ability).to receive(:can?).at_least(1).times do |meth, obj|
                if meth == :manage && obj == CohortApplication
                    false
                else
                    true
                end
            end

            put :batch_update,
                params: {
                    :format => :json,
                    :records => []
                }
            assert_401
        end

        it "should work" do
            allow(controller).to receive(:current_user).and_return(User.first)
            allow(controller.current_ability).to receive(:can?).and_return(true)
            cohort = Cohort.joins(:cohort_applications, :slack_rooms).first
            cohort_applications = CohortApplication
                                    .where(cohort_id: cohort.id, cohort_slack_room_id: nil)
                                    .limit(2)

            put :batch_update,
                    params: {
                        :format => :json,
                        :records => [
                            {
                                id: cohort_applications[0].id,
                                cohort_slack_room_id: cohort.slack_rooms[0].id
                            },
                            {
                                id: cohort_applications[1].id,
                                cohort_slack_room_id: cohort.slack_rooms[1].id
                            }
                        ]
                    }
            assert_200

            expect(cohort_applications[0].reload.cohort_slack_room_id).to eq(cohort.slack_rooms[0].id)
            expect(cohort_applications[1].reload.cohort_slack_room_id).to eq(cohort.slack_rooms[1].id)
        end
    end

    describe "DELETE destroy" do
        it "should send back relevant_cohort in push messages" do
            application = CohortApplication.first
            allow_any_instance_of(Cohort).to receive(:user_can_destroy_application?).and_return(true)
            stub_current_user(controller, [:learner])
            expect(controller).to receive(:set_relevant_cohort_push_messages).and_call_original
            post :destroy, params: { :format => :json, :id => application.id }
            assert_200
        end
    end

    describe "GET refund_details" do

        it "should 401 for non-admin" do
            stub_current_user(controller, [:learner])
            get :refund_details,
                params: {
                    :format => :json
                }
            assert_401
        end

        it "should work" do
            stub_current_user(controller, [:learner])
            allow(controller.current_ability).to receive(:can?).and_return(true)
            application = CohortApplication.first
            user = application.user
            expect_any_instance_of(CohortApplication).to receive(:refund_details) do |_application|
                expect(_application).to eq(user.last_application)
                'refund_details'
            end

            get :refund_details,
                params: {
                    :format => :json,
                    :filters => {user_id: user.id}
                }
            assert_200
            expect(body_json['contents']['refund_details']).to eq('refund_details')
        end
    end

    describe "GET sign" do

        attr_accessor :cohort_application, :user

        before(:each) do
            @user = users(:accepted_mba_cohort_user)
            stub_current_user(controller, user)
            @cohort_application = user.last_application
        end

        it "should 404 if application does not have a signable document" do
            cohort_application.enrollment_agreement&.destroy!
            get :sign, params: {id: cohort_application.id}
            assert_error_response(404)
        end

        it "should 404 if application does not require an agreement, without doing unnecessary work" do
            allow_any_instance_of(CohortApplication).to receive(:should_have_enrollment_agreement?) { false }
            expect_any_instance_of(CohortApplication).not_to receive(:ensure_enrollment_agreement_if_necessary)
            get :sign, params: {id: cohort_application.id}
            assert_error_response(404)
        end

        it "should redirect to the signing link if it exists" do
            mock_signing_link = 'https://some-signing-link'
            allow_any_instance_of(CohortApplication).to receive(:should_have_enrollment_agreement?) { true }
            allow_any_instance_of(CohortApplication).to receive(:need_to_generate_new_enrollment_agreement?) { false }
            allow_any_instance_of(CohortApplication).to receive(:enrollment_agreement) {
                SignableDocument.new(sign_now_signing_link: mock_signing_link)
            }
            get :sign, params: {id: cohort_application.id}
            assert_redirected_to(mock_signing_link)
        end

    end

end