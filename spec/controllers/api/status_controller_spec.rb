require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::StatusController do

    include ControllerSpecHelper
    include StripeHelper
    json_endpoint

    describe "set_push_messages" do

        before(:each) do
            SafeCache.delete("content_views_refresh_updated_at")
            @default_push_messages = {
                'event_logger' => {
                    'disable_auto_events_for' => AppConfig.disable_auto_events_for
                }
            }
        end

        it "should set event_logger stuff" do
            content_views_refresh_updated_at = ContentViewsRefresh.first.updated_at
            stub_no_current_user(controller)
            @controller.set_push_messages
            expect(@controller.meta).to eq({'push_messages' => @default_push_messages.merge({'content_views_refresh_updated_at' => content_views_refresh_updated_at.to_timestamp }) })
        end

        it "should call appropriate methods to set push message data" do
            expect(@controller).to receive(:set_user_push_messages)
            expect(@controller).to receive(:set_institution_push_messages)
            expect(@controller).to receive(:set_relevant_cohort_push_messages)
            expect(@controller).to receive(:set_primary_subscription_push_messages)
            expect(@controller).to receive(:set_config_push_messages)
            expect(@controller).to receive(:set_careers_push_messages)
            expect(@controller).to receive(:set_content_refresh_push_messages)
            expect(@controller).to receive(:add_cohort_applications_to_push_messages)
            expect(@controller).to receive(:set_id_verification_push_messages)
            expect(@controller).to receive(:add_num_recommended_positions_to_push_messages)
            @controller.set_push_messages
        end

        describe "content_views_refresh_updated_at" do
            it "should set updated_at" do
                content_views_refresh_updated_at = ContentViewsRefresh.first.updated_at
                @controller.set_push_messages
                expect(@controller.meta["push_messages"]["content_views_refresh_updated_at"]).to eq(content_views_refresh_updated_at.to_timestamp)
            end

            it "should be piggybacked on if the user has an accepted application so that the student dashboard cache will be invalidated and updated cohort applications sent back" do
                user = User.joins(:cohort_applications).where("cohort_applications.status = 'accepted' and cohort_applications.graduation_status='pending'").first
                cohort_application = user.accepted_application
                allow(@controller).to receive(:current_user).and_return(user)

                cohort_application.status = 'pending'
                cohort_application.save!
                user.reload
                @controller.set_push_messages
                expect(@controller.meta["push_messages"]["content_views_refresh_updated_at"]).to eq(cohort_application.updated_at.to_timestamp)

                user.cohort_applications.delete_all
                user.reload
                @controller.set_push_messages
                expect(@controller.meta["push_messages"]["content_views_refresh_updated_at"]).to eq(ContentViewsRefresh.first.updated_at.to_timestamp)
            end
        end

        describe "current_user" do

            it "should set stuff for current user" do
                user = users(:user_with_subscriptions_enabled)
                expect(user).to receive(:can_edit_career_profile).and_return('can_edit_career_profile').at_least(1).times
                expect(user).to receive(:can_purchase_paid_certs).and_return('can_purchase_paid_certs').at_least(1).times

                last_application = CohortApplication.first
                create_subscription(user)
                expect(last_application).to receive(:curriculum_status).and_return('foo')
                expect(user).to receive(:last_application).at_least(1).and_return(last_application)

                allow(controller).to receive(:current_user).and_return(user)
                controller.set_push_messages
                value = controller.meta["push_messages"]["current_user"]
                expect(value['can_edit_career_profile']).to eq('can_edit_career_profile')
                expect(value['active_playlist_locale_pack_id']).to eq(user.active_playlist_locale_pack_id)
                expect(value['can_purchase_paid_certs']).to eq('can_purchase_paid_certs')
                expect(value['curriculum_status']).to eq('foo')
                expect(value['user_id_verifications']).to eq(user.user_id_verifications_for_json.as_json)
                expect(value['idology_verifications']).to eq(user.idology_verifications_for_json.as_json)
            end
        end

        describe "hiring_application" do
            it "should add status" do
                hiring_manager = users(:hiring_manager)
                allow(controller).to receive(:current_user).and_return(hiring_manager)

                hiring_manager.hiring_application.status = "updated"
                hiring_manager.hiring_application.save!

                @controller.set_push_messages
                expect(@controller.meta["push_messages"]["hiring_application"]["status"]).to eq("updated")
            end
        end
    end

    describe "GET simple" do

        it "should set push_messages" do

            expect(@controller).to receive(:set_push_messages)
            expected_calls = Set.new([
                [:hiring_relationship, 'current'],
                [:open_position, 'current'],
                [:candidate_position_interest, 'current']
            ])
            expect(@controller).to receive(:add_last_updated_to_push_messages).exactly(3).times do |key, before_or_current|
                expected_calls.delete([key, before_or_current])
            end
            expect(@controller).to receive(:set_progress_max_updated_at)
            get :simple, params: {format: :json}
            expect(response.status).to eq(200) # ok
            expect(expected_calls).to be_empty
        end

    end

end
