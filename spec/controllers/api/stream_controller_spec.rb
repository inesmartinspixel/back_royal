require 'controllers/api/helpers/api_content_controller_spec_helper'

describe Api::StreamController do
    attr_reader :user

    include ApiContentControllerSpecHelper
    include StripeHelper

    fixtures(:users)

    add_destroy_specs
    add_images_specs

    add_create_specs({
        :valid => {:title=>"My Stream 8", :description=>"An awesome stream", :author => {}, :chapters => nil},
        :invalid => {:description=>"An awesome stream"}
    })

    add_update_specs({
        :valid => Proc.new { |instance_json|
            instance_json['title'] = 'Some other title'
            instance_json
        },
        :invalid => Proc.new { |instance_json|
            instance_json['title'] = nil
            instance_json
        }
    })

    describe "index" do

        it "should return instances" do
            expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                expect(to_json_instance.filters[:user_can_see]).to be(true)
                expect(to_json_instance.params[:id]).to be(nil)
                expect(to_json_instance.params[:user_id]).to eq(controller.current_user_id)
                expect(to_json_instance.params[:user]).to eq(controller.current_user)
                "[]"
            end
            get :index, params: { :format => :json }
            assert_successful_response({
                "lesson_streams" => []
            })
        end

        it "should raise unauthorized if requesting unpublished content without permission" do
            stub_current_user(controller, [:learner])
            get :index, params: { :filters => {:published => false}, :format => :json }
            assert_error_response(401, {'message' => "You are not authorized to do that."})
        end

        it "should allow permitted user to see unpublished content" do
            expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                expect(to_json_instance.filters[:published]).to be(false)
                "[]"
            end
            stub_current_user(controller, [:admin])
            get :index, params: { :filters => {:published => false}, :format => :json }
            assert_successful_response({
                "lesson_streams" => []
            })
        end

        it "should allow permitted user to pass false for user_can_see" do
            stub_current_user(controller, [:admin])
            expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                expect(to_json_instance.filters[:user_can_see]).to eq(false)
                "[]"
            end
            get :index, params: { :filters => {:user_can_see => false}, :format => :json }
            assert_200
        end

        it "should error if not logged in and no appropriate view_as group param" do
            # Note: We allow anonymous users to pass cohort:PROMOTED_DEGREE to view_as.
            # See ability.rb#add_abilities_for_user
            stub_no_current_user(controller)
            get :index, params: { :format => :json }
            assert_error_response(401, {'message' => "You are not authorized to do that."})
        end

        describe "with view_as param" do

            it "should allow non-admins to view_as SMARTER" do
                stub_current_user(controller, [:learner])

                expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                    expect(to_json_instance.filters['view_as']).to eq('cohort:PROMOTED_DEGREE')
                    expect(to_json_instance.filters['published']).to eq(true)
                    "[]"
                end
                get :index, params: { :filters => {:view_as => 'cohort:PROMOTED_DEGREE'}, :format => :json }
                assert_successful_response({
                    "lesson_streams" => []
                })
            end

            it "should not allow non-admins to view_as" do
                stub_current_user(controller, [:learner])
                expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).not_to receive(:json)
                get :index, params: { :filters => {:view_as => 'group:SOME_GROUP'}.to_json, :format => :json }
                assert_error_response(401)
            end

            it "should allow admin to view any group" do
                stub_current_user(controller, [:admin])

                expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                    expect(to_json_instance.filters['view_as']).to eq('group:RESTRICTED')
                    expect(to_json_instance.filters['published']).to eq(true)
                    "[]"
                end
                get :index, params: { :filters => {:view_as => 'group:RESTRICTED'}.to_json, :format => :json }
                assert_successful_response({
                    "lesson_streams" => []
                })
            end

            it "should allow anonymous user to view_as SMARTER" do
                stub_no_current_user
                expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                    expect(to_json_instance.filters['view_as']).to eq('cohort:PROMOTED_DEGREE')
                    expect(to_json_instance.filters['in_users_locale_or_en']).to be_nil
                    expect(to_json_instance.filters['user_can_see']).to be_nil
                    expect(to_json_instance.filters['published']).to eq(true)
                    "[]"
                end
                get :index, params: { :filters => {:view_as => 'cohort:PROMOTED_DEGREE', :in_users_locale_or_en => nil, :user_can_see => nil }.to_json, :format => :json }
                assert_200
            end
        end

        describe "with user param" do

            it "should allow super_editors to filter by any user" do
                stub_current_user(controller, [:super_editor])
                user = User.first
                expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                    expect(to_json_instance.user).to eq(user)
                "[]"
                end
                get :index, params: { :user_id => user.id, :format => :json }
                assert_successful_response({
                    "lesson_streams" => []
                })
            end

            it "should allow institutional report viewers to filter by user for their institutional users" do
                institution = Institution.joins(:reports_viewers).first
                reports_viewer = institution.reports_viewers.first
                expect(reports_viewer).not_to be_nil

                stub_current_user(controller, reports_viewer)

                target_user = institution.users.where.not(id: reports_viewer.id).first
                expect(target_user).not_to be_nil
                user = target_user

                expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                    expect(to_json_instance.user).to eq(user)
                "[]"
                end
                get :index, params: { :user_id => user.id, :format => :json }
                assert_successful_response({
                    "lesson_streams" => []
                })
            end

            it "should not allow non-institutional report viewers with access below super_editor to filter by user" do
                stub_current_user(controller, [:editor])
                other_user = User.where.not(id: controller.current_user.id).first
                expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).not_to receive(:json)
                get :index, params: { :user_id => other_user.id, :format => :json }
                assert_error_response(401)
            end

            it "should not institutional report viewers filter by user if that user is not within a permitted institution" do
                institution = Institution.joins(:reports_viewers).first
                reports_viewer = institution.reports_viewers.first
                expect(reports_viewer).not_to be_nil

                stub_current_user(controller, reports_viewer)

                target_user = User.where.not(id: institution.users.ids).first
                expect(target_user).not_to be_nil
                user = target_user

                expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).not_to receive(:json)
                get :index, params: { :user_id => user.id, :format => :json }
                assert_error_response(401)
            end

        end

        # filters comes in as a json-formatted hash.  The controller is supposed to
        # parse the json
        it "should support filters" do
            stub_current_user(controller, [:admin])
            expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                expect(to_json_instance.filters[:favorite]).to eq(true)
                "[]"
            end
            get :index, params: { :filters => {favorite: true}.to_json, :format => :json }
            assert_successful_response({
                "lesson_streams" => []
            })
        end

        it "should call get_has_available_incomplete_streams if requested" do
            stub_current_user(controller, [:learner])
            expect(controller).to receive(:get_has_available_incomplete_streams)
            get :index, params: { :get_has_available_incomplete_streams => true, :format => :json }
        end
    end

    describe "get_has_available_incomplete_streams" do
        it "should set value to false if no streams returned" do
            stub_current_user(controller, [:learner])
            expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                expect(to_json_instance.filters[:user_can_see]).to eq(true)
                expect(to_json_instance.filters[:published]).to eq(true)
                expect(to_json_instance.filters[:complete]).to eq(false)
                expect(to_json_instance.user_id).to eq(controller.current_user_id)
                expect(to_json_instance.fields).to eq(['id'])
                expect(to_json_instance.limit).to eq(1)
                "[]"
            end

            controller.get_has_available_incomplete_streams
            expect(controller.meta['has_available_incomplete_streams']).to be(false)
        end

        it "should set value to true if streams returned" do
            stub_current_user(controller, [:admin])
            expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                expect(to_json_instance.filters[:user_can_see]).to eq(true)
                expect(to_json_instance.filters[:published]).to eq(true)
                expect(to_json_instance.filters[:complete]).to eq(false)
                expect(to_json_instance.user_id).to eq(controller.current_user_id)
                expect(to_json_instance.fields).to eq(['id'])
                expect(to_json_instance.limit).to eq(1)
                [{id: SecureRandom.uuid}].to_json
            end

            controller.get_has_available_incomplete_streams
            expect(controller.meta['has_available_incomplete_streams']).to be(true)
        end
    end

    describe "show" do

        before(:each) do
            @instance = Lesson::Stream.where.not(id: Lesson::Stream.all_published.pluck('id')).first
        end

        it "should return an instance if it finds one" do
            expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                expect(to_json_instance.filters[:user_can_see]).to be(true)
                expect(to_json_instance.params[:id]).to eq(@instance.id)
                expect(to_json_instance.params[:user]).to eq(controller.current_user)
                "[{\"id\":\"#{@instance.id}\"}]"
            end
            get :show, params: { :id => @instance.id, :format => :json }
            assert_successful_response({
                "lesson_streams" => [{"id"=>@instance.id}]
            })
        end

        it "should 404 if instance cannot be found" do
            id  = SecureRandom.uuid
            expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json).and_return('[]')
            get :show, params: { :id => id, :format => :json }
            assert_error_response(404, {'message' => "No #{"lesson_streams".singularize} found for id=\"#{id}\""})
        end


        describe "with no current_user" do

            it "should return an instance" do
                expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                    expect(to_json_instance.filters[:user_can_see]).to be(nil)
                    expect(to_json_instance.filters[:view_as]).to eq('cohort:PROMOTED_DEGREE')
                    expect(to_json_instance.params[:id]).to eq(@instance.id)
                    "[{\"id\":\"#{@instance.id}\"}]"
                end
                stub_no_current_user(controller)
                get :show, params: { :id => @instance.id, :filters => { :view_as => 'cohort:PROMOTED_DEGREE' , :user_can_see => nil }.to_json, :format => :json }
                assert_successful_response({
                    "lesson_streams" => [{"id"=>@instance.id}]
                })
            end

        end

        describe "Lesson::Stream::ToJsonFromApiParams::UnauthorizedError handling" do

            it "should render logged_out error with no user when trying to load_full_content for content that is not open" do
                # For example, lessons marked as unrestricted can be loaded by anonymous users for things like the demo lesson player.

                stub_no_current_user(controller)
                expect_unauthorized_error
                get :show, params: {
                    :id => @instance.id,
                    :load_full_content_for_lesson_id => SecureRandom.uuid,
                    :format => :json,
                    :filters => {
                        :user_can_see => nil,
                        :view_as => 'cohort:PROMOTED_DEGREE'
                    }.to_json
                }
                assert_error_response(401, {"message"=>"You must be logged in to do that", "not_logged_in"=>true})
            end

            it "should render unauthorized error with user" do
                stub_current_user(controller, [:learner])
                expect_unauthorized_error
                get :show, params: {
                    :id => @instance.id,
                    :load_full_content_for_lesson_id => SecureRandom.uuid,
                    :format => :json
                }
                assert_error_response(401, {'message' => "You are not authorized to do that."})
            end

            def expect_unauthorized_error
                expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json).and_raise(Lesson::Stream::ToJsonFromApiParams::UnauthorizedError.new(''))
            end
        end


        describe "with_user_with_subscription" do
            before(:each) do
                @user = users(:user_with_subscriptions_enabled)
                allow(controller).to receive(:current_user).and_return(@user)
            end

            it "should allow show if user has active subscription" do

                create_customer_with_default_source(user)

                expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                    expect(to_json_instance.params[:id]).to eq(@instance.id)
                    "[{\"id\":\"#{@instance.id}\"}]"
                end

                create_subscription(user)
                user.save!
                get :show, params: { :id => @instance.id, :format => :json }
                assert_successful_response({
                    "lesson_streams" => [{"id"=>@instance.id}]
                })

            end

            it "should not show if user does not have active subscription" do
                user.save!
                expect(user.primary_subscription).to be_nil #sanity check
                get :show, params: { :id => @instance.id, :format => :json }
                assert_error_response(401, {'message' => "You are not authorized to do that."})
            end
        end

        it "should allow permitted user to see unpublished content" do
            expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                expect(to_json_instance.filters[:published]).to be(false)
                "[{\"id\":\"#{@instance.id}\"}]"
            end
            stub_current_user(controller, [:admin])
            get :show, params: { :id => @instance.id, :filters => {:published => false}, :format => :json }
            assert_successful_response({
                "lesson_streams" => [{"id"=>@instance.id}]
            })
        end

        # filters comes in as a json-formatted hash.  The controller is supposed to
        # parse the json
        it "should support filters" do
            stub_current_user(controller, [:admin])
            expect_any_instance_of(Lesson::Stream::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                expect(to_json_instance.filters[:favorite]).to eq(true)
                "[{\"id\":\"#{@instance.id}\"}]"
            end
            get :show, params: { :id => @instance.id, :filters => {favorite: true}.to_json, :format => :json }
            assert_successful_response({
                "lesson_streams" => [{"id"=>@instance.id}]
            })
        end

    end

    # NOTE: In the index specs, we mock out ToJsonFromApiParams and just check
    # the parameters that went to it.  It would be nice to do that here,
    # but since we make two calls to ToJsonFromApiParams, and we rely on the results
    # of the first one, it seemed like it would just add complexity to try to do 
    # it that way here.  So, here we actually let the calls go through to ToJsonFromApiParams
    # and we assert on the results
    describe "get_outdated" do
        it "should load up stream that has been re-published" do
            streams_in_client, params = get_streams(2)

            # The streams have already been loaded in the client.  Re-publish
            # some of them on the server so they will be returned by get_outdated
            streams_in_client[0].publish!(update_published_at: true)

            post :get_outdated, params: params

            returned_streams = body_json['contents']['lesson_streams']
            expect(returned_streams.pluck('id')).to eq([streams_in_client[0]['id']])
        end

        it "should load up stream with a lesson that has been re-published" do
            streams_in_client, params = get_streams(2)

            # The streams have already been loaded in the client.  Re-publish
            # some of them on the server so they will be returned by get_outdated
            streams_in_client[0].lessons[0].publish!(update_published_at: true)

            post :get_outdated, params: params

            returned_streams = body_json['contents']['lesson_streams']
            expect(returned_streams.pluck('id')).to eq([streams_in_client[0]['id']])
        end

        it "should load up full content for a stream if requested" do
            stream = lesson_streams(:stream_with_frame_list_lesson)
            params = {
                :format => :json,
                :streams => [get_stream_entry(stream).merge(load_full_content: true)]
            }
            stream.publish!(update_published_at: true)
            post :get_outdated, params: params
            returned_streams = body_json['contents']['lesson_streams']
            expect(returned_streams[0]['lessons'][0]['frames']&.any?).to be(true)
        end

        it "should not load up full content for a stream if not requested" do
            stream = lesson_streams(:stream_with_frame_list_lesson)
            params = {
                :format => :json,
                :streams => [get_stream_entry(stream)]
            }
            stream.publish!(update_published_at: true)
            post :get_outdated, params: params
            returned_streams = body_json['contents']['lesson_streams']
            expect(returned_streams[0]['lessons'][0]['frames']&.any?).not_to be(true)
        end

        def get_streams(count)
            streams = []
            Lesson::Stream.all_published.each do |stream|

                # we need streams where published_at is set (some of the 
                # content created in fixture builder does not have published_at set)
                next unless stream.published_version.published_at
                next unless stream.lessons&.first&.published_version&.published_at

                # we need the streams we load up to have distinct lessons
                next if (stream.lessons.map(&:id) & streams.map(&:lessons).flatten.map(&:id)).any?
                streams << stream
                break if streams.size == count
            end
            expect(streams.size).to eq(count)
            [
                streams.map(&:published_version),
                {
                    :format => :json,
                    :streams => streams.map { |stream| get_stream_entry(stream) } 
                }
            ]
        end

        def get_stream_entry(stream)
            content_updated_at = [
                stream.published_version.published_at, 
                stream.lessons.map(&:published_version).compact.map(&:published_at)
            ].flatten.max
            {
                id: stream['id'],
                content_updated_at: content_updated_at.to_timestamp
            }
        end
    end

    def create_one_stream_with_one_published_lesson
        Lesson::Stream.destroy_all
        stream = FactoryBot.create(:stream)

        # publish the stream and only one of it's lessons
        expect(stream.lessons.size).to be > 1 # sanity check
        stream.lessons.each(&:unpublish!)
        stream.lessons.first.publish!
        stream.chapters =
        [{
            "title"=>"Default Chapter Title",
            "lesson_ids"=>stream.lessons.map(&:id)
        }]

        stream.publish!
        stream.add_to_group("GROUP")
        stream
    end

end
