require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::CandidatePositionInterestsController do
    include ControllerSpecHelper
    json_endpoint

    before(:each) do
        stub_client_params(@controller)
    end

    describe "GET index" do

        it "should 401 if ability says cannot" do
            allow(controller.current_ability).to receive(:can?).and_return(false)
            get :index, params: { :format => :json, candidate_id: SecureRandom.uuid }
            assert_error_response(401)
        end

        describe "with_valid_auth" do
            attr_accessor :can_manage

            before(:each) do
                allow(controller.current_ability).to receive(:can?) do |meth, obj|
                    if meth == :manage
                        can_manage
                    else
                        true
                    end
                end
                self.can_manage = true

                # using hiring_manager_with_team because ze has multiple positions, which some specs require
                @hiring_manager = users(:hiring_manager_with_team)
                @open_position = @hiring_manager.open_positions.first
                @candidate = users(:learner)
            end

            it "should return position interests for a hiring manager" do
                interest = CandidatePositionInterest.create!({candidate_id: @candidate.id, open_position_id: @open_position.id, candidate_status: 'accepted' })
                allow(controller).to receive(:current_user).and_return(@hiring_manager)
                get :index, params: { :format => :json, filters: { hiring_manager_id: @hiring_manager.id }.to_json }
                assert_200
                entries = body_json['contents']['candidate_position_interests']
                expect(entries.pluck('id')).to include(interest.id)
            end

            it "should return position interests for a hiring manager and their team" do
                hiring_manager = users(:hiring_manager_with_team)
                interest = CandidatePositionInterest.create!({
                    candidate_id: @candidate.id, open_position_id: hiring_manager.open_positions.first.id, candidate_status: 'accepted'
                })

                teammate = users(:hiring_manager_teammate)
                teammates_interest = CandidatePositionInterest.create!({
                    candidate_id: @candidate.id, open_position_id: teammate.open_positions.first.id, candidate_status: 'accepted'
                })

                allow(controller).to receive(:current_user).and_return(@hiring_manager)
                get :index, params: { :format => :json, filters: { hiring_team_id: hiring_manager.hiring_team_id }.to_json }
                assert_200
                entries = body_json['contents']['candidate_position_interests']
                expect(entries.pluck('id')).to include(interest.id)
                expect(entries.pluck('id')).to include(teammates_interest.id)
            end

            it "should return position interests for a candidate" do
                interest = CandidatePositionInterest.create!({candidate_id: @candidate.id, open_position_id: @open_position.id, candidate_status: 'accepted' })
                allow(controller).to receive(:current_user).and_return(@candidate)
                get :index, params: { :format => :json, filters: { candidate_id: @candidate.id }.to_json }
                assert_200
                entries = body_json['contents']['candidate_position_interests']
                expect(entries.pluck('id')).to include(interest.id)
            end

            it "should filter by open_position_id" do
                interest = CandidatePositionInterest.create!({candidate_id: @candidate.id, open_position_id: @open_position.id, candidate_status: 'accepted' })
                allow(controller).to receive(:current_user).and_return(@candidate)

                get :index, params: { :format => :json, filters: { candidate_id: @candidate.id, open_position_id: [@open_position.id] }.to_json }
                assert_200
                entries = body_json['contents']['candidate_position_interests']
                expect(entries.pluck('id')).to include(interest.id)

                get :index, params: { :format => :json, filters: { candidate_id: @candidate.id, open_position_id: ['not-found'] }.to_json }
                assert_200
                entries = body_json['contents']['candidate_position_interests'] || []
                expect(entries.pluck('id')).not_to include(interest.id)
            end

            it "should filter by hiring_manager_status_not" do
                interest = CandidatePositionInterest.create!({
                    candidate_id: @candidate.id,
                    open_position_id: @open_position.id,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'hidden'
                })
                allow(controller).to receive(:current_user).and_return(@hiring_manager)
                get :index, params: { :format => :json, filters: { hiring_manager_id: @open_position.hiring_manager_id, hiring_manager_status_not: 'hidden' }.to_json }
                assert_200
                entries = body_json['contents']['candidate_position_interests'] || []
                expect(entries.pluck('id')).not_to include(interest.id)
            end

            it "should filter by candidate_status_not" do
                interest = CandidatePositionInterest.create!({
                    candidate_id: @candidate.id,
                    open_position_id: @open_position.id,
                    candidate_status: 'rejected',
                    hiring_manager_status: 'hidden'
                })
                allow(controller).to receive(:current_user).and_return(@hiring_manager)
                get :index, params: { :format => :json, filters: { hiring_manager_id: @open_position.hiring_manager_id, candidate_status_not: 'rejected' }.to_json }
                assert_200
                entries = body_json['contents']['candidate_position_interests'] || []
                expect(entries.pluck('id')).not_to include(interest.id)
            end

            describe "anonymization" do
                attr_accessor :candidate_ids, :current_user
                before(:each) do
                    allow(controller).to receive(:current_user) do
                        current_user
                    end
                    self.current_user = @hiring_manager

                    self.candidate_ids = CareerProfile.active.limit(2).pluck(:user_id)
                    2.times do |i|
                        CandidatePositionInterest.create!({
                            candidate_id: candidate_ids[i],
                            open_position_id: @open_position.id,
                            candidate_status: 'accepted',
                            hiring_manager_status: 'pending'
                        })
                    end
                end

                it "should not prevent a candidate from viewing zir own interests" do
                    self.can_manage = false
                    self.current_user = User.find(candidate_ids.first)
                    get :index, params: { :format => :json, filters: {
                        open_position_id: @open_position.id,
                        candidate_id: current_user.id
                    }.to_json }
                    assert_200
                    entries = body_json['contents']['candidate_position_interests']

                    expect(entries.size).to eq(1)
                    expect(entries[0]['candidate_id']).to eq(self.current_user.id) # sanity check
                end

                it "should pay attention to should_anonymize_interest?" do
                    self.can_manage = false
                    interests = @open_position.candidate_position_interests.limit(2)
                    expect_any_instance_of(HiringTeam).to receive(:should_anonymize_interest?) do |hiring_team, interest|
                        interests[0] == interest
                    end.at_least(1).times

                    get :index, params: { :format => :json, filters: { open_position_id: @open_position.id }.to_json }
                    assert_200
                    entries = body_json['contents']['candidate_position_interests'].index_by { |e| e['id'] }

                    expect(entries[interests[0].id]['career_profile']['anonymized']).to eq(true)
                    expect(entries[interests[1].id]['career_profile']['anonymized']).to eq(false)
                end

                it "should not try to call should_anonymize_interest? if admin access" do
                    self.can_manage = true
                    expect_any_instance_of(HiringTeam).not_to receive(:should_anonymize_interest?)

                    get :index, params: { :format => :json, filters: { open_position_id: @open_position.id }.to_json }
                    assert_200
                    entries = body_json['contents']['candidate_position_interests']

                    expect(entries.map { |e| e['career_profile']['anonymized']}.uniq).to eq([false])
                end

            end

            describe "hiring_manager_status" do
                it "should exclude hiring_manager_status when provided in except" do
                    CandidatePositionInterest.create!({candidate_id: @candidate.id, open_position_id: @open_position.id, candidate_status: 'accepted' })
                    allow(controller).to receive(:current_user).and_return(@hiring_manager)
                    get :index, params: { :format => :json, filters: { hiring_manager_id: @hiring_manager.id }.to_json, except: ['hiring_manager_status'] }
                    assert_200
                    entries = body_json['contents']['candidate_position_interests']
                    expect(entries[0]).not_to have_key("hiring_manager_status")
                end

                it "should include hiring_manager_status when not provided in except" do
                    CandidatePositionInterest.create!({candidate_id: @candidate.id, open_position_id: @open_position.id, candidate_status: 'accepted' })
                    allow(controller).to receive(:current_user).and_return(@hiring_manager)
                    get :index, params: { :format => :json, filters: { hiring_manager_id: @hiring_manager.id }.to_json }
                    assert_200
                    entries = body_json['contents']['candidate_position_interests']
                    expect(entries[0]).to have_key("hiring_manager_status")
                end
            end
        end

        describe "preload_for_index" do
            it "should use eager loading" do

                # Mock this out, since in prod it will almost always be cached
                allow(Cohort).to receive(:cached_published).and_return(
                    OpenStruct.new(
                        name: 'MBA1',
                        id: '1234',
                        start_date: Time.now + 10.days,
                        end_date: Time.now + 1.year
                    )
                )

                interests = controller.preload_for_index

                # Once the records are preloaded, there should be no more queries required to convert them to json.
                expect(ActiveRecord::Base.connection).not_to receive(:exec_query).and_call_original
                json = interests.as_json(career_profile_options: {fields: []})

                # sanity checks. make sure that we actually generated a full json tree
                expect(interest = json[0]).not_to be_nil
                expect(interest['hiring_manager_status']).not_to be_nil
            end
        end

    end

    describe "POST create" do

        it "should allow admin to create and set everything" do
            allow(controller).to receive(:current_user).and_return(users(:admin))
            open_position = OpenPosition.first
            candidate = users(:learner)
            expect {
                post :create, params: {
                    :format => :json,
                    record: {
                        open_position_id: open_position.id,
                        candidate_id: candidate.id,
                        candidate_status: 'accepted',
                        hiring_manager_status: CandidatePositionInterest.reviewed_hiring_manager_statuses.first,
                        hiring_manager_priority: 1
                    }
                }
            }.to change { CandidatePositionInterest.count }.by(1)
            assert_200

            record = CandidatePositionInterest.reorder('created_at').last
            expect(record.open_position_id).to eq(open_position.id)
            expect(record.candidate_id).to eq(candidate.id)
            expect(record.candidate_status).to eq('accepted')
            expect(record.hiring_manager_status).to eq(CandidatePositionInterest.reviewed_hiring_manager_statuses.first)
            expect(record.hiring_manager_priority).to eq(1.0)
        end

        it "should allow candidate to create and set everything but hiring_manager_status, hiring_manager_priority, and admin_status" do
            candidate = users(:learner)
            allow(controller).to receive(:current_user).and_return(candidate)
            open_position = OpenPosition.first
            expect {
                post :create, params: {
                    :format => :json,
                    record: {
                        open_position_id: open_position.id,
                        candidate_id: candidate.id,
                        candidate_status: 'rejected',
                        hiring_manager_status: CandidatePositionInterest.reviewed_hiring_manager_statuses.first,
                        hiring_manager_priority: 1,
                        admin_status: 'outstanding'
                    }
                }
            }.to change { CandidatePositionInterest.count }.by(1)
            assert_200

            record = CandidatePositionInterest.reorder('created_at').last
            expect(record.open_position_id).to eq(open_position.id)
            expect(record.candidate_id).to eq(candidate.id)
            expect(record.candidate_status).to eq('rejected')

            # we should have ignored the value that came in, since the candidate
            # is not allowed to set it
            expect(record.hiring_manager_status).to eq('hidden')
            expect(record.hiring_manager_priority).to be(nil)
            expect(record.admin_status).to eq('unreviewed')
        end

        it "should not allow hiring_manager to create" do
            hiring_manager = users(:hiring_manager)
            allow(controller).to receive(:current_user).and_return(hiring_manager)
            expect {
                post :create, params: {
                    format: :json,
                    record: {
                        open_position_id: SecureRandom.uuid
                    }
                }
            }.not_to change { CandidatePositionInterest.count }
            assert_401
        end

        it "should update an existing record" do
            allow(controller).to receive(:current_user).and_return(users(:admin))
            open_position = OpenPosition.first
            candidate = users(:learner)

            record_hash = {
                        open_position_id: open_position.id,
                        candidate_id: candidate.id,
                        candidate_status: 'accepted',
                        hiring_manager_status: CandidatePositionInterest.reviewed_hiring_manager_statuses.first
                    }

            CandidatePositionInterest.create!(record_hash)

            expect {
                post :create, params: {
                    :format => :json,
                    record: record_hash
                }
            }.not_to change { CandidatePositionInterest.count }

            assert_200
        end

        it "should call add_num_recommended_positions_to_push_messages" do
            candidate = users(:learner)
            allow(controller).to receive(:current_user).and_return(candidate)
            expect(controller).to receive(:add_num_recommended_positions_to_push_messages)
            post :create, params: {
                :format => :json,
                record: {
                    open_position_id: OpenPosition.first.id,
                    candidate_id: candidate.id,
                    candidate_status: 'rejected',
                    hiring_manager_status: CandidatePositionInterest.reviewed_hiring_manager_statuses.first,
                    hiring_manager_priority: 1,
                    admin_status: 'outstanding'
                }
            }
        end
    end

    describe "PUT update" do

        it "should add updated_ats to meta" do
            hiring_manager = users(:hiring_manager)
            record = CandidatePositionInterest.where({
                candidate_status: 'accepted',
                hiring_manager_status: 'unseen'
            }).first
            hiring_manager = record.open_position.hiring_manager
            allow(hiring_manager.hiring_team).to receive(:has_full_access?).and_return(true)
            allow(controller).to receive(:current_user).and_return(record.open_position.hiring_manager)

            start = Time.now.to_f
            put :update, params: {
                :format => :json,
                record: record.as_json(career_profile_options: {fields: []}).merge(
                    hiring_manager_status: 'pending'
                )
            }
            assert_200
            record.reload

            expect(body_json['meta']['push_messages']['unloaded_change_detector']['candidate_position_interest_before_changes'] > start).to be(false)
            expect(body_json['meta']['push_messages']['unloaded_change_detector']['candidate_position_interest'] > start).to be(true)
        end

        it "should allow admin to update everything" do
            record = CandidatePositionInterest.where(candidate_status: 'accepted', hiring_manager_status: 'unseen').first
            allow(controller).to receive(:current_user).and_return(users(:admin))

            expected_last_curated_at = Time.now
            allow(Time).to receive(:now).and_return(expected_last_curated_at)

            expect {
            expect {
                put :update, params: {
                    :format => :json,
                    record: record.as_json(career_profile_options: {fields: []}).merge(
                        candidate_status: 'rejected',
                        hiring_manager_status: 'hidden'
                    ),
                    meta: {
                        last_curated_at: expected_last_curated_at.to_timestamp
                    }
                }
                assert_200
                record.reload
            }.to change { record.candidate_status}
            }.to change { record.hiring_manager_status}

            expect(record.open_position.last_curated_at.to_timestamp).to eq(expected_last_curated_at.to_timestamp)

            expect(record.candidate_status).to eq('rejected')
            expect(record.hiring_manager_status).to eq('hidden')
        end

        it "should not allow regular user to update last_curated_at" do
            record = CandidatePositionInterest.where(candidate_status: 'accepted', hiring_manager_status: 'hidden').first
            record.open_position.update_column(:last_curated_at, nil)

            allow(controller).to receive(:current_user).and_return(record.candidate)
            put :update, params: {
                :format => :json,
                record: record.as_json(career_profile_options: {fields: []}).merge(
                    candidate_status: 'rejected',
                    hiring_manager_status: 'pending'
                ),
                meta: {
                    last_curated_at: Time.now
                }
            }
            assert_200
            expect(record.open_position.reload.last_curated_at).to be_nil
        end

        it "should allow candidate to update candidate_status and cover_letter" do
            record = CandidatePositionInterest.where(candidate_status: 'accepted', hiring_manager_status: 'hidden').first
            record.update_columns(cover_letter: {content: "foo"})
            allow(controller).to receive(:current_user).and_return(record.candidate)

            expect {
            expect {
                put :update, params: {
                    :format => :json,
                    record: record.as_json(career_profile_options: {fields: []}).merge(
                        candidate_status: 'rejected',
                        hiring_manager_status: 'pending',
                        cover_letter: {content: "bar"}
                    )
                }
                assert_200
                record.reload
            }.to change { record.candidate_status }.and change { record.cover_letter }
            }.not_to change { record.hiring_manager_status}
        end

        it "should allow hiring_manager to update hiring_manager_status" do
            hiring_manager = users(:hiring_manager)
            record = CandidatePositionInterest.where({
                candidate_status: 'accepted',
                hiring_manager_status: 'unseen'
            }).first
            hiring_manager = record.open_position.hiring_manager
            allow(hiring_manager.hiring_team).to receive(:has_full_access?).and_return(true)
            allow(controller).to receive(:current_user).and_return(record.open_position.hiring_manager)

            expect {
            expect {
                put :update, params: {
                    :format => :json,
                    record: record.as_json(career_profile_options: {fields: []}).merge(
                        candidate_status: 'rejected',
                        hiring_manager_status: 'pending'
                    )
                }
                assert_200
                record.reload
            }.not_to change { record.candidate_status}
            }.to change { record.hiring_manager_status}
        end

        it "should allow hiring_manager to update hiring_manager_status for teammates' interests" do
            hiring_manager = users(:hiring_manager_with_team)
            teammate = users(:hiring_manager_teammate)

            record = CandidatePositionInterest.joins(:open_position).where(open_position_id: hiring_manager.open_positions.pluck(:id)).first
            record.update_columns({
                candidate_status: 'accepted',
                hiring_manager_status: 'unseen'
            })

            allow(controller).to receive(:current_user).and_return(teammate)
            allow(teammate).to receive(:can?).with(:update_candidate_position_interests, anything).and_return(true)

            expect {
            expect {
                put :update, params: {
                    :format => :json,
                    record: record.as_json(career_profile_options: {fields: []}).merge(
                        candidate_status: 'rejected',
                        hiring_manager_status: 'pending'
                    ),
                    meta: {
                        last_curated_at: Time.now.to_timestamp
                    }
                }
                assert_200
                record.reload
            }.not_to change { record.candidate_status}
            }.to change { record.hiring_manager_status}
        end

        it "should render 409 conflict code with updated record if CandidatePositionInterest::OldVersion is raised" do
            record = CandidatePositionInterest.first
            hiring_manager = record.open_position.hiring_manager
            allow(controller).to receive(:current_user).and_return(hiring_manager)
            allow(hiring_manager.hiring_team).to receive(:has_full_access?).and_return(true)
            expect(CandidatePositionInterest).to receive(:update_from_hash!).and_raise(CandidatePositionInterest::OldVersion)

            allow(controller).to receive(:current_user).and_return(record.open_position.hiring_manager)
            put :update, :params => {
                :format => :json,
                :record => record.as_json(career_profile_options: {anonymize: false})
            }
            assert_error_response(409)
            expect(body_json['message']).to eq('Interest has been saved more recently')
            expect(body_json['meta']['candidate_position_interest']['id']).to eq(record.id)
            expect(body_json['meta']['candidate_position_interest']['career_profile']['anonymized']).to eq(false)
        end

        it "should 401 if user cannot update" do
            record = CandidatePositionInterest.first
            permission_checked = false
            expect(controller).to receive(:current_user).and_return(record.open_position.hiring_manager).at_least(1).times
            expect(controller.current_ability).to receive(:can?) do |meth, obj|
                if meth == :update && obj.is_a?(CandidatePositionInterest)
                    permission_checked = true
                    false
                else
                    true
                end
            end

            put :update, :params => {
                :format => :json,
                :record => record.as_json(career_profile_options: {fields: []})
            }
            assert_error_response(401)
            expect(permission_checked).to be(true)
        end

        it "should call add_num_recommended_positions_to_push_messages" do
            record = CandidatePositionInterest.where(candidate_status: 'accepted', hiring_manager_status: 'hidden').first
            allow(controller).to receive(:current_user).and_return(record.candidate)
            expect(controller).to receive(:add_num_recommended_positions_to_push_messages)
            put :update, params: {
                :format => :json,
                record: record.as_json(career_profile_options: {fields: []}).merge(
                    candidate_status: 'rejected',
                    hiring_manager_status: 'pending'
                )
            }
        end
    end

    describe "PUT batch_update" do

        it "should 401 if non-admin" do
            stub_current_user
            put :batch_update,
                params: {
                    :candidate_position_interest_ids => CandidatePositionInterest.limit(5).pluck(:id),
                    :candidate_status => 'accepted'
                }
            assert_error_response(401)
        end

        it "should work" do
            interests = CandidatePositionInterest.limit(5)
            interests.update_all(candidate_status: 'new')
            interests.update_all(hiring_manager_priority: nil)
            interests = interests.to_a

            interests.each_with_index do |interest, index|
                interests[index].candidate_status = 'accepted'
                interests[index].hiring_manager_priority = index
            end

            stub_current_user(controller, [:admin])
            put :batch_update,
                params: {
                    format: :json,
                    records: interests.as_json(career_profile_options: {fields: []})
                }
            assert_200

            interests.each_with_index do |interest, index|
                record = CandidatePositionInterest.find(interest.id)
                expect(record.candidate_status).to eq('accepted')
                expect(record.hiring_manager_priority).to be(index)
            end

            returned_ids = body_json['contents']['candidate_position_interests'].map {|interest| interest['id']}
            expected_ids = interests.map {|interest| interest['id']}
            expect(returned_ids).to match_array(expected_ids)
        end

        it "should render 409 conflict code with updated record if CandidatePositionInterest::OldVersion is raised" do
            interests = CandidatePositionInterest.limit(5)
            interests = interests.to_a

            stub_current_user(controller, [:admin])

            expect(CandidatePositionInterest).to receive(:update_from_hash!).and_raise(CandidatePositionInterest::OldVersion)

            put :batch_update,
                :params => {
                    :format => :json,
                    :records => interests.as_json(career_profile_options: {fields: []})
            }
            assert_error_response(409)
            expect(body_json['message']).to eq('Interest has been saved more recently')
            expect(body_json['meta']['candidate_position_interest']['id']).to eq(interests.first.id)
            expect(body_json['meta']['candidate_position_interest']['career_profile']['anonymized']).to eq(false)
        end
    end
end