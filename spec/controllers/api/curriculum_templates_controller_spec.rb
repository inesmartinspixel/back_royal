require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::CurriculumTemplatesController do

    fixtures :users

    include ControllerSpecHelper
    include StripeHelper
    json_endpoint

    before(:each) do
        SafeCache.delete("stripe/plans_list")
        stub_client_params(controller)
        stub_current_user(controller, [:admin])
        create_default_plan
    end

    describe "GET index" do

        before(:each) do
             @coupon = Stripe::Coupon.create(
                :amount_off => 15000,
                :duration => 'forever',
                :currency => 'usd',
                :id => 'discount_150'
            )
        end

        it "should work" do
            create_default_plan
            CurriculumTemplate.destroy_all
            CurriculumTemplate.create!({"name" => "test template", "program_type" => "mba"})
            get :index, params: {format: :json}
            expect(body_json["contents"]["curriculum_templates"].size).to eq(1)
            expect(body_json["contents"]["curriculum_templates"][0]["name"]).to eq("test template")
            expect(body_json["meta"]["stripe_plans"]).to match_array([@default_plan.to_h.slice(:id, :amount).merge({ name: @default_plan[:nickname], frequency: "monthly" }).as_json])
            expect(body_json["meta"]["stripe_coupons"]).to match_array([{"id"=>"none", "amount_off"=>0, "percent_off"=>0}.as_json, @coupon.to_h.slice(:id, :name, :amount_off, :percent_off).as_json])
        end

        describe "learner_projects" do

            it "should include learner_projects when get_learner_projects" do
                LearnerProject.create!(title: 'title',requirement_identifier: 'requirement_identifier')
                get :index, params: {get_learner_projects: true, format: :json}
                expect(body_json["meta"]["learner_projects"].map { |e| e['id']}).to match_array(LearnerProject.pluck(:id))
            end
        end
    end

    describe "POST create" do
        it "creates a new one" do
            expect {
                post :create, params: {record: {"name" => "test template", "program_type" => "mba", "enrollment_deadline_days_offset" => 4 }, format: :json}
            }.to change(CurriculumTemplate.all, :count).by(1)
            expect(response.status).to eq(200) # ok
            expect(body_json["contents"]["curriculum_templates"].size).to eq(1)
            expect(body_json["contents"]["curriculum_templates"][0]["name"]).to eq("test template")
            expect(body_json["contents"]["curriculum_templates"][0]["enrollment_deadline_days_offset"]).to eq(4)
        end

        it "creates a new one with groups" do
            groups = [AccessGroup.first, AccessGroup.second]
            expect {
                post :create, params: {record: { "name" => "test template" , "groups" => groups, "program_type" => "mba" }, format: :json}
            }.to change(CurriculumTemplate.all, :count).by(1)
            expect(response.status).to eq(200) # ok
            expect(body_json["contents"]["curriculum_templates"].size).to eq(1)
            expect(body_json["contents"]["curriculum_templates"][0]["groups"]).to eq(groups.as_json)
        end
    end

    describe "PUT update" do
        describe "as non-admin" do
            before(:each) do
                @user = users(:user_with_career_profile)
                allow(controller).to receive(:current_user).and_return(@user)
            end

            it "should 401" do
                process :update, params: { :format => :json }
                assert_error_response(401)
            end
        end

        describe "as admin" do

            before(:each) do
                stub_current_user(controller, [:admin])
            end

            it "should work" do
                curriculum_template = CurriculumTemplate.create!({
                    name: "old name",
                    program_type: "mba"
                })
                required_playlist_pack_ids = [Playlist::LocalePack.first.id, Playlist::LocalePack.second.id]
                specialization_playlist_pack_ids = [Playlist::LocalePack.third.id, Playlist::LocalePack.fourth.id]
                get :update, params: { :format => :json, :record => {
                    id: curriculum_template.id,
                    name: "new name",
                    playlist_collections: [{title: 'Some Playlist Collection Title', required_playlist_pack_ids: required_playlist_pack_ids}],
                    specialization_playlist_pack_ids: specialization_playlist_pack_ids,
                    updated_at: Time.now.to_timestamp
                } }
                assert_200

                curriculum_template.reload
                expect(curriculum_template.name).to eq("new name")
                expect(curriculum_template.playlist_collections).to eq([{'title' => 'Some Playlist Collection Title', 'required_playlist_pack_ids' => required_playlist_pack_ids}])
                expect(curriculum_template.specialization_playlist_pack_ids).to eq(specialization_playlist_pack_ids)
            end
        end
    end

    describe "DELETE destroy" do
        it "should work" do
            curriculum_template = CurriculumTemplate.create!({"name" => "test template", "program_type" => "mba"})
            expect {
                delete :destroy, params: {id: curriculum_template.to_param, format: :json}
            }.to change(CurriculumTemplate, :count).by(-1)
            # response.status.should == 204 # no_content
            expect(response.status).to eq(200) # ok
        end

    end
end