require 'spec_helper'
require 'controllers/api/helpers/api_content_controller_spec_helper'

describe Api::CohortPromotionsController do

    fixtures :cohorts

    include ControllerSpecHelper
    json_endpoint

    let(:valid_attributes) {
        {
            "name" => "Organization 1",
            "start_date" => Time.now,
            "end_date" => Time.now,
            "program_type" => "mba",
            "periods" => [{"foo" => "bar"}]
        }
    }

    def new_cohort_params
        {
            "name" => "Organization 1",
            "start_date" => Time.now.to_timestamp,
            "end_date" => Time.now.to_timestamp,
            "program_type" => "mba",
            "periods" => [{"foo" => "bar"}]
        }
    end

    before(:each) do
        stub_client_params(controller)
        stub_current_user(controller, [:admin])
    end

    describe "POST create" do
        it "creates a new record" do
            CohortPromotion.delete_all
            cohort = cohorts(:published_career_network_only_cohort)
            allow_any_instance_of(Cohort).to receive(:supports_manual_promotion?).and_return(true)

            expect {
                post :create, params: {record: { cohort_id: cohort.id}, format: :json, meta: {}}
            }.to change(CohortPromotion.all, :count).by(1)

            expect(response.status).to eq(200) # ok

            expect(body_json["contents"]["cohort_promotions"].size).to eq(1)
            expect(body_json["contents"]["cohort_promotions"][0]["cohort_id"]).to eq(cohort.id)
        end
    end

    describe "PUT update" do
        before :each do
            @cohort_promotion = CohortPromotion.first
        end

        it "updates a record" do
            another_cohort_id = Cohort.all_published.where.not(id: @cohort_promotion.cohort_id).first.id
            allow_any_instance_of(Cohort).to receive(:supports_manual_promotion?).and_return(true)

            put :update, params: {record: {id: @cohort_promotion.id, cohort_id: another_cohort_id}, format: :json}
            expect(response.status).to eq(200), response.body # ok
            expect(@cohort_promotion.reload.cohort_id).to eq(another_cohort_id)

        end

    end

end
