require 'spec_helper'
require 'controllers/api/helpers/api_content_controller_spec_helper'

describe Api::GroupsController do

    include ControllerSpecHelper
    json_endpoint

    fixtures(:access_groups)
    let(:valid_attributes) { { "name" => "GROUP" } }

    before(:each) do
        @streams = Lesson::Stream.limit(2)
        stub_client_params(controller)
        stub_current_user(controller, [:admin])
    end

    describe "GET index" do
        it "assigns all groups as @groups" do
            access_group = AccessGroup.find_by_name('GROUP')
            expect(access_group.lesson_stream_locale_packs.count > 0).to be(true)
            get :index, params: {include_streams: true, format: :json}
            expect(body_json["contents"]["groups"].size).to eq(AccessGroup.count)
            entry = body_json["contents"]["groups"].detect { |e| e['name'] == 'GROUP'}

            expect(entry["name"]).to eq(valid_attributes["name"])
        end
    end

    describe "POST create" do
        describe "with valid params" do
            it "creates a new Group" do
                expect {
                    post :create, params: {record: { "name" => "NEW_GROUP" } , format: :json}
                }.to change(AccessGroup, :count).by(1)

                expect(response.status).to eq(200) # ok
                expect(body_json["contents"]["groups"].size).to eq(1)
                expect(body_json["contents"]["groups"][0]["name"]).to eq("NEW_GROUP")

            end

            it "prevents you from creating a new Group with a duplicate group name" do
                expect(AccessGroup.where(name: 'GROUP').count).to eq(1)
                expect {
                    post :create, params: {record: {name: "GROUP"}, format: :json}
                }.to change(AccessGroup, :count).by(0)

                expect(response.status).to eq(406) # not_acceptable
                expect(body_json["message"]).to eq("Validation failed: Name has already been taken")

            end

            it "create with stream_locale_pack_ids" do
                locale_pack = Lesson::Stream::LocalePack.first
                expect {
                    post :create, params: {record: { "name" => "NEW_GROUP", "stream_locale_pack_ids" => [locale_pack.id]} , format: :json}
                }.to change(AccessGroup, :count).by(1)

                expect(response.status).to eq(200) # ok
                expect(body_json["contents"]["groups"][0]["stream_locale_pack_ids"]).to eq([locale_pack.id])
            end

        end
    end

    describe "PUT update" do
        describe "with valid params" do
            it "updates the requested group" do
                group = AccessGroup.first
                new_text = "tagtext #{rand}"
                put :update, params: { "record" => {"name" => new_text, "id" => group.id}, format: :json}
                expect(response.status).to eq(200) # ok
                expect(body_json["contents"]["groups"].size).to eq(1)
                expect(body_json["contents"]["groups"][0]["name"]).to eq(new_text)
                expect(group.reload.name).to eq(new_text)
            end

        end

    end

    describe "DELETE destroy" do
        it "destroys the requested group" do
            group = access_groups(:default_group)
            expect {
                delete :destroy, params: {id: group.id, format: :json}
            }.to change(AccessGroup, :count).by(-1)
            # response.status.should == 204 # no_content
            expect(response.status).to eq(200) # ok
        end

        it "should 406 if attempting to destroy indestructible access group" do
            group = access_groups(:default_group)
            expect(AccessGroup::INDESTRUCTIBLE_ACCESS_GROUPS).to receive(:include?).with(group.name).and_return(true)
            delete :destroy, params: {id: group.id, format: :json}
            assert_error_response(406, {'message' => "Validation failed: #{group.name.capitalize} is indestructible. Refer to engineering for assistance."})
        end
    end

end
