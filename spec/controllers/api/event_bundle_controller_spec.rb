require 'controllers/api/helpers/api_content_controller_spec_helper'

describe Api::EventBundleController do
    include ControllerSpecHelper
    include ApiContentControllerSpecHelper

    before(:each) do
        stub_client_params(controller)
        stub_current_user(controller, [:learner])
    end

    describe "basic event tracking" do

        describe "create" do
            before(:each) do
                Event.destroy_all

                @now = Time.now
                allow_any_instance_of(Api::EventBundleController).to receive(:now).and_return(@now);

                @basic_event_params = {
                    "id" => SecureRandom.uuid,
                    "event_type" => "event",
                    "buffered_time" => 1.5,
                    "client_utc_timestamp" => 12345.5,
                    "page_load_id" => "1",
                    "guid" => "guid1"
                }

                @basic_bundle = {
                    "events" => [@basic_event_params]
                }
            end

            it "should set request_queued_for_seconds when it is set in http_queue" do
                expect(Event).to receive(:create_batch_from_hash!).with(any_args()) { |events, options|
                    expect(options[:request_queued_for_seconds]).to eq(42)
                }

                post :create, params: {
                    'record' => @basic_bundle,
                    'http_queue' => {
                        'queued_for_seconds' => 42
                    },
                    :format => :json}
            end

            it "should set request_queued_for_seconds when it is not set in http_queue" do
                expect(Event).to receive(:create_batch_from_hash!).with(any_args()) { |events, options|
                    expect(options[:request_queued_for_seconds]).to eq(0)
                }

                post :create, params: {
                    'record' => @basic_bundle,
                    :format => :json}
            end

            it "should work with an anonymous user id" do
                stub_no_current_user(controller)

                now = @now

                record = {
                    "events" => [@basic_event_params.merge({
                        "event_type" => "lesson:play",
                        "server_timestamp" => 12345
                    })]
                }

                request.headers['auid'] = SecureRandom.uuid

                post :create, params: { 'record' => record, :format => :json }
                assert_200

                expect(Event.count).to eq(1)
                event = Event.first
            end

            it "should fail with no current user and no anonymous user id" do
                stub_no_current_user(controller)

                now = @now

                record = {
                    "events" => [@basic_event_params.merge({
                        "event_type" => "lesson:play"
                    })]
                }

                post :create, params: { 'record' => record, :format => :json }
                expect(response.code).to eq("401")
            end


        end

    end

end
