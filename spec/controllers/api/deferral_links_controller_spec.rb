require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::DeferralLinksController do
    include ControllerSpecHelper
    json_endpoint

    fixtures(:users)

    before(:each) do
        stub_client_params(controller)
    end

    describe "GET show" do
        attr_accessor :learner, :active_deferral_link, :all_published_mba_cohorts

        before(:each) do
            @learner = users(:learner)
            @all_published_mba_cohorts = Cohort.all_published.reorder(:start_date).where(program_type: 'mba')

            expect(all_published_mba_cohorts.size).to be >= 3 # sanity check

            all_published_mba_cohorts.first.update!(start_date: Time.now - 1.weeks)
            all_published_mba_cohorts.first.publish!

            # Ensure at least one is far enough in the future
            all_published_mba_cohorts.second.update!(start_date: Time.now + 1.months)
            all_published_mba_cohorts.second.publish!

            CohortApplication.create!({
                cohort_id: all_published_mba_cohorts.first.id,
                status: 'accepted',
                user_id: learner.id,
                applied_at: Time.now
            })
            @active_deferral_link = DeferralLink.create!(
                user: learner, expires_at: Time.now + 30.days, from_cohort_application_id: learner.accepted_application.id
            )
        end

        it "should return deferral_link with upcoming cohorts to defer to" do
            stub_current_user(controller, learner)
            get :show, params: { format: :json, id: active_deferral_link.id }
            assert_200
            expect(body_json["contents"]["deferral_links"].count).to eq(1)
            expect(body_json["contents"]["deferral_links"][0]).to eq(active_deferral_link.as_json(fields: ['active', 'from_cohort_application']))
            expect(body_json["meta"]["upcoming_cohorts"].count).to eq(1)
        end
    end

    describe "POST create" do
        it "should create a deferral link for a user" do
            stub_current_user(controller, [:admin])
            learner = users(:accepted_emba_cohort_user)
            post :create, params: {record: {user_id: learner.id, from_cohort_application_id: learner.accepted_application.id}, format: :json}
            assert_200
            expect(body_json["contents"]["deferral_links"].count).to eq(1)
            expect(body_json["contents"]["deferral_links"][0]).to eq(DeferralLink.where(user_id: learner.id).first.as_json)
        end

        it "should 401 if not an admin" do
            learner = users(:accepted_emba_cohort_user)
            stub_current_user(controller, learner)
            post :create, params: {record: {user_id: learner.id, from_cohort_application_id: learner.accepted_application.id}, format: :json}
            assert_401
        end
    end

    describe "PUT update" do

        it "should 401 if admin cannot update_deferral_link" do
            admin = users(:admin)
            stub_current_user(controller, admin)
            cohort_application = CohortApplication.where.not(user_id: admin.id).first
            user = cohort_application.user
            active_deferral_link = DeferralLink.create!(user: user, expires_at: Time.now + 30.days, from_cohort_application: cohort_application)
            expect(controller.current_ability).to receive(:cannot?).with(:update_deferral_link, anything, anything).and_return(true)
            put :update, params: {
                record: {
                    id: active_deferral_link.id,
                    user_id: active_deferral_link.user_id
                },
                meta: {
                    # For the purposes of this spec, we don't care about the `to_cohort_id`
                    # value as we should be returning a 401 before it even becomes relevant,
                    # so we just mock it to some random id since it's a required param.
                    to_cohort_id: SecureRandom.uuid
                },
                format: :json
            }
            assert_401
        end

        it "should use a deferral link for a user" do
            learner = users(:learner)
            all_published_mba_cohorts = Cohort.all_published.reorder(:start_date).where(program_type: 'mba')
            from_cohort_application = CohortApplication.create!({
                cohort_id: all_published_mba_cohorts.first.id,
                status: 'accepted',
                user_id: learner.id,
                applied_at: Time.now
            })
            active_deferral_link = DeferralLink.create!(user: learner, expires_at: Time.now + 30.days, from_cohort_application: from_cohort_application)
            expect(all_published_mba_cohorts.size).to be > 1 # sanity check

            expect(controller.current_ability).to receive(:cannot?).with(:update_deferral_link, anything, anything).and_return(false)
            stub_current_user(controller, learner)
            expect_any_instance_of(User).to receive(:defer_into_cohort).and_call_original

            put :update, params: {
                record: {
                    id: active_deferral_link.id,
                    user_id: active_deferral_link.user_id
                },
                meta: {
                    to_cohort_id: all_published_mba_cohorts.second.id
                },
                format: :json
            }

            assert_200
            from_cohort_application.reload
            active_deferral_link.reload
            learner.reload
            expect(from_cohort_application.status).to eq('deferred')
            expect(active_deferral_link.used).to eq(true)
            expect(learner.pre_accepted_application.cohort_id).to eq(all_published_mba_cohorts.second.id)
            expect(learner.pre_accepted_application.status).to eq('pre_accepted')
            expect(body_json['meta']['to_cohort_application']['id']).to eq(learner.pre_accepted_application.id)
        end
    end
end
