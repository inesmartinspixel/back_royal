require 'controllers/api/helpers/api_content_controller_spec_helper'

describe Api::AutoSuggestOptionsController do
    include ApiContentControllerSpecHelper

    describe "index" do

        before(:each) do
            stub_current_user("learner")
        end

        it "should return instances" do
            expect_any_instance_of(AutoSuggestOption::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                expect(to_json_instance.filters[:type]).to eq("skills")
                expect(to_json_instance.filters[:search_text]).to eq("test")
                "[]"
            end
            get :index, params: { :filters => { :search_text => 'test', :type => 'skills', :suggested => true }, :format => :json }
            assert_successful_response({
                "skills_options" => []
            })
        end

    end

    describe "POST create" do
        it "should work" do
            stub_current_user(controller, [:admin])

            # Make sure to have a second locale version too
            ProfessionalOrganizationOption.create!({
                "text": "test org",
                "locale": "en"
            })

            post :create, params: {
                :format => :json,
                :record => {
                    :type => "professional_organization",
                    :text => "test org",
                    :locale => "zh",
                    :suggest => true
                }
            }
            assert_200
            expect(body_json["contents"]["professional_organization_options"].first["text"]).to eq("test org")
            expect(body_json["contents"]["professional_organization_options"].first["locale"]).to eq("zh")
            expect(body_json["contents"]["professional_organization_options"].first["suggest"]).to eq(true)
        end
    end

    describe "PUT update" do

        it "should update an auto suggest option" do
            stub_current_user(controller, [:admin])
            organization_option = ProfessionalOrganizationOption.create!({
                "text": "test org",
                "locale": "en"
            })

            # Make sure to have a second locale version too
            ProfessionalOrganizationOption.create!({
                "text": "test org",
                "locale": "es"
            })

            put :update, params: {
                :format => :json,
                :record => {
                    :id => organization_option.id,
                    :type => 'professional_organization',
                    :text => organization_option.text,
                    :locale => organization_option.locale,
                    :suggest => false
                }
            }
            assert_200
            reloaded = ProfessionalOrganizationOption.find_by_id(organization_option.id)
            expect(reloaded.suggest).to eq(false)
        end
    end

    describe "POST upload_icon" do
        it "should create an icon asset, update the organization image_url property, and return the asset" do
            stub_current_user(controller, [:admin])
            file = fixture_file_upload('files/test.png', 'image/png')
            organization = ProfessionalOrganizationOption.first
            original_image_url = organization.image_url

            post :upload_icon, params: {
                :format => :json,
                :organization_icon => file,
                :record => {:type => "professional_organization", :id => organization.id}
            }

            assert_200
            reloaded = ProfessionalOrganizationOption.find_by_id(organization.id)
            expect(reloaded.image_url).not_to eq(original_image_url)
            expect(body_json['contents']['organization_icon']['file_file_name']).to eq('test.png')
        end

        it "should allow upload for a not yet existing organization record" do
            stub_current_user(controller, [:admin])
            file = fixture_file_upload('files/test.png', 'image/png')

            post :upload_icon, params: {
                :format => :json,
                :organization_icon => file,
                :record => {:type => "professional_organization"}
            }

            assert_200
            expect(body_json['contents']['organization_icon']['file_file_name']).to eq('test.png')
        end
    end

end