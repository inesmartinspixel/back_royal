require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::S3TranscriptAssetsController do
    include ControllerSpecHelper

    fixtures(:users, :education_experiences)

    before(:each) do
        stub_client_params(controller)
        @file = fixture_file_upload('files/test.pdf', 'application/pdf')
    end

    describe "POST create" do
        it "should 401 and rollback if not admin" do
            experience = CareerProfile::EducationExperience.first
            allow(controller).to receive(:current_user).and_return(experience.career_profile.user)
            expect(Proc.new {
                post :create, params: {
                    :format => :json,
                    :record => {
                        file: @file
                    }
                }
            }).not_to change { S3TranscriptAsset.count }
            assert_401
        end

        describe "as admin" do
            attr_accessor :user, :another_user

            before(:each) do
                @user = users(:admin)
                @another_user = User.joins(:career_profile).where.not(:id => user.id).first

                another_user.s3_transcript_assets.destroy_all
                another_user.save!
            end

            it "should error when not providing an education_experience_id" do
                allow(controller).to receive(:current_user).and_return(user)

                expect {
                    post :create, params: {
                        :format => :json,
                        :record => {
                            file: @file
                        }
                    }
                }.to raise_error('Must provide an existing education experience')
            end

            it "should error when providing a non-existant education_experience_id" do
                allow(controller).to receive(:current_user).and_return(user)

                expect {
                    post :create, params: {
                        :format => :json,
                        :record => {
                            file: @file,
                            education_experience_id: SecureRandom.uuid
                        }
                    }
                }.to raise_error('Must provide an existing education experience')
            end

            it "should work when admin is creating for an education experience" do
                allow(controller).to receive(:current_user).and_return(user)

                education_experience = education_experiences(:education_experience_with_no_transcripts)

                post :create, params: {
                    :format => :json,
                    :record => {
                        file: @file,
                        education_experience_id: education_experience.id
                    }
                }
                assert_200

                education_experience.reload
                expect(education_experience.transcripts.size).to be(1)
                expect(education_experience.transcripts.first.file_file_name).to eq('test.pdf')
                expect(education_experience.transcripts.first.file_content_type).to eq('application/pdf')
            end

            it "should unset transcript_waiver on the associated education_experience" do
                allow(controller).to receive(:current_user).and_return(user)

                education_experience = CareerProfile::EducationExperience.first
                education_experience.update_columns(transcript_waiver: 'test waiver')

                post :create, params: {
                    :format => :json,
                    :record => {
                        file: @file,
                        education_experience_id: education_experience.id
                    }
                }
                assert_200

                education_experience.reload
                expect(education_experience.transcript_waiver).to be_nil
            end
        end

        it "should record a timeline event for the enrollment action" do
            admin = users(:admin)

            education_experience = education_experiences(:education_experience_with_two_transcripts)
            education_experience.educational_organization.update_columns(text: 'Foo State')

            allow(controller).to receive(:current_user).and_return(admin)
            expect(controller).to receive(:create_upload_timeline_event).with(an_instance_of(S3TranscriptAsset), "Foo State transcript", admin)
            post :create, params: {
                format: :json,
                record: {
                    file: @file,
                    education_experience_id: education_experience.id
                }
            }
        end
    end

    describe "DELETE destroy" do
        it "should 401 if not admin" do
            education_experience = education_experiences(:education_experience_with_two_transcripts)
            allow(controller).to receive(:current_user).and_return(education_experience.career_profile.user)
            delete :destroy, params: { format: :json, id: education_experience.transcripts.first.id }
            assert_401
        end

        it "should record a timeline event for the enrollment action" do
            education_experience = education_experiences(:education_experience_with_two_transcripts)
            education_experience.educational_organization.update_columns(text: 'Foo State')
            admin = users(:admin)
            allow(controller).to receive(:current_user).and_return(admin)
            expect(controller).to receive(:create_deletion_timeline_event).with(an_instance_of(S3TranscriptAsset), 'Foo State transcript', admin)
            delete :destroy, params: { :format => :json, :id => education_experience.transcripts.first.id }
        end

        it "should 406 if deleting a transcript for an approved education experience" do
            education_experience = education_experiences(:education_experience_with_two_transcripts)
            education_experience.update_columns(transcript_approved: true)
            admin = users(:admin)
            allow(controller).to receive(:current_user).and_return(admin)
            delete :destroy, params: { :format => :json, :id => education_experience.transcripts.first.id }
            assert_406
            expect(body_json["message"]).to eq("Can't delete transcript if approved")
        end
    end

    describe "GET show_file" do
        it "should call send_document" do
            admin_user = users(:admin)
            allow(controller).to receive(:current_user).and_return(admin_user)
            expect(controller).to receive(:send_document).with(S3TranscriptAsset, 'private_user_documents:transcript_accessed') do
                controller.head :ok
            end

            get :show_file, params: { :id => S3TranscriptAsset.first.id }
            assert_200
        end
    end
end