require 'spec_helper'
require 'controllers/api/helpers/api_content_controller_spec_helper'

describe Api::EntityMetadataController do

    include ControllerSpecHelper
    json_endpoint

    let(:valid_attributes) { { "title" => "seo title",
        "description" => "seo description",
        "canonical_url" => "some seo friendly url" } }

    before(:each) do
        stub_client_params(controller)
        stub_current_user(controller, [:admin])
        @lesson = Lesson.first
        @entity_metadata = EntityMetadata.create! valid_attributes
        @lesson.entity_metadata = @entity_metadata
        @lesson.save!
    end

    describe "PUT update" do
        describe "with valid params" do
            it "updates the requested lesson seo metadata" do
                new_title = "new seo title"
                new_tweet_template = "new tweet template"
                put :update, params: {id: @entity_metadata.to_param, record: @lesson.entity_metadata.as_json.merge({ "title" => new_title, "tweet_template" =>  new_tweet_template}), format: :json}
                expect(response.status).to eq(200) # ok
                expect(body_json["contents"]["entity_metadata"].size).to eq(1)
                expect(body_json["contents"]["entity_metadata"][0]["title"]).to eq(new_title)
                expect(body_json["contents"]["entity_metadata"][0]["tweet_template"]).to eq(new_tweet_template)
                @lesson.reload
                expect(@lesson.entity_metadata.title).to eq(new_title)

            end

            it "updates the associated image" do
                image = S3Asset.first
                new_title = "new seo title"
                put :update, params: {id: @entity_metadata.to_param, record: @lesson.entity_metadata.as_json.merge({ "image" => {"id" => image.id} }), format: :json}
                expect(response.status).to eq(200) # ok

                @lesson.entity_metadata.reload
                expect(@lesson.entity_metadata.image_id).to eq(image.id)

            end

            it "can set the associated image to nil" do
                image = S3Asset.first
                @lesson.entity_metadata.image = image
                @lesson.entity_metadata.save!

                update_params = @lesson.entity_metadata.as_json
                update_params.delete("image")
                put :update, params: {id: @entity_metadata.to_param, record: update_params, format: :json}
                expect(response.status).to eq(200) # ok
                expect(body_json["contents"]["entity_metadata"].size).to eq(1)

                @lesson.entity_metadata.reload
                expect(@lesson.entity_metadata.image).to be(nil)

            end

        end

    end


end
