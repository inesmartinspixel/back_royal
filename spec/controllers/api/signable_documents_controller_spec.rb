require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::SignableDocumentsController do
    include ControllerSpecHelper

    describe "GET show_file" do

        it "should call send_document" do
            admin_user = users(:admin)
            allow(controller).to receive(:current_user).and_return(admin_user)
            expect(controller).to receive(:send_document).with(SignableDocument, 'private_user_documents:signable_document_accessed') do
                controller.head :ok
            end

            get :show_file, params: { :id => SecureRandom.uuid }
            assert_200
        end

    end

    describe "POST create" do
        attr_reader :learner, :admin, :file

        before(:each) do
            @file = fixture_file_upload('files/test.pdf', 'application/pdf')
            @admin = users(:admin)
            @learner = User.joins(:roles).where("roles.name = 'learner'").left_outer_joins(:signable_documents).where("signable_documents.id is null").first
        end

        it "should 401 if not admin" do
            allow(controller).to receive(:current_user).and_return(learner)
            expect(SignableDocument).not_to receive(:create!)
            post :create, params: {
                :format => :json,
                :record => {}
            }
            assert_401
        end

        it "should work for admin" do
            post_create
            signable_document = learner.signable_documents.first
            expect(signable_document.file_file_name).to eq('test.pdf')
            expect(signable_document.file_content_type).to eq('application/pdf')
            expect(signable_document.document_type).to eq(SignableDocument::TYPE_ENROLLMENT_AGREEMENT)
            expect(signable_document.signed_at).to be_within(1.second).of(Time.now)
            expect(signable_document.metadata).to eq({'some' => 'metadata'})
        end

        describe "with existing signable_documents" do

            attr_accessor :doc

            before(:each) do
                @doc = SignableDocument.create!({
                    user_id: learner.id,
                    document_type: SignableDocument::TYPE_ENROLLMENT_AGREEMENT,
                    metadata: {
                        'program_type' => 'emba'
                    }
                })
            end

            it "should delete any unsigned enrollment agreements for user_id and program_type before uploading new agreement" do
                post_create({
                    'program_type' => 'emba'
                })
                expect(SignableDocument.find_by_id(doc.id)).to be(nil)
                expect(learner.reload.signable_documents.first).not_to be(nil)
            end

            it "should error if there is a signed enrollment agreement for user_id and program_type" do
                doc.update_column(:signed_at, Time.now)

                allow(controller).to receive(:current_user).and_return(admin)
                post :create, params: {
                    :format => :json,
                    :record => {
                        file: file,
                        user_id: learner.id,
                        document_type: SignableDocument::TYPE_ENROLLMENT_AGREEMENT,
                        metadata: {
                            'program_type' => 'emba'
                        }
                    }
                }
                assert_406
            end

        end

        it "should record a timeline event for the enrollment action" do
            expect(controller).to receive(:create_upload_timeline_event).with(an_instance_of(SignableDocument), SignableDocument::TYPE_ENROLLMENT_AGREEMENT, admin)
            post_create
        end

        def post_create(meta = {})
            allow(controller).to receive(:current_user).and_return(@admin)

            post :create, params: {
                :format => :json,
                :record => {
                    file: file,
                    user_id: learner.id,
                    document_type: SignableDocument::TYPE_ENROLLMENT_AGREEMENT,
                    metadata: meta.merge({'some' => 'metadata'})
                }
            }
            assert_200
        end

    end
end