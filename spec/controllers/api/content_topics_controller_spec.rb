require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::ContentTopicsController do
    include ControllerSpecHelper
    json_endpoint

    before(:each) do
        stub_client_params(controller)
    end

    describe "GET 'index'" do

        it "should render all records as editor" do
            expect(ContentTopic.all.any?).to be(true)
            stub_current_user(controller, [:editor])
            get :index, params: { format: :json }
            expect(body_json['contents']['content_topics']).to eq(ContentTopic.includes(:en_lesson_streams).reorder(Arel.sql("locales->>'en'")).as_json)
        end

        it "should 401 as learner" do
            stub_current_user(controller, [:learner])
            get :index, params: { format: :json }
            assert_error_response(401)
        end

    end

    describe "POST create" do
        it "should 401 as learner" do
            stub_current_user(controller, [:learner])
            post :create, params: { format: :json }
            assert_error_response(401)
        end
    end

    describe "PUT update" do
        it "should 401 as learner" do
            stub_current_user(controller, [:learner])
            put :update, params: { format: :json }
            assert_error_response(401)
        end
    end

    describe "DELETE destroy" do
        it "should 401 as learner" do
            stub_current_user(controller, [:learner])
            delete :destroy, params: { format: :json }
            assert_error_response(401)
        end
    end
end
