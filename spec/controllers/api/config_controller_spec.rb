require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::ConfigController do
    include ControllerSpecHelper
    json_endpoint

    describe "GET 'index'" do

        it "should get config if signed in" do
            stub_current_user(controller, [:learner], ["GROUP"])

            get :index, params: { :format => :json }
            expect(response).to be_successful
            expect(response.body).to include("segmentio_key")
        end

        it "should get config if not signed in" do
            get :index, params: { :format => :json }
            expect(response).to be_successful
            expect(response.body).to include("segmentio_key")
        end

    end

end
