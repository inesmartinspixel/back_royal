require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'
require 'stripe_helper'

describe Api::CouponsController do

	include ControllerSpecHelper
	include StripeHelper
    json_endpoint

	before(:each) do
        stub_client_params(controller)
		@user = controller.instance_variable_set(:@current_user, users(:user_with_subscriptions_enabled))
        @default_coupon = default_coupon
	end

	describe "GET show" do

        it "should render a Stripe coupon" do
            allow_any_instance_of(Cohort).to receive(:coupon_prefix) { @default_coupon.id[0..2] }
            get :show, params: {format: :json, id: @default_coupon.id, filters: {cohort_id: Cohort.first.id}}
            assert_200
            expect(body_json["contents"]["coupons"].length).to eq(1)
            expect(body_json["contents"]["coupons"][0]["id"]).to eq(@default_coupon.id)
            expect(body_json["contents"]["coupons"][0]["amount_off"]).to eq(@default_coupon.amount_off)
        end

        it "should render an empty collection if failing to provide a valid cohort_id" do
            get :show, params: {format: :json, id: @default_coupon.id, filters: {cohort_id: 'invalid'}}
            assert_200
            expect(body_json["contents"]["coupons"]).to match_array([])
        end


        it "should render an empty collection if failing to provide an acceptable prefix" do
            cohort = Cohort.first
            allow(cohort.program_type_config).to receive(:coupon_prefix) { 'NOPE-NOPE-NOPE' }
            get :show, params: {format: :json, id: @default_coupon.id, filters: {cohort_id: cohort.id}}
            assert_200
            expect(body_json["contents"]["coupons"]).to match_array([])
        end

	end


end
