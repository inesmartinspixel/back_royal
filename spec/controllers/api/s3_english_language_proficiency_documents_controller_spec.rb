require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::S3EnglishLanguageProficiencyDocumentsController do
    include ControllerSpecHelper

    fixtures(:users)

    before(:each) do
        stub_client_params(controller)
        @file = fixture_file_upload('files/test.pdf', 'application/pdf')
    end

    describe "POST create" do

        it "should 401 and rollback if creating for another user" do
            user = users(:learner)
            another_user = get_user_with_english_language_proficiency_document(User.where.not(:id => user.id).first)

            original_document_ids = another_user.s3_english_language_proficiency_document_ids

            allow(controller).to receive(:current_user).and_return(user)

            expect(Proc.new {
                post :create, params: {
                    :format => :json,
                    :record => {
                        file: @file,
                        user_id: another_user.id
                    }
                }
            }).not_to change { S3EnglishLanguageProficiencyDocument.count }
            assert_401

            another_user.reload
            expect(another_user.s3_english_language_proficiency_document_ids).to eq(original_document_ids)
        end

        it "should work for admin" do
            user = users(:admin)
            another_user = User.where.not(:id => user.id).first
            another_user.s3_english_language_proficiency_documents.destroy_all
            another_user.save!

            allow(controller).to receive(:current_user).and_return(user)

            post :create, params: {
                :format => :json,
                :record => {
                    file: @file,
                    user_id: another_user.id
                }
            }
            assert_200

            another_user.reload
            expect(another_user.s3_english_language_proficiency_documents.size).not_to be(0)
            expect(another_user.s3_english_language_proficiency_documents.first.user_id).to eq(another_user.id)
            expect(another_user.s3_english_language_proficiency_documents.first.file_file_name).to eq('test.pdf')
            expect(another_user.s3_english_language_proficiency_documents.first.file_content_type).to eq('application/pdf')
        end

        it "should work if creating for themself" do
            user = users(:learner)
            user.s3_english_language_proficiency_documents.destroy_all
            user.save!

            allow(controller).to receive(:current_user).and_return(user)

            post :create, params: {
                :format => :json,
                :record => {
                    file: @file,
                    user_id: user.id
                }
            }
            assert_200

            user.reload
            expect(user.s3_english_language_proficiency_documents.size).not_to be(0)
            expect(user.s3_english_language_proficiency_documents.first.user_id).to eq(user.id)
            expect(user.s3_english_language_proficiency_documents.first.file_file_name).to eq('test.pdf')
            expect(user.s3_english_language_proficiency_documents.first.file_content_type).to eq('application/pdf')
        end

        it "should record a timeline event for the enrollment action" do
            learner = users(:learner)
            admin = users(:admin)
            allow(controller).to receive(:current_user).and_return(admin)
            expect(controller).to receive(:create_upload_timeline_event).with(an_instance_of(S3EnglishLanguageProficiencyDocument), 'english_proficiency', admin)
            post :create, params: {
                :format => :json,
                :record => {
                    file: @file,
                    user_id: learner.id
                }
            }
        end
    end

    describe "DELETE destroy" do
        it "should work if a user is deleting their own english language proficiency document" do
            user = get_user_with_english_language_proficiency_document(users(:learner))
            document_id = user.s3_english_language_proficiency_documents.first.id

            allow(controller).to receive(:current_user).and_return(user)

            delete :destroy, params: { :format => :json, :id => document_id }
            assert_200

            user.reload
            expect(user.s3_english_language_proficiency_documents.first).to be(nil)
            expect(S3EnglishLanguageProficiencyDocument.exists?(document_id)).to be(false)
        end

        it "should not work if a user is deleting another user's english language proficiency document" do
            malicious_user = users(:learner)
            poor_innocent_user = get_user_with_english_language_proficiency_document(User.where.not(id: malicious_user.id).first)

            allow(controller).to receive(:current_user).and_return(malicious_user)

            delete :destroy, params: { :format => :json, :id => poor_innocent_user.s3_english_language_proficiency_documents.first.id }
            assert_401
        end

        it "should record a timeline event for the enrollment action" do
            user = get_user_with_english_language_proficiency_document(users(:learner))
            admin = users(:admin)
            document_id = user.s3_english_language_proficiency_documents.first.id
            allow(controller).to receive(:current_user).and_return(admin)
            expect(controller).to receive(:create_deletion_timeline_event).with(an_instance_of(S3EnglishLanguageProficiencyDocument), 'english_proficiency', admin)
            delete :destroy, params: { :format => :json, :id => document_id }
        end
    end

    describe "GET show_file" do

        it "should call send_document" do
            admin_user = users(:admin)
            allow(controller).to receive(:current_user).and_return(admin_user)
            expect(controller).to receive(:send_document).with(S3EnglishLanguageProficiencyDocument, 'private_user_documents:english_language_proficiency_document_accessed') do
                controller.head :ok
            end

            regular_user = get_user_with_english_language_proficiency_document(users(:learner))

            get :show_file, params: { :id => regular_user.s3_english_language_proficiency_documents.first.id }
            assert_200
        end
    end

    def get_user_with_english_language_proficiency_document(user)
        english_language_proficiency_document = S3EnglishLanguageProficiencyDocument.create!({
            :file => @file,
            :user_id => user.id
        })
        user.s3_english_language_proficiency_documents = [english_language_proficiency_document]
        user.save!

        user
    end
end