require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::StudentNetworkEventsController do
    include ControllerSpecHelper
    json_endpoint

    fixtures(:users)

    before(:each) do
        stub_client_params(controller)
        stub_current_user
    end

    describe "GET index" do

        it "should 401 if cannot index_student_network_events" do
            expect(controller.current_ability).to receive(:can?).with(:index_student_network_events, anything).and_return(false)
            get :index, params: { :format => :json}
            assert_error_response(401)
        end

        it "should return student_network_events" do
            expect(controller.current_ability).to receive(:can?).with(:index_student_network_events, anything).and_return(true)
            get :index, params: { :format => :json}
            assert_200
            event_ids = StudentNetworkEvent.where("published = TRUE and end_time >= NOW()").pluck('id') # By default, the mixin will filter out non-published and past events not still occurring
            expect(body_json['contents']['student_network_events'].map { |e| e['id']}).to match_array(event_ids)
        end

        it "should support a limit and offset" do
            expect(controller.current_ability).to receive(:can?).with(:index_student_network_events, anything).and_return(true)
            get :index, params: {
                :limit => 1,
                :offset => 1,
                :format => :json
            }
            assert_200
            event_ids = StudentNetworkEvent.where("published = TRUE and end_time >= NOW()").pluck('id') # By default, the mixin will filter out non-published and past events not still occurring
            expect(body_json['contents']['student_network_events'].map { |e| e['id']}).to match_array(event_ids.slice(1, 1))
        end

        describe 'eager_loading' do

            it 'should eager load when loading admin fields' do
                # performance
                controller.params['fields'] = ['ADMIN_FIELDS']

                # see should take advantage of active record preloading to optimize data loading in conversations_controller_spec for reference
                events = controller.preload_events_for_index

                # Once the records are preloaded, there should be no more queries required to convert them to json.
                expect(ActiveRecord::Base.connection).not_to receive(:exec_query).and_call_original

                json = events.as_json(fields: ['ADMIN_FIELDS'])

                # sanity checks. make sure that we actually generated a full json tree
                expect(event = json[0]).not_to be_nil
                expect(event['author_name']).not_to be_nil
                json
            end

        end

        describe 'sort' do

            before(:each) do
                author = users(:admin)
                # trying to get some of these in an order that will allow for testing sorting
                [1,3,2,7,5,6].each do |i|
                    StudentNetworkEvent.create!({
                        id: SecureRandom.uuid,
                        title: "Some event #{SecureRandom.uuid}",
                        description: 'super-fun thing',
                        event_type: 'book_club',
                        start_time: Time.now + 3.months + i.days,
                        end_time:Time.now + 3.months + i.days + 2.hours,
                        author_id: author.id,
                        published: true,
                        published_at: Time.now
                    })
                end
            end

            it "should sort by title" do
                assert_sort('title', 'asc')
            end

            it "should sort by start_time" do
                assert_sort('start_time', 'asc')
            end

            it "should sort descending" do
                assert_sort('title', 'desc')
            end

            def assert_sort(sort, direction)
                expect(controller.current_ability).to receive(:can?).with(:index_student_network_events, anything).and_return(true)
                get :index, params: {
                    :sort => [sort],
                    :direction => direction,
                    :format => :json
                }
                assert_200
                event_ids = StudentNetworkEvent.where("published = TRUE and end_time >= NOW()").order("#{sort} #{direction}").pluck('id') # By default, the mixin will filter out non-published and past events not still occurring
                expect(body_json['contents']['student_network_events'].map { |e| e['id']}).to eq(event_ids)

            end

        end

    end

    describe "POST create" do

        attr_accessor :post_params, :uuid

        before(:each) do
            self.post_params = {
                title: 'An Event',
                description: 'A super cool event that you should totally attend',
                event_type: 'meetup',
                start_time: (Time.now + 5.hours).to_timestamp,
                end_time: (Time.now + 7.hours).to_timestamp
            }
        end

        it "should 401 if cannot manage_student_network_event" do
            expect(controller.current_ability).to receive(:can?).with(:manage, StudentNetworkEvent).and_return(false)
            post :create,
                params: {
                    :format => :json,
                    :record => post_params
                }
            assert_error_response(401)
        end

        it "should call StudentNetworkEvent#create_from_hash! with expected values" do
            expect(controller.current_ability).to receive(:can?).with(:manage, StudentNetworkEvent).and_return(true)
            # NOTE: we exclude the timestamps here because of weirdness with `hash_including`
            expect(StudentNetworkEvent).to receive(:create_from_hash!).with(hash_including(post_params.except!(:start_time, :end_time)))
            post :create,
                params: {
                    :format => :json,
                    :record => post_params
                }
        end

    end

    describe "PUT update" do

        attr_accessor :event, :put_params

        before(:each) do
            self.event = StudentNetworkEvent.first
            self.put_params = {
                id: event.id,
                title: 'A new title',
                description: event.description,
                event_type: event.event_type,
                author_id: event.author_id
            }
        end

        it "should 401 if cannot manage_student_network_event" do
            expect(controller.current_ability).to receive(:can?).with(:manage, StudentNetworkEvent).and_return(false)
            put :update,
                params: {
                    :format => :json,
                    :record => put_params
                }
            assert_error_response(401)
        end

        it "should call StudentNetworkEvent#update_from_hash! with expected values" do
            expect(controller.current_ability).to receive(:can?).with(:manage, StudentNetworkEvent).and_return(true)
            expect(StudentNetworkEvent).to receive(:update_from_hash!).with(hash_including(put_params))
            put :update,
                params: {
                    :format => :json,
                    :record => put_params
                }
        end

    end

    describe "DELETE destroy" do

        attr_accessor :event, :delete_params

        before(:each) do
            self.event = StudentNetworkEvent.first
            self.delete_params = {
                :format => :json,
                id: event.id
            }
        end

        it "should 401 if cannot destroy_student_network_event" do
            expect(controller.current_ability).to receive(:can?).with(:manage, StudentNetworkEvent).and_return(false)
            delete :destroy,
                params: delete_params
            assert_error_response(401)
        end

        it "should delete record" do
            expect(controller.current_ability).to receive(:can?).with(:destroy, StudentNetworkEvent).and_return(true)
            expect(controller.current_ability).to receive(:can?).with(:manage, StudentNetworkEvent).and_return(true)
            delete :destroy,
                params: delete_params
            expect {
                event.reload
            }.to raise_error(ActiveRecord::RecordNotFound)
        end
    end
end