require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::SignNowController do
    include ControllerSpecHelper

    describe "POST hook" do

        describe "with document.complete event" do
            it "should queue a job" do
                document_id = SecureRandom.uuid
                expect(ProcessCompletedSignNowDocumentJob).to receive(:ensure_job).with(document_id)

                post :hook, params: get_sign_now_request_params({
                    document_id: document_id,
                    event: 'document.complete'
                })
                assert_200
            end
        end

        describe "with unexpected event" do
            it "should log to raven" do\
                expect(ProcessCompletedSignNowDocumentJob).not_to receive(:ensure_job)
                expect(Raven).to receive(:capture_exception).with("Unexpected request to SignNowController", anything)

                post :hook, params: get_sign_now_request_params({
                    event: 'unexpected'
                })
                assert_200
            end
        end

    end

    def get_sign_now_request_params(attrs)
        params = {
            "sign_now" => {
                "content" => {
                    "document_id" => attrs[:document_id]
                },
                "meta" => {
                    "event" => attrs[:event]
                }
            }
        }
    end

end