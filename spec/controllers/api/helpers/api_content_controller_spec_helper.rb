require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

module ApiContentControllerSpecHelper
    extend ActiveSupport::Concern

    included do
        include ControllerSpecHelper
        fixtures(:lesson_streams, :playlists, :lessons, :users)
        json_endpoint

        before(:each) do
            stub_current_user(controller, [:learner], ["GROUP"])
            stub_client_params(controller)
            allow(controller).to receive(:set_push_messages) # mock these out so we don't have to account for them in expectations
            # allow(controller).to receive(:client_build_number).and_return(4122) # if we need to test something based on client version
        end
    end

    module ClassMethods


        # attrs should have two entries, one for :valid, and one for :invalid.  Both should be hashes
        def add_create_specs(attrs)
            valid_attrs = attrs[:valid]
            invalid_attrs = attrs[:invalid]

            describe "PUT create" do

                before(:each) do
                    @valid_attrs = valid_attrs.is_a?(Proc) ? valid_attrs.call : valid_attrs
                    @invalid_attrs = invalid_attrs.is_a?(Proc) ? invalid_attrs.call : invalid_attrs
                end

                it "should create an instance" do
                    existing_ids = controller.class.model.pluck('id')
                    orig_count = controller.class.model.count;
                    stub_current_user(controller, [:admin], ["GROUP"])
                    post :create, params: {'record' => @valid_attrs, :format => :json}
                    assert_200
                    new_instance = controller.class.model.where('id NOT IN (?)', existing_ids).first
                    expect(new_instance).not_to be_nil
                    assert_successful_response({
                        controller.class.model.table_name => [new_instance]
                    }, nil, {:include_publishing_context => true})
                    expect(controller.class.model.count).to eq(orig_count + 1)
                end

                it "should duplicate an instance" do
                    stub_current_user(controller, [:admin], ["GROUP"])
                    original, dupe = controller.class.model.limit(2).to_a
                    meta = ActionController::Parameters.new({duplicate_from_id: 1})
                    expect(controller.class.model).to receive(:duplicate!).with(controller.current_user, ActionController::Parameters.new({record: 1}), meta).and_return(dupe)
                    post :create, params: {'record' => {record: 1}, 'meta' => meta, :format => :json}
                    assert_200

                    actual_id = body_json['contents'][controller.collection_key(dupe)][0]['id']
                    expect(actual_id).to eq(dupe.id)
                end

                it "should 406 on a validation error" do
                    orig_count = controller.class.model.count;
                    stub_current_user(controller, [:admin], ["GROUP"])
                    allow_any_instance_of(ActiveRecord::RecordInvalid).to receive(:message) { 'Error' }
                    post :create, params: {'record' => @invalid_attrs, :format => :json}
                    assert_error_response(406, {'message' => 'Error'})
                    expect(controller.class.model.count).to eq(orig_count)
                end

                it "should error if no admin access" do
                    orig_count = controller.class.model.count;
                    stub_current_user(controller, [], ["GROUP"])
                    post :create, params: {'record' => @valid_attrs, :format => :json}
                    assert_error_response(401, {'message' => "You are not authorized to do that."})
                    expect(controller.class.model.count).to eq(orig_count)
                end

                it "should set the current user as the author" do
                    orig_count = controller.class.model.count;
                    stub_current_user(controller, [:admin], ["GROUP"])
                    post :create, params: {'record' => @valid_attrs.merge({
                        'author' => {id: 99, name: "Ignore me"}
                    }), :format => :json}
                    assert_successful_response({
                        controller.class.model.table_name => [controller.class.model.last]
                    }, nil, {:include_publishing_context => true})
                    expect(controller.class.model.last.author).to eq(controller.current_user)
                end
            end
        end

        # updaters should have two entries, one for :valid, and one for :invalid.  Both should be procs, which
        # take an instance and return a hash of parameters that will be passed to the update call
        def add_update_specs(updates)

            describe "POST update" do

                before(:each) do
                    @valid_update = updates[:valid]
                    @invalid_update = updates[:invalid]
                end

                # successful update access for editors and admins
                [:super_editor, :admin].each do |role|

                    # super editors cannot update playlists
                    next if role == :super_editor && self.name == 'RSpec::ExampleGroups::ApiPlaylistsController::POSTUpdate'


                    describe "as #{role}" do
                        it "should update a instance" do
                            stub_current_user(controller, [role], ["GROUP"])
                            instance = assert_update
                            assert_successful_response({
                                controller.class.model.table_name => [instance]
                            }, nil, {:include_publishing_context => true, :format_for_update_response => true, :fields => ['UPDATE_FIELDS']})
                        end
                    end
                end

                # unsuccessful update access for learners
                [:learner].each do |role|
                    describe "as #{role}" do
                        it "should error on update lesson if only #{role} access" do
                            stub_current_user(controller, [role], ["GROUP"])
                            try_to_update
                            assert_error_response(401, {'message' => "You are not authorized to do that."})
                        end
                    end
                end

                # successful publish access for admins
                [:admin].each do |role|
                    describe "as #{role}" do
                        it "should publish an instance" do
                            stub_current_user(controller, [role], ["GROUP"])
                            instance = controller.class.model
                                .where.not(id: controller.class.model.all_published.pluck('id'))
                                .where.not(locale_pack_id: nil)
                                .first
                            updated_instance = assert_update(instance, {}, {'should_publish' => true})
                            updated_instance.reload
                            expect(updated_instance.published_version.updated_at.to_timestamp).to eq(updated_instance.updated_at.to_timestamp)
                            assert_successful_response({
                                controller.class.model.table_name => [updated_instance]
                            }, nil, {:include_publishing_context => true, :format_for_update_response => true, :fields => ['UPDATE_FIELDS']})
                        end

                        it "should unpublish an instance" do
                            stub_current_user(controller, [role], ["GROUP"])

                            # I want to unpublish this instance, so I grab one that is currently unpublished
                            # so that I know I will be allowed to unpublish it again
                            instance = controller.class.model.where({locale: 'en'}).detect { |i| !i.has_published_version? }
                            instance.publish!
                            updated_at = instance.updated_at
                            updated_instance = assert_update(instance, {'updated_at' => updated_at.to_timestamp}, {'should_unpublish' => true})
                            updated_instance.reload
                            expect(updated_instance.published_version).to be(nil)
                            assert_successful_response({
                                controller.class.model.table_name => [updated_instance]
                            }, nil, {:include_publishing_context => true, :format_for_update_response => true, :fields => ['UPDATE_FIELDS']})
                        end

                    end
                end

                # unsuccessful publish access for editors and learners
                [:super_editor, :learner].each do |role|
                    describe "as #{role}" do
                        it "should error on publishing an instance if only #{role} access" do
                            stub_current_user(controller, [role], ["GROUP"])
                            instance = controller.class.model.where.not(id: controller.class.model.all_published.pluck('id')).first
                            updated_at = instance.updated_at
                            updated_instance = try_to_update(instance, {:updated_at => updated_at.to_timestamp}, {:should_publish => true})
                            assert_error_response(401)
                        end

                        it "should error on unpublishing an instance if only #{role} access" do
                            stub_current_user(controller, [role], ["GROUP"])
                            instance = controller.class.model.first
                            instance.publish!
                            updated_at = instance.updated_at
                            updated_instance = try_to_update(instance, {:updated_at => updated_at.to_timestamp}, {:should_unpublish => true})
                            assert_error_response(401)
                        end
                    end
                end

                it "should 406 on a validation error" do
                    instance = controller.class.model.first
                    stub_current_user(controller, [:admin], ["GROUP"])
                    params = @invalid_update.call(instance.as_json({:user_id => controller.current_user.id}))
                    params["updated_at"] = (instance.updated_at - 1.day).to_timestamp # spoof someone updating out from under us
                    put :update, params: {'record' => params, :format => :json}
                    expect(response.status).to eq(406)
                    expect(body_json['message']).to match(/Validation failed/)
                end

                it "should 406 if no updated_at is passed" do
                    instance = controller.class.model.first
                    stub_current_user(controller, [:admin], ["GROUP"])
                    params = instance.as_json(:user_id => controller.current_user.id)
                    params.delete('updated_at')
                    put :update, params: {'record' => params, :format => :json}
                    assert_error_response(406, {
                        'message' => "No updated_at provided"
                    })
                end

                it "should not change the author" do
                    stub_current_user(controller, [:admin], ["GROUP"])
                    current_user = controller.current_user
                    instance = controller.class.model.first
                    author = instance.author
                    expect(author).not_to be(nil)
                    expect(current_user).not_to eq(author) # sanity check
                    params = instance.as_json(:user_id => controller.current_user.id).merge({
                        author: {id: 99, name: "Ignore me"},
                    })
                    put :update, params: {'record' => params, :format => :json}
                    assert_successful_response({
                        controller.class.model.table_name => [controller.class.model.find(instance.id)]
                    }, nil, {:include_publishing_context => true, :format_for_update_response => true, :fields => ['UPDATE_FIELDS']})

                    # author should not have changed
                    expect(instance.reload.author).to eq(author)
                end

                # FIXME: we're currently throwing away the response body and are only testing the updated values
                def assert_update(instance = nil, additional_params = {}, meta = {})
                    instance, params = try_to_update(instance, additional_params, meta)
                    assert_200
                    updated_instance = controller.class.model.find(instance.id)
                    updated_instance
                end

                def try_to_update(instance = nil, additional_params = {}, meta = {})
                    instance = instance || controller.class.model.first
                    expect(instance).not_to be(nil)
                    instance_json = instance.as_json(:user_id => controller.current_user.id)
                    params = @valid_update.call(instance_json)
                    put :update, params: {'record' => params.merge!(additional_params), 'meta' => meta, :format => :json}
                    [instance, params]
                end
            end
        end

        def add_destroy_specs

            describe "DELETE destroy" do

                it "should delete an instance if it finds one" do
                    expect_any_instance_of(controller.class.model).to receive(:has_published_version?).and_return(false)
                    instance = send(controller.class.model.table_name.to_sym, :en_only_item)
                    allow_any_instance_of(Lesson::Stream).to receive(:raise_unless_all_playlists_including_stream_are_valid)
                    stub_current_user(controller, [:admin], ["GROUP"])
                    delete :destroy, params: {:id => instance.id, :format => :json}
                    assert_200
                    # check that it was actually deleted
                    expect { controller.class.model.find(instance.id) }.to raise_error(ActiveRecord::RecordNotFound)
                end

                it "should 404 if instance cannot be found" do
                    stub_current_user(controller, [:admin], ["GROUP"])
                    id = SecureRandom.uuid
                    delete :destroy, params: {:id => id, :format => :json}
                    assert_error_response(404, {'message' => "No #{controller.class.model.table_name.singularize} found for id=\"#{id}\""})
                end

                it "should error if no editor access" do
                    stub_current_user(controller, [], ["GROUP"])
                    instance = controller.class.model.first
                    delete :destroy, params: {:id => instance.id, :format => :json}
                    assert_error_response(401, {'message' => "You are not authorized to do that."})
                    expect expect(controller.class.model.first).to eq(instance)
                end

                it "should error if trying to delete something that is published" do
                    instance = controller.class.model.first
                    instance.publish!
                    stub_current_user(controller, [:admin], ["GROUP"])
                    delete :destroy, params: {:id => instance.id, :format => :json}
                    assert_error_response(406, {'message' => "Validation failed: Cannot delete a published item.  Unpublish first if you wish to delete."})
                end

                it "should 406 on RecordInvalid error" do
                    instance = controller.class.model.first
                    expect_any_instance_of(controller.class.model).to receive(:has_published_version?).and_return(false)
                    error = ActiveRecord::RecordInvalid.new(instance)
                    allow_any_instance_of(controller.class.model).to receive(:destroy).and_raise(error)
                    stub_current_user(controller, [:admin], ["GROUP"])
                    delete :destroy, params: {:id => instance.id, :format => :json}
                    expect(response.status).to eq(406)
                    expect(body_json["message"]).to eq(error.message)
                end
            end
        end

        def add_images_specs

            describe "POST images" do

                it "should create S3Assets and return them" do
                    instance = controller.class.model.first
                    expect(instance).not_to be(nil)
                    stub_current_user(controller, [:super_editor], ["GROUP"])
                    mock_s3_asset = double(:mock_s3_asset)
                    expect(mock_s3_asset).to receive(:as_json).and_return("asset_json")
                    expect(mock_s3_asset).to receive(:save!)
                    expect_any_instance_of(controller.class.model).to receive(:get_image_asset).and_return(mock_s3_asset)
                    post :images, params: {:id => instance.id, 'images' => ['url.png'], :format => :json}
                    assert_successful_response(
                        {'images' => ["asset_json"]}
                    )
                end

            end

        end

    end # end ClassMethods

end