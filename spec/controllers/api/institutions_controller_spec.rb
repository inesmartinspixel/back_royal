require 'spec_helper'
require 'controllers/api/helpers/api_content_controller_spec_helper'

describe Api::InstitutionsController do

    include ControllerSpecHelper
    json_endpoint

    let(:valid_attributes) { { "name" => "Organization 1" } }

    before(:each) do
        stub_client_params(controller)
        stub_current_user(controller, [:admin])
    end

    describe "show" do
        it "should use eager loading when requesting users" do
            # The quantic institution is referenced when retreiving the as_json for each institution record.
            # In the wild the quantic institution is cached, but in our specs, it causes a query to execute,
            # so we preload it here to prevent the extra query from executing and causing this spec to fail.
            Institution.quantic

            institution = controller.send(:preload_users).first

            expect(ActiveRecord::Base.connection).not_to receive(:exec_query).and_call_original
            institution.as_json(fields: ['users'])
        end
    end

    describe "GET index" do
        it "assigns all institutions as @institutions" do
            institutions = Institution.all
            expected_institution_ids = institutions.pluck(:id)
            get :index, params: {format: :json}
            expect(body_json["contents"]["institutions"].size).to eq(institutions.size)
            expect(body_json["contents"]["institutions"].map { |institution| institution["id"] }).to eq(expected_institution_ids)
        end
    end

    describe "POST create" do
        describe "with valid params" do
            it "creates a new Institution" do
                expect {
                    post :create, params: {record: valid_attributes, format: :json}
                }.to change(Institution.all, :count).by(1)

                expect(response.status).to eq(200) # ok
                expect(body_json["contents"]["institutions"].size).to eq(1)
                expect(body_json["contents"]["institutions"][0]["name"]).to eq(valid_attributes["name"])

            end

            it "prevents you from creating a new Institution with a duplicate name" do
                Institution.create!(valid_attributes)
                expect {
                    post :create, params: {record: valid_attributes, format: :json}
                }.to change(Institution.all, :count).by(0)

                expect(response.status).to eq(406) # not_acceptable
                expect(body_json["message"]).to eq("Validation failed: Institution with this name already exists.")

            end

            it "prevents you from creating a new Institution if passed an existing ID" do
                existing_institution = Institution.create!(valid_attributes)

                expect {
                    post :create, params: {record: valid_attributes.merge(id: existing_institution.id), format: :json}
                }.to change(Institution.all, :count).by(0)

                expect(response.status).to eq(406) # not_acceptable
                expect(body_json["message"]).to eq("Validation failed: Institution with this name already exists.")

            end

        end
    end

    describe "PUT update" do
        before :each do
            @group1 = AccessGroup.create!(name: "CATS")
            @group2 = AccessGroup.create!(name: "DOGS")
        end

        describe "with valid params" do
            it "should update the requested institution" do
                now = Time.now
                an_hour_ago = now - 1.hour
                allow(Time).to receive(:now).and_return(an_hour_ago)
                institution = Institution.create! valid_attributes

                allow(Time).to receive(:now).and_return(now)
                new_name = "Organization #{rand}"
                put :update, params: {record: { id: institution.to_param, updated_at: institution.updated_at.to_timestamp, "name" => new_name, "sign_up_code" => "JLL_FREE_TRIAL" }, format: :json}
                expect(response.status).to eq(200) # ok
                expect(body_json["contents"]["institutions"].size).to eq(1)
                expect(body_json["contents"]["institutions"][0]["name"]).to eq(new_name)
                expect(body_json["contents"]["institutions"][0]["updated_at"]).to eq(now.to_timestamp)
                expect(body_json["contents"]["institutions"][0]["sign_up_code"]).to eq("JLL_FREE_TRIAL")

            end

            it "should not let you set a duplicate sign_up_cpde" do
                institution = Institution.create!(name: "JLL", sign_up_code: "JLL_SIGN_UP")
                institution2 = Institution.create!(name: "JLL2")

                put :update, params: {record: { id: institution2.to_param, updated_at: institution2.updated_at.to_timestamp, "sign_up_code" => "JLL_SIGN_UP" }, format: :json}
                expect(response.status).to eq(406) # ok
                expect(body_json["message"]).to eq("Validation failed: Sign up code has already been taken")

            end

            it "should add groups on institution update" do
                now = Time.now
                an_hour_ago = now - 1.hour
                allow(Time).to receive(:now).and_return(an_hour_ago)
                institution = Institution.create! valid_attributes

                allow(Time).to receive(:now).and_return(now)
                new_name = "Organization #{rand}"
                put :update, params: {record: { id: institution.to_param, updated_at: institution.updated_at.to_timestamp, "name" => new_name, "groups" =>  [@group1.as_json]}, format: :json}
                expect(response.status).to eq(200) # ok
                expect(body_json["contents"]["institutions"].size).to eq(1)
                expect(body_json["contents"]["institutions"][0]["name"]).to eq(new_name)
                expect(body_json["contents"]["institutions"][0]["updated_at"]).to eq(now.to_timestamp)
                expect(body_json["contents"]["institutions"][0]["groups"].size).to eq(1)
                expect(body_json["contents"]["institutions"][0]["groups"][0]["name"]).to eq("CATS")

            end

            it "should remove groups on institution update" do
                now = Time.now
                an_hour_ago = now - 1.hour
                allow(Time).to receive(:now).and_return(an_hour_ago)
                institution = Institution.create! valid_attributes.merge("access_groups" =>  [@group1])

                allow(Time).to receive(:now).and_return(now)
                new_name = "Organization #{rand}"
                put :update, params: {record: { id: institution.to_param, updated_at: institution.updated_at.to_timestamp, "name" => new_name, "groups" =>  []}, format: :json}
                expect(response.status).to eq(200) # ok
                expect(body_json["contents"]["institutions"].size).to eq(1)
                expect(body_json["contents"]["institutions"][0]["name"]).to eq(new_name)
                expect(body_json["contents"]["institutions"][0]["updated_at"]).to eq(now.to_timestamp)
                expect(body_json["contents"]["institutions"][0]["groups"].size).to eq(0)
                institution.reload
                expect(institution.access_groups.size).to eq(0)
            end

            it "should set reports_viewers" do
                user = User.joins(:institutions).where("users.id not in (#{User.joins(:views_reports_for_institutions).select(:id).to_sql})").first
                institution = user.institutions.first
                put :update, params: {record: { id: institution.to_param, updated_at: institution.updated_at.to_timestamp, "reports_viewer_ids" =>  [user.id]}, format: :json}
                expect(response.status).to eq(200) # ok
                institution.reload
                expect(institution.reports_viewers).to include(user)
            end
        end

    end

    describe "DELETE destroy" do
        it "destroys the requested institution" do
            institution = Institution.create! valid_attributes
            expect {
                delete :destroy, params: {id: institution.to_param, format: :json}
            }.to change(Institution, :count).by(-1)
            # response.status.should == 204 # no_content
            expect(response.status).to eq(200) # ok
        end

    end

end
