require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::S3IdentificationAssetsController do
    include ControllerSpecHelper

    fixtures(:users)

    before(:each) do
        stub_client_params(controller)
        @file = fixture_file_upload('files/test.pdf', 'application/pdf')
    end

    describe "POST create" do

        it "should 401 and rollback if creating for another user" do
            user = users(:learner)
            another_user = get_user_with_identification(User.where.not(:id => user.id).first)

            original_asset_id = another_user.s3_identification_asset.id

            allow(controller).to receive(:current_user).and_return(user)
            expect(Proc.new {
                post :create, params: {
                    :format => :json,
                    :record => {
                        file: @file,
                        user_id: another_user.id
                    }
                }
            }).not_to change { S3IdentificationAsset.count }
            assert_401

            another_user.reload
            expect(another_user.s3_identification_asset.id).to eq(original_asset_id)
        end

        it "should work for admin" do
            user = users(:admin)
            another_user = User.where.not(:id => user.id).first
            another_user.s3_identification_asset.destroy! if another_user.s3_identification_asset
            another_user.save!

            allow(controller).to receive(:current_user).and_return(user)

            post :create, params: {
                :format => :json,
                :record => {
                    file: @file,
                    user_id: another_user.id
                }
            }
            assert_200

            another_user.reload
            expect(another_user.s3_identification_asset).not_to be(nil)
            expect(another_user.s3_identification_asset.user_id).to eq(another_user.id)
            expect(another_user.s3_identification_asset.file_file_name).to eq('test.pdf')
            expect(another_user.s3_identification_asset.file_content_type).to eq('application/pdf')
        end

        it "should work if creating for themself" do
            user = users(:learner)
            user.s3_identification_asset.destroy! if user.s3_identification_asset
            user.save!

            allow(controller).to receive(:current_user).and_return(user)

            post :create, params: {
                :format => :json,
                :record => {
                    file: @file,
                    user_id: user.id
                }
            }
            assert_200

            user.reload
            expect(user.s3_identification_asset).not_to be(nil)
            expect(user.s3_identification_asset.user_id).to eq(user.id)
            expect(user.s3_identification_asset.file_file_name).to eq('test.pdf')
            expect(user.s3_identification_asset.file_content_type).to eq('application/pdf')
        end

        it "should fallback to current_user.id if user_id param is not present" do
            user = User.first
            user.s3_identification_asset = nil
            user.save!

            allow(controller).to receive(:current_user).and_return(user)

            params = {
                file: @file
            }
            post :create, params: { :format => :json, :record => params }
            assert_200

            user.reload
            expect(user.s3_identification_asset).not_to be(nil)
            expect(user.s3_identification_asset.user_id).to eq(user.id)
            expect(user.s3_identification_asset.file_file_name).to eq('test.pdf')
            expect(user.s3_identification_asset.file_content_type).to eq('application/pdf')
        end

        it "should 409 if identity_verified is already true" do
            user = users(:learner)
            user.s3_identification_asset.destroy! if user.s3_identification_asset
            user.identity_verified = true
            user.save!

            allow(controller).to receive(:current_user).and_return(user)

            post :create, params: {
                :format => :json,
                :record => {
                    file: @file,
                    user_id: user.id
                }
            }
            expect(response.status).to eq(409), "Status: #{response.status}. Body: #{response.body}"
        end

        it "should record a timeline event for the enrollment action" do
            learner = users(:learner)
            admin = users(:admin)
            allow(controller).to receive(:current_user).and_return(admin)
            expect(controller).to receive(:create_upload_timeline_event).with(an_instance_of(S3IdentificationAsset), 'id', admin)

            post :create, params: {
                :format => :json,
                :record => {
                    file: @file,
                    user_id: learner.id
                }
            }
        end
    end

    describe "DELETE destroy" do
        it "should work if a user is deleting their own identification" do
            user = get_user_with_identification(users(:learner))
            identification_id = user.s3_identification_asset.id

            allow(controller).to receive(:current_user).and_return(user)

            delete :destroy, params: { :format => :json, :id => identification_id }
            assert_200

            user.reload
            expect(user.s3_identification_asset).to be(nil)
            expect(S3IdentificationAsset.exists?(identification_id)).to be(false)
        end

        it "should not work if a user is deleting another user's identification" do
            poor_innocent_user = get_user_with_identification(users(:learner))
            malicious_user = users(:editor)

            allow(controller).to receive(:current_user).and_return(malicious_user)

            delete :destroy, params: { :format => :json, :id => poor_innocent_user.s3_identification_asset.id }
            assert_401
        end

        it "should record a timeline event for the enrollment action" do
            user = get_user_with_identification(users(:learner))
            admin = users(:admin)
            identification_id = user.s3_identification_asset.id
            allow(controller).to receive(:current_user).and_return(admin)
            expect(controller).to receive(:create_deletion_timeline_event).with(an_instance_of(S3IdentificationAsset), 'id', admin)
            delete :destroy, params: { :format => :json, :id => identification_id }
        end
    end

    describe "GET show_file" do

        it "should call send_document" do
            admin_user = users(:admin)
            allow(controller).to receive(:current_user).and_return(admin_user)
            expect(controller).to receive(:send_document).with(S3IdentificationAsset, 'private_user_documents:identification_accessed') do
                controller.head :ok
            end

            regular_user = get_user_with_identification(users(:learner))

            get :show_file, params: { :id => regular_user.s3_identification_asset.id }
            assert_200
        end
    end

    def get_user_with_identification(user)
        identification_asset = S3IdentificationAsset.create!({
            :file => @file,
            :user_id => user.id
        })
        user.s3_identification_asset = identification_asset
        user.save!

        user
    end
end