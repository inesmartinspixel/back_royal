require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::CareerProfilesController do
    include ControllerSpecHelper
    json_endpoint

    fixtures(:users, :hiring_relationships)

    before(:each) do
        stub_client_params(controller)
    end

    describe "GET index" do
        attr_accessor :user

        describe "authorize_index_with_params" do

            it "should raise unauthorized error if disallowed" do
                expect(controller).to receive(:render_unauthorized_error)
                expect(controller.current_ability).to receive(:cannot?).with(:index_using_career_profiles_controller, controller.params).and_return(true)
                controller.authorize_index_with_params
            end
        end

        describe "with permission" do

            before(:each) do
                # skip auth checks.  authorize_index_with_params is tested on it's own below,
                # and specific rules are tested in ability_spec.rb
                @has_admin_access = false
                @can_view_full_hiring_manager_experience = true
                allow(controller.current_ability).to receive(:can?) do |meth, object|
                    if meth == :manage
                        @has_admin_access
                    elsif meth == :view_full_hiring_manager_experience
                        @can_view_full_hiring_manager_experience
                    elsif [:index, :index_using_career_profiles_controller].include?(meth)
                        true
                    else
                        raise "unexpected call to can? with meth=#{meth}"
                    end
                end
                @user = User.first
                allow(controller).to receive(:current_user).and_return(@user)
            end

            describe "save search" do
                it "should save search" do
                    filters = {'a' =>  'b'}
                    search = CareerProfile::Search.new(id: SecureRandom.uuid)
                    expect(CareerProfile::Search).to receive(:save_search).with(
                        controller.current_user_id,
                        filters
                    ).and_return(search)
                    process :index, :params => {:format => :json, :view => 'career_profiles', :filters => filters.to_json, :save_search => "true"}

                    expect(body_json['meta']['career_profile_search']).to eq(search.as_json)
                end

                it "should not save search if not instructed" do
                    expect(controller).not_to receive(:save_search)
                    process :index, :params => {:format => :json, :view => 'career_profiles'}
                end

                it "should even save search in ghost mode if save_search is true" do
                    filters = {'a' =>  'b'}
                    search = CareerProfile::Search.new(id: SecureRandom.uuid)
                    expect(CareerProfile::Search).to receive(:save_search).with(
                        controller.current_user_id,
                        filters
                    ).and_return(search)
                    process :index, :params => {:format => :json, :filters => filters.to_json, :view => 'career_profiles', :save_search => "true", :ghost_mode => "true"}

                    expect(body_json['meta']['career_profile_search']).to eq(search.as_json)
                end
            end

            describe "filters" do

                describe "has_candidate_relationship" do

                    it "should not filter out pending candidates (featured)" do
                        hiring_relationship = hiring_relationships(:pending_hidden)
                        allow(controller).to receive(:current_user).and_return(hiring_relationship.hiring_manager)
                        process :index, :params => {:format => :json, :view => 'career_profiles', :filters => {has_candidate_relationship: false}.to_json}
                        assert_200
                        expect(body_json['contents']['career_profiles'].map { |p| p['user_id']}).to include(hiring_relationship.candidate_id)
                    end

                    it "should work when false" do
                        hiring_relationship = hiring_relationships(:accepted_pending)
                        allow(controller).to receive(:current_user).and_return(hiring_relationship.hiring_manager)
                        process :index, :params => {:format => :json, :view => 'career_profiles', :filters => {has_candidate_relationship: false}.to_json}
                        assert_200
                        expect(body_json['contents']['career_profiles'].map { |p| p['user_id']}).not_to include(hiring_relationship.candidate_id)
                    end

                    it "should filter out candidates that have been accepted by team members when false" do
                        team = HiringTeam.create!(domain: 'browns.com', title: 'The Browns')
                        hiring_manager = users(:hiring_manager_with_team)
                        teammate = users(:hiring_manager_teammate)
                        hiring_manager.candidate_relationships.destroy_all
                        hiring_manager.update(hiring_team_id: team.id)
                        teammate.update(hiring_team_id: team.id)
                        teammate.candidate_relationships.destroy_all
                        allow(controller).to receive(:current_user).and_return(hiring_manager)
                        candidate_ids = CareerProfile.limit(3).pluck(:user_id)

                        process :index, :params => {:format => :json, :view => 'career_profiles', :filters => {has_candidate_relationship: false}.to_json}
                        assert_200
                        expect(body_json['contents']['career_profiles'].map { |p| p['user_id']}).to include(*candidate_ids)

                        candidate_ids.each do |candidate_id|
                            HiringRelationship.create!(hiring_manager_id: teammate.id, candidate_id: candidate_id, hiring_manager_status: "accepted")
                        end

                        process :index, :params => {:format => :json, :view => 'career_profiles', :filters => {has_candidate_relationship: false}.to_json}
                        assert_200
                        expect(body_json['contents']['career_profiles'].map { |p| p['user_id']}).not_to include(*candidate_ids)
                    end
                end

                describe "user_id_not" do
                    it "should work" do
                        career_profile = CareerProfile.first
                        process :index, :params => {:format => :json, :view => 'career_profiles', :filters => {user_id_not: [career_profile.user_id]}.to_json}
                        assert_200
                        expect(body_json['contents']['career_profiles'].map { |p| p['user_id']}).not_to include(career_profile.user_id)
                    end
                end

                describe "can_edit_career_profile" do
                    it "should work" do
                        cannot_edit_user = CareerProfile.joins(:user).where("can_edit_career_profile = ?", false).first.user
                        get :index, params: {:format => :json, :view => 'career_profiles', :filters => {:can_edit_career_profile => true}.to_json}
                        assert_200
                        expect(body_json['contents']['career_profiles'].map { |r| r["user_id"] }).not_to include(cannot_edit_user.id)
                    end
                end

                describe "available_for_relationships_with_hiring_manager" do
                    it "should filter out a candidate that is not available for a hiring manager" do
                        hiring_manager = HiringApplication.joins(:user).where("professional_organization_option_id is not null").first.user
                        expect(controller).to receive(:current_user).and_return(hiring_manager).at_least(1).times
                        user = User.joins(:career_profile).first
                        available = User.where(id: user.id)
                        expect(User).to receive(:available_for_relationships)
                            .with(hiring_manager.professional_organization_option_id)
                            .and_return(available)
                        get :index, params: {:format => :json, :view => 'career_profiles', :filters => {:available_for_relationships_with_hiring_manager => hiring_manager.id}.to_json}
                        assert_200
                        expect(body_json['contents']['career_profiles'].map { |r| r["id"] }).to eq([user.career_profile.id])
                    end

                    it "should not filter out a candidate set to do_not_create_relationships" do
                        hiring_manager = HiringApplication.joins(:user).where("professional_organization_option_id is not null").first.user
                        expect(controller).to receive(:current_user).and_return(hiring_manager).at_least(1).times
                        user = User.joins(:career_profile).first
                        user.career_profile.update_attribute(:do_not_create_relationships, true)
                        available_users = User.where("true")
                        expect(User).to receive(:available_for_relationships)
                            .with(hiring_manager.professional_organization_option_id)
                            .and_return(available_users)
                        get :index, params: {
                            :format => :json,
                            :view => 'career_profiles',
                            :filters => {
                                :available_for_relationships_with_hiring_manager => hiring_manager.id
                            }.to_json
                        }
                        assert_200
                        expect(body_json['contents']['career_profiles'].map { |r| r["id"] }).to include(user.career_profile.id)
                    end
                end

                describe "admin_filters" do
                    describe "accepted_or_pre_accepted_in" do
                        it "should work" do
                            career_profile = CareerProfile.joins(:user => :cohort_applications)
                                                .where(:cohort_applications => {status: 'pre_accepted'})
                                                .first
                            application = career_profile.user.last_application
                            cohort_id = application.cohort_id

                            get :index, params: {:format => :json, :view => 'editable', :filters => {:accepted_or_pre_accepted_in => cohort_id}.to_json}
                            assert_200
                            results = (body_json['contents']['career_profiles'] || []).map { |r| r["id"] }
                            expect(results).to include(career_profile.id)

                            application.update(status: 'accepted')
                            get :index, params: {:format => :json, :view => 'editable', :filters => {:accepted_or_pre_accepted_in => cohort_id}.to_json}
                            assert_200
                            results = (body_json['contents']['career_profiles'] || []).map { |r| r["id"] }
                            expect(results).to include(career_profile.id)

                            application.update(status: 'rejected')
                            get :index, params: {:format => :json, :view => 'editable', :filters => {:accepted_or_pre_accepted_in => cohort_id}.to_json}
                            assert_200
                            results = (body_json['contents']['career_profiles'] || []).map { |r| r["id"] }
                            expect(results).not_to include(career_profile.id)
                        end
                    end

                    describe "registered" do
                        it "should work" do
                            cohort_application = CohortApplication.where(status: 'pre_accepted', registered: false).first

                            get :index, params: {:format => :json, :view => 'editable', :filters => {:registered => true}.to_json}
                            assert_200
                            results = (body_json['contents']['career_profiles'] || []).map { |r| r["id"] }
                            expect(results).not_to include(cohort_application.user.career_profile.id)

                            cohort_application.update(registered: true)
                            get :index, params: {:format => :json, :view => 'editable', :filters => {:registered => true}.to_json}
                            assert_200
                            results = (body_json['contents']['career_profiles'] || []).map { |r| r["id"] }
                            expect(results).to include(cohort_application.user.career_profile.id)
                        end
                    end
                end

            end

            describe "sort" do

                describe "PRIORITIZE_HIRING_INDEX" do
                    before(:each) do
                        CareerProfileList.where(:name => 'hiring_index').delete_all
                    end

                    it "should prioritize profile(s) from hiring_index" do
                        career_profile_ids = CareerProfile.joins(:career_profile_search_helper).limit(2).pluck('id')
                        career_profiles_hiring_index = CareerProfileList.first
                        career_profiles_hiring_index.name = "hiring_index"
                        career_profiles_hiring_index.career_profile_ids = career_profile_ids
                        career_profiles_hiring_index.save!

                        # we have this expectation here to ensure that the opposite
                        # expectation is valid in `it "should not prioritize profile(s) from hiring_index" do`
                        expect(controller).to receive(:hiring_index_prioritization).and_call_original

                        get :index, params: {
                            :format => :json,
                            :view => 'career_profiles',
                            :sort => ['PRIORITIZE_HIRING_INDEX', 'BEST_SEARCH_MATCH_FOR_HM']
                        }
                        assert_200
                        career_profiles = body_json['contents']['career_profiles'].map { |r| r["id"] }

                        expect(career_profiles.slice(0, 2)).to match_array(career_profile_ids)
                    end

                    it "should not prioritize profile(s) from hiring_index" do
                        # in `it "should prioritize profile(s) from hiring_index" do` we assert that
                        # this is the method that would do the hiring_index prioritization
                        expect(controller).not_to receive(:hiring_index_prioritization)

                        get :index, params: {
                            :format => :json,
                            :view => 'career_profiles',
                        :sort => ['BEST_SEARCH_MATCH_FOR_HM']
                        }
                        assert_200
                        career_profiles = body_json['contents']['career_profiles'].map { |r| r["id"] }
                    end
                end

                describe "BEST_SEARCH_MATCH_FOR_HM" do

                    # NOTE: years_experience ordering is tested along with the years_experience filter.

                    describe "with industry filter" do
                        it "should prioritize matches on experience over matches on job_sectors_of_interest" do
                            career_profiles = CareerProfile.limit(3)

                            # matches against interests only
                            career_profiles[0].update_attribute(:job_sectors_of_interest, ['a', 'b'])

                            # matches against experience and interests
                            professional_organization_id = ProfessionalOrganizationOption.first.id
                            work_experience = CareerProfile::WorkExperience.create({
                                career_profile_id: career_profiles[1].id,
                                job_title: 'NA',
                                start_date: Time.now,
                                professional_organization_option_id: professional_organization_id,
                                industry: 'b'
                            })
                            career_profiles[1].work_experiences = [work_experience]
                            career_profiles[1].job_sectors_of_interest = ['d']
                            career_profiles[1].save!

                            # matches against experience only
                            work_experience = CareerProfile::WorkExperience.create({
                                career_profile_id: career_profiles[2].id,
                                job_title: 'NA',
                                start_date: Time.now,
                                professional_organization_option_id: professional_organization_id,
                                industry: 'b'
                            })
                            career_profiles[2].work_experiences = [work_experience]
                            career_profiles[2].job_sectors_of_interest = ['c']
                            career_profiles[2].save!

                            update_career_profile_search_helpers

                            get :index,
                                params: {
                                    :format => :json,
                                    :view => 'career_profiles',
                                    :filters => {:industries => ['b', 'd']}.to_json,
                                    :sort => ['BEST_SEARCH_MATCH_FOR_HM']
                                }
                            assert_200

                            # we only care about the order of these profiles,
                            # so only consider them, even though other profiles
                            # might have been returned
                            relevant_ids = [
                                career_profiles[0].id,
                                career_profiles[1].id,
                                career_profiles[2].id
                            ]
                            all_actual_ids = body_json['contents']['career_profiles'].map { |r| r["id"] }
                            actual_ids = all_actual_ids & relevant_ids

                            expect(actual_ids).to eq([
                                career_profiles[1].id,
                                career_profiles[2].id,
                                career_profiles[0].id
                            ])
                        end
                    end

                    describe "with roles filter" do

                        it "should use expected prioritization when searching for a top-level key" do

                            career_profiles = setup_career_profiles_for_role_filtering('top-level-key', 'low-level-key')

                            get :index,
                                params: {
                                    :format => :json,
                                    :view => 'career_profiles',
                                    :filters => {
                                        :roles => {
                                            # when searching for a top-level key, primary_areas_of_interest and
                                            # work_experience_roles behave the same (this is setup in the client)
                                            # see https://trello.com/c/H5fAM37k, under the header
                                            # "### Change role logic not to search in all_areas for
                                            # work_experiences_roles when a sub-category is specified"
                                            :primary_areas_of_interest => ['top-level-key', 'low-level-key'],
                                            :preferred_primary_areas_of_interest => ['top-level-key'],
                                            :work_experience_roles => ['top-level-key', 'low-level-key'],
                                            :preferred_work_experience_roles => ['top-level-key']
                                        },

                                        :user_id => career_profiles.values.map(&:user_id)
                                    }.to_json,
                                    :sort => ['BEST_SEARCH_MATCH_FOR_HM']
                                }
                            assert_200
                            actual_ids = body_json['contents']['career_profiles'].map { |r| r["id"] }

                            expected_keys = [
                                :current_wexp_exact_primary_areas_of_interest_exact,
                                :previous_wexp_exact_primary_areas_of_interest_exact,

                                :current_wexp_exact_primary_areas_of_interest_inexact,
                                :previous_wexp_exact_primary_areas_of_interest_inexact,

                                :current_wexp_inexact_primary_areas_of_interest_exact,
                                :previous_wexp_inexact_primary_areas_of_interest_exact,

                                :current_wexp_inexact_primary_areas_of_interest_inexact,
                                :previous_wexp_inexact_primary_areas_of_interest_inexact,

                                :current_wexp_exact,
                                :previous_wexp_exact,

                                :current_wexp_inexact,
                                :previous_wexp_inexact,

                                :primary_areas_of_interest_exact,
                                :primary_areas_of_interest_inexact
                            ]

                            actual_keys = actual_ids.map do |id|
                                career_profile = career_profiles.values.detect { |cp| cp.id == id }
                                career_profiles.invert[career_profile]
                            end

                            expect(actual_keys).to eq(expected_keys)
                        end

                        it "should use expected prioritization when searching for a low-level key" do

                            career_profiles = setup_career_profiles_for_role_filtering('low-level-key', 'top-level-key')

                            get :index,
                                params: {
                                    :format => :json,
                                    :view => 'career_profiles',
                                    :filters => {
                                        :roles => {
                                            # when searching for a low-level key, primary_areas_of_interest accepts more
                                            # values than does work_experience_roles (this is setup in the client)
                                            # see https://trello.com/c/H5fAM37k, under the header
                                            # "### Change role logic not to search in all_areas for
                                            # work_experiences_roles when a sub-category is specified"
                                            :primary_areas_of_interest => ['top-level-key', 'low-level-key'],
                                            :preferred_primary_areas_of_interest => ['low-level-key'],
                                            :work_experience_roles => ['low-level-key'],
                                            :preferred_work_experience_roles => ['low-level-key']
                                        },

                                        :user_id => career_profiles.values.map(&:user_id)
                                    }.to_json,
                                    :sort => ['BEST_SEARCH_MATCH_FOR_HM']
                                }
                            assert_200
                            actual_ids = body_json['contents']['career_profiles'].map { |r| r["id"] }

                            expected_keys = [
                                :current_wexp_exact_primary_areas_of_interest_exact,
                                :previous_wexp_exact_primary_areas_of_interest_exact,

                                :current_wexp_exact_primary_areas_of_interest_inexact,
                                :previous_wexp_exact_primary_areas_of_interest_inexact,

                                :current_wexp_exact,
                                :previous_wexp_exact,

                                :primary_areas_of_interest_exact,

                                # since inexact matches on the work experience are ignored here,
                                # these profiles fall down to here in the ordering
                                :previous_wexp_inexact_primary_areas_of_interest_exact,
                                :current_wexp_inexact_primary_areas_of_interest_exact,

                                :primary_areas_of_interest_inexact,

                                # since inexact matches on the work experience are ignored here,
                                # these profiles fall down to here in the ordering
                                :previous_wexp_inexact_primary_areas_of_interest_inexact,
                                :current_wexp_inexact_primary_areas_of_interest_inexact

                                # since inexact matches on the work experience are ignored here,
                                # these profiles do not show up at all
                                # :current_wexp_inexact,
                                # :previous_wexp_inexact,
                            ]

                            actual_keys = actual_ids.map do |id|
                                career_profile = career_profiles.values.detect { |cp| cp.id == id }
                                career_profiles.invert[career_profile]
                            end

                            expect(actual_keys).to eq(expected_keys)
                        end


                        def setup_career_profiles_for_role_filtering(exact_match, inexact_match)


                            return_value = {}
                            professional_organization_id = ProfessionalOrganizationOption.first.id

                            config = {
                                :current_wexp_exact_primary_areas_of_interest_exact => [1, exact_match, nil, exact_match],
                                :previous_wexp_exact_primary_areas_of_interest_exact => [2, nil, exact_match, exact_match],

                                :current_wexp_exact_primary_areas_of_interest_inexact => [3, exact_match, nil, inexact_match],
                                :previous_wexp_exact_primary_areas_of_interest_inexact => [4, nil, exact_match, inexact_match],

                                :current_wexp_inexact_primary_areas_of_interest_exact => [5, inexact_match, nil, exact_match],
                                :previous_wexp_inexact_primary_areas_of_interest_exact => [6, nil, inexact_match, exact_match],

                                :current_wexp_inexact_primary_areas_of_interest_inexact => [7, inexact_match, nil, inexact_match],
                                :previous_wexp_inexact_primary_areas_of_interest_inexact => [8, nil, inexact_match, inexact_match],

                                :current_wexp_exact => [9, exact_match, nil, nil],
                                :previous_wexp_exact => [10, nil, exact_match, nil],

                                :current_wexp_inexact => [11, inexact_match, nil],
                                :previous_wexp_inexact => [12, nil, inexact_match, nil],

                                :primary_areas_of_interest_exact => [13, nil, nil, exact_match],
                                :primary_areas_of_interest_inexact => [14, nil, nil, inexact_match]
                            }

                            career_profiles = get_profiles_with_equal_ordering_priorities(config.size).to_a
                            config.each do |key, matches|

                                default_priority, current_wexp_match, previous_wexp_match, primary_areas_of_interest_match = matches

                                career_profile = career_profiles.shift
                                career_profile.work_experiences.destroy_all

                                work_experiences = [
                                    # current work experience
                                    CareerProfile::WorkExperience.create({
                                        career_profile_id: career_profile.id,
                                        job_title: 'NA',
                                        start_date: Time.now - 1.year,
                                        end_date: nil,
                                        professional_organization_option_id: professional_organization_id,
                                        role: current_wexp_match ? current_wexp_match : 'no_match'
                                    }),

                                    # previous work expeirence
                                    CareerProfile::WorkExperience.create({
                                        career_profile_id: career_profile.id,
                                        job_title: 'NA',
                                        start_date: Time.now - 2.years,
                                        end_date: Time.now - 1.year,
                                        professional_organization_option_id: professional_organization_id,
                                        role: previous_wexp_match ? previous_wexp_match : 'no_match'
                                    })
                                ]

                                career_profile.work_experiences = work_experiences

                                career_profile.primary_areas_of_interest = primary_areas_of_interest_match ? [primary_areas_of_interest_match] : ['no_match']

                                # all else equal, created_at is used for prioritization. This ensures
                                # the same ordering every time for things that are otherwise the same
                                career_profile.created_at = Time.new(2000, 01, default_priority)
                                career_profile.save!
                                return_value[key] = career_profile

                            end

                            update_career_profile_search_helpers
                            return_value
                        end

                    end

                    describe "with years_experience filter" do

                        it "should sort with the most experience first" do
                            setup_profiles_with_different_years_experience

                            # by default, move the wrong one to the top. That
                            # ay we can ensure that something else is moving it down
                            allow(controller).to receive(:default_prioritization) do |query|
                                query = query.order(Arel.sql("case when user_id='#{@career_profiles[3].user_id}' then 0 else 1 end"))
                            end

                            get :index, params: {
                                :format => :json,
                                :view => 'career_profiles',
                                :filters => {
                                    :years_experience => ['0_1_years', '1_2_years', '2_4_years', '4_6_years', '6_plus_years'],
                                    :user_id => @career_profiles.map(&:user_id)
                                }.to_json,
                                :sort => ['BEST_SEARCH_MATCH_FOR_HM']
                            }

                            # the expected order is setup in setup_profiles_with_different_years_experience
                            expect(body_json['contents']['career_profiles'].map { |r| r["id"] }).to eq(@career_profiles.map(&:id))
                        end
                    end

                    describe "with places filter" do

                        it "should prioritize local over willing to relocate over flexible" do
                            add_locations_to_profiles

                            # a location close to dc should find
                            # dc candidates
                            location = Location.new("38.9847° N, 77.0947° W")
                            bethesda = {
                                lat: location.lat,
                                lng: location.lng
                            }

                            # a location close to boston
                            location = Location.new("42.3736° N, 71.1097° W")
                            cambridge = {
                                lat: location.lat,
                                lng: location.lng
                            }

                            # since we have to set up the OR clauses ourselves inside the ORDER clause,
                            # rather than relying on active record, I'm passing in two locations here
                            # just to be sure it all works.
                            get :index, params: {
                                :format => :json,
                                :view => 'career_profiles',
                                :sort => ['BEST_SEARCH_MATCH_FOR_HM'],
                                :filters => {:places => [bethesda, cambridge]}.to_json
                            }
                            assert_200

                            # we only care about the order of these profiles,
                            # so only consider them, even though other profiles
                            # might have been returned
                            relevant_ids = [
                                @dc_profile.id,
                                @boston_profile.id,
                                @relocatable_profile.id,
                                @boston_relocatable_profile.id,
                                @flexible_us_profile.id
                            ]
                            all_actual_ids = body_json['contents']['career_profiles'].map { |r| r["id"] }
                            actual_ids = all_actual_ids & relevant_ids

                            # the local candidate is best
                            expect([actual_ids[0], actual_ids[1]]).to match_array([@dc_profile.id, @boston_profile.id])

                            # the relocatable candidates are next
                            expect([actual_ids[2], actual_ids[3]]).to match_array([@relocatable_profile.id, @boston_relocatable_profile.id])

                            # flexible candidate is last
                            expect(actual_ids[4]).to eq(@flexible_us_profile.id)
                        end

                        # this was blowing up when the keys array was empty in order_by_places
                        it "should work if none of the provided places are in our list" do
                            add_locations_to_profiles

                            # a location far from all known locations
                            location = Location.new("36.1540° N, 95.9928° W")
                            tulsa = {
                                lat: location.lat,
                                lng: location.lng
                            }

                            # since we have to set up the OR clauses ourselves inside the ORDER clause,
                            # rather than relying on active record, I'm passing in two locations here
                            # just to be sure it all works.
                            get :index, params: {
                                :format => :json,
                                :view => 'career_profiles',
                                :sort => ['BEST_SEARCH_MATCH_FOR_HM'],
                                :filters => {:places => [tulsa]}.to_json
                            }
                            assert_200
                        end

                    end

                    describe "in_school handling" do

                        before(:each) do
                            @in_school_career_profile = CareerProfile.joins(:career_profile_search_helper)
                                                            .joins(:education_experiences).first
                            @not_in_school_career_profile = CareerProfile.joins(:career_profile_search_helper)
                                                                .left_outer_joins(:education_experiences)
                                                                .where("education_experiences.id is null").first
                            @in_school_career_profile.education_experiences.each { |e| e.update_attribute(:graduation_year, (Time.now + 1.years).year) }
                            @career_profiles = [@in_school_career_profile, @not_in_school_career_profile]

                            # by default, move the in_school_career_profile to the top. That
                            # ay we can ensure that something else is moving it down
                            allow(controller).to receive(:default_prioritization) do |query|
                                query = query.order(Arel.sql("case when user_id='#{@in_school_career_profile.user_id}' then 0 else 1 end"))
                            end
                            allow(controller).to receive(:order_by_years_experience) { |filters, query| query }

                            # we need to be able to set these filters, since they define how things
                            # are ordered, but we don't actually want them to be used for filtering here
                            allow(controller).to receive(:filter_career_profiles_by_employment_types_of_interest) { |filters, query| query }
                            allow(controller).to receive(:filter_career_profiles_by_years_experience) { |filters, query| query }

                            update_career_profile_search_helpers(@career_profiles)
                        end


                        it "should deprioritize in_school people" do
                            get :index, params: {
                                :format => :json,
                                :view => 'career_profiles',
                                :sort => ['BEST_SEARCH_MATCH_FOR_HM'],
                                :filters => {
                                    user_id: @career_profiles.map(&:user_id),
                                    employment_types_of_interest: ['not_internship'],
                                    years_experience: ['not_0_1_years']
                                }
                            }
                            assert_200

                            results = body_json['contents']['career_profiles'].map { |r| r["id"] }
                            expect(results).to eq([@not_in_school_career_profile.id, @in_school_career_profile.id])

                        end

                        it "should NOT deprioritize in_school people if looking for those interested in internships" do
                            get :index, params: {
                                :format => :json,
                                :view => 'career_profiles',
                                :sort => ['BEST_SEARCH_MATCH_FOR_HM'],
                                :filters => {user_id: @career_profiles.map(&:user_id), :employment_types_of_interest => ['internship']}
                            }
                            assert_200

                            results = body_json['contents']['career_profiles'].map { |r| r["id"] }
                            expect(results).to eq([@in_school_career_profile.id, @not_in_school_career_profile.id])

                        end

                        it "should NOT deprioritize in_school people if looking for those with 0-1 years experience" do
                            get :index, params: {
                                :format => :json,
                                :view => 'career_profiles',
                                :sort => ['BEST_SEARCH_MATCH_FOR_HM'],
                                :filters => {user_id: @career_profiles.map(&:user_id), :years_experience => ['0_1_years']}
                            }
                            assert_200

                            results = body_json['contents']['career_profiles'].map { |r| r["id"] }
                            expect(results).to eq([@in_school_career_profile.id, @not_in_school_career_profile.id])

                        end

                    end

                    describe "default prioritization" do

                        it "should prioritize US and CA applicants" do
                            add_locations_to_profiles

                            get :index, params: {
                                :format => :json,
                                :view => 'career_profiles',
                                :sort => ['BEST_SEARCH_MATCH_FOR_HM']
                            }
                            assert_200

                            # we only care about the order of these profiles,
                            # so only consider them, even though other profiles
                            # might have been returned
                            relevant_ids = [
                                @flexible_non_us_profile.id,
                                @flexible_us_profile.id
                            ]
                            all_actual_ids = body_json['contents']['career_profiles'].map { |r| r["id"] }
                            actual_ids = all_actual_ids & relevant_ids

                            expect(actual_ids).to eq([
                                @flexible_us_profile.id,
                                @flexible_non_us_profile.id
                            ])
                        end

                        it "should prioritize those most interested in a job" do
                            add_interested_in_joining_new_company_to_profiles

                            get :index, params: {
                                :format => :json,
                                :view => 'career_profiles',
                                :sort => ['BEST_SEARCH_MATCH_FOR_HM']
                            }
                            assert_200

                            # we only care about the order of these profiles,
                            # so only consider them, even though other profiles
                            # might have been returned
                            relevant_ids = [
                                @very_interested.id,
                                @interested.id,
                                @neutral.id,
                                @not_interested.id
                            ]
                            all_actual_ids = body_json['contents']['career_profiles'].map { |r| r["id"] }
                            actual_ids = all_actual_ids & relevant_ids

                            expect(actual_ids).to eq([
                                @very_interested.id,
                                @interested.id,
                                @neutral.id,
                                @not_interested.id
                            ])
                        end

                        it "should prioritize !do_not_create_relationships over do_not_create_relationships" do
                            add_do_not_create_relationships_to_profiles

                            get :index, params: {
                                :format => :json,
                                :view => 'career_profiles',
                                :sort => ['BEST_SEARCH_MATCH_FOR_HM']
                            }
                            assert_200

                            # we only care about the order of these profiles,
                            # so only consider them, even though other profiles
                            # might have been returned
                            relevant_ids = [
                                @disabled_profile.id,
                                @enabled_profile.id
                            ]
                            all_actual_ids = body_json['contents']['career_profiles'].map { |r| r["id"] }
                            actual_ids = all_actual_ids & relevant_ids

                            expect(actual_ids).to eq([
                                @enabled_profile.id,
                                @disabled_profile.id
                            ])
                        end

                        it "should prioritize those who have been accepted by more hiring managers" do
                            career_profiles = add_hiring_relationships_to_profiles

                            get :index, params: {
                                :format => :json,
                                :view => 'career_profiles',
                                :filters => {
                                    user_id: career_profiles.map(&:user_id)
                                },
                                :sort => ['BEST_SEARCH_MATCH_FOR_HM']
                            }
                            assert_200
                            all_actual_ids = body_json['contents']['career_profiles'].map { |r| r["id"] }

                            # add_hiring_relationships_to_profiles makes the first career profile
                            # the one with the most accepted relationships
                            expect(all_actual_ids).to eq(career_profiles.map(&:id))
                        end
                    end

                end

            end

            it "should return student profile if requested and have partial access" do
                expect(controller.current_user).to receive(:has_full_student_network_access?).at_least(1).times.and_return(false)

                other_user = users(:accepted_mba_cohort_user)
                expect(other_user.pref_student_network_privacy).to eq('full')
                # remove the default filters so we can get the user we want
                allow(controller).to receive(:filter_for_access_to_network) { |filters, query| query }
                process :index, :params => {:format => :json, :view => 'student_network_profiles', :filters => {:user_id => other_user.id}}
                assert_200

                # check that some fields that are missing because we have restricted access
                expect(body_json['contents']['career_profiles'][0]['anonymized']).to eq(true)

                # check that some fields that are missing because we're asking for the student profile
                expect(body_json['contents']['career_profiles'][0].key?('phone')).to be(false)
                expect(body_json['contents']['career_profiles'][0].key?('awards_and_interests')).to be(false)
                expect(body_json['contents']['career_profiles'][0].key?('skills')).to be(false)
            end

            it "should return student profile if requested and have complete access" do

                expect(controller.current_user).to receive(:has_full_student_network_access?).at_least(1).times.and_return(true)
                # remove the default filters so we can get the user we want
                allow(controller).to receive(:filter_for_access_to_network) { |filters, query| query }

                other_user = users(:accepted_mba_cohort_user)
                expect(other_user.pref_student_network_privacy).to eq('full')
                process :index, :params => {:format => :json, :view => 'student_network_profiles', :filters => {:user_id => other_user.id}}
                assert_200

                # assert some fields that should be there, since we're an admin
                expect(body_json['contents']['career_profiles'][0]['anonymized']).to eq(false)

                # check that some fields that are missing because we're asking for the student profile
                expect(body_json['contents']['career_profiles'][0].key?('phone')).to be(false)
                expect(body_json['contents']['career_profiles'][0].key?('awards_and_interests')).to be(false)
                expect(body_json['contents']['career_profiles'][0].key?('skills')).to be(false)
            end

            it "should show all career profile fields to an hm with can_view_full_hiring_manager_experience" do
                user = HiringApplication.find_by_status('accepted').user
                user_with_career_profile = users(:user_with_career_profile)
                allow(controller).to receive(:current_user).and_return(user)
                # remove the default filters so we can get the user we want
                allow(controller).to receive(:filter_for_access_to_network) { |filters, query| query }
                process :index, :params => {:format => :json, :view => 'career_profiles', :filters => {:user_id => user_with_career_profile.id}}
                assert_200

                # check for fields that would be filtered out by restricted access
                expect(body_json['contents']['career_profiles'][0]['anonymized']).to be(false)
            end

            it "should hide some fields from an hm without can_view_full_hiring_manager_experience" do
                user = HiringApplication.find_by_status('accepted').user
                user_with_career_profile = users(:user_with_career_profile)
                allow(controller).to receive(:current_user).and_return(user)
                @can_view_full_hiring_manager_experience = false
                # remove the default filters so we can get the user we want
                allow(controller).to receive(:filter_for_access_to_network) { |filters, query| query }
                process :index, :params => {:format => :json, :view => 'career_profiles', :filters => {:user_id => user_with_career_profile.id}}
                assert_200

                # check that some fields that are missing because we have restricted access
                expect(body_json['contents']['career_profiles'][0]['anonymized']).to be(true)

            end

            def setup_profiles_with_different_years_experience
                career_profile_ids = CareerProfile::WorkExperience.distinct.reorder(nil).limit(8).pluck('career_profile_id')
                @career_profiles = CareerProfile.find(career_profile_ids)
                expect(@career_profiles.size).to eq(8)

                @without_current_experience = {}
                [6, 18, 24, 7*12].each_with_index do |months_experience, i|
                    career_profile = @career_profiles[i]
                    exp = career_profile.work_experiences[0]
                    exp.start_date = Time.parse('2000/06/01').to_date
                    exp.end_date = (exp.start_date + months_experience * 1.month).to_date
                    exp.save!
                    (career_profile.work_experiences.to_a - [exp]).each(&:destroy)
                    career_profile.education_experiences.destroy_all
                    @without_current_experience[months_experience] = career_profile
                end

                @with_current_experience = {}
                [-72, 5, 18.1, 7.1*12].each_with_index do |months_experience, i|
                    career_profile = @career_profiles[i+@without_current_experience.size]
                    exp = career_profile.work_experiences[0]
                    exp.start_date = Time.now - months_experience * 1.month
                    exp.end_date = nil
                    exp.save!
                    (career_profile.work_experiences.to_a - [exp]).each(&:destroy)
                    career_profile.education_experiences.destroy_all
                    @with_current_experience[months_experience] = career_profile
                end

                update_career_profile_search_helpers(@career_profiles)
                @career_profiles = [
                    @with_current_experience.fetch(7.1*12),
                    @without_current_experience.fetch(7*12),
                    @without_current_experience.fetch(24),
                    @with_current_experience.fetch(18.1),
                    @without_current_experience.fetch(18),
                    @without_current_experience.fetch(6),
                    @with_current_experience.fetch(5),
                    @with_current_experience.fetch(-72)
                ]
            end


        end

        describe "preload_career_profiles_for_index" do
            it "should use eager loading with view='career_profiles'" do
                assert_eager_loaded('career_profiles')
            end

            it "should use eager loading with view='student_network_profiles'" do
                assert_eager_loaded('student_network_profiles')
            end

            it "should use eager loading with view='editable'" do
                assert_eager_loaded('student_network_profiles')
            end

            it "should use eager loading with non-anonymized profiles with avatar assets" do
                user = CareerProfile.joins(:career_profile_search_helper).first.user
                user.avatar = AvatarAsset.new(url: 'http://path/to/avatar')
                user.save!
                controller.params['filters'] = {'user_id' => user.id}
                json = assert_eager_loaded('career_profiles')
                entry = json.detect { |e| e['user_id'] == user.id }
                expect(entry['avatar_url']).not_to be_nil
            end

            def assert_eager_loaded(view)
                # Mock this out, since in prod it will almost always be cached
                allow(Cohort).to receive(:cached_published).and_return(
                    OpenStruct.new(
                        name: 'MBA1',
                        id: '1234',
                        start_date: Time.now + 10.days,
                        end_date: Time.now + 1.year
                    )
                )

                # performance
                controller.params['limit'] = 5
                controller.params['max_total_count'] = 5

                # see should take advantage of active record preloading to optimize data loading in conversations_controller_spec for reference
                career_profiles = controller.preload_career_profiles_for_index

                # Once the records are preloaded, there should be no more queries required to convert them to json.
                expect(ActiveRecord::Base.connection).not_to receive(:exec_query).and_call_original

                # If the call to as_json on the career profiles fails with an error that looks like an infinite loop,
                # you've made a change to the as_json that makes an extra query.
                # We set anonymize=false because that will require more information.  Seems hard to imagine that this
                # would ever pass with anonymize=false and fail with anonymize=true, but it could maybe fail only
                # when anonymize=false.
                json = career_profiles.as_json(user_id: controller.current_user_id, view: view, anonymize: false)

                # sanity checks. make sure that we actually generated a full json tree
                expect(career_profile = json[0]).not_to be_nil
                expect(career_profile['user_id']).not_to be_nil
                json
            end
        end

        describe "limit" do
            attr_accessor :has_full_access

            before(:each) do
                allow(controller).to receive(:authorize_index_with_params)
                @user = User.first
                allow(controller).to receive(:current_user).and_return(@user)

                expect(controller.current_ability).to receive(:can?) do |action, object|
                    if action == :view_full_hiring_manager_experience
                        self.has_full_access
                    else
                        true
                    end
                end.at_least(1).times
                self.has_full_access = true
            end

            it "should limit and return the total count if only limit is provided" do
                user_ids = CareerProfile.limit(5).pluck(:user_id)

                process :index, :params => {
                    :format => :json,
                    :view => 'career_profiles',
                    :filters => {
                        user_id: user_ids
                    }.as_json,
                    :limit => "3" # get params come in as strings
                }
                assert_200
                expect(body_json['meta']['total_count']).to eq(5)
                expect(body_json['contents']['career_profiles'].size).to eq(3)
            end

            it "should limit and return the total count if max_total_count and there are fewer results than the max_total_count" do
                user_ids = CareerProfile.limit(5).pluck(:user_id)

                process :index, :params => {
                    :format => :json,
                    :view => 'career_profiles',
                    :filters => {
                        user_id: user_ids
                    }.as_json,
                    :limit => "3", # get params come in as strings
                    :max_total_count => "10"
                }
                assert_200
                expect(body_json['meta']['total_count']).to eq(5)
                expect(body_json['contents']['career_profiles'].size).to eq(3)

            end

            it "should limit and return the max_total_count if max_total_count and there are more results than the max_total_count" do
                user_ids = CareerProfile.limit(5).pluck(:user_id)

                process :index, :params => {
                    :format => :json,
                    :view => 'career_profiles',
                    :filters => {
                        user_id: user_ids
                    }.as_json,
                    :limit => "3", # get params come in as strings
                    :max_total_count => "4"
                }
                assert_200
                expect(body_json['meta']['total_count']).to eq(4)
                expect(body_json['contents']['career_profiles'].size).to eq(3)

            end

            it "should set max_total_count if there is no limit provided" do
                user_ids = CareerProfile.limit(5).pluck(:user_id)

                process :index, :params => {
                    :format => :json,
                    :view => 'career_profiles',
                    :filters => {
                        user_id: user_ids
                    }.as_json
                }
                assert_200
                expect(body_json['meta']['total_count']).to eq(5)
                expect(body_json['contents']['career_profiles'].size).to eq(5)

            end

            it "should limit to 3 results for a hiring manager who does not have full access" do
                self.has_full_access = false

                user_ids = CareerProfile.limit(5).pluck(:user_id)

                process :index, :params => {
                    :format => :json,
                    :view => 'career_profiles',
                    :filters => {
                        user_id: user_ids
                    }.as_json
                }
                assert_200
                expect(body_json['meta']['total_count']).to eq(5)
                expect(body_json['contents']['career_profiles'].size).to eq(3)

            end

        end

    end

    describe "GET show" do

        it "should require the user to be logged in" do
            get :show, params: {format: :json, id: SecureRandom.uuid}
            assert_error_response(401, {message: "You must be logged in to do that", not_logged_in: true})
        end

        it "should 404 if career profile is not found" do
            stub_current_user
            get :show, params: {format: :json, id: SecureRandom.uuid}
            assert_error_response(404)
        end

        describe "when user cannot show_career_profile" do

            describe "when deep_link" do

                it "should 401 if deep_link" do
                    career_profile = set_can_show_career_profile(false, true)
                    get :show, params: {deep_link: 'true', format: :json, id: career_profile}
                    assert_error_response(401, {message: "Sorry, the profile you've requested is no longer active in our system."})
                end
            end

            describe "when !deep_link" do

                it "should render_unauthorized_error" do
                    career_profile = set_can_show_career_profile(false)
                    expect(controller).to receive(:render_unauthorized_error)
                    get :show, params: {format: :json, id: career_profile}
                end
            end
        end

        describe "when user can show_career_profile" do
            attr_accessor :can_view_full_hiring_manager_experience

            before(:each) do
                @career_profile = set_can_show_career_profile(true)
            end

            it "should render an editable career profile" do
                get :show, params: {format: :json, id: @career_profile.id, view: 'editable'}
                assert_200
                expect(body_json["contents"]["career_profiles"].length).to eq(1)
                expect(body_json["contents"]["career_profiles"][0]["id"]).to eq(@career_profile.id)
                # check for a field that is only in editable
                expect(body_json["contents"]["career_profiles"][0]["consider_early_decision"]).to eq(@career_profile.consider_early_decision)
            end

            it "should render non-anonymized fields if allowed" do
                self.can_view_full_hiring_manager_experience = true
                get :show, params: {format: :json, id: @career_profile.id, view: 'career_profiles'}
                assert_200
                expect(body_json["contents"]["career_profiles"].length).to eq(1)
                expect(body_json["contents"]["career_profiles"][0]["id"]).to eq(@career_profile.id)
                # check for an field that is only available in non-anonymous profile
                expect(body_json["contents"]["career_profiles"][0].key?('resume')).to be(true)
            end

            it "should render anonymized fields if restricted" do
                self.can_view_full_hiring_manager_experience = false
                get :show, params: {format: :json, id: @career_profile.id, view: 'career_profiles'}
                assert_200
                expect(body_json["contents"]["career_profiles"].length).to eq(1)
                expect(body_json["contents"]["career_profiles"][0]["id"]).to eq(@career_profile.id)
                # check for an field that is only available in non-anonymous profile
                expect(body_json["contents"]["career_profiles"][0].key?('resume')).to be(false)
            end
        end

        def set_can_show_career_profile(val, deep_link = false)
            stub_current_user
            career_profile = users(:user_with_career_profile).career_profile
            expect(controller.current_ability).to receive(:can?) do |action, object|
                if action == :show
                    true
                elsif action == :view_full_hiring_manager_experience
                    can_view_full_hiring_manager_experience
                elsif action == :show_career_profile
                    expect(object[:career_profile]).to eq(career_profile)
                    if deep_link
                        expect(object[:params][:deep_link]).to eq('true')
                    else
                        expect(object[:params][:deep_link]).to be_nil
                    end
                    val
                end
            end.at_least(:once)
            career_profile
        end
    end

    describe "GET share" do

        it "should work" do
            user, another_user = [User.where.not(email: nil).first, User.where.not(email: nil).second]
            allow(controller).to receive(:current_user).and_return(user)
            expect(controller.current_ability).to receive(:can?).at_least(1).times.and_return(true)
            career_profile = CareerProfile.active.first

            expect(user).to receive(:share_career_profile).with(
                career_profile,
                another_user,
                'check it out'
            )
            get :share, params: {format: :json, record: {
                id: career_profile.id,
                email: another_user.email,
                message: 'check it out'
            }}
            assert_200
            expect(body_json["contents"]["career_profiles"][0]['id']).to eq(career_profile.id)
        end
    end

    describe "POST create" do

        before(:each) do
            @user = users(:learner)
            @user.career_profile.destroy! unless @user.career_profile.nil?

            @another_user = User.where.not(id: @user.id).first
            @another_user.career_profile.destroy! unless @another_user.career_profile.nil?

            allow(controller).to receive(:current_user).and_return(@user)
            @career_profile_params = {
                user_id: @user.id
            }
        end

       it "should 401 if trying to create profile for another user" do
           expect(Proc.new {
               process :create, :method => :post, :params => {:format => :json, :record => @career_profile_params.merge({
                   user_id: @another_user.id
               })}
           }).not_to change { CareerProfile.count }

           assert_error_response(401)
       end

        it "should 401 if trying to create profile for another user" do
            expect(Proc.new {
                post :create, :params => {:format => :json, :record => @career_profile_params.merge({
                    user_id: @another_user.id
                })}
            }).not_to change { CareerProfile.count }

            assert_error_response(401)
        end

        it "should work in normal case" do
            post :create, :params => {:format => :json, :record => @career_profile_params}
            assert_200
            expect(@user.reload.career_profile).not_to be_nil
        end

    end

    describe "PUT update" do

        before(:each) do
            @user = users(:user_with_career_profile)
            allow(controller).to receive(:current_user).and_return(@user)
        end

        it "should 401 if trying to update profile for another user" do
            another_user = users(:another_user_with_career_profile)

            expect(CareerProfile).not_to receive(:update_from_hash!)
            process :update, :method => :put, :params => {:format => :json, :record => {
                id: another_user.career_profile.id
            }}
            assert_error_response(401)
        end

        it "should work in normal case" do
            career_profile_params = {
                id: @user.career_profile.id,
                user_id: @user.id,
                place_id: "1337",
                native_english_speaker: true,
                earned_accredited_degree_in_english: true,
                survey_years_full_time_experience: 'some_years',
                survey_most_recent_role_description: 'some_recent_role',
                survey_highest_level_completed_education_description: 'some_completed_education_description',
                short_answer_why_pursuing: 'foo',
                short_answer_use_skills_to_advance: 'bar',
                short_answer_ask_professional_advice: 'baz',
                has_no_formal_education: false
            }
            allow(controller).to receive(:career_profile_params).and_return(career_profile_params)
            expect(CareerProfile).to receive(:update_from_hash!).with(career_profile_params, anything).and_return(@user.career_profile)

            process :update, :method => :put, :params => {:format => :json, :record => career_profile_params}
            assert_200
        end

        it "should call add_num_recommended_positions_to_push_messages" do
            career_profile_params = {
                id: @user.career_profile.id,
                user_id: @user.id,
                place_id: "1337"
            }
            expect(controller).to receive(:add_num_recommended_positions_to_push_messages)
            process :update, :method => :put, :params => {:format => :json, :record => career_profile_params}
        end

        it "should update last_updated_at_by_student if record belongs to current_user" do
            original_last_updated_at_by_student = @user.career_profile.last_updated_at_by_student
            put :update, :params => {:format => :json, :record => {
                id: @user.career_profile.id,
                user_id: @user.id
            }}

            assert_200
            expect(@user.career_profile.reload.last_updated_at_by_student).not_to eq(original_last_updated_at_by_student)
        end

        it "should not update last_updated_at_by_student if record does not belong to current_user" do
            other_user = users(:admin)
            allow(controller).to receive(:current_user).and_return(other_user)
            original_last_updated_at_by_student = @user.career_profile.last_updated_at_by_student
            put :update, :params => {:format => :json, :record => {
                id: @user.career_profile.id,
                user_id: @user.id
            }}

            assert_200
            expect(@user.career_profile.reload.last_updated_at_by_student).to eq(original_last_updated_at_by_student)
        end

        describe "user properties" do

            before(:each) do
                # stub the ActiveRecord call to find the user to make
                # stubbing methods on the user easier
                allow(User).to receive(:find).and_return(@user)
            end

            it "should update user if its properties changed" do
                put :update, params: {
                    :format => :json,
                    :record => {
                        id: @user.career_profile.id,
                        place_id: "1337",
                        name: "Mister Anderson",
                        nickname: "The Godfather",
                        pref_student_network_privacy: "hidden"
                    }
                }
                assert_200
                expect(body_json["contents"]["career_profiles"].first["place_id"]).to eq("1337")
                expect(body_json["contents"]["career_profiles"].first["name"]).to eq("Mister Anderson")
                expect(body_json["contents"]["career_profiles"].first["nickname"]).to eq("The Godfather")
                expect(body_json["contents"]["career_profiles"].first["pref_student_network_privacy"]).to eq("hidden")

                @user.reload
                @user.career_profile.reload
                expect(@user.career_profile.place_id).to eq("1337")
                expect(@user.name).to eq("Mister Anderson")
                expect(@user.nickname).to eq("The Godfather")
                expect(@user.pref_student_network_privacy).to eq("hidden")
            end

            it "should update all of city, state, country of user if any of those properties changed" do
                @user.city = "Boulder"
                @user.state = "CO"
                @user.country = "US"
                @user.save!

                put :update, params: {
                    :format => :json,
                    :record => {
                        id: @user.career_profile.id,
                        state: "Tokyo",
                        country: "JP"
                    }
                }
                assert_200
                expect(body_json["contents"]["career_profiles"].first["city"]).to be_nil
                expect(body_json["contents"]["career_profiles"].first["state"]).to eq("Tokyo")
                expect(body_json["contents"]["career_profiles"].first["country"]).to eq("JP")

                @user.reload
                @user.career_profile.reload
                expect(@user.city).to be_nil # verify that Boulder got properly cleared out
                expect(@user.state).to eq("Tokyo")
                expect(@user.country).to eq("JP")
            end

            it "should not update user if its properties did not change" do
                allow(@controller).to receive(:update_last_seen_at)
                allow(@user).to receive(:save!)
                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: @user.career_profile.id,
                            name: @user.name # same name
                        }
                    }
                assert_200

                expect(@user).not_to have_received(:save!)
            end

            it "should always update user if program_type is specified" do
                allow(@controller).to receive(:update_last_seen_at)
                allow(@user).to receive(:program_type=)
                allow(@user).to receive(:save!)
                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: @user.career_profile.id,
                            program_type: @user.program_type # same program_type
                        }
                    }
                assert_200

                expect(@user).to have_received(:program_type=).with(@user.program_type)
                expect(@user).to have_received(:save!)
            end

            it "should set meta properties if user properties are changed" do
                expect(controller).to receive(:add_career_profile_user_fields_to_push_messages)
                expect(controller).to receive(:add_cohort_applications_to_push_messages)
                expect(controller).to receive(:set_relevant_cohort_push_messages)
                expect(controller).to receive(:set_institution_push_messages)

                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: @user.career_profile.id,
                            place_id: "1337",
                            name: "Mister Anderson"
                        }
                    }
                assert_200
            end

            # https://trello.com/c/usVkUfJo
            it "should set student_network_email to nil if blank" do
                @user.update!(student_network_email: 'soememail@example.com')
                put :update, params: {
                    :format => :json,
                    :record => {
                        id: @user.career_profile.id,
                        student_network_email: ""
                    }
                }
                assert_200
                expect(body_json["contents"]["career_profiles"].first["student_network_email"]).to be_nil

                @user.reload
                @user.career_profile.reload
                expect(@user.student_network_email).to be_nil
            end

            # https://trello.com/c/usVkUfJo
            it "should not convert an empty race array to nil" do
                @user.race = ['asian']
                @user.save!

                put :update, params: {
                    :format => :json,
                    :record => {
                        id: @user.career_profile.id,
                        race: []
                    }
                }
                assert_200
                expect(body_json["contents"]["career_profiles"].first["race"]).to eq([])

                @user.reload
                @user.career_profile.reload
                expect(@user.race).to eq([])
            end
        end

        describe "work experience" do
            before(:each) do

                @work_experiences = [
                    {
                        professional_organization: {
                            text: "Glorious Foos Inc.",
                            locale: "en"
                        },
                        start_date: Time.now.to_timestamp,
                        end_date: Time.now.to_timestamp,
                        job_title: 'Foo Handler',
                        featured: false,
                        responsibilities: ['Handling the foo', 'Washing the foo']
                    },
                    {
                        professional_organization: {
                            text: "Metal Bar World",
                            locale: "en"
                        },
                        start_date: Time.now.to_timestamp,
                        end_date: Time.now.to_timestamp,
                        job_title: 'Bar Engineer',
                        featured: false,
                        responsibilities: ['Designing bar', 'Creating bar']
                    }
                ]

            end

            it "should add a new work experience" do
                @user.career_profile.work_experiences.destroy_all

                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: @user.career_profile.id,
                            work_experiences: [@work_experiences.first, @work_experiences.second]
                        }
                    }
                assert_200
                expect(@user.career_profile.reload.work_experiences.size).to be(2)
            end

            it "should update an existing work experience" do
                expect(@user.career_profile.work_experiences.first).not_to be_nil

                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: @user.career_profile.id,
                            work_experiences: [@user.career_profile.work_experiences.first.as_json.merge(
                                "job_title" => "Updated Title",
                                "employment_type" => "part_time"
                            )]
                        }
                    }
                assert_200

                expect(@user.career_profile.reload.work_experiences.first[:job_title]).to eq("Updated Title")
                expect(@user.career_profile.reload.work_experiences.first[:employment_type]).to eq("part_time")
            end
        end

        describe "education experience" do
             before(:each) do
                @education_experiences = [
                    {
                        educational_organization: {
                            text: "Institute of Higher Bar",
                            locale: "en"
                        },
                        graduation_year: 2012,
                        degree: 'Bachelor of Bar',
                        major:  'Bar Science',
                        minor: 'Bar Studies',
                        gpa: '3.5',
                        degree_program: true,
                        will_not_complete: false
                    },
                    {
                        educational_organization: {
                            text: "Foo University",
                            locale: "en"
                        },
                        graduation_year: 2016,
                        degree: 'Bachelor of Foo',
                        major:  'Foo Science',
                        minor: 'Foo Studies',
                        gpa: '3.3',
                        degree_program: true,
                        will_not_complete: false
                    },
                ]
            end

            it "should add a new education experience" do
                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: @user.career_profile.id,
                            education_experiences: [@education_experiences.first, @education_experiences.second]
                        }
                    }
                assert_200
                expect(@user.career_profile.reload.education_experiences.size).to be(2)
            end

            it "should update an existing education experience" do
                expect(@user.career_profile.work_experiences.first).not_to be_nil

                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: @user.career_profile.id,
                            education_experiences: [@user.career_profile.education_experiences.first.as_json.merge(
                                "graduation_year" => 1337
                            )]
                        }
                    }
                assert_200

                expect(@user.career_profile.reload.education_experiences.first[:graduation_year]).to eq(1337)
            end
        end

        describe "career_profiles student network fields" do

            before(:each) do
                @student_network_interests = [
                    StudentNetworkInterestsOption.first,
                    StudentNetworkInterestsOption.second
                ]

                @student_network_looking_for = [
                    'foo_bar',
                    'bar_foo',
                    'rab_oof'
                ]
            end

            it "should add new field(s)" do
                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: @user.career_profile.id,
                            student_network_interests: @student_network_interests,
                            student_network_looking_for: @student_network_looking_for
                        }
                    }
                assert_200
                expect(@user.career_profile.reload.student_network_interests).to match_array(@student_network_interests)
                expect(@user.career_profile.reload.student_network_looking_for).to match_array(@student_network_looking_for)
            end

            it "should update existing field(s)" do
                user = User.find(@user.id)
                user.career_profile.student_network_interests = @student_network_interests
                user.career_profile.student_network_looking_for = @student_network_looking_for
                user.career_profile.save!

                put :update,
                    params: {
                        :format => :json,
                        :record => {
                            id: user.career_profile.id,
                            student_network_interests: [user.career_profile.student_network_interests.first.as_json.merge( locale: 'es' )],
                            student_network_looking_for: user.career_profile.student_network_looking_for.clone.push('some_updated_text')
                        }
                    }
                assert_200

                user.career_profile.reload
                expect(user.career_profile.student_network_interests.first[:locale]).to eq('es')
                expect(user.career_profile.student_network_looking_for.size).to be(4)
                expect(user.career_profile.student_network_looking_for.last).to eq('some_updated_text')
            end

        end

    end

   describe "career_profile_json_options" do

        describe "with view=career_profiles" do

            it "should return non-anonymized json if allowed" do
                received_expected_meth = false
                expect(controller.current_ability).to receive(:can?).at_least(1).times do |meth, object|
                    if meth == :view_full_hiring_manager_experience
                        received_expected_meth = true
                        true
                    else
                        false
                    end
                end
                expect(controller.career_profile_json_options('career_profiles')).to eq({
                    view: 'career_profiles',
                    anonymize: false
                })
                expect(received_expected_meth).to be(true)
            end

            it "should return anonymized json if not allowed" do
                received_expected_meth = false
                expect(controller.current_ability).to receive(:can?).at_least(1).times do |meth, object|
                    if meth == :view_full_hiring_manager_experience
                        received_expected_meth = true
                    end
                    false
                end
                expect(controller.career_profile_json_options('career_profiles')).to eq({
                    view: 'career_profiles',
                    anonymize: true
                })
                expect(received_expected_meth).to be(true)
            end

        end

        describe "with view=student_network_profiles" do

            before(:each) do
                user = User.first
                expect(controller).to receive(:current_user).and_return(user).at_least(1).times
            end


            it "should request non-anonymized profile if allowed" do
                expect(controller.current_user).to receive(:has_full_student_network_access?).and_return(true)
                expect(controller.career_profile_json_options('student_network_profiles')).to eq({
                    view: 'student_network_profiles',
                    anonymize: false
                })
            end

            it "should request anonymized profile if not allowed" do
                expect(controller.current_user).to receive(:has_full_student_network_access?).and_return(false)
                expect(controller.career_profile_json_options('student_network_profiles')).to eq({
                    view: 'student_network_profiles',
                    anonymize: true
                })
            end
        end

    end


    def add_locations_to_profiles
        career_profiles = career_profiles = get_profiles_with_equal_ordering_priorities(7)

        country_us = {'short' => 'US'}
        country_de = {'short' => 'DE'}

        @flexible_non_us_profile = career_profiles[0]
        @flexible_non_us_profile.update({:locations_of_interest => ['flexible'], :place_details => {'country' => country_de}})

        @flexible_us_profile = career_profiles[1]
        @flexible_us_profile.update({:locations_of_interest => ['flexible'], :place_details => {'country' => country_us}})

        @dc_profile = career_profiles[2]
        @dc_profile.update({:locations_of_interest => [], :place_details => {'lng' => -77.0369, 'lat' => 38.9072, 'country' => country_us}})

        @boston_profile = career_profiles[3]
        @boston_profile.update({:locations_of_interest => [], :place_details => {'lng' => -71.0589, 'lat' => 42.3601, 'country' => country_us}})

        @relocatable_profile = career_profiles[4]
        @relocatable_profile.update_attribute(:locations_of_interest, ['washington_dc'])

        @boston_relocatable_profile = career_profiles[5]
        @boston_relocatable_profile.update_attribute(:locations_of_interest, ['boston'])

        @irrelevant_profile = career_profiles[6]
        @irrelevant_profile.update({:locations_of_interest => ['cleveland'], :place_details => {'lng' => 0, 'lat' => 0}})

        update_career_profile_search_helpers
    end

    def add_do_not_create_relationships_to_profiles
        career_profiles = career_profiles = get_profiles_with_equal_ordering_priorities(2)

        @disabled_profile = career_profiles[0]
        @disabled_profile.update_attribute(:do_not_create_relationships, true)

        @enabled_profile = career_profiles[1]
        @enabled_profile.update_attribute(:do_not_create_relationships, false)

        update_career_profile_search_helpers
    end

    def add_interested_in_joining_new_company_to_profiles
        career_profiles = career_profiles = get_profiles_with_equal_ordering_priorities(4)
        @neutral = career_profiles[0]
        @neutral.update_attribute(:interested_in_joining_new_company, 'neutral')

        @interested = career_profiles[1]
        @interested.update_attribute(:interested_in_joining_new_company, 'interested')

        @very_interested = career_profiles[2]
        @very_interested.update_attribute(:interested_in_joining_new_company, 'very_interested')

        @not_interested = career_profiles[3]
        @not_interested.update_attribute(:interested_in_joining_new_company, 'not_interested')
        update_career_profile_search_helpers
    end

    def add_hiring_relationships_to_profiles
        career_profiles = get_profiles_with_equal_ordering_priorities(2)

        add_hiring_relationship(career_profiles[0])
        add_hiring_relationship(career_profiles[0])
        add_hiring_relationship(career_profiles[1])
        update_career_profile_search_helpers

        career_profiles
    end

    def add_hiring_relationship(career_profile)
        user = User.where.not(id: career_profile.user.hiring_manager_relationships.reload.map(&:hiring_manager_id)).first
        user.ensure_candidate_relationships([career_profile.user.id])
        user.candidate_relationships.reorder('created_at').last.update_attribute(:hiring_manager_status, 'accepted')
    end

    def get_profiles_with_equal_ordering_priorities(candidate_ids_or_count)
        if candidate_ids_or_count.is_a?(Integer)
            query = CareerProfile.limit(candidate_ids_or_count)
        else
            query = CareerProfile.where(user_id: candidate_ids_or_count)
        end
        career_profiles = query.reorder("created_at desc")

        place_details = career_profiles[0].place_details

        # make the career profiles the same by all the other ordering rules
        career_profiles.each do |cp|
            cp.user.hiring_manager_relationships.destroy_all
            cp.education_experiences.destroy_all # make in_school the same for all
            cp.update(
                place_details: place_details,
                do_not_create_relationships: false,
                interested_in_joining_new_company: 'very_interested'
            )
        end

        career_profiles
    end

    def update_career_profile_search_helpers(career_profiles = [])
        # if you have not changed anything about the career profile that would
        # touch its updated at, then you have to pass it in here.  Otherwise
        # you can save a couple milliseconds by not passing it in
        career_profiles.map(&:touch)
        CareerProfile::SearchHelper.write_new_records
    end

end