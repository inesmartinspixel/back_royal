require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::PersistedTimelineEventsController do
    include ControllerSpecHelper
    json_endpoint

    describe "POST create" do

        it "should 401 for non-admin" do
            stub_current_user(controller, [:learner])
            post :create, params: { format: :json }
            assert_error_response(401)
        end

        it "should work in normal case" do
            allow(controller).to receive(:current_user).and_return(users(:admin))

            record = {
                a: 1
            }
            meta = {
                b: 2
            }
            event = double('event')
            expect(event).to receive(:as_json).and_return({'some' => 'json'})
            expect(event.class).to receive(:table_name).and_return('persisted_timeline_events')
            expect(ActivityTimeline::PersistedTimelineEvent).to receive(:create_from_hash!).with(
                hash_including(record.merge(
                    editor_id: controller.current_user.id,
                    editor_name: controller.current_user.name
                )),
                hash_including(meta)
            ).and_return(event)

            post :create,
                params: {
                    :format => :json,
                    :record => record,
                    :meta => meta
                }
            assert_200

            assert_successful_response({'persisted_timeline_events' => [{'some' => 'json'}]})
        end

    end

    describe "DELETE destroy" do

        it "should 401 for non-admin" do
            stub_current_user(controller, [:learner])
            delete :destroy, params: { id: SecureRandom.uuid, format: :json }
            assert_error_response(401)
        end

        it "should work if allowed" do
            stub_current_user(controller, [:admin])
            record = ActivityTimeline::PersistedTimelineEvent.create!(
                time: Time.at(42),
                event: 'note',
                category: 'note'
            )

            expect {
                delete :destroy, params: { id: record.id, format: :json }
            }.to change(ActivityTimeline::PersistedTimelineEvent, :count).by(-1)
            assert_200
        end

    end

end
