require 'controllers/api/helpers/api_content_controller_spec_helper'

describe Api::ReportsController do
    include ApiContentControllerSpecHelper

    before(:each) do
        allow(Report).to receive(:const_get).with(:UserLessonProgressRecord).and_return(Report::UserLessonProgressRecord)
        allow(Report).to receive(:const_get).with(:ThingsReport).and_return(Report)
        allow(Report).to receive(:const_get).with(:Report).and_return(Report)
        allow(Report).to receive(:const_get).with(:REPORT_CONFIG).and_return({})
    end

    def disable_maintenance_mode
        expect(controller).to receive(:check_maintenance_mode)
    end

    describe "POST 'create'" do

        it "should work for super_editors" do
            disable_maintenance_mode
            expect_report_render
            stub_current_user(controller, [:super_editor])

            post :create, params: { :format => :json, :record => { report_type: 'ThingsReport' } }
            assert_successful_response({
                'reports' => ['json']
            })
        end

        it "should 401 for non-super_editors" do
            disable_maintenance_mode
            stub_current_user(controller, [:editor])
            post :create, params: { :format => :json }
            assert_error_response(401, {'message' => "You are not authorized to do that."})
        end

        it "should return an empty response during maintenance mode" do
            allow(AppConfig).to receive(:reporting_maintenance).and_return('true')
            assert_maintenance_mode
        end

        it "should enable maintenance mode id UserLessonProgressRecord is empty" do
            Report::UserLessonProgressRecord.delete_all
            assert_maintenance_mode
        end

        it "should enable maintenance mode on a PG::ObjectNotInPrerequisiteState error" do
            disable_maintenance_mode

            # setup an error in the query
            err = ActiveRecord::StatementInvalid.new("")
            expect(err).to receive(:cause).and_return(PG::ObjectNotInPrerequisiteState.new(""))
            allow(Report).to receive(:create_from_hash!).and_raise(err)

            # assert
            assert_maintenance_mode
        end

        describe "with reports viewer" do

            before(:each) do
                @user = users(:institutional_reports_viewer)
                @institution = @user.views_reports_for_institutions.first
                stub_current_user(controller, @user)
                disable_maintenance_mode
            end

            it "should 401 without an institutional filter" do
                post :create, params: { :format => :json, :record => { report_type: 'ThingsReport' } }
                assert_error_response(401, {'message' => "You are not authorized to do that."})
            end

            it "should work with an allowed institutional filter" do
                expect_report_render
                post :create,
                    params: {
                        :format => :json,
                        :record => {
                            report_type: 'ThingsReport',
                            filters: [
                                {filter_type:"InstitutionFilter",value:[@institution.id]}
                            ]
                        }
                    }
                assert_successful_response({
                    'reports' => ['json']
                })
            end

            it "should 401 with a disallowed institutional filter" do
                another_institution = Institution.where("id != ?", @institution.id).first
                post :create,
                    params: {
                        :format => :json,
                        :record => {
                            report_type: 'ThingsReport',
                            filters: [
                                {filter_type:"InstitutionFilter",value:[another_institution.id]}
                            ]
                        }
                    }

                assert_error_response(401, {'message' => "You are not authorized to do that."})
            end
        end


        def expect_report_render
            expect_any_instance_of(Report).to receive(:as_json).and_return('json')
        end

        def assert_maintenance_mode
            # expect_report_render
            stub_current_user(controller, [:super_editor])

            post :create, params: { :format => :json, :record => { report_type: 'ThingsReport' } }
            assert_successful_response({
                'reports' => []
            })
            expect(body_json['meta']['reporting_maintenance']).to eq('true')
        end


    end

    describe "GET 'filter_options'" do

        before(:each) do
            disable_maintenance_mode
        end

        describe "without institution_id" do
            it "should work for super_editors" do
                stub_current_user(controller, [:super_editor])
                result = [{"a" => 1}]
                expect(Report).to receive(:get_filter_options).and_return(result)
                get :filter_options, params: { :format => :json }
                assert_successful_response({
                    'filter_options' => result
                })
            end

            it "should 401 for non-super_editors" do
                stub_current_user(controller, [:editor])
                expect(Report).not_to receive(:get_filter_options)
                get :filter_options, params: { :format => :json }
                assert_error_response(401, {'message' => "You are not authorized to do that."})
            end


            it "should 401 for reports viewers" do
                user = users(:institutional_reports_viewer)
                stub_current_user(controller, user)
                expect(Report).not_to receive(:get_filter_options)
                get :filter_options, params: { :format => :json }
                assert_error_response(401, {'message' => "You are not authorized to do that."})
            end
        end

        describe "with an institution_id" do

            it "should work for super_editor" do
                institution = Institution.first
                stub_current_user(controller, [:super_editor])
                result = [{"a" => 1}]
                expect(Report).to receive(:get_filter_options_for_institution).with(institution, controller.current_user.pref_locale).and_return(result)
                get :filter_options, params: { :institution_id => institution.id, :format => :json }
                assert_successful_response({
                    'filter_options' => result
                })
            end

            it "should work for an institutional report viewer requesting options for her institution" do
                user = users(:institutional_reports_viewer)
                institution = user.views_reports_for_institutions.first
                stub_current_user(controller, user)
                result = [{"a" => 1}]
                expect(Report).to receive(:get_filter_options_for_institution).with(institution, controller.current_user.pref_locale).and_return(result)
                get :filter_options, params: { :institution_id => institution.id, :format => :json }
                assert_successful_response({
                    'filter_options' => result
                })
            end

            it "should 401 for non-super_editors" do
                institution = Institution.first
                stub_current_user(controller, [:editor])
                expect(Report).not_to receive(:get_filter_options_for_institution)
                get :filter_options, params: { :institution_id => institution.id, :format => :json }
                assert_error_response(401, {'message' => "You are not authorized to do that."})
            end

            it "should 401 for an institutional report viewer requesting options for another institution" do
                user = users(:institutional_reports_viewer)
                institution = user.views_reports_for_institutions.first
                another_institution = Institution.where("id != ?", institution.id).first
                stub_current_user(controller, user)
                expect(Report).not_to receive(:get_filter_options_for_institution)
                get :filter_options, params: { :institution_id => another_institution.id, :format => :json }
                assert_error_response(401, {'message' => "You are not authorized to do that."})
            end

        end


    end

end
