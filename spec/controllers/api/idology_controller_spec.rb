require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::IdologyController do
    include ControllerSpecHelper


    describe "GET hook" do

        it "should handle a message with an id_number" do
            id_number = "12345"
            expect(GetIdologyResultJob).to receive(:perform_later).with(id_number)
            expect(Raven).not_to receive(:capture_exception)
            post_idology_response({
                "id_number"=>id_number,
                "id_scan_result"=>{"key"=>"capture.completed", "message"=>"Completed"}
            }.to_xml(root: 'response'))
            assert_200
        end

        it "should handle a message with no id_number" do
            id_number = "12345"
            idology_response = {
                "id_number"=>nil,
                "id_scan_result"=>{"key"=>"something.else", "message"=>"Completed"}
            }
            expect(GetIdologyResultJob).not_to receive(:perform_later)
            expect(Raven).to receive(:capture_exception).with("IDology hook with no id_number.",
                extra: {
                    response: idology_response
                }
            )
            post_idology_response(idology_response.to_xml(root: 'response'))
            assert_200
        end

        it "should handle malformed XML" do
            idology_response = 'blah blah'
            expect(GetIdologyResultJob).not_to receive(:perform_later)
            expect(Raven).to receive(:capture_exception).with("IDology hook with no id_number.",
                extra: {
                    response: idology_response
                }
            )
            post_idology_response(idology_response)
            assert_200
        end

        it "should handle a blank body" do
            idology_response = ''
            expect(GetIdologyResultJob).not_to receive(:perform_later)
            expect(Raven).to receive(:capture_exception).with("IDology hook with no id_number.",
                extra: {
                    response: idology_response
                }
            )
            post_idology_response(idology_response)
            assert_200
        end

        def post_idology_response(response)
            post :hook, body: response
            response
        end

        def set_idology_response(response)
            # mock_body = {}
            # expect(mock_body).to receive(:read).and_return({"response" => response}.to_xml)
            # expect(controller.request).to receive(:body).and_return(mock_body)
            # debugger
            # response
            {"response" => response}.to_xml
        end
    end

    describe "POST request_idology_link" do

        it "should 401 if disallowed" do
            called = false
            expect(controller.current_ability).to receive(:can?) do |meth, obj|
                if meth == :request_idology_link_with_params
                    called = true
                    false
                else
                    true
                end
            end
            get :request_idology_link, params: { format: :json }
            assert_401
        end

        it "should work" do
            expect(controller.current_ability).to receive(:can?).and_return(true)
            allow(controller).to receive(:current_user).and_return(User.first)
            get :request_idology_link, params: { user_id: controller.current_user.id, email: 'email', phone: 'phone', format: :json }

            job = Delayed::Job.find_by_queue(:request_idology_link)
            expect(job.payload_object.job_data['arguments'][0].symbolize_keys.slice(:user_id, :email, :phone)).to eq({
                user_id: controller.current_user.id,
                email: 'email',
                phone: 'phone'
            })
        end
    end

    describe "GET ping" do
        it "should 401 if not allowed" do
            user_id = SecureRandom.uuid
            allow(controller.current_ability).to receive(:can?) do |meth, obj|
                if meth == :ping_idology_with_params
                    expect(obj[:user_id]).to eq(user_id)
                    false
                else
                    true
                end
            end
            get :ping, params: { :format => :json, user_id: user_id }
            assert_error_response(401)
        end

        it "should work" do
            instance = IdologyVerification.create!(
                user_id: User.limit(1).pluck('id').first,
                idology_id_number: 12345,
                scan_capture_url: 'scan_capture_url'
            )
            user = instance.user
            allow(controller.current_ability).to receive(:can?).and_return(true)
            expect_any_instance_of(IdologyVerification).to receive(:ensure_job_if_necessary)
            get :ping, params: { :format => :json, user_id: user.id }
        end
    end
end