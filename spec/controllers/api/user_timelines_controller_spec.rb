require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'
require 'stripe_helper'

describe Api::UserTimelinesController do

    attr_reader :user

    include ControllerSpecHelper
    json_endpoint

    describe "show" do

        before(:each) do
            user = User.first
            allow(controller).to receive(:current_user).and_return(user)
        end

        it "should work" do
            expect(controller.current_ability).to receive(:can?).with(:show, ActivityTimeline::UserTimeline).and_return(true)
            user = User.where.not(id: controller.current_user.id).first
            user_timeline = ActivityTimeline::UserTimeline.new(user.id).preload
            get :show,
                params: {
                    :id => user.id,
                    :format => :json
                }
            assert_200
            result_json = body_json['contents']['user_timelines'][0]
            expect(result_json).to eq(user_timeline.as_json)
        end

        it "should 401 if unauthorized" do
            expect(controller.current_ability).to receive(:can?).with(:show, ActivityTimeline::UserTimeline).and_return(false)
            get :show,
                params: {
                    :id => SecureRandom.uuid,
                    :format => :json
                }
            assert_error_response(401)
        end

    end
end