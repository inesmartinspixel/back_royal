require 'spec_helper'
require 'controllers/api/helpers/api_content_controller_spec_helper'

describe Api::GlobalMetadataController do

    include ControllerSpecHelper
    json_endpoint

    before(:each) do
        stub_client_params(controller)
        stub_current_user(controller, [:admin])
        @global_metadata = GlobalMetadata.create! valid_attributes

    end

    describe "PUT update" do
        describe "with valid params" do
            it "updates the requested global metadata" do
                new_default_title = "new seo title"
                put :update, params: {id: @global_metadata.to_param, record: @global_metadata.as_json.merge({ "default_title" => new_default_title }), format: :json}
                expect(response.status).to eq(200) # ok
                expect(body_json["contents"]["global_metadata"].size).to eq(1)
                expect(body_json["contents"]["global_metadata"][0]["default_title"]).to eq(new_default_title)

            end

        end

    end

    describe "GET index" do
        it "assigns all global_metadata as @global_metadata" do
            get :index, params: {format: :json}
            expect(body_json["contents"]["global_metadata"].size).to eq(GlobalMetadata.count)
            expect(body_json["contents"]["global_metadata"].map { |e| e['id']}.include?(@global_metadata.id)).to be(true)
        end
    end

    describe "POST create" do
        describe "with valid params" do

            it "creates a new Global Metadata" do
                    expect {
                    post :create, params: {record: valid_attributes, format: :json}
                }.to change(GlobalMetadata.all, :count).by(1)

                expect(response.status).to eq(200) # ok
                expect(body_json["contents"]["global_metadata"].size).to eq(1)
                expect(body_json["contents"]["global_metadata"][0]["default_title"]).to eq(valid_attributes["default_title"])

            end

            it "prevents you from creating a new Global Metadata entry with a duplicate site_name" do
                record = GlobalMetadata.first
                expect {
                    post :create, params: {record: {site_name: record.site_name}, format: :json}
                }.to change(GlobalMetadata.all, :count).by(0)

                expect(response.status).to eq(406) # not_acceptable
                expect(body_json["message"]).to eq("Validation failed: Global Metadata with this site name already exists.")

            end

        end
    end


    describe "DELETE destroy" do
        it "destroys the requested global metadata" do
            global_metadata = GlobalMetadata.create! valid_attributes
            expect {
                delete :destroy, params: {id: global_metadata.to_param, format: :json}
            }.to change(GlobalMetadata, :count).by(-1)
            # response.status.should == 204 # no_content
            expect(response.status).to eq(200) # ok
        end

    end

    def valid_attributes
        {
            "site_name" => SecureRandom.uuid,
            "default_title" => "seo title",
            "default_description" => "seo description",
            "default_canonical_url" => "pedago.com"
        }
    end


end
