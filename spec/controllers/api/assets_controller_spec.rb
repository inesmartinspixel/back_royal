require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::AssetsController do
    include ControllerSpecHelper

    fixtures(:users)

    before(:each) do
        stub_client_params(controller)
        @file = fixture_file_upload('files/test.pdf', 'application/pdf')
    end

    describe "POST create" do

        it "should 401 for non-admin" do
            user = users(:learner)
            allow(controller).to receive(:current_user).and_return(user)

            expect {
                post :create, params: {
                    :format => :json
                }
            }.not_to change { S3Asset.count }
            assert_401
        end

        it "should work for admin" do
            user = users(:admin)
            allow(controller).to receive(:current_user).and_return(user)

            expect{
                post :create, params: {
                    :format => :json,
                    :files => [@file]
                }
            }.to change { S3Asset.count }.by(1)
            assert_200
        end
    end
end