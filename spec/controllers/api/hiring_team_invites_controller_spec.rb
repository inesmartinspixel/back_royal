require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::HiringTeamInvitesController do
    include ControllerSpecHelper
    json_endpoint

    fixtures(:users)

    before(:each) do
        stub_client_params(controller)
    end

    describe "POST create" do

        it "should 401 if not allowed" do
            stub_current_user(controller, [:learner])
            expect(controller.current_ability).to receive(:can?).with(:create_hiring_team_invite_with_params, anything).and_return(false)
            post :create, params: { format: :json }
            assert_error_response(401)
        end

        it "should work" do
            user = HiringApplication.first.user
            expect(controller.current_ability).to receive(:can?).with(:create_hiring_team_invite_with_params, anything).and_return(true)
            stub_current_user(controller, [:learner])
            record_params = {
                inviter_id: user.id,
                invitee_email: 'invitee@example.com',
                invitee_name: 'invitee'
            }
            expect(HiringTeamInvite).to receive(:invite!).with(hash_including(record_params)).and_call_original

            expect {
                post :create,
                    params: {
                        :format => :json,
                        :record => record_params
                    }
                assert_200
            }.to change { User.count }.by(1) # invitee created
        end

        it "should send down details of InviteeExists error" do
            expect(controller.current_ability).to receive(:can?).with(:create_hiring_team_invite_with_params, anything).and_return(true)
            stub_current_user(controller, [:learner])
            err = HiringTeamInvite::InviteeExists.new(User.first)
            expect(HiringTeamInvite).to receive(:invite!).and_raise(err)

            post :create,
                params: {
                    :format => :json,
                    :record => {}
                }
            assert_406

            expect(body_json['meta']).to have_entries(err.details.stringify_keys)
        end

    end

end
