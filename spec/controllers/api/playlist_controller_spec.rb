require 'controllers/api/helpers/api_content_controller_spec_helper'

describe Api::PlaylistsController do

    include ApiContentControllerSpecHelper

    before(:each) do
        # mock out this validation, because it's annoying to try to set up
        # and does not need to be tested here anyhow
        allow_any_instance_of(Playlist).to receive(:all_stream_entries_have_valid_groups).and_return(true)
        stub_current_user(controller, [:admin])
    end

    add_images_specs
    add_destroy_specs

    add_create_specs({
        valid: { title: 'My playlist 8', description: 'An awesome playlist', author: {} },
        invalid: { description: 'An awesome playlist' }
    })

    add_update_specs({
        valid: Proc.new { |instance_json|
            instance_json['title'] = 'Some other title'
            instance_json
        },
        invalid: Proc.new { |instance_json|
            instance_json['title'] = nil
            instance_json
        }
    })

    describe "index" do

        it "should return instances" do
            expect_any_instance_of(Playlist::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                expect(to_json_instance.params[:id]).to be(nil)
                expect(to_json_instance.params[:user]).to be(controller.current_user)
                "[]"
            end
            get :index, params: { :format => :json }
            assert_successful_response({
                "playlists" => []
            })
        end

        it "should raise unauthorized if requesting unpublished content without permission" do
            stub_current_user(controller, [:learner])
            get :index, params: { :filters => {:published => false}, :format => :json }
            assert_error_response(401, {'message' => "You are not authorized to do that."})
        end

        it "should allow permitted user to see unpublished content" do
            expect_any_instance_of(Playlist::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                expect(to_json_instance.filters[:published]).to be(false)
                "[]"
            end
            get :index, params: { :filters => {:published => false}, :format => :json }
            assert_successful_response({
                "playlists" => []
            })
        end

        it "should error if not logged in" do
            stub_no_current_user(controller)
            get :index, params: { :format => :json }
            assert_logged_out_error
        end

        # filters comes in as a json-formatted hash.  The controller is supposed to
        # parse the json
        it "should support filters" do
            expect_any_instance_of(Playlist::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                expect(to_json_instance.filters[:published]).to eq(true)
                "[]"
            end
            get :index, params: { :filters => {published: true}.to_json, :format => :json }
            assert_successful_response({
                "playlists" => []
            })
        end

        describe "user_id" do

            it "should allow admin to request playlists for someone else" do
                expect_any_instance_of(Playlist::ToJsonFromApiParams).to receive(:json).and_return("[]")
                stub_current_user(controller, [:admin])
                another_user_id = users(:learner).id
                get :index, params: { :filters => {published: true}.to_json, :user_id => another_user_id, :format => :json }
                assert_successful_response({
                    "playlists" => []
                })
            end

            it "should not allow non-admin user to request playlists for someone else" do
                learner = users(:learner)
                stub_current_user(controller, [:learner])
                admin_user_id = users(:admin).id
                get :index, params: { :filters => {published: true}.to_json, :user_id => admin_user_id, :format => :json }
                assert_error_response(401, {'message' => "You are not authorized to do that."})
            end
        end

    end


    describe "show" do

        before(:each) do
            @instance = Playlist.first
        end

        it "should return an instance if it finds one" do
            expect_any_instance_of(Playlist::ToJsonFromApiParams).to receive(:json) do |to_json_instance|
                expect(to_json_instance.params[:id]).to eq(@instance.id)
                expect(to_json_instance.params[:user]).to be(controller.current_user)
                "[{\"id\":\"#{@instance.id}\"}]"
            end
            get :show, params: { :id => @instance.id, :format => :json }
            assert_successful_response({
                "playlists" => [{"id"=>@instance.id}]
            })
        end

        it "should 404 if instance cannot be found" do
            id  = SecureRandom.uuid
            expect_any_instance_of(Playlist::ToJsonFromApiParams).to receive(:json).and_return('[]')
            get :show, params: {:id => id, :format => :json}
            assert_error_response(404, {'message' => "No #{"playlists".singularize} found for id=\"#{id}\""})
        end
    end
end