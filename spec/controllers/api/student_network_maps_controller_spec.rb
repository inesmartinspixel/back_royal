require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::StudentNetworkMapsController do
    include ControllerSpecHelper
    json_endpoint

    fixtures(:users)

    before(:each) do
        stub_client_params(controller)
    end

    describe "GET index" do
        attr_accessor :user

        describe "authorize_index_with_params" do

            it "should raise unauthorized error if disallowed" do
                expect(controller).to receive(:render_unauthorized_error)
                expect(controller.current_ability).to receive(:cannot?).with(:index_student_network_maps, controller.params).and_return(true)
                controller.authorize_index_with_params
            end

            it "should 401 if no current_user" do
                stub_no_current_user(controller)
                process :index, :params => {:format => :json, :filters => {}.to_json}
                assert_error_response(401)
            end
        end

        describe "with permission" do

            before(:each) do
                # skip auth checks.  authorize_index_with_params is tested on it's own,
                # and specific rules are tested in ability_spec.rb
                allow(controller).to receive(:authorize_index_with_params).and_return(true)
                stub_current_user(controller, [:learner])
            end

            it "should work" do
                get :index, :params => {:format => :json, :filters => {}.to_json}
                assert_200
            end

        end

    end
end