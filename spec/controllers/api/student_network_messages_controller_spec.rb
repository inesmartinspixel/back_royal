require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe Api::StudentNetworkMessagesController do
    attr_accessor :recipient, :sender

    include ControllerSpecHelper
    json_endpoint

    before(:each) do
        stub_client_params(controller)
    end

    describe "POST create" do

        before(:each) do
            @recipient = User.first
            @sender = User.second
            allow(controller).to receive(:current_user).and_return(@sender)
        end

        describe "authorize_create_with_params" do

            it "should render_unauthorized_error if current_ability cannot create_student_network_messages" do
                controller.params = {
                    record: {
                        recipient_id: recipient.id,
                        sender_id: sender.id,
                        reply_to: sender.email,
                        subject: 'Subject Foo',
                        message_body: 'Message Body Bar'
                    },
                    :format => :json
                }
                expect(User).to receive(:find_by_id).with(recipient.id).and_return(recipient)
                expect(controller.current_ability).to receive(:cannot?).with(:create_student_network_messages, controller.create_params, recipient).and_return(true)
                expect(controller).to receive(:render_unauthorized_error)
                controller.authorize_create_with_params
            end
        end

        describe "with permission" do

            attr_accessor :record_params, :event

            before(:each) do
                # skip auth checks.  authorize_create_with_params is tested on it's own,
                # and specific rules are tested in ability_spec.rb
                allow(controller).to receive(:authorize_create_with_params)
                controller.message_recipient = recipient
                allow(controller).to receive(:current_user).and_return(sender)

                @record_params = {
                    recipient_id: recipient.id,
                    sender_id: sender.id,
                    reply_to: sender.email,
                    subject: 'Subject Foo',
                    message_body: 'Message Body Bar'
                }
                @event = Event.new
            end

            it "should log a message_sent event" do
                expect(Event).to receive(:create_server_event!).with(
                    anything,
                    sender.id,
                    'student_network:message_sent',
                    {
                        recipient_id: recipient.id
                    }
                ).ordered
                expect(Event).to receive(:create_server_event!).with(any_args).ordered.and_return(event)
                expect(controller).to receive(:create_params).at_least(:once).and_return(record_params)
                post :create, :params => { :record => record_params, :format => :json }
                assert_200
            end

            describe "message_received event" do

                attr_accessor :event

                before(:each) do
                    expect(Event).to receive(:create_server_event!).with(any_args).ordered
                end

                it "should work" do
                    expect(Event).to receive(:create_server_event!).with(
                        anything,
                        recipient.id,
                        'student_network:message_received',
                        record_params.merge(to: recipient.email)
                    ).and_return(event)
                    expect(event).to receive(:log_to_external_systems)
                    expect(controller).to receive(:create_params).at_least(:once).and_return(record_params)
                    post :create, :params => { :record => record_params, :format => :json }
                    assert_200
                end

                it "should favor recipient's student_network_email" do
                    recipient.student_network_email = 'non-anon-recipient@example.com'
                    recipient.save!

                    expect(Event).to receive(:create_server_event!).with(
                        anything,
                        recipient.id,
                        'student_network:message_received',
                        record_params.merge(to: recipient.student_network_email)
                    ).and_return(event)
                    expect(event).to receive(:log_to_external_systems)
                    expect(controller).to receive(:create_params).at_least(:once).and_return(record_params)
                    post :create, :params => { :record => record_params, :format => :json }
                    assert_200
                end

                it "should sanitize HTML" do
                    record_params_unsanitized = record_params.merge({
                        subject: 'Subject <b>Foo</b>',
                        message_body: '<strong>Message Body Bar</strong>'
                    })
                    record_params_sanitized = record_params
                    expect(Event).to receive(:create_server_event!).with(
                        anything,
                        recipient.id,
                        'student_network:message_received',
                        record_params_sanitized.merge(to: recipient.email)
                    ).and_return(event)
                    expect(event).to receive(:log_to_external_systems)
                    expect(controller).to receive(:create_params).at_least(:once).and_return(record_params_unsanitized)
                    post :create, :params => { :record => record_params_unsanitized, :format => :json }
                    assert_200
                end

            end

        end
    end
end
