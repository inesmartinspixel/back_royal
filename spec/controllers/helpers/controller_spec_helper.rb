# FIXME: `Parameters` is a constant source of churn and pain in Rails-land. Recently, they've deprecated and then r
# emoved  the `with_indifferent_access` method as `Parameters` no longer extends from `Hash`. Most all of our Rails
# controller tests emulate `Parameters` with vanilla `Hash` values, which can be problematic. We should favor mocking
# with strong `Parameters` with any new controller specs.

module ControllerSpecHelper
    extend ActiveSupport::Concern

	def self.included(target)
		target.send(:include, Devise::Test::ControllerHelpers)
        target.fixtures(:users)
	end

    module ClassMethods
        def json_endpoint
            before(:each) do
                # see http://stackoverflow.com/questions/33872958/rails-5-controller-spec-example-passing-params-set-to-nil-value-gets-their-v
                # and https://github.com/rspec/rspec-rails/issues/1655 and
                @request.env["CONTENT_TYPE"] = "application/json"
            end
        end
    end

	def stub_client_params(controller)
		allow(controller).to receive(:client_params).and_return({
				'fr-client-version' => '42',
				'fr-client' => 'web'
		})
	end

	class CurrentUserStub

		attr_reader :current_user

		def initialize(test, controller, current_user)
				test.allow_any_instance_of(controller.class).to test.receive(:current_user).and_return(current_user)
		end

		def stub(*args, &block)
				current_user.stub(*args, &block)
		end

		def as_json
			created_at = current_user.created_at
			updated_at = current_user.updated_at
			current_user.as_json.merge("created_at" => created_at.as_json, "updated_at" => updated_at.as_json)
		end
	end

	def stub_current_user(controller = nil, roles_or_user = [], groups = [])
		controller ||= @controller
        if roles_or_user.is_a?(User)
            user = roles_or_user
            sign_in user
        else
            roles = roles_or_user
            user = users(:current_user)
    		user.roles = []
    		roles.each do |role|
    			user.add_role(role)
    		end
    		groups.each do |group|
    			user.add_to_group(group)
    		end
    		sign_in user
        end
        CurrentUserStub.new(self, controller, user)
	end

	def stub_no_current_user(controller = nil)
        controller ||= @controller
		allow_any_instance_of(controller.class).to receive(:current_user).and_return(nil)
	end

	def assert_200
		expect(response.status).to eq(200), "Status: #{response.status}. Body: #{response.body}"
	end

    def assert_401
        expect(response.status).to eq(401), "Status: #{response.status}. Body: #{response.body}"
    end

    def assert_422
        expect(response.status).to eq(422), "Status: #{response.status}. Body: #{response.body}"
    end

    def assert_406
        expect(response.status).to eq(406), "Status: #{response.status}. Body: #{response.body}"
    end

    def assert_404
        expect(response.status).to eq(404), "Status: #{response.status}. Body: #{response.body}"
    end

	def body_json
		ActiveSupport::JSON.decode(response.body)
	rescue Oj::ParseError
		raise "Cannot parse json: #{response.body.inspect}"
	end


	def assert_error_response(code, expected_response_body = nil)
        expect(response.status).to eq(code), {status: response.status, body: response.body}.inspect
        return unless expected_response_body

        expected_response_body = expected_response_body.with_indifferent_access
        indifferent_body_json = body_json.respond_to?(:with_indifferent_access) ? body_json.with_indifferent_access : body_json

        if !expected_response_body.key?(:meta)
            indifferent_body_json.delete(:meta)
        end
        expect(indifferent_body_json).to eq(expected_response_body)
	end

	def assert_logged_out_error
		assert_error_response(401, {"message"=>"You must be logged in to do that", "not_logged_in"=>true})
	end

	def check_body(expected_response_body, options = {})
        indifferent_body_json = body_json.respond_to?(:with_indifferent_access) ? body_json.with_indifferent_access : body_json
        expected_response_body = expected_response_body.as_json(options)
        expect(indifferent_body_json).to eq(expected_response_body)
	end

    def check_body_contents(expected_contents, options = {})
        indifferent_body_json = body_json.respond_to?(:with_indifferent_access) ? body_json.with_indifferent_access : body_json
        expected_contents = expected_contents.as_json(options)
        expect(indifferent_body_json['contents']).to eq(expected_contents)
    end

    def assert_successful_response(expected_contents, expected_meta = nil, options = {})
        assert_200

        # if expected_meta is nil, we do not bother asserting on it at all

        if expected_meta
            expected_meta['push_messages'] ||= {}
            expected_meta['push_messages'].deep_merge!({
                'event_logger' => {
                    'disable_auto_events_for' => AppConfig.disable_auto_events_for
                }
            })
        end

        if expected_meta && controller.current_user
            expected_meta['push_messages'].deep_merge!({
                'current_user' => {
                    'can_edit_career_profile' => controller.current_user.can_edit_career_profile,
                    'last_relationship_updated_at' => controller.current_user.get_last_relationship_updated_at_f
                }
            })
        end

        if controller.current_user
            options = options.merge(:user_id => controller.current_user.id)
        end

        if expected_meta
            expected_meta = {
                'min_allowed_client_version' => ClientConfig::WebClientConfig::MIN_ALLOWED_VERSION,
                'min_suggested_client_version' => ClientConfig::WebClientConfig::MIN_SUGGESTED_VERSION,
                'client_app_store_name'=> nil
            }.merge(expected_meta)
        end

        if expected_meta
            check_body({
                'contents' => expected_contents,
                'meta' => expected_meta
            }, options)
        else
            check_body_contents(expected_contents, options)
        end
    end

end