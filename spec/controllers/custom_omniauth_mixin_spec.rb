require 'spec_helper'

describe CustomOmniauthMixin do
    attr_accessor :instance

    class CustomOmniauthMixinKlass
        include CustomOmniauthMixin
        def initialize
            @resource = OpenStruct.new
        end
    end

    before(:each) do
        @instance = CustomOmniauthMixinKlass.new

        # preventing intermittent spec failures when email is changing
        allow_any_instance_of(User).to receive(:stripe_customer)
    end

    describe "setup_new_record" do
        describe "with experiment_id" do
            attr_accessor :user
            before(:each) do
                @user = User.left_outer_joins(:institutions).where(institutions: {id: nil}).first
                allow_any_instance_of(User).to receive(:register_content_access) do
                    # register_content_access will add an institution
                    user.ensure_institution(Institution.quantic)
                end
            end

            it "should set an experiment" do
                instance.setup_new_record(user, valid_attrs({
                    'experiment_id' => 'foo'
                }))
                user.reload
                expect(user.experiment_ids).to eq(['foo'])
            end

            it "should handle null experiment_id" do
                instance.setup_new_record(user, valid_attrs({
                    'experiment_id' => 'null'
                }))
                user.reload
                expect(user.experiment_ids).to eq([])
            end

            it "should configure institution fields" do
                instance.setup_new_record(user, valid_attrs)
                user.reload
                expect(user.institutions).to eq([Institution.quantic])
                expect(user.active_institution).to eq(Institution.quantic)
            end

            def valid_attrs(overrides = {})
                {
                    'uid' => 'uid',
                    'email' => 'test@career.profile',
                    'sign_up_code' => 'FREEMBA',
                    'password' => '12345678',
                }.merge(overrides)
            end
        end

        describe "career_profile" do
            it "should set fields" do
                user = User.joins(:career_profile).first
                user.career_profile.update_columns(
                    primary_reason_for_applying: nil,
                    survey_years_full_time_experience: nil,
                    survey_most_recent_role_description: nil,
                    survey_highest_level_completed_education_description: nil,
                    salary_range: nil
                )
                instance.setup_new_record(user, {
                    'uid' => 'uid',
                    'email' => 'test@career.profile',
                    'sign_up_code' => 'FREEMBA',
                    'password' => '12345678',
                    'primary_reason_for_applying' => 'baz',
                    'survey_years_full_time_experience' => 'foo',
                    'survey_most_recent_role_description' => 'bar',
                    'survey_highest_level_completed_education_description' => 'baz',
                    'salary' => 'over_200000'
                })
                user.reload
                expect(user.career_profile.primary_reason_for_applying).to eq('baz')
                expect(user.career_profile.survey_years_full_time_experience).to eq('foo')
                expect(user.career_profile.survey_most_recent_role_description).to eq('bar')
                expect(user.career_profile.survey_highest_level_completed_education_description).to eq('baz')
                expect(user.career_profile.salary_range).to eq('over_200000')
            end

            it "should use existing values if present" do
                user = User.joins(:career_profile).first
                user.career_profile.update_columns(
                    primary_reason_for_applying: 'existing',
                    survey_years_full_time_experience: 'existing',
                    survey_most_recent_role_description: 'existing',
                    survey_highest_level_completed_education_description: 'existing',
                    salary_range: 'existing'
                )
                instance.setup_new_record(user, {
                    'uid' => 'uid',
                    'email' => 'test@career.profile',
                    'sign_up_code' => 'FREEMBA',
                    'password' => '12345678',
                    'primary_reason_for_applying' => 'baz',
                    'survey_years_full_time_experience' => 'foo',
                    'survey_most_recent_role_description' => 'bar',
                    'survey_highest_level_completed_education_description' => 'baz',
                    'salary' => 'over_200000'
                })
                user.reload
                expect(user.career_profile.primary_reason_for_applying).to eq('existing')
                expect(user.career_profile.survey_years_full_time_experience).to eq('existing')
                expect(user.career_profile.survey_most_recent_role_description).to eq('existing')
                expect(user.career_profile.survey_highest_level_completed_education_description).to eq('existing')
                expect(user.career_profile.salary_range).to eq('existing')
            end
        end
    end

    describe "sanitize_null" do
        it "should return value if present" do
            expect(instance.send(:sanitize_null, "foo")).to eq("foo")
        end

        it "should return type nil if value is null string" do
            expect(instance.send(:sanitize_null, "null")).to be_nil
        end
    end
end
