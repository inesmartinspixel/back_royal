require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe MarketingController do
    include ControllerSpecHelper

    describe "self.google_optimize_active_route_patterns" do

        after(:all) do
            ENV['GOOGLE_OPTIMIZE_ACTIVE_ROUTES'] = ''
        end

        it "should escape the sanitized URL and replace wildcard matchers in for each URL listed in the GOOGLE_OPTIMIZE_ACTIVE_ROUTES ENV variable and add it to returned cache" do
            expect(ENV).to receive(:[]).with('GOOGLE_OPTIMIZE_ACTIVE_ROUTES').at_least(:once).and_return('smart.ly/foo,smart.ly/*/baz,smart.ly/qux?foo=bar')
            urls = ENV['GOOGLE_OPTIMIZE_ACTIVE_ROUTES'].split(',')
            urls.each { |url| expect(controller.class).to receive(:get_sanitized_url).with(url).ordered.and_return(url) }
            expected_result = urls.map { |url| Regexp.escape(url).gsub('\*', '.*?') }
            expect(controller.class.google_optimize_active_route_patterns).to eq(expected_result)
        end
    end

    describe "self.get_sanitized_url" do

        it "should return unchanged url if passed in url is already sanitized" do
            expect(controller.class.get_sanitized_url('smart.ly')).to eq('smart.ly')
            expect(controller.class.get_sanitized_url('smart.ly/about')).to eq('smart.ly/about')
            expect(controller.class.get_sanitized_url('smart.ly/about?foo=bar')).to eq('smart.ly/about?foo=bar')
        end

        it "should return the passed in url stripped of leading and trailing whitespace, with 'http://', 'https://', and 'www.' removed from beginning of url, and trailing slashes removed" do
            # leading and trailing whitespace
            expect(controller.class.get_sanitized_url('   smart.ly')).to eq('smart.ly')
            expect(controller.class.get_sanitized_url('smart.ly/about   ')).to eq('smart.ly/about')
            expect(controller.class.get_sanitized_url('    smart.ly/join/account?foo=bar   ')).to eq('smart.ly/join/account?foo=bar')

            # 'http://', 'https://', and 'www.' combinations
            expect(controller.class.get_sanitized_url('http://smart.ly')).to eq('smart.ly')
            expect(controller.class.get_sanitized_url('https://smart.ly')).to eq('smart.ly')
            expect(controller.class.get_sanitized_url('www.smart.ly')).to eq('smart.ly')
            expect(controller.class.get_sanitized_url('http://www.smart.ly')).to eq('smart.ly')
            expect(controller.class.get_sanitized_url('https://www.smart.ly')).to eq('smart.ly')

            # trailing slashes
            expect(controller.class.get_sanitized_url('smart.ly/')).to eq('smart.ly')
            expect(controller.class.get_sanitized_url('smart.ly/about///')).to eq('smart.ly/about')

            # everything at once
            expect(controller.class.get_sanitized_url('   http://smart.ly/about///'   )).to eq('smart.ly/about')
            expect(controller.class.get_sanitized_url('   https://smart.ly/about///'   )).to eq('smart.ly/about')
            expect(controller.class.get_sanitized_url('   http://www.smart.ly/about///'   )).to eq('smart.ly/about')
            expect(controller.class.get_sanitized_url('   https://www.smart.ly/about///'   )).to eq('smart.ly/about')
            expect(controller.class.get_sanitized_url('   http://smart.ly/join/account?foo=bar'   )).to eq('smart.ly/join/account?foo=bar')
            expect(controller.class.get_sanitized_url('   https://smart.ly/join/account?foo=bar'   )).to eq('smart.ly/join/account?foo=bar')
            expect(controller.class.get_sanitized_url('   http://www.smart.ly/join/account?foo=bar'   )).to eq('smart.ly/join/account?foo=bar')
            expect(controller.class.get_sanitized_url('   https://www.smart.ly/join/account?foo=bar'   )).to eq('smart.ly/join/account?foo=bar')
        end
    end

    describe "dynamic_landing_page" do

        it "should set @form to single-step if 'error' query param is present" do
            base_params = {
                'program' => 'h',
                'header_text' => 'n',
                'background' => 'b',
                'form' => 'm', # request the multi-step form
                'testimonials_or_videos' => 'v',
                'bottom_section' => 'r',
                'form_question_set' => 's'
            }
            allow(controller).to receive(:params).and_return(base_params)
            allow(controller.request).to receive(:query_parameters).and_return({})
            allow(controller).to receive(:render)
            controller.dynamic_landing_page
            expect(controller.form).to eq('multi-step')
            allow(controller.request).to receive(:query_parameters).and_return({
                "error" => "Login canceled"
            })
            allow(controller).to receive(:render)
            controller.dynamic_landing_page
            expect(controller.form).to eq('single-step')
        end
    end

    describe "dynamic_landing_page_params" do
        it "should work as expected" do
            allow(controller).to receive(:params).and_return({
                'program' => 'h',
                'header_text' => 'n',
                'background' => 'b',
                'form' => 's',
                'testimonials_or_videos' => 'v',
                'bottom_section' => 'r',
                'form_question_set' => 's',
                'experiment_id' => 's'
            })
            expect(controller.dynamic_landing_page_param(:program)).to eq('hybrid')
            expect(controller.dynamic_landing_page_param(:header_text)).to eq('Not all elite business schools are draped in ivy.')
            expect(controller.dynamic_landing_page_param(:background)).to eq('blue')
            expect(controller.dynamic_landing_page_param(:form)).to eq('single-step')
            expect(controller.dynamic_landing_page_param(:testimonials_or_videos)).to eq('videos')
            expect(controller.dynamic_landing_page_param(:bottom_section)).to eq('Are you ready')
            # expect(controller.dynamic_landing_page_param(:form_question_set)).to eq('motivation') # FIXME - When more options are added
            expect(controller.dynamic_landing_page_param(:experiment_id)).to eq('show non-mba program choices')
        end

        it "should fall back to defaults with unknown values" do

            allow(controller).to receive(:params).and_return({
                'program' => 'WRONG',
                'header_text' => 'WRONG',
                'background' => 'WRONG',
                'form' => 'WRONG',
                'testimonials_or_videos' => 'WRONG',
                'bottom_section' => 'WRONG',
                'form_question_set' => 'WRONG'
            })
            assert_defaults
        end

        it "should fall back to defaults with nil values" do

            allow(controller).to receive(:params).and_return({})
            assert_defaults
        end

        def assert_defaults
            expect(controller.dynamic_landing_page_param(:program)).to eq('emba')
            expect(controller.dynamic_landing_page_param(:header_text)).to eq('Not all elite business schools are draped in ivy.')
            expect(controller.dynamic_landing_page_param(:background)).to eq('blue')
            expect(controller.dynamic_landing_page_param(:form)).to eq('multi-step')
            expect(controller.dynamic_landing_page_param(:testimonials_or_videos)).to eq('testimonials')
            expect(controller.dynamic_landing_page_param(:bottom_section)).to eq('none')
            expect(controller.dynamic_landing_page_param(:form_question_set)).to eq('motivation')
            expect(controller.dynamic_landing_page_param(:experiment_id)).to eq('hide non-mba program choices')
        end
    end

    describe "set_google_optimize_active_route" do

        before(:each) do
            controller.request = double("Controller request", :original_url => "https://smart.ly")
        end

        it "should set @google_optimize_active_route to false when in test environment even if google_optimize_active_route?" do
            expect(Rails.env).to receive(:test?).and_return(true)
            allow(controller.class).to receive(:google_optimize_active_route?).and_return(true)
            controller.set_google_optimize_active_route
            expect(controller.instance_variable_get(:@google_optimize_active_route)).to be(false)
        end

        describe "when not in test environment" do

            before(:each) do
                expect(Rails.env).to receive(:test?).and_return(false)
            end

            it "should set @google_optimize_active_route to true if google_optimize_active_route?" do
                expect(controller.class).to receive(:google_optimize_active_route?).and_return(true)
                controller.set_google_optimize_active_route
                expect(controller.instance_variable_get(:@google_optimize_active_route)).to be(true)
            end

            it "should set @google_optimize_active_route to false if !google_optimize_active_route?" do
                expect(controller.class).to receive(:google_optimize_active_route?).and_return(false)
                controller.set_google_optimize_active_route
                expect(controller.instance_variable_get(:@google_optimize_active_route)).to be(false)
            end
        end
    end
end