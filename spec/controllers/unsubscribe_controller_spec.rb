require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe UnsubscribeController do
    include ControllerSpecHelper
    render_views

    describe "GET 'index'" do

        # We're assuming that all string notification preferences will following the convention of using 'never'
        notification_string_types = ['notify_candidate_positions_recommended']
        notification_boolean_types = User::NOTIFICATIONS.keys - notification_string_types

        before :each do
            @user = User.first

            notification_boolean_types.each do |type|
                @user[type] = true
            end

            notification_string_types.each do |type|
                @user[type] = nil
            end

            @user.save!
        end

        it "should redirect without any params" do
            get 'index'
            expect(response).to be_redirect
        end

        it "should redirect with invalid params" do

            get 'index', params: {:id => 'invalid', :email => @user.email}
            expect(response).to be_redirect

            get 'index', params: {:id => @user.id, :email => 'invalid@invalid.com'}
            expect(response).to be_redirect

        end

        it "should succeed with valid params" do
            get 'index', params: {:id => @user.id, :email => @user.email}
            expect(response).to be_successful
        end

        it "should succeed with valid params" do
            get 'index', params: {:id => @user.id, :email => @user.email}
            expect(response).to be_successful
        end

        it "should default to all notifications" do

            get 'index', params: {:id => @user.id, :email => @user.email}
            expect(response).to be_successful

            @user.reload

            notification_boolean_types.each do |type|
                expect(@user.read_attribute(type)).to be(false)
            end

            notification_string_types.each do |type|
                expect(@user.read_attribute(type)).to eq('never')
            end
        end


        it "should default to all notifications with an invalid type" do

            get 'index', params: {:id => @user.id, :email => @user.email, :type => "invalid"}
            expect(response).to be_successful

            @user.reload

            notification_boolean_types.each do |type|
                expect(@user.read_attribute(type)).to be(false)
            end

            notification_string_types.each do |type|
                expect(@user.read_attribute(type)).to eq('never')
            end
        end

        it "should default a boolean notification that is nil to false" do
            @user.update_column(:notify_candidate_positions, nil)
            get 'index', params: {:id => @user.id, :email => @user.email, :type => "notify_candidate_positions"}
            expect(response).to be_successful
            expect(@user.reload.notify_candidate_positions).to be(false)
        end

        it "should only set the appropriate notification type if provided a type" do

            get 'index', params: {:id => @user.id, :email => @user.email, :type => "notify_hiring_updates"}
            expect(response).to be_successful

            @user.reload

            notification_boolean_types.each do |type|
                expect(@user.read_attribute(type)).to be(type != "notify_hiring_updates")
            end

        end


        it "should only set the appropriate notification type if provided a type for a string notification" do

            get 'index', params: {:id => @user.id, :email => @user.email, :type => "notify_candidate_positions_recommended"}
            expect(response).to be_successful

            @user.reload

            notification_boolean_types.each do |type|
                expect(@user.read_attribute(type)).to be(true)
            end

            notification_string_types.each do |type|
                expect(@user.read_attribute(type)).to eq('never')
            end
        end

        it "should support the legacy newsletter type" do

            get 'index', params: {:id => @user.id, :email => @user.email, :type => "newsletter"}
            expect(response).to be_successful

            @user.reload

            notification_boolean_types.each do |type|
                expect(@user.read_attribute(type)).to be(type != "notify_email_newsletter")
            end

        end


    end
end
