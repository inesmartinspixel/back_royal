require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe DeviseTokenAuth::CustomRegistrationsController do

    include ControllerSpecHelper
    include StripeHelper

    before(:each) do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        create_default_plan
    end

    describe "create" do

        describe "with register_content_access integration" do

            it "should register_content_access and return a fully update user" do

                existing_user_ids = User.pluck('id')
                expected_favorite_locale_packs = Lesson::Stream::LocalePack.limit(2)
                expected_group = AccessGroup.first

                expect_any_instance_of(User).to receive(:register_content_access) do
                    user = controller.instance_variable_get(:@user_resource)
                    # register_content_access might add favorites and groups. We have to
                    # actually add them here just to make sure they get into the
                    # json response
                    user.favorite_lesson_stream_locale_packs = expected_favorite_locale_packs
                    user.add_to_group expected_group.name
                end

                put :create, params: create_params

                # if favorites and groups have been added, then they need
                # to be in the json response
                new_user = User.where('id NOT IN (?)', existing_user_ids).first
                expect(new_user).not_to be(nil)
                user_json = ActiveSupport::JSON.decode(@response.body)['data']
                expect(user_json['id']).to eq(new_user.id)
                expect(user_json['favorite_lesson_stream_locale_packs']).to match_array(expected_favorite_locale_packs.map { |s| {'id' => s.id }})
                expect(user_json['groups'][0]['name']).to eq(expected_group.name)

            end
        end

        describe "with register_content_access mocked out" do

            before(:each) do
                expect_any_instance_of(User).to receive(:register_content_access)
            end

            it "should allow self-signup" do
                put :create, params: create_params
                assert_200
            end

            it "should assign an auid to the user" do
                id = SecureRandom.uuid
                put :create, params: create_params.merge(id: id)
                assert_200
                expect(User.reorder("created_at desc").first.id).to eq(id)
            end

            it "should fail gracefully if the auid is already in the db" do
                id = User.pluck('id').first
                now = Time.now
                put :create, params: create_params.merge(id: id)
                assert_200
                expect(User.where("created_at > ?", now).first.id).not_to eq(id)
            end

            it "should log to slack" do
                expect_any_instance_of(User).to receive(:tell_slack_about_create)
                put :create, params: create_params
            end

            it "should handle signup codes not mapping to join_config" do
                put :create, params: create_params.merge({
                        sign_up_code: 'ADMIN',
                    })
                assert_200
            end

            describe "name fields" do
                it "should process name field" do
                    put :create, params: create_params.merge({
                        email: 'newname@pedago.com',
                        name: 'New Name',
                    })

                    assert_200
                    user = User.find_by_email('newname@pedago.com')
                    expect(user.name).to eq('New Name')
                end

            end
        end

        describe "with existing user" do

            before(:each) do
                # When an existing user tries to login but with the wrong credentials,
                # we shouldn't even try to register_content_access. Instead, we should
                # just skip to rendering the error response.
                expect_any_instance_of(User).not_to receive(:register_content_access)
            end

            it "should give a useful message when logging in with email provider and existing user is oauth" do
                user = User.first
                user.update({
                    'provider' => 'facebook'
                })

                put :create, params: create_params.merge({
                    email: user.email,
                    sign_up_code: 'FREEMBA',
                    password: 'password',
                    provider: 'email'
                })

                assert_422
                expect(body_json['errors']['full_messages']).to include(
                   "Please try logging in with Facebook."
                )
            end

            it "should give a useful message when logging in with email provider and existing user is email" do
                user = User.where(provider: 'email').first

                put :create, params: create_params.merge({
                    email: user.email,
                    password: 'password',
                    provider: 'email'
                })

                assert_422
                expect(body_json['errors']['full_messages']).to include(
                   "Email has already been taken"
                )
            end

            # We don't have any other institutions using phone_no_password, but here's how we would test it if we did
            #
            # it "should give a useful message when logging in with phone_no_password provider" do
            #     user = User.first
            #     user.update({
            #         'provider' => 'phone_no_password',
            #         'phone' => '+12025551234'
            #     })

            #     put :create, params: create_params.merge({
            #         phone: user.phone,
            #         sign_up_code: 'SOME_CODE',
            #         provider: 'phone_no_password',
            #         password: nil,
            #         email: nil
            #     })

            #     assert_422
            #     expect(body_json['errors']['full_messages']).to include(
            #        "An account with that phone number already exists."
            #     )
            # end
        end

        describe "with 'HIRING' sign_up_code" do
            it "should create hiring_application" do
                put :create, params: create_params.merge({
                    email: 'bob@acme.com',
                    sign_up_code: 'HIRING',
                    password: '12345678',
                    job_role: 'job_role_response',
                    job_title: 'job_title_response',
                    company_year: '1999',
                    company_employee_count: 'company_employee_count_response',
                    company_annual_revenue: 'company_annual_revenue_response',
                    company_sells_recruiting_services: false,
                    place_id: 'ChIJoyh8pLf7MIgRlIwC3eq9pFM',
                    place_details: {
                        locality: {short:'Cleveland Heights', long: 'Cleveland Heights' },
                        administrative_area_level_2: { short: 'Cuyahoga County', long: 'Cuyahoga County'},
                        administrative_area_level_1: { short: 'OH', long: 'Ohio'},
                        country: {short: 'US', long: 'United States'},
                        formatted_address: 'Cleveland Heights, OH, USA',
                        utc_offset: -240
                    },
                    professional_organization: ProfessionalOrganizationOption.reorder(:text).first,
                })

                assert_200

                user = User.find_by_email('bob@acme.com')
                expect(user).not_to be_nil
                expect(user.hiring_application).not_to be_nil
                expect(user.hiring_application.applied_at).to be_within(10.seconds).of(Time.now)
                expect(user.hiring_application.status).to eq('pending')
                expect(user.hiring_application.job_role).to eq('job_role_response')
                expect(user.hiring_application.company_year).to eq(1999)
                expect(user.hiring_application.company_employee_count).to eq('company_employee_count_response')
                expect(user.hiring_application.company_annual_revenue).to eq('company_annual_revenue_response')
                expect(user.hiring_application.company_sells_recruiting_services).to eq(false)
                response_body = ActiveSupport::JSON.decode(response.body)['data']

                # ensure that the new hiring application is actually in the response
                expect(response_body['hiring_application']).not_to be_nil
                expect(response_body['hiring_application']['id']).to eq(user.hiring_application.id)
            end

            describe "with professional_organization param" do
                it "should create a new professional_organization if it does not exist and add it to the user" do
                    non_existing_company_text = "A NON-EXISTANT COMPANY"
                    expect(ProfessionalOrganizationOption.find_by_text(non_existing_company_text)).to be_nil

                    put :create, params: create_params.merge({
                        email: 'bob@acme.com',
                        sign_up_code: 'HIRING',
                        password: '12345678',
                        job_role: 'job_role_response',
                        job_title: 'job_title_response',
                        company_year: '1999',
                        company_employee_count: 'company_employee_count_response',
                        company_annual_revenue: 'company_annual_revenue_response',
                        company_sells_recruiting_services: false,
                        place_id: 'ChIJoyh8pLf7MIgRlIwC3eq9pFM',
                        place_details: {
                            locality: {short:'Cleveland Heights', long: 'Cleveland Heights' },
                            administrative_area_level_2: { short: 'Cuyahoga County', long: 'Cuyahoga County'},
                            administrative_area_level_1: { short: 'OH', long: 'Ohio'},
                            country: {short: 'US', long: 'United States'},
                            formatted_address: 'Cleveland Heights, OH, USA',
                            utc_offset: -240
                        },
                        professional_organization: {
                            locale: "en",
                            text: non_existing_company_text
                        }
                    })

                    assert_200

                    user = User.find_by_email('bob@acme.com')
                    expect(user.professional_organization).not_to be_nil
                    expect(ProfessionalOrganizationOption.find_by_text(non_existing_company_text)).not_to be_nil
                end

                it "should use an existing professional_organization if it exists and add it to the user" do
                    existing_company_text = "AN EXISTANT COMPANY"
                    ProfessionalOrganizationOption.create!(:locale => "en", :text => existing_company_text)
                    expect(ProfessionalOrganizationOption.where(:text => existing_company_text).size).to be(1)

                    put :create, params: create_params.merge({
                        email: 'bob@acme.com',
                        sign_up_code: 'HIRING',
                        password: '12345678',
                        job_role: 'job_role_response',
                        job_title: 'job_title_response',
                        company_year: '1999',
                        company_employee_count: 'company_employee_count_response',
                        company_annual_revenue: 'company_annual_revenue_response',
                        company_sells_recruiting_services: false,
                        place_id: 'ChIJoyh8pLf7MIgRlIwC3eq9pFM',
                        place_details: {
                            locality: {short:'Cleveland Heights', long: 'Cleveland Heights' },
                            administrative_area_level_2: { short: 'Cuyahoga County', long: 'Cuyahoga County'},
                            administrative_area_level_1: { short: 'OH', long: 'Ohio'},
                            country: {short: 'US', long: 'United States'},
                            formatted_address: 'Cleveland Heights, OH, USA',
                            utc_offset: -240
                        },
                        professional_organization: {
                            locale: "en",
                            text: existing_company_text
                        }
                    })

                    assert_200

                    user = User.find_by_email('bob@acme.com')
                    expect(user.professional_organization).not_to be_nil
                    expect(ProfessionalOrganizationOption.where(:text => existing_company_text).size).to be(1)
                end
            end
        end

        describe "with default sign_up_code" do
            it "should create a career profile" do
                allow(AppConfig).to receive(:join_config).and_return({
                    "default" => {
                        "signup_code" => "FREEMBA"
                    },
                    "hiring" => {
                        "signup_code" => "HIRING"
                    }
                })

                put :create, params: create_params.merge({
                    email: 'test@career.profile',
                    sign_up_code: 'FREEMBA',
                    password: '12345678'
                })

                assert_200

                user = User.find_by_email('test@career.profile')
                expect(user.career_profile).not_to be_nil
            end
        end

        describe "with skip_apply" do
            it "should not set the skip_apply flag if not provided" do
                allow(AppConfig).to receive(:join_config).and_return({
                    "default" => {
                        "signup_code" => "FREEMBA"
                    },
                    "hiring" => {
                        "signup_code" => "HIRING"
                    }
                })

                put :create, params: create_params.merge({
                    email: 'test@career.profile',
                    sign_up_code: 'FREEMBA',
                    password: '12345678'
                })

                assert_200

                user = User.find_by_email('test@career.profile')
                expect(user.career_profile).not_to be_nil
                expect(user.skip_apply).to be(false)
            end

            it "should set the skip_apply flag if provided" do
                allow(AppConfig).to receive(:join_config).and_return({
                    "default" => {
                        "signup_code" => "FREEMBA"
                    },
                    "hiring" => {
                        "signup_code" => "HIRING"
                    }
                })

                put :create, params: create_params.merge({
                    email: 'test@career.profile',
                    sign_up_code: 'FREEMBA',
                    password: '12345678',
                    skip_apply: 'true'
                })

                assert_200

                user = User.find_by_email('test@career.profile')
                expect(user.career_profile).not_to be_nil
                expect(user.skip_apply).to be(true)
            end
        end

        describe "with program_type" do
            it "should default the program type and confirm if valid" do
                allow(AppConfig).to receive(:join_config).and_return({
                    "default" => {
                        "signup_code" => "FREEMBA"
                    },
                    "hiring" => {
                        "signup_code" => "HIRING"
                    }
                })

                allow_any_instance_of(User).to receive(:register_content_access)

                put :create, params: create_params.merge({
                    email: 'test@career.profile',
                    sign_up_code: 'FREEMBA',
                    password: '12345678',
                    program_type: 'the_business_certificate'
                })

                assert_200

                user = User.find_by_email('test@career.profile')
                expect(user.career_profile).not_to be_nil
                expect(user.fallback_program_type).to eq('the_business_certificate')
                expect(user.program_type_confirmed).to be(true)
            end

            it "should ignore the program type if invalid" do
                allow(AppConfig).to receive(:join_config).and_return({
                    "default" => {
                        "signup_code" => "FREEMBA"
                    },
                    "hiring" => {
                        "signup_code" => "HIRING"
                    }
                })

                put :create, params: create_params.merge({
                    email: 'test@career.profile',
                    sign_up_code: 'FREEMBA',
                    password: '12345678',
                    program_type: 'invalid_program_type'
                })

                assert_200

                user = User.find_by_email('test@career.profile')
                expect(user.career_profile).not_to be_nil
                expect(user.fallback_program_type).not_to eq('invalid_program_type') # we mock register_content_access, so just check it's not set to the wrong thing
                expect(user.program_type_confirmed).to be(false)
            end
        end

        describe "with referred_by_id" do
            it "should set referred_by if valid" do
                referrer = User.first

                allow_any_instance_of(User).to receive(:register_content_access)

                put :create, params: create_params.merge({
                    email: 'test@career.profile',
                    sign_up_code: 'FREEMBA',
                    password: '12345678',
                    referred_by_id: referrer.id
                })

                assert_200

                user = User.find_by_email('test@career.profile')
                expect(user.referred_by.id).to eq(referrer.id)
            end

            it "should ignore the referred_by_id if invalid" do
                allow_any_instance_of(User).to receive(:register_content_access)

                put :create, params: create_params.merge({
                    email: 'test@career.profile',
                    sign_up_code: 'FREEMBA',
                    password: '12345678',
                    referred_by_id: 'not-a-valid-uuid'
                })

                assert_200

                user = User.find_by_email('test@career.profile')
                expect(user.referred_by).to be_nil
            end
        end

        describe "with timezone" do
            it "should set timezone if valid" do
                timezone = 'America/Chicago'
                referrer = User.first

                allow_any_instance_of(User).to receive(:register_content_access)
                expect_any_instance_of(Timezone::Zone).to receive(:valid?).and_return(true)

                put :create, params: create_params.merge({
                    email: 'test@career.profile',
                    sign_up_code: 'FREEMBA',
                    password: '12345678',
                    timezone: timezone
                })

                assert_200

                user = User.find_by_email('test@career.profile')
                expect(user.timezone).to eq(timezone)
            end
        end

        describe "with experiment_id" do
            before(:each) do
                allow_any_instance_of(User).to receive(:register_content_access)
            end

            def assert_experiment_ids(values)
                assert_200
                user = User.find_by_email('test@experiment.com')
                expect(user.experiment_ids).to eq(values)
            end

            it "should set an experiment" do
                put :create, params: create_params.merge({
                    email: 'test@experiment.com',
                    sign_up_code: 'FREEMBA',
                    password: '12345678',
                    experiment_id: 'foo'
                })
                assert_experiment_ids(['foo'])
            end

            it "should handle null experiment_id" do
                put :create, params: create_params.merge({
                    email: 'test@experiment.com',
                    sign_up_code: 'FREEMBA',
                    password: '12345678',
                    experiment_id: 'null'
                })
                assert_experiment_ids([])
            end
        end

        describe "apply_career_profile_params" do
            it "should set fields" do
                expect_any_instance_of(User).to receive(:register_content_access) do
                    user = controller.instance_variable_get(:@user_resource)
                    user.ensure_career_profile
                    user.career_profile.primary_reason_for_applying = nil
                    user.career_profile.survey_years_full_time_experience = nil
                    user.career_profile.survey_most_recent_role_description = nil
                    user.career_profile.survey_highest_level_completed_education_description = nil
                    user.career_profile.salary_range = nil
                end
                put :create, params: create_params.merge({
                    email: 'test@career.profile',
                    sign_up_code: 'FREEMBA',
                    password: '12345678',
                    primary_reason_for_applying: 'baz',
                    survey_years_full_time_experience: 'foo',
                    survey_most_recent_role_description: 'bar',
                    survey_highest_level_completed_education_description: 'baz',
                    salary: 'over_200000'
                })
                assert_200
                user = User.find_by_email('test@career.profile')
                expect(user.career_profile.primary_reason_for_applying).to eq('baz')
                expect(user.career_profile.survey_years_full_time_experience).to eq('foo')
                expect(user.career_profile.survey_most_recent_role_description).to eq('bar')
                expect(user.career_profile.survey_highest_level_completed_education_description).to eq('baz')
                expect(user.career_profile.salary_range).to eq('over_200000')
            end

            it "should use existing values if present" do
                expect_any_instance_of(User).to receive(:register_content_access) do
                    user = controller.instance_variable_get(:@user_resource)
                    user.ensure_career_profile
                    user.career_profile.primary_reason_for_applying = 'existing'
                    user.career_profile.survey_years_full_time_experience = 'existing'
                    user.career_profile.survey_most_recent_role_description = 'existing'
                    user.career_profile.survey_highest_level_completed_education_description = 'existing'
                    user.career_profile.salary_range = 'existing'
                end
                put :create, params: create_params.merge({
                    email: 'test@career.profile',
                    sign_up_code: 'FREEMBA',
                    password: '12345678',
                    primary_reason_for_applying: 'baz',
                    survey_years_full_time_experience: 'foo',
                    survey_most_recent_role_description: 'bar',
                    survey_highest_level_completed_education_description: 'baz',
                    salary: 'over_200000'
                })
                assert_200
                user = User.find_by_email('test@career.profile')
                expect(user.career_profile.primary_reason_for_applying).to eq('existing')
                expect(user.career_profile.survey_years_full_time_experience).to eq('existing')
                expect(user.career_profile.survey_most_recent_role_description).to eq('existing')
                expect(user.career_profile.survey_highest_level_completed_education_description).to eq('existing')
                expect(user.career_profile.salary_range).to eq('existing')
            end
        end

        def create_params
            {
                confirm_success_url: "http://localhost:3001/",
                email: "#{SecureRandom.uuid}@track.com",
                sign_up_code: 'FREEMBA',
                name: "test6",
                password: "password",
                pref_locale: "en"
            }
        end

    end

    describe "update" do

        describe "name fields" do
            before (:each) do
                stub_current_user(controller, [:learner])

                # NOTE: bypass annoying devise_token_auth `set_user_by_token` requirements
                controller.instance_variable_set(:@resource, controller.current_user)
            end

            it "should process name field" do
                user = controller.current_user

                put :update, params: {
                    id: user.id,
                    email: user.email,
                    name: 'New Name'
                }
                assert_200

                updated_user = User.find_by_email(user.email)
                expect(updated_user.name).to eq('New Name')
            end

            it "should touch the career profile if should_touch_career_profile is true" do
                # ensure the user has a career profile
                controller.current_user.career_profile = nil
                controller.current_user.career_profile = CareerProfile.create!(user_id: controller.current_user.id)
                user = controller.current_user
                old_updated_at = user.career_profile.updated_at

                allow(controller).to receive(:should_touch_career_profile).and_return(true) # mock this out (it has its own specs)
                put :update, params: {
                    id: user.id,
                    email: user.email,
                    name: 'New Name'
                }
                assert_200
                expect(controller).to have_received(:should_touch_career_profile)

                updated_user = User.find_by_email(user.email)
                expect(updated_user.name).to eq('New Name')
                expect(updated_user.career_profile.updated_at).to be > old_updated_at
            end

            it "should not touch the career profile if should_touch_career_profile is false" do
                # ensure the user has a career profile
                CareerProfile.create!(user_id: controller.current_user.id) if controller.current_user.career_profile.nil?
                user = controller.current_user.reload
                expected_updated_at = user.career_profile.updated_at.to_timestamp

                allow(controller).to receive(:should_touch_career_profile).and_return(false) # mock this out (it has its own specs)
                put :update, params: {
                    id: user.id,
                    email: user.email
                }
                assert_200
                expect(controller).to have_received(:should_touch_career_profile)

                updated_user = User.find_by_email(user.email)
                expect(updated_user.career_profile.updated_at.to_timestamp).to eq(expected_updated_at)
            end

        end
    end

    describe "should_touch_career_profile" do

        before (:each) do
            stub_current_user(controller, [:learner])

            # NOTE: bypass annoying devise_token_auth `set_user_by_token` requirements
            controller.instance_variable_set(:@resource, controller.current_user)
        end

        it "should return false if user has no career profile" do
            # Mock out that the user has no career profile
            allow(controller.current_user).to receive(:career_profile).and_return(nil)
            expect(controller.should_touch_career_profile).to be(false)
        end

        it "should return false if user has a career profile, but no pending cohort application" do
            # Mock out that the user has a career profile, but no pending cohort application.
            allow(controller.current_user).to receive(:career_profile).and_return(CareerProfile.first)
            allow(controller.current_user).to receive(:pending_application).and_return(nil)
            expect(controller.should_touch_career_profile).to be(false)
        end

        it "should return false if user has a career profile and a pending cohort application, but no account info was updated" do
            # Mock out that the user has a career profile and a pending cohort application.
            controller.current_user.career_profile = CareerProfile.create!(user_id: controller.current_user.id)
            cohort = Cohort.first
            controller.current_user.cohort_applications.push(CohortApplication.create_from_hash!(user_id: controller.current_user.id, cohort_id: cohort.id, status: 'pending'))

            # save the current user without making any changes to their account info so nothing shows
            # up when previous_changes is called
            controller.current_user.save!
            expect(controller.should_touch_career_profile).to be(false)
        end

        it "should return true if user has a career profile and a pending cohort application and the user's email is updated" do
            # Mock out that the user has a career profile and a pending cohort application.
            controller.current_user.career_profile = CareerProfile.create!(user_id: controller.current_user.id)
            cohort = Cohort.first
            controller.current_user.cohort_applications.push(CohortApplication.create_from_hash!(user_id: controller.current_user.id, cohort_id: cohort.id, status: 'pending'))

            # change the user's email
            controller.current_user.email = 'foobar@bazqux.com'
            controller.current_user.save! # save the email change so it shows up when previous_changes is called
            expect(controller.should_touch_career_profile).to be(true)
        end

        it "should return true if user has a career profile and a pending cohort application and the user's name is updated" do
            # Mock out that the user has a career profile and a pending cohort application.
            controller.current_user.career_profile = CareerProfile.create!(user_id: controller.current_user.id)
            cohort = Cohort.first
            controller.current_user.cohort_applications.push(CohortApplication.create_from_hash!(user_id: controller.current_user.id, cohort_id: cohort.id, status: 'pending'))

            # change the user's name
            controller.current_user.name = 'New Name'
            controller.current_user.save! # save the name change so it shows up when previous_changes is called
            expect(controller.should_touch_career_profile).to be(true)
        end

        it "should return true if user has a career profile and a pending cohort application and the user's nickname is updated" do
            # Mock out that the user has a career profile and a pending cohort application.
            controller.current_user.career_profile = CareerProfile.create!(user_id: controller.current_user.id)
            cohort = Cohort.first
            controller.current_user.cohort_applications.push(CohortApplication.create_from_hash!(user_id: controller.current_user.id, cohort_id: cohort.id, status: 'pending'))

            # change the user's nickname
            controller.current_user.nickname = 'New Nickname'
            controller.current_user.save! # save the nickname change so it shows up when previous_changes is called
            expect(controller.should_touch_career_profile).to be(true)
        end

    end

    describe "sanitize_null" do
        it "should return value if present" do
            expect(controller.send(:sanitize_null, "foo")).to eq("foo")
        end

        it "should return type nil if value is null string" do
            expect(controller.send(:sanitize_null, "null")).to be_nil
        end
    end

    describe "captcha_not_required" do

        it "should be true for cordova ios" do
            expect(AppConfig).to receive(:google_oauth_id_ios).and_return("google_oauth_id_ios")
            expect(controller).to receive(:params).and_return({client_id: "google_oauth_id_ios"}).at_least(1).times
            expect(controller.send(:captcha_not_required)).to be_truthy
        end

        it "should be true for cordova android" do
            expect(AppConfig).to receive(:google_oauth_id_ios).and_return("google_oauth_id_android")
            expect(controller).to receive(:params).and_return({client_id: "google_oauth_id_android"}).at_least(1).times
            expect(controller.send(:captcha_not_required)).to be_truthy
        end

        it "should be true for BOS_HIGH" do
            expect(controller).to receive(:params).at_least(1).times.and_return({sign_up_code: "BOSHIGH", client_id: "not cordova"})
            expect(controller.send(:captcha_not_required)).to be_truthy
        end

        it "should be true if captcha_validation_turned_off?" do
            expect(controller).to receive(:params).and_return({client_id: "not cordova"}).at_least(1).times
            expect(controller).to receive(:captcha_validation_turned_off?).and_return(true)
            expect(controller.send(:captcha_not_required)).to be_truthy
        end

        it "should be true if using the dynamic landing page and ENV variable is set" do
            expect(controller).to receive(:params).and_return({using_dynamic_landing_page: "true"}).at_least(1).times
            expect(controller.send(:captcha_not_required)).to be_falsey

            ENV['CAPTCHA_NOT_REQUIRED_FOR_DLP'] = 'true'
            expect(controller.send(:captcha_not_required)).to be_truthy
            ENV['CAPTCHA_NOT_REQUIRED_FOR_DLP'] = ''
        end

        it "should be false normally" do
            expect(controller).to receive(:params).and_return({client_id: "not cordova"}).at_least(1).times
            expect(controller).to receive(:captcha_validation_turned_off?).and_return(false)
            expect(controller.send(:captcha_not_required)).not_to be_truthy
        end

    end
end