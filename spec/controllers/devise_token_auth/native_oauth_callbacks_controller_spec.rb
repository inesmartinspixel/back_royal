require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe NativeOauthCallbacksController do

    include ControllerSpecHelper

    fixtures(:lesson_streams)

    before(:each) do
        @expiry = 10.days.from_now.to_timestamp
        @request.env["devise.mapping"] = Devise.mappings[:user]
        controller.raise_exceptions = true
    end

    describe "Facebook" do

        before(:each) do
            @provider = 'facebook'
            stub_token_info
        end

        describe "error handling" do

            before(:each) do
                controller.raise_exceptions = false
            end

            it "should show a failure if providing an invalid access token" do
                expect_graph_response({'error' => 'any invalid token error'})
                expect(controller).to receive(:render_error_response).with("Login failed", :unauthorized).and_call_original
                get_unauthorized
            end

            it "should raise on a failure without an error message" do
                expect_graph_response({}, '500')
                expect(controller).to receive(:render_error_response).with("Login failed", :unauthorized).and_call_original
                get_unauthorized
            end

        end

        describe "success handling" do
            it "should show a succeed if trying to login as existing user" do
                existing_user = users(:facebook_user)
                expect_graph_response({'id' => existing_user.uid})

                get_success

                expected_response = {'data' => {'auth_token' => 'token', 'client_id' => 'client_id', 'expiry' => @expiry, 'config' => nil }.merge(existing_user.token_validation_response) }
                parsed_response = body_json

                expect(parsed_response['data']['id']).to eq(expected_response['data']['id'])
                expect(parsed_response['data']['uid']).to eq(expected_response['data']['uid'])
                expect(parsed_response['data']['provider']).to eq(expected_response['data']['provider'])
                expect(parsed_response['data']['provider_user_info']).to eq({'id' => existing_user.uid})

            end
        end

        it "should lookup user by email if email is defined" do
            user = User.where.not(email: nil).where.not(sign_up_code: 'FREEMBA').first
            expect_graph_response({
                'id' => SecureRandom.uuid,
                'email' => user.email
            })
            get_success
            parsed_response = body_json
            expect(parsed_response['data']['uid']).to eq(user.uid)
        end

        it "should not lookup user by email if email is undefined" do
            expect_graph_response({
                'id' => SecureRandom.uuid
            })
            expect {
                get_success
            }.to change { User.count }.by(1)
        end

        describe "with find_or_create_and_sign_in" do

            it "should not raise an error if the resource is allowed" do
                user = User.includes(:hiring_application).where( :hiring_applications => { :user_id => nil } ).where.not(email: nil).where.not(deactivated: true).first

                expect_graph_response({
                    'id' => SecureRandom.uuid,
                    'email' => user.email
                })
                expect {
                    get_success
                }.not_to raise_error()
            end

            it "should raise an error if the resource is deactivated" do
                user = User.where.not(email: nil).first
                user.deactivated = true
                user.save!

                expect_graph_response({
                    'id' => SecureRandom.uuid,
                    'email' => user.email
                })
                expect {
                    get_success
                }.to raise_error(RuntimeError)
            end

            it "should raise an error if the resource is hiring manager" do
                user = users(:hiring_manager_with_team)

                expect_graph_response({
                    'id' => SecureRandom.uuid,
                    'email' => user.email
                })
                expect {
                    get_success
                }.to raise_error(RuntimeError)
            end

        end

    end


    describe "Google" do

        before(:each) do
            @provider = 'google_oauth2'
            stub_token_info
        end

        describe "error handling" do

            before(:each) do
                controller.raise_exceptions = false
            end

            it "should show a failure if omitting a valid email" do
                @params = { provider_data: "{}" }
                expect(controller).to receive(:render_error_response).with("Login failed", :unauthorized).and_call_original
                get_unauthorized
            end

        end

        describe "success handling" do
            it "should show a succeed if trying to login as existing user" do
                existing_user = users(:google_user_with_email)
                parsed_google_response = get_native_google_oauth_success({
                    "email" => existing_user.email,
                    "sub" => existing_user.uid,
                    "aud" => 'google_oauth_id'
                })

                expected_response = {
                    'data' => {
                        'auth_token' => 'token',
                        'client_id' => 'client_id',
                        'expiry' => @expiry,
                        'config' => nil
                    }.merge(existing_user.token_validation_response)
                }
                parsed_response = body_json

                expect(parsed_response['data']['id']).to eq(expected_response['data']['id'])
                expect(parsed_response['data']['uid']).to eq(expected_response['data']['uid'])
                expect(parsed_response['data']['provider']).to eq(expected_response['data']['provider'])
                expect(parsed_response['data']['provider_user_info']).to eq(parsed_google_response)

            end

            it "should lookup user by email if email is defined" do
                existing_user = User.where.not(email: nil).where.not(sign_up_code: 'FREEMBA').first
                get_native_google_oauth_success({
                    "email" => existing_user.email,
                    "sub" => existing_user.uid,
                    "aud" => 'google_oauth_id'
                })
                parsed_response = body_json
                expect(parsed_response['data']['uid']).to eq(existing_user.uid)
            end

            it "should not lookup user by email if email is undefined" do
                expect {
                    get_native_google_oauth_success({
                        "email" => nil,
                        "sub" => "some_user_id",
                        "aud" => 'google_oauth_id'
                    })
                }.to change { User.count }.by(1)
            end

            # https://trello.com/c/yqkfqqKY/2282-bug-native-oauth-login-fails-if-uid-is-an-email-and-email-is-null
            it "should login if there is an email as a uid but no value in the email field" do
                existing_user = User.where.not(email: nil).where.not(sign_up_code: 'FREEMBA').first
                get_native_google_oauth_success({
                    "sub" => existing_user.email,
                    "aud" => 'google_oauth_id'
                })
                parsed_response = body_json
                expect(parsed_response['data']['uid']).to eq(existing_user.uid)
            end

        end

        describe "with find_or_create_and_sign_in" do

            it "should not raise an error if the resource is allowed" do
                user = User.where.not(email: nil).where.not(sign_up_code: 'FREEMBA').first
                expect {
                    get_native_google_oauth_success({
                        "email" => user.email,
                        "sub" => user.uid,
                        "aud" => 'google_oauth_id'
                    })
                }.not_to raise_error()
            end

            it "should raise an error if the resource is deactivated" do
                user = User.where.not(email: nil).where.not(sign_up_code: 'FREEMBA').first
                user.deactivated = true
                user.save!

                expect {
                    get_native_google_oauth_success({
                        "email" => user.email,
                        "sub" => user.uid,
                        "aud" => 'google_oauth_id'
                    })
                }.to raise_error(RuntimeError)
            end

            it "should raise an error if the resource is hiring manager" do
                user = users(:hiring_manager_with_team)

                expect {
                    get_native_google_oauth_success({
                        "email" => user.email,
                        "sub" => user.uid,
                        "aud" => 'google_oauth_id'
                    })
                }.to raise_error(RuntimeError)
            end

        end

        def get_native_google_oauth_success(parsed_google_response)
            @id_token = 'provided_id_token'
            @native = true

            expect(AppConfig).to receive(:google_oauth_id_ios).and_return('google_oauth_id')
            expect(AppConfig).to receive(:google_oauth_id_android).and_return('google_oauth_id')

            raw_google_response = "response"
            expect(raw_google_response).to receive(:parsed_response).and_return(parsed_google_response)
            expect(HTTParty).to receive(:get).with(
                "https://www.googleapis.com/oauth2/v3/tokeninfo",
                query: { id_token: @id_token }
            ).and_return(raw_google_response)

            get_success

            parsed_google_response
        end

    end


    describe "Apple" do

        before(:each) do
            @provider = 'apple'
            stub_token_info
        end

        describe "error handling" do

            before(:each) do
                controller.raise_exceptions = false
            end

            it "should show a failure if omitting a valid email" do
                @params = { provider_data: "{}" }
                expect(controller).to receive(:render_error_response).with("Login failed", :unauthorized).and_call_original
                get_unauthorized
            end

        end

        describe "success handling" do
            it "should show a succeed if trying to login as existing user" do
                existing_user = users(:apple_user)
                parsed_apple_response = get_native_apple_oauth_success({
                    "email" => existing_user.email,
                    "user" => existing_user.uid,
                    "fullName" => { "givenName" => "" }
                })

                expected_response = {
                    'data' => {
                        'auth_token' => 'token',
                        'client_id' => 'client_id',
                        'expiry' => @expiry,
                        'config' => nil
                    }.merge(existing_user.token_validation_response)
                }
                parsed_response = body_json

                expect(parsed_response['data']['id']).to eq(expected_response['data']['id'])
                expect(parsed_response['data']['uid']).to eq(expected_response['data']['uid'])
                expect(parsed_response['data']['provider']).to eq(expected_response['data']['provider'])
                expect(parsed_response['data']['provider_user_info']).to eq(parsed_apple_response)

            end

            it "should lookup user by email if email is defined" do
                existing_user = User.where.not(email: nil).where.not(sign_up_code: 'FREEMBA').first
                get_native_apple_oauth_success({
                    "email" => existing_user.email,
                    "user" => existing_user.uid,
                    "fullName" => { "givenName" => existing_user.name }
                })
                parsed_response = body_json
                expect(parsed_response['data']['uid']).to eq(existing_user.uid)
            end

            it "should not lookup user by email if email is undefined" do
                expect {
                    get_native_apple_oauth_success({
                        "email" => nil,
                        "user" => "some_user_id",
                        "fullName" => { "givenName" => "Some Random" }
                    })
                }.to change { User.count }.by(1)
            end

        end

        describe "with find_or_create_and_sign_in" do

            it "should not raise an error if the resource is allowed" do
                user = User.where.not(email: nil).where.not(sign_up_code: 'FREEMBA').first
                expect {
                    get_native_apple_oauth_success({
                        "email" => user.email,
                        "user" => user.uid,
                        "fullName" => { "givenName" => "" }
                    })
                }.not_to raise_error()
            end

            it "should raise an error if the resource is deactivated" do
                user = User.where.not(email: nil).where.not(sign_up_code: 'FREEMBA').first
                user.deactivated = true
                user.save!

                expect {
                    get_native_apple_oauth_success({
                        "email" => user.email,
                        "user" => user.uid,
                        "fullName" => { "givenName" => "" }
                    })
                }.to raise_error(RuntimeError)
            end

            it "should raise an error if the resource is hiring manager" do
                user = users(:hiring_manager_with_team)

                expect {
                    get_native_apple_oauth_success({
                        "email" => user.email,
                        "user" => user.uid,
                        "fullName" => { "givenName" => "" }
                    })
                }.to raise_error(RuntimeError)
            end

        end


        def get_native_apple_oauth_success(parsed_apple_response)
            @params = { "provider_data" => parsed_apple_response.to_json }
            get_success
            parsed_apple_response
        end

    end


    def expect_graph_response(response_obj, code = '200')
        response = Net::HTTPResponse::CODE_TO_OBJ[code].new("1.1", code, response_obj)
        allow(response).to receive(:success?).and_return(code == '200')
        allow(response).to receive(:parsed_response).and_return(response_obj)
        expect(HTTParty).to receive(:get).and_return(response)
    end

    def stub_token_info(params = {})
        allow(controller).to receive(:set_token_on_resource) do
            token = OpenStruct.new(token: 'token', client: 'client_id', expiry: @expiry)
            controller.instance_variable_set(:@token, token)
        end
    end


    def get_action_for_provider
        if @provider == 'facebook'
            params = @params || {
                access_token: 'access_token'
            }
        elsif @provider == 'google_oauth2' && @native == true
            params = @params || {
                provider_data: "{
                    \"idToken\": \"#{@id_token}\"
                }"
            }
        elsif @provider == 'google_oauth2'
            default_email = users(:google_user_with_email).email
            params = @params || {
                provider_data: "{
                    \"email\": \"#{default_email}\",
                    \"userId\": \"some_user_id\"
                }"
            }
        elsif @provider == 'apple'
            user = users(:apple_user)
            params = @params || {
                provider_data: "{
                    \"email\": \"#{user.email}\",
                    \"fullName\": \"#{user.name}\",
                }"
            }
        end
        get @provider.to_sym, params: params
    end

    def get_success
        get_action_for_provider
        assert_200
    end

    def get_unauthorized
        get_action_for_provider
        assert_401
    end

    def get_not_acceptable
        get_action_for_provider
        assert_406
    end

end