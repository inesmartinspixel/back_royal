require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe DeviseTokenAuth::CustomSessionsController do

    include ControllerSpecHelper
    include StripeHelper

    before(:each) do
        @request.env["devise.mapping"] = Devise.mappings[:user]
    end

    describe "create" do

        it "should log an error to sentry and 406 if an invalid user attempts to login" do
            # name is only required if confirmed_profile_info is true, so we need this user
            # in order to make her invalid by removing the name
            user = User.find_by_confirmed_profile_info(true)
            user.name = nil
            user.save(validate: false) # bypass validations so that the user becomes invalid
            expect(Raven).to receive(:capture_exception)
            put :create, params: {
                provider: 'email',
                email: user.email,
                password: 'password'
            }
            assert_406
        end

        it "should 401 if a user is deactivated" do
            @user = User.where(provider: 'email').first
            @user.deactivated = true
            @user.save!

            put :create, params: {
                provider: 'email',
                email: @user.email,
                password: 'password'}
            assert_401
        end

        it "should 401 if a user is a non-miyamiya user on a miyamiya client" do
            user = User.left_outer_joins(:institutions).where("institutions.id is null").first

            put :create, params: {
                provider: 'email',
                email: user.email,
                password: 'password',
                client: 'miya_miya'
            }

            assert_401
        end

        it "should not 401 if a user is a miyamiya user on a miyamiya client" do
            user = User.left_outer_joins(:hiring_application).where('hiring_applications.id is null').where(provider: 'email').first
            user.ensure_institution(Institution.miya_miya)
            user.active_institution = Institution.miya_miya

            put :create, params: {
                provider: 'email',
                email: user.email,
                password: 'password',
                client: 'miya_miya'
            }

            assert_200
        end

        it "should 401 if a user is a hiring manager on a cordova client" do
            @user = users(:hiring_manager_with_team)
            @user.save!

            @request.headers['fr-client'] = "android"

            put :create, params: {
                provider: 'email',
                email: @user.email,
                password: 'password'}

            assert_401
        end

        it "should 200 if a user is a hiring manager not on a cordova client" do
            @user = users(:hiring_manager_with_team)
            @user.save!

            @request.headers['fr-client'] = "web"

            put :create, params: {
                provider: 'email',
                email: @user.email,
                password: 'password'}

            assert_200
        end

        describe "with email provider" do
            before(:each) do
                @user = User.where(provider: 'email', sign_up_code: 'FREEMBA').first
                @user.password = @user.password_confirmation = 'password'
                @user.save!
            end

            it "should allow for logging in with the correct password" do
                put :create, params: {
                    provider: 'email',
                    email: @user.email,
                    password: 'password'}
                assert_200
            end

            it "should not allow for logging in with the wrong password" do
                put :create, params: {
                    provider: 'email',
                    email: @user.email,
                    password: 'not the right password'}
                assert_401
            end
        end

        describe "with phone provider" do
            it "should allow for logging in with a phone number and no password" do
                user = User.where(provider: 'email', sign_up_code: 'FREEMBA').first
                phone = '201-555-5556'
                user.update({
                    'provider' => 'phone_no_password',
                    'phone' => phone
                })

                put :create, params: {provider: 'phone_no_password', phone: phone}
                assert_200
            end

            it "should 401 if no user found with phone" do
                user = User.where(provider: 'email', sign_up_code: 'FREEMBA').first
                phone = '201-555-5556'
                user.update({
                    'provider' => 'phone_no_password',
                    'phone' => phone
                })

                put :create, params: {provider: 'phone_no_password', phone: '201-555-5557'}
                assert_401
            end
        end

        describe "with hiring_team_invite provider" do

            it "should allow for logging in with just an email and should assign a password" do

                email = 'invitee@pedago.com'
                inviter = HiringApplication.first.user
                invitee = HiringTeamInvite.invite!(
                    inviter_id: inviter.id,
                    invitee_email: email,
                    invitee_name: 'name'
                )

                @request.headers['fr-client'] = "web"

                put :create, params: {email: email, password: 'newpassword', provider: 'hiring_team_invite'}
                assert_200
                expect(invitee.reload.valid_password?('newpassword')).to be(true)

            end

            it "should not allow a non-hiring_team_invite user to change passwords with this endpoint" do

                user = User.where(provider: 'email').first

                put :create, params: {email: user.email, password: 'newpassword', provider: 'hiring_team_invite'}
                assert_401
                expect(user.reload.valid_password?('newpassword')).to be(false)

            end

        end
    end

    describe "destroy" do
        it "should log an error to sentry and raise an error if an invalid user attempts to logout" do
            # name is only required if confirmed_profile_info is true, so we need this user
            # in order to make her invalid by removing the name
            user = User.find_by_confirmed_profile_info(true)
            # sign the user in
            put :create, params: {provider: 'email', email: user.email, password: 'password'}
            assert_200

            user.name = nil # setting the user's name to nil will make user the invalid after the record is saved

            user.tokens = controller.instance_variable_get(:@resource).tokens # set the user's session tokens to the tokens received from the sign-in
            user.save(validate: false) # save the user and bypass the validations to make the user invalid

            # mock out the controller's instance variables
            controller.instance_variable_set(:@resource, user)
            allow_any_instance_of(DeviseTokenAuth::TokenFactory::Token).to receive(:client) { user.tokens.keys[0] }

            expect(Raven).to receive(:capture_exception)

            # make the api call and expect it to raise an error
            expect {
                delete :destroy
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Name can't be blank")
        end
    end

end