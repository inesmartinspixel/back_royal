require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe DeviseTokenAuth::CustomOmniauthCallbacksController do
    attr_reader :user

    include ControllerSpecHelper
    include StripeHelper

    fixtures(:lesson_streams)

    default_new_user_hash = {
        uid: 'new_uid',
        info: {
            email: 'new_user@mobileapp.com'
        }
    }

    before(:each) do
        @facebook_user = users(:facebook_user)
        @request.env["devise.mapping"] = Devise.mappings[:user]
        set_window_type
        set_auth_hash
        @provider = 'facebook'
        controller.raise_exceptions = true

        allow(controller).to receive(:omniauth_params).and_return(omniauth_params)
        allow(AppConfig).to receive(:playlist_locale_pack_id).and_return(Playlist.all_published.first.locale_pack_id)
        create_default_plan
    end

    describe "log_saml_response" do

        before(:each) do
            ENV['LOG_ALL_SAML_RESPONSES'] = 'true'
        end

        after(:each) do
            ENV['LOG_ALL_SAML_RESPONSES'] = 'false'
        end

        it "should do nothing if LOG_ALL_SAML_RESPONSES is not 'true'" do
            ENV['LOG_ALL_SAML_RESPONSES'] = 'false'
            expect_any_instance_of(ActionDispatch::Request).not_to receive(:params)
            controller.log_saml_response
        end

        it "should do nothing if params['SAMLResponse'] is not set" do
            expect(OneLogin::RubySaml::Response).not_to receive(:new)
            expect_any_instance_of(ActionDispatch::Request).to receive(:params).at_least(1).and_return({})
            controller.log_saml_response
        end

        it "should log saml response" do
            expect_any_instance_of(ActionDispatch::Request).to receive(:params).at_least(1).and_return({
                'SAMLResponse' => example_saml_response
            })
            expect(Raven).not_to receive(:capture_exception)
            expect(LogToS3).to receive(:log).with("omniauth-debugging", ":saml_response", "**** SAML Response ****\n\n#{decoded_saml_response}\n\n**** auth_hash ****\n\n{}")
            controller.log_saml_response
        end

        it "should catch errors" do
            expect_any_instance_of(ActionDispatch::Request).to receive(:params).at_least(1).times.and_return({
                'SAMLResponse' => example_saml_response
            })
            err = RuntimeError.new('')
            expect(Raven).to receive(:capture_exception).with(err)
            expect(LogToS3).to receive(:log).and_raise(err)
            controller.log_saml_response
        end

    end

    describe "error handling" do

        before(:each) do
            controller.raise_exceptions = false
        end

        it "should capture exception and set generic message" do
            err = RuntimeError.new("")
            expect(controller).to receive(:omniauth_success).and_raise(err)
            expect(Raven).to receive(:capture_exception).with(err)
            get :omniauth_success,  params: {
                provider: @provider
            }
            expect(controller.params['message']).to eq('Login failed')
        end

    end

    describe "redirect_callbacks" do

        before(:each) do
            @request.env["omniauth.auth"] = {}
        end

        it "should hardcode devise_mapping and redirect" do
            get :redirect_callbacks,  params: { :provider => "facebook", :format => :json }
            assert_redirected_to("/api/auth/facebook/callback")
        end

    end

    describe "get_resource_from_auth_hash" do

        describe "with a new user" do

            before(:each) do
                set_auth_hash(default_new_user_hash)
            end

            it "should set the uid as the email if it is an email" do
                set_auth_hash({
                    uid: 'i_am_an@email.com',
                    info: {
                        email: nil
                    }
                })
                user = assert_new_user_created
                expect(user.email).to eq("i_am_an@email.com")
            end

            it "should not set the uid as the email if it is not an email" do
                set_auth_hash({
                    info: {email: nil}
                })
                user = assert_new_user_created
                expect(user.email).to be_nil
            end

            it "should downcase the email" do
                set_auth_hash({
                    uid: 'I_AM_AN@email.com',
                    info: {
                        email: nil
                    }
                })
                user = assert_new_user_created
                expect(user.email).to eq("i_am_an@email.com")
            end

            it "should register_content_access" do
                expect_any_instance_of(User).to receive(:register_content_access)
                assert_new_user_created
            end

            it "should set provider_user_info" do
                set_auth_hash({
                    info: {
                        name: 'Some Name',
                        email: 'new_user#@somesite.com' # deep_merge = not so hot.
                    }
                })
                user = assert_new_user_created
                expect(user.provider_user_info['name']).to eq('Some Name')
            end

            it "should strip unnecessary fields before setting providver_user_info" do
                set_auth_hash({
                    info: {
                        something_cool: 'something cool',
                        email: 'new_user#@somesite.com' # deep_merge = not so hot.
                    }
                })
                user = assert_new_user_created
                expect(user.provider_user_info['something_cool']).to be_nil
            end

            it "should assign an auid to the user" do
                id = SecureRandom.uuid
                allow(controller).to receive(:omniauth_params).and_return(omniauth_params({
                    'id' => id
                }))
                user = assert_new_user_created
                expect(user.id).to eq(id)
            end

            it "should handle literal 'undefined' in some params" do
                allow(controller).to receive(:omniauth_params).and_return(omniauth_params({
                    'sign_up_code' => 'undefined',
                    'pref_locale' => 'undefined',
                    'timezone' => 'undefined'
                }))
                user = assert_new_user_created
                expect(user.sign_up_code).to eq('FREEMBA')
                expect(user.pref_locale).to eq('en')
                expect(user.timezone).to eq(nil) # should be nil; not 'undefined'
            end

            it "should handle valid program_type in params" do
                allow(controller).to receive(:omniauth_params).and_return(omniauth_params({
                    'sign_up_code' => 'FREEMBA',
                    'program_type' => 'the_business_certificate'
                }))
                user = assert_new_user_created
                expect(user.sign_up_code).to eq('FREEMBA')
                expect(user.fallback_program_type).to eq('the_business_certificate')
                expect(user.program_type_confirmed).to be(true)
            end

            it "should ignore invalid program_type in params" do
                allow(controller).to receive(:omniauth_params).and_return(omniauth_params({
                    'sign_up_code' => 'FREEMBA',
                    'program_type' => 'invalid_program_type'
                }))
                user = assert_new_user_created
                expect(user.sign_up_code).to eq('FREEMBA')
                expect(user.fallback_program_type).not_to eq('invalid_program_type')
                expect(user.program_type_confirmed).to be(false)
            end

            it "should handle valid referred_by_id in params" do
                referrer = User.first
                allow(controller).to receive(:omniauth_params).and_return(omniauth_params({
                    'referred_by_id' => referrer.id
                }))
                user = assert_new_user_created
                expect(user.referred_by.id).to eq(referrer.id)
            end

            it "should handle invalid referred_by_id in params" do
                allow(controller).to receive(:omniauth_params).and_return(omniauth_params({
                    'referred_by_id' => 'not-a-valid-uuid'
                }))
                user = assert_new_user_created
                expect(user.referred_by).to be_nil
            end

            it "should fail gracefully if the auid is already in the db" do
                id = User.pluck('id').first
                allow(controller).to receive(:omniauth_params).and_return(omniauth_params({
                    'id' => id
                }))
                user = assert_new_user_created
                expect(user.id).not_to eq(id)
            end

            it "should set skip_apply if the param specifies" do
                allow(controller).to receive(:params).and_return(ActionController::Parameters.new({
                    skip_apply: 'true',
                    provider: 'facebook'
                }))
                id = SecureRandom.uuid
                allow(controller).to receive(:omniauth_params).and_return(omniauth_params({
                    'id' => id,
                    'skip_apply': 'true'
                }))
                user = assert_new_user_created
                expect(user.id).to eq(id)
                expect(user.skip_apply).to be(true)
            end

            it "should not set skip_apply if the param is not set" do
                id = SecureRandom.uuid
                allow(controller).to receive(:omniauth_params).and_return(omniauth_params({
                    'id' => id
                }))
                user = assert_new_user_created
                expect(user.id).to eq(id)
                expect(user.skip_apply).to be(false)
            end

            describe "confirmed_profile_info handling" do

                it "should set to true if there is a email and name" do
                    user = assert_new_user_created
                    expect(user.confirmed_profile_info).to be(true)
                end

                it "should set to false on registering a user with no email" do
                    set_auth_hash(info: {email: nil})
                    user = assert_new_user_created
                    expect(user.confirmed_profile_info).to be(false)
                end

                it "should set to false on registering a user with no first name" do
                    # we also have to set the email to nil.  Otherwise we use the email
                    # as the name, which is maybe a bad idea.
                    set_auth_hash(info: {name: nil, email: nil})
                    user = assert_new_user_created
                    expect(user.confirmed_profile_info).to be(false)
                end

                it "should not change on logins of existing users" do
                    user = users(:facebook_user)
                    expect(user.confirmed_profile_info).to be(true)
                    assert_login_existing_user(user)
                    expect(user.confirmed_profile_info).to be(true)
                end

            end

        end

        describe "with an existing oauth user" do

            it "should login with uid and provider if available" do
                user = users(:facebook_user)
                user.email = "unknown_email@example.com"
                user.save!
                assert_login_existing_user(user)
            end

            it "should login with an email that does not match the case" do
                user = users(:facebook_user)
                user.email = "unknown_email@example.com"
                user.save!
                assert_login_existing_user(user, {
                    uid: 'Unknown_Email@example.com',
                    info: {
                        email: 'Unknown_Email@example.com'
                    }
                })
            end

        end

        describe "with an existing user with a different provider" do

            before(:each) do
                @user = User.where(provider: 'email').first
                set_auth_hash(info: {
                    email: @user.email,
                    name: 'bob smith'
                })
            end

            it "should login as existing user" do
                assert_login_existing_user(user)
            end

            # see https://trello.com/c/v0yQCEJs/1170-bug-joining-with-oauth-will-change-a-user-s-sign-up-code
            it "should not update the sign_up_code" do
                user.sign_up_code = 'ORIG'
                user.save!

                # if we never call whitelisted_params, then we cannot accidentally update the sign_up_code
                expect(controller).not_to receive(:whitelisted_params)
                assert_login_existing_user(user)
                expect(user.reload.sign_up_code).to eq('ORIG')
            end

        end

        describe "with a deactivated user" do
            attr_accessor :user

            before(:each) do
                @user = User.where(provider: 'email', sign_up_code: 'FREEMBA').first
                set_auth_hash(info: {
                    email: @user.email,
                    name: 'bob smith'
                })
            end

            it "should not raise if user is not deactivated" do
                @user.deactivated = false
                @user.save!
                expect { controller.get_resource_from_auth_hash }.not_to raise_error()
            end

            it "should raise if user is deactivated" do
                @user.deactivated = true
                @user.save!
                expect { controller.get_resource_from_auth_hash }.to raise_error(RuntimeError)
            end

        end


        describe "with a hiring manager user" do
            attr_accessor :user

            before(:each) do
                @user = users(:hiring_manager_with_team)
                set_auth_hash(info: {
                    email: @user.email,
                    name: 'bob smith'
                })
            end

            it "should not raise if user is on web" do
                expect(controller).to receive(:cordova_client?).and_return(false)
                @user.save!
                expect { controller.get_resource_from_auth_hash }.not_to raise_error()
            end

            it "should raise if user is on a cordova platform" do
                expect(controller).to receive(:cordova_client?).and_return(true)
                @user.save!
                expect { controller.get_resource_from_auth_hash }.to raise_error(RuntimeError)
            end

        end

    end

    describe "deactivation" do

        attr_accessor :user

        before(:each) do
            set_auth_hash(info: {
                email: 'email@email.com'
            })
            @user = assert_new_user_created
            controller.instance_variable_set(:@resource, user)
        end

        it "should call sign_in on success if the resource is not deactivated" do
            expect(controller).to receive(:sign_in)
            get :omniauth_success,  params: {
                provider: @provider
            }
        end

        it "should not call sign_in on success if the resources is deactivated" do
            user.deactivated = true
            user.save!
            expect(controller).not_to receive(:sign_in)
            get :omniauth_success,  params: {
                provider: @provider
            }
        end

    end


    describe "with a hiring manager user" do
        attr_accessor :user

        before(:each) do
            @user = users(:hiring_manager_with_team)
            set_auth_hash(info: {
                email: @user.email,
                name: 'bob smith'
            })
        end

        it "should call sign_in on success if user is on web" do
            expect(controller).to receive(:cordova_client?).and_return(false)
            controller.instance_variable_set(:@resource, user)

            expect(controller).to receive(:sign_in)
            get :omniauth_success,  params: {
                provider: @provider
            }
        end

        it "should not call sign_in on success user is on a cordova platform" do
            expect(controller).to receive(:cordova_client?).and_return(true)
            controller.instance_variable_set(:@resource, user)

            expect(controller).not_to receive(:sign_in)
            get :omniauth_success,  params: {
                provider: @provider
            }
        end

    end

    describe "omniauth_failure" do

        before(:each) do
            controller.raise_exceptions = false
            expect(controller).to receive(:omniauth_success).and_raise(RuntimeError.new(""))
            expect(controller).to receive(:set_user_facing_error_message)
        end

        it "should have special handling in the institutional case" do
            expect(controller).to receive(:institutional?).and_return(true)
            expect(controller).to receive(:render_plain_text_error).with("A server error occurred. Please contact your company's administrator for help: Login failed")
            get_success
        end

        it "should work in normal case" do
            get :omniauth_success,  params: {
                provider: @provider
            }
            expect(controller.params['message']).to eq('Login failed')
        end

    end

    describe "set_user_facing_error_message" do
        it "should set a custom message" do
            controller.params[:message] = 'user_denied'
            controller.set_user_facing_error_message
            expect(controller.params[:message]).to eq("Login canceled")
        end

        it "should handle a message with no customized value" do
            controller.params[:message] = 'unknown error'
            controller.set_user_facing_error_message
            expect(controller.params[:message]).to eq('Login failed')
        end

        it "should handle deactivated user error" do
            controller.params[:message] = 'error_account_deactivated'
            controller.set_user_facing_error_message
            expect(controller.params[:message]).to eq('error_account_deactivated')
        end

        it "should handle hiring web-only user error" do
            controller.params[:message] = 'error_hiring_web_only'
            controller.set_user_facing_error_message
            expect(controller.params[:message]).to eq('error_hiring_web_only')
        end
    end

    describe "assign_provider_attrs" do

        it "should assign email and last name" do
            set_auth_hash(info: {
                email: 'email@email.com',
                name: 'bob smith'
            })
            user = assert_new_user_created
            expect(user.name).to eq('bob smith')
            expect(user.email).to eq('email@email.com')
        end

        it "should assign name when first_name and last_name are provided" do
            set_auth_hash( default_new_user_hash.deep_merge(info: {name: nil, first_name: 'Freddy', last_name: 'Mercury'}) )
            user = assert_new_user_created
            expect(user.name).to eq('Freddy Mercury')
        end

        it "should assign name when none is provided" do
            set_auth_hash( default_new_user_hash.deep_merge(info: {name: nil}) )
            user = assert_new_user_created
            expect(user.name).to eq(user.email)
        end

        it "should not update attributes for preexisting users" do
            user = users(:facebook_user)
            updated_user = assert_login_existing_user(user, {info: {name: 'new name'}})
            expect(updated_user.name).not_to eq('new name')
        end
    end

    describe "auth_origin_url" do

        it "should default to super in normal failure case" do
            allow(controller).to receive(:action_name).and_return('omniauth_failure')
            allow(controller).to receive(:omniauth_params).and_return(omniauth_params({
                'auth_origin_url' => 'auth/origin/url'
            }))
            expect(controller.auth_origin_url).to be_nil # non-whitelisted URLs are nil
        end

        it "should default to home in normal success case" do
            allow(controller).to receive(:action_name).and_return('omniauth_success')
            expect(controller.auth_origin_url).to eq('https://test.host/home')
        end

        it "should default to sign-in when resource is deactivated" do
            user = User.new
            user.deactivated = true
            controller.instance_variable_set(:@resource, user)
            allow(controller).to receive(:action_name).and_return('omniauth_success')
            expect(controller.auth_origin_url).to eq('https://test.host/sign-in')
        end

        describe "with redirect_whitelist support enabled" do

            it "should support file:// URLs with a valid redirect path" do
                expect(DeviseTokenAuth::Url.whitelisted?('file://some/User/some/guid-id/#/onboarding/hybrid/login')).to eq(true)
            end
        end

        describe "with using_saml_authentication? = true" do

            before(:each) do
                user = User.new
                user.provider = "provider"
                controller.instance_variable_set(:@resource, user)
                allow(user).to receive(:using_saml_authentication?).and_return(true)
                allow(controller).to receive(:omniauth_params).and_return({})
            end

            it "should work when use_saml_subdomains = true " do
                allow(Rails.application.config).to receive(:use_saml_subdomains).and_return(true)
                expect(controller.auth_origin_url).to eq("https://provider.test.host/home")
                expect(controller.params['subdomain']).to be_nil
            end

            it "should work when use_saml_subdomains = false" do
                allow(Rails.application.config).to receive(:use_saml_subdomains).and_return(false)
                expect(controller.auth_origin_url).to eq("https://test.host/home")
                expect(controller.params['subdomain']).to eq("provider")
            end

        end

    end

    describe "slack integration" do

        it "should log new user creation to slack" do
            set_auth_hash({
                uid: 'new_uid',
                info: {
                    email: 'new_user@mobileapp.com'
                }
            })
            expect_any_instance_of(User).to receive(:tell_slack_about_create)
            assert_new_user_created
        end

        it "should not log to slack with existing user login" do
            user = users(:facebook_user)
            expect_any_instance_of(User).not_to receive(:tell_slack_about_create)
            assert_login_existing_user(user)
        end

    end

    def assert_new_user_created
        allow_any_instance_of(User).to receive(:register_content_access)
        set_window_type('newWindow') # do not redirect
        orig_count = User.count
        get_success
        expect(User.count).to eq(orig_count+1)
        assert user = assigns(:resource)
        expect(controller.instance_variable_get(:@oauth_registration)).to be(true)
        user
    end

    def assert_login_existing_user(user, auth_hash_params = {})
        set_window_type('newWindow') # do not redirect
        set_auth_hash({
            uid: user.uid,
            info: {
                name: user.name,
                email: user.email
            }
        }.deep_merge(auth_hash_params))
        orig_count = User.count
        get_success
        expect(User.count).to eq(orig_count)
        resource = assigns(:resource)
        expect(resource.id).to eq(user.id)
        resource
    end

    def set_auth_hash(params = {})
        allow(controller).to receive(:auth_hash).and_return(controller.get_auth_hash(OmniAuth::AuthHash.new({
            uid: 'uid',
            provider: @provider,
            info: {
                name: @facebook_user.name,
                email: @facebook_user.email
            }
        }.deep_merge(params))))
    end

    def set_window_type(window_type = 'sameWindow')
        allow(controller).to receive(:omniauth_window_type).and_return(window_type)
    end

    def omniauth_params(extra = {})
        {
            sign_up_code: 'SIGN_UP_CODE',
            pref_locale: 'en'
        }.with_indifferent_access.merge(extra)
    end

    def get_success
        get :omniauth_success,  params: {
            provider: @provider
        }
        assert_200
    end

    def example_saml_response
        "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c2FtbDJwOlJlc3BvbnNlIHhtbG5zOnNhbWwycD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnByb3RvY29sIiBEZXN0aW5hdGlvbj0iaHR0cDovL2xvY2FsaG9zdDozMDAxL29tbmlhdXRoL2psbC9jYWxsYmFjayIgSUQ9ImlkODc5NTI0NDYxODAwNTg0MTQ3NTE1NTAwMiIgSXNzdWVJbnN0YW50PSIyMDE1LTA4LTMxVDEzOjI3OjE1LjMwNVoiIFZlcnNpb249IjIuMCIgeG1sbnM6eHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIj48c2FtbDI6SXNzdWVyIHhtbG5zOnNhbWwyPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YXNzZXJ0aW9uIiBGb3JtYXQ9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpuYW1laWQtZm9ybWF0OmVudGl0eSI+aHR0cDovL3d3dy5va3RhLmNvbS9leGs0ZGRpb3p1MmlIQXVoczBoNzwvc2FtbDI6SXNzdWVyPjxkczpTaWduYXR1cmUgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxkczpTaWduZWRJbmZvPjxkczpDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8+PGRzOlNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiLz48ZHM6UmVmZXJlbmNlIFVSST0iI2lkODc5NTI0NDYxODAwNTg0MTQ3NTE1NTAwMiI+PGRzOlRyYW5zZm9ybXM+PGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNlbnZlbG9wZWQtc2lnbmF0dXJlIi8+PGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyI+PGVjOkluY2x1c2l2ZU5hbWVzcGFjZXMgeG1sbnM6ZWM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIgUHJlZml4TGlzdD0ieHMiLz48L2RzOlRyYW5zZm9ybT48L2RzOlRyYW5zZm9ybXM+PGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jI3NoYTI1NiIvPjxkczpEaWdlc3RWYWx1ZT5lWUN4R2tTZzdMZTBUc0FsWFVLM3g4b1Y5ak9QcTc5aVBSWm9GZUV3dlBzPTwvZHM6RGlnZXN0VmFsdWU+PC9kczpSZWZlcmVuY2U+PC9kczpTaWduZWRJbmZvPjxkczpTaWduYXR1cmVWYWx1ZT5vdUJxUWNNdGJUMzlzOXo0N0tvOXV5Qitkb3ZJWUtqeTA4dmFzbmhoNHVycld2UnROOE1OVk9jNTdVS3VoNGk3c3RiL0Y5QTd2SDljSXBteG9BTkZ6SHZZZkhSa3lJeVkwNmcvdlNIOEhMU2VCWGEvLzVjQTFLSWcrSDY0OXMvZi9OaFBxeGxMU0hGenUybFYwTEtIK2cyYlcrNVdRZUNjU0dWN3Q2MXpwaGZXZkxBQ3VsNERIbkl3RFU2czVuNGhneG04endMbXBQREtydnhReWRYMkRUOGh2Z3puMW5EbWliSm5ERUNNTnJuc2RZZ1RIa0F3QWF3TnRHYnoxeFZPSE1HaXZXamp1a2lHUjFUMzdXWXFJeFMxSjZEaER6djJQRTlGNWJmNUkyRS9zUFc4REl5NmJDbTVNQ084NkJIVktLb1J6WlQzUUVwZnhDaGpSSFBZTUE9PTwvZHM6U2lnbmF0dXJlVmFsdWU+PGRzOktleUluZm8+PGRzOlg1MDlEYXRhPjxkczpYNTA5Q2VydGlmaWNhdGU+TUlJRHBEQ0NBb3lnQXdJQkFnSUdBVTRzWk00dU1BMEdDU3FHU0liM0RRRUJCUVVBTUlHU01Rc3dDUVlEVlFRR0V3SlZVekVUTUJFRwpBMVVFQ0F3S1EyRnNhV1p2Y201cFlURVdNQlFHQTFVRUJ3d05VMkZ1SUVaeVlXNWphWE5qYnpFTk1Bc0dBMVVFQ2d3RVQydDBZVEVVCk1CSUdBMVVFQ3d3TFUxTlBVSEp2ZG1sa1pYSXhFekFSQmdOVkJBTU1DbVJsZGkwNU1EQTROakF4SERBYUJna3Foa2lHOXcwQkNRRVcKRFdsdVptOUFiMnQwWVM1amIyMHdIaGNOTVRVd05qSTFNakF5TXpRd1doY05ORFV3TmpJMU1qQXlORFF3V2pDQmtqRUxNQWtHQTFVRQpCaE1DVlZNeEV6QVJCZ05WQkFnTUNrTmhiR2xtYjNKdWFXRXhGakFVQmdOVkJBY01EVk5oYmlCR2NtRnVZMmx6WTI4eERUQUxCZ05WCkJBb01CRTlyZEdFeEZEQVNCZ05WQkFzTUMxTlRUMUJ5YjNacFpHVnlNUk13RVFZRFZRUUREQXBrWlhZdE9UQXdPRFl3TVJ3d0dnWUoKS29aSWh2Y05BUWtCRmcxcGJtWnZRRzlyZEdFdVkyOXRNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDQVFFQQpwY2hmdVdXMHRRMlA4cC9TNVVmc0RiMGZnalB3MmtnbXVQTWVRUFR2dzZ4ZldsM0ZlQ2FqcERFNEd5TEhsd1FHaWROa2t1Y3NsMnA4Ck42Rm5HY25RMGVPM3ZsZmoyU3JUb2VWYnl6bHJKM0NkamJqUElndlkzOENWWm1SMDJPT3dpbnR6QXh2Wk9JSFMzQ1JyS25SejBGK04KMUNCWDBXSmFNenNoSE1yWGFIMmdVTVdhalRTR29SU3QrczVHb1pvcWN5NUFFK2xCWCtMd2xwTUlOWC9ZYWU0U1d6UFZQV0c2NmtUZgpGTlVxbGxPL01saS9KRXp2SERkb0s2OU5KNFA0aSszc2tWL2tHQ1liWlFYbHB6R296WGkwVHVrekJtWVg0OGJlajhnamRSNFF0WGZCCmVxTDY2eklCTDlNaWlYOGYzblFjT2xabE5RVkxWZkVvRlBib053SURBUUFCTUEwR0NTcUdTSWIzRFFFQkJRVUFBNElCQVFDQ3NWdTEKblRjT2ZOOTMwSFJZWnBQRDNKclFwbE4ranJ2aXlaQ3ZUbThySEZFMVd3RG40andPdHppVDJMeWMxbEY0d3A5RExLdkwySUkraWY1KwpIZFEwSzJsUXh6RnFiSDkybk5jWUdJamV3SHdUVkxVTUlwSXY2R0hJQXNjcWFjZWp4ZmMvY3p6anp1cGpUS1laWkZ0Q1EvdjRlRGp6CmtyWk9wTm4yYnJjb0JaaHo5S3VDNDA0b3JURUpneTJYY2ZMOVVLMmZPNlFOTkMvSU1sbHdaRDc4SlpTaXlYVkhUTXRGTUhxejJOcFIKVlk1ZmwxdlM1ck5Ldnh2RENWODJxelNiWVkzUW8xclhkZTVORzhpSlp0aXhTVG56a2U1WHdBemtqUlBkcTIzanVRRkVRaUJ3bTRiWQpuVmgzeVFNeHduTE0vWWtoZGp5OGc3SmpPblZiU3dkejwvZHM6WDUwOUNlcnRpZmljYXRlPjwvZHM6WDUwOURhdGE+PC9kczpLZXlJbmZvPjwvZHM6U2lnbmF0dXJlPjxzYW1sMnA6U3RhdHVzIHhtbG5zOnNhbWwycD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnByb3RvY29sIj48c2FtbDJwOlN0YXR1c0NvZGUgVmFsdWU9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpzdGF0dXM6U3VjY2VzcyIvPjwvc2FtbDJwOlN0YXR1cz48c2FtbDI6QXNzZXJ0aW9uIHhtbG5zOnNhbWwyPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YXNzZXJ0aW9uIiBJRD0iaWQ4Nzk1MjQ0NjE4MTMwMDEzNzE0NDM3MTM0IiBJc3N1ZUluc3RhbnQ9IjIwMTUtMDgtMzFUMTM6Mjc6MTUuMzA1WiIgVmVyc2lvbj0iMi4wIiB4bWxuczp4cz0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEiPjxzYW1sMjpJc3N1ZXIgRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6bmFtZWlkLWZvcm1hdDplbnRpdHkiIHhtbG5zOnNhbWwyPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YXNzZXJ0aW9uIj5odHRwOi8vd3d3Lm9rdGEuY29tL2V4azRkZGlvenUyaUhBdWhzMGg3PC9zYW1sMjpJc3N1ZXI+PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+PGRzOlNpZ25lZEluZm8+PGRzOkNhbm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48ZHM6U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxkc2lnLW1vcmUjcnNhLXNoYTI1NiIvPjxkczpSZWZlcmVuY2UgVVJJPSIjaWQ4Nzk1MjQ0NjE4MTMwMDEzNzE0NDM3MTM0Ij48ZHM6VHJhbnNmb3Jtcz48ZHM6VHJhbnNmb3JtIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI2VudmVsb3BlZC1zaWduYXR1cmUiLz48ZHM6VHJhbnNmb3JtIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIj48ZWM6SW5jbHVzaXZlTmFtZXNwYWNlcyB4bWxuczplYz0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIiBQcmVmaXhMaXN0PSJ4cyIvPjwvZHM6VHJhbnNmb3JtPjwvZHM6VHJhbnNmb3Jtcz48ZHM6RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxlbmMjc2hhMjU2Ii8+PGRzOkRpZ2VzdFZhbHVlPmJFR0FsMkZEY2xzNC9MNHZId2lqTXlhWkEyeG9kaUVoeFFVbEdHRVhPK0U9PC9kczpEaWdlc3RWYWx1ZT48L2RzOlJlZmVyZW5jZT48L2RzOlNpZ25lZEluZm8+PGRzOlNpZ25hdHVyZVZhbHVlPlFrYzgyK3J0Mm5UNHgyZktkZG4yK3VGalEzcURhdDczUnJVMWlCK3dmYWxDNlI5aTFBMWdXZHc4cEN4SURJM2w5TllONzYwaTNhclpNL0dnV29LWjAveVpUL05rUkVTK0RJaTEyUm5HZkFZbTFqcXZqOGpWQVZNZ2tVK2o3MEhoSGx6Zk1RQzNuRGwvVmhvVGVsSmZvdVVuMjBiTGtDZDAyMzNYRzNFNWVQcFBscXFCZTI3dk9PcjRJTTRtU21MeXlVQVc0Rmg3V2traWlKR0pIblVsR1I2YXNSWThrOWpqSjdOYjJDS1ZtMENBMkMzNVB5OG9XeWM0YmJHcFlRVFpGYzZEcTJwaXMreXg2SGFici9oRkJaS2hoK2FaVkdXUjFBZWlWVE1iRUNvVGs4VTJEYldoRTN0Uk8xTk0zcDNjN1VYdzAwL2pyaFY2VVFkQnhRcjRLQT09PC9kczpTaWduYXR1cmVWYWx1ZT48ZHM6S2V5SW5mbz48ZHM6WDUwOURhdGE+PGRzOlg1MDlDZXJ0aWZpY2F0ZT5NSUlEcERDQ0FveWdBd0lCQWdJR0FVNHNaTTR1TUEwR0NTcUdTSWIzRFFFQkJRVUFNSUdTTVFzd0NRWURWUVFHRXdKVlV6RVRNQkVHCkExVUVDQXdLUTJGc2FXWnZjbTVwWVRFV01CUUdBMVVFQnd3TlUyRnVJRVp5WVc1amFYTmpiekVOTUFzR0ExVUVDZ3dFVDJ0MFlURVUKTUJJR0ExVUVDd3dMVTFOUFVISnZkbWxrWlhJeEV6QVJCZ05WQkFNTUNtUmxkaTA1TURBNE5qQXhIREFhQmdrcWhraUc5dzBCQ1FFVwpEV2x1Wm05QWIydDBZUzVqYjIwd0hoY05NVFV3TmpJMU1qQXlNelF3V2hjTk5EVXdOakkxTWpBeU5EUXdXakNCa2pFTE1Ba0dBMVVFCkJoTUNWVk14RXpBUkJnTlZCQWdNQ2tOaGJHbG1iM0p1YVdFeEZqQVVCZ05WQkFjTURWTmhiaUJHY21GdVkybHpZMjh4RFRBTEJnTlYKQkFvTUJFOXJkR0V4RkRBU0JnTlZCQXNNQzFOVFQxQnliM1pwWkdWeU1STXdFUVlEVlFRRERBcGtaWFl0T1RBd09EWXdNUnd3R2dZSgpLb1pJaHZjTkFRa0JGZzFwYm1adlFHOXJkR0V1WTI5dE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBCnBjaGZ1V1cwdFEyUDhwL1M1VWZzRGIwZmdqUHcya2dtdVBNZVFQVHZ3NnhmV2wzRmVDYWpwREU0R3lMSGx3UUdpZE5ra3Vjc2wycDgKTjZGbkdjblEwZU8zdmxmajJTclRvZVZieXpsckozQ2RqYmpQSWd2WTM4Q1ZabVIwMk9Pd2ludHpBeHZaT0lIUzNDUnJLblJ6MEYrTgoxQ0JYMFdKYU16c2hITXJYYUgyZ1VNV2FqVFNHb1JTdCtzNUdvWm9xY3k1QUUrbEJYK0x3bHBNSU5YL1lhZTRTV3pQVlBXRzY2a1RmCkZOVXFsbE8vTWxpL0pFenZIRGRvSzY5Tko0UDRpKzNza1Yva0dDWWJaUVhscHpHb3pYaTBUdWt6Qm1ZWDQ4YmVqOGdqZFI0UXRYZkIKZXFMNjZ6SUJMOU1paVg4ZjNuUWNPbFpsTlFWTFZmRW9GUGJvTndJREFRQUJNQTBHQ1NxR1NJYjNEUUVCQlFVQUE0SUJBUUNDc1Z1MQpuVGNPZk45MzBIUllacFBEM0pyUXBsTitqcnZpeVpDdlRtOHJIRkUxV3dEbjRqd090emlUMkx5YzFsRjR3cDlETEt2TDJJSStpZjUrCkhkUTBLMmxReHpGcWJIOTJuTmNZR0lqZXdId1RWTFVNSXBJdjZHSElBc2NxYWNlanhmYy9jenpqenVwalRLWVpaRnRDUS92NGVEanoKa3JaT3BObjJicmNvQlpoejlLdUM0MDRvclRFSmd5MlhjZkw5VUsyZk82UU5OQy9JTWxsd1pENzhKWlNpeVhWSFRNdEZNSHF6Mk5wUgpWWTVmbDF2UzVyTkt2eHZEQ1Y4MnF6U2JZWTNRbzFyWGRlNU5HOGlKWnRpeFNUbnprZTVYd0F6a2pSUGRxMjNqdVFGRVFpQndtNGJZCm5WaDN5UU14d25MTS9Za2hkank4ZzdKak9uVmJTd2R6PC9kczpYNTA5Q2VydGlmaWNhdGU+PC9kczpYNTA5RGF0YT48L2RzOktleUluZm8+PC9kczpTaWduYXR1cmU+PHNhbWwyOlN1YmplY3QgeG1sbnM6c2FtbDI9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iPjxzYW1sMjpOYW1lSUQgRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoxLjE6bmFtZWlkLWZvcm1hdDplbWFpbEFkZHJlc3MiPm5hdGVAcGVkYWdvLmNvbTwvc2FtbDI6TmFtZUlEPjxzYW1sMjpTdWJqZWN0Q29uZmlybWF0aW9uIE1ldGhvZD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmNtOmJlYXJlciI+PHNhbWwyOlN1YmplY3RDb25maXJtYXRpb25EYXRhIE5vdE9uT3JBZnRlcj0iMjAxNS0wOC0zMVQxMzozMjoxNS4zMDVaIiBSZWNpcGllbnQ9Imh0dHA6Ly9sb2NhbGhvc3Q6MzAwMS9vbW5pYXV0aC9qbGwvY2FsbGJhY2siLz48L3NhbWwyOlN1YmplY3RDb25maXJtYXRpb24+PC9zYW1sMjpTdWJqZWN0PjxzYW1sMjpDb25kaXRpb25zIE5vdEJlZm9yZT0iMjAxNS0wOC0zMVQxMzoyMjoxNS4zMDVaIiBOb3RPbk9yQWZ0ZXI9IjIwMTUtMDgtMzFUMTM6MzI6MTUuMzA1WiIgeG1sbnM6c2FtbDI9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iPjxzYW1sMjpBdWRpZW5jZVJlc3RyaWN0aW9uPjxzYW1sMjpBdWRpZW5jZT5zbWFydGx5PC9zYW1sMjpBdWRpZW5jZT48L3NhbWwyOkF1ZGllbmNlUmVzdHJpY3Rpb24+PC9zYW1sMjpDb25kaXRpb25zPjxzYW1sMjpBdXRoblN0YXRlbWVudCBBdXRobkluc3RhbnQ9IjIwMTUtMDgtMzFUMTM6Mjc6MTUuMzA1WiIgU2Vzc2lvbkluZGV4PSJpZDE0NDEwMjc2MzUzMDUuMTI2NTk4MjYxOSIgeG1sbnM6c2FtbDI9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iPjxzYW1sMjpBdXRobkNvbnRleHQ+PHNhbWwyOkF1dGhuQ29udGV4dENsYXNzUmVmPnVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphYzpjbGFzc2VzOlBhc3N3b3JkUHJvdGVjdGVkVHJhbnNwb3J0PC9zYW1sMjpBdXRobkNvbnRleHRDbGFzc1JlZj48L3NhbWwyOkF1dGhuQ29udGV4dD48L3NhbWwyOkF1dGhuU3RhdGVtZW50PjxzYW1sMjpBdHRyaWJ1dGVTdGF0ZW1lbnQgeG1sbnM6c2FtbDI9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iPjxzYW1sMjpBdHRyaWJ1dGUgTmFtZT0iZmlyc3ROYW1lIiBOYW1lRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YXR0cm5hbWUtZm9ybWF0OnVuc3BlY2lmaWVkIj48c2FtbDI6QXR0cmlidXRlVmFsdWUgeG1sbnM6eHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIiB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiB4c2k6dHlwZT0ieHM6c3RyaW5nIj5OYXQ8L3NhbWwyOkF0dHJpYnV0ZVZhbHVlPjwvc2FtbDI6QXR0cmlidXRlPjxzYW1sMjpBdHRyaWJ1dGUgTmFtZT0ibGFzdE5hbWUiIE5hbWVGb3JtYXQ9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphdHRybmFtZS1mb3JtYXQ6dW5zcGVjaWZpZWQiPjxzYW1sMjpBdHRyaWJ1dGVWYWx1ZSB4bWxuczp4cz0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEiIHhtbG5zOnhzaT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEtaW5zdGFuY2UiIHhzaTp0eXBlPSJ4czpzdHJpbmciPkJydXN0ZWluPC9zYW1sMjpBdHRyaWJ1dGVWYWx1ZT48L3NhbWwyOkF0dHJpYnV0ZT48c2FtbDI6QXR0cmlidXRlIE5hbWU9ImVtYWlsIiBOYW1lRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YXR0cm5hbWUtZm9ybWF0OnVuc3BlY2lmaWVkIj48c2FtbDI6QXR0cmlidXRlVmFsdWUgeG1sbnM6eHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIiB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiB4c2k6dHlwZT0ieHM6c3RyaW5nIj5uYXRlK29rdGFAcGVkYWdvLmNvbTwvc2FtbDI6QXR0cmlidXRlVmFsdWU+PC9zYW1sMjpBdHRyaWJ1dGU+PC9zYW1sMjpBdHRyaWJ1dGVTdGF0ZW1lbnQ+PC9zYW1sMjpBc3NlcnRpb24+PC9zYW1sMnA6UmVzcG9uc2U+"
    end

    def decoded_saml_response
        %Q|<?xml version="1.0" encoding="UTF-8"?><saml2p:Response xmlns:saml2p="urn:oasis:names:tc:SAML:2.0:protocol" Destination="http://localhost:3001/omniauth/jll/callback" ID="id8795244618005841475155002" IssueInstant="2015-08-31T13:27:15.305Z" Version="2.0" xmlns:xs="http://www.w3.org/2001/XMLSchema"><saml2:Issuer xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" Format="urn:oasis:names:tc:SAML:2.0:nameid-format:entity">http://www.okta.com/exk4ddiozu2iHAuhs0h7</saml2:Issuer><ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/><ds:Reference URI="#id8795244618005841475155002"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"><ec:InclusiveNamespaces xmlns:ec="http://www.w3.org/2001/10/xml-exc-c14n#" PrefixList="xs"/></ds:Transform></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>eYCxGkSg7Le0TsAlXUK3x8oV9jOPq79iPRZoFeEwvPs=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue>ouBqQcMtbT39s9z47Ko9uyB+dovIYKjy08vasnhh4urrWvRtN8MNVOc57UKuh4i7stb/F9A7vH9cIpmxoANFzHvYfHRkyIyY06g/vSH8HLSeBXa//5cA1KIg+H649s/f/NhPqxlLSHFzu2lV0LKH+g2bW+5WQeCcSGV7t61zphfWfLACul4DHnIwDU6s5n4hgxm8zwLmpPDKrvxQydX2DT8hvgzn1nDmibJnDECMNrnsdYgTHkAwAawNtGbz1xVOHMGivWjjukiGR1T37WYqIxS1J6DhDzv2PE9F5bf5I2E/sPW8DIy6bCm5MCO86BHVKKoRzZT3QEpfxChjRHPYMA==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIIDpDCCAoygAwIBAgIGAU4sZM4uMA0GCSqGSIb3DQEBBQUAMIGSMQswCQYDVQQGEwJVUzETMBEG\nA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzENMAsGA1UECgwET2t0YTEU\nMBIGA1UECwwLU1NPUHJvdmlkZXIxEzARBgNVBAMMCmRldi05MDA4NjAxHDAaBgkqhkiG9w0BCQEW\nDWluZm9Ab2t0YS5jb20wHhcNMTUwNjI1MjAyMzQwWhcNNDUwNjI1MjAyNDQwWjCBkjELMAkGA1UE\nBhMCVVMxEzARBgNVBAgMCkNhbGlmb3JuaWExFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xDTALBgNV\nBAoMBE9rdGExFDASBgNVBAsMC1NTT1Byb3ZpZGVyMRMwEQYDVQQDDApkZXYtOTAwODYwMRwwGgYJ\nKoZIhvcNAQkBFg1pbmZvQG9rdGEuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA\npchfuWW0tQ2P8p/S5UfsDb0fgjPw2kgmuPMeQPTvw6xfWl3FeCajpDE4GyLHlwQGidNkkucsl2p8\nN6FnGcnQ0eO3vlfj2SrToeVbyzlrJ3CdjbjPIgvY38CVZmR02OOwintzAxvZOIHS3CRrKnRz0F+N\n1CBX0WJaMzshHMrXaH2gUMWajTSGoRSt+s5GoZoqcy5AE+lBX+LwlpMINX/Yae4SWzPVPWG66kTf\nFNUqllO/Mli/JEzvHDdoK69NJ4P4i+3skV/kGCYbZQXlpzGozXi0TukzBmYX48bej8gjdR4QtXfB\neqL66zIBL9MiiX8f3nQcOlZlNQVLVfEoFPboNwIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQCCsVu1\nnTcOfN930HRYZpPD3JrQplN+jrviyZCvTm8rHFE1WwDn4jwOtziT2Lyc1lF4wp9DLKvL2II+if5+\nHdQ0K2lQxzFqbH92nNcYGIjewHwTVLUMIpIv6GHIAscqacejxfc/czzjzupjTKYZZFtCQ/v4eDjz\nkrZOpNn2brcoBZhz9KuC404orTEJgy2XcfL9UK2fO6QNNC/IMllwZD78JZSiyXVHTMtFMHqz2NpR\nVY5fl1vS5rNKvxvDCV82qzSbYY3Qo1rXde5NG8iJZtixSTnzke5XwAzkjRPdq23juQFEQiBwm4bY\nnVh3yQMxwnLM/Ykhdjy8g7JjOnVbSwdz</ds:X509Certificate></ds:X509Data></ds:KeyInfo></ds:Signature><saml2p:Status xmlns:saml2p="urn:oasis:names:tc:SAML:2.0:protocol"><saml2p:StatusCode Value="urn:oasis:names:tc:SAML:2.0:status:Success"/></saml2p:Status><saml2:Assertion xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" ID="id8795244618130013714437134" IssueInstant="2015-08-31T13:27:15.305Z" Version="2.0" xmlns:xs="http://www.w3.org/2001/XMLSchema"><saml2:Issuer Format="urn:oasis:names:tc:SAML:2.0:nameid-format:entity" xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion">http://www.okta.com/exk4ddiozu2iHAuhs0h7</saml2:Issuer><ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/><ds:Reference URI="#id8795244618130013714437134"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"><ec:InclusiveNamespaces xmlns:ec="http://www.w3.org/2001/10/xml-exc-c14n#" PrefixList="xs"/></ds:Transform></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/><ds:DigestValue>bEGAl2FDcls4/L4vHwijMyaZA2xodiEhxQUlGGEXO+E=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue>Qkc82+rt2nT4x2fKddn2+uFjQ3qDat73RrU1iB+wfalC6R9i1A1gWdw8pCxIDI3l9NYN760i3arZM/GgWoKZ0/yZT/NkRES+DIi12RnGfAYm1jqvj8jVAVMgkU+j70HhHlzfMQC3nDl/VhoTelJfouUn20bLkCd0233XG3E5ePpPlqqBe27vOOr4IM4mSmLyyUAW4Fh7WkkiiJGJHnUlGR6asRY8k9jjJ7Nb2CKVm0CA2C35Py8oWyc4bbGpYQTZFc6Dq2pis+yx6Habr/hFBZKhh+aZVGWR1AeiVTMbECoTk8U2DbWhE3tRO1NM3p3c7UXw00/jrhV6UQdBxQr4KA==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIIDpDCCAoygAwIBAgIGAU4sZM4uMA0GCSqGSIb3DQEBBQUAMIGSMQswCQYDVQQGEwJVUzETMBEG\nA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzENMAsGA1UECgwET2t0YTEU\nMBIGA1UECwwLU1NPUHJvdmlkZXIxEzARBgNVBAMMCmRldi05MDA4NjAxHDAaBgkqhkiG9w0BCQEW\nDWluZm9Ab2t0YS5jb20wHhcNMTUwNjI1MjAyMzQwWhcNNDUwNjI1MjAyNDQwWjCBkjELMAkGA1UE\nBhMCVVMxEzARBgNVBAgMCkNhbGlmb3JuaWExFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xDTALBgNV\nBAoMBE9rdGExFDASBgNVBAsMC1NTT1Byb3ZpZGVyMRMwEQYDVQQDDApkZXYtOTAwODYwMRwwGgYJ\nKoZIhvcNAQkBFg1pbmZvQG9rdGEuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA\npchfuWW0tQ2P8p/S5UfsDb0fgjPw2kgmuPMeQPTvw6xfWl3FeCajpDE4GyLHlwQGidNkkucsl2p8\nN6FnGcnQ0eO3vlfj2SrToeVbyzlrJ3CdjbjPIgvY38CVZmR02OOwintzAxvZOIHS3CRrKnRz0F+N\n1CBX0WJaMzshHMrXaH2gUMWajTSGoRSt+s5GoZoqcy5AE+lBX+LwlpMINX/Yae4SWzPVPWG66kTf\nFNUqllO/Mli/JEzvHDdoK69NJ4P4i+3skV/kGCYbZQXlpzGozXi0TukzBmYX48bej8gjdR4QtXfB\neqL66zIBL9MiiX8f3nQcOlZlNQVLVfEoFPboNwIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQCCsVu1\nnTcOfN930HRYZpPD3JrQplN+jrviyZCvTm8rHFE1WwDn4jwOtziT2Lyc1lF4wp9DLKvL2II+if5+\nHdQ0K2lQxzFqbH92nNcYGIjewHwTVLUMIpIv6GHIAscqacejxfc/czzjzupjTKYZZFtCQ/v4eDjz\nkrZOpNn2brcoBZhz9KuC404orTEJgy2XcfL9UK2fO6QNNC/IMllwZD78JZSiyXVHTMtFMHqz2NpR\nVY5fl1vS5rNKvxvDCV82qzSbYY3Qo1rXde5NG8iJZtixSTnzke5XwAzkjRPdq23juQFEQiBwm4bY\nnVh3yQMxwnLM/Ykhdjy8g7JjOnVbSwdz</ds:X509Certificate></ds:X509Data></ds:KeyInfo></ds:Signature><saml2:Subject xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion"><saml2:NameID Format="urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress">nate@pedago.com</saml2:NameID><saml2:SubjectConfirmation Method="urn:oasis:names:tc:SAML:2.0:cm:bearer"><saml2:SubjectConfirmationData NotOnOrAfter="2015-08-31T13:32:15.305Z" Recipient="http://localhost:3001/omniauth/jll/callback"/></saml2:SubjectConfirmation></saml2:Subject><saml2:Conditions NotBefore="2015-08-31T13:22:15.305Z" NotOnOrAfter="2015-08-31T13:32:15.305Z" xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion"><saml2:AudienceRestriction><saml2:Audience>smartly</saml2:Audience></saml2:AudienceRestriction></saml2:Conditions><saml2:AuthnStatement AuthnInstant="2015-08-31T13:27:15.305Z" SessionIndex="id1441027635305.1265982619" xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion"><saml2:AuthnContext><saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport</saml2:AuthnContextClassRef></saml2:AuthnContext></saml2:AuthnStatement><saml2:AttributeStatement xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion"><saml2:Attribute Name="firstName" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified"><saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">Nat</saml2:AttributeValue></saml2:Attribute><saml2:Attribute Name="lastName" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified"><saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">Brustein</saml2:AttributeValue></saml2:Attribute><saml2:Attribute Name="email" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified"><saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">nate+okta@pedago.com</saml2:AttributeValue></saml2:Attribute></saml2:AttributeStatement></saml2:Assertion></saml2p:Response>|
    end

end