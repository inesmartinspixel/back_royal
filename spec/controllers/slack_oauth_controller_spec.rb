require 'spec_helper'
require 'controllers/helpers/controller_spec_helper'

describe SlackOauthController do
    include ControllerSpecHelper

    describe "::COHORT_EXERCISES_SLACK_APP_SCOPES" do

        # See https://api.slack.com/apps/A012LAWE5JT/oauth for explanations
        # of why these specific scopes are requested. NOTE: This list should
        # match the scopes listed on that page.
        it "should contain the appropriate scopes" do
            expect(SlackOauthController::COHORT_EXERCISES_SLACK_APP_SCOPES).to eq([
                'channels:join',
                'channels:manage',
                'channels:read',
                'chat:write',
                'chat:write.customize',
                'chat:write.public',
                'files:write',
                'pins:write'
            ])
        end
    end

    describe "auth" do

        it "should 401 if no current_user" do
            stub_no_current_user
            get 'auth'
            assert_401
        end

        it "should 401 if current_user does not have admin role" do
            stub_current_user # no role
            get 'auth'
            assert_401
        end

        describe "when current_user is an admin" do
            before(:each) do
                stub_current_user(controller, [:admin])
            end

            it "should create an encrypted cookie with a unique state identifier for the request and redirect_to the SLACK_OAUTH_REDIRECT_URL with the appropriate query params" do
                slack_room = CohortSlackRoom.first
                mock_state = SecureRandom.uuid
                mock_redirect_uri = 'https://foo.bar/slack/auth_callback'
                mock_request_origin_url = 'https://foo.bar?baz=qux'
                expected_cookie_value = {
                    slack_room_id: slack_room.id,
                    request_origin_url: mock_request_origin_url
                }
                query_params = {
                    'scope' => SlackOauthController::COHORT_EXERCISES_SLACK_APP_SCOPES.join(','),
                    'client_id' => ENV['COHORT_EXERCISES_SLACK_APP_CLIENT_ID'],
                    'redirect_uri' => mock_redirect_uri,
                    'state' => mock_state
                }
                expect(SecureRandom).to receive(:uuid).and_return(mock_state)
                expect(controller).to receive(:redirect_uri).and_return(mock_redirect_uri)
                expect(cookies.encrypted["#{SlackOauthController::SLACK_APP_COOKIE_IDENTIFIER_PREFIX}#{mock_state}"]).to be_nil

                get 'auth', params: { slack_room_id: slack_room.id, request_origin_url: mock_request_origin_url }
                assert_redirected_to "#{SlackOauthController::SLACK_OAUTH_REDIRECT_URL}?#{query_params.to_query}"

                expect(cookies.encrypted["#{SlackOauthController::SLACK_APP_COOKIE_IDENTIFIER_PREFIX}#{mock_state}"]).to eq(expected_cookie_value)
            end
        end
    end

    describe "auth_callback" do

        it "should 401 if no current_user" do
            stub_no_current_user
            get 'auth_callback', format: :json
            assert_401
        end

        it "should 401 if current_user does not have admin role" do
            stub_current_user # no role
            get 'auth_callback', format: :json
            assert_401
        end

        describe "when current_user is an admin" do
            before(:each) do
                stub_current_user(controller, [:admin])
                @code = SecureRandom.uuid
                @state = SecureRandom.uuid
                @cookie_identifier = "#{SlackOauthController::SLACK_APP_COOKIE_IDENTIFIER_PREFIX}#{@state}"
                allow(controller).to receive(:slack_app_cookie_identifier).with(@state).and_return(@cookie_identifier)
                @request_origin_url = 'https://foo.bar?baz=qux'
                @slack_room_id = CohortSlackRoom.first.id
                @cookie_value = {
                    slack_room_id: @slack_room_id,
                    request_origin_url: @request_origin_url
                }
            end

            it "should render forbidden error if CSRF attack is detected" do
                expect(controller).to receive(:check_for_csrf_attack).with(@cookie_identifier).and_return([nil, 'some_csrf_attack_error']) # returning nil indicates a CSRF attack
                expect(controller).not_to receive(:check_for_auth_callback_request_error)
                expect(controller).not_to receive(:exchange_temporary_auth_code_for_access_token)
                expect(controller).not_to receive(:update_slack_room)

                get 'auth_callback', params: auth_callback_params, format: :json
                assert_error_response(403, {'message' => 'some_csrf_attack_error'})
            end

            it "should redirect_to request_origin_url early if access_denied auth_callback request error" do
                expect(controller).to receive(:check_for_csrf_attack).with(@cookie_identifier).and_return(@cookie_value)
                expect(controller).to receive(:check_for_auth_callback_request_error).and_return('access_denied')
                expect(controller).not_to receive(:exchange_temporary_auth_code_for_access_token)
                expect(controller).not_to receive(:update_slack_room)

                # it's not really important that we include the error in the params as we're mocking out
                # the return value of check_for_auth_callback_request_error to be an access_denied error,
                # but it's more clear in the spec about what the actual behavior would be in the wild.
                get 'auth_callback', params: auth_callback_params(error: 'access_denied'), format: :json
                assert_redirected_to(@request_origin_url)
            end

            it "should redirect_to request_origin_url with slack_oauth_error query param if auth_callback request error is not access_denifed" do
                expect(controller).to receive(:check_for_csrf_attack).with(@cookie_identifier).and_return(@cookie_value)
                expect(controller).to receive(:check_for_auth_callback_request_error).and_return('not_access_denied')
                expect(controller).not_to receive(:exchange_temporary_auth_code_for_access_token)
                expect(controller).not_to receive(:update_slack_room)

                # it's not really important that we include the error in the params as we're mocking out
                # the return value of check_for_auth_callback_request_error to be an access_denied error,
                # but it's more clear in the spec about what the actual behavior would be in the wild.
                get 'auth_callback', params: auth_callback_params(error: 'not_access_denied'), format: :json
                assert_redirected_to("#{@request_origin_url}&slack_oauth_error=not_access_denied")
            end

            it "should redirect_to request_origin_url with slack_oauth_error query param if we failed to exchange_temporary_auth_code_for_access_token" do
                expect(controller).to receive(:check_for_csrf_attack).with(@cookie_identifier).and_return(@cookie_value)
                expect(controller).to receive(:check_for_auth_callback_request_error).and_return(nil)
                expect(controller).to receive(:exchange_temporary_auth_code_for_access_token).and_return([nil, 'some_oauth_v2_access_method_error'])
                expect(controller).not_to receive(:update_slack_room)

                get 'auth_callback', params: auth_callback_params, format: :json
                assert_redirected_to("#{@request_origin_url}&slack_oauth_error=some_oauth_v2_access_method_error")
            end

            it "should redirect_to request_origin_url with slack_oauth_error query param if we failed to update_slack_room with the access_token" do
                expected_access_token = SecureRandom.uuid
                expect(controller).to receive(:check_for_csrf_attack).with(@cookie_identifier).and_return(@cookie_value)
                expect(controller).to receive(:check_for_auth_callback_request_error).and_return(nil)
                expect(controller).to receive(:exchange_temporary_auth_code_for_access_token).and_return({'access_token' => expected_access_token})
                expect(controller).to receive(:update_slack_room).with(expected_access_token).and_return('some_update_error')

                get 'auth_callback', params: auth_callback_params, format: :json
                assert_redirected_to("#{@request_origin_url}&slack_oauth_error=some_update_error")
            end

            it "should redirect_to request_origin_url with slack_oauth_success query param after we update_slack_room with the access_token" do
                expected_access_token = SecureRandom.uuid
                expect(controller).to receive(:check_for_csrf_attack).with(@cookie_identifier).and_return(@cookie_value)
                expect(controller).to receive(:check_for_auth_callback_request_error).and_return(nil)
                expect(controller).to receive(:exchange_temporary_auth_code_for_access_token).and_return({'access_token' => expected_access_token})
                expect(controller).to receive(:update_slack_room).with(expected_access_token).and_return(nil) # no error returned indicates success

                get 'auth_callback', params: auth_callback_params, format: :json
                assert_redirected_to("#{@request_origin_url}&slack_oauth_success=#{@slack_room_id}")
            end
        end
    end

    describe "check_for_csrf_attack" do
        before(:each) do
            stub_current_user(controller, [:admin])
            @code = controller.code = SecureRandom.uuid
            @state = controller.state = SecureRandom.uuid
            @cookie_identifier = "#{SlackOauthController::SLACK_APP_COOKIE_IDENTIFIER_PREFIX}#{@state}"
        end

        it "should log to Sentry and return blank cookie and error message if cookie can't be found" do
            expect(Raven).to receive(:capture_exception).with('Possible CSRF attack detected in OAuth callback request from Slack', {
                level: 'warning',
                extra: { state: @state, code: @code }
            })
            expect(controller).to receive(:check_for_csrf_attack).with(@cookie_identifier).and_call_original
            expect(controller.send(:check_for_csrf_attack, @cookie_identifier)).to eq([nil, 'A possible CSRF attack was detected in the OAuth callback request sent from Slack. Engineers have been notified. Please retry adding the Slack app to the workspace or contact an engineer directly if the issue persists.'])
        end
    end

    describe "check_for_auth_callback_request_error" do
        before(:each) do
            controller.code = SecureRandom.uuid
            controller.state = SecureRandom.uuid
            controller.slack_room_id = SecureRandom.uuid
            controller.params = {:error => 'not_access_denied'}
        end

        it "should return error without logging to Sentry if error param is access_denied" do
            controller.params = {:error => 'access_denied'}
            expect(Raven).not_to receive(:capture_exception)
            expect(controller.send(:check_for_auth_callback_request_error)).to eq('access_denied')
        end

        it "should log to Sentry and return error message if error param is present and not access_denied" do
            controller.params = {:error => 'not_access_denied'}
            expect(Raven).to receive(:capture_exception).with('Slack OAuth sent request to auth_callback with an unexpected error.', {
                extra: {
                    error: 'not_access_denied',
                    slack_room_id: controller.slack_room_id,
                    code: controller.code,
                    state: controller.state
                }
            })
            expect(controller.send(:check_for_auth_callback_request_error)).to eq('Slack responded to the authorization request with the following error: not_access_denied. Engineers have been notified. Contact an engineer directly if the issue persists.')
        end
    end

    describe "exchange_temporary_auth_code_for_access_token" do
        before(:each) do
            @slack_room_id = controller.slack_room_id = SecureRandom.uuid
            @state = controller.state = SecureRandom.uuid
            @code = controller.code = SecureRandom.uuid
            ENV['COHORT_EXERCISES_SLACK_APP_CLIENT_ID'] = SecureRandom.uuid
            ENV['COHORT_EXERCISES_SLACK_APP_CLIENT_SECRET'] = SecureRandom.uuid
        end

        it "should make an oauth_v2_access request and return API response without error message on success" do
            mock_redirect_uri = 'https://foo.bar?baz=qux'
            mock_client = double('Slack::Web::Client')
            mock_response = { 'team' => { 'name' => 'Team Foo' } }
            expect(controller).to receive(:redirect_uri).and_return(mock_redirect_uri)
            expect(Slack::Web::Client).to receive(:new).with(token: nil).and_return(mock_client)
            expect(mock_client).to receive(:oauth_v2_access).with({
                code: @code,
                client_id: ENV['COHORT_EXERCISES_SLACK_APP_CLIENT_ID'],
                client_secret: ENV['COHORT_EXERCISES_SLACK_APP_CLIENT_SECRET'],
                redirect_uri: mock_redirect_uri
            }).and_return({ 'team' => { 'name' => 'Team Foo' } })
            expect(controller.send(:exchange_temporary_auth_code_for_access_token)).to eq([mock_response, nil])
        end

        it "should rescue from Slack::Web::Api::Error during oauth_v2_access request, log it to Sentry, and return an empty response along with the error message" do
            mock_redirect_uri = 'https://foo.bar?baz=qux'
            mock_client = double('Slack::Web::Client')
            expect(controller).to receive(:redirect_uri).and_return(mock_redirect_uri)
            expect(Slack::Web::Client).to receive(:new).and_return(mock_client)
            expect(mock_client).to receive(:oauth_v2_access).and_raise(Slack::Web::Api::Errors::InvalidCode.new('invalid_code'))
            expect(Raven).to receive(:capture_exception).with('Slack OAuth responded with an error for oauth.v2.access method.', {
                extra: { error: 'invalid_code', slack_room_id: @slack_room_id, state: @state, code: @code }
            })
            expect(controller.send(:exchange_temporary_auth_code_for_access_token)).to eq([nil, 'Slack responded to oauth.v2.access request with the following error: invalid_code.'])
        end
    end

    describe "update_slack_room" do
        before(:each) do
            @slack_room_id = controller.slack_room_id = CohortSlackRoom.first.id
            @state = controller.state = SecureRandom.uuid
            @code = controller.code = SecureRandom.uuid
        end

        it "should log to Sentry and return error message if the slack room can't be found" do
            @slack_room_id = controller.slack_room_id = SecureRandom.uuid
            expect(Raven).to receive(:capture_exception).with('Could not find CohortSlackRoom during Slack OAuth flow.', {
                extra: { slack_room_id: @slack_room_id, state: @state, code: @code }
            })
            expect(controller.send(:update_slack_room, 'some_access_token')).to eq("Could not find a CohortSlackRoom record in our database for the following id: #{@slack_room_id}.")
        end

        describe "when CohortSlackRoom record is found" do

            it "should update bot_user_access_token on slack room and not return an error message when slack room is found" do
                expect(Raven).not_to receive(:capture_exception)
                expect(controller.send(:update_slack_room, 'some_access_token')).to be_nil
                expect(CohortSlackRoom.find(@slack_room_id).bot_user_access_token).to eq('some_access_token')
            end

            it "should return error message without logging to Sentry if we failed to update bot_user_access_token on slack room" do
                expect(Raven).not_to receive(:capture_exception)
                expect_any_instance_of(CohortSlackRoom).to receive(:update_attribute).with(:bot_user_access_token, 'some_access_token').and_return(false)
                expect(controller.send(:update_slack_room, 'some_access_token')).to eq("Failed to update bot_user_access_token for CohortSlackRoom record #{@slack_room_id}.")
                expect(CohortSlackRoom.find(@slack_room_id).bot_user_access_token).not_to eq('some_access_token')
            end
        end
    end

    def auth_callback_params(params = {})
        {
            code: @code,
            state: @state
        }.merge(params)
    end
end
