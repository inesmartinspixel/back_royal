const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite({
    email: 'hiring_manager_pending@pedago.com',
    initialRouteLoadedSelector: 'hiring-choose-plan'
});

it('should render choose plan page', async () => {
    await PuppetMaster.assert({
        captureSelector: '.main-box'
    });
});

it('should render no team application modal', async () => {
    await PuppetMaster.mockUserProperty('hiring_team', null);

    await PuppetMaster.assert({
        captureSelector: '.main-box'
    });
});

it('should render we are reviewing page', async () => {
    await PuppetMaster.mockUserProperty('usingUnlimitedWithSourcingHiringPlan', true);
    await PuppetMaster.waitForSelector('.we-are-reviewing');
    await PuppetMaster.assert({
        captureSelector: '.main-box'
    });
});
