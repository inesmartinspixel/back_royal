const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite();

describe('as hiring manager', () => {
    beforeAll(async () => {
        await PuppetMaster.auth('hiring_manager@pedago.com', 'password', 'hiring-positions');
    });

    it('should render app-header on hiring pages', async () => {
        await PuppetMaster.waitForSelector('app-header');
        await PuppetMaster.assert({
            captureSelector: '.app-header',
            mock: 'app-menu-user-avatar',
        });
    });

    it('should render mobile menu on hiring pages', async () => {
        await PuppetMaster.waitForSelector('.app-menu-mobile');
        await PuppetMaster.assert({
            captureSelector: '.app-menu-mobile',
            viewportNames: ['iphone5'],
        });
    });

    afterAll(async () => {
        await PuppetMaster.signOut();
    });
});

describe('as learner', () => {
    beforeAll(async () => {
        await PuppetMaster.auth('mba_user@pedago.com');
        await PuppetMaster.waitForSelector('student-dashboard');
    });

    it('should render app-header on learner pages', async () => {
        await PuppetMaster.updateAngularLocationUrl('/dashboard', 'app-header');
        await PuppetMaster.assert({
            captureSelector: '.app-header',
            mock: 'app-menu-user-avatar',
        });
    });

    it('should render mobile menu on learner pages', async () => {
        await PuppetMaster.updateAngularLocationUrl('/dashboard', '.app-menu-mobile');
        await PuppetMaster.assert({
            captureSelector: '.app-menu-mobile',
            viewportNames: ['iphone5'],
        });
    });

    afterAll(async () => {
        await PuppetMaster.signOut();
    });
});
