const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite({
    email: 'candidate_using_positions@pedago.com',
});

beforeAll(async () => {
    await PuppetMaster.evaluate(() => {
        evalHelper.setConfig({
            activate_ziprecruiter_search: 'true',
            ziprecruiter_api_key_us: 'foo_bar_baz',
            ziprecruiter_search_url: 'https://api.ziprecruiter.com/jobs/v1',
        });
    });

    await PuppetMaster.page.setRequestInterception(true);
    PuppetMaster.page.on('request', interceptedRequest => {
        if (/api\.ziprecruiter\.com/.test(interceptedRequest.url())) {
            interceptedRequest.respond({
                status: 200,
                contentType: 'application/json',
                body: JSON.stringify({
                    jobs: [
                        {
                            id: 'zip_recruiter_1',
                            salary_min: 999999,
                            salary_max: 999999,
                            salary_interval: 'yearly',
                            city: 'Saffron City',
                            location: 'Saffron City, Kanto',
                            salary_source: 'provided',
                            hiring_company: {
                                name: 'Silph Co',
                                url: 'https://foobar.com',
                            },
                            name: 'Mechanical Engineer',
                            url: 'https://foobar.com',
                            snippet:
                                'Looking for a competent mechanical engineer to help us develop state of the art Pokémon technology!',
                        },
                        {
                            id: 'zip_recruiter_2',
                            salary_min: 999999,
                            salary_max: 999999,
                            salary_interval: 'yearly',
                            city: 'Lumiose City',
                            location: 'Lumiose City, Kalos',
                            salary_source: 'parsed',
                            hiring_company: {
                                name: 'Lysandre Labs',
                                url: 'https://foobar.com',
                            },
                            name: 'Research Assistant III',
                            url: 'https://foobar.com',
                            snippet: '...come help us activate the ultimate weapon!',
                        },
                    ],
                }),
            });
        } else {
            interceptedRequest.continue();
        }
    });
});

beforeEach(async () => {
    await PuppetMaster.mockUserDefinedProperty('hasCareersNetworkAccess', true);
    await PuppetMaster.mockUserDefinedProperty('hasCareersTabAccess', true);
    await PuppetMaster.mockUserDefinedProperty('onboardingComplete', true); // Skip redirect in route_resolvers
    await PuppetMaster.mockUserDefinedProperty('careerProfilePrimaryAreasOfInterest', ['mechanical', 'research']);
});

const assertTab = async (options = {}) => {
    await PuppetMaster.removeCountsFromTabsAndPagination();
    await PuppetMaster.assert({
        captureSelector: '.main-box',
        mock: ['.app-header', '.app-menu-mobile'],
        ...options,
    });
};

describe('candidate positions page', () => {
    it('should render browse tab', async () => {
        await PuppetMaster.updateAngularLocationUrl('/careers/positions?list=open', 'featured-positions');
        await assertTab();
    });

    it('should render under review tab', async () => {
        await PuppetMaster.updateAngularLocationUrl('/careers/positions?list=accepted', 'featured-positions');
        await assertTab();
    });

    it('should render connected tab', async () => {
        await PuppetMaster.updateAngularLocationUrl('/careers/positions?list=connected', 'featured-positions');
        await assertTab();
    });

    it('should render closed tab', async () => {
        await PuppetMaster.updateAngularLocationUrl('/careers/positions?list=rejected', 'featured-positions');
        await assertTab();
    });

    it('should render recommended tab', async () => {
        await PuppetMaster.updateAngularLocationUrl('/careers/positions?list=recommended', 'featured-positions');
        await assertTab();
    });

    it('should render filters UI when expanded on mobile', async () => {
        await PuppetMaster.updateAngularLocationUrl('/careers/positions?list=rejected', 'featured-positions');
        await PuppetMaster.setViewport('iphone5');
        await PuppetMaster.click('.toggle-filters');
        await assertTab({
            viewportNames: ['iphone5'],
        });
    });
});
