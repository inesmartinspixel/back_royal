const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite({
    email: 'closed_by_candidate@pedago.com',
});

beforeEach(async () => {
    await PuppetMaster.mockUserDefinedProperty('hasCareersNetworkAccess', true);
    await PuppetMaster.mockUserDefinedProperty('hasCareersTabAccess', true);
    await PuppetMaster.mockUserDefinedProperty('onboardingComplete', true); // Skip redirect in route_resolvers
});

it('should render candidate closed connections page', async () => {
    await PuppetMaster.updateAngularLocationUrl('/careers/connections?list=closedConnections', 'connection-bar-button');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render candidate closed conversation page', async () => {
    await PuppetMaster.updateAngularLocationUrl('/careers/connections?list=closedConnections', 'connection-bar-button');
    await PuppetMaster.click('button.messages');

    // FIXME: Can remove if we solve the background-image url problem. Trying to mock
    // this out would cause content to shift downward so I'm just hiding it.
    await PuppetMaster.waitForSelector('careers-avatar');
    await PuppetMaster.evaluate(() => {
        const element = document.querySelector('careers-avatar');
        element.style.visibility = 'hidden';
    });

    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});
