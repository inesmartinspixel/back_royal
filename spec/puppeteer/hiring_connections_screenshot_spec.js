const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite({
    email: 'hiring_manager@pedago.com',
    initialRouteLoadedSelector: 'hiring-positions',
});

it('should render hiring open connections', async () => {
    await PuppetMaster.updateAngularLocationUrl('/hiring/tracker?list=activeConnections', 'hiring-connections');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render hiring conversation with messages', async () => {
    await PuppetMaster.updateAngularLocationUrl(
        '/hiring/tracker?list=activeConnections&connectionId=807f3686-45dd-43a7-b9a9-dae04ec610d7',
        'connection-conversation',
    );

    // FIXME: Can remove if we solve the background-image url problem. Trying to mock
    // this out would cause content to shift downward so I'm just hiding it.
    await PuppetMaster.waitForSelector('careers-avatar');
    await PuppetMaster.evaluate(() => {
        const elements = document.querySelectorAll('connection-conversation-message careers-avatar');
        for (let element of elements) {
            element.style.visibility = 'hidden';
        }
    });

    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render hiring team connections', async () => {
    await PuppetMaster.updateAngularLocationUrl('/hiring/tracker?list=teamConnections', 'hiring-connections');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render hiring teammate conversation with messages', async () => {
    await PuppetMaster.updateAngularLocationUrl(
        '/hiring/tracker?list=teamConnections&connectionId=3ac3fd70-08cc-41cd-a103-cd641197fca6',
        'connection-conversation',
    );

    // FIXME: Can remove if we solve the background-image url problem. Trying to mock
    // this out would cause content to shift downward so I'm just hiding it.
    await PuppetMaster.waitForSelector('careers-avatar');
    await PuppetMaster.evaluate(() => {
        const elements = document.querySelectorAll('connection-conversation-message careers-avatar');
        for (let element of elements) {
            element.style.visibility = 'hidden';
        }
    });

    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render hiring closed connections', async () => {
    await PuppetMaster.updateAngularLocationUrl('/hiring/tracker?list=closedConnections', 'candidate-list');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render hiring closed conversation with messages', async () => {
    await PuppetMaster.updateAngularLocationUrl(
        '/hiring/tracker?list=closedConnections&connectionId=de657dd9-25fd-48a2-933a-c062fe8ba3ef',
        'connection-conversation',
    );

    // FIXME: Can remove if we solve the background-image url problem. Trying to mock
    // this out would cause content to shift downward so I'm just hiding it.
    await PuppetMaster.waitForSelector('careers-avatar');
    await PuppetMaster.evaluate(() => {
        const elements = document.querySelectorAll('connection-conversation-message careers-avatar');
        for (let element of elements) {
            element.style.visibility = 'hidden';
        }
    });

    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render hiring tab with no team', async () => {
    await PuppetMaster.mockUserDefinedProperty('hiring_team_id', null);
    await PuppetMaster.updateAngularLocationUrl('/hiring/tracker?list=teamConnections', 'hiring-connections');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});
