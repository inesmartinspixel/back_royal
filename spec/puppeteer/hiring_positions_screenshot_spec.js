const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite({
    email: 'hiring_manager@pedago.com',
    initialRouteLoadedSelector: 'hiring-positions',
});

it('should render hiring positions page', async () => {
    await PuppetMaster.updateAngularLocationUrl('/hiring/positions?list=openPositions', 'position-bar');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render edit position page', async () => {
    // this id is hardcoded in the fixtures
    await PuppetMaster.updateAngularLocationUrl(
        '/hiring/positions?list=openPositions&positionId=9358610b-c2db-4ef0-99be-422509040b02&action=edit&page=1',
        'position-form',
    );
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

describe('review position page', () => {
    beforeEach(async () => {
        // this id is hardcoded in the fixtures
        await PuppetMaster.updateAngularLocationUrl(
            '/hiring/positions?list=openPositions&positionId=9358610b-c2db-4ef0-99be-422509040b02&action=edit&page=2',
            'position-form',
        );
    });

    it('should render position card with full description hidden', async () => {
        await PuppetMaster.assert({
            captureSelector: '.main-box',
            mock: ['.app-menu-mobile'],
        });
    });

    it('should render position card with full description showing', async () => {
        await PuppetMaster.click('position-card .show-hide-description');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
            mock: ['.app-menu-mobile'],
        });
    });
});

describe('position review', () => {
    it('should render all interested tab', async () => {
        await PuppetMaster.updateAngularLocationUrl('/hiring/positions?list=openPositions', 'position-bar');
        await PuppetMaster.click('.position');
        await PuppetMaster.waitForSelector('candidate-list');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
            mock: ['.app-menu-mobile'],
        });
    });

    it('should render saved tab', async () => {
        await PuppetMaster.updateAngularLocationUrl('/hiring/positions?list=openPositions', 'position-bar');
        await PuppetMaster.click('.position');
        await PuppetMaster.waitForSelector('.tabs');
        await PuppetMaster.click('.positions-saved');
        await PuppetMaster.waitForSelector('candidate-list');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
            mock: ['.app-menu-mobile'],
        });
    });

    it('should render connected tab', async () => {
        await PuppetMaster.updateAngularLocationUrl('/hiring/positions?list=openPositions', 'position-bar');
        await PuppetMaster.click('.position');
        await PuppetMaster.waitForSelector('.tabs');
        await PuppetMaster.click('.positions-connected');
        await PuppetMaster.waitForSelector('candidate-list');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
            mock: ['.app-menu-mobile'],
        });
    });

    it('should render passed tab', async () => {
        await PuppetMaster.updateAngularLocationUrl('/hiring/positions?list=openPositions', 'position-bar');
        await PuppetMaster.click('.position');
        await PuppetMaster.waitForSelector('.tabs');
        await PuppetMaster.click('.positions-passed');
        await PuppetMaster.waitForSelector('candidate-list');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
            mock: ['.app-menu-mobile'],
        });
    });
});
