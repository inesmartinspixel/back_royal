const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite({
    email: 'mba_user@pedago.com',
});

async function resetLastCohortApplicationAfterSpec() {
    await PuppetMaster.evaluate(() => {
        const CohortApplication = evalHelper.inj('CohortApplication');
        const originalLastCohortApplication = CohortApplication.new(
            evalHelper.currentUser.lastCohortApplication.asJson(),
        );
        originalLastCohortApplication.$$embeddedIn = evalHelper.currentUser;
        window.originalLastCohortApplication = originalLastCohortApplication;
    });
    PuppetMaster.resetFunctions.push(async () => {
        await PuppetMaster.page.evaluate(() => {
            evalHelper.currentUser.cohort_applications[0] = window.originalLastCohortApplication;
            delete window.originalLastCohortApplication;
            evalHelper.currentUser.subscriptions = [];
            return evalHelper.rerender();
        });
    });
}

it('should render application_status page for career_network_only, pending user', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;
        currentUser.lastCohortApplication.status = 'pending';
        currentUser.lastCohortApplication.program_type = 'career_network_only';
        return evalHelper.rerender();
    });
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render application_status page for mba, pending user', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;
        currentUser.lastCohortApplication.status = 'pending';
        currentUser.lastCohortApplication.program_type = 'mba';
        return evalHelper.rerender();
    });
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render application_status page for mba, pre_accepted user', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;
        currentUser.lastCohortApplication.status = 'pre_accepted';
        currentUser.lastCohortApplication.program_type = 'mba';
        return evalHelper.rerender();
    });
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render application_status page for mba, accepted user', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;
        currentUser.lastCohortApplication.status = 'accepted';
        currentUser.lastCohortApplication.program_type = 'mba';
        return evalHelper.rerender();
    });
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render application_status page for emba, accepted, registered user, with no required payments', async () => {
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;
        const lastCohortApplication = currentUser.lastCohortApplication;
        currentUser.relevant_cohort.program_type = 'emba';
        lastCohortApplication.program_type = 'emba';
        lastCohortApplication.status = 'accepted';
        lastCohortApplication.registered = true;

        // this makes it so that they have no required payments
        lastCohortApplication.in_good_standing = true;
        currentUser.subscriptions = [];

        return evalHelper.rerender();
    });
    await PuppetMaster.assert({
        captureSelector: '.main-box',
        mock: ['.welcome-section.video', 'careers-avatar'],
    });
});

it('should render application_status page for emba, accepted, registered, user that still requires payments', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(async () => {
        const currentUser = evalHelper.currentUser;

        // Mocking for paymentRequired
        currentUser.lastCohortApplication.in_good_standing = true;
        currentUser.subscriptions = [{}]; // create an active subscription
        currentUser.lastCohortApplication.total_num_required_stripe_payments = 5;
        currentUser.lastCohortApplication.scholarship_level = {
            name: 'No Scholarship',
            coupons: {
                emba_800: {
                    id: 'none',
                    amount_off: 0,
                    percent_off: 0,
                },
                emba_bi_4800: {
                    id: 'discount_600',
                    amount_off: 60000,
                    percent_off: null,
                },
                emba_once_9600: {
                    id: 'discount_1600',
                    amount_off: 160000,
                    percent_off: null,
                },
            },
            standard: {
                emba_800: {
                    id: 'none',
                    amount_off: 0,
                    percent_off: 0,
                },
                emba_bi_4800: {
                    id: 'discount_600',
                    amount_off: 60000,
                    percent_off: null,
                },
                emba_once_9600: {
                    id: 'discount_1600',
                    amount_off: 160000,
                    percent_off: null,
                },
            },
            early: {
                emba_800: {
                    id: 'none',
                    amount_off: 0,
                    percent_off: 0,
                },
                emba_bi_4800: {
                    id: 'discount_600',
                    amount_off: 60000,
                    percent_off: null,
                },
                emba_once_9600: {
                    id: 'discount_1600',
                    amount_off: 160000,
                    percent_off: null,
                },
            },
        };

        currentUser.lastCohortApplication.program_type = 'emba';
        currentUser.relevant_cohort.program_type = 'emba';
        currentUser.lastCohortApplication.status = 'accepted';
        currentUser.lastCohortApplication.registered = true;
        currentUser.lastCohortApplication.locked_due_to_past_due_payment = false;
        currentUser.lastCohortApplication.stripe_plan_id = 'emba_bi_4800';
        currentUser.lastCohortApplication.stripe_plans = [
            {
                id: 'emba_800',
                name: 'Monthly Plan',
                amount: '80000',
                frequency: 'monthly',
            },
            {
                id: 'emba_bi_4800',
                name: 'Bi-Annual Plan',
                amount: '480000',
                frequency: 'bi_annual',
            },
            {
                id: 'emba_once_9600',
                name: 'Single Payment',
                amount: '960000',
                frequency: 'once',
            },
        ];

        await evalHelper.rerender();

        const scope = $('tuition-status-registered').isolateScope();
        Object.defineProperty(scope, 'paymentRequired', {
            value: true,
            configurable: false,
        });
        scope.$digest();

        return evalHelper.rerender();
    });
    await PuppetMaster.assert({
        captureSelector: '.main-box',
        mock: ['.welcome-section.video', 'careers-avatar'],
    });
});

it('should render application_status page for emba, pre_accepted, unregistered, partial scholarship user', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;
        currentUser.lastCohortApplication.program_type = 'emba';
        currentUser.relevant_cohort.program_type = 'emba';
        currentUser.lastCohortApplication.status = 'pre_accepted';
        currentUser.lastCohortApplication.registered = false;

        currentUser.lastCohortApplication.stripe_plans = [
            {
                id: 'emba_800',
                name: 'Monthly Plan',
                amount: '80000',
                frequency: 'monthly',
            },
            {
                id: 'emba_bi_4800',
                name: 'Bi-Annual Plan',
                amount: '480000',
                frequency: 'bi_annual',
            },
            {
                id: 'emba_once_9600',
                name: 'Single Payment',
                amount: '960000',
                frequency: 'once',
            },
        ];

        currentUser.lastCohortApplication.scholarship_level = {
            name: 'Level 1',
            coupons: {
                emba_800: {
                    id: 'discount_150',
                    amount_off: 15000,
                    percent_off: null,
                },
                emba_bi_4800: {
                    id: 'discount_1400',
                    amount_off: 140000,
                    percent_off: null,
                },
                emba_once_9600: {
                    id: 'discount_3100',
                    amount_off: 310000,
                    percent_off: null,
                },
            },
            standard: {
                emba_800: {
                    id: 'discount_150',
                    amount_off: 15000,
                    percent_off: null,
                },
                emba_bi_4800: {
                    id: 'discount_1400',
                    amount_off: 140000,
                    percent_off: null,
                },
                emba_once_9600: {
                    id: 'discount_3100',
                    amount_off: 310000,
                    percent_off: null,
                },
            },
            early: {
                emba_800: {
                    id: 'discount_150',
                    amount_off: 15000,
                    percent_off: null,
                },
                emba_bi_4800: {
                    id: 'discount_1400',
                    amount_off: 140000,
                    percent_off: null,
                },
                emba_once_9600: {
                    id: 'discount_3100',
                    amount_off: 310000,
                    percent_off: null,
                },
            },
        };

        return evalHelper.rerender();
    });

    await PuppetMaster.assert({
        captureSelector: '.main-box',
        mock: ['.welcome-section.video', 'careers-avatar'],
    });
});

it('should render application_status page for emba, pre_accepted, unregistered, paid in full outside of product user', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;

        currentUser.lastCohortApplication.program_type = 'emba';
        currentUser.relevant_cohort.program_type = 'emba';
        currentUser.lastCohortApplication.status = 'pre_accepted';
        currentUser.lastCohortApplication.registered = false;
        currentUser.lastCohortApplication.num_charged_payments = 1;
        currentUser.lastCohortApplication.total_num_required_stripe_payments = 0;

        currentUser.lastCohortApplication.shareable_with_classmates = false; // not important, but keeps consistency with previous screenshot
        currentUser.lastCohortApplication.stripe_plan_id = 'emba_800';
        currentUser.lastCohortApplication.stripe_plans = [
            {
                id: 'emba_800',
                name: 'Monthly Plan',
                amount: '80000',
                frequency: 'monthly',
            },
        ];
        currentUser.lastCohortApplication.scholarship_level = {
            name: 'Partial Scholarship',
            standard: {
                emba_800: {
                    id: 'discount_150',
                    amount_off: 15000,
                    percent_off: null,
                },
            },
        };

        return evalHelper.rerender();
    });

    await PuppetMaster.assert({
        captureSelector: '.main-box',
        mock: ['.welcome-section.video', 'careers-avatar'],
    });
});

it('should render application_status page for emba, pre_accepted, unregistered, partial scholarship user alternative view', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;

        currentUser.lastCohortApplication.program_type = 'emba';
        currentUser.relevant_cohort.program_type = 'emba';
        currentUser.lastCohortApplication.status = 'pre_accepted';
        currentUser.lastCohortApplication.registered = false;

        currentUser.lastCohortApplication.stripe_plans = [
            {
                id: 'emba_800',
                name: 'Monthly Plan',
                amount: '80000',
                frequency: 'monthly',
            },
            {
                id: 'emba_once_9600',
                name: 'Single Payment',
                amount: '960000',
                frequency: 'once',
            },
        ];

        currentUser.lastCohortApplication.stripe_plans = [
            {
                id: 'emba_800',
                name: 'Monthly Plan',
                amount: '80000',
                frequency: 'monthly',
            },
            {
                id: 'emba_once_9600',
                name: 'Single Payment',
                amount: '960000',
                frequency: 'once',
            },
        ];

        currentUser.lastCohortApplication.scholarship_level = {
            name: 'Level 1',
            coupons: {
                emba_800: {
                    id: 'discount_150',
                    amount_off: 15000,
                    percent_off: null,
                },
                emba_once_9600: {
                    id: 'discount_3100',
                    amount_off: 310000,
                    percent_off: null,
                },
            },
            standard: {
                emba_800: {
                    id: 'discount_150',
                    amount_off: 15000,
                    percent_off: null,
                },
                emba_once_9600: {
                    id: 'discount_3100',
                    amount_off: 310000,
                    percent_off: null,
                },
            },
            early: {
                emba_800: {
                    id: 'discount_250',
                    amount_off: 25000,
                    percent_off: null,
                },
                emba_once_9600: {
                    id: 'discount_4100',
                    amount_off: 410000,
                    percent_off: null,
                },
            },
        };

        return evalHelper.rerender();
    });

    await PuppetMaster.assert({
        captureSelector: '.main-box',
        mock: ['.welcome-section.video', 'careers-avatar'],
    });
});

it('should render application_status page for emba, pre_accepted, unregistered, full scholarship user', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;

        currentUser.lastCohortApplication.program_type = 'emba';
        currentUser.relevant_cohort.program_type = 'emba';
        currentUser.lastCohortApplication.status = 'pre_accepted';
        currentUser.lastCohortApplication.registered = false;

        currentUser.lastCohortApplication.stripe_plans = [
            {
                id: 'emba_800',
                name: 'Monthly Plan',
                amount: '80000',
                frequency: 'monthly',
            },
            {
                id: 'emba_bi_4800',
                name: 'Bi-Annual Plan',
                amount: '480000',
                frequency: 'bi_annual',
            },
            {
                id: 'emba_once_9600',
                name: 'Single Payment',
                amount: '960000',
                frequency: 'once',
            },
        ];

        currentUser.lastCohortApplication.scholarship_level = {
            name: 'Full Scholarship',
            coupons: {
                emba_800: {
                    id: 'discount_100_percent',
                    amount_off: null,
                    percent_off: 100,
                },
                emba_bi_4800: {
                    id: 'discount_100_percent',
                    amount_off: null,
                    percent_off: 100,
                },
                emba_once_9600: {
                    id: 'discount_100_percent',
                    amount_off: null,
                    percent_off: 100,
                },
            },
            standard: {
                emba_800: {
                    id: 'discount_100_percent',
                    amount_off: null,
                    percent_off: 100,
                },
                emba_bi_4800: {
                    id: 'discount_100_percent',
                    amount_off: null,
                    percent_off: 100,
                },
                emba_once_9600: {
                    id: 'discount_100_percent',
                    amount_off: null,
                    percent_off: 100,
                },
            },
            early: {
                emba_800: {
                    id: 'discount_100_percent',
                    amount_off: null,
                    percent_off: 100,
                },
                emba_bi_4800: {
                    id: 'discount_100_percent',
                    amount_off: null,
                    percent_off: 100,
                },
                emba_once_9600: {
                    id: 'discount_100_percent',
                    amount_off: null,
                    percent_off: 100,
                },
            },
        };

        return evalHelper.rerender();
    });

    await PuppetMaster.assert({
        captureSelector: '.main-box',
        mock: ['.welcome-section.video', 'careers-avatar'],
    });
});

it('should render application_status page for emba, pre_accepted, unregistered, no scholarship user', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;

        currentUser.lastCohortApplication.program_type = 'emba';
        currentUser.relevant_cohort.program_type = 'emba';
        currentUser.lastCohortApplication.status = 'pre_accepted';
        currentUser.lastCohortApplication.registered = false;
        currentUser.lastCohortApplication.shareable_with_classmates = true; // not important, but keeps consistency with previous screenshot

        currentUser.lastCohortApplication.stripe_plans = [
            {
                id: 'emba_800',
                name: 'Monthly Plan',
                amount: '80000',
                frequency: 'monthly',
            },
            {
                id: 'emba_bi_4800',
                name: 'Bi-Annual Plan',
                amount: '480000',
                frequency: 'bi_annual',
            },
            {
                id: 'emba_once_9600',
                name: 'Single Payment',
                amount: '960000',
                frequency: 'once',
            },
        ];

        currentUser.lastCohortApplication.scholarship_level = {
            name: 'No Scholarship',
            coupons: {
                emba_800: {
                    id: 'none',
                    amount_off: 0,
                    percent_off: 0,
                },
                emba_bi_4800: {
                    id: 'discount_600',
                    amount_off: 60000,
                    percent_off: null,
                },
                emba_once_9600: {
                    id: 'discount_1600',
                    amount_off: 160000,
                    percent_off: null,
                },
            },
            standard: {
                emba_800: {
                    id: 'none',
                    amount_off: 0,
                    percent_off: 0,
                },
                emba_bi_4800: {
                    id: 'discount_600',
                    amount_off: 60000,
                    percent_off: null,
                },
                emba_once_9600: {
                    id: 'discount_1600',
                    amount_off: 160000,
                    percent_off: null,
                },
            },
            early: {
                emba_800: {
                    id: 'none',
                    amount_off: 0,
                    percent_off: 0,
                },
                emba_bi_4800: {
                    id: 'discount_600',
                    amount_off: 60000,
                    percent_off: null,
                },
                emba_once_9600: {
                    id: 'discount_1600',
                    amount_off: 160000,
                    percent_off: null,
                },
            },
        };

        return evalHelper.rerender();
    });

    await PuppetMaster.assert({
        captureSelector: '.main-box',
        mock: ['.welcome-section.video', 'careers-avatar'],
    });
});

it('should render application_status page for emba, pre_accepted, unregistered, no scholarship user alternative view', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;

        currentUser.lastCohortApplication.program_type = 'emba';
        currentUser.relevant_cohort.program_type = 'emba';
        currentUser.lastCohortApplication.stripe_plans = [
            {
                id: 'emba_800',
                name: 'Monthly Plan',
                amount: '80000',
                frequency: 'monthly',
            },
            {
                id: 'emba_once_9600',
                name: 'Single Payment',
                amount: '960000',
                frequency: 'once',
            },
        ];

        currentUser.lastCohortApplication.stripe_plans = [
            {
                id: 'emba_800',
                name: 'Monthly Plan',
                amount: '80000',
                frequency: 'monthly',
            },
            {
                id: 'emba_once_9600',
                name: 'Single Payment',
                amount: '960000',
                frequency: 'once',
            },
        ];

        currentUser.lastCohortApplication.scholarship_level = {
            name: 'No Scholarship',
            coupons: {
                emba_800: {
                    id: 'none',
                    amount_off: 0,
                    percent_off: 0,
                },
                emba_once_9600: {
                    id: 'discount_1600',
                    amount_off: 160000,
                    percent_off: null,
                },
            },
            standard: {
                emba_800: {
                    id: 'none',
                    amount_off: 0,
                    percent_off: 0,
                },
                emba_once_9600: {
                    id: 'none',
                    amount_off: 0,
                    percent_off: null,
                },
            },
            early: {
                emba_800: {
                    id: 'discount_100',
                    amount_off: 100,
                    percent_off: 0,
                },
                emba_once_9600: {
                    id: 'discount_2600',
                    amount_off: 260000,
                    percent_off: null,
                },
            },
        };
        currentUser.lastCohortApplication.shareable_with_classmates = true; // not important, but keeps consistency with previous screenshot

        return evalHelper.rerender();
    });

    await PuppetMaster.assert({
        captureSelector: '.main-box',
        mock: ['.welcome-section.video', 'careers-avatar'],
    });
});

it('should render application_status page for emba, pre_accepted, registered user, with no required payments', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;
        const lastCohortApplication = currentUser.lastCohortApplication;
        currentUser.relevant_cohort.program_type = 'emba';
        lastCohortApplication.program_type = 'emba';
        lastCohortApplication.status = 'pre_accepted';
        lastCohortApplication.registered = true;

        // this makes it so that they have no required payments
        lastCohortApplication.in_good_standing = true;
        currentUser.subscriptions = [];

        return evalHelper.rerender();
    });
    await PuppetMaster.assert({
        captureSelector: '.main-box',
        mock: ['.welcome-section.video', 'careers-avatar'],
    });
});

it('should render application_status page for emba, pre_accepted, registered, past due user', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;

        // Mocking for paymentRequired
        currentUser.lastCohortApplication.in_good_standing = false;
        currentUser.subscriptions = [{}]; // create an primarySubscription
        currentUser.lastCohortApplication.total_num_required_stripe_payments = 5;
        currentUser.lastCohortApplication.scholarship_level = {
            name: 'No Scholarship',
            coupons: {
                emba_800: {
                    id: 'none',
                    amount_off: 0,
                    percent_off: 0,
                },
                emba_bi_4800: {
                    id: 'discount_600',
                    amount_off: 60000,
                    percent_off: null,
                },
                emba_once_9600: {
                    id: 'discount_1600',
                    amount_off: 160000,
                    percent_off: null,
                },
            },
            standard: {
                emba_800: {
                    id: 'none',
                    amount_off: 0,
                    percent_off: 0,
                },
                emba_bi_4800: {
                    id: 'discount_600',
                    amount_off: 60000,
                    percent_off: null,
                },
                emba_once_9600: {
                    id: 'discount_1600',
                    amount_off: 160000,
                    percent_off: null,
                },
            },
            early: {
                emba_800: {
                    id: 'none',
                    amount_off: 0,
                    percent_off: 0,
                },
                emba_bi_4800: {
                    id: 'discount_600',
                    amount_off: 60000,
                    percent_off: null,
                },
                emba_once_9600: {
                    id: 'discount_1600',
                    amount_off: 160000,
                    percent_off: null,
                },
            },
        };

        currentUser.lastCohortApplication.program_type = 'emba';
        currentUser.relevant_cohort.program_type = 'emba';
        currentUser.lastCohortApplication.registered = true;
        currentUser.lastCohortApplication.locked_due_to_past_due_payment = true;
        currentUser.lastCohortApplication.stripe_plan_id = 'emba_800';
        currentUser.lastCohortApplication.stripe_plans = [
            {
                id: 'emba_800',
                name: 'Monthly Plan',
                amount: '80000',
                frequency: 'monthly',
            },
            {
                id: 'emba_bi_4800',
                name: 'Bi-Annual Plan',
                amount: '480000',
                frequency: 'bi_annual',
            },
            {
                id: 'emba_once_9600',
                name: 'Single Payment',
                amount: '960000',
                frequency: 'once',
            },
        ];

        return evalHelper.rerender();
    });
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });

    await PuppetMaster.evaluate(() => {});
});

it('should render application_status page for emba, pre_accepted, unregistered, partial payment user', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;
        currentUser.lastCohortApplication.program_type = 'emba';
        currentUser.relevant_cohort.program_type = 'emba';
        currentUser.lastCohortApplication.registered = false;
        currentUser.lastCohortApplication.locked_due_to_past_due_payment = false;
        currentUser.lastCohortApplication.in_good_standing = undefined;
        currentUser.lastCohortApplication.num_charged_payments = 3;
        currentUser.lastCohortApplication.total_num_required_stripe_payments = 5;
        currentUser.lastCohortApplication.shareable_with_classmates = true; // not important, but keeps consistency with previous screenshot

        currentUser.lastCohortApplication.stripe_plan_id = 'emba_800';
        currentUser.lastCohortApplication.stripe_plans = [
            {
                id: 'emba_800',
                name: 'Monthly Plan',
                amount: '80000',
                frequency: 'monthly',
            },
        ];
        currentUser.lastCohortApplication.scholarship_level = {
            name: 'No Scholarship',
            standard: {
                emba_800: {
                    id: 'none',
                    amount_off: 0,
                    percent_off: 0,
                },
            },
        };

        return evalHelper.rerender();
    });

    await PuppetMaster.assert({
        captureSelector: '.main-box',
        mock: ['.welcome-section.video', 'careers-avatar'],
    });
});

it('should render application_status page for emba, deferred user', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;
        currentUser.lastCohortApplication.status = 'deferred';
        currentUser.lastCohortApplication.program_type = 'emba';
        currentUser.relevant_cohort.program_type = 'emba';

        return evalHelper.rerender();
    });
    await PuppetMaster.assert({
        captureSelector: '.main-box',
        mock: ['.welcome-section.video', 'careers-avatar'],
    });
});

it('should render application_status page for emba, pending user', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;
        currentUser.lastCohortApplication.program_type = 'emba';
        currentUser.relevant_cohort.program_type = 'emba';
        currentUser.lastCohortApplication.status = 'pending';
        return evalHelper.rerender();
    });
    await PuppetMaster.assert({
        captureSelector: '.main-box',
        mock: ['.welcome-section.video', 'careers-avatar'],
    });
});

// Since paid certs are not live right now, I'm leaving these commented out rather than fixing them
xit('should render application_status page for paid_cert, pre_accepted user', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;
        currentUser.lastCohortApplication.status = 'pre_accepted';
        currentUser.lastCohortApplication.program_type = 'paid_cert_data_analytics';
        currentUser.relevant_cohort.id = 'bceb59b6-1fc1-43e5-b6a7-7b8252540013';
        currentUser.can_purchase_paid_certs = true;
        currentUser.registered = false;
        return evalHelper.rerender();
    });
    await PuppetMaster.waitForSelector('paid-certificate-checkout');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

xit('should render application_status page for paid_cert, accepted user', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;
        currentUser.lastCohortApplication.status = 'accepted';
        currentUser.lastCohortApplication.program_type = 'paid_cert_data_analytics';
        currentUser.relevant_cohort.id = 'bceb59b6-1fc1-43e5-b6a7-7b8252540013';
        currentUser.can_purchase_paid_certs = true;
        currentUser.registered = false;
        return evalHelper.rerender();
    });
    await PuppetMaster.waitForSelector('paid-certificate-checkout');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

xit('should render application_status page for user that should see paid cert selection', async () => {
    resetLastCohortApplicationAfterSpec();
    await PuppetMaster.updateAngularLocationUrl('/settings/application_status', 'settings');
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;
        currentUser.lastCohortApplication.status = 'rejected';
        currentUser.can_purchase_paid_certs = true;
        return evalHelper.rerender();
    });
    await PuppetMaster.waitForSelector('paid-certificate-checkout');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
    await PuppetMaster.evaluate(() => {
        const currentUser = evalHelper.currentUser;
        currentUser.lastCohortApplication.status = 'accepted';
        currentUser.can_purchase_paid_certs = false;
        return evalHelper.rerender();
    });
});
