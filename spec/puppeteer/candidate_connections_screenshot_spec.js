const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite({
    email: 'invited_candidate@pedago.com',
});

beforeEach(async () => {
    await PuppetMaster.mockUserDefinedProperty('hasCareersNetworkAccess', true);
    await PuppetMaster.mockUserDefinedProperty('hasCareersTabAccess', true);
    await PuppetMaster.mockUserDefinedProperty('onboardingComplete', true); // Skip redirect in route_resolvers
});

it('should render candidate open connections page', async () => {
    await PuppetMaster.updateAngularLocationUrl(
        '/careers/connections?list=activeConnections',
        '.connections-container',
    );
    await PuppetMaster.assert({
        captureSelector: '.main-box',
        mock: ['.app-menu-mobile', 'careers-avatar'],
    });
});

it('should render candidate open conversation page', async () => {
    await PuppetMaster.updateAngularLocationUrl(
        '/careers/connections?list=activeConnections',
        '.connections-container',
    );
    await PuppetMaster.click('button.chat');

    // FIXME: Can remove if we solve the background-image url problem. Trying to mock
    // this out would cause content to shift downward so I'm just hiding it.
    await PuppetMaster.waitForSelector('careers-avatar');
    await PuppetMaster.evaluate(() => {
        const elements = document.querySelectorAll('careers-avatar');
        for (let element of elements) {
            element.style.visibility = 'hidden';
        }
    });

    await PuppetMaster.waitForSelector('connection-conversation');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
        mock: '.app-menu-mobile',
    });
});

it('should render simple hiring manager card', async () => {
    await PuppetMaster.updateAngularLocationUrl(
        '/careers/connections?list=activeConnections',
        '.connections-container',
    );
    await PuppetMaster.click('button.chat');
    await PuppetMaster.waitForSelector('connection-conversation');

    const originalHiringRelationshipViewModel = await PuppetMaster.evaluate(() => {
        const scope = $('connection-conversation').isolateScope();
        const originalHiringRelationshipViewModel = Object.assign({}, scope.hiringRelationshipViewModel);

        scope.$apply(() => {
            scope.hiringRelationshipViewModel.hiringApplication.website_url = undefined;
            scope.hiringRelationshipViewModel.hiringApplication.company_logo_url = undefined;
            scope.hiringRelationshipViewModel.hiringApplication.company_year = undefined;
            scope.hiringRelationshipViewModel.hiringApplication.funding = undefined;
            scope.hiringRelationshipViewModel.hiringApplication.company_annual_revenue = undefined;
            scope.hiringRelationshipViewModel.hiringApplication.company_employee_count = undefined;
            scope.hiringRelationshipViewModel.hiringApplication.team_mission = undefined;
            scope.hiringRelationshipViewModel.hiringApplication.industry = undefined;
            scope.hiringRelationshipViewModel.hiringApplication.team_name = undefined;
            scope.hiringRelationshipViewModel.hiringApplication.role_descriptors = [];
            scope.hiringRelationshipViewModel.hiringApplication.fun_fact = undefined;
            scope.hiringRelationshipViewModel.hiringApplication.job_role = undefined;
            scope.hiringRelationshipViewModel.hiringApplication.avatar_url = undefined;
        });

        return originalHiringRelationshipViewModel;
    });

    await PuppetMaster.resetFunctions.push(async () => {
        await PuppetMaster.evaluate(originalHiringRelationshipViewModel => {
            const scope = $('connection-conversation').isolateScope();
            scope.hiringRelationshipViewModel = originalHiringRelationshipViewModel;
        }, originalHiringRelationshipViewModel);
    });

    // https://github.com/GoogleChrome/puppeteer/issues/2977#issuecomment-412807613
    await PuppetMaster.evaluate(() => {
        document.querySelector('button.profile').click();
    });

    await PuppetMaster.waitForSelector('hiring-manager-card');
    await PuppetMaster.assert({
        captureSelector: 'hiring-manager-card',
        mock: '.app-menu-mobile',
    });
});
