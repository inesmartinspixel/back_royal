const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite();

const waitForCaptcha = async () => {
    await PuppetMaster.waitForSelector('.g-recaptcha iframe');
    const frames = await PuppetMaster.page.frames();
    await frames[1].waitForSelector('.rc-anchor-content');
};

it('should render candidates signup page with gdpr privacy notice', async () => {
    await PuppetMaster.goto('/candidates/signup');
    await PuppetMaster.page.$eval('[marketing-sign-up-form]', form => {
        const scope = $(form).scope();
        scope.$apply(() => {
            scope.gdprAppliesToUser = true;
        });
    });
    await PuppetMaster.waitForSelector('.privacy-notice');
    await PuppetMaster.assert({
        captureSelector: '.onboarding__card'
    });
});

it('should render employers signup page with gdpr privacy notice', async () => {
    // /employers/signup is a Smartly route, so we need to force the host to be
    // smart.ly so that the page will render on localhost rather than actually
    // redirecting us to smart.ly.
    await PuppetMaster.goto('/employers/signup?force_host=smart.ly');
    await PuppetMaster.page.$eval('[marketing-sign-up-form]', form => {
        const scope = $(form).scope();
        scope.$apply(() => {
            scope.gdprAppliesToUser = true;
        });
    });
    await PuppetMaster.waitForSelector('.privacy-notice');
    await PuppetMaster.assert({
        captureSelector: '.onboarding__card'
    });
});

it('should render demo signup page', async () => {
    await PuppetMaster.goto('/demo');
    await waitForCaptcha();
    await PuppetMaster.assert({
        captureSelector: '.demo-join'
    });
});

it('should render logged out page', async () => {
    await PuppetMaster.goto('/logged-out');
    await PuppetMaster.waitForSelector('logged-out-page');
    await PuppetMaster.assert({
        captureSelector: '.sessions'
    });
});

it('should render forgot password page', async () => {
    await PuppetMaster.goto('/forgot-password');
    await PuppetMaster.waitForSelector('onboarding-forgot-password');
    await PuppetMaster.assert({
        captureSelector: '.onboarding_wrapper'
    });
});

it('should render georgetown join page', async () => {
    await PuppetMaster.goto('/georgetownmsb/join/account');
    await waitForCaptcha();
    await PuppetMaster.assert({
        captureSelector: '.institution-register'
    });
});

it('should render emirates join page', async () => {
    await PuppetMaster.goto('/emiratesdb/join/account');
    await waitForCaptcha();
    await PuppetMaster.assert({
        captureSelector: '.institution-register'
    });
});

it('should render sign in page', async () => {
    await PuppetMaster.goto('/sign-in');
    await PuppetMaster.waitForSelector('onboarding-login');
    await PuppetMaster.assert({
        captureSelector: '.onboarding_wrapper'
    });
});

it('should render georgetown sign in page', async () => {
    await PuppetMaster.goto('/georgetownmsb/sign-in');
    await PuppetMaster.waitForSelector('sign-in');
    await PuppetMaster.assert({
        captureSelector: '.sign-in'
    });
});

it('should render emirates sign in page', async () => {
    await PuppetMaster.goto('/emiratesdb/sign-in');
    await PuppetMaster.waitForSelector('sign-in');
    await PuppetMaster.assert({
        captureSelector: '.sign-in'
    });
});
