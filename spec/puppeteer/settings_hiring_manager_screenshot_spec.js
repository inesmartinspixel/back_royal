const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite({
    email: 'hiring_manager@pedago.com',
    initialRouteLoadedSelector: 'hiring-positions',
});

it('should render hiring preferences page', async () => {
    await PuppetMaster.updateAngularLocationUrl('/settings/preferences', 'settings');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render hiring notifications page', async () => {
    await PuppetMaster.updateAngularLocationUrl('/settings/notifications', 'settings');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render hiring billing page', async () => {
    await PuppetMaster.updateAngularLocationUrl('/settings/billing', 'settings');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render hiring billing success page', async () => {
    await PuppetMaster.updateAngularLocationUrl('/settings/billing', 'settings');
    await PuppetMaster.evaluate(() => {
        $('hiring-billing').isolateScope().showScreen = 'success';
    });
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render hiring profile page 1', async () => {
    await PuppetMaster.updateAngularLocationUrl('/settings/profile?page=1', 'settings');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render hiring profile page 2', async () => {
    await PuppetMaster.updateAngularLocationUrl('/settings/profile?page=2', 'settings');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render hiring profile page 3', async () => {
    await PuppetMaster.updateAngularLocationUrl('/settings/profile?page=3', 'settings');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
        mock: ['careers-avatar'],
    });
});

it('should render hiring profile page 4', async () => {
    await PuppetMaster.updateAngularLocationUrl('/settings/profile?page=4', 'settings');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render hiring profile page 5', async () => {
    await PuppetMaster.updateAngularLocationUrl('/settings/profile?page=5', 'settings');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
        mock: 'hiring-manager-card .body',
    });
});
