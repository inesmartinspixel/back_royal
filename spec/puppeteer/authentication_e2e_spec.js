const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite();

describe('pref_locale', () => {
    afterEach(async () => {
        await PuppetMaster.signOut();
    });

    it('should assign pref_locale to force_locale query string param if present', async () => {
        await PuppetMaster.signUp(`${Math.random()}@example.com`, 'password', '?force_locale=es');
        const prefLocale = await PuppetMaster.evaluate(() => evalHelper.currentUser.pref_locale);
        expect(prefLocale).toEqual('es');
        const preferredCode = await PuppetMaster.evaluate(() => evalHelper.inj('Locale').preferredCode);
        expect(preferredCode).toEqual('es');
    });

    it("should default to 'en' for FREEMBA signups", async () => {
        await PuppetMaster.evaluate(() => {
            window.serverDeterminedLocale = 'es';
        });
        await PuppetMaster.signUp(`${Math.random()}@example.com`, 'password');
        const prefLocale = await PuppetMaster.evaluate(() => evalHelper.currentUser.pref_locale);
        expect(prefLocale).toEqual('en');
        const preferredCode = await PuppetMaster.evaluate(() => evalHelper.inj('Locale').preferredCode);
        expect(preferredCode).toEqual('en');
    });
});
