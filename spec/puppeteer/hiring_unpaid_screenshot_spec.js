const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite({
    email: 'hiring_manager_unpaid@pedago.com',
    initialRouteLoadedSelector: 'hiring-browse-candidates',
});

it('should render upsell message', async () => {
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render hiring unpaid settings page', async () => {
    await PuppetMaster.updateAngularLocationUrl('/settings/billing');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});
