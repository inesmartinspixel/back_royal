const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite({
    email: 'mba_user@pedago.com',
});

it('should render account page', async () => {
    await PuppetMaster.updateAngularLocationUrl('/settings/account', 'settings');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

describe('career profile forms', () => {
    it('should render basic info form', async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/my-profile?page=1', 'edit-career-profile');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });

    it('should render more about you form', async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/my-profile?page=2', 'edit-career-profile');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });

    describe('education form', () => {
        it('should render locked', async () => {
            await PuppetMaster.updateAngularLocationUrl('/settings/my-profile?page=3', 'edit-career-profile');
            await PuppetMaster.assert({
                captureSelector: '.main-box',
            });
        });

        xit('should render unlocked', async () => {
            // use the pending applied_mba_user@pedago.com user
        });
    });

    it('should render work history form', async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/my-profile?page=4', 'edit-career-profile');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });

    it('should render skills form', async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/my-profile?page=5', 'edit-career-profile');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });

    it('should render career preferences form', async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/my-profile?page=6', 'edit-career-profile');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });

    it('should render personal summary form', async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/my-profile?page=7', 'edit-career-profile');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });

    it('should render resume and links form', async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/my-profile?page=8', 'edit-career-profile');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });

    it('should render network settings form', async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/my-profile?page=9', 'edit-career-profile');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });

    it('should render profile preivew', async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/my-profile?page=10', 'edit-career-profile');

        await PuppetMaster.page.evaluate(() => {
            // This can change with fixtures changes, and we don't care
            $('.card-list-item').height(200).css({
                overflow: 'hidden',
            });
        });

        await PuppetMaster.assert({
            captureSelector: '.main-box',
            mock: ['.card-list-item'],
        });
    });
});

describe('career profile forms in review', () => {
    it('should render basic info form with in review message', async () => {
        await PuppetMaster.mockUserProperty('can_edit_career_profile', false);
        await PuppetMaster.mockUserDefinedProperty('hasEditCareerProfileAccess', true);
        await PuppetMaster.updateAngularLocationUrl('/settings/my-profile?page=1', 'edit-career-profile');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });
});

it('should render notifications page', async () => {
    await PuppetMaster.updateAngularLocationUrl('/settings/notifications', 'settings');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render preferences page', async () => {
    await PuppetMaster.updateAngularLocationUrl('/settings/preferences', 'settings');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

// it('should render transcripts page', async() => {
//     await PuppetMaster.updateAngularLocationUrl('/settings/transcripts', 'settings');
//     await PuppetMaster.assert({
//         captureSelector: '.main-box'
//     });
// });

(() => {
    const setCanEditMbaApplication = async () => {
        await PuppetMaster.mockUserProperty('can_edit_career_profile', false);
        await PuppetMaster.mockUserDefinedProperty('lastCohortApplicationStatus', undefined);
        await PuppetMaster.mockUserDefinedProperty('acceptedCohortApplication', undefined);
        await PuppetMaster.mockUserDefinedProperty('hasStudentNetworkSettingsAccess', false);
    };

    const runMbaApplicationTest = async index => {
        it(`should render application page ${index}`, async () => {
            await setCanEditMbaApplication();
            await PuppetMaster.updateAngularLocationUrl(`/settings/application?page=${index}`, 'edit-career-profile');

            // FIXME: Investigate the background-image url rendering issue with .icon
            await PuppetMaster.assert({
                captureSelector: '.main-box',
                mock: ['.program-icon .icon'],
            });
        });
    };

    for (let i = 1; i <= 9; i++) {
        runMbaApplicationTest(i);
    }

    it('should render application page 3 with english language proficiency work experience input', async () => {
        await setCanEditMbaApplication();
        await PuppetMaster.updateAngularLocationUrl('/settings/application?page=3', 'edit-career-profile');

        // The editCareerProfile directive creates a proxy of the user's
        // career_profile and exposes it as scope.careerProfile. So, we
        // can't just call `mockCareerProfileProperty` here.
        await PuppetMaster.evaluate(() => {
            const scope = $('edit-career-profile').isolateScope();
            Object.defineProperty(scope.careerProfile, 'sufficient_english_work_experience', {
                value: true,
                configurable: false,
            });
            Object.defineProperty(scope.careerProfile, 'english_work_experience_description', {
                value: 'Pedago LLC',
                configurable: false,
            });
            scope.$digest();
        });

        await PuppetMaster.assert({
            captureSelector: '.main-box',
            mock: ['.program-icon .icon'],
        });
    });

    /* eslint-disable */
    const assertHelpscoutScreenshot = async () => {
        return PuppetMaster.assert({
            // the .form-group matcher should hide most everything inside of the
            // form so that hopefully some irrelevant changes in there will not affect this
            // screenshot.  We still want to be able to see the save button though
            mock: ['.responsive-nav', '.main-header', '.form-group.row:not(.form-actions)'],
            setViewport: false,
        });
    };
    /* eslint-enable */

    // We have to handle the viewports explicitly because
    // 1. the helpScout beacon behaves differently depending on the size
    // of the screen when it is initialized
    // 2. we want to see screenshots on realistic heights with the scroll set
    // in different places
    ['desktop', 'mobile'].forEach(viewport => {
        xit(`should render helpScoutBeacon on ${viewport}}`, async () => {
            await setCanEditMbaApplication();
            await PuppetMaster.page.setViewport({
                width: viewport === 'mobile' ? 475 : 1200,
                height: viewport === 'desktop' ? 667 : 900,
            });

            // pick a tall page so we can see how the beacon button lines up with the
            // submit button.  Take a screenshot when the user is scrolled to the top
            // of the page
            await PuppetMaster.updateAngularLocationUrl('/settings/application?page=3', 'edit-career-profile');
            await PuppetMaster.evaluate(() => {
                evalHelper.inj('helpScoutBeacon').stopWatching();
                evalHelper.inj('helpScoutBeacon').startWatching();
                $('[ng-controller]').injector().get('scrollHelper').scrollToPosition(0);
            });

            await PuppetMaster.page.waitForSelector('.BeaconFabButtonFrame');

            await assertHelpscoutScreenshot();

            // take another screenshot when the user is scrolled all the way down
            await PuppetMaster.evaluate(() => {
                $('[ng-controller]').injector().get('scrollHelper').scrollToPosition(200000);
            });

            await assertHelpscoutScreenshot();

            await PuppetMaster.evaluate(() => {
                evalHelper.inj('helpScoutBeacon').stopWatching();
            });
        });
    });
})();

it('should render application page 5 for career_network_only', async () => {
    await PuppetMaster.mockUserProperty('can_edit_career_profile', true);
    await PuppetMaster.mockUserDefinedProperty('lastCohortApplicationStatus', undefined);
    await PuppetMaster.evaluate(
        () => (evalHelper.currentUser.lastCohortApplication.program_type = 'career_network_only'),
    );
    PuppetMaster.resetFunctions.push(async () => {
        await PuppetMaster.evaluate(() => (evalHelper.currentUser.lastCohortApplication.program_type = 'mba'));
    });
    await PuppetMaster.updateAngularLocationUrl('/settings/application?page=5', 'edit-career-profile');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

it('should render basic info form with invalid fields links', async () => {
    await PuppetMaster.mockUserProperty('can_edit_career_profile', false);
    await PuppetMaster.mockUserDefinedProperty('lastCohortApplicationStatus', undefined);
    await PuppetMaster.mockUserDefinedProperty('acceptedCohortApplication', undefined);
    await PuppetMaster.mockUserDefinedProperty('hasStudentNetworkSettingsAccess', false);
    await PuppetMaster.mockCareerProfileProperty('nickname', null);
    await PuppetMaster.mockCareerProfileProperty('birthday', null);
    await PuppetMaster.mockCareerProfileProperty('place_id', null);
    await PuppetMaster.updateAngularLocationUrl('/settings/application?page=2', 'edit-career-profile');
    await PuppetMaster.click('[name=save]');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});

describe('program lockdown', () => {
    beforeAll(async () => {
        await PuppetMaster.mockUserProperty('can_edit_career_profile', false);
        await PuppetMaster.mockUserDefinedProperty('acceptedOrPreAcceptedCohortApplication', false);
        await PuppetMaster.mockUserDefinedProperty('reapplyingOrEditingApplication', true);
        await PuppetMaster.mockUserDefinedProperty('acceptedCohortApplication', undefined);
        await PuppetMaster.updateAngularLocationUrl('/settings/account');
    });

    it('should render program lockdown page', async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/application?page=1', 'edit-career-profile');
        await PuppetMaster.evaluate(() => {
            const scope = $('program-lockdown-form').scope();
            scope.decisionDate = 'January 1';
            scope.$digest();
        });
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });

    it('should render program lockdown page with option to switch to EMBA', async () => {
        await PuppetMaster.evaluate(() => (evalHelper.currentUser.lastCohortApplication.can_convert_to_emba = true));
        await PuppetMaster.updateAngularLocationUrl('/settings/application?page=1', 'edit-career-profile');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
            mock: ['.program-icon .icon'],
        });
    });

    afterAll(async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/account');
    });
});

it('should render documents page', async () => {
    await PuppetMaster.mockUserDefinedProperty('isAccepted', true);
    await PuppetMaster.updateAngularLocationUrl('/settings/documents', 'settings');
    await PuppetMaster.assert({
        captureSelector: '.main-box',
    });
});
