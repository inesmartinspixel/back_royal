const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite();

describe('graduated mba user', () => {
    beforeAll(async () => {
        await PuppetMaster.auth('frame_list_cohort_user@pedago.com');
    });

    afterAll(async () => {
        await PuppetMaster.signOut();
    });

    it('should generate graded transcript data', async () => {
        const transcriptData = await PuppetMaster.evaluate(() => {
            const $rootScope = $('[ng-controller]')
                .injector()
                .get('$rootScope');
            const TranscriptHelper = $('[ng-controller]')
                .injector()
                .get('TranscriptHelper');
            const scope = $rootScope.$new();

            scope.transcriptHelper = new TranscriptHelper();
            return scope.transcriptHelper._buildTranscriptData($rootScope.currentUser, true);
        });

        expect(transcriptData).toMatchSnapshot();
    });

    it('should generate ungraded transcript data', async () => {
        const transcriptData = await PuppetMaster.evaluate(() => {
            const $rootScope = $('[ng-controller]')
                .injector()
                .get('$rootScope');
            const TranscriptHelper = $('[ng-controller]')
                .injector()
                .get('TranscriptHelper');
            const scope = $rootScope.$new();

            scope.transcriptHelper = new TranscriptHelper();
            return scope.transcriptHelper._buildTranscriptData($rootScope.currentUser, false);
        });

        expect(transcriptData).toMatchSnapshot();
    });
});
