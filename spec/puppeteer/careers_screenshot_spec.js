const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite({
    email: 'mba_user@pedago.com',
});

it('should render welcome page', async () => {
    await PuppetMaster.updateAngularLocationUrl('/careers/welcome', 'careers-welcome');
    await PuppetMaster.waitFor('position-card');
    await PuppetMaster.assert({
        captureSelector: '.careers-welcome',
    });
});

it('should render nav', async () => {
    await PuppetMaster.updateAngularLocationUrl('/careers/welcome', '.main-box');
    await PuppetMaster.assert({
        viewportNames: ['desktop'],
        mock: ['.main-box', '.app-header'],
    });

    await PuppetMaster.setViewport(['iphone5']);
    await PuppetMaster.click('.mobile-nav-toggle-btn');
    await PuppetMaster.assert({
        viewportNames: ['iphone5'],
        mock: ['.app-menu-mobile'],
    });
});

describe('careers card', () => {
    it('should render in pending state', async () => {
        await mockExperienceDuration();
        await PuppetMaster.mockUserProperty('can_edit_career_profile', true);
        await PuppetMaster.mockCareerProfileProperty('complete', false);
        await PuppetMaster.mockCareerProfileProperty('last_calculated_complete_percentage', 0);
        await PuppetMaster.rootScopeApply();

        await PuppetMaster.updateAngularLocationUrl('/careers/card', 'preview-candidate-card');

        await PuppetMaster.evaluate(() => {
            const scope = $('preview-profile-status-message').isolateScope();
            Object.defineProperty(scope, 'status', {
                value: 'applied',
            });
            scope.$digest();
        });

        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });

    it('should render in reviewing state', async () => {
        await PuppetMaster.mockUserProperty('can_edit_career_profile', false);
        await PuppetMaster.mockCareerProfileProperty('complete', true);
        await PuppetMaster.mockCareerProfileProperty('last_calculated_complete_percentage', 100);
        await PuppetMaster.mockUserDefinedProperty('lastCohortApplicationStatus', 'accepted');
        await PuppetMaster.rootScopeApply();

        await PuppetMaster.updateAngularLocationUrl('/careers/card', 'preview-candidate-card');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
            mock: '.card-list-item',
        });
    });

    it('should render in incomplete state', async () => {
        await mockExperienceDuration();
        await PuppetMaster.mockUserProperty('can_edit_career_profile', true);
        await PuppetMaster.mockCareerProfileProperty('complete', false);
        await PuppetMaster.mockCareerProfileProperty('last_calculated_complete_percentage', 0);
        await PuppetMaster.rootScopeApply();

        await PuppetMaster.updateAngularLocationUrl('/careers/card', 'preview-candidate-card');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });

    it('should render in complete state', async () => {
        await PuppetMaster.mockUserProperty('can_edit_career_profile', true);
        await PuppetMaster.mockCareerProfileProperty('complete', true);
        await PuppetMaster.mockCareerProfileProperty('last_calculated_complete_percentage', 100);
        await PuppetMaster.rootScopeApply();

        await PuppetMaster.updateAngularLocationUrl('/careers/card', 'preview-candidate-card');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
            mock: 'candidate-list-card',
        });
    });

    it('should render in rejected state', async () => {
        await PuppetMaster.mockUserProperty('can_edit_career_profile', false);
        await PuppetMaster.mockCareerProfileProperty('complete', true);
        await PuppetMaster.mockCareerProfileProperty('last_calculated_complete_percentage', 100);
        await PuppetMaster.mockUserDefinedProperty('isAccepted', false);
        await PuppetMaster.mockUserDefinedProperty('isDeferred', false);
        await PuppetMaster.mockUserDefinedProperty('isPreAccepted', false);
        await PuppetMaster.mockUserDefinedProperty('isRejectedOrExpelled', true);
        await PuppetMaster.rootScopeApply();

        await PuppetMaster.updateAngularLocationUrl('/careers/card', 'preview-candidate-card');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });

    it('should render candidate list card', async () => {
        await mockExperienceDuration();
        await PuppetMaster.mockUserProperty('can_edit_career_profile', true);
        await PuppetMaster.mockCareerProfileProperty('complete', true);
        await PuppetMaster.mockCareerProfileProperty('last_calculated_complete_percentage', 100);
        await PuppetMaster.rootScopeApply();

        await PuppetMaster.updateAngularLocationUrl('/careers/card', '.card-list-item');
        await PuppetMaster.rerender();
        await PuppetMaster.assert({
            captureSelector: '.card-list-item',
        });
    });

    async function mockExperienceDuration() {
        const fortyTwoYears = 42 * 365.25 * 24 * 60 * 60 * 1000 + 7 * 24 * 60 * 60 * 1000;
        await PuppetMaster.mockPrototypeGetter('WorkExperience', 'duration', fortyTwoYears);
    }
});

// FIXME: I had to put this test at the bottom due to digest errors that it caused for later tests. Since
// we teardown and setup the browser on each suite, doing this will be less problematic than it was in Casper.
// But still, I would like to figure out what the issue is.
it('should render apply modal', async () => {
    await PuppetMaster.updateAngularLocationUrl('/settings/account');
    await PuppetMaster.mockUserDefinedProperty('hasAppliedOrHasAccessToCareers', false);
    await PuppetMaster.rootScopeApply();

    await PuppetMaster.updateAngularLocationUrl('/careers/apply', '.main-container');
    await PuppetMaster.assert({
        selector: 'partial-screen-dialog-modal',
        mock: ['.app-header', '.app-menu-mobile'],
    });
});
