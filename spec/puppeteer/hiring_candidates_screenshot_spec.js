const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite({
    email: 'hiring_manager@pedago.com',
    initialRouteLoadedSelector: 'hiring-positions',
});

it('should render hiring featured candidates page', async () => {
    await PuppetMaster.updateAngularLocationUrl('/hiring/browse-candidates?tab=featured', 'candidate-list');
    await PuppetMaster.removeThingsFromHiringBrowseThatChangeBasedOnNumberOfResults();
    await PuppetMaster.assert({
        captureSelector: '.page-with-left-nav-and-main-box',
        mock: ['.app-menu-mobile', '.list-wrapper'],
    });
});

it('should render invite to connect modal', async () => {
    await PuppetMaster.updateAngularLocationUrl('/hiring/browse-candidates?tab=featured', 'candidate-list');
    await PuppetMaster.click('button.invite');
    await PuppetMaster.waitForSelector('.modal-content .front-royal-form-container');
    await PuppetMaster.assert({
        captureSelector: '.modal-content',
        mock: ['.app-menu-mobile', '.list-wrapper'],
    });
});

it('should render hiring featured candidates page with all reviewed', async () => {
    await PuppetMaster.click('.modal-content .close');
    await stubNoCandidatesAndAssert('featured');
});

it('should render hiring hidden candidates page', async () => {
    await PuppetMaster.updateAngularLocationUrl('/hiring/browse-candidates?tab=hidden', 'candidate-list');
    await PuppetMaster.removeThingsFromHiringBrowseThatChangeBasedOnNumberOfResults();
    await PuppetMaster.assert({
        captureSelector: '.page-with-left-nav-and-main-box',
        mock: ['.app-menu-mobile', '.list-wrapper'],
    });
});

it('should render hiring hidden candidates page with none hidden', async () => {
    await stubNoCandidatesAndAssert('hidden');
});

it('should render hiring browse candidates page', async () => {
    await PuppetMaster.updateAngularLocationUrl('/hiring/browse-candidates?tab=all', 'candidate-list');
    await PuppetMaster.evaluate(() => {
        // First, apply no filters to make sure we load up some candidates
        const scope = $('hiring-browse-candidates').isolateScope();
        scope.filters.locations = [];
        scope.filters.primary_areas_of_interest = [];
        scope.filters.years_experience = [];
        scope.filters.only_local = false;
        scope.applyFilters();

        // set some filters but don't actually run them yet for a prettier screenshot...
        scope.filters.places = [
            {
                locality: {
                    short: 'Beijing',
                    long: 'Beijing',
                },
                administrative_area_level_1: {
                    short: 'Beijing',
                    long: 'Beijing',
                },
                country: {
                    short: 'CN',
                    long: 'China',
                },
                formatted_address: 'Beijing, China',
                utc_offset: 480,
                lat: 39.90419989999999,
                lng: 116.4073963,
            },
        ];
        scope.filters.only_local = true;
        scope.proxy.showAdvancedFilters = true;
        evalHelper.$rootScope.$apply();
    });
    await PuppetMaster.waitForSelector('.list-wrapper');
    await PuppetMaster.removeThingsFromHiringBrowseThatChangeBasedOnNumberOfResults();
    await PuppetMaster.assert({
        captureSelector: '.page-with-left-nav-and-main-box',
        mock: ['.app-menu-mobile', '.list-wrapper'],
    });
});

it('should render mobile filters', async () => {
    await PuppetMaster.updateAngularLocationUrl('/hiring/browse-candidates?tab=all', 'candidate-list');
    await PuppetMaster.evaluate(() => {
        // cleanup for future specs
        const scope = $('hiring-browse-candidates').isolateScope();
        scope.mobileState.expanded = true;
        evalHelper.$rootScope.$apply();
    });
    await PuppetMaster.resetFunctions.push(async () => {
        await PuppetMaster.evaluate(() => {
            // cleanup for future specs
            const scope = $('hiring-browse-candidates').isolateScope();
            scope.mobileState.expanded = true;
            evalHelper.$rootScope.$apply();
        });
    });
    await PuppetMaster.assert({
        viewportNames: ['iphone5'],
        captureSelector: '.responsive-nav',
        mock: ['.app-menu-mobile'],
    });
});

const stubNoCandidatesAndAssert = async tab => {
    await PuppetMaster.updateAngularLocationUrl(`/hiring/browse-candidates?tab=${tab}`);
    const originalHiringRelationshipViewModels = await PuppetMaster.evaluate(tab => {
        const scope = $('hiring-browse-candidates').isolateScope();
        const originalHiringRelationshipViewModels = scope.careersNetworkViewModel.hiringRelationshipViewModels;
        scope.careersNetworkViewModel.hiringRelationshipViewModels = _.reject(
            scope.careersNetworkViewModel.hiringRelationshipViewModels,
            scope.tabConfigs[tab].filter,
        );
        evalHelper.$rootScope.$apply();
        $('[name="apply-filters"]').click();
        return originalHiringRelationshipViewModels;
    }, tab);
    await PuppetMaster.resetFunctions.push(async () => {
        await PuppetMaster.evaluate(originalHiringRelationshipViewModels => {
            $(
                'hiring-browse-candidates',
            ).isolateScope().careersNetworkViewModel.hiringRelationshipViewModels = originalHiringRelationshipViewModels;
            evalHelper.$rootScope.$apply();
        }, originalHiringRelationshipViewModels);
    });
    await PuppetMaster.waitForSelector('.all-reviewed');
    await PuppetMaster.assert({
        captureSelector: '.page-with-left-nav-and-main-box',
        mock: ['.app-menu-mobile'],
    });
};
