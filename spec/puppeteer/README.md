## Screenshot Testing ##

[Puppeteer](https://pptr.dev/) is a Chromium automation library provided by Google.
[Jest](https://jestjs.io/docs/en/getting-started) is a unit testing framework by Facebook built on Jasmine and introduces image snapshotting
capabilities, courtesy of the [jest-image-snapshot](https://github.com/americanexpress/jest-image-snapshot) library.

We've written some scripts that spawn Jest commands to generate screenshots and diff against existing versions (baselines). This is also wired up
in our CI process. For consistent screenshotting, we must use the same environment as CI, so that we guarantee OS / architecture consistency in
rendering of things such as system fonts / rasterization of images / etc. For this reason, most of our entry points into Jest / Puppeteer use a
Docker container using the same image as our CI server.

### Developer Process ###

- _Optional:_ Run `npm run testserver` to spawn a Test environment server instance. The `puppet` commands will do this for you if one is not already
started, but this will speed up subsequent `puppet` invocations.
- Ensure `npm run build-dev` has been run so that frontend assets are built.
- Use `puppet` to run all suites for failures (see also `.back_royal.bash`)
    - Optionally use `fit` and `fdescribe` for individual tests
- If there are expected failures run `puppet update` to regenerate the screenshots


### Puppet Aliases ###

#### Headless / Containerized Context

`puppet` to run specs against existing baselines
`puppet update` to update baselines
`puppet watch` to watch for changes and re-run specs as applicable
`puppet kill` kill the containerized Jest runner if Docker container becomes unresponsive

#### Headful / Slowed / Localized Context

`puppet debug <OPTIONAL PATTERN>` to run specs against existing baselines
`puppet debug-watch <OPTIONAL PATTERN>` to watch for changes and re-run specs as applicable

### Test creation tips ###
- Make sure that you do not put cleanup code after the `assert`. If a test fails due to screenshot mismatch then the remainder of the test will not run, which can cause other tests to fail (e.g., if you are trying to `signOut`). Make use of the `before` and `after` functions if you need to do cleanup.
- Be sure to make use of the `resetFunctions` paradigm when mocking state between tests that use the same user. If your tests `signOut` and `auth` again then the state will naturally be reset.

### Debugging tips ###
- Remember that you can use all of the jasmine labels like `fit`, `fdescribe`, `xit`, `xdescribe`
   - `fit` and `fdescribe` only work within a file, so if you're using them, you probably need to use `puppet debug` and pass a file name
- When you run `puppet debug`, it uses the local chromium version, so image comparisons will likely erroneously fail
- If you want to run a single file inside of docker, use `puppet pattern PATTERN`
- Use `puppet debug` with a `debugger` inside of an `evaluate` to debug inside of the headful browser / app context
    - Use `puppet debug <OPTIONAL PATTERN>` to run only certain suites
    - See the `PuppetMaster.debugger()` convenience method for easily placing web debug points
    - Remember that `debugger` statements in the app code itself will be picked up!
- `evalHelper` contains utility methods that need to be evaluated against the application (not Puppeteer context) and contains things like `inj` to get an AngularJS `$injector` reference.
- Note that tests within a suite are not run in random order. I wanted to record this piece of information since I had to do some digging to find the answer. At some point in Jasmine 2.x they added an option to run tests within a suite in random order; that will actually be the default in jasmine 3. But Jest uses Jasmine 2.x, and I found [this GitHub issue](https://github.com/facebook/jest/issues/4386) which confirms that tests are not run in random order.

### Issues we have come across and how we solved them ###
- Navigating is not working correctly and / or is causing digest errors in the Chrome console.
    - We had some issues where navigating would sometimes not work and digest errors might be thrown in the console. We realized that we needed to provide an initial `waitForSelector` after authenticating when initializing the suite since route resolvers may not be finished by the time the test tries to do things like mock out properties or navigate somewhere else. Remember that when we `auth` for a suite we are only waiting for the `app-shell` to appear.
- Trying to capture a directive element, like `careers-welcome` in the `/careers/welcome` test, actually captures the entire screen. Saw similar behavior with `edit-hiring-manager-profile`.
    - This is not preferable, but I wrapped the `careers-welcome` directive in a `div` with a class that I could reference. It would be better to see what is different and / or if we did something when running the same test in Casper.
- Trying to capture a specific element causes scrolling if it is taller than the viewport height.
    - See [this note](https://pptr.dev/#?product=Puppeteer&version=v1.6.0&show=api-elementhandlescreenshotoptions) in the docs.
    - Update: This should not be a problem anymore now that we are dynamically calculating the viewport height.
- Animations and transitions can cause lots of false positives due to not being complete when the screenshot is taken. We do several things to remedy this, see `casper_mode.js`.
- Forgetting to `await` an async function call will produce weird issues since you will immediately run code that was intended to execute after an async operation.
- Make sure that cleanup is done in the `before` and `after` functions. I've had several issues where a failing test due to a screenshot diff caused other failures because of cleanup code after the `assert`, which does not run if the test fails. The `before` and `after` functions will always run even if the screenshot diff fails.
- We had issues with hover styles being different between runs, presumably because the mouse would end up in different places. We added code to move the mouse to position `0,0` before asserting so you shouldn't see any issues with this going forward; I wanted to mention it here nonetheless.
- We had issues with elements that had a background-image set to a url where the image would render slightly differently and cause a failure. We fixed this by either removing the background-image style from mocked elements in casper.scss or hiding the elements completely.
- We had issues with assets that are loaded from third parties, such as the LinkedIn share button, intermittently failing. I actually put a conditional to check for casperMode in the code itself, which is typically a last resort, because we really shouldn't be hitting third parties in automated tests.

### TODO ###

- Investigate the intermittent image rendering failures that can occur with elements that have a background-image set to a url, and undo any mocking or hiding done specifically for this.
    - See `.lesson-progress-icon, .hexagon` in `stream_dashboard_screenshot_spec.js`
    - See `.mba-icon, .certificate, .certificate-generic, and .status.graduated h2:after` in `student_dashboard_screenshot_spec.js`
    - See `careers-avatar` in `candidate_closed_connections_screenshot_spec.js`
    - See `careers-avatar` in `hiring_connections_screenshot_spec.js`
    - See `careers-avatar` in `candidate_connections_screenshot_spec.js`
    - See `.program-icon .icon` in `settings_screenshot_spec.js`
- Investigate the intermittent failures on some `img` elements that use `image-fade-in-on-load` and / or `ng-src`
- See if we can make some tweaks such that a failure in the desktop viewport doesn't stop the mobile viewport assertion from running
    - See [this desktop failure](https://99681-83a6a3bb-cf4b-42c6-90c5-be3d933003fd-bb.circle-artifacts.com/2/screenshot_failures/student-dashboard-screenshot-spec-js-applied-mba-user-should-render-dashboard-1-diff.png) that masked [this mobile failure](https://99648-83a6a3bb-cf4b-42c6-90c5-be3d933003fd-bb.circle-artifacts.com/2/screenshot_failures/student-dashboard-screenshot-spec-js-applied-mba-user-should-render-dashboard-2-diff.png) that was the same test
- Rename remaining casper variables and classes
    - See FIXME in puppet_master.js#init
    - See casper_mode.js
    - See casper.scss
- Consider setting the casperMode client storage variable in puppet_master.js itself rather than relying on the request params. We should probably keep the logic that sets it via request params just for debugging purposes.
- Utilize the same `application.yml` that CI uses for consistency. Might require having `puppet` commands do some file copying / reverting.
- Update fixtures to generate work / education experience (plus any other date value that could be used to determine an interval) and ensure that it is based on a consistent offset (-10 days, -2 years, etc). This will prevent failures around calculated intervals. Alternatively, we mock them all out in client-side code. Whichever is more sustainable, etc.
- Add support for a top-level `__diff_output__` directory. This is currently not configurable in `jest-image-snapshot` and requires all sorts of paths being defined in `.circle/config.yml`
    - See [this GitHub issue](https://github.com/americanexpress/jest-image-snapshot/issues/97) that I made
- Clear out __diff_output__ on every run?
    - Or possibly add a puppet clean command?
- Figure out why passing a directive as the selector takes a full-page screenshot
- Add ability to pass an absolute filename to the puppet command so we can easily copy and paste a filename
- Monitor status of the [--watch performance fix PR](https://github.com/facebook/jest/pull/6732)
- Figure out why `console.log` output is sometimes being swallowed
    - It is probably due to the ASCII codes Jest is writing to the terminal
- Try to change upstream jest-image-snapshot to allow for a file suffix. It would be nice to keep the built-in naming logic (which uses the describe and it labels to create the name), but be able to suffix with the viewport alias. Currently you can only replace the naming logic with your own name.
- See if we can figure out how to keep browser from closing in debug mode
