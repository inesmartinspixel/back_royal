const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite({
    email: 'admin@pedago.com',
});

async function clickIndicatorAndAssert(options = {}) {
    options.nthChild = options.nthChild || 1;
    options.waitForSelector = options.waitForSelector || '[ng-controller]';

    await PuppetMaster.click(`.indicator:nth-child(${options.nthChild})`);
    await PuppetMaster.page.waitForSelector(options.waitForSelector, {
        visible: true,
    });
    await PuppetMaster.assert({
        mouseY: 200,
        mock: options.mock,
    });
}

beforeAll(async () => {
    await PuppetMaster.waitForSelector('[ng-controller]');
});

xit('multiple_choice_multiple_card frame 1 slide 1', async () => {
    await PuppetMaster.updateAngularLocationUrl(
        '/course/4ac7aa46-8353-43bf-94c8-fe30002bc579/chapter/0/lesson/4ac7aa46-8353-43bf-94c8-fe30002bc571/show',
        '.indicator:nth-child(1)',
    );
    await clickIndicatorAndAssert({
        nthChild: 1,
        waitForSelector: '[component-id="0c97525d-064c-4629-9427-e906ca08ad5b"]',
    });
});

// FIXME: So weird, this one behaves oddly in regular mode, but works perfectly when debugging
xit('multiple_choice_multiple_card frame 1 slide 2', async () => {
    await PuppetMaster.updateAngularLocationUrl(
        '/course/4ac7aa46-8353-43bf-94c8-fe30002bc579/chapter/0/lesson/4ac7aa46-8353-43bf-94c8-fe30002bc571/show',
        '.indicator:nth-child(1)',
    );
    await PuppetMaster.click('.indicator:nth-child(1)');
    await PuppetMaster.click('.forward');
    await PuppetMaster.page.waitForSelector('[component-id="c4999698-a8c6-4ef7-ac2f-921d95e2e2cc"] img', {
        visible: true,
    });
    await PuppetMaster.assert({
        mouseY: 200,
    });
});

xit('this_or_that frame 1 slide 1', async () => {
    await PuppetMaster.updateAngularLocationUrl(
        '/course/4ac7aa46-8353-43bf-94c8-fe30002bc579/chapter/0/lesson/4ac7aa46-8353-43bf-94c8-fe30002bc571/show',
        '.indicator:nth-child(2)',
    );
    await clickIndicatorAndAssert({
        nthChild: 2,
        waitForSelector: '[component-id="4c3faecd-7a5f-410b-9108-80d8aa345d54"]',
    });
});

xit('fill_in_the_blanks frame 1', async () => {
    await PuppetMaster.updateAngularLocationUrl(
        '/course/4ac7aa46-8353-43bf-94c8-fe30002bc579/chapter/0/lesson/4ac7aa46-8353-43bf-94c8-fe30002bc571/show',
        '.indicator:nth-child(3)',
    );
    await clickIndicatorAndAssert({
        nthChild: 3,
        waitForSelector: '.cf-challenge-blank',
    });
});

// FIXME: This one works in isolation, but has some spacing issues when run with the full suite
xit('matching frame 1', async () => {
    await PuppetMaster.updateAngularLocationUrl(
        '/course/4ac7aa46-8353-43bf-94c8-fe30002bc579/chapter/0/lesson/4ac7aa46-8353-43bf-94c8-fe30002bc571/show',
        '.indicator:nth-child(4)',
    );
    await clickIndicatorAndAssert({
        nthChild: 4,
        waitForSelector: '[component-id="11071592-b7ef-4054-dc0e-b5152b68989d"]',
    });
});

xit('mathjax frame 1', async () => {
    await PuppetMaster.updateAngularLocationUrl(
        '/course/4ac7aa46-8353-43bf-94c8-fe30002bc579/chapter/0/lesson/4ac7aa46-8353-43bf-94c8-fe30002bc571/show',
        '.indicator:nth-child(5)',
    );
    await clickIndicatorAndAssert({
        nthChild: 5,
        waitForSelector: '[component-id="a7841380-f00d-41d2-9542-a7e8105ce7bb"] .mjx-chtml',
    });
});

xit('compose_blanks_on_image frame 1 slide 1', async () => {
    await PuppetMaster.updateAngularLocationUrl(
        '/course/4ac7aa46-8353-43bf-94c8-fe30002bc579/chapter/0/lesson/4ac7aa46-8353-43bf-94c8-fe30002bc571/show',
        '.indicator:nth-child(6)',
    );

    await PuppetMaster.click('.indicator:nth-child(6)');
    await PuppetMaster.page.waitForSelector('[component-id="cf85c09f-b375-446e-96c7-1a8d9d386733"] textarea'); // the first cf-challenge-blank's textarea
    await PuppetMaster.page.waitFor(2000); // hackily give time for the scaling to finish (see component_overlay_dir.js)
    await PuppetMaster.page.type('[component-id="cf85c09f-b375-446e-96c7-1a8d9d386733"] textarea', '30');
    await PuppetMaster.assert({
        mouseY: 200,
        viewportNames: ['desktop'],
    });

    await PuppetMaster.setViewport('iphone5');
    await PuppetMaster.page.waitFor(2000); // hackily give time for the scaling to finish (see component_overlay_dir.js)
    await PuppetMaster.assert({
        mouseY: 200,
        viewportNames: ['iphone5'],
    });
});
