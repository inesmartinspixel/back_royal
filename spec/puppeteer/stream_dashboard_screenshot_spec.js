const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite({
    email: 'mba_user_stream_dashboard@pedago.com',
});

it('should render in progress course', async () => {
    await PuppetMaster.goto('/course/title/b835df0a-c9b9-43b8-8e60-41d93323ccb0', '.hexagons-box');

    await PuppetMaster.assert({
        captureSelector: '#sp-page',
        mock: ['.app-menu-mobile', '.app-header'],
    });
});

it('should render unstarted course', async () => {
    await PuppetMaster.goto('/course/title/d9a3385d-2230-4411-b50d-ca8a1e28965f', '.hexagons-box');

    await PuppetMaster.assert({
        captureSelector: '#sp-page',
        mock: ['.app-menu-mobile', '.app-header'],
    });
});

it('should render unstarted exam', async () => {
    await PuppetMaster.goto('/course/title/456ec52c-dabd-43a9-874f-3d61816b43b8', '.hexagons-box');

    await PuppetMaster.assert({
        captureSelector: '#sp-page',
        mock: ['.app-menu-mobile', '.app-header'],
    });
});

it('should render started exam', async () => {
    await PuppetMaster.goto('/course/title/45493f54-97d5-4508-945e-9d8e4b15453c', '.hexagons-box');

    // started exam
    await PuppetMaster.evaluate(() => {
        const scope = $('stream-dashboard').isolateScope();
        const streamProgress = scope.stream.lesson_streams_progress;

        // FIXME: Trying to make sense of intermittent failures here
        if (!streamProgress) {
            const promise = scope.currentUser.progress._streamProgressPromise;
            const state = promise && promise.$$state;
            const progress = state && state.value;
            console.error(
                'No progress record found for exam stream',
                JSON.stringify({
                    locale_pack_id: scope.stream.locale_pack_id,
                    exam_title: scope.stream.title,
                    streamProgress:
                        progress &&
                        progress.streamProgress.map(p => ({
                            locale_pack_id: p.locale_pack_id,
                            completed_at: p.completed_at,
                        })),
                }),
            );
        }

        streamProgress.time_runs_out_at = 123456; // this value doesn't matter, since we're setting msLeftInTimeLimit, but it has to be defined
        streamProgress.last_progress_at = 123456;
        Object.defineProperty(scope.stream.lesson_streams_progress, 'msLeftInTimeLimit', {
            value: 1000 * 60 * 94.4,
            configurable: true,
        });
        const lessonProgress = scope.stream.orderedLessons[0].lesson_progress;
        lessonProgress.complete = true;
        lessonProgress.last_progress_at = 123456;
        lessonProgress.best_score = 0.42;
    });

    // have to reload, scope.$apply is not enough. So go to another page and come back
    await PuppetMaster.updateAngularLocationUrl('/course/title/d9a3385d-2230-4411-b50d-ca8a1e28965f', '.hexagons-box');
    await PuppetMaster.updateAngularLocationUrl('/course/title/45493f54-97d5-4508-945e-9d8e4b15453c', '.hexagons-box');

    await PuppetMaster.assert({
        captureSelector: '#sp-page',
        mock: ['.app-menu-mobile', '.app-header'],
    });
});

it('should render completed exam', async () => {
    await PuppetMaster.goto('/course/title/45493f54-97d5-4508-945e-9d8e4b15453c', '.hexagons-box');

    // completed exam
    await PuppetMaster.evaluate(() => {
        const scope = $('stream-dashboard').isolateScope();
        const streamProgress = scope.stream.lesson_streams_progress;
        streamProgress.complete = true;
        streamProgress.official_test_score = 0.42;
        [0, 1].forEach(i => {
            const lessonProgress = scope.stream.orderedLessons[i].lesson_progress;
            lessonProgress.complete = true;
            lessonProgress.last_progress_at = 123456;
            lessonProgress.best_score = 0.42;
        });
    });

    // have to reload, scope.$apply is not enough. So go to another page and come back
    await PuppetMaster.updateAngularLocationUrl('/course/title/d9a3385d-2230-4411-b50d-ca8a1e28965f', '.hexagons-box');
    await PuppetMaster.updateAngularLocationUrl(
        '/course/title/45493f54-97d5-4508-945e-9d8e4b15453c',
        '.header-box.top',
    );

    // expand the chapter if it isn't
    await PuppetMaster.evaluate(() => {
        if ($('.content-box.lesson-info').hasClass('ng-hide')) {
            $('.header-box.top').click();
        }
    });
    await PuppetMaster.waitFor(() => {
        return !$('.content-box.lesson-info').hasClass('ng-hide');
    });

    await PuppetMaster.assert({
        captureSelector: '#sp-page',
        mock: ['.app-menu-mobile', '.app-header'],
    });
});

// FIXME: I had to move this test to the bottom because for some reason it will cause subsequent
// tests to fail by causing them to have no "RESOURCES" nor "CREDITS" widgets. I can't figure out
// why. I've tested manually in an 8800 browser. I've watched headful mode run the tests and the widgets
// are present. I need to come back later and figure this out, but for now I just moved this spec to the bottom.
it('should render completed course', async () => {
    await PuppetMaster.goto('/course/title/dc6d936c-5052-4d89-b474-576ced0e41e6', '.header-box.top');

    // expand the chapter
    await PuppetMaster.click('.header-box.top');

    // Saw intermittent failures where this was not loaded
    await PuppetMaster.waitForSelector('.certificate-container img');

    // FIXME: Investigate the background-image url rendering issue with .lesson-progress-icon, '.hexagon'
    await PuppetMaster.assert({
        captureSelector: '#sp-page',
        mock: ['.app-menu-mobile', '.app-header', '.lesson-progress-icon'],
    });
});
