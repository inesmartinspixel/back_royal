const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite();

it('should work', async () => {
    await PuppetMaster.db.none('truncate events');

    // There are some events that we do not persist to the DB and
    // send straight to Redshift. This is a good way to ensure we
    // do hit the event_bundles endpoint with them.
    let interceptedEvents = [];
    await PuppetMaster.page.setRequestInterception(true);
    PuppetMaster.page.on('request', interceptedRequest => {
        if (/api\/event_bundles\.json/.test(interceptedRequest.url())) {
            // Just hoover up all events sent to the endpoint, we filter by type below.
            const events = JSON.parse(interceptedRequest.postData()).record.events || [];
            interceptedEvents = interceptedEvents.concat(events);
        }
        interceptedRequest.continue();
    });

    await PuppetMaster.goto(
        '/start?utm_source=source&&utm_content=content&&utm_campaign=campaign&&utm_medium=medium',
        8000,
    );

    await PuppetMaster.waitFor(() => {
        return !!window.preSignupForm;
    });

    await PuppetMaster.assert({
        captureSelector: 'body',
    });

    await PuppetMaster.page.select('[data-step="1"] [name="primary_reason_for_applying"]', 'start_own_company');
    await PuppetMaster.page.click('[data-step="1"] .emba-form__button--next');

    await PuppetMaster.page.select(
        '[data-step="2"] [name="survey_highest_level_completed_education_description"]',
        'high_school',
    );
    await PuppetMaster.page.click('[data-step="2"] .emba-form__button--next');

    await PuppetMaster.page.select('[data-step="3"] [name="survey_years_full_time_experience"]', '0_2');
    await PuppetMaster.page.select('[data-step="3"] [name="survey_most_recent_role_description"]', 'management_role');
    await PuppetMaster.page.click('[data-step="3"] .emba-form__button--next');

    // I had a screenshot for the sign-in form, but it kept failing on
    // ci and eventually I just gave up on it.  Now just asserting that
    // the form is on the screen (by checking for a social button)
    PuppetMaster.waitForSelector('.onboarding__social-auth-button');

    // We scroll twice, but only expect one event below
    PuppetMaster.page.evaluate(() => {
        window.scrollBy(0, 42);
    });
    PuppetMaster.page.evaluate(() => {
        window.scrollBy(0, 21);
    });

    // Wait 5 seconds
    await PuppetMaster.page.waitFor(5000);

    // Assert we intercepted at least one `still_on_page` event
    expect(eventsForType(interceptedEvents, 'still_on_page').length).toBeGreaterThanOrEqual(1);

    const eventTypes = interceptedEvents.map(e => e.event_type);

    // These things are specific to this page
    const clickedNextStepEvents = eventsForType(interceptedEvents, 'dynamic_landing_page:clicked_next_step');
    const currentStepIndexes = clickedNextStepEvents.map(e => e.current_step_index);
    expect(currentStepIndexes).toEqual([1, 2, 3]);

    // These things should work on all pages.  They are defined in
    // logPageTimes.  Maybe they should be tested somewhere more general,
    // but leaving it here for now
    expect(eventTypes.includes('loading_page')).toBe(true);
    expect(eventTypes.includes('dom_content_loaded')).toBe(true);
    expect(eventTypes.includes('dynamic_landing_page:preSignupForm_initialized')).toBe(true);

    // We scrolled twice, but only expect one event
    expect(eventTypes.includes('scrolled_first_time')).toBe(true);
    expect(eventsForType(interceptedEvents, 'scrolled_first_time').length).toBe(1);

    const utmParamEvent = eventsForType(interceptedEvents, 'user:set_utm_params')[0];
    expect(utmParamEvent.utm_source).toEqual('source');
    expect(utmParamEvent.utm_content).toEqual('content');
    expect(utmParamEvent.utm_campaign).toEqual('campaign');
    expect(utmParamEvent.utm_medium).toEqual('medium');
});

function eventsForType(events, eventType) {
    return events.filter(e => e.event_type === eventType);
}
