const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite();

describe('applied mba user', () => {
    beforeAll(async () => {
        await PuppetMaster.auth('applied_mba_user@pedago.com');
    });

    afterAll(async () => {
        await PuppetMaster.signOut();
    });

    it('should render dashboard', async () => {
        await PuppetMaster.updateAngularLocationUrl('/dashboard', '.main-column');

        // FIXME: Investigate the background-image url rendering issue with .mba-icon
        await PuppetMaster.assert({
            captureSelector: '#sp-page',
            mock: ['.app-menu-mobile', '.app-header', '.mba-icon', '.certificate-generic'],
        });
    });
});

describe('external institution', () => {
    describe('user with playlists', () => {
        beforeAll(async () => {
            await PuppetMaster.auth('external_inst_user_with_playlists@pedago.com');
        });

        afterAll(async () => {
            await PuppetMaster.signOut();
        });

        // FIXME: Investigate the background-image url rendering issue with .certificate-generic
        it('should render dashboard', async () => {
            await PuppetMaster.updateAngularLocationUrl('/dashboard', '.main-column');
            await PuppetMaster.assert({
                captureSelector: '#sp-page',
                mock: ['.app-menu-mobile', '.app-header', '.certificate-generic'],
            });
        });
    });

    describe('user with no playlists', () => {
        beforeAll(async () => {
            await PuppetMaster.auth('external_inst_user_without_playlists@pedago.com');
        });

        afterAll(async () => {
            await PuppetMaster.signOut();
        });

        // FIXME: Investigate the background-image url rendering issue with .certificate-generic
        it('should render dashboard as an external-institution user with no playlists', async () => {
            await PuppetMaster.updateAngularLocationUrl('/dashboard', '.main-column');
            await PuppetMaster.assert({
                captureSelector: '#sp-page',
                mock: ['.app-menu-mobile', '.app-header', '.certificate-generic'],
            });
        });
    });
});

describe('pending user invited to interview', () => {
    beforeAll(async () => {
        await PuppetMaster.auth('mba_user@pedago.com');
    });

    afterAll(async () => {
        await PuppetMaster.signOut();
    });

    // FIXME: Investigate the background-image url rendering issue with .certificate-generic
    it('should render dashboard as a pending user that has been invited to interview', async () => {
        await PuppetMaster.evaluate(() => {
            const scope = $('student-dashboard').isolateScope();

            Object.defineProperty(scope, 'showScheduleInterview', {
                value: true,
                configurable: true,
            });

            Object.defineProperty(scope, 'showEnrollment', {
                value: false,
                configurable: true,
            });

            Object.defineProperty(scope, 'showSchedule', {
                value: false,
                configurable: true,
            });
        });

        await PuppetMaster.updateAngularLocationUrl('/dashboard', '.main-column');
        await PuppetMaster.assert({
            captureSelector: '#sp-page',
            mock: ['.app-menu-mobile', '.app-header', '.certificate'],
        });
    });
});

describe('accepted mba user', () => {
    beforeAll(async () => {
        await PuppetMaster.auth('mba_user@pedago.com');
    });

    afterAll(async () => {
        await PuppetMaster.signOut();
    });

    async function setupUserAndAssert(state) {
        await PuppetMaster.mockUserDefinedProperty(
            'unverifiedForCurrentIdVerificationPeriod',
            state.unverifiedForCurrentIdVerificationPeriod,
        );
        await PuppetMaster.evaluate(state => {
            const scope = $('student-dashboard-schedule').isolateScope();

            Object.defineProperty(scope, 'status', {
                value: state.key,
                configurable: true,
            });

            Object.defineProperty($('student-dashboard').isolateScope(), 'showEnrollment', {
                value: state.showEnrollment,
                configurable: true,
            });

            // Mock out different states of enrollment sidebar todos
            if (state.showEnrollment) {
                const currentUser = $('student-dashboard').isolateScope().currentUser;

                // Identity todo complete
                Object.defineProperty(currentUser, 'identity_verified', {
                    value: true,
                    configurable: true,
                });

                Object.defineProperty(currentUser.career_profile.education_experiences[0], 'transcriptInReview', {
                    value: true,
                    configurable: true,
                });

                Object.defineProperty(
                    currentUser.career_profile.education_experiences[1],
                    'transcriptApprovedOrWaived',
                    {
                        value: true,
                        configurable: true,
                    },
                );
            }

            Object.defineProperty(scope.relevantCohort, 'graduation_days_offset_from_end', {
                value: 200,
            });

            if (state.showSchedule) {
                scope.relevantCohort.current_period_index = state.current_period_index || 0;
            }

            if (state.periods) {
                scope.relevantCohort.periods = state.periods;
                Object.defineProperty(scope.relevantCohort.periods[0], 'canHaveSpecializationStyle', {
                    value: true,
                });
                Object.defineProperty(scope.relevantCohort.periods[1], 'canHaveSpecializationStyle', {
                    value: true,
                });
                Object.defineProperty(scope.relevantCohort.periods[2], 'canHaveSpecializationStyle', {
                    value: true,
                });
                Object.defineProperty(scope.relevantCohort, 'currentPeriod', {
                    value: state.periods[1],
                });
                Object.defineProperty(scope.relevantCohort.currentPeriod, 'periodTitle', {
                    value: state.periods[1].title,
                });
            }

            scope.showSchedule = state.showSchedule;
            scope.showDiplomaCheck = state.showDiplomaCheck;
        }, state);

        await PuppetMaster.rootScopeApply();

        await PuppetMaster.waitForSelector('student-dashboard .main-column');

        // FIXME: Investigate the background-image url rendering issue with .mba-icon, .certificate, and .status.graduated h2::after
        await PuppetMaster.evaluate(() => {
            const elementGraduated = document.querySelector('.status.graduated h2');
            if (elementGraduated) {
                elementGraduated.className += ' puppet-mock-pseudo';
            }

            const elementFailed = document.querySelector('.status.failed h2');
            if (elementFailed) {
                elementFailed.className += ' puppet-mock-pseudo';
            }

            const elementWeekOne = document.querySelector('.status.week_1 h2');
            if (elementWeekOne) {
                elementWeekOne.className += ' puppet-mock-pseudo';
            }
        });

        await PuppetMaster.assert({
            captureSelector: '#sp-page',
            mock: ['.app-menu-mobile', '.app-header', '.mba-icon', '.certificate', '.diploma-frames'],
        });
    }

    it('should render dashboard with week_0 status', async () => {
        await setupUserAndAssert({
            key: 'week_0',
            showDiplomaCheck: false,
            showSchedule: false,
            showEnrollment: true,
            unverifiedForCurrentIdVerificationPeriod: false,
        });
    });

    it('should render dashboard with not_on_track status', async () => {
        await setupUserAndAssert({
            key: 'not_on_track',
            showDiplomaCheck: false,
            showSchedule: false,
            showEnrollment: false,
            unverifiedForCurrentIdVerificationPeriod: true,
        });
    });

    it('should render dashboard with almost_there status', async () => {
        await setupUserAndAssert({
            key: 'almost_there',
            showDiplomaCheck: false,
            showSchedule: false,
            showEnrollment: false,
            unverifiedForCurrentIdVerificationPeriod: false,
        });
    });

    it('should render dashboard with week_1 status', async () => {
        await setupUserAndAssert({
            key: 'week_1',
            showDiplomaCheck: false,
            showSchedule: true,
            showEnrollment: false,
            unverifiedForCurrentIdVerificationPeriod: false,
        });
    });

    it('should render dashboard with on_track status', async () => {
        await setupUserAndAssert({
            key: 'on_track',
            showDiplomaCheck: false,
            showSchedule: true,
            showEnrollment: false,
            unverifiedForCurrentIdVerificationPeriod: false,
        });
    });

    it('should render dashboard when there are projects', async () => {
        await PuppetMaster.evaluate(() => {
            window.$originalCurrentPeriodIndex = evalHelper.currentUser.relevant_cohort.current_period_index;
            evalHelper.currentUser.relevant_cohort.current_period_index = 1;
            evalHelper.inj('$route').reload();
        });
        await PuppetMaster.updateAngularLocationUrl('/settings');
        await PuppetMaster.updateAngularLocationUrl('/dashboard', '.main-column');
        await setupUserAndAssert({
            key: 'on_track',
            showDiplomaCheck: false,
            showSchedule: true,
            showEnrollment: false,
            unverifiedForCurrentIdVerificationPeriod: false,
            current_period_index: 1,
        });
        await PuppetMaster.evaluate(() => {
            evalHelper.currentUser.relevant_cohort.current_period_index = window.$originalCurrentPeriodIndex;
            evalHelper.inj('$route').reload();
        });
        await PuppetMaster.updateAngularLocationUrl('/settings');
        await PuppetMaster.updateAngularLocationUrl('/dashboard', '.main-column');
    });

    it('should render dashboard with on_track_finished status', async () => {
        await setupUserAndAssert({
            key: 'on_track_finished',
            showDiplomaCheck: false,
            showSchedule: true,
            showEnrollment: false,
            unverifiedForCurrentIdVerificationPeriod: false,
        });
    });

    it('should render dashboard with finished status', async () => {
        await setupUserAndAssert({
            key: 'finished',
            showDiplomaCheck: true,
            showSchedule: false,
            showEnrollment: false,
            unverifiedForCurrentIdVerificationPeriod: false,
        });
    });

    it('should render dashboard with graduated status', async () => {
        await setupUserAndAssert({
            key: 'graduated',
            showDiplomaCheck: true,
            showSchedule: false,
            showEnrollment: false,
            unverifiedForCurrentIdVerificationPeriod: false,
        });
    });

    it('should render dashboard with honors status', async () => {
        await setupUserAndAssert({
            key: 'honors',
            showDiplomaCheck: false,
            showSchedule: false,
            showEnrollment: false,
            unverifiedForCurrentIdVerificationPeriod: false,
        });
    });

    it('should render dashboard with failed status', async () => {
        await setupUserAndAssert({
            key: 'failed',
            showDiplomaCheck: false,
            showSchedule: false,
            showEnrollment: false,
            unverifiedForCurrentIdVerificationPeriod: false,
        });
    });

    it('should render dashboard with specialization period', async () => {
        const periods = [
            {
                startDate: new Date('January 1, 2019'),
                endDate: new Date('January 7, 2019'),
                style: 'specialization',
                specialization_style: 'specialization_1',
                title: 'Week 20: Foo',
            },
            {
                startDate: new Date('January 8, 2019'),
                endDate: new Date('January 14, 2019'),
                style: 'specialization',
                specialization_style: 'specialization_1',
                title: 'Week 21: Foo',
            },
            {
                startDate: new Date('January 15, 2019'),
                endDate: new Date('January 21, 2019'),
                style: 'project',
                specialization_style: 'specialization_1',
                title: 'Project Foo Capstone',
            },
        ];
        await setupUserAndAssert({
            key: 'on_track',
            showDiplomaCheck: false,
            showSchedule: true,
            showEnrollment: false,
            unverifiedForCurrentIdVerificationPeriod: false,
            periods: periods,
        });
    });

    it('should show library services modal', async () => {
        await PuppetMaster.updateAngularLocationUrl('/dashboard', '.main-column');
        // NOTE: Chromium starts in screen-sm/mobile then changes in the assert
        await PuppetMaster.click('[name="research-resources-mobile"] button');
        await PuppetMaster.assert({
            captureSelector: '.modal-content',
            mock: ['.app-menu-mobile', '.app-header'],
        });
    });
});
