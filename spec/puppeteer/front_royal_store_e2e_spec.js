/* eslint-disable no-await-in-loop */
const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite();

/*
    The way we make sure external progress changes get pushed down to clients
    is a complex dance between the server and a bunch of different things
    on the client, so I decided to add an integration spec here to give us
    some extra security around it.
*/
describe('front_royal_store', () => {
    const userId = 'ad497487-da26-45e5-b4ab-5066c647af15';

    beforeAll(async () => {
        // Do this before in case the last spec was canceled
        await clearProgress(userId);

        const email = 'front_royal_store_user@pedago.com';
        await PuppetMaster.auth(email);
        await PuppetMaster.waitForSelector('student-dashboard .main-column');
    });

    afterAll(async () => {
        await PuppetMaster.signOut();
    });

    afterEach(async () => {
        await clearProgress(userId);

        // Reload the page.  This works to clear out storage because we're using fake-indexeddb here
        await PuppetMaster.page.reload({ waitUntil: ['networkidle0', 'domcontentloaded'] });
    });

    it('should fetch updates when a ping detects changes on the server', async () => {
        await PuppetMaster.waitForSelector('.playlist-course-list .row:nth-child(1) .course-box.not_started');
        await insertStreamProgressForFirstStream(userId);
        await triggerPing();
        await waitForClassOnFirstStreamLinkBox('in_progress');
    });

    it('should fetch updates when a progress save detects changes on the server', async () => {
        await PuppetMaster.waitForSelector('.playlist-course-list .row:nth-child(1) .course-box.not_started');
        await insertStreamProgressForFirstStream(userId);

        // When we launch the lesson player, a lesson progress save will go out.
        // The interceptor should detect that before that save, the server had an
        // updated_at that was later than ours, and trigger the reload.
        await startSecondStreamAndReturnToDashboard();
        await waitForClassOnFirstStreamLinkBox('in_progress');
    });
});

async function clearProgress(userId) {
    await PuppetMaster.db.none('delete from lesson_streams_progress where user_id = $1', [userId]);
    await PuppetMaster.db.none('delete from lesson_progress where user_id = $1', [userId]);
}

async function startSecondStreamAndReturnToDashboard() {
    // launch stream dashboard for second stream
    await PuppetMaster.click('.playlist-course-list .row:nth-child(2) a');
    // click the start button
    await PuppetMaster.waitForAndClick('.lesson-box a');
    // exit player
    await PuppetMaster.waitForAndClick('a.home-link');
    // back to dashboard
    await PuppetMaster.waitForAndClick('[name=home] a.app-header-pill');
}

async function insertStreamProgressForFirstStream(userId) {
    const localePackId = await PuppetMaster.evaluate(() => $('stream-link-box').isolateScope().stream.localePackId);

    return PuppetMaster.db.none(
        `
            insert into "lesson_streams_progress" (
                "created_at",
                "updated_at",
                "started_at",
                "user_id",
                "locale_pack_id"
            ) values (
                $1, $2, $3, $4, $5)
            `,
        [new Date(), new Date(), new Date(), userId, localePackId]
    );
}

async function triggerPing() {
    await PuppetMaster.page.keyboard.down('Control');
    await PuppetMaster.page.keyboard.down('KeyG');
}

async function waitForClassOnFirstStreamLinkBox(expectedClassName) {
    // After the ping, it will take some time before the request that refetches
    // progress to complete.  We will only see the results on the dashboard if
    // we enter the dashboard AFTER that request is complete.  So we go back
    // and forth between /careers and /dashboard until the dashboard is updated
    for (let i = 0; i < 6; i += 1) {
        await PuppetMaster.click('[name=careers] a.app-header-pill');
        await PuppetMaster.waitForSelector('careers');
        await PuppetMaster.click('[name=home] a.app-header-pill');

        const courseBox = await PuppetMaster.waitForSelector('.playlist-course-list .row:nth-child(1) .course-box');
        const properties = await courseBox.getProperty('className');
        const className = await properties.jsonValue();

        if (className.match(expectedClassName)) {
            break;
        }
    }

    // We've either tried a few times and given up, or we've seen the expected
    // class on the dashboard. Assert to find out which it is.

    await PuppetMaster.waitForSelector(`.playlist-course-list .row:nth-child(1) .course-box.${expectedClassName}`, {
        timeout: 0
    });
}
