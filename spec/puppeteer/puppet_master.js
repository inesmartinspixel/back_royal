/* eslint-disable no-shadow */
const puppeteer = require('puppeteer');
const ip = require('ip');
const path = require('path');
const fs = require('fs');
const pgp = require('pg-promise')({});

// We are temporarily using changes in an active PR that extremely improve performance.
// See https://github.com/facebook/jest/issues/5163
// See https://github.com/americanexpress/jest-image-snapshot/pull/89
const { toMatchImageSnapshot } = require('jest-image-snapshot');

expect.extend({
    toMatchImageSnapshot,
});

const viewportWidths = {
    iphone5: 320,
    desktop: 1200,
};

const rootHost = process.env.ROOT_HOST || ip.address();
const dbHost = process.env.ROOT_HOST || 'localhost';
const devMode = !!process.env.DEV_MODE;
const rootUrl = `http://${rootHost}:8800`;
const debugMode = process.env.JEST_DEBUG && process.env.JEST_DEBUG !== 'false';

// See https://pptr.dev/#?product=Puppeteer&version=v1.6.0&show=api-pagewaitforselectorselector-options
let waitTimeout;
if (debugMode) {
    waitTimeout = 0;
} else if (devMode) {
    // we've seen ci fail sometimes with a waitTimeout of only 5 seconds
    // but locally, we don't have time to wait 15 seconds.  We wanna know if it failed NOW!
    waitTimeout = 5000;
} else {
    waitTimeout = 15000;
}
const defaultWaitOptions = {
    timeout: waitTimeout,
};

// Configure the jasmine.DEFAULT_TIMEOUT_INTERVAL
// See https://github.com/facebook/jest/issues/5247#issuecomment-369952418
if (debugMode) {
    jest.setTimeout(100000);
} else {
    // Base the overall jasmine timeout on a factor of the waitTimeout. We had to
    // bump the waitTimeout from 5 seconds to 10 seconds for CircleCI to handle
    // degraded service, but we had forgotten to bump the overall jasmine timeout,
    // which caused tests with a number of wait calls to still time out.
    jest.setTimeout(defaultWaitOptions.timeout * 4);
}

class PuppetMaster {
    static get db() {
        if (!this._db) {
            // - the `puppetmaster` user is created in `rake test:ensure_puppetmaster_role`
            // - `docker.for.mac.host.internal` is the way that docker refers back to your machine
            // - we probably shouldn't hardcode the db, user and (lack of) password, but it works for now
            this._db = pgp(`postgres://puppetmaster@${dbHost}/back_royal_puppet`);
        }
        return this._db;
    }

    static async setupSuite(authArgs) {
        global.beforeAll(async () => {
            // When we call functions to mock out state we store another function to reset the state back to what
            // it was. See also mockUserDefinedProperty below. Then after each test we can go through the array
            // of resetFunctions and execute them. Thought this might be nice for reducing boilerplate, since
            // typically we are redefining properties to get the UI state to look like we want.
            PuppetMaster.resetFunctions = [];

            // When we mock elements we maintain the selectors for unmocking after each test
            PuppetMaster.mockedElements = [];

            // Get the name of the suite from the caller filename
            // Ex. foo_bar_screenshot_spec.js has a suiteName of foo_bar
            const callerFilename = path.basename(module.parent.filename);
            PuppetMaster.suiteName = callerFilename.split('_screenshot_spec')[0];

            await PuppetMaster.initializeApp();

            if (authArgs) {
                await PuppetMaster.auth(authArgs.email, authArgs.password, authArgs.initialRouteLoadedSelector);
            }
        });

        global.afterAll(async () => {
            if (this._db) {
                this._db.$pool.end();
            }
            await PuppetMaster.close();
        });

        global.afterEach(async () => {
            // Restore state that was mocked
            if (PuppetMaster.resetFunctions) {
                await Promise.all(PuppetMaster.resetFunctions.map(fn => fn()));
            }
            PuppetMaster.resetFunctions = [];

            // If we don't have AngularJS, we don't have a $rootScope and
            // don't need to call rootScopeApply().
            const controller = await PuppetMaster.page.$('[ng-controller]');
            if (controller) {
                // rootScopeApply will blow up if we do not yet have the
                // evalHelper set up
                await PuppetMaster.ensureEvalHelper();
                await PuppetMaster.rootScopeApply();
                await PuppetMaster.unmockElements(PuppetMaster.mockedElements);

                // Don't totally understand it, but unmocking can sometimes result
                // in us navigating to a different place.  Need to wait for that navigation
                // to complete to prevent race conditions. (fwiw, I'm not convinced
                // this is actually fixing anything.  Put it in to try to prevent
                // some intermittent issues, but I don't think it helped.)
                await PuppetMaster.waitFor(
                    () => !$('[ng-controller]').injector().get('$route').frontRoyalIsResolvingRoute,
                );
            }
        });
    }

    static async initializeApp() {
        let args = puppeteer.defaultArgs();

        // As of Puppeteer 1.7 this flag is added by default, which seems weird
        // because there is a puppeteer launch option for it. Since we want to use the
        // default args plus a few of our own we need to manually remove `--headless`
        // and just let the puppeteer launch option below dictate whether it is added
        // or not.
        args = args.filter(arg => arg !== '--headless');

        args.push('--disable-gpu');
        args.push('--ignore-certificate-errors');
        args.push('--disable-web-security');
        args.push('--no-sandbox');
        args.push('--disable-setuid-sandbox');
        args.push('--no-zygote');
        args.push('--enable-font-antialiasing');
        args.push('--incognito');

        // Prevents issue in Docker with a low shared memory space
        // See https://developers.google.com/web/tools/puppeteer/troubleshooting
        args.push('--disable-dev-shm-usage');

        // see also: https://pptr.dev/#?product=Puppeteer&version=v1.6.0&show=api-class-puppeteer
        const puppeteerOptions = {
            args,
            dumpio: true,
            env: {
                TZ: 'UTC',
            },
        };

        // allow for known Chrome path, such as in a Docker container
        if (process.env.CHROME_BIN) {
            puppeteerOptions.executablePath = process.env.CHROME_BIN;
        }

        // run in a way that makes it easier for debugging, if desired
        if (debugMode) {
            puppeteerOptions.headless = false;
            puppeteerOptions.devtools = true;
            puppeteerOptions.slowMo = 250;
        }

        // FIXME: we are seeing this set to undefined if `CHROME_BIN` is not specified. When verbose
        // logging is enabled in Jest, we see an underlying exception trace indicating the issue. Seems
        // like we're not handling this promise flow properly.
        PuppetMaster.browser = await puppeteer.launch(puppeteerOptions);

        // Use the about:blank page that opens by default
        const pages = await PuppetMaster.browser.pages();
        PuppetMaster.page = pages[0];

        // Test Environment info
        const browserVersion = await PuppetMaster.browser.version();
        // eslint-disable-next-line no-console
        console.info(`Connecting to ${rootUrl} using ${browserVersion} ...`);

        // Load the app
        await PuppetMaster.page.goto(PuppetMaster.urlWithCasperMode(rootUrl));

        // Add the puppet style to the html element
        await PuppetMaster.evaluate(() => {
            const htmlElement = document.querySelector('html');
            htmlElement.classList.add('puppet');

            // FIXME: We could also set casperMode here as well, rather than
            // doing it on the angular side which requires the URL parameters (see urlWithCasperMode).
            // Feels more straightforward to do it here.
        });
    }

    static async close() {
        await PuppetMaster.browser.close();
    }

    // Note: be careful with this method. See DANGER comment below for a subtle potential race condition.
    // Note: We wanted to use '#ng-view' as the default selector here but for some reason
    // that does not work for the pre-authenticated tests. It should, so I made a ticket
    // to investigate later.
    // See https://trello.com/c/qdqNm9LH
    // See https://bitbucket.org/pedago-ondemand/back_royal/pull-requests/2622/fix-casper-allow-a-spec-to-specify-which/diff
    static async updateAngularLocationUrl(relativeUrl, waitForSelector = '[ng-controller]') {
        relativeUrl = PuppetMaster.urlWithCasperMode(relativeUrl);

        // Navigate Angular to the relativeUrl
        await PuppetMaster.page.evaluate(relativeUrl => {
            $('[ng-controller]').injector().get('$location').url(relativeUrl);
            $('[ng-controller]').injector().get('safeApply')($('[ng-controller]').injector().get('$rootScope'));
        }, relativeUrl);

        // DANGER: if the waitForSelector below is already on the page prior to the above URL change, we think there
        // may be a race condition where the waitForSelector will immediately trigger. The end result is that the
        // puppeteer spec that has invoked this call will continue on as if it was already on the new page,
        // when in fact it is still on the old page.
        //
        // Be very careful about which selector you wait on, and think about the page from which you are
        // navigating from (i.e.: the state leftover from the previous spec in your suite).
        //
        // An example bug we have seen is a series of specs that test different stream dashboard pages; each was waiting
        // on the same selector (the .hexagons-box) which was shared across the stream dashboard pages.

        await PuppetMaster.waitForSelector(waitForSelector);
    }

    // NOTE: If you're trying to go to a Smartly route, make sure to include `force_host=smart.ly`
    // in the query params of the relativeUrl. Otherwise you'll actually be redirected to smart.ly.
    static async goto(relativeUrl, waitForSelectorOrTimeout = '[ng-controller]') {
        relativeUrl = PuppetMaster.urlWithCasperMode(relativeUrl);

        await PuppetMaster.page.goto(`${rootUrl}${relativeUrl}`, {
            waitUntil: 'networkidle0',
        });
        await PuppetMaster.waitFor(waitForSelectorOrTimeout);
        await PuppetMaster.ensureEvalHelper();
    }

    static urlWithCasperMode(url) {
        // casperMode turns off animations and ensures that the page blocks until fonts load
        // It should probably also implement the disable randomization stuff we do in the frame screeenshotting
        // See casper_mode.js
        if (url.match(/\?/)) {
            url = `${url}&casperMode=true`;
        } else {
            url = `${url}?casperMode=true`;
        }
        return url;
    }

    static async setViewport(name) {
        const width = viewportWidths[name];

        // First set the viewport width based on the viewport name; height is arbitrary right now and will
        // be set based on the scrollHeight below.
        await PuppetMaster.page.setViewport({
            width,
            height: 600,
        });

        // This seems to help stabilize viewport height during spec runs
        await PuppetMaster.page.waitFor(500);

        // Get what the scrollable height is after setting the width so we can set the viewport
        // height to be exactly correct. For some mobile pages, like the careers forms, we have to
        // use `.main-box` instead of `#app-main-container` to get the `scrollHeight`.
        //
        // FIXME: We may need to make this configurable by suite or test so we can clean up
        // all the special casing
        const renderHeight = await PuppetMaster.page.evaluate(() => {
            const appMainContainerElement = $('#app-main-container').get(0);
            const appHeaderElement = $('app-header').get(0);
            const mainBoxElement = $('.main-box').get(0);

            // We use different footers on different pages
            const dashboardFooterElement = $('dashboard-footer').get(0);
            const frontRoyalFooterElement = $('.front-royal-footer').get(0);

            // Used in responsive `page-with-left-nav-and-main-box` screens
            const responsiveNav = $('.responsive-nav').get(0);
            const appMenuMobile = $('.app-menu-mobile').get(0);

            const scrollableElement =
                document.documentElement.offsetWidth < 992 && mainBoxElement && mainBoxElement.scrollHeight > 0
                    ? mainBoxElement
                    : appMainContainerElement;

            const renderHeight =
                (scrollableElement ? scrollableElement.scrollHeight : 0) +
                (appHeaderElement ? appHeaderElement.scrollHeight : 0) +
                (dashboardFooterElement ? dashboardFooterElement.scrollHeight : 0) +
                (frontRoyalFooterElement ? frontRoyalFooterElement.scrollHeight : 0) +
                (responsiveNav ? responsiveNav.scrollHeight : 0) +
                (appMenuMobile ? appMenuMobile.scrollHeight : 0);
            return renderHeight;
        });

        await PuppetMaster.page.setViewport({
            width,
            height: renderHeight,
        });

        // This seems to help stabilize viewport height during spec runs
        await PuppetMaster.page.waitFor(500);
    }

    static async assert(options = {}) {
        options.viewportNames = options.viewportNames || ['desktop', 'iphone5'];
        if (options.setViewport === false) {
            options.viewportNames = ['custom'];
        }

        // Mock out elements we don't care about
        if (options.mock) {
            if (typeof options.mock === 'string' || options.mock instanceof String) {
                options.mock = [options.mock];
            }
            await PuppetMaster.mockElements(options.mock);
        }

        // Use the loop here instead of forEach so that these
        // are run in sequence, not in parallel.
        // eslint-disable-next-line no-restricted-syntax
        for (const viewportName of options.viewportNames) {
            if (viewportName !== 'custom') {
                // eslint-disable-next-line no-await-in-loop
                await PuppetMaster.setViewport(viewportName);
            }

            // Move the mouse to avoid random hover failures
            // eslint-disable-next-line no-await-in-loop
            await PuppetMaster.page.mouse.move(options.mouseX || 0, options.mouseY || 0);

            let screenshot;
            if (options.captureSelector) {
                try {
                    // eslint-disable-next-line no-await-in-loop
                    const elementHandle = await PuppetMaster.page.$(options.captureSelector);
                    // eslint-disable-next-line no-await-in-loop
                    screenshot = await elementHandle.screenshot();
                } catch (e) {
                    throw new Error(
                        `Could not take screenshot of ${options.captureSelector} in ${defaultWaitOptions.timeout}ms`,
                    );
                }
            } else {
                // eslint-disable-next-line no-await-in-loop
                screenshot = await PuppetMaster.page.screenshot();
            }

            // Organize suite screenshots by folder.
            // See https://nodejs.org/docs/latest/api/modules.html#modules_dirname
            const customSnapshotsDir = `${__dirname}/__image_snapshots__/${PuppetMaster.suiteName}/${viewportName}`;

            // FIXME: Investigate the intermittent image rendering failures so we can remove the really low, but still
            // undesirable, failureThreshold.
            // See: https://github.com/americanexpress/jest-image-snapshot#optional-configuration
            //
            // Note: We specify the failureThresholdType as 'percent', but the conditional in code is really
            // checking against the ratio (i.e. it's not multiplied by 100). This is wrong IMO. I'm going
            // to make a ticket for it, but just know that by saying '0.0001' we mean a threshold of '0.01%'.
            // See: https: //github.com/americanexpress/jest-image-snapshot/issues/99
            expect(screenshot).toMatchImageSnapshot({
                customSnapshotsDir,
                customDiffDir: `${__dirname}/__diff_output__/`,
            });
        }
    }

    static async auth(
        email = 'mba_user@pedago.com',
        password = 'password',
        initialRouteLoadedSelector = 'student-dashboard',
    ) {
        const authUrl = PuppetMaster.urlWithCasperMode(`${rootUrl}/sign-in`);
        await PuppetMaster.page.goto(authUrl);
        await PuppetMaster.page.waitForSelector('input[name="email"]');
        await PuppetMaster.page.waitForSelector('.form-submit');

        // Logging in is painful when debugging with slowMo because the delay is applied to
        // each keystroke. So rather than use the `type` API let's just set the form values in
        // jQuery directly. FWIW, I feel that the delay option for `type` should override `slowMo`.
        // See https://github.com/GoogleChrome/puppeteer/issues/1058
        await PuppetMaster.page.evaluate(
            (email, password) => {
                const form = $('sign-in-form');
                form.find('[name="email"]').val(email).change();
                form.find('[name="password"]').val(password).change();
            },
            email,
            password,
        );

        // Hackish: we've seen this fail with the native puppeteer click() method, possibly
        // because the .form-submit button isn't being measured properly internally in puppeteer
        // There are numerous examples online of puppeteer's native click failing when the javascript-based
        // click will work, so we're using that here to avoid the issue entirely.
        await PuppetMaster.clickViaJavascript('.form-submit');
        await PuppetMaster.page.waitForSelector('app-shell');

        // Wait for the initial route to actually load up since we are using
        // properties that we might want to mock in the route resolvers. We saw issues that
        // ended up being due to prematurely trying to set properties before all the route resolvers
        // were finished.
        await PuppetMaster.waitForSelector(initialRouteLoadedSelector);

        // Set up the evalHelper once the user is on the $rootScope
        await PuppetMaster.ensureEvalHelper();

        PuppetMaster.signedIn = true;
    }

    static async signUp(email = 'mba_user@pedago.com', password = 'password', queryParams = '') {
        const signUpUrl = PuppetMaster.urlWithCasperMode(`${rootUrl}/candidates/signup${queryParams}`);
        await PuppetMaster.page.goto(signUpUrl);
        await PuppetMaster.waitForSelector('input[name="email"]');

        await PuppetMaster.page.evaluate(
            (email, password) => {
                const form = $('[marketing-sign-up-form]');
                form.find('[name="name"]').val('John Doe').change();
                form.find('[name="email"]').val(email).change();
                form.find('[name="password"]').val(password).change();
            },
            email,
            password,
        );

        await PuppetMaster.page.click('.sm-form__submit');
        await PuppetMaster.waitForSelector('app-shell');

        // Wait for the initial route to actually load up since we are using
        // properties that we might want to mock in the route resolvers. We saw issues that
        // ended up being due to prematurely trying to set properties before all the route resolvers
        // were finished.
        await PuppetMaster.waitForSelector('[ng-view] [compile] > *');

        // Set up the evalHelper once the user is on the $rootScope
        await PuppetMaster.ensureEvalHelper();

        PuppetMaster.signedIn = true;
    }

    static async signOut() {
        if (!PuppetMaster.signedIn) {
            return;
        }

        await PuppetMaster.page.evaluate(() => {
            $('[ng-controller]').injector().get('$auth').signOut();
        });

        await PuppetMaster.waitForSelector('app-shell', {
            hidden: true,
        });
    }

    static async mockElement(selector) {
        await PuppetMaster.mockElements([selector]);
    }

    static async mockElements(selectors) {
        await PuppetMaster.page.evaluate(selectors => {
            selectors.forEach(selector => {
                // see Prototye.js#makePositioned
                const element = $(selector);
                const position = element.css('position');
                const styles = {};

                if (position === 'static' || !position) {
                    styles.position = 'relative';
                    element.css(styles);
                }

                element.addClass('casper-mock');
            });
        }, selectors);

        PuppetMaster.mockedElements = PuppetMaster.mockedElements.concat(selectors);
    }

    static async unmockElements(selectors = ['[ng-controller]']) {
        const promises = selectors.map(async selector => {
            await PuppetMaster.page.evaluate(selector => {
                if (window.$) {
                    const elements = $(selector).filter('.casper-mock');
                    elements.removeClass('casper-mock');
                }
            }, selector);
        });
        await Promise.all(promises);
    }

    static async mockUserProperty(property, val) {
        await PuppetMaster.evaluate(
            (property, val) => {
                if (!window.originalUserProperties) {
                    window.originalUserProperties = {};
                }
                window.originalUserProperties[property] = evalHelper.currentUser[property];

                Object.defineProperty(evalHelper.currentUser, property, {
                    value: val,
                    configurable: true,
                });

                evalHelper.$rootScope.$apply();
            },
            property,
            val,
        );

        PuppetMaster.resetFunctions.unshift(async () => {
            await PuppetMaster.page.evaluate(property => {
                const originalVal = window.originalUserProperties[property];
                delete window.originalUserProperties[property];
                Object.defineProperty(evalHelper.currentUser, property, {
                    value: originalVal,
                    configurable: true,
                });
            }, property);
        });
    }

    static async mockConfigProperty(property, val) {
        // We always reset config.  See where we push a function onto
        // resetFunctions in setupSuite
        await PuppetMaster.page.evaluate(
            (property, val) => {
                const config = evalHelper.inj('ConfigFactory')._config;
                config[property] = val;
                evalHelper.$rootScope.$apply();
            },
            property,
            val,
        );

        const originalVal = await PuppetMaster.evaluate(
            property => evalHelper.inj('ConfigFactory').getSync()[property],
            property,
        );

        PuppetMaster.resetFunctions.unshift(async () => {
            await PuppetMaster.page.evaluate(
                (property, originalVal) => {
                    evalHelper.inj('ConfigFactory').getSync()[property] = originalVal;
                },
                property,
                originalVal,
            );
        });
    }

    // FIXME: We might be able to move this into the hiring_profile suite. We don't currently
    // know if it will be used elsewhere at this point
    static async mockHiringApplicationProperty(property, val) {
        const originalVal = await PuppetMaster.evaluate(
            (property, val) => {
                const originalVal = evalHelper.currentUser.hiring_application[property];

                Object.defineProperty(evalHelper.currentUser.hiring_application, property, {
                    value: val,
                    configurable: true,
                });

                return originalVal;
            },
            property,
            val,
        );

        PuppetMaster.resetFunctions.unshift(async () => {
            await PuppetMaster.page.evaluate(
                (property, originalVal) => {
                    Object.defineProperty(evalHelper.currentUser.hiring_application, property, {
                        value: originalVal,
                        configurable: true,
                    });
                },
                property,
                originalVal,
            );
        });
    }

    // FIXME: We might be able to move this into the careers suite. We don't currently
    // know if it will be used elsewhere at this point
    static async mockCareerProfileProperty(property, val) {
        const originalVal = await PuppetMaster.evaluate(
            (property, val) => {
                const originalVal = evalHelper.currentUser.career_profile[property];

                Object.defineProperty(evalHelper.currentUser.career_profile, property, {
                    value: val,
                    configurable: true,
                });

                return originalVal;
            },
            property,
            val,
        );

        PuppetMaster.resetFunctions.unshift(async () => {
            await PuppetMaster.page.evaluate(
                (property, originalVal) => {
                    Object.defineProperty(evalHelper.currentUser.career_profile, property, {
                        value: originalVal,
                        configurable: true,
                    });
                },
                property,
                originalVal,
            );
        });
    }

    static async mockPrototypeGetter(factoryName, property, val) {
        // Since the original getter is a function, it cannot be
        // serialized and passed back from the `evaluate` call.  So we
        // generate an id with which to save it on the window.
        const id = Math.random().toString();
        await PuppetMaster.evaluate(
            (factoryName, property, val, id) => {
                const klass = evalHelper.inj(factoryName);
                const originalGetter = Object.getOwnPropertyDescriptor(klass.prototype, property).get;
                window[`mockGetter${id}`] = originalGetter;

                Object.defineProperty(klass.prototype, property, {
                    value: val,
                    configurable: true,
                });

                return originalGetter;
            },
            factoryName,
            property,
            val,
            id,
        );

        PuppetMaster.resetFunctions.unshift(async () => {
            await PuppetMaster.page.evaluate(
                (factoryName, property, id) => {
                    const klass = evalHelper.inj(factoryName);
                    const originalGetter = window[`mockGetter${id}`];
                    delete window[`mockGetter${id}`];
                    Object.defineProperty(klass.prototype, property, {
                        get: originalGetter,
                        configurable: true,
                    });
                },
                factoryName,
                property,
                id,
            );
        });
    }

    static async mockUserDefinedProperty(property, val) {
        await PuppetMaster.page.evaluate(
            (property, val) => {
                Object.defineProperty(evalHelper.currentUser, property, {
                    value: val,
                    configurable: true,
                });
            },
            property,
            val,
        );

        PuppetMaster.resetFunctions.push(async () => {
            await PuppetMaster.page.evaluate(property => {
                const userPrototype = Object.getPrototypeOf(evalHelper.currentUser);
                const previousDescriptor = Object.getOwnPropertyDescriptor(userPrototype, property);

                // FIXME: For some reason when we moved the global setup and teardown functions to puppet_master.js
                // instead of the suite file itself I started seeing an error when running hiring_connections_screenshot
                // that previousDescriptor wasn't defined. It doesn't cause a problem to just return when that
                // happens, but I would like to figure out exactly what is happening.
                if (!previousDescriptor) {
                    return;
                }

                previousDescriptor.configurable = true;
                Object.defineProperty(evalHelper.currentUser, property, previousDescriptor);
            }, property);
        });
    }

    static async removeThingsFromHiringBrowseThatChangeBasedOnNumberOfResults() {
        await PuppetMaster.removeCountsFromTabsAndPagination();
        await PuppetMaster.waitForSelector('.list-wrapper');
        await PuppetMaster.page.evaluate(() => {
            // This keeps changing with fixtures changes, and we don't care
            $('.list-wrapper').height(200).css({
                overflow: 'hidden',
            });
        });
    }

    static async removeCountsFromTabsAndPagination() {
        // Wait until all tabs are loaded.  We can tell this by waiting
        // for them to all have an open parenthesis followed by a digit (or followed
        // by an N, since the test may have already mocked them out)
        //
        // Buttons that do not have parentheses at all do not concern us
        await PuppetMaster.waitFor(() => {
            let unloadedButton = false;
            const buttons = $('.tab button');

            if (buttons.length === 0) {
                return false;
            }
            buttons.each((i, el) => {
                const buttonText = $(el).text();
                if (buttonText.match(/\(/) && !buttonText.match(/\([\dN]/)) {
                    unloadedButton = true;
                }
            });
            return !unloadedButton;
        });
        await PuppetMaster.evaluate(() => {
            $('.tab button').each(function () {
                const button = $(this);
                const text = button.text();
                button.text(text.replace(/\d+/g, 'N'));
            });
        });

        await PuppetMaster.waitForSelector('.pagination');
        await PuppetMaster.evaluate(() => {
            $('.pagination .current').each(function () {
                const button = $(this);
                const text = button.text();
                button.text(text.replace(/\d+/g, 'N'));
            });
        });
    }

    static rerender() {
        return PuppetMaster.evaluate(async () => {
            await evalHelper.rerender();
        });
    }

    static evaluate(pageFunction, ...args) {
        return PuppetMaster.page.evaluate(pageFunction, ...args);
    }

    static async waitForAndClick(selector) {
        await PuppetMaster.waitForSelector(selector);
        return PuppetMaster.click(selector);
    }

    static async click(selector, options) {
        // When I was running the
        // front_royal_store_e2e_spec in debug mode, PuppetMaster.page.click was
        // not working at all, even though actually clicking on the elements in
        // the Chromium browser was working.
        //
        // It's possible that when PuppetMaster.click doesn't work even though
        // $(selector).click() works, there is a real issue.  See https://github.com/puppeteer/puppeteer/issues/372#issuecomment-359126986
        // For that reason, I didn't want to enable this generally. If you need it,
        // you can use the clickViaJavascript helper method.

        try {
            return await PuppetMaster.page.click(selector, options);
        } catch (err) {
            await PuppetMaster.takeErrorScreenshot();
            throw err;
        }
    }

    // Puppeteer's page.click method works by moving the mouse cursor over the center of the element and then
    // deep clicking into it to trigger all associated side-effects. However, if there is trouble measuring the element,
    // e.g.: it returns a 0x0 quad or something, the click will fail with a "Node is either not visible or not an HTMLElement"
    // error. In these cases, it's often a real problem, but sometimes it's just a side effect of being a container element
    // or something similar. In these cases, an acceptable workaround may be to use this method to trigger the click via javascript.
    static async clickViaJavascript(selector) {
        return PuppetMaster.page.evaluate(_selector => {
            document.querySelector(_selector).click();
        }, selector);
    }

    static debugger() {
        return PuppetMaster.page.evaluate(() => {
            // eslint-disable-next-line no-debugger
            debugger;
        });
    }

    static rootScopeApply() {
        return PuppetMaster.page.evaluate(() => {
            evalHelper.$rootScope.$apply();
        });
    }

    // FIXME: We need to reconcile this wrapper function with a Puppeteer one that was introduced with the same name
    static waitForSelector(selector, additionalWaitOptions = {}) {
        return PuppetMaster.waitFor(selector, additionalWaitOptions);
    }

    static async waitFor(selectorOrFunctionOrTimeout, additionalWaitOptions = {}) {
        const options = { ...defaultWaitOptions, ...additionalWaitOptions };
        try {
            return await PuppetMaster.page.waitFor(selectorOrFunctionOrTimeout, options);
        } catch (e) {
            await PuppetMaster.takeErrorScreenshot();
            throw new Error(`waitFor ${selectorOrFunctionOrTimeout} failed with timeout of ${options.timeout}ms`);
        }
    }

    static async takeErrorScreenshot() {
        const errorDir = `${__dirname}/__errors__`;

        // https://stackoverflow.com/a/26815894/1747491
        if (!fs.existsSync(errorDir)) {
            fs.mkdirSync(errorDir);
        }

        await PuppetMaster.page.screenshot({
            path: `${errorDir}/error_waitFor_${PuppetMaster.suiteName}_${new Date().getTime()}.png`,
            fullPage: true,
        });
    }

    static async ensureEvalHelper() {
        await PuppetMaster.waitFor(() => !!$('[ng-controller]').injector());

        await PuppetMaster.page.evaluate(() => {
            if (window.evalHelper) {
                return;
            }

            const injector = $('[ng-controller]').injector();

            window.evalHelper = {
                init() {
                    // We REALLY should have made free_mba a flag on a user!!!
                    // We do this so that mba_user@pedago.com can have isFreeMBA = true
                    // but also have a group that only has content in it that is valid
                    // for the browser
                    this.setConfig({
                        free_mba_groups: 'CASPER_MBA',
                        alternate_banner_message: 'Alternate <strong>banner</strong> message.',
                    });
                },

                inj(name) {
                    return injector.get(name);
                },

                setConfig(overrides) {
                    _.extend(this.inj('ConfigFactory')._config, overrides);
                    this.$rootScope.$apply();
                },

                // FIXME: add support for some sort of resetWindow or generalized resetContext
                // should this and resetCurrentUser be called on every suite / spec / what?

                resetCurrentUser() {
                    // FIXME: This needs some more work
                    this.$rootScope.currentUser = this.$rootScope.originalCurrentUser;
                    this.$rootScope.$apply();
                },

                rerender() {
                    this.$rootScope.casperRender = false;
                    this.$rootScope.$apply();
                    this.$rootScope.casperRender = true;
                    this.$rootScope.$apply();
                    // Returning `this.$timeout()` here because it will return a promise
                    // that is resolved after the current digest cycle. We typically
                    // call `evalHelper.rerender()` inside of an Puppeteer `Page.evaluate()`
                    // function, which will wait for that promise to resolve.
                    return this.$timeout();
                },
            };

            Object.defineProperty(window.evalHelper, '$rootScope', {
                get() {
                    return this.inj('$rootScope');
                },
            });

            Object.defineProperty(window.evalHelper, '$timeout', {
                get() {
                    return this.inj('$timeout');
                },
            });

            Object.defineProperty(window.evalHelper, 'currentUser', {
                get() {
                    return this.$rootScope.currentUser;
                },
            });

            window.evalHelper.init();
        });
    }
}

module.exports = PuppetMaster;
