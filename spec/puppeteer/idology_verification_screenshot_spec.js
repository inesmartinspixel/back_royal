const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite({
    email: 'mba_user@pedago.com',
});

beforeAll(async () => {
    await PuppetMaster.waitForSelector('[ng-controller]');
});

describe('idology_verification', () => {
    beforeEach(async () => {
        await PuppetMaster.mockUserProperty('unverifiedForCurrentIdVerificationPeriod', true);

        // ensure showEnrollment is false
        await PuppetMaster.mockUserProperty('recordsIndicateUserShouldUploadIdentificationDocument', false);
        await PuppetMaster.mockUserProperty('careerProfileIndicatesUserShouldProvideTranscripts', false);
        await PuppetMaster.mockUserProperty(
            'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments',
            false,
        );
        await PuppetMaster.mockUserProperty('missingAddress', false);

        await PuppetMaster.evaluate(() => {
            // prevent auth error on server
            evalHelper.currentUser._requestIdologyLink = evalHelper.currentUser.requestIdologyLink;
            evalHelper.currentUser.requestIdologyLink = () => {};
        });
    });

    afterEach(async () => {
        // undo stuff
        await PuppetMaster.evaluate(() => {
            evalHelper.currentUser.requestIdologyLink = evalHelper.currentUser._requestIdologyLink;
            delete evalHelper.currentUser._requestIdologyLink;
        });
    });

    it('should show the failed step', async () => {
        // anywhwere other than dashboard to start
        await PuppetMaster.updateAngularLocationUrl('/settings');

        await PuppetMaster.evaluate(() => {
            const ClientStorage = evalHelper.inj('ClientStorage');
            ClientStorage.removeItem('launchVerificationModalIfLastIdologyVerificationFailed.42');

            // set up a failed idology verification so it will pop up the
            // failed page of the modal
            evalHelper.currentUser.idology_verifications = [
                {
                    response_received_at: (Date.now() - 60000 * 5) / 1000,
                    verified: false,
                    idology_id_number: 42,
                },
            ];
        });

        await PuppetMaster.updateAngularLocationUrl('/dashboard');

        // verification modal will pop up automatically because we have the idology verification
        await PuppetMaster.assert({
            captureSelector: '[data-id="contents"]',
        });

        // undo stuff
        await PuppetMaster.evaluate(() => {
            const ClientStorage = evalHelper.inj('ClientStorage');
            ClientStorage.removeItem('launchVerificationModalIfLastIdologyVerificationFailed.42');
            evalHelper.currentUser.idology_verifications = [];
        });
    });

    it('should show the form step', async () => {
        await openVerificationModal();
        await PuppetMaster.assert({
            captureSelector: '[data-id="contents"]',
        });
    });

    it('should show the verifying step', async () => {
        await openVerificationModal();
        await PuppetMaster.click('[data-id="contents"] [type="submit"]');
        await PuppetMaster.assert({
            captureSelector: '[data-id="contents"]',
        });
    });

    it('should show the success step', async () => {
        await openVerificationModal();
        await PuppetMaster.mockUserProperty('unverifiedForCurrentIdVerificationPeriod', false);
        await PuppetMaster.assert({
            captureSelector: '[data-id="contents"]',
        });
    });

    async function openVerificationModal() {
        await PuppetMaster.updateAngularLocationUrl('/dashboard');
        await PuppetMaster.rerender();
        await PuppetMaster.click('student-dashboard-identity-verification button[name=verify_id]');
    }
});
