const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite();

beforeAll(async () => {
    await PuppetMaster.waitForSelector('[ng-controller]');
});

it('should render onboarding hybrid page', async () => {
    await PuppetMaster.goto('/onboarding/hybrid');
    await PuppetMaster.waitForSelector('onboarding-hybrid-start');
    await PuppetMaster.assert({
        captureSelector: '.slider',
        viewportNames: ['iphone5']
    });
});

it('should render onboarding hybrid register page', async () => {
    await PuppetMaster.goto('/onboarding/hybrid/register');
    await PuppetMaster.waitForSelector('onboarding-hybrid-register');
    await removeCaptcha();
    await PuppetMaster.assert({
        captureSelector: '.onboarding_hybrid_outter_wrapper',
        viewportNames: ['iphone5']
    });
});

it('should render onboarding hybrid login page', async () => {
    await PuppetMaster.goto('/onboarding/hybrid/login');
    await PuppetMaster.waitForSelector('onboarding-hybrid-login');
    await PuppetMaster.assert({
        captureSelector: '.onboarding_hybrid_outter_wrapper',
        viewportNames: ['iphone5']
    });
});

it('should render onboarding hybrid forgot-password page', async () => {
    await PuppetMaster.goto('/onboarding/hybrid/forgot-password');
    await PuppetMaster.waitForSelector('onboarding-hybrid-forgot-password');
    await PuppetMaster.assert({
        captureSelector: '.onboarding_hybrid_outter_wrapper',
        viewportNames: ['iphone5']
    });
});

const removeCaptcha = async () => {
    await PuppetMaster.waitForSelector('.g-recaptcha');
    await PuppetMaster.evaluate(() => {
        $('.g-recaptcha').remove();
        evalHelper.$rootScope.$apply();
    });
};
