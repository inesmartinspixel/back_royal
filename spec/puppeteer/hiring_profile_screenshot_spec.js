const PuppetMaster = require('./puppet_master');

PuppetMaster.setupSuite({
    email: 'hiring_manager@pedago.com',
    initialRouteLoadedSelector: 'hiring-positions',
});

describe('hiring application forms', () => {
    it('should render company form', async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/profile?page=1', 'edit-hiring-manager-profile');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });

    it('should render team form', async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/profile?page=2', 'edit-hiring-manager-profile');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });

    it('should render you form', async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/profile?page=3', 'edit-hiring-manager-profile');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });

    it('should render candidates form', async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/profile?page=4', 'edit-hiring-manager-profile');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });

    it('should render preview profile with complete card', async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/profile?page=5', 'preview-form');
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });

    it('should render complete card overlay', async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/profile?page=5', 'preview-form');
        await PuppetMaster.click('hiring-manager-card a.view-all');
        await PuppetMaster.assert({
            captureSelector: '.careers-card-view-all-overlay',
        });
    });

    it('should render preview profile with incomplete card', async () => {
        await PuppetMaster.updateAngularLocationUrl('/settings/profile?page=5', 'preview-form');
        await PuppetMaster.page.$eval('edit-hiring-manager-profile', directive => {
            const scope = $(directive).isolateScope();
            scope.$apply(() => {
                scope.hiringApplication = {
                    complete: false,
                };
            });
        });
        await PuppetMaster.assert({
            captureSelector: '.main-box',
        });
    });
});
