#!/bin/bash

JEST_FLAGS="$1 $2"
case "$1" in
    -u|--updateSnapshot)
        DOCKER_MESSAGE="Running containerized Jest in UPDATE mode ..."
        ;;
    --watch)
        DOCKER_MESSAGE="Running containerized Jest in WATCH mode ..."
        ;;
    "")
        DOCKER_MESSAGE="Running containerized Jest in COMPARE mode ..."
        ;;
    --debug)
        DOCKER_MESSAGE="Running localized Jest in COMPARE [DEBUG] mode ..."
        DEBUG_MODE=true
        JEST_FLAGS="$2"
        ;;
    --debug-watch)
        DOCKER_MESSAGE="Running localized Jest in WATCH [DEBUG] mode ..."
        DEBUG_MODE=true
        JEST_FLAGS="--watch $2"
        ;;
    *)
        ;;
esac


if [ ! -f tmp/pids/testserver.pid ]; then
    echo "Starting test server ..."
    npm run testserver 2>&1 >/dev/null &
    while [ -z "$TESTSERVER_PID" ]; do
        sleep 1
        # suppress stderr during polling
        TESTSERVER_PID="$(cat tmp/pids/testserver.pid 2> /dev/null)"
    done
else
    echo "Test server already running, skipping startup ..."
fi


echo $DOCKER_MESSAGE
if [ -z "$DEBUG_MODE" ]; then
    # NOTE: Host mounts create the biggest performance bottleneck on this. We may wish to abandon the
    # native OSXFS implementation and use nfs or rsync instead, if we utilize this container for more
    # intensive developer operations (such as a full dev environment loadout). This seemed to be the
    # only / preferred approach prior to OSXFS performance optimizations with `:cached` and
    # `:delegated` host-mount flags. The trade-offs here would be complexity and future support paths.
    #
    # See also: https://docs.docker.com/docker-for-mac/osxfs-caching/#cached
    docker run \
        --net="host" \
        -v $(pwd):/home/circleci/back_royal:rw,cached  \
        -v $HOME/.ssh:/home/circleci/.ssh:ro,cached \
        -e PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true \
        -e ROOT_HOST=docker.for.mac.host.internal \
        -e DEV_MODE=true \
        -w /home/circleci/back_royal \
        -t -i --rm \
        --name="screenshot-container" \
        booleanbetrayal/circleci-trusty-ruby-node-python-awscli-psql-chrome:1.0.21 \
        ./node_modules/.bin/jest -c jest/config.puppeteer.json $JEST_FLAGS --runInBand
else
    JEST_DEBUG=true jest -c jest/config.puppeteer.json $JEST_FLAGS --runInBand
fi


if [ ! -z "$TESTSERVER_PID" ]; then
    echo "Killing test server ..."
    kill -9 $TESTSERVER_PID
    rm -f tmp/pids/testserver.pid
fi


echo "Done!"