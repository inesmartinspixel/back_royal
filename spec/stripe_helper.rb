require 'stripe_mock'

# FIXME: Remove once stripe_ruby_mock supports 2018-02-05+
MockProduct = Struct.new(:id, :type)
MockUsageRecord = Struct.new(:id, :quantity, :timestamp)

module StripeHelper


    extend ActiveSupport::Concern

    included do
        let(:stripe_helper) { StripeMock.create_test_helper }
        before {

            StripeMock.start

            # FIXME: Remove once stripe_ruby_mock supports 2018-02-05+
            allow_any_instance_of(Stripe::Plan).to receive(:product).and_return(MockProduct.new("some_id", "some_type"))
            allow_any_instance_of(Stripe::Plan).to receive(:nickname) { |plan| plan.name }
            allow(Stripe::SubscriptionItem).to receive(:create_usage_record).and_return(MockUsageRecord.new("some_id", 1, DateTime.now))

            # FIXME: Remove once stripe_ruby_mock supports 2018-11-08+
            allow_any_instance_of(Stripe::Invoice).to receive(:auto_advance) { |invoice| !invoice.closed }
            allow_any_instance_of(Stripe::Invoice).to receive(:auto_advance=) { |invoice, val| invoice.closed = !val }

            # FIXME: Remove once stripe_ruby_mock supports 2019-03-14+
            allow_any_instance_of(Stripe::Invoice).to receive(:created) { |invoice| invoice.date }


        }
        after { StripeMock.stop }
        Stripe.api_key = 'stripe_api_key'
    end

    def create_default_plan
        default_plan
    end


    # NOTE: We are currently drifting from the Stripe API in that `stripe_ruby_mock` does not support
    # API version 2018-02-05+ which requires providing `product` attributes as well

    def default_plan
        @default_plan ||= Stripe::Plan.create(
            :amount => 80000,
            :interval => 'month',
            :nickname => 'Default Plan',
            :name => 'Default Plan', # FIXME: Remove once stripe_ruby_mock supports 2018-02-05+
            :currency => 'usd',
            :id => 'default_plan',
            :trial_period_days => 0,
            :product => { id: 'default_product', :name => 'Default Product', :type => 'service', :statement_descriptor => 'statement_descriptor' },
            :usage_type => "licensed"
        )
    end

    def plan_with_trial_period
        @plan_with_trial_period ||= Stripe::Plan.create(
            :amount => 80000,
            :interval => 'month',
            :nickname => 'Plan w Trial Period',
            :name => 'Plan w Trial Period', # FIXME: Remove once stripe_ruby_mock supports 2018-02-05+
            :currency => 'usd',
            :id => 'default_plan',
            :trial_period_days => 14,
            :product => { id: 'default_product', :name => 'Default Product', :type => 'service' },
            :usage_type => "licensed"
        )
    end

    def pay_per_post_plan
        product_config = HiringTeam.stripe_product_config[:hiring_pay_per_post]

        @pay_per_post_plan ||= Stripe::Plan.create(product_config[:plans][:hiring_pay_per_post_89].merge(
            id: 'hiring_pay_per_post_89',
            product: product_config.except(:plans).merge(id: 'hiring_pay_per_post')
        ))
    end

    def unlimited_with_sourcing_plan
        product_config = HiringTeam.stripe_product_config[:hiring_unlimited_w_sourcing]

        @unlimited_with_sourcing_plan ||= Stripe::Plan.create(product_config[:plans][:hiring_unlimited_w_sourcing_249].merge(
            id: 'hiring_unlimited_w_sourcing_249',
            product: product_config.except(:plans).merge(id: 'hiring_unlimited_w_sourcing')
        ))
    end

    def hiring_unlimited_plan
        product_config = HiringTeam.stripe_product_config[:hiring_unlimited]

        @unlimited_with_sourcing_plan ||= Stripe::Plan.create(product_config[:plans][:hiring_unlimited_300].merge(
            id: 'hiring_unlimited_300',
            product: product_config.except(:plans).merge(id: 'hiring_unlimited')
        ))
    end

    def metered_plan
        product_config = HiringTeam.stripe_product_config[:hiring_per_match]

        @metered_plan ||= Stripe::Plan.create(product_config[:plans][:hiring_per_match_20].merge(
            id: 'hiring_per_match_20',
            product: product_config.except(:plans).merge(id: 'hiring_per_match')
        ))
    end

    def bi_annual_plan
        @bi_annual_plan ||= Stripe::Plan.create(
            :amount => 4800 * 100,
            :interval => 'month',
            :interval_count => 6,
            :nickname => 'Bi-Annual Plan',
            :name => 'Bi-Annual Plan', # FIXME: Remove once stripe_ruby_mock supports 2018-02-05+
            :currency => 'usd',
            :id => 'bi_annual_plan',
            :trial_period_days => 0,
            :product => { id: 'default_product', :name => 'Default Product', :type => 'service' },
            :usage_type => "licensed"
        )
    end

    def premium_coupon_plan
        @premium_coupon_plan ||= Stripe::Plan.create(
            :amount => 80000,
            :interval => 'month',
            :nickname => 'Premium Plan',
            :name => 'Premium Plan', # FIXME: Remove once stripe_ruby_mock supports 2018-02-05+
            :currency => 'usd',
            :id => 'premium_coupon_plan',
            :trial_period_days => 0,
            :metadata => {
                :allow_premium_coupons => 'true'
            },
            :product => { id: 'premium_product', :name => 'Premium Product', :type => 'service' },
            :usage_type => "licensed"
        )
    end

    def upgradable_plan
        two_year_plan # ensure the plan exists, so someone can upgrade to it
        @upgradable_plan ||= Stripe::Plan.create(
            :amount => 2000,
            :interval => 'month',
            :nickname => 'Upgradable Plan',
            :name => 'Upgradable Plan', # FIXME: Remove once stripe_ruby_mock supports 2018-02-05+
            :currency => 'usd',
            :id => 'upgradable_plan',
            :trial_period_days => 0,
            :metadata => {
                :upgrade_to => 'two_year_plan'
            },
            :product => { id: 'upgradeable_product', :name => 'Upgradable Product', :type => 'service' },
            :usage_type => "licensed"
        )
    end

    def two_year_plan
        @two_year_plan ||= Stripe::Plan.create(
            :amount => 2000,
            :interval => 'month',
            :nickname => 'Two Year Plan',
            :name => 'Two Year Plan', # FIXME: Remove once stripe_ruby_mock supports 2018-02-05+
            :currency => 'usd',
            :id => 'two_year_plan',
            :trial_period_days => 0,
            :metadata => {
                :override_duration_months => '24' # this comes down as a string from stripe
            },
            :product => { id: 'two_year_product', :name => 'Two Year Product', :type => 'service' },
            :usage_type => "licensed"
        )
    end

    def two_day_plan
        @two_day_plan ||= Stripe::Plan.create(
            :amount => 2000,
            :interval => 'day',
            :nickname => 'Two Day Plan',
            :name => 'Two Day Plan', # FIXME: Remove once stripe_ruby_mock supports 2018-02-05+
            :currency => 'usd',
            :id => 'two_day_plan',
            :trial_period_days => 1,
            :metadata => {
                :override_duration_days => '2' # this comes down as a string from stripe
            },
            :product => { id: 'two_day_product', :name => 'Two Day Product', :type => 'service' },
            :usage_type => "licensed"
        )
    end

    def auto_canceling_plan
        @auto_canceling_plan ||= Stripe::Plan.create(
            :amount => 2000,
            :interval => 'month',
            :nickname => 'Auto Canceling Plan',
            :name => 'Auto Canceling Plan', # FIXME: Remove once stripe_ruby_mock supports 2018-02-05+
            :currency => 'usd',
            :id => 'auto_canceling_plan',
            :trial_period_days => 0,
            :metadata => {
                :disallow_auto_renew => 'true'# this comes down as a string from stripe
            },
            :product => { id: 'auto_canceling_product', :name => 'Auto Canceling Product', :type => 'service' },
            :usage_type => "licensed"
        )
    end

    def mba_plan
        @mba_plan ||= Stripe::Plan.create(
            :amount => 2000,
            :interval => 'month',
            :nickname => 'MBA Plan',
            :name => 'MBA Plan', # FIXME: Remove once stripe_ruby_mock supports 2018-02-05+
            :currency => 'usd',
            :id => 'mba_plan',
            :trial_period_days => 0,
            :metadata => {
                :override_duration_months => '24', # this comes down as a string from stripe
                :disallow_auto_renew => 'true'# this comes down as a string from stripe
            },
            :product => { id: 'mba_product', :name => 'MBA Product', :type => 'service' },
            :usage_type => "licensed"
        )
    end

    def default_coupon
        @default_coupon ||= Stripe::Coupon.create(
                :amount_off => 150*100,
                :duration => 'forever',
                :currency => 'usd',
                :id => 'discount_150'
            )
    end

    def custom_coupon
        @custom_coupon ||= Stripe::Coupon.create(
                :amount_off => 42*100,
                :duration => 'forever',
                :currency => 'usd',
                :id => 'special_secret_coupon'
            )
    end

    def create_customer_with_default_source(owner)
        owner.update_stripe_card(stripe_helper.generate_card_token)
    end


    def create_for_owner_and_plan!(owner, stripe_plan_id, coupon_id = nil, trial_end_ts = nil, metadata = {})
        # bad things happen if an empty string is passed in
        coupon_id = nil if coupon_id.blank?
        Subscription.create_for_owner(owner: owner, stripe_plan_id: stripe_plan_id, coupon_id: coupon_id, trial_end_ts: trial_end_ts, metadata: metadata)
    end

    def create_subscription(owner, stripe_plan_id = default_plan.id, coupon_id = nil, metadata = {})
        owner.ensure_stripe_customer
        owner.update_stripe_card(stripe_helper.generate_card_token)
        create_for_owner_and_plan!(owner, stripe_plan_id, coupon_id, nil, metadata)
    end

    def stub_metered_invoice(owner)
        subscription = owner.primary_subscription || create_subscription(owner, metered_plan.id)
        raise "Not a metered plan" unless subscription.metered?
        amount = subscription.plan.amount * 42

        stub_invoice_with_charge(owner, {
            amount: amount,
            lines: [
                OpenStruct.new({
                    quantity: 42,
                    plan: subscription.plan,
                    amount: amount
                })
            ]
        })
    end

    def stub_invoice_with_charge(owner, invoice_params = {})
        owner.primary_subscription || create_subscription(owner, default_plan.id)
        amount = invoice_params[:amount] || 4200
        charge = Stripe::Charge.create(
            customer: owner.id,
            livemode: false,
            amount: amount,
            currency: 'usd',
            statement_descriptor: 'statement_descriptor'
        )

        invoice = stub_invoices(owner, [{
            amount: amount,
            charge: charge.id
        }.merge(invoice_params)]).first

        [invoice, charge]
    end

    def create_invoices(owner, invoice_params, stripe_subscription = nil, disable_subscription = false)
        # if you don't want this method to automatically create a subscription in
        # our db, then you can pass in a stripe subscription.  This allows testing
        # of the case where invoice events hit us when we have no subscription in our db
        if stripe_subscription.nil? && !disable_subscription
            subscription = owner.primary_subscription || create_subscription(owner)
            stripe_subscription = subscription.stripe_subscription
        end

        invoices = invoice_params.map do |params|
            Stripe::Invoice.create({
                customer: owner.id,
                subscription: stripe_subscription ? stripe_subscription.id : nil,
                number: '12349283',
                hosted_invoice_url: 'http://hosted_invoice_url',
                invoice_pdf: 'http://invoice_pdf'
            }.merge(params))
        end
        invoices
    end

    # It sucks to have to mock so much stuff here, but ruby-stripe-mock
    # does not make it easy for us to make the invoice we want.  See
    # also create_invoices above, where we just actually create one and
    # do not mock out so much stuff.
    #
    # It's possible that at this point things have
    # changed and we could use create_invoices in the places where we are
    # using stub_invoices now, but some of them are tricky.  For example, how
    # do we product a negative total for the test
    # "should not process invoices where credits were applied".
    def stub_invoices(owner, invoice_params, stripe_subscription = nil, disable_subscription = false)
        # if you don't want this method to automatically create a subscription in
        # our db, then you can pass in a stripe subscription.  This allows testing
        # of the case where invoice events hit us when we have no subscription in our db
        if stripe_subscription.nil? && !disable_subscription
            subscription = owner.primary_subscription || create_subscription(owner)
            stripe_subscription = subscription.stripe_subscription
        end

        invoices = invoice_params.map do |params|
            params = params.with_indifferent_access

            # by default, add a charge to the invoice
            total = params['total']
            if total && total > 0 && !params.key?(:charge)
                charge = Stripe::Charge.create(
                    customer: owner.id,
                    livemode: false,
                    amount: total,
                    currency: 'usd',
                    statement_descriptor: 'statement_descriptor'
                )
                params[:charge] = charge.id
            end

            invoice = Stripe::Invoice.create(
                customer: owner.id,
                subscription: stripe_subscription ? stripe_subscription.id : nil,
                number: '12349283',
                hosted_invoice_url: 'http://hosted_invoice_url',
                invoice_pdf: 'http://invoice_pdf',
                amount_paid: total || 0
            )
            json = invoice.as_json
            params.each do |key, value|
                allow(invoice).to receive(key).and_return(value)
                json[key.to_s] = value
            end
            allow(invoice).to receive(:as_json).and_return(json)
            allow(Stripe::Invoice).to receive(:retrieve).with(invoice.id).and_return(invoice)
            invoice
        end
        allow(Stripe::Invoice).to receive(:all).with(hash_including(:customer => owner.id)).and_return(invoices)
        invoice_list = Object.new
        allow(invoice_list).to receive(:data).and_return(invoices)
        allow(Stripe::Invoice).to receive(:list).with(hash_including(:customer => owner.id)).and_return(invoice_list)

        invoices
    end

end