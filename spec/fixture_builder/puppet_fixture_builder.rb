require Rails.root.join('spec', 'report_spec_helper')
require File.expand_path("./../is_back_royal_fixture_builder", __FILE__)

module FixtureBuilder
    class PuppetFixtureBuilder < Fixturies
        include FixtureBuilder::IsBackRoyalFixtureBuilder

        EASTERN_TIME = ActiveSupport::TimeZone['Eastern Time (US & Canada)']

        # Use `set_fixtures_directory` to tell Fixturies
        # where to put all of the fixtures files
        set_fixtures_directory Rails.root.join('spec', 'fixtures_puppet')

        # # useful for performance debugging
        # def with_timer(name)
        #     start = Time.now
        #     yield
        #     puts "#{(Time.now - start).round} seconds for #{name}"
        # end

        # we need all content to actually work in a browser
        def only_allow_frame_list_streams?
            true
        end

        def create_mba_user(email, cohort, streams, exam_stream, user_id = SecureRandom.uuid, career_profile_id = SecureRandom.uuid)
            mba_user = create_user({
                id: user_id,
                email: email,
                password: 'password',
                password_confirmation: 'password',
                has_seen_welcome: true,
                has_seen_accepted: true,
                has_seen_careers_welcome: true,
                can_edit_career_profile: true,
                program_type_confirmed: true,
                fallback_program_type: 'mba',
                phone: '+12163714694'
            })

            # must complete career profile before filling out application so
            # notification  preferences get set as expected
            complete_career_profile(mba_user, {
                'id' => career_profile_id,
                'professional_organizations' => ProfessionalOrganizationOption.reorder(:text).limit(5),
                'educational_organizations' => CareerProfile::EducationalOrganizationOption.reorder(:text).limit(5),
                'skills' => SkillsOption.reorder(:text).limit(5),
                'awards_and_interests' => CareerProfile::AwardsAndInterestsOption.reorder(:text).limit(5)
            })

            create_cohort_application_for_user(cohort, 'accepted', mba_user)
            mba_user.add_to_group('CASPER_MBA')
            mba_user.reload
            avatar_url = 'https://uploads-development.smart.ly/fixture_assets/avatar_1.png'
            User.connection.execute("UPDATE users SET avatar_url = '#{avatar_url}' where id = '#{mba_user.id}'")

            # because of the way the user progress cache works and the way
            # we mock out the exam timing and stuff, these all need to exist
            # before we load the page.
            Lesson::StreamProgress.create_or_update!({
                :user_id => mba_user.id,
                :locale_pack_id => exam_stream.locale_pack_id
            })
            LessonProgress.create_or_update!({
                :user_id => mba_user.id,
                :locale_pack_id => exam_stream.lessons[0].locale_pack_id
            })
            LessonProgress.create_or_update!({
                :user_id => mba_user.id,
                :locale_pack_id => exam_stream.lessons[1].locale_pack_id
            })

            # for consistency in recent streams UI, we need to fix
            # the time when these streams were last done
            complete_stream(streams[0], mba_user, {last_progress_at: EASTERN_TIME.parse('2017/06/20 12:00'), completed_at: EASTERN_TIME.parse('2017/06/20 14:00')})
            start_stream(streams[1], mba_user, {last_progress_at: EASTERN_TIME.parse('2017/01/01 12:00') - 20.seconds})

            mba_user
        end

        build do
            # the client only accepts frame_list lessons
            playlists, streams = create_frame_list_playlists('CASPER_MBA', {
                stream_ids: ["dc6d936c-5052-4d89-b474-576ced0e41e6", "b835df0a-c9b9-43b8-8e60-41d93323ccb0", "d9a3385d-2230-4411-b50d-ca8a1e28965f"]
            })

            # add a coming_soon lesson to the unstarted stream
            streams[2].chapters[0]['lesson_hashes'].last['coming_soon'] = true
            streams[2].publish!

            cohort = FactoryBot.create(:cohort, {
                start_date: '2016/06/28 12:00:00 UTC',
                end_date: '2016/09/28 12:00:00 UTC',
                name: 'casper mba',
                title: 'casper mba',
                program_type: 'mba',
                graduation_days_offset_from_end: 20
            })

            required_playlist_pack_ids = playlists.map(&:locale_pack_id)
            cohort.playlist_collections = [{title: '', required_playlist_pack_ids: required_playlist_pack_ids}]

            # one exam that mba_user has started
            exam_stream = create_frame_list_stream('CASPER_MBA exam', {
                id: "45493f54-97d5-4508-945e-9d8e4b15453c",
                exam: true,
                time_limit_hours: 48
            })
            exam_stream.lessons.each { |lesson| lesson.test = true; lesson.publish! }

            # one exam that mba_user has not started
            exam_stream_2 = create_frame_list_stream('CASPER_MBA exam 2', {
                id: "456ec52c-dabd-43a9-874f-3d61816b43b8",
                exam: true,
                time_limit_hours: 48
            })
            exam_stream_2.lessons.each { |lesson| lesson.test = true; lesson.publish! }
            cohort.periods = [
                {
                    style: 'exam',
                    stream_entries: [{
                        locale_pack_id: exam_stream.locale_pack_id,
                        required: true
                    },
                    {
                        locale_pack_id: exam_stream_2.locale_pack_id,
                        required: true
                    }],
                    days_offset: 0,
                    days: 7
                },
                {
                    style: 'project',
                    project_style: 'casper_capstone',
                    learner_project_ids: [@learner_project.id, @another_learner_project.id],
                    stream_entries: [{
                        locale_pack_id: streams[0].locale_pack_id,
                        required: true
                    }],
                    days_offset: 0,
                    days: 7
                },
            ]
            cohort.program_guide_url  = 'http://example.com'
            cohort.save!
            cohort.add_to_group('CASPER_MBA')
            cohort.publish!

            ########### mba_user@pedago.com (shared among many specs)

            # We hardcode the id for this user because some casper specs depend on it
            mba_user = create_mba_user('mba_user@pedago.com', cohort, streams, exam_stream, 'f2f9358e-ac20-4753-83b8-e01078a3f6aa', '0d101f0c-1c50-411a-b5c1-4b60f88cefe9')

            ########### mba_user_stream_dashboard@pedago.com (stream_dashboard_screenshot_spec.js)

            create_mba_user('mba_user_stream_dashboard@pedago.com', cohort, streams, exam_stream)

            ########### graduated_mba@pedago.com
            graduated_mba = create_cohort_application_for_user(cohort, 'accepted', nil, 'graduated')
            graduated_mba.add_to_group('CASPER_MBA')
            graduated_mba.reload
            graduated_mba.update({
                active_playlist_locale_pack_id: playlists[0].locale_pack_id,
                email: 'graduated_mba@pedago.com',
                password: 'password',
                password_confirmation: 'password',
                has_seen_welcome: true,
                has_seen_accepted: true,
                can_edit_career_profile: true,
                fallback_program_type: 'mba',
                phone: '+12163714695'
            })
            avatar_url = 'https://uploads-development.smart.ly/fixture_assets/avatar_1.png'
            User.connection.execute("UPDATE users SET avatar_url = '#{avatar_url}' where id = '#{graduated_mba.id}'")

            complete_stream(streams[0], graduated_mba)
            start_stream(streams[1], graduated_mba)

            # get 5 to test limits in the UI
            complete_career_profile(graduated_mba, {
                'professional_organizations' => ProfessionalOrganizationOption.reorder(:text).limit(5),
                'educational_organizations' => CareerProfile::EducationalOrganizationOption.reorder(:text).limit(5),
                'skills' => SkillsOption.reorder(:text).limit(5),
                'awards_and_interests' => CareerProfile::AwardsAndInterestsOption.reorder(:text).limit(5)
            })

            ############# applied_mba_user@pedago.com
            applied = create_cohort_application_for_user(cohort, 'pending')
            applied.add_to_group('CASPER_MBA')
            applied.save!
            applied.update({
                active_playlist_locale_pack_id: playlists[0].locale_pack_id,
                email: 'applied_mba_user@pedago.com',
                password: 'password',
                password_confirmation: 'password',
                has_seen_welcome: true,
                has_seen_accepted: true,
                fallback_program_type: 'mba',
                program_type_confirmed: true,
            })

            ############# front_royal_store_user@pedago.com
            front_royal_store_user = create_user({
                id: 'ad497487-da26-45e5-b4ab-5066c647af15',
                active_playlist_locale_pack_id: playlists[0].locale_pack_id,
                email: 'front_royal_store_user@pedago.com',
                password: 'password',
                password_confirmation: 'password',
                has_seen_welcome: true,
                has_seen_accepted: true,
                fallback_program_type: 'mba',
                program_type_confirmed: true,
                enable_front_royal_store: true
            })
            front_royal_store_user =  create_cohort_application_for_user(cohort, 'pending', front_royal_store_user)
            front_royal_store_user.add_to_group('CASPER_MBA')
            front_royal_store_user.save!

            ############# user_with_subscriptions_enabled@pedago.com
            user_with_subscriptions_enabled = create_user_with_subscriptions_enabled
            user_with_subscriptions_enabled.add_to_group('CASPER_MBA')


            # FIXME: I tried to use this guy to test the student dashboard rejected state, but
            # it didn't work, I think because we are defining multiple promoted cohorts in here,
            # and the casper cohort is not winning
            # ############# rejected_mba_user@pedago.com
            # rejected_mba_user = create_cohort_application_for_user(cohort, 'rejected')
            # rejected_mba_user.add_to_group('CASPER_MBA')
            # rejected_mba_user.save!
            # rejected_mba_user.update({
            #     active_playlist_locale_pack_id: playlists[0].locale_pack_id,
            #     email: 'rejected_mba_user@pedago.com',
            #     password: 'password',
            #     password_confirmation: 'password',
            #     has_seen_welcome: true,
            #     has_seen_accepted: true,
            #     program_type_confirmed: true
            # })


            ############# external_inst_user_with_playlists@pedago.com
            institution = FactoryBot.create(:institution)
            external_inst_user_with_playlists = FactoryBot.create(:user)

            AccessGroup.create(name: 'INST_WITH_PLAYLISTS')
            institution.add_to_group('INST_WITH_PLAYLISTS')
            playlists, streams = create_frame_list_playlists('INST_WITH_PLAYLISTS')
            institution.playlist_pack_ids = playlists.map(&:locale_pack_id)
            external_inst_user_with_playlists.institutions << institution
            external_inst_user_with_playlists.active_institution = institution
            external_inst_user_with_playlists.update({
                email: 'external_inst_user_with_playlists@pedago.com',
                password: 'password',
                password_confirmation: 'password',
                active_playlist_locale_pack_id: playlists[0].locale_pack_id
            })


            ############# external_inst_user_without_playlists@pedago.com
            institution = FactoryBot.create(:institution)

            AccessGroup.create(name: 'INST_WITHOUT_PLAYLISTS')
            institution.add_to_group('INST_WITHOUT_PLAYLISTS')
            stream = create_frame_list_stream('INST WOUT PLAYLISTS')
            stream.locale_pack.add_to_group('INST_WITHOUT_PLAYLISTS')
            inst_user_without_playlists = FactoryBot.create(:user)
            inst_user_without_playlists.update({
                email: 'external_inst_user_without_playlists@pedago.com',
                password: 'password',
                password_confirmation: 'password'
            })
            inst_user_without_playlists.institutions << institution
            inst_user_without_playlists.active_institution = institution
            inst_user_without_playlists.bookmark_stream_locale_pack(stream.locale_pack)

            ########### phone user

            FactoryBot.create(:user, {
                uid: SecureRandom.uuid,
                provider: 'phone_no_password',
                phone: '+12163714695'
            })

            ########### hiring_manager@pedago.com

            hiring_manager = FactoryBot.create(:user, {
                id: 'd9e61f9e-097e-4b08-b9c6-01dc9eac8584',
                email: 'hiring_manager@pedago.com',
                password: 'password',
                password_confirmation: 'password',
                has_seen_welcome: true,
                job_title: 'bossman',
                phone: '+12163714691',
                name: "Bossmane",
                nickname: "Bossmane",
                has_seen_hiring_tour: true,

                # if we don't set this initially, then it will change automatically in specs and make things dependent
                # on the order in which specs run
                has_seen_featured_positions_page: true
            })
            hiring_manager.professional_organization = ProfessionalOrganizationOption.reorder(:text).first
            hiring_manager.hiring_team = HiringTeam.create!(title: 'Casper Team', subscription_required: true, owner_id: hiring_manager.id, hiring_plan: HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING)
            hiring_manager.save!
            avatar_url = 'https://uploads-development.smart.ly/fixture_assets/avatar_1.png'
            User.connection.execute("UPDATE users SET avatar_url = '#{avatar_url}' where id = '#{hiring_manager.id}'")

            subscription = Subscription.new(
                stripe_plan_id: 'hiring_per_match_20',
                stripe_subscription_id: 'fake_sub_for_hiring_manager@pedago.com',
                past_due: false,
                stripe_product_id: 'fake',
            )
            subscription.hiring_team = hiring_manager.hiring_team
            subscription.save!

            HiringApplication.create!(
                user_id: hiring_manager.id,
                status: 'accepted',
                place_id: '123',
                place_details: {
                    locality: {
                        long: "Bossmane's City"
                    },
                    administrative_area_level_1: {
                        short: "ZZ"
                    },
                    formatted_address: '1234 Main St'
                },
                company_year: 1942,

                company_employee_count: '5000_plus',
                company_annual_revenue: '1_to_5_million',
                company_sells_recruiting_services: true,
                website_url: 'http://example.com',
                industry: 'consumer_products',
                funding: 'seed',

                # Team
                hiring_for: 'own_team',
                team_name: 'Sales & Marketing',
                team_mission: 'asdf asdfsad fasdfsadfsadfas',
                role_descriptors: ["marketing", "legal", "general_management", "product_management"],

                # You
                job_role: "senior_management",
                fun_fact: "asdfsadfasdfasdfas",

                # Candidate Preferences
                where_descriptors: ["relocate"],
                position_descriptors: ["part_time"],
                salary_ranges: ["any"],
                has_seen_welcome: true
            )

            hiring_manager.candidate_relationships.destroy_all

            # 2 pending relationships
            another_candidate = create_candidate(id: '71b5a66f-1605-4e2e-960a-5db69c93192e')
            hiring_manager.ensure_candidate_relationships([mba_user.id, another_candidate.id])
            # ensure that the another_candidate relationship is the first to show up on the featured page
            hiring_manager.candidate_relationships.where(candidate_id: another_candidate.id).first.update!(hiring_manager_priority: 0)
            hiring_manager.candidate_relationships.where(candidate_id: mba_user.id).first.update!(hiring_manager_priority: 1)


            # rejected by hiring manager
            another_candidate = create_candidate(id: 'b4e1712d-afc0-4522-bae5-1119fe23c193')
            hiring_manager.ensure_candidate_relationships([another_candidate.id])
            hiring_manager.candidate_relationships.where(candidate_id: another_candidate.id).first.update!(hiring_manager_status: "rejected", updated_at: EASTERN_TIME.parse('2017/06/01 12:00:00'))

            # waiting on candidate
            another_candidate = create_candidate(id: 'b8518df5-bca0-4917-8cf9-c43ecdd57164')
            hiring_manager.ensure_candidate_relationships([another_candidate.id])
            hiring_manager.candidate_relationships.where(candidate_id: another_candidate.id).first.update!(hiring_manager_status: "accepted", candidate_status: 'pending', updated_at: EASTERN_TIME.parse('2017/06/01 12:01:00'))

            # saved_for_later
            another_candidate = create_candidate(id: 'a5de6d23-03f4-48eb-bf95-22e511bf1fbd')
            hiring_manager.ensure_candidate_relationships([another_candidate.id])
            hiring_manager.candidate_relationships.where(candidate_id: another_candidate.id).first.update!(hiring_manager_status: "saved_for_later", candidate_status: 'hidden', updated_at: EASTERN_TIME.parse('2017/06/01 12:01:01'))


            # invited, waiting on candidate
            another_candidate = create_candidate(id: '11e64713-8fb6-4f2d-8c92-74c3cbe553a3')
            hiring_manager.ensure_candidate_relationships([another_candidate.id])
            hiring_relationship = hiring_manager.candidate_relationships.where(candidate_id: another_candidate.id).first

            created_at = EASTERN_TIME.parse('2018/08/04')
            fish_tank_diver = OpenPosition.create_or_update_from_hash!({
                id: '9358610b-c2db-4ef0-99be-422509040b02', # needs to be hardcoded because we need to know the id for screenshot specs
                title: 'Fish tank diver',
                hiring_manager_id: hiring_manager.id,
                salary: 42000,
                available_interview_times: 'Tuesday at midnight',
                last_invitation_at: Time.now - 10.seconds,
                featured: true,
                job_post_url: 'http://example.com',
                role: 'research',
                place_id: "ChIJoyh8pLf7MIgRlIwC3eq9pFM",
                place_details:
                    {"locality"=>{"short"=>"Cleveland Heights", "long"=>"Cleveland Heights"},
                    "administrative_area_level_2"=>{"short"=>"Cuyahoga County", "long"=>"Cuyahoga County"},
                    "administrative_area_level_1"=>{"short"=>"OH", "long"=>"Ohio"},
                    "country"=>{"short"=>"US", "long"=>"United States"},
                    "formatted_address"=>"Cleveland Heights, OH, USA",
                },
                short_description: 'Dive to the murky depths of fish tanks around the world in the name of research and to recover hidden gems and riches.',
                description: 'As a fish tank diver, you\'ll be resonsible for leading (relatively) deep underwater exploration missions to research the everyday habits and routines of your average domesticated Nemo. In addition, the recovery of lost treasure buried under 10 - 15 gallons of tap water and a thin layer of pebbles is of paramount importance. You\'ll also have the pleasure of working alongside world renown sailor, Captain Ahab, and notoriously evasive and inebriate pirate, Captain Jack Sparrow, as you learn the ins and outs of the trade.',
                position_descriptors: ['permanent'],
                auto_expiration_date: created_at + 60.days
            })
            fish_tank_diver.update_column(:updated_at, EASTERN_TIME.parse('2017/06/01 12:01:02'))
            fish_tank_diver.update_column(:created_at, created_at)

            toenail_clipper = OpenPosition.create_or_update_from_hash!({
                title: 'Toenail clipper',
                hiring_manager_id: hiring_manager.id,
                salary: 42000,
                available_interview_times: 'Tuesday at midnight',
                last_invitation_at: Time.now, # make sure this is the default on the invite to interview form
                featured: true,
                auto_expiration_date: created_at + 60.days
            })
            toenail_clipper.update_column(:updated_at, EASTERN_TIME.parse('2017/06/01 12:01:02'))
            toenail_clipper.update_column(:created_at, created_at)

            featured_positions = [fish_tank_diver, toenail_clipper]

            hiring_relationship.update!(
                hiring_manager_status: "accepted",
                candidate_status: 'pending',
                updated_at: EASTERN_TIME.parse('2017/06/01 12:01:02'),
                open_position_id: featured_positions[0].id
            )

            # positions page: unreviewed candidate
            create_candidate_position_interest(create_candidate, featured_positions[0], {
                candidate_status: 'accepted',
                hiring_manager_status: 'pending' # if it was unseen then it would change once it was looked at, messing up data for later specs
            })

            # positions page: liked candidate
            liked_from_featured_positions = create_candidate
            create_candidate_position_interest(liked_from_featured_positions, featured_positions[0], {
                candidate_status: 'accepted',
                hiring_manager_status: 'accepted'
            })
            hiring_manager.ensure_candidate_relationships([liked_from_featured_positions.id])
            hiring_manager.candidate_relationships.where(candidate_id: liked_from_featured_positions.id).first.update!(
                hiring_manager_status: "accepted",
                updated_at: EASTERN_TIME.parse('2017/06/01 12:03:00')
            )

            # positions page: saved candidate
            saved_from_featured_positions = create_candidate
            create_candidate_position_interest(saved_from_featured_positions, featured_positions[0], {
                candidate_status: 'accepted',
                hiring_manager_status: 'saved_for_later'
            })
            hiring_manager.ensure_candidate_relationships([saved_from_featured_positions.id])
            hiring_manager.candidate_relationships.where(candidate_id: saved_from_featured_positions.id).first.update!(
                hiring_manager_status: "saved_for_later",
                updated_at: EASTERN_TIME.parse('2017/06/01 12:03:00')
            )

            # positions page: hidden candidate
            hidden_from_featured_positions = create_candidate
            create_candidate_position_interest(hidden_from_featured_positions, featured_positions[0], {
                candidate_status: 'accepted',
                hiring_manager_status: 'rejected'
            })
            hiring_manager.ensure_candidate_relationships([hidden_from_featured_positions.id])
            hiring_manager.candidate_relationships.where(candidate_id: hidden_from_featured_positions.id).first.update!(
                hiring_manager_status: "rejected",
                updated_at: EASTERN_TIME.parse('2017/06/01 12:03:00')
            )

            # positions page: invited to position
            invited_from_featured_positions = create_candidate
            candidate_position_interest = create_candidate_position_interest(invited_from_featured_positions, featured_positions[0], {
                candidate_status: 'accepted',
                hiring_manager_status: 'invited'
            })
            invite_to_interview_and_match(hiring_manager, invited_from_featured_positions, candidate_position_interest)

            # Make sure Toenail Clipper has something to keep its ordering on the positions
            # page from changing with Teammate position
            create_candidate_position_interest(create_candidate, featured_positions[1], {
                candidate_status: 'accepted',
                hiring_manager_status: 'pending' # if it was unseen that it would change once it was looked at, messing up data for later specs
            })

            # rejected by candidate
            another_candidate = create_candidate(id: 'ec739ea3-7810-41e0-b69f-6f2212644d05')
            hiring_manager.ensure_candidate_relationships([another_candidate.id])
            hiring_manager.candidate_relationships.where(candidate_id: another_candidate.id).first.update!(hiring_manager_status: "accepted", candidate_status: 'rejected', updated_at: EASTERN_TIME.parse('2017/06/01 12:02:00'))

            # liked and matched
            another_candidate = create_candidate(id: '77fd35f8-5729-4062-9331-c9cd39a410f8')
            hiring_manager.ensure_candidate_relationships([another_candidate.id])
            hiring_manager.candidate_relationships.where(candidate_id: another_candidate.id).first.update!(
                hiring_manager_status: "accepted",
                candidate_status: 'accepted',
                updated_at: EASTERN_TIME.parse('2017/06/01 12:03:00')
            )

            # invite to inteview, match, conversation
            invited_candidate = create_candidate(id: '807f3686-45dd-43a7-b9a9-dae04ec610d7')
            invited_candidate.update_attribute(:email, "invited_candidate@pedago.com")

            # accepte one position, reject another, and a third is pending
            # note that this does not use any of hiring_manager@pedago.com's positions.  This is just used for screenshotting the client UI I guess
            candidate_position_interest = create_candidate_position_interest(invited_candidate, featured_positions[0], {candidate_status: 'accepted'})
            create_candidate_position_interest(invited_candidate, featured_positions[1], {candidate_status: 'rejected'})

            invite_to_interview_and_match(hiring_manager, invited_candidate, candidate_position_interest)
            identify(invited_candidate, "invited_candidate")

            # closed by hiring manager (with no messages)
            another_candidate = create_candidate(id: '134c00b8-5446-40f3-8feb-823f13c9b934')
            hiring_manager.ensure_candidate_relationships([another_candidate.id])
            hiring_manager.candidate_relationships.where(candidate_id: another_candidate.id).first.update!(hiring_manager_status: "accepted", candidate_status: 'accepted', hiring_manager_closed: true, updated_at: EASTERN_TIME.parse('2017/06/01 12:05:00'))

            # closed by candidate (with messages)
            closed_by_candidate = create_candidate(id: 'de657dd9-25fd-48a2-933a-c062fe8ba3ef')
            closed_by_candidate.update_attribute(:email, "closed_by_candidate@pedago.com")
            hiring_manager.ensure_candidate_relationships([closed_by_candidate.id])
            hiring_relationship = hiring_manager.candidate_relationships.where(candidate_id:closed_by_candidate.id).first

            conversation_json = {
                subject: "new subject",
                messages: [{
                    "body" => "Hey what's up.",
                    "sender_id" => closed_by_candidate.id,
                    "sender_type" => 'user'
                }],
                recipients: [
                    {
                        'type' => 'user',
                        'id' => hiring_manager.id
                    },
                    {
                        'type' => 'user',
                        'id' => closed_by_candidate.id
                    }]
            }
            conversation = hiring_relationship.conversation = Mailboxer::Conversation.create_or_update_from_hash!(conversation_json, closed_by_candidate, hiring_relationship)
            hiring_relationship.update!(hiring_manager_status: "accepted", candidate_status: 'accepted',updated_at: EASTERN_TIME.parse('2017/06/01 12:07:00'))

            # mark all as read because otherwise specs will mark them as read and things will change from spec to spec
            i = 1
            conversation.messages.reorder(:created_at).each { |m| m.update(created_at: EASTERN_TIME.parse("2017/06/01 12:07:0#{i}"), updated_at: EASTERN_TIME.parse("2017/06/01 12:07:0#{i}")); i += 1 }
            conversation.messages.map(&:receipts).flatten.sort_by(&:created_at).each { |r| r.update(is_read: true, created_at: EASTERN_TIME.parse("2017/06/01 12:07:0#{i}"), updated_at: EASTERN_TIME.parse('2017/06/01 12:07:0#{i}')); i += 1 }
            hiring_relationship.assign_attributes(candidate_closed: true, updated_at: EASTERN_TIME.parse("2017/06/01 12:07:0#{i}"))
            hiring_relationship.save!(touch: false)
            identify(closed_by_candidate, "closed_by_candidate")

            # match no messages (this state is no longer possible, since a
            #  candidate has to start a conversation when they accept a request,
            #  but I don't feel like dealing with this specs that break if I remove it)
            another_candidate = create_candidate(id: 'b1558567-7bc6-414a-835b-1f51619da961')
            hiring_manager.ensure_candidate_relationships([another_candidate.id])
            hiring_manager.candidate_relationships.where(candidate_id: another_candidate.id).first.update!(hiring_manager_status: "accepted", candidate_status: 'accepted', updated_at: EASTERN_TIME.parse('2017/06/01 12:08:00'))

            ########### hiring_manager_unpaid@pedago.com
            hiring_manager_unpaid = create_and_accept_hiring_manager({email: 'hiring_manager_unpaid@pedago.com'})
            hiring_manager_unpaid.hiring_team.update({
                subscription_required: true,
                owner_id: hiring_manager_unpaid.id,
                hiring_plan: HiringTeam::HIRING_PLAN_UNLIMITED_WITH_SOURCING
            })

            created_at = EASTERN_TIME.parse('2017/08/04')
            position = OpenPosition.create_or_update_from_hash!({
                title: 'Work for me!!!!!!',
                hiring_manager_id: hiring_manager_unpaid.id,
                salary: 42000,
                available_interview_times: 'Tuesday at midnight',
                last_invitation_at: Time.now, # make sure this is the default on the invite to interview form
                featured: true,
                auto_expiration_date: created_at + 60.days
            })
            position.update_column(:created_at, created_at) # push this to the bottom so it does not change existing casper specs

            # re-use some candidates from above
            candidates = User.find(['134c00b8-5446-40f3-8feb-823f13c9b934', 'b1558567-7bc6-414a-835b-1f51619da961', 'de657dd9-25fd-48a2-933a-c062fe8ba3ef', '807f3686-45dd-43a7-b9a9-dae04ec610d7', '77fd35f8-5729-4062-9331-c9cd39a410f8'])
                candidates.each_with_index do |candidate, j|
                    create_candidate_position_interest(candidate, position, {
                        candidate_status: 'accepted',
                        hiring_manager_status: 'pending',
                        admin_status: 'reviewed',
                        hiring_manager_priority: j
                    })
            end

            ########### hiring_manager_pending@pedago.com
            create_unaccepted_hiring_manager({
                email: 'hiring_manager_pending@pedago.com'
            })

            ########### hiring_manager_past_due@pedago.com
            hiring_manager_past_due =  create_and_accept_hiring_manager({email: 'hiring_manager_past_due@pedago.com'})
            hiring_manager_past_due.hiring_team.update(subscription_required: true)

            # instantiate a subscription without an associated stripe subscription
            subscription = Subscription.new(
                stripe_plan_id: 'hiring_per_match_20',
                stripe_subscription_id: 'fake_sub_for_hiring_manager_past_due',
                past_due: true,
                stripe_product_id: 'fake',
            )
            subscription.hiring_team = hiring_manager_past_due.hiring_team
            subscription.save!

            ########### hiring_teammate@pedago.com
            hiring_teammate = FactoryBot.create(:user, {
                email: 'hiring_teammate@pedago.com',
                password: 'password',
                password_confirmation: 'password',
                has_seen_welcome: true,
                job_title: 'teammate',
                phone: '+12163714691',
                name: "teammate",
                nickname: "teammate",
            })
            hiring_teammate.professional_organization = ProfessionalOrganizationOption.reorder(:text).first
            hiring_teammate.hiring_team = hiring_manager.hiring_team
            hiring_teammate.save!
            avatar_url = 'https://uploads-development.smart.ly/fixture_assets/avatar_1.png'
            User.connection.execute("UPDATE users SET avatar_url = '#{avatar_url}' where id = '#{hiring_teammate.id}'")

            HiringApplication.create!(
                user_id: hiring_teammate.id,
                status: 'accepted',
                place_id: '123',
                place_details: {
                    locality: {
                        long: "Bossmane's City"
                    },
                    administrative_area_level_1: {
                        short: "ZZ"
                    },
                    formatted_address: '1234 Main St'
                },
                company_year: 1942,

                company_employee_count: '5000_plus',
                company_annual_revenue: '1_to_5_million',
                company_sells_recruiting_services: true,
                website_url: 'http://example.com',
                industry: 'consumer_products',
                funding: 'seed',

                # Team
                hiring_for: 'own_team',
                team_name: 'Sales & Marketing',
                team_mission: 'asdf asdfsad fasdfsadfsadfas',
                role_descriptors: ["marketing", "legal", "general_management", "product_management"],

                # You
                job_role: "senior_management",
                fun_fact: "asdfsadfasdfasdfas",

                # Candidate Preferences
                where_descriptors: ["relocate"],
                position_descriptors: ["part_time"],
                salary_ranges: ["any"],
                has_seen_welcome: true
            )

            hiring_teammate.candidate_relationships.destroy_all

            # rejected by hiring manager
            another_candidate = create_candidate(id: 'd592e0cf-9a50-4cce-a946-e2d21b920c0b')
            hiring_teammate.ensure_candidate_relationships([another_candidate.id])
            hiring_teammate.candidate_relationships.where(candidate_id: another_candidate.id).first.update!(hiring_manager_status: "rejected", updated_at: EASTERN_TIME.parse('2017/06/01 12:00:00'))

            # waiting on candidate
            another_candidate = create_candidate(id: '25ce613d-fb10-494b-8903-0db0d952800a')
            hiring_teammate.ensure_candidate_relationships([another_candidate.id])
            hiring_teammate.candidate_relationships.where(candidate_id: another_candidate.id).first.update!(hiring_manager_status: "accepted", candidate_status: 'pending', updated_at: EASTERN_TIME.parse('2017/06/01 12:01:00'))

            # saved_for_later
            another_candidate = create_candidate(id: 'a4e073fa-53d8-4cf3-9bab-b5c100c29048')
            hiring_teammate.ensure_candidate_relationships([another_candidate.id])
            hiring_teammate.candidate_relationships.where(candidate_id: another_candidate.id).first.update!(hiring_manager_status: "saved_for_later", candidate_status: 'hidden', updated_at: EASTERN_TIME.parse('2017/06/01 12:01:01'))

            another_saved_candidate = create_candidate(id: '00061c11-6c30-4759-8506-b882743f46b4')
            hiring_teammate.ensure_candidate_relationships([another_saved_candidate.id])
            hiring_teammate.candidate_relationships.where(candidate_id: another_saved_candidate.id).first.update!(hiring_manager_status: "saved_for_later", candidate_status: 'hidden', updated_at: EASTERN_TIME.parse('2017/06/01 12:01:01'))


            # invited, waiting on candidate
            another_candidate = create_candidate(id: '34b2cf3f-63ab-4f34-9809-310688a59f15')
            hiring_teammate.ensure_candidate_relationships([another_candidate.id])
            hiring_relationship = hiring_teammate.candidate_relationships.where(candidate_id: another_candidate.id).first

            created_at = EASTERN_TIME.parse('2018/08/04')
            open_position = OpenPosition.create_or_update_from_hash!({
                title: 'Teammate position',
                hiring_manager_id: hiring_teammate.id,
                salary: 42000,
                available_interview_times: 'Tuesday at midnight',
                last_invitation_at: Time.now,
                featured: true,
                auto_expiration_date: created_at + 60.days
            })
            open_position.update_column(:updated_at, EASTERN_TIME.parse('2017/06/01 12:01:02'))
            open_position.update_column(:created_at, created_at)

            hiring_relationship.update!(
                hiring_manager_status: "accepted",
                candidate_status: 'pending',
                updated_at: EASTERN_TIME.parse('2017/06/01 12:01:02'),
                open_position_id: open_position.id
            )

            # rejected by candidate
            another_candidate = create_candidate(id: '38de1e3a-41bd-4df9-be40-10ab55bf49cd')
            hiring_teammate.ensure_candidate_relationships([another_candidate.id])
            hiring_teammate.candidate_relationships.where(candidate_id: another_candidate.id).first.update!(hiring_manager_status: "accepted", candidate_status: 'rejected', updated_at: EASTERN_TIME.parse('2017/06/01 12:02:00'))

            # liked and matched
            another_candidate = create_candidate(id: 'cb2dfd44-b370-41f4-8af6-84c81e10e8f0')
            hiring_teammate.ensure_candidate_relationships([another_candidate.id])
            hiring_teammate.candidate_relationships.where(candidate_id: another_candidate.id).first.update!(
                hiring_manager_status: "accepted",
                candidate_status: 'accepted',
                updated_at: EASTERN_TIME.parse('2017/06/01 12:03:00')
            )

            # invite to inteview, match, conversation
            invited_candidate = create_candidate(id: '3ac3fd70-08cc-41cd-a103-cd641197fca6')
            invited_candidate.update_attribute(:email, "invited_by_teammate_candidate@pedago.com")
            hiring_teammate.ensure_candidate_relationships([invited_candidate.id])
            hiring_relationship = hiring_teammate.candidate_relationships.where(candidate_id: invited_candidate.id).first

            conversation_json = {
                subject: "new subject",
                messages: [{
                    "body" => "You should come dive in our fish tank.  Our fish are hungry.",
                    "sender_id" => hiring_teammate.id,
                    "sender_type" => 'user',
                    "metadata" => {
                        senderCompanyName: hiring_teammate.hiring_application.company_name,
                        openPosition: open_position.as_json
                    }
                }],
                recipients: [
                    {
                        'type' => 'user',
                        'id' => hiring_teammate.id
                    },
                    {
                        'type' => 'user',
                        'id' => invited_candidate.id
                    }]
            }
            conversation = hiring_relationship.conversation = Mailboxer::Conversation.create_or_update_from_hash!(conversation_json, hiring_teammate, hiring_relationship)
            hiring_relationship.update!(
                hiring_manager_status: "accepted",
                candidate_status: 'accepted',
                open_position_id: open_position.id,
                updated_at: EASTERN_TIME.parse('2017/06/01 12:04:00')
            )
            hiring_relationship.save!(touch: false)
            conversation_json = conversation.as_json
            conversation_json['messages'] << {
                "body" => "Sounds great!.  I'm in",
                "sender_id" => invited_candidate.id,
                "sender_type" => 'user'
            }
            conversation = Mailboxer::Conversation.create_or_update_from_hash!(conversation_json, invited_candidate, hiring_relationship)
            conversation_json = conversation.as_json
            conversation_json['messages'] << {
                "body" => "Awesome.  You're hired!",
                "sender_id" => hiring_teammate.id,
                "sender_type" => 'user'
            }

            conversation = Mailboxer::Conversation.create_or_update_from_hash!(conversation_json, hiring_teammate, hiring_relationship)
            # mark all as read because otherwise specs will mark them as read and things will change from spec to spec
            i = 1
            conversation.messages.reorder(:created_at).each { |m| m.update(created_at: EASTERN_TIME.parse("2017/06/01 12:04:0#{i}"), updated_at: EASTERN_TIME.parse("2017/06/01 12:04:0#{i}")); i += 1 }
            conversation.messages.map(&:receipts).flatten.sort_by(&:created_at).each { |r| r.update(is_read: true, created_at: EASTERN_TIME.parse("2017/06/01 12:04:0#{i}"), updated_at: EASTERN_TIME.parse("2017/06/01 12:04:0#{i}")); i += 1 }

            # closed by hiring manager (with no messages)
            another_candidate = create_candidate(id: '233f7d5d-68e3-473d-a8ff-139fbc1823a7')
            hiring_teammate.ensure_candidate_relationships([another_candidate.id])
            hiring_teammate.candidate_relationships.where(candidate_id: another_candidate.id).first.update!(hiring_manager_status: "accepted", candidate_status: 'accepted', hiring_manager_closed: true, updated_at: EASTERN_TIME.parse('2017/06/01 12:05:00'))

            # closed by candidate (with messages)
            closed_by_candidate = create_candidate(id: '6f03a3cf-d5cd-4d98-8d80-50848835babb')
            closed_by_candidate.update_attribute(:email, "teammate_closed_by_candidate@pedago.com")
            hiring_teammate.ensure_candidate_relationships([closed_by_candidate.id])
            hiring_relationship = hiring_teammate.candidate_relationships.where(candidate_id:closed_by_candidate.id).first

            conversation_json = {
                subject: "new subject",
                messages: [{
                    "body" => "Hey what's up.",
                    "sender_id" => closed_by_candidate.id,
                    "sender_type" => 'user'
                }],
                recipients: [
                    {
                        'type' => 'user',
                        'id' => hiring_teammate.id
                    },
                    {
                        'type' => 'user',
                        'id' => closed_by_candidate.id
                    }]
            }
            conversation = hiring_relationship.conversation = Mailboxer::Conversation.create_or_update_from_hash!(conversation_json, closed_by_candidate, hiring_relationship)
            hiring_relationship.update!(hiring_manager_status: "accepted", candidate_status: 'accepted',updated_at: EASTERN_TIME.parse('2017/06/01 12:07:00'))
            # mark all as read because otherwise specs will mark them as read and things will change from spec to spec
            i = 1
            conversation.messages.reorder(:created_at).each { |m| m.update(created_at: EASTERN_TIME.parse("2017/06/01 12:07:0#{i}"), updated_at: EASTERN_TIME.parse("2017/06/01 12:07:0#{i}")); i += 1 }
            conversation.messages.map(&:receipts).flatten.sort_by(&:created_at).each { |r| r.update(is_read: true, created_at: EASTERN_TIME.parse("2017/06/01 12:07:0#{i}"), updated_at: EASTERN_TIME.parse("2017/06/01 12:07:0#{i}")); i += 1 }
            hiring_relationship.assign_attributes(candidate_closed: true, updated_at: EASTERN_TIME.parse("2017/06/01 12:07:0#{i}"))
            hiring_relationship.save!(touch: false)

            # match no messages (this state is no longer possible, since a
            #  candidate has to start a conversation when they accept a request,
            #  but I don't feel like dealing with this specs that break if I remove it)
            another_candidate = create_candidate(id: '7214dd3a-c5a3-4c60-bc39-3470831fd016')
            hiring_teammate.ensure_candidate_relationships([another_candidate.id])
            hiring_teammate.candidate_relationships.where(candidate_id: another_candidate.id).first.update!(hiring_manager_status: "accepted", candidate_status: 'accepted', updated_at: EASTERN_TIME.parse('2017/06/01 12:08:00'))

            ########### another_hiring_teammate@pedago.com

            another_hiring_teammate = FactoryBot.create(:user, {
            email: 'another_hiring_teammate@pedago.com',
            password: 'password',
            password_confirmation: 'password',
            has_seen_welcome: true,
            job_title: 'teammate',
            phone: '+12163714691',
            name: "teammate 2",
            nickname: "teammate 2"
            })
            another_hiring_teammate.professional_organization = ProfessionalOrganizationOption.reorder(:text).first
            another_hiring_teammate.hiring_team = hiring_manager.hiring_team
            another_hiring_teammate.save!
            avatar_url = "https://uploads-development.smart.ly/fixture_assets/smartly_icon_1.png"
            User.connection.execute("UPDATE users SET avatar_url = '#{avatar_url}' where id = '#{another_hiring_teammate.id}'")

            HiringApplication.create!(
            user_id: another_hiring_teammate.id,
            status: 'accepted',
            place_id: '123',
            place_details: {
                locality: {
                    long: "Bossmane's City"
                },
                administrative_area_level_1: {
                    short: "ZZ"
                },
                formatted_address: '1234 Main St'
            },
            company_year: 1942,

            company_employee_count: '5000_plus',
            company_annual_revenue: '1_to_5_million',
            company_sells_recruiting_services: true,
            website_url: 'http://example.com',
            industry: 'consumer_products',
            funding: 'seed',

            # Team
            hiring_for: 'own_team',
            team_name: 'Sales & Marketing',
            team_mission: 'asdf asdfsad fasdfsadfsadfas',
            role_descriptors: ["marketing", "legal", "general_management", "product_management"],

            # You
            job_role: "senior_management",
            fun_fact: "asdfsadfasdfasdfas",

            # Candidate Preferences
            where_descriptors: ["relocate"],
            position_descriptors: ["part_time"],
            salary_ranges: ["any"],
            has_seen_welcome: true
            )

            another_hiring_teammate.candidate_relationships.destroy_all

            # saved_for_later candidate from hiring_teammate@pedago.com above
            another_hiring_teammate.ensure_candidate_relationships([another_saved_candidate.id])
            another_hiring_teammate.candidate_relationships.where(candidate_id: another_saved_candidate.id).first.update!(hiring_manager_status: "saved_for_later", candidate_status: 'hidden', updated_at: EASTERN_TIME.parse('2017/06/01 12:01:01'))

            ########### hiring_manager_first_time@pedago.com

            hiring_manager_first_time = create_hiring_manager(
                {email: 'hiring_manager_first_time@pedago.com'},
                {has_seen_welcome: false}
            )
            another_candidate = CareerProfile.active.where.not(user_id: mba_user.id).first.user
            hiring_manager_first_time.ensure_candidate_relationships([mba_user.id, another_candidate.id])


            ########## candidate_using_positions@pedago.com
            candidate_using_positions = create_candidate
            candidate_using_positions.update({
                email: "candidate_using_positions@pedago.com",
                has_seen_careers_welcome: true
            })

            # Liked: position the candidate has liked and no action has been taken
            create_position_with_interest(candidate_using_positions, 'accepted', {title: 'Liked job'})

            # Connected: position the candidate has liked and is actively connected with the hiring manager
            candidate_position_interest = create_position_with_interest(candidate_using_positions, 'accepted', {title: 'Connected with HM job'})
            invite_to_interview_and_match(candidate_position_interest.hiring_manager, candidate_position_interest.candidate, candidate_position_interest)

            # Passed by me: position the candidate passed on from the open/browse list
            create_position_with_interest(candidate_using_positions, 'rejected', {title: 'Passed job'})

            # Closed by me: position the candidate liked, connected with the employer on, then closed the connection
            candidate_position_interest = create_position_with_interest(candidate_using_positions, 'accepted', {title: 'Job I closed'})
            hiring_relationship = invite_to_interview_and_match(candidate_position_interest.hiring_manager, candidate_position_interest.candidate, candidate_position_interest)
            hiring_relationship.update(candidate_closed: true)

            # Closed by employer: position the candidate liked then the hiring manager passed them on
            candidate_position_interest = create_position_with_interest(candidate_using_positions, 'accepted', {title: 'Job they closed'})
            hiring_relationship = invite_to_interview_and_match(candidate_position_interest.hiring_manager, candidate_position_interest.candidate, candidate_position_interest)
            hiring_relationship.update(hiring_manager_closed: true)

            # Recommended position
            # select place_details->>'lat'
            #     , place_details->>'lng'
            #     , employment_types_of_interest
            #     , primary_areas_of_interest
            # from career_profiles cp
            #     join users u on cp.user_id = u.id
            #     join career_profile_search_helpers_2 h on h.career_profile_id = cp.id
            # where u.email = 'candidate_using_positions@pedago.com';
            hiring_manager_with_recommended = create_hiring_manager
            create_open_position!({
                title: 'Recommended Position 1',
                hiring_manager_id: hiring_manager_with_recommended.id,
                position_descriptors: ['contract'],
                role: 'marketing',
                place_details: {
                    lat: 41.499,
                    lng: -81.694
                },
                desired_years_experience: {
                    min: nil,
                    max: nil
                },
                featured: true,
                archived: false
            })
        end

        build do
            cohort = FactoryBot.create(:cohort, {
                start_date: Time.parse('2030/01/01 12:00:00 UTC'),
                end_date: Time.parse('2031/01/01 12:00:00 UTC'),
                name: 'promoted mba',
                title: 'promoted mba',
                program_type: 'mba',
                graduation_days_offset_from_end: 20,
                admission_rounds: [
                    {"application_deadline_days_offset"=>0, "decision_date_days_offset"=>0}
                ]
            })
            cohort.publish!
        end

        build do
            career_profile_ids = CareerProfile.joins(:user).where("users.avatar_url is not null").reorder("career_profiles.created_at").limit(3).pluck('id')
            CareerProfileList.create!(
                name: 'hiring_index',
                career_profile_ids: career_profile_ids.slice(0,3)
            )
        end

        # special cohort for transcript generation e2e spec
        build do
            cohort = FactoryBot.create(:cohort, {
                program_type: 'mba',
                name: 'frame_list_cohort',
                start_date: Time.at(1514764800) - 1.year,
                graduation_days_offset_from_end: 10
            })

            add_content_to_cohort(cohort, {frame_list: true})
            cohort.publish!

            user = FactoryBot.create(:user)
            user.update!({
                name: 'frame_list_cohort_user',
                email: 'frame_list_cohort_user@pedago.com',
                address_line_1: '123 Timbuktoo Lane',
                address_line_2: 'Apt 1337',
                city: 'Testville',
                state: 'Some State',
                country: 'United States',
                zip: '12345'
            })
            identify(create_cohort_application_for_user(cohort, 'accepted', user), "frame_list_cohort_user")
            user.cohort_applications.reload.first.update(disable_exam_locking: true)
            user.ensure_institution(Institution.quantic)
            user.active_institution = Institution.quantic
            # see extra setup on frame_list_cohort_user below
        end

        # refresh materialized content views
        build do
            RefreshMaterializedContentViews.refresh_and_rebuild_derived_content_tables
        end

        # Do some extra setup for frame_list_cohort_user now that we have
        # run materialized views and can access that stuff
        build do
            user = User.find_by_email('frame_list_cohort_user@pedago.com')
            all_stream_pack_ids = user.relevant_cohort.get_required_stream_locale_pack_ids +
                user.relevant_cohort.get_optional_stream_locale_pack_ids

            all_stream_pack_ids.each do |pack_id|
                stream = Lesson::Stream.find_by_locale_pack_id(pack_id)

                stream_progress = complete_stream(stream, user, {
                    completed_at: Time.now
                })

                if stream.exam?
                    stream_progress.update_attribute(:official_test_score, 0.899)
                end

                stream.lessons.each do |lesson|
                    lesson_progress = LessonProgress.create_or_update!({
                        user_id: user.id,
                        locale_pack_id: lesson.locale_pack_id
                    })
                    lesson_progress.update_attribute(:completed_at, Time.now)

                    if lesson.assessment?
                        lesson_progress.update_attribute(:best_score, 0.911)
                    elsif stream.exam?
                        lesson_progress.update_attribute(:best_score, 0.899)
                    end

                    # events are needed for user_lesson_progress_records to be built, which avg_test_score needs
                    RedshiftEvent.create!(id: SecureRandom.uuid, user_id: user.id, event_type: 'lesson:finish', lesson_id: lesson.id, created_at: Time.now, estimated_time: Time.now)
                end
            end

            Report::UserLessonProgressRecord.write_new_records

            # since ruby specs are going to use the same red_royal database, and since we don't use transactions
            # to clean those things up after each spec, it is confusing if fixture builder creates redshift events,
            # so delete them all once we've used them for write_new_records.
            RedshiftEvent.delete_all
        end

        # WriteRegularlyIncrementallyUpdatedTablesJob
        # - career_profile search helpers are needed for hiring manager specs
        #
        # RefreshMaterializedInternalReportsViews
        # - cohort_user_progress_records is needed for transcript_e2e specs
        build do
            WriteRegularlyIncrementallyUpdatedTablesJob.perform_now

            CohortApplication.pluck(:id).each do |id|
                CohortStatusChange.update_cohort_status_changes_for_application(id)
            end

            RefreshMaterializedInternalReportsViews.perform_now
        end

        # this should be the last thing.  Make sure all content is relationship
        build do
            if Lesson.all.distinct.reorder(nil).pluck('lesson_type') != ['frame_list']
                # we create unrealistic lessons for ruby specs, because we don't necessarily
                # need frames and stuff, but lessons for puppet specs have to be able to render
                # in a browser
                raise "puppet specs should only innclude frame_list lessons"
            end
        end

        build do
          admin_user = FactoryBot.create(:user, {:email => 'admin@pedago.com'}) # used in integration specs
          admin_user.ensure_institution(Institution.quantic)
          admin_user.active_institution = Institution.quantic
          admin_user.roles = [Role.find_by_name('admin')]
          admin_user.add_to_group('CASPER_MBA')
          admin_user.save!

          # Create a separate stream to house content for lesson content tests
          test_content_stream = create_frame_list_stream("Test Content", {id: "4ac7aa46-8353-43bf-94c8-fe30002bc579"})
          test_content_stream.locale_pack.add_to_group('CASPER_MBA')

          # Add the test content
          test_content_lesson = test_content_stream.lessons[0]
          test_content_lesson.content_json = content_json_for_test_lesson
          test_content_lesson.frame_count = content_json_for_test_lesson[:frames].size
          test_content_lesson.publish!

          # If we paste content from "Show JSON" id will be a symbol, but from rails console id will be a string
          test_content_frame_ids = content_json_for_test_lesson[:frames].pluck(:id) + content_json_for_test_lesson[:frames].pluck("id")
          test_content_frame_ids = test_content_frame_ids.uniq.compact
          completed_frames_hash = {}
          test_content_frame_ids.each do |frame_id|
            completed_frames_hash[frame_id] = true
          end

          # Make progress to avoid the intro screen and mark all frames as completed for indicator consistency
          LessonProgress.create_or_update!({
            :user_id => admin_user.id,
            :locale_pack_id => test_content_lesson.locale_pack_id,
            :completed_frames => completed_frames_hash
          })
          Lesson::StreamProgress.create_or_update!({
            :user_id => admin_user.id,
            :locale_pack_id => test_content_stream.locale_pack_id,
            :completed_at => Time.now
          })
        end

        def create_frame_list_playlists(group_name, options = {})
            content_item_prefix ||= group_name.downcase.gsub('_', ' ')
            options[:stream_ids] ||= [SecureRandom.uuid, SecureRandom.uuid, SecureRandom.uuid]
            AccessGroup.find_or_create_by!(name: group_name)
            streams = [
                create_frame_list_stream("#{content_item_prefix} stream1", {id: options[:stream_ids][0]}),
                create_frame_list_stream("#{content_item_prefix} stream2", {id: options[:stream_ids][1]}),
                create_frame_list_stream("#{content_item_prefix} stream3", {id: options[:stream_ids][2]})
            ]
            streams.each do |stream|
                stream.locale_pack.add_to_group(group_name)
            end

            playlists = []

            2.times do |i|
                title = "#{content_item_prefix} playlist"
                title += " #{i}" unless i == 0
                playlist = create_playlist(streams, { title: title })
                playlist.publish!
                playlist.locale_pack.add_to_group(group_name)
                playlists << playlist
            end


            [playlists, streams]
        end

        def create_position_with_interest(candidate, status, position_attrs = {})
            position = create_hiring_manager_with_position(position_attrs)
            create_candidate_position_interest(candidate, position, {candidate_status: status})
        end

        def create_candidate_position_interest(candidate, position, attrs = {})
            attrs = attrs.unsafe_with_indifferent_access
            raise 'candidate_status must be provided in attrs' if attrs["candidate_status"].nil?
            attrs["hiring_manager_status"] ||= 'hidden' # defaults to 'hidden' because that's what the default is in the database

            CandidatePositionInterest.create!({
                candidate_id: candidate.id,
                open_position_id: position.id,
                cover_letter: {
                    created_at: EASTERN_TIME.parse('2017/05/31 10:14:00'), # hardcoded to prevent screenshot specs from failing
                    content: %q(Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                    )
                }
            }.merge(attrs))
        end

        def create_hiring_manager_with_position(position_attrs = {})
            hiring_manager = create_hiring_manager
            create_open_position!({
                hiring_manager_id: hiring_manager.id
            }.merge(position_attrs))
        end

        def create_and_accept_hiring_manager(user_attrs = {}, hiring_application_attrs = {})
            hiring_manager = create_hiring_manager(user_attrs, hiring_application_attrs)
            hiring_manager.ensure_hiring_team!
            hiring_manager.hiring_application.update(status: 'accepted')
            hiring_manager
        end

        def create_unaccepted_hiring_manager(user_attrs = {}, hiring_application_attrs = {})
            professional_org =  ProfessionalOrganizationOption.reorder(:text).first
            hiring_manager = FactoryBot.create(:user, {
                password: 'password',
                password_confirmation: 'password',
                has_seen_welcome: true,
                has_drafted_open_position: true,
                has_seen_first_position_review_modal: true,
                job_title: 'bossman',
                phone: '+17035967691',
                name: "Bossmang",
                nickname: "Bossmang"
            }.merge(user_attrs))
            hiring_manager.professional_organization = professional_org
            hiring_manager.save!
            avatar_url = 'https://uploads-development.smart.ly/fixture_assets/avatar_1.png'
            User.connection.execute("UPDATE users SET avatar_url = '#{avatar_url}' where id = '#{hiring_manager.id}'")

            HiringApplication.create!({
                user_id: hiring_manager.id,
                status: 'pending',
                place_id: '123',
                place_details: {
                    locality: {
                        long: "Bossmane's City"
                    },
                    administrative_area_level_1: {
                        short: "ZZ"
                    },
                    formatted_address: '1234 Main St'
                },
                company_year: 1942,

                company_employee_count: '5000_plus',
                company_annual_revenue: '1_to_5_million',
                website_url: 'http://example.com',
                industry: 'consumer_products',
                funding: 'seed',
                company_sells_recruiting_services: false,

                # Team
                hiring_for: 'own_team',
                team_name: 'Sales & Marketing',
                team_mission: 'asdf asdfsad fasdfsadfsadfas',
                role_descriptors: ["marketing", "legal", "general_management", "product_management"],

                # You
                job_role: "senior_management",
                fun_fact: "asdfsadfasdfasdfas",

                # Candidate Preferences
                where_descriptors: ["relocate"],
                position_descriptors: ["part_time"],
                salary_ranges: ["any"],
                has_seen_welcome: true
            }.merge(hiring_application_attrs))

            hiring_manager.reload.association(:hiring_team).reload
            if !hiring_manager.hiring_team
                # jumping through hoops here to prevent this validation error: ActiveRecord::RecordInvalid: Validation failed: Owner must be a member of the team
                hiring_team = HiringTeam.new(title: "Team for #{hiring_manager.email}", subscription_required: true, hiring_plan: nil)
                hiring_team.owner = hiring_manager
                hiring_team.hiring_managers << hiring_manager
                hiring_manager.hiring_team = hiring_team
                hiring_team.save!
                hiring_manager.save!
            end

            hiring_manager
        end

        def create_hiring_manager(user_attrs = {}, hiring_application_attrs = {})
            hiring_manager = create_unaccepted_hiring_manager(user_attrs, hiring_application_attrs)
            hiring_manager.ensure_hiring_team!
            hiring_manager.hiring_application.update(status: 'accepted')
            hiring_manager
        end

        def invite_to_interview_and_match(hiring_manager, candidate, candidate_position_interest)
            hiring_manager.ensure_candidate_relationships([candidate.id])
            hiring_relationship = hiring_manager.candidate_relationships.where(candidate_id: candidate.id).first
            position = candidate_position_interest.open_position
            conversation_json = {
                subject: "new subject",
                candidate_position_interest_id: candidate_position_interest.id,
                messages: [{
                    "body" => "You should come dive in our fish tank.  Our fish are hungry.",
                    "sender_id" => hiring_manager.id,
                    "sender_type" => 'user',
                    "metadata" => {
                        senderCompanyName: hiring_manager.hiring_application.company_name,
                        openPosition: position.as_json
                    }
                }],
                recipients: [
                    {
                        'type' => 'user',
                        'id' => hiring_manager.id
                    },
                    {
                        'type' => 'user',
                        'id' => candidate.id
                    }]
            }
            conversation = hiring_relationship.conversation = Mailboxer::Conversation.create_or_update_from_hash!(conversation_json, hiring_manager, hiring_relationship)

            hiring_relationship.update!(
                hiring_manager_status: "accepted",
                candidate_status: 'accepted',
                open_position_id: position.id,
                updated_at: EASTERN_TIME.parse('2017/06/01 12:04:00')
            )
            conversation_json = conversation.as_json
            conversation_json['messages'] << {
                "body" => "Sounds great!.  I'm in",
                "sender_id" => candidate.id,
                "sender_type" => 'user'
            }
            conversation = Mailboxer::Conversation.create_or_update_from_hash!(conversation_json, candidate, hiring_relationship)
            conversation_json = conversation.as_json
            conversation_json['messages'] << {
                "body" => "Awesome.  You're hired!",
                "sender_id" => hiring_manager.id,
                "sender_type" => 'user'
            }
            conversation = Mailboxer::Conversation.create_or_update_from_hash!(conversation_json, hiring_manager, hiring_relationship)
            # mark all as read because otherwise specs will mark them as read and things will change from spec to spec
            i = 1
            conversation.messages.reorder(:created_at).each { |m| m.update(created_at: EASTERN_TIME.parse("2017/06/01 12:04:0#{i}"), updated_at: EASTERN_TIME.parse("2017/06/01 12:04:0#{i}")); i += 1 }
            conversation.messages.map(&:receipts).flatten.sort_by(&:created_at).each { |r| r.update(is_read: true, created_at: EASTERN_TIME.parse("2017/06/01 12:04:0#{i}"), updated_at: EASTERN_TIME.parse("2017/06/01 12:04:0#{i}")); i += 1 }
            hiring_relationship
        end

        # Use the "Show JSON" feature in the editor to get the JSON for a frame, place it in a file
        # in `spec/fixtures/files/content`, then add it to the content_json_for_test_lesson frames array below.
        # Or use `Lesson.find('713ec82c-7805-4d70-90d3-6e46a86ee2dd').frames[0].as_json` if you're having problems
        # with things not being escaped properly.
        # Use `rake lesson:urls_for_editor_template template=template_type` to find slides for a template type
        # to copy over.
        # Note: Smartly Interactions has a lot of good ones
        # Note: Escaping gets messed up sometimes with the "Show JSON" feature
        # Note: If you need to write a ruby hash to file as JSON use:
        #   `File.open("tmp/mathjax_frame_1.json","w") { |f| f.write(Lesson.find('713ec82c-7805-4d70-90d3-6e46a86ee2dd').content_json["frames"].first.to_json) }`
        def content_json_for_test_lesson
          @content_json_for_test_lesson ||= {
            "lesson_type": "frame_list",
            "frames": [
              multiple_card_multiple_choice_frame_1,
              this_or_that_frame_1,
              fill_in_the_blanks_frame_1,
              matching_frame_1,
              mathjax_frame_1,
              compose_blanks_on_image_frame_1,
            ]
          }
        end

        # /editor/lesson/52eb8073-19b8-45e9-b206-2b33bdb00532/edit?frame=a705ef7e-bbdc-4998-cb50-fc90ce5028dd
        def multiple_card_multiple_choice_frame_1
          file = File.open('spec/fixtures/files/content/multiple_card_multiple_choice_frame_1.json')
          data = JSON.parse(file)
          file.close
          data
        end

        # /editor/lesson/52eb8073-19b8-45e9-b206-2b33bdb00532/edit?frame=6df1c7cd-1ab7-4d5c-ffe0-755c8d8fc099
        def this_or_that_frame_1
          file = File.open('spec/fixtures/files/content/this_or_that_frame_1.json')
          data = JSON.parse(file)
          file.close
          data
        end

        # /editor/lesson/52eb8073-19b8-45e9-b206-2b33bdb00532/edit?frame=a2404415-f519-4f9c-8711-acd2640f6ef5
        def fill_in_the_blanks_frame_1
          file = File.open('spec/fixtures/files/content/fill_in_the_blanks_frame_1.json')
          data = JSON.parse(file)
          file.close
          data
        end

        # /editor/lesson/52eb8073-19b8-45e9-b206-2b33bdb00532/edit?frame=5348b0e7-eaad-493a-954a-dc81a8accb0a
        def matching_frame_1
          file = File.open('spec/fixtures/files/content/matching_frame_1.json')
          data = JSON.parse(file)
          file.close
          data
        end

        # /editor/lesson/713ec82c-7805-4d70-90d3-6e46a86ee2dd/edit?frame=c55d1dee-be02-445e-b550-1eec1cbcbe33
        # Lesson.find('713ec82c-7805-4d70-90d3-6e46a86ee2dd').content_json["frames"].first
        def mathjax_frame_1
          file = File.open('spec/fixtures/files/content/mathjax_frame_1.json')
          data = JSON.parse(file)
          file.close
          data
        end

        # /editor/lesson/2c5a3853-db06-4dec-bd5b-4ab2824307eb/edit?frame=4938cdc3-a4a7-46b7-e952-097f497b9e85
        def compose_blanks_on_image_frame_1
            file = File.open('spec/fixtures/files/content/compose_blanks_on_image_frame_1.json')
            data = JSON.parse(file)
            file.close
            data
        end

        build do
            cluster_tables
        end
    end
end