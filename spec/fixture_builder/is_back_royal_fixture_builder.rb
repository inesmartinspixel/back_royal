module FixtureBuilder
    module IsBackRoyalFixtureBuilder
        extend ActiveSupport::Concern

        included do
            table_names_to_skip << 'spatial_ref_sys'
            build do
                @start_is_back_royal_fixture_builder_setup = Time.now
            end
            build_internal_institutions
            build_roles
            build_access_groups
            build_assessment_stream
            build_exam_stream
            build_learner_project
            build_things_to_associate_with_profiles
            build_global_metadata
            build do
                elapsed = Time.now - @start_is_back_royal_fixture_builder_setup
                # this was at 2-3 seconds on 2019/02/01.  We don't want it to get to long
                # since it is duplicated across both fixture builders
                if elapsed > 5
                    puts "****** WARNING: The setup that is shared between FixtureBuilders is taking longer than it used to"
                end
            end
        end

        # HACK: at the end of initialization, we'll re-cluster all of the tables to try to make test runs more deterministic
        def cluster_tables
            ActiveRecord::Base.connection.tables.each do |table|
                index = ActiveRecord::Base.connection.indexes(table).detect{|i| i.columns == ['created_at'] && i.where.nil?}

                if !index.nil?
                    ActiveRecord::Base.connection.execute("CLUSTER VERBOSE #{table} USING #{index.name}")
                elsif ActiveRecord::Base.connection.columns(table).map(&:name).include?('created_at')
                    # create the index ourselves and then use it!
                    ActiveRecord::Base.connection.execute("CREATE INDEX index_#{table}_on_created_at_hack ON #{table}(created_at)")
                    ActiveRecord::Base.connection.execute("CLUSTER VERBOSE #{table} USING index_#{table}_on_created_at_hack")
                end
            end
        end

        # override parent's clear_db, because of foreign key constraints.  Is the
        # a safe way to move this into the gem?
        def clear_db
            self.class.table_names.each do |table_name|
                quoted_table_name = ActiveRecord::Base.connection.quote_table_name(table_name)
                # sql = "DELETE FROM #{quoted_table_name} "
                # ActiveRecord::Base.connection.delete(sql, "#{quoted_table_name} Delete all")
                ActiveRecord::Base.connection.execute("TRUNCATE #{quoted_table_name} CASCADE")
            end

        end

        module ClassMethods
            def build_internal_institutions
                build do
                    Institution.create!(Institution.valid_quantic_attrs)
                    Institution.create!(Institution.valid_smartly_attrs)
                    Institution.create!(Institution.valid_miya_miya_attrs)
                end
            end

            def build_roles
                build do
                    @admin = Role.find_or_create_by!(name: 'admin')
                    identify(@admin, 'admin')
                    learner = Role.find_or_create_by!(name: 'learner')
                    identify(learner, 'learner')
                end
            end

            def build_access_groups
                build do

                    access_groups = []
                    [
                        "GROUP",
                        "SMARTER",
                        "SUPERVIEWER",
                        "MBA",
                        "TRANSFORMAFRICA",
                        "HIRING",
                        "BLUEOCEAN",
                        "SIGN_UP_CODE",
                        "DEMO",
                        "OPEN COURSES",
                        "EXTRA COURSES"
                    ].each do |group_name|
                        access_groups << AccessGroup.create!(name: group_name)
                    end
                    identify(access_groups[0], 'default_group')

                    user = create_user({pref_locale: 'en'})
                    user.add_to_group('SUPERVIEWER')
                    identify(user, 'en_superviewer')
                end
            end

            def build_assessment_stream
                build do
                    lessons = (0..2).map { only_allow_frame_list_streams? ? create_frame_list_lesson : create_lesson }

                    lessons[0].update_attribute('assessment', true)
                    lessons[0].publish!
                    identify(lessons[0], 'assessment_lesson')
                    @assessment_lesson = lessons[0]

                    lessons[1].update_attribute('assessment', true)
                    lessons[1].publish!
                    @another_assessment_lesson = lessons[1]

                    lessons[2].update_attribute('assessment', true)
                    lessons[2].publish!
                    @a_third_assessment_lesson = lessons[2]

                    @assessment_stream = create_stream(true, {
                        exam: false
                    })
                    @assessment_stream.lessons = [@assessment_lesson, @another_assessment_lesson, @a_third_assessment_lesson]
                    @assessment_stream.chapters = [
                        {
                            "title" => "Chapter 1",
                            "lesson_hashes" => [
                                {"lesson_id": @assessment_lesson.id,"coming_soon":false},
                                {"lesson_id": @another_assessment_lesson.id,"coming_soon":false},
                                {"lesson_id": @a_third_assessment_lesson.id,"coming_soon":false}
                            ],
                            "lesson_ids" => [@assessment_lesson.id, @another_assessment_lesson.id, @a_third_assessment_lesson.id]
                        }
                    ]
                    @assessment_stream.publish!({update_published_at: true})
                    identify(@assessment_stream, 'assessment_stream')
                end
            end

            def build_exam_stream
                build do
                    @exam_stream = create_stream(true, {
                        exam: true,
                        time_limit_hours: 42
                    })
                    @exam_stream.lessons.each do |lesson|
                        lesson.test = true
                        lesson.publish!({update_published_at: true})
                    end
                    @exam_stream.publish!({update_published_at: true})
                    identify(@exam_stream, 'exam_stream')
                end
            end

            def build_learner_project
                build do
                    @learner_project = LearnerProject.create!(
                        title: 'Learner project',
                        project_documents: [
                            {title: 'Project 1 Document 1', url: 'http://path/to/doc'},
                            {title: 'Project 1 Document 2', url: 'http://path/to/doc'}
                        ],
                        requirement_identifier: 'default_project'
                    )
                    @another_learner_project = LearnerProject.create!(
                        title: 'Learner project 2',
                        project_documents: [
                            {title: 'Project 2 Document 1', url: 'http://path/to/doc'},
                            {title: 'Project 2 Document 2', url: 'http://path/to/doc'}
                        ],
                        requirement_identifier: 'another_project'
                    )
                end
            end

            def build_global_metadata
                build do
                    GlobalMetadata.find_or_create_by({
                        site_name: "__defaults"
                    })
                end
            end

            def build_things_to_associate_with_profiles
                build do
                    @professional_organizations = (1..5).map do |i|
                        ProfessionalOrganizationOption.create!({text: "Professional Organization #{i}", suggest: true})
                    end

                    (1..5).map do |i|
                        CareerProfile::EducationalOrganizationOption.create!({text: "Educational Organization #{i}", suggest: true})
                    end

                    (1..5).map do |i|
                        StudentNetworkInterestsOption.create!({text: "Student Network Interest #{i}", suggest: true})
                    end
                    (1..5).map do |i|
                        SkillsOption.create!({text: "Skill #{i}", suggest: true})
                    end
                    (1..5).map do |i|
                        CareerProfile::AwardsAndInterestsOption.create!({text: "Award and Interest #{i}", suggest: true})
                    end
                end
            end
        end

        def create_frame_list_stream(content_item_prefix = nil, stream_attrs = {})
            content_item_prefix ||= 'A stream'
            frame_list_lessons = (1..2).map do |i|
                mock_id = SecureRandom.uuid

                # if a predetermined stream id is passed in then make the lesson id
                # predetermined as well
                if stream_attrs[:id].present?
                    mock_id = stream_attrs[:id].dup
                    mock_id[-1] = i.to_s
                end

                frame_list_lesson = create_frame_list_lesson({id: mock_id})

                frame_list_lesson.content = {
                    lesson_type: 'frame_list',
                    frames: [example_frame, example_frame]
                }
                frame_list_lesson.lesson_type = 'frame_list'
                frame_list_lesson.title = "#{content_item_prefix} lesson #{i}"
                frame_list_lesson.frame_count = 2

                frame_list_lesson.publish!(update_published_at: true)
                frame_list_lesson.reload # need so that has_published_version? will return true
                frame_list_lesson
            end

            stream = create_stream(true, stream_attrs)
            stream.lessons = frame_list_lessons
            stream.title = content_item_prefix
            stream.chapters = [{
                "title" => "Chapter 1",
                "lesson_ids" => frame_list_lessons.map(&:id),
                "lesson_hashes" => [{
                    "lesson_id" => frame_list_lessons[0].id
                },{
                    "lesson_id" => frame_list_lessons[1].id
                }]
            }]
            stream.publish!(update_published_at: true)
            stream.reload # need so that has_published_version? will return true
            stream
        end

        def create_lesson(attrs = {}, meta = {})
            unless attrs.key?(:author_id) || @generic_editor
                @generic_editor ||= create_user
                @generic_editor.add_role(:editor)
            end
            author = attrs.key?(:author_id) ? User.find(attrs[:author_id]) : @generic_editor
            lesson = Lesson.create_from_hash!({
                title: "Lesson #{SecureRandom.uuid}",
                author: author,
                tag: 'tag'
            }.merge(attrs), meta.with_indifferent_access)
            lesson.entity_metadata.image = S3Asset.first || FactoryBot.create(:s3_asset)
            lesson.entity_metadata.save!
            lesson
        end

        def create_frame_list_lesson(attrs = {}, meta = {})
            lesson = create_lesson(attrs)
            convert_to_frame_list_lesson(lesson)
            lesson.save!
            lesson
        end

        def convert_to_frame_list_lesson(lesson)
            lesson.content = {
                lesson_type: 'frame_list',
                frames: [example_frame, example_frame]
            }
            lesson.lesson_type = 'frame_list'
            lesson.frame_count = 2
        end

        def only_allow_frame_list_streams?
            false
        end

        def create_stream(new_lessons = false, attrs = {}, opts = {})


            # if frame_list is true, then newly created lessons will be
            # frame list lessons (this only makes a difference if you
            # are creating new lessons)
            frame_list = opts[:frame_list] || only_allow_frame_list_streams?
            unless @generic_editor
                @generic_editor ||= create_user
                @generic_editor.add_role(:editor)
            end
            author = @generic_editor

            # this gives us kind of unrealistic fixtures, since in
            # the wild lessons are not shared between streams, but removing
            # it slows things down significantly
            new_lessons = 4 if new_lessons == true
            if new_lessons && new_lessons > 0
                lessons = (1..new_lessons).map {
                    frame_list ? create_frame_list_lesson : create_lesson
                }
            else
                lessons = Lesson.reorder(Arel.sql('random()')).limit(4).to_a
            end

            chapters = [
                {
                    "title" => "Chapter 1",
                    "lesson_hashes" => lessons.slice(0, 2).map { |l| {"lesson_id":l.id,"coming_soon":false} },
                    "lesson_ids" => lessons.slice(0, 2).map(&:id)
                }
            ]
            if lessons.size > 2
                chapters << {
                    "title" => "Chapter 2",
                    "lesson_hashes" => lessons.slice(2, 99999).map { |l| {"lesson_id":l.id,"coming_soon":false} },
                    "lesson_ids" => lessons.slice(2, 999999).map(&:id)
                }
            end

            # create stream core
            stream = Lesson::Stream.create_from_hash!({
                tag: "tag",
                title: "Stream #{SecureRandom.uuid}",
                author: {id: author.id},
                description: "An awesome stream",
                credits: "some credits",
                lessons: lessons.map(&:as_json),
                resource_downloads: [{
                    "url" => "https://uploads.smart.ly/images/820bf3aa1fbc1fdb3167c63f2cfcf41f/1400x550/820bf3aa1fbc1fdb3167c63f2cfcf41f.jpg",
                    "title" => "A picture of a dog collar"
                }],
                chapters: chapters,
            }.merge(attrs))

            # create a new image
            image = stream.get_image_asset('some_image')
            image.save!

            # attach to the stream
            hash = stream.as_json.merge({
                image: image.as_json
            }.merge(attrs))

            # update some metadata
            stream.entity_metadata.update!({
                'description' => 'description',
                'canonical_url' => "/course/title/#{stream.id}"
            })

            stream = Lesson::Stream.update_from_hash!(author, hash)
        end

        def create_playlist(streams = nil, attrs = {})
            unless @generic_editor
                @generic_editor ||= create_user
                @generic_editor.add_role(:editor)
            end
            streams ||= [@stream_with_all_groups]
            Playlist.create_from_hash!({
                title: 'title',
                tag: 'tag',
                author: @generic_editor,
                description: 'description',
                stream_entries: streams.map { |stream|
                    {
                        stream_id: stream.id, # this shouldn't be set anymore after localization
                        locale_pack_id: stream.locale_pack_id,
                        description: 'this stream has all the groups!'
                    }
                },
                locale: streams[0].locale
            }.deep_merge(attrs))
        end

        def example_frame
            frame_navigator_id = SecureRandom.uuid
            main_ui_component_id = SecureRandom.uuid
            text_id = SecureRandom.uuid
            continue_button_id = SecureRandom.uuid
            {
                "components" => [
                {
                "id" => frame_navigator_id,
                "component_type" => "ComponentizedFrame.FrameNavigator"
                },
                {
                "id" => main_ui_component_id,
                "component_type" => "ComponentizedFrame.TextImageInteractive",
                "editor_template" => "no_interaction",
                "content_for_text_id" => text_id
                },
                {
                "id" => text_id,
                "component_type" => "ComponentizedFrame.Text",
                "text" => "Example",
                "formatted_text" => "<p>Example</p>"
                },
                {
                "id" => continue_button_id,
                "component_type" => "ComponentizedFrame.AlwaysReadyContinueButton"
                }
                ],
                "frame_type" => "componentized",
                "id" => SecureRandom.uuid,
                "frame_navigator_id" => frame_navigator_id,
                "main_ui_component_id" => main_ui_component_id,
                "continue_button_id" => continue_button_id
            }
        end

        def create_user(overrides = {})
            FactoryBot.create(:user, overrides)
        end

        def create_cohort_application_for_user(cohort, status, user = nil, graduation_status = 'pending', attrs = {})
            user ||= create_user

            user.ensure_institution(cohort.institution)
            user.active_institution = cohort.institution

            if !user.roles.include?("admin")
                user.mba_content_lockable = true
                user.save!
            end

            cohort_application = CohortApplication.create_from_hash!({
                user_id: user.id,
                cohort_id: cohort.id,
                status: status,
                graduation_status: graduation_status,
                scholarship_level: cohort.stripe_plans ? cohort.scholarship_levels[0] : nil,
                total_num_required_stripe_payments: nil,
                notes: "some notes",
                cohort_slack_room_id: ['accepted', 'pre_accepted'].include?(status) ? cohort.slack_rooms[0]&.id : nil
            }.merge(attrs), {is_admin: true}) # use is_admin to allow setting all attributes

            cohort_application.user
        end

        def complete_stream(stream, user, overrides = {})

            if !@certificate_image
                @certificate_image = FactoryBot.create(:s3_asset)
                @certificate_image.update_columns(
                    "file_file_name"=>"output_certificate_0.928660622033046.png",
                    "file_content_type"=>"image/png",
                    "file_file_size"=>91624,
                    "file_updated_at"=>Time.parse('Wed, 25 Mar 2015 20:53:10 UTC +00:00'),
                    "directory"=>"images/",
                    "styles"=>
                    "{\"1400x550\":\"1400x550>\",\"700x275\":\"700x275>\",\"1020x400\":\"1020x400>\",\"510x200\":\"510x200>\",\"168x66\":\"168x66>\",\"84x33\":\"84x33>\",\"816x320\":\"816x320>\",\"408x160\":\"408x160>\",\"586x230\":\"586x230>\",\"293x115\":\"293x115>\",\"1146x450\":\"1146x450>\",\"573x225\":\"573x225>\",\"688x270\":\"688x270>\",\"344x135\":\"344x135>\"}",
                    "file_fingerprint"=>"f6397c2d57bd08b52ca75324bc61c29f",
                    "dimensions"=>{"width"=>1496.0, "height"=>978.0},
                    "formats"=>
                    {"1400x550"=>{"url"=>"https://uploads.smart.ly/images/f6397c2d57bd08b52ca75324bc61c29f/1400x550/f6397c2d57bd08b52ca75324bc61c29f.png", "width"=>1400, "height"=>550},
                    "700x275"=>{"url"=>"https://uploads.smart.ly/images/f6397c2d57bd08b52ca75324bc61c29f/700x275/f6397c2d57bd08b52ca75324bc61c29f.png", "width"=>700, "height"=>275},
                    "1020x400"=>{"url"=>"https://uploads.smart.ly/images/f6397c2d57bd08b52ca75324bc61c29f/1020x400/f6397c2d57bd08b52ca75324bc61c29f.png", "width"=>1020, "height"=>400},
                    "510x200"=>{"url"=>"https://uploads.smart.ly/images/f6397c2d57bd08b52ca75324bc61c29f/510x200/f6397c2d57bd08b52ca75324bc61c29f.png", "width"=>510, "height"=>200},
                    "168x66"=>{"url"=>"https://uploads.smart.ly/images/f6397c2d57bd08b52ca75324bc61c29f/168x66/f6397c2d57bd08b52ca75324bc61c29f.png", "width"=>168, "height"=>66},
                    "84x33"=>{"url"=>"https://uploads.smart.ly/images/f6397c2d57bd08b52ca75324bc61c29f/84x33/f6397c2d57bd08b52ca75324bc61c29f.png", "width"=>84, "height"=>33},
                    "816x320"=>{"url"=>"https://uploads.smart.ly/images/f6397c2d57bd08b52ca75324bc61c29f/816x320/f6397c2d57bd08b52ca75324bc61c29f.png", "width"=>816, "height"=>320},
                    "408x160"=>{"url"=>"https://uploads.smart.ly/images/f6397c2d57bd08b52ca75324bc61c29f/408x160/f6397c2d57bd08b52ca75324bc61c29f.png", "width"=>408, "height"=>160},
                    "586x230"=>{"url"=>"https://uploads.smart.ly/images/f6397c2d57bd08b52ca75324bc61c29f/586x230/f6397c2d57bd08b52ca75324bc61c29f.png", "width"=>586, "height"=>230},
                    "293x115"=>{"url"=>"https://uploads.smart.ly/images/f6397c2d57bd08b52ca75324bc61c29f/293x115/f6397c2d57bd08b52ca75324bc61c29f.png", "width"=>293, "height"=>115},
                    "1146x450"=>{"url"=>"https://uploads.smart.ly/images/f6397c2d57bd08b52ca75324bc61c29f/1146x450/f6397c2d57bd08b52ca75324bc61c29f.png", "width"=>1146, "height"=>450},
                    "573x225"=>{"url"=>"https://uploads.smart.ly/images/f6397c2d57bd08b52ca75324bc61c29f/573x225/f6397c2d57bd08b52ca75324bc61c29f.png", "width"=>573, "height"=>225},
                    "688x270"=>{"url"=>"https://uploads.smart.ly/images/f6397c2d57bd08b52ca75324bc61c29f/688x270/f6397c2d57bd08b52ca75324bc61c29f.png", "width"=>688, "height"=>270},
                    "344x135"=>{"url"=>"https://uploads.smart.ly/images/f6397c2d57bd08b52ca75324bc61c29f/344x135/f6397c2d57bd08b52ca75324bc61c29f.png", "width"=>344, "height"=>135},
                    "original"=>{"url"=>"https://uploads.smart.ly/images/f6397c2d57bd08b52ca75324bc61c29f/original/f6397c2d57bd08b52ca75324bc61c29f.png"}}
                )
            end

            stream_progress = Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => stream.locale_pack_id,
                :lesson_bookmark_guid => stream.lessons.first.id,
                :user_id => user.id,
                :complete => true,
                :certificate_image_id => @certificate_image.id
            }.merge(overrides))
            if (overrides[:completed_at])
                stream_progress.update_attribute(:completed_at, overrides[:completed_at])
            end

            published_lessons = stream.lessons.select(&:has_published_version?)
            raise "No published lessons found" unless published_lessons.size > 0

            published_lessons.each do |lesson|
                LessonProgress.create_or_update!({
                    :locale_pack_id => lesson.locale_pack_id,
                    :frame_bookmark_guid => SecureRandom.uuid,
                    :user_id => user.id,
                    :complete => true
                })
            end

            if overrides[:last_progress_at]
                stream_progress.update_attribute(:updated_at,  overrides[:last_progress_at])
            end

            stream_progress
        end

        def start_stream(stream, user, overrides = {})
            stream_progress = Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => stream.locale_pack_id,
                :lesson_bookmark_guid => stream.lessons.first.id,
                :user_id => user.id,
                :complete => false
            }.merge(overrides))

            published_lessons = stream.lessons.select(&:has_published_version?)
            raise "No published lessons found" unless published_lessons.size > 0

            LessonProgress.create_or_update!({
                :locale_pack_id => published_lessons[0].locale_pack_id,
                :frame_bookmark_guid => SecureRandom.uuid,
                :user_id => user.id,
                :complete => false
            })

            if overrides[:last_progress_at]
                stream_progress.update_attribute(:updated_at,  overrides[:last_progress_at])
            end

            stream_progress
        end

        def complete_career_profile(user, overrides = {})
            # In case a career_profile was already made with this user elsewhere in the fixtures we need to know
            # in order to not hit validation problems in user#ensure_career_profile_if_flagged
            user.reload

            user.sex = "male"
            user.ethnicity = true
            user.race = ["american_indian_or_alaska", "asian", "african_american", "native_hawaiian_or_other"]
            user.how_did_you_hear_about_us = "facebook"
            user.phone = '+16015551337'
            user.can_edit_career_profile = true # required for user.career_profile.career_profile_active?
            user.save!
            avatar_url = 'https://uploads-development.smart.ly/fixture_assets/avatar_1.png'
            User.connection.execute("UPDATE users SET avatar_url = '#{avatar_url}' where id = '#{user.id}'")

            id = overrides[:id] || overrides['id']
            if id
                ActiveRecord::Base.connection.execute("update career_profiles set id='#{id}' where id='#{user.career_profile.id}'")
                user.career_profile = CareerProfile.find(id)
            end

            career_profile = user.career_profile
            attrs = {
                id: career_profile.id,
                user_id: user.id,
                "score_on_gmat"=>"740",
                "score_on_sat"=>"1500",
                "sat_max_score"=>"1600",
                "score_on_act"=>"31",
                "score_on_gre_verbal"=>"150",
                "score_on_gre_quantitative"=>"150",
                "score_on_gre_analytical"=>"3.5",
                "short_answer_greatest_achievement"=>"Defeating Dr. Octopus",
                "primary_reason_for_applying"=>"To help manage my superhero business",
                "short_answer_why_pursuing"=>"Need for Speed: Hot Pursuit 2 just wasn't cutting it for me anymore.",
                "short_answer_use_skills_to_advance"=>"I've always thought my speling could be better.",
                "short_answer_ask_professional_advice"=>"In the words of the great Matthew McConaughey, \"Me, in 10 years.\"",
                "has_no_formal_education"=>false,
                "willing_to_relocate"=>true,
                "open_to_remote_work"=>true,
                "authorized_to_work_in_us"=>"h1visa",
                "top_mba_subjects"=>["finance_and_accounting", "operations_management", "statistics"],
                "top_personal_descriptors"=>["creative", "resilient", "driven"],
                "top_motivations"=>["helping_others", "innovation", "personal_growth"],
                "top_workplace_strengths"=>["problem_solving", "professionalism", "relationship_building"],
                "bio"=>
                "I am the best greatest person ever ever ever you should hire me yesterday, sir. I am the best greatest person ever ever ever you should hire me yesterday, sir. I am the best greatest asdasdas asdsadas",
                "personal_fact"=>"Can flip you for real",
                "preferred_company_culture_descriptors"=>["best_in_class", "merit_based", "entrepreneurial"],
                "li_profile_url"=>nil,
                "resume_id"=>nil,
                "personal_website_url"=>"http://example.com",
                "blog_url"=>"http://example.com",
                "tw_profile_url"=>"http://example.com",
                "fb_profile_url"=>"http://example.com",
                "github_profile_url"=>"http://example.com",
                "job_sectors_of_interest"=>["consumer_products", "health_care", "manufacturing"],
                "employment_types_of_interest"=>["1-24", "100-499", "part_time", "contract"],
                "company_sizes_of_interest"=>["permanent", "contract", "internship", "25-99", "100-499"],
                "primary_areas_of_interest"=>["marketing", "human_resources", "product_management"],
                "place_id"=>"ChIJoyh8pLf7MIgRlIwC3eq9pFM",
                "place_details"=>
                {"locality"=>{"short"=>"Cleveland Heights", "long"=>"Cleveland Heights"},
                "administrative_area_level_2"=>{"short"=>"Cuyahoga County", "long"=>"Cuyahoga County"},
                "administrative_area_level_1"=>{"short"=>"OH", "long"=>"Ohio"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "formatted_address"=>"Cleveland Heights, OH, USA",
                'lng' => -81.694,
                'lat' => 41.499,
                "utc_offset"=>-240},
                "work_industry"=>"consulting",
                "salary_range"=>"prefer_not_to_disclose",
                "interested_in_joining_new_company"=>"very_interested",
                "locations_of_interest"=>["flexible"],
                "last_calculated_complete_percentage"=>100,
                "native_english_speaker" => false,
                "earned_accredited_degree_in_english" => false,
                "sufficient_english_work_experience" => false,
                "survey_years_full_time_experience" => '0_2',
                "survey_most_recent_role_description" => 'skilled_labor_role',
                "survey_highest_level_completed_education_description" => 'high_school'
            }

            professional_organizations = overrides.delete('professional_organizations') || []
            educational_organizations = overrides.delete('educational_organizations') || []
            skills = overrides.delete('skills') || SkillsOption.limit(4)
            awards_and_interests = overrides.delete('awards_and_interests') || CareerProfile::AwardsAndInterestsOption.limit(2)
            attrs.merge!(overrides)
            career_profile = CareerProfile.update_from_hash!(attrs)

            professional_organizations.each_with_index do |org, i|
                end_date = i == professional_organizations.size - 1 ? nil : Time.parse('1970/01/01') + (i+1).years
                featured = i == professional_organizations.size - 1 ? true : false
                CareerProfile::WorkExperience.create!({
                    career_profile_id: career_profile.id,
                    job_title: "Lion tamer",

                    # dates are important to ensure consistency of casper specs
                    start_date: Time.parse('1970/01/01') + i.years,
                    end_date: end_date.nil? ? end_date : Time.parse('1970/01/01') + (i+1).years,
                    responsibilities: ['A', 'B', 'C'],
                    professional_organization_option_id: org.id,
                    industry: featured ? 'education' : 'other',
                    role: 'other',
                    featured: featured
                })
            end

            educational_organizations.each_with_index do |org, i|
                career_profile.education_experiences.build({
                    career_profile_id: career_profile.id,

                    # dates are important to ensure consistency of casper specs
                    graduation_year: 1970 + i,
                    degree: "Degree",
                    major: "Major",
                    gpa: "1000",
                    gpa_description: "1337ness scale",
                    educational_organization_option_id: org.id,
                    degree_program: true,
                    will_not_complete: false,
                    official_transcript_required: i == educational_organizations.size - 1 ? true : false
                }).save!
            end


            career_profile.skills = skills
            career_profile.awards_and_interests = awards_and_interests
            career_profile.save!
            user.reload

            career_profile
        end

        # FIXME: can I move this and create_cohorts_for_program_types back to ruby?
        def create_user_with_subscriptions_enabled
            if !@cohort_with_plan
                create_cohorts_for_program_types('emba')
            end

            user_with_subscriptions_enabled = create_cohort_application_for_user(@cohort_with_plan, 'accepted')
            user_with_subscriptions_enabled.reload
            user_with_subscriptions_enabled.update({
                active_playlist_locale_pack_id: Playlist.first.locale_pack_id,
                email: 'user_with_subscriptions_enabled@pedago.com',
                password: 'password',
                password_confirmation: 'password',
                has_seen_welcome: true,
                has_seen_accepted: true,
                can_edit_career_profile: true,
                program_type_confirmed: true,
                fallback_program_type: 'emba',
                phone: '+12163714694',
                notify_candidate_positions: true,
                notify_candidate_positions_recommended: 'daily'
            })
            avatar_url = 'https://uploads-development.smart.ly/fixture_assets/avatar_1.png'
            User.connection.execute("UPDATE users SET avatar_url = '#{avatar_url}' where id = '#{user_with_subscriptions_enabled.id}'")
            identify(user_with_subscriptions_enabled, 'user_with_subscriptions_enabled')
            user_with_subscriptions_enabled
        end

        def create_cohorts_for_program_types(*program_types)
            subscription_progam_types = ['emba', 'the_business_certificate']

            @cohort_with_plan = nil

            # FIXME: It takes about 3 - 4 seconds to build a cohort fixture. It would be nice to chop this
            # list down, but a lot of specs will need to be tweaked.
            cohorts = program_types.map do |program_type|

                # determine stripe support and provide some simple
                stripe_plan_id = subscription_progam_types.include?(program_type) ? 'default_plan' : nil
                stripe_plans = nil
                scholarship_levels = nil
                if stripe_plan_id
                    stripe_plans = [{id: stripe_plan_id, amount: 80000, frequency: 'monthly' }]
                    scholarship_levels = get_proper_scholarship_levels
                end

                cohort = FactoryBot.create(:cohort, {
                    program_type: program_type,
                    stripe_plans: stripe_plans,
                    scholarship_levels: scholarship_levels
                })

                cohort.start_date = Time.parse('2026/07/01') # way in the future so that this can be promoted
                cohort.end_date = Time.parse('2026/09/01')

                if cohort.supports_slack_rooms?
                    (1..3).each do |i|
                        CohortSlackRoom.create!(
                            title: "Room #{i}",
                            url: "http://path/to/slack/room/#{i}/for/#{cohort.id}",
                            bot_user_access_token: "token_for_slack_room_#{i}_#{cohort.id}",
                            cohort_id: cohort.id
                        )
                    end
                end

                cohort.publish!

                stream = Lesson::Stream.all_published.where.not(exam:true).first

                create_user
                create_cohort_application_for_user(cohort, 'accepted', @user)

                add_content_to_cohort(cohort)

                # promote it via admission rounds
                if !cohort.supports_manual_promotion?
                    cohort.admission_rounds = [{
                        application_deadline_days_offset: -50,
                        decision_date_days_offset: -41
                    },{
                        application_deadline_days_offset: -30,
                        decision_date_days_offset: -21
                    }]
                end

                cohort.publish!

                # published_mba_cohort
                # published_emba_cohort
                identify(cohort, "published_#{program_type}_cohort")

                if cohort.supports_manual_promotion?
                    CohortPromotion.create!(cohort_id: cohort.id)
                end

                if Cohort.promoted_cohort(cohort.program_type).attributes['id'] != cohort.attributes['id']
                    raise "expected cohort to be promoted"
                end

                name = cohort.name
                AccessGroup.where(name: name).first_or_create!
                cohort.add_to_group(name)

                # create and identify test users with preset statuses for their cohort applications
                ['pending', 'accepted', 'rejected', 'pre_accepted', 'deferred', 'expelled'].each do |status|
                    identify(create_cohort_application_for_user(cohort, status), "#{status}_#{program_type}_cohort_user")
                end

                # Only emba cohorts have plans
                if program_type == 'emba'
                    @cohort_with_plan = cohort
                end

                cohort

            end

            # we have to run this so the cohort has a PublishedCohortContentDetail, which is needed for creating
            # a server events for the cohort applications
            RefreshMaterializedContentViews.refresh(true)
            RefreshMaterializedContentViews.tables.each do |klass|
                klass.rebuild
            end

            cohorts
        end

        def add_content_to_cohort(cohort, opts = {})
            create_stream_opts = {frame_list: opts[:frame_list]}
            frame_list = create_stream_opts[:frame_list]

            program_type = cohort.program_type
            # make sure we have 5 required playlists and 3 specialization playlists
            # in the cohort, each with different
            # streams.
            playlists = []
            8.times do |i|

                stream = create_stream(1, {title: "Stream p#{i}-1"}, create_stream_opts)
                stream.lessons.map(&:publish!)
                stream.publish!

                # Let's give the test cohort's foundation playlist a few more streams
                if program_type == 'test' && i == 0
                    stream2 = create_stream(1, {title: "Stream p#{i}-2"}, create_stream_opts)
                    stream2.publish!

                    stream3 = create_stream(1, {title: "Stream p#{i}-3"}, create_stream_opts)
                    stream3.publish!

                    stream4 = create_stream(1, {title: "Stream p#{i}-4"}, create_stream_opts)
                    stream4.publish!

                    playlist = create_playlist([stream, stream2, stream3, stream4])
                else
                    playlist = create_playlist([stream])
                end

                playlist.publish!
                playlists << playlist
            end

            required_playlist_pack_ids = playlists.slice(0, 5).map(&:locale_pack_id)
            cohort.playlist_collections = [{title: "Playlist for #{cohort.name}", required_playlist_pack_ids: required_playlist_pack_ids}]
            cohort.specialization_playlist_pack_ids = playlists.slice(5, 3).map(&:locale_pack_id)

            schedule_streams = []
            2.times do |i|
                stream = create_stream(1, {title: "Stream #{i}"}, create_stream_opts)
                stream.lessons.map(&:publish!)
                stream.publish!
                schedule_streams << stream
            end

            if cohort.supports_schedule?
                # all of the required playlists, except for the last one,
                # are required in the schedule
                (cohort.required_playlist_pack_ids - [cohort.required_playlist_pack_ids.last]).each do |playlist_pack_id|
                    playlist = playlists.detect { |p| p.locale_pack_id == playlist_pack_id }
                    cohort.periods << {
                        "title" => "period to do playlist #{playlist.title}",
                        "stream_entries" => playlist.stream_locale_pack_ids.map do |pack_id|
                            {
                                "locale_pack_id" =>  pack_id,
                                "required" => true,
                                "caption" => "asdas"
                            }
                        end,
                        "days" => 7,
                        "days_offset" => 0,
                        "style" => "standard"
                    }
                end

                # there are a couple streams that are in the schedule
                # but not in any playlists
                cohort.periods << {
                    "title" => 'period with non-playlist courses',
                    "stream_entries" => [
                        {
                            "locale_pack_id" =>  schedule_streams.first.locale_pack_id,
                            "required" => true,
                            "caption" => "asdas"
                        },
                        {
                            "locale_pack_id" =>  schedule_streams.second.locale_pack_id,
                            "required" => false,
                            "caption" => "asdas"
                        }
                    ],
                    "days" => 7,
                    "days_offset" => 0,
                    "style" => "standard",
                }

                if frame_list
                    exam_stream = create_stream(true, {
                        exam: true,
                        time_limit_hours: 42,
                        title: "Exam Stream 1"
                    }, create_stream_opts)
                    exam_stream.lessons.each do |lesson|
                        lesson.test = true
                        lesson.publish!({update_published_at: true})
                    end
                    exam_stream.publish!({update_published_at: true})

                    assessment_stream = create_stream(true, {
                        exam: false,
                        title: "Assessment Stream 1"
                    }, create_stream_opts)
                    assessment_stream.lessons.each do |lesson|
                        lesson.assessment = true
                        lesson.publish!({update_published_at: true})
                    end
                    assessment_stream.publish!({update_published_at: true})
                else
                    assessment_stream = @assessment_stream
                    exam_stream = @exam_stream
                end

                # assessment period
                cohort.periods << {
                    "title" => "assessment period",
                    "stream_entries" => [
                        {
                            "locale_pack_id" =>  assessment_stream.locale_pack_id,
                            "required" => true,
                            "caption" => "asdas"
                        }
                    ],
                    "style" => 'standard',
                    "days" => 7,
                    "days_offset" => 0,
                    "additional_specialization_playlists_to_complete" => 0
                }

                # exam period
                cohort.periods << {
                    "title" => "exam period",
                    "stream_entries" => [
                        {
                            "locale_pack_id" =>  exam_stream.locale_pack_id,
                            "required" => true,
                            "caption" => "asdas"
                        }
                    ],
                    "style" => 'exam',
                    "exam_style" => 'final',
                    "days" => 7,
                    "days_offset" => 0,
                    "additional_specialization_playlists_to_complete" => 0
                }

                # project period
                cohort.periods << {
                    "title" => "project period",
                    "stream_entries" => [],
                    "style" => 'project',
                    "project_style" => "standard",
                    "days" => 7,
                    "days_offset" => 0,
                    "learner_project_ids" => [@learner_project.id]
                }
            end
        end

        def get_proper_scholarship_levels
            [
                {"name": "No Scholarship",      "standard": {"default_plan": {"id": "none", "amount_off": 0, "percent_off": 0} },                    "early": {"default_plan": {"id": "none", "amount_off": 0, "percent_off": 0} } },
                {"name": "Level 1",             "standard": {"default_plan": {"id": "discount_150", "amount_off": 15000, "percent_off": nil} },        "early": {"default_plan": {"id": "discount_150", "amount_off": 25000, "percent_off": nil} } },
                {"name": "Level 2",             "standard": {"default_plan": {"id": "discount_250", "amount_off": 25000, "percent_off": nil} },        "early": {"default_plan": {"id": "discount_250", "amount_off": 35000, "percent_off": nil} } },
                {"name": "Level 3",             "standard": {"default_plan": {"id": "discount_350", "amount_off": 35000, "percent_off": nil} },        "early": {"default_plan": {"id": "discount_350", "amount_off": 45000, "percent_off": nil} } },
                {"name": "Level 4",             "standard": {"default_plan": {"id": "discount_450", "amount_off": 45000, "percent_off": nil} },        "early": {"default_plan": {"id": "discount_450", "amount_off": 55000, "percent_off": nil} } },
                {"name": "Level 5",             "standard": {"default_plan": {"id": "discount_550", "amount_off": 55000, "percent_off": nil} },        "early": {"default_plan": {"id": "discount_550", "amount_off": 65000, "percent_off": nil} } },
                {"name": "Full Scholarship",    "standard": {"default_plan": {"id": "discount_100_percent", "amount_off": nil, "percent_off": 100} },  "early": {"default_plan": {"id": "discount_100_percent", "amount_off": nil, "percent_off": 100} } },
            ]
        end

        def create_candidate(user_overrides = {})
            user = create_user(user_overrides)
            user.ensure_institution(Institution.quantic)
            user.active_institution = Institution.quantic
            complete_career_profile(user)
            user.update_attribute(:can_edit_career_profile, true)
            user
        end

        def create_open_position!(attrs = {})
            OpenPosition.create!({
                # hiring_manager_id: MUST_BE_PASSED_IN,
                created_at: Time.parse('2018/08/04'),
                auto_expiration_date: Date.parse('2018/08/04'),
                title: "Some job",
                salary: 42000,
                perks: ["bonus", "remote"],
                available_interview_times: 'When the moon is high',
                place_id: "ChIJoyh8pLf7MIgRlIwC3eq9pFM",
                place_details:
                    {"locality"=>{"short"=>"Cleveland Heights", "long"=>"Cleveland Heights"},
                    "administrative_area_level_2"=>{"short"=>"Cuyahoga County", "long"=>"Cuyahoga County"},
                    "administrative_area_level_1"=>{"short"=>"OH", "long"=>"Ohio"},
                    "country"=>{"short"=>"US", "long"=>"United States"},
                    "formatted_address"=>"Cleveland Heights, OH, USA",
                },
                role: 'data_science',
                job_post_url: 'http://example.com',
                short_description: 'Coffee runner',
                description: 'Mostly just getting coffee and stuff.',
                desired_years_experience: {"min"=>3, "max"=>6},
                featured: true,
                archived: false,
                position_descriptors: ["permanent", "part_time"]
            }.merge(attrs))
        end

    end
end