require Rails.root.join('spec', 'report_spec_helper')
require File.expand_path("./../is_back_royal_fixture_builder", __FILE__)


# FIXME: We would like to refactor all of our fixtures to hard-code IDs and timestamps such that re-running of `rake test:build_fixtures`
# produces idempotent YAML output. This would not only eliminate the requirement that dozens of YAML files are committed each update
# (making mreview difficult) but would fix a couple of known Casper screenshotting issues around last-field ID ordering in certain queries.
# see also: https://trello.com/c/VSX5n1sC

module FixtureBuilder
    class RubyFixtureBuilder < Fixturies
        include FixtureBuilder::IsBackRoyalFixtureBuilder

        # Use `set_fixtures_directory` to tell Fixturies
        # where to put all of the fixtures files
        set_fixtures_directory Rails.root.join('spec', 'fixtures')

        set_fixture_class lesson_streams: Lesson::Stream,
            lesson_streams_progress:  Lesson::StreamProgress,
            education_experiences: CareerProfile::EducationExperience

        # Build out various models ...

        build do
            EditorTracking.global_user = "FixtureBuilder"
        end

        build do
            lessons = (0..2).map { create_lesson }
            identify(lessons[0], 'unpublished_lesson')
            @unpublished_lesson = lessons[0]

            stream = create_frame_list_stream
            identify(stream.lessons[1], 'frame_list_lesson')
            @stream_with_frame_list_lesson = stream
            identify(stream, 'stream_with_frame_list_lesson')

            lessons[2].publish!
            identify(lessons[2], 'published_lesson_without_stream')
        end

        build do
            FactoryBot.create_list(:user, 4)

            # create a user to be the current user who is not also used for other stuff
            current_user = FactoryBot.create(:user, {
                email: 'current_user@example.com'
            })
            current_user.roles.destroy_all
            identify(current_user, 'current_user')

            editor = FactoryBot.create(:user)
            editor.add_role(:editor)
            identify(editor, 'editor')

            lessons = [create_lesson, create_lesson, create_lesson]
            ['lesson_editor', 'previewer', 'reviewer'].each_with_index do |role, i|
                editor.roles << Role.create!(
                    name: role,
                    resource_type: 'Lesson',
                    resource_id: lessons[i].id
                )
                identify(lessons[i], "editor_has_#{role}")
            end
            editor.save!

            identify(create_lesson(author_id: editor.id), "editor_created")

            # create one other eidtor role to check to filtering.  See https://trello.com/c/wDRQCzHF/1885-bug-linda-a-contract-writer-can-t-edit-lessons-that-weren-t-written-by-her
            # and filter_editable_resources
            another_editor = FactoryBot.create(:user)
            another_editor.add_role(:editor)
            identify(another_editor, 'another_editor')
            lesson = create_lesson
            another_editor.roles << Role.create!(
                name: 'lesson_editor',
                resource_type: 'Lesson',
                resource_id: lesson.id
            )

            @learner = learner = FactoryBot.create(:user)
            learner.add_role(:learner)
            identify(learner, 'learner')

            @admin_user = FactoryBot.create(:user, {:email => 'admin@pedago.com'}) # used in integration specs
            @admin_user.add_role(:admin)
            identify(@admin_user, 'admin')

            @super_editor = FactoryBot.create(:user)
            @admin_user.add_role(:super_editor)
            identify(@super_editor, 'super_editor')

            @facebook_user = FactoryBot.create(:user, {
                provider: 'facebook'
            })
            identify(@facebook_user, 'facebook_user')

            @google_user = FactoryBot.create(:user, {
                provider: 'google_oauth2'
            })
            identify(@google_user, 'google_user')

            @apple_user = FactoryBot.create(:user, {
                provider: 'apple_quantic'
            })
            identify(@apple_user, 'apple_user')


            # need a mba user with no cohort application
            user = FactoryBot.create(:user, {sign_up_code: 'FREEMBA'})
            user.register_content_access
            user.save!

            # need a demo user with no cohort application
            user = FactoryBot.create(:user, {sign_up_code: 'DEMO'})
            user.register_content_access
            user.save!
            raise "expected fallback_program_type=demo" unless user.fallback_program_type == 'demo'
        end

        build do
            streams = (1..8).map do |i|
                create_stream
            end
            identify(streams[0], 'unpublished_stream')


            @stream_with_many_versions = streams[1]
            identify(@stream_with_many_versions, 'stream_with_many_versions')

            @stream_with_many_versions.update!(title: 'updated')
            @stream_with_many_versions.was_published = true # normally happens in update_from_hash!
            @stream_with_many_versions.ensure_locale_pack
            @stream_with_many_versions.publish!({update_published_at: true})
            @stream_with_many_versions.was_published = false # normally happens in update_from_hash!
            @stream_with_many_versions.update!(title: 'updated again')

            stream_with_all_groups = create_stream(true)
            AccessGroup.all.each do |access_group|
                stream_with_all_groups.ensure_locale_pack.add_to_group(access_group.name)
            end
            stream_with_all_groups.save!
            stream_with_all_groups.lessons[0].publish!

            identify(stream_with_all_groups, 'stream_with_all_groups')
            @stream_with_all_groups = stream_with_all_groups

            published_stream = create_stream(true)
            published_stream.ensure_locale_pack

            # publish all but one lesson
            published_stream.lessons.to_a.slice(1, 999999).each do |lesson|
                lesson.title = "published title #{rand}"
                lesson.publish!({update_published_at: true})
                lesson.title = "working title #{rand}"
                lesson.save!
            end
            published_stream.publish!({update_published_at: true})
            @published_stream = published_stream
            identify(@published_stream, 'published_stream')

            blue_ocean_stream = streams[4]
            blue_ocean_stream.ensure_locale_pack.add_to_group('BLUEOCEAN')
            blue_ocean_stream.publish!({update_published_at: true})
            identify(blue_ocean_stream, 'blue_ocean_stream')
            @blue_ocean_stream = blue_ocean_stream

            hiring_stream = streams[7]
            hiring_stream.ensure_locale_pack.add_to_group('HIRING')
            hiring_stream.publish!({update_published_at: true})
            identify(hiring_stream, 'hiring_stream')

            @non_exam_stream = create_stream(true, {
                exam: false
            })
            @non_exam_stream.lessons.map(&:publish!)
            @non_exam_stream.publish!({update_published_at: true})
            identify(@non_exam_stream, 'non_exam_stream')

        end

        build do
            FactoryBot.create_list(:event, 8)
        end

        build do
            FactoryBot.create_list(:redshift_event, 8)

            event = FactoryBot.create(:redshift_event)
            identify(event, 'event_not_in_a_frame_event')

            user = FactoryBot.build(:user, {
                email: 'user_with_page_load_events@pedago.com'
            })
            user.save!
            page_load_events = FactoryBot.create_list(:redshift_event, 4)

            # mobile web
            page_load_events[0].client = 'web'
            page_load_events[0].user_agent = 'Mozilla/5.0 (iPad; CPU OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) CriOS/30.0.1599.12 Mobile/11A465 Safari/8536.25 (3B92C18B-D9DE-4CB7-A02A-22FD2AF17C8F)'

            # mobile app
            page_load_events[1].cordova = true
            page_load_events[1].client = 'ios'
            page_load_events[1].user_agent = 'Mozilla/5.0 (iPad; CPU OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) CriOS/30.0.1599.12 Mobile/11A465 Safari/8536.25 (3B92C18B-D9DE-4CB7-A02A-22FD2AF17C8F)'

            # desktop
            page_load_events[2].client = 'web'
            page_load_events[2].user_agent = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'

            # unknown
            page_load_events[3].client = 'web'
            page_load_events[3].user_agent = 'unrecognized'

            page_load_events.each do |ev|
                ev.event_type = 'page_load:load'
                ev.user_id = user.id
                ev.save!
            end

        end

        build do
            LessonProgress.create_or_update!({
                :locale_pack_id => Lesson.all_published.first.locale_pack_id,
                :frame_bookmark_guid => SecureRandom.uuid,
                :user_id => @learner.id
            })
        end

        build do
            ContentTopic.create!(locales: { en: 'A topic' })
            ContentTopic.create!(locales: { en: 'Another topic' })
        end

        build do
            count = 5
            streams = setup_locale_pack((1..count).map { create_stream }, false)

            # setup some more streams because we need 2 in the playlists for user_with_active_playlist
            another_en_stream = create_stream
            another_en_stream.ensure_locale_pack
            another_es_stream = create_stream
            another_es_stream.locale = 'es'
            another_es_stream.locale_pack = another_en_stream.locale_pack
            another_zh_stream = create_stream
            another_zh_stream.locale = 'zh'
            another_zh_stream.locale_pack = another_en_stream.locale_pack
            other_streams = [another_en_stream, another_es_stream, another_zh_stream]
            other_streams.each(&:publish!)

            playlists = setup_locale_pack((1..count).map { |i|
                create_playlist([
                    streams[0],
                    other_streams[0]
                ].compact)
            }, false)
            @playlist_locale_pack_with_three_locales = playlists[0].locale_pack

            identify(playlists[0], "playlist_with_multiple_locales")

            # we need a unpublished spanish items that are not
            # yet in locale packs for the is_content_item specs
            create_playlist([another_es_stream])
            create_stream(true, {locale: 'es'})
            create_lesson({locale: 'es'})

        end

        def combine_in_locale_pack(*items)
            locale_pack = items.first.ensure_locale_pack
            items.to_a.slice(1, 99999).each do |item|
                item.locale_pack = locale_pack
            end
            items.map(&:save!)
            items
        end

        def setup_locale_pack(items, auto_identify = true)
            klass = items.first.class
            en_item, es_item, zh_item, en_only_item, es_only_item = items
            en_item.locale = 'es'
            locale_pack = klass::LocalePack.new
            locale_pack.add_to_group('SUPERVIEWER') if locale_pack.respond_to?(:add_to_group)
            locale_pack.save!
            en_item.locale_pack = es_item.locale_pack = zh_item.locale_pack = locale_pack

            en_item.locale = 'en'
            en_item.save!
            identify(en_item, 'en_item') if auto_identify
            es_item.locale = 'es'
            es_item.save!
            identify(es_item, 'es_item')if auto_identify
            zh_item.locale = 'zh'
            zh_item.save!
            identify(zh_item, 'zh_item')if auto_identify

            locale_pack.add_to_group('SUPERVIEWER') if locale_pack.respond_to?(:add_to_group)

            if en_only_item
                en_only_item.locale = 'en'
                en_only_item.ensure_locale_pack
                en_only_item.save!
                identify(en_only_item, 'en_only_item') if auto_identify
            end

            if es_only_item
                es_only_item.locale = 'es'
                es_only_item.locale_pack = nil;
                es_only_item.save!
                identify(es_only_item, 'es_only_item') if auto_identify
                raise "No locale pack expected" unless es_only_item.locale_pack.nil?
            end


            [en_item, es_item, zh_item, en_only_item].compact.map(&:publish!)

            items
        end

        build do
            count = 5
            setup_locale_pack (1..count).map { create_lesson }
            streams = setup_locale_pack((1..count).map { create_stream })
            playlists = (1..count).map do |i|
                create_playlist([streams[0]])
            end
            setup_locale_pack(playlists)

            # we need two playlists without locale_packs for is_content_item specs
            stream = Lesson::Stream.where(locale: 'es').first
            (1..2).map do
                unless create_playlist([stream]).locale_pack.nil?
                    raise "expecting no locale_pack"
                end
            end

            # we need 2 streams without locale_packs for is_content_item specs
            unless create_stream(false, {locale: 'es'}).locale_pack.nil?
                raise "expecting no locale_pack"
            end

            # we need a stream that is only published in spanish
            unpublished_en = create_stream(false, locale: 'en')
            published_only_in_es = create_stream(false, locale: 'es')
            published_only_in_es.locale_pack = unpublished_en.locale_pack
            published_only_in_es.publish!
            identify(published_only_in_es, 'published_only_in_es')

            # we need a playlist that is only published in spanish
            unpublished_en = create_playlist([@published_stream])
            published_only_in_es = create_playlist([@published_stream])
            published_only_in_es.locale = 'es'
            published_only_in_es.locale_pack = unpublished_en.locale_pack
            published_only_in_es.publish!
            identify(published_only_in_es, 'published_only_in_es')

        end

        def create_and_publish_exam_stream
            exam = create_stream(true, {
                exam: true,
                time_limit_hours: 42
            })
            exam.lessons.each do |lesson|
                lesson.test = true
                lesson.publish!({update_published_at: true})
            end
            exam.publish!({update_published_at: true})
            exam
        end

        build do

            cohorts = create_cohorts_for_program_types(
                'mba',
                'emba',
                'test',
                'career_network_only',
                'the_business_certificate'
            )

            @published_mba_cohort, @published_emba_cohort = cohorts.slice(0, 2)

            # needed in report_spec.rb
            @user = create_cohort_application_for_user(@published_mba_cohort, 'accepted', nil, 'graduated')

            unpublished_cohort = FactoryBot.create(:cohort)
            identify(unpublished_cohort, 'unpublished_cohort')

            cohort = FactoryBot.create(:cohort, {program_type: 'mba'})
            cohort.publish!

            identify(cohort, 'published_mba_cohort_with_no_courses')

            # midterm exam stream
            midterm = create_and_publish_exam_stream
            identify(midterm, "midterm_exam")

            # final exam stream
            final_exam = create_and_publish_exam_stream
            identify(final_exam, "final_exam")

            cohort = FactoryBot.create(:cohort, {program_type: 'mba'})
            streams = Lesson::Stream.all_published.where(locale: 'en')
            exam_streams = (streams.select(&:exam) - [midterm, final_exam])
            streams = streams.reject(&:exam)
            cohort.periods = [
                {
                    style: 'standard',
                    stream_entries: [{
                        locale_pack_id: streams.pop.locale_pack_id,
                        required: true
                    }, {
                        locale_pack_id: streams.pop.locale_pack_id,
                        required: false
                    }],
                    days_offset: 0,
                    days: 7
                },
                {
                    style: 'exam',
                    exam_style: 'review',
                    stream_entries: [{
                        locale_pack_id: streams.pop.locale_pack_id,
                        required: true
                    }],
                    days_offset: 0,
                    days: 7
                },
                {
                    style: 'exam',
                    exam_style: 'intermediate',
                    stream_entries: [
                        {
                            locale_pack_id: streams.pop.locale_pack_id,
                            required: false
                        },
                        {
                            locale_pack_id: midterm.locale_pack_id,
                            required: true
                        }
                    ],
                    days_offset: 0,
                    days: 7
                },
                {
                    style: 'standard',
                    stream_entries: [{
                        locale_pack_id: streams.pop.locale_pack_id,
                        required: true
                    }, {
                        locale_pack_id: streams.pop.locale_pack_id,
                        required: false
                    }],
                    days_offset: 0,
                    days: 7
                },
                {
                    style: 'exam',
                    exam_style: 'final',
                    stream_entries: [{
                        locale_pack_id: final_exam.locale_pack_id,
                        required: true
                    }],
                    days_offset: 0,
                    days: 7
                }
            ]
            cohort.publish!
            identify(cohort, 'published_mba_cohort_with_exams')

            # Add paid certificate cohorts
            cohort = FactoryBot.create(:cohort, {
                id: 'bceb59b6-1fc1-43e5-b6a7-7b8252540013', # hardcoding for use in application-status casper specs
                program_type: 'paid_cert_data_analytics',
                description: 'The path of the righteous man is beset on all sides by the inequities of the selfish and the tyranny of evil men.',
                title: 'Data Analytics',
                stripe_plans: [{id: 'paid_cert_stripe_plan', amount: 160000, frequency: 'once' }]
            })
            cohort.learner_project_ids = [@learner_project.id]
            cohort.publish!
            CohortPromotion.create!(cohort_id: cohort.id)
            identify(cohort, 'published_paid_cert_data_analytics_cohort')

            cohort = FactoryBot.create(:cohort, {
                program_type: 'paid_cert_competitive_strategy',
                title: 'Competitive Strategy',
                description: 'The path of the righteous man is beset on all sides by the inequities of the selfish and the tyranny of evil men.',
                stripe_plans: [{id: 'paid_cert_stripe_plan', amount: 160000, frequency: 'once' }]
            })
            cohort.publish!
            CohortPromotion.create!(cohort_id: cohort.id)
            identify(cohort, 'published_paid_cert_competitive_strategy_cohort')

            # Add jordanian_math cohort for Miya Miya institution
            cohort = FactoryBot.create(:cohort, {
                program_type: 'jordanian_math',
                title: 'For Miya Miya',
                description: 'The path of the righteous man is beset on all sides by the inequities of the selfish and the tyranny of evil men.',
            })
            cohort.publish!
            CohortPromotion.create!(cohort_id: cohort.id)
            identify(cohort, "published_#{cohort.program_type}_cohort")
        end

        build do
            create_user_with_subscriptions_enabled
        end

        # Subscription
        build do
            user = create_user

            # make sure we don't hit stripe
            SubscriptionUserJoin.skip_callback('create', :after, :ensure_stripe_customer_current)
            subscription = Subscription.new(
                stripe_plan_id: 'default_plan',
                stripe_subscription_id: 'fake_fixture_subscription',
                past_due: true,
                stripe_product_id: 'fake',
            )
            subscription.user = user
            subscription.save!
        end

        # special cohort for slack_room_assignment spec
        build do
            cohort = FactoryBot.create(:cohort, {
                program_type: 'emba',
                stripe_plans: [{id: 'default_plan', amount: 80000, frequency: 'monthly' }],
                scholarship_levels: get_proper_scholarship_levels.select {|s| ['Full Scholarship', 'No Scholarship'].include?(s[:name]) }
            })

            (1..2).each do |i|
                CohortSlackRoom.create!(
                    title: "Room #{i}",
                    url: "http://path/to/slack/room/#{i}/for/#{cohort.id}",
                    bot_user_access_token: "token_for_slack_room_#{i}_#{cohort.id}",
                    cohort_id: cohort.id
                )
            end
            cohort.publish!

            sexes = ['male', 'female', 'no_identify']
            full_scholarships = [true, false]
            full_scholarship_level = get_proper_scholarship_levels.detect {|s| s[:name] == 'Full Scholarship' }
            locations_for_slack_room_assignment_spec.each do |entry|

                sex = sexes.pop
                sexes.unshift(sex)

                full_scholarship = full_scholarships.pop
                full_scholarships.unshift(full_scholarship)

                user = FactoryBot.create(:user, {
                    sex: sex
                })
                user.ensure_career_profile
                user.career_profile.assign_attributes(entry)
                user.career_profile.save!
                attrs = {}
                if full_scholarship
                    attrs[:scholarship_level] = full_scholarship_level
                end
                create_cohort_application_for_user(cohort, 'pre_accepted', user, 'pending', attrs)
            end

            identify(cohort, 'cohort_for_slack_room_assignment_spec')
        end

        build do
            hiring_team = HiringTeam.create!(title: 'The Browns', domain: 'browns.com')
            identify(hiring_team, 'hiring_team')

            professional_organization = ProfessionalOrganizationOption.create!({text: "The Brown Company", suggest: true})

            hiring_manager_with_team = FactoryBot.create(:user)
            hiring_manager_with_team.professional_organization = professional_organization
            hiring_manager_with_team.hiring_team = hiring_team
            hiring_team.owner = hiring_manager_with_team
            hiring_team.save!
            hiring_manager_with_team.save!
            HiringApplication.create!(
                user_id: hiring_manager_with_team.id,
                status: 'accepted'
            )

            OpenPosition.create!(
                hiring_manager_id: hiring_manager_with_team.id,
                title: 'My job'
            )

            @featured_positions = []

            # Fixing the created_at ensures consistent ordering in UI
            created_at = Time.now - 1.day
            1.upto(8) do |i|
                attrs = {
                    hiring_manager_id: hiring_manager_with_team.id,
                    title: "Featured job #{i}",
                    featured: true,
                    created_at: created_at
                }

                # create some of them without the extra fields filled out
                # so we can have screenshots of how those look
                if i % 2 == 0
                    attrs[:salary] = nil
                    attrs[:available_interview_times] = nil
                    attrs[:description] = nil
                    attrs[:desired_years_experience] = {}
                    attrs[:position_descriptors] = []
                end
                @featured_positions << create_open_position!(attrs)
            end
            identify(hiring_manager_with_team, 'hiring_manager_with_team')

            career_profiles = CareerProfile.limit(3)

            # has a relationship already with one user
            hiring_manager_with_team.ensure_candidate_relationships([career_profiles.first.user_id])
            hiring_relationship = hiring_manager_with_team.candidate_relationships.first
            hiring_relationship.update(hiring_manager_status: 'accepted', candidate_status: 'accepted')
            conversation = create_conversation(hiring_relationship, hiring_manager_with_team)

            # another candidate has expressed interest in some positions
            CandidatePositionInterest.create!(
                candidate_id: career_profiles.second.user_id,
                open_position_id: @featured_positions[0].id,
                candidate_status: 'accepted',
                hiring_manager_status: 'unseen'
            )
            CandidatePositionInterest.create!(
                candidate_id: career_profiles.second.user_id,
                open_position_id: @featured_positions[1].id,
                candidate_status: 'accepted',
                hiring_manager_status: 'hidden'
            )
            CandidatePositionInterest.create!(
                candidate_id: career_profiles.second.user_id,
                open_position_id: @featured_positions[2].id,
                candidate_status: 'accepted',
                hiring_manager_status: 'pending'
            )

            hiring_manager_teammate = FactoryBot.create(:user)
            hiring_manager_teammate.professional_organization = professional_organization
            hiring_manager_teammate.hiring_team = hiring_team
            hiring_manager_teammate.save!
            HiringApplication.create!(
                user_id: hiring_manager_teammate.id,
                status: 'accepted'
            )
            OpenPosition.create!(
                hiring_manager_id: hiring_manager_teammate.id,
                title: 'My teammate\'s job'
            )
            identify(hiring_manager_teammate, 'hiring_manager_teammate')
            update_with_new_message(conversation, hiring_manager_teammate)
        end

        # must come after Cohort
        build do
            playlists = []
            3.times do
                playlists << create_playlist
            end

            playlist_with_no_groups = playlists[0]
            identify(playlist_with_no_groups, 'playlist_with_no_groups')

            playlist_with_all_groups = playlists[1]
            playlist_with_all_groups.streams_for_all_locales.map(&:publish!)
            AccessGroup.all.each do |access_group|
                playlist_with_all_groups.ensure_locale_pack.add_to_group(access_group.name)
            end
            identify(playlist_with_all_groups, 'playlist_with_all_groups')
            playlist_with_all_groups = Playlist.update_from_hash!(@admin_user, {
                id: playlist_with_all_groups.id,
                updated_at: playlist_with_all_groups.updated_at.to_timestamp + 1
            }, {
                should_publish: true,
            })
            @playlist_with_all_groups = playlist_with_all_groups


            published_playlist = playlists[2]
            published_playlist.ensure_locale_pack
            published_playlist.locale_pack.access_groups = @published_stream.access_groups

            published_streams = Lesson::Stream.all_published
                                    .where.not(locale_pack: nil)
                                    .where(locale: 'en').to_a
            raise "Expecting at least 3 published streams" unless published_streams.uniq.size >= 3
            published_playlist.stream_entries = published_streams.to_a.slice(0, 2).map do |stream|
                {
                    stream_id: stream.id,
                    locale_pack_id: stream.locale_pack_id,
                    description: "stream #{stream.title.inspect}"
                }
            end
            raise "Expected 2 streams" unless published_playlist.stream_entries.size == 2
            published_playlist.publish!
            identify(published_playlist, 'published_playlist')

            user_with_active_playlist = FactoryBot.create(:user)
            raise "playlist_locale_pack_with_three_locales not yet defined" unless @playlist_locale_pack_with_three_locales
            user_with_active_playlist.active_playlist_locale_pack = @playlist_locale_pack_with_three_locales

            # make sure there are favorite streams both in and
            # not in this playlist
            streams_not_in_playlist = []
            streams_in_playlist = []
            playlist = user_with_active_playlist.active_playlists.where(locale: 'en').first

            # go through streams that have both english and spanish
            # and setup some favorites
            published_streams
                .select { |s| s.locale == playlist.locale }
                .select { |s| s.locale_pack.content_items.where(locale: 'es').exists? }
                .select { |s| s.locale_pack.content_items.where(locale: 'en').exists? }
                .each do |stream|
                if !playlist.stream_locale_packs.include?(stream.locale_pack)
                    streams_not_in_playlist << stream
                else
                    streams_in_playlist << stream
                end
            end
            streams_not_in_playlist = [streams_not_in_playlist.first].compact
            raise "Did not find a stream that is not in playlist" unless streams_not_in_playlist.size == 1
            raise "Did not find 2 streams in the playlist" unless streams_in_playlist.size > 1
            streams_in_playlist = [streams_in_playlist.first]

            (streams_not_in_playlist + streams_in_playlist).each do |stream|
                user_with_active_playlist.bookmark_stream_locale_pack(stream.locale_pack)
            end

            user_with_active_playlist.add_to_group 'SUPERVIEWER'


            completed_streams = [
                combine_in_locale_pack(create_stream(false, {locale: :en}), create_stream(false, {locale: :es})),
                combine_in_locale_pack(create_stream(false, {locale: :en}), create_stream(false, {locale: :es}))
            ].flatten

            completed_streams.each(&:publish!)
            en_completed_streams = completed_streams.select { |s| s.locale == 'en' }
            Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => en_completed_streams[0].locale_pack_id,
                :lesson_bookmark_id => en_completed_streams[0].lessons[0].id,
                :user_id => user_with_active_playlist.id,
                :complete => true,
                :completed_at => Time.now,
                :certificate_image_id => FactoryBot.create(:s3_asset).id
            })
            Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => en_completed_streams[1].locale_pack_id,
                :lesson_bookmark_id => en_completed_streams[1].lessons[0].id,
                :user_id => user_with_active_playlist.id,
                :complete => true,
                :completed_at => Time.now,
                :certificate_image_id => FactoryBot.create(:s3_asset).id
            })

            # unfavorite one completed stream.  it should still show up in student dashboard
            user_with_active_playlist.favorite_lesson_stream_locale_packs = user_with_active_playlist.favorite_lesson_stream_locale_packs - [completed_streams[0].locale_pack]

            identify(user_with_active_playlist, "user_with_active_playlist")
            user_with_active_playlist.save!

            # create a playlist with only one stream that is in turn only one playlist
            stream_in_one_playlist = create_stream
            stream_in_one_playlist.title = 'stream_in_one_playlist'
            stream_in_one_playlist.locale = 'en'
            stream_in_one_playlist.publish!

            playlist_with_one_stream = create_playlist
            playlist_with_one_stream.title = 'playlist_with_one_stream'
            playlist_with_one_stream.locale = 'en'
            playlist_with_one_stream.stream_entries = [{
                stream_id: stream_in_one_playlist.id,
                locale_pack_id: stream_in_one_playlist.locale_pack_id,
                description: "stream #{stream_in_one_playlist.title.inspect}"
            }]
            playlist_with_one_stream.ensure_locale_pack
            playlist_with_one_stream.publish!
        end

        # Institutions (must come after playlists)
        build do
            institutions = FactoryBot.create_list(:institution, 3)

            institution_with_streams = institutions[0]
            name = institution_with_streams.name
            AccessGroup.where(name: name).first_or_create!
            institution_with_streams.add_to_group(name)
            user = create_user
            user.institutions << institution_with_streams
            user.save!
            stream = @stream_with_all_groups
            stream.locale_pack.add_to_group(name)
            identify(institution_with_streams, 'institution_with_streams')

            institution_with_playlists = institutions[1]
            playlist_pack_ids = Playlist.all_published.reorder(nil).distinct.limit(2).pluck(:locale_pack_id)
            while playlist_pack_ids.size < 2
                playlist = create_playlist
                playlist.publish!
                playlist_pack_ids << playlist.locale_pack_id
            end
            raise "Not enough playlists" unless playlist_pack_ids.size == 2
            institution_with_playlists.playlist_pack_ids = playlist_pack_ids
            institution_with_playlists.save!
            user = create_user
            user.institutions << institution_with_playlists
            user.save!
            identify(institution_with_playlists, 'institution_with_playlists')

            boshigh_institution = FactoryBot.create(:institution, {
                sign_up_code: 'BOSHIGH',
                name: 'BOSHIGH'
            })
            boshigh_institution = Institution.update_from_hash!(boshigh_institution.as_json.merge({
                'groups' => [{'id' => @blue_ocean_stream.locale_pack.groups.first.id}]
            }))
            identify(boshigh_institution, 'boshigh_institution')

            OMNIAUTH_SAML_PROVIDERS.each do |provider|
                institution = FactoryBot.create(:institution, {
                    sign_up_code: provider,
                    name: provider.titleize
                })
                identify(institution, provider)
            end

            institution = FactoryBot.create(:institution)
            @institutional_reports_viewer = FactoryBot.build(:user, {
                :email => 'institutional_admin@pedago.com'
            }) # used in integration specs
            @institutional_reports_viewer.institutions << institution
            institution.reports_viewers << @institutional_reports_viewer
            identify(@institutional_reports_viewer, 'institutional_reports_viewer')

            another_institution_user = FactoryBot.build(:user) # used in integration specs
            another_institution_user.institutions << institution
            another_institution_user.save!

            # some stream progress for these learners
            complete_stream(stream, @institutional_reports_viewer, {
                completed_at: Time.now - 2.day
            })
            complete_stream(stream, another_institution_user, {
                completed_at: Time.now - 1.day
            })
        end

        # must come after Cohort
        build do
            certificate_images = FactoryBot.create_list(:s3_asset, 2)
            identify(certificate_images[0], 'certificate_image_1')
            identify(certificate_images[1], 'certificate_image_2')

            stream =  @published_stream
            Lesson::StreamProgress.create_or_update!({
                :locale_pack_id => stream.locale_pack_id,
                :lesson_bookmark_id => stream.lessons.first.id,
                :user_id => @learner.id
            })

            stream_completer = @learner
            completed = complete_stream(stream, stream_completer)
            identify(completed, 'completed_stream_progress')

            # we need one that is started and not completed
            another_stream = Lesson::Stream.all_published.where.not(locale_pack_id: stream.id).first
            start_stream(another_stream, stream_completer)
        end

        # practice lessons. must come after cohort
        build do
            # without_practice_locale_pack
            lessons = (1..3).map { create_lesson }
            setup_locale_pack(lessons, false)
            identify(lessons[0], 'without_practice_locale_pack')

            # with_only_en_practice_lesson
            lessons = (1..3).map { create_lesson }
            setup_locale_pack(lessons, false)
            create_lesson({title: "Practice for \"#{lessons[0].title}\""}, {is_practice_for_locale_pack_id: lessons[0].locale_pack_id})
            identify(lessons[0], 'with_only_en_practice_lesson')

            # with_practice_lesson
            lessons = (1..3).map { create_lesson }
            setup_locale_pack(lessons, false)
            lessons.each do |lesson|
                create_lesson({
                    title: "Practice for \"#{lesson.title}\"",
                    locale: lesson.locale
                },
                {is_practice_for_locale_pack_id: lesson.locale_pack_id})
            end

            practice_lessons = lessons.map(&:reload).map(&:practice_lesson)
            identify(lessons[0], 'with_practice_lesson')
            identify(practice_lessons[0], 'practice_lesson')


            frame_list_lessons = [create_frame_list_lesson, create_frame_list_lesson]
            practice_lessons = frame_list_lessons.map do |lesson|
                practice_lesson = create_lesson({
                        title: "Practice for \"#{lesson.title}\"",
                        locale: lesson.locale
                    },
                    {is_practice_for_locale_pack_id: lesson.locale_pack_id}
                )
                convert_to_frame_list_lesson(practice_lesson)
                lesson.publish!
                practice_lesson
            end
            practice_lessons[0].publish!
            practice_lessons[1].save!
            identify(frame_list_lessons[0], 'with_published_practice_frames')
            identify(frame_list_lessons[1], 'with_unpublished_practice_frames')

        end

        build do

            @users_for_email_name_filter_tests = [
                FactoryBot.create(:user, {
                    :email => "#{SecureRandom.uuid}@pedago.com",
                    :phone => '+12163714693'
                }),
                FactoryBot.create(:user, {
                    :email => "#{SecureRandom.uuid}@random.com",
                    :name => "MoPedaGo"
                })
            ]

            @users_for_email_name_filter_tests.each do |user|
                ReportSpecHelper.create_time_series_things(user)
            end

            # # see base times series report spec: should group by group_name
            # institution = Institution.first
            # user = institution.users.detect { |u| u.groups.empty? }
            # ReportSpecHelper.create_time_series_things(user)

            @reportgroup = AccessGroup.find_or_create_by(name: "REPORTGROUP")

            [
                {id: SecureRandom.uuid, email: "#{SecureRandom.uuid}@example.com", sign_up_code: 'a', provider: 'email'},
                {id: SecureRandom.uuid, email: "#{SecureRandom.uuid}@example.com", sign_up_code: 'b', provider: 'phone_no_password', phone: '+12163714692', uid: SecureRandom.uuid}
            ].each do |attrs|
                attrs.merge!({
                    name: "Billy #{SecureRandom.uuid}"
                })
                user = User.create!(attrs.merge({
                    created_at: Time.parse('2015/01/01'),
                    updated_at: Time.parse('2015/01/01'),
                    password: "password",
                    password_confirmation: 'password'
                }))
                user.add_to_group(@reportgroup.name)
                ReportSpecHelper.create_time_series_things(user)
            end
        end

        build do

            # see email_or_name filter specs in report_spec_helper.rb
            users = @users_for_email_name_filter_tests + [
                FactoryBot.create(:user),
                FactoryBot.create(:user),
            ]

            ReportSpecHelper.create_tabular_things(users[0], {
                color: 'red',
                shape: 'square'
            })
            ReportSpecHelper.create_tabular_things(users[1], {
                color: 'red',
                shape: 'circle'
            })
            ReportSpecHelper.create_tabular_things(users[2], {
                color: 'blue',
                shape: 'square'
            })
            ReportSpecHelper.create_tabular_things(users[3], {
                color: 'blue',
                shape: 'circle'
            })

            ['a', 'b'].each do |sign_up_code|

                user = User.where(sign_up_code: sign_up_code).first
                ReportSpecHelper.create_tabular_things(user)

            end

            # some more for testing accounts
            user = FactoryBot.create(:user, {
                provider: 'facebook',
                uid: SecureRandom.uuid,
                confirmed_profile_info: false,
                email: nil
            })
            ReportSpecHelper.create_tabular_things(user)
            identify(user, 'user_with_no_email')

            user = FactoryBot.create(:user, {
                provider: 'facebook',
                uid: SecureRandom.uuid,
                confirmed_profile_info: false,
                email: 'facebook_user@example.com'
            })
            ReportSpecHelper.create_tabular_things(user)
            identify(user, 'fb_user_with_email')

            user = FactoryBot.create(:user, {
                provider: 'google_oauth2',
                uid: SecureRandom.uuid,
                confirmed_profile_info: false,
                email: 'google_user@example.com'
            })
            ReportSpecHelper.create_tabular_things(user)
            identify(user, 'google_user_with_email')

            user = FactoryBot.create(:user, {
                uid: SecureRandom.uuid,
                provider: 'phone_no_password',
                phone: '+12163714632'
            })
            ReportSpecHelper.create_tabular_things(user)
            identify(user, 'phone_no_password_user')

        end

        build do
            @hiring_manager_professional_organization_options = @professional_organizations

            unlinked_professional_organization = ProfessionalOrganizationOption.create!({text: "Professional Organization 6", suggest: true})
            identify(unlinked_professional_organization, 'unlinked_professional_organization')

            @professional_organizations_for_candidates = (1..10).map do |i|
                ProfessionalOrganizationOption.create!({text: "Professional Organization #{i} for Candidates", suggest: true})
            end
        end

        build do
            users = (1..10).map do |i|
                FactoryBot.create(:user)
            end

            educational_organizations = CareerProfile::EducationalOrganizationOption.limit(users.size).to_a
            educational_organizations.pop # have to leave one unused for the users controller spec
            skills = SkillsOption.limit(users.size)
            awards_and_interests = CareerProfile::AwardsAndInterestsOption.limit(users.size)

            users.each_with_index do |user, i|
                overrides = {
                    'professional_organizations' => [@professional_organizations_for_candidates[i]],
                    'educational_organizations' => [educational_organizations[i] || educational_organizations[0]],
                    'skills' => [skills[i] || skills[0]],
                    'awards_and_interests' => [awards_and_interests[i] || awards_and_interests[0]]
                }
                complete_career_profile(user, overrides)
            end

            create_cohort_application_for_user(Cohort.first, 'pending', users[0])
            identify(users[0], "user_with_career_profile")
            identify(users[1], "another_user_with_career_profile")

            # Create a user fixture with a career profile for each available value for interested_in_joining_new_company
            interest_levels = CareerProfile.new_company_interest_levels
            users = (1..[interest_levels.size, users.size].max).map do |i|
                create_cohort_application_for_user(Cohort.first, 'accepted')
            end

            users.each_with_index do |user, i|
                overrides = {
                    'professional_organizations' => [@professional_organizations_for_candidates[i]],
                    'educational_organizations' => [educational_organizations[i] || educational_organizations[0]],
                    'skills' => [skills[i] || skills[0]],
                    'awards_and_interests' => [awards_and_interests[i] || awards_and_interests[0]],
                    'interested_in_joining_new_company' => interest_levels[i] || interest_levels[0]
                }
                complete_career_profile(user, overrides)
                users[i].write_attribute(:can_edit_career_profile, true)
                interest_level = users[i].career_profile.interested_in_joining_new_company
                identify(users[i], "user_with_#{interest_level}_career_profile") unless i >= interest_levels.size
            end
        end

        build do
            FactoryBot.create_list(:s3_asset, 2)
            FactoryBot.create_list(:s3_transcript_asset, 3)
        end

        # Add some transcripts to an education_experience
        build do
            education_experience_with_two_transcripts = CareerProfile::EducationExperience.first
            education_experience_with_two_transcripts.transcripts = S3TranscriptAsset.limit(2).to_a
            education_experience_with_two_transcripts.save!
            identify(education_experience_with_two_transcripts, 'education_experience_with_two_transcripts')

            education_experience_with_no_transcripts = CareerProfile::EducationExperience
                .where.not(id: education_experience_with_two_transcripts.id).first
            education_experience_with_no_transcripts.transcripts.destroy_all
            education_experience_with_no_transcripts.save!

            if education_experience_with_no_transcripts.transcripts.size != 0 ||
                education_experience_with_no_transcripts.transcript_waiver != nil ||
                education_experience_with_no_transcripts.transcript_approved != false
                raise 'Incorrect state for education_experience_with_no_transcripts'
            end

            identify(education_experience_with_no_transcripts, 'education_experience_with_no_transcripts')
        end

        build do
            identify(FactoryBot.create(:user), "user_without_hiring_application")

            pending_hiring_manager = FactoryBot.create(:user)
            pending_hiring_manager.professional_organization = @hiring_manager_professional_organization_options.first
            pending_hiring_manager.save!
            HiringApplication.create!(
                user_id: pending_hiring_manager.id,
                status: 'pending'
            )
            identify(pending_hiring_manager, 'pending_hiring_manager')

            rejected_hiring_manager = FactoryBot.create(:user)
            rejected_hiring_manager.professional_organization = @hiring_manager_professional_organization_options.first
            rejected_hiring_manager.save!
            HiringApplication.create!(
                user_id: rejected_hiring_manager.id,
                status: 'rejected'
            )
            identify(rejected_hiring_manager, 'rejected_hiring_manager')

            hiring_manager = FactoryBot.create(:user)
            hiring_manager.professional_organization = @hiring_manager_professional_organization_options.first
            hiring_manager.hiring_team = HiringTeam.create!(title: "Core Team", domain: 'coreteam.com')
            hiring_manager.save!

            HiringApplication.create!(
                user_id: hiring_manager.id,
                status: 'accepted'
            )
            identify(hiring_manager, 'hiring_manager')

            # We used to auto-associate hiring managers with candidates based on some various policies
            # We have since removed this, but it is convenient to establish these relationships for
            # testing purposes; particularly where matches are in the pending state.
            hiring_manager.ensure_candidate_relationships(CareerProfile.where(last_calculated_complete_percentage: 100).limit(15).map(&:user_id))

            relationships = hiring_manager.reload.candidate_relationships
            relationship = relationships[0]
            identify(relationship, "pending_hidden")

            relationship = relationships[1]
            relationship.hiring_manager_status = 'accepted'
            relationship.candidate_status = 'pending'
            relationship.save!
            identify(relationship, "accepted_pending")

            relationship = relationships[2]
            relationship.hiring_manager_status = 'rejected'
            relationship.candidate_status = 'hidden'
            relationship.save!
            identify(relationship, "rejected_hidden")

            relationship = relationships[3]
            relationship.hiring_manager_status = 'accepted'
            relationship.candidate_status = 'rejected'
            relationship.save!
            identify(relationship, "accepted_rejected")

            relationship = relationships[4]
            relationship.hiring_manager_status = 'accepted'
            relationship.candidate_status = 'accepted'
            message = relationship.hiring_manager.send_message(relationship.candidate, "body", "hiring relationship conversation")
            relationship.conversation_id = message.conversation.id
            relationship.save!
            identify(relationship, "accepted_accepted")

            # not sure this is a real state, but we want to test hidden
            # on the hiring manager side as well
            relationship = relationships[5]
            relationship.hiring_manager_status = 'hidden'
            relationship.candidate_status = 'hidden'
            relationship.save!
            identify(relationship, "hidden_hidden")

            relationship = relationships[6]
            relationship.hiring_manager_status = 'accepted'
            relationship.candidate_status = 'pending'
            relationship.open_position = OpenPosition.create!(
                hiring_manager_id: relationship.hiring_manager_id,
                title: 'A great job'
            )
            relationship.save!
            identify(relationship, "with_open_position")

            relationship = relationships[7]
            relationship.hiring_manager_status = 'saved_for_later'
            relationship.candidate_status = 'pending'
            relationship.save!
            identify(relationship, "saved_for_later_pending")

            relationship = relationships[8]
            relationship.hiring_manager_status = 'saved_for_later'
            relationship.candidate_status = 'hidden'
            relationship.save!
            identify(relationship, "saved_for_later_hidden")

        end

        build do
            hiring_team = HiringTeam.create!(title: "Team Rocket", domain: 'rocket.com')
            professional_organization = @hiring_manager_professional_organization_options.first
            (1..5).each do |i|
                hiring_manager = FactoryBot.create(:user)
                hiring_manager.professional_organization = professional_organization
                hiring_manager.hiring_team_id = hiring_team.id
                hiring_manager.save!
                HiringApplication.create!(
                    user_id: hiring_manager.id,
                    status: 'accepted'
                )
            end

            hiring_team = HiringTeam.create!(title: "Team Ash", domain: 'ash.com')
            professional_organization = @hiring_manager_professional_organization_options.second
            (1..5).each do |i|
                hiring_manager = FactoryBot.create(:user)
                hiring_manager.professional_organization = professional_organization
                hiring_manager.hiring_team_id = hiring_team.id
                hiring_manager.save!
                HiringApplication.create!(
                    user_id: hiring_manager.id,
                    status: 'accepted'
                )
            end
        end

        build do
            users = FactoryBot.create_list(:user, 2)
            users[1].send_message(users[0], "body", "trashed conversation")
            users[0].trash(users[0].mailbox.conversations.first)
            users[1].send_message(users[0], "body", "inbox conversation")
            users[0].send_message(users[1], "body", "sent conversation")
            if users[0].mailbox.inbox.empty?
                raise "Expected inbox not to be empty"
            end
            if users[0].mailbox.trash.empty?
                raise "Expected trash not to be empty"
            end
            identify(users[0], "user_with_multiple_conversations")
        end

        build do

            # create user with 10 events that were stored in order
            user = FactoryBot.create(:user, {
                email: 'happy-path-eventer@pedago.com'
            })

            1.upto(9) do |i|
                FactoryBot.create(:redshift_event, {
                    user_id: user.id,
                    event_type: 'dropped the bomb',
                    estimated_time: Time.parse('2016-01-01 00:00:00 UTC') + i.seconds,
                    created_at: Time.parse('2016-01-01 00:00:00 UTC') + i.seconds,
                    updated_at: Time.parse('2016-01-01 00:00:00 UTC') + i.seconds
                })
            end

            # create user with more than one batch's worth of events
            user = FactoryBot.create(:user, {
                email: 'eventer-with-lots-at-same-time@pedago.com'
            })
            1.upto(8) do |i|
                FactoryBot.create(:redshift_event, {
                    user_id: user.id,
                    event_type: 'dropped the bomb',
                    estimated_time: Time.parse('2016-01-01 00:00:00 UTC'), # all at the same estimated_time
                    created_at: Time.parse('2016-01-01 00:00:00 UTC'),
                    updated_at: Time.parse('2016-01-01 00:00:00 UTC') + i.seconds
                })
            end

            9.upto(10) do |i|
                FactoryBot.create(:redshift_event, {
                    user_id: user.id,
                    event_type: 'dropped the bomb',
                    estimated_time: Time.parse('2016-01-01 00:00:00 UTC') + i.seconds,
                    created_at: Time.parse('2016-01-01 00:00:00 UTC') + i.seconds,
                    updated_at: Time.parse('2016-01-01 00:00:00 UTC') + i.seconds
                })
            end

            ActiveRecord::Base.connection.execute('TRUNCATE archived_events');
            ActiveRecord::Base.connection.execute('INSERT INTO archived_events (SELECT * FROM events)');

        end

        build do

            career_profile_ids = CareerProfile.joins(:user).where("users.avatar_url is not null").reorder("career_profiles.created_at").limit(5).pluck('id')
            CareerProfileList.create!(
                name: 'hiring_index',
                career_profile_ids: career_profile_ids.slice(0,3)
            )
            CareerProfileList.create!(
                name: 'another_index',
                career_profile_ids: career_profile_ids.slice(3,5)
            )
        end

        build do
            def create_cupertino_event(start_time, end_time, event_type = 'meetup')

                date_tbd = !start_time && !end_time

                meetup = StudentNetworkEvent.create!({
                    id: SecureRandom.uuid,
                    title: 'Random Event',
                    description: 'A super cool meetup that you should totally attend',
                    event_type: event_type,
                    start_time: start_time,
                    end_time: end_time,
                    date_tbd: date_tbd,
                    date_tbd_description: date_tbd ? 'Date TBD' : nil,
                    author_id: @admin_user.id,
                    published: true,
                    published_at: Time.now
                })
                # skip callbacks so we don't try to hit Google's API in
                # StudentNetworkEvent#set_place_details_anonymized
                meetup.update_columns({
                    place_details: {
                        "locality"=>{"short"=>"Cupertino", "long"=>"Cupertino"},
                        "administrative_area_level_2"=>{"short"=>"Santa Clara County", "long"=>"Santa Clara County"},
                        "administrative_area_level_1"=>{"short"=>"CA", "long"=>"California"},
                        "country"=>{"short"=>"US", "long"=>"United States"},
                        "formatted_address"=>"Cupertino, CA, USA",
                        "utc_offset"=>-420,
                        "lat"=>37.3229978,
                        "lng"=>-122.0321823
                    },
                    location: "0101000020E6100000A58059460F825EC009D7EDFD57A94240",
                    timezone: 'America/Los_Angeles'
                })
                meetup.update_fulltext # place_details are in the keyword vectors for fulltext searching
                meetup
            end

            toronto = {
                "locality"=>{"short"=>"Toronto", "long"=>"Toronto"},
                "administrative_area_level_2"=>{"short"=>"Toronto Division", "long"=>"Toronto Division"},
                "administrative_area_level_1"=>{"short"=>"ON", "long"=>"Ontario"},
                "country"=>{"short"=>"CA", "long"=>"Canada"},
                "formatted_address"=>"Toronto, ON, Canada",
                "utc_offset"=>-240,
                "lat"=>43.653226,
                "lng"=>-79.3831843
            }

            @cupertino_past_meetup = create_cupertino_event(Time.now - 3.hours, Time.now - 1.hour)
            identify(@cupertino_past_meetup, 'cupertino_past_meetup')

            @cupertino_old_conference = create_cupertino_event(Time.now - 1.week - 1.hour, Time.now - 1.week, 'conference')
            identify(@cupertino_old_conference, 'cupertino_old_conference')

            @cupertino_recent_conference = create_cupertino_event(Time.now - 6.days, Time.now - 6.days + 1.hour, 'conference')
            identify(@cupertino_recent_conference, 'cupertino_recent_conference')

            @cupertino_present_meetup = create_cupertino_event(Time.now - 3.hours, Time.now + 2.days)
            identify(@cupertino_present_meetup, 'cupertino_present_meetup')

            @cupertino_future_meetup = create_cupertino_event(Time.now + 2.months, Time.now + 2.months)
            identify(@cupertino_future_meetup, 'cupertino_future_meetup')

            @cupertino_indeterminate_meetup = create_cupertino_event(nil, nil)
            identify(@cupertino_indeterminate_meetup, 'cupertino_indeterminate_meetup')


            @toronto_future_book_club = StudentNetworkEvent.create!({
                id: SecureRandom.uuid,
                title: 'Book Club',
                description: 'A radical, though-provoking book club that you should totally attend',
                event_type: 'book_club',
                start_time: Time.now + 3.months,
                end_time:Time.now + 3.months,
                author_id: @admin_user.id,
                published: true,
                published_at: Time.now
            })
            # skip callbacks so we don't try to hit Google's API in
            # StudentNetworkEvent#set_place_details_anonymized
            @toronto_future_book_club.update_columns({
                place_details: toronto,
                location: "0101000020E6100000CD35711786D853C0CD72D9E89CD34540",
                timezone: 'America/New_York'
            })
            @toronto_future_book_club.update_fulltext # place_details are in the keyword vectors for fulltext searching
            identify(@toronto_future_book_club, 'toronto_future_book_club')
        end

        def create_conversation(hiring_relationship, sender)
            recipient = sender == hiring_relationship.hiring_manager ? hiring_relationship.candidate : hiring_relationship.hiring_manager
            hiring_relationship.conversation = Mailboxer::Conversation.create_from_hash!({
                subject: "new subject",
                messages: [{
                    "body" => "new message body",
                    "sender_id" => sender.id,
                    "sender_type" => 'user',
                    "metadata" => {"some" => "metadata"},
                    "receipts" => [{
                        "receiver_id" => sender.id
                    }]
                }],
                recipients: [
                    {
                        'type' => 'user',
                        'id' => sender.id
                    },
                    {
                        'type' => 'user',
                        'id' => recipient.id
                    }]
            }, sender, hiring_relationship)

            # mark all as read because otherwise specs will mark them as read and things will change from spec to spec
            i = 1
            hiring_relationship.conversation.messages.reorder(:created_at).each { |m| m.update(created_at: hiring_relationship.updated_at + i, updated_at: hiring_relationship.updated_at + i); i += 1 }
            hiring_relationship.conversation.messages.map(&:receipts).flatten.sort_by(&:created_at).each { |r| r.update(is_read: true, created_at: hiring_relationship.updated_at + i, updated_at: Time.parse("2017/06/01 12:04:0#{i}")); i += 1 }


            hiring_relationship.save!
            hiring_relationship.conversation
        end

        def update_with_new_message(conversation, user)
            hash = conversation.as_json
            hash['messages'] << {
                "body" => "new message body",
                "sender_id" => user.id,
                "sender_type" => 'user',
                "metadata" => {"some" => "metadata"},
                "receipts" => [{
                    "receiver_id" => user.id,
                    "is_read" => true,
                    "trashed" => false,
                    "deleted" => false,
                    "mailbox_type" => 'sentbox'
                }]
            }
            Mailboxer::Conversation.update_from_hash!(hash, user, conversation.hiring_relationship)
        end


        def locations_for_slack_room_assignment_spec

            san_jose_list = [{"location"=>"0101000020E61000009CA0979BB9785EC0981E03684AAB4240",
            "place_details"=>
            {"locality"=>{"short"=>"San Jose", "long"=>"San Jose"},
                "administrative_area_level_2"=>{"short"=>"Santa Clara County", "long"=>"Santa Clara County"},
                "administrative_area_level_1"=>{"short"=>"CA", "long"=>"California"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>37.3382082,
                "lng"=>-121.88632860000001,
                "formatted_address"=>"San Jose, CA, USA",
                "utc_offset"=>-420}},
            {"location"=>"0101000020E6100000647E7F94227D5EC0AAE05C6853AD4240",
            "place_details"=>
            {"locality"=>{"short"=>"Santa Clara", "long"=>"Santa Clara"},
                "administrative_area_level_2"=>{"short"=>"Santa Clara County", "long"=>"Santa Clara County"},
                "administrative_area_level_1"=>{"short"=>"CA", "long"=>"California"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>37.35410789999999,
                "lng"=>-121.95523559999998,
                "formatted_address"=>"Santa Clara, CA, USA",
                "utc_offset"=>-420}},
            {"location"=>"0101000020E6100000F40E01D15D855EC0590861246AB14240",
            "place_details"=>
            {"locality"=>{"short"=>"Mountain View", "long"=>"Mountain View"},
                "administrative_area_level_2"=>{"short"=>"Santa Clara County", "long"=>"Santa Clara County"},
                "administrative_area_level_1"=>{"short"=>"CA", "long"=>"California"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>37.3860517,
                "lng"=>-122.0838511,
                "formatted_address"=>"Mountain View, CA, USA",
                "utc_offset"=>-480}},
            {"location"=>"0101000020E61000009BA0979BB9785EC0981E03684AAB4240",
            "place_details"=>
            {"locality"=>{"short"=>"San Jose", "long"=>"San Jose"},
                "administrative_area_level_2"=>{"short"=>"Santa Clara County", "long"=>"Santa Clara County"},
                "administrative_area_level_1"=>{"short"=>"CA", "long"=>"California"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "formatted_address"=>"San Jose, CA, USA",
                "utc_offset"=>-420,
                "lat"=>37.3382082,
                "lng"=>-121.8863286}},
            {"location"=>"0101000020E61000009CA0979BB9785EC0981E03684AAB4240",
            "place_details"=>
            {"locality"=>{"short"=>"San Jose", "long"=>"San Jose"},
                "administrative_area_level_2"=>{"short"=>"Santa Clara County", "long"=>"Santa Clara County"},
                "administrative_area_level_1"=>{"short"=>"CA", "long"=>"California"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>37.3382082,
                "lng"=>-121.88632860000001,
                "formatted_address"=>"San Jose, CA, USA",
                "utc_offset"=>-420}},
            {"location"=>"0101000020E61000009CA0979BB9785EC0981E03684AAB4240",
            "place_details"=>
            {"locality"=>{"short"=>"San Jose", "long"=>"San Jose"},
                "administrative_area_level_2"=>{"short"=>"Santa Clara County", "long"=>"Santa Clara County"},
                "administrative_area_level_1"=>{"short"=>"CA", "long"=>"California"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>37.3382082,
                "lng"=>-121.88632860000001,
                "formatted_address"=>"San Jose, CA, USA",
                "utc_offset"=>-480}},
            {"location"=>"0101000020E61000002C10E26F208F5EC0D6C21B881BBE4240",
            "place_details"=>
            {"locality"=>{"short"=>"Redwood City", "long"=>"Redwood City"},
                "administrative_area_level_2"=>{"short"=>"San Mateo County", "long"=>"San Mateo County"},
                "administrative_area_level_1"=>{"short"=>"CA", "long"=>"California"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>37.48521520000001,
                "lng"=>-122.23635480000002,
                "formatted_address"=>"Redwood City, CA, USA",
                "utc_offset"=>-420}},
            {"location"=>"0101000020E610000040CE458D53825EC050E449D235AF4240",
            "place_details"=>
            {"locality"=>{"short"=>"Sunnyvale", "long"=>"Sunnyvale"},
                "administrative_area_level_2"=>{"short"=>"Santa Clara County", "long"=>"Santa Clara County"},
                "administrative_area_level_1"=>{"short"=>"CA", "long"=>"California"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>37.36883,
                "lng"=>-122.0363496,
                "formatted_address"=>"Sunnyvale, CA, USA",
                "utc_offset"=>-420}},
            {"location"=>"0101000020E61000009CA0979BB9785EC0981E03684AAB4240",
            "place_details"=>
            {"locality"=>{"short"=>"San Jose", "long"=>"San Jose"},
                "administrative_area_level_2"=>{"short"=>"Santa Clara County", "long"=>"Santa Clara County"},
                "administrative_area_level_1"=>{"short"=>"CA", "long"=>"California"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>37.3382082,
                "lng"=>-121.88632860000001,
                "formatted_address"=>"San Jose, CA, USA",
                "utc_offset"=>-480}},
            {"location"=>"0101000020E61000001CC17C68D5945EC0B0B5AD1C10C84240",
            "place_details"=>
            {"locality"=>{"short"=>"San Mateo", "long"=>"San Mateo"},
                "administrative_area_level_2"=>{"short"=>"San Mateo County", "long"=>"San Mateo County"},
                "administrative_area_level_1"=>{"short"=>"CA", "long"=>"California"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>37.5629917,
                "lng"=>-122.3255254,
                "formatted_address"=>"San Mateo, CA, USA",
                "utc_offset"=>-420}},
            {"location"=>"0101000020E6100000A48059460F825EC009D7EDFD57A94240",
            "place_details"=>
            {"locality"=>{"short"=>"Cupertino", "long"=>"Cupertino"},
                "administrative_area_level_2"=>{"short"=>"Santa Clara County", "long"=>"Santa Clara County"},
                "administrative_area_level_1"=>{"short"=>"CA", "long"=>"California"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>37.3229978,
                "lng"=>-122.03218229999999,
                "formatted_address"=>"Cupertino, CA, USA",
                "utc_offset"=>-480}},
            {"location"=>"0101000020E6100000A48059460F825EC009D7EDFD57A94240",
            "place_details"=>
            {"locality"=>{"short"=>"Cupertino", "long"=>"Cupertino"},
                "administrative_area_level_2"=>{"short"=>"Santa Clara County", "long"=>"Santa Clara County"},
                "administrative_area_level_1"=>{"short"=>"CA", "long"=>"California"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>37.3229978,
                "lng"=>-122.03218229999999,
                "formatted_address"=>"Cupertino, CA, USA",
                "utc_offset"=>-420}},
            {"location"=>"0101000020E6100000647E7F94227D5EC0AAE05C6853AD4240",
            "place_details"=>
            {"locality"=>{"short"=>"Santa Clara", "long"=>"Santa Clara"},
                "administrative_area_level_2"=>{"short"=>"Santa Clara County", "long"=>"Santa Clara County"},
                "administrative_area_level_1"=>{"short"=>"CA", "long"=>"California"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>37.35410789999999,
                "lng"=>-121.95523559999998,
                "formatted_address"=>"Santa Clara, CA, USA",
                "utc_offset"=>-480}}
            ]

            united_states_list = [{"location"=>"0101000020E6100000A8F809B1C4C351C053AEF02E172E4540",
            "place_details"=>
            {"locality"=>{"short"=>"Boston", "long"=>"Boston"},
                "administrative_area_level_2"=>{"short"=>"Suffolk County", "long"=>"Suffolk County"},
                "administrative_area_level_1"=>{"short"=>"MA", "long"=>"Massachusetts"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>42.3600825,
                "lng"=>-71.05888010000001,
                "formatted_address"=>"Boston, MA, USA",
                "utc_offset"=>-240}},
            {"location"=>"0101000020E6100000A8C0C936F0D955C09D3A45FDE4F84240",
            "place_details"=>
            {"locality"=>{"short"=>"Newburgh", "long"=>"Newburgh"},
                "administrative_area_level_3"=>{"short"=>"Ohio Township", "long"=>"Ohio Township"},
                "administrative_area_level_2"=>{"short"=>"Warrick County", "long"=>"Warrick County"},
                "administrative_area_level_1"=>{"short"=>"IN", "long"=>"Indiana"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "postal_code"=>{"short"=>"47630", "long"=>"47630"},
                "lat"=>37.9444882,
                "lng"=>-87.40528649999999,
                "formatted_address"=>"Newburgh, IN 47630, USA",
                "utc_offset"=>-360}},
            {"location"=>"0101000020E6100000983C9E961F5558C0E00BEE62F5D74240",
            "place_details"=>
            {"locality"=>{"short"=>"Wichita", "long"=>"Wichita"},
                "administrative_area_level_2"=>{"short"=>"Sedgwick County", "long"=>"Sedgwick County"},
                "administrative_area_level_1"=>{"short"=>"KS", "long"=>"Kansas"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>37.68717609999999,
                "lng"=>-97.33005300000002,
                "formatted_address"=>"Wichita, KS, USA",
                "utc_offset"=>-300}},
            {"location"=>"0101000020E61000004098DBBDDC8052C04F35C4C25A8B4440",
            "place_details"=>
            {"locality"=>{"short"=>"Nanuet", "long"=>"Nanuet"},
                "administrative_area_level_3"=>{"short"=>"Clarkstown", "long"=>"Clarkstown"},
                "administrative_area_level_2"=>{"short"=>"Rockland County", "long"=>"Rockland County"},
                "administrative_area_level_1"=>{"short"=>"NY", "long"=>"New York"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>41.0887073,
                "lng"=>-74.01347299999998,
                "formatted_address"=>"Nanuet, NY, USA",
                "utc_offset"=>-300}},
            {"location"=>"0101000020E6100000D04C9C81367252C075649B0AA7804440",
            "place_details"=>
            {"locality"=>{"short"=>"Scarsdale", "long"=>"Scarsdale"},
                "administrative_area_level_3"=>{"short"=>"Scarsdale", "long"=>"Scarsdale"},
                "administrative_area_level_2"=>{"short"=>"Westchester County", "long"=>"Westchester County"},
                "administrative_area_level_1"=>{"short"=>"NY", "long"=>"New York"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "postal_code"=>{"short"=>"10583", "long"=>"10583"},
                "lat"=>41.0050977,
                "lng"=>-73.78457679999997,
                "formatted_address"=>"Scarsdale, NY 10583, USA",
                "utc_offset"=>-300}},
            {"location"=>"0101000020E61000002461DF4EA2DA51C0861DC6A4BF234540",
            "place_details"=>
            {"locality"=>{"short"=>"Framingham", "long"=>"Framingham"},
                "administrative_area_level_2"=>{"short"=>"Middlesex County", "long"=>"Middlesex County"},
                "administrative_area_level_1"=>{"short"=>"MA", "long"=>"Massachusetts"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>42.279286,
                "lng"=>-71.4161565,
                "formatted_address"=>"Framingham, MA, USA",
                "utc_offset"=>-240}},
            {"location"=>"0101000020E6100000963A6D324F4A5DC09240834D9D5B4040",
            "place_details"=>
            {"locality"=>{"short"=>"San Diego", "long"=>"San Diego"},
                "administrative_area_level_2"=>{"short"=>"San Diego County", "long"=>"San Diego County"},
                "administrative_area_level_1"=>{"short"=>"CA", "long"=>"California"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "formatted_address"=>"San Diego, CA, USA",
                "utc_offset"=>-420,
                "lat"=>32.715738,
                "lng"=>-117.1610838}},
            {"location"=>"0101000020E6100000E8F120E28BE252C06B7121EA99DE4340",
            "place_details"=>
            {"locality"=>{"short"=>"Wilmington", "long"=>"Wilmington"},
                "administrative_area_level_2"=>{"short"=>"New Castle County", "long"=>"New Castle County"},
                "administrative_area_level_1"=>{"short"=>"DE", "long"=>"Delaware"},
                "country"=>{"short"=>"US", "long"=>"United States"},
                "lat"=>39.73907210000001,
                "lng"=>-75.5397878,
                "formatted_address"=>"Wilmington, DE, USA",
                "utc_offset"=>-300}}]

            canada_list = [{"location"=>"0101000020E61000005CC5E23785DD53C098AC8A7093074640",
            "place_details"=>
            {"locality"=>{"short"=>"Newmarket", "long"=>"Newmarket"},
                "administrative_area_level_3"=>{"short"=>"King", "long"=>"King"},
                "administrative_area_level_2"=>{"short"=>"York Regional Municipality", "long"=>"York Regional Municipality"},
                "administrative_area_level_1"=>{"short"=>"ON", "long"=>"Ontario"},
                "country"=>{"short"=>"CA", "long"=>"Canada"},
                "lat"=>44.05918700000001,
                "lng"=>-79.46125599999999,
                "formatted_address"=>"Newmarket, ON, Canada",
                "utc_offset"=>-240}},
            {"location"=>"0101000020E6100000C4C4D5B702EC53C07AA9D898D7BB4540",
            "place_details"=>
            {"locality"=>{"short"=>"Oakville", "long"=>"Oakville"},
                "administrative_area_level_2"=>{"short"=>"Halton Regional Municipality", "long"=>"Halton Regional Municipality"},
                "administrative_area_level_1"=>{"short"=>"ON", "long"=>"Ontario"},
                "country"=>{"short"=>"CA", "long"=>"Canada"},
                "lat"=>43.467517,
                "lng"=>-79.68766590000001,
                "formatted_address"=>"Oakville, ON, Canada",
                "utc_offset"=>-300}},
            {"location"=>"0101000020E6100000B85F9912A4C94FC0E78C28ED0D534640",
            "place_details"=>
            {"locality"=>{"short"=>"Halifax", "long"=>"Halifax"},
                "administrative_area_level_3"=>{"short"=>"Halifax", "long"=>"Halifax"},
                "administrative_area_level_2"=>{"short"=>"Halifax Regional Municipality", "long"=>"Halifax Regional Municipality"},
                "administrative_area_level_1"=>{"short"=>"NS", "long"=>"Nova Scotia"},
                "country"=>{"short"=>"CA", "long"=>"Canada"},
                "lat"=>44.6488625,
                "lng"=>-63.5753196,
                "formatted_address"=>"Halifax, NS, Canada",
                "utc_offset"=>-180}},
            {"location"=>"0101000020E6100000CC35711786D853C0CD72D9E89CD34540",
            "place_details"=>
            {"locality"=>{"short"=>"Toronto", "long"=>"Toronto"},
                "administrative_area_level_2"=>{"short"=>"Toronto Division", "long"=>"Toronto Division"},
                "administrative_area_level_1"=>{"short"=>"ON", "long"=>"Ontario"},
                "country"=>{"short"=>"CA", "long"=>"Canada"},
                "lat"=>43.653226,
                "lng"=>-79.38318429999998,
                "formatted_address"=>"Toronto, ON, Canada",
                "utc_offset"=>-300}},
            {"location"=>"0101000020E610000010D99B73CBF053C08FFA905CA3DD4540",
            "place_details"=>
            {"locality"=>{"short"=>"Brampton", "long"=>"Brampton"},
                "administrative_area_level_2"=>{"short"=>"Peel Regional Municipality", "long"=>"Peel Regional Municipality"},
                "administrative_area_level_1"=>{"short"=>"ON", "long"=>"Ontario"},
                "country"=>{"short"=>"CA", "long"=>"Canada"},
                "lat"=>43.7315479,
                "lng"=>-79.76241770000001,
                "formatted_address"=>"Brampton, ON, Canada",
                "utc_offset"=>-300}},
            {"location"=>"0101000020E6100000B0F171B7355B4AC0ED31EC8BDFC74740",
            "place_details"=>
            {"locality"=>{"short"=>"St. John's", "long"=>"St. John's"},
                "administrative_area_level_3"=>{"short"=>"Division No. 1, Subd. D", "long"=>"Division No. 1, Subd. D"},
                "administrative_area_level_2"=>{"short"=>"Division No. 1", "long"=>"Division No. 1"},
                "administrative_area_level_1"=>{"short"=>"NL", "long"=>"Newfoundland and Labrador"},
                "country"=>{"short"=>"CA", "long"=>"Canada"},
                "lat"=>47.5615096,
                "lng"=>-52.712576799999965,
                "formatted_address"=>"St. John's, NL, Canada",
                "utc_offset"=>-210}},
            {"location"=>"0101000020E610000090E33DBD88845CC0BF4B040539864940",
            "place_details"=>
            {"locality"=>{"short"=>"Calgary", "long"=>"Calgary"},
                "administrative_area_level_2"=>{"short"=>"Division No. 6", "long"=>"Division No. 6"},
                "administrative_area_level_1"=>{"short"=>"AB", "long"=>"Alberta"},
                "country"=>{"short"=>"CA", "long"=>"Canada"},
                "lat"=>51.0486151,
                "lng"=>-114.0708459,
                "formatted_address"=>"Calgary, AB, Canada",
                "utc_offset"=>-420}}]

                # We need a bunch of these to make sure that there are enough
                # movable applications that everything can be balanced.
                # Seems like place details should not matter here, but it seems
                # like they do
                boondocks_list = [{"location"=>"0101000020E6100000B09F200C97BE5F400A85083884C84240",
                "place_details"=>
                {"locality"=>{"short"=>"Seoul", "long"=>"Seoul"},
                    "administrative_area_level_1"=>{"short"=>"Seoul", "long"=>"Seoul"},
                    "country"=>{"short"=>"KR", "long"=>"South Korea"},
                    "lat"=>37.566535,
                    "lng"=>126.97796919999996,
                    "formatted_address"=>"Seoul, South Korea",
                    "utc_offset"=>540}},
                {"location"=>"0101000020E61000008095E2F43F1A3A40DEFF6C50A0364640",
                "place_details"=>
                {"locality"=>{"short"=>"Bucharest", "long"=>"Bucharest"},
                    "administrative_area_level_2"=>{"short"=>"Bucharest", "long"=>"Bucharest"},
                    "administrative_area_level_1"=>{"short"=>"Bucharest", "long"=>"Bucharest"},
                    "country"=>{"short"=>"RO", "long"=>"Romania"},
                    "lat"=>44.4267674,
                    "lng"=>26.102538399999958,
                    "formatted_address"=>"Bucharest, Romania",
                    "utc_offset"=>120}},
                {"location"=>"0101000020E6100000D099ED0A7D0C5840FC1065B9B6DD3040",
                "place_details"=>
                {"locality"=>{"short"=>"Yangon", "long"=>"Yangon"},
                    "administrative_area_level_1"=>{"short"=>"Yangon Region", "long"=>"Yangon Region"},
                    "country"=>{"short"=>"MM", "long"=>"Myanmar (Burma)"},
                    "lat"=>16.8660694,
                    "lng"=>96.19513200000006,
                    "formatted_address"=>"Yangon, Myanmar (Burma)",
                    "utc_offset"=>390}},
                {"location"=>"0101000020E6100000F0A1444B1EB65A4035689E12C6D518C0",
                "place_details"=>
                {"colloquial_area"=>{"short"=>"Jakarta", "long"=>"Jakarta"},
                    "administrative_area_level_1"=>{"short"=>"Jakarta", "long"=>"Jakarta"},
                    "country"=>{"short"=>"ID", "long"=>"Indonesia"},
                    "lat"=>-6.2087634,
                    "lng"=>106.84559899999999,
                    "formatted_address"=>"Jakarta, Indonesia",
                    "utc_offset"=>420}},
                {"location"=>"0101000020E6100000F024C74219395A400FF4AB94F9162740",
                "place_details"=>
                {"locality"=>{"short"=>"PP", "long"=>"Phnom Penh"},
                    "administrative_area_level_1"=>{"short"=>"Phnom Penh", "long"=>"Phnom Penh"},
                    "country"=>{"short"=>"KH", "long"=>"Cambodia"},
                    "lat"=>11.5448729,
                    "lng"=>104.89216680000004,
                    "formatted_address"=>"Phnom Penh, Cambodia",
                    "utc_offset"=>420}},
                {"location"=>"0101000020E610000000D820DD19871840FF45D09849CE4840",
                "place_details"=>
                {"locality"=>{"short"=>"Luxembourg City", "long"=>"Luxembourg City"},
                    "administrative_area_level_2"=>{"short"=>"Luxembourg", "long"=>"Luxembourg"},
                    "administrative_area_level_1"=>{"short"=>"Luxembourg District", "long"=>"Luxembourg District"},
                    "country"=>{"short"=>"LU", "long"=>"Luxembourg"},
                    "lat"=>49.61162100000001,
                    "lng"=>6.131934600000022,
                    "formatted_address"=>"Luxembourg City, Luxembourg",
                    "utc_offset"=>60}},
                {"location"=>"0101000020E610000080AAB0BE26681140323F92EDD76C4940",
                "place_details"=>
                {"locality"=>{"short"=>"Brussels", "long"=>"Brussels"},
                    "administrative_area_level_1"=>{"short"=>"Brussels", "long"=>"Brussels"},
                    "country"=>{"short"=>"BE", "long"=>"Belgium"},
                    "lat"=>50.8503396,
                    "lng"=>4.351710300000036,
                    "formatted_address"=>"Brussels, Belgium",
                    "utc_offset"=>60}},
                {"location"=>"0101000020E610000000221F4F26812540DA6A20A7F9F44D40",
                "place_details"=>
                {"locality"=>{"short"=>"Oslo", "long"=>"Oslo"},
                    "administrative_area_level_1"=>{"short"=>"Oslo", "long"=>"Oslo"},
                    "country"=>{"short"=>"NO", "long"=>"Norway"},
                    "lat"=>59.9138688,
                    "lng"=>10.752245399999993,
                    "formatted_address"=>"Oslo, Norway",
                    "utc_offset"=>60}},
                {"location"=>"0101000020E6100000007FF8F9EFC9184068476062E2314740",
                "place_details"=>
                {"locality"=>{"short"=>"Signy-Avenex", "long"=>"Signy-Avenex"},
                    "administrative_area_level_2"=>{"short"=>"Nyon", "long"=>"Nyon"},
                    "administrative_area_level_1"=>{"short"=>"VD", "long"=>"Vaud"},
                    "country"=>{"short"=>"CH", "long"=>"Suisse"},
                    "postal_code"=>{"short"=>"1274", "long"=>"1274"},
                    "lat"=>46.3897212,
                    "lng"=>6.197204499999998,
                    "formatted_address"=>"1274 Signy-Avenex, Suisse",
                    "utc_offset"=>60}},
                {"location"=>"0101000020E61000004022E5828E113240F026BF4527AA4D40",
                "place_details"=>
                {"locality"=>{"short"=>"Stockholm", "long"=>"Stockholm"},
                    "administrative_area_level_1"=>{"short"=>"Stockholm County", "long"=>"Stockholm County"},
                    "country"=>{"short"=>"SE", "long"=>"Sweden"},
                    "lat"=>59.32932349999999,
                    "lng"=>18.068580800000063,
                    "formatted_address"=>"Stockholm, Sweden",
                    "utc_offset"=>60}},
                {"location"=>"0101000020E610000000DA82EF47C31240A7A498CD2D274A40",
                "place_details"=>
                {"locality"=>{"short"=>"Hoofddorp", "long"=>"Hoofddorp"},
                    "administrative_area_level_2"=>{"short"=>"Haarlemmermeer", "long"=>"Haarlemmermeer"},
                    "administrative_area_level_1"=>{"short"=>"NH", "long"=>"North Holland"},
                    "country"=>{"short"=>"NL", "long"=>"Netherlands"},
                    "lat"=>52.3060853,
                    "lng"=>4.690704099999948,
                    "formatted_address"=>"Hoofddorp, Netherlands",
                    "utc_offset"=>60}},
                {"location"=>"0101000020E6100000A0FF46F1E0AF3640382806ED7A3A4E40",
                "place_details"=>
                {"locality"=>{"short"=>"Paimio", "long"=>"Paimio"},
                    "country"=>{"short"=>"FI", "long"=>"Finland"},
                    "postal_code"=>{"short"=>"21530", "long"=>"21530"},
                    "lat"=>60.4568764,
                    "lng"=>22.687026100000026,
                    "formatted_address"=>"21530 Paimio, Finland",
                    "utc_offset"=>120}},
                {"location"=>"0101000020E610000000255CC823981D40BCB1A03028272240",
                "place_details"=>
                {"locality"=>{"short"=>"Abuja", "long"=>"Abuja"},
                    "administrative_area_level_1"=>{"short"=>"Federal Capital Territory", "long"=>"Federal Capital Territory"},
                    "country"=>{"short"=>"NG", "long"=>"Nigeria"},
                    "lat"=>9.0764785,
                    "lng"=>7.398574000000053,
                    "formatted_address"=>"Abuja, Nigeria",
                    "utc_offset"=>60}},
                {"location"=>"0101000020E610000080206E98B25F3040112F9974A51A4840",
                "place_details"=>
                {"locality"=>{"short"=>"Vienna", "long"=>"Vienna"},
                    "administrative_area_level_1"=>{"short"=>"Vienna", "long"=>"Vienna"},
                    "country"=>{"short"=>"AT", "long"=>"Austria"},
                    "lat"=>48.2081743,
                    "lng"=>16.37381890000006,
                    "formatted_address"=>"Vienna, Austria",
                    "utc_offset"=>120}},
                {"location"=>"0101000020E6100000382F4E7C357953C00B9CC7BCE9793240",
                "place_details"=>
                {"locality"=>{"short"=>"Montego Bay", "long"=>"Montego Bay"},
                    "administrative_area_level_1"=>{"short"=>"St. James Parish", "long"=>"St. James Parish"},
                    "country"=>{"short"=>"JM", "long"=>"Jamaica"},
                    "lat"=>18.4762228,
                    "lng"=>-77.8938895,
                    "formatted_address"=>"Montego Bay, Jamaica",
                    "utc_offset"=>-300}},
                {"location"=>"0101000020E6100000D0B3379D099A414083CC84155DE73F40",
                "place_details"=>
                {"locality"=>{"short"=>"Ramallah", "long"=>"Ramallah"}, "lat"=>31.9037641, "lng"=>35.20341840000003, "formatted_address"=>"Ramallah", "utc_offset"=>120}}]

            switzerland_list = [{"place_details"=>
            {"locality"=>{"short"=>"Geneva", "long"=>"Geneva"},
                "administrative_area_level_2"=>{"short"=>"Genève", "long"=>"Genève"},
                "administrative_area_level_1"=>{"short"=>"GE", "long"=>"Geneva"},
                "country"=>{"short"=>"CH", "long"=>"Switzerland"},
                "lat"=>46.2043907,
                "lng"=>6.143157699999961,
                "formatted_address"=>"Geneva, Switzerland",
                "utc_offset"=>60},
            "location"=>"0101000020E610000080AE9EEE97921840A30D7679291A4740"},
            {"place_details"=>
            {"locality"=>{"short"=>"Geneva", "long"=>"Geneva"},
                "administrative_area_level_2"=>{"short"=>"Genève", "long"=>"Genève"},
                "administrative_area_level_1"=>{"short"=>"GE", "long"=>"Geneva"},
                "country"=>{"short"=>"CH", "long"=>"Switzerland"},
                "lat"=>46.2043907,
                "lng"=>6.143157699999961,
                "formatted_address"=>"Geneva, Switzerland",
                "utc_offset"=>60},
            "location"=>"0101000020E610000080AE9EEE97921840A30D7679291A4740"},
            {"place_details"=>
            {"locality"=>{"short"=>"Baden", "long"=>"Baden"},
                "administrative_area_level_2"=>{"short"=>"Baden District", "long"=>"Baden District"},
                "administrative_area_level_1"=>{"short"=>"AG", "long"=>"Aargau"},
                "country"=>{"short"=>"CH", "long"=>"Switzerland"},
                "lat"=>47.47288,
                "lng"=>8.308089999999993,
                "formatted_address"=>"Baden, Switzerland",
                "utc_offset"=>60},
            "location"=>"0101000020E61000000073F4F8BD9D20405C77F35487BC4740"}]

            israel_list = [{"place_details"=>
            {"locality"=>{"short"=>"Bet Shemesh", "long"=>"Bet Shemesh"},
                "administrative_area_level_1"=>{"short"=>"Jerusalem District", "long"=>"Jerusalem District"},
                "country"=>{"short"=>"IL", "long"=>"Israel"},
                "lat"=>31.747041,
                "lng"=>34.988099000000034,
                "formatted_address"=>"Bet Shemesh, Israel",
                "utc_offset"=>120},
            "location"=>"0101000020E6100000F01A2D077A7E414069C537143EBF3F40"},
            {"place_details"=>
            {"locality"=>{"short"=>"Tel Aviv-Yafo", "long"=>"Tel Aviv-Yafo"},
                "administrative_area_level_1"=>{"short"=>"Tel Aviv District", "long"=>"Tel Aviv District"},
                "country"=>{"short"=>"IL", "long"=>"Israel"},
                "lat"=>32.0852999,
                "lng"=>34.78176759999997,
                "formatted_address"=>"Tel Aviv-Yafo, Israel",
                "utc_offset"=>180},
            "location"=>"0101000020E61000004089F1F510644140116D6C1BEB0A4040"}]

            denmark_list = [{"place_details"=>
            {"locality"=>{"short"=>"Copenhagen", "long"=>"Copenhagen"},
                "country"=>{"short"=>"DK", "long"=>"Denmark"},
                "lat"=>55.6760968,
                "lng"=>12.568337199999974,
                "formatted_address"=>"Copenhagen, Denmark",
                "utc_offset"=>120},
            "location"=>"0101000020E61000004033EE17FD222940137706578AD64B40"},
            {"place_details"=>
            {"locality"=>{"short"=>"Copenhagen", "long"=>"Copenhagen"},
                "country"=>{"short"=>"DK", "long"=>"Denmark"},
                "lat"=>55.6760968,
                "lng"=>12.568337199999974,
                "formatted_address"=>"Copenhagen, Denmark",
                "utc_offset"=>60},
            "location"=>"0101000020E61000004033EE17FD222940137706578AD64B40"}]

            san_jose_list + united_states_list + canada_list + boondocks_list + switzerland_list + israel_list + denmark_list
        end

        def fake_place_details
            {"locality"=>{"short"=>SecureRandom.uuid, "long"=>SecureRandom.uuid},
                "administrative_area_level_2"=>{"short"=>SecureRandom.uuid, "long"=>SecureRandom.uuid},
                "administrative_area_level_1"=>{"short"=>SecureRandom.uuid, "long"=>SecureRandom.uuid},
                "country"=>{"short"=>SecureRandom.uuid, "long"=>SecureRandom.uuid},
                "lat"=>51.0486151,
                "lng"=>-114.0708459,
                "formatted_address"=>SecureRandom.uuid,
                "utc_offset"=>-420}
        end

        build do
            RefreshMaterializedContentViews.refresh_and_rebuild_derived_content_tables

            # since we're publishing in this file without RefreshMaterializedContentViews, things can
            # be wrongly cached.  clear the cache now and then check to make sure that sister_cohort
            # is not messed up, as we've seen intermittently failing specs caused by sister_cohort
            # not being set up as expected
            Rails.cache.clear

            reloaded_mba = Cohort.find(@published_mba_cohort.id)
            reloaded_emba = Cohort.find(@published_emba_cohort.id)
            sister_cohort = reloaded_mba.sister_cohort
            details = {
                mba: reloaded_mba&.application_deadline,
                emba: reloaded_emba&.application_deadline
            }
            if reloaded_mba.nil?
                raise "No mba cohort"
            elsif reloaded_emba.nil?
                raise "No emba cohort"
            elsif sister_cohort.nil?
                raise "No sister cohort.  What is the deal? (deadlines: #{details.to_json})"
            elsif sister_cohort.attributes['id'] != reloaded_emba.id
                raise "Sister cohort is not published_emba_cohort.  What is the deal? (deadlines: #{details.to_json})"
            end
        end

        build do
            WriteRegularlyIncrementallyUpdatedTablesJob.perform_now

            CohortApplication.pluck(:id).each do |id|
                CohortStatusChange.update_cohort_status_changes_for_application(id)
            end

            RefreshMaterializedInternalReportsViews.perform_now
            # Trying to make sense of specs that fail intermittently on ci because there are
            # no rows in cohort_user_progress_records
            if CohortUserProgressRecord.count == 0
                raise_on_missing_cohort_user_progress_records
            end

            Report::RefreshInternalContentTitles.perform_now(true)
        end

        build do
            cluster_tables
        end

        def raise_on_missing_cohort_user_progress_records
            result = ActiveRecord::Base.connection.execute(%Q~
                with current_status AS MATERIALIZED (
                    select
                        cohort_applications_plus.cohort_id
                        , cohort_applications_plus.user_id
                        , cohort_applications_plus.cohort_name
                        , published_cohorts.program_type
                        , published_cohorts.start_date
                        , cohort_status_changes.status
                    from cohort_applications_plus
                        join published_cohorts
                            on published_cohorts.id = cohort_applications_plus.cohort_id
                        join cohort_status_changes on
                            cohort_status_changes.cohort_id = cohort_applications_plus.cohort_id
                            and cohort_status_changes.user_id = cohort_applications_plus.user_id
                            and cohort_status_changes.until_time = '01-01-2099'
                    where
                        cohort_applications_plus.was_accepted=true
                        and published_cohorts.program_type != 'career_network_only'
                )

                -- For emba (or any cohort with required specializations), we only count the test
                -- scores from the n best specializations, where n is the num_required_specializations,
                -- This ensures that people are not penalized for doing extra ones that are more difficult
                --
                -- First step is to grab the average test score from each specialization.  It is currently
                -- the case that each specialization only has one test stream with one test lesson, but this
                -- query does not assume that to be the case.  If there is more than one, it averages the
                -- scores.  (See note below near avg_test_score about how we average lesson scores to get the
                -- total score)
                --
                -- See e61f9411-21ba-44cd-a4e4-eb44ea30499e for an example of a user who did extra specializations
                , specialization_test_scores AS MATERIALIZED (
                    SELECT
                        current_status.user_id
                        , current_status.cohort_id
                        , playlists.playlist_title
                        , lessons.stream_title
                        , lessons.stream_locale_pack_id
                        , cohorts_versions.num_required_specializations
                        , array_agg(lessons.lesson_title) lessons
                        , array_agg(lessons.lesson_locale_pack_id) lesson_locale_pack_ids
                        , avg(lesson_progress.official_test_score) avg_test_score
                    from current_status
                        join published_cohorts
                            on published_cohorts.id = current_status.cohort_id
                        join cohorts_versions
                            on published_cohorts.version_id = cohorts_versions.version_id
                            and cohorts_versions.num_required_specializations > 0
                        join published_cohort_playlist_locale_packs playlists
                            on playlists.cohort_id = current_status.cohort_id
                            and playlists.specialization = true
                        join published_playlist_streams
                            on published_playlist_streams.playlist_locale_pack_id = playlists.playlist_locale_pack_id
                        join published_stream_lesson_locale_packs lessons
                            on lessons.stream_locale_pack_id = published_playlist_streams.stream_locale_pack_id
                            and lessons.test = true
                        join lesson_streams_progress
                            on lesson_streams_progress.locale_pack_id = lessons.stream_locale_pack_id
                            and lesson_streams_progress.user_id = current_status.user_id
                            and lesson_streams_progress.completed_at is not null
                        join user_lesson_progress_records lesson_progress
                            on lesson_progress.locale_pack_id = lessons.lesson_locale_pack_id
                            and lesson_progress.user_id = current_status.user_id
                    group by

                        current_status.user_id
                        , current_status.cohort_id
                        , playlists.playlist_title
                        , lessons.stream_title
                        , lessons.stream_locale_pack_id
                        , cohorts_versions.num_required_specializations
                )

                -- Assign each specialization a rank, ordering them by best avg_test_score
                -- for each user.  So the best score for a user will have a 1, then next
                -- will have a 2, etc.
                , ranked_specialization_test_scores AS MATERIALIZED (
                    SELECT
                        specialization_test_scores.*
                        , rank() OVER (
                            PARTITION BY cohort_id, user_id
                            ORDER BY avg_test_score DESC
                        )
                    FROM
                        specialization_test_scores
                    order by
                        specialization_test_scores.cohort_id
                        , specialization_test_scores.user_id
                        , rank
                )

                -- We only count lessons from the top n specializations, where n
                -- is the num_required_specializations for the cohort.  The unnesting
                -- here handles the case where one of the specialization has multiple
                -- lessons (though this currently is never the case, see note above).
                -- Since there could be multiple test lessons in a specialization, we
                -- might count more than num_required_specializations lessons.
                , test_scores_to_count AS MATERIALIZED (
                    -- specialization tests
                    SELECT
                        cohort_id
                        , user_id
                        , avg_test_score as official_test_score
                        , unnest(lesson_locale_pack_ids) lesson_locale_pack_id
                    FROM
                        ranked_specialization_test_scores
                    WHERE
                        rank <= num_required_specializations

                    UNION

                    -- required tests
                    SELECT
                        lesson_progress.cohort_id
                        , lesson_progress.user_id
                        , lesson_progress.official_test_score
                        , lesson_progress.lesson_locale_pack_id
                    from cohort_applications_plus
                        left join cohort_user_lesson_progress_records lesson_progress on
                            cohort_applications_plus.cohort_id = lesson_progress.cohort_id
                            and cohort_applications_plus.user_id = lesson_progress.user_id
                    where
                        lesson_progress.test
                        and lesson_progress.required

                )

                -- We count all test scores from required lessons as well as test scores
                -- from specializations (see notes above about which ones we count).  Note that
                -- we are averaging all the test lessons.  This means that an exam with more lessons
                -- gets weighted more than an exam with fewer.  This is helpful in MBA where the final
                -- has more lessons and should have more weight.  It is also fine in EMBA, where specializations
                -- each have one lesson each.
                , test_scores AS MATERIALIZED (
                    SELECT
                        cohort_id
                        , user_id
                        , avg(official_test_score) as avg_test_score
                    FROM
                        test_scores_to_count
                    group by
                        cohort_id
                        , user_id
                )

                , lesson_counts AS MATERIALIZED (
                    select
                        lesson_progress.cohort_id
                        , lesson_progress.user_id
                        , sum(case
                                when foundations = true and completed_at is not null then 1
                                else 0
                                end
                            ) as foundations_lessons_complete
                        , sum(case
                                when required = true and completed_at is not null then 1
                                else 0
                                end
                            ) as required_lessons_complete
                        , sum(case
                                when test = true and completed_at is not null then 1
                                else 0
                                end
                            ) as test_lessons_complete
                        , sum(case
                                when elective = true and completed_at is not null then 1
                                else 0
                                end
                            ) as elective_lessons_complete
                        , avg(case when assessment then average_assessment_score_first else null end) as average_assessment_score_first
                        , avg(case when assessment then average_assessment_score_best else null end) as average_assessment_score_best
                        , min(case when assessment then average_assessment_score_best else null end) as min_assessment_score_best
                    from cohort_applications_plus
                        left join cohort_user_lesson_progress_records lesson_progress on
                            cohort_applications_plus.cohort_id = lesson_progress.cohort_id
                            and cohort_applications_plus.user_id = lesson_progress.user_id
                    where
                        cohort_applications_plus.was_accepted=true
                    group by
                        lesson_progress.cohort_id
                        , lesson_progress.user_id
                )
                , stream_counts AS MATERIALIZED (
                    select
                        cohort_applications_plus.cohort_id
                        , cohort_applications_plus.user_id
                        , sum(case
                                when foundations = true and completed_at is not null then 1
                                else 0
                                end
                            ) as foundations_streams_complete
                        , sum(case
                                when required = true and completed_at is not null then 1
                                else 0
                                end
                            ) as required_streams_complete
                        , sum(case
                                when exam = true and completed_at is not null then 1
                                else 0
                                end
                            ) as exam_streams_complete
                        , sum(case
                                when elective = true and completed_at is not null then 1
                                else 0
                                end
                            ) as elective_streams_complete
                    from cohort_applications_plus
                        join published_cohort_stream_locale_packs
                            on published_cohort_stream_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                        left join lesson_streams_progress
                            on published_cohort_stream_locale_packs.stream_locale_pack_id = lesson_streams_progress.locale_pack_id
                            and cohort_applications_plus.user_id = lesson_streams_progress.user_id
                    where
                        cohort_applications_plus.was_accepted=true
                    group by
                        cohort_applications_plus.cohort_id
                        , cohort_applications_plus.user_id
                )

                , playlist_counts AS MATERIALIZED (
                    select
                        cohort_applications_plus.cohort_id
                        , cohort_applications_plus.user_id
                        , sum(case
                                when required = true and completed = true then 1
                                else 0
                                end
                            ) as required_playlists_complete
                        , sum(case
                                when specialization = true and completed = true then 1
                                else 0
                                end
                            ) as specialization_playlists_complete
                    from cohort_applications_plus
                        join published_cohort_playlist_locale_packs
                            on published_cohort_playlist_locale_packs.cohort_id = cohort_applications_plus.cohort_id
                        left join playlist_progress
                            on published_cohort_playlist_locale_packs.playlist_locale_pack_id = playlist_progress.locale_pack_id
                            and cohort_applications_plus.user_id = playlist_progress.user_id
                    where
                        cohort_applications_plus.was_accepted=true
                    group by
                        cohort_applications_plus.cohort_id
                        , cohort_applications_plus.user_id
                )
                , with_counts AS MATERIALIZED (
                    select
                        current_status.cohort_name
                        , current_status.cohort_id
                        , current_status.program_type
                        , current_status.start_date
                        , current_status.user_id
                        , current_status.status

                        , stream_counts.required_streams_complete
                        , stream_counts.foundations_streams_complete
                        , stream_counts.exam_streams_complete
                        , stream_counts.elective_streams_complete

                        , playlist_counts.required_playlists_complete
                        , playlist_counts.specialization_playlists_complete

                        , lesson_counts.required_lessons_complete
                        , lesson_counts.foundations_lessons_complete
                        , lesson_counts.test_lessons_complete
                        , lesson_counts.elective_lessons_complete
                        , lesson_counts.average_assessment_score_first
                        , lesson_counts.average_assessment_score_best
                        , lesson_counts.min_assessment_score_best
                        , test_scores.avg_test_score

                    from current_status
                        join lesson_counts
                            on current_status.cohort_id = lesson_counts.cohort_id
                            and current_status.user_id = lesson_counts.user_id
                        join stream_counts
                            on current_status.cohort_id = stream_counts.cohort_id
                            and current_status.user_id = stream_counts.user_id
                        join playlist_counts
                            on current_status.cohort_id = playlist_counts.cohort_id
                            and current_status.user_id = playlist_counts.user_id
                        join test_scores
                            on current_status.cohort_id = test_scores.cohort_id
                            and current_status.user_id = test_scores.user_id


                )
                , with_averages AS MATERIALIZED (
                    select
                        with_counts.*

                        -- no entry in the table means you did not participate at all, and get a score of 0
                        , coalesce(user_participation_scores.score, 0) as participation_score
                        , coalesce(cohort_applications_plus.project_score, 0) as project_score

                        , cohort_applications_plus.final_score
                        , cohort_applications_plus.meets_graduation_requirements
                    from with_counts
                        left join user_participation_scores
                            on user_participation_scores.user_id = with_counts.user_id
                        left join cohort_applications_plus
                            on cohort_applications_plus.cohort_id = with_counts.cohort_id
                            and cohort_applications_plus.user_id = with_counts.user_id
                )
                select '*****current_status' as cte, (select count(*) from current_status) as ct
                union
                select 'specialization_test_scores' as cte, (select count(*) from specialization_test_scores) as ct
                union
                select 'ranked_specialization_test_scores' as cte, (select count(*) from ranked_specialization_test_scores) as ct
                union
                select 'test_scores_to_count' as cte, (select count(*) from test_scores_to_count) as ct
                union
                select 'test_scores' as cte, (select count(*) from test_scores) as ct
                union
                select 'lesson_counts' as cte, (select count(*) from lesson_counts) as ct
                union
                select 'stream_counts' as cte, (select count(*) from stream_counts) as ct
                union
                select 'playlist_counts' as cte, (select count(*) from playlist_counts) as ct
                union
                select 'with_counts' as cte, (select count(*) from with_counts) as ct
                union
                select 'with_averages' as cte, (select count(*) from with_averages) as ct
                union
                select '*****cohort_applications_plus' as cte, (select count(*) from cohort_applications_plus where was_accepted=true) as ct
                union
                select '*****cohort_status_changes' as cte, (select count(*) from cohort_status_changes) as ct
                union
                select '*****published_cohorts' as cte, (select count(*) from published_cohorts) as ct

            ~).to_a

            raise "No cohort_user_progress_records: #{result.inspect}"
        end
    end
end
