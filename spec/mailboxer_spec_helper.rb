module MailboxerSpecHelper

    def create_conversation(sender, recipient, candidate_position_interest, hiring_relationship = nil)
        Mailboxer::Conversation.create_from_hash!({
            subject: "new subject",
            candidate_position_interest_id: candidate_position_interest.id,
            messages: [{
                "body" => "new message body",
                "sender_id" => sender.id,
                "sender_type" => 'user'
            }],
            recipients: [
                {
                    'type' => 'user',
                    'id' => sender.id
                },
                {
                    'type' => 'user',
                    'id' => recipient.id
                }]
        }, sender, hiring_relationship)
    end

end