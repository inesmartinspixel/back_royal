module DerivedContentTableSpecHelper

    def assert_replace_cohort_rows(klass, cohort, &block)
        assert_replace_rows_for_content_item(klass, cohort, :id, :cohort_id, &block)
    end

    def assert_replace_playlist_rows(klass, playlist, &block)
        assert_replace_rows_for_content_item(klass, playlist, :locale_pack_id, :playlist_locale_pack_id, &block)
    end

    # This helper asserts that all the rows for a particular content item get
    # replaced when a change to that content item is published.  The provided
    # block has to actually make a change to the content item that will trigger
    # the change
    def assert_replace_rows_for_content_item(klass, content_item, identifier, col, &block)
        id = content_item.send(identifier)
        assert_replace_rows(klass, [id], col) do
            yield
            content_item.publish!
        end
    end

    # This helper asserts that all of the rows for a list of identifiers get
    # replaced when some block is run.
    def assert_replace_rows(klass, ids, col, &block)
        max_created_at = klass.maximum('created_at')
        orig_count = klass.count
        orig_content_item_count = klass.where(col => ids).count

        yield

        # only rows for these content items should have been modified
        modified_records = klass.where('created_at > ?', max_created_at)
        expect(modified_records.map(&col).uniq).to contain_exactly(*ids)
        expect(modified_records.size).to eq(orig_content_item_count)

        # the total number of records in the db should not have changed (ensuring that we
        # did not accidentally clear out some unrelated records)
        expect(klass.count).to eq(orig_count)
    end

end