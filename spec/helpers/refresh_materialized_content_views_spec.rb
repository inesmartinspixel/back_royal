require 'spec_helper'

describe RefreshMaterializedContentViews do
    fixtures :lesson_streams, :cohorts

    before(:each) do
    end

    describe "groups" do
        it "should refresh views after the groups have changed" do
            institution = Institution.first
            group = AccessGroup.create!(name: 'new_group_for_testing_refresh')
            institution.add_to_group('new_group_for_testing_refresh')
            item = lesson_streams(:published_stream)

            RefreshMaterializedContentViews.refresh(true)
            stream_locale_pack_ids = ActiveRecord::Base.connection.execute("select distinct stream_locale_pack_id from institution_stream_locale_packs where institution_id='#{institution.id}'").to_a.map { |r| r['stream_locale_pack_id']}
            expect(stream_locale_pack_ids).not_to include(item.locale_pack_id)

            hash = {
                'id' => item.id,
                'updated_at' => Time.now,
                'locale_pack' => {
                    'id' => item.locale_pack_id,
                    'content_items' => [
                        {'id' => item.id}
                    ],
                    'groups' => [
                        AccessGroup.find_by_name('SUPERVIEWER'),
                        AccessGroup.find_by_name('new_group_for_testing_refresh')
                    ].as_json
                }

            }
            result = Lesson::Stream.update_from_hash!(@user, hash, {})

            RefreshMaterializedContentViews.refresh(true)
            stream_locale_pack_ids = ActiveRecord::Base.connection.execute("select distinct stream_locale_pack_id from institution_stream_locale_packs where institution_id='#{institution.id}'").to_a.map { |r| r['stream_locale_pack_id']}
            expect(stream_locale_pack_ids).to include(item.locale_pack_id)
        end

        it "should refresh views after the groups have changed" do
            cohort = cohorts(:published_mba_cohort)
            group = AccessGroup.create!(name: 'new_group_for_testing_refresh')
            item = lesson_streams(:published_stream)
            item.locale_pack.add_to_group('new_group_for_testing_refresh')

            RefreshMaterializedContentViews.refresh(true)
            stream_locale_pack_ids = ActiveRecord::Base.connection.execute("select distinct stream_locale_pack_id from published_cohort_stream_locale_packs where cohort_id='#{cohort.id}'").to_a.map { |r| r['stream_locale_pack_id']}
            expect(stream_locale_pack_ids).not_to include(item.locale_pack_id)

            Cohort.update_from_hash!(@user, {updated_at: Time.now, id: cohort.id, groups: [group.as_json]})

            RefreshMaterializedContentViews.refresh(true)
            stream_locale_pack_ids = ActiveRecord::Base.connection.execute("select distinct stream_locale_pack_id from published_cohort_stream_locale_packs where cohort_id='#{cohort.id}'").to_a.map { |r| r['stream_locale_pack_id']}
            expect(stream_locale_pack_ids).to include(item.locale_pack_id)
        end
    end
end