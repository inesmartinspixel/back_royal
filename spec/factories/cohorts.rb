# == Schema Information
#
# Table name: cohorts
#
#  id                                      :uuid             not null, primary key
#  created_at                              :datetime         not null
#  updated_at                              :datetime         not null
#  start_date                              :datetime         not null
#  end_date                                :datetime         not null
#  was_published                           :boolean          default(FALSE)
#  name                                    :text             not null
#  specialization_playlist_pack_ids        :uuid             default([]), is an Array
#  periods                                 :json             not null, is an Array
#  program_type                            :text             default("mba"), not null
#  num_required_specializations            :integer          default(0), not null
#  title                                   :text             not null
#  description                             :text
#  admission_rounds                        :json             is an Array
#  registration_deadline_days_offset       :integer          default(0)
#  external_schedule_url                   :text
#  program_guide_url                       :text
#  enrollment_deadline_days_offset         :integer          default(0), not null
#  stripe_plans                            :json             is an Array
#  scholarship_levels                      :json             is an Array
#  internal_notes                          :text
#  project_submission_email                :string
#  early_registration_deadline_days_offset :integer          default(0)
#  id_verification_periods                 :json             is an Array
#  graduation_days_offset_from_end         :integer
#  diploma_generation_days_offset_from_end :integer
#  learner_project_ids                     :uuid             default([]), not null, is an Array
#  enrollment_agreement_template_id        :text
#  playlist_collections                    :json             not null, is an Array
#  isolated_network                        :boolean          default(FALSE), not null
#

# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do

    factory :cohort do
        start_date { Time.now }
        end_date { Time.now }
        name do
            "cohort_#{rand}"
        end
        title do
            "title_#{rand}"
        end
        playlist_collections { [] }
        specialization_playlist_pack_ids { [] }
    end
end
