# == Schema Information
#
# Table name: roles
#
#  id            :uuid             not null, primary key
#  name          :string(255)
#  resource_id   :uuid
#  resource_type :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryBot.define do
    factory :role do
        name { 'admin' }
    end
end
