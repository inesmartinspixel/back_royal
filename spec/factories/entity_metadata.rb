# == Schema Information
#
# Table name: entity_metadata
#
#  id             :uuid             not null, primary key
#  created_at     :datetime
#  updated_at     :datetime
#  title          :string
#  description    :string
#  canonical_url  :text
#  image_id       :uuid
#  tweet_template :text
#

# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
    factory :entity_metadata do

        title { "My seo title" }
        description { "An awesome lesson" }
        canonical_url { "http://smart.ly/awesome" }

        after(:build) do |entity_metadata|
                entity_metadata.image = S3Asset.new({
                    file: "some_image.png",
                    directory: "images/",
                    :styles => {

                            # student dashboard
                            "50x50"  => '50x50>',
                            "100x100"  => '100x100>',

                            # stream dashboard
                            "110x110"   => '110x110>',
                            "220x220"   => '220x220>'
                    }.to_json
            })
            entity_metadata.image.save!

        end

    end
end
