
FactoryBot.define do

    factory :event, :class => Event do

        association :user, :factory => :user
        client_timestamp = 1234567890
        event_type = "lesson:start"

        estimated_time { Time.at(client_timestamp) }
        created_at { Time.at(client_timestamp) }
        client_reported_time { Time.at(client_timestamp) }
        event_type { event_type }
        total_buffered_seconds { 1 }
        hit_server_at { Time.at(client_timestamp+1) }

        # this must be in a block, otherwise all events
        # will share the same payload instance and a change
        # to one will be a change to all
        payload do
        	{
                'event_type' => event_type,
        		'buffered_time' => 1,
        		'page_load_id' => SecureRandom.uuid,
                'client_utc_timestamp' => client_timestamp
        	}
        end

        after(:build) do |event, evaluator|
            event.user.save! if event.user.present?

            # this is backwards, but it seems like FactoryBot will
            # not let me set the id
            event.payload['id'] = event.id = SecureRandom.uuid
            event.save!
        end
    end
end
