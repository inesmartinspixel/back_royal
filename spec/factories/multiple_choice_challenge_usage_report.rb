
FactoryBot.define do

    factory :multiple_choice_challenge_usage_report, :class => Lesson::MultipleChoiceChallengeUsageReport do

        lesson_id { 'lesson_id' }
        frame_id { 'frame_id' }
        editor_template { 'editor_template' }
        challenge_id { 'challenge_id' }
        answer_id { 'first_answer_id' }
        count { 42 }
    end
end