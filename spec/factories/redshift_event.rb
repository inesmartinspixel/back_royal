FactoryBot.define do

    factory :redshift_event, :class => RedshiftEvent do

        sequence :id do |n|
            SecureRandom.uuid
        end

        sequence :page_load_id do |n|
            SecureRandom.uuid
        end

        client_timestamp = 1234567890
        event_type = "lesson:start"

        estimated_time { Time.at(client_timestamp) }
        client_reported_time { Time.at(client_timestamp) }
        event_type { event_type }
        total_buffered_seconds { 1 }
        hit_server_at { Time.at(client_timestamp+1) }
    end
end
