# == Schema Information
#
# Table name: institutions
#
#  id                :uuid             not null, primary key
#  created_at        :datetime
#  updated_at        :datetime
#  name              :string(255)
#  sign_up_code      :string(255)
#  scorm_token       :uuid
#  playlist_pack_ids :uuid             default([]), is an Array
#  domain            :text
#  external          :boolean          default(TRUE), not null
#

# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do

    sequence :name do |n|
        "institution_name#{rand}"
    end

    factory :institution do
        name
        sign_up_code { "#{name}" }
    end
end
