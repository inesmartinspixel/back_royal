# == Schema Information
#
# Table name: users
#
#  id                                              :uuid             not null, primary key
#  email                                           :string(255)      default("")
#  encrypted_password                              :string(255)      default(""), not null
#  reset_password_token                            :string(255)
#  reset_password_sent_at                          :datetime
#  remember_created_at                             :datetime
#  sign_in_count                                   :integer          default(0)
#  current_sign_in_at                              :datetime
#  last_sign_in_at                                 :datetime
#  current_sign_in_ip                              :string(255)
#  last_sign_in_ip                                 :string(255)
#  created_at                                      :datetime         not null
#  updated_at                                      :datetime         not null
#  failed_attempts                                 :integer          default(0)
#  unlock_token                                    :string(255)
#  locked_at                                       :datetime
#  sign_up_code                                    :string(255)
#  reset_password_redirect_url                     :string(255)
#  provider                                        :string(255)
#  uid                                             :string(255)      default(""), not null
#  tokens                                          :json
#  has_seen_welcome                                :boolean          default(FALSE), not null
#  notify_email_newsletter                         :boolean          default(TRUE), not null
#  school                                          :text
#  provider_user_info                              :json
#  free_trial_started                              :boolean          default(FALSE), not null
#  confirmed_profile_info                          :boolean          default(TRUE), not null
#  pref_decimal_delim                              :string           default("."), not null
#  pref_locale                                     :string           default("en"), not null
#  active_playlist_locale_pack_id                  :uuid
#  avatar_url                                      :text
#  additional_details                              :json
#  mba_content_lockable                            :boolean          default(FALSE), not null
#  job_title                                       :text
#  phone                                           :text
#  country                                         :text
#  has_seen_accepted                               :boolean          default(FALSE), not null
#  name                                            :text
#  nickname                                        :text
#  phone_extension                                 :text
#  can_edit_career_profile                         :boolean          default(FALSE), not null
#  professional_organization_option_id             :uuid
#  pref_show_photos_names                          :boolean          default(TRUE), not null
#  identity_verified                               :boolean          default(FALSE), not null
#  pref_keyboard_shortcuts                         :boolean          default(FALSE), not null
#  sex                                             :text
#  ethnicity                                       :text
#  race                                            :text             default([]), not null, is an Array
#  how_did_you_hear_about_us                       :text
#  anything_else_to_tell_us                        :text
#  birthdate                                       :date
#  address_line_1                                  :text
#  address_line_2                                  :text
#  city                                            :text
#  state                                           :text
#  zip                                             :text
#  last_seen_at                                    :datetime
#  fallback_program_type                           :string
#  program_type_confirmed                          :boolean          default(FALSE), not null
#  invite_code                                     :string
#  notify_hiring_updates                           :boolean          default(TRUE), not null
#  notify_hiring_spotlights                        :boolean          default(TRUE), not null
#  notify_hiring_content                           :boolean          default(TRUE), not null
#  notify_hiring_candidate_activity                :boolean          default(TRUE), not null
#  hiring_team_id                                  :uuid
#  referred_by_id                                  :uuid
#  has_seen_featured_positions_page                :boolean          default(FALSE), not null
#  pref_one_click_reach_out                        :boolean          default(FALSE), not null
#  has_seen_careers_welcome                        :boolean          default(FALSE), not null
#  can_purchase_paid_certs                         :boolean          default(FALSE), not null
#  has_seen_hide_candidate_confirm                 :boolean          default(FALSE), not null
#  pref_positions_candidate_list                   :boolean          default(TRUE), not null
#  skip_apply                                      :boolean          default(FALSE), not null
#  has_seen_student_network                        :boolean          default(FALSE), not null
#  pref_student_network_privacy                    :text             default("full"), not null
#  notify_hiring_spotlights_business               :boolean          default(TRUE), not null
#  notify_hiring_spotlights_technical              :boolean          default(TRUE), not null
#  has_drafted_open_position                       :boolean          default(FALSE), not null
#  deactivated                                     :boolean          default(FALSE), not null
#  avatar_id                                       :uuid
#  has_seen_first_position_review_modal            :boolean          default(FALSE), not null
#  timezone                                        :text
#  pref_allow_push_notifications                   :boolean          default(TRUE), not null
#  english_language_proficiency_comments           :text
#  english_language_proficiency_documents_type     :text
#  english_language_proficiency_documents_approved :boolean          default(FALSE), not null
#  transcripts_verified                            :boolean          default(FALSE), not null
#  has_logged_in                                   :boolean          default(FALSE), not null
#  has_seen_mba_submit_popup                       :boolean          default(FALSE), not null
#  has_seen_short_answer_warning                   :boolean          default(FALSE), not null
#  recommended_positions_seen_ids                  :uuid             default([]), is an Array
#  allow_password_change                           :boolean          default(FALSE), not null
#  notify_candidate_positions                      :boolean
#  notify_candidate_positions_recommended          :text
#  has_seen_hiring_tour                            :boolean          default(FALSE), not null
#  academic_hold                                   :boolean          default(FALSE), not null
#  experiment_ids                                  :string           default([]), not null, is an Array
#  enable_front_royal_store                        :boolean          default(FALSE), not null
#  pref_sound_enabled                              :boolean          default(TRUE), not null
#  student_network_email                           :string(255)
#

# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do

    sequence :email do |n|
        "example#{rand}@example.com"
    end

    factory :user do
        name { 'Testuser' }
        email
        password { 'password' }
        password_confirmation { 'password' }
        uid { "#{email}" }
        provider { "email" }
        phone { "+12025551337" }
        # required if the Devise Confirmable module is used
        # confirmed_at Time.now
    end
end
