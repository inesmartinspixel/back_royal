# Read about factories at https://github.com/thoughtbot/factory_bot

FactoryBot.define do
    factory :s3_transcript_asset do
        file_file_name { "file_name.png" }
        file_content_type { "image/png" }
        file_file_size { 42 }
        file_updated_at { Time.now }
        file_fingerprint { SecureRandom.uuid }
    end
end
