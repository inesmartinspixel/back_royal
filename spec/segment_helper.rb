# inspired by from https://robots.thoughtbot.com/segment-io-and-ruby
# mock out segment so we don't hit the service or require a key in tests
module Segment
    class Analytics
        def initialize
            @identified_users = []
            @tracked_events = []
        end

        def identify(user)
            @identified_users << user
        end

        def track(options)
            @tracked_events << options
        end
    end
end


Object.send(:remove_const, :Analytics) if Object.const_defined?(:Analytics)
Analytics = Segment::Analytics.new