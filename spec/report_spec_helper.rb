module ReportSpecHelper
    extend ActiveSupport::Concern

    class TimeSeriesThing < ApplicationRecord
        belongs_to :user
        self.table_name = 'time_series_things'

        belongs_to :user
    end

    class TabularThing < ApplicationRecord
        belongs_to :user
        self.table_name = 'tabular_things'

        belongs_to :user
    end

    class OldAssessmentScore < ApplicationRecord
        self.table_name = 'old_assessment_scores'
    end

    included do
        delegate :start_time, :finish_time, :days,
                    :to => :report_spec_helper
        # greppable titles:
        # def days; def start_time; def finish_time;
        # def  create_things
        fixtures(:institutions)

        before(:each) do
            # in order to make newly created events available to the dblink, we need to
            # commit the redshift transaction.  Then we destroy them manually in an after(:each)
            RedshiftEvent.connection.transaction_manager.commit_transaction
        end

        after(:each) do
            RedshiftEvent.connection.execute("truncate events")
        end
    end

    # ----------------------------------  email filter helper

    module ClassMethods

        def describe_email_or_name_filter
            describe "email_or_name filter" do
                attr_reader :first_user

                before(:each) do
                    @first_user = relevant_users.where("email ilike ?", '%@pedago.com%').first
                    expect(first_user).not_to be_nil
                    expect(first_user.phone).not_to be_nil

                    last_user = relevant_users.where("name ilike ?", '%pedago%').first
                    expect(last_user).not_to be_nil
                end

                it "should work" do
                    # this should find the users with both email and first name matching
                    expect(count_results(filter_by_email_or_name('pedago'))).to eq(2)

                    # this should only find the email user
                    expect(count_results(filter_by_email_or_name('@pedago.com'))).to eq(1)

                    # it should work with an id
                    expect(count_results(filter_by_email_or_name(first_user.id.slice(0, 5)))).to eq(1)

                    # it should work with a phone number (we may have multiple users with the same phone at this point)
                    expect(count_results(filter_by_email_or_name(first_user.phone.slice(0, 5)))).not_to eq(0)

                    # expect to find no users
                    expect(count_results(filter_by_email_or_name('smartly'))).to eq(0)
                end
            end
        end

    end

    def filter_by_email_or_name(val)
        get_report({
            'filters' => [{
                'filter_type' => 'EmailNameFilter',
                'value' => val
            }]
        })
    end


    # ----------------------------------  sign-up code filter helper

    module ClassMethods

        def describe_sign_up_code_filter

            describe "sign_up_code filter" do
                it "should work" do
                    # sanity checks
                    expect(relevant_users.where(sign_up_code: 'a').count > 0).to be(true)
                    expect(relevant_users.where(sign_up_code: 'b').count > 0).to be(true)

                    # when we only accept the value 'a', only one user is
                    # returned
                    expect(count_results(filter_by_sign_up_code(['a']))).to eq(
                        relevant_users.where(sign_up_code: 'a').count
                    )

                    # when we accept 'a' or 'b', two users are returned
                    expect(count_results(filter_by_sign_up_code(['a', 'b']))).to eq(
                        relevant_users.where(sign_up_code: ['a', 'b']).count
                    )
                end
            end
        end
    end

    def filter_by_sign_up_code(vals)
        get_report({
            'filters' => [{
                'filter_type' => 'SignUpCodeFilter',
                'value' => vals
            }]
        })
    end

    # ----------------------------------------- institution filter helper

    module ClassMethods

        def describe_institution_filter
            describe "institution filter" do

                before(:each) do
                    @institution, @another_institution = Institution.limit(2).to_a

                    @user = @institution.users.first
                    create_things(@user)

                    # sanity checks, there should be users both in and not in
                    # institutions who have activity
                    expect(relevant_users.select(&:institution).size > 0).to be(true)
                    expect(relevant_users.reject(&:institution).size > 0).to be(true)

                end

                it "should work" do

                    # when there is no user in the institution, nothing is returned
                    expect(count_results(filter_by_institution([@another_institution.id]))).to eq(0)

                    # when there is a user in the institution, he is returned
                    expect(count_results(filter_by_institution([@institution.id]))).to eq(
                        @institution.users.where(id: relevant_users.ids).count
                    )
                end

                it "should work with null values" do

                    # asking for null should return all reports users except for the one in an institution
                    expect(count_results(filter_by_institution(['__NULL__']))).to eq(
                        relevant_users
                            .joins("LEFT JOIN institutions_users ON institutions_users.user_id = users.id")
                            .where("institutions_users.institution_id IS NULL")
                            .count
                    )

                    # when looking for null or the institution, all users should be returned
                    expect(count_results(filter_by_institution(['__NULL__', @institution.id]))).to eq(relevant_users.size)
                end

            end
        end
    end

    def filter_by_institution(vals)
        get_report({
            'filters' => [{
                'filter_type' => 'InstitutionFilter',
                'value' => vals
            }]
        })
    end

    # ----------------------------------------- cohort filter helper

    module ClassMethods

        def describe_cohort_filter
            describe "cohort filter" do

                before(:each) do
                    @cohort, @another_cohort = Cohort.limit(2).to_a

                    @user = CohortApplication.find_by_cohort_id(@cohort.id).user
                    create_things(@user)

                    # sanity checks, there should be users both in and not in
                    # cohorts who have activity
                    expect(relevant_users.select(&:application_for_relevant_cohort).size > 0).to be(true)
                    expect(relevant_users.reject(&:application_for_relevant_cohort).size > 0).to be(true)

                end

                it "should work" do

                    # when there is no user in the cohort, nothing is returned
                    expect(count_results(filter_by_cohort([@another_cohort.id]))).to eq(0)

                    # when there is a user in the cohort, he is returned
                    expect(count_results(filter_by_cohort([@cohort.id]))).to eq(
                        CohortApplication.where(user_id: relevant_users.ids, cohort_id: @cohort.id).count
                    )
                end

                it "should work with null values" do

                    # asking for null should return all reports users except for the one in an cohort
                    expect(count_results(filter_by_cohort(['__NULL__']))).to eq(
                        relevant_users
                            .joins("LEFT JOIN cohort_applications ON cohort_applications.user_id = users.id")
                            .where("cohort_applications.user_id IS NULL")
                            .count
                    )

                    # when looking for null or the cohort, all users should be returned
                    expect(count_results(filter_by_cohort(['__NULL__', @cohort.id]))).to eq(relevant_users.size)
                end

            end
        end
    end

    def filter_by_cohort(vals)
        get_report({
            'filters' => [{
                'filter_type' => 'CohortFilter',
                'value' => vals
            }]
        })
    end


    # ----------------------------------------------------------- role filter helper
    module ClassMethods

        def describe_role_filter

            describe "role filter" do

                before(:each) do
                    @learner_role = Role.where(:name => "learner").first
                    @fake_role = Role.find_or_create_by(:name => "fake")
                end

                it "should work" do

                    # when there is no user in the role, nothing is returned
                    expect(count_results(filter_by_role([@fake_role.name]))).to eq(0)

                    # ensure we find 2 learners, as expected
                    expect(count_results(filter_by_role([@learner_role.name]))).to eq(relevant_users.size)
                end
            end
        end
    end

    def filter_by_role(vals)
        get_report({
            'filters' => [{
                'filter_type' => 'RoleFilter',
                'value' => vals
            }]
        })
    end

    # ------------------------------------------------------- group filter helper

    module ClassMethods

        def describe_group_filter

            describe "group filter" do

                before(:each) do
                    @institution = institutions(:institution_with_streams)
                    @institution_group = AccessGroup.where(name: @institution.name).first
                    expect(@institution_group).not_to be_nil

                    # sanity check.  There should be no users who are directly assigned
                    # to this group.  Only users assigned to the institution
                    expect(User.joins(:access_groups).where("access_groups.name = ?", @institution_group.name).count).to eq(0)

                    @fake_group = AccessGroup.find_or_create_by(:name => "fake_group")

                    @institution_user = @institution.users.first

                    # add a new user in an institution
                    create_things(@institution_user)

                    # add a new user with no groups for testing purposes
                    ungrouped_user = User.joins("LEFT JOIN access_groups_users ON access_groups_users.user_id = users.id")
                                    .joins("LEFT JOIN institutions_users ON institutions_users.user_id = users.id")
                                    .where.not(id: relevant_users.ids)
                                    .where("access_groups_users.access_group_id IS NULL")
                                    .where("institutions_users.institution_id IS NULL")
                                    .first
                    expect(ungrouped_user).not_to be_nil
                    create_things(ungrouped_user)

                    @grouped_user_count = @default_group.users.where("users.id" => User.ids).count

                    @institution_user_count = 1
                    @ungrouped_users = relevant_users.select do |reports_user|
                        reports_user.access_groups.empty? && !reports_user.institutions.include?(@institution)
                    end
                    @ungrouped_user_count = @ungrouped_users.size
                end

                it "should work" do

                    # when there is no user in the group, nothing is returned
                    expect(count_results(filter_by_group([@fake_group.name]))).to eq(0)

                    # when there is a user in the institution with the group, they are returned
                    expect(count_results(filter_by_group([@institution_group.name]))).to eq(
                        @institution_user_count
                    )

                    # when there is a user in the group, they are returned
                    expect(count_results(filter_by_group([@default_group.name]))).to eq(
                        @grouped_user_count
                    )

                    # with both groups return everyone
                    expect(count_results(filter_by_group([@default_group.name, @institution_group.name]))).to eq(
                        @grouped_user_count + @institution_user_count
                    )
                end

                it "should work with null values" do
                    # asking for null should return any users not in groups
                    expect(count_results(filter_by_group(['__NULL__']))).to eq(
                        @ungrouped_user_count
                    )

                    # when looking for null or the institution group, any groups users should be found
                    expect(count_results(filter_by_group(['__NULL__', @institution_group.name]))).to eq(
                        @ungrouped_user_count + @institution_user_count
                    )

                    # when looking for null or the institution group or default group, all users should be returned
                    expect(count_results(filter_by_group(['__NULL__', @institution_group.name, @default_group.name]))).to eq(
                        @ungrouped_user_count + @institution_user_count + @grouped_user_count
                    )
                end
            end
        end
    end

    # ------------------------------------------------------- cohort status filter helper
    module ClassMethods

        def describe_cohort_status_filter

            describe "cohort status filter" do

                it "should find a cohort status" do
                    user = relevant_users.first
                    cohort = Cohort.first
                    CohortApplication.create!(user_id: user.id, status: 'accepted', cohort_slack_room_id: cohort.slack_rooms[0]&.id, cohort_id: cohort.id, applied_at: Time.now)
                    expected_users = relevant_users.joins(:cohort_applications).where("cohort_applications.status" =>  'accepted')
                    expect(expected_users.count).to be > 0
                    expect(count_results(filter_by_cohort_status(['accepted']))).to eq(expected_users.count)
                end

                it "should find a graduation status" do
                    user = relevant_users.first
                    cohort = Cohort.first
                    CohortApplication.create!(user_id: user.id, status: 'accepted', cohort_slack_room_id: cohort.slack_rooms[0]&.id, cohort_id: cohort.id, applied_at: Time.now, graduation_status: 'honors')
                    expected_users = relevant_users.joins(:cohort_applications).where("cohort_applications.graduation_status" =>  'honors')
                    expect(expected_users.count).to be > 0
                    expect(count_results(filter_by_cohort_status(['honors']))).to eq(expected_users.count)
                end

                it "should find users with no applications" do
                    expected_users = relevant_users.left_outer_joins(:cohort_applications).where("cohort_applications.id" =>  nil)
                    expect(expected_users.count).to be > 0
                    expect(count_results(filter_by_cohort_status(['__NULL__']))).to eq(expected_users.count)
                end
            end
        end
    end

    def filter_by_cohort_status(vals)
        get_report({
            'filters' => [{
                'filter_type' => 'CohortStatusFilter',
                'value' => vals
            }]
        })
    end

    def filter_by_group(vals)
        get_report({
            'filters' => [{
                'filter_type' => 'GroupFilter',
                'value' => vals
            }]
        })
    end

    def self.start_time
        Time.parse('2015/01/01 00:00:00 UTC')
    end

    def self.finish_time
        Time.parse('2015/03/15 00:00:00 UTC')
    end

    def self.setup_user_for_users_report_specs

        streams = Lesson::Stream.all_published.where(locale: 'en').limit(2)
        assessment_lessons = Lesson.all_published.where(locale: 'en', assessment: true).limit(3)
        non_assessment_lessons = Lesson.all_published.where(locale: 'en', assessment: false, test: false).limit(2)

        ######## user_for_users_report_spec
        user = FactoryBot.create(:user)

        ReportSpecHelper.do_lesson(
            user,
            non_assessment_lessons[0],
            {
                total_lesson_time: 21,
                complete: false
            }
        )
        ReportSpecHelper.do_lesson(
            user,
            non_assessment_lessons[1],
            {
                total_lesson_time: 21,
                complete: true
            }
        )
        ReportSpecHelper.do_lesson(
            user,
            assessment_lessons[0], {
                total_lesson_time: 21,
                complete: true,
                assessment_scores: [0.76, 0.98, 0.34]
            }
        )

        ReportSpecHelper.do_stream(user, streams[0], {complete: false})
        ReportSpecHelper.do_stream(user, streams[1], {complete: true})

        WriteRegularlyIncrementallyUpdatedTablesJob.perform_now
        RefreshMaterializedInternalReportsViews.perform_now
        user
    end

    def self.setup_user_with_old_assessment_scores
        assessment_lessons = Lesson.all_published.where(locale: 'en', assessment: true).limit(3)
        user = FactoryBot.create(:user)

        # start the lesson, but do not put any assessment scores in finish events
        ReportSpecHelper.do_lesson(
            user,
            assessment_lessons[0],
            {
                total_lesson_time: 21,
                complete: true
            }
        )

        OldAssessmentScore.create!(
            user_id: user.id,
            locale_pack_id: assessment_lessons[0].locale_pack_id,
            average_assessment_score_first: 0.42,
            average_assessment_score_last: 0.84
        )

        WriteRegularlyIncrementallyUpdatedTablesJob.perform_now
        RefreshMaterializedInternalReportsViews.perform_now
        user
    end

    def self.setup_user_with_one_finish_on_an_assessment_lesson
        assessment_lessons = Lesson.all_published.where(locale: 'en', assessment: true).limit(3)
        user = FactoryBot.create(:user)
        ReportSpecHelper.do_lesson(
            user,
            assessment_lessons[0], {
                total_lesson_time: 21,
                complete: true,
                assessment_scores: [0.98]
            }
        )
        WriteRegularlyIncrementallyUpdatedTablesJob.perform_now
        RefreshMaterializedInternalReportsViews.perform_now
        user
    end

    def self.days
        if !defined? @days
            days = []
            day = start_time - 15.days
            while day < (finish_time + 15.days) do
                days << day
                day = day + 1.day
            end
            @days = days.map(&:utc)
        end
        @days
    end

    def self.create_time_series_things(user)

        ReportSpecHelper.days.map do |day|
            [{}].map do |attrs|
                user_id = user.id
                ReportSpecHelper::TimeSeriesThing.create!(attrs.merge(
                    user_id: user_id,
                    time: day
                ))
            end
        end

        user
    end

    def self.create_tabular_things(user, attrs = {})
        ReportSpecHelper::TabularThing.create!({
            user_id: user.id,
            color: 'red',
            shape: 'square'
        }.merge(attrs))
    end

    def self.do_lesson(user, lesson, options = {})
        time = Time.parse('2017/01/01')
        events = []
        events << RedshiftEvent.create!(
            id: SecureRandom.uuid,
            user_id: user.id,
            event_type: 'lesson:frame:unload',
            estimated_time: time,
            duration_total: options[:total_lesson_time] || 1,
            lesson_id: lesson.id
        )

        LessonProgress.create!(
            user_id: user.id,
            locale_pack_id: lesson.locale_pack_id,
            started_at: time,
            completed_at: options[:complete] ? time + 10.minutes : nil,
            best_score: (options[:assessment_scores] || []).max
        )

        if options[:complete] && !options[:assessment_scores]
            events << RedshiftEvent.create!(
                id: SecureRandom.uuid,
                user_id: user.id,
                event_type: 'lesson:finish',
                estimated_time: time,
                lesson_id: lesson.id
            )
        end

        (options[:assessment_scores] || []).each_with_index do |assessment_score, i|
            events << RedshiftEvent.create!(
                id: SecureRandom.uuid,
                user_id: user.id,
                event_type: 'lesson:finish',
                estimated_time: time + i.seconds,
                score: assessment_score,
                lesson_id: lesson.id
            )
        end
    end

    def self.do_stream(user, stream, options = {})
        time = Time.parse('2017/01/01')

        Lesson::StreamProgress.create!(
            user_id: user.id,
            locale_pack_id: stream.locale_pack_id,
            started_at: time,
            completed_at: options[:complete] ? time + 10.minutes : nil
        )
    end

    def report_spec_helper
        ReportSpecHelper
    end

end