require 'spec_helper'
require 'back_royal/object'

describe ActiveSupport::JSON do

    describe "InvalidUtf8Support" do

        it "should rescue from invalid escaped character JSON::ParserError and replace with unicode REPLACEMENT CHARACTER" do
            invalid_event_json = '{"event_type":"\ud83d"}'

            result = nil
            expect {
                result = ActiveSupport::JSON.decode(invalid_event_json)
            }.not_to raise_error
            expect(result['event_type']).to eq("\uFFFD")
        end

        it "should raise if json does not match event_type" do
            invalid_event_json = '{"foobar":"\ud83d"}'

            result = nil
            expect {
                result = ActiveSupport::JSON.decode(invalid_event_json)
            }.to raise_error(JSON::ParserError)
        end

        it "should raise if any other JSON::ParserError" do
            invalid_event_json = '{"event_type":"foobar"}'

            err = JSON::ParserError.new("foobar")
            expect(JSON).to receive(:parse).and_raise(err)

            expect {
                ActiveSupport::JSON.decode(invalid_event_json)
            }.to raise_error(JSON::ParserError)
        end

        it "should work for valid json" do
            expect(ActiveSupport::JSON.decode('{"event_type":"foobar"}')).to eq({"event_type"=>"foobar"})
        end

    end

end