require 'spec_helper'
require 'back_royal/object'

describe ActiveRecord::Base do


    # see https://github.com/rails/rails/issues/27201
    describe "changes" do

        it "should not include a json field changing from nil to nil" do
            user = User.first
            ActiveRecord::Base.connection.execute("update users set provider_user_info=NULL where id='#{user.id}'")
            user = User.find(user.id)
            expect(user.changes).to be_empty
        end

    end

    # see https://github.com/rails/rails/issues/27201
    describe "previous_changes" do

        it "should not include a json field changing from nil to nil" do
            user = User.first
            User.connection.execute("update users set provider_user_info=NULL where id='#{user.id}'")
            user = User.find(user.id)
            user.save!

            expect(user.previous_changes['provider_user_info']).to be_nil
        end

    end

end