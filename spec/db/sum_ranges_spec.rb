require 'spec_helper'

describe 'sum_ranges aggregate' do

    it "should work" do
        # The following will return '4 days'::interval,
        # since the first two ranges overlap to create a total of
        # 3 days and the last range adds another day
        result = ActiveRecord::Base.connection.execute(%Q~

            with rows AS MATERIALIZED (
                select tsrange('2011/01/01', '2011/01/03') as t
                union
                select tsrange('2011/01/02', '2011/01/04') as t
                union
                select tsrange('2011/01/06', '2011/01/07') as t
            )
            select extract(epoch from sum_ranges(t order by t)) as result from rows;
        ~).to_a[0]['result']
        expect(result).to eq(4.days)
    end

    it "should raise if ranges are not ordered" do
        expect {
            result = ActiveRecord::Base.connection.execute(%Q~
                with rows AS MATERIALIZED (
                    select tsrange('2011/01/01', '2011/01/03') as t
                    union
                    select tsrange('2011/01/02', '2011/01/04') as t
                    union
                    select tsrange('2011/01/06', '2011/01/07') as t
                )
                select extract(epoch from sum_ranges(t order by t desc)) as result from rows;
            ~).to_a[0]['result']
        }.to raise_error(ActiveRecord::StatementInvalid, /ranges must be ordered with the lowest/)
    end

end