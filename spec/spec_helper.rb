# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/mocks'
require Rails.root.join('spec', 'fixture_builder', 'ruby_fixture_builder')
require Rails.root.join('spec', 'fixture_builder', 'puppet_fixture_builder')
require Rails.root.join('spec', 'stripe_helper')
require Rails.root.join('spec', 'segment_helper')

Dir.glob(Rails.root.join('spec', 'matchers', '**', '*.rb')).each do |path|
    require path
end

# Checks for pending migrations before tests are run.
# If you are not using ActiveRecord, you can remove this line
ActiveRecord::Migration.maintain_test_schema!

# disable spoof detection during tests
module Paperclip
    module Validators
        class MediaTypeSpoofDetectionValidator < ActiveModel::EachValidator
            def validate_each(record, attribute, value)
            end
        end
    end

    class Attachment
        def save
            return true
        end

        def queue_all_for_delete
            return true
        end
    end

    # FIXME: https://trello.com/c/ua2I3fLM
    # class BulkQueue
    #     def process
    #         raise 'test'
    #     end
    # end
end

# Ensures we try not to do anything with AWS
module Aws
    module Core
        class Client
            def client_request name, options, &read_block
                raise "Stay out of AWS in tests"
            end
        end
    end
end

# we are nto using real images, so we can't calculate
# real dimensions
require File.expand_path("../../app/models/s3_asset", __FILE__)
module StubbedAddDimensions

    def add_dimensions(file, skip_stub = false)
        if skip_stub
            super(file)
        else
            self.dimensions = {
                width: 42,
                height: 42
            }
            self
        end
    end

    # keep the old alias_method_chain naming, since we're relying
    # on it in a bunch of places
    def add_dimensions_without_stub(file)
        add_dimensions(file, true)
    end

end

S3Asset.send(:prepend, StubbedAddDimensions)

class CertificateGenerator
    def self.generate_asset(user, opts)
        S3Asset.first
    end
end


# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

class Object

    def log_queries
        @orig_logger = ActiveRecord::Base.logger
        ActiveRecord::Base.logger = Logger.new(STDOUT)

        if block_given?
            return_value = yield
            stop_logging_queries
            return_value
        end
    end

    def stop_logging_queries
        ActiveRecord::Base.logger = @orig_logger
    end

end

RSpec.configure do |config|

    if ENV.key?('f')
        config.filter_run_including :focus => true
    end

    config.before(:suite) do
        FileUtils.mkdir_p(Rails.root.join('tmp', 'test_cache'))
    end

    config.around(:each) do |example|
        start = Time.now
        example.run
        elapsed = Time.now - start

        puts_longer_than = nil
        if puts_longer_than && elapsed > puts_longer_than
            puts "#{elapsed} seconds : #{self.inspect}"
        end

        # clear the cache after each spec
        Rails.cache.clear
    end

    config.before(:each) do
        allow_any_instance_of(Paperclip::Attachment).to receive(:save).and_return(true)
        allow_any_instance_of(Paperclip::Attachment).to receive(:queue_all_for_delete).and_return(true)

        # FIXME: Add methods that return true for these too -- https://trello.com/c/ua2I3fLM
        allow(Paperclip::BulkQueue).to receive(:process).and_return(true)
        allow_any_instance_of(Paperclip::Storage::S3).to receive(:expiring_url).and_return('fake url')


        allow_any_instance_of(PrivateDocumentMixin).to receive(:save_attached_files).and_return(true)
        allow_any_instance_of(PrivateDocumentMixin).to receive(:delete_attached_files).and_return(true)
        allow_any_instance_of(PrivateDocumentMixin).to receive(:post_process).and_return(true)

        [AvatarAsset, S3Asset, S3ExerciseDocument, S3ProjectDocument].each do |klass|
            allow(klass).to receive(:save_attached_files).and_return(true)
            allow(klass).to receive(:delete_attached_files).and_return(true)
            allow(klass).to receive(:post_process).and_return(true)
        end

        # It would be a pain to have to worry about this validation in specs, so just mock it everywhere
        # but the `institution_user_join` spec suite, where we unmock it.
        # See https://trello.com/c/QFVa6l5W
        allow_any_instance_of(InstitutionUserJoin).to receive(:ensure_no_acceptable_application_for_user)
    end

    # ## Mock Framework
    #
    # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
    #
    # config.mock_with :mocha
    # config.mock_with :flexmock
    # config.mock_with :rr


    config.infer_spec_type_from_file_location!

    config.raise_errors_for_deprecations!

    config.backtrace_exclusion_patterns = [
            /lib\/rspec/,
            /lib\/factory_bot/
    ]

    # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
    config.fixture_path = "#{::Rails.root}/spec/fixtures"

    # If you're not using ActiveRecord, or you'd prefer not to run each of your
    # examples within a transaction, remove the following line or assign false
    # instead of true.
    config.use_transactional_fixtures = true

    # If true, the base class of anonymous controllers will be inferred
    # automatically. This will be the default behavior in future versions of
    # rspec-rails.
    config.infer_base_class_for_anonymous_controllers = false

    # Run specs in random order to surface order dependencies. If you find an
    # order dependency and want to debug it, you can fix the order by providing
    # the seed, which is printed after each run.
    #     --seed 1234
    config.order = "random"

end

module ActiveModel::Validations

    def stub_invalid(test, message = 'Stubbed out to be invalid')
        test.allow(self).to test.receive(:valid?).and_return(false)
        mock_errors = {}
        test.allow(mock_errors).to test.receive(:full_messages).and_return([message])
        test.allow(self).to test.receive(:errors).and_return(mock_errors)
    end

end
