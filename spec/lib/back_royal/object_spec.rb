require 'spec_helper'
require 'back_royal/object'

describe Object do

    describe "looks_like_uuid?" do

        it "should return the expected value in some example cases" do

            expect(SecureRandom.uuid.looks_like_uuid?).to be(true)
            expect("#{SecureRandom.uuid}@example.com".looks_like_uuid?).to be(false)
            expect("prefix#{SecureRandom.uuid}".looks_like_uuid?).to be(false)
            expect("asfsadfsad".looks_like_uuid?).to be(false)
            expect(1.looks_like_uuid?).to be(false)

        end

    end

end
