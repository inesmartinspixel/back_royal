require 'spec_helper'
require 'back_royal/array'

describe Array do
    describe "to_sql" do
        it "should work with strings" do
            expect(['foo', 'bar'].to_sql).to eq("ARRAY['foo','bar']")
        end

        it "should work non-strings" do
            expect([1, 2,].to_sql).to eq("ARRAY[1,2]")
            expect([1.0, 2.0,].to_sql).to eq("ARRAY[1.0,2.0]")
        end

        it "should throw out nil values" do
            expect(['foo', nil, 'bar'].to_sql).to eq("ARRAY['foo','bar']")
        end

        it "should return a null array if no values" do
            expect([].to_sql).to eq("ARRAY[NULL]")
        end
    end
end
