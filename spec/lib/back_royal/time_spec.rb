require 'spec_helper'
require 'back_royal/time'

describe Time do
    attr_accessor :last_standard_ts, :last_dst_ts, :dst_ts

    before(:each) do
        @last_standard_ts = Time.at(1520704800) # 1520704800 => 03.10.2018 (last day of standard time in 2018)
        @last_dst_ts = Time.at(1541264400) # 1541264400 - 11.03.2018 (last day of daylight savings in 2018)
        @dst_ts = Time.at(1532581200) # 1532581200 - 07.26.2018
    end

    describe "::DEFAULT_RELATIVE_THRESHOLD_TIMEZONE" do

        it "should be 'Pacific Time (US & Canada)'" do
            expect(Time::DEFAULT_RELATIVE_THRESHOLD_TIMEZONE).to eq('Pacific Time (US & Canada)')
        end
    end

    describe "add_dst_aware_offset" do

        describe "with days" do

            it "should correctly add day offset when epoch crosses from standard to DST" do
                expect(@last_standard_ts.add_dst_aware_offset(1.days).to_timestamp).to eq((@last_standard_ts.utc + 23.hours).to_timestamp)
            end

            it "should correctly add day offset when epoch crosses from DST to standard" do
                expect(@last_dst_ts.add_dst_aware_offset(1.days).to_timestamp).to eq((@last_dst_ts.utc + 25.hours).to_timestamp)
            end

            it "should work with negative offset" do
                ts = @last_dst_ts + 24.hours
                expect(ts.add_dst_aware_offset(-1.days).to_timestamp).to eq((ts.utc - 25.hours).to_timestamp)
            end

            it "should correctly add day offset when epoch does not cross DST boundary" do
                expect(@dst_ts.add_dst_aware_offset(1.days).to_timestamp).to eq((@dst_ts.utc + 24.hours).to_timestamp)
            end

        end

        describe "with hours" do

            it "should ignore dst when epoch crosses from standard to DST" do
                expect(@last_standard_ts.add_dst_aware_offset(24.hours).to_timestamp).to eq((@last_standard_ts.utc + 24.hours).to_timestamp)
            end

            it "should ignore dst when epoch crosses from DST to standard" do
                expect(@last_dst_ts.add_dst_aware_offset(24.hours).to_timestamp).to eq((@last_dst_ts.utc + 24.hours).to_timestamp)
            end

            it "should work with negative offset" do
                ts = @last_dst_ts + 24.hours
                expect(ts.add_dst_aware_offset(-24.hours).to_timestamp).to eq((ts.utc - 24.hours).to_timestamp)
            end

            it "should correctly add hours offset when epoch does not cross DST boundary" do
                expect(@dst_ts.add_dst_aware_offset(24.hours).to_timestamp).to eq((@dst_ts.utc + 24.hours).to_timestamp)
            end

        end

    end

    describe "relative_to_threshold" do

        describe "when no timezone is passed in" do

            it "should not apply threshold if hour is 11pm in #{Time::DEFAULT_RELATIVE_THRESHOLD_TIMEZONE}" do
                time = Time.utc(2018, 1, 2, 7) # January 1, 2018 @ 11:00 PM Pacific Time (US & Canada)
                expect(time.relative_to_threshold).to eq(time)
            end

            it "should apply threshold if hour is before 11pm in #{Time::DEFAULT_RELATIVE_THRESHOLD_TIMEZONE}" do
                time = Time.utc(2018, 1, 2, 6) # January 1, 2018 @ 10:00 PM Pacific Time (US & Canada)
                expect(time.relative_to_threshold).to eq(time.yesterday)
            end
        end

        describe "when timezone is passed in" do

            it "should not apply threshold if hour is 11pm in the specified timezone" do
                time = Time.utc(2018, 1, 1, 22) # January 2, 2018 @ 11:00 PM Europe/Warsaw
                expect(time.relative_to_threshold(timezone: 'Europe/Warsaw')).to eq(time)
            end

            it "should apply threshold if hour is before 11pm in the specified timezone" do
                time = Time.utc(2018, 1, 1, 21) # January 2, 2018 @ 10:00 PM Europe/Warsaw
                expect(time.relative_to_threshold(timezone: 'Europe/Warsaw')).to eq(time.yesterday)
            end
        end
    end

end
