require 'spec_helper'
require 'url_helper'

describe UrlHelper do

    describe "domain_root" do

        before(:each) do
            @original_app_env_name = ENV['APP_ENV_NAME']
        end

        after(:each) do
            ENV['APP_ENV_NAME'] = @original_app_env_name
        end

        it "should be localhost in local development mode" do
            expect(Rails.env).to receive(:development?).and_return(true)
            expect(UrlHelper.domain_root).to eq('localhost')
        end

        it "should be support the staging ENV" do
            ENV['APP_ENV_NAME'] = 'staging'
            expect(UrlHelper.domain_root).to eq("staging.#{AppConfig.quantic_domain}")
        end

        it "should default to AppConfig.quantic_domain" do
            expect(UrlHelper.domain_root).to eq(AppConfig.quantic_domain)
        end

        it "should support an alternative domain" do
            ENV['APP_ENV_NAME'] = 'staging'
            expect(UrlHelper.domain_root(AppConfig.smartly_domain)).to eq("staging.#{AppConfig.smartly_domain}")
        end

    end

    describe "protocol" do

        describe "without request" do

            it "should be http in local development" do
                expect(Rails.env).to receive(:development?).and_return(true)
                expect(UrlHelper.protocol).to eq('http://')
            end

            it "should be https otherwise" do
                expect(Rails.env).to receive(:development?).and_return(false)
                expect(UrlHelper.protocol).to eq('https://')
            end

        end

        describe "with request" do

            it "should be http if the host is localhost" do
                mock_request = OpenStruct.new(host: 'localhost')
                expect(UrlHelper.protocol(mock_request)).to eq('http://')
            end

            it "should be http if the host is *.elasticbeanstalk.com" do
                mock_request = OpenStruct.new(host: 'some-site.elasticbeanstalk.com')
                expect(UrlHelper.protocol(mock_request)).to eq('http://')
            end

            it "should be https otherwise" do
                mock_request = OpenStruct.new(host: 'quantic.edu')
                expect(UrlHelper.protocol(mock_request)).to eq('https://')
            end

        end

    end

    describe "port" do

        describe "without request" do

            it "should be :3000 in local development" do
                expect(Rails.env).to receive(:development?).and_return(true)
                expect(UrlHelper.port).to eq(':3000')
            end

            it "should be empty otherwise" do
                expect(Rails.env).to receive(:development?).and_return(false)
                expect(UrlHelper.port).to eq('')
            end

        end

        describe "with request" do

            it "should be mirror the request port" do
                mock_request = OpenStruct.new(port: 1234)
                expect(UrlHelper.port(mock_request)).to eq(':1234')
            end

        end

    end

    describe "domain_url" do
        it "should be include the protocol, domain, and port" do
            expect(UrlHelper).to receive(:protocol) { 'https://' }
            expect(UrlHelper).to receive(:domain_root) { 'some-domain.com' }
            expect(UrlHelper).to receive(:port) { ':12345' }
            expect(UrlHelper.domain_url).to eq('https://some-domain.com:12345')
        end
    end

end