require 'spec_helper'

describe OpenPosition::OpenPositionRelationExtensions do
    fixtures(:users)

    before(:each) do
        @candidate = users(:user_with_career_profile)
        @candidate.hiring_manager_relationships.delete_all
        @candidate.candidate_position_interests.delete_all

        @hiring_manager = users(:hiring_manager)
        @hiring_manager_without_team = User.joins(:hiring_application).left_outer_joins(:hiring_team).where("hiring_teams.id IS NULL").first
        @hiring_manager_with_team = users(:hiring_manager_with_team)
        @hiring_manager_teammate = users(:hiring_manager_teammate)
        expect(@hiring_manager_teammate.hiring_team_id).to eq(@hiring_manager_with_team.hiring_team_id) # sanity
    end

    def update_career_profile_search_helper
        @candidate.career_profile.touch
        CareerProfile::SearchHelper.write_new_records
    end

    describe "viewable_when_considering_relationships_and_interests" do
        it "should filter out positions from hiring teams who have liked the candidate" do
            position = OpenPosition.where(hiring_manager_id: @hiring_manager_with_team.id).first
            HiringRelationship.create!({hiring_manager_id: @hiring_manager_teammate.id, candidate_id: @candidate.id, hiring_manager_status: 'accepted', candidate_status: 'accepted'})
            results = OpenPosition.all.viewable_when_considering_relationships_and_interests(candidate_or_id: @candidate)
            expect(results.pluck(:id)).not_to include(position.id)
        end

        it "should filter out positions from hiring managers who have rejected the candidate" do
            position = OpenPosition.where(hiring_manager_id: @hiring_manager.id).first
            HiringRelationship.create!({hiring_manager_id: @hiring_manager.id, candidate_id: @candidate.id, hiring_manager_status: 'rejected'})
            results = OpenPosition.all.viewable_when_considering_relationships_and_interests(candidate_or_id: @candidate)
            expect(results.pluck(:id)).not_to include(position.id)
        end

        it "should filter out positions for a hiring manager with no team who has liked this candidate" do
            position = OpenPosition.create!(hiring_manager_id: @hiring_manager_without_team.id, title: "foo", available_interview_times: 'times', featured: true)
            HiringRelationship.create!(hiring_manager_id: @hiring_manager_without_team.id,candidate_id: @candidate.id, hiring_manager_status: "accepted")
            results = OpenPosition.all.viewable_when_considering_relationships_and_interests(candidate_or_id: @candidate)
            expect(results.pluck(:id)).not_to include(position.id)
        end

        it "should not filter out positions that the candidate has accepted" do
            positions = OpenPosition.where(hiring_manager_id: @hiring_manager_with_team.id).limit(3)
            expect(positions.length).to be(3) # sanity

            CandidatePositionInterest.create!(
                candidate_id: @candidate.id,
                open_position_id: positions.first.id,
                candidate_status:  'accepted',
                admin_status: 'reviewed'
            )
            CandidatePositionInterest.create!(
                candidate_id: @candidate.id,
                open_position_id: positions.second.id,
                candidate_status:  'accepted',
                admin_status: 'outstanding'
            )
            CandidatePositionInterest.create!(
                candidate_id: @candidate.id,
                open_position_id: positions.third.id,
                candidate_status:  'accepted',
                admin_status:  'unreviewed'
            )

            HiringRelationship.create!({hiring_manager_id: @hiring_manager_teammate.id, candidate_id: @candidate.id, hiring_manager_status: 'accepted', candidate_status: 'accepted'})

            results = OpenPosition.all.viewable_when_considering_relationships_and_interests(candidate_or_id: @candidate)
            ids = results.pluck(:id)
            expect(ids).to include(positions.first.id)
            expect(ids).to include(positions.second.id)
            expect(ids).to include(positions.third.id)
        end

        it "should filter out positions that the candidate has rejected" do
            position = OpenPosition.where(hiring_manager_id: @hiring_manager_with_team.id).first
            expect(position).not_to be_nil # sanity

            CandidatePositionInterest.create!(
                candidate_id: @candidate.id,
                open_position_id: position.id,
                candidate_status:  'rejected'
            )
            HiringRelationship.create!({hiring_manager_id: @hiring_manager_teammate.id, candidate_id: @candidate.id, hiring_manager_status: 'accepted', candidate_status: 'accepted'})

            results = OpenPosition.all.viewable_when_considering_relationships_and_interests(candidate_or_id: @candidate)
            expect(results.pluck(:id)).not_to include(position.id)
        end

        it "should not filter out positions if teammate has non-liked relationship" do
            position = OpenPosition.where(hiring_manager_id: @hiring_manager_with_team.id).first
            expect(position).not_to be_nil # sanity

            HiringRelationship.create!({hiring_manager_id: @hiring_manager_teammate.id, candidate_id: @candidate.id, hiring_manager_status: 'saved_for_later', candidate_status: 'accepted'})

            results = OpenPosition.all.viewable_when_considering_relationships_and_interests(candidate_or_id: @candidate)
            expect(results.pluck(:id)).to include(position.id)
        end

        # FIXME: Really I don't think this logic should care about whether or not a hiring manager
        # is accepted, and thus has a team, and thus whose positions can be seen; but we ended up
        # taking out the clause that this spec was testing in commit 2491967, `hiring_managers.hiring_team_id IS NULL`,
        # and adding it back in now would cause positions from non-accepted hiring managers to show up. It's
        # accidental though IMO, so I'm leaving this spec here commented out for now.
        # it "should not filter out positions because of the hiring manager not having a team" do
        #     # Ran into this issue due to the fact that `SELECT NULL NOT IN (1)` is false, so a hiring manager
        #     # that did not have a team would be thrown out if the candidate had accepted relationships
        #     # with anyone else that was on a team. It is interesting to note that if the candidate did not
        #     # have other relationships then the check would pass because `SELECT NULL NOT IN (empty set)` is
        #     # actually true.
        #     # See the clause `hiring_managers.hiring_team_id NOT IN (#{hiring_teams_who_have_liked_this_candidate.to_sql})`

        #     position = OpenPosition.create!(hiring_manager_id: @hiring_manager_without_team.id, title: "foo", available_interview_times: 'times', featured: true, archived: false)

        #     # Ensure the candidate has a relationship with a different hiring manager that is on a team.
        #     HiringRelationship.create!(hiring_manager_id: @hiring_manager_with_team.id,
        #         candidate_id: @candidate.id,
        #         hiring_manager_status: "accepted")

        #     results = OpenPosition.all.viewable_when_considering_relationships_and_interests(candidate_or_id: @candidate)
        #     expect(results.pluck(:id)).to include(position.id)
        # end

        it "should not filter out positions if the candidate has relationships with other hiring_managers not on a team" do
            # Ran into this issue due to the fact that `SELECT 1 NOT IN (NULL)` is false, so since we were getting null
            # values when selecting the hiring_team_id, an open_position for a hiring_manager that did have a team would
            # be thrown out. Actually, this would also happen if the open_position's hiring manager was not on a team
            # as well because `SELECT NULL NOT IN (NULL)` is false as well.

            position = OpenPosition.create!({
                hiring_manager_id: @hiring_manager_with_team.id,
                title: "foo",
                available_interview_times: 'times'})

            HiringRelationship.create!({
                hiring_manager_id: @hiring_manager_without_team.id,
                candidate_id: @candidate.id,
                hiring_manager_status: 'accepted',
                candidate_status: 'pending'})

            results = OpenPosition.all.viewable_when_considering_relationships_and_interests(candidate_or_id: @candidate)
            expect(results.pluck(:id)).to include(position.id)
        end

        describe "candidate_has_acted_on" do
            before(:each) do
                @positions = OpenPosition.where(hiring_manager_id: @hiring_manager_with_team.id).limit(4)
                expect(@positions.length).to be(4) # sanity

                CandidatePositionInterest.create!(
                    candidate_id: @candidate.id,
                    open_position_id: @positions.first.id,
                    candidate_status:  'accepted',
                    admin_status: 'reviewed'
                )
                CandidatePositionInterest.create!(
                    candidate_id: @candidate.id,
                    open_position_id: @positions.second.id,
                    candidate_status:  'accepted',
                    admin_status: 'outstanding'
                )
                CandidatePositionInterest.create!(
                    candidate_id: @candidate.id,
                    open_position_id: @positions.third.id,
                    candidate_status:  'rejected',
                    admin_status:  'unreviewed'
                )
                CandidatePositionInterest.create!(
                    candidate_id: @candidate.id,
                    open_position_id: @positions.fourth.id,
                    candidate_status:  'rejected',
                    admin_status: 'outstanding'
                )
            end

            it "should work when true" do
                ids = OpenPosition.all.viewable_when_considering_relationships_and_interests(candidate_or_id: @candidate, candidate_has_acted_on: true).pluck(:id)
                @positions.pluck(:id).each do |position_id|
                    expect(ids).to include(position_id)
                end
            end

            it "should work when false" do
                ids = OpenPosition.all.viewable_when_considering_relationships_and_interests(candidate_or_id: @candidate, candidate_has_acted_on: false).pluck(:id)
                @positions.pluck(:id).each do |position_id|
                    expect(ids).not_to include(position_id)
                end
            end
        end
    end

    describe "recommended" do
        before(:each) do
            @candidate.career_profile.update!({
                employment_types_of_interest: ['permanent'],
                primary_areas_of_interest: ['foo'],
                place_details: {'lat' => 42.3601, 'lng' => -71.0589}, locations_of_interest: ['boston']
            })
            allow_any_instance_of(CareerProfile::SearchHelper).to receive(:get_years_of_experience).and_return(100);

            boston_details = {'lat' => 42.3601, 'lng' => -71.0589}
            OpenPosition.update_all({
                position_descriptors: ['permanent'],
                role: 'foo',
                title: 'aaa',
                place_details: boston_details,
                desired_years_experience: {min: 100, max: 101}
            })

            expect(OpenPosition.count).to be > 5 # sanity check - need at least this many to avoid false positives
        end

        describe "select_recommended" do
            it "should compute recommended flag" do
                @positions = OpenPosition.limit(4)

                @positions[0].update_columns(position_descriptors: ['permanent', 'part_time'])
                @positions[1].update_columns(position_descriptors: ['contract'])
                @positions[2].update_columns(position_descriptors: ['contract', 'internship'])
                @positions[3].update_columns(position_descriptors: [])
                @candidate.career_profile.update!(employment_types_of_interest: ['contract'])

                results = OpenPosition.all.recommended(candidate_or_id: @candidate).select_recommended(candidate_or_id: @candidate)
                expect(results.to_a.pluck("recommended")).to match_array([true, true])
            end
        end

        describe "without necessary information" do
            def assert_no_results
                results = OpenPosition.all.recommended(candidate_or_id: @candidate)
                expect(results.to_a.pluck(:id)).to be_empty
            end

            it "should return nothing if no employment_types_of_interest" do
                @candidate.career_profile.update!(employment_types_of_interest: [])
                assert_no_results
            end

            it "should return nothing if no primary_areas_of_interest" do
                @candidate.career_profile.update!(primary_areas_of_interest: [])
                assert_no_results
            end

            it "should return nothing if neither place_details nor real_locations_of_interest" do
                @candidate.career_profile.update!(place_details: {}, locations_of_interest: ['none', 'flexible'])
                assert_no_results
            end
        end

        describe "position_descriptors" do
            before(:each) do
                @positions = OpenPosition.limit(4)
                @positions[0].update_columns(position_descriptors: ['permanent', 'part_time'])
                @positions[1].update_columns(position_descriptors: ['contract'])
                @positions[2].update_columns(position_descriptors: ['contract', 'internship'])
                @positions[3].update_columns(position_descriptors: [])
            end

            it "should include positions when candidate has one descriptor" do
                @candidate.career_profile.update!(employment_types_of_interest: ['contract'])
                results = OpenPosition.all.recommended(candidate_or_id: @candidate)
                expect(results.pluck(:id)).to match_array([@positions[1].id, @positions[2].id])
            end

            it "should include positions when candidate has multiple descriptors" do
                @candidate.career_profile.update!(employment_types_of_interest: ['contract', 'part_time'])
                results = OpenPosition.all.recommended(candidate_or_id: @candidate)
                expect(results.pluck(:id)).to match_array([@positions[0].id, @positions[1].id, @positions[2].id])
            end

            it "should work when no matches" do
                @candidate.career_profile.update!(employment_types_of_interest: ['seasonal'])
                results = OpenPosition.all.recommended(candidate_or_id: @candidate)
                expect(results.pluck(:id)).to be_empty
            end
        end

        describe "role" do
            before(:each) do
                @positions = OpenPosition.limit(5)
                @positions[0].update_columns(role: 'foo')
                @positions[1].update_columns(role: 'bar')
                @positions[2].update_columns(role: 'baz')
                @positions[3].update_columns(role: nil)
                @positions[4].update_columns(role: nil, title: 'A foo creator')
            end

            it "should include positions when candidate has one descriptor" do
                @candidate.career_profile.update!(primary_areas_of_interest: ['baz'])
                results = OpenPosition.all.recommended(candidate_or_id: @candidate)
                expect(results.pluck(:id)).to match_array([@positions[2].id])
            end

            it "should include positions when candidate has multiple descriptors" do
                @candidate.career_profile.update!(primary_areas_of_interest: ['baz', 'bar'])
                results = OpenPosition.all.recommended(candidate_or_id: @candidate)
                expect(results.pluck(:id)).to match_array([@positions[1].id, @positions[2].id])
            end

            it "should not match on 'other'" do
                @candidate.career_profile.update!(primary_areas_of_interest: ['bar', 'other'])
                @positions[0].update_columns(role: 'other')
                results = OpenPosition.all.recommended(candidate_or_id: @candidate)
                expect(results.pluck(:id)).to match_array([@positions[1].id])
            end

            it "should work when no matches" do
                @candidate.career_profile.update!(employment_types_of_interest: ['nothing'])
                results = OpenPosition.all.recommended(candidate_or_id: @candidate)
                expect(results.pluck(:id)).to be_empty
            end
        end

        describe "location" do
            it "should include location that is near one of the locations_of_interest" do
                @candidate.career_profile.update!(place_details: {}, locations_of_interest: ['washington_dc'])
                @positions = OpenPosition.limit(3)
                @positions[0].update_columns(place_details: {'lat' => 38.9072, 'lng' => -77.0369}) # dc
                @positions[1].update_columns(place_details: {'lat' => 38.9072, 'lng' => -77.0369}) # dc
                @positions[2].update_columns(place_details: {'lat' => 38.9847, 'lng' => -77.0947}) # bethesda

                results = OpenPosition.all.recommended(candidate_or_id: @candidate)
                expect(results.pluck(:id)).to match_array([@positions[0].id, @positions[1].id, @positions[2].id])
            end

            it "should include location that is near the location of the candidate" do
                @candidate.career_profile.update!(place_details: {'lat' => 38.9072, 'lng' => -77.0369}, locations_of_interest: []) # dc
                @positions = OpenPosition.limit(3)
                @positions[0].update_columns(place_details: {'lat' => 38.9072, 'lng' => -77.0369}) # dc
                @positions[1].update_columns(place_details: {'lat' => 38.9072, 'lng' => -77.0369}) # dc
                @positions[2].update_columns(place_details: {'lat' => 38.9847, 'lng' => -77.0947}) # bethesda

                results = OpenPosition.all.recommended(candidate_or_id: @candidate)
                expect(results.pluck(:id)).to match_array([@positions[0].id, @positions[1].id, @positions[2].id])
            end

            it "should work when no matches" do
                @candidate.career_profile.update!(place_details: {'lat' => 0, 'lng' => 0}, locations_of_interest: ['none'])
                results = OpenPosition.all.recommended(candidate_or_id: @candidate)
                expect(results.pluck(:id)).to be_empty
            end

            it "should not error for edge case where there are no locations to use" do
                @candidate.career_profile.update!(place_details: {}, locations_of_interest: ['none'])
                results = OpenPosition.all.recommended(candidate_or_id: @candidate)
                expect(results.pluck(:id)).to be_empty
            end
        end

        describe "years_of_experience" do
            def setup_and_assert_years_of_experience(candidate_value, position_min, position_max, expected = true)
                allow_any_instance_of(CareerProfile::SearchHelper).to receive(:get_years_of_experience).and_return(candidate_value)
                position = OpenPosition.first
                position.update_columns(desired_years_experience: {min: position_min, max: position_max})

                results = OpenPosition.all.recommended(candidate_or_id: @candidate)

                if expected
                    expect(results.pluck(:id)).to eq([position.id])
                else
                    expect(results.pluck(:id)).to be_empty
                end
            end

            describe "nil years_of_experience" do
                it "should work when should be included" do
                    setup_and_assert_years_of_experience(nil, 0, 1)
                end

                it "should work when should not be included" do
                    setup_and_assert_years_of_experience(nil, 1, 2, false)
                end
            end

            describe "defined min and max" do
                it "should work when inside range" do
                    setup_and_assert_years_of_experience(0.9, 0, 1)
                end

                it "should be a fuzzy min" do
                    setup_and_assert_years_of_experience(0.1, 1, 2)
                end

                it "should be a fuzzy max" do
                    setup_and_assert_years_of_experience(2.9, 1, 2)
                end

                it "should not work when outside range" do
                    setup_and_assert_years_of_experience(3, 1, 2, false)
                end
            end

            describe "null min" do
                it "should work when null min and included" do
                    setup_and_assert_years_of_experience(0.9, nil, 1)
                end

                it "should work when null min and not included" do
                    setup_and_assert_years_of_experience(2, nil, 1, false)
                end
            end

            describe "null max" do
                it "should work when null max and included" do
                    setup_and_assert_years_of_experience(2, 1, nil)
                end

                it "should not work when null max not included" do
                    setup_and_assert_years_of_experience(0, 1, nil, false)
                end
            end
        end
    end

    describe "not notified" do
        it "should work" do
            open_positions = OpenPosition.limit(5).to_a
            expect(OpenPosition.where(id: open_positions.pluck(:id)).not_notified(candidate_or_id: @candidate).size).to be(5)
            NotifiedRecommendedOpenPosition.create!(user_id: @candidate.id, open_position_id: open_positions.first.id)
            NotifiedRecommendedOpenPosition.create!(user_id: @candidate.id, open_position_id: open_positions.last.id)
            expect(OpenPosition.where(id: open_positions.pluck(:id)).not_notified(candidate_or_id: @candidate).size).to be(3)
        end
    end

    describe "order_by_recommended" do
        before(:each) do
            # Prevent intermittent failures if the open_positions that get deleted
            # in some of the proceding specs are referenced by a hiring_relationship
            HiringRelationship.delete_all

            # Ensure consistent ordering and grab some test positions in the middle to prevent confusing
            # failures and false positives
            @candidate = CareerProfile::WorkExperience.first.career_profile.user
            CareerProfile::WorkExperience.update_all(role: nil)
            CareerProfile::WorkExperience.update_all(industry: nil)

            @candidate.career_profile.update!(employment_types_of_interest: [])
            OpenPosition.update_all(position_descriptors: [])

            @candidate.career_profile.update!(primary_areas_of_interest: [])
            OpenPosition.update_all(role: nil, title: 'title')

            @candidate.career_profile.update!(place_details: {}, locations_of_interest: [])
            OpenPosition.update_all(place_details: {})

            OpenPosition.update_all(desired_years_experience: {'min': 100, 'max': 100})

            OpenPositionsSkillsOptionsJoin.delete_all

            update_career_profile_search_helper
            OpenPosition.update_all(updated_at: Time.now)

            default_orders = ["open_positions.updated_at DESC", :title, :id] # see also order block in open_positions_controller_spec.rb
            @not_first_by_default_positions = OpenPosition.order(*default_orders).limit(3).offset(4)
            @position = @not_first_by_default_positions.first
        end

        describe "position_descriptors" do
            it "should order using employment_types_of_interest keys of the candidate on the position_descriptors" do
                @candidate.career_profile.update_attribute(:employment_types_of_interest, ['foo_time', 'bar_time', 'something_else'])
                @position.update_columns(position_descriptors: ['bar_time', 'baz_time'])
                results = OpenPosition.all.order_by_recommended(candidate_or_id: @candidate)
                expect(results.first.id).to eq(@position.id)
            end
        end

        describe "career_profile roles" do
            # By doing a cast like "Array[]::text[]" we prevent any issues with an empty array
            it "should handle empty array for primary_areas_of_interest keys" do
                @candidate.career_profile.update_attribute(:employment_types_of_interest, [])
                @position.update_column(:position_descriptors, ['bar_time', 'baz_time'])
                expect {
                    OpenPosition.all.order_by_recommended(candidate_or_id: @candidate)
                }.not_to raise_error
            end

            it "should order using primary_areas_of_interest keys of the candidate on the position role" do
                @candidate.career_profile.update_attribute(:primary_areas_of_interest, ['foo', 'bar', 'something_else'])
                @position.update_column(:role, 'bar')
                results = OpenPosition.all.order_by_recommended(candidate_or_id: @candidate)
                expect(results.first.id).to eq(@position.id)
            end

            it "should exclude 'other'" do
                @candidate.career_profile.update_attribute(:primary_areas_of_interest, ['foo', 'bar', 'other'])
                @position.update_column(:role, 'other')
                results = OpenPosition.all.order_by_recommended(candidate_or_id: @candidate)
                expect(results.first.id).not_to eq(@position.id)
            end
        end

        describe "location" do
            before(:each) do
                open_positions = OpenPosition.limit(7)

                @country_us = {'short' => 'US'}
                @country_de = {'short' => 'DE'}

                @dc_position = open_positions[2]
                @dc_position.update!({:place_details => {'lng' => -77.0369, 'lat' => 38.9072, 'country' => @country_us}})

                @irrelevant_position = open_positions[6]
                @irrelevant_position.update!({
                    :title => 'aaaaa',
                    :place_details => {'lng' => 0, 'lat' => 0}
                })

                update_career_profile_search_helper
            end

            it "should order using location of candidate" do
                # Bethesda should find dc position
                @candidate.career_profile.update_attribute(:place_details, {'lng' => -77.0947, 'lat' => 38.9847, 'country' => @country_us})
                results = OpenPosition.all.order_by_recommended(candidate_or_id: @candidate)
                expect(results.first.id).to eq(@dc_position.id)
            end

            it "should not error for edge case where there are no locations to use" do
                @candidate.career_profile.update!({
                    place_details: {},
                    locations_of_interest: ['none']
                })
                expect {
                    OpenPosition.all.order_by_recommended(candidate_or_id: @candidate)
                }.not_to raise_error
            end

            describe "locations_of_interest for the candidate" do
                it "should order when matches one" do
                    # Their career_profile location should not order dc position higher, but
                    # their locations_of_interest should
                    @candidate.career_profile.update!({
                        :locations_of_interest => ['san_francisco', 'washington_dc', 'austin']
                    })
                    results = OpenPosition.all.order_by_recommended(candidate_or_id: @candidate)
                    expect(results.first.id).to eq(@dc_position.id)
                end

                it "should not order when none or flexible" do
                    # Their career_profile should not order dc position higher
                    @candidate.career_profile.update!({
                        :locations_of_interest => ['none', 'flexible']
                    })
                    results = OpenPosition.all.order_by_recommended(candidate_or_id: @candidate)
                    expect(results.first.id).not_to eq(@dc_position.id)
                end
            end
        end

        describe "work_experience" do
            describe "roles" do
                it "should order using employment_types_of_interest keys of the candidate on the position_descriptors" do
                    @candidate.career_profile.work_experiences.first.update!(role: 'foo_engineering')
                    @position.update_columns(role: 'foo_engineering')
                    update_career_profile_search_helper
                    results = OpenPosition.all.order_by_recommended(candidate_or_id: @candidate)
                    expect(results.first.id).to eq(@position.id)
                end
            end

            describe "industries" do
                it "should order using employment_types_of_interest keys of the candidate on the position_descriptors" do
                    @candidate.career_profile.work_experiences.first.update!(industry: 'bar_production')
                    @position.hiring_manager.hiring_application.update!(industry: 'bar_production')

                    # Ensure only one position
                    other_positions = @position.hiring_manager.open_positions.where.not(id: @position.id)
                    CandidatePositionInterest.where(open_position_id: other_positions.pluck(:id)).delete_all
                    other_positions.delete_all
                    update_career_profile_search_helper

                    results = OpenPosition.all.order_by_recommended(candidate_or_id: @candidate)
                    expect(results.first.id).to eq(@position.id)
                end
            end

            describe "years_of_experience" do
                def setup_and_assert_years_of_experience(candidate_value, position_min, position_max, expected = true)
                    allow_any_instance_of(CareerProfile::SearchHelper).to receive(:get_years_of_experience).and_return(candidate_value);
                    @position.update_columns(desired_years_experience: {min: position_min, max: position_max})
                    results = OpenPosition.all.order_by_recommended(candidate_or_id: @candidate)

                    if expected
                        expect(results.first.id).to eq(@position.id)
                    else
                        expect(results.first.id).not_to eq(@position.id)
                    end
                end

                describe "nil years_of_experience" do
                    it "should work when should be included" do
                        setup_and_assert_years_of_experience(nil, 0, 1)
                    end

                    it "should work when should not be included" do
                        setup_and_assert_years_of_experience(nil, 1, 2, false)
                    end
                end

                describe "defined min and max" do
                    it "should work when inside range" do
                        setup_and_assert_years_of_experience(0.9, 0, 1)
                    end

                    it "should be a fuzzy min" do
                        setup_and_assert_years_of_experience(0.1, 1, 2)
                    end

                    it "should be a fuzzy max" do
                        setup_and_assert_years_of_experience(2.9, 1, 2)
                    end

                    it "should not work when outside range" do
                        setup_and_assert_years_of_experience(3, 1, 2, false)
                    end
                end

                describe "null min" do
                    it "should work when null min and included" do
                        setup_and_assert_years_of_experience(0.9, nil, 1)
                    end

                    it "should work when null min and not included" do
                        setup_and_assert_years_of_experience(2, nil, 1, false)
                    end
                end

                describe "null max" do
                    it "should work when null max and included" do
                        setup_and_assert_years_of_experience(2, 1, nil)
                    end

                    it "should not work when null max not included" do
                        setup_and_assert_years_of_experience(0, 1, nil, false)
                    end
                end
            end
        end

        describe "skills" do

            it "should not blow up with single quotes in skills_options columns" do
                skill = SkillsOption.create(text: "Master of Kung's and Foo's")

                @candidate.career_profile.skills = [skill]
                @candidate.career_profile.save!

                position = @not_first_by_default_positions.first
                position.skills = [skill]
                position.save!

                update_career_profile_search_helper

                expect {
                    OpenPosition.all.order_by_recommended(candidate_or_id: @candidate).to_a
                }.not_to raise_error
            end

            # This essentially tests using COALESCE
            it "should not order positions with no skills first" do
                @candidate.career_profile.skills = [SkillsOption.create(text: '123')]
                @candidate.career_profile.save!

                position_with_no_skills = @not_first_by_default_positions.first
                position_with_no_skills.skills&.destroy_all
                position_with_no_skills.update_columns(updated_at: Time.now + 10.years)

                # Has skills but none of them match
                position_with_no_matching_skills = @not_first_by_default_positions.second
                position_with_no_matching_skills.skills = [SkillsOption.create(text: '456')]
                position_with_no_matching_skills.save!
                position_with_no_matching_skills.update_columns(updated_at: Time.now + 11.years)

                update_career_profile_search_helper

                results = OpenPosition.all.order_by_recommended(candidate_or_id: @candidate)
                expect(results.first.id).not_to eq(position_with_no_skills.id)
            end

            it "should order by number of skills for the candidate matching skills on the position" do
                shared_skill = SkillsOption.create(text: "Master Creator of Foo")
                another_shared_skill = SkillsOption.create(text: "Bar Developer")

                @candidate.career_profile.skills = [shared_skill, another_shared_skill, SkillsOption.create(text: '123')]
                @candidate.career_profile.save!

                single_match_position = @not_first_by_default_positions.first
                single_match_position.skills = [shared_skill, SkillsOption.create(text: '456')]
                single_match_position.save!

                double_match_position = @not_first_by_default_positions.second
                double_match_position.skills = [shared_skill, another_shared_skill, SkillsOption.create(text: '789')]
                double_match_position.save!

                update_career_profile_search_helper

                results = OpenPosition.all.order_by_recommended(candidate_or_id: @candidate)
                expect(results.first.id).to eq(double_match_position.id)
                expect(results.second.id).to eq(single_match_position.id)
            end
        end
    end
end