require 'spec_helper'
require 'back_royal/paginate_fastly'

describe ActiveRecord::Relation do

    describe "paginate_fastly" do

        it "should work with a limit and an offset" do

            users = User.reorder(:email).limit(10).offset(10)

            result, total_count = User.reorder(:email).paginate_fastly(limit: 10, offset: 10)

            expect(result.map(&:id)).to eq(users.map(&:id))
            expect(total_count).to eq(User.count)

        end

        it "should work with a limit and no offset" do

            users = User.reorder(:email).limit(10)

            result, total_count = User.reorder(:email).paginate_fastly(limit: 10, offset: nil)

            expect(result.map(&:id)).to eq(users.map(&:id))
            expect(total_count).to eq(User.count)

        end

        it "should work with no limit" do

            users = User.reorder(:email)

            result, total_count = User.reorder(:email).paginate_fastly(limit: nil, offset: nil)

            expect(result.map(&:id)).to eq(users.map(&:id))
            expect(total_count).to eq(User.count)

        end

        it "should work with a page" do

            users = User.reorder(:email).limit(10).offset(10)

            result, total_count = User.reorder(:email).paginate_fastly(limit: 10, page: 2)

            expect(result.map(&:id)).to eq(users.map(&:id))
            expect(total_count).to eq(User.count)

        end

        it "should support max_total_count" do
            max_total_count = User.count - 10
            users = User.reorder(:email).limit(10)

            result, total_count = User.reorder(:email).paginate_fastly(limit: 10, offset: nil, max_total_count: User.count - 10)

            expect(result.map(&:id)).to eq(users.map(&:id))
            expect(total_count).to eq(max_total_count)

        end

        it "should work when no records are returned for given offset" do
            result, total_count = User.reorder(:email).paginate_fastly(limit: 10, offset: User.count + 1)
            expect(result).to be_empty
            expect(total_count).to eq(User.count)
        end

        it "should work when the offset is more than the max_total_count" do
            result, total_count = User.reorder(:email).paginate_fastly(limit: 10, offset: 101, max_total_count: 100)
            expect(result.size).to eq(10)
            expect(total_count).to eq(100)
        end

        it "should work when the offset is more than the number of records in db" do
            result, total_count = User.reorder(:email).paginate_fastly(limit: 10, offset: User.count + 10, max_total_count: 100)
            expect(result.size).to eq(0)
            expect(total_count).to eq(100)
        end

        it "should work when the offset is close to the total_count" do
            total_count_in_db = User.count
            max_total_count = total_count_in_db - 10
            offset =  max_total_count - 10
            result, total_count = User.reorder(:email).paginate_fastly(limit: 10, offset: offset, max_total_count: max_total_count)
            expect(result.size).to eq(10)
            expect(total_count).to eq(max_total_count)
        end

    end

end