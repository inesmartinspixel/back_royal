require 'spec_helper'
require 'tabu_search'

describe "sitemap configuration" do

    before(:all) do
        FileUtils.rm_rf(Rails.root.join(SitemapHelper.public_path))
        SitemapGenerator::Interpreter.run
        xml =  # if your xml is in the 'data.xml' file
        @smartly_urls =  get_urls('smart.ly')
        @quantic_urls =  get_urls('quantic.edu')
    end
    
    describe "smart.ly sitemap" do

        it "should include a smartly-only route" do
            expect(@smartly_urls).to include('https://smart.ly/the-free-certificates')
        end

        it "should not include a quantic-only route" do
            expect(@smartly_urls).not_to include('https://smart.ly/reviews')
        end

        it "should include a shared route" do
            expect(@smartly_urls).to include('https://smart.ly/sign-in')
        end

        it "should not include courses" do
            course_entry = get_course_entry(@smartly_urls)
            expect(course_entry ).to be_nil
        end

        # special-case
        it "should include /our-programs " do
            expect(@smartly_urls).to include('https://smart.ly/our-programs')
        end

    end

    describe "quantic.edu sitemap" do

        it "should not include a smartly-only route" do
            expect(@quantic_urls).not_to include('https://quantic.edu/the-free-certificates')
        end

        it "should include a quantic-only route" do
            expect(@quantic_urls).to include('https://quantic.edu/reviews')
        end

        it "should include a shared route" do
            expect(@quantic_urls).to include('https://quantic.edu/sign-in')
        end

        it "should include course sitemap" do
            course_entry = get_course_entry(@quantic_urls)
            expect(course_entry).to start_with('https://quantic.edu')
        end

    end


    def get_urls(domain)
        path = SitemapHelper.main_sitemap_path(domain)
        xml = File.open(path).read
        hash = Hash.from_xml( xml )
        Set.new hash['urlset']['url'].pluck('loc')
    end

    def get_course_entry(urls)
        urls.detect { |url| url.match('course/title')}
    end

end