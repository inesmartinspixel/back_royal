require 'spec_helper'
require 'http_helpers_extensions'

describe HttpHelpersExtensions do

    def expect_request(absolute_url, headers, constraint_klass, expect_value)
        host = URI.parse(absolute_url).host
        env = Rack::MockRequest.env_for(absolute_url, headers.merge({"HTTP_HOST" => host}))
        request = ActionDispatch::TestRequest.create(env)
        expect(constraint_klass.new.matches?(request)).to eq expect_value
    end

    def mock_development
        allow(Rails).to receive(:env) { "development".inquiry }
    end

    def mock_staging
        allow(Rails).to receive(:env) { "production".inquiry }
    end

    def mock_production
        allow(Rails).to receive(:env) { "production".inquiry }
    end

    describe "QuanticRedirectConstraint" do

        def expect_quantic_route(absolute_url, headers={})
            expect_request(absolute_url, headers, HttpHelpersExtensions::QuanticRedirectConstraint, false)
        end

        def expect_quantic_redirect(absolute_url, headers={})
            expect_request(absolute_url, headers, HttpHelpersExtensions::QuanticRedirectConstraint, true)
        end

        describe('in development') do
            before(:each) do
                mock_development
            end

            it "should not redirect with localhost" do
                expect_quantic_route("http://localhost:3001/testing")
            end

            it "should redirect with localhost and force_host value" do
                expect_quantic_redirect("http://localhost:3001/testing?force_host=smart.ly")
            end
        end

        describe('in staging') do
            before(:each) do
                mock_staging
            end

            it "should not redirect with staging quantic host" do
                expect_quantic_route("https://staging.quantic.edu/testing")
            end

            it "should redirect with staging smartly host" do
                expect_quantic_redirect("https://staging.smart.ly/testing")
            end
        end

        describe('in production') do
            before(:each) do
                mock_production
            end

            it "should not redirect with quantic host" do
                expect_quantic_route("https://quantic.edu/testing")
            end

            it "should redirect with smartly host" do
                expect_quantic_redirect("https://smart.ly/testing")
            end

            it "should not redirect with alternative staging host" do
                allow(AppConfig).to receive(:is_alternative_staging_environment).and_return("true")
                expect_quantic_route("https://smartly-staging-foobar.us-east-1.elasticbeanstalk.com")
            end
        end
    end


    describe "get_smartly" do

        def expect_smartly_route(absolute_url, headers={})
            expect_request(absolute_url, headers, HttpHelpersExtensions::SmartlyConstraint, true)
        end

        def expect_smartly_not_route(absolute_url, headers={})
            expect_request(absolute_url, headers, HttpHelpersExtensions::SmartlyConstraint, false)
        end

        describe('in development') do
            before(:each) do
                mock_development
            end

            it "should not match route with localhost" do
                expect_smartly_not_route("http://localhost:3001/testing")
            end

            it "should match route with localhost and force_host value" do
                expect_smartly_route("http://localhost:3001/testing?force_host=smart.ly")
            end
        end

        describe('in production') do
            before(:each) do
                mock_production
            end

            it "should not match route with quantic host" do
                expect_smartly_not_route("https://quantic.edu/testing")
            end

            it "should match route with smartly host" do
                expect_smartly_route("https://smart.ly/testing")
            end

            it "should match route with with alternative staging host" do
                allow(AppConfig).to receive(:is_alternative_staging_environment).and_return("true")
                expect_smartly_route("https://smartly-staging-foobar.us-east-1.elasticbeanstalk.com")
            end
        end

    end
end