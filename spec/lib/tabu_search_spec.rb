require 'spec_helper'
require 'tabu_search'

describe TabuSearch do

    class MyTabuSearch < TabuSearch

        def initialize(states)
            super()
            @states = states
        end

        def generate_initial_state
            @states[0]
        end

        def possible_moves(state)
            @states.select { |possible_state|
                (possible_state[:value] - state[:value]).abs < 2
            }.map do |_state|
                {
                    move_to: _state
                }
            end
        end

        def result_of_move(state, move)
            move[:move_to]
        end

        def value_after_move(move, current_state, current_state_value)
            new_state = result_of_move(current_state, move)
            value_of_state(new_state)
        end

        def move_is_tabu?(state, move)
            @tabu_state == move[:move_to]
        end

        def value_of_state(state, log=false)
            state[:value]
        end

        def after_move(state, move_details)
            @tabu_state = state
        end

        def minimum_substantive_value_change
            0
        end

        def verbose
            false
        end

    end

    before(:each) do
        @states = 1.upto(100).map do |i|
            {
                value: i
            }
        end
    end

    describe "exec" do

        it "should find the best state" do
            best_state = @states.last
            instance = MyTabuSearch.new(@states).exec
            expect(instance.best_state).to eq(best_state)
        end

        it "should skip a tabu state" do
            best_state = @states.last
            expected_state = @states.reverse.second

            instance = MyTabuSearch.new(@states)
            expect(instance).to receive(:move_is_tabu?).at_least(1).times do |old_state, move|
                move[:move_to] == best_state
            end
            expect(instance).to receive(:move_satisfies_aspiration_criteria?).at_least(1).times.and_return(false)
            instance.exec
            expect(instance.best_state).to eq(expected_state)
        end

        it "should allow a tabu state if move results in a better value than the best we have found" do
            best_state = @states.last
            instance = MyTabuSearch.new(@states)
            expect(instance).to receive(:move_is_tabu?).at_least(1).times do |old_state, move|
                move[:move_to] == best_state
            end
            instance.exec
            expect(instance.best_state).to eq(best_state)
        end

    end
end