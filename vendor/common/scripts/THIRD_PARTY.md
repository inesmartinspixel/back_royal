The following scripts are unmanaged, third-party dependencies:

`modernizr.custom.91603.js` - a custom Modernizr build built from https://modernizr.com/
`ui-bootstrap-custom-tpls-2.5.0.js` - custom Angular UI Bootstrap package built from https://angular-ui.github.io/bootstrap/