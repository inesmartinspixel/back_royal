/* eslint-disable func-names */
/* eslint-disable no-restricted-globals */

//------------------------------
// HTMLLIElement poly (And. 4.4)
//------------------------------

// see also: https://developer.mozilla.org/en-US/docs/Web/API/ChildNode/remove#Polyfill
if (!('remove' in Element.prototype)) {
    Element.prototype.remove = function () {
        this.parentNode.removeChild(this);
    };
}

//------------------------------
// Number.isInteger poly
//------------------------------

// see also: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isInteger#Polyfill
Number.isInteger =
    Number.isInteger || (value => typeof value === 'number' && isFinite(value) && Math.floor(value) === value);

//------------------------------
// Number.MAX_SAFE_INTEGER poly
//------------------------------

// see also: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/MAX_SAFE_INTEGER
if (!Number.MAX_SAFE_INTEGER) {
    Number.MAX_SAFE_INTEGER = 2 ** 53 - 1; // 9007199254740991
}

//------------------------------
// Endpoint Root
//------------------------------

// On web, this is set in app/views/shared/_head_scripts.html.erb
if (window.CORDOVA) {
    // NOTE: to override, see hybrid/js/cordova_bootstrap.js
    window.ENDPOINT_ROOT = window.CORDOVA.endpointRoot || 'https://quantic.edu';
}

//-------------------------------
// Common Inititialization
//-------------------------------

$(() => {
    //------------------------------
    // iOS External Link Workaround
    //------------------------------

    // Super hack to prevent mobile apps saved to springboard from redirecting back to Safari
    // http://www.bennadel.com/blog/2302-Preventing-Links-In-Standalone-iPhone-Applications-From-Opening-In-Mobile-Safari.htm
    if (window.navigator.standalone) {
        $(document).on('click', 'a', function (event) {
            // Don't steal clicks for links marked as external or using rails data-* attributes
            if (
                $(this).data('external') ||
                $(this).data('method') ||
                $(this).data('remote') ||
                $(this).data('dismiss') ||
                $(this).data('toggle') ||
                $(this).data('confirm') ||
                $(this).attr('target') === '_blank'
            ) {
                return;
            }

            const destination = $(event.target).attr('href');

            if (destination) {
                event.preventDefault();
                location.href = destination;
            }
        });
    }

    //------------------------------
    // IE 10+ Cond-Comments Failover
    //------------------------------

    if ('ActiveXObject' in window) {
        $('html').addClass('ie');
    }

    //------------------------------
    // Some craziness that let's us listen for elements
    // to be removed (Used by RouteAnimationHelper)
    //------------------------------

    $.event.special.destroyed = {
        remove(o) {
            if (o.handler) {
                o.handler();
            }
        },
    };
});

//------------------------------
// RAF Polyfill (Older Android)
//------------------------------

// FIXME: Revisit this once we no longer have many Android 4.x or IE 9.x requests

// RequestAnimationFrame (IE9, Android 4.1, 4.2, 4.3)
/*! @author Paul Irish */
/*! @source https://gist.github.com/paulirish/1579671 */
// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating

// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel

// MIT license

(() => {
    let lastTime = 0;

    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = callback => {
            const currTime = Date.now();
            const timeToCall = Math.max(0, 16 - (currTime - lastTime));
            const id = window.setTimeout(() => {
                callback(currTime + timeToCall);
            }, timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
    }

    if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = id => {
            clearTimeout(id);
        };
    }
})();

//------------------------------
// Null auth_headers Workaround
//------------------------------

if (window.Modernizr && window.Modernizr.localstorage) {
    if (!window.localStorage.auth_headers) {
        window.localStorage.auth_headers = '{}';
    }
}

//------------------------------
// Angular Bootstrapping
//------------------------------

if (window.MANUAL_BOOTSTRAP_MODULE) {
    // Wait for deferred scripts to load before bootstrapping the angular app.
    // jQuery ready() is not very reliable with `defer` scripts
    const scriptsWithSrc = {};
    $('script[defer]').each(function () {
        const script = $(this);
        const src = script.attr('src');

        if (src) {
            /*
                We can ignore the deferred scripts for `common` chunks because the code in this file is bundled inside of a
                `common` chunk and webpack will only execute the code for an entry point once all of it's respective chunks
                have been loaded. In other words, if this code is running, we know all of the chunks for the `common` entry
                point have been loaded, which is why we can ignore deferred scripts for `common` chunks. Additionally, the
                `runtime.js` chunk contains the webpack boilerplate responsible for resolving/executing the required modules
                in each split chunk. This module resolution/execution only happens when/if the `runtime.js` chunk has been
                loaded, which, again, means that if this code is running, we know that the `runtime.js` chunk has been loaded,
                so we can ignore the deferred script for the `runtime.js` chunk here too.
                NOTE: Be sure to use a pattern that survives minification!

                On the web, some of the deferred scripts that we want to ensure are loaded before bootstrapping the app
                may not be coming from `/assets/scripts`, so we can't just check if the src starts with `/assets/scripts`,
                but we know that our assets do start with `/`, so we can use that to verify it's an asset that we care about.
                On Cordova, we know that the only deferred scripts that we care about loading before bootstrapping the app
                are coming from `assets/scripts` (see hybrid/www/index.html), so in contrast to how we verify this on the
                web, we can simply check here if the script src starts with `assets/scripts` to determine if it's a script
                we care about. NOTE: On web, the asset path has a prepended forward slash, but on Cordova, no prepended
                forward slash is present.
            */
            if (
                /~?(common|runtime|dynamic-landing-page)(~.*.js$|.js$|-\w*.js$)/.test(src) || // NOTE: Make sure this regex works for dev and prod builds
                (!window.CORDOVA && !(!src.includes('//') && !src.includes('/browser-sync'))) ||
                (window.CORDOVA && !/^assets\/scripts/.test(src))
            ) {
                return;
            }

            scriptsWithSrc[src] = true;

            // delete entries on successful load
            script.on('load', () => {
                delete scriptsWithSrc[src];

                // We need to grab only the first ng-controller element, because there might
                // be other elements nested inside that have their own controllers.  See, for example
                // signup-candidates_new
                const appElement = angular.element('[ng-controller]').first();

                // if all scripts are loaded, bootstrap!
                if ($.isEmptyObject(scriptsWithSrc)) {
                    // wait for deviceready in Cordova
                    if (window.CORDOVA) {
                        // according to the docs: "The deviceready event behaves somewhat differently from others. Any event handler registered after the deviceready event fires has its callback function called immediately."
                        document.addEventListener('deviceready', () => {
                            angular.bootstrap(appElement, [window.MANUAL_BOOTSTRAP_MODULE]);
                        });
                    } else {
                        angular.bootstrap(appElement, [window.MANUAL_BOOTSTRAP_MODULE]);
                    }
                }
            });
        }
    });
}
