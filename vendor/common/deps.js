// eslint-disable-next-line no-unused-vars
import { $, angular } from 'jqueryAndAngular';

import 'core-js/stable';

import './scripts/common_init';
import './scripts/modernizr.custom.91603';

import 'styles/main/common.scss';
