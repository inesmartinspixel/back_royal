import ClientStorage from './ClientStorage';
import cookieFun from './cookieFun';

export { cookieFun };
export default ClientStorage;
