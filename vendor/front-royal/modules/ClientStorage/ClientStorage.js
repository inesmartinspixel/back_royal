import cookieFun from './cookieFun';

// provide window.localStorage if available, otherwise fall back to
// cookies using the localStorage interface with ipCookie under the hood.

// Since localStorageShim has state we use this ensure function so that we only
// try to initialize it once
let localStorageShim;
function ensureLocalStorageShim() {
    if (!localStorageShim) {
        localStorageShim = {};
        localStorageShim.getItem = key => localStorageShim[key];

        localStorageShim.setItem = (key, value) => {
            value = String(value);
            localStorageShim[key] = value;
            return value;
        };
        localStorageShim.removeItem = key => {
            delete localStorageShim[key];
        };
        localStorageShim.clear = () => {
            localStorageShim = null;
        };
    }

    return localStorageShim;
}

// Since ipCookieWrapper does not have state we just declare it
const ipCookieWrapper = {
    getItem(key) {
        return cookieFun(key);
    },
    setItem(key, value) {
        cookieFun(key, String(value), {
            path: '/',
            expires: 9999,
            expirationUnit: 'days',
        });
        return value;
    },
    removeItem(key) {
        cookieFun.remove(key, {
            path: '/',
        });
    },
};

function clientStorage() {
    if (window.RUNNING_IN_TEST_MODE) {
        return ensureLocalStorageShim();
    }
    if (window.localStorage) {
        return window.localStorage;
    }
    return ipCookieWrapper;
}

// See https://trello.com/c/t0RsxyqY
function CustomQuotaExceededError(message) {
    this.name = 'CustomQuotaExceededError';
    this.message = message;
    this.stack = new Error().stack;
}
CustomQuotaExceededError.prototype = new Error();

const ClientStorage = {
    getItem(key) {
        try {
            return clientStorage().getItem(key);
        } catch (e) {
            throw this.errorOrCustomQuotaExceededError(e, {
                key,
                method: 'getItem',
            });
        }
    },
    setItem(key, value) {
        try {
            return clientStorage().setItem(key, value);
        } catch (e) {
            throw this.errorOrCustomQuotaExceededError(e, {
                key,
                method: 'setItem',
            });
        }
    },
    removeItem(key) {
        try {
            clientStorage().removeItem(key);
        } catch (e) {
            throw this.errorOrCustomQuotaExceededError(e, {
                key,
                method: 'removeItem',
            });
        }
    },

    // Note: This does not work with the ipCookieWrapper. It is currently only
    // used in session_tracker.js, and would only be a problem if we are falling back
    // to cookies instead of localStorage
    keys() {
        try {
            return Object.keys(clientStorage());
        } catch (e) {
            throw this.errorOrCustomQuotaExceededError(e, {
                method: 'keys',
            });
        }
    },

    // Note: This is only used for the testing shim
    clear() {
        clientStorage().clear();
    },

    errorOrCustomQuotaExceededError(e, extra) {
        if (e.message.match(/QuotaExceededError/)) {
            const customError = new CustomQuotaExceededError('Error using ClientStorage');
            customError.extra = extra;
            throw customError;
        }
        return e;
    },
};

export default ClientStorage;
