import Injector from './Injector';

angular.module('Injector', []).factory('injector', ['$injector', $injector => new Injector($injector)]);
