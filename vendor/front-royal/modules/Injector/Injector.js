const factories = [];

export default class Injector {
    static factory(name, fn) {
        factories[name] = fn;
    }

    constructor($injector) {
        this.$injector = $injector;
        this.values = {};
    }

    get(name, opts) {
        opts = opts || {};
        const optional = opts.optional || false;
        if (!this.values[name]) {
            const factory = factories[name];

            try {
                this.values[name] = factory ? factory() : this.$injector.get(name);
            } catch (err) {
                if (!optional || !err.message.match(/Unknown provider/)) {
                    throw err;
                }
            }
        }
        return this.values[name];
    }
}
