import 'AngularSpecHelper';
import 'Porter-stemmer/angularModule';

describe('stemmer factory', () => {
    let $window;
    let stemmer;

    beforeEach(() => {
        angular.mock.module('porterStemmer');

        angular.mock.inject([
            '$injector',
            $injector => {
                $window = $injector.get('$window');
                stemmer = $injector.get('stemmer');
            },
        ]);
    });

    afterEach(() => {
        delete $window.serverDeterminedLocale;
    });

    it('should scrub non-word characters when locale is English', () => {
        Object.defineProperty($window, 'serverDeterminedLocale', {
            get() {
                return 'en';
            },
            configurable: true,
        });

        const originalSearch = 'test , test . test && test !@#$%^&*()+~ test';
        const words = stemmer.stemmedWords(originalSearch);
        expect(words).toEqual(['test', 'test', 'test', 'test', 'test']);
    });

    it('should stemm when locale is English', () => {
        Object.defineProperty($window, 'serverDeterminedLocale', {
            get() {
                return 'en';
            },
            configurable: true,
        });

        const originalSearch = 'LeapING mARChing';
        const words = stemmer.stemmedWords(originalSearch);
        expect(words).toContain('leap');
        expect(words).toContain('march');
    });

    // Acceptable characters are those in extended ASCII, or the first 256 unicode characters
    it('should call toLowerCase() when using acceptable characters', () => {
        Object.defineProperty($window, 'serverDeterminedLocale', {
            get() {
                return 'es';
            },
            configurable: true,
        });

        const originalSearch = 'maÑana';
        const words = stemmer.stemmedWords(originalSearch);
        expect(words).toEqual(['mañana']);
    });

    it('should not scrub when locale is Spanish', () => {
        Object.defineProperty($window, 'serverDeterminedLocale', {
            get() {
                return 'es';
            },
            configurable: true,
        });

        const originalSearch = 'test & tést';
        const words = stemmer.stemmedWords(originalSearch);
        expect(words).toEqual(['test', '&', 'tést']);
    });

    it('should not scrub when locale is Chinese', () => {
        Object.defineProperty($window, 'serverDeterminedLocale', {
            get() {
                return 'zh';
            },
            configurable: true,
        });

        const originalSearch = '铽特牠 & 忒慝忑: 貸蟘';
        const words = stemmer.stemmedWords(originalSearch);
        expect(words).toEqual(['铽特牠', '&', '忒慝忑:', '貸蟘']);
    });
});
