import 'AngularSpecHelper';
import 'ClickKey/angularModule';

describe('Interactivity.clickKey', () => {
    let scope;
    let $document;
    let $compile;
    let $window;
    let $rootScope;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('ClickKey', 'SpecHelper');

        angular.mock.inject((_$document_, _$compile_, _$window_, $injector) => {
            $compile = _$compile_;
            $document = _$document_;
            $window = _$window_;
            $rootScope = $injector.get('$rootScope');
        });
    });

    beforeEach(() => {
        // If this expectation fails, it means that a modal is still open from a spec
        // outside of this file. Use DialogModal.removeAlerts() or DialogModal.hideAlerts()
        // inside an afterEach() callback function in the outside spec to ensure that
        // all open modals get closed.
        expect($('body').hasClass('modal-open')).toBe(false);

        // eslint-disable-next-line no-shadow
        angular.mock.inject($rootScope => {
            scope = $rootScope.$new();
        });
        scope.clickHandler = jest.fn();
        scope.$digest();
    });

    const createElement = (keyString, disabled, noCurrentUser) => {
        if (disabled === undefined) {
            disabled = false;
        }

        if (!noCurrentUser) {
            $rootScope.currentUser = { pref_keyboard_shortcuts: true };
        }
        const element = $compile(`<button ng-click="clickHandler()" click-key="${keyString}">Click Me</button>`)(scope);
        if (disabled) {
            element.attr('disabled', 'disabled');
        }
        return element;
    };

    const createAndFire = (keyString, keyCode, disabled, noCurrentUser) => {
        createElement(keyString, disabled, noCurrentUser);
        const e = $window.jQuery.Event('keydown');
        e.which = keyCode;
        $document.trigger(e);
    };

    const createAndFireWithExpect = (keyString, keyCode, noCurrentUser) => {
        createAndFire(keyString, keyCode, undefined, noCurrentUser);
        expect(scope.clickHandler).toHaveBeenCalled();
        if (noCurrentUser) {
            expect(scope.currentUser).toBeUndefined(); // sanity check
        }
    };

    it('should still respond if no current user', () => {
        createAndFireWithExpect('1', 49, true);
    });

    it('should respond to expected registered keypresses', () => {
        createAndFireWithExpect('1', 49);
        createAndFireWithExpect('ESC', 27);
        createAndFireWithExpect('ENT', 13);
        createAndFireWithExpect('SPACE', 32);
    });

    it('should not respond to non-registered keypresses', () => {
        createAndFire('1', 13);
        expect(scope.clickHandler).not.toHaveBeenCalled();
    });

    it('should not respond when disabled', () => {
        createAndFire('1', 49, true);
        expect(scope.clickHandler).not.toHaveBeenCalled();
    });

    it('should support handling numpad key values as corresponding numerics', () => {
        createAndFire('1', 97, false);
        expect(scope.clickHandler).toHaveBeenCalled();
    });

    it('should support firing non-click events if supplied alternatives', () => {
        const element = $compile(
            '<button ng-mouseup="clickHandler()" click-key="ENT" click-key-event="mouseup">Click Me</button>',
        )(scope);
        const e = $window.jQuery.Event('keydown');
        e.which = 13;
        $document.trigger(e);
        expect(scope.clickHandler).toHaveBeenCalled();
        element.remove();
    });

    it('should prevent firing if nested in a disabled component', () => {
        const element = $compile(
            '<div disabled="disabled"><button ng-mouseup="clickHandler()" click-key="ENT" click-key-event="mouseup">Click Me</button></div>',
        )(scope);
        const e = $window.jQuery.Event('keydown');
        e.which = 13;
        $document.trigger(e);
        expect(scope.clickHandler).not.toHaveBeenCalled();
        element.remove();
    });

    it('should prevent firing if event is fired on input', () => {
        const element = $compile('<button ng-click="clickHandler()" click-key="ENT" >Click Me</button>')(scope);
        const e = $window.jQuery.Event('keydown');
        e.which = 13;
        e.target = $('<input/>')[0];
        $document.trigger(e);
        expect(scope.clickHandler).not.toHaveBeenCalled();
        element.remove();
    });

    it('should prevent firing if event is fired on input', () => {
        const element = $compile('<button ng-click="clickHandler()" click-key="ENT" >Click Me</button>')(scope);
        const e = $window.jQuery.Event('keydown');
        e.which = 13;
        e.target = $('<textarea/>')[0];
        $document.trigger(e);
        expect(scope.clickHandler).not.toHaveBeenCalled();
        element.remove();
    });

    it('should prevent firing if the current user has pref_keyboard_shortcuts set to false', () => {
        const element = createElement('1');
        // re-stub the current user with pref_keyboard_shortcuts set to false
        $rootScope.currentUser = {
            pref_keyboard_shortcuts: false,
        };
        const e = $window.jQuery.Event('keydown');
        e.which = 49;
        $document.trigger(e);
        expect(scope.clickHandler).not.toHaveBeenCalled();
        element.remove();
    });
});
