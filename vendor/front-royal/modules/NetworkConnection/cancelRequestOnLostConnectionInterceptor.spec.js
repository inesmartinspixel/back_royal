import cancelRequestOnLostConnectionInterceptor from './cancelRequestOnLostConnectionInterceptor';
import NetworkConnection from './NetworkConnection';

jest.mock('./NetworkConnection', () => {
    return { offline: false };
});

describe('cancelRequestOnLostConnectionInterceptor', () => {
    let request;
    let response;
    let responseError;
    let timeoutPromise;
    let $timeout;
    let requestCanceled;
    let config;

    beforeEach(() => {
        NetworkConnection.offline = false;
        requestCanceled = false;

        $timeout = delay => {
            timeoutPromise = new Promise(resolve => {
                setTimeout(() => {
                    resolve();
                }, delay);
            });
            return timeoutPromise;
        };
        $timeout.cancel = jest.fn();

        const injector = {
            get: key =>
                ({
                    $q: fn => {
                        return new Promise(fn);
                    },
                    $timeout,
                }[key]),
        };
        const interceptor = cancelRequestOnLostConnectionInterceptor(injector);
        ({ request, response, responseError } = interceptor);
    });

    afterEach(() => {
        if (config?.stopListeningForCancelRequestOnLostConnection) {
            config?.stopListeningForCancelRequestOnLostConnection();
        }
    });

    it('should cancel the request if network connection is lost for 5+ seconds', async () => {
        makeRequestAndGoOffline();

        await advanceTimersAndRunAllClicks(4999);
        expect(requestCanceled).toBe(false);

        await advanceTimersAndRunAllClicks(1);
        expect(requestCanceled).toBe(true);
    });

    it('should not cancel the request if the network connection is re-established within 5 seconds', async () => {
        makeRequestAndGoOffline();

        await advanceTimersAndRunAllClicks(4999);
        expect(requestCanceled).toBe(false);

        NetworkConnection.offline = false;
        await advanceTimersAndRunAllClicks(1);
        expect(requestCanceled).toBe(false);
    });

    it('should stop listening for offline if the request succeeds', async () => {
        assertStopsListeningForOfflineEvent(() => {
            response({ config });
        });
    });

    it('should stop listening for offline if the request errors', async () => {
        assertStopsListeningForOfflineEvent(() => {
            responseError({ config });
        });
    });

    it('should cancel timeout if the request succeeds after the offline event during the 5 second delay', async () => {
        assertCancelsTimeout(() => {
            response({ config });
        });
    });

    it('should cancel timeout if the request errors after the offline event during the 5 second delay', async () => {
        assertCancelsTimeout(() => {
            responseError({ config });
        });
    });

    async function assertCancelsTimeout(fn) {
        config = {};
        requestAndWatchForRequestCanceled(config);

        // We mock out going offline and dispatching the offline event
        NetworkConnection.offline = true;
        const event = new Event('offline');
        window.dispatchEvent(event);

        await advanceTimersAndRunAllClicks(1000);
        fn();
        expect($timeout.cancel).toHaveBeenCalledWith(timeoutPromise);
    }

    async function assertStopsListeningForOfflineEvent(fn) {
        config = {};
        requestAndWatchForRequestCanceled(config);
        fn();

        // We mock out going offline and dispatching the offline event
        NetworkConnection.offline = true;
        const event = new Event('offline');
        window.dispatchEvent(event);

        await advanceTimersAndRunAllClicks(5000);
        expect(requestCanceled).toBe(false);
    }

    function requestAndWatchForRequestCanceled() {
        request(config);
        config.timeout.then(() => {
            requestCanceled = true;
        });
    }

    function makeRequestAndGoOffline() {
        config = {};
        requestAndWatchForRequestCanceled(config);

        // We mock out going offline and dispatching the offline event
        NetworkConnection.offline = true;
        const event = new Event('offline');
        window.dispatchEvent(event);

        return config;
    }

    async function advanceTimersAndRunAllClicks(time) {
        // timeouts (macros)
        await jest.advanceTimersByTime(time);

        // promises (micros)
        await jest.runAllTicks();
    }
});
