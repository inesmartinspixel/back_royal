// window.navigator.onLine is not reliable in cordova
function checkCordovaConnection() {
    // This stuff causes errors in js specs, since there is no
    // window.navigator.connection.  Didn't want to have to mock it
    // in different places, and we never need it to run in specs,
    // so just turning it off.
    if (window.RUNNING_IN_TEST_MODE) {
        return true;
    }

    const networkState = window.navigator.connection.type;

    const states = {};
    states[window.Connection.UNKNOWN] = 'Unknown connection';
    states[window.Connection.ETHERNET] = 'Ethernet connection';
    states[window.Connection.WIFI] = 'WiFi connection';
    states[window.Connection.CELL_2G] = 'Cell 2G connection';
    states[window.Connection.CELL_3G] = 'Cell 3G connection';
    states[window.Connection.CELL_4G] = 'Cell 4G connection';
    states[window.Connection.CELL] = 'Cell generic connection';
    states[window.Connection.NONE] = 'No network connection';

    if (states[networkState].indexOf('WiFi') !== -1 || states[networkState].indexOf('Cell') !== -1) {
        return true;
    }

    return false;
}

const NetworkConnection = {
    get online() {
        if (window.CORDOVA) {
            return checkCordovaConnection();
        }

        return window.navigator.onLine;
    },

    get offline() {
        return !this.online;
    },
};

export default NetworkConnection;
