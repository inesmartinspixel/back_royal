import NetworkConnection from './NetworkConnection';
import cancelRequestOnLostConnectionInterceptor from './cancelRequestOnLostConnectionInterceptor';

export default NetworkConnection;

export { cancelRequestOnLostConnectionInterceptor };
