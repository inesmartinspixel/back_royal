expect.extend({
    toBeBetweenInclusive(received, min, max) {
        if (this.isNot && min <= received && max >= received) {
            return {
                pass: true, // Since this is `isNot`, true means false
                message: () => `Expected ${received} not to be between ${min} and ${max}`,
            };
        }
        if (!this.isNot) {
            expect(received).toBeGreaterThanOrEqual(min);
            expect(received).toBeLessThanOrEqual(max);
        }

        // If we reach here, then the test should pass, that means it needs to be
        // `true` when used normally, and `false` when `.not` was used.
        return {
            pass: !this.isNot,
        };
    },
});
