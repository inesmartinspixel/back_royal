import './toBeBetweenInclusive';

describe('toBeBetweenInclusive', () => {
    it('should pass if between', () => {
        expect(1).toBeBetweenInclusive(0, 3);
    });

    it('should pass if at lower threshold', () => {
        expect(0).toBeBetweenInclusive(0, 3);
    });

    it('should fail if below lower threshold', () => {
        expect(() => {
            expect(-1).toBeBetweenInclusive(0, 3);
        }).toThrowError();
    });

    it('should pass if at upper threshold', () => {
        expect(3).toBeBetweenInclusive(0, 3);
    });

    it('should fail if above upper threshold', () => {
        expect(() => {
            expect(4).toBeBetweenInclusive(0, 3);
        }).toThrowError();
    });

    describe('not', () => {
        it('should fail if at lower threshold', () => {
            expect(() => {
                expect(0).not.toBeBetweenInclusive(0, 3);
            }).toThrowError();
        });

        it('should fail if at upper threshold', () => {
            expect(() => {
                expect(3).not.toBeBetweenInclusive(0, 3);
            }).toThrowError();
        });

        it('should pass if below lower threshold', () => {
            expect(-1).not.toBeBetweenInclusive(0, 3);
        });

        it('should pass if above upper threshold', () => {
            expect(4).not.toBeBetweenInclusive(0, 3);
        });

        it('should fail if between', () => {
            expect(() => {
                expect(2).not.toBeBetweenInclusive(0, 3);
            }).toThrowError();
        });
    });
});
