import './toBeBetween';

describe('toBeBetween', () => {
    it('should pass if between', () => {
        expect(1).toBeBetween(0, 3);
    });

    it('should fail if at lower threshold', () => {
        expect(() => {
            expect(0).toBeBetween(0, 3);
        }).toThrowError();
    });

    it('should fail if below lower threshold', () => {
        expect(() => {
            expect(-1).toBeBetween(0, 3);
        }).toThrowError();
    });

    it('should fail if at upper threshold', () => {
        expect(() => {
            expect(3).toBeBetween(0, 3);
        }).toThrowError();
    });

    it('should fail if above upper threshold', () => {
        expect(() => {
            expect(4).toBeBetween(0, 3);
        }).toThrowError();
    });

    describe('not', () => {
        it('should pass if at lower threshold', () => {
            expect(0).not.toBeBetween(0, 3);
        });

        it('should pass if at upper threshold', () => {
            expect(3).not.toBeBetween(0, 3);
        });

        it('should pass if below lower threshold', () => {
            expect(-1).not.toBeBetween(0, 3);
        });

        it('should pass if above upper threshold', () => {
            expect(4).not.toBeBetween(0, 3);
        });

        it('should fail if between', () => {
            expect(() => {
                expect(2).not.toBeBetween(0, 3);
            }).toThrowError();
        });
    });
});
