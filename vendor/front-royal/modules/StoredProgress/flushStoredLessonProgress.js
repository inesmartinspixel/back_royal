import { mergeIncomingChanges } from 'LessonProgress';
import { find } from 'lodash/fp';

async function saveAttrsFromServer(table, record, incomingAttrs) {
    const updatedRecord = {
        ...record,
        ...incomingAttrs,
    };

    // Merge in any modifications that came from the server.
    // If the version in indexed db is still the version that We
    // saved, mark it as synced.
    await table
        .where({
            '[user_id+locale_pack_id]': [record.user_id, record.locale_pack_id],
        })
        .modify(function modifyRecord(existingRecord) {
            this.value = {
                ...updatedRecord,
                synced_to_server: existingRecord.fr_version_id === record.fr_version_id ? 1 : 0,
            };
        });
}

async function flushProgressForLessonAndSaveResults(record, injector) {
    const $http = injector.get('$http');
    const $window = injector.get('$window');
    const frontRoyalStore = injector.get('frontRoyalStore');

    let streamProgressKeys = record.linked_stream_locale_pack_ids?.map(localePackId => [record.user_id, localePackId]);
    if (!streamProgressKeys) {
        streamProgressKeys = [];
    }

    const streamProgressRecords = await frontRoyalStore.retryAfterHandledError(db =>
        db.streamProgress.where('[user_id+locale_pack_id]').anyOf(streamProgressKeys).toArray(),
    );

    const formData = new FormData();
    formData.append('record', JSON.stringify(record));
    formData.append(
        'meta',
        JSON.stringify({
            stream_progress_records: streamProgressRecords,
        }),
    );
    formData.append('flushingFrontRoyalStore', 'true');

    const request = {
        method: record.id ? 'PUT' : 'POST',
        url: `${$window.ENDPOINT_ROOT}/api/lesson_progress.json`,
        data: formData,
        headers: {
            Accept: 'application/json',

            // We have to set Content-type to undefined here.  When we
            // do that, the content-type ends up getting set to
            // multipart form with an appropriate boundary.  Otherwise
            // it ends up being application/json.  I guess maybe
            // $http is doing that
            'Content-type': undefined,
        },
    };

    const response = await $http(request);
    const incomingAttrs = mergeIncomingChanges(record, response.data.contents.lesson_progress[0]);
    const lessonProgressPromises = [
        frontRoyalStore.retryAfterHandledError(db => saveAttrsFromServer(db.lessonProgress, record, incomingAttrs)),
    ];

    response.data.meta.stream_progress_records?.forEach(streamProgressAttrs => {
        const streamProgressRecord = find({
            locale_pack_id: streamProgressAttrs.locale_pack_id,
        })(streamProgressRecords);
        if (!streamProgressRecord) {
            throw new Error('No stream progress record found.');
        }
        lessonProgressPromises.push(
            frontRoyalStore.retryAfterHandledError(db =>
                saveAttrsFromServer(db.streamProgress, streamProgressRecord, streamProgressAttrs),
            ),
        );
    });

    return Promise.all(lessonProgressPromises);
}

export default async function flushStoredLessonProgress(injector) {
    const frontRoyalStore = injector.get('frontRoyalStore');

    const lessonProgressRecords = await frontRoyalStore.retryAfterHandledError(db =>
        db.lessonProgress.where('synced_to_server').equals(0).toArray(),
    );

    const promises = lessonProgressRecords.map(record => flushProgressForLessonAndSaveResults(record, injector));

    return Promise.all(promises);
}
