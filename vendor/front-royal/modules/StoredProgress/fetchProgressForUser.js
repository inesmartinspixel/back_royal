import { LESS_URGENT_THAN_NORMAL } from 'HttpQueue';

export default async function fetchProgressForUser(userId, db, $http, filters = {}) {
    const response = await $http({
        get: 'POST',
        url: `${window.ENDPOINT_ROOT}/api/lesson_streams_progress.json`,
        params: {
            filters: {
                user_id: userId,
                ...filters,
            },
            include_lesson_progress: true,
        },
        headers: {
            Accept: 'application/json',
        },
        httpQueueOptions: {
            priority: LESS_URGENT_THAN_NORMAL,
        },
    });

    const streamProgressRecords = response.data.contents.lesson_streams_progress || [];
    const lessonProgressRecords = response.data.meta?.lesson_progress || [];

    const promises = [];
    if (streamProgressRecords.length > 0) {
        promises.push(db.streamProgress.bulkPut(streamProgressRecords));
    }
    if (lessonProgressRecords.length > 0) {
        promises.push(db.lessonProgress.bulkPut(lessonProgressRecords));
    }

    await Promise.all(promises);

    // record the fact that we have loaded progress for this user
    db.progressFetches.put({ user_id: userId });

    return {
        streamProgressRecords,
        lessonProgressRecords,
    };
}
