import { minKey, maxKey } from 'DexieHelper';

export default function allForUser(table, userId) {
    return table.where('[user_id+locale_pack_id]').between([userId, minKey], [userId, maxKey]);
}
