import allForUser from './allForUser';

export default function clearProgressForUser(userId, db) {
    const promises = [
        allForUser(db.streamProgress, userId).delete(),
        allForUser(db.lessonProgress, userId).delete(),
        allForUser(db.bookmarkedStreams, userId).delete(),
        db.progressFetches.where({ user_id: userId }).delete(),
    ];

    return Promise.all(promises);
}
