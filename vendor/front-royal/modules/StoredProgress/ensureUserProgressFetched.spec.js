import openTestDb from 'FrontRoyalStore/openTestDb';
import { map } from 'lodash/fp';
import ensureUserProgressFetched from './ensureUserProgressFetched';

describe('ensureUserProgressFetched', () => {
    const db = openTestDb();
    const userId = 'userId';
    let $http;
    const localePackIds = map('locale_pack_id');

    beforeEach(async () => {
        $http = jest.fn();
        await db.bookmarkedStreams.put({ locale_pack_id: 'bookmarkedLocalePackId', user_id: userId });
    });

    it('should fetchProgress when it has not yet been fetched from server', async () => {
        mockHttpRequest();
        const result = await ensureUserProgressFetched(userId, db, $http);
        assertReturnValue(result);

        // check stream progress in the database
        const storedStreamProgressRecords = await db.streamProgress.toArray();
        expect(localePackIds(storedStreamProgressRecords)).toEqual(['streamLocalePackId']);

        // check lesson progress in the database
        const storedLessonProgressRecords = await db.lessonProgress.toArray();
        expect(localePackIds(storedLessonProgressRecords)).toEqual(['lessonLocalePackId']);

        // check bookmarked streamsin the database
        const storedBookmarkedStreams = await db.bookmarkedStreams.toArray();
        expect(storedBookmarkedStreams).toEqual([{ user_id: userId, locale_pack_id: 'bookmarkedLocalePackId' }]);

        // check that we recorded a progres fetch in the database
        const progressFetches = await db.progressFetches.toArray();
        expect(map('user_id')(progressFetches)).toEqual([userId]);
    });

    it('should get progress from store if progress has previously been fetched from server', async () => {
        // make one request that will load up from $http
        mockHttpRequest();
        await ensureUserProgressFetched(userId, db, $http);

        // Now that progress is cached in the local db, a subsequent request
        // should not hit $http
        $http.mockClear();
        const result = await ensureUserProgressFetched(userId, db, $http);
        expect($http).not.toHaveBeenCalled();
        assertReturnValue(result);
    });

    // this method checks that the return value matches the values
    // mocked out in mockHttpRequest
    function assertReturnValue(result) {
        expect(localePackIds(result.streamProgress)).toEqual(['streamLocalePackId']);
        expect(localePackIds(result.lessonProgress)).toEqual(['lessonLocalePackId']);
        expect(Object.keys(result.favoriteStreamsSet)).toEqual(['bookmarkedLocalePackId']);
    }

    function mockHttpRequest() {
        $http.mockReturnValue(
            Promise.resolve({
                data: {
                    contents: {
                        lesson_streams_progress: [{ user_id: userId, locale_pack_id: 'streamLocalePackId' }],
                    },
                    meta: {
                        lesson_progress: [{ user_id: userId, locale_pack_id: 'lessonLocalePackId' }],
                    },
                },
            }),
        );
    }
});
