import buildApiResponse from 'buildApiResponse';
import getVersionId from 'getVersionId';
import { has, some, identity } from 'lodash/fp';
import isPresent from 'isPresent';

async function handleRequest({ lessonProgress, config, $injector }) {
    const frontRoyalStore = $injector.get('frontRoyalStore');
    const [responseRecord, meta] = await frontRoyalStore.retryAfterHandledError(db =>
        saveLessonAndStreamProgress(db, lessonProgress, config),
    );

    frontRoyalStore.flush();

    return buildApiResponse(
        config,
        {
            lesson_progress: [responseRecord],
        },
        meta,
    );
}

async function saveLessonAndStreamProgress(db, lessonProgress, config) {
    return db.transaction('rw', db.lessonProgress, db.streamProgress, async () => {
        let streamMeta;
        let streamLocalePackId;
        let streamProgress;
        try {
            // We can do [0] because the client only ever sends up
            // one stream progress at a time.  The api suports sending
            // multiples for when the store is flushed.  See flushStoredLessonProgress
            streamProgress = config.data.meta.stream_progress_records[0];
            // eslint-disable-next-line no-empty
        } catch (err) {}

        if (streamProgress) {
            streamMeta = await saveStreamProgress(db, streamProgress);
            streamLocalePackId = streamMeta.lesson_streams_progress.locale_pack_id;
        }

        const record =
            (await db.lessonProgress
                .where('[user_id+locale_pack_id]')
                .equals([lessonProgress.user_id, lessonProgress.locale_pack_id])
                .first()) || lessonProgress;

        // The logic here matches what is on the server in LessonProgress::create_or_update
        ['frame_bookmark_id', 'frame_history', 'frame_durations', 'complete'].forEach(key => {
            record[key] = lessonProgress[key];
        });

        const now = new Date().getTime() / 1000;
        if (record) {
            mergeCompletedFrames(record, lessonProgress.completed_frames);
            mergeChallengeScores(record, lessonProgress.challenge_scores);
        }

        if (isPresent(lessonProgress.best_score) && lessonProgress.best_score >= (record.best_score || 0)) {
            record.best_score = lessonProgress.best_score;
        }

        if (!record.created_at) {
            record.created_at = now;
        }

        record.started_at = record.started_at || now;
        record.completed_at = lessonProgress.complete && !record.completed_at ? now : record.completed_at;
        record.last_progress_at = now;
        record.synced_to_server = 0;
        record.fr_version = getVersionId();

        // When we flush the lesson progress to the server, we have to bundle
        // the stream up with the lesson
        if (streamLocalePackId) {
            // In almost all cases, each lesson progress record should
            // be assocated with one stream_locale_pack_id, but conceptually
            // there could be more.
            if (!record.linked_stream_locale_pack_ids) {
                record.linked_stream_locale_pack_ids = [];
            }
            if (!record.linked_stream_locale_pack_ids.includes(streamLocalePackId)) {
                record.linked_stream_locale_pack_ids.push(streamLocalePackId);
            }
        }

        await db.lessonProgress.put(record);

        // FIXME: we should just return modifications (this is post-MVP, when we switch to
        // optimistic locking.  For now
        // we should be able to avoid changing the logic in lesson_progress.js#_save)
        return [record, streamMeta];
    });
}

// This is the same logic as the server-side method of the same name.
function mergeCompletedFrames(existingRecord, incomingCompletedFrames) {
    if (!incomingCompletedFrames) {
        return;
    }

    if (!some(identity)(incomingCompletedFrames) && !existingRecord.for_test_lesson) {
        existingRecord.completed_frames = {};
        return;
    }

    existingRecord.completed_frames = {
        ...existingRecord.completed_frames,
        ...incomingCompletedFrames,
    };
}

// This is the same logic as the server-side method of the same name.
function mergeChallengeScores(existingRecord, incomingChallengeScores) {
    if (!incomingChallengeScores) {
        return;
    }

    if (!some(identity)(incomingChallengeScores) && !existingRecord.for_test_lesson) {
        existingRecord.challenge_scores = {};
    }

    Object.keys(incomingChallengeScores).forEach(key => {
        const score = incomingChallengeScores[key];
        const currentScore = existingRecord.challenge_scores[key];
        const hasCurrentScore = has(key)(existingRecord.challenge_scores);

        const forTestOrAssessmentLesson = existingRecord.for_test_lesson || existingRecord.for_assessment_lesson;
        const shouldUpdateScore =
            (forTestOrAssessmentLesson && !hasCurrentScore) || (!forTestOrAssessmentLesson && score !== currentScore);

        if (shouldUpdateScore) {
            existingRecord.challenge_scores[key] = score;
        }
    });
}

async function saveStreamProgress(db, streamProgress) {
    const meta = {};

    let record = await db.streamProgress
        .where('[user_id+locale_pack_id]')
        .equals([streamProgress.user_id, streamProgress.locale_pack_id])
        .first();

    if (!record) {
        record = {
            locale_pack_id: streamProgress.locale_pack_id,
            user_id: streamProgress.user_id,
        };
    }

    if (!record.created_at) {
        record.created_at = new Date().getTime() / 1000;
        meta.append_to_favorite_lesson_stream_locale_packs = [
            {
                id: record.locale_pack_id,
            },
        ];
    }

    record.synced_to_server = 0;
    record.fr_version_id = getVersionId();

    ['official_test_score', 'lesson_bookmark_id'].forEach(key => {
        record[key] = streamProgress[key];
    });

    await db.streamProgress.put(record);

    meta.lesson_streams_progress = record;

    return meta;
}

export default function lessonProgressInterceptor(config, $injector) {
    const isSave = ['post', 'put'].includes(config.method.toLowerCase());
    if (!isSave) {
        return null;
    }

    // Don't intercept when we're trying to send results to the server
    if (config.data.get && config.data.get('flushingFrontRoyalStore') === 'true') {
        return null;
    }

    const isLessonProgressCall = config.url.match('api/lesson_progress.json');
    if (isLessonProgressCall) {
        return () =>
            handleRequest({
                lessonProgress: config.data.record,
                config,
                $injector,
            });
    }

    return null;
}
