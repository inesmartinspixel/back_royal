import openTestDb from 'FrontRoyalStore/openTestDb';
import lessonProgressInterceptor from './lessonProgressInterceptor';

import 'JestMatchers/toBeBetweenInclusive';

describe('lessonProgressInterceptor', () => {
    const db = openTestDb();
    const $injector = {
        get: key =>
            ({
                frontRoyalStore: { retryAfterHandledError: fn => fn(db), flush: jest.fn() },
            }[key]),
    };
    const userId = 'user_id';
    const lessonLocalePackId = 'lesson_locale_pack_id';
    let start;

    describe('lessonProgress update', () => {
        it('should create a new record', async () => {
            const lessonProgress = {
                frame_bookmark_id: 'frame_bookmark_id',
                frame_history: ['1', '2'],
                frame_durations: {
                    '1': 0.42,
                    '2': 0.42,
                },
            };
            const [savedLessonProgress, response] = await interceptAndGetLessonProgress(lessonProgress);
            expect(savedLessonProgress.frame_bookmark_id).toEqual(lessonProgress.frame_bookmark_id);
            expect(savedLessonProgress.frame_history).toEqual(lessonProgress.frame_history);
            expect(savedLessonProgress.frame_durations).toEqual(lessonProgress.frame_durations);

            const now = new Date().getTime() / 1000;
            expect(savedLessonProgress.started_at).toBeBetweenInclusive(start, now);
            expect(savedLessonProgress.created_at).toBeBetweenInclusive(start, now);
            expect(savedLessonProgress.completed_at).toBeUndefined();
            expect(savedLessonProgress.last_progress_at).toBeBetweenInclusive(start, now);
            expect(savedLessonProgress.synced_to_server).toEqual(0);
            expect(savedLessonProgress.fr_version).not.toBeUndefined();

            expect(response.data.contents.lesson_progress[0]).toEqual(savedLessonProgress);
        });

        it('should update an existing record', async () => {
            const createdAt = 1;
            const lessonProgress = {
                locale_pack_id: lessonLocalePackId,
                user_id: userId,
                frame_bookmark_id: 'frame_bookmark_id',
                frame_history: ['1', '2'],
                frame_durations: {
                    '1': 0.42,
                    '2': 0.42,
                },
                started_at: createdAt,
                created_at: createdAt,
                last_progress_at: createdAt,
                synced_to_server: 1,
                fr_version: 'oldVersion',
            };
            await db.lessonProgress.put(lessonProgress);
            lessonProgress.frame_bookmark_id = 'another_frame';
            lessonProgress.frame_history.push('3');
            lessonProgress.frame_durations['3'] = 0.42;
            const [savedLessonProgress, response] = await interceptAndGetLessonProgress(lessonProgress);

            expect(savedLessonProgress.frame_bookmark_id).toEqual(lessonProgress.frame_bookmark_id);
            expect(savedLessonProgress.frame_history).toEqual(lessonProgress.frame_history);
            expect(savedLessonProgress.frame_durations).toEqual(lessonProgress.frame_durations);

            const now = new Date().getTime() / 1000;
            expect(savedLessonProgress.started_at).toEqual(createdAt);
            expect(savedLessonProgress.created_at).toEqual(createdAt);
            expect(savedLessonProgress.completed_at).toBeUndefined();
            expect(savedLessonProgress.last_progress_at).toBeBetweenInclusive(start, now);
            expect(savedLessonProgress.synced_to_server).toEqual(0);
            expect(savedLessonProgress.fr_version).not.toEqual(lessonProgress.fr_version);

            expect(response.data.contents.lesson_progress[0]).toEqual(savedLessonProgress);
        });
    });

    describe('complete', () => {
        it('should set completed_at and complete if record is marked as complete', async () => {
            const lessonProgress = {
                locale_pack_id: lessonLocalePackId,
                user_id: userId,
            };
            await db.lessonProgress.put(lessonProgress);

            lessonProgress.complete = true;
            const [savedLessonProgress] = await interceptAndGetLessonProgress(lessonProgress);
            const now = new Date().getTime() / 1000;
            expect(savedLessonProgress.completed_at).toBeBetweenInclusive(start, now);
            expect(savedLessonProgress.complete).toBe(true);
        });
    });

    describe('best_score', () => {
        it('should set best_score if currently undefined', async () => {
            const lessonProgress = {
                best_score: 0.42,
            };
            const [savedLessonProgress] = await interceptAndGetLessonProgress(lessonProgress);
            expect(savedLessonProgress.best_score).toEqual(0.42);
        });

        it('should update best_score if increasing', async () => {
            const lessonProgress = {
                locale_pack_id: lessonLocalePackId,
                user_id: userId,
                best_score: 0.42,
            };
            await db.lessonProgress.put(lessonProgress);
            lessonProgress.best_score = 0.43;
            const [savedLessonProgress] = await interceptAndGetLessonProgress(lessonProgress);
            expect(savedLessonProgress.best_score).toEqual(0.43);
        });

        it('should not update best_score if not increasing', async () => {
            const lessonProgress = {
                locale_pack_id: lessonLocalePackId,
                user_id: userId,
                best_score: 0.42,
            };
            await db.lessonProgress.put(lessonProgress);
            lessonProgress.best_score = 0.41;
            const [savedLessonProgress] = await interceptAndGetLessonProgress(lessonProgress);
            expect(savedLessonProgress.best_score).toEqual(0.42);
        });
    });

    describe('mergeCompletedFrames', () => {
        it('should not reset completed_frames on a test lesson', async () => {
            const lessonProgress = {
                locale_pack_id: lessonLocalePackId,
                user_id: userId,
                for_test_lesson: true,
                completed_frames: {
                    '1': true,
                },
            };
            await db.lessonProgress.put(lessonProgress);
            lessonProgress.completed_frames = {};
            const [savedLessonProgress] = await interceptAndGetLessonProgress(lessonProgress);
            expect(savedLessonProgress.completed_frames).toEqual({
                '1': true,
            });
        });

        it('should reset completed_frames on a non-test lesson', async () => {
            const lessonProgress = {
                locale_pack_id: lessonLocalePackId,
                user_id: userId,
                for_test_lesson: false,
                completed_frames: {
                    '1': true,
                },
            };
            await db.lessonProgress.put(lessonProgress);
            lessonProgress.completed_frames = {};
            const [savedLessonProgress] = await interceptAndGetLessonProgress(lessonProgress);
            expect(savedLessonProgress.completed_frames).toEqual({});
        });

        it('should merge completed_frames', async () => {
            const lessonProgress = {
                locale_pack_id: lessonLocalePackId,
                user_id: userId,
                for_test_lesson: false,
                completed_frames: {
                    '1': true,
                    '2': true,
                },
            };
            await db.lessonProgress.put(lessonProgress);
            lessonProgress.completed_frames = {
                '2': true,
                '3': true,
            };
            const [savedLessonProgress] = await interceptAndGetLessonProgress(lessonProgress);
            expect(savedLessonProgress.completed_frames).toEqual({
                '1': true,
                '2': true,
                '3': true,
            });
        });
    });

    describe('mergeChallengeScores', () => {
        it('should not reset challenge_scores on a test lesson', async () => {
            const lessonProgress = {
                locale_pack_id: lessonLocalePackId,
                user_id: userId,
                for_test_lesson: true,
                challenge_scores: {
                    '1': 1,
                },
            };
            await db.lessonProgress.put(lessonProgress);
            lessonProgress.challenge_scores = {};
            const [savedLessonProgress] = await interceptAndGetLessonProgress(lessonProgress);
            expect(savedLessonProgress.challenge_scores).toEqual({
                '1': 1,
            });
        });

        it('should reset challenge_scores on a non-test lesson', async () => {
            const lessonProgress = {
                locale_pack_id: lessonLocalePackId,
                user_id: userId,
                for_test_lesson: false,
                challenge_scores: {
                    '1': 1,
                },
            };
            await db.lessonProgress.put(lessonProgress);
            lessonProgress.challenge_scores = {};
            const [savedLessonProgress] = await interceptAndGetLessonProgress(lessonProgress);
            expect(savedLessonProgress.challenge_scores).toEqual({});
        });

        it('should add a score on a test or assessment lesson that does not yet have a score', async () => {
            const lessonProgress = {
                locale_pack_id: lessonLocalePackId,
                user_id: userId,
                for_test_lesson: true,
                challenge_scores: {
                    '1': 1,
                },
            };
            await db.lessonProgress.put(lessonProgress);
            lessonProgress.challenge_scores = {
                '1': 1,
                '2': 1,
            };
            const [savedLessonProgress] = await interceptAndGetLessonProgress(lessonProgress);
            expect(savedLessonProgress.challenge_scores).toEqual({
                '1': 1,
                '2': 1,
            });
        });

        it('should update a score on a non-test or assessment lesson', async () => {
            const lessonProgress = {
                locale_pack_id: lessonLocalePackId,
                user_id: userId,
                for_test_lesson: false,
                challenge_scores: {
                    '1': 0,
                },
            };
            await db.lessonProgress.put(lessonProgress);
            lessonProgress.challenge_scores = {
                '1': 1,
            };
            const [savedLessonProgress] = await interceptAndGetLessonProgress(lessonProgress);
            expect(savedLessonProgress.challenge_scores).toEqual({
                '1': 1,
            });
        });

        it('should not update a score on a test or assessment lesson', async () => {
            const lessonProgress = {
                locale_pack_id: lessonLocalePackId,
                user_id: userId,
                for_test_lesson: true,
                challenge_scores: {
                    '1': 0,
                },
            };
            await db.lessonProgress.put(lessonProgress);
            lessonProgress.challenge_scores = {
                '1': 1,
            };
            const [savedLessonProgress] = await interceptAndGetLessonProgress(lessonProgress);
            expect(savedLessonProgress.challenge_scores).toEqual({
                '1': 0,
            });
        });
    });

    describe('saveStreamProgress', () => {
        let streamProgress;

        beforeEach(() => {
            streamProgress = {
                user_id: userId,
                locale_pack_id: 'locale_pack_id',
            };
            start = new Date().getTime() / 1000;
        });

        it('should save a new stream progress', async () => {
            streamProgress.lesson_bookmark_id = 'some_lesson_id';
            const [savedStreamProgress, response] = await interceptAndGetSavedStreamProgress();

            // a new created_at should have been assigned
            expect(savedStreamProgress.created_at).toBeBetweenInclusive(start, new Date().getTime() / 1000);
            expect(savedStreamProgress.synced_to_server).toEqual(0);
            expect(savedStreamProgress.fr_version_id).not.toBeUndefined();
            expect(savedStreamProgress.lesson_bookmark_id).toEqual(streamProgress.lesson_bookmark_id);

            expect(response.data.meta.append_to_favorite_lesson_stream_locale_packs).toEqual([
                {
                    id: streamProgress.locale_pack_id,
                },
            ]);
            expect(response.data.meta.lesson_streams_progress).toEqual(savedStreamProgress);
        });

        it('should update an existing stream progress', async () => {
            // Before making the request, put a stream_progress record
            // into the database
            streamProgress = {
                ...streamProgress,
                created_at: 42,
                fr_version_id: '1',
                synced_to_server: true,
                lesson_bookmark_id: 'lesson_id',
            };
            await db.streamProgress.put(streamProgress);

            streamProgress.lesson_bookmark_id = 'some_other_lesson_id';
            streamProgress.official_test_score = '0.42';
            const [savedStreamProgress, response] = await interceptAndGetSavedStreamProgress();

            // a new created_at should not have been assigned
            expect(savedStreamProgress.created_at).toBe(streamProgress.created_at);
            // a new fr_version_id should have been assigned
            expect(savedStreamProgress.fr_version_id).not.toEqual(streamProgress.fr_version_id);
            // the record should have been marked as unsynced
            expect(savedStreamProgress.synced_to_server).toEqual(0);

            // some proeprties should have been saved
            expect(savedStreamProgress.lesson_bookmark_id).toEqual(streamProgress.lesson_bookmark_id);
            expect(savedStreamProgress.official_test_score).toEqual(streamProgress.official_test_score);

            // We only bookmark streams when they are first started
            expect(response.data.meta.append_to_favorite_lesson_stream_locale_packs).toBeUndefined();
            expect(response.data.meta.lesson_streams_progress).toEqual(savedStreamProgress);
        });

        it('should set linked_stream_locale_pack_ids', async () => {
            await interceptAndGetSavedStreamProgress();
            await assertLinkedStreamLocalePackIds([streamProgress.locale_pack_id]);
        });

        it('should not duplicate linked_stream_locale_pack_ids', async () => {
            // this stream locale_pack_id is already in the linked_stream_locale_pack_ids,
            // and so we should not duplicate it when saving again
            await db.lessonProgress.put({
                user_id: userId,
                locale_pack_id: lessonLocalePackId,
                linked_stream_locale_pack_ids: [streamProgress.locale_pack_id],
            });
            await interceptAndGetSavedStreamProgress();
            await assertLinkedStreamLocalePackIds([streamProgress.locale_pack_id]);
        });

        it('should save multiple linked_stream_locale_pack_ids', async () => {
            // there is another id already in linked_stream_locale_pack_ids,
            // so after saving there should be 2
            await db.lessonProgress.put({
                user_id: userId,
                locale_pack_id: lessonLocalePackId,
                linked_stream_locale_pack_ids: ['another_stream_locale_pack_id'],
            });
            await interceptAndGetSavedStreamProgress();
            await assertLinkedStreamLocalePackIds(['another_stream_locale_pack_id', streamProgress.locale_pack_id]);
        });

        async function assertLinkedStreamLocalePackIds(expectedIds) {
            const record = await db.lessonProgress
                .where('[user_id+locale_pack_id]')
                .equals([userId, lessonLocalePackId])
                .first();

            expect(record.linked_stream_locale_pack_ids).toEqual(expectedIds);
        }

        async function interceptAndGetSavedStreamProgress() {
            const response = await interceptAndHandle({}, [streamProgress]);

            const savedStreamProgress = await db.streamProgress
                .where('[user_id+locale_pack_id]')
                .equals([streamProgress.user_id, streamProgress.locale_pack_id])
                .first();

            expect(savedStreamProgress).not.toBeUndefined();

            return [savedStreamProgress, response];
        }
    });

    async function interceptAndHandle(record, streamProgressRecords) {
        start = new Date().getTime() / 1000;
        const handleRequest = await lessonProgressInterceptor(
            {
                method: 'post',
                url: 'http://path/to/api/lesson_progress.json',
                data: {
                    record: {
                        user_id: userId,
                        locale_pack_id: lessonLocalePackId,
                        ...record,
                    },
                    meta: {
                        stream_progress_records: streamProgressRecords,
                    },
                },
            },
            $injector,
        );
        return handleRequest();
    }

    async function interceptAndGetLessonProgress(lessonProgress) {
        const response = await interceptAndHandle(lessonProgress);

        const savedLessonProgress = await db.lessonProgress
            .where('[user_id+locale_pack_id]')
            .equals([userId, lessonLocalePackId])
            .first();

        expect(savedLessonProgress).not.toBeUndefined();

        return [savedLessonProgress, response];
    }
});
