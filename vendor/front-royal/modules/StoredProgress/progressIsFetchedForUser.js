export default async function progressIsFetchedForUser(userId, db) {
    // If there is a record in progressFetches for this user, that
    // means that we have fetched progress for them in the past and we have been
    // syncing that progress up and down from the server
    const progressFetch = await db.progressFetches.where({ user_id: userId }).first();
    return !!progressFetch;
}
