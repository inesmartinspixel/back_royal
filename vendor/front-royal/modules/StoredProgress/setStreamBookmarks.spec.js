import openTestDb from 'FrontRoyalStore/openTestDb';
import { map } from 'lodash/fp';
import setStreamBookmarks from './setStreamBookmarks';

describe('setStreamBookmarks', () => {
    const db = openTestDb();
    const userId = 'userId';

    it('should work', async () => {
        db.bookmarkedStreams.bulkPut([
            { user_id: userId, locale_pack_id: '1' },
            { user_id: userId, locale_pack_id: '2' },
            { user_id: 'another_user', locale_pack_id: 'x' },
        ]);

        await setStreamBookmarks(userId, ['2', '3'], db);

        const bookmarkedStreams = await db.bookmarkedStreams.toArray();
        expect(map('locale_pack_id')(bookmarkedStreams).sort()).toEqual(['2', '3', 'x']);
    });
});
