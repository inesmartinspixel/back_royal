import { getOr } from 'lodash/fp';
import allForUser from './allForUser';
import fetchProgressForUser from './fetchProgressForUser';
import progressIsFetchedForUser from './progressIsFetchedForUser';

export default async function ensureUserProgressFetched(userId, db, $http, options) {
    const allowFetch = getOr(true, 'allowFetch')(options);
    let streamProgressRecords = [];
    let lessonProgressRecords = [];
    let bookmarkedStreams = [];

    if (await progressIsFetchedForUser(userId, db)) {
        // Run these three queries in parallel
        const promises = [
            allForUser(db.streamProgress, userId)
                .toArray()
                .then(r => {
                    streamProgressRecords = r;
                }),

            allForUser(db.lessonProgress, userId)
                .toArray()
                .then(r => {
                    lessonProgressRecords = r;
                }),
        ];
        await Promise.all(promises);
    } else if (allowFetch) {
        ({ streamProgressRecords, lessonProgressRecords } = await fetchProgressForUser(userId, db, $http));
    }

    // These are initially set in the initializer for the frontRoyalStore angular
    // module and then kept updated in the current_user_interceptor from push messages.
    // There is a checkbox on https://trello.com/c/At6NM5M5 which talks about
    // moving this information to a flag on streams_progress so that we could detect
    // changes to individual bookmarks, rather than having to push down the full list.
    const result = await allForUser(db.bookmarkedStreams, userId).toArray();
    bookmarkedStreams = result;

    const favoriteStreamsSet = {};
    bookmarkedStreams.forEach(s => {
        favoriteStreamsSet[s.locale_pack_id] = true;
    });

    return {
        streamProgress: streamProgressRecords,
        lessonProgress: lessonProgressRecords,
        favoriteStreamsSet,
    };
}
