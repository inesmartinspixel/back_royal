import openTestDb from 'FrontRoyalStore/openTestDb';
import { map } from 'lodash/fp';
import fetchProgressForUser from './fetchProgressForUser';

describe('fetchProgressForUser', () => {
    const db = openTestDb();
    const userId = 'userId';
    let $http;
    const localePackIds = map('locale_pack_id');

    beforeEach(() => {
        $http = jest.fn();
    });

    it('should fetchProgress and store in db', async () => {
        mockHttpRequest();
        const result = await fetchProgressForUser(userId, db, $http);
        assertReturnValue(result);

        // check stream progress in the database
        const storedStreamProgressRecords = await db.streamProgress.toArray();
        expect(localePackIds(storedStreamProgressRecords)).toEqual(['streamLocalePackId']);

        // check lesson progress in the database
        const storedLessonProgressRecords = await db.lessonProgress.toArray();
        expect(localePackIds(storedLessonProgressRecords)).toEqual(['lessonLocalePackId']);

        // check that we recorded a progres fetch in the database
        const progressFetches = await db.progressFetches.toArray();
        expect(map('user_id')(progressFetches)).toEqual([userId]);
    });

    // this method checks that the return value matches the values
    // mocked out in mockHttpRequest
    function assertReturnValue(result) {
        expect(localePackIds(result.streamProgressRecords)).toEqual(['streamLocalePackId']);
        expect(localePackIds(result.lessonProgressRecords)).toEqual(['lessonLocalePackId']);
    }

    function mockHttpRequest() {
        $http.mockReturnValue(
            Promise.resolve({
                data: {
                    contents: {
                        lesson_streams_progress: [{ user_id: userId, locale_pack_id: 'streamLocalePackId' }],
                    },
                    meta: {
                        lesson_progress: [{ user_id: userId, locale_pack_id: 'lessonLocalePackId' }],
                    },
                },
            }),
        );
    }
});
