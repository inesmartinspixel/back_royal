import { minKey, maxKey } from 'DexieHelper';
import { difference, map } from 'lodash/fp';

export default async function (userId, newLocalePackIds, db) {
    return db.transaction('rw', db.bookmarkedStreams, async () => {
        const currentRecords = await db.bookmarkedStreams
            .where('[user_id+locale_pack_id]')
            .between([userId, minKey], [userId, maxKey])
            .toArray();
        const currentLocalePackIds = map('locale_pack_id')(currentRecords);

        const localePackIdsToRemove = difference(currentLocalePackIds, newLocalePackIds);
        const localePackIdsToAdd = difference(newLocalePackIds, currentLocalePackIds);

        const promises = [];
        if (localePackIdsToRemove.length > 0) {
            const keys = localePackIdsToRemove.map(id => [userId, id]);
            promises.push(db.bookmarkedStreams.where('[user_id+locale_pack_id]').anyOf(keys).delete());
        }

        if (localePackIdsToAdd.length > 0) {
            const records = localePackIdsToAdd.map(id => ({ user_id: userId, locale_pack_id: id }));
            promises.push(db.bookmarkedStreams.bulkPut(records));
        }

        await Promise.all(promises);
    });
}
