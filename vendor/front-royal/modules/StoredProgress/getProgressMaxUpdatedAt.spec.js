import openTestDb from 'FrontRoyalStore/openTestDb';
import { map } from 'lodash/fp';
import getProgressMaxUpdatedAt from './getProgressMaxUpdatedAt';

describe('getProgressMaxUpdatedAt', () => {
    const db = openTestDb();
    const userId = 'userId';

    it('should be undefined when progress has not yet been fetched', async () => {
        const result = await getProgressMaxUpdatedAt(userId, db);
        expect(result).toBe(undefined);
    });
    it('should be undefined when progress has been fetched but there is none', async () => {
        await insertProgressFetch();
        const result = await getProgressMaxUpdatedAt(userId, db);
        expect(result).toBe(0);
    });

    it('should get value from lessonProgress', async () => {
        await insertProgressFetch();
        await Promise.all([putProgress(db.lessonProgress, [0, 1]), putProgress(db.streamProgress, [0])]);
        const result = await getProgressMaxUpdatedAt(userId, db);
        expect(result).toBe(1);
    });

    it('should get value from streamProgress', async () => {
        await insertProgressFetch();
        await Promise.all([putProgress(db.lessonProgress, [0]), putProgress(db.streamProgress, [0, 1])]);
        const result = await getProgressMaxUpdatedAt(userId, db);
        expect(result).toBe(1);
    });

    it('should ignore records from another user', async () => {
        await insertProgressFetch();
        await Promise.all([
            putProgress(db.lessonProgress, [0, 1]),
            putProgress(db.lessonProgress, [2], 'someOtherUser'),
        ]);
        const result = await getProgressMaxUpdatedAt(userId, db);
        expect(result).toBe(1);
    });

    function insertProgressFetch() {
        return db.progressFetches.put({ user_id: userId });
    }

    function putProgress(table, updatedAts, userIdOverride) {
        let i = 0;
        const records = map(updatedAt => {
            i += 1;
            return {
                updated_at: updatedAt,
                locale_pack_id: i,
                user_id: userIdOverride || userId,
            };
        })(updatedAts);

        return table.bulkPut(records);
    }
});
