import openTestDb from 'FrontRoyalStore/openTestDb';
import getVersionId from 'getVersionId';
import { map } from 'lodash/fp';
import LessonProgress from 'LessonProgress';
import flushStoredLessonProgress from './flushStoredLessonProgress';

import 'JestMatchers/toBeBetweenInclusive';

jest.mock('LessonProgress', () => ({}));

describe('flushStoredLessonProgress', () => {
    const db = openTestDb();
    let $injector;
    let $http;
    let start;
    let runWhileRequestInFlight;

    beforeEach(async () => {
        runWhileRequestInFlight = () => {};
        $http = jest.fn().mockImplementation(async request => {
            await runWhileRequestInFlight();
            const lessonProgressRecordSentToServer = JSON.parse(request.data.get('record')).lesson_progress;
            const streamProgressRecordSentToServer = JSON.parse(request.data.get('meta')).stream_progress_records;
            return {
                data: {
                    contents: {
                        lesson_progress: [
                            {
                                ...lessonProgressRecordSentToServer,
                                updated_at: new Date().getTime() / 1000,
                            },
                        ],
                    },
                    meta: {
                        stream_progress_records: streamProgressRecordSentToServer.map(streamProgressRecord => ({
                            ...streamProgressRecord,
                            updated_at: new Date().getTime() / 1000,
                        })),
                    },
                },
            };
        });

        LessonProgress.mergeIncomingChanges = jest.fn().mockImplementation((record, incomingAttrs) => incomingAttrs);

        $injector = {
            get: key =>
                ({
                    $http,
                    $window: {
                        ENDPOINT_ROOT: 'http://endpoint/root',
                    },
                    frontRoyalStore: { retryAfterHandledError: fn => fn(db) },
                }[key]),
        };
    });

    it('should make a request for each lesson progress', async () => {
        const records = await putLessonProgressRecords(
            {
                synced_to_server: 0,
            },
            {
                synced_to_server: 0,
            },
            {
                synced_to_server: 1,
            },
        );
        await flushStoredLessonProgress($injector);

        expect($http.mock.calls.length).toEqual(2);
        assertCall(0, records[0]);
        assertCall(1, records[1]);
    });

    it('should save lesson progress changes that come down from server', async () => {
        const lessonProgressRecords = await putLessonProgressRecords({
            updated_at: 1,
        });
        const lessonProgressRecord = lessonProgressRecords[0];
        start = new Date().getTime() / 1000;
        await flushStoredLessonProgress($injector);
        const savedLessonProgressRecord = await db.lessonProgress
            .where({
                '[user_id+locale_pack_id]': [lessonProgressRecord.user_id, lessonProgressRecord.locale_pack_id],
            })
            .first();
        expect(savedLessonProgressRecord.updated_at).toBeBetweenInclusive(start, new Date().getTime() / 1000);

        // mergeIncomingChanges should have been called with
        // the record that came back from the server
        expect(LessonProgress.mergeIncomingChanges).toHaveBeenCalled();
        expect(LessonProgress.mergeIncomingChanges.mock.calls[0][0].locale_pack_id).toEqual(
            lessonProgressRecord.locale_pack_id,
        );
    });

    it('should set synced_to_server on lesson_progress if version has not changed', async () => {
        const lessonProgressRecords = await putLessonProgressRecords({});
        const lessonProgressRecord = lessonProgressRecords[0];
        await flushStoredLessonProgress($injector);
        const savedLessonProgressRecord = await db.lessonProgress
            .where({
                '[user_id+locale_pack_id]': [lessonProgressRecord.user_id, lessonProgressRecord.locale_pack_id],
            })
            .first();
        expect(savedLessonProgressRecord.synced_to_server).toEqual(1);
    });

    it('should not set synced_to_server on lesson_progress if version has changed', async () => {
        const lessonProgressRecords = await putLessonProgressRecords({});
        const lessonProgressRecord = lessonProgressRecords[0];
        const query = db.lessonProgress.where({
            '[user_id+locale_pack_id]': [lessonProgressRecord.user_id, lessonProgressRecord.locale_pack_id],
        });

        // mock somebody updating the record while the request is in flight
        runWhileRequestInFlight = async () => {
            await query.modify({
                fr_version_id: getVersionId(),
            });
        };

        await flushStoredLessonProgress($injector);
        const savedLessonProgressRecord = await query.first();
        expect(savedLessonProgressRecord.synced_to_server).toEqual(0);
    });

    describe('streamProgress', () => {
        it('should include any stream progress records referenced in linked_stream_locale_pack_ids', async () => {
            const [streamProgressRecords, lessonProgressRecord] = await putStreamProgressRecords({}, {});
            await flushStoredLessonProgress($injector);

            expect($http.mock.calls.length).toEqual(1);
            assertCall(0, lessonProgressRecord, streamProgressRecords);
        });

        it('should save stream progress changes that come down from server', async () => {
            const streamProgressRecord = (
                await putStreamProgressRecords({
                    updated_at: 1,
                })
            )[0][0];
            start = new Date().getTime() / 1000;
            await flushStoredLessonProgress($injector);
            const savedStreamProgressRecord = await db.streamProgress
                .where({
                    '[user_id+locale_pack_id]': [streamProgressRecord.user_id, streamProgressRecord.locale_pack_id],
                })
                .first();
            expect(savedStreamProgressRecord.updated_at).toBeBetweenInclusive(start, new Date().getTime() / 1000);
        });

        it('should set synced_to_server on stream_progress if version has not changed', async () => {
            const streamProgressRecord = (await putStreamProgressRecords({}))[0][0];
            await flushStoredLessonProgress($injector);
            const savedStreamProgressRecord = await db.streamProgress
                .where({
                    '[user_id+locale_pack_id]': [streamProgressRecord.user_id, streamProgressRecord.locale_pack_id],
                })
                .first();
            expect(savedStreamProgressRecord.synced_to_server).toEqual(1);
        });

        it('should not set synced_to_server on stream_progress if version has changed', async () => {
            const streamProgressRecord = (await putStreamProgressRecords({}))[0][0];
            const query = db.streamProgress.where({
                '[user_id+locale_pack_id]': [streamProgressRecord.user_id, streamProgressRecord.locale_pack_id],
            });

            // mock somebody updating the record while the request is in flight
            runWhileRequestInFlight = async () => {
                await query.modify({
                    fr_version_id: getVersionId(),
                });
            };

            await flushStoredLessonProgress($injector);
            const savedLessonProgressRecord = await query.first();
            expect(savedLessonProgressRecord.synced_to_server).toEqual(0);
        });

        // This happens on the show lesson endpoint (which is not used publicly as of
        // 2019/12/01 but is useful for testing `/lesson/:lesson_title/show/:lesson_id`)
        it('should not blow up if there is no stream', async () => {
            $http.mockImplementation(async request => {
                await runWhileRequestInFlight();
                const lessonProgressRecordSentToServer = JSON.parse(request.data.get('record')).lesson_progress;
                return {
                    data: {
                        contents: {
                            lesson_progress: [
                                {
                                    ...lessonProgressRecordSentToServer,
                                    updated_at: new Date().getTime() / 1000,
                                },
                            ],
                        },

                        // No stream_progress_records
                        meta: {},
                    },
                };
            });
            await putLessonProgressRecords({ linked_stream_locale_pack_ids: null });
            await flushStoredLessonProgress($injector);
        });
    });

    function assertCall(i, expectedLessonProgressRecords, expectedStreamProgressRecords) {
        const arg = $http.mock.calls[i][0];

        expect(arg.headers).toEqual({
            Accept: 'application/json',
            'Content-type': undefined,
        });
        expect(arg.method).toEqual('POST');
        expect(arg.url).toEqual('http://endpoint/root/api/lesson_progress.json');

        expect(arg.data.get('flushingFrontRoyalStore')).toEqual('true');

        const mapById = map('id');

        const lessonProgressRecordSentToServer = JSON.parse(arg.data.get('record'));
        expect(mapById(lessonProgressRecordSentToServer)).toEqual(mapById(expectedLessonProgressRecords));

        if (expectedStreamProgressRecords) {
            const streamProgressRecordSentToServer = JSON.parse(arg.data.get('meta')).stream_progress_records;
            expect(mapById(streamProgressRecordSentToServer)).toEqual(mapById(expectedStreamProgressRecords));
        }

        return lessonProgressRecordSentToServer;
    }

    async function putLessonProgressRecords(...args) {
        const lessonProgressRecords = args.map((lessonProgressRecord, i) => ({
            synced_to_server: 0,
            linked_stream_locale_pack_ids: [],
            user_id: '1',
            locale_pack_id: `locale_pack_id_${i}`,
            fr_version_id: getVersionId(),
            ...lessonProgressRecord,
        }));
        await db.lessonProgress.bulkPut(lessonProgressRecords);
        return lessonProgressRecords;
    }

    async function putStreamProgressRecords(...args) {
        const streamProgressRecords = args.map((streamProgressRecord, i) => ({
            synced_to_server: 0,
            user_id: '1',
            locale_pack_id: `locale_pack_id_${i}`,
            fr_version_id: getVersionId(),
            ...streamProgressRecord,
        }));
        await db.streamProgress.bulkPut(streamProgressRecords);

        // put a lessonProgressRecord that is linked to the stream progress
        // records
        const lessonProgressRecords = await putLessonProgressRecords({
            linked_stream_locale_pack_ids: map('locale_pack_id')(streamProgressRecords),
        });
        return [streamProgressRecords, lessonProgressRecords[0]];
    }
});
