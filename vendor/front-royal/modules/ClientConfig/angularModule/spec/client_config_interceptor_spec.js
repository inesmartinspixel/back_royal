import 'AngularSpecHelper';
import 'ClientConfig/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import clientConfigInterceptorLocales from 'ClientConfig/locales/client_config/client_config_interceptor-en.json';
import clientConfigLocales from 'ClientConfig/locales/client_config/client_config-en.json';

setSpecLocales(clientConfigInterceptorLocales);
setSpecLocales(clientConfigLocales);

describe('ClientConfigInterceptor', () => {
    let SpecHelper;
    let ClientConfigInterceptor;
    let clientConfig;
    let DialogModal;
    let $window;
    let $timeout;
    let FormatsText;
    let ClientStorage;

    beforeEach(() => {
        // SpecHelper has to be first so
        // that localStorage will be cleaned up before the
        // Interceptor is initialized in each test
        angular.mock.module('FrontRoyal.ClientConfig', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                ClientConfigInterceptor = $injector.get('ClientConfigInterceptor');
                clientConfig = $injector.get('ClientConfig').current;
                clientConfig.identifier = 'client_identifier';
                clientConfig.versionNumber = 42;
                DialogModal = $injector.get('DialogModal');
                FormatsText = $injector.get('FormatsText');
                ClientStorage = $injector.get('ClientStorage');
                jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
                $window = $injector.get('$window');
                $timeout = $injector.get('$timeout');
                SpecHelper.mockCapabilities();
                SpecHelper.stubConfig();
            },
        ]);
    });

    afterEach(() => {
        $window.CORDOVA = false;
    });

    describe('request', () => {
        it('should ignore urls without /api', () => {
            const config = {
                url: 'http://path',
            };
            ClientConfigInterceptor.request(config);
            expect(config.url).toEqual('http://path');
        });

        it('should add client info to requests', () => {
            const config = {
                url: 'http://api/path',
                headers: {},
            };
            ClientConfigInterceptor.request(config);
            expect(config.headers['fr-client-version']).toEqual(42);
            expect(config.headers['fr-client']).toEqual('client_identifier');
        });
    });

    describe('response', () => {
        it('should ignore urls without /api', () => {
            const response = {
                config: {
                    url: 'http://path',
                },
                data: {},
            };
            const metaSpy = jest.fn();
            Object.defineProperty(response.data, 'meta', {
                get: metaSpy,
            });
            expect(ClientConfigInterceptor.response(response)).toBe(response);

            // since the url does not match, we should never
            // have tried to access the meta object
            expect(metaSpy).not.toHaveBeenCalled();
        });

        it('should not error if no metadata', () => {
            const response = {
                config: {
                    url: 'http://api/',
                },
                data: {},
            };
            expect(ClientConfigInterceptor.response(response)).toBe(response);
        });

        it('should set the upgrade url', () => {
            // FIXME: (HACK) for now, we hard-code the appStoreName to be the iOS app store so that
            // the web client can link to it. We'll fix this in the future.
            // expect(clientConfig.appStoreName).toBeUndefined();
            const response = {
                config: {
                    url: 'http://api/',
                },
                data: {
                    meta: {
                        client_app_store_name: 'awesome-app',
                    },
                },
            };
            expect(ClientConfigInterceptor.response(response)).toBe(response);
            expect(clientConfig.appStoreName).toEqual('awesome-app');
        });

        describe('with disallowed version', () => {
            let response;

            beforeEach(() => {
                response = {
                    config: {
                        url: 'http://api/',
                    },
                    data: {
                        meta: {
                            min_allowed_client_version: 43,
                        },
                    },
                };
            });

            it('should show an alert with default message', () => {
                // assert that the interceptor returns a
                // promise (see comment in the code for why)
                const interceptorResponse = ClientConfigInterceptor.response(response);
                expect(interceptorResponse.then).not.toBeUndefined();
                $timeout.flush(); // propogate call to promise to the `then` callback
                expect(DialogModal.alert).toHaveBeenCalledWith({
                    content: '<upgrade-client message="message"></upgrade-client>',
                    scope: {
                        message: 'A new version of Quantic is available and an update is required.',
                    },
                    title: undefined,
                    size: 'small',
                    hideCloseButton: true,
                });
            });

            it('should show an alert with custom message', () => {
                jest.spyOn(FormatsText, 'processMarkdown').mockReturnValue('formatted text');
                response.data.meta.upgrade_message = 'custom message';
                // assert that the interceptor returns a
                // promise (see comment in the code for why)
                const interceptorResponse = ClientConfigInterceptor.response(response);
                expect(interceptorResponse.then).not.toBeUndefined();
                $timeout.flush(); // propogate call to promise to the `then` callback
                expect(DialogModal.alert).toHaveBeenCalledWith({
                    content: '<upgrade-client message="message"></upgrade-client>',
                    scope: {
                        message: 'formatted text',
                    },
                    title: undefined,
                    size: 'small',
                    hideCloseButton: true,
                });
                expect(FormatsText.processMarkdown).toHaveBeenCalledWith(response.data.meta.upgrade_message);
            });

            it('should also use native alert for initial user interaction in Cordova', () => {
                // stub out CORDOVA env
                $window.CORDOVA = true;

                // stub out native notifications plugin
                $window.navigator.notification = {
                    alert() {},
                };
                const alertSpy = jest.spyOn($window.navigator.notification, 'alert');

                // assert that the interceptor returns a
                // promise (see comment in the code for why)
                const interceptorResponse = ClientConfigInterceptor.response(response);
                expect(interceptorResponse.then).not.toBeUndefined();
                $timeout.flush(); // propogate call to promise to the `then` callback

                // ensure that the native alert is called as expected
                expect(alertSpy).toHaveBeenCalled();
                const args = alertSpy.mock.calls[0];
                expect(args[0]).toEqual('A new version of Quantic is available and an update is required.');
                expect(args[2]).toEqual('Update Required');
                expect(args[3]).toEqual('Refresh');

                // ensure that the provided callback to the alert behaves as expected
                const callBack = args[1];
                jest.spyOn(clientConfig, 'upgrade').mockImplementation(() => {});
                callBack();
                expect(clientConfig.upgrade).toHaveBeenCalled();
                expect(DialogModal.alert).toHaveBeenCalledWith({
                    content: '<upgrade-client message="message"></upgrade-client>',
                    scope: {
                        message: 'A new version of Quantic is available and an update is required.',
                    },
                    title: 'Update Required',
                    size: 'small',
                    hideCloseButton: true,
                });
            });
        });

        describe('with non-recommended version', () => {
            let response;

            beforeEach(() => {
                response = {
                    config: {
                        url: 'http://api/',
                    },
                    data: {
                        meta: {
                            min_suggested_client_version: 43,
                            min_allowed_client_version: 1,
                        },
                    },
                };
            });

            it('should show an alert with default message', () => {
                // assert that the interceptor returns a
                // promise (see comment in the code for why)
                const interceptorResponse = ClientConfigInterceptor.response(response);
                expect(interceptorResponse.then).toBeDefined();
                $timeout.flush(); // propogate call to promise to the `then` callback
                expect(interceptorResponse.$$state.value).toBe(response);
                expect(DialogModal.alert).toHaveBeenCalledWith({
                    content: '<upgrade-client message="message"></upgrade-client>',
                    scope: {
                        message: 'A new version of Quantic is available!',
                    },
                    size: 'small',
                    hideCloseButton: false,
                });
            });

            it('should show an alert with custom message', () => {
                jest.spyOn(FormatsText, 'processMarkdown').mockReturnValue('formatted text');
                response.data.meta.upgrade_message = 'custom message';
                // assert that the interceptor returns a
                // promise (see comment in the code for why)
                const interceptorResponse = ClientConfigInterceptor.response(response);
                expect(interceptorResponse.then).toBeDefined();
                $timeout.flush(); // propogate call to promise to the `then` callback
                expect(DialogModal.alert).toHaveBeenCalledWith({
                    content: '<upgrade-client message="message"></upgrade-client>',
                    scope: {
                        message: 'formatted text',
                    },
                    size: 'small',
                    hideCloseButton: false,
                });
                expect(FormatsText.processMarkdown).toHaveBeenCalledWith(response.data.meta.upgrade_message);
                expect(interceptorResponse.$$state.value).toBe(response);
            });

            it('should not show a message if it has already been shown for a particular version', () => {
                // initially, nothing should be save in local storage
                expect(ClientStorage.getItem('shownSuggestedUpgrades')).toBeUndefined();

                // the first time the message should be shown, and the fact
                // that a message has been shown for this versionNumber
                // should be saved in local storage
                let interceptorResponse = ClientConfigInterceptor.response(response);
                expect(interceptorResponse.then).toBeDefined();
                $timeout.flush(); // propogate call to promise to the `then` callback
                expect(interceptorResponse.$$state.value).toBe(response);
                expect(DialogModal.alert.mock.calls.length).toBe(1);
                expect(
                    JSON.parse(ClientStorage.getItem('shownSuggestedUpgrades'))[
                        response.data.meta.min_suggested_client_version
                    ],
                ).toBe(true);

                // the second time, since the user has seen the message,
                // we do not show it again
                expect(ClientConfigInterceptor.response(response)).toBe(response);
                expect(DialogModal.alert.mock.calls.length).toBe(1);

                // After the user has upgraded, and the new version is no
                // longer suggested, the user should see a new message
                response.data.meta.min_suggested_client_version += 1;
                interceptorResponse = ClientConfigInterceptor.response(response);
                expect(interceptorResponse.then).toBeDefined();
                $timeout.flush(); // propogate call to promise to the `then` callback
                expect(interceptorResponse.$$state.value).toBe(response);
                expect(DialogModal.alert.mock.calls.length).toBe(2);
                expect(
                    JSON.parse(ClientStorage.getItem('shownSuggestedUpgrades'))[
                        response.data.meta.min_suggested_client_version
                    ],
                ).toBe(true);
            });
        });
    });
});
