import './scripts/client_config_module';

import './scripts/client_config_interceptor';
import './scripts/upgrade_client_dir';
import './scripts/client_config';

import 'ClientConfig/locales/client_config/client_config-en.json';
import 'ClientConfig/locales/client_config/client_config_interceptor-en.json';
