import ClientStorage, { cookieFun } from 'ClientStorage';
import { generateGuid } from 'guid';

const Auid = {
    get(injector) {
        let storedAuid = injector?.get('$rootScope')?.currentUser?.id || ClientStorage.getItem('auid');

        // we've seen this somehow set to 'undefined' in ClientStorage (perhaps due to some legacy code),
        // so we're insulating from error if we've detected this situation.
        if (storedAuid === 'undefined' || storedAuid === 'null') {
            storedAuid = undefined;
            this.remove();
        }
        return storedAuid;
    },

    set(auid) {
        if (!auid) {
            this.remove();
            return;
        }
        ClientStorage.setItem('auid', auid);

        // we want to ensure that this get set in cookieFun so that it
        // gets sent in requests for webpages, not just api requests.
        cookieFun('auid', this.get(), {
            path: '/',
            expires: 9999,
            expirationUnit: 'days',
        });
    },

    ensure(injector) {
        if (!this.get(injector)) {
            const auid = generateGuid();
            this.set(auid);
        }

        return this.get();
    },

    reset() {
        this.remove();
        return this.ensure();
    },

    remove() {
        cookieFun.remove('auid');
        ClientStorage.removeItem('auid');
    },
};

export default Auid;
