import angularModule from 'Navigation/angularModule/scripts/navigation_module';
import 'FrontRoyalUiBootstrap/popover';
import template from 'Navigation/angularModule/views/app_menu.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('appMenu', [
    '$injector',
    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const $window = $injector.get('$window');
        const $location = $injector.get('$location');
        const $route = $injector.get('$route');
        const ngToast = $injector.get('ngToast');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const AppMenuMixin = $injector.get('Navigation.AppMenuMixin');
        const TranslationHelper = $injector.get('TranslationHelper');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const HiringTeamInviteModal = $injector.get('HiringTeamInviteModal');
        const offlineModeManager = $injector.get('offlineModeManager');

        return {
            restrict: 'E',
            scope: {},
            templateUrl,
            link(scope) {
                NavigationHelperMixin.onLink(scope);
                AppMenuMixin.onLink(scope);
                scope.offlineModeManager = offlineModeManager;

                scope.openInviteTeammateModal = () => {
                    HiringTeamInviteModal.open();
                };

                scope.viewModel = AppHeaderViewModel;

                // This has to be a getter/setter so it doesn't get
                // eaten up by the ng-if
                Object.defineProperty(scope, 'tourStep', {
                    get() {
                        return scope.$tourStep;
                    },
                    set(val) {
                        scope.$tourStep = val;
                    },
                });

                //-------------------------
                // Menu Display
                //-------------------------

                // Setup localization keys
                const translationHelper = new TranslationHelper('navigation.app_menu');

                // NOTE: we also have CSS that hides these on media queries for web, but we want to ensure
                // that in hybrid, we never display this content as these modules will not be loaded. this
                // may change in a future date if we decide to support editor / admin on mobile clients.
                scope.editorAndReportsAndAdminEnabled = !$window.CORDOVA;

                //-------------------------
                // Navigation / Actions
                //-------------------------
                scope.clearStreamProgress = () => {
                    if ($window.confirm(translationHelper.get('clear_progress_confirm'))) {
                        scope.currentUser.clearAllProgress().then(() => {
                            $location.url('/library');
                            $route.reload();
                            /* eslint-disable */
                            $rootScope.viewAs = $rootScope.viewAs; // re-trigger any view updates (This does not do anything, right?)
                            /* eslint-enable */
                            ngToast.create({
                                content: translationHelper.get('clear_progress_success'),
                                className: 'success',
                            });
                        });
                    }
                };
            },
        };
    },
]);
