import 'ng-toast';
import 'angular-selectize2/dist/angular-selectize';

import 'Authentication/angularModule';
import 'Careers/angularModule';
import 'DialogModal/angularModule';
import 'ClientStorage/angularModule';
import 'ErrorLogging/angularModule';
import 'EventLogger/angularModule';
import 'Feedback/angularModule';
import 'FrontRoyalUiBootstrap/angularModule';
import 'Institutions/angularModule';
import 'guid/angularModule';
import 'IsMobile/angularModule';
import 'LearnerContentCache/angularModule';
import 'OfflineMode/angularModule';
import 'OnImageLoad/angularModule';
import 'RouteAnimationHelper/angularModule';
import 'SafeApply/angularModule';
import 'ScrollHelper/angularModule';
import 'SearchEngineOptimization/angularModule';
import 'Sequence/angularModule';
import 'Translation/angularModule';
import 'Users/angularModule';

export default angular.module('FrontRoyal.Navigation', [
    'FrontRoyal.Careers',
    'LearnerContentCache',
    'scrollHelper',
    'AClassAbove',
    'ClientStorage',
    'EventLogger',
    'FrontRoyal.Feedback',
    'FrontRoyal.Institutions',
    'Translation',
    'FrontRoyal.UiBootstrap',
    'FrontRoyal.Users',
    'guid',
    'FrontRoyal.Authentication', // include this instead of ng-token-auth to make sure ng-token-auth gets configured right
    'ngToast',
    'selectize',
    'FrontRoyal.RouteAnimationHelper',
    'onImageLoad',
    'SiteMetadata',
    'DialogModal',
    'FrontRoyal.ErrorLogService',
    'isMobile',
    'safeApply',
    'Sequence',
    'OfflineMode',
    'ngRoute',
]);

angular.module('FrontRoyal.Navigation').run([
    '$injector',
    $injector => {
        const $rootScope = $injector.get('$rootScope');
        const $location = $injector.get('$location');
        const $window = $injector.get('$window');
        const RouteEventHandler = $injector.get('RouteEventHandler');
        const ClientStorage = $injector.get('ClientStorage');
        const RouteAnimationHelper = $injector.get('RouteAnimationHelper');
        const SiteMetadata = $injector.get('SiteMetadata');
        const scrollHelper = $injector.get('scrollHelper');
        const $route = $injector.get('$route');

        Object.defineProperty($rootScope, 'homePath', {
            get() {
                const user = $rootScope.currentUser;

                if (!user) {
                    return $window.CORDOVA ? '/onboarding/hybrid' : '/';
                }
                if (user.noHiringManagerAccessDueToPastDuePayment) {
                    return '/settings/billing';
                }
                if (user.hasRejectedHiringManagerAccess) {
                    return '/hiring/sorry';
                }
                if (user.hasDisabledHiringManagerAccess) {
                    return '/hiring/disabled';
                }
                if (user.hasHiringChoosePlanAccess) {
                    return '/hiring/plan';
                }
                if (user.hasSourcingUpsellAccess && !user.usingPayPerPostHiringPlan) {
                    // Note that pay-per-post users have upsell access, but we want their homePath
                    // to still be the positions page
                    return '/hiring/browse-candidates';
                }
                if (user.hasHiringPositionsAccess) {
                    // NOTE: if we ever change the home page for hiring managers to
                    // something other than /hiring/positions, we could recreate the
                    // issue described in https://trello.com/c/RPaqFNrl
                    return '/hiring/positions';
                }
                if (user.defaultsToHiringExperience) {
                    $injector.get('ErrorLogService').notify('Unknown homePath for hiring manager');
                    return '/settings';
                }

                return '/dashboard';
            },
            configurable: true,
        });

        $rootScope.goHome = () => {
            $location.path($rootScope.homePath);
        };

        // see comment in route_resolvers.js
        $rootScope.redirectHomeWhileRouting = () => {
            // If we're sending the user to the place they were going anyway,
            // we have to resolve the original promise.  Redirect won't work
            if ($route.current.$$route.originalPath === $rootScope.homePath) {
                return;
            }

            // If we're sending them to a new place, then we reject with a redirect
            // eslint-disable-next-line no-throw-literal
            throw { redirect: $rootScope.homePath };
        };

        $rootScope.pushBackButtonHistory = (currentUrl, currentDirective) => {
            // we like to keep track of this in Cordova in case of transparent app restarts, etc. (see also: ValidationResponder::forwardToTargetUrl)
            // We only store this if there is a logged in user.
            if ($window.CORDOVA && $rootScope.currentUser) {
                ClientStorage.setItem('last_visited_route', currentUrl);
            }

            $rootScope.pageHistory.push({
                url: currentUrl,
                directive: currentDirective,
            });
        };

        $rootScope.back = animate => {
            animate = angular.isDefined(animate) ? animate : true;

            // if we don't have any in-app history, but we have a recorded history entry and it
            // matches the same domain as the application, then just back-nav. otherwise allow defaults.
            if (
                !window.CORDOVA &&
                $rootScope.pageHistory.length <= 1 &&
                document.referrer &&
                document.referrer.match(/:\/\/(.[^/]+)/)[1] === window.location.host
            ) {
                const path = document.referrer.substring(document.referrer.lastIndexOf('/'));
                // if we're not logged in, or the referrer path isn't to /, use referrer
                // this is because if we're logged in and the path is to /, we want to be
                // smarter about which root to send them to
                if (!$rootScope.currentUser || path !== '/') {
                    window.location.assign(document.referrer);
                    return;
                }
            }

            // if there was no referrer, or the referrer didn't match our domain, or there was no history, etc.
            // we might need to send them "back" to a root URL. Choose which one based on whether they are logged in or not.
            const defaultRootURL = $rootScope.homePath;

            // pop the previous URL off the stack or default to proper root URL
            const prevUrl =
                $rootScope.pageHistory.length > 1 ? $rootScope.pageHistory.splice(-2)[0].url : defaultRootURL;

            if (animate) {
                RouteAnimationHelper.animatePathChange(prevUrl, 'slide-right');
            } else {
                $location.url(prevUrl);
                scrollHelper.scrollToTop();
            }

            SiteMetadata.updateHeaderMetadata({
                defaultCanonicalUrl: prevUrl,
            });
        };

        // Setup history, route change success/error, and backbutton listeners
        RouteEventHandler.setupEventListeners();
    },
]);
