import angularModule from 'Navigation/angularModule/scripts/navigation_module';
import template from 'Navigation/angularModule/views/view_as_menu.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('viewAsMenu', [
    '$injector',
    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const Institution = $injector.get('Institution');
        const Cohort = $injector.get('Cohort');

        return {
            restrict: 'E',
            scope: {},
            templateUrl,
            link(scope) {
                scope.$rootScope = $rootScope;

                if ($rootScope.viewAs) {
                    scope.viewAsType = $rootScope.viewAs.split(':')[0];
                } else {
                    scope.viewAsType = 'group';
                }

                scope.$watch('$rootScope.viewAs', viewAs => {
                    if (viewAs) {
                        scope.viewingAsType = viewAs.split(':')[0];
                        scope.viewingAsName = scope.getViewAsName(viewAs);
                    }
                });

                scope.getViewAsName = viewAs => {
                    const temp = viewAs.split(':');
                    const type = temp[0];
                    const value = temp[1];

                    if (type === 'group') {
                        return value;
                    }
                    if (type === 'institution') {
                        const institution = _.findWhere($rootScope.availableInstitutions, {
                            id: value,
                        });
                        return institution.name;
                    }
                    if (type === 'cohort') {
                        const cohort = _.findWhere($rootScope.availableCohorts, {
                            id: value,
                        });
                        return cohort.name;
                    }
                };

                scope.loadViewAsType = type => {
                    $rootScope.availableGroups = null;
                    $rootScope.availableInstitutions = null;
                    $rootScope.availableCohorts = null;

                    scope.loadingViewAsType = true;
                    if (type === 'group') {
                        // In the case of groups they are already there, so just grab them.
                        $rootScope.availableGroups = [];
                        if (!$rootScope.currentUser || !$rootScope.currentUser.available_groups) {
                            return;
                        }
                        $rootScope.currentUser.available_groups.forEach(group => {
                            $rootScope.availableGroups.push(group);
                        });
                        $rootScope.availableGroups = _.sortBy($rootScope.availableGroups, 'name');
                        scope.loadingViewAsType = false;
                    } else if (type === 'institution') {
                        Institution.index()
                            .then(response => {
                                // Currently this directive is used in a popover that will be reopened multiple times
                                // so since it is an admin directive I am being lazy and caching the available view-as data
                                // on the $rootScope.
                                $rootScope.availableInstitutions = _.sortBy(response.result, 'name');
                            })
                            .finally(() => {
                                scope.loadingViewAsType = false;
                            });
                    } else if (type === 'cohort') {
                        Cohort.index({
                            fields: ['BASIC_FIELDS'],
                        })
                            .then(response => {
                                $rootScope.availableCohorts = _.sortBy(response.result, 'name');
                            })
                            .finally(() => {
                                scope.loadingViewAsType = false;
                            });
                    }
                };
            },
        };
    },
]);
