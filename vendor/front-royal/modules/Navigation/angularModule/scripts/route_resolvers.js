import angularModule from 'Navigation/angularModule/scripts/navigation_module';
import { EMPLOYER_SIGNUP_LOCATION } from 'SignupLocations';
import { cookieFun } from 'ClientStorage';
import RPC from 'FrontRoyalFrameRpc';

// Provides support for custom route resolvers to respond to navigation flows as necessary
angularModule
    .constant('Navigation.RouteResolvers.AVAILABLE_RESOLVERS', [
        'profileConfirmationRequired',
        'redirectToOnboardingRegisterIfMiyaMiya',
        'redirectToHomeIfAuthorized',
        'redirectToHome',
        'redirectToRootDefault',
        'loadJoinAndRedirect',
        'unrestrictedAccess',
        'hasHiringManagerAccess',
        'hasHiringChoosePlanAccess',
        'hasHiringSourcingOrUpsellAccess',
        'hasHiringPositionsAccess',
        'hasHiringTrackerAccess',
        'hasRejectedHiringManagerAccess',
        'hasDisabledHiringManagerAccess',
        'hasLearnerAccess',
        'hasEditorAccess',
        'hasSuperEditorAccess',
        'hasAdminAccess',
        'hasReportsAccess',
        'hasSuperEditorOrInterviewerAccess',
        'hasCareersTabAccess',
        'hasStudentNetworkEventsAccessForEventId',
        'scormAccess',
        'demoAccess',
        'handle404',
        'redirectToDefaultCareersSection',
        'hasCordovaNoNetwork',
    ])

    .factory('Navigation.RouteResolvers', [
        '$injector',
        $injector => {
            const $rootScope = $injector.get('$rootScope');
            const $q = $injector.get('$q');
            const blockAuthenticatedAccess = $injector.get('blockAuthenticatedAccess');
            const $location = $injector.get('$location');
            const $window = $injector.get('$window');
            const isMobile = $injector.get('isMobile');
            const RouteAssetLoader = $injector.get('Navigation.RouteAssetLoader');
            const ConfigFactory = $injector.get('ConfigFactory');
            const ValidationResponder = $injector.get('ValidationResponder');
            const LearnerContentCache = $injector.get('LearnerContentCache');
            const offlineModeManager = $injector.get('offlineModeManager');

            function waitForNetworkBootstrap() {
                return $q(resolve => {
                    const cancel = $rootScope.$watch('networkBootstrapCompleted', networkBootstrapCompleted => {
                        if (networkBootstrapCompleted) {
                            resolve();
                            cancel();
                        }
                    });
                });
            }

            function redirectWithTarget(path) {
                return {
                    redirect: path,
                    search: {
                        target: $location.url(),
                    },
                };
            }

            function waitForAuthCallComplete() {
                let chain = $q.when();

                // We have to wait for the network bootstrap before
                // we can try to load config
                chain = chain.then(waitForNetworkBootstrap);

                // enter offline mode if there is no network connection
                // and redirect if necessary
                chain = chain.then(() => offlineModeManager.resolveRoute());

                // So that we can use getSync inside of most of our directives,
                // we make sure that the config is loaded before rendering the
                // main view
                chain = chain.then(ConfigFactory.getConfig.bind(ConfigFactory));
                chain = chain.then(ValidationResponder.waitForAuthCallComplete.bind(ValidationResponder));
                return chain;
            }

            function getSignInRedirect() {
                let path = '/sign-in';
                if ($window.CORDOVA && $window.CORDOVA.forcedUrlPrefix) {
                    path = `/${$window.CORDOVA.forcedUrlPrefix}/sign-in`;
                } else if ($window.CORDOVA && isMobile()) {
                    path = '/onboarding/hybrid';
                }
                return redirectWithTarget(path);
            }

            function hasAppropriateAccess(type) {
                const propertyName = `has${type.charAt(0).toUpperCase()}${type.slice(1)}Access`;

                return loadAccountAndRejectIfInvalid($injector).then(currentUser =>
                    $q((resolve, reject) => {
                        if (!currentUser || !currentUser[propertyName]) {
                            reject(getSignInRedirect());
                        } else {
                            const stopWatching = $rootScope.$watch(() => {
                                const hasAccess = currentUser[propertyName];
                                if (!hasAccess) {
                                    $rootScope.goHome();
                                }
                            });
                            $rootScope.$on('$routeChangeStart', stopWatching);
                            resolve();
                        }
                    }),
                );
            }

            function loadAccountAndRejectIfInvalid() {
                return $q((resolve, reject) => {
                    const $route = $injector.get('$route');

                    if (blockAuthenticatedAccess.block) {
                        reject(getSignInRedirect());
                        return;
                    }

                    waitForAuthCallComplete()
                        .then(() => {
                            if ($rootScope.currentUser) {
                                // preload student dashboard data so it will be there when we need it
                                // We don't need this on every page, but we need it broadly enough that
                                // it seems worth it to always preload it.  We need it for sure on the
                                // student dashboard, stream dashbard, and player (the last two are needed
                                // so that we can calculate whether prereqs are met)
                                if ($window.location && /^\/admin/.test($window.location.pathname)) {
                                    return undefined;
                                }

                                /*
                                    We have to wrap any api requests in route resolvers in
                                    rejectInOfflineMode.  If we don't, then a network error will
                                    cause the request to hang forever (see comment near rejectInOfflineMode).
                                    When we use rejectInOfflineMode, the request can throw a DisconnectedError.
                                    If that error is thrown, it will be caught in routes.js, in the resolvers
                                    implementation.

                                    ensureStudentDashboard uses rejectInOfflineMode internally,
                                    se we don't have to call it explicitly here.
                                */
                                return LearnerContentCache.ensureStudentDashboard();
                            }

                            return undefined;
                        })
                        .then(() => {
                            const currentUser = $rootScope.currentUser;

                            if (!currentUser) {
                                resolve(undefined);
                            }

                            // If the user has to confirm zir name and email, send zir to the
                            // confirm profile page
                            else if (
                                !currentUser.confirmed_profile_info &&
                                !$route.current.$$route.regexp.test('/complete-registration')
                            ) {
                                // retain target redirect support since this can happen to older users who might be reloading a path
                                reject(redirectWithTarget('/complete-registration'));
                            }

                            // If we have an override for where to send the user after logging in
                            else if (cookieFun('signup_target')) {
                                const redirectTarget = cookieFun('signup_target');
                                cookieFun.remove('signup_target', {
                                    path: '/',
                                });

                                // prevent any more redirects for this user session
                                currentUser.$$hasBeenRedirectedToApplication = true;
                                reject({
                                    redirect: redirectTarget, // "redirectWithoutTarget"
                                });
                            }

                            // On first loading the app, send the user to the application if they
                            // need to fill one out
                            else if (
                                currentUser.shouldRedirectOnIncompleteOnboarding &&
                                !currentUser.onboardingComplete &&
                                !currentUser.$$hasBeenRedirectedToApplication &&
                                !($route.current.$$route && $route.current.$$route.allowedWhenOnboardingIncomplete)
                            ) {
                                // User.onboardingComplete used to rely on User.has_seen_welcome. Now, it
                                // relies on the user's application state/program_type. Since this value
                                // will be false until a user submits a cohort application, we want to make
                                // sure we don't get stuck in a redirect loop. Since currentUser.$$hasBeenRedirectedToApplication
                                // is not stored, it will be undefined on login/reload, which is what we want.
                                currentUser.$$hasBeenRedirectedToApplication = true;
                                reject({
                                    redirect: '/settings/application', // "redirectWithoutTarget"
                                });
                            }

                            // https://trello.com/c/5ymWBCJp
                            else if (
                                !currentUser.$$hasCompletedFirstLoad &&
                                $route.current.$$route &&
                                $route.current.$$route.regexp.test($rootScope.homePath) &&
                                currentUser.lastCohortApplication &&
                                currentUser.lastCohortApplication.can_convert_to_emba
                            ) {
                                reject({
                                    redirect: '/settings/application', // "redirectWithoutTarget"
                                });
                            }

                            // On first loading the app, send the user to the cohort billing page
                            // if necessary
                            else if (
                                !currentUser.$$hasCompletedFirstLoad &&
                                $route.current.$$route &&
                                $route.current.$$route.regexp.test($rootScope.homePath) &&
                                currentUser.shouldVisitApplicationStatus
                            ) {
                                // this would practically never get hit in Cordova-land, because we restore the last viewed route on
                                // reload, but we don't currently care since we're directing users to the website for billing anyhow
                                currentUser.$$hasBeenRedirectedToBilling = true;
                                reject({
                                    redirect: '/settings/application_status', // "redirectWithoutTarget"
                                });
                            }

                            // Hiring managers who do not have access (either rejected or past
                            // due) are only allowed to go to settings or the home page (which is /sorry
                            // for rejected hiring managers or billing for past due hiring managers)
                            else if (
                                currentUser.defaultsToHiringExperience &&
                                !currentUser.hasHiringManagerAccess &&
                                $route.current.$$route &&
                                !$route.current.$$route.regexp.test('/settings') &&
                                !$route.current.$$route.regexp.test($rootScope.homePath)
                            ) {
                                reject({
                                    redirect: $rootScope.homePath, // "redirectWithoutTarget"
                                });
                            } else {
                                currentUser.$$hasCompletedFirstLoad = true;
                                resolve(currentUser);
                            }
                        })
                        .catch(err => {
                            // Make sure redirects thrown anywhere above bubble up to
                            // the route event handler (or any other error too, really)
                            reject(err);
                        });
                });
            }

            function hasAccessToCareerSection(currentUser, section) {
                return (
                    currentUser &&
                    ((section === 'welcome' && currentUser.hasCareersNetworkAccess) ||
                        (section === 'positions' &&
                            currentUser.hasCareersNetworkAccess &&
                            currentUser.hasCompleteCareerProfile) ||
                        (section === 'connections' && currentUser.hasCareersNetworkAccess) ||
                        (section === 'card' && currentUser.hasAppliedOrHasAccessToCareers) ||
                        (section === 'edit' && currentUser.hasEditCareerProfileAccess) ||
                        (section === 'apply' && !currentUser.hasAppliedOrHasAccessToCareers))
                );
            }

            return {
                profileConfirmationRequired() {
                    return $q((resolve, reject) => {
                        waitForAuthCallComplete().then(() => {
                            if ($rootScope.currentUser && !$rootScope.currentUser.confirmed_profile_info) {
                                resolve();
                            } else {
                                reject({
                                    redirect: $rootScope.homePath,
                                });
                            }
                        });
                    });
                },

                redirectToHome() {
                    const deferred = $q.defer();
                    if (blockAuthenticatedAccess.block) {
                        return undefined;
                    }
                    waitForAuthCallComplete().then(() => {
                        deferred.reject({
                            redirect: $rootScope.homePath,
                        });
                    });
                    return deferred.promise;
                },

                redirectToRootDefault() {
                    if ($window.CORDOVA) {
                        ValidationResponder.forwardToTargetUrl(); // NOTE: this handles last_visited_route for us
                    } else {
                        $window.location.href = '/';
                    }
                },

                redirectToOnboardingRegisterIfMiyaMiya() {
                    const deferred = $q.defer();
                    if ($window.CORDOVA?.miyaMiya) {
                        deferred.reject({
                            redirect: '/onboarding/hybrid/register?animate',
                        });
                    } else {
                        deferred.resolve();
                    }
                    return deferred.promise;
                },

                redirectToHomeIfAuthorized() {
                    const deferred = $q.defer();
                    if (blockAuthenticatedAccess.block) {
                        return undefined;
                    }
                    waitForAuthCallComplete().then(() => {
                        if ($rootScope.currentUser) {
                            deferred.reject({
                                redirect: $rootScope.homePath,
                            });
                        } else {
                            deferred.resolve();
                        }
                    });
                    return deferred.promise;
                },

                canAccessCareersSection(currentUser, section) {
                    return hasAccessToCareerSection(currentUser, section);
                },

                redirectToDefaultCareersSection() {
                    // We know there will be a currentUser because the routes.js file
                    // includes hasCareersTabAccess before this one
                    const currentUser = $rootScope.currentUser;

                    const $route = $injector.get('$route');

                    let section = $route.current.pathParams.section;

                    // if a section was passed in, verify the user can really access that section
                    // if they can, we'll resolve the promise; else, we'll set up a new redirect
                    if (section) {
                        if (hasAccessToCareerSection(currentUser, section)) {
                            return $q.resolve();
                        }

                        // if we got here, the user didn't have rights to access that section, so we'll clear out
                        // the section and trigger the code below to redirect them to a default section based on
                        // what they can access...
                        section = undefined;
                    }

                    // If we came to /careers with no section then decide on the subsection based
                    // on the user's network access and career profile active status
                    if (!section) {
                        // If the user has an active career profile then send them to the connections subsection
                        if (currentUser.hasActiveCareerProfile && currentUser.hasCareersNetworkAccess) {
                            section = currentUser.has_seen_careers_welcome ? 'connections' : 'welcome';
                        }

                        // If they have access to edit their profile and it isn't complete, send them to the edit career profile subsection
                        // If the user does not have an active career profile and they've submitted a profile
                        else if (
                            (currentUser.hasCareersNetworkAccess && !currentUser.hasCompleteCareerProfile) ||
                            (!currentUser.hasActiveCareerProfile && currentUser.hasAppliedOrHasAccessToCareers)
                        ) {
                            section = 'card';
                        }

                        // Otherwise the user needs to apply first to see anything
                        else {
                            section = 'apply';
                        }
                    }

                    // if we got here, we want to redirect them to another section...
                    return $q.reject({
                        redirect: `/careers/${section}`,
                    });
                },

                loadJoinAndRedirect() {
                    const $route = $injector.get('$route');
                    const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
                    const deferred = $q.defer();

                    const urlPrefix =
                        $window.CORDOVA && $window.CORDOVA.forcedUrlPrefix
                            ? $window.CORDOVA.forcedUrlPrefix
                            : $route.current.pathParams.url_prefix;

                    if (urlPrefix === 'hiring') {
                        NavigationHelperMixin.loadUrl(window.location.origin + EMPLOYER_SIGNUP_LOCATION, '_self');
                    } else if (urlPrefix) {
                        deferred.reject({
                            redirect: `/${urlPrefix}/join/account`,
                            search: $injector.get('$location').search(),
                        });
                    } else {
                        throw new Error('urlPrefix required');
                    }
                    return deferred.promise;
                },

                // user access-level validation responders

                unrestrictedAccess() {
                    return loadAccountAndRejectIfInvalid();
                },

                hasLearnerAccess() {
                    return hasAppropriateAccess('learner');
                },

                hasHiringManagerAccess() {
                    return hasAppropriateAccess('hiringManager');
                },

                hasHiringChoosePlanAccess() {
                    return hasAppropriateAccess('hiringChoosePlan');
                },

                hasHiringSourcingOrUpsellAccess() {
                    return hasAppropriateAccess('hiringSourcingOrUpsell');
                },

                hasHiringPositionsAccess() {
                    return hasAppropriateAccess('hiringPositions');
                },

                hasHiringTrackerAccess() {
                    return hasAppropriateAccess('hiringTracker');
                },

                hasRejectedHiringManagerAccess() {
                    return hasAppropriateAccess('rejectedHiringManager');
                },

                hasDisabledHiringManagerAccess() {
                    return hasAppropriateAccess('disabledHiringManager');
                },

                hasCareersTabAccess() {
                    return hasAppropriateAccess('careersTab'); // you can have access to the tab without yet having access to the career network itself
                },

                // We use the same route for both the open-access networkTab and event drilldown.
                // If an event-id was provided ensure the user has full event access.
                hasStudentNetworkEventsAccessForEventId() {
                    return hasAppropriateAccess('networkTab').then(() => {
                        if ($injector.get('$route').current.params['event-id']) {
                            return hasAppropriateAccess('studentNetworkEvents');
                        }
                        return $q.when();
                    });
                },

                hasEditorAccess() {
                    return hasAppropriateAccess('editor').then(() => RouteAssetLoader.loadEditorDependencies());
                },

                hasSuperEditorAccess() {
                    return hasAppropriateAccess('superEditor').then(() => RouteAssetLoader.loadAdminDependencies());
                },

                hasAdminAccess() {
                    return hasAppropriateAccess('admin').then(() => RouteAssetLoader.loadAdminDependencies());
                },

                hasReportsAccess() {
                    return hasAppropriateAccess('reports').then(() => RouteAssetLoader.loadReportsDependencies());
                },

                hasSuperEditorOrInterviewerAccess() {
                    return hasAppropriateAccess('superEditorOrInterviewer').then(() =>
                        RouteAssetLoader.loadAdminDependencies(),
                    );
                },

                // SCORM Authentication

                scormAccess() {
                    return $q(resolve => {
                        const origin = document.referrer;
                        const rpcFunc = RPC;
                        const rpc = rpcFunc($window, $window.parent, origin);

                        // make a request for the SCO-stamped scorm_token
                        // eslint-disable-next-line no-console
                        console.info('making RPC call to requestScormToken ...');
                        rpc.call('requestScormToken', scormToken => {
                            // eslint-disable-next-line no-console
                            console.info('scormToken received from SCO', scormToken);
                            $window.scormToken = scormToken;
                            resolve();
                        });
                    });
                },

                // Demo Route

                demoAccess() {
                    return $q((resolve, reject) => {
                        if (blockAuthenticatedAccess.block) {
                            reject(getSignInRedirect());
                            return;
                        }

                        waitForAuthCallComplete().then(() => {
                            if (!$rootScope.currentUser) {
                                resolve(undefined);
                            } else {
                                resolve($rootScope.currentUser);
                            }
                        });
                    });
                },

                // 404
                handle404() {
                    if ($window.CORDOVA) {
                        // possible the route has been removed since last visit
                        $location.url($rootScope.homePath);
                        return undefined;
                    }

                    // use a resolver that will never resolve so
                    // that nothing renders as we forward back to the rails
                    // /404 page
                    $window.location.href = '/404';
                    return $injector.get('$q')(() => {});
                },

                hasCordovaNoNetwork() {
                    return $q((resolve, reject) => {
                        if ($window.CORDOVA && !$rootScope.networkBootstrapCompleted) {
                            resolve();
                        } else {
                            reject({
                                redirect: $rootScope.homePath,
                            });
                        }
                    });
                },
            };
        },
    ]);
