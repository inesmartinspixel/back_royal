import angularModule from 'Navigation/angularModule/scripts/navigation_module';
import { setupBrandNameProperties, setupScopeProperties } from 'AppBrandMixin';
import template from 'Navigation/angularModule/views/app_header.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import backCaret from 'images/back_caret.png';
import share from 'images/share.png';
import bookmarkChecked from 'images/bookmark_checked.png';
import bookmarkUnchecked from 'images/bookmark_unchecked.png';
import smartlyHeaderLogoDarkBlue from 'vectors/smartly_header_logo_dark_blue.svg';
import wordmarkWhite from 'vectors/wordmark_white.svg';
import playerExit from 'vectors/player_exit.svg';
import playerBackSmall from 'vectors/player_back_small.svg';
import learnerLayoutLogo from 'vectors/header_logo.svg';
import learnerLayoutLogoQuantic from 'vectors/header_logo_quantic.svg';
import lessonLayout from 'vectors/header_logo_white.svg';
import lessonLayoutQuantic from 'vectors/header_logo_white_quantic.svg';
import wordmarkQuantic from 'vectors/wordmark_white_quantic.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('appHeader', [
    '$injector',

    function factory($injector) {
        const $window = $injector.get('$window');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const ShareService = $injector.get('Navigation.ShareService');
        const SiteMetadata = $injector.get('SiteMetadata');
        const offlineModeManager = $injector.get('offlineModeManager');
        const ConfigFactory = $injector.get('ConfigFactory');

        return {
            restrict: 'E',
            scope: {
                currentUser: '<',
            },
            templateUrl,
            link(scope) {
                scope.backCaret = backCaret;
                scope.share = share;
                scope.bookmarkChecked = bookmarkChecked;
                scope.bookmarkUnchecked = bookmarkUnchecked;
                scope.smartlyHeaderLogoDarkBlue = smartlyHeaderLogoDarkBlue;
                scope.wordmarkWhite = wordmarkWhite;
                scope.playerExit = playerExit;
                scope.playerBackSmall = playerBackSmall;

                function setupAppBrandProperties(config) {
                    setupBrandNameProperties($injector, scope, config);
                    setupScopeProperties(
                        $injector,
                        scope,
                        [
                            { prop: 'wordmarkSrc', quantic: wordmarkQuantic, fallback: wordmarkWhite },
                            {
                                prop: 'learnerLayoutLogo',
                                quantic: learnerLayoutLogoQuantic,
                                fallback: learnerLayoutLogo,
                            },
                            { prop: 'lessonLayoutLogo', quantic: lessonLayoutQuantic, fallback: lessonLayout },
                        ],
                        config,
                    );
                }

                ConfigFactory.getConfig().then(setupAppBrandProperties);

                scope.viewModel = AppHeaderViewModel;
                scope.offlineModeManager = offlineModeManager;
                scope.isMobileApp = $window.CORDOVA;
                scope.rowThreshold = 10;

                // See setupBrandNameProperties for where brandNameAbbr gets added to the scope
                Object.defineProperty(scope, 'logoAlt', {
                    get() {
                        return `(${scope.brandNameAbbr})`;
                    },
                });

                scope.shareStream = () => {
                    ShareService.shareMobileApp(
                        SiteMetadata.contentDefaultShareInfo(scope.currentUser, scope.viewModel.viewedStream),
                    );
                };

                scope.toggleBookmark = stream => {
                    if (scope.currentUser) {
                        scope.currentUser.toggleBookmark(stream);
                    }
                };

                scope.appHeaderClasses = () => {
                    const classes = ['app-header'];

                    classes.push(scope.viewModel.layout);

                    // try/catch so we don't need a string of null checks
                    let hasUneditableLesson = false;
                    try {
                        hasUneditableLesson = scope.viewModel.playerViewModel.lesson.editable === false;
                        // eslint-disable-next-line no-empty
                    } catch (e) {}

                    if (hasUneditableLesson) {
                        classes.push('showing-uneditable-lesson');
                    }

                    return classes;
                };

                scope.playerHeaderClasses = () => {
                    let headerBgClass = 'bg-coral';
                    if (scope.viewModel.backgroundClass === 'bg-purple') {
                        headerBgClass = 'bg-purple';
                    }
                    const classes = ['player-header', headerBgClass, scope.viewModel.headerTextColorClass];
                    return classes;
                };

                scope.toggleLanguageSwitcher = val => {
                    if (angular.isUndefined(val)) {
                        val = !scope.showLanguageSwitcher;
                    }

                    const playerViewModel = scope.viewModel.playerViewModel;
                    // Do not show language switcher in editor, as we could
                    // accidentally save the swapped text
                    const canShow =
                        scope.currentUser &&
                        scope.currentUser.canViewLanguageSwitcher &&
                        playerViewModel &&
                        playerViewModel.lesson.locale !== 'en' &&
                        !playerViewModel.editorMode &&
                        !playerViewModel.previewMode;

                    if (!canShow) {
                        scope.showLanguageSwitcher = false;
                    } else {
                        scope.showLanguageSwitcher = val;
                    }
                };

                // perform scroll cleanup on destroy
                scope.$on('$destroy', scope.cleanupScroll);
            },
        };
    },
]);
