import angularModule from 'Navigation/angularModule/scripts/navigation_module';
import casperMode from 'casperMode';

// Loads asset requirements designed for use in route changes
angularModule.factory('Navigation.RouteAssetLoader', [
    '$injector',
    $injector => {
        const $ocLazyLoad = $injector.get('$ocLazyLoad');
        const $q = $injector.get('$q');
        const $location = $injector.get('$location');
        const $window = $injector.get('$window');
        const ConfigFactory = $injector.get('ConfigFactory');
        const Locale = $injector.get('Locale');
        const $translate = $injector.get('$translate');
        let googlePlacesDependencyPromise = false;

        function lazyLoad(options) {
            const promise = $ocLazyLoad.load(options);

            // See https://trello.com/c/qxMHkpEp/229-feat-special-handing-for-failure-to-load-module-on-route-change
            // If there has been a rollout since this user's page was
            // loaded, then they might have stale urls for dependencies,
            // and some of the files may fail to load.  In that case, we
            // need to
            //
            // * cancel the default handling of a routeChangeError
            // * reload the page to get the new urls
            // * make sure that we don't get into an infinite loop if
            //     reloading did not fix the problem
            promise.catch(rejection => {
                // prevent infinte reload loops
                if ($location.search().lazyLoadReload === '1') {
                    return undefined;
                }

                // This rejection will be passed along
                // through the routeChangeError listener setup
                // in this file.  It needs to know that the
                // error has been handled.
                rejection.handled = true;

                $location.search({
                    lazyLoadReload: '1',
                });
                $window.location.reload();
                return rejection;
            });
            return promise;
        }

        return {
            loadGooglePlacesDependencies() {
                if (casperMode()) {
                    return $q.when();
                }

                return ConfigFactory.getConfig().then(config => {
                    const mapsUrl = `js!https://maps.googleapis.com/maps/api/js?libraries=places&key=${config.google_api_key}`;

                    // Seems crazy that we have to track this promise ourself.  LazyLoad should be
                    // doing this for us.  But, it didn't seem like it was working that way.  Isaac and Nate
                    // saw lazyload injecting the script tag twice on the edit student network event page.
                    if (googlePlacesDependencyPromise) {
                        return googlePlacesDependencyPromise;
                    }

                    if (!$window.google || !$window.google.maps || !$window.google.maps.places) {
                        googlePlacesDependencyPromise = lazyLoad({
                            files: [mapsUrl],
                        });
                        return googlePlacesDependencyPromise;
                    }

                    return $q.when();
                });
            },

            loadStripeCheckoutDependencies() {
                const stripeUrl = 'js!https://checkout.stripe.com/checkout.js';
                if (!$window.StripeCheckout) {
                    if (window.RUNNING_IN_TEST_MODE && !window.SILENCE_LOAD_STRIPE_CHECKOUT_ERROR) {
                        // For some reason throwing an error here just makes it fail silently
                        // eslint-disable-next-line no-console
                        console.error(
                            '. ************************* DO NOT IGNORE!!! You need to call SpecHelper.stubStripeCheckout() or you will get strange intemittent errors.',
                        );
                    }
                    return lazyLoad({
                        files: [stripeUrl],
                    });
                }

                return $q.when();
            },

            loadEditorDependencies() {
                const files = this._getFilePathsFromWebpackManifestForLazyLoading('editor', 'css')
                    .concat(['assets/mathjax/MathJax.js?config=TeX-AMS_CHTML-full&delayStartupUntil=configured'])
                    .concat(this._getFilePathsFromWebpackManifestForLazyLoading('editor', 'js'));
                const promises = [
                    lazyLoad({
                        files,
                        serie: true,
                    }),
                ];

                // if we haven't already loaded the Arabic font due to being in that locale,
                // load it up so that Arabic content will display properly.
                promises.push(this.loadArabicFontDependencies());

                return $q.all(promises);
            },

            loadAdminDependencies() {
                const files = this._getFilePathsFromWebpackManifestForLazyLoading('admin', 'css')
                    .concat(['assets/styles/fullcalendar.min.css'])
                    .concat(this._getFilePathsFromWebpackManifestForLazyLoading('admin', 'js'));
                // Admin needs some editor stuff, like for batch lesson grading
                // and ContentItemEditorModule, etc. Must be loaded first!
                return this.loadEditorDependencies()
                    .then(this.loadReportsDependencies.bind(this))
                    .then(() =>
                        lazyLoad({
                            files,
                            serie: true,
                        }),
                    );
            },

            loadReportsDependencies() {
                const files = this._getFilePathsFromWebpackManifestForLazyLoading('reports', 'css').concat(
                    this._getFilePathsFromWebpackManifestForLazyLoading('reports', 'js'),
                );
                const promises = [
                    lazyLoad({
                        files,
                        serie: true,
                    }).then(() => {
                        // Since loading reports.js added some locales to window.Smartly.locales.modules.DEFAULT_LOCALE,
                        // we need to tell $translate to refresh, which will tell the translationLoader to reload
                        // window.Smartly.locales.modules.DEFAULT_LOCALE.  (If the user is not currently
                        // in the default locale, the translationLoader will be smart enough to do nothing.)
                        const currentLang = $translate.use(false);
                        $translate.refresh(currentLang);
                    }),
                ];

                return $q.all(promises);
            },

            loadArabicFontDependencies() {
                return lazyLoad({
                    files: [window.webpackManifest['arabic-font-families.css'].replace(/^\//, '')],
                });
            },

            loadGlobalDependencies() {
                const files = this._getFilePathsFromWebpackManifestForLazyLoading('front-royal', 'css');

                // main stylesheet and translations
                const promises = [
                    lazyLoad({
                        files,
                    }),
                ];

                // if our locale is arabic, preload the fonts
                if (Locale.activeCode === 'ar') {
                    promises.push(this.loadArabicFontDependencies());
                }

                // not necessary to block on the loading of font-awesome, so
                // don't include it in the promise
                lazyLoad({
                    files: ['assets/fontawesome/v5/css/all.min.css'],
                });

                return $q.all(promises);
            },

            /*
                Finds all of the paths from the webpack manifest for the specified targetModule
                (e.g. 'admin', 'editor', etc.) for the given fileExt that don't have code shared
                with `common` or `front-royal`.

                For example, given the following webpack manifest:
                    {
                        'admin.js': '/assets/scripts/admin.js',
                        'admin~editor.js': '/assets/scripts/admin~editor.js',
                        'vendors~admin.js': '/assets/scripts/vendors~admin.js',
                        'vendors~admin~editor.css': '/assets/scripts/vendors~admin~editor.css',
                        'vendors~admin~front-royal.js': '/assets/scripts/vendors~admin~front-royal.js',
                        'reports.js': '/assets/scripts/reports.js'
                    }
                If the targetModule is 'admin' and the fileExt is 'js', the return value would contain:
                    [
                        'assets/scripts/admin.js',
                        'assets/scripts/admin~editor.js',
                        'assets/scripts/vendors~admin.js',
                        'assets/scripts/vendors~admin~editor.js'
                    ]
                NOTE: The ordering of these files isn't important because webpack's runtime code
                ensures that the code is executed in the order specified by our deps.js files.
            */
            _getFilePathsFromWebpackManifestForLazyLoading(targetModule, fileExt) {
                const targetModuleRegex = new RegExp(`${targetModule}.*.${fileExt}$`);
                return Object.values(window.webpackManifest)
                    .filter(path => {
                        if (targetModuleRegex.test(path)) {
                            if (fileExt === 'css') {
                                return !/common/.test(path);
                            }
                            // `common` and `front-royal` JS dependencies are always added into the HTML as script tags
                            // as part of the initial page render by Rails, so we avoid loading them up again here.
                            return !/common|front-royal/.test(path);
                        }
                        return false;
                    })
                    .map(path => path.replace(/^\//, '')); // remove the prepended forward slash
            },
        };
    },
]);
