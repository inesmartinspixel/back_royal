import angularModule from 'Navigation/angularModule/scripts/navigation_module';
import 'FrontRoyalUiBootstrap/popover';
import { setupBrandNameProperties } from 'AppBrandMixin';
import template from 'Navigation/angularModule/views/app_menu_user.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('appMenuUser', [
    '$injector',
    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const FeedbackModal = $injector.get('FeedbackModal');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const offlineModeManager = $injector.get('offlineModeManager');
        const ConfigFactory = $injector.get('ConfigFactory');

        return {
            restrict: 'E',
            scope: {},
            templateUrl,
            link(scope) {
                scope.viewModel = AppHeaderViewModel;
                NavigationHelperMixin.onLink(scope);
                scope.offlineModeManager = offlineModeManager;

                ConfigFactory.getConfig().then(config => {
                    setupBrandNameProperties($injector, scope, config);
                });

                // since this can be rendered before authentication
                // completes, we have to watch for currentUser to change.
                scope.$watch(
                    () => $rootScope.currentUser,
                    currentUser => {
                        scope.currentUser = currentUser;
                    },
                );

                scope.$watchGroup(['currentUser', 'viewModel.layout'], () => {
                    if (scope.currentUser) {
                        scope.popoverTemplate = 'defaultPopover.html';
                    } else if (!scope.currentUser) {
                        scope.popoverTemplate = 'noUserPopover.html';
                    }
                });

                scope.launchFeedback = () => {
                    FeedbackModal.launchFeedback();
                };

                scope.signOut = () => {
                    // don't bother injecting unless we're signing out
                    $injector.get('SignOutHelper').signOut();
                };
            },
        };
    },
]);
