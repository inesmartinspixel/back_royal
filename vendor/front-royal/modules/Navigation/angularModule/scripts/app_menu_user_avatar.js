import angularModule from 'Navigation/angularModule/scripts/navigation_module';
import avatarDefault from 'images/avatar_default.png';
import avatarDefaultQuantic from 'images/avatar_default_quantic.png';
import avatarDefaultMiyaMiya from 'images/onboarding/Message-Sender_miyamiya.png';

angularModule.directive('appMenuUserAvatar', [
    '$injector',
    function factory($injector) {
        const $window = $injector.get('$window');
        const ConfigFactory = $injector.get('ConfigFactory');

        return {
            restrict: 'E',
            scope: {
                user: '<',
            },
            link(scope, elem) {
                scope.$watchGroup(['user.avatar_url', 'user.shouldSeeQuanticBranding', 'user.isMiyaMiya'], () => {
                    let url;
                    if (scope.user) {
                        if (scope.user.avatar_url) {
                            url = scope.user.avatar_url;
                        } else if (scope.user.isMiyaMiya) {
                            url = avatarDefaultMiyaMiya;
                        } else if (scope.user.shouldSeeQuanticBranding) {
                            url = avatarDefaultQuantic;
                        } else {
                            url = avatarDefault;
                        }
                    }
                    // If we're in the Miya Miya mobile app, make sure to show
                    // the default avatar image for Miya Miya.
                    else if ($window.CORDOVA?.miyaMiya) {
                        url = avatarDefaultMiyaMiya;
                    }
                    // in cases where we don't have a user, delegate to the config
                    // to figure out what background-image should be shown
                    else if (ConfigFactory.isInitialized()) {
                        const config = ConfigFactory.getSync();
                        url = config.isQuantic() ? avatarDefaultQuantic : avatarDefault;
                    }
                    // We saw evidence that when the user relaunches the mobile app, the config hasn't yet
                    // been initialized, so if we see that this is the case, we retrieve the config and use
                    // it to determine what background-image should be shown. Note that in this case we don't
                    // bother setting the `url` until after the config has been retrieved. This is okay because
                    // we don't render the mobile app's UI until this API call has finished, so it's not like
                    // the user sees this directive without a background-image.
                    else {
                        ConfigFactory.getConfig().then(config => {
                            url = config.isQuantic() ? avatarDefaultQuantic : avatarDefault;
                            elem.css('background-image', `url(${url})`);
                        });
                    }

                    // If we have to retrieve the config with an API call to determine what background-image
                    // should be shown, the `url` variable shouldn't be set. This is okay because we don't
                    // render the mobile app's UI until this API call has finished, so it's not like the user
                    // sees this directive without a background-image.
                    if (url) {
                        elem.css('background-image', `url(${url})`);
                    }
                });
            },
        };
    },
]);
