import angularModule from 'Navigation/angularModule/scripts/navigation_module';
import { setupBrandNameProperties, setupScopeProperties } from 'AppBrandMixin';
import template from 'Navigation/angularModule/views/sessions_navigation.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import logoRed from 'vectors/logo-red.svg';
import logoRedQuantic from 'vectors/logo-red_quantic.svg';
import logoWhite from 'vectors/logo-white.svg';
import logoWhiteQuantic from 'vectors/logo-white_quantic.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('sessionsNavigation', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const $window = $injector.get('$window');
        const $timeout = $injector.get('$timeout');
        const $route = $injector.get('$route');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const safeApply = $injector.get('safeApply');
        const JoinConfig = $injector.get('JoinConfig');
        const ConfigFactory = $injector.get('ConfigFactory');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                subscription: '<',
                urlPrefix: '<',
            },
            link(scope, elem) {
                NavigationHelperMixin.onLink(scope);

                const config = ConfigFactory.getSync();
                setupBrandNameProperties($injector, scope, config);
                setupScopeProperties(
                    $injector,
                    scope,
                    [
                        { prop: 'logoRedImgSrc', quantic: logoRedQuantic, fallback: logoRed },
                        { prop: 'logoWhiteImgSrc', quantic: logoWhiteQuantic, fallback: logoWhite },
                    ],
                    config,
                );

                JoinConfig.getConfig(scope.urlPrefix).then(urlConfig => {
                    scope.showNavElements = !urlConfig.institutional_demo;
                    scope.hasSubtitleMessage = !!urlConfig.subtitle_message;
                });

                Object.defineProperty(scope, 'currentRouteDirective', {
                    get() {
                        return $route && $route.current && $route.current.$$route && $route.current.$$route.directive;
                    },
                    configurable: true, // for tests
                });

                Object.defineProperty(scope, 'extraTallBackgroundGradient', {
                    get() {
                        // If there is a subtitle above the form container, then everything
                        // is pushed down and we need to make the background gradient
                        // extra tall to accomodate it
                        return (
                            (scope.hasSubtitleMessage || scope.urlPrefix === 'hiring') &&
                            scope.currentRouteDirective !== 'forgot-password'
                        );
                    },
                });

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.navToggleClick = value => {
                    scope.navOpen = angular.isDefined(value) ? value : !scope.navOpen;

                    elem.find('.nav-elements ul li').removeClass('animate');
                    if (scope.navOpen) {
                        elem.find('.nav-elements ul li').each(function (i) {
                            const t = $(this);
                            $timeout(() => {
                                t.addClass('animate');
                            }, (i + 1) * 125);
                        });
                    }
                };

                // Start with nav closed
                scope.navOpen = false;

                // On resize of window, detect if nav is open and close it
                function onResize() {
                    if ($($window).width() > 767 && scope.navOpen) {
                        safeApply(scope, () => {
                            scope.navToggleClick(false);
                        });
                    }
                }
                $($window).on('resize', onResize);

                // remove listener when destroying directive
                scope.$on('$destroy', () => {
                    $($window).off('resize', onResize);
                });
            },
        };
    },
]);
