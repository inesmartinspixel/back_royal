import angularModule from 'Navigation/angularModule/scripts/navigation_module';
import template from 'Navigation/angularModule/views/sharing_buttons.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

cacheAngularTemplate(angularModule, 'Navigation/sharing_buttons.html', template);
