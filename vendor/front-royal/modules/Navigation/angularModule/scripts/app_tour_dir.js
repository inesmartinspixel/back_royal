import angularModule from 'Navigation/angularModule/scripts/navigation_module';
import template from 'Navigation/angularModule/views/app_tour.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

('use strict');

angularModule.directive('appTour', [
    '$injector',
    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const isMobileMixin = $injector.get('isMobileMixin');
        const $timeout = $injector.get('$timeout');

        return {
            restrict: 'E',
            scope: {
                tourStep: '=', // sends current step back to app menu
            },
            templateUrl,
            link(scope) {
                NavigationHelperMixin.onLink(scope);
                isMobileMixin.onLink(scope);

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.tourSteps = scope.xsOrSm ? ['post', 'source', 'inbox'] : ['post', 'source', 'inbox', 'invite'];
                if (!scope.currentUser.hasSourcingAccess) {
                    scope.tourSteps = _.without(scope.tourSteps, 'source');
                }
                scope.tourStepIndex = 0;

                scope.$watchGroup(['tourStepIndex'], () => {
                    // When finishing the tour, tourStepIndex gets set to null right
                    // before this directive gets destroyed
                    scope.tourStep = scope.tourStepIndex !== null && scope.tourSteps[scope.tourStepIndex];
                });

                Object.defineProperty(scope, 'nextKey', {
                    get() {
                        return scope.tourStepIndex === scope.tourSteps.length - 1 ? 'finish' : 'next';
                    },
                });

                scope.gotoStep = index => {
                    scope.tourStepIndex = index;
                };

                scope.previous = () => {
                    if (scope.tourStepIndex > 0) {
                        scope.tourStepIndex--;
                    }
                };

                scope.next = () => {
                    if (scope.tourStepIndex === scope.tourSteps.length - 1) {
                        // finish tour.  We need the timeout so that
                        // `tourStep` gets unset on the parent directive
                        // before this one disappears
                        scope.tourStep = null;
                        scope.tourStepIndex = null;

                        $timeout().then(() => {
                            scope.currentUser.has_seen_hiring_tour = true;

                            if (!scope.currentUser.ghostMode) {
                                scope.currentUser.save().catch(angular.noop);
                            }
                        });
                    } else {
                        scope.tourStepIndex++;
                    }
                };
            },
        };
    },
]);
