import angularModule from 'Navigation/angularModule/scripts/navigation_module';
import { setupBrandNameProperties, setupScopeProperties } from 'AppBrandMixin';
import template from 'Navigation/angularModule/views/app_menu_mobile.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import menuSearchWhite from 'images/menu_search_white.png';
import menuWorkWhite from 'images/menu_work_white.png';
import menuWorldWhite from 'images/menu_world_white.png';
import menuSettingsWhite from 'images/menu_settings_white.png';
import menuSpeechBubbleWhite from 'images/menu_speech_bubble_white.png';
import menuHomeWhite from 'images/miyamiya/menu_home_white.png';
import menuLogoWhite from 'images/menu_logo_white.png';
import menuLogoWhiteQuantic from 'images/menu_logo_white_quantic.png';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('appMenuMobile', [
    '$injector',
    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const AppMenuMixin = $injector.get('Navigation.AppMenuMixin');
        const $location = $injector.get('$location');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const ClientStorage = $injector.get('ClientStorage');
        const offlineModeManager = $injector.get('offlineModeManager');

        return {
            restrict: 'E',
            scope: {},
            templateUrl,
            link(scope) {
                scope.menuSearchWhite = menuSearchWhite;
                scope.menuWorkWhite = menuWorkWhite;
                scope.menuWorldWhite = menuWorldWhite;
                scope.menuSettingsWhite = menuSettingsWhite;
                scope.menuSpeechBubbleWhite = menuSpeechBubbleWhite;

                NavigationHelperMixin.onLink(scope);
                AppMenuMixin.onLink(scope);

                // These mixins expect the `currentUser` to be defined on the scope.
                // The `AppMenuMixin` actually takes care of this for us.
                setupBrandNameProperties($injector, scope);
                setupScopeProperties($injector, scope, [
                    {
                        prop: 'dashboardImgSrc',
                        quantic: menuLogoWhiteQuantic,
                        // Miya Miya users see a generic home button on mobile, whereas all other
                        // non-Quantic users should see the Smartly logo as the home button.
                        fallback: () => (scope.currentUser?.isMiyaMiya ? menuHomeWhite : menuLogoWhite),
                    },
                ]);

                scope.viewModel = AppHeaderViewModel;
                scope.offlineModeManager = offlineModeManager;

                // This has to be a getter/setter so it doesn't get
                // eaten up by the ng-if
                Object.defineProperty(scope, 'tourStep', {
                    get() {
                        return scope.$tourStep;
                    },
                    set(val) {
                        scope.$tourStep = val;
                    },
                });

                //-----------------
                // Active tab logic
                //-----------------

                // When initializing set the active tab based on location
                scope.activeTab = setActiveTab();

                // If in an inner screen see which tab was active last
                if (!scope.activeTab) {
                    scope.activeTab = ClientStorage.getItem('activeTab');
                }

                // When changing routes try to set the new active tab
                $rootScope.$on('$routeChangeSuccess', () => {
                    setActiveTab();
                });

                function setActiveTab() {
                    if ($location.path().startsWith('/dashboard')) {
                        scope.activeTab = 'dashboard';
                    } else if ($location.path().startsWith('/library')) {
                        scope.activeTab = 'library';
                    } else if ($location.path().startsWith('/careers')) {
                        scope.activeTab = 'careers';
                    } else if ($location.path().startsWith('/student-network')) {
                        scope.activeTab = 'student-network';
                    } else if ($location.path().startsWith('/hiring/thanks')) {
                        scope.activeTab = 'candidates';
                    } else if ($location.path().startsWith('/hiring/sorry')) {
                        scope.activeTab = 'candidates';
                    } else if ($location.path().startsWith('/hiring/browse-candidates')) {
                        scope.activeTab = 'browse-candidates';
                    } else if ($location.path().startsWith('/hiring/activity')) {
                        scope.activeTab = 'activity';
                    } else if ($location.path().startsWith('/hiring/positions')) {
                        scope.activeTab = 'positions';
                    } else if ($location.path().startsWith('/hiring/tracker')) {
                        scope.activeTab = 'connections';
                    } else if ($location.path().startsWith('/settings')) {
                        scope.activeTab = 'settings';
                    }

                    if (scope.activeTab) {
                        ClientStorage.setItem('activeTab', scope.activeTab);
                    }
                }
            },
        };
    },
]);
