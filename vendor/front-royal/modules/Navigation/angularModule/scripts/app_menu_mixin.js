import angularModule from 'Navigation/angularModule/scripts/navigation_module';

angularModule.factory('Navigation.AppMenuMixin', [
    '$injector',

    $injector => {
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
        const $rootScope = $injector.get('$rootScope');

        return {
            onLink(scope) {
                // since this can be rendered before authentication
                // completes, we have to watch for currentUser to change.
                scope.$watch(
                    () => $rootScope.currentUser,
                    currentUser => {
                        scope.currentUser = currentUser;
                    },
                );

                scope.$watchGroup(['viewModel.layout', 'currentUser.hasCareersNetworkAccess'], () => {
                    if (!scope.currentUser || !scope.viewModel) {
                        scope.careersNetworkViewModel = null;
                    } else if (scope.viewModel.layout === 'hiring-manager') {
                        scope.careersNetworkViewModel = CareersNetworkViewModel.get('hiringManager');
                    } else if (scope.viewModel.layout === 'learner' && scope.currentUser.hasCareersNetworkAccess) {
                        scope.careersNetworkViewModel = CareersNetworkViewModel.get('candidate');
                    } else {
                        scope.careersNetworkViewModel = null;
                    }
                });

                scope.$watch(
                    () => {
                        let count = 0;
                        if (scope.currentUser && scope.currentUser.num_recommended_positions) {
                            count += scope.currentUser.num_recommended_positions;
                        }
                        if (
                            scope.careersNetworkViewModel &&
                            scope.careersNetworkViewModel.connectionsNumNotifications
                        ) {
                            count += scope.careersNetworkViewModel.connectionsNumNotifications;
                        }
                        return count;
                    },
                    newVal => {
                        scope.numCareersNotifications = newVal;
                    },
                );
            },
        };
    },
]);
