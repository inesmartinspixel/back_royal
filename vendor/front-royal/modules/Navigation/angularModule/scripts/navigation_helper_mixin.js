import angularModule from 'Navigation/angularModule/scripts/navigation_module';
/*
    Adds scope methods for route and url based navigation
*/

angularModule.factory('Navigation.NavigationHelperMixin', [
    '$injector',

    $injector => {
        const $window = $injector.get('$window');
        const isMobile = $injector.get('isMobile');
        const $location = $injector.get('$location');
        const $route = $injector.get('$route');
        const $rootScope = $injector.get('$rootScope');
        const safeApply = $injector.get('safeApply');

        return {
            loadUrl(url, target = '_self', options) {
                if (target === '_blank' && $window.CORDOVA) {
                    target = '_system';
                }

                if (target === '_self' && $window.CORDOVA) {
                    target = '_system';
                }

                if (url.slice(0, 1) === '/') {
                    url = $window.ENDPOINT_ROOT + url;
                }

                $window.open(url, target, options);

                safeApply($rootScope);
            },

            onLink(scope) {
                const NavigationHelperMixin = this;

                scope.loadRoute = (path, preserveParams) => {
                    // eslint-disable-next-line no-shadow
                    function openRoute(path) {
                        if (preserveParams) {
                            $location.path(path);
                        } else {
                            $location.url(path);
                        }
                    }

                    openRoute(path);
                    safeApply($rootScope);
                };

                scope.loadUrl = (url, target, options) => NavigationHelperMixin.loadUrl(url, target, options);

                // sometimes we have content that might be entered as a URL, but we want to
                // invoke it as a route if it makes sense. this method accomplishes that.
                scope.loadUrlOrMaybeRoute = (url, target, options) => {
                    if (!url) {
                        return;
                    }

                    // To allow for intra-Smartly urls that work properly, we look for ones beginning with https://quantic.edu/ and convert them to paths
                    const smartlyPrefix = 'https://quantic.edu/';

                    // if the url starts with the smartly prefix (but is more than just a url to the root of the homepage)
                    if (url.includes(smartlyPrefix) && url.length > smartlyPrefix.length) {
                        const path = url.substr(smartlyPrefix.length);
                        scope.loadRoute(path);
                    } else {
                        scope.loadUrl(url, target, options);
                    }
                };

                scope.loadRouteNoProp = (url, $event) => {
                    $event.preventDefault();
                    $event.stopImmediatePropagation();
                    scope.loadRoute(url);
                };

                scope.getUrlPrefix = () => {
                    let urlPrefix;
                    if ($window.CORDOVA && $window.CORDOVA.forcedUrlPrefix) {
                        urlPrefix = $window.CORDOVA.forcedUrlPrefix;
                    } else if ($route && $route.current && $route.current.pathParams) {
                        urlPrefix = $route.current.pathParams.url_prefix;
                    }

                    urlPrefix = urlPrefix ? `${urlPrefix}/` : '';
                    return urlPrefix;
                };

                scope.openFAQ = () => {
                    scope.loadUrl('/help', '_blank');
                };

                scope.pricingURL = '/pricing';
                scope.gotoPricing = () => {
                    if ($window.RUNNING_IN_TEST_MODE) {
                        throw new Error('Do not reload page in test mode');
                    }
                    $window.location.assign($window.location.origin + scope.pricingURL);
                };

                scope.gotoForgotPassword = preserveParams => {
                    const urlPrefix = scope.getUrlPrefix();

                    if ($window.CORDOVA && isMobile()) {
                        scope.loadRoute('/onboarding/hybrid/forgot-password', preserveParams);
                    } else {
                        scope.loadRoute(`/${urlPrefix}forgot-password`, preserveParams);
                    }
                };

                scope.gotoJoin = preserveParams => {
                    const urlPrefix = scope.getUrlPrefix();
                    scope.loadRoute(`/${urlPrefix}join`, preserveParams);
                };

                scope.gotoSignIn = preserveParams => {
                    const urlPrefix = scope.getUrlPrefix();
                    scope.loadRoute(`/${urlPrefix}sign-in`, preserveParams);
                };

                scope.gotoMarketingHome = () => {
                    let urlPrefix = scope.getUrlPrefix();
                    // HACK: should ideally look up valid prefixes that work on the homepage here; just hardcode for now
                    if (urlPrefix !== '/mba') {
                        urlPrefix = '';
                    }
                    $window.location.assign(`${$window.location.origin}/${urlPrefix}`);
                };

                scope.gotoHome = () => {
                    scope.loadRoute('/home');
                };

                scope.isActive = urls => {
                    for (let i = 0; i < urls.length; i++) {
                        if ($location.path().startsWith(urls[i])) {
                            return true;
                        }
                    }

                    return false;
                };
            },
        };
    },
]);
