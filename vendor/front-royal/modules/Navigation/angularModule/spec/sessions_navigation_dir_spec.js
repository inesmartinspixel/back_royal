import 'AngularSpecHelper';
import 'Navigation/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import sessionsNavigationLocales from 'Navigation/locales/navigation/sessions_navigation-en.json';

setSpecLocales(sessionsNavigationLocales);

describe('Navigation::SessionsNavigationDirSpec', () => {
    let SpecHelper;
    let elem;
    let scope;
    let JoinConfig;
    let $timeout;
    let $q;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Navigation', 'SpecHelper');
        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            JoinConfig = $injector.get('JoinConfig');
            $timeout = $injector.get('$timeout');
            $q = $injector.get('$q');

            SpecHelper.stubConfig();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render(urlConfigOverrides) {
        let defaultJoinConfig;
        JoinConfig.getConfig('default').then(config => {
            defaultJoinConfig = config;
        });
        $timeout.flush();

        const deferred = $q.defer();
        jest.spyOn(JoinConfig, 'getConfig').mockReturnValue(deferred.promise);
        deferred.resolve(_.extend({}, defaultJoinConfig, urlConfigOverrides));

        const renderer = SpecHelper.renderer();
        renderer.render('<sessions-navigation url-prefix="\'default\'"></sessions-navigation>');
        elem = renderer.elem;
        scope = elem.isolateScope();

        expect(JoinConfig.getConfig).toHaveBeenCalledWith('default');
    }

    it('should have taller background gradient if there is a subtitle', () => {
        render({
            subtitle_message: 'foo',
        });
        expect(scope.extraTallBackgroundGradient).toBe(true);
    });

    it("should not have taller background gradient if there is a subtitle but we're on the forgot-password screen", () => {
        render({
            subtitle_message: 'foo',
        });
        Object.defineProperty(scope, 'currentRouteDirective', {
            value: 'forgot-password',
        });
        expect(scope.extraTallBackgroundGradient).toBe(false);
    });
});
