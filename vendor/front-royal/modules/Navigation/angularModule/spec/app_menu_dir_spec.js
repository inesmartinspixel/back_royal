import 'AngularSpecHelper';
import 'Careers/angularModule/spec/careers_spec_helper';
import 'Navigation/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import appMenuLocales from 'Navigation/locales/navigation/app_menu-en.json';
import appTourLocales from 'Navigation/locales/navigation/app_tour-en.json';

setSpecLocales(appMenuLocales, appTourLocales);

describe('Navigation::AppMenuDirSpec', () => {
    let SpecHelper;
    let CareersSpecHelper;
    let elem;
    let scope;
    let $injector;
    let $location;
    let $route;
    let $rootScope;
    let $timeout;
    let $window;
    let $q;
    let user;
    let CareersNetworkViewModel;
    let AppHeaderViewModel;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Navigation', 'SpecHelper');

        // mock out $window
        $window = {
            location: {
                href: '',
            },
            document: {
                getElementsByTagName() {
                    return [];
                },
                documentElement: {},
            },
            // eslint-disable-next-line lodash-fp/prefer-constant
            confirm() {
                return true;
            },
            open() {},
            navigator: {
                onLine: true,
            },
            getComputedStyle() {
                return {};
            },
            performance: window.performance,
        };
        angular.mock.module($provide => {
            $provide.value('$window', $window);
        });

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;

                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                $location = $injector.get('$location');
                $route = $injector.get('$route');
                $rootScope = $injector.get('$rootScope');
                $timeout = $injector.get('$timeout');
                $q = $injector.get('$q');
                CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
                AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');

                user = stubCurrentUser('learner');
                SpecHelper.stubConfig();
            },
        ]);
    });

    function stubCurrentUser(role) {
        return SpecHelper.stubCurrentUser(role);
    }

    describe('primary navigation', () => {
        it('should load a route when clicked on', () => {
            render();
            const link = SpecHelper.expectElementText(elem, '[name="home"] a', 'Home', 0);
            jest.spyOn($location, 'url').mockImplementation(() => {});
            link.click();
            $timeout.flush();
            expect($location.url).toHaveBeenCalledWith('/dashboard');
        });

        it('should hide admin button if non-admin user', () => {
            stubCurrentUser('learner');
            render();
            SpecHelper.expectNoElement(elem, '[name="admin"]');
        });

        it('should hide admin button in cordova environment', () => {
            stubCurrentUser('admin');
            $window.CORDOVA = true;
            render();
            SpecHelper.expectNoElement(elem, '[name="admin"]');
        });

        it('should work with no currentUser', () => {
            SpecHelper.stubNoCurrentUser();
            render();
            SpecHelper.expectElementNotHidden(elem, '[name="home"]', true);
            SpecHelper.expectElementNotHidden(elem, '[name="library"]', true);
            SpecHelper.expectNoElement(elem, '[name="careers"]', true);
            SpecHelper.expectElementHidden(elem, '[name="admin-menu"]', false);
            SpecHelper.expectElementHidden(elem, '[name="view-as-menu"]', false);
        });

        it('should highlight an active tab', () => {
            jest.spyOn($location, 'path').mockReturnValue('/library');
            render();
            SpecHelper.expectHasClass(elem, '[name="library"] a', 'active');
        });
    });

    describe('admin popover', () => {
        beforeEach(() => {
            stubCurrentUser('admin');
            render();
            const adminMenuButton = SpecHelper.expectElement(elem, '[name="admin-menu"] a');
            adminMenuButton.trigger('mouseenter');
            SpecHelper.expectElement(elem, '.popover');
        });

        it('should clear progress when clicked on', () => {
            jest.spyOn($window, 'confirm').mockReturnValue(true);
            let resolveClearAllProgress;
            jest.spyOn($rootScope.currentUser, 'clearAllProgress').mockReturnValue(
                $q(_resolve => {
                    resolveClearAllProgress = _resolve;
                }),
            );
            jest.spyOn($location, 'url').mockImplementation(() => {});
            jest.spyOn($route, 'reload').mockImplementation(() => {});

            const link = SpecHelper.expectElementText(elem, '[name="clear-progress"]', 'Clear Progress');
            link.click();

            expect($window.confirm).toHaveBeenCalledWith('Are you sure you want to clear all your lesson progress?');

            resolveClearAllProgress();
            $timeout.flush();

            expect($rootScope.currentUser.clearAllProgress).toHaveBeenCalled();
            expect($location.url).toHaveBeenCalledWith('/library');
            expect($route.reload).toHaveBeenCalled();
        });

        it('should change color of an active popover item', () => {});
    });

    describe('careers network', () => {
        it('should not tie in to view model if there is no user', () => {
            SpecHelper.stubNoCurrentUser();
            render();
            expect(scope.careersNetworkViewModel).toBe(null);
        });

        it('hiring manager should tie into appropriate view model', () => {
            Object.defineProperty(AppHeaderViewModel, 'layout', {
                value: 'hiring-manager',
            });
            jest.spyOn(CareersNetworkViewModel, 'get').mockImplementation(() => {});
            render();
            expect(CareersNetworkViewModel.get).toHaveBeenCalledWith('hiringManager');
        });

        it('candidate should tie into appropriate view model', () => {
            Object.defineProperty(AppHeaderViewModel, 'layout', {
                value: 'learner',
            });
            Object.defineProperty(user, 'hasCareersNetworkAccess', {
                value: true,
            });

            jest.spyOn(CareersNetworkViewModel, 'get').mockImplementation(() => {});
            render();
            expect(CareersNetworkViewModel.get).toHaveBeenCalledWith('candidate');
        });

        it('should show notification badge for hiring-manager', () => {
            Object.defineProperty(AppHeaderViewModel, 'layout', {
                value: 'hiring-manager',
            });
            jest.spyOn(AppHeaderViewModel, 'showHiringManagerNav', 'get').mockReturnValue(true);
            const vm = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');

            Object.defineProperty(vm, 'connectionsNumNotifications', {
                value: 3,
            });

            render();
            SpecHelper.expectElementText(elem, '.connection-number', '3');
        });

        it('should disable the invite button a non-accepted hiring-manager', () => {
            Object.defineProperty(AppHeaderViewModel, 'layout', {
                value: 'hiring-manager',
            });

            Object.defineProperty(user, 'hasAcceptedHiringManagerAccess', {
                value: false,
            });
            jest.spyOn(AppHeaderViewModel, 'showHiringManagerNav', 'get').mockReturnValue(true);

            render();
            SpecHelper.expectElementDisabled(elem, '.connections.with-button button');
        });

        it('should disable the invite button for an accepted hiring-manager', () => {
            Object.defineProperty(AppHeaderViewModel, 'layout', {
                value: 'hiring-manager',
            });

            Object.defineProperty(user, 'hasAcceptedHiringManagerAccess', {
                value: true,
            });

            jest.spyOn(AppHeaderViewModel, 'showHiringManagerNav', 'get').mockReturnValue(true);

            render();
            SpecHelper.expectElementEnabled(elem, '.connections.with-button button');
        });

        it('should show notification badge for learner', () => {
            const vm = enableCandidateCareersPill();

            Object.defineProperty(vm, 'connectionsNumNotifications', {
                value: 2,
            });

            user.num_recommended_positions = 3;
            render();
            SpecHelper.expectElementText(elem, '.connection-number', '5');
        });

        it('should load careers on click', () => {
            enableCandidateCareersPill();
            render();
            assertClickAndNavigate('careers', '/careers');
        });
    });

    describe('student-network', () => {
        beforeEach(() => {
            enableStudentNetworkPill();
        });

        it('should navigate to student network when clicked', () => {
            render();
            assertClickAndNavigate('student-network', '/student-network');
        });

        it('should show notification if the user has access and has not seen the onboarding', () => {
            jest.spyOn($rootScope.currentUser, 'hasStudentNetworkAccess', 'get').mockReturnValue(true);
            $rootScope.currentUser.has_seen_student_network = false;
            render();
            SpecHelper.expectElementText(elem, '.connection-number', '1');
        });

        it('should show notification if the user has access and requires a valid student network email', () => {
            jest.spyOn($rootScope.currentUser, 'hasStudentNetworkAccess', 'get').mockReturnValue(true);
            jest.spyOn($rootScope.currentUser, 'missingRequiredStudentNetworkEmail', 'get').mockReturnValue(true);

            render();
            SpecHelper.expectElementText(elem, '.connection-number', '1');
        });
    });

    describe('OfflineMode', () => {
        beforeEach(() => {
            SpecHelper.stubEventLogging();
        });

        it('should disable links', () => {
            enableCandidateCareersPill();
            enableStudentNetworkPill();

            const offlineModeManager = $injector.get('offlineModeManager');
            offlineModeManager.inOfflineMode = true;
            render();
            assertLinkDisabled('library');
            assertLinkDisabled('careers');
            assertLinkDisabled('student-network');
        });

        it('should hide admin stuff', () => {
            stubCurrentUser('admin');
            const offlineModeManager = $injector.get('offlineModeManager');
            offlineModeManager.inOfflineMode = true;

            render();
            SpecHelper.expectElementHidden(elem, '[name="admin-menu"]');
            SpecHelper.expectElementHidden(elem, '[name="view-as-menu"]');
        });

        function assertLinkDisabled(name) {
            SpecHelper.expectHasClass(elem, `[name="${name}"] a`, 'disabled');
            jest.spyOn(scope, 'loadRouteNoProp').mockImplementation(() => {});
            SpecHelper.click(elem, `[name="${name}"] a`);
            $timeout.flush();
            expect(scope.loadRouteNoProp).not.toHaveBeenCalled();
        }
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<app-menu></app-menu>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.currentUser = $rootScope.currentUser;
        scope.$digest();
    }

    function enableCandidateCareersPill() {
        Object.defineProperty(AppHeaderViewModel, 'layout', {
            value: 'learner',
        });
        Object.defineProperty(user, 'hasCareersNetworkAccess', {
            value: true,
        });
        Object.defineProperty(user, 'canApplyToSmartly', {
            value: true,
        });
        return CareersSpecHelper.stubCareersNetworkViewModel('candidate');
    }

    function enableStudentNetworkPill() {
        Object.defineProperty(user, 'hasNetworkTabAccess', {
            value: true,
        });
    }

    function assertClickAndNavigate(name, route) {
        jest.spyOn(scope, 'loadRouteNoProp').mockImplementation(() => {});
        SpecHelper.click(elem, `[name="${name}"] a`);
        expect(scope.loadRouteNoProp).toHaveBeenCalled();
        expect(scope.loadRouteNoProp.mock.calls[0][0]).toEqual(route);
    }
});
