import 'AngularSpecHelper';
import 'Navigation/angularModule';

describe('AppHeaderViewModel', () => {
    let SpecHelper;
    let $injector;
    let $rootScope;
    let viewModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper', 'NoUnhandledRejectionExceptions');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                $rootScope = $injector.get('$rootScope');
                viewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
            },
        ]);

        SpecHelper.stubCurrentUser();
    });

    describe('showHiringManagerNav', () => {
        it('should be true if layout is hiring-manager, user has a hiring plan, and user is not onboarding as unlimited-with-sourcing', () => {
            jest.spyOn(viewModel, 'layout', 'get').mockReturnValue('hiring-manager');
            jest.spyOn($rootScope.currentUser, 'hiringPlan', 'get').mockReturnValue('foo');
            jest.spyOn($rootScope.currentUser, 'onboardingUnlimitedWithSourcing', 'get').mockReturnValue(false);
            expect(viewModel.showHiringManagerNav).toBe(true);
        });

        it('should be true if layout is hiring-manager and using a legacy plan', () => {
            jest.spyOn(viewModel, 'layout', 'get').mockReturnValue('hiring-manager');
            jest.spyOn($rootScope.currentUser, 'hiringPlan', 'get').mockReturnValue(null);
            jest.spyOn($rootScope.currentUser, 'onboardingUnlimitedWithSourcing', 'get').mockReturnValue(true);

            // But true if legacy
            jest.spyOn($rootScope.currentUser, 'usingLegacyHiringPlan', 'get').mockReturnValue(true);

            expect(viewModel.showHiringManagerNav).toBe(true);
        });

        it('should be false if no hiring plan', () => {
            jest.spyOn(viewModel, 'layout', 'get').mockReturnValue('hiring-manager');
            jest.spyOn($rootScope.currentUser, 'hiringPlan', 'get').mockReturnValue(null);
            jest.spyOn($rootScope.currentUser, 'onboardingUnlimitedWithSourcing', 'get').mockReturnValue(false);
            expect(viewModel.showHiringManagerNav).toBe(false);
        });

        it('should be false if onboarding as unlimited-with-sourcing user', () => {
            jest.spyOn(viewModel, 'layout', 'get').mockReturnValue('hiring-manager');
            jest.spyOn($rootScope.currentUser, 'hiringPlan', 'get').mockReturnValue('foo');
            jest.spyOn($rootScope.currentUser, 'onboardingUnlimitedWithSourcing', 'get').mockReturnValue(true);
            expect(viewModel.showHiringManagerNav).toBe(false);
        });
    });
});
