import 'AngularSpecHelper';
import 'Navigation/angularModule';

describe('Navigation::ShareService', () => {
    const context = 'share_spec';
    let $window;
    let ShareService;
    let EventLogger;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Navigation', 'SpecHelper');

        // mock out $window
        $window = {
            open: jest.fn(),
            screen: {
                width: 1000,
                height: 500,
            },
            document: window.document,
            navigator: {
                onLine: true,
            },
            performance: window.performance,
        };
        angular.mock.module($provide => {
            $provide.value('$window', $window);
        });

        angular.mock.inject($injector => {
            ShareService = $injector.get('Navigation.ShareService');
            $window = $injector.get('$window');
            EventLogger = $injector.get('EventLogger');
        });
    });

    describe('share', () => {
        const validShareInfo = {
            url: 'http://example.com',
            title: 'title value',
            description: 'standard description value',
            campaignMedium: 'social',
            campaignDescription: 'campaign',

            twitter: {
                description: 'twitter description value',
            },

            email: {
                title: 'email_subject',
                description: 'email description value',
            },
        };

        describe('validation', () => {
            it('should error if the service does not exist', () => {
                expect(() => {
                    ShareService.share(context, 'foobar', {});
                }).toThrow(new Error("No sharing service for 'foobar' found"));
            });

            it('should error if the URL is not provided', () => {
                const brokenInfo = angular.copy(validShareInfo);
                delete brokenInfo.url;
                expect(() => {
                    ShareService.share(context, 'facebook', brokenInfo);
                }).toThrow(
                    new Error(
                        'In order to perform a social sharing function, a URL and title must be specified at a minimum',
                    ),
                );
            });

            it('should error if the title is not provided', () => {
                const brokenInfo = angular.copy(validShareInfo);
                delete brokenInfo.title;
                expect(() => {
                    ShareService.share(context, 'facebook', brokenInfo);
                }).toThrow(
                    new Error(
                        'In order to perform a social sharing function, a URL and title must be specified at a minimum',
                    ),
                );
            });
        });

        describe('url generation', () => {
            let expectedScreenX;
            let expectedScreenY;

            beforeEach(() => {
                expectedScreenX = $window.screen.width / 2 - (450 / 2 + 10);
                expectedScreenY = $window.screen.height / 2 - (350 / 2 + 50);
            });

            it('should generate Facebook URLs correctly', () => {
                ShareService.share(context, 'facebook', validShareInfo);
                expect($window.open).toHaveBeenCalledWith(
                    'https://www.facebook.com/sharer/sharer.php?t=title%20value&u=http%3A%2F%2Fexample.com%3Futm_source%3Dfacebook%26utm_medium%3Dsocial%26utm_campaign%3Dcampaign&',
                    '_blank',
                    `location=no,toolbar=no,status=no,height=350,width=450,resizable=yes,left=${expectedScreenX},top=${expectedScreenY},screenX=${expectedScreenX},screenY=${expectedScreenY},menubar=no,scrollbars=no,directories=no`,
                );
            });

            it('should generate LinkedIn URLs correctly', () => {
                ShareService.share(context, 'linkedin', validShareInfo);
                expect($window.open).toHaveBeenCalledWith(
                    'http://www.linkedin.com/shareArticle?title=title%20value&url=http%3A%2F%2Fexample.com%3Futm_source%3Dlinkedin%26utm_medium%3Dsocial%26utm_campaign%3Dcampaign&source=http%3A%2F%2Fexample.com%3Futm_source%3Dlinkedin%26utm_medium%3Dsocial%26utm_campaign%3Dcampaign&text=standard%20description%20value&mini=true&',
                    '_blank',
                    `location=no,toolbar=no,status=no,height=350,width=450,resizable=yes,left=${expectedScreenX},top=${expectedScreenY},screenX=${expectedScreenX},screenY=${expectedScreenY},menubar=no,scrollbars=no,directories=no`,
                );
            });

            it('should generate Twitter URLs correctly', () => {
                ShareService.share(context, 'twitter', validShareInfo);
                expect($window.open).toHaveBeenCalledWith(
                    'https://twitter.com/intent/tweet?undefined=title%20value&url=http%3A%2F%2Fexample.com%3Futm_source%3Dtwitter%26utm_medium%3Dsocial%26utm_campaign%3Dcampaign&source=http%3A%2F%2Fexample.com%3Futm_source%3Dtwitter%26utm_medium%3Dsocial%26utm_campaign%3Dcampaign&text=twitter%20description%20value&',
                    '_blank',
                    `location=no,toolbar=no,status=no,height=350,width=450,resizable=yes,left=${expectedScreenX},top=${expectedScreenY},screenX=${expectedScreenX},screenY=${expectedScreenY},menubar=no,scrollbars=no,directories=no`,
                );
            });

            it('should generate Email URLs correctly', () => {
                ShareService.share(context, 'email', validShareInfo);
                expect($window.open).toHaveBeenCalledWith(
                    'mailto:?subject=email_subject&body=email%20description%20value&',
                    '_blank',
                    `location=no,toolbar=no,status=no,height=350,width=450,resizable=yes,left=${expectedScreenX},top=${expectedScreenY},screenX=${expectedScreenX},screenY=${expectedScreenY},menubar=no,scrollbars=no,directories=no`,
                );
            });
        });

        describe('event logging', () => {
            beforeEach(() => {
                jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            });

            it('should log events appropriately', () => {
                ShareService.share(context, 'facebook', validShareInfo);
                expect(EventLogger.prototype.log).toHaveBeenCalledWith('share', {
                    label: context,
                    service: 'facebook',
                    campaign: 'social',
                });
            });

            it('should log events appropriately', () => {
                ShareService.share(context, 'facebook', validShareInfo, {
                    additionalProperty: true,
                });
                expect(EventLogger.prototype.log).toHaveBeenCalledWith('share', {
                    label: context,
                    service: 'facebook',
                    campaign: 'social',
                    additionalProperty: true,
                });
            });
        });
    });
});
