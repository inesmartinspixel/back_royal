import 'AngularSpecHelper';
import 'Navigation/angularModule';

describe('Navigation.ViewAsMenu', () => {
    let SpecHelper;
    let elem;
    let $injector;
    let $rootScope;
    let user;
    let Institution;
    let Cohort;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Navigation', 'SpecHelper', 'FrontRoyal.Institutions');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;

                SpecHelper = $injector.get('SpecHelper');
                $rootScope = $injector.get('$rootScope');
                Institution = $injector.get('Institution');
                Cohort = $injector.get('Cohort');

                SpecHelper.stubConfig();
            },
        ]);

        user = SpecHelper.stubCurrentUser('admin');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<view-as-menu></view-as-menu>');
        elem = renderer.elem;
    }

    it('should reset viewAs when reset button is pressed', () => {
        $rootScope.viewAs = 'group:foo';
        render();
        expect($rootScope.viewAs).toBe('group:foo');
        SpecHelper.click(elem, '[name="reset"]');
        expect($rootScope.viewAs).toBeUndefined();
    });

    it('should set viewAs when an item is pressed', () => {
        user.available_groups = ['foo', 'bar'];
        render();

        SpecHelper.click(elem, '[name="load"]');
        expect($rootScope.availableGroups.length).toBe(2);
        SpecHelper.click(elem, '[name="group"]:first');
        expect($rootScope.viewAs).toBe('group:foo');
    });

    describe('loading', () => {
        it('should load groups', () => {
            user.available_groups = ['foo', 'bar'];
            render();

            SpecHelper.toggleRadio(elem, 'input[type="radio"]', 0);
            SpecHelper.click(elem, '[name="load"]');
            expect($rootScope.availableGroups.length).toBe(2);
        });

        it('should load institutions', () => {
            render();

            Institution.expect('index');
            SpecHelper.toggleRadio(elem, 'input[type="radio"]', 1);
            SpecHelper.click(elem, '[name="load"]');
        });

        it('should load cohorts', () => {
            render();

            Cohort.expect('index').toBeCalledWith({
                fields: ['BASIC_FIELDS'],
            });
            SpecHelper.toggleRadio(elem, 'input[type="radio"]', 2);
            SpecHelper.click(elem, '[name="load"]');
        });
    });
});
