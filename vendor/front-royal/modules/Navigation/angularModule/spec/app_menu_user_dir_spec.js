import 'AngularSpecHelper';
import 'Navigation/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import appMenuUserLocales from 'Navigation/locales/navigation/app_menu_user-en.json';

setSpecLocales(appMenuUserLocales);

describe('Navigation::AppMenuUserDirSpec', () => {
    let SpecHelper;
    let elem;
    let scope;
    let $injector;
    let $rootScope;
    let FeedbackModal;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Navigation', 'SpecHelper');

        angular.mock.module($provide => {
            $provide.value('gravatarFilter', () => 'a/gravatar/url');
        });

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;

                SpecHelper = $injector.get('SpecHelper');
                $rootScope = $injector.get('$rootScope');
                FeedbackModal = $injector.get('FeedbackModal');

                stubCurrentUser('learner');

                SpecHelper.stubConfig();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render(noUser) {
        if (noUser) {
            $rootScope.currentUser = undefined;
        }
        const renderer = SpecHelper.renderer();
        renderer.render('<app-menu-user></app-menu-user>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$digest();
    }

    function stubCurrentUser(role) {
        SpecHelper.stubCurrentUser(role);
    }

    describe('user popover', () => {
        describe('with currentUser', () => {
            beforeEach(() => {
                $rootScope.currentUser.avatar_url = 'http://awesome.avatar';
                render();
                const menuArea = SpecHelper.expectElement(elem, '.app-menu-user');
                menuArea.trigger('mouseenter');
                SpecHelper.expectElement(elem, '.menu-user-popover');
            });

            it('should open feedback when clicked', () => {
                jest.spyOn(FeedbackModal, 'launchFeedback').mockImplementation(() => {});

                const link = SpecHelper.expectElementText(elem, '[name="launch-feedback"]', 'Give Feedback');
                link.click();

                expect(FeedbackModal.launchFeedback).toHaveBeenCalled();
            });

            describe('billing', () => {
                it('should show billing if currentUser is requiresHiringSubscription', () => {
                    jest.spyOn($rootScope.currentUser, 'requiresHiringSubscription', 'get').mockReturnValue(true);
                    scope.$digest();
                    SpecHelper.expectElement(elem, '[name="billing"]');
                });

                it('should not show billing if currentUser is not requiresHiringSubscription', () => {
                    jest.spyOn($rootScope.currentUser, 'requiresHiringSubscription', 'get').mockReturnValue(false);
                    scope.$digest();
                    SpecHelper.expectNoElement(elem, '[name="billing"]');
                });
            });
        });

        describe('without currentUser', () => {
            beforeEach(() => {
                render(true);
                const menuArea = SpecHelper.expectElement(elem, '.app-menu-user');
                menuArea.trigger('mouseenter');
                SpecHelper.expectElement(elem, '.menu-user-popover');
            });

            it('should have marketing links', () => {
                jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});
                const link = SpecHelper.expectElementText(elem, 'nav > p:eq(0)', 'Why Quantic');
                link.click();

                expect(scope.loadUrl).toHaveBeenCalledWith('/why');
            });
        });
    });
});
