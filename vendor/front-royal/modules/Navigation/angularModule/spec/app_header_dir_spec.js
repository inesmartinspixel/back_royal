import 'AngularSpecHelper';
import 'Navigation/angularModule';
import 'Editor/angularModule';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import setSpecLocales from 'Translation/setSpecLocales';
import appHeaderLocales from 'Navigation/locales/navigation/app_header-en.json';
import lessonPlayerViewModelLocales from 'Lessons/locales/lessons/models/lesson/lesson_player_view_model-en.json';
import appMenuLocales from 'Navigation/locales/navigation/app_menu-en.json';
import studentDashboardLearningBoxLocales from 'Lessons/locales/lessons/stream/student_dashboard_learning_box-en.json';
import appHeaderViewModelLocales from 'Navigation/locales/navigation/app_header_view_model-en.json';

setSpecLocales(
    appHeaderLocales,
    lessonPlayerViewModelLocales,
    appMenuLocales,
    studentDashboardLearningBoxLocales,
    appHeaderViewModelLocales,
);

describe('Navigation::AppHeaderDirSpec', () => {
    let AppHeaderViewModel;
    let SpecHelper;
    let elem;
    let scope;
    let $timeout;
    let $location;
    let $rootScope;
    let FrameList;
    let lesson;
    let stream;
    let playerViewModel;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Navigation', 'FrontRoyal.Editor', 'FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            $location = $injector.get('$location');
            $timeout = $injector.get('$timeout');
            $rootScope = $injector.get('$rootScope');
            AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');

            SpecHelper.stubConfig();
            SpecHelper.stubEventLogging();
            SpecHelper.stubDirective('appMenuUser');

            FrameList = $injector.get('Lesson.FrameList');

            $injector.get('LessonFixtures');
            $injector.get('ComponentizedFixtures');
            $injector.get('StreamFixtures');

            // set up lesson
            lesson = FrameList.fixtures.getInstance();
            stream = lesson.stream();

            // set up stream
            lesson.stream().createStreamViewModel();
            lesson = stream.lessons[0];
            playerViewModel = lesson.createPlayerViewModel();

            playerViewModel.linkToWindow($injector);

            // Normally, this would be called by RouteAssetLoader.loadGlobalDependencies()
            AppHeaderViewModel.refreshTranslations();
        });
    });

    it('pause should display the correct defaults for the root path', () => {
        playerViewModel.destroy();
        render();
        SpecHelper.expectNoElement(elem, '.icon.exit');
    });

    it('pause should display an alternate home button when configured', () => {
        playerViewModel.destroy();
        AppHeaderViewModel.showAlternateHomeButton = true;
        $timeout.flush();
        render();
        SpecHelper.expectNoElement(elem, '.icon.exit');
        SpecHelper.expectElement(elem, '.icon.home');
    });

    it('should have the exit button navigate back to stream dashboard', () => {
        jest.spyOn($location, 'url').mockImplementation(() => {});
        SpecHelper.stubConfig();
        render();
        AppHeaderViewModel.exitButtonClick();
        expect($location.url).toHaveBeenCalledWith(AppHeaderViewModel.playerViewModel.stream.streamDashboardPath);
    });

    it('should have the exit button navigate back to home if there is no stream', () => {
        jest.spyOn($rootScope, 'back').mockImplementation(() => {});
        SpecHelper.stubConfig();
        jest.spyOn(AppHeaderViewModel.playerViewModel.lesson, 'stream').mockReturnValue(undefined);
        render();
        AppHeaderViewModel.exitButtonClick();
        expect($rootScope.back).toHaveBeenCalled();
    });

    it('should show the main header when there is no playerViewModel', () => {
        AppHeaderViewModel.playerViewModel = undefined;
        render();
        SpecHelper.expectElement(elem, '.main-header');
        SpecHelper.expectNoElement(elem, '.player-header');
    });

    it('should display the player header when there is a playerViewModel', () => {
        Object.defineProperty(AppHeaderViewModel, 'layout', {
            writable: true,
        });
        AppHeaderViewModel.layout = 'lesson-player';

        AppHeaderViewModel.playerViewModel = playerViewModel;
        AppHeaderViewModel._showAlternateHomeButton = true;
        render();
        SpecHelper.expectElement(elem, '.player-header');
        SpecHelper.expectNoElement(elem, '.main-header');
        SpecHelper.expectElement(elem, 'a.home-link');
        SpecHelper.expectNoElement(elem, '.brand .logo');
        SpecHelper.expectElement(elem, '.exit-player-container');
    });

    it('should display the player header for a demo playerViewModel', () => {
        Object.defineProperty(AppHeaderViewModel, 'layout', {
            writable: true,
        });
        AppHeaderViewModel.layout = 'lesson-player';

        AppHeaderViewModel.playerViewModel = playerViewModel;
        AppHeaderViewModel._showAlternateHomeButton = true;
        playerViewModel.demoMode = true;
        render();
        SpecHelper.expectElement(elem, '.brand.demo .logo');
        SpecHelper.expectElement(elem, '.exit-player-container');
        SpecHelper.expectElement(elem, '.exit-demo-desktop');
        SpecHelper.expectNoElement(elem, 'a.home-link');
        SpecHelper.expectNoElement(elem, '.lesson-details');
    });

    it('should show lesson title', () => {
        Object.defineProperty(AppHeaderViewModel, 'layout', {
            writable: true,
        });
        AppHeaderViewModel.layout = 'lesson-player';

        AppHeaderViewModel.playerViewModel.lesson.title = 'lesson title';
        render();
        SpecHelper.expectElementText(elem, '.lesson-details h2', 'lesson title');
        SpecHelper.expectElementText(elem, '.lesson-details h3', 'Chapter 1 • Lesson 1 of 3');
    });

    it('should show previous button if shouldShowPreviousButton', () => {
        Object.defineProperty(AppHeaderViewModel.playerViewModel, 'shouldShowPreviousButton', {
            writable: true,
        });
        AppHeaderViewModel.playerViewModel.shouldShowPreviousButton = true;

        Object.defineProperty(AppHeaderViewModel, 'layout', {
            writable: true,
        });
        AppHeaderViewModel.layout = 'lesson-player';

        render();
        SpecHelper.expectElement(elem, '.back-player-container');
    });

    it('should not show previous button if !shouldShowPreviousButton', () => {
        Object.defineProperty(AppHeaderViewModel.playerViewModel, 'shouldShowPreviousButton', {
            writable: true,
        });
        AppHeaderViewModel.playerViewModel.shouldShowPreviousButton = false;
        render();
        SpecHelper.expectNoElement(elem, '.back-player-container');
    });

    it('should display the editor header when in the editor', () => {
        Object.defineProperty(AppHeaderViewModel, 'layout', {
            writable: true,
        });
        AppHeaderViewModel.layout = 'lesson-editor';

        render();
        SpecHelper.expectElement(elem, '.lesson-editor-header');
        SpecHelper.expectNoElement(elem, '.player-header');
        SpecHelper.expectNoElement(elem, '.main-header');
    });

    it("should display the current user's name and show a menu if logged in and no title is set", () => {
        SpecHelper.stubCurrentUser();
        AppHeaderViewModel.setTitleHTML();
        playerViewModel.destroy();
        render();

        SpecHelper.expectElementText(elem, '.main-header .title', scope.currentUser.name.toUpperCase());
        SpecHelper.expectNoElement(elem, '.ghost-mode-label');
    });

    it('should not error if current user has no first name', () => {
        const user = SpecHelper.stubCurrentUser();
        user.name = null;
        AppHeaderViewModel.setTitleHTML();
        playerViewModel.destroy();
        render();

        SpecHelper.expectElementText(elem, '.main-header .title', '');
        SpecHelper.expectNoElement(elem, '.ghost-mode-label');
    });

    it('should display special text if logged in as another user', () => {
        SpecHelper.stubCurrentUser();
        $rootScope.currentUser.ghostMode = true;
        render();

        SpecHelper.expectElementText(
            elem,
            '.ghost-mode-label',
            `Logged in as ${$rootScope.currentUser.email}. Hard refresh (Cmd+R) to return to your user.`,
        );
    });

    it('should have a tap bar if touch is enabled', () => {
        const scrollHelper = $injector.get('scrollHelper');
        AppHeaderViewModel.allowTapScroll = true;
        render();
        const tapContainer = SpecHelper.expectElement(elem, '.tap-scroll-container');
        jest.spyOn(scrollHelper, 'scrollToTop').mockImplementation(() => {});
        tapContainer.click();
        expect(scrollHelper.scrollToTop).toHaveBeenCalled();
    });

    it('should not have a tap bar if touch is disabled', () => {
        render();
        SpecHelper.expectNoElement(elem, '.tap-scroll-container');
    });

    describe('open courses button', () => {
        it('should have an open courses button if in the active_playlist view with multiple playlists', () => {
            AppHeaderViewModel.learningBoxMode = {
                mode: 'active_playlist',
                hasSinglePlaylist: false,
            };
            render();
            SpecHelper.expectElement(elem, '.mba-button');
        });

        it('should not have an open courses button if in the active_playlist view with a single playlist', () => {
            AppHeaderViewModel.learningBoxMode = {
                mode: 'active_playlist',
                hasSinglePlaylist: true,
            };
            render();
            SpecHelper.expectNoElement(elem, '.mba-button');
        });
    });

    describe('frameInstructions', () => {
        it('should show try again on a failed lesson', () => {
            playerViewModel.showStartScreen = false;
            playerViewModel.showFinishScreen = true;
            AppHeaderViewModel.playerViewModel = playerViewModel;
            Object.defineProperty(playerViewModel, 'lessonFailed', {
                value: true,
            });
            expect(AppHeaderViewModel.frameInstructions).toEqual('Try Again');
        });
    });

    describe('frameInstructionsForView', () => {
        beforeEach(() => {
            Object.defineProperty(AppHeaderViewModel, 'frameInstructions', {
                writable: true,
            });
        });

        it('should initially show blank', () => {
            expect(AppHeaderViewModel.frameInstructionsForView).toBe('');
        });
        it('should show new value after a delay', () => {
            const newValue = 'Begin Lesson';
            AppHeaderViewModel.frameInstructions = newValue;
            expect(AppHeaderViewModel.frameInstructionsForView).toBe('');
            expect(AppHeaderViewModel.frameInstructionsForView).toBe(''); // test multiple invocations
            $timeout.flush();
            expect(AppHeaderViewModel.frameInstructionsForView).toBe(newValue);
            expect(AppHeaderViewModel.frameInstructionsForView).toBe(newValue); // test multiple invocations
        });
        it('should use the latest value if the value changes again in the interim', () => {
            const newValue = 'Begin Lesson';
            AppHeaderViewModel.frameInstructions = newValue;
            expect(AppHeaderViewModel.frameInstructionsForView).toBe('');
            const newerValue = 'End Lesson';
            AppHeaderViewModel.frameInstructions = newerValue;
            $timeout.flush();
            expect(AppHeaderViewModel.frameInstructionsForView).toBe(newerValue);
        });
    });

    describe('setBodyBackground', () => {
        it('should change body background', () => {
            const bodyElem = $('body');

            // set to a custom color
            AppHeaderViewModel.setBodyBackground('purple');
            SpecHelper.expectElementHasClass(bodyElem, 'bg-purple');
            SpecHelper.expectElementDoesNotHaveClass(bodyElem, 'bg-beige');

            // set to a different custom color
            AppHeaderViewModel.setBodyBackground('blue');
            SpecHelper.expectElementHasClass(bodyElem, 'bg-blue');
            SpecHelper.expectElementDoesNotHaveClass(bodyElem, 'bg-purple');

            // no argument should go back to beige
            AppHeaderViewModel.setBodyBackground();
            SpecHelper.expectElementHasClass(bodyElem, 'bg-beige');
            SpecHelper.expectElementDoesNotHaveClass(bodyElem, 'bg-blue');
        });
    });

    describe('editor layout', () => {
        beforeEach(() => {
            Object.defineProperty(AppHeaderViewModel, 'layout', {
                writable: true,
            });
        });

        it("should display the appropriate text when there's no version history", () => {
            AppHeaderViewModel.layout = 'lesson-editor';
            render();
            SpecHelper.expectElementText(elem, '.lesson-editor-header .title', lesson.title);
        });

        it("should display the appropriate text when there's version history", () => {
            AppHeaderViewModel.layout = 'lesson-editor';

            const dateTime = new Date().getTime();
            const formattedDate = lesson.formatDate(dateTime);

            lesson.version_history = [
                {
                    updated_at: dateTime,
                    editorName: 'Some Editor',
                },
                {
                    updated_at: 0,
                    editorName: 'Another Editor',
                },
            ];
            lesson.last_editor = {
                name: 'Some Editor',
            };
            lesson.updated_at = dateTime;
            render();
            SpecHelper.expectElementText(
                elem,
                '.lesson-editor-header .title',
                `${lesson.title} Last saved by Some Editor at ${formattedDate} (not pinned)`,
            );
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<app-header current-user="currentUser"></app-header>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
