import 'AngularSpecHelper';
import 'Navigation/angularModule';

describe('Navigation::RouteEventHandler', () => {
    let $rootScope;
    let $location;
    let $window;
    let RouteEventHandler;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Navigation', 'SpecHelper');

        angular.mock.inject($injector => {
            $rootScope = $injector.get('$rootScope');
            $location = $injector.get('$location');
            $window = $injector.get('$window');
            RouteEventHandler = $injector.get('RouteEventHandler');
        });
    });

    afterEach(() => {
        $window.CORDOVA = undefined;
    });

    describe('deleteOnboardingHistory', () => {
        beforeEach(() => {
            $rootScope.pageHistory = [
                {
                    url: '/onboarding/hybrid/start',
                    directive: 'onboarding-hybrid-start',
                },
                {
                    url: '/onboarding/hybrid/sign-in',
                    directive: 'onboarding-hybrid-sign-in',
                },
                {
                    url: '/foo/bar',
                    directive: 'foo-bar',
                },
            ];
        });

        it('should remove onboarding entries from history', () => {
            RouteEventHandler.deleteOnboardingHistory();
            expect($rootScope.pageHistory).toEqual([
                {
                    url: '/foo/bar',
                    directive: 'foo-bar',
                },
            ]);
        });
    });

    describe('setupEventListeners', () => {
        describe('validation-responder:login-success Event listener', () => {
            beforeEach(() => {
                jest.spyOn(RouteEventHandler, 'deleteOnboardingHistory');
            });

            it('should call deleteOnboardingHistory if Cordova', () => {
                $window.CORDOVA = true;
                RouteEventHandler.setupEventListeners();
                $rootScope.$broadcast('validation-responder:login-success');
                expect(RouteEventHandler.deleteOnboardingHistory).toHaveBeenCalled();
            });

            it('should not call deleteOnboardingHistory if not Cordova', () => {
                $window.CORDOVA = false;
                RouteEventHandler.setupEventListeners();
                $rootScope.$broadcast('validation-responder:login-success');
                expect(RouteEventHandler.deleteOnboardingHistory).not.toHaveBeenCalled();
            });
        });

        describe('Cordova backbutton Event listener', () => {
            const event = new Event('backbutton');

            beforeEach(() => {
                $window.CORDOVA = true;
                $window.navigator.app = {
                    exitApp: jest.fn(),
                };
                $rootScope.pageHistory = [
                    {
                        url: '/some/directive',
                        directive: 'some-directive',
                    },
                    {
                        url: '/another/directive',
                        directive: 'another-directive',
                    },
                    {
                        url: '/yet/another/directive',
                        directive: 'yet-another-directive',
                    },
                ];
                jest.spyOn($location, 'url').mockImplementation(() => {});
                RouteEventHandler.setupEventListeners();
            });

            it('should change location when history length is not zero', () => {
                window.document.dispatchEvent(event);
                expect($location.url).toHaveBeenCalledWith('/another/directive');
            });

            describe('when Android and history length is zero ', () => {
                beforeEach(() => {
                    $window.clientIdentifier = 'android';
                    jest.spyOn($rootScope, 'homePath', 'get').mockReturnValue('/home/path');

                    // Since we pop the stack before checking the length, we need to
                    // add something to the stack before we pop it.
                    $rootScope.pageHistory = [{}];
                });

                it('should exit the application $location.url equals $rootScope.homePath', () => {
                    jest.spyOn($location, 'url').mockReturnValue('/home/path');
                    $window.document.dispatchEvent(event);
                    expect($window.navigator.app.exitApp).toHaveBeenCalled();
                });

                it('should navigate to $rootScope.homePath $location.url does not equal $rootScope.homePath', () => {
                    jest.spyOn($location, 'url').mockReturnValue('/not/home/path');
                    $window.document.dispatchEvent(event);
                    expect($location.url).toHaveBeenCalledWith('/home/path');
                });
            });
        });

        it('should not add backbutton listener if not Cordova', () => {
            $window.CORDOVA = false;
            jest.spyOn($window.document, 'addEventListener');
            RouteEventHandler.setupEventListeners();
            expect($window.document.addEventListener).not.toHaveBeenCalled();
        });
    });
});
