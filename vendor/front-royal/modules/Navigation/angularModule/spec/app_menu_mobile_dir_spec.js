import 'AngularSpecHelper';
import 'Careers/angularModule/spec/careers_spec_helper';
import 'Navigation/angularModule';

describe('Navigation::AppMenuMobileDirSpec', () => {
    let SpecHelper;
    let CareersSpecHelper;
    let elem;
    let scope;
    let $injector;
    let $rootScope;
    let $location;
    let ClientStorage;
    let CareersNetworkViewModel;
    let user;
    let AppHeaderViewModel;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Navigation', 'SpecHelper');

        angular.mock.module($provide => {
            $provide.value('gravatarFilter', () => 'a/gravatar/url');
        });

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;

                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                $rootScope = $injector.get('$rootScope');
                $location = $injector.get('$location');
                ClientStorage = $injector.get('ClientStorage');
                CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
                AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');

                user = SpecHelper.stubCurrentUser('learner');
                SpecHelper.stubConfig();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<app-menu-mobile></app-menu-mobile>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.currentUser = $rootScope.currentUser;
        scope.$digest();
    }

    it('should set active tab if on a main screen', () => {
        jest.spyOn($location, 'path').mockReturnValue('/library');
        render();
        expect(scope.activeTab).toBe('library');
    });

    it('should set active tab when a route change occurs', () => {
        render();
        scope.activeTab = 'library';
        jest.spyOn($location, 'path').mockReturnValue('/settings');
        $rootScope.$broadcast('$routeChangeSuccess');
        expect(scope.activeTab).toBe('settings');
    });

    it('should pull the last active tab from client storage if not on a main screen', () => {
        jest.spyOn($location, 'path').mockReturnValue('/an/inner/screen');
        jest.spyOn(ClientStorage, 'getItem').mockReturnValue('dashboard');
        render();
        expect(scope.activeTab).toBe('dashboard');
    });

    it('should persist new active tab to client storage when a main screen is hit', () => {
        jest.spyOn($location, 'path').mockReturnValue('/careers');
        jest.spyOn(ClientStorage, 'setItem').mockImplementation(() => {});
        render();
        expect(ClientStorage.setItem).toHaveBeenCalled();
    });

    it('should only show the careers section when appropriate', () => {
        let hasCareersNetworkAccess = false;
        Object.defineProperty($rootScope.currentUser, 'hasCareersNetworkAccess', {
            get() {
                return hasCareersNetworkAccess;
            },
        });

        Object.defineProperty($rootScope.currentUser, 'canApplyToSmartly', {
            get() {
                return hasCareersNetworkAccess;
            },
        });

        render();
        SpecHelper.expectNoElement(elem, '.careers-link');

        hasCareersNetworkAccess = true;
        jest.spyOn(CareersNetworkViewModel, 'get').mockImplementation(() => {});
        render();
        SpecHelper.expectElement(elem, '.careers-link');
    });

    it('should show notification badge for learner', () => {
        const vm = CareersSpecHelper.stubCareersNetworkViewModel('candidate');

        Object.defineProperty(AppHeaderViewModel, 'layout', {
            value: 'learner',
        });
        Object.defineProperty(user, 'hasCareersNetworkAccess', {
            value: true,
        });
        Object.defineProperty(user, 'canApplyToSmartly', {
            value: true,
        });
        Object.defineProperty(vm, 'connectionsNumNotifications', {
            value: 2,
        });

        user.num_recommended_positions = 3;
        render();
        SpecHelper.expectElementText(elem, '[name="careers-connection-number"]', '5');
    });

    it('should show the appropriate logo in the home button', () => {
        const shouldSeeQuanticBrandingSpy = jest.spyOn(user, 'shouldSeeQuanticBranding', 'get').mockReturnValue(true);
        const isMiyaMiyaSpy = jest.spyOn(user, 'isMiyaMiya', 'get').mockReturnValue(true);

        render();

        // Quantic users see a Quantic logo in the home button
        SpecHelper.expectElementImgSrc(
            elem,
            '[role="toolbar"] .btn-group:first-of-type img',
            'menu_logo_white_quantic.png',
        );

        shouldSeeQuanticBrandingSpy.mockReturnValue(false);
        scope.$digest();
        // Miya Miya users see a generic logo in the home button
        SpecHelper.expectElementImgSrc(elem, '[role="toolbar"] .btn-group:first-of-type img', 'menu_home_white.png');

        isMiyaMiyaSpy.mockReturnValue(false);
        scope.$digest();
        // non-Quantic users that are also non-MiyaMiya users see the Smartly logo in the home button
        SpecHelper.expectElementImgSrc(elem, '[role="toolbar"] .btn-group:first-of-type img', 'menu_logo_white.png');
    });

    describe('student-network', () => {
        beforeEach(() => {
            jest.spyOn($rootScope.currentUser, 'hasNetworkTabAccess', 'get').mockReturnValue(true);
        });

        it('should show notification if the user has access and has not seen the onboarding', () => {
            jest.spyOn($rootScope.currentUser, 'hasStudentNetworkAccess', 'get').mockReturnValue(true);
            $rootScope.currentUser.has_seen_student_network = false;
            render();
            SpecHelper.expectElementText(elem, '.connection-number', '1');
        });

        it('should show notification if the user has access and requires a valid student network email', () => {
            jest.spyOn($rootScope.currentUser, 'hasStudentNetworkAccess', 'get').mockReturnValue(true);
            jest.spyOn($rootScope.currentUser, 'missingRequiredStudentNetworkEmail', 'get').mockReturnValue(true);

            render();
            SpecHelper.expectElementText(elem, '.connection-number', '1');
        });
    });
});
