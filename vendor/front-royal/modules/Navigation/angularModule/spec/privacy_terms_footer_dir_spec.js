import 'AngularSpecHelper';
import 'Navigation/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import privacyTermsFooter from 'Navigation/locales/navigation/privacy_terms_footer-en.json';

setSpecLocales(privacyTermsFooter);

describe('Navigation::privacyTermsFooter', () => {
    let SpecHelper;
    let elem;
    let scope;
    let $injector;
    let $window;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Navigation', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $window = $injector.get('$window');
            },
        ]);

        jest.spyOn($window, 'open').mockImplementation(() => {});
        render();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<privacy-terms-footer></privacy-terms-footer>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$digest();
    }

    it('should open the Privacy Policy when clicked', () => {
        SpecHelper.expectElementText(elem, 'a:eq(0)', 'Privacy Policy');
        SpecHelper.click(elem, 'a:eq(0)');
        expect($window.open).toHaveBeenCalledWith(`${$window.ENDPOINT_ROOT}/privacy`, '_blank', undefined);
    });

    it('should open the Terms when clicked', () => {
        SpecHelper.expectElementText(elem, 'a:eq(1)', 'Terms');
        SpecHelper.click(elem, 'a:eq(1)');
        expect($window.open).toHaveBeenCalledWith(`${$window.ENDPOINT_ROOT}/terms`, '_blank', undefined);
    });
});
