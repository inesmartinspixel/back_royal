import 'AngularSpecHelper';
import 'Navigation/angularModule';

import { EMPLOYER_SIGNUP_LOCATION } from 'SignupLocations';

import 'FrontRoyalFrameRpc';

let eventName;
let callback;
jest.mock('FrontRoyalFrameRpc', () => ({
    __esModule: true,
    default: () => ({
        call: (en, cb) => {
            eventName = en;
            callback = cb;
        },
    }),
}));

describe('Navigation::RouteResolvers', () => {
    let $injector;
    let RouteResolvers;
    let $q;
    let $rootScope;
    let SpecHelper;
    let promiseResolved;
    let promiseResponse;
    let ConfigFactory;
    let blockAuthenticatedAccess;
    let $routeMock;
    let isMobile;
    let RouteAssetLoader;
    let $window;
    let LearnerContentCache;
    let User;
    let offlineModeManager;
    let $timeout;

    beforeEach(() => {
        angular.mock.module(
            'FrontRoyal.Navigation',
            'SpecHelper',

            // NoUnhandledRejectionExceptions needed by the checkOfflineMode spec.
            // Not sure why
            'NoUnhandledRejectionExceptions',
        );

        $routeMock = {
            current: {
                pathParams: {
                    url_prefix: undefined,
                },
            },
        };
        angular.mock.module($provide => {
            $provide.value('$route', $routeMock);

            isMobile = jest.fn();
            isMobile.mockReturnValue(false);
            $provide.value('isMobile', isMobile);

            $window = {
                location: {
                    href: '',
                },
                document: window.document,
                markdown: window.markdown,
                confirm: () => true,
                open() {},
                navigator: {
                    onLine: true,
                },
                performance: window.performance,
            };
            $provide.value('$window', $window);
        });

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            RouteResolvers = $injector.get('Navigation.RouteResolvers');
            $q = $injector.get('$q');
            $rootScope = $injector.get('$rootScope');
            ConfigFactory = $injector.get('ConfigFactory');
            blockAuthenticatedAccess = $injector.get('blockAuthenticatedAccess');
            RouteAssetLoader = $injector.get('Navigation.RouteAssetLoader');
            LearnerContentCache = $injector.get('LearnerContentCache');
            User = $injector.get('User');
            $timeout = $injector.get('$timeout');
        });

        $rootScope.networkBootstrapCompleted = true;

        promiseResolved = undefined;
        promiseResponse = undefined;

        Object.defineProperty(blockAuthenticatedAccess, 'block', {
            value: false,
        });

        offlineModeManager = $injector.get('offlineModeManager');
        jest.spyOn(LearnerContentCache, 'ensureStudentDashboard').mockImplementation(() => $q.when({}));
        jest.spyOn(offlineModeManager, 'rejectInOfflineMode').mockImplementation(fn => fn());
        jest.spyOn(offlineModeManager, 'resolveRoute').mockImplementation(() => $q.when({}));
    });

    describe('profileConfirmationRequired', () => {
        it('should resolve if the user is present but not confirmed', () => {
            stubWaitForAuthCallComplete('learner');
            $rootScope.currentUser.confirmed_profile_info = false;
            executeResolver(RouteResolvers.profileConfirmationRequired, true, undefined);
        });

        it('should reject if the user is not present', () => {
            stubWaitForAuthCallComplete();
            executeResolver(RouteResolvers.profileConfirmationRequired, false, {
                redirect: '/',
            });
        });

        it('should reject if the user is present but confirmed', () => {
            stubWaitForAuthCallComplete('learner');
            $rootScope.currentUser.confirmed_profile_info = true;
            executeResolver(RouteResolvers.profileConfirmationRequired, false, {
                redirect: '/dashboard',
            });
        });
    });

    describe('redirects in loadAccountAndRejectIfInvalid', () => {
        describe('incomplete profile registration redirect', () => {
            it('should reject with redirect to /complete-registration if !user.confirmed_profile_info', () => {
                stubWaitForAuthCallComplete('learner');
                $rootScope.currentUser.confirmed_profile_info = false;
                const $route = $injector.get('$route');
                $route.current.$$route = {
                    regexp: new RegExp($rootScope.homePath), // only applies to home path
                };
                executeResolver(RouteResolvers.hasLearnerAccess, false, {
                    redirect: '/complete-registration',
                    search: {
                        target: '',
                    },
                });
            });
        });

        describe('unrestrictedAccess', () => {
            it('should resolve if the user is not present', () => {
                stubWaitForAuthCallComplete();
                executeResolver(RouteResolvers.unrestrictedAccess, true, undefined, false);
            });

            it('should reject with the application page if shouldRedirectOnIncompleteOnboarding and !onboardingComplete', () => {
                stubWaitForAuthCallComplete('learner');
                jest.spyOn($rootScope.currentUser, 'shouldRedirectOnIncompleteOnboarding', 'get').mockReturnValue(true);
                jest.spyOn($rootScope.currentUser, 'onboardingComplete', 'get').mockReturnValue(false);

                User.expect('save');

                executeResolver(RouteResolvers.unrestrictedAccess, false, {
                    redirect: '/settings/application',
                });
                expect($rootScope.currentUser.$$hasBeenRedirectedToApplication).toBe(true);
            });

            it("should reject with the application page if currentUser's lastCohortApplication can_convert_to_emba", () => {
                stubWaitForAuthCallComplete('learner');
                const $route = $injector.get('$route');
                $route.current.$$route = {
                    regexp: new RegExp($rootScope.homePath), // only applies to home path
                };
                jest.spyOn($rootScope.currentUser, 'shouldRedirectOnIncompleteOnboarding', 'get').mockReturnValue(
                    false,
                ); // ensure no false positives
                jest.spyOn($rootScope.currentUser, 'lastCohortApplication', 'get').mockReturnValue({
                    can_convert_to_emba: true,
                });

                User.expect('save');

                executeResolver(RouteResolvers.unrestrictedAccess, false, {
                    redirect: '/settings/application',
                });
            });

            it('should not reject with the application page if already redirected', () => {
                stubWaitForAuthCallComplete('learner');
                jest.spyOn($rootScope.currentUser, 'shouldRedirectOnIncompleteOnboarding', 'get').mockReturnValue(true);
                jest.spyOn($rootScope.currentUser, 'onboardingComplete', 'get').mockReturnValue(false);

                $rootScope.currentUser.$$hasBeenRedirectedToApplication = true;

                User.expect('save');

                executeResolver(RouteResolvers.unrestrictedAccess, true, $rootScope.currentUser, true);
            });

            it('should not reject with the application page if !currentUser.shouldRedirectOnIncompleteOnboarding', () => {
                stubWaitForAuthCallComplete('editor');
                jest.spyOn($rootScope.currentUser, 'shouldRedirectOnIncompleteOnboarding', 'get').mockReturnValue(
                    false,
                );
                jest.spyOn($rootScope.currentUser, 'onboardingComplete', 'get').mockReturnValue(false);

                User.expect('save');

                executeResolver(RouteResolvers.unrestrictedAccess, true, $rootScope.currentUser, true);
            });

            it('should not reject to application_status page if user is not shouldVisitApplicationStatus', () => {
                const $route = $injector.get('$route');
                $route.current.$$route = {
                    regexp: new RegExp($rootScope.homePath), // only applies to home path
                };

                stubWaitForAuthCallComplete(
                    'learner',
                    {},
                    {
                        shouldVisitApplicationStatus: false,
                    },
                );

                executeResolver(RouteResolvers.unrestrictedAccess, true, $rootScope.currentUser, true);
            });

            it('should not reject with the application_status page another route has already been loaded', () => {
                const $route = $injector.get('$route');
                $route.current.$$route = {
                    regexp: new RegExp('another_route'),
                };

                stubWaitForAuthCallComplete(
                    'learner',
                    {},
                    {
                        shouldVisitApplicationStatus: false,
                    },
                );

                executeResolver(RouteResolvers.unrestrictedAccess, true, $rootScope.currentUser, true);

                $route.current.$$route = {
                    regexp: new RegExp($rootScope.homePath), // only applies to home path
                };

                executeResolver(RouteResolvers.unrestrictedAccess, true, $rootScope.currentUser, true);
            });

            it('should not reject to application_status page if user $$hasCompletedFirstLoad', () => {
                const $route = $injector.get('$route');
                $route.current.$$route = {
                    regexp: new RegExp($rootScope.homePath), // only applies to home path
                };

                stubWaitForAuthCallComplete(
                    'learner',
                    {},
                    {
                        shouldVisitApplicationStatus: true,
                    },
                );

                $rootScope.currentUser.$$hasCompletedFirstLoad = true;

                executeResolver(RouteResolvers.unrestrictedAccess, true, $rootScope.currentUser, true);
            });

            it('should resolve with the user if everything is okay', () => {
                stubWaitForAuthCallComplete('learner');
                $rootScope.currentUser.has_seen_welcome = true;
                executeResolver(RouteResolvers.unrestrictedAccess, true, $rootScope.currentUser, true);
            });

            it('should reject the promise if explicitly blocked', () => {
                Object.defineProperty(blockAuthenticatedAccess, 'block', {
                    value: true,
                });
                stubWaitForAuthCallComplete('learner');
                executeResolver(RouteResolvers.unrestrictedAccess, false, {
                    redirect: '/sign-in',
                    search: {
                        target: '',
                    },
                });
            });

            describe('shouldVisitApplicationStatus', () => {
                it('should reject with the application_status page if the user shouldVisitApplicationStatus', () => {
                    const $route = $injector.get('$route');
                    $route.current.$$route = {
                        regexp: new RegExp($rootScope.homePath), // only applies to home path
                    };

                    stubWaitForAuthCallComplete(
                        'learner',
                        {},
                        {
                            shouldVisitApplicationStatus: true,
                        },
                    );
                    executeResolver(RouteResolvers.unrestrictedAccess, false, {
                        redirect: '/settings/application_status',
                    });
                });

                it('should not reject with the application_status page if user is not shouldVisitApplicationStatus', () => {
                    const $route = $injector.get('$route');
                    $route.current.$$route = {
                        regexp: new RegExp($rootScope.homePath), // only applies to home path
                    };

                    stubWaitForAuthCallComplete(
                        'learner',
                        {},
                        {
                            shouldVisitApplicationStatus: false,
                        },
                    );

                    executeResolver(RouteResolvers.unrestrictedAccess, true, $rootScope.currentUser, true);
                });
            });
        });

        describe('hiring manager access redirects', () => {
            it('should redirect a hiring manager with no access', () => {
                jest.spyOn(User.prototype, 'hasHiringManagerAccess', 'get').mockReturnValue(false);
                jest.spyOn($rootScope, 'homePath', 'get').mockReturnValue('homePath');
                stubWaitForAuthCallComplete('acceptedHiringManager');
                const $route = $injector.get('$route');
                $route.current.$$route = {
                    regexp: new RegExp('some-other-place'), // only applies to home path
                };
                executeResolver(RouteResolvers.hasHiringManagerAccess, false, {
                    redirect: 'homePath',
                });
            });

            it('should not redirect a hiring manager with access', () => {
                jest.spyOn(User.prototype, 'hasHiringManagerAccess', 'get').mockReturnValue(true);
                stubWaitForAuthCallComplete('acceptedHiringManager');
                const $route = $injector.get('$route');
                $route.current.$$route = {
                    regexp: new RegExp('some-other-place'), // only applies to home path
                };
                executeResolver(RouteResolvers.hasHiringManagerAccess, true);
            });
        });
    });

    describe('redirectToHome', () => {
        it('should reject with a redirect to the dashboard', () => {
            stubWaitForAuthCallComplete('learner');
            executeResolver(RouteResolvers.redirectToHome, false, {
                redirect: '/dashboard',
            });
        });

        it('should break the promise if explicitly blocked', () => {
            Object.defineProperty(blockAuthenticatedAccess, 'block', {
                value: true,
            });
            stubWaitForAuthCallComplete('learner');
            expect(RouteResolvers.redirectToHome()).toBeUndefined();
        });
    });

    describe('redirectToHomeIfAuthorized', () => {
        it('should reject with a redirect to the dashboard if the user is present', () => {
            stubWaitForAuthCallComplete('learner');
            executeResolver(RouteResolvers.redirectToHomeIfAuthorized, false, {
                redirect: '/dashboard',
            });
        });

        it('should resolve the user is not present', () => {
            stubWaitForAuthCallComplete();
            executeResolver(RouteResolvers.redirectToHomeIfAuthorized, true, undefined);
        });

        it('should break the promise if explicitly blocked', () => {
            Object.defineProperty(blockAuthenticatedAccess, 'block', {
                value: true,
            });
            stubWaitForAuthCallComplete('learner');
            expect(RouteResolvers.redirectToHomeIfAuthorized()).toBeUndefined();
        });
    });

    describe('loadJoinAndRedirect', () => {
        beforeEach(() => {
            stubWaitForAuthCallComplete();
        });

        afterEach(() => {
            $window.CORDOVA = undefined;
        });

        it('should redirect hiring page to a special place', () => {
            $routeMock.current.pathParams.url_prefix = 'hiring';
            const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
            jest.spyOn(NavigationHelperMixin, 'loadUrl').mockImplementation(() => {});
            RouteResolvers.loadJoinAndRedirect();
            expect(NavigationHelperMixin.loadUrl).toHaveBeenCalledWith(
                window.location.origin + EMPLOYER_SIGNUP_LOCATION,
                '_self',
            );
        });

        it('should respect the forcedUrlPrefix for Cordova builds', () => {
            $window.CORDOVA = {
                forcedUrlPrefix: 'forced_prefix',
            };
            executeResolver(RouteResolvers.loadJoinAndRedirect, false, {
                redirect: '/forced_prefix/join/account',
                search: {},
            });
        });

        it('should respect the url_prefix in the pathParams, otherwise', () => {
            $routeMock.current.pathParams.url_prefix = 'path_prefix';
            executeResolver(RouteResolvers.loadJoinAndRedirect, false, {
                redirect: '/path_prefix/join/account',
                search: {},
            });
        });
    });

    describe('hasLearnerAccess', () => {
        it('should reject to /sign-in if the user is not present', () => {
            stubWaitForAuthCallComplete();
            executeResolver(RouteResolvers.hasLearnerAccess, false, {
                redirect: '/sign-in',
                search: {
                    target: '',
                },
            });
        });

        it('should resolve if the user is a learner', () => {
            stubWaitForAuthCallComplete('learner');
            executeResolver(RouteResolvers.hasLearnerAccess, true, undefined, true);
        });

        it('should resolve if the user is greater than a learner', () => {
            stubWaitForAuthCallComplete('editor');
            executeResolver(RouteResolvers.hasLearnerAccess, true, undefined, true);
        });

        it('should reject the promise if explicitly blocked', () => {
            Object.defineProperty(blockAuthenticatedAccess, 'block', {
                value: true,
            });
            stubWaitForAuthCallComplete('learner');
            executeResolver(RouteResolvers.hasLearnerAccess, false, {
                redirect: '/sign-in',
                search: {
                    target: '',
                },
            });
        });
    });

    describe('hasEditorAccess', () => {
        it('should reject to /sign-in if the user is not present', () => {
            stubWaitForAuthCallComplete();
            executeResolver(RouteResolvers.hasEditorAccess, false, {
                redirect: '/sign-in',
                search: {
                    target: '',
                },
            });
        });

        it('should reject if the user is less than an editor', () => {
            stubWaitForAuthCallComplete('learner');
            executeResolver(RouteResolvers.hasEditorAccess, false, {
                redirect: '/sign-in',
                search: {
                    target: '',
                },
            });
        });

        it('should resolve if the user is an editor', () => {
            stubWaitForAuthCallComplete('editor');
            jest.spyOn(RouteAssetLoader, 'loadEditorDependencies').mockReturnValue($q.when());
            executeResolver(RouteResolvers.hasEditorAccess, true, undefined, true);
            expect(RouteAssetLoader.loadEditorDependencies).toHaveBeenCalled();
        });

        it('should resolve if the user is greater than an editor', () => {
            stubWaitForAuthCallComplete('super_editor');
            jest.spyOn(RouteAssetLoader, 'loadEditorDependencies').mockReturnValue($q.when());
            executeResolver(RouteResolvers.hasEditorAccess, true, undefined, true);
            expect(RouteAssetLoader.loadEditorDependencies).toHaveBeenCalled();
        });

        it('should reject the promise if explicitly blocked', () => {
            Object.defineProperty(blockAuthenticatedAccess, 'block', {
                value: true,
            });
            stubWaitForAuthCallComplete('editor');
            executeResolver(RouteResolvers.hasEditorAccess, false, {
                redirect: '/sign-in',
                search: {
                    target: '',
                },
            });
        });
    });

    describe('hasSuperEditorAccess', () => {
        it('should reject to /sign-in if the user is not present', () => {
            stubWaitForAuthCallComplete();
            executeResolver(RouteResolvers.hasSuperEditorAccess, false, {
                redirect: '/sign-in',
                search: {
                    target: '',
                },
            });
        });

        it('should reject if the user is less than a super-editor', () => {
            stubWaitForAuthCallComplete('editor');
            executeResolver(RouteResolvers.hasSuperEditorAccess, false, {
                redirect: '/sign-in',
                search: {
                    target: '',
                },
            });
        });

        it('should resolve if the user is a super-editor', () => {
            stubWaitForAuthCallComplete('super_editor');
            jest.spyOn(RouteAssetLoader, 'loadAdminDependencies').mockReturnValue($q.when());
            executeResolver(RouteResolvers.hasSuperEditorAccess, true, undefined, true);
            expect(RouteAssetLoader.loadAdminDependencies).toHaveBeenCalled();
        });

        it('should resolve if the user is greater than a super-editor', () => {
            stubWaitForAuthCallComplete('admin');
            jest.spyOn(RouteAssetLoader, 'loadAdminDependencies').mockReturnValue($q.when());
            executeResolver(RouteResolvers.hasSuperEditorAccess, true, undefined, true);
            expect(RouteAssetLoader.loadAdminDependencies).toHaveBeenCalled();
        });

        it('should reject the promise if explicitly blocked', () => {
            Object.defineProperty(blockAuthenticatedAccess, 'block', {
                value: true,
            });
            stubWaitForAuthCallComplete('super_editor');
            executeResolver(RouteResolvers.hasSuperEditorAccess, false, {
                redirect: '/sign-in',
                search: {
                    target: '',
                },
            });
        });
    });

    describe('hasAdminAccess', () => {
        it('should reject to /sign-in if the user is not present', () => {
            stubWaitForAuthCallComplete();
            executeResolver(RouteResolvers.hasAdminAccess, false, {
                redirect: '/sign-in',
                search: {
                    target: '',
                },
            });
        });

        it('should reject if the user is less than an admin', () => {
            stubWaitForAuthCallComplete('super_editor');
            executeResolver(RouteResolvers.hasAdminAccess, false, {
                redirect: '/sign-in',
                search: {
                    target: '',
                },
            });
        });

        it('should resolve if the user is an admin', () => {
            stubWaitForAuthCallComplete('admin');
            jest.spyOn(RouteAssetLoader, 'loadAdminDependencies').mockReturnValue($q.when());
            executeResolver(RouteResolvers.hasAdminAccess, true, undefined, true);
            expect(RouteAssetLoader.loadAdminDependencies).toHaveBeenCalled();
        });

        it('should reject the promise if explicitly blocked', () => {
            Object.defineProperty(blockAuthenticatedAccess, 'block', {
                value: true,
            });
            stubWaitForAuthCallComplete('admin');
            executeResolver(RouteResolvers.hasAdminAccess, false, {
                redirect: '/sign-in',
                search: {
                    target: '',
                },
            });
        });
    });

    describe('hasReportsAccess', () => {
        it('should reject to /sign-in if the user is not present', () => {
            stubWaitForAuthCallComplete();
            executeResolver(RouteResolvers.hasReportsAccess, false, {
                redirect: '/sign-in',
                search: {
                    target: '',
                },
            });
        });

        it('should reject if the user is less than a reports user', () => {
            stubWaitForAuthCallComplete('learner');
            executeResolver(RouteResolvers.hasReportsAccess, false, {
                redirect: '/sign-in',
                search: {
                    target: '',
                },
            });
        });

        it('should resolve if the user is a reports user', () => {
            stubWaitForAuthCallComplete('learner');
            $rootScope.currentUser.is_institutional_reports_viewer = true;
            jest.spyOn(RouteAssetLoader, 'loadReportsDependencies').mockReturnValue($q.when());
            executeResolver(RouteResolvers.hasReportsAccess, true, undefined, true);
            expect(RouteAssetLoader.loadReportsDependencies).toHaveBeenCalled();
        });

        it('should resolve if the user is greater than a reports userr', () => {
            stubWaitForAuthCallComplete('super_editor');
            jest.spyOn(RouteAssetLoader, 'loadReportsDependencies').mockReturnValue($q.when());
            executeResolver(RouteResolvers.hasReportsAccess, true, undefined, true);
            expect(RouteAssetLoader.loadReportsDependencies).toHaveBeenCalled();
        });

        it('should reject the promise if explicitly blocked', () => {
            Object.defineProperty(blockAuthenticatedAccess, 'block', {
                value: true,
            });
            stubWaitForAuthCallComplete('reports');
            executeResolver(RouteResolvers.hasReportsAccess, false, {
                redirect: '/sign-in',
                search: {
                    target: '',
                },
            });
        });
    });

    describe('hasStudentNetworkEventsAccessForEventId', () => {
        let $route;

        beforeEach(() => {
            stubWaitForAuthCallComplete('learner');
            $route = $injector.get('$route');
        });

        it('should resolve if there is no event-id but the user has hasNetworkTabAccess', () => {
            Object.defineProperty($rootScope.currentUser, 'hasNetworkTabAccess', {
                value: true,
            });
            $route.current.params = {};
            executeResolver(RouteResolvers.hasStudentNetworkEventsAccessForEventId, true);
        });

        it('should not resolve if there is no event-id and does not have hasNetworkTabAccess', () => {
            Object.defineProperty($rootScope.currentUser, 'hasNetworkTabAccess', {
                value: false,
            });
            $route.current.params = {};
            executeResolver(RouteResolvers.hasStudentNetworkEventsAccessForEventId, false, {
                redirect: '/sign-in',
                search: {
                    target: '',
                },
            });
        });

        it('should resolve if there is an event-id and the user has hasStudentNetworkEventsAccess', () => {
            Object.defineProperty($rootScope.currentUser, 'hasNetworkTabAccess', {
                value: true,
            });
            Object.defineProperty($rootScope.currentUser, 'hasStudentNetworkEventsAccess', {
                value: true,
            });
            $route.current.params = { 'event-id': 1234 };
            executeResolver(RouteResolvers.hasStudentNetworkEventsAccessForEventId, true);
        });

        it('should reject if there is an event-id and the user does not have hasStudentNetworkEventsAccess', () => {
            Object.defineProperty($rootScope.currentUser, 'hasNetworkTabAccess', {
                value: true,
            });
            Object.defineProperty($rootScope.currentUser, 'hasStudentNetworkEventsAccess', {
                value: false,
            });
            $route.current.params = { 'event-id': 1234 };
            executeResolver(RouteResolvers.hasStudentNetworkEventsAccessForEventId, false, {
                redirect: '/sign-in',
                search: {
                    target: '',
                },
            });
        });
    });

    describe('scormAccess', () => {
        it('should call into the RPC API', () => {
            RouteResolvers.scormAccess().then(() => {
                promiseResolved = true;
            });
            expect(eventName).toEqual('requestScormToken');
            expect(typeof callback).toEqual('function');
            expect(promiseResolved).toBeUndefined();
            callback.call();
            $rootScope.$digest();
            expect(promiseResolved).toBe(true);
        });
    });

    describe('demoAccess', () => {
        it('should resolve with a redirect to the dashboard if the user is present', () => {
            stubWaitForAuthCallComplete('learner');
            executeResolver(RouteResolvers.demoAccess, true, $rootScope.currentUser);
        });

        it('should resolve if the user is not present', () => {
            stubWaitForAuthCallComplete();
            executeResolver(RouteResolvers.demoAccess, true, undefined);
        });

        // TODO: blockAuthenticatedAccess
    });

    describe('handle404', () => {
        afterEach(() => {
            $window.CORDOVA = undefined;
        });

        it('should redirect to homePath in Cordova', () => {
            const $location = $injector.get('$location');
            jest.spyOn($location, 'url').mockImplementation(() => {});
            $window.CORDOVA = {};
            RouteResolvers.handle404();
            expect($location.url).toHaveBeenCalledWith($rootScope.homePath);
        });

        it('should redirect to /404 in Desktop', () => {
            const $location = $injector.get('$location');

            jest.spyOn($location, 'url').mockImplementation(() => {});
            RouteResolvers.handle404();
            expect($window.location.href).toEqual('/404');
        });
    });

    describe('redirectToDefaultCareersSection', () => {
        beforeEach(() => {
            stubWaitForAuthCallComplete('learner');
        });

        describe('with section=connections', () => {
            beforeEach(() => {
                const $route = $injector.get('$route');
                $route.current.pathParams = {
                    section: 'connections',
                };
            });

            it('should work if you have access', () => {
                Object.defineProperty($rootScope.currentUser, 'hasCareersNetworkAccess', {
                    value: true,
                });
                executeResolver(RouteResolvers.redirectToDefaultCareersSection, true);
            });

            it("should redirect if you don't have any access", () => {
                Object.defineProperty($rootScope.currentUser, 'hasCareersNetworkAccess', {
                    value: false,
                });
                executeResolver(RouteResolvers.redirectToDefaultCareersSection, false, {
                    redirect: '/careers/apply',
                });
            });
        });

        describe('with section=card', () => {
            beforeEach(() => {
                const $route = $injector.get('$route');
                $route.current.pathParams = {
                    section: 'card',
                };
            });

            it('should work if you have applied or have access', () => {
                Object.defineProperty($rootScope.currentUser, 'hasAppliedOrHasAccessToCareers', {
                    value: true,
                });
                executeResolver(RouteResolvers.redirectToDefaultCareersSection, true);
            });

            it("should redirect to apply if you haven't applied or don't have access", () => {
                Object.defineProperty($rootScope.currentUser, 'hasAppliedOrHasAccessToCareers', {
                    value: false,
                });
                executeResolver(RouteResolvers.redirectToDefaultCareersSection, false, {
                    redirect: '/careers/apply',
                });
            });
        });

        describe('with section=apply', () => {
            beforeEach(() => {
                const $route = $injector.get('$route');
                $route.current.pathParams = {
                    section: 'apply',
                };
            });

            it("should work if you don't have any access", () => {
                Object.defineProperty($rootScope.currentUser, 'hasAppliedOrHasAccessToCareers', {
                    value: false,
                });
                executeResolver(RouteResolvers.redirectToDefaultCareersSection, true);
            });

            it('should redirect to card you have basic access', () => {
                Object.defineProperty($rootScope.currentUser, 'hasAppliedOrHasAccessToCareers', {
                    value: true,
                });
                executeResolver(RouteResolvers.redirectToDefaultCareersSection, false, {
                    redirect: '/careers/card',
                });
            });
        });

        describe('with no section provided', () => {
            it('should go to welcome if career profile is active but user has not seen welcome', () => {
                Object.defineProperty($rootScope.currentUser, 'hasActiveCareerProfile', {
                    value: true,
                });
                Object.defineProperty($rootScope.currentUser, 'hasCareersNetworkAccess', {
                    value: true,
                });
                $rootScope.currentUser.has_seen_careers_welcome = false;
                executeResolver(RouteResolvers.redirectToDefaultCareersSection, false, {
                    redirect: '/careers/welcome',
                });
            });

            it('should go to connections if career profile is active and user has seen welcome', () => {
                Object.defineProperty($rootScope.currentUser, 'hasActiveCareerProfile', {
                    value: true,
                });
                Object.defineProperty($rootScope.currentUser, 'hasCareersNetworkAccess', {
                    value: true,
                });
                $rootScope.currentUser.has_seen_careers_welcome = true;
                executeResolver(RouteResolvers.redirectToDefaultCareersSection, false, {
                    redirect: '/careers/connections',
                });
            });

            it('should go to edit if career profile is incomplete and is in the network', () => {
                Object.defineProperty($rootScope.currentUser, 'hasActiveCareerProfile', {
                    value: false,
                });
                Object.defineProperty($rootScope.currentUser, 'hasCompleteCareerProfile', {
                    value: false,
                });
                Object.defineProperty($rootScope.currentUser, 'hasCareersNetworkAccess', {
                    value: true,
                });
                executeResolver(RouteResolvers.redirectToDefaultCareersSection, false, {
                    redirect: '/careers/card',
                });
            });

            it("should go to card if they have applied to the network, but aren't yet in", () => {
                Object.defineProperty($rootScope.currentUser, 'hasActiveCareerProfile', {
                    value: false,
                });
                Object.defineProperty($rootScope.currentUser, 'hasAppliedOrHasAccessToCareers', {
                    value: true,
                });
                Object.defineProperty($rootScope.currentUser, 'hasCareersNetworkAccess', {
                    value: false,
                });
                executeResolver(RouteResolvers.redirectToDefaultCareersSection, false, {
                    redirect: '/careers/card',
                });
            });

            it('should go to apply if user does not have access to careers yet', () => {
                Object.defineProperty($rootScope.currentUser, 'hasActiveCareerProfile', {
                    value: false,
                });
                Object.defineProperty($rootScope.currentUser, 'hasCompleteCareerProfile', {
                    value: false,
                });
                Object.defineProperty($rootScope.currentUser, 'hasAppliedOrHasAccessToCareers', {
                    value: false,
                });
                executeResolver(RouteResolvers.redirectToDefaultCareersSection, false, {
                    redirect: '/careers/apply',
                });
            });
        });
    });

    function stubWaitForAuthCallComplete(
        userRole,
        config,
        options = {
            needsToProvideCohortBillingInfo: false,
            registeredForClass: true,
        },
    ) {
        if (userRole) {
            SpecHelper.stubCurrentUser(userRole);
            if (userRole === 'learner') {
                $rootScope.currentUser.has_seen_welcome = true;
            } else if (userRole === 'pendingHiringManager') {
                $rootScope.currentUser.hiring_application = {
                    status: 'pending',
                };
            } else if (userRole === 'acceptedHiringManager') {
                $rootScope.currentUser.hiring_application = {
                    status: 'accepted',
                };
            }

            jest.spyOn($rootScope.currentUser, 'shouldVisitApplicationStatus', 'get').mockReturnValue(
                options.shouldVisitApplicationStatus,
            );
        } else {
            $rootScope.currentUser = undefined;
        }

        SpecHelper.stubConfig(angular.extend({}, config));
        $rootScope.authCallComplete = true;

        jest.spyOn(ConfigFactory, 'getConfig').mockReturnValue($q.when(ConfigFactory._config));
    }

    describe('hasAppropriateAccess', () => {
        it('should watch for changes', () => {
            // setup spies an stuff.  Note that the particular `hasAccess` properties
            // used here makes no difference (so long as we don't use ones that have strange
            // side effects).  They all get passed throught he hasAppropriateAccess
            stubWaitForAuthCallComplete();
            SpecHelper.stubCurrentUser();
            SpecHelper.stubConfig();
            let hasHiringManagerAccess = true;
            let hasHiringChoosePlanAccess = false;
            jest.spyOn($rootScope.currentUser, 'hasHiringManagerAccess', 'get').mockImplementation(
                () => hasHiringManagerAccess,
            );
            jest.spyOn($rootScope.currentUser, 'hasHiringChoosePlanAccess', 'get').mockImplementation(
                () => hasHiringChoosePlanAccess,
            );
            jest.spyOn($rootScope, 'goHome').mockImplementation();

            // Simulate going to a page that requires hasHiringManagerAccess
            $rootScope.$broadcast('$routeChangeStart');
            RouteResolvers.hasHiringManagerAccess();
            $timeout.flush();

            // When access changes, the learner is sent home
            hasHiringManagerAccess = false;
            hasHiringChoosePlanAccess = true;
            $rootScope.$digest();

            // Since `hasHiringManagerAccess` is false now, `goHome` gets called
            expect($rootScope.goHome).toHaveBeenCalled();
            $rootScope.goHome.mockClear();

            // simulate being sent to a new page that requires hasHiringChoosePlanAccess
            $rootScope.$broadcast('$routeChangeStart');
            RouteResolvers.hasHiringChoosePlanAccess();
            $timeout.flush();

            // ensure that the listener on hasHiringManagerAccess was turned off, so
            // goHome was never called
            expect($rootScope.goHome).not.toHaveBeenCalled();
        });
    });

    describe('waitForAuthCallComplete', () => {
        it('should cancel routing when offlineModeManager.resolveRoute is rejected', () => {
            SpecHelper.stubConfig();
            offlineModeManager.resolveRoute.mockReturnValue($q.reject());
            let rejected;

            // Nothing special about has hasLearnerAccess.  Anything that hits
            // waitForAuthCallComplete will call offlineModeManager.resolveRoute
            RouteResolvers.hasLearnerAccess().catch(() => {
                rejected = true;
            });
            $timeout.flush();
            expect(rejected).toBe(true);
            expect(offlineModeManager.resolveRoute).toHaveBeenCalled();
        });
    });

    function executeResolver(resolverPromise, resolved, response, expectDashboard) {
        resolverPromise()
            // eslint-disable-next-line no-shadow
            .then(response => {
                promiseResponse = response;
                promiseResolved = true;
            })
            // eslint-disable-next-line no-shadow
            .catch(response => {
                promiseResponse = response;
                promiseResolved = false;
            });

        $rootScope.$digest();

        expect(promiseResolved).toBe(resolved);
        expect(promiseResponse).toEqual(response);

        if (expectDashboard) {
            expect(LearnerContentCache.ensureStudentDashboard).toHaveBeenCalled();
        }
    }
});
