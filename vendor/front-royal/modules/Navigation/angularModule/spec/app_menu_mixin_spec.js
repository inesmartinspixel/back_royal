import 'AngularSpecHelper';
import 'Careers/angularModule/spec/careers_spec_helper';
import 'Navigation/angularModule';

describe('Navigation::AppMenuMixin', () => {
    let $injector;
    let SpecHelper;
    let CareersSpecHelper;
    let $rootScope;
    let scope;
    let AppMenuMixin;
    let careersNetworkViewModel;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Navigation', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            CareersSpecHelper = $injector.get('CareersSpecHelper');
            $rootScope = $injector.get('$rootScope');
            AppMenuMixin = $injector.get('Navigation.AppMenuMixin');
        });

        SpecHelper.stubCurrentUser();
        SpecHelper.stubConfig();
        scope = $rootScope.$new();

        careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
        AppMenuMixin.onLink(scope);
        scope.$digest();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should update numCareersNotifications', () => {
        scope.careersNetworkViewModel = careersNetworkViewModel;

        careersNetworkViewModel.connectionsNumNotifications = 5;
        $rootScope.currentUser.num_recommended_positions = 5;
        scope.$digest();
        expect(scope.numCareersNotifications).toBe(10);
        scope.careersNetworkViewModel.connectionsNumNotifications = 6;
        scope.$digest();
        expect(scope.numCareersNotifications).toBe(11);
    });
});
