import 'AngularSpecHelper';
import 'Navigation/angularModule';
import 'Users/angularModule/spec/_mock/fixtures/users';

describe('Navigation::NavigationModuleSpec', () => {
    let SpecHelper;
    let $rootScope;
    let User;
    let $window;
    let ErrorLogService;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Navigation', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $rootScope = $injector.get('$rootScope');
            $window = $injector.get('$window');
            ErrorLogService = $injector.get('ErrorLogService');

            User = $injector.get('User');
            $injector.get('UserFixtures');
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
        $window.CORDOVA = false;
    });

    describe('homePath', () => {
        it('should be root if no currentUser', () => {
            expect($rootScope.homePath).toBe('/');
        });

        it('should be onboarding if no current user on cordova', () => {
            $window.CORDOVA = true;
            expect($rootScope.homePath).toBe('/onboarding/hybrid');
        });

        it('should be /dashboard if currentUser is a learner', () => {
            $rootScope.currentUser = User.fixtures.getInstance({
                hiring_application: null,
            });

            expect($rootScope.homePath).toBe('/dashboard');
        });

        describe('hiring', () => {
            beforeEach(() => {
                SpecHelper.stubCurrentUser();
                jest.spyOn($rootScope.currentUser, 'defaultsToHiringExperience', 'get').mockReturnValue(true);
            });

            it('should be /hiring/plan if currentUser hasHiringChoosePlanAccess', () => {
                jest.spyOn($rootScope.currentUser, 'hasHiringChoosePlanAccess', 'get').mockReturnValue(true);
                expect($rootScope.homePath).toBe('/hiring/plan');
            });

            it('should be /hiring/browse-candidates if currentUser hasSourcingUpsellAccess and not pay-per-post', () => {
                jest.spyOn($rootScope.currentUser, 'hasSourcingUpsellAccess', 'get').mockReturnValue(true);
                jest.spyOn($rootScope.currentUser, 'usingPayPerPostHiringPlan', 'get').mockReturnValue(false);
                expect($rootScope.homePath).toBe('/hiring/browse-candidates');
            });

            it('should be /hiring/positions if currentUser hasSourcingUpsellAccess but is a pay-per-post user', () => {
                jest.spyOn($rootScope.currentUser, 'hasSourcingUpsellAccess', 'get').mockReturnValue(true);
                jest.spyOn($rootScope.currentUser, 'usingPayPerPostHiringPlan', 'get').mockReturnValue(true);
                jest.spyOn($rootScope.currentUser, 'hasHiringPositionsAccess', 'get').mockReturnValue(true);
                expect($rootScope.homePath).toBe('/hiring/positions');
            });

            it('should be /hiring/positions if currentUser hasHiringPositionsAccess and not choosePlan or sourcingUpsell', () => {
                jest.spyOn($rootScope.currentUser, 'hasHiringChoosePlanAccess', 'get').mockReturnValue(false);
                jest.spyOn($rootScope.currentUser, 'hasSourcingUpsellAccess', 'get').mockReturnValue(false);
                jest.spyOn($rootScope.currentUser, 'hasHiringPositionsAccess', 'get').mockReturnValue(true);
                expect($rootScope.homePath).toBe('/hiring/positions');
            });

            it('should be /hiring/disabled if currentUser.hasDisabledHiringManagerAccess', () => {
                jest.spyOn($rootScope.currentUser, 'hasDisabledHiringManagerAccess', 'get').mockReturnValue(true);
                jest.spyOn($rootScope.currentUser, 'defaultsToHiringExperience', 'get').mockReturnValue(true);
                expect($rootScope.homePath).toBe('/hiring/disabled');
            });

            it('should be /hiring/sorry if currentUser has a rejected hiring-application', () => {
                jest.spyOn($rootScope.currentUser, 'hasRejectedHiringManagerAccess', 'get').mockReturnValue(true);
                jest.spyOn($rootScope.currentUser, 'defaultsToHiringExperience', 'get').mockReturnValue(true);
                expect($rootScope.homePath).toBe('/hiring/sorry');
            });

            it('should be /settings and log an error if we somehow do not match a route for hiring manager', () => {
                jest.spyOn($rootScope.currentUser, 'hasHiringChoosePlanAccess', 'get').mockReturnValue(false);
                jest.spyOn($rootScope.currentUser, 'hasSourcingUpsellAccess', 'get').mockReturnValue(false);
                jest.spyOn($rootScope.currentUser, 'hasHiringPositionsAccess', 'get').mockReturnValue(false);
                jest.spyOn(ErrorLogService, 'notify');
                expect($rootScope.homePath).toBe('/settings');
                expect(ErrorLogService.notify).toHaveBeenCalledWith('Unknown homePath for hiring manager');
            });

            it('should be /settings/billing if a hiring manager is past due', () => {
                jest.spyOn($rootScope.currentUser, 'noHiringManagerAccessDueToPastDuePayment', 'get').mockReturnValue(
                    true,
                );
                jest.spyOn($rootScope.currentUser, 'defaultsToHiringExperience', 'get').mockReturnValue(true);
                expect($rootScope.homePath).toBe('/settings/billing');
            });
        });
    });
});
