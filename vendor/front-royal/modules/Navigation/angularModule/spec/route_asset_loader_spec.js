import 'AngularSpecHelper';
import 'Navigation/angularModule';

describe('Navigation::RouteAssetLoader', () => {
    let RouteAssetLoader;
    let $q;
    let $ocLazyLoad;
    let $rootScope;
    let ConfigFactory;
    let Locale;
    let origManifest;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Navigation', 'SpecHelper');

        angular.mock.inject($injector => {
            RouteAssetLoader = $injector.get('Navigation.RouteAssetLoader');
            $q = $injector.get('$q');
            $ocLazyLoad = $injector.get('$ocLazyLoad');
            $rootScope = $injector.get('$rootScope');
            ConfigFactory = $injector.get('ConfigFactory');
            Locale = $injector.get('Locale');

            origManifest = window.webpackManifest;
        });
    });

    afterEach(() => {
        window.webpackManifest = origManifest;
    });

    describe('loadAdminDependencies', () => {
        it('should loadEditorDependencies, then loadReportsDependencies, and then load admin deps', () => {
            window.webpackManifest = {
                'admin.css': '/assets/styles/admin.css',
                'admin.js': '/assets/scripts/admin.js',
                'vendors~admin.js': '/assets/scripts/vendors~admin.js',
                'vendors~front-royal~admin.js': '/assets/scripts/vendors~front-royal~admin.js',
            };
            setupSpies();

            jest.spyOn(RouteAssetLoader, 'loadEditorDependencies').mockReturnValue($q.when());
            jest.spyOn(RouteAssetLoader, 'loadReportsDependencies').mockReturnValue($q.when());

            RouteAssetLoader.loadAdminDependencies();

            expect(RouteAssetLoader.loadEditorDependencies).toHaveBeenCalled();
            $rootScope.$digest();

            expect(RouteAssetLoader.loadReportsDependencies).toHaveBeenCalled();
            $rootScope.$digest();

            expect($ocLazyLoad.load).toHaveBeenCalledWith({
                serie: true,
                files: [
                    'assets/styles/admin.css',
                    'assets/styles/fullcalendar.min.css',
                    'assets/scripts/admin.js',
                    'assets/scripts/vendors~admin.js',
                ],
            });
        });
    });

    describe('loadEditorDependencies', () => {
        it('should load editor vendors deps, loadArabicFontDependencies, and then load the rest of the editor deps', () => {
            window.webpackManifest = {
                'editor.css': '/assets/styles/editor.css',
                'editor.js': '/assets/scripts/editor.js',
                'vendors~editor.js': '/assets/scripts/vendors~editor.js',
                'vendors~editor~front-royal.js': '/assets/scripts/vendors~editor~front-royal.js',
            };
            setupSpies();
            jest.spyOn(RouteAssetLoader, 'loadArabicFontDependencies').mockReturnValue($q.when());

            RouteAssetLoader.loadEditorDependencies();

            expect($ocLazyLoad.load).toHaveBeenCalledWith({
                serie: true,
                files: [
                    'assets/styles/editor.css',
                    'assets/mathjax/MathJax.js?config=TeX-AMS_CHTML-full&delayStartupUntil=configured',
                    'assets/scripts/editor.js',
                    'assets/scripts/vendors~editor.js',
                ],
            });
            expect(RouteAssetLoader.loadArabicFontDependencies).toHaveBeenCalled();
        });
    });

    it('should support loading global dependencies', () => {
        window.webpackManifest = {
            'front-royal.css': '/assets/styles/front-royal.css',
        };
        const deferred = setupSpies();

        RouteAssetLoader.loadGlobalDependencies();

        expect($ocLazyLoad.load).toHaveBeenCalledWith({
            files: ['assets/styles/front-royal.css'],
        });

        deferred.resolve();
        $rootScope.$digest();

        expect($ocLazyLoad.load).toHaveBeenCalledWith({
            files: ['assets/fontawesome/v5/css/all.min.css'],
        });
    });

    it('should support loading global dependencies when in the Arabic locale', () => {
        window.webpackManifest = {
            'front-royal.css': '/assets/styles/front-royal.css',
        };
        const deferred = setupSpies();
        Locale.activeCode = 'ar';
        jest.spyOn(RouteAssetLoader, 'loadArabicFontDependencies').mockReturnValue($q.when());

        RouteAssetLoader.loadGlobalDependencies();

        expect(RouteAssetLoader.loadArabicFontDependencies).toHaveBeenCalled();

        expect($ocLazyLoad.load).toHaveBeenCalledWith({
            files: ['assets/styles/front-royal.css'],
        });

        deferred.resolve();
        $rootScope.$digest();

        expect($ocLazyLoad.load).toHaveBeenCalledWith({
            files: ['assets/fontawesome/v5/css/all.min.css'],
        });
    });

    describe('loadReportsDependencies', () => {
        it('should load reports vendors deps, addPartAndRefreshTranslationTables, and then load the rest of the reports deps', () => {
            window.webpackManifest = {
                'reports.css': '/assets/styles/reports.css',
                'reports.js': '/assets/scripts/reports.js',
                'vendors~reports.js': '/assets/scripts/vendors~reports.js',
                'admin~editor~front-royal~reports.js': '/assets/scripts/admin~editor~front-royal~reports.js',
            };
            setupSpies();

            RouteAssetLoader.loadReportsDependencies();

            expect($ocLazyLoad.load).toHaveBeenCalledWith({
                serie: true,
                files: ['assets/styles/reports.css', 'assets/scripts/reports.js', 'assets/scripts/vendors~reports.js'],
            });
        });
    });

    it('should support loading google places dependencies', () => {
        const deferred = setupSpies();

        const googlePlacesApiKey = '1337';
        jest.spyOn(ConfigFactory, 'getConfig').mockReturnValue(
            $q.when({
                google_api_key: '1337',
            }),
        );

        RouteAssetLoader.loadGooglePlacesDependencies();

        deferred.resolve();
        $rootScope.$digest();

        expect($ocLazyLoad.load).toHaveBeenCalledWith({
            files: [`js!https://maps.googleapis.com/maps/api/js?libraries=places&key=${googlePlacesApiKey}`],
        });
        jest.spyOn(RouteAssetLoader, 'loadGooglePlacesDependencies').mockReturnValue($q.when());
    });

    it('should support loading stripe checkout dependencies', () => {
        window.SILENCE_LOAD_STRIPE_CHECKOUT_ERROR = true;
        const deferred = setupSpies();
        RouteAssetLoader.loadStripeCheckoutDependencies();

        deferred.resolve();
        $rootScope.$digest();

        expect($ocLazyLoad.load).toHaveBeenCalledWith({
            files: ['js!https://checkout.stripe.com/checkout.js'],
        });
        jest.spyOn(RouteAssetLoader, 'loadStripeCheckoutDependencies').mockReturnValue($q.when());
    });

    it('should support loading Arabic font dependencies', () => {
        window.webpackManifest = { 'arabic-font-families.css': '/assets/styles/arabic-font-families.css' };
        const deferred = setupSpies();

        RouteAssetLoader.loadArabicFontDependencies();
        expect($ocLazyLoad.load).toHaveBeenCalledWith({
            files: ['assets/styles/arabic-font-families.css'],
        });

        deferred.resolve();
        $rootScope.$digest();
    });

    describe('_getFilePathsFromWebpackManifestForLazyLoading', () => {
        it('should return the file paths from the webpack manifest for the specified module and file extension, excluding common and front-royal file paths', () => {
            window.webpackManifest = {
                'admin.css': '/assets/styles/admin.css',
                'admin.js': '/assets/scripts/admin.js',
                'vendors~admin.js': '/assets/scripts/vendors~admin.js',
                'admin~editor~front-royal~reports.js': '/assets/scripts/admin~editor~front-royal~reports.js',
            };
            const paths = RouteAssetLoader._getFilePathsFromWebpackManifestForLazyLoading('admin', 'js');

            // `common` and `front-royal` dependencies are always added into the HTML as script tags as
            // part of the initial page render by Rails, so we shouldn't see them loaded up here again.
            expect(paths).toEqual(['assets/scripts/admin.js', 'assets/scripts/vendors~admin.js']);
        });
    });

    function setupSpies() {
        const deferred = $q.defer();
        jest.spyOn($ocLazyLoad, 'load').mockReturnValue(deferred.promise);
        return deferred;
    }
});
