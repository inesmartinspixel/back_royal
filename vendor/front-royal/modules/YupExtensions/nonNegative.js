import * as Yup from 'yup';

Yup.addMethod(Yup.number, 'nonNegative', function (message) {
    return this.test('non-negative', message, function (value) {
        return !value || value > 0 || this.createError(message);
    });
});
