import angularModule from 'FrontRoyalConfig/angularModule/scripts/front_royal_config_module';
// iguana service wrapper class
// currently only opening up /api/users.json which returns info about the current user
angularModule.factory('Config', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');

        return Iguana.subclass(function () {
            this.setCollection('config');
            this.alias('Config');
            return {
                segmentioWriteKey() {
                    return this.segmentio_key;
                },
                segmentioEnabled() {
                    return this.segmentio_disabled !== 'true';
                },
                httpQueueEventsEnabled() {
                    return this.http_queue_events_enabled === 'true';
                },
                forceIdpInitiatedLoginUrlFor(provider) {
                    return this.force_idp_initiated_login_urls[provider];
                },
                defaultPageMetadata() {
                    return this.default_page_metadata;
                },
                premiumCouponPrefixes() {
                    return this.premium_coupon_prefixes || [];
                },
                disable3xImageSupport() {
                    return this.disable_3x_image_support === 'true';
                },
                embaComingSoon() {
                    return this.emba_coming_soon === 'true' || this.emba_coming_soon === true;
                },
                freeMBAGroups() {
                    if (this.free_mba_groups) {
                        return this.free_mba_groups.split(',');
                    }
                    return ['SMARTER', 'MBA'];
                },
                skipCohortApplicationForm() {
                    return this.skip_cohort_application_form === 'true';
                },
                gdprAppliesToUser() {
                    return this.gdpr_user === 'true';
                },
                appEnvType() {
                    const appEnvName = this.app_env_name || '';
                    if (appEnvName.match(/production/i)) {
                        return 'production';
                    }
                    if (appEnvName.match(/staging/i)) {
                        return 'staging';
                    }
                    return 'development';
                },
                isQuantic() {
                    return this.is_quantic === 'true';
                },
                canonicalDomain() {
                    return this.isQuantic() ? this.quantic_domain : this.smartly_domain;
                },
                generateCanonicalUrl(canonicalUrl) {
                    // The canonicalUrl may contain references to Quantic, so we dynamically
                    // replace those references to be contextual for Smartly users since it
                    // would be weird if the brand name in the canonicalDomain was different
                    // than the brand name in the canonicalUrl.
                    canonicalUrl = this.isQuantic() ? canonicalUrl : canonicalUrl.replace('quantic', 'smartly');
                    if (canonicalUrl.indexOf('/') === 0) {
                        canonicalUrl = `https://${this.canonicalDomain()}${canonicalUrl}`;
                    } else {
                        canonicalUrl = `https://${this.canonicalDomain()}/${canonicalUrl}`;
                    }
                    return canonicalUrl;
                },
                zipRecruiterApiKeyForCountry(countryCode, useFallback) {
                    // Unfortunately, ZipRecruiter issues API keys based on
                    // country. If the ZipSearch endpoint is hit with a location
                    // outside of the country specified by the API key, the
                    // ZipSearch API will return an empty result.
                    let keyByCountry;
                    if (countryCode) {
                        keyByCountry = this[`ziprecruiter_api_key_${countryCode.toLowerCase()}`];
                    }

                    return useFallback ? keyByCountry || this.ziprecruiter_api_key_us : keyByCountry;
                },
                zipRecruiterSearchActivated() {
                    return this.activate_ziprecruiter_search === 'true';
                },
            };
        });
    },
]);
