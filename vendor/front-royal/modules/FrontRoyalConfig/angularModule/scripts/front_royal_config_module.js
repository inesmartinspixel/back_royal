import 'Injector/angularModule';

export default angular.module('FrontRoyal.Config', ['Iguana', 'Injector']);
