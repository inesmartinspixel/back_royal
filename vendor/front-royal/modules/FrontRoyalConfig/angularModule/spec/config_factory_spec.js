import { DisconnectedError } from 'FrontRoyalStore';
import 'OfflineMode/angularModule';
import 'AngularSpecHelper';
import 'FrontRoyalConfig/angularModule';

describe('ConfigFactory', () => {
    let $injector;
    let SpecHelper;
    let ConfigFactory;
    let Config;
    let frontRoyalStore;
    let $q;
    let now;
    let DialogModal;
    const configResponse = { prop: 'value' };
    const requestTime = 2000; // we mock out the request to take 2 seconds
    const serverTimestamp = 42;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Config', 'SpecHelper');

        angular.mock.module($provide => [
            '$injector',
            _$injector => {
                $q = _$injector.get('$q');

                const offlineModeManager = {
                    rejectInOfflineMode: jest.fn().mockImplementation(fn => fn()),
                };
                $provide.value('offlineModeManager', offlineModeManager);

                frontRoyalStore = {
                    waitForEnabled: jest.fn().mockReturnValue($q.when()),
                    retryAfterHandledError: jest.fn(),
                    getConfig: jest.fn(),
                };
                $provide.value('frontRoyalStore', frontRoyalStore);

                DialogModal = {
                    showFatalError: jest.fn(),
                };
                $provide.value('DialogModal', DialogModal);
            },
        ]);

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                ConfigFactory = $injector.get('ConfigFactory');
                Config = $injector.get('Config');
                frontRoyalStore = $injector.get('frontRoyalStore');

                jest.spyOn(ConfigFactory, '_now').mockImplementation(() => now);

                now = 0;
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('getConfig', () => {
        it('should load from API', () => {
            Config.expect('index').returns([configResponse]).returnsMeta({ server_timestamp: serverTimestamp });
            assertGetConfig();
        });

        it('should reload from API if we cannot set the server time', () => {
            const $timeout = $injector.get('$timeout');
            let requestCount = 0;
            jest.spyOn(Config, 'index').mockImplementation(() => {
                if (requestCount > 1) {
                    throw new Error('Infinite loop');
                }

                // The first request takes 61 seconds, so ...
                if (requestCount === 0) {
                    now += 61000;
                    requestCount += 1;
                    return $q.resolve({
                        result: [Config.new(configResponse)],
                        meta: { server_timestamp: 1 },
                    });
                }

                // It is rejected and we make a second request,
                // which complets fast enough
                now += requestTime;
                requestCount += 1;
                return $q.resolve({
                    result: [Config.new(configResponse)],
                    meta: { server_timestamp: serverTimestamp },
                });
            });

            let config;
            ConfigFactory.getConfig().then(_ => {
                config = _;
            });
            $timeout.flush();
            expect(Config.index.mock.calls.length).toEqual(2);
            assertConfig(config);
        });

        it('should capture disconnected error and load from store', () => {
            const storedConfig = {
                ...configResponse,

                // hardcode this to what a previous API call would have
                // set it to
                serverClientTimeOffset: now + requestTime / 2 - 1000 * serverTimestamp,
            };

            frontRoyalStore.getConfig.mockReturnValue(
                // toArray normally returns a native promise, but in order to flush
                // the promises, we need to use $q here
                $q.resolve(storedConfig),
            );
            Config.expect('index').fails(new DisconnectedError());
            assertGetConfig();
        });

        it('should error if cannot load config from api or store', () => {
            frontRoyalStore.getConfig.mockReturnValue(
                // toArray normally returns a native promise, but in order to flush
                // the promises, we need to use $q here
                $q.resolve(null),
            );
            Config.expect('index').fails(new DisconnectedError());
            try {
                getConfig();
            } catch (err) {
                expect(err.message).toEqual('Could not load config from either api or store');
            }
            expect(DialogModal.showFatalError).toHaveBeenCalled();
        });

        function assertGetConfig() {
            const config = getConfig();
            assertConfig(config);

            // `now` is now fixed at the time the request returned,
            // which is requestTime/2 after the server timestamp that came
            // back in the request
            expect(ConfigFactory.getServerTimestamp()).toEqual(serverTimestamp * 1000 + requestTime / 2);
        }

        function assertConfig(config) {
            expect(config).not.toBeUndefined();
            // This is testing the setting of _config, which
            // is a private property, which we would not
            // normally do, but having this specs allows us to
            // test getSync below by manually setting _config
            expect(ConfigFactory._config).toBe(config);
            expect(config.isA(Config)).toBe(true);
            expect(config.prop).toEqual(configResponse.prop);
        }
    });

    describe('getSync', () => {
        const config = {};
        // assertConfig above ensures that _config is set, so we can
        // set it directly here for these specs
        it('should return config if initialized', () => {
            ConfigFactory._config = config;
            expect(ConfigFactory.getSync()).toBe(config);
        });

        it('should error if not initialized', () => {
            ConfigFactory._config = null;
            expect(() => ConfigFactory.getSync()).toThrow('');
        });

        it('should not error if not initialized and withoutThrow=true', () => {
            ConfigFactory._config = null;
            expect(ConfigFactory.getSync(true)).toEqual(null);
        });
    });

    describe('frontRoyalStore persistence', () => {
        let put;
        let $rootScope;

        beforeEach(() => {
            $rootScope = $injector.get('$rootScope');
            put = jest.fn();
            frontRoyalStore.retryAfterHandledError.mockImplementation(fn =>
                fn({
                    configRecords: { put },
                }),
            );
        });

        it('should store config whenever the store is enabled or the config changes', () => {
            // When config is first loaded, it should be stored
            frontRoyalStore.enabled = true;
            Config.expect('index').returns([configResponse]).returnsMeta({ server_timestamp: serverTimestamp });
            getConfig();
            assertConfigStored();

            // When config changes, it should be stored
            ConfigFactory._config = Config.new({ some: 'newConfig' });
            assertConfigStored();

            // When enabled is toggled on and off, config should be stored
            frontRoyalStore.enabled = false;
            $rootScope.$digest();
            frontRoyalStore.enabled = true;
            assertConfigStored();
        });

        function assertConfigStored() {
            $rootScope.$digest();
            expect(put).toHaveBeenCalledWith({
                // We always use the same id so that this
                // always replaces the single config object
                id: 'frontRoyalConfig',
                serverClientTimeOffset: ConfigFactory._serverTime.serverClientTimeOffset,
                ...ConfigFactory._config.asJson(),
            });
            put.mockReset();
        }
    });

    describe('getServerTimestamp', () => {
        // this is tested up in the getConfig block
    });

    function getConfig() {
        let config;
        ConfigFactory.getConfig().then(_ => {
            config = _;
        });
        now += requestTime; // mock out the amount of time it takes to make the request
        Config.flush('index');
        return config;
    }
});
