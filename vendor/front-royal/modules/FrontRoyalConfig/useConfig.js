import { useContext, useState, useEffect } from 'react';
import AngularContext from 'AngularContext';

export default function useConfig() {
    const [config, setConfig] = useState();

    const $injector = useContext(AngularContext);

    useEffect(() => {
        const ConfigFactory = $injector.get('ConfigFactory');
        ConfigFactory.getConfig().then(setConfig);
    }, [$injector]);

    return config;
}
