import { generateGuid } from 'guid';
import Auid from 'Auid';
import fetch from 'FrontRoyalFetch';
import { ensurePageLoadId } from './PageLoadId';

export default function logTinyEvent(type, obj, serverTime) {
    const clientTimestamp = Date.now();
    const date = new Date(clientTimestamp);

    const extra = {
        log_to_customerio: false,
        event_type: type,
        server_timestamp: serverTime.getServerTimestamp(date.getTime()) / 1000,
        id: generateGuid(),
        cordova: false,
        page_load_id: ensurePageLoadId(),
        url: window.location.pathname,
        buffered_time: 0,
        client_utc_timestamp: clientTimestamp / 1000,
        client_local_time: [
            date.getFullYear(),
            date.getMonth(),
            date.getDate(),
            date.getHours(),
            date.getMinutes(),
            date.getSeconds(),
            date.getMilliseconds(),
        ],
        client_utc_time: [
            date.getUTCFullYear(),
            date.getUTCMonth(),
            date.getUTCDate(),
            date.getUTCHours(),
            date.getUTCMinutes(),
            date.getUTCSeconds(),
            date.getUTCMilliseconds(),
        ],
        client_offset_from_utc: date.getTimezoneOffset() / 60,
    };

    const event = {
        ...obj,
        ...extra,
    };

    // FIXME: remove the meta once we've made sense of https://sentry.io/organizations/pedago/issues/1524364394/?project=1491374&referrer=trello_plugin
    return fetch(`${window.ENDPOINT_ROOT}/api/event_bundles.json`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            auid: Auid.ensure(),
        },
        body: JSON.stringify({
            record: {
                events: [event],
            },
            meta: {
                debug: {
                    obj,
                    extra,
                },
            },
        }),
    });
}
