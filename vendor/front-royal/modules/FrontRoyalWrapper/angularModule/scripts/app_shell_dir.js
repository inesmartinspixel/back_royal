import angularModule from 'FrontRoyalWrapper/angularModule/scripts/front_royal_wrapper_module';
import casperMode from 'casperMode';

import cacheAngularTemplate from 'cacheAngularTemplate';
import template from '../views/app_shell.html';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('appShell', [
    '$injector',
    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        let AppHeaderViewModel;

        return {
            restrict: 'E',
            scope: {
                resolvingRouteTakingSomeTime: '<',
                routeAnimationClasses: '&',
            },
            templateUrl,
            link(scope) {
                Object.defineProperty(scope, 'inPlayer', {
                    get() {
                        return AppHeaderViewModel && AppHeaderViewModel.playerViewModel !== undefined;
                    },
                });

                Object.defineProperty(scope, 'signingOut', {
                    get() {
                        return $rootScope.signingOut;
                    },
                });

                scope.showApp = true;
                AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');

                const listenerCancelers = [];

                listenerCancelers.push(
                    $rootScope.$watch('currentUser', () => {
                        scope.currentUser = $rootScope.currentUser;
                    }),
                );

                scope.$on('$destroy', () => {
                    listenerCancelers.forEach(func => {
                        func();
                    });
                });

                if (casperMode()) {
                    scope.$watch(
                        () => casperMode() === true && $rootScope.casperRender === false,
                        casperHide => {
                            scope.casperHide = casperHide;
                        },
                    );
                }
            },
        };
    },
]);
