import 'AngularSpecHelper';
import casperMode from 'casperMode';
import 'FrontRoyalWrapper/angularModule';

jest.mock('casperMode', () => jest.fn());

describe('FrontRoyal::Wrapper::AppShellDirSpec', () => {
    let SpecHelper;
    let elem;
    let scope;
    let $rootScope;
    let AppHeaderViewModel;

    function doInject() {
        // FIXME: once we've modularized the AppHeaderViewModel, we could remove this.
        angular.mock.module($provide => {
            AppHeaderViewModel = {};
            $provide.value('Navigation.AppHeader.AppHeaderViewModel', AppHeaderViewModel);
            $provide.value('User', { index: jest.fn() });
        });

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $rootScope = $injector.get('$rootScope');
            const $q = $injector.get('$q');

            const User = $injector.get('User');
            User.index.mockReturnValue(
                $q.when({
                    result: [
                        {
                            id: 12345,
                            email: 'stuff@things.com',
                        },
                    ],
                    meta: {},
                }),
            );

            SpecHelper.stubConfig();
        });
    }

    beforeEach(() => {
        casperMode.mockReset();
        angular.mock.module('FrontRoyal.Wrapper', 'SpecHelper');
    });

    it('should contain the appropriate top-level elements', () => {
        doInject();

        render();
        SpecHelper.expectElement(elem, '#sp-page');
    });

    it('should not include mobile menu if in player', () => {
        doInject();

        Object.defineProperty(AppHeaderViewModel, 'playerViewModel', {
            value: true,
            configurable: true,
        });
        render();
        expect(scope.inPlayer).toBe(true);
        SpecHelper.expectNoElement(elem, 'app-menu-mobile');
    });

    describe('casperHide', () => {
        it('should not hide if casperMode is false', () => {
            casperMode.mockReturnValue(false);
            doInject();

            render();

            expect(scope.casperHide).toBeUndefined();
            SpecHelper.expectElement(elem, '#app-main-container');

            $rootScope.casperRender = true;
            $rootScope.$apply();
            expect(scope.casperHide).toBeUndefined();
            SpecHelper.expectElement(elem, '#app-main-container');
        });

        it('should not hide if casperMode is undefined', () => {
            casperMode.mockReturnValue(undefined);

            doInject();
            render();

            expect(scope.casperHide).toBeUndefined();
            SpecHelper.expectElement(elem, '#app-main-container');

            $rootScope.casperRender = true;
            $rootScope.$apply();
            expect(scope.casperHide).toBeUndefined();
            SpecHelper.expectElement(elem, '#app-main-container');
        });

        describe('casperMode = true', () => {
            beforeEach(() => {
                casperMode.mockReturnValue(true);

                doInject();
            });

            it('should not hide if $rootScope.casperRender is true', () => {
                render();

                $rootScope.casperRender = true;
                $rootScope.$apply();
                expect(scope.casperHide).toBe(false);
                SpecHelper.expectElement(elem, '#app-main-container');
            });

            it('should not hide if $rootScope.casperRender is undefined', () => {
                render();

                $rootScope.casperRender = undefined;
                $rootScope.$apply();
                expect(scope.casperHide).toBeFalsy();
                SpecHelper.expectElement(elem, '#app-main-container');
            });

            it('should hide if casperMode is true and $rootScope.casperRender is false', () => {
                render();

                $rootScope.casperRender = false;
                $rootScope.$apply();
                expect(scope.casperHide).toBe(true);
                SpecHelper.expectNoElement(elem, '#app-main-container');
            });
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<app-shell></app-shell>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
