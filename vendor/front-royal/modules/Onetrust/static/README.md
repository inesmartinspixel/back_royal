# NOTE

This /static directory setup is slightly unconventional. We have several reasons for this:

    1. We have a onetrustCookieHelper angular module that is used throughout our application,
    2. the onetrustCookieHelper expects a <provided> JS snippet to be present on the page,
    3. we have different snippets per domain and we don't want to bundle them both in our JS output to avoid bloat.

SO - each directory inside /static has an entryPoint.js that is used as an entry point in our Webpack config.

We then conditionally put the correct JS snippet on the page in app/views/shared/_onetrust.html.erb
