import _ from 'underscore';
import 'angular-cookie';

// This class acts as a bridge between OneTrust's "Privacy Preferences" cookie pop-up
// and the integrations we have enabled in Segment via event_logger.js.
//
// The idea is to retrieve the preferences that are saved via the OneTrust script to a global
// variable in JS and then convert them to a list of disabled integrations for use in the
// Segment JS API. EventLogger will check these preferences on every log call.
//
// See https://onetrust.com/documentation/iapp/user_guide_free.pdf for more information about
// how OneTrust's cookie preference panel provides this data to us.
export default angular.module('onetrustCookieHelper', ['ipCookie']).factory('onetrustCookieHelper', [
    '$injector',
    $injector => {
        const $window = $injector.get('$window');
        const ipCookie = $injector.get('ipCookie');

        // There are 4 groups of cookies that we've configured for users to control in OneTrust
        // Each group is identifier by a string; specifically the strings '1', '2', '3', and '4'
        //
        // '1' - Strictly necessary cookies. Can't be disabled.
        // '2' - Performance cookies. These cookies allow us to measure and improve the performance of our site.
        // '3' - Functional cookies. These cookies enable the website to provide enhanced functionality and personalization.
        // '4' - Targeting cookies. These cookies may be set through our site by our advertising partners.
        //
        // For the above taxonomy, we list each of the Segment integrations below and their corresponding category to disable.
        // If we add a new integration without adding it to this list, we will leave it enabled by default for now.
        const identifierToIntegration = {
            '1': [
                'Google Tag Manager', // we will put rules into Google Tag Manager itself for the integrations it provides
            ],
            '2': ['Amplitude', 'Crazy Egg', 'Google Analytics'],
            '3': ['HubSpot'],
            '4': [
                'ActiveCampaign',
                'AdWords',
                'AdRoll',
                'Facebook Custom Audiences',
                'Facebook Conversion Tracking',
                'Facebook Pixel',
                'LinkedIn Insight Tag',
                'LinkedIn Conversion Tracking', // This isn't a real integration in segment.  See EventLogger._logLinkedInPixel
                'Perfect Audience',
                'Quora Conversion Pixel',
                'Twitter Ads',
            ],
        };

        return {
            integrationDisabled(integration) {
                return this.getDisabledIntegrationsObject()[integration] === false;
            },

            // Returns a map of integrations that are disabled for use with Segment library
            // e.g.: given group 3 being disabled, we'd expect a return value like:
            //
            // {
            //     "HubSpot": false
            // }
            //
            getDisabledIntegrationsObject() {
                const groups = this._getOneTrustActiveGroups();
                const disabledGroups = _.difference(['1', '2', '3', '4'], groups);
                const disabledIntegrationsMap = _.chain(disabledGroups)
                    .map(group => identifierToIntegration[group])
                    .flatten()
                    .compact()
                    .reduce((memo, integration) => {
                        memo[integration] = false;
                        return memo;
                    }, {})
                    .value();

                return disabledIntegrationsMap;
            },

            // Gets an array of active group IDs from cookies or JS.
            // We first look at the JS, since if the script is loaded, it will always have data for us.
            // However, we fall back to cookies just in case they had previously expressed some preferences
            // and are now accessing Smartly from the same browser in a non-EU country.
            //
            // It's not clear to me if we can always rely on cookies and remove the JS check, but this seems
            // to work well in practice.
            _getOneTrustActiveGroups() {
                // Example JS global variable value:
                // ,101,1,2,102,103,3,104,4,105,106,107,108,109,110,111,112,113,115,116,117,0_2000,118,119,121,122,0_1999,0_2001,0_2004,0_2005,0_1997,0_2003,0_2008,0_2006,0_2007,0_2009,0_2002,120,
                const activeGroupsStrings = $window.OptanonActiveGroups;

                const jsGroups = activeGroupsStrings && activeGroupsStrings.split(',');

                // Example cookie value:
                // landingPath=NotLandingPage&datestamp=Tue+May+01+2018+16:47:15+GMT-0400+(EDT)&version=3.6.18&groups=101:1,1:1,2:1,102:1,103:1,3:1,104:1,4:1,105:1,106:1,107:1,108:1,109:1,110:1,111:1,112:1,113:1,115:1,116:1,117:1,0_2000:1,118:1,119:1,121:1,122:1,0_1999:1,0_2001:1,0_2004:1,0_2005:1,0_1997:1,0_2003:1,0_2008:1,0_2006:1,0_2007:1,0_2009:1,0_2002:1,120:1&AwaitingReconsent=false
                const cookie = `${ipCookie('OptanonConsent')}`;

                const cookieSections = cookie && cookie.split('&');

                const cookieMap =
                    cookieSections &&
                    _.reduce(
                        cookieSections,
                        (memo, section) => {
                            const keyval = section.split('=');
                            memo[keyval[0]] = keyval[1];
                            return memo;
                        },
                        {},
                    );

                const groupEntries = cookieMap && cookieMap.groups && cookieMap.groups.split(',');

                const cookieGroups =
                    groupEntries &&
                    _.chain(groupEntries)
                        .map(entry => {
                            const keyval = entry.split(':');
                            if (keyval[1] === '1') {
                                return keyval[0];
                            }
                            return undefined;
                        })
                        .compact()
                        .value();

                return jsGroups || cookieGroups || ['1', '2', '3', '4'];
            },

            _isOneTrustLoaded() {
                return !!$window.OptanonActiveGroups;
            },
        };
    },
]);
