import 'AngularSpecHelper';
import 'Onetrust/angularModule';
import 'angular-cookie';

describe('onetrustCookieHelper', () => {
    let onetrustCookieHelper;
    let $window;
    let ipCookie;

    beforeEach(() => {
        angular.mock.module('SpecHelper', 'onetrustCookieHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                $window = $injector.get('$window');
                ipCookie = $injector.get('ipCookie');
                onetrustCookieHelper = $injector.get('onetrustCookieHelper');
            },
        ]);

        ipCookie.remove('OptanonConsent');
        $window.OptanonActiveGroups = undefined;
    });

    afterEach(() => {
        ipCookie.remove('OptanonConsent');
        $window.OptanonActiveGroups = undefined;
    });

    it('should work with OneTrust not loaded at all', () => {
        expect(onetrustCookieHelper.getDisabledIntegrationsObject()).toEqual({});
    });

    // Note: I captured the values for OptanonActiveGroups / Cookies below from the local test instance of
    // the site after setting various combinations of preferences.
    describe('getDisabledIntegrationsObject with JS', () => {
        it('should work with nothing disabled', () => {
            $window.OptanonActiveGroups =
                ',101,1,2,102,103,3,104,4,105,106,107,108,109,110,111,112,113,115,116,117,0_2000,118,119,121,122,0_1999,0_2001,0_2004,0_2005,0_1997,0_2003,0_2008,0_2006,0_2007,0_2009,0_2002,120,';
            expect(onetrustCookieHelper.getDisabledIntegrationsObject()).toEqual({});
        });

        it('should work with group 2 disabled', () => {
            $window.OptanonActiveGroups =
                ',101,1,102,103,3,104,4,105,106,107,108,109,110,111,112,113,115,116,117,0_2000,118,119,121,122,0_1999,0_2001,0_2004,0_2005,0_1997,0_2003,0_2008,0_2006,0_2007,0_2009,0_2002,120,';
            expect(onetrustCookieHelper.getDisabledIntegrationsObject()).toEqual({
                Amplitude: false,
                'Crazy Egg': false,
                'Google Analytics': false,
            });
        });

        it('should work with group 3 disabled', () => {
            $window.OptanonActiveGroups =
                ',101,1,2,102,103,104,4,105,106,107,108,109,110,111,112,113,115,116,117,0_2000,118,119,121,122,0_1999,0_2001,0_2004,0_2005,0_1997,0_2003,0_2008,0_2006,0_2007,0_2009,0_2002,120,';
            expect(onetrustCookieHelper.getDisabledIntegrationsObject()).toEqual({
                HubSpot: false,
            });
        });

        it('should work with group 4 disabled', () => {
            $window.OptanonActiveGroups =
                ',101,1,2,102,103,3,104,105,106,107,108,109,110,111,112,113,115,116,117,0_2000,118,119,121,122,0_1999,0_2001,0_2004,0_2005,0_1997,0_2003,0_2008,0_2006,0_2007,0_2009,0_2002,120,';
            expect(onetrustCookieHelper.getDisabledIntegrationsObject()).toEqual({
                ActiveCampaign: false,
                AdWords: false,
                AdRoll: false,
                'Facebook Custom Audiences': false,
                'Facebook Conversion Tracking': false,
                'Facebook Pixel': false,
                'LinkedIn Insight Tag': false,
                'LinkedIn Conversion Tracking': false,
                'Perfect Audience': false,
                'Quora Conversion Pixel': false,
                'Twitter Ads': false,
            });
        });

        it('should work with groups 2 and 3 disabled', () => {
            $window.OptanonActiveGroups =
                ',101,1,102,103,104,4,105,106,107,108,109,110,111,112,113,115,116,117,0_2000,118,119,121,122,0_1999,0_2001,0_2004,0_2005,0_1997,0_2003,0_2008,0_2006,0_2007,0_2009,0_2002,120,';
            expect(onetrustCookieHelper.getDisabledIntegrationsObject()).toEqual({
                Amplitude: false,
                'Crazy Egg': false,
                'Google Analytics': false,
                HubSpot: false,
            });
        });
    });

    describe('getDisabledIntegrationsObject with Cookies', () => {
        it('should work with nothing disabled', () => {
            ipCookie(
                'OptanonConsent',
                'landingPath=NotLandingPage&datestamp=Tue+May+01+2018+16:47:15+GMT-0400+(EDT)&version=3.6.18&groups=101:1,1:1,2:1,102:1,103:1,3:1,104:1,4:1,105:1,106:1,107:1,108:1,109:1,110:1,111:1,112:1,113:1,115:1,116:1,117:1,0_2000:1,118:1,119:1,121:1,122:1,0_1999:1,0_2001:1,0_2004:1,0_2005:1,0_1997:1,0_2003:1,0_2008:1,0_2006:1,0_2007:1,0_2009:1,0_2002:1,120:1&AwaitingReconsent=false',
            );
            expect(onetrustCookieHelper.getDisabledIntegrationsObject()).toEqual({});
        });

        it('should work with a malformed cookie', () => {
            ipCookie('OptanonConsent', 'askdfjasklfdjasdasd');
            expect(onetrustCookieHelper.getDisabledIntegrationsObject()).toEqual({});
        });

        it('should work with group 2 disabled', () => {
            ipCookie(
                'OptanonConsent',
                'landingPath=NotLandingPage&datestamp=Tue+May+01+2018+16:47:15+GMT-0400+(EDT)&version=3.6.18&groups=101:1,1:1,2:0,102:1,103:1,3:1,104:1,4:1,105:1,106:1,107:1,108:1,109:1,110:1,111:1,112:1,113:1,115:1,116:1,117:1,0_2000:1,118:1,119:1,121:1,122:1,0_1999:1,0_2001:1,0_2004:1,0_2005:1,0_1997:1,0_2003:1,0_2008:1,0_2006:1,0_2007:1,0_2009:1,0_2002:1,120:1&AwaitingReconsent=false',
            );
            expect(onetrustCookieHelper.getDisabledIntegrationsObject()).toEqual({
                Amplitude: false,
                'Crazy Egg': false,
                'Google Analytics': false,
            });
        });

        it('should work with group 3 disabled', () => {
            ipCookie(
                'OptanonConsent',
                'landingPath=NotLandingPage&datestamp=Tue+May+01+2018+16:47:15+GMT-0400+(EDT)&version=3.6.18&groups=101:1,1:1,2:1,102:1,103:1,3:0,104:1,4:1,105:1,106:1,107:1,108:1,109:1,110:1,111:1,112:1,113:1,115:1,116:1,117:1,0_2000:1,118:1,119:1,121:1,122:1,0_1999:1,0_2001:1,0_2004:1,0_2005:1,0_1997:1,0_2003:1,0_2008:1,0_2006:1,0_2007:1,0_2009:1,0_2002:1,120:1&AwaitingReconsent=false',
            );
            expect(onetrustCookieHelper.getDisabledIntegrationsObject()).toEqual({
                HubSpot: false,
            });
        });

        it('should work with group 4 disabled', () => {
            ipCookie(
                'OptanonConsent',
                'landingPath=NotLandingPage&datestamp=Tue+May+01+2018+16:47:15+GMT-0400+(EDT)&version=3.6.18&groups=101:1,1:1,2:1,102:1,103:1,3:1,104:1,4:0,105:1,106:1,107:1,108:1,109:1,110:1,111:1,112:1,113:1,115:1,116:1,117:1,0_2000:1,118:1,119:1,121:1,122:1,0_1999:1,0_2001:1,0_2004:1,0_2005:1,0_1997:1,0_2003:1,0_2008:1,0_2006:1,0_2007:1,0_2009:1,0_2002:1,120:1&AwaitingReconsent=false',
            );
            expect(onetrustCookieHelper.getDisabledIntegrationsObject()).toEqual({
                ActiveCampaign: false,
                AdWords: false,
                AdRoll: false,
                'Facebook Custom Audiences': false,
                'Facebook Conversion Tracking': false,
                'Facebook Pixel': false,
                'LinkedIn Insight Tag': false,
                'LinkedIn Conversion Tracking': false,
                'Perfect Audience': false,
                'Quora Conversion Pixel': false,
                'Twitter Ads': false,
            });
        });

        it('should work with groups 2 and 3 disabled', () => {
            ipCookie(
                'OptanonConsent',
                'landingPath=NotLandingPage&datestamp=Tue+May+01+2018+16:47:15+GMT-0400+(EDT)&version=3.6.18&groups=101:1,1:1,2:0,102:1,103:1,3:0,104:1,4:1,105:1,106:1,107:1,108:1,109:1,110:1,111:1,112:1,113:1,115:1,116:1,117:1,0_2000:1,118:1,119:1,121:1,122:1,0_1999:1,0_2001:1,0_2004:1,0_2005:1,0_1997:1,0_2003:1,0_2008:1,0_2006:1,0_2007:1,0_2009:1,0_2002:1,120:1&AwaitingReconsent=false',
            );
            expect(onetrustCookieHelper.getDisabledIntegrationsObject()).toEqual({
                Amplitude: false,
                'Crazy Egg': false,
                'Google Analytics': false,
                HubSpot: false,
            });
        });
    });

    describe('integrationDisabled', () => {
        it('should work', () => {
            jest.spyOn(onetrustCookieHelper, 'getDisabledIntegrationsObject').mockReturnValue({
                Amplitude: false,
            });
            expect(onetrustCookieHelper.integrationDisabled('Amplitude')).toEqual(true);
        });
    });
});
