adapted from http://blog.parkji.co.uk/2013/08/11/native-drag-and-drop-in-angularjs.html

-   I've tried to figure out how to test this, but according to the following page,
    it is very hard to find a cross-browser way to create drag events:

    -   http://help.dottoro.com/ljnxhosf.php

== Making things draggable:

   <div
		draggable="dragActive"
		dragstart="doSomething($event)"
		drag-data="item"
		></div>

-   make something draggable by adding the 'draggable' attribute. The value of the directive is an expression indicating
    whether draggable is enabled or not. By default, this is "true"
-   add any of the following event listeners, passing the \$event local:
    -   dragstart - called when the drag starts
    -   dragstop - a pseudo-event (not defined in html5 spec) called when the drag finishes by being
        dropped somewhere on the document.body. This event exists to replace dragend, since
        dragend does not have reliable x and y values
    -   dragmove - a pseudo-event (not defined in html5 spec) called repeatedly as the dragged element is moved anywhere
        over the document.body. This event exists to replace drag, since
        drag does not have reliable x and y values. Note: This callback will not be wrapped
        in a scope.\$apply, since the frequency of the event could lead to performance issues.
    -   dragend - called when the drag ends. This event does not have reliable x and y values (bug in webkit browsers?)
        so you may want to use dragstop instead.
    -   drag - called repeatedly as the dragged element is moved. This event does not have reliable x and y values (bug in webkit browsers?)
        so you may want to use dragmove instead. Note: This callback will not be wrapped
        in a scope.\$apply, since the frequency of the event could lead to performance issues.
-   add some data to the event using the 'drag-data' attribute. This data will be available to any other event related to this drag through
    the property event.dragAndDrop.data

== Making drop targets

    <div
    	droppable
    	drop="doSomething($event)"
    	></div>

-   Make something into a drop target by adding the 'droppable' attribute.
-   add any of the following listeners, passing the \$event local:
    -   drop - called when something is dropped on this element
    -   dragover - called repeatedly as a dragged element is moved over this element. Due to a bug in webkit browsers (dataTransfer
        has nothing in it) may not have anything useful in event.dragAndDrop. Note: This callback will not be wrapped
        in a scope.\$apply, since the frequency of the event could lead to performance issues.
    -   dragenter - called once when a dragged element is first moved over this element
    -   dragleave - called when a dragged element is moved out of this element

== the dragAndDrop object

For events passed to any of the callbacks described above, a property called 'dragAndDrop' will be added with the following sub-properties:

-   data - whatever is defined in drag-data of the dragged element
-   movedX - the horizontal distance, in pixels, that the mouse has moved from the place where the drag started (due to an apparent bug,
    this currently is not set on dragend or drag events)
-   movedY - same as movedX, but vertical
-   startEvent - the dragstart event from when this drag was initialized

== dragsort

The dragsort directive allows elements that represent items in a list to be re-ordered by dragging and dropping them on one another.

    <div ng-repeat="item in list">
    	<div dragsort="item in list"></div>
    </div>

The element with the dragsort directive is both draggable and droppable and supports all of the callbacks described
for those directives. When one of the 'dragsort' elements is dropped on another, the dropped item will move in the list, ending up
before the item it was dropped on.
