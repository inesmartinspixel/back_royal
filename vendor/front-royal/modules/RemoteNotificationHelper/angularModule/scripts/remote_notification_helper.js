import * as userAgentHelper from 'userAgentHelper';

export default angular.module('remoteNotificationHelper', []).factory('remoteNotificationHelper', [
    '$injector',
    $injector => {
        const $rootScope = $injector.get('$rootScope');
        const $window = $injector.get('$window');
        const EventLogger = $injector.get('EventLogger');
        const ErrorLogService = $injector.get('ErrorLogService');

        return {
            registerDeviceForRemoteNotifications() {
                const self = this;
                const push = self._initializePlugin();

                // Since`registerDeviceForRemoteNotifications` will only ever be called on
                // application start or when the application is re-opened after being killed,
                // we can check here to see if the application was opened from interacting
                // with a push notification.
                // See also: `getAndroidIntentArgs` in hybrid/www/cordova_bootstrap.js
                if (
                    userAgentHelper.isAndroidDevice() &&
                    $window.pushNotificationState &&
                    $window.pushNotificationState.openedFromPushNotification
                ) {
                    self._logNotificationOpenedEventWithData($window.pushNotificationState.androidIntentArgs);
                }

                self._setupEventListeners();

                // After registering with a third-party push service (FCM in our case), we need to
                // log an event so we can properly identify this device's token, platform,git  etc.
                push.on('registration', data => {
                    self._logDeviceRegisteredEventWithData(data);
                });

                // If a push notification fails for whatever reason, log an error to Sentry.
                push.on('error', error => {
                    self._logRemoteNotificationErrorWithError(error);
                });

                // If a push notification is opened, log an event so we can properly track it.
                push.on('notification', data => {
                    if (data.additionalData && !data.additionalData.foreground && !data.additionalData.dismissed) {
                        self._logNotificationOpenedEventWithData(data.additionalData);
                    }
                });
            },

            _initializePlugin() {
                // Initialize the plugin with specific options for target platforms.
                //
                // NOTE: PushNotification.init() can only be called after the `deviceReady`
                // event fires.
                return window.PushNotification.init({
                    android: {
                        sound: true,
                        vibrate: true,
                        forceShow: true, // show notifications when app is in foreground
                        icon: 'icon_push', // tell plugin which icon to use for foreground notifications
                    },
                    browser: {}, // not used
                    ios: {
                        alert: true,
                        badge: true,
                        sound: true,
                    },
                    windows: {}, // not used
                });
            },

            /**
             * @description Binds function(s) that will be called when certain
             *  events are dispatched.
             */
            _setupEventListeners() {
                const self = this;

                // Listen for a custom synthetic event fired from hybrid/www/cordova_bootstrap.js
                // when the Android application is resumed from the background by tapping a notification.
                $window.document.addEventListener(
                    'androidApplicationResumedFromNotification',
                    () => {
                        self._logNotificationOpenedEventWithData($window.pushNotificationState.androidIntentArgs);
                    },
                    false,
                );
            },

            /**
             * @description Logs a `cordova:notification-opened` event with
             *  passed in data object.
             * @param {} data - an object containing push notification delivery data
             */
            _logNotificationOpenedEventWithData(data) {
                if (!$rootScope.currentUser) {
                    return;
                }

                if (!data['CIO-Delivery-ID'] || !data['CIO-Delivery-Token']) {
                    ErrorLogService.notify(
                        '_logNotificationOpenedEventWithData called without delivery ID and/or token',
                        null,
                        data,
                    );
                }

                EventLogger.log(
                    'cordova:notification-opened',
                    {
                        user_id: $rootScope.currentUser.id,
                        delivery_id: data['CIO-Delivery-ID'],
                        device_id: data['CIO-Delivery-Token'],
                        event: 'opened',
                    },
                    {
                        segmentio: false,
                    },
                );
            },

            _logDeviceRegisteredEventWithData(data) {
                let devicePlatform;
                if (userAgentHelper.isAndroidDevice()) {
                    devicePlatform = 'android';
                } else if (userAgentHelper.isiOSDevice()) {
                    devicePlatform = 'ios';
                }

                EventLogger.log(
                    'cordova:device-registered',
                    {
                        user_id: $rootScope.currentUser.id,
                        device_token: data.registrationId,
                        platform: devicePlatform,
                    },
                    {
                        segmentio: false,
                    },
                );
            },

            _logRemoteNotificationErrorWithError(error) {
                ErrorLogService.notify('Remote notification error', error.message);
            },
        };
    },
]);
