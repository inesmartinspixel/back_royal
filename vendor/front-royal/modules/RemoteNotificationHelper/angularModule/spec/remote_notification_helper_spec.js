import * as userAgentHelper from 'userAgentHelper';
import 'AngularSpecHelper';
import 'RemoteNotificationHelper/angularModule';

describe('remoteNotificationHelper', () => {
    let remoteNotificationHelper;
    let $rootScope;
    let EventLogger;
    let ErrorLogService;
    let pluginInstance;

    beforeEach(() => {
        angular.mock.module('SpecHelper', 'remoteNotificationHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                remoteNotificationHelper = $injector.get('remoteNotificationHelper');
                $rootScope = $injector.get('$rootScope');
                EventLogger = $injector.get('EventLogger');
                ErrorLogService = $injector.get('ErrorLogService');
            },
        ]);
        pluginInstance = {
            init: jest.fn(),
            on: jest.fn(),
        };
        window.PushNotification = pluginInstance;
        $rootScope.currentUser = { id: 'userId' };
    });

    describe('registerDeviceForRemoteNotifications', () => {
        beforeEach(() => {
            jest.spyOn(remoteNotificationHelper, '_initializePlugin').mockReturnValue(pluginInstance);
            jest.spyOn(remoteNotificationHelper, '_setupEventListeners').mockImplementation(() => {
                // noop
            });
            jest.spyOn(remoteNotificationHelper, '_logNotificationOpenedEventWithData').mockImplementation(() => {
                // noop
            });
        });
        it('should call _initializePlugin', () => {
            remoteNotificationHelper.registerDeviceForRemoteNotifications();
            expect(remoteNotificationHelper._initializePlugin).toHaveBeenCalled();
        });
        it('should call _logNotificationOpenedEventWithData if Android and window.pushNotificationState.openedFromPushNotification', () => {
            jest.spyOn(userAgentHelper, 'isAndroidDevice').mockReturnValue(true);
            window.pushNotificationState = {
                androidIntentArgs: {
                    foo: 'bar',
                },
                openedFromPushNotification: true,
            };
            remoteNotificationHelper.registerDeviceForRemoteNotifications();
            expect(remoteNotificationHelper._logNotificationOpenedEventWithData).toHaveBeenCalledWith({
                foo: 'bar',
            });
        });
        it('should call _setupEventListeners', () => {
            remoteNotificationHelper.registerDeviceForRemoteNotifications();
            expect(remoteNotificationHelper._setupEventListeners).toHaveBeenCalled();
        });
        it("should setup 'registration' and 'error' and 'notification 'callbacks", () => {
            remoteNotificationHelper.registerDeviceForRemoteNotifications();
            expect(pluginInstance.on).toHaveBeenCalledWith('registration', expect.any(Function));
            expect(pluginInstance.on).toHaveBeenCalledWith('error', expect.any(Function));
            expect(pluginInstance.on).toHaveBeenCalledWith('notification', expect.any(Function));
        });
    });

    describe('_initializePlugin', () => {
        it('should initialize the plugin with options', () => {
            remoteNotificationHelper._initializePlugin();
            expect(pluginInstance.init).toHaveBeenCalledWith({
                android: {
                    sound: true,
                    vibrate: true,
                    forceShow: true,
                    icon: 'icon_push',
                },
                browser: {},
                ios: {
                    alert: true,
                    badge: true,
                    sound: true,
                },
                windows: {},
            });
        });
    });

    describe('_setupEventListeners', () => {
        describe('with androidApplicationResumedFromNotification event', () => {
            it('should create an event that calls _logNotificationOpenedEventWithData', () => {
                jest.spyOn(remoteNotificationHelper, '_logNotificationOpenedEventWithData').mockImplementation(() => {
                    // noop
                });
                window.pushNotificationState = {
                    androidIntentArgs: {
                        foo: 'bar',
                    },
                };
                remoteNotificationHelper._setupEventListeners();
                const event = new Event('androidApplicationResumedFromNotification');
                window.document.dispatchEvent(event);
                expect(remoteNotificationHelper._logNotificationOpenedEventWithData).toHaveBeenCalledWith({
                    foo: 'bar',
                });
            });
        });
    });

    describe('_logNotificationOpenedEventWithData', () => {
        let data;

        beforeEach(() => {
            data = {
                'CIO-Delivery-ID': '12345',
                'CIO-Delivery-Token': '54321',
            };
            jest.spyOn(EventLogger.prototype, 'log');
        });

        it('should log an open event with notification properties', () => {
            remoteNotificationHelper._logNotificationOpenedEventWithData(data);
            expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                'cordova:notification-opened',
                {
                    user_id: $rootScope.currentUser.id,
                    delivery_id: '12345',
                    device_id: '54321',
                    event: 'opened',
                },
                {
                    segmentio: false,
                },
            );
        });

        it('should return early if no current user', () => {
            $rootScope.currentUser = undefined;
            remoteNotificationHelper._logNotificationOpenedEventWithData(data);
            expect(EventLogger.prototype.log).not.toHaveBeenCalled();
        });

        it('should log an error if data does not contain delivery properties', () => {
            jest.spyOn(ErrorLogService, 'notify').mockImplementation();
            remoteNotificationHelper._logNotificationOpenedEventWithData({});
            expect(ErrorLogService.notify).toHaveBeenCalledWith(
                '_logNotificationOpenedEventWithData called without delivery ID and/or token',
                null,
                {},
            );
        });
    });

    describe('_logDeviceRegisteredEventWithData', () => {
        it('should log an event with device properties', () => {
            jest.spyOn(EventLogger.prototype, 'log');
            jest.spyOn(userAgentHelper, 'isAndroidDevice').mockReturnValue(true);
            remoteNotificationHelper._logDeviceRegisteredEventWithData({
                registrationId: '12345',
            });
            expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                'cordova:device-registered',
                {
                    user_id: $rootScope.currentUser.id,
                    device_token: '12345',
                    platform: 'android',
                },
                {
                    segmentio: false,
                },
            );
        });
    });

    describe('_logRemoteNotificationErrorWithError', () => {
        it('should log an error', () => {
            jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => 'foobar');
            remoteNotificationHelper._logRemoteNotificationErrorWithError({
                message: 'foobar',
            });
            expect(ErrorLogService.notify).toHaveBeenCalledWith('Remote notification error', 'foobar');
        });
    });
});
