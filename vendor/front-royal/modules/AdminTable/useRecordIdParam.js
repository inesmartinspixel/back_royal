import { useState, useEffect, useRef, useReducer } from 'react';
import { useLocation } from 'react-use';
import { find, clone, isEqual, without, some, identity } from 'lodash/fp';
import { generateGuid } from 'guid';

// Function that finds the record for the edit form
// when there is an id in the query params
function ensureRecordLoaded({
    recordId,
    records,
    setRecord,
    makeRequest,
    defaultFilters,
    setRecordId,
    loadingRecordForId,
    setLoadingRecordForId,
    getBlankRecord,
}) {
    // if the defaultFilters aren't setup yet,
    // we have to wait until they are
    if (!defaultFilters) {
        return null;
    }

    if (recordId === 'create') {
        return setRecord(getBlankRecord());
    }

    // First, check for the record in the list of records
    // we already have
    const record = find({
        id: recordId,
    })(records);

    if (record) {
        return setRecord(record);
    }

    // If we didn't find it, but are already loading it,
    // don't do anything
    if (loadingRecordForId === recordId) {
        return null;
    }

    // If we aren't loading it, check the server.  This
    // assumes that the api supports an id filter
    setLoadingRecordForId(recordId);

    makeRequest({
        id: recordId,
        ...defaultFilters,
    }).then(result => {
        const resultRecord = find({
            id: recordId,
        })(result);

        // If we found the record on the server, set it
        if (resultRecord) {
            setRecord(resultRecord);
        }

        // If we didn't find the record, then remove the id
        // from the url and go back to the main table
        else {
            setRecordId(null);
        }
    });

    return null;
}

export default function useRecordIdParam({ idParam, fetchData, defaultFilters, filters, getBlankRecord }) {
    const location = useLocation();
    const url = new URL(location.href);
    const recordId = url.searchParams.get(idParam);
    const [record, setRecord] = useState();
    const [records, setRecords] = useState();
    const [filtersUsedToLoadRecords, setFiltersUsedToLoadRecords] = useState();
    const [loadingRecordForId, setLoadingRecordForId] = useState();
    const forceUpdate = useState()[1];

    // reducer that keeps track of which requests are
    // in flight
    const [activeRequests, dispatchRequestAction] = useReducer((_activeRequests, action) => {
        if (action.type === 'requestSent') {
            _activeRequests = _activeRequests.concat([action.request]);
        } else if (action.type === 'requestCompleted') {
            _activeRequests = without([action.request])(_activeRequests);
        }
        return _activeRequests;
    }, []);

    // Wrapper for fetchData that uses the activeRequests
    // reducer to keep track of which requests are
    // in flight
    const makeRequest = useRef(_filters => {
        const id = generateGuid();
        dispatchRequestAction({
            request: id,
            type: 'requestSent',
        });
        return fetchData(_filters).finally(() => {
            dispatchRequestAction({
                request: id,
                type: 'requestCompleted',
            });
        });
    }).current;

    // callback that can be used to set the record
    // id in the future (i.e. by clicking on a row
    // in the table)
    const setRecordId = useRef(id => {
        if (id) {
            url.searchParams.set(idParam, id);
        } else {
            url.searchParams.delete(idParam, id);
        }

        const href = `${location.pathname}?${url.searchParams.toString()}`;
        window.history.replaceState({}, '', href);
        forceUpdate(href);
    }).current;

    // This is the magic.  Watch for a whole bunch of things to
    // change and then make sure that we have the appropriate
    // records loaded
    useEffect(() => {
        // If the record we're expecting is already loaded, do nothing
        if (recordId && record && (record.id === recordId || recordId === 'create')) {
            return;
        }

        // Ensure that if we have a recordId, the record is
        // available.  We will first look in the list of loaded records (if
        // records are loaded already).  If we don't find it there, we
        // will make an api call to load up that one record.
        if (recordId) {
            ensureRecordLoaded({
                recordId,
                record,
                records,
                setRecord,
                makeRequest,
                defaultFilters,
                setRecordId,
                loadingRecordForId,
                setLoadingRecordForId,
                getBlankRecord,
            });
        } else {
            setRecord(null);
        }

        // Load up the list of records any time filters change.  We
        // make this call even if we're showing a particular record initially,
        // so that the records are preloaded before we go back to the table.
        if (!isEqual(filtersUsedToLoadRecords, filters)) {
            setFiltersUsedToLoadRecords(clone(filters));
            makeRequest(filters).then(loadedRecords => {
                setRecords(loadedRecords);
            });
        }
    }, [
        record,
        recordId,
        defaultFilters,
        records,
        setRecords,
        makeRequest,
        setRecordId,
        filters,
        filtersUsedToLoadRecords,
        loadingRecordForId,
        setLoadingRecordForId,
        getBlankRecord,
    ]);

    // Callback function that will unload all the
    // records.  This is used, for example, after
    // a record is updated.
    function resetRecords() {
        setFiltersUsedToLoadRecords(null);
        setRecords(null);
    }

    return {
        recordId,
        setRecordId,
        record,
        records,
        setRecords,
        resetRecords,
        loading: some(identity)(activeRequests),
    };
}
