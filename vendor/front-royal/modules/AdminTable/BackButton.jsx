import React, { useContext } from 'react';
import { useTheme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import HomeIcon from '@material-ui/icons/Home';
import AdminTableContext from './AdminTableContext';

export default function BackButton(props) {
    const { setRecordId } = useContext(AdminTableContext);
    const theme = useTheme();

    return (
        <Paper elevation={0} style={{ padding: theme.spacing(1, 2), marginBottom: 20 }}>
            <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />} aria-label="breadcrumb">
                <Link color="secondary" onClick={() => setRecordId(null)} style={{ display: 'flex' }}>
                    <HomeIcon style={{ marginRight: theme.spacing(0.5), width: 20, height: 20 }} />
                    {props.home}
                </Link>
                <Typography color="textPrimary">{props.detail}</Typography>
            </Breadcrumbs>
        </Paper>
    );
}
