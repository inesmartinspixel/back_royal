import { useContext } from 'react';
import AngularContext from 'AngularContext';
import AdminTableContext from './AdminTableContext';

/*
    This function is used when setting up an edit form for an AdminTable.

    It returns

    - initialValues - object to be passed into the formik form
    - onSubmit - callback function to be passed into the formik form
    - destroy - callback function that will destroy the record

*/
export default function useRecordFromAdminTable(args) {
    const { fromForm, toForm, metaData } = args;

    const { record, collection, onRecordUpdated, onRecordDeleted, setRecordId } = useContext(AdminTableContext);

    const $injector = useContext(AngularContext);

    function onSubmit(values, actions) {
        let _metadata = metaData || {};
        if (metaData && typeof metaData === 'function') {
            _metadata = metaData();
        }

        const $http = $injector.get('$http');
        const formData = new FormData();
        const updates = {};

        const fromValues = fromForm(values);
        Object.keys(fromValues).forEach(key => {
            const val = fromValues[key];
            if (val && val.constructor && val.constructor.name === 'File') {
                formData.append(key, val);
            } else {
                updates[key] = val;
            }
        });

        formData.append('record', JSON.stringify(updates));
        formData.append('meta', JSON.stringify(_metadata));

        const isNew = !updates.id;
        const baseUrl = `${window.ENDPOINT_ROOT}/api/${collection}`;
        const url = isNew ? `${baseUrl}.json` : `${baseUrl}/${updates.id}.json`;
        const request = {
            method: isNew ? 'POST' : 'PUT',
            url,
            data: formData,
            headers: {
                Accept: 'application/json',

                // We have to set Content-type to undefined here.  When we
                // do that, the content-type ends up getting set to
                // multipart form with an appropriate boundary.  Otherwise
                // it ends up being application/json.  I guess maybe
                // $http is doing that
                'Content-type': undefined,
            },
        };

        $http(request)
            .then(response => {
                let result;
                try {
                    result = response.data.contents[collection][0];
                } catch (e) {
                    throw new Error('No result found in response.');
                }
                const updatedRecord = {
                    ...updates,
                    ...result,
                };
                actions.setValues(toForm(updatedRecord));
                onRecordUpdated(updatedRecord);
                if (isNew) {
                    setRecordId(updatedRecord.id);
                }
            })
            .finally(() => {
                actions.setSubmitting(false);
            });
    }

    function destroy(event) {
        const $http = $injector.get('$http');
        event.preventDefault();
        if (window.confirm('Are you sure you want to delete?')) {
            const baseUrl = `${window.ENDPOINT_ROOT}/api/${collection}`;
            const url = `${baseUrl}/${record.id}.json`;
            const request = {
                method: 'DELETE',
                url,
                headers: {
                    Accept: 'application/json',
                },
            };

            $http(request).then(() => {
                onRecordDeleted(record);
            });
        }
    }

    function duplicate(formik) {
        formik.setFieldValue('id', null);
        setRecordId('create');
    }

    return {
        initialValues: toForm(record.asJson()),
        onSubmit,
        destroy,
        duplicate,
    };
}
