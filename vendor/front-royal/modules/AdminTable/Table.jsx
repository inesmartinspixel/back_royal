import React, { useContext } from 'react';
import { some, identity } from 'lodash/fp';
import MaterialUiTable from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableFooter from '@material-ui/core/TableFooter';
import TableRow from '@material-ui/core/TableRow';
import { makeStyles } from '@material-ui/core/styles';
import Oreo from 'Oreo';
import AdminTableContext from './AdminTableContext';
import Pagination from './Pagination';
import CreateButton from './CreateButton';
import FilterButton from './FilterButton';
import ColumnHeaders from './ColumnHeaders';
import SelectedFilterChips, { useSelectedFilterChips } from './SelectedFilterChips';

const useStyles = makeStyles(() => ({
    disabledRow: {
        opacity: 0.3,
    },
    bodyRow: {
        cursor: 'pointer',
        '&:hover': {
            backgroundColor: Oreo.COLOR_V3_BEIGE_LIGHT,
        },
    },
}));

export default function Table() {
    const classes = useStyles();

    const {
        setRecordId,
        loading,
        reactTable: { headers, page, prepareRow },
    } = useContext(AdminTableContext);

    const selectedFilterChips = useSelectedFilterChips();

    return (
        <>
            <MaterialUiTable>
                <TableHead>
                    {/* TableHead row 1: Action buttons and pagination */}
                    <TableRow>
                        <TableCell colSpan={2}>
                            <CreateButton />
                            <FilterButton />
                        </TableCell>
                        <Pagination colspan={headers.length - 2} />
                    </TableRow>

                    {/* TableHead row 2: Selected filter chips (if there are some) */}
                    {some(identity)(selectedFilterChips) && (
                        <TableRow>
                            <TableCell colSpan={headers.length}>
                                <SelectedFilterChips {...{ selectedFilterChips }} />
                            </TableCell>
                        </TableRow>
                    )}

                    {/* TableHead row 3: column headers */}
                    <TableRow className={loading ? classes.disabledRow : ''}>
                        <ColumnHeaders />
                    </TableRow>
                </TableHead>

                <TableBody>
                    {page.map(
                        row =>
                            prepareRow(row) || (
                                <TableRow
                                    key={row.original.id}
                                    {...row.getRowProps()}
                                    onClick={() => setRecordId(row.original.id)}
                                    className={`${loading ? classes.disabledRow : ''} ${classes.bodyRow}`}
                                >
                                    {row.cells.map(cell => (
                                        <TableCell key={cell.id} {...cell.getCellProps()}>
                                            {cell.render('Cell')}
                                        </TableCell>
                                    ))}
                                </TableRow>
                            ),
                    )}
                </TableBody>

                <TableFooter>
                    <TableRow>
                        <Pagination />
                    </TableRow>
                </TableFooter>
            </MaterialUiTable>
        </>
    );
}
