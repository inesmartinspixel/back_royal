import React, { useState, useEffect } from 'react';
import { materialUiTheme as theme } from 'FrontRoyalMaterialUiForm';
import { ThemeProvider } from '@material-ui/styles';
import AngularContext from 'AngularContext';
import { useTable, usePagination, useSortBy } from 'react-table';
import { find, without } from 'lodash/fp';
import Container from '@material-ui/core/Container';
import AdminTableContext from './AdminTableContext';
import useRecordIdParam from './useRecordIdParam';
import useFilters from './useFilters';
import Table from './Table';

const defaultCell = ({ cell: { value = '' } }) => (value === null ? '' : String(value));

function setColumnDefaults(rawColumns) {
    const _columns = [];

    // Make nulls render as empty strings rather than `'null'`
    rawColumns.forEach(col => {
        _columns.push({
            Cell: defaultCell,
            ...col,
        });
    });
    return _columns;
}

const emptyArray = [];

function showToastSuccess($injector, msg) {
    $injector.get('ngToast').create({
        content: msg,
        className: 'success',
    });
}

export default function AdminTable({
    $injector,
    idParam,
    columns: rawColumns,
    collection,
    fetchData,
    filterFields,
    EditComponent,
    getBlankRecord,
}) {
    // Setup the filters
    const { filters, setFilters, defaultFilters } = useFilters({ collection, filterFields });

    // Handle loading records and deciding when to show a single record to edit and
    // when to show the whole table
    const { recordId, setRecordId, record, records, setRecords, resetRecords, loading } = useRecordIdParam({
        idParam,
        fetchData,
        defaultFilters,
        filters,
        getBlankRecord,
    });

    // Set some defaults on columns
    const [columns, setColumns] = useState([]);
    useEffect(() => {
        setColumns(setColumnDefaults(rawColumns));
    }, [rawColumns]);

    // Any time filters change, go back to the first page.  It would
    // be nice to move this to useFilters, but it needs access to the reactTable,
    // which can't be setup until after useFilters has been called.
    useEffect(() => {
        reactTable.gotoPage(0);
    }, [filters, reactTable]);

    const reactTable = useTable(
        {
            columns: columns || emptyArray,
            data: records || emptyArray,
        },
        useSortBy,
        usePagination,
    );

    function onRecordUpdated() {
        // Since we can't really know if the new record or updated record
        // should be included in the current list based on the current filters,
        // We unload the records and force a reload
        resetRecords();
        showToastSuccess($injector, 'Saved');
    }

    function onRecordDeleted(deletedRecord) {
        const existingRecord = find({
            id: deletedRecord.id,
        })(records);
        setRecords(without([existingRecord])(records));
        setRecordId(null);
        showToastSuccess($injector, 'Deleted');
    }

    return (
        <ThemeProvider theme={theme}>
            <AdminTableContext.Provider
                value={{
                    columns,
                    setFilters,
                    filters,
                    defaultFilters,
                    records,
                    filterFields,
                    reactTable,
                    setRecordId,
                    collection,
                    record,
                    onRecordUpdated,
                    onRecordDeleted,
                    loading,
                }}
            >
                <AngularContext.Provider value={$injector}>
                    {!recordId && records && <Table />}
                    {record && (
                        <Container maxWidth="lg">
                            <EditComponent {...{ record }} />
                        </Container>
                    )}
                    {/* {!records && 'FIXME: Spinner'} */}
                </AngularContext.Provider>
            </AdminTableContext.Provider>
        </ThemeProvider>
    );
}
