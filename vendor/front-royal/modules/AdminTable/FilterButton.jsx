import React, { useContext } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilter } from '@fortawesome/pro-regular-svg-icons/faFilter';
import MaterialUiPopoverButton, { usePopover } from 'MaterialUiPopoverButton';
import AdminTableContext from './AdminTableContext';
import FilterForm from './FilterForm';

export default function FilterButton() {
    const popover = usePopover();

    const { setFilters, filterFields, filters } = useContext(AdminTableContext);

    return (
        <div style={{ display: 'inline' }}>
            <MaterialUiPopoverButton
                title="Filter list"
                icon={<FontAwesomeIcon icon={faFilter} />}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                {...popover}
            >
                <div style={{ maxWidth: 400, padding: 12, paddingTop: 20 }}>
                    <FilterForm
                        {...{ filterFields, filters }}
                        onSubmit={values => {
                            setFilters(values);
                            popover.setOpen(false);
                        }}
                    />
                </div>
            </MaterialUiPopoverButton>
        </div>
    );
}
