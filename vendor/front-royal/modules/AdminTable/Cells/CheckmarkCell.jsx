import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/pro-regular-svg-icons/faCheck';

export default function Checkmark({ cell: { value } }) {
    return <>{value && <FontAwesomeIcon icon={faCheck} />}</>;
}
