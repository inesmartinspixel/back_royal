import React, { useContext } from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import { makeStyles } from '@material-ui/core/styles';
import AdminTableContext from './AdminTableContext';

const useStyles = makeStyles(() => ({
    // We may want to reuse visuallyHidden at some point somewhere
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
    disabled: {
        cursor: 'auto',
        '&:hover': {
            color: 'inherit',
        },
        '&:hover .MuiTableSortLabel-icon': {
            opacity: 0,
        },
    },
    disabledSortIcon: {
        opacity: 0,
    },
}));

export default function ColumnHeaders() {
    const classes = useStyles();

    const {
        loading: disabled,
        reactTable: { headers },
    } = useContext(AdminTableContext);

    return (
        <>
            {headers.map(column => (
                <TableCell
                    key={column.id}
                    {...column.getHeaderProps(!disabled && column.getSortByToggleProps())}
                    sortDirection={column.isSorted ? (column.isSortedDesc ? 'desc' : 'asc') : false}
                >
                    <TableSortLabel
                        active={column.isSorted}
                        direction={column.isSortedDesc ? 'desc' : 'asc'}
                        className={disabled ? classes.disabled : ''}
                        classes={disabled ? { icon: classes.disabledSortIcon } : {}}
                    >
                        {column.render('Header')}
                        {column.isSorted ? (
                            <span className={classes.visuallyHidden}>
                                {column.isSortedDesc ? 'sorted descending' : 'sorted ascending'}
                            </span>
                        ) : null}
                    </TableSortLabel>
                </TableCell>
            ))}
        </>
    );
}
