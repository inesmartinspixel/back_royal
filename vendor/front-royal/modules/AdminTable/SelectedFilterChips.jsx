import React, { useEffect, useState, useContext } from 'react';
import Chip from '@material-ui/core/Chip';
import { clone, flow, keyBy, map, compact, without } from 'lodash/fp';
import { Select } from 'FrontRoyalMaterialUiForm';
import extractProperties from 'extractProperties';
import AdminTableContext from './AdminTableContext';

function getSummaryLabelGenerator(component) {
    switch (component) {
        case Select:
            return (val, filterFieldDefinition) => {
                const options = filterFieldDefinition.options;

                // The default functions here come from the internals
                // of Select.  Not totally dry, but unlikely to change
                const optionLabel = filterFieldDefinition.optionLabel || (opt => opt.label);
                const optionValue = filterFieldDefinition.optionValue || (opt => opt.value);
                const optionsByValue = keyBy(opt => optionValue(opt))(options);

                // Sometimes there is an option that unsets the
                // filter. In that case, do not show a chip.
                if (val === null || val === undefined || val === '') {
                    return null;
                }
                const opt = optionsByValue[val];
                return optionLabel(opt);
            };

        default:
            return val => {
                if (val !== undefined && val !== '' && val !== null) {
                    return String(val);
                }
            };
    }
}

export function useSelectedFilterChips() {
    const { filters, filterFields } = useContext(AdminTableContext);

    const [selectedFilterChips, setSelectedFilterChips] = useState([]);

    useEffect(() => {
        let _selectedFilterChips = [];
        filterFields.forEach(filterFieldDefinition => {
            if (filterFieldDefinition.hidden) {
                return;
            }
            const val = filters[filterFieldDefinition.name];
            const vals = Array.isArray(val) ? val : [val];
            const fn =
                filterFieldDefinition.getLabelsForSelectFilterChips ||
                getSummaryLabelGenerator(filterFieldDefinition.component);

            _selectedFilterChips = _selectedFilterChips.concat(
                flow(
                    map(_val => {
                        const label = fn(_val, filterFieldDefinition);
                        if (!label) {
                            return null;
                        }
                        return {
                            label,
                            filterFieldDefinition,
                            value: _val,
                        };
                    }),
                    compact,
                )(vals),
            );
        });
        setSelectedFilterChips(_selectedFilterChips);
    }, [filterFields, filters]);

    return selectedFilterChips;
}

export default function SelectedFilterChips(props) {
    const { filters, setFilters, defaultFilters } = useContext(AdminTableContext);

    const [{ selectedFilterChips }, passthrough] = extractProperties(props, 'selectedFilterChips');

    function removeFilter(selectedFilter) {
        const {
            filterFieldDefinition: { name },
            value,
        } = selectedFilter;

        const newFilters = clone(filters);
        if (Array.isArray(filters[name])) {
            newFilters[name] = without([value])(newFilters[name]);
        } else {
            newFilters[name] = defaultFilters[name];
        }
        setFilters(newFilters);
    }

    return (
        <div {...passthrough} style={{ display: 'flex', justifyContent: 'center' }}>
            {selectedFilterChips.map((selectedFilter, i) => (
                <Chip
                    key={i}
                    label={selectedFilter.label}
                    variant="outlined"
                    color="primary"
                    onDelete={() => removeFilter(selectedFilter)}
                />
            ))}
        </div>
    );
}
