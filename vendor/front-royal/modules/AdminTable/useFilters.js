import { useEffect, useState } from 'react';
import { useLocalStorage } from 'react-use';
import getFilterFieldDefaultValue from './getFilterFieldDefaultValue';

export default function useFilters({ filterFields, collection }) {
    const [filters, _setFilters] = useState();
    const [storedFilterJson, setStoredFilterJson] = useLocalStorage(`${collection}-filters`);
    const [defaultFilters, setDefaultFilters] = useState();

    useEffect(() => {
        let storedFilters;
        try {
            storedFilters = JSON.parse(storedFilterJson);
        } catch (e) {
            storedFilters = null;
        }

        const _defaultFilters = {};
        _.each(filterFields, filterFieldDefinition => {
            _defaultFilters[filterFieldDefinition.name] = getFilterFieldDefaultValue(filterFieldDefinition);
        });
        setDefaultFilters(_defaultFilters);

        _setFilters(storedFilters || _defaultFilters);
    }, [filterFields, storedFilterJson]);

    // We prefix the actual setter because that should only
    // be called in this file.  Anything external should use
    // this setFilters function to set the filters through local storage.
    function setFilters(filters) {
        setStoredFilterJson(JSON.stringify(filters));
    }

    return {
        setFilters,
        filters,
        defaultFilters,
    };
}
