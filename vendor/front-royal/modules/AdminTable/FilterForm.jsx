import React, { useContext } from 'react';
import { flow, reject, map, omit } from 'lodash/fp';
import { Formik, Form } from 'formik';
import { Field } from 'FrontRoyalForm';
import Button from '@material-ui/core/Button';
import { useTheme } from '@material-ui/core/styles';
import AdminTableContext from './AdminTableContext';

function FlexField(props) {
    return (
        <div style={{ flexGrow: '1', minWidth: '120px', paddingBottom: '12px', width: '100%' }}>
            {/* eslint-disable-next-line react/jsx-props-no-spreading */}
            <Field {...props} />
        </div>
    );
}

export default function FilterForm({ filterFields, filters, onSubmit }) {
    const theme = useTheme();

    const { defaultFilters } = useContext(AdminTableContext);

    const style = {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
        alignItems: 'flex-start',
        alignContent: 'flex-start',
    };

    return (
        <Formik
            initialValues={filters}
            onSubmit={onSubmit}
            render={formik => (
                <Form>
                    <div {...{ style }}>
                        {flow(
                            reject(fieldDef => fieldDef.hidden),
                            // eslint-disable-next-line react/jsx-props-no-spreading
                            map(fieldDef => <FlexField key={fieldDef.name} {...omit('summaryLabels')(fieldDef)} />),
                        )(filterFields)}
                    </div>

                    <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                        <div style={{ flex: '0 1 0' }}>
                            <Button
                                variant="contained"
                                type="button"
                                color="secondary"
                                disabled={formik.isSubmitting}
                                onClick={() => {
                                    _.each(defaultFilters, (val, key) => {
                                        formik.setFieldValue(key, val);
                                    });
                                }}
                                style={{ marginRight: theme.spacing() }}
                            >
                                Reset
                            </Button>
                        </div>

                        <div style={{ flex: '0 1 0' }}>
                            <Button
                                variant="contained"
                                type="submit"
                                color="primary"
                                disabled={!formik.isValid || formik.isSubmitting}
                            >
                                Filter
                            </Button>
                        </div>
                    </div>
                </Form>
            )}
        />
    );
}
