import React, { useContext } from 'react';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import AdminTableContext from './AdminTableContext';

export default function CreateButton() {
    const { setRecordId } = useContext(AdminTableContext);

    return (
        <Tooltip title="Create New">
            <Fab color="secondary" aria-label="Create" onClick={() => setRecordId('create')}>
                <AddIcon />
            </Fab>
        </Tooltip>
    );
}
