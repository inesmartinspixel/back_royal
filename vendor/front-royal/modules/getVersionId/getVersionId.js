import { generateGuid } from 'guid';
import moment from 'moment-timezone';

export default function getVersionId() {
    return `${moment().format('YYYY-MM-DD_HH:mm:ss.SSS')}-${generateGuid()}`;
}
