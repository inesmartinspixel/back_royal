/*
    For our development and production builds, we leverage the expose-loader to tell webpack
    to shim certain libraries onto the global window object. However, since Jest unfortunately
    doesn't use webpack's config for compiling the assets, we need to do the shimming manually.
*/

import './jsdom.poly.js';

//---------------------------
// Common Libraries
//---------------------------

import $ from 'jquery';

import UAParser from 'ua-parser-js';

import _ from 'underscore';

import Selectize from 'selectize';

//---------------------------
// Front-Royal Libraries
//---------------------------

//---------------------------
// Reports Libraries
//---------------------------

import Plotly from 'plotly.js/dist/plotly.min.js';

//---------------------------
// Editor Libraries
//---------------------------

import jsondiffpatch from 'jsondiffpatch/dist/jsondiffpatch.umd.js';

import diff_match_patch from 'diff-match-patch';

window.$ = $;
window.jQuery = $;
window.jquery = $;

// NOTE: HAS to be after jquery, and HAS to be
// a require() for some odd reason
require('angular');

window.UAParser = UAParser;
window._ = _;
window.Selectize = Selectize;
window.Plotly = Plotly;
window.jsondiffpatch = jsondiffpatch;
window.diff_match_patch = diff_match_patch;
