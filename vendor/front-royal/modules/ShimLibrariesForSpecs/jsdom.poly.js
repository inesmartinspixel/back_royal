//---------------------------
// JSDOM Polyfills
//---------------------------

window.URL.createObjectURL = () => 'https://some-test-url';
window.URL.revokeObjectURL = () => {};
window.open = () => {};

window.HTMLCanvasElement.prototype.getContext = function () {
    return {
        fillRect() {},
        clearRect() {},
        getImageData(x, y, w, h) {
            return {
                data: new Array(w * h * 4),
            };
        },
        putImageData() {},
        createImageData() {
            return [];
        },
        setTransform() {},
        drawImage() {},
        save() {},
        fillText() {},
        restore() {},
        beginPath() {},
        moveTo() {},
        lineTo() {},
        closePath() {},
        stroke() {},
        translate() {},
        scale() {},
        rotate() {},
        arc() {},
        fill() {},
        measureText() {
            return {
                width: 0,
            };
        },
        transform() {},
        rect() {},
        clip() {},
    };
};

window.HTMLCanvasElement.prototype.toDataURL = function () {
    return '';
};

function noOp() {}
if (typeof window.URL.createObjectURL === 'undefined') {
    Object.defineProperty(window.URL, 'createObjectURL', {
        value: noOp,
    });
}
