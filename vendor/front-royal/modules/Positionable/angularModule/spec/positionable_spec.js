import 'AngularSpecHelper';
import 'Positionable/angularModule';

describe('Positionable dir', () => {
    let elem;
    let scope;
    let SpecHelper;
    let positionableScope;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('Positionable', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should position the element', () => {
        render({
            x: 13,
            y: 233,
        });
        expect(elem.css('left')).toBe('13px');
        expect(elem.css('top')).toBe('233px');
    });

    it('should respect posUnits', () => {
        render({
            x: 13,
            y: 33,
            units: '%',
        });

        // .css() will never return % value
        expect(elem[0].style.left).toBe('13%');
        expect(elem[0].style.top).toBe('33%');

        expect(() => {
            render({
                x: 13,
                y: 33,
                units: 'invalid',
            }).toThrow(new Error(''));
        });
    });

    it('should not be draggable if posMovable is false', () => {
        render();
        expect(elem.attr('draggable')).toBe('false');
    });

    it('should be draggable if posMovable is false', () => {
        render({
            movable: true,
        });
        expect(elem.attr('draggable')).toBe('true');
    });

    it('should set px values when drag ends', () => {
        render({
            x: 100,
            y: 100,
            movable: true,
        });
        positionableScope.onPositionableDragStop({
            dragAndDrop: {
                movedX: 10,
                movedY: 10,
            },
        });
        scope.$digest();
        expect(scope.x).toBe(110);
        expect(scope.y).toBe(110);
        expect(elem.css('left')).toBe('110px');
        expect(elem.css('top')).toBe('110px');
    });

    it('should set % values when drag ends', () => {
        render({
            x: 5,
            y: 5,
            units: '%',
            movable: true,
        });

        jest.spyOn($.prototype, 'height').mockReturnValue(100);
        jest.spyOn($.prototype, 'width').mockReturnValue(100);

        const initialXPixels = elem.position().left;
        const initialYPixels = elem.position().top;

        // drag the element over 10px and down 10px
        positionableScope.onPositionableDragStop({
            dragAndDrop: {
                movedX: 10,
                movedY: 10,
            },
        });
        scope.$digest();

        // I tried to explicitly set the width and height of the parent
        // to make this a little less abstract, but strange things went wrong.
        // So, instead, I'm just going to look up the size of the parent
        // and calculate the expected values from that.
        const newXPixels = initialXPixels + 10;
        const newYPixels = initialYPixels + 10;
        const parent = elem.offsetParent();

        SpecHelper.expectVeryClose(100 * (newXPixels / parent.width()), scope.x, 0.001);
        SpecHelper.expectVeryClose(100 * (newYPixels / parent.height()), scope.y, 0.001);
        SpecHelper.expectVeryClose(scope.x, Number(elem[0].style.left.replace('%', ''), 0.001));
        SpecHelper.expectVeryClose(scope.y, Number(elem[0].style.top.replace('%', '')), 0.001);
    });

    describe('resizing', () => {
        it('should support resizing', () => {
            render({
                resizable: true,
                resizeWidth: 100,
                resizeHeight: 100,
            });
            $timeout.flush();

            const resizeTarget = elem.find('.resize_this');
            expect(resizeTarget.width()).toBe(100);
            expect(resizeTarget.height()).toBe(100);

            const resizeHandle = SpecHelper.expectElement(elem, 'resize-handle');

            // start the drag
            resizeHandle.isolateScope().onResizeStart();
            scope.$apply();

            // dragmove event
            resizeHandle.isolateScope().previewResize({
                dragAndDrop: {
                    movedX: 20,
                    movedY: -20,
                },
            });
            scope.$apply();

            // the target element should have been resized, but the model
            // should not yet have been updated
            expect(resizeTarget.width()).toBe(120);
            expect(resizeTarget.height()).toBe(80);
            expect(scope.resizeWidth).toBe(100);
            expect(scope.resizeHeight).toBe(100);

            resizeHandle.isolateScope().onResized({
                dragAndDrop: {
                    movedX: 30,
                    movedY: -30,
                },
            });
            scope.$apply();

            // the element should have been resized, and the
            // model should have been updated
            expect(resizeTarget.width()).toBe(130);
            expect(resizeTarget.height()).toBe(70);
            expect(scope.resizeWidth).toBe(130);
            expect(scope.resizeHeight).toBe(70);
        });

        it('should support resizing with percentages', () => {
            // I couldn't figure out how to test this.  Seems like the
            // parent always has no width and height
        });

        it('should support restricting the aspectRatio', () => {
            render({
                resizable: true,
                resizeWidth: 100,
                resizeHeight: 100,
                restrictAspectRatio: true,
            });
            $timeout.flush();

            const resizeTarget = elem.find('.resize_this');
            const resizeHandle = SpecHelper.expectElement(elem, 'resize-handle');

            resizeHandle.isolateScope().onResizeStart();
            scope.$apply();
            resizeHandle.isolateScope().onResized({
                dragAndDrop: {
                    movedX: 30,
                    movedY: -30,
                },
            });
            scope.$apply();

            // the element should have been resized, and the
            // model should have been updated
            expect(resizeTarget.width()).toBe(70);
            expect(resizeTarget.height()).toBe(70);
            expect(scope.resizeWidth).toBe(70);
            expect(scope.resizeHeight).toBe(70);
        });

        it('should fire afterResized', () => {
            const callback = jest.fn();
            render({
                resizable: true,
                resizeWidth: 100,
                resizeHeight: 100,
                afterResized: callback,
            });
            $timeout.flush();

            const resizeHandle = SpecHelper.expectElement(elem, 'resize-handle');

            resizeHandle.isolateScope().onResizeStart();
            scope.$apply();
            const evt = {
                dragAndDrop: {
                    movedX: 30,
                    movedY: -30,
                },
            };
            resizeHandle.isolateScope().onResized(evt);
            scope.$apply();

            expect(callback).toHaveBeenCalledWith(evt);
        });
    });

    function render(options) {
        const parentDiv = $('<div></div>');
        parentDiv.css({
            id: 'parent',
            position: 'relative',
        });
        const renderer = SpecHelper.renderer();
        renderer.scope.posMovable = false;
        angular.extend(renderer.scope, options);

        let html = '<div positionable pos-x="x" pos-y="y" pos-units="units" pos-movable="movable" ';
        html = `${html} pos-resizable="resizable" pos-resize-width="resizeWidth" pos-resize-height="resizeHeight" `;
        html = `${html} pos-resize-target=".resize_this" pos-after-resized="afterResized($event)" `;
        html = `${html} pos-restrict-aspect-ratio="restrictAspectRatio" `;
        html = `${html} ><div class="resize_this"></div></div`;
        renderer.render(html, parentDiv);
        elem = renderer.elem.find('[positionable]');
        scope = renderer.scope;
        positionableScope = renderer.childScope;
    }
});
