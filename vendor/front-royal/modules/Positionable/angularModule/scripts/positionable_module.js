import 'AspectRatioEnforcer/angularModule';
import 'DragAndDrop/angularModule';

export default angular.module('Positionable', ['DragAndDrop', 'AspectRatioEnforcer']);
