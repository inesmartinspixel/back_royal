import React from 'react';
import { mount } from 'enzyme';
import { default as ReactDatePicker, default as ReactDatepicker } from 'react-datepicker';
import moment from 'moment-timezone';
import { Field } from 'formik';

import { MockForm } from 'FrontRoyalForm/specHelpers';
import DatePicker from './DatePicker';

describe('<DatePicker>', () => {
    let elem;

    afterEach(() => {
        elem && elem.unmount();
    });

    describe('onChange', () => {
        it('should work', () => {
            const selectedDate = new Date('01/01/1942');
            elem = render(null);

            elem.find(ReactDatepicker).props().onChange(selectedDate);
            elem.update();
            expect(elem.find(DatePicker).props().form.values.myDate).toEqual(selectedDate);
        });
    });

    describe('onBlur', () => {
        it('should work', () => {
            elem = render(null);

            elem.find(ReactDatepicker).props().onBlur();
            elem.update();
            expect(elem.find(DatePicker).props().form.touched.myDate).toEqual(true);
        });
    });

    describe('calendar button', () => {
        it('should be visible if the value is null and initialize the value on click', () => {
            elem = render(null);
            expect(elem.find('.hide-datepicker').props().style.display).toEqual('none');
            expect(elem.exists('button')).toBe(true);
            elem.find('button').simulate('click');

            elem.update();
            expect(elem.find('.hide-datepicker').props().style.display).toEqual('block');

            // check that we autofocused the input after clicking the button
            expect(elem.find(ReactDatePicker).props().autoFocus).toBe(true);
        });

        it('should be hidden if value is not null and show the picker', () => {
            elem = render(new Date('2018/01/01'));
            expect(elem.find('.hide-datepicker').props().style.display).toEqual('block');
            expect(elem.exists('button')).toBe(false);

            // check that we do not autofocus the input
            expect(elem.find(ReactDatePicker).props().autoFocus).toBe(false);
        });
    });

    describe('dateFormat and timeFormat', () => {
        it('should include the timezone', () => {
            jest.spyOn(moment.tz, 'guess').mockReturnValue('America/New_York');

            // EDT
            elem = render(new Date('2018/07/01'));
            expect(elem.find(ReactDatePicker).props().dateFormat).toEqual("MMMM d, yyyy h:mm aa 'EDT'");
            expect(elem.find(ReactDatePicker).props().timeFormat).toEqual("h:mm aa 'EDT'");

            // EST
            elem = render(new Date('2018/01/01'));
            expect(elem.find(ReactDatePicker).props().dateFormat).toEqual("MMMM d, yyyy h:mm aa 'EST'");
            expect(elem.find(ReactDatePicker).props().timeFormat).toEqual("h:mm aa 'EST'");
        });
    });

    function MyForm(props) {
        return (
            <MockForm {...props}>
                <Field component={DatePicker} name="myDate" />
            </MockForm>
        );
    }

    function render(initialValue) {
        return mount(<MyForm initialValues={{ myDate: initialValue }} />);
    }
});
