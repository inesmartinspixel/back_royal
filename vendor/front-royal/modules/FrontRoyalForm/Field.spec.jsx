import React from 'react';
import { Formik, Form, Field as FormikField } from 'formik';
import { mount } from 'enzyme';
import Field from './Field';

jest.mock(
    './addFormValidationErrorClass.js',
    () =>
        function (name, formik, className) {
            return ['error', `addFormValidationErrorClass called with ${name}, ${className}`];
        },
);

describe('Field', () => {
    describe('value default', () => {
        it('should default text field to an empty string if undefined', () => {
            const wrapper = mount(
                <MockForm initialValues={{}}>
                    <Field name="prop" />
                </MockForm>,
            );
            expect(wrapper.find(FormikField).props().value).toEqual('');
        });

        it('should default text field to an empty string if null', () => {
            const wrapper = mount(
                <MockForm initialValues={{ prop: null }}>
                    <Field name="prop" />
                </MockForm>,
            );
            expect(wrapper.find(FormikField).props().value).toEqual('');
        });

        it('should leave a defined text field untouched', () => {
            const wrapper = mount(
                <MockForm initialValues={{ prop: 'okay' }}>
                    <Field name="prop" />
                </MockForm>,
            );
            expect(wrapper.find(FormikField).props().value).toBeUndefined();
        });

        // Before we started using `getIn`, this test would fail
        it('should leave a defined text field untouched if in a nested value', () => {
            const wrapper = mount(
                <MockForm initialValues={{ sub: { prop: 'okay' } }}>
                    <Field name="sub.prop" />
                </MockForm>,
            );
            expect(wrapper.find(FormikField).props().value).toBeUndefined();
        });
    });

    describe('className', () => {
        it('should add the form validation error class', () => {
            const wrapper = mount(
                <MockForm>
                    <Field name="prop" className="some-class" />
                </MockForm>,
            );
            expect(wrapper.find(FormikField).props().className).toEqual(
                'addFormValidationErrorClass called with prop, some-class field',
            );
        });
    });
});

// In order for `connect` to send a formik object down into
// our fields, we have to wrap these in a form
function MockForm(props) {
    return (
        <Formik
            initialValues={props.initialValues || {}}
            render={() => (
                <Form className="front-royal-form-container admin-edit-billing-transaction">{props.children}</Form>
            )}
        />
    );
}
