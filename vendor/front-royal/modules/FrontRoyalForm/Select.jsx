/*
    front-royal-select provides a wrapper around react-select.

    It implements the standard behavior from frontRoyalField
    and also styles the component.
*/

import { default as ReactSelect } from 'react-select';
import oreo from 'Oreo';
import React from 'react';

function Select(props) {
    const getOptionValue = props.getOptionValue || (option => option.value);

    const {
        options,
        field: { name, value },
        form,
    } = props;

    const selectedOption = options ? options.find(option => getOptionValue(option) === value) : '';

    props = {
        ...props,
        name,
        options,
        value: selectedOption,

        isDisabled: props.disabled,

        // Add the react-select className
        className: `${props.className} react-select`,

        // Integrate with formik
        onChange: option => {
            form.setFieldValue(name, getOptionValue(option));
        },
        onBlur() {
            form.setFieldTouched(name);
        },

        // Style the element using the react-select styling api
        classNamePrefix: 'react-select',
        theme: theme => ({
            ...theme,
            borderRadius: 4,
            colors: {
                ...theme.colors,
                primary: oreo.COLOR_V3_BEIGE_DARKEST,
                primary75: oreo.COLOR_V3_BEIGE_DARKER,
                primary50: oreo.COLOR_V3_BEIGE_DARK,
                primary25: oreo.COLOR_V3_BEIGE_MIDDARK,

                danger: oreo.COLOR_V3_RED,
                dangerLight: oreo.COLOR_V3_CORAL,

                neutral0: oreo.COLOR_V3_WHITE,
                neutral5: oreo.COLOR_V3_BEIGE_LIGHTER,
                neutral10: oreo.COLOR_V3_BEIGE_MEDIUM,

                // neutral20 is the border-color, which should match the border color
                // in front_royal_form_variables.scss#$SMARTLY_FORM_BORDER_STYLE
                neutral20: oreo.COLOR_V3_BEIGE_MIDDARK,
                neutral30: oreo.COLOR_V3_BEIGE_MIDDARK,
                neutral40: oreo.COLOR_V3_BEIGE_DARK,
                neutral50: oreo.COLOR_V3_BEIGE_DARK,
                neutral60: oreo.COLOR_V3_BEIGE_DARKER,
                neutral70: oreo.COLOR_V3_BEIGE_DARKEST,
                neutral80: oreo.COLOR_V3_BEIGE_BEYOND_DARK,
                neutral90: oreo.COLOR_V3_BEIGE_BEYOND_BEYOND_DARK,
            },
        }),
        styles: {
            singleValue: base => ({
                ...base,
                color: oreo.COLOR_V3_BLACK,
            }),
        },
    };

    return <ReactSelect {...props} />;
}

export default Select;
