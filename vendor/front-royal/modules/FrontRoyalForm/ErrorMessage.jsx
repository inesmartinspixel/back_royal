import React from 'react';
import ReactIf from 'ReactIf';
import { ErrorMessage as FormikErrorMessage, getIn, connect } from 'formik';

const ErrorMessage = connect(props => {
    const { showWhenUntouched, formik, name, className } = props;
    let errorMessage;
    if (showWhenUntouched) {
        errorMessage = getIn(formik.errors, name);
    }
    return (
        <div className={`error-message ${className}`}>
            <ReactIf if={!showWhenUntouched}>
                <FormikErrorMessage {...props} />
            </ReactIf>
            <ReactIf if={errorMessage}>{errorMessage}</ReactIf>
        </div>
    );
});

export default ErrorMessage;
