import React from 'react';
import { mount } from 'enzyme';
import { Formik, Form, Field } from 'formik';
import { default as ReactSelect } from 'react-select';
import Select from './Select';

describe('<Select>', () => {
    let elem;

    afterEach(() => {
        elem && elem.unmount();
    });

    describe('onChange', () => {
        it('should work', () => {
            const selectedOption = { value: 'newValue' };
            elem = mount(
                <MyForm>
                    <Field name="myProp" component={Select} options={[selectedOption]} />
                </MyForm>,
            );

            elem.find(ReactSelect).props().onChange(selectedOption);
            elem.update();
            expect(elem.find(Select).props().form.values.myProp).toEqual('newValue');
        });
    });

    describe('onBlur', () => {
        it('should work', () => {
            const selectedOption = { value: 'newValue' };
            elem = mount(
                <MyForm>
                    <Field name="myProp" component={Select} options={[selectedOption]} />
                </MyForm>,
            );

            elem.find(ReactSelect).props().onBlur();
            elem.update();
            expect(elem.find(Select).props().form.touched.myProp).toEqual(true);
        });
    });

    describe('className', () => {
        it('should add react-select to any provided className', () => {
            elem = mount(
                <MyForm>
                    <Field className="my-class" component={Select} name="myProp" />
                </MyForm>,
            );

            // For some reason I do not understand, enzyme is finding
            // multiple elements that match .my-class.react-select.  But,
            // at least confirms there is at least 1.
            expect(elem.find('.my-class').some('.react-select')).toBe(true);
        });
    });

    describe('initialValue', () => {
        it('should use the value in initialValues to select the indicated option', () => {
            const options = [
                { label: 'One', value: 1 },
                { label: 'Two', value: 2 },
            ];
            elem = mount(
                <MyForm initialValues={{ myProp: 2 }}>
                    <Field component={Select} name="myProp" options={options} />
                </MyForm>,
            );

            // See https://github.com/airbnb/enzyme/issues/836 for an explanation of hostNodes
            expect(elem.find('.react-select__single-value').hostNodes().text()).toEqual('Two');
        });

        it('should respect getOptionValue', () => {
            const options = [
                { label: 'One', thisIsTheValue: 1 },
                { label: 'Two', thisIsTheValue: 2 },
            ];
            elem = mount(
                <MyForm initialValues={{ myProp: 2 }}>
                    <Field
                        component={Select}
                        name="myProp"
                        options={options}
                        getOptionValue={option => option.thisIsTheValue}
                    />
                </MyForm>,
            );
            expect(elem.find('.react-select__single-value').hostNodes().text()).toEqual('Two');
        });
    });

    // In order for `connect` to send a formik object down into
    // our fields, we have to wrap these in a form
    function MyForm(props) {
        return <Formik initialValues={props.initialValues || {}} render={() => <Form>{props.children}</Form>} />;
    }
});
