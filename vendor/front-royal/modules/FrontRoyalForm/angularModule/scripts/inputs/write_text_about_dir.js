import angularModule from 'FrontRoyalForm/angularModule/scripts/front_royal_form_module';
import template from 'FrontRoyalForm/angularModule/views/inputs/write_text_about.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('writeTextAbout', [
    '$injector',
    function factory() {
        return {
            restrict: 'E',
            scope: {
                ngModel: '=',
                placeholder: '<?',
                minlength: '<?',
                maxlength: '<?',
                disabled: '<',
                shrinkToInput: '<',
                textareaClass: '@?',
            },
            require: '?^ngModel',
            templateUrl,

            link(scope, elem, attrs, modelController) {
                //---------------------------
                // Initialization
                //---------------------------

                // Keep in mind - http://stackoverflow.com/a/17076323/1747491
                scope.maxlength = scope.maxlength || 150;

                scope.isRequired = angular.isDefined(attrs.required);

                scope.$watch('ngModel', newVal => {
                    if (scope.minlength && newVal && newVal.length < scope.minlength) {
                        modelController.$setValidity('minlength', false);
                    } else {
                        modelController.$setValidity('minlength', true);
                    }

                    if (newVal && newVal.length > scope.maxlength) {
                        modelController.$setValidity('maxlength', false);
                    } else {
                        modelController.$setValidity('maxlength', true);
                    }
                });
            },
        };
    },
]);
