import angularModule from 'FrontRoyalForm/angularModule/scripts/front_royal_form_module';
import template from 'FrontRoyalForm/angularModule/views/inputs/choose_an_item.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('chooseAnItem', [
    '$injector',
    function factory($injector) {
        const TranslationHelper = $injector.get('TranslationHelper');
        const translationHelper = new TranslationHelper('front_royal_form.inputs.choose_an_item');
        const locationPlaceDetails = $injector.get('LOCATION_PLACE_DETAILS');

        return {
            restrict: 'E',
            scope: {
                // the multi-select directive replaces the ngModel when
                // an item is removed, thus two-way binding
                ngModel: '=',
                itemKey: '@',
                placeholderText: '<?',
                mode: '<?',
                min: '<?',
                max: '<?',
                disableOrdering: '<?',
                allowCreate: '<?',
                allOption: '<?',
                displaySelectedOptions: '<?',
                shouldDisable: '<?',
                useLabelAsValue: '<?',
            },
            require: '?^ngModel',
            templateUrl,

            link(scope, elem, attrs) {
                let fieldOptionsTranslationHelper;
                let optionKeys;

                if (_.contains(['student_network_looking_for', 'student_network_interest'], scope.itemKey)) {
                    fieldOptionsTranslationHelper = new TranslationHelper('student_network.field_options');
                } else if (_.contains(['industry', 'location', 'place'], scope.itemKey)) {
                    fieldOptionsTranslationHelper = new TranslationHelper('careers.field_options');
                } else {
                    throw 'Unsupported item key';
                }

                if (scope.itemKey === 'student_network_looking_for') {
                    optionKeys = $injector.get('STUDENT_NETWORK_LOOKING_FOR_KEYS');
                } else if (scope.itemKey === 'student_network_interest') {
                    optionKeys = $injector.get('STUDENT_NETWORK_INTEREST_KEYS').popular_interests;
                } else if (scope.itemKey === 'industry') {
                    optionKeys = $injector.get('CAREERS_INDUSTRY_KEYS');
                } else if (scope.itemKey === 'location') {
                    optionKeys = _.without(
                        $injector.get('CAREERS_LOCATIONS_OF_INTEREST_KEYS'),
                        'flexible',
                        'show_all_locations',
                    );
                } else if (scope.itemKey === 'place') {
                    optionKeys = _.without(
                        $injector.get('CAREERS_LOCATIONS_OF_INTEREST_KEYS'),
                        'flexible',
                        'show_all_locations',
                    );
                } else {
                    throw 'Unsupported item key';
                }

                //-------------------------------
                // Global Config
                //-------------------------------

                scope.isRequired = angular.isDefined(attrs.required);
                scope.placeholderText = scope.placeholderText || translationHelper.get(scope.itemKey);
                scope.mode = attrs.mode || 'select';
                scope.displaySelectedOptions = angular.isDefined(scope.displaySelectedOptions)
                    ? scope.displaySelectedOptions
                    : true;

                //-------------------------------
                // Multi-Select Config
                //-------------------------------

                scope.min = scope.min || 0;
                scope.disableOrdering = scope.disableOrdering || false;
                scope.allowCreate = scope.allowCreate || false;

                scope.getOptionLabel = option => {
                    if (scope.useLabelAsValue) {
                        return option;
                    }

                    return fieldOptionsTranslationHelper.get(option);
                };

                scope.getOptionValue = option => {
                    if (scope.itemKey === 'place') {
                        return locationPlaceDetails[option];
                    }
                    return option;
                };

                scope.orderByLabel = option => option.label;

                //-------------------------------
                // Initialization
                //-------------------------------

                const values = scope.useLabelAsValue
                    ? _.map(optionKeys, key => fieldOptionsTranslationHelper.get(key))
                    : optionKeys;

                scope.options =
                    scope.mode === 'multi'
                        ? values
                        : _.map(optionKeys, key => ({
                              value: key,
                              label: fieldOptionsTranslationHelper.get(key),
                          }));
            },
        };
    },
]);
