import 'AngularSpecHelper';
import 'FrontRoyalForm/angularModule';
import selectSkillsFormLocales from 'Careers/locales/careers/edit_career_profile/select_skills_form-en.json';
import selectedPillsLocales from 'FrontRoyalForm/locales/front_royal_form/selected_pills-en.json';
import inputConstAutosuggestLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/input_const_autosuggest-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(selectSkillsFormLocales, selectedPillsLocales, inputConstAutosuggestLocales);

describe('FrontRoyal.form.PillsSelect', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let pills;
    const numDefaultPillsInSections = 75;
    const numDefaultSections = 5;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Form', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);

        SpecHelper.stubCurrentUser();
    });

    function render(params = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = pills;
        renderer.scope.maxPills = params.maxPills;
        renderer.render('<pills-select ng-model="ngModel" locale-key="skills" max-pills="maxPills"></pills-select>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    function populatePills(numPills) {
        pills = [];

        for (let i = 0; i < numPills; i++) {
            pills.push({
                locale: 'en',
                text: 'CMS',
            });
        }
    }

    describe('maxPills', () => {
        it('should not disable input when maxPills has not been reached', () => {
            populatePills(1);
            render({
                maxPills: 2,
            });
            expect(scope.ngModel.length).toBe(1);
            SpecHelper.expectElementEnabled(elem, 'input');
        });

        it('should disable input when maxPills is reached', () => {
            populatePills(1);
            render({
                maxPills: 1,
            });
            expect(scope.ngModel.length).toBe(1);
            SpecHelper.expectElementDisabled(elem, 'input');
        });
    });

    it('should toggle a pill properly', () => {
        populatePills(10);
        render();
        // add a new pill
        scope.togglePill('angularjs');
        expect(scope.ngModel.length).toBe(11);
        expect(_.chain(pills).pluck('text').indexOf('AngularJS').value()).toBe(10);
        // remove that pill
        scope.togglePill('angularjs');
        expect(scope.ngModel.length).toBe(10);
        expect(_.chain(pills).pluck('text').indexOf('AngularJS').value()).toBe(-1);
    });

    it('should display pills and sections properly', () => {
        // clear out any skills so we're left with just the default selections
        populatePills(0);
        render();
        SpecHelper.expectElements(elem, '.pills-container', numDefaultSections);
        SpecHelper.expectElements(elem, '.pill', numDefaultPillsInSections);
    });

    describe('indexOfPillTranslation', () => {
        beforeEach(() => {
            populatePills(1);
            render();
        });

        it('should return index of pill', () => {
            expect(scope.indexOfPillTranslation('cms')).toBe(0);
        });

        it('should return -1 if pill not found in ngModel', () => {
            // crm = localekey in skills locales, but not one in our ngModel
            expect(scope.indexOfPillTranslation('crm')).toBe(-1);
        });
    });

    describe('maxPills', () => {
        it('should not disable pills when maxPills has not been reached', () => {
            populatePills(1);
            render({
                maxPills: 2,
            });
            expect(scope.ngModel.length).toBe(1);
            SpecHelper.expectElementDoesNotHaveClass(elem.find('.pill'), 'disabled');
        });

        it('should disable pills when maxPills is reached', () => {
            populatePills(1);
            render({
                maxPills: 1,
            });
            expect(scope.ngModel.length).toBe(1);
            SpecHelper.expectElementHasClass(elem.find('.pill'), 'disabled');
        });

        it('should not add pill to ngModel when maxPills has been reached', () => {
            populatePills(0);
            render({
                maxPills: 1,
            });
            expect(scope.ngModel.length).toBe(0);
            scope.togglePill('angularjs');
            expect(scope.ngModel.length).toBe(1);
            scope.$apply();
            SpecHelper.expectElementHasClass(elem.find('.pill'), 'disabled');
            scope.togglePill('react');
            expect(scope.ngModel.length).toBe(1); // should not add react
        });
    });
});
