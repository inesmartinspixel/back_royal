import 'AngularSpecHelper';
import 'FrontRoyalForm/angularModule';
import 'Users/angularModule/spec/_mock/fixtures/users';
import setSpecLocales from 'Translation/setSpecLocales';
import uploadAvatarLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/upload_avatar-en.json';

setSpecLocales(uploadAvatarLocales);

describe('FrontRoyal.Form.Inputs.UploadAvatar', () => {
    let $injector;
    let SpecHelper;
    let Upload;
    let renderer;
    let elem;
    let scope;
    let $q;
    let user;
    let $window;
    let ErrorLogService;
    let LinkedinOauthService;
    let User;
    let Capabilities;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Form', 'SpecHelper', 'FrontRoyal.Careers');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                Upload = $injector.get('Upload');
                $q = $injector.get('$q');
                $window = $injector.get('$window');
                ErrorLogService = $injector.get('ErrorLogService');
                LinkedinOauthService = $injector.get('LinkedinOauthService');
                User = $injector.get('User');
                Capabilities = $injector.get('Capabilities');
            },
        ]);

        $injector.get('UserFixtures');
        SpecHelper.stubDirective('careersAvatar');

        user = User.fixtures.getInstance({
            avatar_url: 'http://originalUrl',
            career_profile: {
                avatar_url: 'http://originalUrl',
            },
        });
    });

    afterEach(() => {
        delete $window.IN;
        delete $window.CORDOVA;
        delete $window.Camera;
        delete navigator.camera;
    });

    function render(options = {}) {
        const instructions = 'testing instructions';

        renderer = SpecHelper.renderer();
        renderer.scope.mainInstructions = instructions;
        renderer.scope.isRequired = options.isRequired;
        renderer.scope.model = user.avatar_url;
        renderer.scope.user = user;
        renderer.scope.disableLinkedin = options.disableLinkedin;

        renderer.render(
            '<form name="formName"><upload-avatar model="model" main-instructions="mainInstructions" is-required="isRequired" disable-linkedin="disableLinkedin" user="user"></upload-avatar></form>',
        );
        elem = renderer.elem;
        scope = elem.find('upload-avatar').isolateScope();
    }

    it('should show instructions if passed in', () => {
        render();
        SpecHelper.expectElementText(elem, '.main-instructions', 'testing instructions');
    });

    it('should have an upload button', () => {
        render();
        SpecHelper.expectElement(elem, '[name="avatarUpload"]');
    });

    describe('isRequired', () => {
        it('should be required by default', () => {
            render();
            expect(scope.avatarSrc).toBe(undefined);
            SpecHelper.expectElement(elem, 'input[type="hidden"]');
            SpecHelper.expectElementAttr(elem, 'input[type="hidden"]', 'required', 'required');
        });

        it('should not be required if isRequired attribute is false', () => {
            const options = {
                isRequired: false,
            };
            render(options);
            expect(scope.isRequired).toBe(false);
            SpecHelper.expectElement(elem, 'input[type="hidden"]');
            SpecHelper.expectElementAttr(elem, 'input[type="hidden"]', 'required', undefined);

            // While we are loading, however, we should set required to true
            // in order to invalidate the input
            scope.loading = true;
            scope.$digest();
            SpecHelper.expectElementAttr(elem, 'input[type="hidden"]', 'required', 'required');
        });

        it('should be required if isRequired attribute is true', () => {
            render({
                isRequired: true,
            });
            SpecHelper.expectElement(elem, 'input[type="hidden"]');
            SpecHelper.expectElementAttr(elem, 'input[type="hidden"]', 'required', 'required');
        });
    });

    describe('image uploading', () => {
        describe('in a browser context', () => {
            let deferred;
            let file;

            beforeEach(() => {
                // mock completion of an image upload
                deferred = $q.defer();

                // mock in prep for image upload
                jest.spyOn(Upload, 'upload').mockReturnValue(deferred.promise);
                jest.spyOn(user, 'avatarUploadUrl').mockReturnValue('fake upload url');

                file = {
                    name: 'image1',
                    size: 2 * 1024 * 1024,
                };
            });

            it('should support uploading from the filesystem', () => {
                // mock the start of an image upload
                render();
                scope.onFileSelect(file);

                // check that upload was called
                expect(Upload.upload).toHaveBeenCalledWith({
                    'FrontRoyal.ApiErrorHandler': {
                        skip: true,
                    },
                    url: 'fake upload url',
                    data: {
                        avatar_image: file,
                    },
                    supportedFormatsForErrorMessage: '.jpg, .png, .svg, .gif',
                });

                // trigger the successful upload
                const response = {
                    status: 200,
                    data: {
                        contents: {
                            avatar: {
                                url: 'original_url.jpg',
                            },
                        },
                    },
                };

                // while we are uploading, the model should be null
                expect(scope.model).toBe(null);

                // and the spinner should be spinning
                scope.$digest();
                SpecHelper.expectElement(elem, 'front-royal-spinner');

                deferred.resolve(response);
                scope.$apply();

                const url = response.data.contents.avatar.url;
                expect(scope.user.avatar_url).toEqual(url);
                expect(scope.user.career_profile.avatar_url).toEqual(url);
                expect(renderer.scope.model).toEqual(url);
                SpecHelper.expectNoElement(elem, 'front-royal-spinner');
            });

            it('should reset avatar if response is not 200', () => {
                // mock the start of an image upload
                render();
                const origUrl = scope.model;
                expect(origUrl).not.toBeUndefined();
                scope.onFileSelect(file);

                // check that upload was called
                expect(Upload.upload).toHaveBeenCalled();

                // trigger the successful upload
                const response = {
                    status: 406,
                };

                // while we are uploading, the model should be null
                expect(scope.model).toBe(null);
                deferred.resolve(response);
                scope.$apply();

                expect(scope.user.avatar_url).toEqual(origUrl);
                expect(scope.user.career_profile.avatar_url).toEqual(origUrl);
                expect(scope.model).toEqual(origUrl);
            });

            it('should reset avatar on error', () => {
                // mock the start of an image upload
                render();
                const origUrl = scope.model;
                expect(origUrl).not.toBeUndefined();
                scope.onFileSelect(file);

                // check that upload was called
                expect(Upload.upload).toHaveBeenCalled();

                // trigger the successful upload
                const response = {
                    status: 406,
                };

                // while we are uploading, the model should be null
                expect(scope.model).toBe(null);
                deferred.reject(response);
                scope.$apply();

                expect(scope.user.avatar_url).toEqual(origUrl);
                expect(scope.user.career_profile.avatar_url).toEqual(origUrl);
                expect(scope.model).toEqual(origUrl);
            });
        });

        describe('in a Cordova context', () => {
            beforeEach(() => {
                $window.CORDOVA = true;
                navigator.camera = {
                    getPicture: angular.noop,
                };
                $window.Camera = {
                    PictureSourceType: {
                        CAMERA: 1,
                        PHOTOLIBRARY: 0,
                    },
                    DestinationType: {
                        FILE_URI: 0,
                    },
                };

                jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
            });

            it('should handle "No Camera Available" errors', () => {
                let callCount = 0;
                jest.spyOn(navigator.camera, 'getPicture').mockImplementation((onSuccess, onFailure) => {
                    callCount++;
                    if (callCount === 1) {
                        onFailure('No Camera Available');
                    }
                });
                render();
                SpecHelper.expectElements(elem, '[name="captureDevicePicture"]', 2);
                SpecHelper.click(elem, '[name="captureDevicePicture"]', 0);
                expect(callCount).toBe(2); // calls again for library access
                SpecHelper.expectElements(elem, '[name="captureDevicePicture"]', 1);
                expect(ErrorLogService.notify).not.toHaveBeenCalled();
            });

            it('should handle "has no access to camera" errors', () => {
                let callCount = 0;
                jest.spyOn(navigator.camera, 'getPicture').mockImplementation((onSuccess, onFailure) => {
                    callCount++;
                    if (callCount === 1) {
                        onFailure('has no access to camera');
                    }
                });
                render();
                SpecHelper.expectElements(elem, '[name="captureDevicePicture"]', 2);
                SpecHelper.click(elem, '[name="captureDevicePicture"]', 0);
                expect(callCount).toBe(1);
                SpecHelper.expectElements(elem, '[name="captureDevicePicture"]', 2);
                SpecHelper.expectElementText(
                    elem,
                    '.sub-text.saved-auto.red',
                    'Please enable camera or library access to use this feature.',
                );
                expect(ErrorLogService.notify).not.toHaveBeenCalled();
            });

            it('should handle "no image selected" errors', () => {
                let callCount = 0;
                jest.spyOn(navigator.camera, 'getPicture').mockImplementation((onSuccess, onFailure) => {
                    callCount++;
                    if (callCount === 1) {
                        onFailure('no image selected');
                    }
                });
                render();
                SpecHelper.expectElements(elem, '[name="captureDevicePicture"]', 2);
                SpecHelper.click(elem, '[name="captureDevicePicture"]', 0);
                expect(callCount).toBe(1);
                SpecHelper.expectElements(elem, '[name="captureDevicePicture"]', 2);
                SpecHelper.expectNoElement(elem, '.sub-text.saved-auto.red');
                expect(ErrorLogService.notify).not.toHaveBeenCalled();
            });

            it('should handle "has no access to assets" errors', () => {
                let callCount = 0;
                jest.spyOn(navigator.camera, 'getPicture').mockImplementation((onSuccess, onFailure) => {
                    callCount++;
                    if (callCount === 1) {
                        onFailure('has no access to assets');
                    }
                });
                render();
                SpecHelper.expectElements(elem, '[name="captureDevicePicture"]', 2);
                SpecHelper.click(elem, '[name="captureDevicePicture"]', 0);
                expect(callCount).toBe(1);
                SpecHelper.expectElements(elem, '[name="captureDevicePicture"]', 2);
                SpecHelper.expectNoElement(elem, '.sub-text.saved-auto.red');
                expect(ErrorLogService.notify).not.toHaveBeenCalled();
            });

            it('should handle unknown errors', () => {
                let callCount = 0;
                jest.spyOn(navigator.camera, 'getPicture').mockImplementation((onSuccess, onFailure) => {
                    callCount++;
                    if (callCount === 1) {
                        onFailure('some_unknown_error');
                    }
                });
                render();
                SpecHelper.expectElements(elem, '[name="captureDevicePicture"]', 2);
                SpecHelper.click(elem, '[name="captureDevicePicture"]', 0);
                expect(callCount).toBe(1);
                SpecHelper.expectElements(elem, '[name="captureDevicePicture"]', 2);
                SpecHelper.expectNoElement(elem, '.sub-text.saved-auto.red');
                expect(ErrorLogService.notify).toHaveBeenCalledWith(
                    'Failed to capture a camera image in Cordova',
                    undefined,
                    {
                        error: 'some_unknown_error',
                    },
                );
            });
        });
    });

    describe('LinkedIn sync', () => {
        let syncDeferred;

        beforeEach(() => {
            syncDeferred = $q.defer();
            jest.spyOn(LinkedinOauthService, 'getProfile').mockImplementation(() => syncDeferred.promise);
            jest.spyOn(Capabilities, 'cookies', 'get').mockImplementation(() => true);
        });

        it('should hide in Cordova', () => {
            $window.CORDOVA = true;
            render();
            SpecHelper.expectNoElement(elem, '#sync-from-linked-in');
            SpecHelper.expectNoElement(elem, 'front-royal-spinner');
        });

        it('should hide if disableLinkedin is true', () => {
            render({
                disableLinkedin: true,
            });
            SpecHelper.expectNoElement(elem, '#sync-from-linked-in');
            SpecHelper.expectNoElement(elem, 'front-royal-spinner');
        });

        it('should hide if Capabilities.cookies is false', () => {
            jest.spyOn(Capabilities, 'cookies', 'get').mockImplementation(() => false);
            render();
            SpecHelper.expectNoElement(elem, '#sync-from-linked-in');
            SpecHelper.expectNoElement(elem, 'front-royal-spinner');
        });

        it('should re-enable and show message on error', () => {
            render();
            SpecHelper.click(elem, '[id="sync-from-linked-in"]');

            // while we are uploading, the model should be null
            expect(scope.model).toBe(null);

            jest.spyOn(scope.user, 'save').mockImplementation(() => {
                scope.$digest();
            });

            syncDeferred.reject({
                errorDescription: 'some error',
            });
            scope.$digest();

            assertEnabled();
            SpecHelper.expectElementText(elem, '.linkedin.error', 'Error syncing from LinkedIn');
            expect(scope.user.save).not.toHaveBeenCalled();
            expect(scope.user.avatar_url).toEqual('http://originalUrl');
            expect(scope.user.career_profile.avatar_url).toEqual('http://originalUrl');
        });

        it('should set the url when complete', () => {
            render();
            SpecHelper.expectNoElement(elem, 'front-royal-spinner');
            SpecHelper.click(elem, '[id="sync-from-linked-in"]');

            // while we are uploading, the model should be null
            expect(scope.model).toBe(null);

            jest.spyOn(scope.user, 'save').mockImplementation(() => {});

            const url = 'http://avatar';
            syncDeferred.resolve({
                pictureUrl: url,
            });
            assertEnabled();
            expect(scope.model).toEqual(url);
            expect(scope.user.avatar_url).toEqual(url);
            expect(scope.user.career_profile.avatar_url).toEqual(url);
            expect(scope.user.save).toHaveBeenCalled();
        });

        it('should unset error message if a second try is successful', () => {
            render();

            // Simulate an error having happened already
            scope.linkedinMessage = 'The boogeyman stole your data packets';
            scope.$digest();

            User.expect('update');
            SpecHelper.click(elem, '[id="sync-from-linked-in"]');
            syncDeferred.resolve({
                pictureUrl: 'http://avatar',
            });

            scope.$digest();
            expect(scope.linkedinMessage).toBeUndefined();
            SpecHelper.expectNoElement(elem, '.linkedin.error');
        });

        it('should show message and reset avatar_url when we get no profileUrl back form a successful API response', () => {
            render();

            SpecHelper.click(elem, '[id="sync-from-linked-in"]');
            expect(scope.model).toBe(null);

            jest.spyOn(scope.user, 'save').mockImplementation(() => {});

            syncDeferred.resolve({});
            scope.$digest();

            expect(scope.linkedinMessage).toBe('LinkedIn was unable to find a profile picture');
            expect(scope.user.save).not.toHaveBeenCalled();
            expect(scope.user.avatar_url).toEqual('http://originalUrl');
            expect(scope.user.career_profile.avatar_url).toEqual('http://originalUrl');
        });

        it('should dirty form', () => {
            render();
            jest.spyOn(scope.formController, '$setDirty');

            SpecHelper.click(elem, 'button#sync-from-linked-in');
            expect(scope.formController.$setDirty).toHaveBeenCalled();
        });

        function assertEnabled() {
            scope.$digest();
            SpecHelper.expectNoElement(elem, 'front-royal-spinner');
            SpecHelper.expectElementEnabled(elem, '[name="avatarUpload"]');
            SpecHelper.expectElementEnabled(elem, '#sync-from-linked-in');
        }
    });
});
