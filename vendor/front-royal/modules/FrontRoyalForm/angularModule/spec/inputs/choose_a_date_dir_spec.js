import 'AngularSpecHelper';
import 'FrontRoyalForm/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import chooseADateLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/choose_a_date-en.json';

setSpecLocales(chooseADateLocales);

describe('FrontRoyal.Form.Inputs.ChooseADate', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Form', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);
    });

    function render(params = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = params.ngModel || null;
        renderer.scope.allowCurrent = params.allowCurrent;
        renderer.scope.allowFuture = params.allowFuture;
        renderer.scope.maxDate = params.maxDate;
        renderer.scope.minDate = params.minDate;
        renderer.scope.dayStrategy = params.dayStrategy;
        renderer.render(
            `<choose-a-date ng-model="ngModel" allow-current="allowCurrent" allow-future="allowFuture" max-date="maxDate" min-date="minDate" day-strategy="${params.dayStrategy}"></choose-a-date>`,
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    function selectAndAssert(year, month, day, hours = 0, minutes = 0, milli = 0) {
        let select = elem.find('select[name="month"]');
        // the first option is a placeholder, so eq(1) gives us January
        let val = elem.find('select[name="month"] option:eq(1)').val();
        select.val(val).change();

        select = elem.find('select[name="year"]');
        // the first option is a placeholder, so eq(1) gives us this year
        val = elem.find('select[name="year"] option:eq(1)').val();
        select.val(val).change();

        const modelAsDate = new Date(scope.ngModel);
        expect(modelAsDate.getUTCMilliseconds()).toBe(milli);
        expect(modelAsDate.getUTCMinutes()).toBe(minutes);
        expect(modelAsDate.getUTCHours()).toBe(hours);
        expect(modelAsDate.getUTCDate()).toBe(day);
        expect(modelAsDate.getUTCMonth()).toBe(month);
        expect(modelAsDate.getUTCFullYear()).toBe(year);
    }

    describe('date input', () => {
        it('should set model to a UTC timestamp when month and year is set', () => {
            render();
            selectAndAssert(new Date().getUTCFullYear(), 0, 1);
        });

        describe('dayStrategy', () => {
            it('should support beginningOfMonth', () => {
                render({
                    dayStrategy: 'beginningOfMonth',
                });
                selectAndAssert(new Date().getUTCFullYear(), 0, 1);
            });

            it('should support endOfMonth', () => {
                render({
                    dayStrategy: 'endOfMonth',
                });
                selectAndAssert(new Date().getUTCFullYear(), 0, 31, 23, 59, 999);
            });

            it('should support no dayStrategy', () => {
                render({
                    dayStrategy: null,
                });
                selectAndAssert(new Date().getUTCFullYear(), 0, 1);
            });
        });
    });

    describe('current', () => {
        it('should not show if allowCurrent is not set', () => {
            render();
            SpecHelper.expectHasClass(elem, '[name="current-container"]', 'ng-hide');
        });

        it('should show if allowCurrent is set', () => {
            render({
                allowCurrent: true,
            });
            SpecHelper.expectDoesNotHaveClass(elem, '[name="current-container"]', 'ng-hide');
        });

        it('should set ngModel to null and should be valid if checked', () => {
            const date = new Date();
            render({
                ngModel: date,
                allowCurrent: true,
            });
            expect(scope.ngModel).toBe(date);
            SpecHelper.toggleCheckbox(elem, '[name="current"]');
            expect(scope.ngModel).toBe(null);

            const modelController = elem.controller('ngModel');
            expect(modelController.$valid).toBe(true);
        });
    });

    describe('future', () => {
        it('should provide additional run off dates if allowFuture is set', () => {
            render({
                allowFuture: true,
            });
            const currentYear = new Date().getUTCFullYear();
            SpecHelper.expectElementText(elem, 'select:eq(1) option:eq(1)', (currentYear + 2).toString());
        });

        it('should not provide additional run off dates if allowFuture is not set', () => {
            render({
                allowFuture: false,
            });
            const currentYear = new Date().getUTCFullYear();
            SpecHelper.expectElementText(elem, 'select:eq(1) option:eq(1)', currentYear.toString());
        });
    });

    describe('maxDate', () => {
        it('should be valid if date is less than maxDate', () => {
            const ngModel = new Date('December 2014');
            const maxDate = new Date('December 2015');
            render({
                ngModel: Date.UTC(ngModel.getUTCMonth(), ngModel.getUTCFullYear()),
                maxDate: Date.UTC(maxDate.getUTCMonth(), maxDate.getUTCFullYear()),
            });
            SpecHelper.expectElementHasClass(elem, 'ng-valid-max-date');
        });

        it('should be invalid if date is more than maxDate', () => {
            const ngModel = new Date('December 2015');
            const maxDate = new Date('December 2014');
            render({
                ngModel: Date.UTC(ngModel.getUTCMonth(), ngModel.getUTCFullYear()),
                maxDate: Date.UTC(maxDate.getUTCMonth(), maxDate.getUTCFullYear()),
            });
            SpecHelper.expectElementHasClass(elem, 'ng-invalid-max-date');
        });

        it('should be valid if maxDate is null', () => {
            const ngModel = new Date('December 2014');
            render({
                ngModel: Date.UTC(ngModel.getUTCMonth(), ngModel.getUTCFullYear()),
                maxDate: null,
            });
            SpecHelper.expectElementHasClass(elem, 'ng-valid-max-date');
        });
    });

    describe('minDate', () => {
        it('should be valid if date is more than minDate', () => {
            const ngModel = new Date('December 2015');
            const minDate = new Date('December 2014');
            render({
                ngModel: Date.UTC(ngModel.getUTCMonth(), ngModel.getUTCFullYear()),
                minDate: Date.UTC(minDate.getUTCMonth(), minDate.getUTCFullYear()),
            });
            SpecHelper.expectElementHasClass(elem, 'ng-valid-min-date');
        });

        it('should be invalid if date is less than minDate', () => {
            const ngModel = new Date('December 2014');
            const minDate = new Date('December 2015');
            render({
                ngModel: Date.UTC(ngModel.getUTCMonth(), ngModel.getUTCFullYear()),
                minDate: Date.UTC(minDate.getUTCMonth(), minDate.getUTCFullYear()),
            });
            SpecHelper.expectElementHasClass(elem, 'ng-invalid-min-date');
        });

        it('should be valid if minDate is null', () => {
            const ngModel = new Date('December 2014');
            render({
                ngModel: Date.UTC(ngModel.getUTCMonth(), ngModel.getUTCFullYear()),
                minDate: null,
            });
            SpecHelper.expectElementHasClass(elem, 'ng-valid-min-date');
        });
    });
});
