import 'AngularSpecHelper';
import 'FrontRoyalForm/angularModule';

describe('FrontRoyal.Form.Inputs.SelectWithOther', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Form', 'SpecHelper');

        window.IGNORE_TRANSLATION_ERRORS = true;

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);
    });

    afterEach(() => {
        window.IGNORE_TRANSLATION_ERRORS = false;
    });

    function render(params = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = params.ngModel || [];
        renderer.scope.optionKeys = params.optionKeys || [
            {
                value: 'foo',
                label: 'Foo',
            },
            {
                value: 'bar',
                label: 'Bar',
            },
        ];

        renderer.render(
            '<select-with-other ng-model="ngModel" select-placeholder="select placeholder text" other-placeholder="other placeholder text" option-keys="optionKeys"></select-with-other>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should add "other" key if not passed in', () => {
        render({
            optionKeys: ['foo', 'bar'],
        });
        expect(scope.optionKeys).toEqual(['foo', 'bar', 'other']);
    });

    it('should not show Other if one of the options is selected', () => {
        render();
        SpecHelper.updateSelect(elem, 'select', 'foo');
        scope.$digest();
        SpecHelper.expectElementHidden(elem, 'input');
    });

    it('should show Other if one of the options is not selected and ngModel is not empty', () => {
        render({
            ngModel: 'notanoption',
        });
        SpecHelper.expectElement(elem, 'input');
    });

    it('should initialize placeholder message for select input', () => {
        render();
        SpecHelper.expectElementText(elem, 'option[disabled][hidden]', 'select placeholder text');
    });

    it('should initialize placeholder message for Other input', () => {
        render();
        SpecHelper.expectElementAttr(elem, 'input', 'placeholder', 'other placeholder text');
    });

    it('should support passing option-labels directly', () => {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = [];
        renderer.scope.optionLabels = ['foo', 'bar'];

        renderer.render(
            '<select-with-other ng-model="ngModel" select-placeholder="select placeholder text" other-placeholder="placeholder text" option-labels="optionLabels"></select-with-other>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();

        SpecHelper.updateSelect(elem, 'select', 'foo');
    });

    it('should support selectize-mode', () => {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = [];
        renderer.scope.optionLabels = ['foo', 'bar'];

        renderer.render(
            '<select-with-other ng-model="ngModel" select-placeholder="select placeholder text" other-placeholder="placeholder text" option-labels="optionLabels" selectize-mode="true"></select-with-other>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();

        SpecHelper.updateSelectize(elem, 'selectize', 'foo');
    });
});
