import 'AngularSpecHelper';
import 'FrontRoyalForm/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import addAnItemLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/add_an_item-en.json';

setSpecLocales(addAnItemLocales);

describe('FrontRoyal.Form.Inputs.AddAnItem', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let isMobile;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Form', 'SpecHelper');

        angular.mock.module($provide => {
            isMobile = jest.fn();
            isMobile.mockReturnValue(false);
            $provide.value('isMobile', isMobile);
        });

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);
    });

    function render(params = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = params.ngModel;
        renderer.scope.placeholder = params.placeholder;
        renderer.scope.addText = params.addText;
        renderer.scope.doneText = params.doneText;
        renderer.scope.min = params.min;
        renderer.scope.max = params.max;
        renderer.scope.inPlaceEdit = params.inPlaceEdit;
        renderer.scope.inputTabindex = params.inputTabindex;
        renderer.scope.showTooltip = params.showTooltip;
        renderer.scope.autoSuggest = params.autoSuggest;
        renderer.scope.autoSuggestHandler = params.autoSuggestHandler;
        renderer.scope.autoSuggestTemplateUrl = params.autoSuggestTemplateUrl;
        renderer.render(
            '<add-an-item required ' +
                'ng-model="ngModel" ' +
                'placeholder="placeholder" ' +
                'add-text="addText" ' +
                'done-text="doneText" ' +
                'min="min" ' +
                'max="max" ' +
                'max-length="maxLength" ' +
                'in-place-edit="inPlaceEdit" ' +
                'input-tabindex="inputTabindex" ' +
                'show-tooltip="showTooltip" ' +
                'show-errors="showErrors" ' +
                'auto-suggest="autoSuggest" ' +
                'auto-suggest-handler="autoSuggestHandler"> ' +
                'auto-suggest-template-url="autoSuggestTemplateUrl"> ' +
                '</add-an-item>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('form', () => {
        it('should not be valid if required and more than max', () => {
            render({
                ngModel: ['foo', 'bar', 'baz'],
                max: 2,
            });

            const modelController = elem.controller('ngModel');
            expect(modelController.$valid).toBe(false);
        });
    });

    describe('scope parameters', () => {
        it('should render with empty model array', () => {
            render();
        });

        it('should render with already defined model array', () => {
            render({
                ngModel: ['foo', 'bar'],
            });
            SpecHelper.expectElements(elem, '.item', 2);
        });
    });

    describe('input area', () => {
        it('should add an item when the button is clicked', () => {
            render();
            SpecHelper.updateInput(elem, 'input[name="item-to-be-added"]', 'TEST');
            SpecHelper.click(elem, 'button[name="add-item"]');
            expect(scope.ngModel).toEqual(['TEST']);
        });

        it('should use passed in placeholder text', () => {
            render({
                placeholder: 'insert foo',
            });
            SpecHelper.expectElement(elem, '[placeholder="insert foo"]');
        });
    });

    describe('items area', () => {
        it('should remove item if delete icon is clicked', () => {
            render({
                ngModel: ['foo'],
            });
            SpecHelper.click(elem, '[name="remove-item"]:eq(0)');
            expect(scope.ngModel).toEqual([]);
        });

        describe('in-place-edit', () => {
            it('should toggle into editing mode when the item is selected', () => {
                render({
                    ngModel: ['foo'],
                    inPlaceEdit: true,
                });

                SpecHelper.expectNoElement(elem, '.commit-checkbox');

                SpecHelper.click(elem, '.click-container');

                SpecHelper.expectElement(elem, '.commit-checkbox');
            });

            it('should change item when button is pressed', () => {
                render({
                    ngModel: ['foo'],
                    inPlaceEdit: true,
                });
                SpecHelper.click(elem, '.click-container');

                SpecHelper.updateTextArea(elem, 'textarea[name="update-item"]', 'bar');
                SpecHelper.click(elem, '[name="save-update"]');
                expect(scope.ngModel).toEqual(['bar']);
            });
        });

        describe('movement', () => {
            it('should allow items to move up', () => {
                render({
                    ngModel: ['foo', 'bar'],
                });
                SpecHelper.click(elem, '[name="arrow-up"]:eq(1)');
                expect(scope.ngModel).toEqual(['bar', 'foo']);
            });

            it('should allow items to move down', () => {
                render({
                    ngModel: ['foo', 'bar'],
                });
                SpecHelper.click(elem, '[name="arrow-down"]:eq(0)');
                expect(scope.ngModel).toEqual(['bar', 'foo']);
            });

            it('should handle attempted movement outside of bounds', () => {
                render({
                    ngModel: ['foo', 'bar'],
                });
                jest.spyOn(scope, 'canMove').mockReturnValue(true);
                scope.$digest();

                SpecHelper.click(elem, '[name="arrow-up"]:eq(0)');
                expect(scope.ngModel).toEqual(['foo', 'bar']);

                SpecHelper.click(elem, '[name="arrow-down"]:eq(1)');
                expect(scope.ngModel).toEqual(['foo', 'bar']);
            });
        });

        describe('autoSuggest', () => {
            it('should support autoSuggest mode', () => {
                render({
                    autoSuggest: true,
                    autoSuggestHandler: angular.noop,
                    ngModel: ['foo', 'bar'],
                });
                SpecHelper.updateInput(elem, 'input[name="item-to-be-added"][uib-typeahead]', 'TEST');
            });
            it('should require a handler', () => {
                expect(() => {
                    render({
                        autoSuggest: true,
                        ngModel: ['foo', 'bar'],
                    });
                }).toThrow(new Error('Cannot operate in auto-suggest mode without autoSuggestHandler being provided.'));
            });
        });
    });
});
