import 'AngularSpecHelper';
import 'FrontRoyalForm/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import organizationAutocompleteLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/organization_autocomplete-en.json';

setSpecLocales(organizationAutocompleteLocales);

describe('FrontRoyal.Form.Inputs.OrganizationAutocomplete', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Form', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);
    });

    function render(params = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = params.ngModel;
        renderer.scope.locale = params.locale;
        renderer.scope.tabindex = params.tabindex;
        renderer.scope.placeholderText = params.placeholderText;
        renderer.scope.shouldDisable = params.shouldDisable;
        renderer.render(
            '<organization-autocomplete ng-model="ngModel" tabindex="tabindex" locale="locale" ' +
                'placeholder-text="placeholderText" options-type="\'professional_organization\'" should-disable="shouldDisable"></organization-autocomplete>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should initialize ngModel if not passed in', () => {
        render();
        expect(scope.ngModel).toEqual({
            text: undefined,
            locale: 'en',
        });
    });

    it('should initialize ngModel with passed in locale value if present', () => {
        render({
            locale: 'es',
        });
        expect(scope.ngModel).toEqual({
            text: undefined,
            locale: 'es',
        });
    });

    it('should use placeholderText if passed in', () => {
        render({
            placeholderText: 'Hello Placeholder',
        });
        SpecHelper.expectElement(elem, '[placeholder="Hello Placeholder"]');
    });

    it('should set tabindex if passed in', () => {
        render({
            tabindex: 5,
        });
        SpecHelper.expectElement(elem, '[tabindex="5"]');
    });

    it('should disable the input if shouldDisable', () => {
        render({
            shouldDisable: true,
        });
        SpecHelper.expectElementDisabled(elem, '[name="organization"]');
    });
});
