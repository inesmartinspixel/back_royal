import 'AngularSpecHelper';
import 'FrontRoyalForm/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import copyTextLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/copy_text-en.json';

setSpecLocales(copyTextLocales);

describe('FrontRoyal.Form.Inputs.CopyText', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Form', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
            },
        ]);

        document.getSelection = function () {
            return {
                removeAllRanges() {},
            };
        };
    });

    function render(params = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.value = params.value || 'testing 123';
        renderer.scope.buttonText = params.buttonText || 'copy_link';
        renderer.render('<copy-text value="value" button-text="{{buttonText}}"></copy-text>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should copy text', () => {
        render();
        document.execCommand = jest.fn();
        SpecHelper.click(elem, 'button');
        expect(document.execCommand).toHaveBeenCalledWith('copy');
        expect(scope.buttonKey).toBe('front_royal_form.inputs.copy_text.copied');
        $timeout.flush(2000);
        expect(scope.buttonKey).toBe('front_royal_form.inputs.copy_text.copy_link');
    });
});
