import 'AngularSpecHelper';
import 'FrontRoyalForm/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import chooseAnItemLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/choose_an_item-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';

setSpecLocales(chooseAnItemLocales, fieldOptionsLocales);

describe('FrontRoyal.Form.ChooseAnItem', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Form', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);
    });

    function render(params = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = params.ngModel || null;
        renderer.scope.allOption = params.allOption;
        const mode = params.mode || 'select';

        renderer.scope.shouldDisable = params.shouldDisable;

        renderer.render(
            `<choose-an-item item-key="industry" all-option="allOption" ng-model="ngModel" mode="${mode}" should-disable="shouldDisable"></choose-an-item>`,
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('select mode with all option', () => {
        it('should not disable the first option', () => {
            render({
                allOption: true,
                mode: 'select',
            });
            SpecHelper.expectElementAttr(elem, 'option:eq(0)', 'disabled');
        });
        it('should disable the first option', () => {
            render({
                allOption: false,
                mode: 'select',
            });
            SpecHelper.expectElementAttr(elem, 'option:eq(0)', 'disabled', 'disabled');
        });
    });

    describe('select mode', () => {
        it('should render a select box', () => {
            render({
                mode: 'select',
            });
            SpecHelper.expectNoElement(elem, '> selectize');
            SpecHelper.expectNoElement(elem, '> multi-select');

            const val = elem.find('option:eq(1)').val();

            SpecHelper.updateSelect(elem, '> select', val);

            const model = scope.ngModel;
            expect(model).toEqual(val);
        });

        it('should disable select if shouldDisable', () => {
            render({
                mode: 'select',
                shouldDisable: true,
            });
            SpecHelper.expectElementDisabled(elem, 'select');
        });
    });

    describe('multi-select mode', () => {
        it('should render a multi-select control', () => {
            render({
                mode: 'multi',
            });
            SpecHelper.expectNoElement(elem, '> select');
            SpecHelper.expectNoElement(elem, '> selectize');

            const val = elem.find('option:eq(1)').val();

            SpecHelper.updateMultiSelect(elem, '> multi-select', val);
            const model = scope.ngModel;
            expect(model).toEqual([val]);
        });
    });
});
