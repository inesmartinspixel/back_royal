import 'AngularSpecHelper';
import 'FrontRoyalForm/angularModule';

describe('FrontRoyal.Form.Inputs.onKeypressEnter', () => {
    let $injector;
    let SpecHelper;
    let elem;
    let e;
    let $rootScope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Form', 'SpecHelper', 'FrontRoyal.Careers');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $rootScope = $injector.get('$rootScope');
            },
        ]);

        e = $.Event('keypress');
        e.which = 13;
    });

    function render() {
        const renderer = SpecHelper.render('<input on-keypress-enter="function()">');
        elem = renderer.elem;
    }

    function renderNoAttrs() {
        const renderer = SpecHelper.render('<input on-keypress-enter="">');
        elem = renderer.elem;
    }

    it('should call scope.$eval when input is <enter>', () => {
        render();

        jest.spyOn($rootScope, '$eval').mockImplementation(() => {});

        elem.trigger(e);

        expect($rootScope.$eval).toHaveBeenCalledWith(expect.any(Function));
    });

    it('should not call scope.$eval when input is not <enter>', () => {
        render();

        jest.spyOn($rootScope, '$eval').mockImplementation(() => {});

        e.which++; // increment value
        elem.trigger(e);

        expect($rootScope.$eval).not.toHaveBeenCalled();
    });

    it('should not call scope.$eval if !attrs.onKeypressEnter', () => {
        renderNoAttrs();

        jest.spyOn($rootScope, '$eval').mockImplementation(() => {});

        elem.trigger(e);

        expect($rootScope.$eval).not.toHaveBeenCalled();
    });
});
