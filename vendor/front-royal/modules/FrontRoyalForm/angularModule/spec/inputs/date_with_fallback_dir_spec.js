import 'AngularSpecHelper';
import 'FrontRoyalForm/angularModule';

import * as userAgentHelper from 'userAgentHelper';
import moment from 'moment-timezone';

describe('FrontRoyal.Form.Inputs.DateWithFallback', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Form', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);
    });

    function render(params = {}) {
        if (params.android) {
            jest.spyOn(userAgentHelper, 'isAndroidDevice').mockReturnValue(true);
        } else {
            jest.spyOn(userAgentHelper, 'isAndroidDevice').mockReturnValue(false);
        }

        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = params.ngModel;
        renderer.scope.min = params.min;
        renderer.scope.max = params.max;
        renderer.scope.viewValue = params.viewValue;

        // For some reason I could not spy on $document.createElement in order to mock this out
        renderer.scope.inputDateSupported = params.inputDateSupported;

        renderer.render(
            '<date-with-fallback ng-model="ngModel" input-date-supported="inputDateSupported" min="min" max="max" view-value="viewValue"></date-with-fallback>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('inputDateSupported', () => {
        it('should be false if on Android device', () => {
            render({
                android: true,
            });
            expect(scope.inputDateSupported).toBe(false);
        });
    });

    describe('type=date supported', () => {
        const params = {
            inputDateSupported: true,
        };

        it('should initialize a proxy date model', () => {
            params.ngModel = '1986-06-25T12:00:00.000Z';
            render(params);
            scope.inputDateSupported = true;
            expect(scope.proxy.date).toEqual(new Date('1986-06-25T12:00:00.000Z'));
        });

        it('should set the ngModel correctly when setDate is called with a valid date', () => {
            render(params);

            // I would rather force the ng-change to happen but I couldn't get that to work
            // SpecHelper.updateTextInput(elem, 'input[type="date"]', '06/25/1986');
            // elem.find('input[type="date"]').triggerHandler('change');
            scope.setDate(new Date('1986-06-25T12:00:00.000Z'));
            expect(scope.ngModel).toEqual('1986/06/25');
        });

        it('should set the ngModel to null when setDate is called with an invalid date', () => {
            render(params);
            scope.setDate('notadate');
            expect(scope.ngModel).toBeNull();
        });

        it('should set the viewValue to undefined when setDate is called with an invalid date', () => {
            render(params);
            scope.setDate(undefined); // undefined is passed when the date is invalid
            expect(scope.viewValue).toBeUndefined();
        });
    });

    describe('type=date not supported', () => {
        const params = {
            inputDateSupported: false,
        };

        it('should initialize a date string', () => {
            params.ngModel = '1986-06-25T12:00:00.000Z';
            render(params);
            expect(scope.proxy.dateString).toEqual('06/25/1986');
        });

        it('should set the ngModel to a date when a valid string is being parsed', () => {
            render(params);

            // I would rather force the ng-change to happen but I couldn't get that to work
            scope.parseDateString('06/25/1986');
            expect(scope.ngModel).toEqual('1986/06/25');
        });

        it('should set the ngModel to null when an invalid string is being parsed', () => {
            render(params);
            scope.parseDateString('06/0');
            expect(scope.ngModel).toBeNull();
        });

        it('should set the viewValue to undefined when an invalid string is being parsed', () => {
            render(params);
            scope.setDate(undefined); // undefined is passed when the date is invalid
            expect(scope.viewValue).toBeUndefined();
        });
    });

    describe('isValid', () => {
        it('should not validate, but return true if no min or max is set', () => {
            render(); // no min or max is set by default
            expect(scope.isValid(moment(scope.ngModel))).toEqual(true);
        });

        // date and text fallback inputs require different formats
        const inputs = [
            {
                type: 'date',
                inputDateSupported: true,
                invalidMin: '2115-12-30',
                invalidMax: '1223-12-31',
            },
            {
                type: 'text',
                inputDateSupported: false,
                invalidMin: '12/30/2222',
                invalidMax: '12/31/1223',
            },
        ];

        inputs.forEach(input => {
            it(`should be invalid ${input.type} input if lower than min date`, () => {
                render({
                    inputDateSupported: input.inputDateSupported,
                    min: 22221231,
                });
                SpecHelper.updateTextInput(elem, 'input', input.invalidMin);
                SpecHelper.expectElementHasClass(elem, 'ng-invalid-min-max');
                expect(scope.ngModel).toBeNull();
            });

            it(`should be invalid ${input.type} input if higher than max date`, () => {
                render({
                    inputDateSupported: input.inputDateSupported,
                    max: 12221231,
                });
                SpecHelper.updateTextInput(elem, 'input', input.invalidMax);
                SpecHelper.expectElementHasClass(elem, 'ng-invalid-min-max');
                expect(scope.ngModel).toBeNull();
            });

            it(`should be invalid ${input.type} input if not between the min and max dates`, () => {
                render({
                    inputDateSupported: input.inputDateSupported,
                    min: 22221231,
                    max: 12221231,
                });
                SpecHelper.updateTextInput(elem, 'input', input.invalidMax);
                SpecHelper.expectElementHasClass(elem, 'ng-invalid-min-max');
                expect(scope.ngModel).toBeNull();
            });
        });
    });

    describe('viewValue', () => {
        it('should leave it undefined if ngModel does not have a value', () => {
            render();
            expect(scope.viewValue).toEqual(undefined);
        });

        it('should set it if ngModel has a value', () => {
            render({
                ngModel: '05/07/1983',
            });
            expect(scope.viewValue).toEqual('19830507'); // a different format is returned on purpose
        });
    });
});
