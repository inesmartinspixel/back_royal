import 'AngularSpecHelper';
import 'FrontRoyalForm/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import inputConstAutosuggestLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/input_const_autosuggest-en.json';
import selectSkillsFormLocales from 'Careers/locales/careers/edit_career_profile/select_skills_form-en.json';

setSpecLocales(inputConstAutosuggestLocales, selectSkillsFormLocales);

describe('FrontRoyal.Form.Inputs.InputConstAutosuggest', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);

        // These params could be anything, but using select_skills_form.html as an example here
        render({
            ngModel: [],
            optionsType: 'skills',
            translationFile: 'careers.edit_career_profile.select_skills_form',
            searchConstant: 'CAREERS_SKILLS_KEYS',
            maxLength: 35,
            maxItems: 3,
        });

        expect(scope.ngModel.length).toBe(0);
    });

    function render(params = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = params.ngModel;
        renderer.scope.optionsType = params.optionsType;
        renderer.scope.translationFile = params.translationFile;
        renderer.scope.searchConstant = params.searchConstant;
        renderer.scope.maxLength = params.maxLength;
        renderer.scope.maxItems = params.maxItems;

        renderer.render(
            '<input-const-autosuggest required ' +
                'ng-model="ngModel" ' +
                'options-type="optionsType" ' +
                'translation-file="translationFile" ' +
                'search-constant="searchConstant" ' +
                'max-length="maxLength" ' +
                'max-items="maxItems">' +
                '</input-const-autosuggest>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$digest();
    }

    function setNgModel(numberOfItems) {
        const items = [];
        for (let i = 0; i < numberOfItems; i++) {
            items.push({
                text: i.toString(),
                locale: 'en',
            });
        }
        scope.ngModel = items;
        scope.$digest();
    }

    it('should validate/invalidate the form when appropriate', () => {
        expect(scope.maxLength).toBe(35); // sanity
        scope.textLength = 36;
        scope.$digest();
        expect(scope.formController.$valid).toBe(false);
        scope.textLength = 35;
        scope.$digest();
        expect(scope.formController.$valid).toBe(true);
    });

    it('should disable input if maxItems limit is reached', () => {
        SpecHelper.expectElementEnabled(elem, '.selectize-input input');
        setNgModel(scope.maxItems); // set ngModel length equal to maxItems limit
        SpecHelper.expectElementDisabled(elem, '.selectize-input input');
        setNgModel(scope.maxItems + 1); // if somehow the maxItems limit is exceeded, we should still expect the input element to be disabled
        SpecHelper.expectElementDisabled(elem, '.selectize-input input');
    });

    describe('removeDisabledCharacters', () => {
        it('should return null if no input', () => {
            scope.disabledCharacters = ['#'];
            expect(scope.removeDisabledCharacters()).toBe(null);
        });

        it('should return input if no disabledCharacters', () => {
            expect(scope.removeDisabledCharacters('#foo$bar#')).toEqual('#foo$bar#');
        });

        it('should work', () => {
            scope.disabledCharacters = ['#', '$'];
            expect(scope.removeDisabledCharacters('#foo$bar#')).toEqual('foobar');
        });
    });

    describe('addItemToModel', () => {
        it('should not add an empty item to the model', () => {
            scope.addItemToModel('');
            expect(scope.ngModel.length).toBe(0);
            expect(scope.text).toBe(null);
        });

        it('should add an item to the model if it is not already included in model', () => {
            scope.addItemToModel('asdf');
            expect(scope.ngModel.length).toBe(1);
            expect(_.contains(_.pluck(scope.ngModel, 'text'), 'asdf')).toBe(true);
            expect(scope.text).toBe(null);
        });

        it('should not add an item to the model if it is already included in model', () => {
            scope.addItemToModel('asdf');
            expect(scope.ngModel.length).toBe(1);
            scope.addItemToModel('asdf');
            expect(scope.ngModel.length).toBe(1);
        });

        it('should not add an item to the model if it is already included in model with different capitalization', () => {
            scope.addItemToModel('asdf');
            expect(scope.ngModel.length).toBe(1);
            scope.addItemToModel('aSDf');
            expect(scope.ngModel.length).toBe(1);
        });

        it('should not add an item to the model if item length > maxLength', () => {
            const string = '123456789012345678901234567890123456'; // 36 chars
            scope.addItemToModel(string);
            expect(scope.ngModel.length).toBe(0);
            expect(_.contains(_.pluck(scope.ngModel, 'text'), string)).toBe(false);
        });

        it('should not add item to model if maxItems limit has been reached', () => {
            setNgModel(scope.maxItems);
            let originalLength = scope.ngModel.length;
            scope.addItemToModel(Math.random().toString()); // try to add a new item to the model
            expect(scope.ngModel.length).toEqual(originalLength);

            // if somehow the maxItems limit has been exceeded, we expect addItemToModel
            // not to add another item to the model
            setNgModel(scope.maxItems + 1);
            originalLength = scope.ngModel.length;
            scope.addItemToModel(Math.random().toString()); // try to add a new item to the model
            expect(scope.ngModel.length).toEqual(originalLength);
        });
    });
});
