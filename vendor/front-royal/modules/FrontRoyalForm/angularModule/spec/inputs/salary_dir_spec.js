import 'AngularSpecHelper';
import 'FrontRoyalForm/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import salaryLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/salary-en.json';

setSpecLocales(salaryLocales);

describe('FrontRoyal.Form.Inputs.Salary', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Form', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);
    });

    function render(params = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = params.ngModel;
        renderer.scope.base = params.base;
        renderer.scope.openToDiscussion = params.openToDiscussion;
        renderer.render('<salary ng-model="ngModel" base="base" open-to-discussion="openToDiscussion"></salary>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should set and unset input', () => {
        render();

        SpecHelper.toggleCheckbox(elem, '#salary-open');
        expect(scope.ngModel).toBe(85000);

        SpecHelper.toggleCheckbox(elem, '#salary-open');
        expect(scope.ngModel).toBe(undefined);
    });

    it('should check openToDiscussion even if a null value is initially passed through', () => {
        const params = {
            ngModel: null,
        };
        render(params);

        expect(scope.ngModel).toBe(params.ngModel);
        SpecHelper.expectCheckboxChecked(elem, '#salary-open');
    });

    it('should be set to model if provided', () => {
        const params = {
            ngModel: 50000,
        };
        render(params);

        expect(scope.ngModel).toBe(params.ngModel);

        SpecHelper.toggleCheckbox(elem, '#salary-open');
        expect(scope.ngModel).toBe(undefined);

        SpecHelper.toggleCheckbox(elem, '#salary-open');
        expect(scope.ngModel).toBe(85000);
    });

    it('should use provided base', () => {
        const params = {
            base: 100000,
        };
        render(params);

        SpecHelper.toggleCheckbox(elem, '#salary-open');
        expect(scope.ngModel).toBe(params.base);

        SpecHelper.toggleCheckbox(elem, '#salary-open');
        expect(scope.ngModel).toBe(undefined);
    });
});
