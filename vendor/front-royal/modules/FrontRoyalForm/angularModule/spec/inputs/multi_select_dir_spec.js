import 'AngularSpecHelper';
import 'FrontRoyalForm/angularModule';

describe('FrontRoyal.Form.Inputs.MultiSelect', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Form', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
            },
        ]);
    });

    function render(params = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = params.ngModel || [];
        renderer.scope.options = params.options || [
            {
                value: 'foo',
                label: 'Foo',
            },
            {
                value: 'bar',
                label: 'Bar',
            },
        ];
        renderer.scope.placeholderText = params.placeholderText || 'placeholder text';
        renderer.scope.min = _.has(params, 'min') ? params.min : 1;
        renderer.scope.max = _.has(params, 'max') ? params.max : 3;

        let ngRequiredString = '';
        if (angular.isDefined(params.ngRequired)) {
            renderer.scope.ngRequired = params.ngRequired;
            ngRequiredString = 'ng-required="ngRequired"';
        }

        const modelOrderByString = _.has(params, 'modelOrderBy') ? `model-order-by="${params.modelOrderBy}"` : '';

        const optionOrderByRenderString = params.optionOrderByRenderString || '';
        if (params.optionOrderBy && params.optionOrderByRenderString) {
            renderer.scope.optionOrderBy = params.optionOrderBy;
        }

        const selectedBehaviorString = _.has(params, 'selectedBehavior')
            ? `selected-behavior="'${params.selectedBehavior}'"`
            : '';

        renderer.scope.showEmpty = _.has(params, 'showEmpty') ? params.showEmpty : true;
        renderer.scope.allowCreate = params.allowCreate || false;
        renderer.scope.disableOrdering = params.disableOrdering || false;
        renderer.scope.disableRemove = params.disableRemove || false;
        renderer.scope.shouldDisable = params.shouldDisable;
        renderer.scope.inputType = params.inputType;
        if (renderer.scope.inputType === 'location-autocomplete') {
            renderer.scope.options = null;
        }
        renderer.scope.displaySelectedOptions = params.displaySelectedOptions;
        renderer.render(
            `<multi-select ng-model="ngModel" placeholder-text="placeholderText" min="min" max="max" options="options" show-empty="showEmpty" allow-create="allowCreate" option-value="$option.value" display-selected-options="displaySelectedOptions" option-label="$option.label" ${modelOrderByString} ${optionOrderByRenderString} disable-ordering="disableOrdering" disable-remove="disableRemove"${selectedBehaviorString} ${ngRequiredString} should-disable="shouldDisable" input-type="{{inputType}}"></multi-select>`,
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    function selectItem(index) {
        if (renderer.scope.allowCreate) {
            // Hack: assumes all option values are accessible by .value
            SpecHelper.updateSelectize(elem, 'selectize', scope.options[index - 1].value);
        } else {
            const select = elem.find('select');
            const val = elem.find(`option:eq(${index})`).val();
            select.val(val).change();
        }
    }

    function submitCustomItem(value) {
        if (renderer.scope.allowCreate) {
            // Since selectize doesn't fully work in this mode, this is good approximation to typing an entry in
            scope.proxy.itemToBeAdded = value;
            scope.$apply();
        } else {
            throw new Error("Can't submit custom item unless allowCreate is enabled!");
        }
    }

    it('should initialize placeholder message', () => {
        render();
        SpecHelper.expectElementText(elem, 'option:eq(0)', 'placeholder text');
        SpecHelper.expectElementDisabled(elem, 'option:eq(0)');
    });

    it('should have empty spaces for each empty spot up to max', () => {
        render({
            ngModel: ['foo'],
            max: 3,
        });
        SpecHelper.expectElements(elem, '.empty-item', 2); // max of 3 - foo
    });

    it('should handle a model that has length greater than max', () => {
        render({
            ngModel: ['foo', 'bar'],
            max: 1,
        });
        SpecHelper.expectElements(elem, '.item', 2);
    });

    it('should add an item to the model when selected', () => {
        render();
        selectItem(1);
        expect(scope.ngModel).toEqual(['foo']);
        expect(elem.find('select').val()).toBe(null); // should clear out the select
    });

    it('should delete an item when delete icon clicked', () => {
        render({
            ngModel: ['foo', 'bar'],
        });

        SpecHelper.expectElementText(elem, '.item', 'Foo', 0);
        SpecHelper.click(elem, '[name="remove-item"]:eq(0)');
        expect(scope.ngModel).toEqual(['bar']);

        SpecHelper.expectElementText(elem, '.item', 'Bar', 0);
        SpecHelper.click(elem, '[name="remove-item"]:eq(0)');
        expect(scope.ngModel).toEqual([]);

        SpecHelper.expectNoElement(elem, '.item');
    });

    it('should not have an option that is already present in the passed in model', () => {
        render({
            ngModel: ['foo'],
        });
        // initially, 2 options. The placeholder + 1 more.
        SpecHelper.expectElements(elem, 'option', 2);
    });

    it('should have an option that is already present in the passed in model if selectedBehavior is disable', () => {
        render({
            ngModel: ['foo'],
            selectedBehavior: 'disable',
        });
        // all the options + the placeholder
        SpecHelper.expectElements(elem, 'option', scope.options.length + 1);
    });

    it('should not have an option that was recently added to the model', () => {
        render();
        // initially, 3 options. The placeholder + 2 more.
        SpecHelper.expectElements(elem, 'option', 3);
        selectItem(1);
        SpecHelper.expectElements(elem, 'option', 2);
    });

    it('should have an option that was recently added to the model if selectedBehavior is disable', () => {
        render({
            selectedBehavior: 'disable',
        });
        // initially, 3 options. The placeholder + 2 more.
        SpecHelper.expectElements(elem, 'option', scope.options.length + 1);
        selectItem(1);
        SpecHelper.expectElements(elem, 'option', scope.options.length + 1);
    });

    it('should be disabled if max items already in list', () => {
        render();
        scope.max = 2;
        selectItem(1);
        selectItem(1); // the first item that was selected gets removed, so the second element becomes the first.
        SpecHelper.expectElementDisabled(elem, 'select');
    });

    it('should be disabled if greather than max items already in list', () => {
        render({
            ngModel: ['foo', 'bar'],
            max: 1,
        });
        SpecHelper.expectElementDisabled(elem, 'select');
    });

    it('should work with no max', () => {
        render({
            min: null,
            max: null,
            options: [
                {
                    value: 'foo',
                    label: 'Foo',
                },
                {
                    value: 'bar',
                    label: 'Bar',
                },
                {
                    value: 'bar3',
                    label: 'Bar3',
                },
                {
                    value: 'bar4',
                    label: 'Bar4',
                },
                {
                    value: 'bar5',
                    label: 'Bar5',
                },
                {
                    value: 'bar6',
                    label: 'Bar6',
                },
            ],
        });
        scope.$apply();
        expect(scope.ngModel.length).toEqual(0);
        SpecHelper.expectElements(elem, '.item', 0);
        for (let i = 0; i < 6; i++) {
            selectItem(1);
        }
        expect(scope.ngModel.length).toEqual(6);
        SpecHelper.expectElements(elem, '.item', 6);
    });

    it('should display currently selected options', () => {
        render();
        selectItem(2);
        expect(scope.ngModel[0]).toEqual('bar');
        SpecHelper.expectElementText(elem, '.item', 'Bar');
    });

    it('should work with non-scalar options', () => {
        render({
            options: [
                {
                    label: 'Foo',
                    value: {
                        name: 'foo',
                    },
                },
                {
                    label: 'Bar',
                    value: {
                        name: 'bar',
                    },
                },
            ],

            // Initially, if one of the options is selected, it
            // will probably be a copy of the thing in the list,
            // not identical.
            ngModel: [
                {
                    name: 'foo',
                },
            ],
        });
        expect(_.pluck(scope.ngModel, 'name')).toEqual(['foo']);
        SpecHelper.expectElementText(elem, '.item', 'Foo');

        selectItem(2);
        expect(_.pluck(scope.ngModel, 'name')).toEqual(['foo', 'bar']);
        SpecHelper.expectElementText(elem, '.item', 'Foo', 0);
        SpecHelper.expectElementText(elem, '.item', 'Bar', 1);

        SpecHelper.click(elem, '[name="remove-item"]:eq(0)');

        expect(_.pluck(scope.ngModel, 'name')).toEqual(['bar']);
        SpecHelper.expectElementText(elem, '.item', 'Bar');
    });

    // See coomment in convertValueToHashKey
    it('should work with non-scalar options with circular references and an id property', () => {
        const foo = {
            name: 'Foo',
            id: 'foo',
        };
        foo.self = foo;
        const bar = {
            name: 'Bar',
            id: 'bar',
        };
        bar.self = bar;

        // Initially, if one of the options is selected, it
        // will probably be a copy of the thing in the list,
        // not identical.
        const fooCopy = {
            name: 'Foo',
            id: 'foo',
        };
        fooCopy.self = fooCopy;
        render({
            options: [
                {
                    label: 'Foo',
                    value: foo,
                },
                {
                    label: 'Bar',
                    value: bar,
                },
            ],
            ngModel: [fooCopy],
        });
        expect(scope.ngModel).toEqual([fooCopy]);
        SpecHelper.expectElementText(elem, '.item', 'Foo');

        selectItem(2);
        expect(scope.ngModel).toEqual([fooCopy, bar]);
        SpecHelper.expectElementText(elem, '.item', 'Foo', 0);
        SpecHelper.expectElementText(elem, '.item', 'Bar', 1);

        SpecHelper.click(elem, '[name="remove-item"]:eq(0)');

        expect(scope.ngModel).toEqual([bar]);
        SpecHelper.expectElementText(elem, '.item', 'Bar');
    });

    it('should support reordering', () => {
        render();
        selectItem(1);
        selectItem(1);
        expect(scope.ngModel).toEqual(['foo', 'bar']);
        SpecHelper.expectElementText(elem, '.item', 'Foo', 0);
        SpecHelper.expectElementDisabled(elem, 'button.unstyled-button.arrow-up:eq(0)');
        SpecHelper.click(elem, '.arrow-down', 0);
        expect(scope.ngModel).toEqual(['bar', 'foo']);
        SpecHelper.expectElementText(elem, '.item', 'Bar', 0);
        SpecHelper.expectElementDisabled(elem, 'button.unstyled-button.arrow-down:eq(1)');
        SpecHelper.click(elem, '.arrow-up', 1);
        expect(scope.ngModel).toEqual(['foo', 'bar']);
        SpecHelper.expectElementText(elem, '.item', 'Foo', 0);
    });

    it('should allow for disabling ordering, and support an option-order-by attribute', () => {
        render({
            disableOrdering: true,
            modelOrderBy: '$option.label',
        });
        selectItem(1);
        selectItem(1);

        // we have no guarantees about the order of ngModel here
        expect(scope.ngModel.sort()).toEqual(['foo', 'bar'].sort());

        // but we do know how it should be displayed in the UI
        SpecHelper.expectElementText(elem, '.item', 'Bar', 0);
        SpecHelper.expectElementText(elem, '.item', 'Foo', 1);

        SpecHelper.expectElement(elem, '.multi-select.disable-ordering');
    });

    it('should allow for disabling the removal of a particular item', () => {
        render({
            disableRemove: disableRemove(),
        });
        selectItem(1);
        SpecHelper.expectElementDisabled(elem, '[name="remove"]');
    });

    function disableRemove() {
        return true;
    }

    it('should handle adding a value to the ngModel from outside of this directive', () => {
        render();
        scope.ngModel.push('unknown value');
        scope.$digest();
        SpecHelper.expectElementText(elem, '.item', 'unknown value', 0);
    });

    describe('with allowCreate enabled', () => {
        beforeEach(() => {
            render({
                allowCreate: true,
            });
        });

        it('should allow selection of entries', () => {
            selectItem(1);
            expect(scope.ngModel).toEqual(['foo']);
        });

        it('should allow typing in a new entry', () => {
            submitCustomItem('this is a test');
            expect(scope.ngModel).toEqual(['this is a test']);
        });

        it('should allow typing in a previously removed custom entry', () => {
            submitCustomItem('this is a test');
            expect(scope.ngModel).toEqual(['this is a test']);

            SpecHelper.expectElementText(elem, '.item', 'this is a test', 0);
            SpecHelper.click(elem, '[name="remove-item"]:eq(0)');
            $timeout.flush();
            expect(scope.ngModel).toEqual([]);

            submitCustomItem('this is a test');
            expect(scope.ngModel).toEqual(['this is a test']);
        });

        it('should prevent typing in a duplicate custom entry', () => {
            submitCustomItem('this is a test');
            expect(scope.ngModel).toEqual(['this is a test']);

            submitCustomItem('this is a test');
            expect(scope.ngModel).toEqual(['this is a test']);
        });

        it('should prevent typing in a duplicate of a previously selected option label', () => {
            selectItem(1);
            expect(scope.ngModel).toEqual(['foo']);

            submitCustomItem('Foo');
            expect(scope.ngModel).toEqual(['foo']);

            expect(scope.proxy.availableSelectizeOptions.length).toEqual(scope.options.length - 1);
        });

        it('should prevent typing in a duplicate of a previously selected option value', () => {
            selectItem(2);
            expect(scope.ngModel).toEqual(['bar']);

            submitCustomItem('bar');
            expect(scope.ngModel).toEqual(['bar']);

            expect(scope.proxy.availableSelectizeOptions.length).toEqual(scope.options.length - 1);
        });

        it('should convert a custom typed entry to an existing option if it matches its label or value', () => {
            submitCustomItem('Foo');
            expect(scope.ngModel).toEqual(['foo']);

            submitCustomItem('bar');
            expect(scope.ngModel).toEqual(['foo', 'bar']);

            expect(scope.proxy.availableSelectizeOptions.length).toEqual(scope.options.length - 2);
        });

        it('should properly render a mixture of pre-existing selected options and custom options', () => {
            render({
                allowCreate: true,
                ngModel: ['foo', 'bar', 'this is a test'],
            });

            SpecHelper.expectElementText(elem, '.item', 'Foo', 0);
            SpecHelper.expectElementText(elem, '.item', 'Bar', 1);
            SpecHelper.expectElementText(elem, '.item', 'this is a test', 2);
        });

        it('should sort options based on optionOrderBy function', () => {
            render({
                optionOrderBy(option) {
                    return option.label;
                },
                optionOrderByRenderString: 'option-order-by="optionOrderBy(option)"',
                options: [
                    {
                        value: 'zebra',
                        label: 'Zebra',
                    },
                    {
                        value: 'apple',
                        label: 'Apple',
                    },
                ],
            });

            selectItem(1);
            expect(scope.ngModel[0]).toEqual('apple');
            SpecHelper.expectElementText(elem, '.item', 'Apple');
        });
    });

    describe('shouldDisable', () => {
        it('should disable select', () => {
            render({
                shouldDisable: true,
            });
            SpecHelper.expectElementDisabled(elem, 'select');
        });

        it('should disable selectize', () => {
            render({
                inputType: 'selectize',
                shouldDisable: true,
            });
            SpecHelper.expectElementDisabled(elem, 'selectize');
        });
    });

    describe('displaySelectedOptions', () => {
        it('should display selected options when true', () => {
            render({
                ngModel: ['foo', 'bar'],
            });
            expect(scope.displaySelectedOptions).toBe(true);
            SpecHelper.expectElements(elem, '.item', 2);
        });

        it('should display selected options when false', () => {
            render({
                ngModel: ['foo', 'bar'],
                displaySelectedOptions: false,
            });
            expect(scope.displaySelectedOptions).toBe(false);
            SpecHelper.expectNoElement(elem, '.item');
        });
    });

    describe('required', () => {
        it('should re-validate on required ng-required value change', () => {
            render({
                ngRequired: false,
            });
            expect(elem.attr('required')).toBeUndefined();
            expect(elem.get(0).classList.contains('ng-valid')).toBe(true);
            renderer.scope.ngRequired = true;
            renderer.scope.$digest();
            expect(elem.get(0).classList.contains('ng-valid')).toBe(false);
        });
    });

    describe('location-autocomplete', () => {
        let HasLocation;
        beforeEach(() => {
            HasLocation = $injector.get('HasLocation');
            SpecHelper.stubDirective('locationAutocomplete');
            render({
                inputType: 'location-autocomplete',
            });
        });

        it('should work', () => {
            const place = {
                some: 'place',
            };
            jest.spyOn(HasLocation, 'locationString').mockImplementation(_place => {
                if (place === _place) {
                    return 'A good place';
                }
            });
            scope.proxy.placeIdToBeAdded = 'somePlaceId';
            scope.proxy.itemToBeAdded = place;
            scope.$digest();
            expect(scope.ngModel).toEqual([place]);
            SpecHelper.expectElementText(elem, '.item', 'A good place');
            expect(scope.proxy.itemToBeAdded).toBeUndefined();
        });

        it('should not allow a duplicated location', () => {
            const place = {
                some: 'place',
            };
            const cloned = _.clone(place);

            scope.proxy.placeIdToBeAdded = 'somePlaceId';
            scope.proxy.itemToBeAdded = place;
            scope.$digest();
            expect(scope.ngModel).toEqual([place]);

            scope.proxy.placeIdToBeAdded = 'somePlaceId';
            scope.proxy.itemToBeAdded = cloned;
            scope.$digest();
            expect(scope.ngModel).toEqual([place]);
        });
    });
});
