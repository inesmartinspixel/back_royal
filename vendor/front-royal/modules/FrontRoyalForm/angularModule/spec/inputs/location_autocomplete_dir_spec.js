import 'AngularSpecHelper';
import 'FrontRoyalForm/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import locationAutocompleteLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/location_autocomplete-en.json';

setSpecLocales(locationAutocompleteLocales);

describe('FrontRoyal.Form.Inputs.LocationAutocomplete', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let $window;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Form', 'SpecHelper');

        angular.mock.module($provide => {
            $window = {
                document: window.document,
                google: {
                    maps: {
                        places: {
                            Autocomplete() {},
                        },
                        event: {
                            addListener() {},
                            removeListener() {},
                        },
                    },
                },
            };

            $provide.value('$window', $window);
        });

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $window = $injector.get('$window');
            },
        ]);
    });

    function render(params = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = params.ngModel;
        renderer.scope.detailsModel = params.detailsModel;
        renderer.scope.selectTextOnFocus = params.selectTextOnFocus || false;
        renderer.render(
            '<location-autocomplete ng-model="ngModel" details-model="detailsModel" select-text-on-focus="selectTextOnFocus"></location-autocomplete>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$digest();
    }

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should render', () => {
        render();
        const input = SpecHelper.expectElement(elem, 'input[name="location"]');
        expect(input.attr('placeholder')).toEqual('Search for City ...');
        expect(input.attr('location-validation')).not.toBeUndefined();
    });

    it('should require a proper formatted address, with details and place backing to be valid', () => {
        render();
        const formattedAddress = 'Somewhere, CO, USA';

        // watcher should fire and update validity
        scope.detailsModel = {
            formatted_address: formattedAddress,
        };
        scope.$digest();
        SpecHelper.expectElement(elem, 'input[name="location"].ng-invalid');

        // watcher should fire
        scope.ngModel = '123abc';
        scope.$digest();
        SpecHelper.expectElement(elem, 'input[name="location"].ng-invalid');

        // watcher should fire
        scope.formattedAddress = formattedAddress;
        scope.$digest();
        SpecHelper.expectElement(elem, 'input[name="location"].ng-valid');
    });

    // This is needed for the integration with multi-select
    it('should clear out details model if placeDetails is cleared out', () => {
        render();
        scope.detailsModel = {
            formatted_address: 'formattedAddress',
        };
        scope.$digest();
        elem.find('input').val('formattedAddress');

        scope.detailsModel = null;
        scope.$digest();
        SpecHelper.expectInputValue(elem, 'input', undefined);
    });

    it('should clear ngModel and detailsModel if input value has been completely removed', () => {
        const ngModel = 'some_place_id';
        const detailsModel = {
            formatted_address: 'formattedAddress',
        };
        render({
            ngModel,
            detailsModel,
        });
        expect(elem.find('input').val()).toEqual('formattedAddress');
        scope.$digest();

        elem.find('input').val('f');
        scope.$digest();
        expect(scope.ngModel).toEqual(ngModel);
        expect(scope.detailsModel).toEqual(detailsModel);

        elem.find('input').val('');
        scope.$digest();
        expect(scope.ngModel).toBeUndefined();
        expect(scope.detailsModel).toBeUndefined();
    });

    it('should decode and sanitize adr_address', () => {
        const ngModel = 'some_place_id';
        const detailsModel = {
            formatted_address: 'formattedAddress',
            adr_address: '<span class="country-name">Côte d&#39;Ivoire</span>',
        };
        render({
            ngModel,
            detailsModel,
        });
        expect(elem.find('input').val()).toEqual('formattedAddress');
        scope.$digest();

        elem.find('input').val('f');
        scope.$digest();
        expect(scope.ngModel).toEqual(ngModel);
        expect(scope.detailsModel.adr_address).toEqual('<span class="country-name">Côte d\'Ivoire</span>');
    });

    it('should disable the input if shouldDisable is true', () => {
        render();
        SpecHelper.expectElementEnabled(elem, 'input');
        scope.shouldDisable = true;
        scope.$digest();
        SpecHelper.expectElementDisabled(elem, 'input');
    });

    it('should call onFocus when location input has been focused', () => {
        render();
        jest.spyOn(scope, 'onFocus').mockImplementation(() => {});
        scope.locationInput.trigger('focus');
        expect(scope.onFocus).toHaveBeenCalled();
    });

    describe('onFocus', () => {
        it('should select the text in the location input if selectTextOnFocus, and ngModel has been set', () => {
            render({
                ngModel: 'some_place_id',
                selectTextOnFocus: true,
            });

            // sanity checks
            expect(scope.ngModel).toEqual('some_place_id');
            expect(scope.selectTextOnFocus).toBe(true);

            jest.spyOn(scope.locationInput, 'select').mockImplementation(() => {});
            scope.onFocus();
            expect(scope.locationInput.select).toHaveBeenCalled();
            scope.locationInput.select.mockClear();

            scope.selectTextOnFocus = false;
            scope.onFocus();
            expect(scope.locationInput.select).not.toHaveBeenCalled();

            scope.selectTextOnFocus = true;
            scope.ngModel = undefined;
            scope.onFocus();
            expect(scope.locationInput.select).not.toHaveBeenCalled();
        });
    });
});
