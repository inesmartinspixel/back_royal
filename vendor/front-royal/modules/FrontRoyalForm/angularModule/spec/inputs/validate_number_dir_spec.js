import 'AngularSpecHelper';
import 'FrontRoyalForm/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import validateNumberLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/validate_number-en.json';

setSpecLocales(validateNumberLocales);

describe('FrontRoyal.Form.Inputs.ValidateNumber', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Form', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);
    });

    function render(params = {}) {
        renderer = SpecHelper.renderer();
        _.extend(
            renderer.scope,
            {
                ngModel: null,
                inputName: 'input-name',
                min: 100,
                max: 200,
                placeholder: 'foo',
                allowFloat: false,
                allowZero: false,
            },
            params,
        );

        renderer.render(
            `<form name="formName"><validate-number ng-model="ngModel" input-name="${renderer.scope.inputName}" min="min" max="max" placeholder="placeholder" allow-float="allowFloat" allow-zero="allowZero"></validate-number></form>`,
        );
        elem = renderer.elem.find('validate-number');
        scope = elem.isolateScope();
    }

    it('should assign the placeholder', () => {
        render();
        SpecHelper.expectElement(elem, `[placeholder=${scope.placeholder}]`);
    });

    it('should be valid if null', () => {
        render();
        expect(scope.ngModel).toBeNull();
        const numberInput = SpecHelper.expectElement(elem, `[name="${scope.inputName}"]`);
        SpecHelper.expectElementHasClass(numberInput, 'ng-valid-number');
    });

    it('should be valid if contains all numeric characters, is greater than lowest possible number, and less than or equal to max', () => {
        render();
        expect(scope.ngModel).toBeNull();
        const numberInput = SpecHelper.expectElement(elem, `[name="${scope.inputName}"]`);
        SpecHelper.expectElementHasClass(numberInput, 'ng-valid-number');

        // Set the value with all numeric characters and assert field is valid
        SpecHelper.updateTextInput(elem, `[name="${scope.inputName}"]`, scope.max);
        expect(scope.ngModel).toEqual(scope.max);
        SpecHelper.expectElementHasClass(numberInput, 'ng-valid-number');
    });

    it('should be invalid if its value is not an integer', () => {
        render();
        const numberInput = SpecHelper.expectElement(elem, `[name="${scope.inputName}"]`);

        // update text input with a valid value
        SpecHelper.updateTextInput(elem, `[name="${scope.inputName}"]`, scope.min);
        expect(scope.ngModel).toEqual(scope.min);
        SpecHelper.expectElementDoesNotHaveClass(numberInput, 'ng-invalid-number');

        // update text input with an invalid value
        SpecHelper.updateTextInput(elem, `[name="${scope.inputName}"]`, scope.min + 0.5);
        expect(scope.ngModel).toEqual(scope.min + 0.5);
        SpecHelper.expectElementHasClass(numberInput, 'ng-invalid-number');
    });

    it('should allow a float', () => {
        render({
            allowFloat: true,
        });
        const numberInput = SpecHelper.expectElement(elem, `[name="${scope.inputName}"]`);

        // update text input with a float value
        SpecHelper.updateTextInput(elem, `[name="${scope.inputName}"]`, scope.min + 0.5);
        expect(scope.ngModel).toEqual(scope.min + 0.5);
        SpecHelper.expectElementHasClass(numberInput, 'ng-valid-number');
    });

    it('should allow a zero', () => {
        render({
            allowZero: true,
            min: 0,
        });
        const numberInput = SpecHelper.expectElement(elem, `[name="${scope.inputName}"]`);

        // update text input with a float value
        SpecHelper.updateTextInput(elem, `[name="${scope.inputName}"]`, 0);
        expect(scope.ngModel).toEqual(0);
        SpecHelper.expectElementHasClass(numberInput, 'ng-valid-number');
    });

    it('should allow a negative when allowFloat', () => {
        render({
            allowFloat: true,
            min: -2,
        });
        const numberInput = SpecHelper.expectElement(elem, `[name="${scope.inputName}"]`);

        // update text input with a float value
        SpecHelper.updateTextInput(elem, `[name="${scope.inputName}"]`, scope.min);
        expect(scope.ngModel).toEqual(scope.min);
        SpecHelper.expectElementHasClass(numberInput, 'ng-valid-number');
    });

    it('should allow a negative when !allowFloat', () => {
        render({
            min: -2,
        });
        const numberInput = SpecHelper.expectElement(elem, `[name="${scope.inputName}"]`);

        // update text input with a float value
        SpecHelper.updateTextInput(elem, `[name="${scope.inputName}"]`, scope.min);
        expect(scope.ngModel).toEqual(scope.min);
        SpecHelper.expectElementHasClass(numberInput, 'ng-valid-number');
    });

    it('should be invalid if its value is less than the lowest possible number', () => {
        render();
        expect(scope.ngModel).toBeNull();
        const numberInput = SpecHelper.expectElement(elem, `[name="${scope.inputName}"]`);
        SpecHelper.expectElementHasClass(numberInput, 'ng-valid-number');

        // Set the value to be lower than min and assert field is invalid
        SpecHelper.updateTextInput(elem, `[name="${scope.inputName}"]`, scope.min - 1);
        expect(scope.ngModel).toEqual(scope.min - 1);
        SpecHelper.expectElementHasClass(numberInput, 'ng-invalid-number');
    });

    it('should be invalid if its value is more than the highest possible number', () => {
        render();
        expect(scope.ngModel).toBeNull();
        const numberInput = SpecHelper.expectElement(elem, `[name="${scope.inputName}"]`);
        SpecHelper.expectElementHasClass(numberInput, 'ng-valid-number');

        // Set the value to be higher than max and assert field is invalid
        SpecHelper.updateTextInput(elem, `[name="${scope.inputName}"]`, scope.max + 1);
        expect(scope.ngModel).toEqual(scope.max + 1);
        SpecHelper.expectElementHasClass(numberInput, 'ng-invalid-number');
    });
});
