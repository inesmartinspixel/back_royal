import 'AngularSpecHelper';
import 'FrontRoyalForm/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import writeTextAboutLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/write_text_about-en.json';

setSpecLocales(writeTextAboutLocales);

describe('FrontRoyal.Form.Inputs.WriteTextAbout', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Form', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);
    });

    function render(params = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = params.ngModel;
        renderer.scope.minlength = params.minlength;
        renderer.scope.maxlength = params.maxlength;
        renderer.scope.placeholderText = params.placeholderText;
        renderer.scope.shrinkToInput = params.shrinkToInput;
        renderer.render(
            '<write-text-about required ng-model="ngModel" maxlength="maxlength" minlength="minlength" placeholder="placeholderText" shrink-to-input="shrinkToInput"></write-text-about>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('form', () => {
        it('should update validity when text changes', () => {
            render({
                ngModel: 'foo string',
                maxlength: 100,
            });

            const modelController = elem.controller('ngModel');
            jest.spyOn(modelController, '$setValidity').mockImplementation(() => {});
            SpecHelper.updateTextInput(elem, 'textarea', 'bar string');
            expect(modelController.$setValidity).toHaveBeenCalled();
        });

        it('should not be valid if ngModel > maxlength', () => {
            render({
                ngModel: 'more than five',
                maxlength: 5,
            });

            const modelController = elem.controller('ngModel');
            expect(modelController.$valid).toBe(false);
        });

        it('should not be valid if required and textarea is blank', () => {
            render({
                ngModel: '',
            });

            const modelController = elem.controller('ngModel');
            expect(modelController.$valid).toBe(false);
        });
    });

    describe('textarea / input', () => {
        it('should initialize text area', () => {
            render({
                ngModel: 'foo string',
            });
            const textarea = elem.find('textarea')[0];
            expect(textarea.value).toBe('foo string');
        });

        it('should initialize placeholder message', () => {
            render({
                ngModel: null,
                placeholderText: 'insert foo',
            });
            SpecHelper.expectElement(elem, 'textarea[placeholder="insert foo"]');
        });

        it('should show input instead of textarea if shrinkToInput is true', () => {
            render({
                shrinkToInput: true,
            });
            SpecHelper.expectHasClass(elem, 'textarea', 'ng-hide');
            SpecHelper.expectDoesNotHaveClass(elem, 'input', 'ng-hide');
        });
    });

    describe('text remaining', () => {
        it('should decrement when text is entered', () => {
            render({
                ngModel: '',
                maxlength: 100,
            });
            SpecHelper.expectElementText(elem, '.remaining', '100 remaining');
            SpecHelper.updateTextInput(elem, 'textarea', 'fivee');
            SpecHelper.expectElementText(elem, '.remaining', '95 remaining');
        });

        it('should decrement to negative is more than max is entered', () => {
            render({
                ngModel: '',
                maxlength: 5,
            });
            SpecHelper.expectElementText(elem, '.remaining', '5 remaining');
            SpecHelper.updateTextInput(elem, 'textarea', 'tententenn');
            SpecHelper.expectElementText(elem, '.remaining', '-5 remaining');
        });

        it('should increment when text is removed', () => {
            render({
                ngModel: 'fivee',
                maxlength: 100,
            });
            SpecHelper.expectElementText(elem, '.remaining', '95 remaining');
            SpecHelper.updateTextInput(elem, 'textarea', '');
            SpecHelper.expectElementText(elem, '.remaining', '100 remaining');
        });

        it('should show length / maxlength when minlength is provided', () => {
            render({
                ngModel: 'fivee',
                minlength: 50,
                maxlength: 100,
            });
            SpecHelper.expectElementText(elem, '.remaining', '5 / 100');
            SpecHelper.updateTextInput(elem, 'textarea', '');
            SpecHelper.expectElementText(elem, '.remaining', '0 / 100');
        });
    });
});
