import 'AngularSpecHelper';
import 'FrontRoyalForm/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import selectedPillsLocales from 'FrontRoyalForm/locales/front_royal_form/selected_pills-en.json';

setSpecLocales(selectedPillsLocales);

describe('FrontRoyal.Form.SelectedPills', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let pills;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);

        SpecHelper.stubCurrentUser();
    });

    function render(params = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = pills;
        renderer.scope.type = 'items';
        renderer.scope.maxPills = params.maxPills;
        renderer.scope.simpleInstructions = params.simpleInstructions;
        renderer.render(
            '<selected-pills ng-model="ngModel" type="type" max-pills="maxPills" simple-instructions="simpleInstructions"></selected-pills>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    function populatePills(numPills, pushToScope) {
        pills = [];

        for (let i = 0; i < numPills; i++) {
            pills.push({
                locale: 'en',
                text: 'skill',
                type: 'skill',
            });
        }

        if (pushToScope) {
            scope.ngModel = pills;
        }
    }

    it('should remove a pill properly', () => {
        populatePills(10);
        render();
        scope.deletePill(0);
        scope.$apply();
        expect(scope.ngModel.length).toBe(9);
    });

    it('should display a pill for each skill', () => {
        populatePills(10);
        render();
        SpecHelper.expectElements(elem, '.pill-wrapper', 10);
    });

    it('should display the pills container when appropriate', () => {
        populatePills(0);
        render();
        SpecHelper.expectNoElement(elem, '#pills-container');
        populatePills(1, true);
        scope.$apply();
        SpecHelper.expectElement(elem, '#pills-container');
    });

    describe('description', () => {
        describe('with no maxPills', () => {
            it('should display drag and drop message when appropriate', () => {
                populatePills(0);
                render();
                SpecHelper.expectElement(elem, '.subtitle');
                SpecHelper.expectElementText(elem, '.subtitle', 'Use input to add custom items.');
                populatePills(1, true);
                scope.$apply();
                SpecHelper.expectElementText(elem, '.subtitle', 'Use input to add custom items.');
                populatePills(2, true);
                scope.$apply();
                SpecHelper.expectElementText(
                    elem,
                    '.subtitle',
                    'Drag and drop to reorder. Use input to add custom items.',
                );
            });
        });

        describe('with maxPills', () => {
            it('should display maxPills and drag and drop message when appropriate', () => {
                populatePills(0);
                render({
                    maxPills: 2,
                });
                SpecHelper.expectElement(elem, '.subtitle');
                SpecHelper.expectElementText(elem, '.subtitle', 'Use input to add up to 2 items.');
                populatePills(1, true);
                scope.$apply();
                SpecHelper.expectElementText(elem, '.subtitle', 'Use input to add up to 2 items.');
                populatePills(2, true);
                scope.$apply();
                SpecHelper.expectElementText(
                    elem,
                    '.subtitle',
                    'Drag and drop to reorder. Use input to add up to 2 items.',
                );
            });
        });

        describe('with simpleInstructions', () => {
            it('should display maxPills and drag and drop message when appropriate', () => {
                populatePills(0);
                render({
                    simpleInstructions: true,
                });
                SpecHelper.expectNoElement(elem, '.subtitle');
                populatePills(1, true);
                scope.$apply();
                SpecHelper.expectNoElement(elem, '.subtitle');
                populatePills(2, true);
                scope.$apply();
                SpecHelper.expectElementText(elem, '.subtitle', 'Drag and drop to reorder.');
            });
        });
    });
});
