import ReactDatepicker from 'react-datepicker';
import React, { useState } from 'react';
import ReactIf from 'ReactIf';
import 'react-datepicker/dist/react-datepicker.css';
import './DatePicker.scss';
import moment from 'moment-timezone';

/*
    Usage:

    <DatePicker
        name="someProperty"
        value={new Date()}
    />

    You can also provide any options supported by react-datepicker.  See https://github.com/Hacker0x01/react-datepicker
*/

function DatePicker(props) {
    const {
        field: { name, value },
        form,
    } = props;

    // clone so we can edit
    props = { ...props };
    const { autoFocus, dateFormat, timeFormat } = props;

    // We want to include the timezone (i.e. EDT) in the formatted date.
    // In date_helper.js, we used to use the `z` flag in the format string.  This has
    // been deprecated though (See http://momentjs.com/docs/#/displaying/format/).
    // So, we now have to get the name of the timezone directly
    // from moment timezone and inject it into the format.
    const tzAbbr = moment.tz.zone(moment.tz.guess()).abbr(value || moment());
    props.dateFormat = dateFormat || `MMMM d, yyyy h:mm aa '${tzAbbr}'`;
    props.timeFormat = timeFormat || `h:mm aa '${tzAbbr}'`;

    const [autofocusDatetimePicker, setAutofocusDatetimePicker] = useState(false);

    const dateTimeProps = {
        ...props,
        autoComplete: 'off',
        value: undefined,

        // This will only work the first time the button is clicked.  If you click the button,
        // delete the date, then click again, the calendar does not popu up automatically. It would
        // be better if it did, but it's not a big deal.
        autoFocus: autoFocus || autofocusDatetimePicker,
        onChange: date => {
            form.setFieldValue(name, date);
        },
        onBlur() {
            form.setFieldTouched(name);
        },
    };
    if (value) {
        dateTimeProps.selected = value;
    }

    const { className, disabled } = props;

    return (
        <div className={`front-royal-date-picker ${className}`}>
            <ReactIf if={!value}>
                <button
                    className="dropdown-toggle styled-as-input btn btn-default"
                    type="button"
                    name="show-picker"
                    onClick={e => {
                        e.preventDefault();
                        setAutofocusDatetimePicker(true);
                        form.setFieldValue(name, new Date());

                        // See the comment above `autoFocus`.  When this button is clicked
                        // for the second time, the input does not end up focused and, if we
                        // don't trigger `onBlur`, the validation error is never removed.
                        dateTimeProps.onBlur();
                    }}
                    disabled={disabled}
                >
                    <i className="fa fa-calendar" />
                </button>
            </ReactIf>

            {/* Hide the picker instead of removing it to prevent window from scrolling to dateTimeProps
        when setting back to null */}
            <div className="hide-datepicker" style={{ display: value ? 'block' : 'none' }}>
                {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                <ReactDatepicker {...dateTimeProps} />
            </div>
        </div>
    );
}

export default DatePicker;
