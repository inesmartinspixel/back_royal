import React from 'react';
import './FieldRow.scss';

/*
    I tried to make this look at the schemaValidation to dynamically
    decide whether to show the required style or not, but it wasn't
    totally reliable. See the comment at https://github.com/jaredpalmer/formik/issues/712#issuecomment-520554091.
    Update: there may be a solution at https://github.com/jaredpalmer/formik/issues/1241
*/
function FieldRow(props) {
    const requiredClassName = props.required === false ? '' : 'required';

    return (
        <div className="form-group no-border row front-royal-form-field-row">
            <div className="col-sm-4 split-group left">
                <label className={requiredClassName} htmlFor={props.for}>
                    {props.label}
                </label>
            </div>
            <div className="col-sm-8 split-group right">{props.children}</div>
        </div>
    );
}

export default FieldRow;
