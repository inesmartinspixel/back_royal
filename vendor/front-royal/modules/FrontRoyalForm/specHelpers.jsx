import React from 'react';
import { Formik, Form } from 'formik';

// In order for `connect` to send a formik object down into
// our fields, we have to wrap these in a form
function MockForm(props) {
    const { initialValues, children } = props;

    return (
        // eslint-disable-next-line react/jsx-props-no-spreading
        <Formik {...props} initialValues={initialValues || {}}>
            <Form>{children}</Form>
        </Formik>
    );
}

// eslint-disable-next-line import/prefer-default-export
export { MockForm };
