import { compact } from 'lodash/fp';
import { getIn } from 'formik';

function addFormValidationErrorClass(name, formik, className) {
    const showError = getIn(formik.errors, name) && getIn(formik.touched, name);

    if (!showError) {
        return [undefined, className || ''];
    }
    return [true, compact(['validation-error', className]).join(' ')];
}

export default addFormValidationErrorClass;
