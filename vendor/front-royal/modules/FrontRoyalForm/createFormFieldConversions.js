/*
    createFormFieldConversions is a function that generates to other
    functions, `toForm` and `fromForm`.

    `toForm` takes a model and converts it into an object that can be passed into `initialValues`
    on a Formik form.

    `fromForm` takes `form.values` and converts it to attributes that can be
    passed into a model.

    `createFormFieldConversions` is called with a list of conversion functions
    that show how to convert the particular fields in your model.  For an
    example, see BillingTransaction/formFieldConversions.js
*/

import { uniq } from 'lodash/fp';

function convert(values, conversions, meth) {
    const clone = {
        ...values,
    };

    // It's possible that the conversion might create new fields
    // that only exist in the form, so, we need to look for keys in
    // both places;
    const allKeys = uniq(Object.keys(clone).concat(Object.keys(conversions)));

    allKeys.forEach(key => {
        const value = clone[key];
        const conversion = conversions[key] && conversions[key][meth];
        clone[key] = conversion ? conversion(clone) : value;
    });

    return clone;
}

export default function createFormFieldConversions(conversions) {
    function toForm(values) {
        return convert(values, conversions, 'toForm');
    }

    function fromForm(values) {
        return convert(values, conversions, 'fromForm');
    }

    return [toForm, fromForm];
}
