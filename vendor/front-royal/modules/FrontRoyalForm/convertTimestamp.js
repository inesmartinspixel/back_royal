export default function (name) {
    const conversions = {
        fromForm: values => values[name] && values[name] / 1000,
        toForm: values => (values[name] ? new Date(values[name] * 1000) : new Date()),
    };
    const obj = {};
    obj[name] = conversions;
    return obj;
}
