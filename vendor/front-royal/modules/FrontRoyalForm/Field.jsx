import React from 'react';
import { connect, getIn, Field as FormikField } from 'formik';
import addFormValidationErrorClass from './addFormValidationErrorClass';

function Field(props) {
    let vanillaInputType;

    const { component, name, className, type, formik } = props;

    // if there is no component, this will be a vanilla HTML input type and
    // we will default it to text type
    if (!component) {
        vanillaInputType = type || 'text';
    }

    // react does not like it if text forms are null or undefined.
    // See the TextField component in FrontRoyalMaterialUiForm for an example
    // of one that sets defaultValueToEmptyString
    const isTextLike = vanillaInputType === 'text' || component === 'textarea' || component.defaultValueToEmptyString;
    const val = getIn(formik.values, name);
    const noValue = val === undefined || val === null;

    if (isTextLike && noValue) {
        props = {
            ...props,
            value: '',
        };
    }

    // Do not submit input fields when enter is pressed
    // https://github.com/davidkpiano/react-redux-form/issues/790
    // (We may want to make this configurable at some point.  For now at least
    // putting ...props underneath so you could override onKeyPress if you want)
    if (['text', 'number', 'email', 'password'].includes(vanillaInputType)) {
        props = {
            onKeyPress: e => {
                if (e.key === 'Enter') {
                    e.preventDefault();
                }
            },
            ...props,
        };
    }

    const [error, errorClassName] = addFormValidationErrorClass(name, formik, `${className || ''} field`);

    // eslint-disable-next-line react/jsx-props-no-spreading
    return <FormikField {...props} error={error} className={errorClassName} />;
}

export default connect(Field);
