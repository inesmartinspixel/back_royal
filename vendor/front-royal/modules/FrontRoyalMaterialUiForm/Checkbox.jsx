import React from 'react';
import MaterialUICheckbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import { useTheme } from '@material-ui/styles';

function InnerCheckbox(props) {
    const theme = useTheme();

    const {
        field: { name, value },
        form,
    } = props;

    function handleChange(event) {
        form.setFieldValue(name, !!event.target.checked);
    }

    return (
        <FormControl>
            <FormControlLabel
                control={<MaterialUICheckbox checked={value || false} onChange={handleChange} color="primary" />}
                style={{ color: theme.palette.text.primary }}
                label={props.label}
            />
        </FormControl>
    );
}

const Checkbox = React.memo(InnerCheckbox, (prevProps, nextProps) => {
    const changed = prevProps.field.value !== nextProps.field.value || prevProps.label !== nextProps.label;

    return !changed;
});

export default Checkbox;
