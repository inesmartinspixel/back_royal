import React from 'react';
import Typography from '@material-ui/core/Typography';
import Oreo from 'Oreo';

export default function SubText(props) {
    return <Typography style={{ color: Oreo.COLOR_V3_BEIGE_BEYOND_BEYOND_DARK }}>{props.children}</Typography>;
}
