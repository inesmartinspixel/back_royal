import React from 'react';
import { useGooglePlaces } from 'GooglePlaces';
import TextField from '@material-ui/core/TextField';
import Chip from '@material-ui/core/Chip';
import { uniq, without } from 'lodash/fp';
import { locationString } from 'Location';

export default function MultiLocationInput(props) {
    const {
        field: { name, value },
        form,
    } = props;
    const locationInputRef = useGooglePlaces({
        onPlaceChanged: (place, placeDetails, input) => {
            /*
                This is inconsistent with the SingleLocationInput.  Here,
                the object that gets outputted from the component is an array
                of `placeDetails`.  There is no access to the `place` or the `placeId`.

                In SingleLocationInput, we do provide access to the placeId.
            */
            form.setFieldValue(name, uniq(value.concat([placeDetails])));
            input.value = '';
        },
    });

    function deletePlace(placeDetails) {
        form.setFieldValue(name, without([placeDetails])(value));
    }

    return (
        <>
            <TextField
                name="placesInput"
                label="Location"
                placeholder="Enter a location"
                variant="outlined"
                ref={locationInputRef}
            />
            {(value || []).map(placeDetails => {
                const str = locationString(placeDetails);
                return (
                    <Chip
                        key={str}
                        label={str}
                        onDelete={() => deletePlace(placeDetails)}
                        variant="outlined"
                        color="primary"
                        classes={
                            {
                                // label: classes.chipLabel,
                                // deleteIcon: classes.chipDeleteIcon
                            }
                        }
                    />
                );
            })}
        </>
    );
}
