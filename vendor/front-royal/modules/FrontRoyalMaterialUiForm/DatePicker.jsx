import React, { useEffect } from 'react';
import moment from 'moment-timezone';
import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider, KeyboardDateTimePicker } from '@material-ui/pickers';
import { omit } from 'lodash/fp';

function setTimezone(date, timezone) {
    return moment.tz(timezone).set({
        year: date.get('year'),
        month: date.get('month'),
        date: date.get('date'),
        hour: date.get('hour'),
        minute: date.get('minute'),
        second: date.get('second'),
    });
}

// There are other choices besides the KeyboardDateTimePicker.  We can look into
// those when we need a date picker or something else.
function InnerDatePicker(props) {
    const { field, form, label, timezone } = props;
    const passthrough = omit(['field', 'form', 'label', 'className', 'timezone'])(props);

    useEffect(() => {
        // moment's handling of these timezones does not appear to
        // be case-sensitive.  I ran into an issue where I had an
        // infinite loop because `timezone` was 'America/New_york', but
        // field.value.tz() was always 'America/New_York'.  I'm not
        // sure that's a realistic case anyway, I may have put myself
        // in that state by entering the wrong case manually.  But, being
        // defensive.
        if (timezone && field.value && field.value.tz() && field.value.tz().toLowerCase() !== timezone.toLowerCase()) {
            const clone = field.value.clone();
            clone.tz(timezone);
            form.setFieldValue(field.name, clone);
        }

        /*
        Ignoring exhaustive-deps here is the right thing to do
        because we only want this to run when the timezone or value changes.
        See also: https://github.com/jaredpalmer/formik/issues/1677
    */
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [field.name, field.value, timezone]);

    return (
        <MuiPickersUtilsProvider utils={MomentUtils}>
            <KeyboardDateTimePicker
                format="YYYY/MM/DD HH:mm"
                margin="none"
                id="date-picker-inline"
                label={label}
                KeyboardButtonProps={{
                    'aria-label': 'change date',
                }}
                inputVariant="outlined"
                {...field}
                {...passthrough}
                onChange={date => {
                    const val = timezone && date && date.isValid() ? setTimezone(date, timezone) : date;
                    form.setFieldValue(field.name, val);
                    form.setFieldTouched(field.name);
                }}
            />
        </MuiPickersUtilsProvider>
    );
}

const DatePicker = React.memo(InnerDatePicker, (prevProps, nextProps) => {
    const changed =
        prevProps.field.value !== nextProps.field.value ||
        prevProps.label !== nextProps.label ||
        prevProps.timezone !== nextProps.timezone ||
        prevProps.error !== nextProps.error;
    return !changed;
});

export default DatePicker;
