import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import { isEqual, omit } from 'lodash/fp';

function InnerSelect(props) {
    const { field, label, options, multiple } = props;
    let { optionLabel, optionValue } = props;

    const { value } = field;
    const noValue = value === undefined || value === null;

    // react does not like null or undefined values
    if (multiple && noValue) {
        field.value = [];
    } else if (noValue) {
        field.value = '';
    }

    // selectProps should include all the properties from `field`,
    // as well as things like `multiple` and `renderValue`
    const selectProps = {
        ...omit(['field', 'label', 'options', 'optionLabel', 'optionValue'])(props),
        ...field,
    };

    optionLabel = optionLabel || (opt => opt.label);
    optionValue = optionValue || (opt => opt.value);

    return (
        <TextField select label={label} SelectProps={selectProps} variant="outlined">
            {
                // eslint-disable-next-line arrow-body-style
                options.map(opt => {
                    return (
                        <MenuItem key={optionValue(opt)} value={optionValue(opt)}>
                            {optionLabel(opt)}
                        </MenuItem>
                    );
                })
            }
        </TextField>
    );
}

const Select = React.memo(InnerSelect, (prevProps, nextProps) => {
    const changed =
        prevProps.field.value !== nextProps.field.value ||
        prevProps.label !== nextProps.label ||
        !isEqual(prevProps.options, nextProps.options);

    return !changed;
});

export default Select;
