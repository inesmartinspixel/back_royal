import { createMuiTheme } from '@material-ui/core/styles';
import Oreo from 'Oreo';

const defaultTheme = createMuiTheme();
const formControlSpacing = {
    paddingRight: defaultTheme.spacing(1),
    paddingLeft: defaultTheme.spacing(1),
};

const theme = createMuiTheme({
    // this is a function just so that if someone
    // tries to use a different theme that does not
    // support this, they will get an error rather than
    // just silently failing
    formControlSpacing: () => formControlSpacing,

    typography: {
        // FIXME: import font
        fontFamily: 'ProximaNovaSoft',
    },

    palette: {
        primary: {
            light: Oreo.COLOR_V3_BLUE_LIGHT,
            main: Oreo.COLOR_V3_BLUE,
            dark: Oreo.COLOR_V3_BLUE_DARK,
            contrastText: Oreo.COLOR_V3_WHITE,
        },

        secondary: {
            light: Oreo.COLOR_V3_CORAL,
            main: Oreo.COLOR_V3_CORAL,
            dark: Oreo.COLOR_V3_CORAL_DARK,
            contrastText: Oreo.COLOR_V3_WHITE,
        },

        text: {
            primary: '#000000',
        },
    },

    breakpoints: {
        values: {
            xs: 0,
            sm: 768,
            md: 992,
            lg: 1200,
            xl: 1920,
        },
    },

    overrides: {
        MuiFormControl: {
            root: {
                width: '100%',
                ...formControlSpacing,
            },
        },

        MuiFormLabel: {
            root: {
                // This is a hack.  It's necessary because of the paddingLeft
                // on MuiFormControl.root.  Material-ui is calculating the position
                // of the label in js and setting it with a transform, and it's really brittle.
                // They don't account for the padding, so, they end up putting it too far
                // left.  This moves it back to the right a little bit
                marginLeft: defaultTheme.spacing(1),
            },
        },

        MuiChip: {
            root: {
                marginTop: '6px',
                marginRight: '6px',
            },
        },
    },
});

export default theme;
