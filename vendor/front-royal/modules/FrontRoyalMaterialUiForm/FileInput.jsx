import React from 'react';
import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';

// We may want to re-use this at some point.  It would really
// apply in any situation where you had a button in place
// of an input
const useStyles = makeStyles(theme => ({
    button: {
        marginTop: theme.spacing(2), // make room for label
        marginLeft: theme.spacing(1), // move a bit to the right
        marginBottom: theme.spacing(1), // add some spacing below so it's not overrunning the next element
    },
}));

function InnerFileInput({ field, form, label }) {
    const classes = useStyles();

    const onChangeHandler = event => {
        const file = event.target.files[0];

        form.setFieldValue(field.name, {
            // If we put the dataUrl directly into the formik.values object,
            // the UI can get really slow as that big string gets passed all around.
            // So, instead, create a function that will generate a dataUrl
            getUrl() {
                return file && window.URL.createObjectURL(file);
            },
            file,
        });
    };

    return (
        <FormControl>
            {/* The label on this should always look the way shrunken
            labels look on the other inputs (for example, see a TextField,
            the way the label shrinks when there is something entered in
            the textbox) */}
            <InputLabel shrink>{label}</InputLabel>

            {/* Since the FormControl has `flex-direction: column`, it's direct children stretch
        to fill the space.  We don't want the button to do that, so we wrap it in a div.  */}
            <div>
                <Button variant="contained" color="default" className={classes.button}>
                    <CloudUploadIcon />
                    <input
                        type="file"
                        onChange={onChangeHandler}
                        style={{
                            cursor: 'pointer',
                            opacity: 0,
                            position: 'absolute',
                            left: '0px',
                            top: '0px',
                            height: '100%',
                            width: '100%',
                        }}
                    />
                </Button>
            </div>
        </FormControl>
    );
}

const FileInput = React.memo(InnerFileInput, (prevProps, nextProps) => {
    const changed = prevProps.label !== nextProps.label;
    return !changed;
});

export default FileInput;
