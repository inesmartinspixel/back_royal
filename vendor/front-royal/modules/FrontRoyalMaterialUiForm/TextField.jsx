import React from 'react';
import MaterialUiTextField from '@material-ui/core/TextField';
import { generateGuid } from 'guid';

function TextField(props) {
    const passthrough = { ...props };
    const { field } = passthrough;
    delete passthrough.field;

    const inputStyles = {};

    // We may eventually need this to be configurable,
    // but for now assume we always want this
    if (props.multiline) {
        inputStyles.resize = 'vertical';
    }

    const id = React.useRef(generateGuid()).current;

    return (
        <MaterialUiTextField
            id={id}
            {...field}
            {...passthrough}
            inputProps={{
                style: inputStyles,
            }}
            variant="outlined"
        />
    );
}

// the Field component in FrontRoyalForm will look out for this
// and convert null or undefined to ''
TextField.defaultValueToEmptyString = true;

export default TextField;
