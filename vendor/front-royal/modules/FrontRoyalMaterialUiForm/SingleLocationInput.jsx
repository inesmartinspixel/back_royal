import React, { useRef } from 'react';
import { useGooglePlaces } from 'GooglePlaces';
import TextField from '@material-ui/core/TextField';
import { locationString, formattedAddress } from 'Location';
import { omit } from 'lodash/fp';

function InnerSingleLocationInput(props) {
    const {
        field: { name, value },
        form,
    } = props;
    const onPlaceChanged = useRef((place, placeDetails) => {
        /*
            This is inconsistent with the MultipleLocationInput.  Here,
            the object that gets outputted from the component has an `id` property
            and a `details` property.  There is more information in the `place`
            object, but we aren't using it today, so leaving it out until we need it.

            In MultipleLocationInput, the input outputs an array of placeDetails.  It does
            not provide any access to the `place` or the `id`.
        */
        form.setFieldValue(name, {
            id: place.id,
            details: placeDetails,
        });
    }).current;

    let autoCompleteOptions;
    let formatter;
    switch (props.placeType) {
        case 'address':
            autoCompleteOptions = {
                types: null, // allow full address input
            };
            formatter = formattedAddress;
            break;

        default:
            autoCompleteOptions = {
                // ['(cities)'] is the default inside attachGooglePlacesToInput
                // anyway, but maybe nice to be explicit here
                types: ['(cities)'],
            };
            formatter = locationString;
            break;
    }

    const locationInputRef = useGooglePlaces({
        autoCompleteOptions,
        onPlaceChanged,
    });

    const passthrough = omit([
        // We remove the value from the props and set
        // defaultValue instead to convert the input into an
        // uncontrolled component.  This seems to be the easiest (only?)
        // way to get the integration with goole places working.
        'value',

        // React complains if we put placeType in the DOM
        'placeType',
    ])(props);

    return (
        <TextField
            name="placesInput"
            defaultValue={value ? formatter(value.details) : ''}
            {...passthrough}
            inputProps={{
                onBlur() {
                    form.setFieldTouched(name);
                },
            }}
            ref={locationInputRef}
            variant="outlined"
        />
    );
}

const SingleLocationInput = React.memo(InnerSingleLocationInput, (prevProps, nextProps) => {
    const changed = prevProps.field.value !== nextProps.field.value || prevProps.label !== nextProps.label;

    return !changed;
});

export default SingleLocationInput;
