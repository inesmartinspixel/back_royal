import angularModule from 'EventLogger/angularModule/scripts/event_logger_module';
import { getConversionPixelIds } from 'FrontRoyalLinkedIn';
import Auid from 'Auid';
import { ensurePageLoadId, unsetPageLoadId, BROWSER_AND_BUILD_INFO } from 'TinyEventLogger';
import { generateGuid } from 'guid';

/*

	EventLogger.log(type, obj);

*/

angularModule.factory('EventLogger', [
    '$injector',
    $injector => {
        const SuperModel = $injector.get('SuperModel');
        const EventBundle = $injector.get('EventLogger.EventBundle');
        const Event = $injector.get('EventLogger.Event');
        const $timeout = $injector.get('$timeout');
        const $window = $injector.get('$window');
        const segmentio = $injector.get('segmentio');
        const $location = $injector.get('$location');
        const $rootScope = $injector.get('$rootScope');
        const PiggybackOnNg = $injector.get('PiggybackOnNg');
        const Singleton = $injector.get('Singleton');
        const $q = $injector.get('$q');
        const ConfigFactory = $injector.get('ConfigFactory');
        const injector = $injector.get('injector');
        let $route; // see below in initialize

        // eslint-disable-next-line func-names
        const EventLogger = SuperModel.subclass(function () {
            this.include(Singleton);

            this.defineSingletonProperty(
                'trackLocationChanges',
                'destroy',
                'log',
                'trackAllNgEvents',
                'trackFirstViewRender',
                'setErrorHandler',
                'logStartEvent',
                'trackFocusAndBlur',
                'setLabelProperty',
                'allowEmptyLabel',
                'tryToClearBuffer',
                'tryToSaveBuffer',
                'setClientConfig',
                'sendEventsImmediately',
                'onUserChange',
            );

            this.createInstance = () => new EventLogger(10 * 1000);

            Object.defineProperty(this, 'disabled', {
                get() {
                    return this.instance._disabled;
                },
                set(value) {
                    this.instance._disabled = value;
                    if (this.instance._disabled && this.instance._clearBufferTimeout) {
                        $timeout.cancel(this.instance._clearBufferTimeout);
                    } else if (!this.instance._disabled) {
                        this.instance._clearBufferAndScheduleNextClear();
                    }
                },
            });

            return {
                initialize(bufferTime) {
                    // this is silly, but injecting route whenever EventLogger is
                    // exists can break totally unrelated tests, so we only inject
                    // it if EventLogger is actually used
                    $route = $injector.get('$route');

                    this.disabled = false;
                    this.bufferTime = bufferTime;
                    this.buffer = [];
                    this.listenerCancelers = [];
                    this.labelProperties = {};
                    this.eventTypesToAllowEmptyLabel = {};

                    this._clearBufferAndScheduleNextClear();

                    $($window).on('beforeunload.EventLogger', this._onUnload.bind(this));
                    this.listenerCancelers.push(() => {
                        $($window).off('beforeunload.EventLogger');
                    });
                },

                sendEventsImmediately() {
                    this._sendEventsImmediately = true;
                    this.tryToSaveBuffer();
                },

                setClientConfig(clientConfig) {
                    /*
                    Within front-royal, we get this from ClientConfig.
                    On marketing pages, we hardcode it in events.js
                        {
                            identifier: ...,
                            versionNumber: ...
                        }
                    */
                    this._clientConfig = clientConfig;
                },

                logStartEvent() {
                    this._sessionStartEvent = this.log(
                        'page_load:load',
                        angular.extend({}, BROWSER_AND_BUILD_INFO, {
                            // track how much time it takes to load the app up
                            // to this point
                            value: window.performance.now(),
                        }),
                        {
                            segmentio: false,
                        },
                    );
                },

                onUserChange(auid) {
                    this._logUnloadEvent();
                    const promise = this.tryToSaveBuffer(true, {
                        force_auid: auid,
                    });
                    this.logStartEvent();
                    return promise;
                },

                /*
                    For some events, we want to map a particular property (like
                    lesson_title) as the 'label' for the event. Label is a special
                    property in Google Analytics that helps define events.  We could
                    simply set this every time we log an event, but because of the way
                    we build up the event payload in various places, it is
                    convenient sometimes to set this globally
                */
                setLabelProperty(prop, eventTypeOrTypes) {
                    let eventTypes;
                    if (_.isArray(eventTypeOrTypes)) {
                        eventTypes = eventTypeOrTypes;
                    } else {
                        eventTypes = [eventTypeOrTypes];
                    }

                    eventTypes.forEach(eventType => {
                        this.labelProperties[eventType] = prop;
                    });
                },

                // some events do not need labels. Calling
                // allowEmptyLabel will silence the error when
                // logging such events to segmentio
                allowEmptyLabel(eventTypeOrTypes) {
                    let eventTypes;
                    if (_.isArray(eventTypeOrTypes)) {
                        eventTypes = eventTypeOrTypes;
                    } else {
                        eventTypes = [eventTypeOrTypes];
                    }

                    eventTypes.forEach(eventType => {
                        this.eventTypesToAllowEmptyLabel[eventType] = true;
                    });
                },

                log(type, obj, options) {
                    // buffer flushing shouldn't be occuring, but don't even bother if disabled
                    if (this.disabled) {
                        return null;
                    }

                    options = angular.extend(
                        {
                            segmentio: true,
                            cordovaFacebook: false,
                        },
                        options || {},
                    );

                    const event = this._getEvent(type, obj);
                    this.buffer.push(event);

                    // Log to SegmentIO
                    if (options.segmentio) {
                        event.properties.log_to_customerio = true;

                        if (!event.properties.label && !this.eventTypesToAllowEmptyLabel[type]) {
                            $injector
                                .get('ErrorLogService')
                                .notify(`'${event.event_type}' event logged to segmentio without a label.`);
                        }

                        // Tricky: when running as a standalone app in iOS saved to the homescreen,
                        // Facebook Custom Audiences causes a redirect to the Facebook app
                        // So, we disable it in that case. Not perfect, but works.
                        const standalone = !!window.navigator.standalone;

                        // Tricky: Facebook started complaining about our first_view_rendered event
                        // because it has a property named "value", which Facebook erroneously believes
                        // means the event is money-related, and therefore needs a Currency property.
                        // To silence this warning, we will disable the Facebook Pixel integration for
                        // the first_view_rendered event type.
                        const firstViewRenderedEvent = type === 'first_view_rendered';

                        // put in a timeout in case anything gets added to the event after the log call
                        $timeout(
                            () => {
                                if (this._inOfflineMode()) {
                                    return;
                                }

                                const eventType = options.segmentioType || event.event_type;
                                const props = angular.copy(event.properties);
                                if (options.segmentioLabel) {
                                    props.label = options.segmentioLabel;
                                }
                                // customize enabled/disabled segmentio integrations
                                const integrations = {
                                    'Facebook Custom Audiences': !standalone,
                                    'Facebook Pixel': !firstViewRenderedEvent,
                                    'Customer.io': false, // we've moved customer.io logging to the server
                                };

                                segmentio.track(eventType, props, {
                                    integrations,
                                });
                            },
                            0,
                            false,
                        );
                    }

                    // Handle special-case Cordova FB logging
                    if (options.cordovaFacebook && window.facebookConnectPlugin) {
                        // NOTE: For some strange reason, wrapping the plugin calls in a `$timeout` causes
                        // the plugin to indicate that it has succeeded, but the event never is reported

                        const props = angular.copy(event.properties);

                        // special case handling for Purchase-related events
                        if (props.label === 'Purchase' && props.currency && angular.isDefined(props.value)) {
                            window.facebookConnectPlugin.logPurchase(props.value, props.currency);
                        } else {
                            // sanitize for FB's char-map + length
                            const eventType = event.event_type.replace(/[^a-z0-9_-]/gim, '_').substring(0, 40);

                            // explicitly pass null as the valueToSum: https://github.com/jeduan/cordova-plugin-facebook4/issues/435
                            window.facebookConnectPlugin.logEvent(eventType, props, null);
                        }
                    }

                    this._logLinkedInPixel(type);

                    /*
                        If every event in the bundle is set to deferIndefinitely,
                        then the bundle will not be sent.  The only place we use this now is
                        for the logging of api request information, in order to prevent
                        the creation of an infinite loop where the success of one request
                        logs events that then have to be logged in a subsequent request.
                    */
                    if (options.deferIndefinitely) {
                        event.deferIndefinitely = true;
                    }

                    if (this._sendEventsImmediately) {
                        this.tryToSaveBuffer();
                    }

                    return event;
                },

                trackLocationChanges() {
                    if ($window.disableFrontRoyalRoutes) {
                        this._logMarketingPageView();
                    } else {
                        const successListener = $rootScope.$on('$routeChangeSuccess', this._logRouteChange.bind(this));
                        this.listenerCancelers.push(successListener);
                    }
                },

                trackFirstViewRender() {
                    // android 4.1 has performance but not performance.now
                    if (!$window.performance || !$window.performance.now) {
                        return;
                    }
                    const cancelListener = $rootScope.$on('$viewContentLoaded', () => {
                        cancelListener();
                        let directive;
                        try {
                            directive = $route.current.$$route.directive;
                            // eslint-disable-next-line no-empty
                        } catch (e) {}
                        this.log('first_view_rendered', {
                            // value is the time since the user
                            // first started loading the page. Using 'value' and 'label'
                            // for special handling in Google Analytics
                            value: $window.performance.now() / 1000,
                            label: directive || 'unknown',
                        });
                    });
                    this.listenerCancelers.push(cancelListener);
                },

                // commented out only because tests mysteriously broke
                // when we moved EventLogger into common.  As far as I know,
                // it still works.  After moving this, we started running into
                // trouble with focus events both here and in challenge_blank_dir (which
                // did not move).  Mysterious.
                // trackFocusAndBlur: function() {
                //     ['input', 'textarea', 'select'].forEach(function(nodeName) {
                //         var directive = $injector.get(nodeName + 'Directive')[0];
                //         var origCompile = directive.compile;

                //         directive.compile = function(element, attr) {
                //             var origResult = origCompile(element, attr);

                //             // textarea and input compiles return an object
                //             // with a 'pre' property.  select returns a function
                //             var origFunc = origResult.pre ? origResult.pre : origResult;
                //             var handler = function(scope, element, attr, ctrls) {

                //                 if (element.type !== 'button' &&
                //                     element.type !== 'submit') {

                //                     var callback = function onFocusAndBlur(event) {
                //                         EventLogger.log(event.type, {
                //                             elem: EventLogger.propertiesForElem(element)
                //                         }, {
                //                             segmentio: false // no need to log this to segmentio until we need it there
                //                         });
                //                     };

                //                     element.on('focus.event_logger', callback);
                //                     element.on('blur.event_logger', callback);

                //                     scope.$on('$destroy.event_logger', function() {
                //                         element.off('focus.event_logger', callback);
                //                         element.off('blur.event_logger', callback);
                //                     });
                //                 }
                //                 return origFunc(scope, element, attr, ctrls);
                //             };

                //             if (origResult.pre) {
                //                 return {
                //                     pre: handler
                //                 };
                //             } else {
                //                 return handler;
                //             }
                //         };
                //     });

                // },

                trackAllNgEvents(directiveName, auto) {
                    const self = this;
                    self.$$autoTrackDirectives = self.$$autoTrackDirectives || {};
                    if (auto === false) {
                        self.$$autoTrackDirectives[directiveName] = false;
                        return;
                    }
                    self.$$autoTrackDirectives[directiveName] = true;

                    PiggybackOnNg.on(directiveName, (scope, element, attr, event) => {
                        // Ignore the app-main-container ng-click="void(0)" hack -- see app_shell.html
                        if (angular.isDefined(attr.noLog)) {
                            return;
                        }

                        let label = attr.label && scope.$eval(attr.label);
                        label = label || attr[directiveName] || attr[`noApply${event.type.camelize(true)}`];

                        // We removed the ability to log Ng events to third parties
                        // like segment and customer.io. I'm leaving this bit of code
                        // here out of paranoia, so we get notified if we missed anything.
                        // If we haven't seen one of these on Sentry after a month, this
                        // can be removed.
                        if (
                            attr.segmentio === '' ||
                            scope.$eval(attr.segmentio) ||
                            scope.$eval(attr.segmentioType) ||
                            scope.$eval(attr.segmentioLabel)
                        ) {
                            $injector
                                .get('ErrorLogService')
                                .notify(`Ng '${event.type}' event tracked with segmentio attrs.`);
                        }

                        // if auto-tracking has been turned off, don't log this event.
                        if (!self.$$autoTrackDirectives[directiveName]) {
                            return;
                        }

                        const evt = this.log(
                            event.type,
                            {
                                label,
                            },
                            {
                                segmentio: false,
                            },
                        );

                        // Track how long it takes us to
                        // handle each click (or whatever) so we
                        // can identify slow handlers.  Theoretically, since
                        // we're adding the performance information asynchronously,
                        // it could be added after the event has been sent, but that
                        // will be extremely rare and not really a problem..
                        const now = new Date();
                        evt.properties.performance = {};
                        $injector.get('$timeout')(
                            () => {
                                const elapsed = new Date() - now;
                                evt.properties.performance.toTimeout = elapsed;

                                // for debugging
                                $rootScope.lastEvent = `${directiveName}: ${elapsed}`;
                                if (elapsed > 100) {
                                    // console.warn('Poorly performing ' + directiveName + '. Took ' + elapsed + ' ms.', evt);
                                }
                            },
                            0,
                            false,
                        );
                        scope.$evalAsync(() => {
                            evt.properties.performance.toEvalAsync = new Date() - now;
                        });
                    });
                },

                destroy() {
                    this.buffer = [];
                    $timeout.cancel(this._clearBufferTimeout);
                    this.listenerCancelers.forEach(func => {
                        func();
                    });
                },

                tryToClearBuffer(lastChance, format) {
                    const events = this.buffer;

                    // Retry later if:
                    // 1. there are no events buffered
                    // 2. ConfigFactory is not yet initialized
                    if (events.length === 0 || !ConfigFactory.isInitialized()) {
                        return null;
                    }

                    // Retry later if:
                    // 1. All events are marked as deferIndefinitely
                    // 2. This was not kicked off by the onUnload listener
                    let undeferredEvent;

                    // eslint-disable-next-line no-restricted-syntax
                    for (const event of events) {
                        if (!event.deferIndefinitely) {
                            undeferredEvent = event;
                            break;
                        }
                    }

                    if (!undeferredEvent && !lastChance) {
                        return null;
                    }

                    this.buffer = [];

                    const jsonnedEvents = events.map(event => event.asJson());

                    const eventBundle = EventBundle.new({
                        events: jsonnedEvents,
                    });

                    // store the original events so that if the request
                    // fails we can stick them back into the buffer
                    eventBundle.$$eventObjects = events;

                    return format === 'json' ? eventBundle.asJson() : eventBundle;
                },

                tryToSaveBuffer(lastChance, meta) {
                    const self = this;
                    const eventBundle = self.tryToClearBuffer(lastChance);

                    // need to check disabled to prevent _onUnload from saving
                    if (eventBundle && !EventLogger.disabled) {
                        return eventBundle
                            .save(meta || {}, {
                                'FrontRoyal.ApiErrorHandler': {
                                    skip: true,
                                },
                            })
                            .catch(response => {
                                if (response.status === 0) {
                                    // add the events back to the beginning of the buffer so
                                    // they will be sent again in the next call
                                    self.buffer = eventBundle.$$eventObjects.concat(self.buffer);
                                    $injector.get('HttpQueue').unfreezeAfterError(response.config);
                                } else if (
                                    response.status === 406 &&
                                    response.data &&
                                    !!response.data.user_from_auid_deleted
                                ) {
                                    // If the server is telling us that it doesn't have a current user,
                                    // and the user referenced by the AUID in the headers/cookies of the
                                    // request has been deleted, we should regenerate an AUID so we aren't
                                    // logging events for a deleted user.
                                    // This also ensures that, should the end-user reregister, the newly-
                                    // registered user won't end up with the same id as their old user.
                                    Auid.reset();
                                    $injector.get('HttpQueue').unfreezeAfterError(response.config);
                                } else if (response.config) {
                                    // do default handling
                                    response.config['FrontRoyal.ApiErrorHandler'].skip = false;

                                    // may not be loaded yet, but use if available
                                    if ($injector.has('ApiErrorHandler')) {
                                        $injector.get('ApiErrorHandler').onResponseError(response);
                                    }
                                } else {
                                    throw response;
                                }
                            });
                    }

                    // eslint-disable-next-line no-else-return
                    else {
                        return $q.when();
                    }
                },

                _getEvent(type, obj) {
                    const props = {};
                    const labelProp = this.labelProperties[type];
                    if (obj && labelProp) {
                        props.label = obj[labelProp];
                    }

                    // NOTE: ensurePageLoad could have been called from outside of EventLogger
                    // if we are on the dynamic_landing_page.  Since ensurePageLoadId stores
                    // the page load on the window under the hood, we will get the same one here.
                    angular.extend(props, obj);
                    props.page_load_id = ensurePageLoadId();
                    const event = new Event(type, props);
                    return event;
                },

                _logRouteChange(event, next) {
                    const nextRoute = {
                        directive: next.$$route ? next.$$route.directive : undefined,
                        params: $route.current.params,
                        path: $location.url(),
                    };
                    this._logRouteLeave(nextRoute);
                    nextRoute.startEvent = this._logRouteEnter(nextRoute);
                    this._currentRoute = nextRoute;
                },

                _logRouteLeave(nextRoute) {
                    if (!this._currentRoute) {
                        return null;
                    }

                    const event = this.log(
                        ['route_change', this._currentRoute.directive, 'leave'].join(':'),
                        {
                            directive: this._currentRoute.directive,
                            params: this._currentRoute.params,
                            path: this._currentRoute.path,
                            destination_directive: nextRoute && nextRoute.directive,
                            destination_params: nextRoute && nextRoute.params,
                            destination_path: nextRoute && nextRoute.path,
                        },
                        {
                            segmentio: false,
                        },
                    );

                    event.addDurationInfo(this._currentRoute.startEvent);
                    return event;
                },

                _logRouteEnter(nextRoute) {
                    const props = {
                        directive: nextRoute.directive,
                        params: nextRoute.params,
                        path: nextRoute.path,
                        source_directive: this._currentRoute && this._currentRoute.directive,
                        source_params: this._currentRoute && this._currentRoute.params,
                        source_path: this._currentRoute && this._currentRoute.path,
                    };

                    const name = props.directive || $location.path();

                    let url;
                    // in CORDOVA, location.href is a long thing starting with file://
                    if ($window.CORDOVA) {
                        url = `Smartly.app${$window.location.href.split('index.html#')[1] || '/'}`;
                    } else {
                        url = $window.location.href;
                    }

                    this._logThirdPartyPageCall(name, {
                        url,
                        path: $location.path(),
                    });

                    // and record the event
                    return this.log(['route_change', nextRoute.directive, 'enter'].join(':'), props, {
                        segmentio: false,
                    });
                },

                _logThirdPartyPageCall(title, opts) {
                    if (this._inOfflineMode()) {
                        return;
                    }

                    segmentio.page(undefined, title, {
                        ...opts,
                        title,
                    });
                },

                _clearBufferAndScheduleNextClear() {
                    this.tryToSaveBuffer(false);
                    this._clearBufferTimeout = $timeout(
                        this._clearBufferAndScheduleNextClear.bind(this),
                        this.bufferTime,
                        false,
                    );
                },

                _logUnloadEvent() {
                    this._logRouteLeave();
                    this._currentRoute = undefined; // in onUserChange, prevent the leave event from being logged for the new user
                    if (this._sessionStartEvent) {
                        this.log(
                            'page_load:unload',
                            {},
                            {
                                segmentio: false,
                            },
                        ).addDurationInfo(this._sessionStartEvent);
                        this._sessionStartEvent = undefined;

                        // We might be changing users but not actually
                        // loading up a new page. In that case, we need a new
                        // page load id.
                        unsetPageLoadId();
                    }
                },

                _onUnload() {
                    this._logUnloadEvent();
                    try {
                        this.tryToSaveBuffer(true);
                    } catch (e) {
                        // ignore message that happens when auto-reloading tests
                        if (!e.message.match(/Unexpected call to event_bundles.create/)) {
                            throw e;
                        }
                    }
                },

                // wrapper that can be mocked in specs
                _getConversionPixelIds(eventType, isQuantic, appEnvType) {
                    return getConversionPixelIds(eventType, isQuantic, appEnvType);
                },

                _logLinkedInPixel(eventType) {
                    if (segmentio.onetrustCookieHelper.integrationDisabled('LinkedIn Conversion Tracking')) {
                        return;
                    }

                    // If the config is not yet defined, we just return.  This prevents
                    // a whole bunch of specs from failing if config is not mocked out.
                    // Also, we probably don't want to add a new place that triggers the
                    // initial config load when events are logged before that has been triggered elsewhere.
                    // Those events (i.e. page_load:load) will presumably not have linkedIn
                    // pixels attached anyway.
                    const config = ConfigFactory.getSync(true);
                    if (!config) {
                        return;
                    }

                    // Add in a linkedIn pixel if one is defined for this event
                    // in getConversionPixelIds
                    this._getConversionPixelIds(eventType, config.isQuantic(), config.appEnvType()).forEach(
                        linkedInConversionId => {
                            // Hardcoding the same pid (LinkedIn Data Partner ID) for all environments.
                            // It is the same as well across the smartly and quantic accounts
                            const url = `https://dc.ads.linkedin.com/collect/?pid=24209&conversionId=${linkedInConversionId}&fmt=gif`;
                            const id = generateGuid();
                            $('body').append(
                                `<img id=${id} height="1" width="1" style="display:none;" alt="" src="${url}" />`,
                            );
                            const img = $(`#${id}`);
                            img.on('load', function removeImage() {
                                $(this).remove();
                            });
                        },
                    );
                },

                _logMarketingPageView() {
                    const url = $window.location.href.split('?')[0];

                    // grab the last part of the url and use it as the
                    // title for the page.  for example http://localhost:3001/marketing/press.html
                    // maps to 'press'
                    let title = url.substr(url.lastIndexOf('/') + 1).split('.')[0];
                    if (!title && $window.location.pathname === '/') {
                        title = 'root';
                    }
                    EventLogger.log(
                        'marketing:pageview',
                        {
                            label: title,
                        },
                        {
                            segmentio: false,
                        },
                    );

                    this._logThirdPartyPageCall(title, {
                        url: $window.location.href,
                        path: $window.location.pathname,
                    });
                },

                _inOfflineMode() {
                    const offlineModeManager = injector.get('offlineModeManager', { optional: true });
                    return offlineModeManager?.inOfflineMode;
                },
            };
        });

        return EventLogger;
    },
]);
