import 'angular-route';
import 'Injector/angularModule';
import 'IguanaSuperModelAndAClassAbove';
import 'FrontRoyalConfig/angularModule';
import 'Segmentio/angularModule';
import 'PiggybackOnNg/angularModule';
import 'ErrorLogging/angularModule';
import 'AngularHttpQueueAndRetry/angularModule';
import 'SessionTracker/angularModule';

export default angular.module('EventLogger', [
    'AClassAbove',
    'FrontRoyal.Config',
    'SuperModel',
    'segmentio',
    'ngRoute',
    'PiggybackOnNg',
    'Iguana',
    'FrontRoyal.ErrorLogService',
    'HttpQueueAndRetry',
    'sessionTracker',
    'Injector',
]);
