import 'AngularSpecHelper';
import 'EventLogger/angularModule';

describe('EventLoggerInterceptor', () => {
    let EventLoggerInterceptor;
    let EventLogger;

    beforeEach(() => {
        // SpecHelper has to be first so
        // that localStorage will be cleaned up before the
        // Interceptor is initialized in each test
        angular.mock.module('SpecHelper', 'EventLogger');

        angular.mock.inject([
            '$injector',
            $injector => {
                EventLoggerInterceptor = $injector.get('EventLoggerInterceptor');
                EventLogger = $injector.get('EventLogger');
            },
        ]);
    });

    describe('response', () => {
        beforeEach(() => {
            jest.spyOn(EventLogger.prototype, 'trackAllNgEvents');
        });

        it('should do nothing with no meta', () => {
            const response = {};
            expect(EventLoggerInterceptor.response(response)).toBe(response);
            expect(EventLogger.prototype.trackAllNgEvents).not.toHaveBeenCalled();
        });

        it('should do turn of tracking for all directives in disable_auto_events_for', () => {
            const response = {
                data: {
                    meta: {
                        push_messages: {
                            event_logger: {
                                disable_auto_events_for: ['ngClick', 'someOtherDirective'],
                            },
                        },
                    },
                },
            };
            expect(EventLoggerInterceptor.response(response)).toBe(response);

            expect(EventLogger.prototype.trackAllNgEvents).toHaveBeenCalledWith('ngClick', false);
            expect(EventLogger.prototype.trackAllNgEvents).toHaveBeenCalledWith('someOtherDirective', false);
        });
    });
});
