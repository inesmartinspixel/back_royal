import 'AngularSpecHelper';
import 'EventLogger/angularModule';
import { generateGuid } from 'guid';

jest.mock('guid', () => ({
    generateGuid: jest.fn(),
}));

describe('EventLogger.Event', () => {
    let Event;
    let SpecHelper;
    let $route;
    let $location;

    beforeEach(() => {
        angular.mock.module('EventLogger', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Event = $injector.get('EventLogger.Event');
                $injector.get('MockIguana');
                SpecHelper = $injector.get('SpecHelper');
                $route = $injector.get('$route');
                $location = $injector.get('$location');

                // Ensure config is initialized so events can be flushed
                SpecHelper.stubConfig();
            },
        ]);

        generateGuid.mockReset();
        let guidI = 0;
        generateGuid.mockImplementation(() => {
            guidI += 1;
            return `guid-${guidI}`;
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('initialize', () => {
        it('shuould add event_type to properties', () => {
            const event = new Event('type', {});
            expect(event.asJson().event_type).toEqual('type');
        });

        it('should add times to properties', () => {
            const event = new Event('type', {}).asJson();

            expect(typeof event.client_utc_timestamp).toEqual('number');
            expect(typeof event.client_offset_from_utc).toEqual('number');
            expect(event.client_utc_time.length).toBe(7);
            expect(event.client_local_time.length).toBe(7);
        });

        it('should add a guid', () => {
            generateGuid.mockReturnValue('guid');
            const event1 = new Event('type', {});
            expect(event1.properties.id).toEqual('guid');
        });

        it('should add route info', () => {
            jest.spyOn($location, 'url').mockReturnValue('someUrl');
            $route.current = {
                $$route: {
                    directive: 'some-directive',
                },
            };
            const evt = new Event('type', {});
            expect(evt.properties.directive).toEqual('some-directive');
            expect(evt.properties.url).toEqual('someUrl');
        });

        it('should not error if no directive in route', () => {
            $route.current = {};
            const evt = new Event('type', {});
            expect(evt.properties.directive).toBeUndefined();
        });
    });

    describe('asJson', () => {
        it('should add the buffered time', () => {
            const event = new Event('type', {}).asJson();
            expect(typeof event.buffered_time).toEqual('number');
        });
    });

    describe('addDurationInfo', () => {
        it('should add the total time', () => {
            const startEvent = new Event('type', {});
            const finishEvent = new Event('type', {}).addDurationInfo(startEvent);
            const startEventJson = startEvent.asJson();
            const finishEventJson = finishEvent.asJson();
            expect(finishEvent.properties.duration_total).toBeCloseTo(
                finishEventJson.client_utc_timestamp - startEventJson.client_utc_timestamp,
                5,
            );
        });
        it('should use a namespace', () => {
            const startEvent = new Event('type', {});
            const finishEvent = new Event('type', {}).addDurationInfo(startEvent, 'timeSinceSomething');
            const startEventJson = startEvent.asJson();
            const finishEventJson = finishEvent.asJson();
            expect(finishEvent.properties.timeSinceSomething.duration_total).toBeCloseTo(
                finishEventJson.client_utc_timestamp - startEventJson.client_utc_timestamp,
                5,
            );
        });
    });
});
