import Auid from 'Auid';
import { BROWSER_AND_BUILD_INFO } from 'TinyEventLogger';
import 'AngularSpecHelper';
import { generateGuid } from 'guid';
import 'EventLogger/angularModule';

jest.mock('guid', () => ({
    generateGuid: jest.fn(),
}));

describe('EventLogger.EventLogger', () => {
    let EventLogger;
    let EventBundle;
    let Event;
    let $timeout;
    let SpecHelper;
    let segmentio;
    let $rootScope;
    let $location;
    let $route;
    let $injector;
    let mockedEventJson;
    let requestOptions;
    let ApiErrorHandler;
    let HttpQueue;
    let $window;
    let offlineModeManager;

    beforeEach(() => {
        // look for a note about NoUnhandledRejectionExceptions use below (error handling)
        angular.mock.module('EventLogger', 'SpecHelper', 'NoUnhandledRejectionExceptions');

        angular.mock.module($provide => {
            // These are optional dependencies.  Make them available
            $provide.value('ApiErrorHandler', {
                onResponseError: jest.fn(),
            });

            $provide.value('offlineModeManager', {});
        });

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                EventLogger = $injector.get('EventLogger');
                EventBundle = $injector.get('EventLogger.EventBundle');
                Event = $injector.get('EventLogger.Event');
                $timeout = $injector.get('$timeout');
                $window = $injector.get('$window');
                $injector.get('MockIguana');
                SpecHelper = $injector.get('SpecHelper');
                segmentio = $injector.get('segmentio');
                $location = $injector.get('$location');
                $rootScope = $injector.get('$rootScope');
                $route = $injector.get('$route');
                HttpQueue = $injector.get('HttpQueue');
                ApiErrorHandler = $injector.get('ApiErrorHandler');
                offlineModeManager = $injector.get('offlineModeManager');
                offlineModeManager.inOfflineMode = false;

                // Ensure config is initialized so events can be flushed
                SpecHelper.stubConfig();

                // turn the default onUnload behavior back on,
                // since this is the only test where we want to test it.
                EventLogger.prototype._onUnload.mockRestore();
                jest.spyOn(EventLogger.prototype, '_onUnload');

                mockedEventJson = {
                    mock: 'json',
                };
                jest.spyOn(Event.prototype, 'asJson').mockReturnValue(mockedEventJson);
                jest.spyOn(segmentio, 'track').mockReturnValue(mockedEventJson);

                $rootScope.currentUser = {};
                EventLogger.allowEmptyLabel('event_type');

                requestOptions = {
                    'FrontRoyal.ApiErrorHandler': {
                        skip: true,
                    },
                };

                generateGuid.mockReset();
                let guidI = 0;
                generateGuid.mockImplementation(() => {
                    guidI += 1;
                    return `guid-${guidI}`;
                });
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
        EventLogger.destroy();
        offlineModeManager.inOfflineMode = false;
    });

    it('should log some events when the timeout is cleared if something is in the buffer', () => {
        EventBundle.expect('save').toBeCalledWith(
            {
                events: [mockedEventJson],
            },
            {},
            requestOptions,
        );
        EventLogger.log('event_type', {
            prop: 'val',
        });
        $timeout.flush();
        EventBundle.flush('save');
    });

    it('should do nothing when the timeout is cleared if nothing is in the buffer', () => {
        jest.spyOn(EventBundle, 'new');
        // eslint-disable-next-line babel/no-unused-expressions
        EventLogger.instance;
        $timeout.flush();
        expect(EventBundle.new).not.toHaveBeenCalled();
    });

    it('should do nothing when the timeout is cleared if there is no current user', () => {
        $rootScope.currentUser = undefined;
        jest.spyOn(EventBundle, 'new');
        // eslint-disable-next-line babel/no-unused-expressions
        EventLogger.instance;
        $timeout.flush();
        expect(EventBundle.new).not.toHaveBeenCalled();
    });

    it('should do nothing when the timeout is cleared if all events are deferred indefinetely', () => {
        EventLogger.log(
            'event_type',
            {
                prop: 'val',
            },
            {
                deferIndefinitely: true,
            },
        );
        jest.spyOn(EventBundle, 'new');
        $timeout.flush();
        expect(EventBundle.new).not.toHaveBeenCalled();
    });

    it('should requeue events after an offline error', () => {
        const config = {};
        EventBundle.expect('save')
            .toBeCalledWith(
                {
                    events: [mockedEventJson],
                },
                {},
                requestOptions,
            )
            .fails({
                status: 0,
                config,
            });
        jest.spyOn(HttpQueue.prototype, 'unfreezeAfterError');

        // an event gets logged
        EventLogger.log('event_type', {
            prop: 'val1',
        });

        $timeout.flush();
        EventBundle.flush('save');
        expect(HttpQueue.prototype.unfreezeAfterError).toHaveBeenCalledWith(config);

        // another event gets logged
        EventLogger.log('event_type', {
            prop: 'val2',
        });

        // now that we're back online, there should be a new
        // timeout to flush, and it should log both events
        EventBundle.expect('save').toBeCalledWith(
            {
                events: [mockedEventJson, mockedEventJson],
            },
            {},
            requestOptions,
        );
        $timeout.flush();
        EventBundle.flush('save');
    });

    it('should reset AUID if 406 caused by user_from_auid_deleted', () => {
        const config = {
            'FrontRoyal.ApiErrorHandler': {
                skip: true,
            },
        };
        EventBundle.expect('save')
            .toBeCalledWith(
                {
                    events: [mockedEventJson],
                },
                {},
                requestOptions,
            )
            .fails({
                status: 406,
                data: {
                    user_from_auid_deleted: true,
                },
                config,
            });
        jest.spyOn(Auid, 'reset');

        // an event gets logged
        EventLogger.log('event_type', {
            prop: 'val1',
        });

        $timeout.flush();
        EventBundle.flush('save');
        expect(Auid.reset).toHaveBeenCalled();
    });

    it('should use default handling for all other types of errors (besides offline)', () => {
        // see also: NoUnhandledRejectionExceptions module use above

        const config = {
            'FrontRoyal.ApiErrorHandler': {
                skip: true,
            },
        };
        EventBundle.expect('save')
            .toBeCalledWith(
                {
                    events: [mockedEventJson],
                },
                {},
                requestOptions,
            )
            .fails({
                status: 42,
                config,
            });
        jest.spyOn(HttpQueue.prototype, 'unfreezeAfterError');
        jest.spyOn(ApiErrorHandler, 'onResponseError');

        // an event gets logged
        EventLogger.log('event_type', {
            prop: 'val1',
        });

        $timeout.flush();
        EventBundle.flush('save');
        expect(HttpQueue.prototype.unfreezeAfterError).not.toHaveBeenCalled();
        expect(ApiErrorHandler.onResponseError).toHaveBeenCalledWith({
            status: 42,
            config: {
                'FrontRoyal.ApiErrorHandler': {
                    skip: false,
                },
            },
        });
    });

    describe('setLabelProperty', () => {
        it('should map a particular property as the label for a particular event type', () => {
            EventLogger.setLabelProperty('prop', 'eventType1');
            EventLogger.setLabelProperty('prop', 'eventType2');
            EventLogger.setLabelProperty('prop', 'eventType3');

            expect(
                EventLogger.log('eventType1', {
                    prop: 'ok',
                }).properties.label,
            ).toBe('ok');
            expect(EventLogger.log('eventType1', {}).properties.label).toBeUndefined();
            expect(
                EventLogger.log('eventType2', {
                    prop: 'ok',
                }).properties.label,
            ).toBe('ok');
            expect(
                EventLogger.log('eventType3', {
                    prop: 'ok',
                }).properties.label,
            ).toBe('ok');
            expect(
                EventLogger.log('eventType4', {
                    prop: 'ok',
                }).properties.label,
            ).toBeUndefined();
        });
    });

    describe('logStartEvent', () => {
        it('should log a start event and then a finish event on unload', () => {
            jest.spyOn(EventLogger.prototype, 'log').mockReturnValue(Event.prototype);
            jest.spyOn(Event.prototype, 'addDurationInfo').mockImplementation(() => {});

            generateGuid.mockReturnValue('pageLoadId');

            // terrible hack; force Singleton mixin to regenerate the EventLogger to get the hacked properties at initialize
            EventLogger._instance = undefined;
            EventLogger.logStartEvent();
            EventLogger.instance._onUnload();

            expect(EventLogger.prototype.log.mock.calls.length).toBe(2);
            expect(EventLogger.prototype.log.mock.calls[0][0]).toBe('page_load:load');
            expect(EventLogger.prototype.log.mock.calls[0][1].client).toBe(BROWSER_AND_BUILD_INFO.client);
            expect(EventLogger.prototype.log.mock.calls[1][0]).toBe('page_load:unload');

            Object.keys(BROWSER_AND_BUILD_INFO, k => {
                const v = BROWSER_AND_BUILD_INFO[k];
                expect(EventLogger.prototype.log.mock.calls[0][1][k]).toEqual(v);
            });
            expect(Event.prototype.addDurationInfo).toHaveBeenCalled();
        });
        it('should log even if all events are deferred indefinetely', () => {
            EventBundle.expect('save').toBeCalledWith(
                {
                    events: [mockedEventJson],
                },
                {},
                requestOptions,
            );

            EventLogger.log(
                'event_type',
                {
                    prop: 'val',
                },
                {
                    deferIndefinitely: true,
                },
            );
            EventLogger.instance._onUnload();
            EventBundle.flush('save');
        });
        it('if not called should do nothing on unload', () => {
            jest.spyOn(EventLogger.prototype, 'log').mockReturnValue(Event.prototype);
            EventLogger.instance._onUnload('page_load:unload');
            expect(EventLogger.prototype.log).not.toHaveBeenCalled();
        });
    });

    describe('logging to segmentio', () => {
        it('should happen by default', () => {
            const event = EventLogger.log('event_type', {
                prop: 'val',
            });
            $timeout.flush(0.001); // only clear the immediate timeout, not the 10sec clearBuffer timeout
            expect(segmentio.track).toHaveBeenCalled();
            expect(event.properties.log_to_customerio).toBe(true);
        });
        it('should not happen if opting out', () => {
            const event = EventLogger.log(
                'event_type',
                {
                    prop: 'val',
                },
                {
                    segmentio: false,
                },
            );
            $timeout.flush(0.001); // only clear the immediate timeout, not the 10sec clearBuffer timeout
            expect(segmentio.track).not.toHaveBeenCalled();
            expect(event.properties.log_to_customerio).toBeUndefined();
        });
        it('should warn if no label', () => {
            const ErrorLogService = $injector.get('ErrorLogService');
            jest.spyOn(ErrorLogService, 'notify');
            EventLogger.log(
                'no_label',
                {},
                {
                    segmentio: true,
                },
            );
            expect(ErrorLogService.notify).toHaveBeenCalledWith(
                "'no_label' event logged to segmentio without a label.",
            );
        });

        it('should not warn if no label is allowed', () => {
            EventLogger.allowEmptyLabel('no_label');
            const ErrorLogService = $injector.get('ErrorLogService');
            jest.spyOn(ErrorLogService, 'notify');
            EventLogger.log(
                'no_label',
                {},
                {
                    segmentio: true,
                },
            );
            expect(ErrorLogService.notify).not.toHaveBeenCalled();
        });

        it('should support setting a special type and label', () => {
            const event = EventLogger.log(
                'event_type',
                {
                    prop: 'val',
                },
                {
                    segmentioType: 'special type',
                    segmentioLabel: 'special label',
                },
            );
            $timeout.flush(0.001); // only clear the immediate timeout, not the 10sec clearBuffer timeout
            expect(segmentio.track).toHaveBeenCalled();
            expect(segmentio.track.mock.calls[0][0]).toEqual('special type');
            expect(segmentio.track.mock.calls[0][1].label).toEqual('special label');
            expect(event.properties.log_to_customerio).toBe(true);
        });

        it('should support disabling integrations via the onetrust cookie helper', () => {
            jest.spyOn(segmentio.onetrustCookieHelper, 'getDisabledIntegrationsObject').mockReturnValue({
                OriAnalytics: false,
            });

            // this is the internal track call that *actually* proxies the call to segment
            segmentio.track.mockRestore();
            jest.spyOn(segmentio, '__track').mockImplementation(() => {});
            EventLogger.log('event_type', {
                prop: 'val',
            });
            $timeout.flush(0.001); // only clear the immediate timeout, not the 10sec clearBuffer timeout
            expect(segmentio.__track).toHaveBeenCalled();
            expect(segmentio.__track.mock.calls[0][2]).toEqual({
                integrations: {
                    'Facebook Custom Audiences': true,
                    'Facebook Pixel': true,
                    OriAnalytics: false,
                    'Customer.io': false,
                },
            });
        });

        it('should disable Facebook Pixel for the first_view_rendered event', () => {
            // this is the internal track call that *actually* proxies the call to segment
            segmentio.track.mockRestore();
            jest.spyOn(segmentio, '__track').mockImplementation(() => {});
            EventLogger.allowEmptyLabel('first_view_rendered');
            EventLogger.log('first_view_rendered', {
                prop: 'val',
            });
            $timeout.flush(0.001); // only clear the immediate timeout, not the 10sec clearBuffer timeout
            expect(segmentio.__track).toHaveBeenCalled();
            expect(segmentio.__track.mock.calls[0][2]).toEqual({
                integrations: {
                    'Facebook Custom Audiences': true,
                    'Facebook Pixel': false,
                    'Customer.io': false,
                },
            });
        });

        it('should not happen when in offlineModeManager', () => {
            offlineModeManager.inOfflineMode = true;
            const event = EventLogger.log('event_type', {
                prop: 'val',
            });
            $timeout.flush(0.001); // only clear the immediate timeout, not the 10sec clearBuffer timeout
            expect(segmentio.track).not.toHaveBeenCalled();
            expect(event.properties.log_to_customerio).toBe(true);
        });
    });

    describe('linked in pixel logging', () => {
        let linkedInConversionId;
        const eventType = 'myEvent';

        beforeEach(() => {
            const config = $injector.get('ConfigFactory').getSync();
            linkedInConversionId = 12345;
            jest.spyOn(EventLogger.prototype, '_getConversionPixelIds').mockReturnValue([linkedInConversionId]);
            jest.spyOn(EventLogger.prototype, 'tryToSaveBuffer');
            jest.spyOn(config, 'isQuantic').mockReturnValue('isQuantic');
            jest.spyOn(config, 'appEnvType').mockReturnValue('appEnvType');
        });

        it('should work', () => {
            const img = setupLinkedInConfigAndLog();
            expect(findPixel(linkedInConversionId)).not.toBeUndefined();
            // the pixel should be immediately removed
            img.trigger('load');
            expect(findPixel(linkedInConversionId)).toBeUndefined();
            expect(EventLogger.prototype._getConversionPixelIds).toHaveBeenCalledWith(
                eventType,
                'isQuantic',
                'appEnvType',
            );
        });

        it('should be disabled if onetrustCookieHelper says so', () => {
            jest.spyOn(segmentio.onetrustCookieHelper, 'integrationDisabled').mockReturnValue(true);
            setupLinkedInConfigAndLog();
            expect(findPixel(linkedInConversionId)).toBeUndefined();
            expect(segmentio.onetrustCookieHelper.integrationDisabled).toHaveBeenCalledWith(
                'LinkedIn Conversion Tracking',
            );
        });

        function setupLinkedInConfigAndLog() {
            EventLogger.unsegmentedConfig = {
                myEvent: {
                    linkedInPixels: [
                        {
                            envType: linkedInConversionId,
                        },
                    ],
                },
            };
            const origImages = $('body > img');
            origImages.attr('here-before-linked-in-pixel-test', 'true');
            EventLogger.log(eventType);
            $timeout.flush(0);
            return $('body > img:not([here-before-linked-in-pixel-test])');
        }

        function findPixel(conversionId) {
            let img;
            $('body')
                .find('img')
                .each(function iter() {
                    // We need `this` so don't use an arrow function
                    const el = $(this);
                    if (
                        el
                            .attr('src')
                            .match(`https://dc.ads.linkedin.com/collect/\\?pid=24209&conversionId=${conversionId}`)
                    ) {
                        img = el;
                    }
                });
            return img;
        }
    });

    describe('logging to cordovaFacebook', () => {
        beforeEach(() => {
            window.CORDOVA = true;
            window.facebookConnectPlugin = {
                logEvent: jest.fn(),
                logPurchase: jest.fn(),
            };
        });

        afterEach(() => {
            window.CORDOVA = undefined;
            window.facebookConnectPlugin = undefined;
        });

        it('should not happen by default', () => {
            EventLogger.log('event_type', {
                prop: 'val',
            });
            expect(window.facebookConnectPlugin.logEvent).not.toHaveBeenCalled();
        });

        it('should not happen if opting in', () => {
            EventLogger.log(
                'event_type',
                {
                    prop: 'val',
                },
                {
                    cordovaFacebook: true,
                },
            );
            expect(window.facebookConnectPlugin.logEvent).toHaveBeenCalled();
            expect(window.facebookConnectPlugin.logEvent.mock.calls[0][0]).toEqual('event_type');
        });

        it('should sanitize improper event types', () => {
            EventLogger.log(
                'something-$super:l0ng+and_invalid_and_stuff',
                {
                    prop: 'val',
                },
                {
                    cordovaFacebook: true,
                },
            );
            expect(window.facebookConnectPlugin.logEvent).toHaveBeenCalled();
            expect(window.facebookConnectPlugin.logEvent.mock.calls[0][0]).toEqual(
                'something-_super_l0ng_and_invalid_and_st',
            );
        });

        it('should use logPurchase for properly formatted purchase events', () => {
            const props = {
                label: 'invalid',
            };

            const opts = {
                cordovaFacebook: true,
            };

            EventLogger.log('some:conversion_goal_event', props, opts);
            expect(window.facebookConnectPlugin.logPurchase).not.toHaveBeenCalled();

            props.label = 'Purchase';
            EventLogger.log('some:conversion_goal_event', props, opts);
            expect(window.facebookConnectPlugin.logPurchase).not.toHaveBeenCalled();

            props.value = 123;
            EventLogger.log('some:conversion_goal_event', props, opts);
            expect(window.facebookConnectPlugin.logPurchase).not.toHaveBeenCalled();

            props.currency = 'USD';
            EventLogger.log('some:conversion_goal_event', props, opts);
            expect(window.facebookConnectPlugin.logPurchase).toHaveBeenCalled();
        });
    });

    describe('trackFirstViewRender', () => {
        beforeEach(() => {
            SpecHelper.stubWindowPerformance(42);
            jest.spyOn(EventLogger.prototype, 'log');
            EventLogger.trackFirstViewRender();
        });

        it('should work with a directive in the route', () => {
            $route.current = {
                $$route: {
                    directive: 'some-directive',
                },
            };
            $rootScope.$emit('$viewContentLoaded');
            expect(EventLogger.prototype.log.mock.calls.length).toBe(1);
            expect(EventLogger.prototype.log.mock.calls[0][1]).toEqual({
                value: 0.042,
                label: 'some-directive',
            });
        });

        it('should work with no directive in the route', () => {
            $route.current = {
                $$route: {},
            };
            $rootScope.$emit('$viewContentLoaded');
            expect(EventLogger.prototype.log.mock.calls.length).toBe(1);
            expect(EventLogger.prototype.log.mock.calls[0][1]).toEqual({
                value: 0.042,
                label: 'unknown',
            });
        });

        it('should only log the first view render', () => {
            $rootScope.$emit('$viewContentLoaded');
            $rootScope.$emit('$viewContentLoaded');
            expect(EventLogger.prototype.log.mock.calls.length).toBe(1);
        });
    });

    describe('trackLocationChanges', () => {
        let routes;
        let params;
        let paths;
        beforeEach(() => {
            routes = [
                {
                    $$route: {
                        directive: 'my-directive-1',
                    },
                },
                {
                    $$route: {
                        directive: 'my-directive-2',
                    },
                },
            ];
            params = [
                {
                    prop: 1,
                },
                {
                    prop: 2,
                },
            ];
            paths = ['/path-1', '/path-2'];
            let pathIndex = 0;

            jest.spyOn($location, 'url').mockImplementation(() => {
                const path = paths[pathIndex];
                pathIndex += 1;
                return path;
            });

            EventLogger.trackLocationChanges();

            jest.spyOn(Event.prototype, 'addDurationInfo').mockImplementation(() => {});
            jest.spyOn(EventLogger.prototype, 'log').mockReturnValue(Event.prototype);
        });

        it('should track location changes on $routeChangeSuccess', () => {
            mockRouteChange(0);
            mockRouteChange(1);
            // mock out the window unload
            EventLogger.instance._onUnload();

            expect(EventLogger.prototype.log.mock.calls.length).toBe(4);
            SpecHelper.expectEqualObjects(
                [
                    'route_change:my-directive-1:enter',
                    {
                        directive: 'my-directive-1',
                        params: params[0],
                        path: paths[0],
                        source_directive: undefined,
                        source_params: undefined,
                        source_path: undefined,
                    },
                    {
                        segmentio: false,
                    },
                ],
                EventLogger.prototype.log.mock.calls[0],
            );

            SpecHelper.expectEqualObjects(
                [
                    'route_change:my-directive-1:leave',
                    {
                        directive: 'my-directive-1',
                        params: params[0],
                        path: paths[0],
                        destination_directive: 'my-directive-2',
                        destination_params: params[1],
                        destination_path: paths[1],
                    },
                    {
                        segmentio: false,
                    },
                ],
                EventLogger.prototype.log.mock.calls[1],
            );

            SpecHelper.expectEqualObjects(
                [
                    'route_change:my-directive-2:enter',
                    {
                        directive: 'my-directive-2',
                        params: params[1],
                        path: paths[1],
                        source_directive: 'my-directive-1',
                        source_params: params[0],
                        source_path: paths[0],
                    },
                    {
                        segmentio: false,
                    },
                ],
                EventLogger.prototype.log.mock.calls[2],
            );
            SpecHelper.expectEqualObjects(
                [
                    'route_change:my-directive-2:leave',
                    {
                        directive: 'my-directive-2',
                        params: params[1],
                        path: paths[1],
                        destination_directive: undefined,
                        destination_params: undefined,
                        destination_path: undefined,
                    },
                    {
                        segmentio: false,
                    },
                ],
                EventLogger.prototype.log.mock.calls[3],
            );
            expect(Event.prototype.addDurationInfo.mock.calls.length).toBe(2);
        });

        it('should make page() calls', () => {
            jest.spyOn(EventLogger.prototype, '_logThirdPartyPageCall').mockImplementation();
            mockRouteChange(0);
            expect(EventLogger.prototype._logThirdPartyPageCall).toHaveBeenCalledWith('my-directive-1', {
                url: window.location.href,
                path: $location.path(),
            });
        });

        describe('onUserChange', () => {
            it('should work', () => {
                jest.spyOn(EventLogger.prototype, 'tryToSaveBuffer');
                EventLogger.logStartEvent();
                mockRouteChange(0);
                EventLogger.onUserChange('auid');
                mockRouteChange(1);
                expect(_.pluck(EventLogger.prototype.log.mock.calls, '0')).toEqual([
                    // with user 1
                    'page_load:load',
                    'route_change:my-directive-1:enter',
                    'route_change:my-directive-1:leave',
                    'page_load:unload',

                    // with user 2
                    'page_load:load',
                    'route_change:my-directive-2:enter',
                ]);
                expect(EventLogger.prototype.tryToSaveBuffer).toHaveBeenCalledWith(true, {
                    force_auid: 'auid',
                });
            });
        });

        function mockRouteChange(i) {
            $route.current = {
                params: params[i],
            };
            $rootScope.$emit('$routeChangeSuccess', routes[i]);
        }
    });

    describe('logMarketingPageView', () => {
        beforeEach(() => {
            $window.disableFrontRoyalRoutes = true;
            jest.spyOn(Event.prototype, 'addDurationInfo').mockImplementation(() => {});
            jest.spyOn(EventLogger.prototype, 'log').mockReturnValue(Event.prototype);
            jest.spyOn(EventLogger.prototype, '_logThirdPartyPageCall').mockImplementation();
        });

        afterEach(() => {
            delete $window.disableFrontRoyalRoutes;
        });

        it('should make the appropriate page and track calls', () => {
            EventLogger.trackLocationChanges();

            expect(EventLogger.prototype.log.mock.calls.length).toBe(1);

            // specs run in a file called root.html, so the computed title based on the path will be 'root'
            const pageTitle = 'root';

            SpecHelper.expectEqualObjects(
                [
                    'marketing:pageview',
                    {
                        label: pageTitle,
                    },
                    {
                        segmentio: false,
                    },
                ],
                EventLogger.prototype.log.mock.calls[0],
            );

            expect(EventLogger.prototype._logThirdPartyPageCall).toHaveBeenCalledWith(pageTitle, {
                url: window.location.href,
                path: window.location.pathname,
            });
        });
    });

    // see comment in code at trackFocusAndBlur concerning
    // why this is commented out.
    // describe('trackFocusAndBlur', function() {
    //     it('should log events on focus and blur on input elements', function() {
    //         assertTracksFocusAndBlur('input');
    //     });

    //     it('should log events on focus and blur on select elements', function() {
    //         assertTracksFocusAndBlur('select');
    //     });

    //     it('should log events on focus and blur on textarea elements', function() {
    //         assertTracksFocusAndBlur('textarea');
    //     });

    //     function assertTracksFocusAndBlur(nodeName) {
    //         var event;
    //         var origLog = EventLogger.prototype.log;
    //         jest.spyOn(EventLogger.prototype, 'log').mockImplementation(function() {
    //             event = origLog.apply(this, arguments);
    //             return event;
    //         });

    //         event = undefined;
    //         EventBundle.expect('save');
    //         EventLogger.prototype.log.mockClear();
    //         EventLogger.trackFocusAndBlur();

    //         var renderer = SpecHelper.render('<div><' + nodeName + ' ng-click="doSomething()" id="id" class="css class" name="name"></' + nodeName + '></div>');
    //         $('body').append(renderer.elem); // spechelper will cleanup
    //         renderer.elem.find(nodeName).focus();

    //         var props = [{
    //             elem: {
    //                 node_type: nodeName.toUpperCase(),
    //                 css_classes: ['css', 'class'],
    //                 name: 'name',
    //                 id: 'id'
    //             }
    //         }, {
    //             segmentio: false
    //         }];

    //         expect(EventLogger.prototype.log).toHaveBeenCalled();
    //         SpecHelper.expectEqualObjects(['focus'].concat(props), EventLogger.prototype.log.mock.calls[0]);

    //         renderer.elem.find(nodeName).blur();
    //         expect(EventLogger.prototype.log).toHaveBeenCalled();
    //         SpecHelper.expectEqualObjects(['blur'].concat(props), EventLogger.prototype.log.mock.calls[1]);

    //         return true;
    //     }
    // });

    describe('trackAllNgEvents', () => {
        let elem;
        let scope;
        let event;
        let renderer;

        beforeEach(() => {
            renderer = SpecHelper.renderer();
            renderer.scope.myLabel = 'theThingIClicked';
            EventBundle.expect('save');
            EventLogger.trackAllNgEvents('ngClick');
            const origLog = EventLogger.prototype.log;
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(function mockLog(...args) {
                // We need `this`, so don't use an arrow function
                event = origLog.apply(this, args);
                return event;
            });
        });

        it('should log events when an ng- listener is triggered', () => {
            render('<div><div ng-click="doSomething()" label="myLabel">some text</div></div>');
            assertClickLogged(
                {
                    label: scope.myLabel,
                },
                {
                    segmentio: false,
                },
            );
        });

        it('should use the ng-click as the label if there is no label', () => {
            render('<div><div ng-click="doSomething()" >some text</div></div>');
            assertClickLogged(
                {
                    label: 'doSomething()',
                },
                {
                    segmentio: false,
                },
            );
        });

        it('should support turning off auto-logging', () => {
            render('<div><div ng-click="doSomething()" >some text</div></div>');
            SpecHelper.click(elem, '[ng-click]');
            $timeout.flush();
            expect(EventLogger.prototype.log).toHaveBeenCalled();
            EventLogger.prototype.log.mockClear();

            EventLogger.trackAllNgEvents('ngClick', false);
            SpecHelper.click(elem, '[ng-click]');
            $timeout.flush();
            expect(EventLogger.prototype.log).not.toHaveBeenCalled();
        });

        it('should track no-apply events', () => {
            render('<div><div no-apply-click="doSomething()">some text</div></div>');
            SpecHelper.click(elem, 'div');
            expect(EventLogger.prototype.log).toHaveBeenCalled();
            SpecHelper.expectEqualObjects(
                [
                    'click',
                    {
                        label: 'doSomething()',
                    },
                    {
                        segmentio: false,
                    },
                ],
                EventLogger.prototype.log.mock.calls[0],
            );

            $timeout.flush();
            SpecHelper.expectEqual(
                ['toEvalAsync', 'toTimeout'],
                Object.keys(event.properties.performance).sort(),
                'performance values set',
            );
        });

        it('should warn if Ng event logged with segmentio attrs', () => {
            const ErrorLogService = $injector.get('ErrorLogService');
            jest.spyOn(ErrorLogService, 'notify');

            render('<div><div ng-click="doSomething()" segmentio label="myLabel">some text</div></div>');
            SpecHelper.click(elem, '[ng-click]');

            expect(ErrorLogService.notify).toHaveBeenCalledWith("Ng 'click' event tracked with segmentio attrs.");
        });

        function render(html) {
            renderer.render(html);
            elem = renderer.elem;
            scope = renderer.scope;
        }

        function assertClickLogged(payload, options) {
            SpecHelper.click(elem, 'div');
            SpecHelper.expectEqual(
                ['toEvalAsync'],
                Object.keys(event.properties.performance),
                'performance object initialized',
            );

            $timeout.flush();
            expect(EventLogger.prototype.log).toHaveBeenCalled();
            SpecHelper.expectEqualObjects(['click', payload, options], EventLogger.prototype.log.mock.calls[0]);

            $timeout.flush();
            SpecHelper.expectEqual(
                ['toEvalAsync', 'toTimeout'],
                Object.keys(event.properties.performance).sort(),
                'performance values set',
            );
        }
    });

    describe('tryToClearBuffer', () => {
        it('should return an eventBundle', () => {
            EventLogger.log('event_type', {
                prop: 'val',
            });
            expect(EventLogger.tryToClearBuffer().events).toEqual([mockedEventJson]);
        });
        it('should return nothing if no events', () => {
            expect(EventLogger.tryToClearBuffer()).toBe(null);
        });
    });

    describe('_logThirdPartyPageCall', () => {
        it('should call segmentio.page', () => {
            jest.spyOn(segmentio, 'page');
            EventLogger.prototype._logThirdPartyPageCall('pageTitle', { url: 'someUrl', path: 'somePath' });
            expect(segmentio.page).toHaveBeenCalledWith(undefined, 'pageTitle', {
                path: 'somePath',
                title: 'pageTitle',
                url: 'someUrl',
            });
        });
    });
});
