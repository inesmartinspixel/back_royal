if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
}

if (!Element.prototype.closest) {
    Element.prototype.closest = function closest(s) {
        let el = this;

        do {
            if (el.matches(s)) return el;
            el = el.parentElement || el.parentNode;
        } while (el !== null && el.nodeType === 1);
        return null;
    };
}

/*
 *  Pixel Select
 */
document.addEventListener('DOMContentLoaded', () => {
    const selectClass = 'select.pixel-select';
    const selectOpenClass = 'pixel-select--open';
    const selectNotSelectedClass = 'pixel-select--not-selected';
    const defaultPlacedholder = 'Select';
    let openSelect = false;

    function bindEvents() {
        const pixelSelect = document.querySelectorAll('div.pixel-select');

        pixelSelect.forEach(eachPixelSelect => {
            const eachPixelSelectCurrent = eachPixelSelect.querySelector('.pixel-select__current');
            const eachPixelSelectDropdown = eachPixelSelect.querySelector('.pixel-select__dropdown');
            const eachPixelSelectOptions = eachPixelSelect.querySelectorAll('.pixel-select__option');

            eachPixelSelectCurrent.addEventListener('click', event => {
                event.preventDefault();
                event.stopPropagation();
                closeAllDropdowns();

                if (openSelect) {
                    openSelect = false;
                } else {
                    eachPixelSelectDropdown.style.display = 'block';
                    eachPixelSelect.classList.add(selectOpenClass);
                    openSelect = true;
                }
            });

            const eachRealSelect = eachPixelSelect.closest('.pixel-select-container').querySelector('select');

            eachPixelSelectOptions.forEach(element => {
                element.addEventListener('click', event => {
                    const clickedOption = event.currentTarget;
                    const clickedOptionText = clickedOption.textContent;
                    const clickedOptionValue = clickedOption.getAttribute('data-value');

                    eachRealSelect.value = clickedOptionValue;

                    let changeEvent;
                    if (typeof Event === 'function') {
                        changeEvent = new Event('change');
                    } else {
                        changeEvent = document.createEvent('Event'); // IE11
                        changeEvent.initEvent('change', true, true);
                    }
                    eachRealSelect.dispatchEvent(changeEvent);

                    eachPixelSelectCurrent.textContent = clickedOptionText;
                    eachPixelSelectOptions.forEach(selectOption => selectOption.removeAttribute('data-selected'));
                    clickedOption.setAttribute('data-selected', '1');

                    eachPixelSelect.classList.remove(selectOpenClass);
                    eachPixelSelect.classList.remove(selectNotSelectedClass);

                    eachPixelSelectDropdown.style.display = 'none';
                    openSelect = false;
                });
            });
        });
    }

    function buildSelect() {
        const allSelects = document.querySelectorAll(selectClass);

        allSelects.forEach(eachRealSelect => {
            const eachRealSelectOptions = eachRealSelect.querySelectorAll('option');
            let selectOptions = '';
            let selectedOption = defaultPlacedholder;
            let placeholderClass = '';

            const pixelSelectContainer = document.createElement('div');
            pixelSelectContainer.classList.add('pixel-select-container');
            pixelSelectContainer.innerHTML = eachRealSelect.outerHTML;
            eachRealSelect.parentNode.insertBefore(pixelSelectContainer, eachRealSelect);
            eachRealSelect.parentNode.removeChild(eachRealSelect); // remove the now wrapped (duplicated) select

            eachRealSelectOptions.forEach(realSelectOption => {
                const realSelectOptionValue = realSelectOption.getAttribute('value');
                const realSelectOptionText = realSelectOption.textContent;
                const realSelectOptionHasNoValue =
                    realSelectOptionValue.length < 1 || realSelectOptionValue === 'undefined';

                if (realSelectOption.getAttribute('selected') !== null) {
                    selectedOption = realSelectOptionText;
                    if (realSelectOptionHasNoValue) {
                        placeholderClass = 'pixel-select--not-selected';
                    }
                }

                if (realSelectOptionValue.length > 0 && realSelectOptionText.length > 0) {
                    selectOptions += `<li class="pixel-select__option" data-value="${realSelectOptionValue}">${realSelectOptionText}</li>`;
                }
            });

            const pixelSelectElem = document.createElement('div');
            pixelSelectElem.innerHTML = `
                <div class="pixel-select__current">${selectedOption}</div>
                <ul class="pixel-select__dropdown">${selectOptions}</ul>
            `;
            pixelSelectElem.classList.add('pixel-select');
            if (placeholderClass) {
                pixelSelectElem.classList.add(placeholderClass);
            }
            pixelSelectContainer.appendChild(pixelSelectElem);
        });

        if (allSelects.length) {
            bindEvents();
        }
    }

    function parents(node) {
        let current = node;
        const list = [];
        while (current.parentNode != null && current.parentNode !== document.documentElement) {
            list.push(current.parentNode);
            current = current.parentNode;
        }
        return list;
    }

    function closeDropdown(event) {
        const pixelSelect = document.querySelectorAll('.pixel-select');
        const pixelSelectDropdown = document.querySelectorAll('.pixel-select__dropdown');

        const targetParents = parents(event.target);
        let hasPixelSelect = false;
        targetParents.forEach(parent => {
            if (parent.classList.contains('pixel-select')) {
                hasPixelSelect = true;
            }
        });

        if (!event.target.classList.contains('pixel-select') && !hasPixelSelect) {
            pixelSelectDropdown.forEach(dropdown => {
                dropdown.style.display = 'none';
            });
            pixelSelect.forEach(select => select.classList.remove(selectOpenClass));
            openSelect = false;
        }
    }

    function closeAllDropdowns() {
        const pixelSelect = document.querySelectorAll('.pixel-select');
        const pixelSelectDropdown = document.querySelectorAll('.pixel-select__dropdown');
        pixelSelect.forEach(select => select.classList.remove(selectOpenClass));
        pixelSelectDropdown.forEach(dropdown => {
            dropdown.style.display = 'none';
        });
    }

    document.querySelector('html').addEventListener('click', event => {
        closeDropdown(event);
    });

    document.addEventListener(
        'touchstart',
        event => {
            closeDropdown(event);
        },
        false,
    );

    document.querySelector('html').addEventListener('tap', event => {
        closeDropdown(event);
    });

    buildSelect();
});
