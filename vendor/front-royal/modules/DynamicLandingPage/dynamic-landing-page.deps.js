import { logTinyEvent } from 'TinyEventLogger';
import ServerTime, { ensureServerClientTimeOffsetOnWindow } from 'ServerTime';

import './pixel-select';
import './emba-form-step';
import './price-comparison-table';

// FIXME: we can remove onChangeEmbaFormSelect now
const serverTime = new ServerTime(ensureServerClientTimeOffsetOnWindow());

// FIXME: See https://trello.com/c/B82ynZ6V
window.authInitVideoModal = true;
function onChangeEmbaFormSelect(event, select) {
    if (!window.embaFormStepInitialized) {
        logTinyEvent(
            'dynamic_landing_page:script_not_available',
            {
                label: 'preSignupForm',
                action: `selected ${select.getAttribute('name').slice(0, 100)}`,
            },
            serverTime,
        );
    }
}

window.onChangeEmbaFormSelect = onChangeEmbaFormSelect;
