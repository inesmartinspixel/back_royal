// On the Dynamic Landing Page, we show price comparison tables. The display
// of these ables depends on some code in vendor/marketing/scripts/main.js.
// Rather than bring that code into the DLP entrypoint, and increasing the
// size of the entrypoint as a result, we just hide the element until we're sure
// the marketing scripts have loaded.

function waitForComparisonTablesReady() {
    function onComparisonTablesAvailable(cb) {
        if (window.comparisonTablesReady) {
            return cb(window.comparisonTablesReady);
        }
        return setTimeout(() => onComparisonTablesAvailable(cb), 500);
    }
    return new Promise(resolve => onComparisonTablesAvailable(resolve));
}

document.addEventListener('DOMContentLoaded', () => {
    const comparisonTables = document.getElementsByClassName('table-slider');
    comparisonTables.forEach(elem => {
        elem.style.visibility = 'hidden';
    });

    waitForComparisonTablesReady().then(() => {
        comparisonTables.forEach(elem => {
            elem.style.visibility = 'visible';
        });

        const spinner = document.getElementById('loading-comparison-tables');
        if (spinner) {
            spinner.style.display = 'none';
        }
    });
});
