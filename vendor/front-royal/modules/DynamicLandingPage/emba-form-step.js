import { logTinyEvent } from 'TinyEventLogger';
import ServerTime, { ensureServerClientTimeOffsetOnWindow } from 'ServerTime';

const serverTime = new ServerTime(ensureServerClientTimeOffsetOnWindow());
const preSignupForm = {};
let showedSignupForm;

// This will be called my marketing-sign-up-form after the user has been
// logged in and we've logged the logged_in_first_time event
window.afterSignedUpOnDynamicLandingPage = function afterSignedUpOnDynamicLandingPage() {
    showSuccess();

    const loadingMessage = document.querySelector('.emba-form__success .front-royal-spinner');
    const continueButtonWrapper = document.querySelector('#continue-button-wrapper');
    loadingMessage.style.display = 'none';
    continueButtonWrapper.style.display = 'block';

    // I have no explanation for why we need to 50ms delay here, but not below
    // when transitioning between form steps.
    setTimeout(() => continueButtonWrapper.classList.add('visible'), 50);
};

function validateForm() {
    const currentStep = document.querySelector('.emba-step__tracker span').textContent;
    const formStep = document.querySelector(`.emba-form__step[data-step="${currentStep}"]`);
    const formStepSelects = formStep.querySelectorAll('select.pixel-select');
    let validForm = true;

    formStepSelects.forEach(element => {
        const selectName = element.name;
        const selectValue = element.value;
        if (typeof selectValue === 'undefined' || selectValue === null) {
            validForm = false;
        } else {
            preSignupForm[selectName] = selectValue;
        }
    });

    if (validForm) {
        formStep.classList.add('emba-form--valid');
        return true;
    }

    return false;
}

function gotoNextStep() {
    const currentStep = document.querySelector('.emba-step__tracker span').textContent;
    const currentStepContainer = document.querySelector(`.emba-form__step[data-step="${currentStep}"]`);
    const nextStep = parseInt(currentStep, 10) + 1;
    const nextStepContainer = document.querySelector(`.emba-form__step[data-step="${nextStep}"]`);
    const nextStepMainTitle = nextStepContainer.getAttribute('data-main-title');
    const nextStepTitle = nextStepContainer.getAttribute('data-title');
    const formIsValid = validateForm(nextStep);

    if (formIsValid) {
        document.querySelector('.emba-step__bar').setAttribute('data-bar-step', nextStep);
        currentStepContainer.style.display = 'none';

        nextStepContainer.style.display = 'block';
        setTimeout(() => nextStepContainer.classList.add('visible'));

        document.querySelector('.emba-step__title').textContent = nextStepTitle;
        document.querySelector('.emba-step__tracker span').textContent = nextStep;

        if (nextStepMainTitle) {
            document.querySelector('.emba-form__main-title').textContent = nextStepMainTitle;
        }

        const onSignUpForm = nextStepContainer.querySelectorAll('[marketing-sign-up-form]').length > 0;

        if (onSignUpForm) {
            showedSignupForm = true;
        }

        if (onSignUpForm && !getInjector()) {
            logTinyEvent(
                'dynamic_landing_page:script_not_available',
                {
                    label: 'frontRoyalApp',
                    action: 'displayed sign up form',
                },
                serverTime,
            );
        }

        // Once the user makes it to the signup form, put the form responses into local storage.
        // If the user the registers through this form, or if ze navigates to candidate/signup and
        // register there, we will have the values available and be able to put them on the
        // logged_in_first_time event, which will allow us to identify "good" signups in facebook
        if (onSignUpForm && getInjector()) {
            const preSignupValues = getInjector().get('preSignupValues');
            preSignupValues.storeDynamicLandingPageMultiStepFormInClientStorage(preSignupForm);
        }
    }

    logTinyEvent(
        'dynamic_landing_page:clicked_next_step',
        {
            label: 'dynamic_landing_page:clicked_next_step',
            current_step_index: parseInt(currentStep, 10),
            current_step_title: currentStepContainer.getAttribute('data-title'),
        },
        serverTime,
    );
}

function getInjector() {
    // Hopefully, by the time we're trying to access the injector,
    // jquery has been lazy-loaded successfully
    if (!window.$) {
        return null;
    }
    return $('[ng-controller]').injector();
}

function showSuccess() {
    document.querySelectorAll('.js-emba').forEach(el => {
        el.style.display = 'none';
    });

    document.querySelector('.emba-form__success').style.display = 'block';

    // scroll the success message onto the screen
    // 1. Only do this if you just filled out the signup form.  See https://trello.com/c/0PTsw1BH for an explanation
    if (showedSignupForm) {
        window.scrollTo(0, 0);
    }

    logTinyEvent(
        'dynamic_landing_page:viewed_success_message',
        { label: 'dynamic_landing_page:viewed_success_message' },
        serverTime,
    );
}

// This is only called if you hit the page with an invalid auth token.
// Once we realize the token is invalid, we remove the success message.
function hideSuccess() {
    document.querySelectorAll('.js-emba').forEach(el => {
        el.style.display = 'block';
    });
    document.querySelector('.emba-form__success').style.display = 'none';
}

function waitForInjector() {
    function onInjectorAvailable(cb) {
        const inj = getInjector();
        if (inj) {
            return cb(inj);
        }
        return setTimeout(() => onInjectorAvailable(cb), 500);
    }
    return new Promise(resolve => onInjectorAvailable(resolve));
}

/*
 *  EMBA Form Multi Step
 */
document.addEventListener('DOMContentLoaded', () => {
    // If we're coming back from oauth, or if the user has refreshed the page,
    // then we will hit this page with auth headers.  In that case, show the success
    // message right away, even though we will wait until the app is loaded
    // before we activate the continue button.
    if (window.hasAuthHeaders) {
        showSuccess();
    }

    const preSignupFormStepButtons = document.querySelectorAll('.emba-form__button');
    window.embaFormStepInitialized = true;
    window.preSignupForm = preSignupForm; // FIXME: remove this assignment to window once we stop logging registration:completed_pre_signup_form

    // If we are on the dynamic landing page, log an event so we know
    // how long it took before we could initialize the form js
    if (preSignupFormStepButtons.length > 0) {
        logTinyEvent('dynamic_landing_page:preSignupForm_initialized', { value: window.performance.now() }, serverTime);
    }

    // Include our hidden input that we just want to passthrough
    // to our backend using the existing preSignupForm infrastructure,
    // which handles both manual and omniauth registration as well as
    // logging an event.
    // See https://trello.com/c/AF4bThbe
    const experimentId = document.querySelector('.main-emba [name="experiment_id"]').value;
    preSignupForm.experiment_id = experimentId;

    window.showSuccess = showSuccess;
    window.hideSuccess = hideSuccess;

    document.querySelectorAll('select.pixel-select').forEach(select => {
        select.addEventListener('change', validateForm);
    });

    preSignupFormStepButtons.forEach(button => {
        button.addEventListener('click', gotoNextStep);
    });

    // We cannot show the application deadline until we have the datehelper.
    // There is a non-angular version of the date helper, so we could just make
    // that available inline, but we'd have to include that code plus moment,
    // so I figured it's fine to wait for angular to load first.
    waitForInjector().then(inj => {
        const dateHelper = inj.get('dateHelper');
        const el = $('[app-deadline]');
        const timestamp = Number.parseInt(el.attr('app-deadline'), 10);
        el.text(dateHelper.formattedUserFacingMonthDayLong(timestamp));
    });
});
