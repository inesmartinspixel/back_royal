import 'AngularSpecHelper';
import 'AddToCalendarWidget/angularModule';
import 'StudentNetwork/angularModule';

import * as userAgentHelper from 'userAgentHelper';
import setSpecLocales from 'Translation/setSpecLocales';
import addToCalendarWidgetLocales from 'AddToCalendarWidget/locales/add_to_calendar_widget/add_to_calendar_widget-en.json';
import 'StudentNetwork/angularModule/spec/_mock/fixtures/student_network_events';

setSpecLocales(addToCalendarWidgetLocales);

describe('FrontRoyal.AddToCalendarWidget.addToCalendarWidget', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let event;
    let StudentNetworkEvent;
    let $window;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.AddToCalendarWidget', 'FrontRoyal.StudentNetwork', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                StudentNetworkEvent = $injector.get('StudentNetworkEvent');
                $window = $injector.get('$window');

                $injector.get('StudentNetworkEventFixtures');
            },
        ]);

        event = StudentNetworkEvent.fixtures.getInstance();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.event = event;
        renderer.render(`
            <add-to-calendar-widget
                class="calendar-options-left"
                event-id="event.id"
                event-title="event.title"
                event-start-date="event.startTime"
                event-end-date="event.endTime"
                event-url="event.external_rsvp_url"
                event-description="event.descriptionWithUrl"
                event-address="event.place_details && event.place_details.formatted_address"
            ></add-to-calendar-widget>
        `);
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('in CORDOVA', () => {
        beforeEach(() => {
            const mockCordovaFunction = () => {};
            $window.plugins = {
                calendar: {
                    createEventInteractivelyWithOptions: mockCordovaFunction,
                },
            };
            jest.spyOn($window.plugins.calendar, 'createEventInteractivelyWithOptions');
        });

        function assertCordovaPluginCall(calOptions) {
            expect($window.plugins.calendar.createEventInteractivelyWithOptions).toHaveBeenCalledWith(
                event.title,
                event.place_details.formatted_address,
                `${event.description}\n\n${event.external_rsvp_url}`,
                event.startTime,
                event.endTime,
                calOptions,
                expect.any(Function),
                expect.any(Function),
            );
        }

        it('should not set the URL option for the plugin when Android', () => {
            jest.spyOn(userAgentHelper, 'isAndroidDevice').mockReturnValue(true);
            render();
            scope.addToCalendar('cordova');
            assertCordovaPluginCall({
                allday: false,
            });
        });

        it('should set the URL option for the plugin when not Android', () => {
            jest.spyOn(userAgentHelper, 'isAndroidDevice').mockReturnValue(false);
            render();
            scope.addToCalendar('cordova');
            assertCordovaPluginCall({
                allday: false,
                url: event.external_rsvp_url,
            });
        });
    });

    describe('template logic', () => {
        it('should show UI for Cordova', () => {
            window.CORDOVA = true;
            render();
            SpecHelper.expectElement(elem, '[name="cordova-calendar"]');
            delete window.CORDOVA;
        });

        it('should show UI for mobile browsers', () => {
            jest.spyOn(userAgentHelper, 'isMobileOrTabletDevice').mockReturnValue(true);
            jest.spyOn(userAgentHelper, 'isiOSDevice').mockReturnValue(false);
            render();
            SpecHelper.expectElement(elem, '[name="mobile-browser-calendar"]');
        });

        it('should not show UI for mobile browser less than iOS 13', () => {
            jest.spyOn(userAgentHelper, 'isMobileOrTabletDevice').mockReturnValue(true);
            jest.spyOn(userAgentHelper, 'isiOSDevice').mockReturnValue(true);
            jest.spyOn(userAgentHelper, 'getUserAgentInfo').mockReturnValue({
                os: {
                    version: '12.1.3.3.7',
                },
            });
            render();
            SpecHelper.expectNoElement(elem, '[name="mobile-browser-calendar"]');
        });

        it('should show select UI for desktop', () => {
            jest.spyOn(userAgentHelper, 'isMobileOrTabletDevice').mockReturnValue(false);
            render();
            scope.modelProxy.showCalendarOptions = true;
            scope.$digest();
            SpecHelper.expectElements(elem, '[name="desktop-calendar"] .calendar-option', 3);
        });
    });
});
