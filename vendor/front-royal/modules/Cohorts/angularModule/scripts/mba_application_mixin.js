import angularModule from 'Cohorts/angularModule/scripts/cohorts_module';

angularModule.factory('MbaApplicationMixin', [
    '$injector',

    $injector => {
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');

        return {
            // FIXME: This mixin is pretty pointless now but kept it to avoid refactoring
            onLink(scope) {
                NavigationHelperMixin.onLink(scope);

                scope.applyToMBA = () => {
                    scope.loadRoute('/settings/application');
                };
            },
        };
    },
]);
