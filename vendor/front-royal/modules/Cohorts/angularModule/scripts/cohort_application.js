import angularModule from 'Cohorts/angularModule/scripts/cohorts_module';
import moment from 'moment-timezone';

angularModule.factory('CohortApplication', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');
        const Cohort = $injector.get('Cohort');
        const PrivateUserDocumentsHelper = $injector.get('PrivateUserDocumentsHelper');

        return Iguana.subclass(function () {
            this.setCollection('cohort_applications');
            this.alias('CohortApplication');
            this.setIdProperty('id');
            this.embeddedIn('user');

            // generate options for selectize
            function statusToSelectize(status) {
                return {
                    text: status.toUpperCase(),
                    value: status,
                };
            }

            this.VALID_STATUSES = ['pending', 'rejected', 'accepted', 'expelled', 'deferred', 'pre_accepted'];
            this.VALID_ENROLLMENT_STATUSES = ['pre_accepted', 'accepted']; // see also cohort_application.rb
            this.VALID_DEFERRAL_STATUSES = this.VALID_ENROLLMENT_STATUSES.concat(['deferred']);
            this.VALID_STUDENT_RECORD_UPLOAD_STATUSES = this.VALID_DEFERRAL_STATUSES;
            this.VALID_INITIAL_DEFERRED_STATUSES = this.VALID_ENROLLMENT_STATUSES;
            this.VALID_GRADUATION_STATUSES = ['pending', 'graduated', 'failed', 'honors'];
            this.VALID_GRADUATED_STATUSES = ['graduated', 'failed', 'honors'];

            this.VALID_STATUS_OPTIONS = this.VALID_STATUSES.map(statusToSelectize);
            this.VALID_INITIAL_DEFERRED_STATUS_OPTIONS = this.VALID_INITIAL_DEFERRED_STATUSES.map(statusToSelectize);

            this.VALID_GRADUATION_STATUS_OPTIONS = this.VALID_GRADUATION_STATUSES.map(statusToSelectize);

            this.setCallback('after', 'copyAttrsOnInitialize', function () {
                this.registered_early = this.registered_early || false;
            });

            Object.defineProperty(this.prototype, 'createdAt', {
                get() {
                    return new Date(this.created_at * 1000);
                },
            });

            Object.defineProperty(this.prototype, 'updatedAt', {
                get() {
                    return new Date(this.updated_at * 1000);
                },
            });

            Object.defineProperty(this.prototype, 'appliedAt', {
                get() {
                    if (!this.$$appliedAt) {
                        this.$$appliedAt = new Date(this.applied_at * 1000);
                    }
                    return this.$$appliedAt;
                },
                set(val) {
                    this.applied_at = val.getTime() / 1000;
                    this.$$appliedAt = undefined;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'acceptedAt', {
                get() {
                    if (!this.accepted_at) {
                        return null;
                    }

                    return new Date(this.accepted_at * 1000);
                },
            });

            Object.defineProperty(this.prototype, 'graduatedAt', {
                get() {
                    return this.graduated_at && new Date(this.graduated_at * 1000);
                },
            });

            Object.defineProperty(this.prototype, 'cohortStartDate', {
                get() {
                    if (!this.$$cohortStartDate) {
                        this.$$cohortStartDate = new Date(this.cohort_start_date * 1000);
                    }
                    return this.$$cohortStartDate;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'paymentGracePeriodEndAt', {
                get() {
                    if (!this.payment_grace_period_end_at) {
                        return null;
                    }
                    if (!this.$$paymentGracePeriodEndAt) {
                        this.$$paymentGracePeriodEndAt = new Date(this.payment_grace_period_end_at * 1000);
                    }
                    return this.$$paymentGracePeriodEndAt;
                },
                set(val) {
                    this.payment_grace_period_end_at = val && val.getTime() / 1000;
                    this.$$paymentGracePeriodEndAt = undefined;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'earlyRegistrationDeadlineHasPassed', {
                get() {
                    return moment().valueOf() >= moment(this.cohort_early_registration_deadline * 1000).valueOf();
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'earlyRegistrationEnabled', {
                get() {
                    if (!this.registered_early && this.tuitionNumSuccessfulPayments > 0) {
                        return false;
                    }
                    return (
                        !this.earlyRegistrationDeadlineHasPassed ||
                        this.allow_early_registration_pricing ||
                        this.registered_early
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'supportsEarlyPricing', {
                get() {
                    const oncePlan = this.getPlanForFrequency('once');

                    // Really legacy pricing plans had no once plan, so just short-circuit with false
                    if (
                        !oncePlan ||
                        !this.scholarship_level ||
                        !this.scholarship_level.standard[oncePlan.id] ||
                        !this.scholarship_level.early[oncePlan.id]
                    ) {
                        return false;
                    }

                    const standard = this.scholarship_level.standard[oncePlan.id];
                    const early = this.scholarship_level.early[oncePlan.id];
                    return standard.amount_off !== early.amount_off || standard.percent_off !== early.percent_off;
                },
                configurable: true,
            });

            // maybe this gets changed to use some sort of persisted flag at some point
            Object.defineProperty(this.prototype, 'defaultPlan', {
                get() {
                    // On EMBA11 we pivoted to new pricing plans for which the "alternative" UI bases prices off
                    // the "once" plan. For old applications with prior pricing plans locked in that are able to re-register (the
                    // ability to re-register like that should be an edge case, maybe if a user gets a full refund or something), we are
                    // sending them to the prior registration page that is based off monthly prices.
                    return this.supportsEarlyPricing
                        ? this.getPlanForFrequency('once')
                        : this.getPlanForFrequency('monthly');
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'existingPlan', {
                get() {
                    return this.getPlanForId(this.stripe_plan_id);
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'existingOrDefaultPlan', {
                get() {
                    return this.existingPlan || this.defaultPlan;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasMadeStripePayments', {
                get() {
                    return this.tuitionNumSuccessfulPayments > 0;
                },
            });

            Object.defineProperty(this.prototype, 'promisedPaymentOutsideOfStripe', {
                get() {
                    // Sometimes, users make full one-time payments outside of Stripe via PayPal or wire transfer.
                    // In such cases, we proactively set `total_num_required_stripe_payments` to 0.
                    //
                    // NOTE: we cannot rely on `tuitionNumPaymentsRemaining` here because it will always be 0
                    // for an unregistered user who hasn't selected a plan.
                    //
                    // Also note that applications for cohorts that do not require payment have an undefined
                    // total_num_required_stripe_payments value, thus there is an implicit assumption that if
                    // total_num_required_stripe_payments is the integer 0 then it was at one time not 0, so thus
                    // the user has made a payment, unless they have a full scholarship. That concept, together
                    // with checking num_charged_payments, tells us if the user probably promised payment outside
                    // of Stripe.
                    //
                    // This could be expressed better, possibly by returning billing_transactions
                    // or something like that, but we decided it's not worth the effort right now.
                    //
                    // UPDATE: this method is not actually totally accurate right now.  There are 2 issues
                    // right now:
                    // 1. users who have a stripe payment AND a non-stripe payment will have
                    //      promisedPaymentOutsideOfStripe = false
                    // 2. users who made a non-stripe payment before mid-2018 will have num_charged_payments > 0,
                    //      and so will incorrectly have promisedPaymentOutsideOfStripe = false (We also saw one user
                    //      from 2019 who had nil in this case instead of 0: b8b362d4-e5f4-4219-8bfa-4375bbd3cb73)
                    return !!(
                        (Cohort.supportsRecurringPayments(this.program_type) ||
                            Cohort.supportsOnlyOneTimePayment(this.program_type)) &&
                        !this.hasFullScholarship &&
                        this.total_num_required_stripe_payments === 0 &&
                        this.num_charged_payments === 0
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasMadePayments', {
                get() {
                    // NOTE: we simplify the logic in this property by simply checking if the user promisedPaymentOutsideOfStripe
                    // rather than checking the billing_transactions to see if they actually did pay outside of Stripe. So, this
                    // logic is tehchnically incorrect, but we're okay with it for now.
                    // See also cohort_application.rb#promised_payment_outside_of_stripe?
                    return this.hasMadeStripePayments || this.promisedPaymentOutsideOfStripe;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasNoScholarship', {
                get() {
                    return !this.defaultPlanAmountOff && !this.defaultPlanPercentOff;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasPartialScholarship', {
                get() {
                    if (this.hasFullScholarship) {
                        return false;
                    }
                    return this.defaultPlanAmountOff > 0 || this.defaultPlanPercentOff > 0;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasCustomScholarship', {
                get() {
                    return this.scholarship_level?.name === 'Custom Scholarship';
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasFullScholarship', {
                get() {
                    return this.scholarship_level?.name === 'Full Scholarship';
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'showAlternateRegistration', {
                get() {
                    // this.supportsEarlyPricing - since it's technically possible for a user on the old pricing
                    // plan to re-register such as a case of a full refund then deferral, we'll
                    // re-route them to the redesigned registration UI
                    return !this.hasFullScholarship && !this.hasMadePayments && this.supportsEarlyPricing;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'statusString', {
                get() {
                    let visibleStatus = this.status;
                    if (this.status === 'accepted' && this.graduation_status !== 'pending') {
                        visibleStatus = this.graduation_status;
                    }
                    return `${this.cohort_name} / ${visibleStatus.toUpperCase()}`;
                },
            });

            Object.defineProperty(this.prototype, 'isGraduated', {
                get() {
                    return this.graduation_status === 'graduated' || this.graduation_status === 'honors';
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'tuitionNumSuccessfulPayments', {
                get() {
                    return (this.num_charged_payments || 0) - (this.num_refunded_payments || 0);
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'tuitionNumPaymentsRemaining', {
                get() {
                    return Math.max(0, this.total_num_required_stripe_payments - this.tuitionNumSuccessfulPayments);
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'tuitionAllPaid', {
                get() {
                    return this.hasMadePayments && this.tuitionNumPaymentsRemaining === 0;
                },
                configurable: true,
            });

            //----------------------------
            // Default Plan Properties
            //----------------------------

            // NOTE: Most all of these following properties effectively map to convenience methods that
            // can take any arbitrary plan, plus we have convenience methods for current and existing plans.
            // At some point, it'd be nice to remove all these references, but that's a fairly big factor once
            // specs are involved.

            Object.defineProperty(this.prototype, 'defaultPlanAmountTotal', {
                get() {
                    return this.defaultPlan && this.defaultPlan.amount;
                },
            });

            Object.defineProperty(this.prototype, 'defaultPlanAmountOff', {
                get() {
                    const planId = this.defaultPlan && this.defaultPlan.id;
                    if (!planId || !this.scholarship_level) {
                        return 0;
                    }
                    return this.scholarship_level.standard[planId].amount_off;
                },
            });

            Object.defineProperty(this.prototype, 'defaultPlanPercentOff', {
                get() {
                    const planId = this.defaultPlan && this.defaultPlan.id;

                    if (!planId || !this.scholarship_level) {
                        return 0;
                    }
                    return this.scholarship_level.standard[planId].percent_off;
                },
            });

            Object.defineProperty(this.prototype, 'defaultPlanOriginalTotal', {
                get() {
                    return (this.defaultPlanAmountTotal * this.defaultPlanNumPaymentIntervals) / 100 || 0;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'defaultPlanScholarshipTotal', {
                get() {
                    if (this.defaultPlanAmountOff) {
                        return (this.defaultPlanNumPaymentIntervals * this.defaultPlanAmountOff) / 100;
                    }
                    if (this.defaultPlanPercentOff) {
                        return (
                            (this.defaultPlanNumPaymentIntervals *
                                this.defaultPlanPercentOff *
                                this.defaultPlanOriginalTotal) /
                            100
                        );
                    }
                    return 0;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'defaultPlanNumPaymentIntervals', {
                get() {
                    return this.getNumIntervalsForPlan(this.defaultPlan);
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'isPaidCert', {
                get() {
                    return Cohort.isPaidCert(this.program_type);
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'indicatesUserShouldUploadIdentificationDocument', {
                get() {
                    return (
                        Cohort.supportsDocumentUpload(this.program_type) &&
                        _.contains(this.constructor.VALID_STUDENT_RECORD_UPLOAD_STATUSES, this.status) &&
                        this.graduation_status !== 'failed'
                    );
                },
            });

            Object.defineProperty(this.prototype, 'indicatesUserShouldProvideTranscripts', {
                get() {
                    return (
                        Cohort.supportsDocumentUpload(this.program_type) &&
                        _.contains(this.constructor.VALID_STUDENT_RECORD_UPLOAD_STATUSES, this.status) &&
                        this.graduation_status !== 'failed'
                    );
                },
            });

            Object.defineProperty(this.prototype, 'indicatesUserShouldUploadEnglishLanguageProficiencyDocuments', {
                get() {
                    return (
                        Cohort.requiresEnglishLanguageProficiency(this.program_type) &&
                        _.contains(
                            ['pending'].concat(this.constructor.VALID_STUDENT_RECORD_UPLOAD_STATUSES),
                            this.status,
                        ) &&
                        this.graduation_status !== 'failed'
                    );
                },
            });

            Object.defineProperty(this.prototype, 'hasActiveEnrollmentAgreementSigningLink', {
                get() {
                    return !!this.activeEnrollmentAgreementSigningLink;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'activeEnrollmentAgreementSigningLink', {
                get() {
                    return this.enrollmentAgreement && !this.enrollmentAgreement.signed_at
                        ? this.enrollmentAgreement.signing_link
                        : null;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasSignedEnrollmentAgreement', {
                get() {
                    return !!this.enrollmentAgreement && !!this.enrollmentAgreement.signed_at;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'enrollmentAgreement', {
                get() {
                    // We sort by signed_at on the off-chance that someone has two enrollment
                    // agreements for the same program_type, which is not expected.  In that
                    // case, prefer one that is signed.
                    return (
                        _.chain(this.user() && this.user().signable_documents)
                            .where({
                                document_type: 'enrollment_agreement',
                            })
                            .sortBy(doc => doc.signed_at || 0)
                            .reverse()
                            .detect(doc => doc.metadata.program_type === this.program_type)
                            .value() || null
                    );
                },
            });

            Object.defineProperty(this.prototype, 'decisionDate', {
                get() {
                    if (this.decision_date) {
                        return new Date(this.decision_date * 1000);
                    }
                    return undefined;
                },
                configurable: true,
            });

            return {
                //----------------------------
                // Plan Details Methods
                //----------------------------

                getPlanForId(planId) {
                    return _.find(this.stripe_plans, {
                        id: planId,
                    });
                },

                getPlanForFrequency(frequency) {
                    return _.find(this.stripe_plans, {
                        frequency,
                    });
                },

                getNumIntervalsForPlan(plan) {
                    switch (plan.frequency) {
                        case 'bi_annual':
                            return 2;
                        case 'monthly':
                            return 12;
                        case 'once':
                            return 1;
                        default:
                            return undefined;
                    }
                },

                getPlanTuitionActualTotal(plan, isStandardPeriod, coupon) {
                    // coupon is only a parameter for the sake of testing
                    if (Cohort.supportsScholarshipLevels(this.program_type)) {
                        coupon =
                            !this.earlyRegistrationEnabled || isStandardPeriod
                                ? this.scholarship_level.standard[plan.id]
                                : this.scholarship_level.early[plan.id];
                    }

                    const amountOff = coupon ? coupon.amount_off : 0;
                    const percentOff = coupon ? coupon.percent_off : 0;
                    const tuitionNumPaymentIntervals = this.getNumIntervalsForPlan(plan);
                    const tuitionOriginalTotal = (plan.amount * tuitionNumPaymentIntervals) / 100 || 0;
                    let tuitionScholarshipTotal;

                    if (amountOff) {
                        tuitionScholarshipTotal = (tuitionNumPaymentIntervals * amountOff) / 100;
                    } else if (percentOff) {
                        tuitionScholarshipTotal = (percentOff * tuitionOriginalTotal) / 100;
                    } else {
                        tuitionScholarshipTotal = 0;
                    }
                    return tuitionOriginalTotal - tuitionScholarshipTotal;
                },

                getPlanPaymentPerIntervalForDisplay(plan) {
                    const tuitionActualTotal = this.getPlanTuitionActualTotal(plan);
                    const tuitionNumPaymentIntervals = this.getNumIntervalsForPlan(plan);
                    const tuitionActualPaymentPerInterval = tuitionActualTotal / tuitionNumPaymentIntervals;
                    return Math.round(tuitionActualPaymentPerInterval);
                },

                getPlanOriginalPaymentPerIntervalForDisplay(plan, isStandardPeriod) {
                    const tuitionOriginalTotal = this.getPlanTuitionActualTotal(plan, isStandardPeriod);
                    const tuitionNumPaymentIntervals = this.getNumIntervalsForPlan(plan);
                    const tuitionOriginalPaymentPerInterval = tuitionOriginalTotal / tuitionNumPaymentIntervals;
                    return Math.round(tuitionOriginalPaymentPerInterval);
                },

                additionalSavingsPerIntervalPerPlan(plan) {
                    const planOriginalTotal = this.getPlanTuitionActualTotal(plan, true);
                    const tuitionActualTotal = this.getPlanTuitionActualTotal(plan);
                    const tuitionNumPaymentIntervals = this.getNumIntervalsForPlan(plan);
                    const tuitionDefaultPaymentPerInterval = planOriginalTotal / tuitionNumPaymentIntervals;
                    const tuitionActualPaymentPerInterval = tuitionActualTotal / tuitionNumPaymentIntervals;
                    return Math.round(tuitionDefaultPaymentPerInterval - tuitionActualPaymentPerInterval);
                },

                additionalSavingsForPlan(plan) {
                    const defaultPlanTotal = this.getPlanTuitionActualTotal(this.defaultPlan);
                    const planActualTotal = this.getPlanTuitionActualTotal(plan);
                    return defaultPlanTotal - planActualTotal;
                },

                canReapplyTo(promotedCohort) {
                    if (!promotedCohort) {
                        return false;
                    }
                    if (['rejected', 'expelled'].includes(this.status)) {
                        // For cohorts with a decision_date (MBA and EMBA), we don't allow users who
                        // applied to Cohort A and were rejected to reapply for the promoted cohort
                        // until two days after Cohort A's decision date.
                        if (Cohort.supportsAdmissionRounds(this.program_type) && this.decisionDate) {
                            return moment(this.decisionDate).add(2, 'days').valueOf() < Date.now();
                        }
                        // Some legacy MBA/EMBA cohorts support admission rounds, but don't have a
                        // decision date.
                        // See also: https://trello.com/c/n9lW30Y7
                        if (Cohort.supportsAdmissionRounds(this.program_type)) {
                            return true;
                        }
                        if (
                            Cohort.supportsReapply(this.program_type) ||
                            Cohort.supportsReapply(promotedCohort.program_type)
                        ) {
                            return true;
                        }
                    }
                    return false;
                },

                downloadEnrollmentAgreement() {
                    return PrivateUserDocumentsHelper.downloadDocument(this.enrollmentAgreement);
                },
            };
        });
    },
]);
