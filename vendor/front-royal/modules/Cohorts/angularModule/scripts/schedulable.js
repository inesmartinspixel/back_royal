import angularModule from 'Cohorts/angularModule/scripts/cohorts_module';
import { formattedBrandName } from 'AppBrandMixin';
import moment from 'moment-timezone';

import miyaMiyaLogoWhite from 'images/miyamiya/miyamiya-logo-white.png';
import paidCertIconDataAnalytics from 'images/paid_cert_icon_data_analytics.png';
import paidCertIconCompetitiveStrategy from 'images/paid_cert_icon_competitive_strategy.png';
// import paidCertIconStrategyAndManagement from 'images/paid_cert_icon_strategy_and_management.png';
import paidCertIconEssentialsOfManagement from 'images/paid_cert_icon_essentials_of_management.png';
import paidCertIconFinancialAndManagerialAccounting from 'images/paid_cert_icon_financial_and_managerial_accounting.png';
import paidCertIconMarketingAndPricingStrategy from 'images/paid_cert_icon_marketing_and_pricing_strategy.png';

angularModule.factory('Schedulable', [
    '$injector',

    $injector => {
        const AModuleAbove = $injector.get('AModuleAbove');
        const AdmissionRound = $injector.get('AdmissionRound');
        const IdVerificationPeriod = $injector.get('IdVerificationPeriod');
        const dateHelper = $injector.get('dateHelper');
        const ngToast = $injector.get('ngToast');
        const Period = $injector.get('Period');
        const SupportsRelativeDates = $injector.get('SupportsRelativeDates');
        const CERTIFICATE_PROGRAM_TYPES = [];

        const programTypes = [
            {
                key: 'mba',
                label: 'MBA', // internal
                institutionId: '85fec419-8dc5-45a5-afbd-0cc285a595b9',
                supportsAdmissionRounds: true,
                supportsSchedule: true,
                supportsGradebook: true,
                supportsEnrollmentDeadline: true,
                supportsDeferralLink: true,
                supportsDocumentUpload: true,
                supportsAcceptanceMessage: true,
                supportsUnlockingWithoutAcceptance: true,
                supportsSpecializations: false,
                canPreviewCareerProfileAfterApplying: false,
                excludesFoundationsPlaylistOnAcceptance: true,
                supportsMobilePhoneNumber: true,
                supportsPeerRecommendations: true,
                supportsRequiredEducation: true,
                supportsExercises: true,
                supportsDiplomaGeneration: true,
                supportsProgramGuide: false,
                supportsSlackRooms: true,
                supportsDelayedCareerNetworkAccessOnAcceptance: true,
                acceptedScreenTitleHtml(brandName) {
                    return `${brandName.toUpperCase()} MBA`;
                },
                studentDashboardProgramBoxTitleHtml(brandName) {
                    return `The ${brandName} <br> MBA`;
                },
                studentDashboardProgramBoxSubtitleKey: 'mba',
                studentDashboardWelcomeBoxKey: 'mba',
                studentDashboardProgramBoxShowProgressBar: true,
                learningBoxDescriptionKey: 'description_mba',
                learningBoxTitleKey: 'mba_curriculum',
                showButtonKey: 'show_mba',
                playlistMapLockedKey: 'locked_mba_message',
                applicationStatusPendingSecondaryMessageKey: 'pending_secondary_message_mba',
                applicationStatusRejectedPrimaryMessageKey: 'rejected_primary_message_reapply_to_next',
                applicationStatusRejectedAfterPreAcceptedPrimaryMessageKey:
                    'rejected_primary_message_reapply_to_next_rejected_after_pre_accepted',
                applicationStatusRejectedPrimaryMessageKeyCantReapply:
                    'rejected_primary_message_reapply_to_next_cant_reapply',
                applicationStatusRejectedAfterPreAcceptedPrimaryMessageKeyCantReapply:
                    'rejected_primary_message_reapply_to_next_rejected_after_pre_accepted_cant_reapply',
                applicationStatusPreAcceptedPrimaryMessageKey: 'pre_accepted_primary_message_mba',
                applicationStatusAcceptedPrimaryMessageKey: 'accepted_primary_message_mba',
                acceptedApplicationShareMessageKey: 'share_message_mba',
                workFormFullTimeWorkHistoryKey: 'provide_full_time_work_after_university',
                workFormPartTimeWorkHistoryKey: 'list_part_time_work_students_and_graduates',
                supportsNetworkAccess: true,
                canFilterNetworkForMyClass: true,
                isDegreeProgram: true,
                requiresEnglishLanguageProficiency: true,
                requiresMailingAddress: true,
                programTitle(brandName) {
                    return `${brandName} MBA`;
                },
                transcriptProgramTitle: 'Master of Business Administration',
                supportsTranscriptDownload: true,
                programChoiceTitle: 'MBA',
                supportsEnrollmentSidebar: true,
                supportsEnrollmentAgreement: true,
                supportsResearchResourcesSidebar: true,
                programChoiceFullTitle(brandName) {
                    return `the ${brandName} early-career MBA`;
                },
                supportsPresentationProjecs: true,
                supportsStudentNetworkTabAccess: true,
                supportsCareersTabAccess: true,
                supportsEditingCareerProfile: true,
                supportsApplyingToSmartly: true,
            },
            {
                key: 'emba',
                label: 'EMBA', // internal
                institutionId: '85fec419-8dc5-45a5-afbd-0cc285a595b9',
                supportsAdmissionRounds: true,
                supportsSchedule: true,
                supportsGradebook: true,
                supportsEnrollmentDeadline: true,
                supportsDeferralLink: true,
                supportsRecurringPayments: true, // this also indicates that the cohort supports registration. See comment below in the cert setup about why paidCerts have this as false
                supportsScholarshipLevels: true,
                supportsOnlyOneTimePayment: false,
                supportsDocumentUpload: true,
                supportsAcceptanceMessage: true,
                supportsAcceptanceMessageOnApplicationPage: true,
                supportsUnlockingWithoutAcceptance: true,
                supportsSpecializations: true,
                canPreviewCareerProfileAfterApplying: false,
                excludesFoundationsPlaylistOnAcceptance: true,
                supportsMobilePhoneNumber: true,
                supportsPeerRecommendations: true,
                supportsExercises: true,
                supportsDiplomaGeneration: true,
                supportsProgramGuide: true,
                supportsSlackRooms: true,
                supportsDelayedCareerNetworkAccessOnAcceptance: true,
                supportsEarlyDecisionConsideration: true,
                acceptedScreenTitleHtml(brandName) {
                    return `${brandName.toUpperCase()} EMBA`;
                },
                studentDashboardProgramBoxTitleHtml(brandName) {
                    return `The ${brandName} <br> EMBA`;
                },
                studentDashboardProgramBoxSubtitleKey: 'emba',
                studentDashboardWelcomeBoxKey: 'emba',
                studentDashboardProgramBoxShowProgressBar: true,
                learningBoxDescriptionKey: 'description_emba',
                learningBoxTitleKey: 'emba_curriculum',
                showButtonKey: 'show_emba',
                playlistMapLockedKey: 'locked_emba_message',
                applicationStatusPendingSecondaryMessageKey: 'pending_secondary_message_mba',
                applicationStatusRejectedPrimaryMessageKey: 'rejected_primary_message_reapply_to_next',
                applicationStatusRejectedAfterPreAcceptedPrimaryMessageKey:
                    'rejected_primary_message_reapply_to_next_rejected_after_pre_accepted',
                applicationStatusRejectedPrimaryMessageKeyCantReapply:
                    'rejected_primary_message_reapply_to_next_cant_reapply',
                applicationStatusRejectedAfterPreAcceptedPrimaryMessageKeyCantReapply:
                    'rejected_primary_message_reapply_to_next_rejected_after_pre_accepted_cant_reapply',
                applicationStatusPreAcceptedPrimaryMessageKey: 'pre_accepted_primary_message_emba',
                applicationStatusAcceptedPrimaryMessageKey: 'accepted_primary_message_emba',
                acceptedApplicationShareMessageKey: 'share_message_emba',
                workFormFullTimeWorkHistoryKey: 'provide_full_time_work',
                workFormPartTimeWorkHistoryKey: 'list_part_time_work',
                supportsNetworkAccess: true,
                canFilterNetworkForMyClass: true,
                isDegreeProgram: true,
                requiresEnglishLanguageProficiency: true,
                requiresMailingAddress: true,
                supportsSkippingProgramChoice: true,
                programTitle(brandName) {
                    return `${brandName} EMBA`;
                },
                transcriptProgramTitle: 'Executive Master of Business Administration',
                supportsTranscriptDownload: true,
                programChoiceTitle: 'EMBA',
                supportsEnrollmentSidebar: true,
                supportsEnrollmentAgreement: true,
                supportsResearchResourcesSidebar: true,
                programChoiceFullTitle: 'the Executive MBA',
                supportsPresentationProjecs: true,
                supportsStudentNetworkTabAccess: true,
                supportsCareersTabAccess: true,
                supportsEditingCareerProfile: true,
                supportsApplyingToSmartly: true,
                supportsIsolatedNetwork: true,
            },
            {
                key: 'career_network_only',
                label: 'Careers Network Only', // internal
                institutionId: 'ebc4730d-19d8-4cb3-b2ca-e20308bec8e3',
                supportsReapply: true,
                supportsUnlockingWithoutAcceptance: true,
                supportsEarlyCareerApplicants: true,
                supportsRequiredResume: true,
                supportsRequiredLinkedInProfile: true,
                supportsSkippingProgramChoice: true,
                supportsLongTermGoalField: true,
                supportsSubmitToStatusPage: true,
                canPreviewCareerProfileAfterApplying: true,
                supportsRequiredEducation: true,
                supportsRequiredWork: true,
                studentDashboardProgramBoxTitleHtml: 'Smartly <br> Talent',
                studentDashboardProgramBoxSubtitleKey: 'career_network_only',
                studentDashboardWelcomeBoxKey: 'career_network_only',
                studentDashboardProgramBoxShowProgressBar: false,
                learningBoxDescriptionKey: 'description_open_courses',
                learningBoxTitleKey: 'open_courses',
                showButtonKey: 'show_career_network_only',
                playlistMapLockedKey: 'locked_mba_message', // career_network_only uses the mba message
                applicationStatusPendingSecondaryMessageKey: 'pending_secondary_message_other',
                applicationStatusRejectedPrimaryMessageKey: 'rejected_primary_message_career_network_only',
                applicationStatusAcceptedPrimaryMessageKey: 'accepted_primary_message_career_network_only',
                workFormFullTimeWorkHistoryKey: 'provide_full_time_work_after_university',
                workFormPartTimeWorkHistoryKey: 'list_part_time_work_students_and_graduates',
                acceptedApplicationShareMessageKey: null, // not needed since supportsAcceptanceMessage is false
                supportsTranscriptDownload: false,
                programTitle() {
                    return this.title;
                },
                programChoiceTitle: 'Career Network Only',
                programChoiceFullTitle: 'Career Network Only',
                supportsPersonalSummaryAnythingElseField: true,
                supportsStudentNetworkTabAccess: true,
                supportsCareersTabAccess: true,
                supportsEditingCareerProfile: true,
                supportsApplyingToSmartly: true,
            },
            // NOTE: The jordanian_math program type is new and we haven't quite flushed out all
            // of its configuration details yet. So, for now, we're just going to give it a minimal
            // config and we'll then come back later and properly configure it.
            getCertificateConfigForProgramType('jordanian_math', {
                institutionId: 'a297c552-8cef-4c78-83b3-465c8711377c',
                internalLabel: 'Jordanian Math',
                programChoiceTitle: 'Jordanian Math',
                programChoiceFullTitle: 'Jordanian Math',
                supportsCompactPlaylistMap: true,
                studentDashboardProgramBoxSubtitleKey: 'jordanian_math',
                learningBoxDescriptionKey: 'description_jordanian_math',
                supportsStudentNetworkTabAccess: false,
                supportsCareersTabAccess: false,
                supportsEditingCareerProfile: false,
                supportsApplyingToSmartly: false,
                supportsAcceptanceMessage: false,
                hideMobileProgramBox: true,
                customLogo: miyaMiyaLogoWhite,
            }),
            getCertificateConfigForProgramType('the_business_certificate', {
                internalLabel: 'The Business Certificate (free)',
                studentDashboardWelcomeBoxKey: 'the_business_certificate',
                supportsRequiredEducation: false,
                programChoiceTitle: 'Fundamentals of Business',
                programChoiceFullTitle: 'the Fundamentals of Business Certificate',
                requiresEnglishLanguageProficiency: true,
                supportsAcceptanceMessage: true,
            }),
            getCertificateConfigForProgramType('paid_cert_data_analytics', {
                internalLabel: 'Data Analytics Certificate',
                isPaidCert: true,
                icon: paidCertIconDataAnalytics,
                marketingPageUrl: '/the-executive-certificates/data-analytics',
                supportsCohortLevelProjects: true,
                requiresGraduationForCertificateDownload: true,
                supportsRequiredEducation: false,
            }),
            getCertificateConfigForProgramType('paid_cert_competitive_strategy', {
                internalLabel: 'Competitive Strategy Certificate',
                isPaidCert: true,
                icon: paidCertIconCompetitiveStrategy,
                marketingPageUrl: '/the-executive-certificates/business-strategy',
                supportsCohortLevelProjects: true,
                requiresGraduationForCertificateDownload: true,
                supportsRequiredEducation: false,
            }),
            getCertificateConfigForProgramType('paid_cert_entrepreneurial_strategy', {
                internalLabel: 'Entrepreneurial Strategy & Management Certificate',
                isPaidCert: true,
                icon: '', // FIXME: paidCertIconStrategyAndManagement,
                marketingPageUrl: '#',
                supportsCohortLevelProjects: true,
                requiresGraduationForCertificateDownload: true,
                supportsRequiredEducation: false,
            }),
            getCertificateConfigForProgramType('paid_cert_essentials_of_management', {
                internalLabel: 'Essentials of Management Certificate',
                isPaidCert: true,
                icon: paidCertIconEssentialsOfManagement,
                marketingPageUrl: '/the-executive-certificates/essentials-of-management',
                supportsCohortLevelProjects: true,
                requiresGraduationForCertificateDownload: true,
                supportsRequiredEducation: false,
            }),
            getCertificateConfigForProgramType('paid_cert_financial_accounting', {
                internalLabel: 'Financial and Managerial Accounting Certificate',
                isPaidCert: true,
                icon: paidCertIconFinancialAndManagerialAccounting,
                marketingPageUrl: '/the-executive-certificates/financial-managerial-accounting',
                supportsCohortLevelProjects: true,
                requiresGraduationForCertificateDownload: true,
                supportsRequiredEducation: false,
            }),
            getCertificateConfigForProgramType('paid_cert_marketing_basics', {
                internalLabel: 'Marketing & Pricing Strategy Certificate',
                isPaidCert: true,
                icon: paidCertIconMarketingAndPricingStrategy,
                marketingPageUrl: '/the-executive-certificates/marketing-pricing-strategy',
                supportsCohortLevelProjects: true,
                requiresGraduationForCertificateDownload: true,
                supportsRequiredEducation: false,
            }),
            getCertificateConfigForProgramType('paid_cert_operations_management', {
                internalLabel: 'Operations Management Certificate',
                isPaidCert: true,
                icon: null, // FIXME. if we want to promote one of these, we need an icon
                marketingPageUrl: '#',
                supportsCohortLevelProjects: true,
                requiresGraduationForCertificateDownload: true,
                supportsRequiredEducation: false,
            }),
        ];

        function getCertificateConfigForProgramType(programType, opts) {
            CERTIFICATE_PROGRAM_TYPES.push(programType);
            opts.institutionId = opts.institutionId || 'ebc4730d-19d8-4cb3-b2ca-e20308bec8e3';
            const isPaidCert = !!opts.isPaidCert;

            return {
                key: programType,
                label: opts.internalLabel,
                institutionId: opts.institutionId,
                isPaidCert,
                icon: opts.icon,
                marketingPageUrl: opts.marketingPageUrl,
                supportsAcceptanceMessage: !isPaidCert && !!opts.supportsAcceptanceMessage, //  since the paid certs support automatice accceptance when you pay, it would be silly to show a message
                supportsAcceptanceMessageOnApplicationPage:
                    !isPaidCert && !!opts.supportsAcceptanceMessageOnApplicationPage,
                supportsCertificateDownload: true,
                supportsRecurringPayments: false, // even though paid certs require payment, they do not require registration and repeated billing, so this is false
                supportsOnlyOneTimePayment: isPaidCert,
                supportsScholarshipLevels: false,
                supportsReapply: true,
                supportsAutograding: true,
                supportsUnlockingWithoutAcceptance: false,
                supportsEarlyCareerApplicants: true,
                supportsSkippingProgramChoice: true,
                supportsRequiredResume: true,
                supportsRequiredLinkedInProfile: true,
                supportsCohortLevelProjects: !!opts.supportsCohortLevelProjects,
                supportsRequiredEducation: !!opts.supportsRequiredEducation,
                requiresGraduationForCertificateDownload: !!opts.requiresGraduationForCertificateDownload,
                canPreviewCareerProfileAfterApplying: true,
                excludesFoundationsPlaylistOnAcceptance: !!opts.excludesFoundationsPlaylistOnAcceptance,
                learningBoxDescriptionKey: opts.learningBoxDescriptionKey || 'description_certificates',
                learningBoxTitleKey: 'certificates_curriculum',
                supportsNetworkAccess: false,
                requiresEnglishLanguageProficiency: !!opts.requiresEnglishLanguageProficiency,
                acceptedScreenTitleHtml() {
                    const upcased = this.titleWithoutThe.toUpperCase().trim();

                    if (upcased.endsWith('CERTIFICATE')) {
                        return upcased.replace('CERTIFICATE', '<br class="hidden-xs">CERTIFICATE');
                    }
                    return upcased;
                },
                studentDashboardProgramBoxTitleHtml() {
                    return this.titleWithoutThe.trim();
                },
                studentDashboardProgramBoxSubtitleKey:
                    opts.studentDashboardProgramBoxSubtitleKey || (isPaidCert ? 'paid_certificates' : 'certificates'),
                studentDashboardWelcomeBoxKey: 'certificates',
                studentDashboardProgramBoxShowProgressBar: false,
                showButtonKey: 'show_certificates',
                playlistMapLockedKey: 'locked_certificates_message',
                applicationStatusPendingSecondaryMessageKey: 'pending_secondary_message_other',
                applicationStatusRejectedPrimaryMessageKey: 'rejected_primary_message_certificates',

                // FIXME: Remove and change the base messageKey after enableQuantic flag is flipped
                // See https://trello.com/c/QFVa6l5W
                applicationStatusRejectedPrimaryMessageKeyEnableQuantic:
                    'rejected_primary_message_certificates_enable_quantic',

                // since users are pre-accepted, it doesn't really make sense to show an accepted message here
                applicationStatusAcceptedPrimaryMessageKey: isPaidCert ? null : 'accepted_primary_message_certificates',
                acceptedApplicationShareMessageKey: 'share_message_certificates',
                workFormFullTimeWorkHistoryKey: 'provide_full_time_work_after_university',
                workFormPartTimeWorkHistoryKey: 'list_part_time_work_students_and_graduates',
                programTitle() {
                    return this.title;
                },
                transcriptProgramTitle: opts.internalLabel,
                supportsTranscriptDownload: true,
                programChoiceTitle: opts.programChoiceTitle || opts.internalLabel,
                programChoiceFullTitle: opts.programChoiceFullTitle || opts.internalLabel,
                supportsCompactPlaylistMap: opts.supportsCompactPlaylistMap || false,
                supportsStudentNetworkTabAccess: angular.isDefined(opts.supportsStudentNetworkTabAccess)
                    ? opts.supportsStudentNetworkTabAccess
                    : true,
                supportsCareersTabAccess: angular.isDefined(opts.supportsCareersTabAccess)
                    ? opts.supportsCareersTabAccess
                    : true,
                supportsEditingCareerProfile: angular.isDefined(opts.supportsEditingCareerProfile)
                    ? opts.supportsEditingCareerProfile
                    : true,
                supportsApplyingToSmartly: angular.isDefined(opts.supportsApplyingToSmartly)
                    ? opts.supportsApplyingToSmartly
                    : true,
                hideMobileProgramBox: angular.isDefined(opts.hideMobileProgramBox) ? opts.hideMobileProgramBox : false,
                customLogo: angular.isDefined(opts.customLogo) ? opts.customLogo : null,
            };
        }

        // for performance, make it accessible via the keys as well
        _.chain(programTypes)
            .pluck('key')
            .each((key, i) => {
                programTypes[key] = programTypes[i];
            });

        // For program types that support admission rounds, the promoted cohort
        // is determined dynamically based on when rounds are ending.  For other
        // program types, we select one to be promoted.
        const promotableProgramTypes = _.reject(programTypes, programType => programType.supportsAdmissionRounds);

        // Program types that should show up on the calendar, i.e.: they have schedules
        const calendarProgramTypes = _.filter(programTypes, programType => programType.supportsSchedule);

        function delegateToProgramType(target, prop, type, withBrandNameSupport = false) {
            const getter = (programType, schedulable) => {
                const val = programTypes[programType] && programTypes[programType][prop];
                if (type === 'bool') {
                    return !!val;
                }
                if (typeof val === 'function') {
                    const brandName = withBrandNameSupport ? formattedBrandName('short') : undefined;
                    return val.call(schedulable, brandName);
                }
                return val;
            };

            target[prop] = programType => getter(programType);

            Object.defineProperty(target.prototype, prop, {
                get() {
                    return getter(this.program_type, this);
                },
                configurable: true,
            });
        }

        return new AModuleAbove({
            included(target) {
                target.include(SupportsRelativeDates);
                target.CERTIFICATE_PROGRAM_TYPES = CERTIFICATE_PROGRAM_TYPES;
                target.embedsMany('groups', 'Group');
                target.embedsMany('periods', 'Period');
                target.embedsMany('admission_rounds', 'AdmissionRound');
                target.embedsMany('id_verification_periods', 'IdVerificationPeriod');

                target.programTypes = programTypes;
                target.promotableProgramTypes = promotableProgramTypes;
                target.calendarProgramTypes = calendarProgramTypes;

                target.setCallback('before', 'save', function () {
                    // re-order admission_rounds and id_verification_periods by their deadlines so they will have the proper index
                    if (this.admission_rounds && this.admission_rounds.length > 1) {
                        this.admission_rounds = _.sortBy(this.admission_rounds, 'applicationDeadline');
                    }
                    if (this.id_verification_periods && this.id_verification_periods.length > 1) {
                        this.id_verification_periods = _.sortBy(this.id_verification_periods, 'dueDate');
                    }
                });

                target.setCallback('after', 'copyAttrsOnInitialize', function () {
                    this.admission_rounds = this.admission_rounds || [];
                    this._setDefaultRegistrationDeadline();
                    this._setDefaultEarlyRegistrationDeadline();
                    this._setDefaultEnrollmentDeadline();

                    // Paid certificates support project documents at the Cohort level
                    // MBA/EMBA support project documents at the Period level
                    this.learner_project_ids = this.learner_project_ids || [];

                    this.id_verification_periods = this.id_verification_periods || [];
                    this.playlist_collections = this.playlist_collections || [];
                });

                target.defineSetter('program_type', function (val) {
                    this.writeKey('program_type', val);

                    if (!this.supportsSchedule) {
                        this.periods = [];
                    }

                    if (!this.supportsSpecializations) {
                        this.num_required_specializations = 0;
                        this.specialization_playlist_pack_ids = [];
                    }

                    if (!this.supportsAdmissionRounds) {
                        this.admission_rounds = [];
                    } else if (this.admission_rounds.length === 0) {
                        this.addAdmissionRound();
                    }

                    if (!this.supportsIdVerificationPeriods) {
                        this.id_verification_periods = [];
                    }

                    this._setDefaultRegistrationDeadline();
                    this._setDefaultEarlyRegistrationDeadline();
                    this._setDefaultEnrollmentDeadline();
                });

                delegateToProgramType(target, 'supportsAdmissionRounds', 'bool');
                delegateToProgramType(target, 'supportsSchedule', 'bool');
                delegateToProgramType(target, 'supportsGradebook', 'bool');
                delegateToProgramType(target, 'supportsRecurringPayments', 'bool');
                delegateToProgramType(target, 'supportsEnrollmentDeadline', 'bool');
                delegateToProgramType(target, 'supportsDeferralLink', 'bool');
                delegateToProgramType(target, 'supportsDocumentUpload', 'bool');
                delegateToProgramType(target, 'supportsAcceptanceMessage', 'bool');
                delegateToProgramType(target, 'supportsAcceptanceMessageOnApplicationPage', 'bool');
                delegateToProgramType(target, 'supportsCertificateDownload', 'bool');
                delegateToProgramType(target, 'supportsReapply', 'bool');
                delegateToProgramType(target, 'supportsAutograding', 'bool');
                delegateToProgramType(target, 'supportsUnlockingWithoutAcceptance', 'bool');
                delegateToProgramType(target, 'supportsEarlyCareerApplicants', 'bool');
                delegateToProgramType(target, 'supportsSkippingProgramChoice', 'bool');
                delegateToProgramType(target, 'supportsSpecializations', 'bool');
                delegateToProgramType(target, 'supportsRequiredResume', 'bool');
                delegateToProgramType(target, 'supportsRequiredLinkedInProfile', 'bool');
                delegateToProgramType(target, 'supportsLongTermGoalField', 'bool');
                delegateToProgramType(target, 'supportsSubmitToStatusPage', 'bool');
                delegateToProgramType(target, 'canPreviewCareerProfileAfterApplying', 'bool');
                delegateToProgramType(target, 'excludesFoundationsPlaylistOnAcceptance', 'bool');
                delegateToProgramType(target, 'supportsMobilePhoneNumber', 'bool');
                delegateToProgramType(target, 'supportsPeerRecommendations', 'bool');
                delegateToProgramType(target, 'supportsRequiredEducation', 'bool');
                delegateToProgramType(target, 'supportsExercises', 'bool');
                delegateToProgramType(target, 'supportsNetworkAccess', 'bool');
                delegateToProgramType(target, 'supportsDiplomaGeneration', 'bool');
                delegateToProgramType(target, 'isPaidCert', 'bool');
                delegateToProgramType(target, 'studentDashboardProgramBoxShowProgressBar', 'bool');
                delegateToProgramType(target, 'supportsOnlyOneTimePayment', 'bool');
                delegateToProgramType(target, 'supportsScholarshipLevels', 'bool');
                delegateToProgramType(target, 'supportsProgramGuide', 'bool');
                delegateToProgramType(target, 'supportsCohortLevelProjects', 'bool');
                delegateToProgramType(target, 'supportsSlackRooms', 'bool');
                delegateToProgramType(target, 'supportsDelayedCareerNetworkAccessOnAcceptance', 'bool');
                delegateToProgramType(target, 'requiresGraduationForCertificateDownload', 'bool');
                delegateToProgramType(target, 'canFilterNetworkForMyClass', 'bool');
                delegateToProgramType(target, 'isDegreeProgram', 'bool');
                delegateToProgramType(target, 'requiresEnglishLanguageProficiency', 'bool');
                delegateToProgramType(target, 'requiresMailingAddress', 'bool');
                delegateToProgramType(target, 'learningBoxDescriptionKey');
                delegateToProgramType(target, 'learningBoxTitleKey');
                delegateToProgramType(target, 'showButtonKey');
                delegateToProgramType(target, 'studentDashboardProgramBoxSubtitleKey');
                delegateToProgramType(target, 'playlistMapLockedKey');
                delegateToProgramType(target, 'applicationStatusRejectedPrimaryMessageKey');
                delegateToProgramType(target, 'applicationStatusRejectedAfterPreAcceptedPrimaryMessageKey');
                delegateToProgramType(target, 'applicationStatusRejectedPrimaryMessageKeyCantReapply');
                delegateToProgramType(target, 'applicationStatusRejectedAfterPreAcceptedPrimaryMessageKeyCantReapply');
                delegateToProgramType(target, 'applicationStatusPendingSecondaryMessageKey');
                delegateToProgramType(target, 'applicationStatusPreAcceptedPrimaryMessageKey');
                delegateToProgramType(target, 'acceptedApplicationShareMessageKey');
                delegateToProgramType(target, 'applicationStatusAcceptedPrimaryMessageKey');
                delegateToProgramType(target, 'workFormFullTimeWorkHistoryKey');
                delegateToProgramType(target, 'workFormPartTimeWorkHistoryKey');
                delegateToProgramType(target, 'icon');
                delegateToProgramType(target, 'marketingPageUrl');
                delegateToProgramType(target, 'studentDashboardWelcomeBoxKey');
                delegateToProgramType(target, 'studentDashboardProgramBoxTitleHtml', undefined, true);
                delegateToProgramType(target, 'acceptedScreenTitleHtml', undefined, true);
                delegateToProgramType(target, 'programTitle', undefined, true);
                delegateToProgramType(target, 'transcriptProgramTitle');
                delegateToProgramType(target, 'supportsTranscriptDownload');
                delegateToProgramType(target, 'programChoiceTitle');
                delegateToProgramType(target, 'supportsEnrollmentSidebar', 'bool');
                delegateToProgramType(target, 'supportsEnrollmentAgreement', 'bool');
                delegateToProgramType(target, 'supportsResearchResourcesSidebar', 'bool');
                delegateToProgramType(target, 'programChoiceFullTitle', undefined, true);
                delegateToProgramType(target, 'supportsPersonalSummaryAnythingElseField', 'bool');
                delegateToProgramType(target, 'supportsPresentationProjecs', 'bool');
                delegateToProgramType(target, 'supportsCompactPlaylistMap', 'bool');
                delegateToProgramType(target, 'supportsStudentNetworkTabAccess', 'bool');
                delegateToProgramType(target, 'supportsCareersTabAccess', 'bool');
                delegateToProgramType(target, 'supportsEditingCareerProfile', 'bool');
                delegateToProgramType(target, 'supportsApplyingToSmartly', 'bool');
                delegateToProgramType(target, 'supportsIsolatedNetwork', 'bool');
                delegateToProgramType(target, 'hideMobileProgramBox', 'bool');
                delegateToProgramType(target, 'customLogo');

                // This is not actually saved to the db, but is calculated on-the-fly.
                // Could make smarter use of caching if this property become used a lot.
                Object.defineProperty(target.prototype, 'endDate', {
                    get() {
                        const computed = this.computeEndDate(this.startDate.getTime(), this.periods);
                        if (this.$$endDate && !moment(this.$$endDate).isSame(computed)) {
                            this.$$endDate = null;
                        }

                        if (!this.$$endDate) {
                            this.$$endDate = computed.toDate();
                        }

                        return this.$$endDate;
                    },
                    configurable: true,
                });

                Object.defineProperty(target.prototype, 'registrationDeadline', {
                    get() {
                        return this._getCachedRelativeDate('registrationDeadline', 'registration_deadline_days_offset');
                    },
                    set(date) {
                        this.registration_deadline_days_offset = this._getRelativeDayOffset(date);
                        return date;
                    },
                });

                Object.defineProperty(target.prototype, 'earlyRegistrationDeadline', {
                    get() {
                        return this._getCachedRelativeDate(
                            'earlyRegistrationDeadline',
                            'early_registration_deadline_days_offset',
                        );
                    },
                    set(date) {
                        this.early_registration_deadline_days_offset = this._getRelativeDayOffset(date);
                        return date;
                    },
                });

                Object.defineProperty(target.prototype, 'enrollmentDeadline', {
                    get() {
                        return this._getCachedRelativeDate('enrollmentDeadline', 'enrollment_deadline_days_offset');
                    },
                    set(date) {
                        this.enrollment_deadline_days_offset = this._getRelativeDayOffset(date);
                        return date;
                    },
                });

                Object.defineProperty(target.prototype, 'diplomaGeneration', {
                    get() {
                        return this._getCachedRelativeDate(
                            'diplomaGeneration',
                            'diploma_generation_days_offset_from_end',
                            this.endDate,
                        );
                    },
                    set(date) {
                        this.diploma_generation_days_offset_from_end = this._getRelativeDayOffset(date, this.endDate);
                        return date;
                    },
                    configurable: true,
                });

                Object.defineProperty(target.prototype, 'graduationDate', {
                    get() {
                        return this._getCachedRelativeDate(
                            'graduationDate',
                            'graduation_days_offset_from_end',
                            this.endDate,
                        );
                    },
                    set(date) {
                        this.graduation_days_offset_from_end = this._getRelativeDayOffset(date, this.endDate);
                        return date;
                    },
                    configurable: true, // for specs
                });

                Object.defineProperty(target.prototype, 'supportsRegistrationDeadline', {
                    get() {
                        return this.supportsPayments && this.supportsSchedule;
                    },
                    configurable: true,
                });

                Object.defineProperty(target.prototype, 'supportsEarlyRegistrationDeadline', {
                    get() {
                        return this.supportsRegistrationDeadline;
                    },
                    configurable: true,
                });

                Object.defineProperty(target.prototype, 'supportsIdVerificationPeriods', {
                    get() {
                        return this.supportsSchedule;
                    },
                    configurable: true,
                });

                // overridden in cohort.js
                Object.defineProperty(target.prototype, 'supportsExternalScheduleUrl', {
                    value: false,
                    configurable: true,
                });

                // FIXME: in the future we don't plan to name cohorts with 'THE' in front,
                // but this will make it look good for now
                Object.defineProperty(target.prototype, 'titleWithoutThe', {
                    get() {
                        return this.title.replace(/^the\s+/i, '');
                    },
                });

                Object.defineProperty(target.prototype, 'supportsPayments', {
                    get() {
                        return this.supportsRecurringPayments || this.supportsOnlyOneTimePayment;
                    },
                    configurable: true,
                });

                Object.defineProperty(target.prototype, '_startDateForRelativeDates', {
                    get() {
                        return this.startDate;
                    },
                    configurable: true,
                });

                // This is temporary.  See https://trello.com/c/Vl9Nix5w
                Object.defineProperty(target.prototype, 'supportsEarlyDecisionConsideration', {
                    get() {
                        const programTypeValue = programTypes[this.program_type].supportsEarlyDecisionConsideration;
                        if (!programTypeValue) {
                            return false;
                        }
                        return !!(this.startDate && this.startDate < new Date('2019/01/01'));
                    },
                    configurable: true,
                });

                Object.defineProperty(target.prototype, 'hasPlaylistCollections', {
                    get() {
                        return this.playlist_collections.length > 0;
                    },
                });

                Object.defineProperty(target.prototype, 'requiredPlaylistPackIds', {
                    get() {
                        return _.reduce(
                            this.playlist_collections,
                            (memo, playlistCollection) => memo.concat(playlistCollection.required_playlist_pack_ids),
                            [],
                        );
                    },
                    configurable: true,
                });
            },

            ensureLessThanOrEqualTo(x, y, message) {
                if (x <= y) {
                    return true;
                }
                ngToast.create({
                    content: message,
                    className: 'warning',
                });
                return false;
            },

            ensureGreaterThanOrEqualTo(x, y, message) {
                if (x >= y) {
                    return true;
                }
                ngToast.create({
                    content: message,
                    className: 'warning',
                });
                return false;
            },

            addAdmissionRound() {
                const attrs = {};
                if (this.admission_rounds.length === 0) {
                    attrs.application_deadline_days_offset = -14;
                } else {
                    attrs.application_deadline_days_offset =
                        _.first(this.admission_rounds).application_deadline_days_offset - 21;
                }

                attrs.decision_date_days_offset = attrs.application_deadline_days_offset + 9;

                const admissionRound = AdmissionRound.new(attrs);
                admissionRound.$$embeddedIn = this;
                this.admission_rounds.unshift(admissionRound);
                return admissionRound;
            },

            addIdVerificationPeriod() {
                const attrs = {};

                if (this.id_verification_periods.length === 0) {
                    attrs.start_date_days_offset = 0;
                } else {
                    attrs.start_date_days_offset = _.last(this.id_verification_periods).due_date_days_offset + 21;
                }

                attrs.due_date_days_offset = attrs.start_date_days_offset + 7;

                const idVerificationPeriod = IdVerificationPeriod.new(attrs);
                idVerificationPeriod.$$embeddedIn = this;
                this.id_verification_periods.push(idVerificationPeriod);
                return idVerificationPeriod;
            },

            /**
             * Computes an end date
             * @param  {integer} startTimestamp The startDate expressed as a timestamp in milliseconds
             * @param  {Array} periods The schedulableItem's periods
             * @return {Moment} A moment object representing the schedulableItem's endDate
             */
            computeEndDate(startTimestamp, periods) {
                let span = 0;
                _.each(periods, period => {
                    span += period.days + period.days_offset;
                });
                return dateHelper.addInDefaultTimeZone(moment(startTimestamp), span, 'days');
            },

            computePeriodDates() {
                // Start with the schedulable item's startDate
                // (Feels like a bug in the calendar widget that we have to set the timezone
                // here and in the options above)
                let rollingDate = moment(this.startDate);

                // Loop through each period and compute the start and end dates based on the duration and offset
                _.each(this.periods, period => {
                    rollingDate = dateHelper.addInDefaultTimeZone(rollingDate, period.days_offset, 'days'); // add the offset
                    // the easiest way to get periods to show up cleanly in the calendar
                    // is to have all of them start and end on day boundaries to prevent
                    // overlap. See comment above about nextDayThreshold
                    period.startDate = rollingDate.toDate();
                    rollingDate = dateHelper.addInDefaultTimeZone(rollingDate, period.days, 'days'); // add the duration

                    period.endDate = rollingDate.toDate();
                });

                if (this.persistsEndDate) {
                    this.end_date = this.endDate.getTime() / 1000;
                }
            },

            computeDefaultStartDate() {
                // Get next Monday at 12:00 AM
                let nextMonday = new Date();
                nextMonday.setDate(nextMonday.getDate() + ((1 + 7 - nextMonday.getDay()) % 7)); // http://stackoverflow.com/a/33078673/1747491
                nextMonday.setHours(0, 0, 0, 0);

                // Shift timezone
                nextMonday = dateHelper.shiftDateToTimezone(nextMonday, 'America/New_York');

                // Add four hours
                nextMonday = moment(nextMonday).add(4, 'hours').toDate();

                return nextMonday;
            },

            periodForRequiredStream(localePackId) {
                return _.detect(this.periods, period => !!period.requiresStream(localePackId)) || null;
            },

            addPeriod(index) {
                index = angular.isDefined(index) ? index : this.periods.length;
                const period = Period.new();
                period.$$embeddedIn = this;
                this.periods.splice(index, 0, period);
            },

            removePeriod(period) {
                this.periods = _.without(this.periods, period);
            },

            couldSupportStripePlan(stripePlan) {
                if (this.supportsOnlyOneTimePayment) {
                    return stripePlan.frequency === 'once';
                }
                return true;
            },

            getPeriodForDate(date) {
                return _.detect(this.periods, period => period.startDate < date && period.endDate >= date);
            },

            // playlist collections are stored as raw JSON on the server,
            // so it doesn't seem necessary to create an Iguana model for
            // playlist collection objects at this point.
            newPlaylistCollection(attrs = {}) {
                return _.extend(
                    {
                        title: '',
                        required_playlist_pack_ids: [],
                    },
                    attrs,
                );
            },

            addPlaylistCollection(attrs) {
                this.playlist_collections.push(this.newPlaylistCollection(attrs));
            },

            removePlaylistCollection(playlistCollection) {
                if (!playlistCollection) {
                    throw new Error('Must specify a playlistCollection');
                }

                this.playlist_collections = _.without(this.playlist_collections, playlistCollection);
            },

            _setDefaultRegistrationDeadline() {
                if (!this.supportsRegistrationDeadline) {
                    this.registration_deadline_days_offset = null;
                } else if (!this.registration_deadline_days_offset) {
                    this.registration_deadline_days_offset = -7;
                }
            },

            _setDefaultEarlyRegistrationDeadline() {
                if (!this.supportsEarlyRegistrationDeadline) {
                    this.early_registration_deadline_days_offset = null;
                } else if (!this.early_registration_deadline_days_offset) {
                    this.early_registration_deadline_days_offset = -12;
                }
            },

            _setDefaultEnrollmentDeadline() {
                if (!this.supportsEnrollmentDeadline) {
                    this.enrollment_deadline_days_offset = 0;
                    return;
                }

                if (!this.enrollment_deadline_days_offset) {
                    this.enrollment_deadline_days_offset = 21;
                }
            },
        });
    },
]);
