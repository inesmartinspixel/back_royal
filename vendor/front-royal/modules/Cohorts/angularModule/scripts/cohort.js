import angularModule from 'Cohorts/angularModule/scripts/cohorts_module';
import moment from 'moment-timezone';

angularModule.factory('Cohort', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');
        const ConfigFactory = $injector.get('ConfigFactory');
        const TranslationHelper = $injector.get('TranslationHelper');
        const $filter = $injector.get('$filter');
        const dateHelper = $injector.get('dateHelper');
        const Schedulable = $injector.get('Schedulable');
        const $rootScope = $injector.get('$rootScope');
        const Stream = $injector.get('Lesson.Stream');
        const guid = $injector.get('guid');
        const HasLearnerProjects = $injector.get('HasLearnerProjects');

        return Iguana.subclass(function () {
            this.setCollection('cohorts');
            this.alias('Cohort');
            this.setIdProperty('id');
            this.include(Schedulable);
            this.include(HasLearnerProjects);

            this.setCallback('after', 'copyAttrsOnInitialize', function () {
                this.specialization_playlist_pack_ids = this.specialization_playlist_pack_ids || [];
                this.periods = this.periods || [];
                this.start_date = this.start_date || this.computeDefaultStartDate().getTime() / 1000;

                // Sort the admission_rounds
                // FIXME: Check if there is an Iguana way to do this
                this.admission_rounds = _.sortBy(this.admission_rounds, 'application_deadline');

                // Fill out dates for all the periods
                this.computePeriodDates();

                this.slack_rooms = this.slack_rooms || [];
            });

            this.VALID_STATUSES = ['closed', 'open'];

            this.extend({
                // Given an admission round, a cohort that contains that admisssion round, and a set of cohorts with that cohort, determine the previous
                // admission round if possible
                getPreviousAdmissionRound(admissionRound, cohort, cohorts) {
                    // Find the index of the admission round we want the previous for
                    const admissionRoundIndex = _.indexOf(cohort.admission_rounds, admissionRound);

                    // If it is not the first then just snag the one before it. Easy peasy.
                    if (admissionRoundIndex > 0) {
                        return cohort.admission_rounds[admissionRoundIndex - 1].applicationDeadline;
                    }
                    if (admissionRoundIndex === 0) {
                        // If it is the first admission around then we need to go to the previous cohort's (of the same type)
                        // last admission round.
                        cohorts = _.where(cohorts, {
                            program_type: cohort.program_type,
                        });
                        cohorts = _.sortBy(cohorts, 'startDate');

                        // Find the cohort that we want the previous for
                        const cohortIndex = _.indexOf(cohorts, cohort);

                        if (cohortIndex > 0) {
                            const previousCohort = cohorts[cohortIndex - 1];
                            return _.isEmpty(previousCohort.admission_rounds)
                                ? null
                                : _.last(previousCohort.admission_rounds).applicationDeadline;
                        }
                        // There is no cohort to get a previous admission round for
                        return null;
                    }
                    console.warn('Could not find admission round');
                    return null;
                },
            });

            // Returns the cohort's title without 'Certificate' on the end of it.
            // Example: 'Data Analytics Certificate' => 'Data Analytics'
            Object.defineProperty(this.prototype, 'titleWithoutTheOrCertificate', {
                get() {
                    const title = this.titleWithoutThe.trim();

                    if (title.endsWith('Certificate')) {
                        return title.replace('Certificate', '').trim();
                    }
                    return title.trim();
                },
            });

            Object.defineProperty(this.prototype, 'currentPeriod', {
                get() {
                    if (
                        this.current_period_index === null ||
                        this.current_period_index === undefined ||
                        this.current_period_index < 0
                    ) {
                        return null;
                    }

                    return this.periods[this.current_period_index];
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'playlistPackIds', {
                get() {
                    return this.requiredPlaylistPackIds.concat(this.specialization_playlist_pack_ids);
                },
            });

            Object.defineProperty(this.prototype, 'foundationsPlaylistLocalePackId', {
                get() {
                    return this.foundations_playlist_locale_pack ? this.foundations_playlist_locale_pack.id : null;
                },
            });

            // this is true for cohorts, but not curriculum templates
            Object.defineProperty(this.prototype, 'supportsExternalNotes', {
                get() {
                    return true;
                },
            });

            Object.defineProperty(this.prototype, 'onlyStripePlanId', {
                get() {
                    if (!this.stripe_plans || this.stripe_plans.length !== 1) {
                        throw new Error(`Expected exactly one stripe plan for cohort ${this.name}`);
                    }
                    return this.stripe_plans[0].id;
                },
            });

            Object.defineProperty(this.prototype, 'price', {
                get() {
                    const plan = _.findWhere(this.stripe_plans, {
                        id: this.onlyStripePlanId,
                    });
                    return plan.amount / 100;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'localizedFoundationsPlaylist', {
                get() {
                    let localizedFoundationsPlaylist = _.findWhere(
                        this.foundations_playlist_locale_pack.content_items,
                        {
                            locale: $rootScope.currentUser.pref_locale,
                        },
                    );

                    // If a version of the foundations playlist does not exist then default to 'en'
                    if (!localizedFoundationsPlaylist) {
                        localizedFoundationsPlaylist = _.findWhere(
                            this.foundations_playlist_locale_pack.content_items,
                            {
                                locale: 'en',
                            },
                        );
                        console.warn(
                            `No localized version exists for ${this.name} foundation playlist. Defaulting to en.`,
                        );
                    }

                    return localizedFoundationsPlaylist;
                },
            });

            Object.defineProperty(this.prototype, 'isEMBA', {
                get() {
                    return this.program_type === 'emba';
                },
            });

            Object.defineProperty(this.prototype, 'isDegreeProgram', {
                get() {
                    return _.contains(['mba', 'emba'], this.program_type);
                },
            });

            Object.defineProperty(this.prototype, 'isUnfinishedDegreeProgram', {
                get() {
                    return this.isDegreeProgram && this.endDate > new Date();
                },
            });

            Object.defineProperty(this.prototype, 'isFinishedDegreeProgram', {
                get() {
                    return this.isDegreeProgram && this.endDate <= new Date();
                },
            });

            Object.defineProperty(this.prototype, 'createdAt', {
                get() {
                    return new Date(this.created_at * 1000);
                },
            });

            Object.defineProperty(this.prototype, 'updatedAt', {
                get() {
                    return new Date(this.updated_at * 1000);
                },
            });

            Object.defineProperty(this.prototype, 'startDate', {
                get() {
                    if (!this.$$startDate) {
                        this.$$startDate = new Date(this.start_date * 1000);
                    }
                    return this.$$startDate;
                },
                set(val) {
                    this.start_date = val.getTime() / 1000;
                    this.$$startDate = undefined;
                },
                configurable: true, // specs
            });

            // See also cohort.rb#trial_end_date
            Object.defineProperty(this.prototype, 'trialEndDate', {
                get() {
                    if (!this.$$trialEndDate) {
                        this.$$trialEndDate = new Date(this.trial_end_date * 1000);
                    }
                    return this.$$trialEndDate;
                },
                configurable: true, // specs
            });

            Object.defineProperty(this.prototype, 'lastSavedAtDateString', {
                get() {
                    if (!this.updated_at) {
                        return 'Unknown';
                    }
                    return $filter('amDateFormat')(this.updatedAt, 'l h:mm:ss a');
                },
            });

            Object.defineProperty(this.prototype, 'modifiedAtDateString', {
                get() {
                    if (!this.modified_at) {
                        return 'Unknown';
                    }
                    return $filter('amDateFormat')(this.modified_at, 'l h:mm:ss a');
                },
            });

            Object.defineProperty(this.prototype, 'publishedAt', {
                get() {
                    return (this.published_at && new Date(1000 * this.published_at)) || undefined;
                },
            });

            Object.defineProperty(this.prototype, 'lastPublishedDateString', {
                get() {
                    if (!this.published_at) {
                        return 'Unknown';
                    }
                    return $filter('amDateFormat')(this.publishedAt, 'l h:mm:ss a');
                },
            });

            Object.defineProperty(this.prototype, 'incomplete', {
                get() {
                    return !this.name || !this.title || !this.start_date;
                },
            });

            _.each(['exam', 'review', 'break', 'specialization', 'project'], style => {
                Object.defineProperty(this.prototype, `${style}Periods`, {
                    get() {
                        return _.where(this.periods, {
                            style,
                        });
                    },
                });
            });

            Object.defineProperty(this.prototype, 'supportsExternalScheduleUrl', {
                get() {
                    return this.supportsSchedule;
                },
            });

            Object.defineProperty(this.prototype, 'requiredStreamPackIdsCache', {
                get() {
                    const self = this;
                    if (!self._requiredStreamPackIdsCache) {
                        self._requiredStreamPackIdsCache = {};
                        _.each(self.getRequiredStreamPackIdsFromPeriods(), localePackId => {
                            self._requiredStreamPackIdsCache[localePackId] = true;
                        });
                    }
                    return self._requiredStreamPackIdsCache;
                },
            });

            Object.defineProperty(this.prototype, 'registrationOpenDate', {
                get() {
                    return new Date(moment(this.registrationDeadline).subtract(42, 'days'));
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'afterRegistrationOpenDate', {
                get() {
                    return moment().valueOf() >= moment(this.registrationOpenDate).startOf('day').valueOf();
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'registrationDeadlineHasPassed', {
                get() {
                    return moment().valueOf() >= moment(this.registrationDeadline).valueOf();
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasVerificationPeriods', {
                get() {
                    return _.any(this.id_verification_periods);
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'requiresIdUpload', {
                get() {
                    return this.supportsDocumentUpload && !this.hasVerificationPeriods;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'activeIdVerificationPeriod', {
                get() {
                    return (
                        _.chain(this.id_verification_periods)
                            .clone()
                            .reverse()
                            .detect(period => period.startDate < new Date())
                            .value() || null
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'previousAndCurrentlyActiveIdVerificationPeriods', {
                get() {
                    return _.filter(this.id_verification_periods, period => period.startDate < new Date());
                },
            });

            Object.defineProperty(this.prototype, 'isLegacyEnrollmentCohort', {
                get() {
                    // special handling for EMBA10 and earlier / MBA15 and earlier where we don't necessarily
                    // want to corrupt the notion of receiving an enrollment agreement by updating the backing data,
                    // but want to indicate that these applications had gone through a similar / comparable process
                    return (
                        (this.program_type === 'mba' && this.startDate < new Date('2018/08/21')) ||
                        (this.program_type === 'emba' && this.startDate < new Date('2018/07/17'))
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'learnerProjectIdsFromPeriods', {
                get() {
                    return _.chain(this.periods).pluck('learner_project_ids').flatten().uniq().compact().value();
                },
            });

            Object.defineProperty(this.prototype, 'allLearnerProjectIds', {
                get() {
                    return this.learner_project_ids.concat(this.learnerProjectIdsFromPeriods);
                },
            });

            Object.defineProperty(this.prototype, 'lastDecisionDate', {
                get() {
                    const lastAdmissionRound = _.last(this.admission_rounds);
                    return lastAdmissionRound && lastAdmissionRound.decisionDate;
                },
            });

            return {
                persistsEndDate: true,
                supportsTitle: true,

                getRequiredPlaylists(playlists) {
                    playlists = _.indexBy(playlists, 'localePackId');
                    return _.chain(this.requiredPlaylistPackIds)
                        .map(localePackId => playlists[localePackId])
                        .compact()
                        .value();
                },

                getSpecializationPlaylists(playlists) {
                    playlists = _.indexBy(playlists, 'localePackId');
                    return _.chain(this.specialization_playlist_pack_ids)
                        .map(localePackId => playlists[localePackId])
                        .compact()
                        .value();
                },

                // determines if the streamLocalePackId passed in is the first stream in the foundations playlist
                isFirstStreamInFoundationsPlaylist(streamLocalePackId) {
                    return this.foundations_lesson_stream_locale_pack_ids[0] === streamLocalePackId;
                },

                // NOTE: this calculation should return the same value as percent_complete_for_cohort in server code
                getPercentComplete(playlists) {
                    // Used for uniq()
                    const getComparisonValue = item => item.id;

                    // Get the playlists that are in this cohort
                    const requiredPlaylistsForCohort = this.getRequiredPlaylists(playlists);

                    // Throw an error if we don't have all the playlists available
                    if (requiredPlaylistsForCohort.length !== this.requiredPlaylistPackIds.length) {
                        throw new Error('Loaded Playlists does not match Cohort#requiredPlaylistPackIds');
                    }

                    const streamsFromRequiredPlaylists = _.chain(requiredPlaylistsForCohort)
                        .pluck('streams')
                        .flatten()
                        .uniq(getComparisonValue)
                        .value();
                    const streamPackIdsFromRequiredPlaylists = _.pluck(streamsFromRequiredPlaylists, 'localePackId');

                    const streamsFromSchedule = _.chain(this.getRequiredStreamPackIdsFromPeriods())
                        .difference(streamPackIdsFromRequiredPlaylists)
                        .map(localePackId => Stream.getCachedForLocalePackId(localePackId))
                        .value();

                    const streamsForCohort = streamsFromRequiredPlaylists.concat(streamsFromSchedule);
                    const streamsCompletedForCohort = _.chain(streamsForCohort).select('complete').value();

                    return Math.round((100 * streamsCompletedForCohort.length) / streamsForCohort.length);
                },

                lastAdmissionRoundDeadlineHasPassed() {
                    if (!this.supportsAdmissionRounds) {
                        return false;
                    }

                    return !this.getApplicableAdmissionRound();
                },

                admissionRoundDeadlineHasPassed(appliedAt) {
                    if (!this.supportsAdmissionRounds) {
                        return false;
                    }

                    const applicableAdmissionRound = this.getApplicableAdmissionRound(appliedAt);
                    return !applicableAdmissionRound || applicableAdmissionRound.applicationDeadline <= Date.now();
                },

                getApplicableAdmissionRound(appliedAt) {
                    if (appliedAt) {
                        return _.find(
                            this.admission_rounds,
                            admissionRound => appliedAt < admissionRound.applicationDeadline,
                        );
                    }

                    const now = new Date();
                    if (this.startDate < now) {
                        return null;
                    }
                    return _.find(this.admission_rounds, admissionRound => admissionRound.applicationDeadline > now);
                },

                getApplicationDeadlineMessage(appliedAt) {
                    // Create default message about the deadline
                    const translationHelper = new TranslationHelper('settings.application_status'); // just cause it is already there

                    if (!this.supportsAdmissionRounds) {
                        return translationHelper.get('deadline_message_rolling_basis');
                    }

                    const config = ConfigFactory.getSync();

                    if (
                        config.alternate_emba_us_banner_message &&
                        this.program_type === 'emba' &&
                        config.embaComingSoon()
                    ) {
                        return config.alternate_emba_us_banner_message;
                    }

                    let applicationDeadlineMessage;
                    const applicableAdmissionRound = this.getApplicableAdmissionRound(appliedAt);

                    if (!applicableAdmissionRound) {
                        return null;
                    }

                    let localeKey;
                    if (this.admissionRoundDeadlineHasPassed(appliedAt) && this.admission_rounds.length > 1) {
                        // multiple rounds (closed)
                        localeKey = `application_deadline_multiple_rounds_closed${
                            applicableAdmissionRound.index + 1 === this.admission_rounds.length ? '_final' : ''
                        }${appliedAt ? '_with_decision_date' : ''}`;
                    } else if (this.admissionRoundDeadlineHasPassed(appliedAt)) {
                        // single round (closed)
                        localeKey = `application_deadline_single_round_closed${appliedAt ? '_with_decision_date' : ''}`;
                    } else if (this.admission_rounds.length > 1) {
                        // multiple rounds (not closed)
                        localeKey = `application_deadline_multiple_rounds${
                            applicableAdmissionRound.index + 1 === this.admission_rounds.length ? '_final' : ''
                        }${appliedAt ? '_with_decision_date' : ''}`;
                    } else {
                        // single round (not closed)
                        localeKey = `application_deadline_single_round${appliedAt ? '_with_decision_date' : ''}`;
                    }

                    applicationDeadlineMessage = translationHelper.get(localeKey, {
                        roundIndex: applicableAdmissionRound.index + 1,
                        applicationDeadline: dateHelper.formattedUserFacingMonthDayYearLong(
                            applicableAdmissionRound.applicationDeadline,
                        ),
                        decisionDate: dateHelper.formattedUserFacingMonthDayYearLong(
                            applicableAdmissionRound.decisionDate,
                        ),
                    });

                    return applicationDeadlineMessage;
                },

                getReapplyAfterDeadlineMessage() {
                    // Create default message about the deadline
                    const translationHelper = new TranslationHelper('settings.application_status'); // just cause it is already there

                    if (!this.supportsAdmissionRounds) {
                        return null;
                    }

                    let applicationDeadlineMessage;
                    const applicableAdmissionRound = _.last(this.admission_rounds);

                    if (!applicableAdmissionRound) {
                        return null;
                    }

                    const localeKey =
                        this.admission_rounds.length > 1
                            ? 're_apply_after_multiple_rounds'
                            : 're_apply_after_single_round';
                    applicationDeadlineMessage = translationHelper.get(localeKey, {
                        roundIndex: applicableAdmissionRound.index + 1,
                        applicationDeadline: dateHelper.formattedUserFacingMonthDayYearLong(
                            applicableAdmissionRound.applicationDeadline,
                        ),
                    });

                    return applicationDeadlineMessage;
                },

                getRequiredStreamPackIdsFromPeriods(maxPeriod) {
                    if (angular.isUndefined(maxPeriod)) {
                        maxPeriod = this.periods.length - 1;
                    }

                    if (!this.supportsSchedule || maxPeriod < 0) {
                        return [];
                    }

                    return _.chain(this.periods.slice(0, maxPeriod + 1))
                        .pluck('requiredStreamLocalePackIds')
                        .flatten()
                        .uniq()
                        .value();
                },

                // NOTE: This logic should match the logic in Cohort#formatted_project_submission_email in cohort.rb.
                getFormattedProjectSubmissionEmail() {
                    // Paid certificates have one project per cohort,
                    // one cohort per program_type, and one email address
                    // for all paid certificates.
                    // Paid certificate email format:
                    //     project_submission_email_username+<cohort name>@pedago.com
                    //     i.e. cert_submission+PCERTMP1@pedago.com
                    //
                    // MBA/EMBA have multiple projects per cohort,
                    // multiple cohorts within a program_type, and
                    // one email address per program_type.
                    // MBA/EMBA email format:
                    //     project_submission_email_username+<cohort name>.<project index>@pedago.com
                    //     i.e. mbaproject+MBA11.p1@pedago.com

                    if (this.project_submission_email) {
                        const components = this.project_submission_email.split('@');
                        const formattedCohortName = this.name.replace(/\s/g, '');

                        if (this.supportsCohortLevelProjects) {
                            return `${components[0]}+${formattedCohortName}@${components[1]}`;
                        }
                        if (this.currentPeriod && this.currentPeriod.style === 'project') {
                            // Compute the project submission email for MBA/EMBA
                            //
                            // 1. Get the ordered list of projects in the cohort
                            // 2. Remove trailing numbers from their names
                            // 3. Unique the list
                            //   3a. If the list is longer than 1, append project index to email addresses

                            const uniqueProjects = _.chain(this.periods)
                                .filter(period => period.style === 'project')
                                .map(period => period.project_style.replace(/(_?\d+)$/, ''))
                                .uniq()
                                .value();

                            const projectIndex = uniqueProjects.indexOf(
                                this.currentPeriod.project_style.replace(/(_?\d+)$/, ''),
                            );

                            if (uniqueProjects.length > 1 && projectIndex > -1) {
                                return `${components[0]}+${formattedCohortName}.p${projectIndex + 1}@${components[1]}`;
                            }
                            return `${components[0]}+${formattedCohortName}@${components[1]}`;
                        }
                    }

                    return null;
                },

                addSlackRoom() {
                    let title = `Room ${this.slack_rooms.length + 1}`;

                    // Make sure we don't select a title that already exists (which could
                    // happen if rooms were added and then removed)
                    if (_.chain(this.slack_rooms).pluck('title').contains(title).value()) {
                        title = '';
                    }

                    const room = {
                        id: guid.generate(),
                        cohort_id: this.id,
                        title,
                    };

                    this.slack_rooms.push(room);

                    _.chain(this.periods)
                        .pluck('exercises')
                        .flatten()
                        .each(exercise => {
                            // If the exercise is set up to send distinct messages or documents
                            // to each slack room, then create an entry for this slack room.
                            if (exercise.slack_room_overrides) {
                                exercise.slack_room_overrides[room.id] = {};
                            }
                        });

                    return room;
                },

                removeSlackRoom(room) {
                    const self = this;
                    self.slack_rooms = _.without(self.slack_rooms, room);

                    _.chain(self.periods)
                        .pluck('exercises')
                        .flatten()
                        .each(exercise => {
                            // If the exercise is set up to send distinct messages or documents
                            // to each slack room, then remove this slack room from the list
                            // of overrides.  If, after removing it, there is only one slack room
                            // left, then revert back to the option of sending one message to
                            // all slack rooms.
                            if (exercise.slack_room_overrides) {
                                delete exercise.slack_room_overrides[room.id];

                                if (exercise.slack_room_overrides && self.slack_rooms.length < 2) {
                                    const entry = _.values(exercise.slack_room_overrides)[0];
                                    exercise.message = entry.message;
                                    exercise.document = entry.document;
                                    delete exercise.slack_room_overrides;
                                }
                            }
                        });
                },
            };
        });
    },
]);
