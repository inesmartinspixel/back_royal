import angularModule from 'Cohorts/angularModule/scripts/cohorts_module';
// This can just go away if we ever wire up multiple projects at the cohort level
angularModule.factory('HasLearnerProjects', [
    '$injector',

    $injector => {
        const AModuleAbove = $injector.get('AModuleAbove');

        return new AModuleAbove({
            included(target) {
                Object.defineProperty(target.prototype, 'learnerProjectId', {
                    get() {
                        if (this.learner_project_ids && this.learner_project_ids.length > 1) {
                            throw new Error('Expecting at most one learner_project_id');
                        }
                        return this.learner_project_ids ? this.learner_project_ids[0] : null;
                    },
                    set(val) {
                        this.learner_project_ids = this.learner_project_ids || [];
                        this.learner_project_ids.splice(0, 99999, val);
                    },
                });
            },
        });
    },
]);
