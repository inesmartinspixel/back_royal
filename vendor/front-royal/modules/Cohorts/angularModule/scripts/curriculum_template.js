import angularModule from 'Cohorts/angularModule/scripts/cohorts_module';

angularModule.factory('CurriculumTemplate', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');
        const Schedulable = $injector.get('Schedulable');
        const HasLearnerProjects = $injector.get('HasLearnerProjects');

        return Iguana.subclass(function () {
            this.setCollection('curriculum_templates');
            this.alias('CurriculumTemplate');
            this.setIdProperty('id');
            this.include(Schedulable);
            this.include(HasLearnerProjects);

            this.setCallback('after', 'copyAttrsOnInitialize', function () {
                this.specialization_playlist_pack_ids = this.specialization_playlist_pack_ids || [];
                this.periods = this.periods || [];
                this.computePeriodDates();
            });

            // This is not actually saved to the db, but it is used in the admin.
            // It follows the structure of cohort
            Object.defineProperty(this.prototype, 'startDate', {
                get() {
                    if (!this.$$startDate) {
                        this.$$startDate = this.computeDefaultStartDate();
                    }
                    return this.$$startDate;
                },
                set(val) {
                    this.$$startDate = val;
                },
            });

            // We do not assign slack rooms at the template level
            Object.defineProperty(this.prototype, 'supportsSlackRooms', {
                value: false,
            });

            // We do not allow the setting of this flag at the template level
            Object.defineProperty(this.prototype, 'supportsIsolatedNetwork', {
                value: false,
            });

            return {
                // match api for cohorts
                supportsSchedule: true,
                supportsTitle: false,
            };
        });
    },
]);
