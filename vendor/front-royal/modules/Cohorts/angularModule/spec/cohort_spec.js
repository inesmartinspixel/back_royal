import moment from 'moment-timezone';
import 'AngularSpecHelper';
import 'Cohorts/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Playlists/angularModule/spec/_mock/fixtures/playlists';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import setSpecLocales from 'Translation/setSpecLocales';
import applicationStatusLocale from 'Settings/locales/settings/application_status-en.json';

setSpecLocales(applicationStatusLocale);

describe('Cohort', () => {
    let $injector;
    let guid;
    let Cohort;
    let Stream;
    let Playlist;
    let SpecHelper;
    let ConfigFactory;
    let relevantCohort;
    let Period;
    let dateHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Cohorts', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                Cohort = $injector.get('Cohort');
                Stream = $injector.get('Lesson.Stream');
                Playlist = $injector.get('Playlist');
                SpecHelper = $injector.get('SpecHelper');
                guid = $injector.get('guid');
                ConfigFactory = $injector.get('ConfigFactory');
                Period = $injector.get('Period');
                dateHelper = $injector.get('dateHelper');

                $injector.get('CohortFixtures');
                $injector.get('LessonFixtures');
                $injector.get('StreamFixtures');
                $injector.get('PlaylistFixtures');

                SpecHelper.stubEventLogging();

                relevantCohort = Cohort.fixtures.getInstance();
            },
        ]);
    });

    describe('admission rounds', () => {
        it('should order by the application deadline before save to ensure proper indices', () => {
            relevantCohort.startDate = moment().add(100, 'days').toDate();

            let sortedByDeadline = _.sortBy(relevantCohort.admission_rounds, 'applicationDeadline');
            expect(sortedByDeadline[0].index).toBe(0);
            expect(sortedByDeadline[1].index).toBe(1);

            // flip the order
            relevantCohort.admission_rounds = relevantCohort.admission_rounds.reverse();

            // verify that the indices are out of whack when sorted by deadline
            sortedByDeadline = _.sortBy(relevantCohort.admission_rounds, 'applicationDeadline');
            expect(sortedByDeadline[0].index).toBe(1);
            expect(sortedByDeadline[1].index).toBe(0);

            // save
            Cohort.expect('update');
            relevantCohort.save();

            // verify all is well again
            sortedByDeadline = _.sortBy(relevantCohort.admission_rounds, 'applicationDeadline');
            expect(sortedByDeadline[0].index).toBe(0);
            expect(sortedByDeadline[1].index).toBe(1);

            Cohort.flush('update');
        });
    });

    describe('lastAdmissionRoundDeadlineHasPassed', () => {
        it('should return false early if supportsAdmissionRounds is falsy', () => {
            jest.spyOn(relevantCohort, 'supportsAdmissionRounds', 'get').mockReturnValue(false);
            jest.spyOn(relevantCohort, 'getApplicableAdmissionRound').mockImplementation(() => {});
            const lastAdmissionRoundDeadlineHasPassed = relevantCohort.lastAdmissionRoundDeadlineHasPassed();
            expect(relevantCohort.getApplicableAdmissionRound).not.toHaveBeenCalled();
            expect(lastAdmissionRoundDeadlineHasPassed).toBe(false);
        });

        it('should be determined by getApplicableAdmissionRound', () => {
            jest.spyOn(relevantCohort, 'getApplicableAdmissionRound').mockImplementation(() => {});
            relevantCohort.lastAdmissionRoundDeadlineHasPassed();
            expect(relevantCohort.getApplicableAdmissionRound).toHaveBeenCalled();
        });

        it('should return false if applicable admission round is found', () => {
            // Stub out call to getApplicableAdmissionRound and its return value.
            jest.spyOn(relevantCohort, 'getApplicableAdmissionRound').mockReturnValue(
                relevantCohort.admission_rounds[0],
            );
            const lastAdmissionRoundDeadlineHasPassed = relevantCohort.lastAdmissionRoundDeadlineHasPassed();
            expect(lastAdmissionRoundDeadlineHasPassed).toBe(false);
        });

        it('should return true if no applicable admission round is found', () => {
            // Stub out call to getApplicableAdmissionRound and its return value.
            jest.spyOn(relevantCohort, 'getApplicableAdmissionRound').mockReturnValue(null);
            const lastAdmissionRoundDeadlineHasPassed = relevantCohort.lastAdmissionRoundDeadlineHasPassed();
            expect(lastAdmissionRoundDeadlineHasPassed).toBe(true);
        });
    });

    describe('admissionRoundDeadlineHasPassed', () => {
        it('should return false if not all admission rounds have completed deadlines', () => {
            relevantCohort.startDate = moment().add(100, 'days').toDate();
            expect(relevantCohort.admissionRoundDeadlineHasPassed()).toEqual(false);
        });

        it('should return true if all admission rounds have completed deadlines', () => {
            relevantCohort.startDate = moment().subtract(100, 'days').toDate();
            expect(relevantCohort.admissionRoundDeadlineHasPassed()).toEqual(true);
        });

        it('should return false if it does not support admission rounds', () => {
            const relevantCohort = Cohort.new({
                admission_rounds: undefined,
            });
            expect(relevantCohort.admissionRoundDeadlineHasPassed()).toEqual(false);
        });
    });

    describe('getApplicableAdmissionRound', () => {
        let admissionRounds;

        beforeEach(() => {
            admissionRounds = [
                {
                    applicationDeadline: moment().subtract(1, 'd').toDate(),
                },
                {
                    applicationDeadline: moment().add(1, 'd').toDate(),
                },
                {
                    applicationDeadline: moment().add(2, 'd').toDate(),
                },
            ];
            relevantCohort.admission_rounds = admissionRounds;
        });

        describe('with appliedAt', () => {
            it('should return first admission round with applicationDeadline after appliedAt', () => {
                const appliedAt = new Date(); // between first and second admission rounds
                const applicableAdmissionRound = relevantCohort.getApplicableAdmissionRound(appliedAt);
                expect(applicableAdmissionRound).toEqual(admissionRounds[1]);
            });

            it('should return undefined if appliedAt is after all application deadlines for admission rounds', () => {
                const appliedAt = moment().add(3, 'd').toDate(); // after last admission round
                const applicableAdmissionRound = relevantCohort.getApplicableAdmissionRound(appliedAt);
                expect(applicableAdmissionRound).toBeUndefined();
            });
        });

        describe('without appliedAt', () => {
            it('should return null if startDate is less than now', () => {
                const startDate = new Date(2017, 0, 1); // in the past relative to now
                jest.spyOn(relevantCohort, 'startDate', 'get').mockReturnValue(startDate);
                expect(relevantCohort.getApplicableAdmissionRound()).toBeNull();
            });

            it('should find first admission round with applicationDeadline greater than now', () => {
                const startDate = moment().add(3, 'days').toDate(); // in the future relative to now
                jest.spyOn(relevantCohort, 'startDate', 'get').mockReturnValue(startDate);
                expect(relevantCohort.getApplicableAdmissionRound()).toEqual(admissionRounds[1]);
            });

            it('should return undefined if all admission round application deadlines are less than now', () => {
                const startDate = moment().add(3, 'days').toDate(); // in the future relative to now
                jest.spyOn(relevantCohort, 'startDate', 'get').mockReturnValue(startDate);

                // ensure all application deadlines for the admission rounds have passed
                _.each(relevantCohort.admission_rounds, admissionRound => {
                    admissionRound.applicationDeadline = moment().subtract(1, 'd').toDate();
                });

                expect(relevantCohort.getApplicableAdmissionRound()).toBeUndefined();
            });
        });
    });

    describe('getApplicationDeadlineMessage', () => {
        it('should return alternate_banner_message if specified as EMBA with coming soon (US) flag', () => {
            relevantCohort.program_type = 'emba';
            jest.spyOn(ConfigFactory, 'getSync').mockReturnValue({
                alternate_emba_us_banner_message: 'A test EMBA USA banner message.',
                embaComingSoon() {
                    return true;
                },
            });
            expect(relevantCohort.getApplicationDeadlineMessage()).toEqual('A test EMBA USA banner message.');
        });

        it('should return application_deadline with no round if there is just one round', () => {
            SpecHelper.stubConfig();
            jest.spyOn(dateHelper, 'formattedUserFacingMonthDayYearLong').mockReturnValue('FORMATTED_DATE');
            relevantCohort.startDate = moment().add(100, 'days').toDate();
            relevantCohort.admission_rounds = relevantCohort.admission_rounds.slice(0, 1);
            expect(relevantCohort.getApplicationDeadlineMessage()).toEqual(
                'Applications close <strong>FORMATTED_DATE</strong>.',
            );
            expect(relevantCohort.admission_rounds[0].applicationDeadline).not.toBeUndefined(); // sanity check
            expect(dateHelper.formattedUserFacingMonthDayYearLong).toHaveBeenCalledWith(
                relevantCohort.admission_rounds[0].applicationDeadline,
            );
        });

        it('should return application_deadline with round index if there are multiple rounds', () => {
            SpecHelper.stubConfig();
            jest.spyOn(dateHelper, 'formattedUserFacingMonthDayYearLong').mockReturnValue('FORMATTED_DATE');
            relevantCohort.startDate = moment().add(100, 'days').toDate(); // sanity check
            expect(relevantCohort.admission_rounds.length > 1).toBe(true);
            const index = relevantCohort.admission_rounds.indexOf(relevantCohort.getApplicableAdmissionRound()) + 1;
            expect(relevantCohort.getApplicationDeadlineMessage()).toEqual(
                `Round ${index} applications close <strong>FORMATTED_DATE</strong>.`,
            );
            expect(relevantCohort.admission_rounds[0].applicationDeadline).not.toBeUndefined(); // sanity check
            expect(dateHelper.formattedUserFacingMonthDayYearLong).toHaveBeenCalledWith(
                relevantCohort.admission_rounds[0].applicationDeadline,
            );
        });

        it('should return application_deadline with final round if there are multiple rounds and it is the last round', () => {
            SpecHelper.stubConfig();
            jest.spyOn(dateHelper, 'formattedUserFacingMonthDayYearLong').mockReturnValue('FORMATTED_DATE');
            relevantCohort.startDate = moment().add(20, 'days').toDate(); // sanity check
            expect(relevantCohort.admission_rounds.length > 1).toBe(true);
            expect(relevantCohort.getApplicationDeadlineMessage()).toEqual(
                'Final round applications close <strong>FORMATTED_DATE</strong>.',
            );
        });

        it('should return null if no active admission round', () => {
            SpecHelper.stubConfig();
            jest.spyOn(relevantCohort, 'getApplicableAdmissionRound').mockReturnValue(null);
            expect(relevantCohort.getApplicationDeadlineMessage()).toEqual(null);
        });

        it('should return a special message for cohorts that do not support admission rounds (career network only)', () => {
            relevantCohort.program_type = 'career_network_only';
            expect(relevantCohort.getApplicationDeadlineMessage()).toEqual(
                'We review on a rolling basis, and usually get back to applicants within 5-10 days.',
            );
        });

        it('should return a special message for cohorts that do not support admission rounds (certificate cohorts)', () => {
            _.each(Cohort.CERTIFICATE_PROGRAM_TYPES, programType => {
                relevantCohort.program_type = programType;
                expect(relevantCohort.getApplicationDeadlineMessage()).toEqual(
                    'We review on a rolling basis, and usually get back to applicants within 5-10 days.',
                );
            });
        });
    });

    describe('getPercentComplete', () => {
        function getPlaylist(
            streams = [Stream.fixtures.getInstance(), Stream.fixtures.getInstance(), Stream.fixtures.getInstance()],
        ) {
            return Playlist.fixtures.getInstance({
                streams,
            });
        }

        it('should calculate correct percentage', () => {
            const playlist = getPlaylist();
            const cohort = Cohort.fixtures.getInstance({
                playlists: [playlist],
                periods: [],
            });

            markStreamCompletion(playlist, [true, true, false]);

            expect(cohort.getPercentComplete([playlist])).toBe(Math.round((100 * 2) / 3));
        });

        it('should count streams in the schedule that are not in a required playlist', () => {
            const playlist = getPlaylist();
            const stream = Stream.fixtures.getInstance();

            const cohort = Cohort.fixtures.getInstance({
                playlists: [playlist],
                periods: [
                    {
                        stream_entries: [
                            {
                                locale_pack_id: stream.localePackId,
                                required: true,
                            },
                        ],
                    },
                ],
            });

            markStreamCompletion(playlist, [false, false, false]);
            stream.lesson_streams_progress = {
                complete: true,
            };

            expect(cohort.getPercentComplete([playlist])).toBe(Math.round((100 * 1) / 4));
        });

        it('should handle duplicate streams', () => {
            const playlists = [getPlaylist()];
            const cloned = Stream.new(playlists[0].streams[0].asJson());
            playlists.push(
                getPlaylist([
                    Stream.fixtures.getInstance(),
                    Stream.fixtures.getInstance(),
                    Stream.fixtures.getInstance(),
                    cloned,
                ]),
            );

            const cohort = Cohort.fixtures.getInstance({
                playlists,
                periods: [],
            });

            markStreamCompletion(playlists[0], [true, false, false]);
            markStreamCompletion(playlists[1], [false, false, false, true]);

            expect(cohort.getPercentComplete(playlists)).toBe(Math.round((100 * 1) / 6));
        });

        it('should not count playlists not in the cohort', () => {
            const playlists = [getPlaylist(), getPlaylist()];

            const cohort = Cohort.fixtures.getInstance({
                playlists: [playlists[0]],
                periods: [],
            });

            markStreamCompletion(playlists[0], [true, false, false]);
            markStreamCompletion(playlists[1], [true, true, true]);

            expect(cohort.getPercentComplete(playlists)).toBe(Math.round((100 * 1) / 3));
        });

        function markStreamCompletion(playlist, vals) {
            _.each(vals, (val, i) => {
                playlist.streams[i].lesson_streams_progress = {
                    complete: val,
                };
            });
        }
    });

    describe('isFirstStreamInFoundationsPlaylist', () => {
        it('should return true if stream locale pack id is first in foundations_lesson_stream_locale_pack_ids', () => {
            relevantCohort.foundations_lesson_stream_locale_pack_ids = ['foo', 'bar', 'baz'];
            expect(relevantCohort.isFirstStreamInFoundationsPlaylist('foo')).toBe(true);
        });

        it('should return false if stream locale pack id is not first in foundations_lesson_stream_locale_pack_ids', () => {
            relevantCohort.foundations_lesson_stream_locale_pack_ids = ['foo', 'bar', 'baz'];
            expect(relevantCohort.isFirstStreamInFoundationsPlaylist('bar')).toBe(false);
        });

        it('should return false if foundations_lesson_stream_locale_pack_ids is empty', () => {
            relevantCohort.foundations_lesson_stream_locale_pack_ids = [];
            expect(relevantCohort.isFirstStreamInFoundationsPlaylist('foo')).toBe(false);
        });
    });

    describe('currentPeriod', () => {
        let cohort;

        beforeEach(() => {
            const cohort_periods = [
                {
                    title: 'period 1',
                },
                {
                    title: 'period 2',
                },
                {
                    title: 'period 3',
                },
            ];

            cohort = Cohort.new({
                periods: cohort_periods,
            });
        });

        it('should work', () => {
            cohort.current_period_index = 1;
            expect(cohort.currentPeriod.title).toEqual('period 2');
        });

        it('should work if server returns index of 0, denoting the first week', () => {
            cohort.current_period_index = 0;
            expect(cohort.currentPeriod).not.toBeNull();
        });

        it('should be null if server returns index of -1', () => {
            cohort.current_period_index = -1;
            expect(cohort.currentPeriod).toBeNull();
        });

        it('should be null if server returns index of null', () => {
            cohort.current_period_index = null;
            expect(cohort.currentPeriod).toBeNull();
        });

        it('should be null if current_period_index is undefined', () => {
            cohort.current_period_index = undefined;
            expect(cohort.currentPeriod).toBeNull();
        });
    });

    describe('playlistPackIds', () => {
        it('should return an array of the required and specialization playlists', () => {
            const specializationPlaylistPackIds = ['foo', 'bar'];
            const requiredPlaylistPackIds = ['baz', 'qux'];
            relevantCohort = Cohort.fixtures.getInstance({
                program_type: 'emba',
                specialization_playlist_pack_ids: specializationPlaylistPackIds,
            });
            const requiredPlaylistPackIdsSpy = jest
                .spyOn(relevantCohort, 'requiredPlaylistPackIds', 'get')
                .mockReturnValue(requiredPlaylistPackIds);

            // Sanity check
            expect(relevantCohort.specialization_playlist_pack_ids).toEqual(specializationPlaylistPackIds);

            expect(relevantCohort.playlistPackIds).toEqual(
                requiredPlaylistPackIds.concat(specializationPlaylistPackIds),
            );
            expect(requiredPlaylistPackIdsSpy).toHaveBeenCalled();
        });
    });

    describe('examPeriods', () => {
        it('should work', () => {
            const cohort = Cohort.new({
                periods: [
                    Period.new({
                        title: 'standard 1',
                        style: 'standard',
                    }),
                    Period.new({
                        title: 'review 1',
                        style: 'review',
                    }),
                    Period.new({
                        title: 'exam test',
                        style: 'exam',
                    }),
                ],
            });

            expect(_.pluck(cohort.examPeriods, 'title')).toEqual(['exam test']);
        });
    });

    describe('reviewPeriods', () => {
        it('should work', () => {
            const cohort = Cohort.new({
                periods: [
                    Period.new({
                        title: 'standard 1',
                        style: 'standard',
                    }),
                    Period.new({
                        title: 'review test',
                        style: 'review',
                    }),
                    Period.new({
                        title: 'exam 1',
                        style: 'exam',
                    }),
                ],
            });

            expect(_.pluck(cohort.reviewPeriods, 'title')).toEqual(['review test']);
        });
    });

    describe('breakPeriods', () => {
        it('should work', () => {
            const cohort = Cohort.new({
                periods: [
                    Period.new({
                        title: 'standard 1',
                        style: 'standard',
                    }),
                    Period.new({
                        title: 'break test',
                        style: 'break',
                    }),
                    Period.new({
                        title: 'exam 1',
                        style: 'exam',
                    }),
                ],
            });

            expect(_.pluck(cohort.breakPeriods, 'title')).toEqual(['break test']);
        });
    });

    describe('afterRegistrationOpenDate', () => {
        let cohort;
        beforeEach(() => {
            cohort = Cohort.new();
        });
        it('should return true if registrationOpenDate is now', () => {
            const now = new Date().getTime();
            jest.spyOn(Date.prototype, 'getTime').mockImplementation(() => now);
            expect(cohort.afterRegistrationOpenDate).toEqual(true);
        });
        it('should return true if after registrationOpenDate', () => {
            const registrationOpenDate = new Date(2017, 10, 20);
            jest.spyOn(cohort, 'registrationOpenDate', 'get').mockReturnValue(registrationOpenDate);
            expect(cohort.afterRegistrationOpenDate).toEqual(true);
        });
        it('should return false if before registrationOpenDate', () => {
            const currentYear = new Date().getFullYear();
            const registrationOpenDate = new Date(currentYear + 1, 10, 20);
            jest.spyOn(cohort, 'registrationOpenDate', 'get').mockReturnValue(registrationOpenDate);
            expect(cohort.afterRegistrationOpenDate).toEqual(false);
        });
        it('should return true if after midnight on registrationOpenDate', () => {
            // Simply setting the registrationOpenDate to today should work for this spec,
            // since we're only checking if NOW is >= today at midnight.
            const registrationOpenDate = new Date();
            Object.defineProperty(cohort, 'registrationOpenDate', {
                get() {
                    return registrationOpenDate;
                },
            });
            expect(cohort.afterRegistrationOpenDate).toEqual(true);
        });
        it('should return false if before midnight on day before registrationOpenDate', () => {
            // We can just set the registrationOpenDate to tomorrow, since NOW will always
            // be <= midnight tomorrow morning.
            const registrationOpenDate = new Date();
            registrationOpenDate.setDate(registrationOpenDate.getDate() + 1);
            Object.defineProperty(cohort, 'registrationOpenDate', {
                get() {
                    return registrationOpenDate;
                },
            });
            expect(cohort.afterRegistrationOpenDate).toEqual(false);
        });
    });

    describe('registrationDeadlineHasPassed', () => {
        let cohort;
        beforeEach(() => {
            cohort = Cohort.new();
        });
        it('should return true if registrationDeadline is now', () => {
            const now = new Date().getTime();
            jest.spyOn(moment.prototype, 'valueOf').mockImplementation(() => now);
            expect(cohort.registrationDeadlineHasPassed).toEqual(true);
        });
        it('should return true if after registrationDeadline', () => {
            const registrationDeadline = new Date(2017, 10, 20);
            Object.defineProperty(cohort, 'registrationDeadline', {
                get() {
                    return registrationDeadline;
                },
            });
            expect(cohort.registrationDeadlineHasPassed).toEqual(true);
        });
        it('should return false if before registrationDeadline', () => {
            const currentYear = new Date().getFullYear();
            const registrationDeadline = new Date(currentYear + 1, 10, 20);
            Object.defineProperty(cohort, 'registrationDeadline', {
                get() {
                    return registrationDeadline;
                },
            });
            expect(cohort.registrationDeadlineHasPassed).toEqual(false);
        });
    });

    describe('getFormattedProjectSubmissionEmail', () => {
        it('should return null if no project_submission_email', () => {
            const cohort = Cohort.new({
                project_submission_email: null,
            });
            expect(cohort.getFormattedProjectSubmissionEmail()).toBe(null);
        });

        describe('when supportsCohortLevelProjects', () => {
            it('should correctly compute a formatted submission email for paid certs', () => {
                const cohort = Cohort.new({
                    program_type: 'paid_cert_data_analytics',
                    name: 'Paid Cert Cohort',
                    project_submission_email: 'project_submission@pedago.com',
                });
                expect(cohort.getFormattedProjectSubmissionEmail()).toEqual(
                    'project_submission+PaidCertCohort@pedago.com',
                );
            });
        });

        describe('when !supportsCohortLevelProjects', () => {
            it('should correctly compute a formatted submission email for MBA/EMBA', () => {
                const cohort = Cohort.new({
                    name: 'MBA11',
                    project_submission_email: 'mba_project_submission@pedago.com',
                    periods: [
                        {
                            style: 'project',
                            project_style: 'mba_accounting',
                        },
                        {
                            style: 'project',
                            project_style: 'mba_accounting2',
                        },
                        {
                            style: 'project',
                            project_style: 'marketing_strategy',
                        },
                        {
                            style: 'project',
                            project_style: 'marketing_strategy2',
                        },
                    ],
                });

                for (const period of cohort.periods) {
                    const projectIndex = period.project_style.startsWith('mba_accounting') ? 1 : 2;

                    Object.defineProperty(cohort, 'currentPeriod', {
                        value: period,
                        configurable: true,
                    });
                    expect(cohort.getFormattedProjectSubmissionEmail()).toEqual(
                        `mba_project_submission+MBA11.p${projectIndex}@pedago.com`,
                    );
                }
            });

            it('should account for different formatting', () => {
                const cohort = Cohort.new({
                    name: 'MBA11',
                    project_submission_email: 'mba_project_submission@pedago.com',
                    periods: [
                        {
                            style: 'project',
                            project_style: 'mba_accounting',
                        },
                        {
                            style: 'project',
                            project_style: 'mba_accounting_2',
                        },
                        {
                            style: 'project',
                            project_style: 'marketing_strategy',
                        },
                        {
                            style: 'project',
                            project_style: 'marketing_strategy_2',
                        },
                    ],
                });

                for (const period of cohort.periods) {
                    const projectIndex = period.project_style.startsWith('mba_accounting') ? 1 : 2;

                    Object.defineProperty(cohort, 'currentPeriod', {
                        value: period,
                        configurable: true,
                    });
                    expect(cohort.getFormattedProjectSubmissionEmail()).toEqual(
                        `mba_project_submission+MBA11.p${projectIndex}@pedago.com`,
                    );
                }
            });

            it('should not append project index if only one project', () => {
                const cohort = Cohort.new({
                    name: 'MBA11',
                    project_submission_email: 'mba_project_submission@pedago.com',
                    periods: [
                        {
                            style: 'project',
                            project_style: 'mba_accounting',
                        },
                        {
                            style: 'project',
                            project_style: 'mba_accounting2',
                        },
                    ],
                });

                Object.defineProperty(cohort, 'currentPeriod', {
                    value: cohort.periods[1],
                });

                expect(cohort.getFormattedProjectSubmissionEmail()).toEqual('mba_project_submission+MBA11@pedago.com');
            });

            it('should return null if currentPeriod is not a project', () => {
                const cohort = Cohort.new({
                    name: 'MBA11',
                    project_submission_email: 'mba_project_submission@pedago.com',
                    periods: [
                        {
                            style: 'project',
                            project_style: 'mba_accounting',
                        },
                        {
                            style: 'exam',
                        },
                    ],
                });

                Object.defineProperty(cohort, 'currentPeriod', {
                    value: cohort.periods[1],
                });

                expect(cohort.getFormattedProjectSubmissionEmail()).toBe(null);
            });
        });
    });

    describe('addSlackRoom', () => {
        it('should work', () => {
            jest.spyOn(guid, 'generate').mockReturnValue('guid');
            const cohort = Cohort.new({
                id: 'cohort_id',
            });
            cohort.addSlackRoom();
            expect(cohort.slack_rooms).toEqual([
                {
                    id: 'guid',
                    title: 'Room 1',
                    cohort_id: 'cohort_id',
                },
            ]);
        });

        it('should not auto-set a title that is the same as one that already exists', () => {
            jest.spyOn(guid, 'generate').mockReturnValue('guid');
            const cohort = Cohort.new({
                id: 'cohort_id',
                slack_rooms: [
                    {
                        title: 'Room 2',
                    },
                ],
            });
            cohort.addSlackRoom();
            expect(cohort.slack_rooms[1].title).toEqual('');
        });

        it('should setup an entry in the slack_room_overrides on an exercise', () => {
            const cohort = Cohort.new({
                id: 'cohort_id',
            });
            const exercise = setUpExercise(cohort);
            cohort.addSlackRoom();
            cohort.addSlackRoom();
            expect(exercise.slack_room_overrides).toBeUndefined();
            setUpSlackRoomOverrides(exercise);
            const lastRoom = cohort.addSlackRoom();
            expect(_.size(exercise.slack_room_overrides)).toEqual(3);
            expect(exercise.slack_room_overrides[lastRoom.id]).not.toBeUndefined();
        });
    });

    describe('removeSlackRoom', () => {
        let cohort;
        let rooms;
        beforeEach(() => {
            cohort = Cohort.new({
                id: 'cohort_id',
            });
            rooms = [cohort.addSlackRoom(), cohort.addSlackRoom(), cohort.addSlackRoom()];
        });

        it('should work', () => {
            cohort.removeSlackRoom(rooms[0]);
            expect(cohort.slack_rooms).toEqual([rooms[1], rooms[2]]);
        });

        it('should remove an entry in the slack_room_overrides on an exercise', () => {
            const exercise = setUpExercise(cohort);
            setUpSlackRoomOverrides(exercise);
            expect(_.size(exercise.slack_room_overrides)).toEqual(3); // sanity check

            // removing a slack room removes the
            cohort.removeSlackRoom(rooms[0]);
            expect(_.keys(exercise.slack_room_overrides)).toEqual([rooms[1].id, rooms[2].id]);

            // if there is only 1 slack room left, then the overrides should go away
            // and the message and document should be copied to the exercise
            cohort.removeSlackRoom(rooms[1]);
            expect(exercise.slack_room_overrides).toBeUndefined();
            expect(exercise.message).toEqual('custom message for room 3');
            expect(exercise.document).toEqual({
                document: 3,
            });
        });
    });

    describe('activeIdVerificationPeriod', () => {
        it('should not return a period in the future', () => {
            const cohort = Cohort.new({
                start_date: moment().subtract(1, 'day').toDate().getTime(),
                id_verification_periods: [
                    {
                        start_date_days_offset: 10,
                        due_date_days_offset: 10,
                    },
                ],
            });
            expect(cohort.activeIdVerificationPeriod).toBe(null);
        });

        it('should return an ongoing period', () => {
            const cohort = Cohort.new({
                start_date: moment().subtract(20, 'day').toDate().getTime() / 1000,
                id_verification_periods: [
                    {
                        start_date_days_offset: 0,
                        due_date_days_offset: 10,
                    },
                    {
                        start_date_days_offset: 15,
                        due_date_days_offset: 25,
                    },
                    {
                        start_date_days_offset: 120,
                        due_date_days_offset: 25,
                    },
                ],
            });
            expect(cohort.activeIdVerificationPeriod).toBe(cohort.id_verification_periods[1]);
        });

        it('should return the last completed period', () => {
            const cohort = Cohort.new({
                start_date: moment().subtract(20, 'day').toDate().getTime() / 1000,
                id_verification_periods: [
                    {
                        start_date_days_offset: 0,
                        due_date_days_offset: 10,
                    },
                    {
                        start_date_days_offset: 10,
                        due_date_days_offset: 15,
                    },
                    {
                        start_date_days_offset: 120,
                        due_date_days_offset: 25,
                    },
                ],
            });
            expect(cohort.activeIdVerificationPeriod).toBe(cohort.id_verification_periods[1]);
        });
    });

    describe('isLegacyEnrollmentCohort', () => {
        it('should return true if MBA and started prior to 2018/08/21', () => {
            let cohort = Cohort.new({
                start_date: moment('2018/08/22').toDate().getTime() / 1000,
                program_type: 'mba',
            });
            expect(cohort.isLegacyEnrollmentCohort).toBe(false);

            cohort = Cohort.new({
                start_date: moment('2018/08/20').toDate().getTime() / 1000,
                program_type: 'mba',
            });
            expect(cohort.isLegacyEnrollmentCohort).toBe(true);
        });

        it('should return true if EMBA and started prior to 2018/07/17', () => {
            let cohort = Cohort.new({
                start_date: moment('2018/07/18').toDate().getTime() / 1000,
                program_type: 'emba',
            });
            expect(cohort.isLegacyEnrollmentCohort).toBe(false);

            cohort = Cohort.new({
                start_date: moment('2018/07/16').toDate().getTime() / 1000,
                program_type: 'emba',
            });
            expect(cohort.isLegacyEnrollmentCohort).toBe(true);
        });
    });

    describe('allLearnerProjectIds', () => {
        it('should work', () => {
            const cohort = Cohort.new({
                learner_project_ids: ['1', '2'],
                periods: [
                    {
                        learner_project_ids: ['3', '4'],
                    },
                    {
                        learner_project_ids: ['5'],
                    },
                ],
            });
            expect(cohort.allLearnerProjectIds).toEqual(['1', '2', '3', '4', '5']);
        });
    });

    describe('learnerProjectIdsFromPeriods', () => {
        it('should work', () => {
            const cohort = Cohort.new({
                learner_project_ids: ['1', '2'],
                periods: [
                    {
                        learner_project_ids: ['3', '4'],
                    },
                    {
                        learner_project_ids: ['5'],
                    },
                ],
            });
            expect(cohort.learnerProjectIdsFromPeriods).toEqual(['3', '4', '5']);
        });
    });

    function setUpExercise(cohort) {
        cohort.addPeriod();
        const period = _.last(cohort.periods);
        const exercise = period.addExercise();
        return exercise;
    }

    function setUpSlackRoomOverrides(exercise) {
        const cohort = exercise.cohort;
        exercise.slack_room_overrides = {};
        const overrides = exercise.slack_room_overrides;
        _.each(cohort.slack_rooms, (room, i) => {
            overrides[room.id] = {
                message: `custom message for room ${i + 1}`,
                document: {
                    document: i + 1,
                },
            };
        });
        return exercise;
    }
});
