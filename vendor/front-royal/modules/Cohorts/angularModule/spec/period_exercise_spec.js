import moment from 'moment-timezone';
import 'AngularSpecHelper';
import 'Cohorts/angularModule';

describe('PeriodExercise', () => {
    let $injector;
    let Cohort;
    let Period;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Cohorts', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                Cohort = $injector.get('Cohort');
                Period = $injector.get('Period');
            },
        ]);
    });

    describe('date', () => {
        let cohort;

        beforeEach(() => {
            const period = Period.new();
            period.addExercise();

            cohort = Cohort.new({
                periods: [period],
            });
        });

        describe('setter', () => {
            let period;
            let exercise;

            beforeEach(() => {
                period = cohort.periods[0];
                exercise = period.exercises[0];
            });

            it('should set the hours_offset_from_end correctly if before the period endDate', () => {
                // Subtract five days from the period endDate and expect to see -5 as the offset
                let selectedDate = moment(period.endDate).subtract(5, 'hours');
                exercise.date = selectedDate.toDate();
                expect(exercise.hours_offset_from_end).toBe(-5);

                // Subtract one day from the period endDate and expect to see -5 as the offset
                selectedDate = moment(period.endDate).subtract(1, 'hours');
                exercise.date = selectedDate.toDate();
                expect(exercise.hours_offset_from_end).toBe(-1);
            });

            it('should set the hours_offset_from_end correctly if after the period endDate', () => {
                // Add five hours from the period endDate and expect to see 5 as the offset
                let selectedDate = moment(period.endDate).add(5, 'hours');
                exercise.date = selectedDate.toDate();
                expect(exercise.hours_offset_from_end).toBe(5);

                // Add one day from the period endDate and expect to see 1 as the offset
                selectedDate = moment(period.endDate).add(1, 'hours');
                exercise.date = selectedDate.toDate();
                expect(exercise.hours_offset_from_end).toBe(1);
            });

            it('should set the hours_offset_from_end correctly if on the period endDate', () => {
                const selectedDate = moment(period.endDate);
                exercise.date = selectedDate.toDate();
                expect(exercise.hours_offset_from_end).toEqual(-0);
            });
        });

        describe('getter', () => {
            let period;
            let exercise;

            beforeEach(() => {
                period = cohort.periods[0];
                exercise = period.exercises[0];
            });

            it('should return the proper date based on hours_offset_from_end if before the period endDate', () => {
                exercise.hours_offset_from_end = -5;
                expect(moment(exercise.date).isSame(moment(period.endDate).subtract(5, 'hours'))).toBe(true);
            });

            it('should return the proper date based on hours_offset_from_end if after the period endDate', () => {
                exercise.hours_offset_from_end = 5;
                expect(moment(exercise.date).isSame(moment(period.endDate).add(5, 'hours'))).toBe(true);
            });

            it('should return the proper date based on hours_offset_from_end if on the period endDate', () => {
                exercise.hours_offset_from_end = 0;
                expect(moment(exercise.date).isSame(moment(period.endDate))).toBe(true);
            });
        });
    });
});
