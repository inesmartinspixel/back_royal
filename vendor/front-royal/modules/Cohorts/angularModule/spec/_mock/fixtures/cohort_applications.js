import 'Cohorts/angularModule';

angular.module('FrontRoyal.Cohorts').factory('CohortApplicationFixtures', [
    '$injector',
    $injector => {
        const CohortApplication = $injector.get('CohortApplication');

        CohortApplication.fixtures = {
            sampleAttrs(overrides = {}) {
                const id = String(Math.random());

                return angular.extend(
                    {
                        id,
                        status: 'pending',
                        created_at: 0,
                        updated_at: 0,
                        applied_at: 0,
                        retargeted_from_program_type: null,
                        program_type: 'program',
                    },
                    overrides,
                );
            },

            getInstance(overrides) {
                return CohortApplication.new(this.sampleAttrs(overrides));
            },
        };

        return {};
    },
]);
