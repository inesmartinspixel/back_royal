import 'Playlists/angularModule/spec/_mock/fixtures/playlists';

angular.module('FrontRoyal.Cohorts').factory('CohortFixtures', [
    '$injector',
    $injector => {
        const Cohort = $injector.get('Cohort');
        const AdmissionRound = $injector.get('AdmissionRound');
        const Playlist = $injector.get('Playlist');
        let counter = 0;
        const Period = $injector.get('Period');
        const guid = $injector.get('guid');
        $injector.get('PlaylistFixtures');

        function addSlackRoomAssignment(cohort, cohortApplications) {
            cohort.slack_room_assignment = {
                slack_room_assignments: _.map(cohort.slack_rooms, slackRoom => ({
                    slack_room_id: slackRoom.id,
                    cohort_application_ids: [],
                })),
                location_assignments: {},
            };

            const locations = [
                ['United States', 'San Jose, CA'],
                ['United States', '(other cities)'],
                ['Canada', '(all cities)'],
                ['Other Countries', ''],
            ];
            let slackRoomIndex = 0;

            const slackRoomAssignments = cohort.slack_room_assignment.slack_room_assignments;
            const locationAssignments = cohort.slack_room_assignment.location_assignments;
            _.each(cohortApplications, cohortApplication => {
                const location = locations.pop();
                slackRoomAssignments[slackRoomIndex].cohort_application_ids.push(cohortApplication.id);
                locationAssignments[cohortApplication.user_id] = location;

                locations.unshift(location);
                slackRoomIndex++;
                if (!cohort.slack_rooms[slackRoomIndex]) {
                    slackRoomIndex = 0;
                }
            });
        }

        Cohort.fixtures = {
            getPlaylistsForCohort(cohort) {
                return cohort.$$playlists;
            },

            sampleAttrs(overrides = {}) {
                const programType = overrides.program_type || 'mba';

                let playlists = overrides.playlists || [
                    Playlist.fixtures.getInstance(),
                    Playlist.fixtures.getInstance(),
                    Playlist.fixtures.getInstance(),
                ];
                delete overrides.playlists;

                const requiredPlaylistPackIds = playlists.map(playlist => {
                    playlist.ensureLocalePack();
                    return playlist.locale_pack.id;
                });

                let foundationPlaylistLocalePack;
                if (playlists && playlists.length > 0) {
                    foundationPlaylistLocalePack = playlists[0].ensureLocalePack();
                }

                // only specify specializations if programType allows it, to match server
                let specializationLocalePackIds = [];
                if (Cohort.supportsSpecializations(programType)) {
                    const specializations = overrides.specializations || [
                        Playlist.fixtures.getInstance(),
                        Playlist.fixtures.getInstance(),
                    ];
                    delete overrides.specializations;

                    specializationLocalePackIds = specializations.map(playlist => {
                        playlist.ensureLocalePack();
                        return playlist.locale_pack.id;
                    });

                    // playlist should include all playlists, including specializations
                    playlists = playlists.concat(specializations);
                }

                const id = String(Math.random());

                return angular.extend(
                    {
                        id,
                        name: `My Cohort ${(counter += 1)}`,
                        notes: 'Some cohort notes',
                        specialization_playlist_pack_ids: specializationLocalePackIds,
                        num_required_specializations: specializationLocalePackIds.length,
                        created_at: 0,
                        updated_at: 0,
                        start_date: 0,
                        end_date: 0,
                        application_deadline: 0,
                        $$playlists: playlists,
                        program_type: programType,
                        title: `Cohort Title ${(counter += 1)}`,
                        description: 'Description',
                        foundations_playlist_locale_pack: foundationPlaylistLocalePack,
                        admission_rounds: [
                            AdmissionRound.new({
                                application_deadline_days_offset: -30,
                                decision_date_days_offset: -21,
                            }),
                            AdmissionRound.new({
                                application_deadline_days_offset: -15,
                                decision_date_days_offset: -6,
                            }),
                        ],
                        periods: [
                            Period.new({
                                title: 'standard 1',
                                style: 'standard',
                            }),
                            Period.new({
                                title: 'review 1',
                                style: 'review',
                            }),
                            Period.new({
                                title: 'exam 1',
                                style: 'exam',
                            }),
                        ],
                        enrollment_deadline_days_offset: 0,
                        graduation_days_offset_from_end: 10,
                        foundations_lesson_stream_locale_pack_ids: [],
                        slack_rooms: [
                            {
                                id: guid.generate(),
                                title: 'Room 1',
                                url: 'room1url',
                                admin_token: 'room1token',
                            },
                            {
                                id: guid.generate(),
                                title: 'Room 2',
                                url: 'room2url',
                                admin_token: 'room2token',
                            },
                        ],
                        playlist_collections: [
                            {
                                title: '',
                                required_playlist_pack_ids: requiredPlaylistPackIds,
                            },
                        ],
                    },
                    overrides,
                );
            },

            getInstance(overrides) {
                const cohort = Cohort.new(this.sampleAttrs(overrides));
                // a promise is created when caching this cohort.  flush it
                // now so it's not interfering with other attempts to deal
                // with timeouts in tests
                $injector.get('$timeout').flush();

                cohort.addSlackRoomAssignment = function (cohortApplications) {
                    addSlackRoomAssignment(this, cohortApplications);
                    return this;
                };

                return cohort;
            },
        };

        return {};
    },
]);
