import 'AngularSpecHelper';
import 'Cohorts/angularModule';
import moment from 'moment-timezone';

describe('Schedulable', () => {
    let $injector;
    let SchedulableItem;
    let Cohort;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Cohorts', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                const Schedulable = $injector.get('Schedulable');
                Cohort = $injector.get('Cohort');

                SchedulableItem = $injector.get('Iguana').subclass(function () {
                    this.include(Schedulable);

                    // need to make sure this is set before
                    // we hit to copyAttrsOnInitialize callback
                    let date = new Date();
                    Object.defineProperty(this.prototype, 'startDate', {
                        get() {
                            return date;
                        },
                        set(val) {
                            date = val;
                        },
                    });
                });
            },
        ]);
    });

    // When you add the first round, we default to -15 days from the start date of the cohort for the offset (i.e.: if the start date is Monday, it will be the Sunday two weeks before for the deadline)

    describe('after copyAttrsOnInitialize', () => {
        it('should set some defaults', () => {
            const schedulableItem = SchedulableItem.new();
            ['admission_rounds', 'learner_project_ids', 'id_verification_periods', 'playlist_collections'].forEach(
                property => {
                    expect(schedulableItem[property]).toEqual([]);
                },
            );
        });
    });

    describe('addAdmissionRound', () => {
        it('should set expected defaults for the first round created', () => {
            const schedulableItem = SchedulableItem.new();
            const admissionRound = schedulableItem.addAdmissionRound();
            expect(schedulableItem.admission_rounds.length).toBe(1);

            expect(admissionRound.application_deadline_days_offset).toEqual(-14);
            expect(admissionRound.decision_date_days_offset).toEqual(-5);
        });

        it('should set expected defaults for subsequent rounds created', () => {
            const schedulableItem = SchedulableItem.new({
                admission_rounds: [
                    {
                        application_deadline_days_offset: -10,
                        decision_date_days_offset: -10,
                    },
                ],
            });
            const admissionRound = schedulableItem.addAdmissionRound();
            expect(schedulableItem.admission_rounds.length).toBe(2);

            expect(admissionRound.application_deadline_days_offset).toEqual(-31);
            expect(admissionRound.decision_date_days_offset).toEqual(-22);
        });
    });

    describe('addIdVerificationPeriod', () => {
        it('should set expected defaults for the first period created', () => {
            const schedulableItem = SchedulableItem.new();
            const idVerificationPeriod = schedulableItem.addIdVerificationPeriod();
            expect(schedulableItem.id_verification_periods.length).toBe(1);

            expect(idVerificationPeriod.start_date_days_offset).toEqual(0);
            expect(idVerificationPeriod.due_date_days_offset).toEqual(7);
        });

        it('should set expected defaults for subsequent periods created', () => {
            const schedulableItem = SchedulableItem.new({
                id_verification_periods: [
                    {
                        start_date_days_offset: 0,
                        due_date_days_offset: 7,
                    },
                ],
            });
            const idVerificationPeriod = schedulableItem.addIdVerificationPeriod();
            expect(schedulableItem.id_verification_periods.length).toBe(2);

            expect(idVerificationPeriod.start_date_days_offset).toEqual(28);
            expect(idVerificationPeriod.due_date_days_offset).toEqual(35);
        });
    });

    describe('registrationDeadline', () => {
        let schedulableItem;
        beforeEach(() => {
            schedulableItem = SchedulableItem.new();
            schedulableItem.startDate = new Date('2017-01-30 12:00:00');
            schedulableItem.registration_deadline_days_offset = -10;
        });
        describe('getter', () => {
            it('should return cached value if nothing has changed', () => {
                const origValue = schedulableItem.registrationDeadline;
                expect(origValue).toEqual(moment('2017-01-20 12:00:00').toDate());
                expect(schedulableItem.registrationDeadline).toBe(origValue);
            });

            it('should return new value if startDate has changed', () => {
                const origValue = schedulableItem.registrationDeadline;
                expect(origValue).toEqual(moment('2017-01-20 12:00:00').toDate());
                schedulableItem.startDate = new Date('2017-01-31 12:00:00');
                expect(schedulableItem.registrationDeadline).toEqual(moment('2017-01-21 12:00:00').toDate());
            });

            it('should return new value if registration_deadline_days_offset has changed', () => {
                const origValue = schedulableItem.registrationDeadline;
                expect(origValue).toEqual(moment('2017-01-20 12:00:00').toDate());
                schedulableItem.registration_deadline_days_offset = -20;
                expect(schedulableItem.registrationDeadline).toEqual(moment('2017-01-10 12:00:00').toDate());
            });
        });
        describe('setter', () => {
            it('should set registration_deadline_days_offset', () => {
                schedulableItem.registrationDeadline = moment('2017-01-10 12:00:00');
                expect(schedulableItem.registration_deadline_days_offset).toBe(-20);
            });

            it('should allow for an offset greater than 0', () => {
                schedulableItem.registrationDeadline = moment('2017-01-31 12:00:00');
                expect(schedulableItem.registration_deadline_days_offset).toBe(1);
            });
        });
        it('should be set on initialize only when supportsRegistrationDeadline is true', () => {
            // Just to make sure these do not interfere (had a copy / paste bug at one point)
            Object.defineProperty(SchedulableItem.prototype, 'supportsEnrollmentDeadline', {
                value: true,
            });

            Object.defineProperty(SchedulableItem.prototype, 'supportsRegistrationDeadline', {
                value: true,
            });
            schedulableItem = SchedulableItem.new({});
            expect(schedulableItem.registrationDeadline).toEqual(
                moment(schedulableItem.startDate).subtract(7, 'days').toDate(),
            );

            Object.defineProperty(SchedulableItem.prototype, 'supportsRegistrationDeadline', {
                value: false,
            });
            schedulableItem = SchedulableItem.new();
            expect(schedulableItem.registrationDeadline).toEqual(null);
        });
        it('should be set when changing program type only when supportsRegistrationDeadline is true', () => {
            schedulableItem = SchedulableItem.new({
                program_type: 'emba',
            });
            expect(schedulableItem.registrationDeadline).toEqual(
                moment(schedulableItem.startDate).subtract(7, 'days').toDate(),
            );
            schedulableItem.program_type = 'mba';
            expect(schedulableItem.registrationDeadline).toEqual(null);
            schedulableItem.program_type = 'emba';
            expect(schedulableItem.registrationDeadline).toEqual(
                moment(schedulableItem.startDate).subtract(7, 'days').toDate(),
            );
        });
    });

    describe('earlyrRegistrationDeadline', () => {
        let schedulableItem;
        beforeEach(() => {
            schedulableItem = SchedulableItem.new();
            schedulableItem.startDate = new Date('2017-01-30 12:00:00');
            schedulableItem.early_registration_deadline_days_offset = -15;
        });
        describe('getter', () => {
            it('should return cached value if nothing has changed', () => {
                const origValue = schedulableItem.earlyRegistrationDeadline;
                expect(origValue).toEqual(moment('2017-01-15 12:00:00').toDate());
                expect(schedulableItem.earlyRegistrationDeadline).toBe(origValue);
            });

            it('should return new value if startDate has changed', () => {
                const origValue = schedulableItem.earlyRegistrationDeadline;
                expect(origValue).toEqual(moment('2017-01-15 12:00:00').toDate());
                schedulableItem.startDate = new Date('2017-01-31 12:00:00');
                expect(schedulableItem.earlyRegistrationDeadline).toEqual(moment('2017-01-16 12:00:00').toDate());
            });

            it('should return new value if early_registration_deadline_days_offset has changed', () => {
                const origValue = schedulableItem.earlyRegistrationDeadline;
                expect(origValue).toEqual(moment('2017-01-15 12:00:00').toDate());
                schedulableItem.early_registration_deadline_days_offset = -20;
                expect(schedulableItem.earlyRegistrationDeadline).toEqual(moment('2017-01-10 12:00:00').toDate());
            });
        });
        describe('setter', () => {
            it('should set early_registration_deadline_days_offset', () => {
                schedulableItem.earlyRegistrationDeadline = moment('2017-01-10 12:00:00');
                expect(schedulableItem.early_registration_deadline_days_offset).toBe(-20);
            });

            it('should allow for an offset greater than 0', () => {
                schedulableItem.earlyRegistrationDeadline = moment('2017-01-31 12:00:00');
                expect(schedulableItem.early_registration_deadline_days_offset).toBe(1);
            });
        });
        it('should be set on initialize only when supportsEarlyRegistrationDeadline is true', () => {
            // Just to make sure these do not interfere (had a copy / paste bug at one point)
            Object.defineProperty(SchedulableItem.prototype, 'supportsEnrollmentDeadline', {
                value: true,
            });

            Object.defineProperty(SchedulableItem.prototype, 'supportsEarlyRegistrationDeadline', {
                value: true,
            });
            schedulableItem = SchedulableItem.new({});
            expect(schedulableItem.earlyRegistrationDeadline).toEqual(
                moment(schedulableItem.startDate).subtract(12, 'days').toDate(),
            );

            Object.defineProperty(SchedulableItem.prototype, 'supportsEarlyRegistrationDeadline', {
                value: false,
            });
            schedulableItem = SchedulableItem.new();
            expect(schedulableItem.earlyRegistrationDeadline).toEqual(null);
        });
        it('should be set when changing program type only when supportsEarlyRegistrationDeadline is true', () => {
            schedulableItem = SchedulableItem.new({
                program_type: 'emba',
            });
            expect(schedulableItem.earlyRegistrationDeadline).toEqual(
                moment(schedulableItem.startDate).subtract(12, 'days').toDate(),
            );
            schedulableItem.program_type = 'mba';
            expect(schedulableItem.earlyRegistrationDeadline).toEqual(null);
            schedulableItem.program_type = 'emba';
            expect(schedulableItem.earlyRegistrationDeadline).toEqual(
                moment(schedulableItem.startDate).subtract(12, 'days').toDate(),
            );
        });
    });

    describe('enrollmentDeadline', () => {
        let schedulableItem;
        beforeEach(() => {
            schedulableItem = SchedulableItem.new();
            schedulableItem.startDate = new Date('2017-01-10 12:00:00');
            schedulableItem.enrollment_deadline_days_offset = 10;
        });
        describe('getter', () => {
            it('should return cached value if nothing has changed', () => {
                const origValue = schedulableItem.enrollmentDeadline;
                expect(origValue).toEqual(moment('2017-01-20 12:00:00').toDate());
                expect(schedulableItem.enrollmentDeadline).toBe(origValue);
            });

            it('should return new value if startDate has changed', () => {
                const origValue = schedulableItem.enrollmentDeadline;
                expect(origValue).toEqual(moment('2017-01-20 12:00:00').toDate());
                schedulableItem.startDate = new Date('2017-01-11 12:00:00');
                expect(schedulableItem.enrollmentDeadline).toEqual(moment('2017-01-21 12:00:00').toDate());
            });

            it('should return new value if enrollment_deadline_days_offset has changed', () => {
                const origValue = schedulableItem.enrollmentDeadline;
                expect(origValue).toEqual(moment('2017-01-20 12:00:00').toDate());
                schedulableItem.enrollment_deadline_days_offset = 20;
                expect(schedulableItem.enrollmentDeadline).toEqual(moment('2017-01-30 12:00:00').toDate());
            });
        });
        describe('setter', () => {
            it('should set registration_deadline_days_offset', () => {
                schedulableItem.enrollmentDeadline = moment('2017-01-30 12:00:00');
                expect(schedulableItem.enrollment_deadline_days_offset).toBe(20);
            });

            it('should allow for an offset greater than 0', () => {
                schedulableItem.enrollmentDeadline = moment('2017-01-11 12:00:00');
                expect(schedulableItem.enrollment_deadline_days_offset).toBe(1);
            });
        });
        it('should be set on initialize only when supportsEnrollmentDeadline is true', () => {
            // Just to make sure these do not interfere (had a copy / paste bug at one point)
            Object.defineProperty(SchedulableItem.prototype, 'supportsRecurringPayments', {
                value: true,
            });

            Object.defineProperty(SchedulableItem.prototype, 'supportsEnrollmentDeadline', {
                value: true,
            });
            schedulableItem = SchedulableItem.new({});
            expect(schedulableItem.enrollmentDeadline).toEqual(
                moment(schedulableItem.startDate).add(21, 'days').toDate(),
            );

            Object.defineProperty(SchedulableItem.prototype, 'supportsEnrollmentDeadline', {
                value: false,
            });
            schedulableItem = SchedulableItem.new();
            expect(schedulableItem.enrollmentDeadline).toEqual(schedulableItem.startDate);
        });
    });

    describe('diplomaGeneration', () => {
        let schedulableItem;
        let endDate;
        beforeEach(() => {
            schedulableItem = SchedulableItem.new();
            schedulableItem.startDate = new Date('2016-06-10 12:00:00');
            jest.spyOn(schedulableItem, 'endDate', 'get').mockImplementation(() => endDate);
            endDate = new Date('2017-01-10 12:00:00');
            schedulableItem.diploma_generation_days_offset_from_end = 10;
        });
        describe('getter', () => {
            it('should return cached value if nothing has changed', () => {
                const origValue = schedulableItem.diplomaGeneration;
                expect(origValue).toEqual(moment('2017-01-20 12:00:00').toDate());
                expect(schedulableItem.diplomaGeneration).toBe(origValue);
            });

            it('should return new value if startDate has changed', () => {
                const origValue = schedulableItem.diplomaGeneration;
                expect(origValue).toEqual(moment('2017-01-20 12:00:00').toDate());
                endDate = new Date('2017-01-11 12:00:00');
                expect(schedulableItem.diplomaGeneration).toEqual(moment('2017-01-21 12:00:00').toDate());
            });

            it('should return new value if diploma_generation_days_offset_from_end has changed', () => {
                const origValue = schedulableItem.diplomaGeneration;
                expect(origValue).toEqual(moment('2017-01-20 12:00:00').toDate());
                schedulableItem.diploma_generation_days_offset_from_end = 20;
                expect(schedulableItem.diplomaGeneration).toEqual(moment('2017-01-30 12:00:00').toDate());
            });
        });
        describe('setter', () => {
            it('should set registration_deadline_days_offset', () => {
                schedulableItem.diplomaGeneration = moment('2017-01-30 12:00:00');
                expect(schedulableItem.diploma_generation_days_offset_from_end).toBe(20);
            });

            it('should allow for an offset greater than 0', () => {
                schedulableItem.diplomaGeneration = moment('2017-01-11 12:00:00');
                expect(schedulableItem.diploma_generation_days_offset_from_end).toBe(1);
            });
        });
    });

    describe('graduationDate', () => {
        let schedulableItem;
        let endDate;
        beforeEach(() => {
            schedulableItem = SchedulableItem.new();
            schedulableItem.startDate = new Date('2016-06-10 12:00:00');
            jest.spyOn(schedulableItem, 'endDate', 'get').mockImplementation(() => endDate);
            endDate = new Date('2017-01-10 12:00:00');
            schedulableItem.graduation_days_offset_from_end = 10;
        });
        describe('getter', () => {
            it('should return cached value if nothing has changed', () => {
                const origValue = schedulableItem.graduationDate;
                expect(origValue).toEqual(moment('2017-01-20 12:00:00').toDate());
                expect(schedulableItem.graduationDate).toBe(origValue);
            });

            it('should return new value if endDate has changed', () => {
                const origValue = schedulableItem.graduationDate;
                expect(origValue).toEqual(moment('2017-01-20 12:00:00').toDate());
                endDate = new Date('2017-01-11 12:00:00');
                expect(schedulableItem.graduationDate).toEqual(moment('2017-01-21 12:00:00').toDate());
            });

            it('should return new value if graduation_days_offset_from_end has changed', () => {
                const origValue = schedulableItem.graduationDate;
                expect(origValue).toEqual(moment('2017-01-20 12:00:00').toDate());
                schedulableItem.graduation_days_offset_from_end = 20;
                expect(schedulableItem.graduationDate).toEqual(moment('2017-01-30 12:00:00').toDate());
            });
        });
        describe('setter', () => {
            it('should set graduation_days_offset_from_end', () => {
                schedulableItem.graduationDate = moment('2017-01-30 12:00:00');
                expect(schedulableItem.graduation_days_offset_from_end).toBe(20);
            });
        });
    });

    describe('promotableProgramTypes', () => {
        it('should work', () => {
            const expected = _.union(['career_network_only'], Cohort.CERTIFICATE_PROGRAM_TYPES);
            expect(_.pluck(SchedulableItem.promotableProgramTypes, 'key')).toEqual(expected);
        });

        it('should set isPaidCert for paid certs', () => {
            expect(
                SchedulableItem.new({
                    program_type: 'mba',
                }).isPaidCert,
            ).toBe(false);

            expect(
                SchedulableItem.new({
                    program_type: 'paid_cert_data_analytics',
                }).isPaidCert,
            ).toBe(true);
        });
    });

    describe('calendarProgramTypes', () => {
        it('should work', () => {
            expect(_.pluck(SchedulableItem.calendarProgramTypes, 'key')).toEqual(['mba', 'emba']);
        });
    });

    describe('endDate', () => {
        it('should computeEndDate properly', () => {
            const Period = $injector.get('Period');
            const periods = [
                Period.new({
                    days: 1,
                    days_offset: 1,
                }),
                Period.new({
                    days: 2,
                    days_offset: 2,
                }),
            ];

            expect(moment(1).add(6, 'days').isSame(SchedulableItem.prototype.computeEndDate(1, periods))).toBe(true);
        });
    });

    describe('periodForRequiredStream', () => {
        it('should work', () => {
            const schedulableItem = SchedulableItem.new({
                periods: [
                    {
                        stream_entries: [
                            {
                                required: false,
                                locale_pack_id: 'non_required',
                            },
                        ],
                    },
                    {
                        stream_entries: [
                            {
                                required: true,
                                locale_pack_id: 'required',
                            },
                        ],
                    },
                ],
            });
            // expect(schedulableItem.periodForRequiredStream('none')).toBeNull();
            // expect(schedulableItem.periodForRequiredStream('non_required')).toBeNull();
            expect(schedulableItem.periodForRequiredStream('required')).toBe(schedulableItem.periods[1]);
        });
    });

    describe('titleWithoutThe', () => {
        it('should work', () => {
            expect(
                SchedulableItem.new({
                    title: 'the something',
                }).titleWithoutThe,
            ).toEqual('something');
            expect(
                SchedulableItem.new({
                    title: 'The something',
                }).titleWithoutThe,
            ).toEqual('something');
            expect(
                SchedulableItem.new({
                    title: 'something',
                }).titleWithoutThe,
            ).toEqual('something');
            expect(
                SchedulableItem.new({
                    title: 'thereapeutic painting',
                }).titleWithoutThe,
            ).toEqual('thereapeutic painting');
        });
    });

    // This is temporary.  See https://trello.com/c/Vl9Nix5w
    describe('supportsEarlyDecisionConsideration', () => {
        it('should work', () => {
            expect(
                Cohort.new({
                    program_type: 'career_network_only',
                }).supportsEarlyDecisionConsideration,
            ).toEqual(false);

            expect(
                Cohort.new({
                    program_type: 'emba',
                    start_date: new Date('2018/12/01').getTime() / 1000,
                }).supportsEarlyDecisionConsideration,
            ).toEqual(true);

            expect(
                Cohort.new({
                    program_type: 'emba',
                    start_date: new Date('2019/1/12').getTime() / 1000,
                }).supportsEarlyDecisionConsideration,
            ).toEqual(false);
        });
    });

    describe('requiredPlaylistPackIds', () => {
        it('should derive locale pack ids from the required_playlist_pack_ids in playlist_collections if there are any playlist_collections', () => {
            const schedulableItem = SchedulableItem.new({
                playlist_collections: [
                    {
                        required_playlist_pack_ids: ['foo'],
                    },
                    {
                        required_playlist_pack_ids: ['bar'],
                    },
                    {
                        required_playlist_pack_ids: ['baz'],
                    },
                ],
            });
            expect(_.any(schedulableItem.playlist_collections)).toBe(true);
            expect(schedulableItem.requiredPlaylistPackIds).toEqual(['foo', 'bar', 'baz']);
        });
    });

    describe('newPlaylistCollection', () => {
        let schedulableItem;

        beforeEach(() => {
            schedulableItem = SchedulableItem.new();
        });

        it('should return an object with an empty title and an empty array of required_playlist_pack_ids if no passed in attrs', () => {
            const newPlaylistCollection = schedulableItem.newPlaylistCollection();
            expect(newPlaylistCollection).toEqual({
                title: '',
                required_playlist_pack_ids: [],
            });
        });

        it('should respect pased in attrs', () => {
            const attrs = {
                title: 'Foo',
                required_playlist_pack_ids: ['bar', 'baz', 'qux'],
            };
            const newPlaylistCollection = schedulableItem.newPlaylistCollection(attrs);
            expect(newPlaylistCollection).toEqual(attrs);
        });
    });

    describe('addPlaylistCollection', () => {
        it('should push a newPlaylistCollection onto playlist_collections, passing along any attrs to the newPlaylistCollection', () => {
            const schedulableItem = SchedulableItem.new();
            const attrs = {
                foo: 'foo',
            };
            const newPlaylistCollection = {
                bar: 'bar', // We just need to make sure that something is returned and the contents of the return value don't matter
            };
            const originalNumPlaylistCollections = schedulableItem.playlist_collections.length;
            jest.spyOn(schedulableItem, 'newPlaylistCollection').mockReturnValue(newPlaylistCollection);
            jest.spyOn(schedulableItem.playlist_collections, 'push');

            schedulableItem.addPlaylistCollection(attrs);

            expect(schedulableItem.newPlaylistCollection).toHaveBeenCalledWith(attrs);
            expect(schedulableItem.playlist_collections.push).toHaveBeenCalledWith(newPlaylistCollection);
            expect(schedulableItem.playlist_collections.length).toEqual(originalNumPlaylistCollections + 1);
            expect(_.last(schedulableItem.playlist_collections)).toBe(newPlaylistCollection);
        });
    });

    describe('removePlaylistCollection', () => {
        let schedulableItem;

        beforeEach(() => {
            schedulableItem = SchedulableItem.new();
            schedulableItem.playlist_collections = [
                {
                    foo: 'foo',
                },
                {
                    bar: 'bar',
                },
                {
                    baz: 'baz',
                },
            ];
        });

        it('should remove the passed in playlistCollection from playlist_collections', () => {
            const playlistCollectionToRemove = _.first(schedulableItem.playlist_collections);

            schedulableItem.removePlaylistCollection(playlistCollectionToRemove);

            expect(schedulableItem.playlist_collections.length).toEqual(2);
            expect(
                schedulableItem.playlist_collections.find(
                    playlistCollection => playlistCollection === playlistCollectionToRemove,
                ),
            ).toBeUndefined();
        });

        it('should throw an error if no playlistCollection is passed in', () => {
            expect(() => {
                schedulableItem.removePlaylistCollection();
            }).toThrow(new Error('Must specify a playlistCollection'));
        });
    });
});
