import 'AngularSpecHelper';
import 'Cohorts/angularModule';
import moment from 'moment-timezone';

describe('AdmissionRound', () => {
    let $injector;
    let AdmissionRound;
    let instance;
    let schedulableItem;
    let ngToast;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Cohorts', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                AdmissionRound = $injector.get('AdmissionRound');
                ngToast = $injector.get('ngToast');
                const Iguana = $injector.get('Iguana');
                const Schedulable = $injector.get('Schedulable');

                const SchedulableItem = Iguana.subclass(function () {
                    this.include(Schedulable);
                });

                schedulableItem = SchedulableItem.new({
                    startDate: moment('2017-01-30 12:00:00').toDate(),
                });

                instance = AdmissionRound.new({
                    application_deadline_days_offset: -15,
                    decision_date_days_offset: -15,
                });
                instance.$$embeddedIn = schedulableItem;

                jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            },
        ]);
    });

    describe('applicationDeadline', () => {
        describe('getter', () => {
            it('should return cached value if nothing has changed', () => {
                const origValue = instance.applicationDeadline;
                expect(origValue).toEqual(moment('2017-01-15 12:00:00').toDate());
                expect(instance.applicationDeadline).toBe(origValue);
            });

            it('should return new value if startDate has changed', () => {
                const origValue = instance.applicationDeadline;
                expect(origValue).toEqual(moment('2017-01-15 12:00:00').toDate());
                schedulableItem.startDate = moment('2017-01-31 12:00:00').toDate();
                expect(instance.applicationDeadline).toEqual(moment('2017-01-16 12:00:00').toDate());
            });

            it('should return new value if application_deadline_days_offset has changed', () => {
                const origValue = instance.applicationDeadline;
                expect(origValue).toEqual(moment('2017-01-15 12:00:00').toDate());
                instance.application_deadline_days_offset = -10;
                expect(instance.applicationDeadline).toEqual(moment('2017-01-20 12:00:00').toDate());
            });
        });
        describe('setter', () => {
            it('should set application_deadline_days_offset', () => {
                instance.applicationDeadline = moment('2017-01-10 12:00:00');
                expect(instance.application_deadline_days_offset).toBe(-20);
            });

            it('should not allow for a date after the decisionDate', () => {
                const origValue = instance.applicationDeadline;
                instance.applicationDeadline = moment(instance.decisionDate).add(1, 'day').toDate();
                expect(instance.applicationDeadline).toEqual(origValue);
                expect(ngToast.create).toHaveBeenCalledWith({
                    content: 'Application deadline must be before decision date',
                    className: 'warning',
                });
            });
        });
    });

    describe('decisionDate', () => {
        describe('getter', () => {
            it('should return cached value if nothing has changed', () => {
                const origValue = instance.decisionDate;
                expect(origValue).toEqual(moment('2017-01-15 12:00:00').toDate());
                expect(instance.decisionDate).toBe(origValue);
            });

            it('should return new value if startDate has changed', () => {
                const origValue = instance.decisionDate;
                expect(origValue).toEqual(moment('2017-01-15 12:00:00').toDate());
                schedulableItem.startDate = moment('2017-01-31 12:00:00').toDate();
                expect(instance.decisionDate).toEqual(moment('2017-01-16 12:00:00').toDate());
            });

            it('should return new value if decision_date_days_offset has changed', () => {
                const origValue = instance.decisionDate;
                expect(origValue).toEqual(moment('2017-01-15 12:00:00').toDate());
                instance.decision_date_days_offset = -10;
                expect(instance.decisionDate).toEqual(moment('2017-01-20 12:00:00').toDate());
            });
        });
        describe('setter', () => {
            it('should set decision_date_days_offset', () => {
                instance.decisionDate = moment('2017-01-28 12:00:00');
                expect(instance.decision_date_days_offset).toBe(-2);
            });

            it('should not allow for an offset greater than 0', () => {
                const origValue = instance.decisionDate;
                instance.decisionDate = moment(schedulableItem.startDate).add(1, 'day').toDate();
                expect(instance.decisionDate).toEqual(origValue);
                expect(ngToast.create).toHaveBeenCalledWith({
                    content: 'Decision date must be before schedule start date',
                    className: 'warning',
                });
            });

            it('should not allow for a date before the applicationDate', () => {
                const origValue = instance.decisionDate;
                instance.decisionDate = moment(instance.applicationDeadline).subtract(1, 'day').toDate();
                expect(instance.decisionDate).toEqual(origValue);
                expect(ngToast.create).toHaveBeenCalledWith({
                    content: 'Decision date must be after application deadline',
                    className: 'warning',
                });
            });
        });
    });
});
