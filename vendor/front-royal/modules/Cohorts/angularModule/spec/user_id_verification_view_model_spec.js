import 'AngularSpecHelper';
import 'Cohorts/angularModule';
import moment from 'moment-timezone';

// A lot of the functionality in this class is actually tested in userIdVerificationForm
describe('FrontRoyal.Cohorts.UserIdVerificationViewModel', () => {
    let $injector;
    let SpecHelper;
    let UserIdVerificationViewModel;
    let currentUser;
    let ClientStorage;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Cohorts', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                ClientStorage = $injector.get('ClientStorage');
                UserIdVerificationViewModel = $injector.get('UserIdVerificationViewModel');
                currentUser = SpecHelper.stubCurrentUser();
            },
        ]);
    });

    afterEach(() => {
        ClientStorage.removeItem('launchVerificationModalIfLastIdologyVerificationFailed.42');
    });

    describe('launchVerificationModalIfLastIdologyVerificationFailed', () => {
        beforeEach(() => {
            jest.spyOn(currentUser, 'unverifiedForCurrentIdVerificationPeriod', 'get').mockReturnValue(true);
        });

        it('should not launch if no lastIdologyVerification', () => {
            currentUser.idology_verifications = [];
            const instance = new UserIdVerificationViewModel();
            jest.spyOn(instance, 'launchVerificationModal').mockImplementation(() => {});
            instance.launchVerificationModalIfLastIdologyVerificationFailed();
            expect(instance.launchVerificationModal).not.toHaveBeenCalled();
        });

        it('should not launch if lastIdologyVerification is not failed', () => {
            currentUser.idology_verifications = [{}];
            const instance = new UserIdVerificationViewModel();
            jest.spyOn(instance, 'launchVerificationModal').mockImplementation(() => {});
            instance.launchVerificationModalIfLastIdologyVerificationFailed();
            expect(instance.launchVerificationModal).not.toHaveBeenCalled();
        });

        it('should launch if there is a failed verification, but only once', () => {
            currentUser.idology_verifications = [
                {
                    response_received_at: moment().subtract(5, 'minutes').toDate().getTime() / 1000,
                    verified: false,
                    idology_id_number: 42,
                },
            ];
            const instance = new UserIdVerificationViewModel();
            jest.spyOn(instance, 'launchVerificationModal').mockImplementation(() => {});
            instance.launchVerificationModalIfLastIdologyVerificationFailed();
            expect(instance.launchVerificationModal).toHaveBeenCalled();
            expect(instance.step).toEqual('failed');

            const anotherInstance = new UserIdVerificationViewModel();
            jest.spyOn(anotherInstance, 'launchVerificationModal').mockImplementation(() => {});
            anotherInstance.launchVerificationModalIfLastIdologyVerificationFailed();
            expect(anotherInstance.launchVerificationModal).not.toHaveBeenCalled();
        });
    });
});
