import moment from 'moment-timezone';
import 'AngularSpecHelper';
import 'Cohorts/angularModule';

describe('PeriodAction', () => {
    let $injector;
    let Cohort;
    let Period;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Cohorts', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                Cohort = $injector.get('Cohort');
                Period = $injector.get('Period');
            },
        ]);
    });

    describe('date', () => {
        let cohort;

        beforeEach(() => {
            const period = Period.new();
            period.addAction();

            cohort = Cohort.new({
                periods: [period],
            });
        });

        // Our code always acts on these dates as though we're in Eastern time,
        // so our expectations in this spec have to do the same.
        function easternTime(date) {
            return moment.tz(date, 'America/New_York');
        }

        describe('setter', () => {
            let period;
            let action;

            beforeEach(() => {
                period = cohort.periods[0];
                action = period.actions[0];
            });

            it('should set the days_offset_from_end correctly if before the period endDate', () => {
                // Subtract five days from the period endDate and expect to see -5 as the offset
                let selectedDate = easternTime(period.endDate).subtract(5, 'days');
                action.date = selectedDate.toDate();
                expect(action.days_offset_from_end).toBe(-5);

                // Subtract one day from the period endDate and expect to see -5 as the offset
                selectedDate = easternTime(period.endDate).subtract(1, 'days');
                action.date = selectedDate.toDate();
                expect(action.days_offset_from_end).toBe(-1);
            });

            it('should set the days_offset_from_end correctly if after the period endDate', () => {
                // Add five days from the period endDate and expect to see 5 as the offset
                let selectedDate = easternTime(period.endDate).add(5, 'days');
                action.date = selectedDate.toDate();
                expect(action.days_offset_from_end).toBe(5);

                // Add one day from the period endDate and expect to see 1 as the offset
                selectedDate = easternTime(period.endDate).add(1, 'days');
                action.date = selectedDate.toDate();
                expect(action.days_offset_from_end).toBe(1);
            });

            it('should set the days_offset_from_end correctly if on the period endDate', () => {
                const selectedDate = easternTime(period.endDate);
                action.date = selectedDate.toDate();
                expect(action.days_offset_from_end).toBe(-0);
            });
        });

        describe('getter', () => {
            let period;
            let action;

            beforeEach(() => {
                period = cohort.periods[0];
                action = period.actions[0];
            });

            it('should return the proper date based on days_offset_from_end if before the period endDate', () => {
                action.days_offset_from_end = -5;
                expect(easternTime(action.date).toDate()).toEqual(
                    easternTime(period.endDate).subtract(5, 'days').toDate(),
                );
            });

            it('should return the proper date based on days_offset_from_end if after the period endDate', () => {
                action.days_offset_from_end = 5;
                expect(easternTime(action.date).toDate()).toEqual(easternTime(period.endDate).add(5, 'days').toDate());
            });

            it('should return the proper date based on days_offset_from_end if on the period endDate', () => {
                action.days_offset_from_end = 0;
                expect(easternTime(action.date).toDate()).toEqual(easternTime(period.endDate).toDate());
            });
        });
    });
});
