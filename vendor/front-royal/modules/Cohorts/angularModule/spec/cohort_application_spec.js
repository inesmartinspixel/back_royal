import moment from 'moment-timezone';
import 'AngularSpecHelper';
import 'Cohorts/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Users/angularModule/spec/_mock/fixtures/users';

describe('CohortApplication', () => {
    let $injector;
    let CohortApplication;
    let Cohort;
    let cohort_application;
    let promotedCohort;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Cohorts', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                CohortApplication = $injector.get('CohortApplication');
                Cohort = $injector.get('Cohort');

                $injector.get('CohortApplicationFixtures');
                $injector.get('CohortFixtures');

                cohort_application = CohortApplication.fixtures.getInstance();
                promotedCohort = Cohort.fixtures.getInstance();
            },
        ]);
    });

    describe('graduatedAt', () => {
        it('should return undefined if no graduated_at', () => {
            cohort_application.graduated_at = undefined;
            expect(cohort_application.graduatedAt).toBeUndefined();
        });

        it('should return a date object if graduated_at', () => {
            cohort_application.graduated_at = 1539809697883;
            expect(cohort_application.graduatedAt).toEqual(new Date(cohort_application.graduated_at * 1000));
        });
    });

    describe('canReapplyTo', () => {
        it('should be false if no passed in promotedCohort', () => {
            expect(cohort_application.canReapplyTo()).toBe(false);
        });

        it('should be false if not rejected/expelled', () => {
            cohort_application.status = 'foo';
            expect(cohort_application.canReapplyTo(promotedCohort)).toBe(false);
        });

        describe('when rejected/expelled', () => {
            ['rejected', 'expelled'].forEach(status => {
                beforeEach(() => {
                    cohort_application.status = status;
                });

                describe('when cohort supportsAdmissionRounds', () => {
                    const slightlyMoreThanTwoDaysAgoInc = 49 * 60 * 60 * -1;
                    const slightlyLessThanTwoDaysAgoInc = 47 * 60 * 60 * -1;
                    const aFortNightInTheFuture = 14 * 24 * 60 * 60;

                    beforeEach(() => {
                        jest.spyOn(Cohort, 'supportsAdmissionRounds').mockReturnValue(true);
                    });

                    it('should be true if now is more than two days after decisionDate', () => {
                        cohort_application.decision_date = new Date().getTime() / 1000 + slightlyMoreThanTwoDaysAgoInc;
                        expect(cohort_application.canReapplyTo(promotedCohort)).toBe(true);
                    });

                    it('should be false if now is less than two days after decisionDate', () => {
                        cohort_application.decision_date = new Date().getTime() / 1000 + slightlyLessThanTwoDaysAgoInc;
                        expect(cohort_application.canReapplyTo(promotedCohort)).toBe(false);
                    });

                    it('should be false if now is before decisionDate', () => {
                        cohort_application.decision_date = new Date().getTime() / 1000 + aFortNightInTheFuture;
                        expect(cohort_application.canReapplyTo(promotedCohort)).toBe(false);
                    });

                    it('should be true if no decisionDate', () => {
                        cohort_application.decision_date = null;
                        expect(cohort_application.canReapplyTo(promotedCohort)).toBe(true);
                    });
                });

                it('should be true if the cohort that was applied to supports reapply', () => {
                    jest.spyOn(Cohort, 'supportsReapply').mockImplementation(programType => programType === 'foobar');
                    cohort_application.program_type = 'foobar';
                    expect(cohort_application.canReapplyTo(promotedCohort)).toBe(true);
                });

                it('should be true if the promoted cohort supports reapply', () => {
                    jest.spyOn(Cohort, 'supportsReapply').mockImplementation(
                        programType => programType === promotedCohort.program_type,
                    );
                    expect(cohort_application.canReapplyTo(promotedCohort)).toBe(true);
                });
            });
        });
    });

    describe('getPlanTuitionActualTotal', () => {
        let plan;
        let planAmount;

        beforeEach(() => {
            planAmount = 100;
            plan = {
                id: 'planId',
                amount: planAmount * 100,
            };
        });

        it('should work with scholarship_level', () => {
            const amountOff = 50;
            cohort_application.scholarship_level = {
                standard: {
                    planId: {
                        amount_off: amountOff * 100,
                    },
                },
            };

            withSpies(
                {
                    supportsScholarshipLevels: true,
                    numIntervals: 12,
                    earlyRegistrationEnabled: false,
                },
                () => {
                    expect(cohort_application.getPlanTuitionActualTotal(plan)).toEqual(
                        planAmount * 12 - amountOff * 12,
                    );
                },
            );
        });

        it('should work with amount off coupon', () => {
            withSpies(
                {
                    supportsScholarshipLevels: false,
                    numIntervals: 12,
                    earlyRegistrationEnabled: false,
                },
                _plan => {
                    const amountOff = 50;
                    const coupon = {
                        amount_off: amountOff * 100,
                    };
                    expect(cohort_application.getPlanTuitionActualTotal(_plan, null, coupon)).toEqual(
                        planAmount * 12 - amountOff * 12,
                    );
                },
            );
        });

        it('should work with no scholarship and no coupon', () => {
            withSpies(
                {
                    supportsScholarshipLevels: false,
                    numIntervals: 12,
                    earlyRegistrationEnabled: false,
                },
                _plan => {
                    expect(cohort_application.getPlanTuitionActualTotal(_plan)).toEqual(planAmount * 12);
                },
            );
        });

        it('should work with percent off coupon', () => {
            withSpies(
                {
                    supportsScholarshipLevels: false,
                    numIntervals: 12,
                    earlyRegistrationEnabled: false,
                },
                _plan => {
                    const percentOff = 0.5;
                    const coupon = {
                        percent_off: percentOff * 100,
                    };
                    expect(cohort_application.getPlanTuitionActualTotal(_plan, null, coupon)).toEqual(
                        planAmount * 12 * percentOff,
                    );
                },
            );
        });

        it('should work with early registration', () => {
            const amountOff = 50;
            cohort_application.scholarship_level = {
                early: {
                    planId: {
                        amount_off: amountOff * 100,
                    },
                },
            };

            withSpies(
                {
                    supportsScholarshipLevels: true,
                    numIntervals: 12,
                    earlyRegistrationEnabled: true,
                },
                () => {
                    expect(cohort_application.getPlanTuitionActualTotal(plan)).toEqual(
                        planAmount * 12 - amountOff * 12,
                    );
                },
            );
        });

        function withSpies(opts, fn) {
            jest.spyOn(cohort_application, 'getNumIntervalsForPlan').mockReturnValue(opts.numIntervals);
            jest.spyOn(cohort_application, 'earlyRegistrationEnabled', 'get').mockReturnValue(
                opts.earlyRegistrationEnabled,
            );
            jest.spyOn(Cohort, 'supportsScholarshipLevels').mockReturnValue(opts.supportsScholarshipLevels);

            fn(plan);
            expect(Cohort.supportsScholarshipLevels).toHaveBeenCalledWith(cohort_application.program_type);
            expect(cohort_application.getNumIntervalsForPlan).toHaveBeenCalledWith(plan);
        }
    });

    describe('promisedPaymentOutsideOfStripe', () => {
        it('should be true with no scholarship, with a recurring payment plan, and num_charged_payments = 0', () => {
            withSpies(
                {
                    supportsRecurringPayments: true,
                    hasFullScholarship: false,
                    numChargedPayments: 0,
                    totalNumPayments: 0,
                },
                () => {
                    expect(cohort_application.promisedPaymentOutsideOfStripe).toBe(true);
                },
            );
        });

        it('should be true with no scholarship, with a recurring payment plan, and num_charged_payments = 0', () => {
            withSpies(
                {
                    supportsOnlyOneTimePayment: true,
                    hasFullScholarship: false,
                    numChargedPayments: 0,
                    totalNumPayments: 0,
                },
                () => {
                    expect(cohort_application.promisedPaymentOutsideOfStripe).toBe(true);
                },
            );
        });

        it('should be false with a scholarship', () => {
            withSpies(
                {
                    supportsRecurringPayments: true,
                    hasFullScholarship: true,
                    numChargedPayments: 0,
                    totalNumPayments: 0,
                },
                () => {
                    expect(cohort_application.promisedPaymentOutsideOfStripe).toBe(false);
                },
            );
        });

        it('should be false with a payment plan', () => {
            withSpies(
                {
                    hasFullScholarship: false,
                    numChargedPayments: 0,
                    totalNumPayments: 0,
                },
                () => {
                    expect(cohort_application.promisedPaymentOutsideOfStripe).toBe(false);
                },
            );
        });

        it('should be false with previous payments', () => {
            withSpies(
                {
                    supportsOnlyOneTimePayment: true,
                    hasFullScholarship: false,
                    numChargedPayments: 1,
                    totalNumPayments: 0,
                },
                () => {
                    expect(cohort_application.promisedPaymentOutsideOfStripe).toBe(false);
                },
            );
        });

        it('should be false with non-zero total_num_required_stripe_payments', () => {
            withSpies(
                {
                    supportsOnlyOneTimePayment: true,
                    hasFullScholarship: false,
                    numChargedPayments: 0,
                    totalNumPayments: 12,
                },
                () => {
                    expect(cohort_application.promisedPaymentOutsideOfStripe).toBe(false);
                },
            );
        });

        function withSpies(opts, fn) {
            jest.spyOn(Cohort, 'supportsRecurringPayments').mockReturnValue(opts.supportsRecurringPayments);
            jest.spyOn(Cohort, 'supportsOnlyOneTimePayment').mockReturnValue(opts.supportsOnlyOneTimePayment);
            jest.spyOn(cohort_application, 'hasFullScholarship', 'get').mockReturnValue(opts.hasFullScholarship);
            cohort_application.total_num_required_stripe_payments = opts.totalNumPayments;
            cohort_application.num_charged_payments = opts.numChargedPayments;
            fn();
        }
    });

    describe('additionalSavingsForPlan', () => {
        it('should work', () => {
            jest.spyOn(cohort_application, 'defaultPlan', 'get').mockReturnValue({
                id: 'defaultPlan',
            });
            jest.spyOn(cohort_application, 'getPlanTuitionActualTotal').mockImplementation(
                plan =>
                    ({
                        nonDefaultPlan: 42,
                        defaultPlan: 84,
                    }[plan.id]),
            );
            expect(
                cohort_application.additionalSavingsForPlan({
                    id: 'nonDefaultPlan',
                }),
            ).toEqual(42);
        });
    });

    describe('getPlanPaymentPerIntervalForDisplay', () => {
        it('should work', () => {
            jest.spyOn(cohort_application, 'getPlanTuitionActualTotal').mockReturnValue(100);
            jest.spyOn(cohort_application, 'getNumIntervalsForPlan').mockReturnValue(10);
            expect(cohort_application.getPlanPaymentPerIntervalForDisplay('plan')).toEqual(10);
            expect(cohort_application.getPlanTuitionActualTotal).toHaveBeenCalledWith('plan');
            expect(cohort_application.getNumIntervalsForPlan).toHaveBeenCalledWith('plan');
        });
    });

    describe('getPlanOriginalPaymentPerIntervalForDisplay', () => {
        it('should work', () => {
            jest.spyOn(cohort_application, 'getPlanTuitionActualTotal').mockReturnValue(100);
            jest.spyOn(cohort_application, 'getNumIntervalsForPlan').mockReturnValue(10);
            expect(cohort_application.getPlanPaymentPerIntervalForDisplay('plan')).toEqual(10);
            expect(cohort_application.getPlanTuitionActualTotal).toHaveBeenCalledWith('plan');
            expect(cohort_application.getNumIntervalsForPlan).toHaveBeenCalledWith('plan');
        });
    });

    describe('earlyRegistrationDeadlineHasPassed', () => {
        it('should return true if cohort_early_registration_deadline is now', () => {
            const now = new Date().getTime();
            jest.spyOn(moment.prototype, 'valueOf').mockImplementation(() => now);
            expect(cohort_application.earlyRegistrationDeadlineHasPassed).toEqual(true);
        });

        it('should return true if after cohort_early_registration_deadline', () => {
            const cohort_early_registration_deadline = new Date(2017, 10, 20).getTime() / 1000;
            Object.defineProperty(cohort_application, 'cohort_early_registration_deadline', {
                get() {
                    return cohort_early_registration_deadline;
                },
            });
            expect(cohort_application.earlyRegistrationDeadlineHasPassed).toEqual(true);
        });

        it('should return false if before earlyRegistrationDeadline', () => {
            const currentYear = new Date().getFullYear();
            const cohort_early_registration_deadline = new Date(currentYear + 1, 10, 20).getTime() / 1000;
            Object.defineProperty(cohort_application, 'cohort_early_registration_deadline', {
                get() {
                    return cohort_early_registration_deadline;
                },
            });
            expect(cohort_application.earlyRegistrationDeadlineHasPassed).toEqual(false);
        });
    });

    describe('supportsEarlyPricing', () => {
        it('should be true for applications that have different prices for early vs standard', () => {
            jest.spyOn(cohort_application, 'getPlanForFrequency').mockReturnValue({
                id: 'once_plan',
                frequency: 'once',
                amount: 960000,
            });
            cohort_application.scholarship_level = {
                standard: {
                    once_plan: {
                        amount_off: 10,
                    },
                },
                early: {
                    once_plan: {
                        amount_off: 5,
                    },
                },
            };
            expect(cohort_application.supportsEarlyPricing).toBe(true);
        });

        it('should be false for applications that have the same prices for early vs standard', () => {
            jest.spyOn(cohort_application, 'getPlanForFrequency').mockReturnValue({
                id: 'once_plan',
                frequency: 'once',
                amount: 960000,
            });
            cohort_application.scholarship_level = {
                standard: {
                    once_plan: {
                        amount_off: 5,
                    },
                },
                early: {
                    once_plan: {
                        amount_off: 5,
                    },
                },
            };
            expect(cohort_application.supportsEarlyPricing).toBe(false);
        });

        it('should handle really legacy users that do not have a once plan', () => {
            jest.spyOn(cohort_application, 'getPlanForFrequency').mockReturnValue(undefined);
            cohort_application.scholarship_level = {
                standard: {
                    monthly: {
                        amount_off: 5,
                    },
                },
                early: {
                    monthly: {
                        amount_off: 5,
                    },
                },
            };
            expect(cohort_application.supportsEarlyPricing).toBe(false);
        });
    });

    describe('defaultPlan', () => {
        it('should be once frequency for supportsEarlyPricing applications', () => {
            jest.spyOn(cohort_application, 'supportsEarlyPricing', 'get').mockReturnValue(true);
            cohort_application.stripe_plans = [
                {
                    id: 'monthly_plan',
                    frequency: 'monthly',
                    amount: 80000,
                },
                {
                    id: 'once_plan',
                    frequency: 'once',
                    amount: 960000,
                },
            ];
            expect(cohort_application.defaultPlan).toBe(cohort_application.stripe_plans[1]);
        });

        it('should be monthly frequency for non-supportsEarlyPricing (legacy pricing plan) applications', () => {
            jest.spyOn(cohort_application, 'supportsEarlyPricing', 'get').mockReturnValue(false);
            cohort_application.stripe_plans = [
                {
                    id: 'monthly_plan',
                    frequency: 'monthly',
                    amount: 80000,
                },
                {
                    id: 'once_plan',
                    frequency: 'once',
                    amount: 960000,
                },
            ];
            expect(cohort_application.defaultPlan).toBe(cohort_application.stripe_plans[0]);
        });

        it('should handle really legacy users that did not have a once frequency plan', () => {
            jest.spyOn(cohort_application, 'supportsEarlyPricing', 'get').mockReturnValue(false);
            cohort_application.stripe_plans = [
                {
                    id: 'monthly_plan',
                    frequency: 'monthly',
                    amount: 80000,
                },
                {
                    id: 'bi_annual_plan',
                    frequency: 'bi_annual',
                    amount: 480000,
                },
            ];
            expect(cohort_application.defaultPlan).toBe(cohort_application.stripe_plans[0]);
        });
    });

    describe('earlyRegistrationEnabled', () => {
        it('should be true if registration deadline has not passed', () => {
            cohort_application.allow_early_registration_pricing = false;
            cohort_application.registered_early = false;
            jest.spyOn(cohort_application, 'earlyRegistrationDeadlineHasPassed', 'get').mockReturnValue(false);
            expect(cohort_application.earlyRegistrationEnabled).toBe(true);
        });

        it('should be true if overriden in the admin', () => {
            cohort_application.allow_early_registration_pricing = true;
            cohort_application.registered_early = false;
            jest.spyOn(cohort_application, 'earlyRegistrationDeadlineHasPassed', 'get').mockReturnValue(true);
            expect(cohort_application.earlyRegistrationEnabled).toBe(true);
        });

        it('should be true if previously registered early', () => {
            cohort_application.allow_early_registration_pricing = false;
            cohort_application.registered_early = true;
            jest.spyOn(cohort_application, 'earlyRegistrationDeadlineHasPassed', 'get').mockReturnValue(true);
            expect(cohort_application.earlyRegistrationEnabled).toBe(true);
        });

        it('should be false if previously not registered early and already made payments', () => {
            cohort_application.allow_early_registration_pricing = false;
            cohort_application.registered_early = false;
            jest.spyOn(cohort_application, 'earlyRegistrationDeadlineHasPassed', 'get').mockReturnValue(false);
            jest.spyOn(cohort_application, 'tuitionNumSuccessfulPayments', 'get').mockReturnValue(1);
            expect(cohort_application.earlyRegistrationEnabled).toBe(false);
        });

        it('should be false otherwise', () => {
            cohort_application.allow_early_registration_pricing = false;
            cohort_application.registered_early = false;
            jest.spyOn(cohort_application, 'earlyRegistrationDeadlineHasPassed', 'get').mockReturnValue(true);
            expect(cohort_application.earlyRegistrationEnabled).toBe(false);
        });
    });

    describe('enrollmentAgreement', () => {
        let user;

        beforeEach(() => {
            const User = $injector.get('User');
            $injector.get('UserFixtures');
            user = User.fixtures.getInstance();
            user.cohort_applications = [cohort_application];
            cohort_application.$$embeddedIn = user;
            expect(cohort_application.program_type).not.toBeUndefined();
        });

        it('should return an agreement for this program_type', () => {
            const doc = {
                document_type: 'enrollment_agreement',
                metadata: {
                    program_type: cohort_application.program_type,
                },
            };
            user.signable_documents = [doc];
            expect(cohort_application.enrollmentAgreement).toEqual(doc);
        });

        it('should not return an agreement for a different program_type', () => {
            const doc = {
                document_type: 'enrollment_agreement',
                metadata: {
                    program_type: 'another program_type',
                },
            };
            user.signable_documents = [doc];
            expect(cohort_application.enrollmentAgreement).toBe(null);
        });

        it('should not return an agreement for a different type', () => {
            const doc = {
                document_type: 'another_type',
                metadata: {
                    program_type: cohort_application.program_type,
                },
            };
            user.signable_documents = [doc];
            expect(cohort_application.enrollmentAgreement).toBe(null);
        });

        it('should return a signed agreement over an unsigned one if there are two', () => {
            const unsignedDoc = {
                document_type: 'enrollment_agreement',
                metadata: {
                    program_type: cohort_application.program_type,
                },
            };
            const signedDoc = {
                document_type: 'enrollment_agreement',
                signed_at: 42,
                metadata: {
                    program_type: cohort_application.program_type,
                },
            };
            user.signable_documents = [unsignedDoc, signedDoc];
            expect(cohort_application.enrollmentAgreement).toEqual(signedDoc);
        });
    });

    describe('hasMadePayments', () => {
        it('should be true if tuitionNumSuccessfulPayments is greater than zero', () => {
            jest.spyOn(cohort_application, 'tuitionNumSuccessfulPayments', 'get').mockReturnValue(1);
            expect(cohort_application.hasMadePayments).toBe(true);
        });

        it('should be true if promisedPaymentOutsideOfStripe', () => {
            jest.spyOn(cohort_application, 'tuitionNumSuccessfulPayments', 'get').mockReturnValue(0);
            jest.spyOn(cohort_application, 'promisedPaymentOutsideOfStripe', 'get').mockReturnValue(true);
            expect(cohort_application.hasMadePayments).toBe(true);
        });

        it('should be false otherwise', () => {
            jest.spyOn(cohort_application, 'tuitionNumSuccessfulPayments', 'get').mockReturnValue(0);
            jest.spyOn(cohort_application, 'promisedPaymentOutsideOfStripe', 'get').mockReturnValue(false);
            expect(cohort_application.hasMadePayments).toBe(false);
        });
    });

    describe('tuitionAllPaid', () => {
        it('should be true if hasMadePayments and tuitionNumPaymentsRemaining is equal to zero', () => {
            jest.spyOn(cohort_application, 'hasMadePayments', 'get').mockReturnValue(true);
            jest.spyOn(cohort_application, 'tuitionNumPaymentsRemaining', 'get').mockReturnValue(0);
            expect(cohort_application.tuitionAllPaid).toBe(true);
        });

        it('should be false if !hasMadePayments', () => {
            jest.spyOn(cohort_application, 'hasMadePayments', 'get').mockReturnValue(false);
            expect(cohort_application.tuitionAllPaid).toBe(false);
        });

        it('should be false if tuitionNumPaymentsRemaining is not zero', () => {
            jest.spyOn(cohort_application, 'tuitionNumPaymentsRemaining', 'get').mockReturnValue(12);
            expect(cohort_application.tuitionAllPaid).toBe(false);
        });
    });
});
