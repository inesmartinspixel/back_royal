import 'AngularSpecHelper';
import 'Cohorts/angularModule';

describe('Period', () => {
    let $injector;
    let Period;
    let Cohort;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Cohorts', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                Period = $injector.get('Period');
                Cohort = $injector.get('Cohort');
            },
        ]);
    });

    describe('addAdmissionRound', () => {
        it('should work', () => {
            const period = Period.new();
            period.addAction();
            period.addAction();

            expect(period.actions.length).toBe(2);
            expect(period.actions[0].id).toBeDefined();
            expect(period.actions[0].type).toEqual('expulsion_warning');
            expect(period.actions[0].rule).toEqual('requirements_not_met');
            expect(period.actions[0].days_offset_from_end).toEqual(-1);
        });
    });

    describe('periodTitle', () => {
        let period;
        let cohort;

        beforeEach(() => {
            period = Period.new();
            cohort = Cohort.new();
            period.$$embeddedIn = cohort;
            cohort.periods.push(period);
        });

        it('should fallback to week index when no title set', () => {
            period.title = undefined;
            expect(period.periodTitle).toEqual('Week 1');

            period.title = '';
            expect(period.periodTitle).toEqual('Week 1');

            cohort.periods.unshift(Period.new());
            expect(period.periodTitle).toEqual('Week 2');

            period.title = 'my awesome title';
            expect(period.periodTitle).toEqual('my awesome title');
        });
    });

    describe('exam_style', () => {
        it('should default to intermediate', () => {
            const period = Period.new({
                style: 'standard',
            });
            period.style = 'exam';
            expect(period.exam_style).toEqual('intermediate');
        });

        it('should be settable only when style is exam or review', () => {
            const period = Period.new();
            period.style = 'exam';
            period.exam_style = 'final';
            expect(period.exam_style).toEqual('final');

            period.style = 'review';
            period.exam_style = 'intermediate';
            expect(period.exam_style).toEqual('intermediate');

            period.style = 'standard';
            expect(() => {
                period.exam_style = 'final';
            }).toThrow(new Error('exam_style can only be set if the style is exam or review'));
        });

        it('should be unset if style set to something other than exam or review', () => {
            const period = Period.new({
                style: 'exam',
                exam_style: 'intermediate',
            });
            period.style = 'standard';
            expect(period.exam_style).toBe(null);
        });
    });

    describe('requiresStream', () => {
        it('should work', () => {
            const period = Period.new({
                stream_entries: [
                    {
                        required: false,
                        locale_pack_id: 'non_required',
                    },
                    {
                        required: true,
                        locale_pack_id: 'required',
                    },
                ],
            });
            expect(period.requiresStream('none')).toBe(false);
            expect(period.requiresStream('non_required')).toBe(false);
            expect(period.requiresStream('required')).toBe(true);
        });
    });
});
