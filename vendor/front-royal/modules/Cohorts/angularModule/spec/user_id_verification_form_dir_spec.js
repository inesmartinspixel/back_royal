import 'AngularSpecHelper';
import 'Cohorts/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import userIdVerificationFormLocales from 'Cohorts/locales/cohorts/user_id_verification_form-en.json';
import moment from 'moment-timezone';

setSpecLocales(userIdVerificationFormLocales);

describe('FrontRoyal.Cohorts.userIdVerificationForm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let UserIdVerificationViewModel;
    let unverifiedForCurrentIdVerificationPeriod;
    let lastIdologyVerification;
    let currentUser;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Cohorts', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                UserIdVerificationViewModel = $injector.get('UserIdVerificationViewModel');
                currentUser = SpecHelper.stubCurrentUser();
                unverifiedForCurrentIdVerificationPeriod = true;
                lastIdologyVerification = null;
                jest.spyOn(currentUser, 'unverifiedForCurrentIdVerificationPeriod', 'get').mockImplementation(
                    () => unverifiedForCurrentIdVerificationPeriod,
                );
                jest.spyOn(currentUser, 'lastIdologyVerification', 'get').mockImplementation(
                    () => lastIdologyVerification,
                );
                SpecHelper.stubConfig();
                SpecHelper.stubDirective('telInput');
            },
        ]);
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.userIdVerificationViewModel = new UserIdVerificationViewModel();
        renderer.render(
            '<user_id_verification_form user-id-verification-view-model="userIdVerificationViewModel"></user_id_verification_form>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('step=form', () => {
        it('should have expected header', () => {
            render();
            SpecHelper.expectElementText(elem, 'h1', 'Please Verify Your Identity');
        });

        it('should allow for toggling betwwen phone and email', () => {
            render();

            SpecHelper.expectElementText(elem, '.email-or-phone-label', 'Mobile Phone Number');
            SpecHelper.expectElementText(elem, '.toggle-contact-method', 'Send an email instead');
            SpecHelper.expectElement(elem, 'tel-input');
            SpecHelper.expectNoElement(elem, '[name="email"]');

            SpecHelper.click(elem, '.toggle-contact-method');
            SpecHelper.expectElementText(elem, '.email-or-phone-label', 'Email Address');
            SpecHelper.expectElementText(elem, '.toggle-contact-method', 'Send a text instead');
            SpecHelper.expectNoElement(elem, 'tel-input');
            SpecHelper.expectElement(elem, '[name="email"]');
        });

        it('should request a link', () => {
            render();
            assertLinkRequested();
        });
    });

    describe('step=polling', () => {
        it('should work', () => {
            render();

            scope.userIdVerificationViewModel.phone = '15405555555';
            scope.userIdVerificationViewModel.linkSentAt = new Date().getTime();
            scope.$digest();

            SpecHelper.expectElement(elem, 'front-royal-spinner');
            SpecHelper.expectElementText(elem, 'h1', 'Please Verify Your Identity');
            const subtitle = SpecHelper.expectElementText(
                elem,
                '.subtitle',
                'Your time-sensitive link has been sent to (540) 555-5555. Open the link on your smartphone to complete the verification process. Send me another link.',
            );

            SpecHelper.click(subtitle, 'a');
            expect(scope.userIdVerificationViewModel.step).toEqual('form');
        });

        it('should work with email link', () => {
            render();

            scope.userIdVerificationViewModel.contactMethod = 'email';
            scope.userIdVerificationViewModel.email = 'someone@example.com';
            scope.userIdVerificationViewModel.linkSentAt = new Date().getTime();
            scope.$digest();

            SpecHelper.expectElement(elem, 'front-royal-spinner');
            SpecHelper.expectElementText(elem, 'h1', 'Please Verify Your Identity');
            const subtitle = SpecHelper.expectElementText(
                elem,
                '.subtitle',
                'Your time-sensitive link has been sent to someone@example.com. Open the link on your smartphone to complete the verification process. Send me another link.',
            );

            SpecHelper.click(subtitle, 'a');
            expect(scope.userIdVerificationViewModel.step).toEqual('form');
        });

        it('should ping', () => {
            const $q = $injector.get('$q');
            const $interval = $injector.get('$interval');
            const statusPoller = $injector.get('FrontRoyal.StatusPoller');
            jest.spyOn(statusPoller, 'send').mockImplementation(() => {}); // unexpected pings can cause errors when we flush $interval, so mock it out

            render();
            jest.spyOn(scope, '_sendPing').mockReturnValue(
                $q.when({
                    data: {},
                }),
            );

            // When not polling , we should not ping
            $interval.flush(30 * 1000);
            expect(scope._sendPing).not.toHaveBeenCalled();

            // When polling , we should ping every 5 seconds
            scope.userIdVerificationViewModel.linkSentAt = new Date().getTime();
            scope.$digest();
            $interval.flush(10 * 1000);
            expect(scope._sendPing.mock.calls.length).toEqual(2);
            scope._sendPing.mockClear();

            // When not polling , we should not ping
            scope.userIdVerificationViewModel.linkSentAt = null;
            scope.$digest();
            $interval.flush(10 * 1000);
            expect(scope._sendPing).not.toHaveBeenCalled();

            // When destroyed, the interval should be cleared
            scope.userIdVerificationViewModel.linkSentAt = new Date().getTime();
            scope.$digest();
            $interval.flush(10 * 1000);
            expect(scope._sendPing.mock.calls.length).toEqual(2);
            scope.$destroy();
            $interval.flush(10 * 1000);
            expect(scope._sendPing.mock.calls.length).toEqual(2);
        });
    });

    describe('step=verified', () => {
        it('should work', () => {
            render();
            unverifiedForCurrentIdVerificationPeriod = false;
            scope.$digest();

            SpecHelper.expectElementText(elem, 'h1', 'Success!');
            SpecHelper.expectElementText(elem, '.subtitle', 'Success! Your identity verification is complete.');
        });
    });

    describe('step=failed', () => {
        it('should work', () => {
            render();
            scope.userIdVerificationViewModel.linkSentAt = moment().subtract(5, 'minutes').toDate().getTime();
            lastIdologyVerification = {
                response_received_at: moment().subtract(2, 'minutes').toDate().getTime() / 1000,
                verified: false,
            };
            scope.$digest();

            SpecHelper.expectElementText(elem, 'h1', 'Identity Verification Failed');
            const subtitle = SpecHelper.expectElementText(
                elem,
                '.subtitle',
                'We were unable to verify your ID and photo. Please try again.',
            );

            SpecHelper.click(subtitle, 'a');
            expect(scope.userIdVerificationViewModel.step).toEqual('form');

            // re-submitting the form should take us back to the polling step,
            // even though the last idology verification is still failed
            assertLinkRequested();
            expect(scope.userIdVerificationViewModel.step).toEqual('polling');
        });
    });

    function assertLinkRequested() {
        jest.spyOn(currentUser, 'requestIdologyLink').mockImplementation(() => {});
        SpecHelper.submitForm(elem);
        expect(currentUser.requestIdologyLink).toHaveBeenCalled();
    }
});
