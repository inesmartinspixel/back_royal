import 'FrontRoyalStore/angularModule';
import 'SafeApply/angularModule';

const angularModule = angular.module('ngStorableImage', ['safeApply', 'FrontRoyalStore']);

angularModule.directive('storableImage', [
    '$injector',
    $injector => {
        const frontRoyalStore = $injector.get('frontRoyalStore');
        const safeApply = $injector.get('safeApply');

        return {
            restrict: 'A',
            link(scope, elem, attrs) {
                attrs.$observe('storableImage', src => {
                    // FIXME: probably need some load error handling, possibly success

                    if (!src) {
                        attrs.$set('src', null);
                        return;
                    }

                    if (!frontRoyalStore.enabled) {
                        attrs.$set('src', src);
                        return;
                    }

                    frontRoyalStore.getDataUrlIfStored(src).then(url => {
                        attrs.$set('src', url);
                        safeApply(scope);
                    });
                });
            },
        };
    },
]);

export default angularModule;
