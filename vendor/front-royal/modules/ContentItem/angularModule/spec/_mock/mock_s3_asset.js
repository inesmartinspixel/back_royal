angular.module('FrontRoyal.ContentItem').factory('MockS3Asset', [
    () => ({
        get(overrides) {
            return angular.extend(
                {
                    file_file_name: 'image.jpg',
                    formats: {
                        original: {
                            url: 'original_url.jpg',
                        },
                    },
                },
                overrides || {},
            );
        },
    }),
]);
