angular.module('FrontRoyal.ContentItem').factory('MockIsContentItem', [
    '$injector',
    $injector => {
        const IsContentItem = $injector.get('IsContentItem');
        const Iguana = $injector.get('Iguana');
        const guid = $injector.get('guid');
        $injector.get('MockIguana');

        const ContentItem = Iguana.subclass(function () {
            this.setCollection('content_items');
            this.include(IsContentItem);
            this.setAdapter('Iguana.Mock.Adapter');

            this.extend({
                getInstance(attrs = {}) {
                    attrs.title = attrs.title || `Content Item ${guid.generate()}`;
                    attrs.id = attrs.id || guid.generate();
                    attrs.updated_at = attrs.updated_at || 42;
                    attrs.locale = attrs.locale || 'en';
                    return ContentItem.new(attrs);
                },

                editorUrl(id) {
                    return `editor/content_item/${id}/edit`;
                },

                fieldsForEditorList: undefined,
            });

            Object.defineProperty(this.prototype, 'factoryName', {
                get() {
                    return 'MockIsContentItem';
                },
                configurable: true,
            });

            return {
                // FIXME: we could potentially DRY this up with all content items
                addLocalePackFixture() {
                    if (this.locale !== 'en') {
                        throw new Error('We are assuming an en locale');
                    }
                    this.locale_pack = $injector.get('LocalePack').new({
                        id: guid.generate(),
                        content_items: [
                            {
                                id: this.id,
                                title: this.title,
                                locale: 'en',
                            },
                            {
                                id: guid.generate(),
                                title: 'Spanish',
                                locale: 'es',
                            },
                            {
                                id: guid.generate(),
                                title: 'Chinese',
                                locale: 'zh',
                            },
                        ],
                    });
                    return this;
                },
            };
        });

        return ContentItem;
    },
]);
