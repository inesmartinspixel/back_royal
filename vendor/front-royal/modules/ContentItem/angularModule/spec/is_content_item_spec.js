import 'AngularSpecHelper';
import 'ContentItem/angularModule';
import 'ContentItem/angularModule/spec/_mock/mock_is_content_item';

describe('IsContentItem', () => {
    let IsContentItem;
    let SpecHelper;
    let $injector;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('FrontRoyal.ContentItem', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                IsContentItem = $injector.get('MockIsContentItem');
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);
    });

    describe('date formatting', () => {
        let today_date_string;
        let now_in_sec;
        let $filter;

        beforeEach(() => {
            const today = Date.now();
            now_in_sec = today / 1000;
            const today_date = new Date(today);
            $filter = $injector.get('$filter');
            today_date_string = $filter('amDateFormat')(today_date, 'l h:mm:ss a');
        });

        it('should pretty format modified_at_date', () => {
            const instance = IsContentItem.new();
            instance.modified_at = now_in_sec;
            SpecHelper.expectEqual(instance.modified_at_date, today_date_string);
        });

        it('should pretty format published_at_date', () => {
            const instance = IsContentItem.new();
            instance.published_at = now_in_sec;
            SpecHelper.expectEqual(instance.last_published_date, today_date_string);
        });

        it('should render dates as Unknown strings if non-existent', () => {
            const instance = IsContentItem.new();
            instance.published_at = undefined;
            SpecHelper.expectEqual(instance.last_published_date, 'Unknown');
        });
    });

    describe('pinnedVersions', () => {
        it('should include pinned versions', () => {
            const instance = IsContentItem.new({
                version_history: [
                    {
                        pinned: true,
                    },
                    {
                        pinned: false,
                    },
                    {
                        pinned: true,
                    },
                ],
            });
            expect(instance.pinnedVersions).toEqual([instance.version_history[0], instance.version_history[2]]);
        });
    });

    describe('launchText for standard lesson', () => {
        it('should be start if not started', () => {
            const instance = IsContentItem.new();
            Object.defineProperty(instance, 'started', {
                value: false,
            });
            Object.defineProperty(instance, 'complete', {
                value: false,
            });
            expect(instance.launchText).toBe('START');
        });

        it('should be resume if started', () => {
            const instance = IsContentItem.new();
            Object.defineProperty(instance, 'started', {
                value: true,
            });
            Object.defineProperty(instance, 'complete', {
                value: false,
            });
            expect(instance.launchText).toBe('RESUME');
        });

        it('should be retake if completed', () => {
            const instance = IsContentItem.new();
            Object.defineProperty(instance, 'started', {
                value: true,
            });
            Object.defineProperty(instance, 'complete', {
                value: true,
            });
            expect(instance.launchText).toBe('RETAKE');
        });
    });

    describe('started', () => {
        it('should throw', () => {
            const instance = IsContentItem.new();
            expect(() => {
                instance.started;
            }).toThrow(new Error('Classes that include IsContentItem should defined "started"'));
        });
    });

    describe('completed', () => {
        it('should throw', () => {
            const instance = IsContentItem.new();
            expect(() => {
                instance.complete;
            }).toThrow(new Error('Classes that include IsContentItem should defined "complete"'));
        });
    });

    describe('duplicate', () => {
        beforeEach(() => {
            IsContentItem.prototype._afterDuplicate = jest.fn();
        });

        it('should work', () => {
            const instance = IsContentItem.getInstance();
            IsContentItem.expect('create').toBeCalledWith(
                {
                    title: 'New Title',
                },
                {
                    in_same_locale_pack: false,
                    duplicate_from_id: instance.id,
                    duplicate_from_updated_at: instance.updated_at,
                },
            );

            instance.duplicate(
                {
                    title: 'New Title',
                },
                {
                    in_same_locale_pack: false,
                },
            );

            IsContentItem.flush('create');
        });
    });

    describe('property englishTitleWithBrackets', () => {
        it('should return an empty string if content item has no english translation', () => {
            const instance = IsContentItem.new();
            instance.title = 'English Title';
            expect(instance.englishTitleWithBrackets).toBe('');
        });

        it('should show the title and English for item with an English translation and title not starting with "Duplicated from"', () => {
            const instance = IsContentItem.new();
            instance.title = 'Translated (Title)';
            Object.defineProperty(instance, 'englishTranslation', {
                value: {
                    title: 'English (Title)',
                },
            });
            expect(instance.englishTitleWithBrackets).toBe(' [English (Title)]');
        });

        it('should return an empty string if content item title starts with "Duplicated from"', () => {
            const instance = IsContentItem.new();
            instance.title = 'Duplicated from "English (Title)"';
            Object.defineProperty(instance, 'englishTranslation', {
                value: {
                    title: 'English (Title)',
                },
            });
            expect(instance.englishTitleWithBrackets).toBe('');
        });
    });
});
