// The linkedIn conversion pixels should really be defined
// in segment, but they are not supported over there, so we
// implement them ourselves
//
// To associate linked in conversion pixels with events,
// you need a conversion ID.
//
// 1. Get access to the Pedago Campaign Manager in linked in
// 1. Goto https://www.linkedin.com/ad/accounts
// 1. in the Account Name column, Click "Pedago, LLC" or "Quantic School of Business and Technology"
// 1. Click Account Assets -> Conversions
//
// Once you have the conversion ID, set it below using definePixel()
//  * The id for `test` will be used in development mode and staging; the
//    id for `production` will be used in production.
//  * The ids defined in the `quantic` and `smartly` respect the domain
//    from which we are currently accessing the site
//
// NOTE: apparently when these pixels are logged from localhost, they do
// not get counted in the linkedin account manager.  ngrok might work, but
// I haven't tested it

const config = {};

definePixel('application-form:submit-application', {
    quantic: {
        test: 1742682, // Application Submitted - Developer Test Pixel
        production: 1742658, // Application Submitted
    },
    smartly: {
        test: 751513, // Application Submit - Developer Test Pixel
        production: 651793, // Application Submit
    },
});

definePixel('user:logged_in_first_time', {
    quantic: {
        test: 1742690, // Sign Up - Developer Test Pixel
        production: 1742650, // Sign Up
    },
    smartly: {
        test: 655561, // PLP Sign Up - Developer Test Pixel
        production: 651777, // PLP Sign Up
    },
});

function definePixel(eventType, pixels) {
    config[eventType] = config[eventType] || {};

    Object.entries(pixels).forEach(pair => {
        const [site, values] = pair;
        config[eventType][site] = config[eventType][site] || {};

        // define development pixel using `test` id
        config[eventType][site].development = config[eventType][site].development || [];
        config[eventType][site].development.push(values.test);

        // define staging pixel using `test` id
        config[eventType][site].staging = config[eventType][site].staging || [];
        config[eventType][site].staging.push(values.test);

        // define production pixel using `production` id
        config[eventType][site].production = config[eventType][site].production || [];
        config[eventType][site].production.push(values.production);
    });
}

export default function getConversionPixelIds(eventType, isQuantic, appEnvType) {
    const site = isQuantic ? 'quantic' : 'smartly';
    try {
        return config[eventType][site][appEnvType];
    } catch (err) {
        return [];
    }
}
