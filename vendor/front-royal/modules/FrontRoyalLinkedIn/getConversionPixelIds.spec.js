import getConversionPixelIds from './getConversionPixelIds';

describe('getConversionPixelIds', () => {
    it('should work', () => {
        expect(getConversionPixelIds('application-form:submit-application', true, 'development')).toEqual([1742682]);
        expect(getConversionPixelIds('application-form:submit-application', true, 'staging')).toEqual([1742682]);
        expect(getConversionPixelIds('application-form:submit-application', true, 'production')).toEqual([1742658]);

        expect(getConversionPixelIds('application-form:submit-application', false, 'development')).toEqual([751513]);
        expect(getConversionPixelIds('application-form:submit-application', false, 'staging')).toEqual([751513]);
        expect(getConversionPixelIds('application-form:submit-application', false, 'production')).toEqual([651793]);
    });
});
