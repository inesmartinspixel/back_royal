let _localStorageEnabled = false;

window.hasAuthHeaders = false;
try {
    window.localStorage.setItem('x', 'x');
    window.localStorage.removeItem('x');
    _localStorageEnabled = true;
} catch (e) {
    // noop
}

// Side effect warning.  Some html.erb files need localStorageEnabled
// to be on the window, since they can't import it (We could probably
// refactor those into .js files, but haven't now)
const localStorageEnabled = _localStorageEnabled;
window.localStorageEnabled = localStorageEnabled;

export default localStorageEnabled;
