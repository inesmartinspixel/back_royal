/* eslint-disable no-empty */
import logPageTimes from 'logPageTimes';
// import ClientStorage from 'ClientStorage';

// NOTE: this has side effects
import localStorageEnabled from './setLocalStorageEnabled';

window.hasAuthHeaders = false;
try {
    if (localStorageEnabled && window.localStorage.auth_headers) {
        const parsedHeaders = JSON.parse(window.localStorage.auth_headers);
        window.hasAuthHeaders = Object.keys(parsedHeaders['access-token']).length > 0;
    }
} catch (e) {}

// Try window.location.host fallback vector to support things like IE compatability mode,
// where origin may be undefined, in an intranet zone, despite being officially supported.
if (window.location.origin) {
    window.ENDPOINT_ROOT = window.location.origin;
} else if (window.location.host) {
    window.ENDPOINT_ROOT = `${window.location.protocol || 'https:'}//${window.location.host}`;
}

logPageTimes();
