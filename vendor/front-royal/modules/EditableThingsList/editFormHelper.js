/*
    This function is used when setting up an edit form for an editableThingsList.

    It returns

    - initialValues - object to be passed into the formik form
    - onSubmit - callback function to be passed into the formik form

    The `save` argument is optional.  It is only needed if you need to pass extra arguments
        into `proxy.save()`.  See AdminEditBillingTransaction/index.jsx for an example
*/
export default function editFormHelper(args) {
    const { thing, editableThingsListViewModel, fromForm, toForm, $injector, metaData } = args;

    const iguanaKlass = thing.constructor;

    function onSubmit(values, actions) {
        let _metadata = metaData || {};
        if (metaData && typeof metaData === 'function') {
            _metadata = metaData();
        }

        const $http = $injector.get('$http');
        const isNew = !thing.id;
        const formData = new FormData();
        const record = {};

        const fromValues = fromForm(values);
        Object.keys(fromValues).forEach(key => {
            const val = fromValues[key];
            if (val && val.constructor && val.constructor.name === 'File') {
                formData.append(key, val);
            } else {
                record[key] = val;
            }
        });
        formData.append('record', JSON.stringify(record));
        formData.append('meta', JSON.stringify(_metadata));

        const baseUrl = `${iguanaKlass.baseUrl}/${iguanaKlass.collection}`;
        const url = isNew ? `${baseUrl}.json` : `${baseUrl}/${thing.id}.json`;
        const request = {
            method: isNew ? 'POST' : 'PUT',
            url,
            data: formData,
            headers: {
                Accept: 'application/json',

                // We have to set Content-type to undefined here.  When we
                // do that, the content-type ends up getting set to
                // multipart form with an appropriate boundary.  Otherwise
                // it ends up being application/json.  I guess maybe
                // $http is doing that
                'Content-type': undefined,
            },
        };

        $http(request)
            .then(response => {
                let result;
                try {
                    result = response.data.contents[thing.constructor.collection][0];
                } catch (e) {
                    throw new Error('No result found in response.');
                }
                thing.copyAttrs(result);
                actions.setValues(toForm(thing.asJson()));
                editableThingsListViewModel.onSaved(thing, isNew);
            })
            .finally(() => {
                actions.setSubmitting(false);
            });
    }

    return [toForm(thing.asJson()), onSubmit];
}
