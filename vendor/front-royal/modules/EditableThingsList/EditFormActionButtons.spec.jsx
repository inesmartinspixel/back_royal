import React from 'react';
import { mount } from 'enzyme';
import { MockForm } from 'FrontRoyalForm/specHelpers';
import { ThemeProvider } from '@material-ui/styles';
import EditFormActionButtons from './EditFormActionButtons';

describe('EditFormActionButtons', () => {
    let elem;

    afterEach(() => elem && elem.unmount());

    describe('shared button logic', () => {
        let thing;

        beforeEach(() => {
            thing = { id: 42 };
        });

        it('should be disabled while submitting', () => {
            elem = mount(
                <Wrapper>
                    <EditFormActionButtons thing={thing} />
                </Wrapper>,
            );
            elem.find('form').simulate('submit');
            expect(elem.find('[name="save"]').prop('disabled')).toBe(true);
            expect(elem.find('[name="destroy"]').prop('disabled')).toBe(true);
        });

        it('should be disabled if props say so', () => {
            elem = mount(
                <Wrapper>
                    <EditFormActionButtons disabled thing={thing} />
                </Wrapper>,
            );
            expect(elem.find('[name="save"]').prop('disabled')).toBe(true);
            expect(elem.find('[name="destroy"]').prop('disabled')).toBe(true);
        });

        it('should not be disabled otherwise', () => {
            elem = mount(
                <Wrapper>
                    <EditFormActionButtons thing={thing} />
                </Wrapper>,
            );
            expect(elem.find('[name="save"]').prop('disabled')).not.toBe(true);
            expect(elem.find('[name="destroy"]').prop('disabled')).not.toBe(true);
        });
    });

    describe('save button', () => {
        it('should be disabled if form is invalid', () => {
            elem = mount(
                <Wrapper validateOnMount initialErrors={{ something: true }}>
                    <EditFormActionButtons thing={{}} />
                </Wrapper>,
            );
            expect(elem.find('[name="save"]').prop('disabled')).toBe(true);
        });
    });

    describe('destroy button', () => {
        let preventDefault;
        let thing;
        let editableThingsListViewModel;

        it('should trigger destroy if confirmed', () => {
            jest.spyOn(window, 'confirm').mockReturnValue(true);
            renderAndClick();
            expect(preventDefault).toHaveBeenCalled();
            expect(thing.destroy).toHaveBeenCalled();
            expect(editableThingsListViewModel.onDestroyed).toHaveBeenCalledWith(thing.id);
        });

        it('should not trigger destroy if not confirmed', () => {
            jest.spyOn(window, 'confirm').mockReturnValue(false);
            renderAndClick();
            expect(preventDefault).toHaveBeenCalled();
            expect(thing.destroy).not.toHaveBeenCalled();
            expect(editableThingsListViewModel.onDestroyed).not.toHaveBeenCalled();
        });

        it('should be disabled if no id', () => {
            thing = {};
            elem = mount(
                <Wrapper>
                    <EditFormActionButtons thing={thing} />
                </Wrapper>,
            );
            expect(elem.find('[name="destroy"]').prop('disabled')).toBe(true);
        });

        function renderAndClick() {
            thing = {
                id: 'myId',
                destroy: jest.fn().mockReturnValue({
                    then: cb => cb(),
                }),
            };
            editableThingsListViewModel = {
                onDestroyed: jest.fn(),
            };
            preventDefault = jest.fn();
            elem = mount(
                <Wrapper>
                    <EditFormActionButtons {...{ thing, editableThingsListViewModel }} />
                </Wrapper>,
            );
            elem.find('[name="destroy"]').simulate('click', { preventDefault });
        }
    });

    function Wrapper({ validateOnMount, initialErrors, children }) {
        return (
            <MockForm {...{ validateOnMount, initialErrors }}>
                <ThemeProvider theme={{ spacing: () => 4 }}>{children}</ThemeProvider>
            </MockForm>
        );
    }
});
