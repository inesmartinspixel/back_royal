import React from 'react';
import { connect } from 'formik';
import { useTheme } from '@material-ui/styles';
import { makeStyles } from '@material-ui/core/styles';

/*
    This component provides a save and a destroy button
    for the edit form used with editable-things-list.

    This component implements destroy on its own.  It does
    not implement save, because there seemed to be more variety
    in the requirements for the save command in various places.
    When the user clicks the save button, the forms onSubmit callback
    will be triggered.

    Usage:

    <EditFormActionButtons
        disabled={someDisabledRule}
        {...{formik, editableThingsListViewModel, thing}}
    />
*/

const useStyles = makeStyles(theme => ({
    editFormActionButtons: {
        marginLeft: theme && theme.spacing(1),
        marginRight: theme && theme.spacing(1),
    },
}));

const EditFormActionButtons = connect(props => {
    const { formik, thing, disabled, editableThingsListViewModel } = props;
    const theme = useTheme();
    const classes = useStyles(theme || {});

    function destroy(e) {
        e.preventDefault();
        if (window.confirm('Are you sure you want to delete?')) {
            thing.destroy().then(() => {
                editableThingsListViewModel.onDestroyed(thing.id);
            });
        }
    }

    return (
        <div className={`form-group row form-actions no-border ${classes.editFormActionButtons}`}>
            <button
                className="btn flat blue pull-left"
                type="submit"
                name="save"
                disabled={!formik.isValid || formik.isSubmitting || disabled}
            >
                Save
            </button>

            <button
                className="btn flat red pull-right"
                name="destroy"
                type="button"
                onClick={destroy}
                disabled={!thing.id || formik.isSubmitting || disabled}
            >
                Delete
            </button>

            <div className="clearfix" />
        </div>
    );
});

export default EditFormActionButtons;
