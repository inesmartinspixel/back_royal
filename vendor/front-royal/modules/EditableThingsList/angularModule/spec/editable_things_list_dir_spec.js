import 'AngularSpecHelper';
import 'EditableThingsList/angularModule';
import 'Lessons/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Playlists/angularModule/spec/_mock/fixtures/playlists';

import Papa from 'papaparse';

class RecentlyEditedLessons {
    // eslint-disable-next-line
    get lessons() {}
}

describe('EditableThingsList', () => {
    let $injector;
    let renderer;
    let things;
    let elem;
    let scope;
    let SpecHelper;
    let $location;
    let $timeout;
    let $q;
    let Thing;
    let resolveIndexCall;
    let resolveShowCall;
    let parentScope;
    let Lesson;
    let ClientStorage;
    const totalCountForServerPagination = 42;
    let $window;
    let responseMeta;
    let thingFromShowCall;

    beforeEach(() => {
        angular.mock.module('EditableThingsList', 'SpecHelper', 'FrontRoyal.Lessons');

        angular.mock.module($provide => {
            Thing = function () {};
            _.extend(Thing, {
                index: jest.fn().mockImplementation(() => getIndexPromise()),
                show: jest.fn().mockImplementation(() => getShowPromise()),
                new() {
                    return new Thing();
                },
                mapClientSortToServerSort(col) {
                    return `${col}-mapped`;
                },
            });
            $provide.value('Thing', Thing);
            $provide.value('RecentlyEditedLessons', RecentlyEditedLessons);
        });

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            $location = $injector.get('$location');
            $timeout = $injector.get('$timeout');
            $q = $injector.get('$q');
            Lesson = $injector.get('Lesson');
            ClientStorage = $injector.get('ClientStorage');
            $window = $injector.get('$window');
            const $rootScope = $injector.get('$rootScope');
            $rootScope.currentUser = {
                hasAdminAccess: true,
            };

            responseMeta = {
                total_count: totalCountForServerPagination,
            };

            jest.spyOn(Lesson, 'index').mockImplementation(() => getIndexPromise());

            things = [];
        });
    });

    function getShowPromise() {
        return $q(_resolveShowCall => {
            resolveShowCall = () => {
                const response = {
                    result: thingFromShowCall,
                };
                _resolveShowCall(response);
                scope.$apply();
                $timeout.flush();
            };
        });
    }

    function getIndexPromise() {
        return $q(_resolveIndexCall => {
            resolveIndexCall = () => {
                const response = {
                    result: things,
                };

                // when using server pagination, we expect to get a total_count prop
                // back in the meta
                if (scope.usingServerPagination) {
                    response.meta = responseMeta;
                }
                _resolveIndexCall(response);
                scope.$apply();
                $timeout.flush();
            };
        });
    }

    afterEach(() => {
        $location.search('id', null);
        SpecHelper.cleanup();
        ClientStorage.removeItem('key_page');
        ClientStorage.removeItem('key_sort');
    });

    function render(html, scopeObj) {
        if (!html) {
            html = '<editable-things-list klass-name="\'Thing\'"></editable-things-list>';
        }
        renderer = SpecHelper.renderer();
        _.extend(renderer.scope, scopeObj);
        renderer.render(html);
        parentScope = renderer.scope;
        elem = renderer.elem;
        scope = elem.isolateScope();

        // start watchGroup on filters, etc.
        // NOTE: We have to specify 0 here so that all of the timeouts that are
        // set explicitly to 0 get flushed
        $timeout.flush();
    }

    it('should render a list of things', () => {
        things = [
            {
                id: 'id1',
            },
            {
                id: 'id2',
            },
        ];

        render();
        SpecHelper.expectElement(elem, 'front-royal-spinner');
        SpecHelper.expectNoElement(elem, '[name="search-message"]'); // server pagination only
        SpecHelper.expectNoElement(elem, '[name="results-wrapper"]');
        expect(Thing.index).toHaveBeenCalled();
        resolveIndexCall();
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
        SpecHelper.expectNoElement(elem, '[name="search-message"]'); // server pagination only
        SpecHelper.expectElement(elem, '[name="results-wrapper"]');
        SpecHelper.expectElements(elem, 'tbody tr', 2);
        SpecHelper.expectElement(elem, 'tbody tr[data-id="id1"]');
        SpecHelper.expectElement(elem, 'tbody tr[data-id="id2"]');
    });

    describe('filters', () => {
        describe('server-side', () => {
            it('should not trigger a new index call if new value is identical to old', () => {
                render('<editable-things-list filters="filters" klass-name="\'Thing\'"></editable-things-list>', {
                    filters: [
                        {
                            value: {
                                some: 'filter',
                            },
                            server: true,
                        },
                        {
                            value: {
                                client: 'filter',
                            },
                        },
                    ],
                });
                resolveIndexCall();

                // When a change is made only to a client filter,
                // no new index call should be made
                Thing.index.mockClear();
                parentScope.filters = [
                    {
                        value: {
                            some: 'filter',
                        },
                        server: true,
                    },
                    {
                        value: {
                            client: 'change',
                        },
                    },
                ];
                scope.$apply();
                expect(Thing.index).not.toHaveBeenCalled();

                // When a change is made to a server filter, a new
                // index call should be made
                parentScope.filters = [
                    {
                        value: {
                            some: 'change',
                        },
                        server: true,
                    },
                    {
                        client: 'change',
                    },
                ];
                scope.$apply();
                expect(Thing.index).toHaveBeenCalled();
            });

            it('should add server filters to the index call', () => {
                render('<editable-things-list filters="filters" klass-name="\'Thing\'"></editable-things-list>', {
                    filters: [
                        {
                            value: {
                                some: 'filter',
                            },
                            server: true,
                        },
                        {
                            value: {
                                client: 'filter',
                            },
                        },
                    ],
                });
                resolveIndexCall();
                expect(Thing.index).toHaveBeenCalledWith({
                    filters: {
                        some: 'filter',
                    },
                });
            });
        });

        describe('client-side', () => {
            it('should ignore server side filters', () => {
                things = [
                    {
                        id: 'id1',
                        some: 'thing',
                        client: '1',
                    },
                    {
                        id: 'id2',
                        some: 'thing else',
                        client: '2',
                    },
                ];
                setFiltersAndExpectThings(
                    ['id1'],
                    [
                        {
                            value: {
                                some: 'thing else',
                            },
                            server: true,
                        },
                        {
                            value: {
                                client: '1',
                            },
                            server: false,
                        },
                    ],
                );
            });

            it('should filter on a text field', () => {
                things = [
                    {
                        id: 'id1',
                        client: '1',
                    },
                    {
                        id: 'id2',
                        client: '2',
                    },
                    {
                        id: 'id3',
                        client: undefined,
                    },
                ];
                setFiltersAndExpectThings(
                    ['id1'],
                    [
                        {
                            value: {
                                client: '1',
                            },
                            server: false,
                        },
                    ],
                );
            });

            it('should filter on a boolean field', () => {
                things = [
                    {
                        id: 'id1',
                        client: true,
                    },
                    {
                        id: 'id2',
                        client: false,
                    },
                    {
                        id: 'id3',
                        client: undefined,
                    },
                ];
                setFiltersAndExpectThings(
                    ['id1'],
                    [
                        {
                            value: {
                                client: true,
                            },
                            server: false,
                        },
                    ],
                );
            });

            it('should accept an array value', () => {
                things = [
                    {
                        id: 'id1',
                        client: '1',
                    },
                    {
                        id: 'id2',
                        client: '2',
                    },
                    {
                        id: 'id3',
                        client: '3',
                    },
                    {
                        id: 'id4',
                        client: undefined,
                    },
                ];
                setFiltersAndExpectThings(
                    ['id1', 'id3'],
                    [
                        {
                            value: {
                                client: ['1', '3'],
                            },
                            server: false,
                        },
                    ],
                );
            });

            it('should filter on a column with a function definition', () => {
                things = [
                    {
                        id: 'id1',
                        client: '1',
                    },
                    {
                        id: 'id2',
                        client: '2',
                    },
                    {
                        id: 'id3',
                        client: undefined,
                    },
                ];
                render(
                    '<editable-things-list filters="filters" klass-name="\'Thing\'" columns="columns"></editable-things-list>',
                    {
                        filters: [
                            {
                                value: {
                                    'my-col': '1',
                                },
                                server: false,
                            },
                        ],
                        columns: [
                            {
                                id: 'my-col',
                                prop(thing) {
                                    return thing.client;
                                },
                            },
                        ],
                    },
                );
                resolveIndexCall();
                expectThings(['id1']);
            });

            describe('quick filter, quickFilterProperties', () => {
                it('should work', () => {
                    things = [
                        {
                            id: 'id1',
                            prop1: 'this will match',
                            prop2: 'nope',
                        },
                        {
                            id: 'id2',
                            prop1: 'nope',
                            prop2: 'this will match',
                        },
                        {
                            id: 'id3',
                            prop1: 'nope',
                            prop2: 'nope',
                        },
                    ];
                    render('<editable-things-list columns="columns" klass-name="\'Thing\'"></editable-things-list>', {
                        columns: [
                            {
                                prop: 'prop1',
                            },
                            {
                                prop: 'prop2',
                            },
                        ],
                    });
                    resolveIndexCall();
                    SpecHelper.updateInput(elem, '[name="quick-filter"]', 'mATch');
                    expectThings(['id1', 'id2'], {
                        ignoreOrder: true,
                    });
                });

                it('should respect quickFilterProperties', () => {
                    things = [
                        {
                            id: 'id1',
                            prop1: 'this will match',
                            prop2: 'nope',
                        },
                        {
                            id: 'id2',
                            prop1: 'nope',
                            prop2: 'this would match except that quickFilterProperties ignores this property',
                        },
                        {
                            id: 'id3',
                            prop1: 'nope',
                            prop2: 'nope',
                        },
                    ];
                    render(
                        '<editable-things-list columns="columns" quick-filter-properties="[\'prop1\']" klass-name="\'Thing\'"></editable-things-list>',
                        {
                            columns: [
                                {
                                    prop: 'prop1',
                                },
                                {
                                    prop: 'prop2',
                                },
                            ],
                        },
                    );
                    resolveIndexCall();
                    SpecHelper.updateInput(elem, '[name="quick-filter"]', 'mATch');

                    // since we're only matching prop1, only id1 matches our quick filter
                    expectThings(['id1']);
                });
            });

            function setFiltersAndExpectThings(thingIds, filters) {
                render('<editable-things-list filters="filters" klass-name="\'Thing\'"></editable-things-list>', {
                    filters,
                });
                resolveIndexCall();
                expectThings(thingIds, {
                    ignoreOrder: true,
                });
            }
        });
    });

    describe('apiFields', () => {
        it('should be passed along in the index call', () => {
            render('<editable-things-list api-fields="[\'a\']" klass-name="\'Thing\'"></editable-things-list>');
            expect(Thing.index.mock.calls[0][0]['fields[]']).toEqual(['a']);
        });
    });

    describe('isContentItem special handling', () => {
        let contentItemEditorLists;

        beforeEach(() => {
            contentItemEditorLists = $injector.get('contentItemEditorLists');
            jest.spyOn(contentItemEditorLists, 'load').mockImplementation(() => ({
                onLoad(callback) {
                    callback(things);
                },
            }));
        });

        // look at locale
        // locale stuff in clientFiltering
        it('should use contentItemEditorLists if only using default filters and not using server pagination', () => {
            things = [
                {
                    id: 'id1',
                },
            ];
            render('<editable-things-list klass-name="\'Lesson\'" filters="filters"></editable-things-list>', {
                filters: [
                    {
                        value: {
                            some: 'filter',
                        },
                        server: true,
                        default: true,
                    },
                ],
            });
            resolveIndexCall();
            expect(Lesson.index).not.toHaveBeenCalled();
            expect(contentItemEditorLists.load).toHaveBeenCalled();
            expect(scope.things).toEqual(things);
        });

        it('should use a regular index call if applying non-default filters', () => {
            things = [
                {
                    id: 'id1',
                },
            ];
            render('<editable-things-list klass-name="\'Lesson\'" filters="filters"></editable-things-list>', {
                filters: [
                    {
                        value: {
                            some: 'filter',
                        },
                        server: true,
                    },
                ],
            });
            resolveIndexCall();
            expect(Lesson.index).toHaveBeenCalled();
            expect(contentItemEditorLists.load).not.toHaveBeenCalled();
            expect(scope.things).toEqual(things);
        });

        it('should use a regular index call if using server pagination', () => {
            // The fact that we have to mock this out shows that this is an unrealistic situation.  See FIXME above
            Lesson.mapClientSortToServerSort = jest.fn();
            things = [
                {
                    id: 'id1',
                },
            ];
            render(
                '<editable-things-list klass-name="\'Lesson\'" filters="filters" using-server-pagination="true"></editable-things-list>',
                {
                    filters: [
                        {
                            value: {
                                some: 'filter',
                            },
                            default: true,
                            server: true,
                        },
                    ],
                },
            );

            // Force initial load
            scope.filters = [];
            scope.$digest();

            resolveIndexCall();

            expect(Lesson.index).toHaveBeenCalled();
            expect(contentItemEditorLists.load).not.toHaveBeenCalled();
            expect(scope.things).toEqual(things);
        });
    });

    describe('pagination controls, perPage', () => {
        it('should work', () => {
            things = [];
            for (let i = 1; i <= 9; i++) {
                things.push({
                    id: `id${i}`,
                    i,
                });
            }
            render('<editable-things-list klass-name="\'Thing\'" per-page="3"></editable-things-list>');
            scope.sort = {
                column: 'i',
                descending: false,
            };
            resolveIndexCall();
            expectThings(['id1', 'id2', 'id3']);

            SpecHelper.click(elem, 'dir-pagination-controls [name="next"]');
            expectThings(['id4', 'id5', 'id6']);
        });

        it('should show total number of items after filtering', () => {
            things = [
                {
                    id: 'id1',
                    client: '1',
                },
                {
                    id: 'id2',
                    client: '2',
                },
            ];
            render('<editable-things-list filters="filters" klass-name="\'Thing\'"></editable-things-list>', {
                filters: [
                    {
                        value: {
                            client: '1',
                        },
                        server: false,
                    },
                ],
            });
            resolveIndexCall();
            SpecHelper.expectElementText(elem, '.results-found', '1 items found.');

            render('<editable-things-list filters="filters" klass-name="\'Thing\'"></editable-things-list>', {
                filters: [],
            });
            resolveIndexCall();
            SpecHelper.expectElementText(elem, '.results-found', '2 items found.');
        });

        it('should support selecting the perPage option', () => {
            things = [];
            for (let i = 1; i <= 9; i++) {
                things.push({
                    id: `id${i}`,
                    i,
                });
            }
            render(
                '<editable-things-list per-page="perPage" per-page-options="perPageOptions" klass-name="\'Thing\'"></editable-things-list>',
                {
                    perPage: 2,
                    perPageOptions: [2, 4],
                },
            );
            scope.sort = {
                column: 'i',
                descending: false,
            };
            resolveIndexCall();
            SpecHelper.expectElementText(elem, '.results-found', '9 items found.');
            expect(scope.perPage).toEqual(2);
            expectThings(['id1', 'id2']);
            SpecHelper.updateSelect(elem, '.pagination-container .results-per-page-container select', 4);
            expect(scope.perPage).toEqual(4);
            expectThings(['id1', 'id2', 'id3', 'id4']);
        });
    });

    describe('disablePagination', () => {
        it('should work', () => {
            render('<editable-things-list klass-name="\'Thing\'" disable-pagination="true"></editable-things-list>');
            expect(scope.perPage).toBe(Number.MAX_SAFE_INTEGER);
        });
    });

    describe('persistSortKey', () => {
        it('should work', () => {
            things = [];
            for (let i = 1; i <= 9; i++) {
                things.push({
                    id: `id${i}`,
                    i,
                });
            }
            render(
                '<editable-things-list klass-name="\'Thing\'" persist-sort-key="\'key\'" per-page="3" columns="columns"></editable-things-list>',
                {
                    columns: [
                        {
                            prop: 'i',
                        },
                    ],
                },
            );

            scope.sort = {
                column: 'i',
                descending: true,
            };
            resolveIndexCall();
            SpecHelper.click(elem, 'dir-pagination-controls [name="next"]');
            expectThings(['id6', 'id5', 'id4']);

            render(
                '<editable-things-list klass-name="\'Thing\'" persist-sort-key="\'key\'" per-page="3"></editable-things-list>',
            );
            resolveIndexCall();
            expectThings(['id6', 'id5', 'id4']);
        });
    });

    describe('usingServerPagination', () => {
        it('should not do an initial load', () => {
            render(
                '<editable-things-list klass-name="\'Thing\'" using-server-pagination="true"></editable-things-list>',
            );
            expect(Thing.index).not.toHaveBeenCalled();
            SpecHelper.expectElement(elem, '[name="search-message"]');
        });

        it('should not show spinner at first, but then show it in subsequent calls', () => {
            render(
                '<editable-things-list klass-name="\'Thing\'" using-server-pagination="true"></editable-things-list>',
            );
            SpecHelper.expectNoElement(elem, 'front-royal-spinner');

            // Force initial load
            scope.filters = [];
            scope.$digest();

            SpecHelper.expectElement(elem, 'front-royal-spinner');
            resolveIndexCall();
            SpecHelper.expectNoElement(elem, 'front-royal-spinner');
        });

        it('should set the perPage value', () => {
            render(
                '<editable-things-list klass-name="\'Thing\'" using-server-pagination="true"></editable-things-list>',
            );
            expect(scope.perPage).toEqual(100);

            render(
                '<editable-things-list klass-name="\'Thing\'" using-server-pagination="true" index-params="{limit: 42}"></editable-things-list>',
            );
            expect(scope.perPage).toEqual(42);

            render(
                '<editable-things-list klass-name="\'Thing\'" using-server-pagination="true" index-params="{per_page: 42}"></editable-things-list>',
            );
            expect(scope.perPage).toEqual(42);
        });

        it('should make an index call when sort changes', () => {
            render(
                '<editable-things-list klass-name="\'Thing\'" using-server-pagination="true"></editable-things-list>',
            );
            scope.sort.column = 'id';
            scope.sort.descending = !scope.sort.descending;
            scope.$apply();
            expect(Thing.index.mock.calls[0][0].sort).toEqual('id-mapped'); // see mocked mapClientSortToServerSort
            expect(Thing.index.mock.calls[0][0].direction).toEqual('asc');
            Thing.index.mockClear();

            scope.sort.descending = !scope.sort.descending;
            scope.$apply();
            expect(Thing.index.mock.calls[0][0].sort).toEqual('id-mapped'); // see mocked mapClientSortToServerSort
            expect(Thing.index.mock.calls[0][0].direction).toEqual('desc');
            Thing.index.mockClear();

            scope.sort.column = 'changed';
            scope.$apply();
            expect(Thing.index.mock.calls[0][0].sort).toEqual('changed-mapped'); // see mocked mapClientSortToServerSort
            expect(Thing.index.mock.calls[0][0].direction).toEqual('desc');
            Thing.index.mockClear();
        });

        it('should make an index call when page changes', () => {
            things = [];
            for (let i = 1; i <= 9; i++) {
                things.push({
                    id: `id${i}`,
                    i,
                });
            }

            render(
                '<editable-things-list klass-name="\'Thing\'" using-server-pagination="true" index-params="{per_page: 3}"></editable-things-list>',
            );

            // Force initial load
            scope.filters = [];
            scope.$digest();

            expect(Thing.index.mock.calls[0][0].page).toEqual(1);
            expect(Thing.index.mock.calls[0][0].per_page).toEqual(3);
            Thing.index.mockClear();
            resolveIndexCall();

            SpecHelper.click(elem, 'dir-pagination-controls [name="next"]');

            expect(Thing.index.mock.calls[0][0].page).toEqual(2);
            expect(Thing.index.mock.calls[0][0].per_page).toEqual(3);
        });

        it('should update the persisted page param when the filters change', () => {
            things = [];
            for (let i = 1; i <= 9; i++) {
                things.push({
                    id: `id${i}`,
                    i,
                });
            }

            render(
                '<editable-things-list klass-name="\'Thing\'" using-server-pagination="true" persist-sort-key="\'key\'" per-page="3"></editable-things-list>',
            );
            scope.filters = [
                {
                    server: true,
                    value: {
                        some: 'filter',
                    },
                },
            ];
            scope.$apply();
            resolveIndexCall();

            SpecHelper.click(elem, 'dir-pagination-controls [name="next"]');
            expect(ClientStorage.getItem('key_page')).toEqual('2');

            // Reset the page param if the filters have changed, because we can't trust that the new result will have this page
            scope.filters = [
                {
                    server: true,
                    value: {
                        some: 'changed',
                    },
                },
            ];
            scope.$apply();
            expect(ClientStorage.getItem('key_page')).toEqual('1');
        });

        it('should display the total number of items', () => {
            things = [
                {
                    id: 'id1',
                },
            ];
            render(
                '<editable-things-list klass-name="\'Thing\'" using-server-pagination="true"></editable-things-list>',
            );

            // Force initial load
            scope.filters = [];
            scope.$digest();

            expect(Thing.index).toHaveBeenCalled();
            Thing.index.mockClear();
            resolveIndexCall();

            SpecHelper.expectElementText(elem, '.results-found', '42 items found.');
            expect(elem.find('tbody tr').attr('total-items')).toEqual(String(totalCountForServerPagination));
        });

        it('should set listMeta', () => {
            things = [
                {
                    id: 'id1',
                },
            ];
            render(
                '<editable-things-list klass-name="\'Thing\'" using-server-pagination="true"></editable-things-list>',
            );

            // Force initial load
            scope.filters = [];
            scope.$digest();

            resolveIndexCall();

            expect(scope.listMeta).toEqual(responseMeta);
        });

        it('should hide the quick-filter', () => {
            render();
            resolveIndexCall();
            SpecHelper.expectElementNotHidden(elem, '.search-form');
            scope.usingServerPagination = true;
            scope.$apply();
            SpecHelper.expectElementHidden(elem, '.search-form');
        });
    });

    describe('indexParams', () => {
        it('should merge the params into the index call', () => {
            render(
                "<editable-things-list klass-name=\"'Thing'\" index-params=\"{a: 'b', filters: {a: 'b'}}\"></editable-things-list>",
            );
            expect(Thing.index.mock.calls[0][0]).toEqual({
                a: 'b',
                filters: {
                    a: 'b',
                },
            });
        });
    });

    describe('klass, klassName', () => {
        // tbh, I don't know if this is actually required functionality.  But the code
        // seemed to expected it when I was backfilling specs, so I added one.  I guess
        // it would make sense if the things were passed in, but the comment on the
        // `things` property says that it is not expecting them to be passed in.
        it('should not trigger an index call if klass unset', () => {
            render('<editable-things-list ></editable-things-list>');
            expect(Thing.index).not.toHaveBeenCalled();
        });

        it('should set klass if klassName is provided', () => {
            render('<editable-things-list klass-name="\'Thing\'"></editable-things-list>');
            expect(scope.klass).toBe(Thing);
        });
    });

    describe('sort, defaultSort, defaultSortOrder', () => {
        it('should respect defaultSort and defaultSortOrder, and allow for sorting on other columns', () => {
            things = [];
            for (let i = 1; i <= 3; i++) {
                things.push({
                    id: `id${i}`,
                    i,
                    j: 10 - i,
                });
            }

            render(
                '<editable-things-list klass-name="\'Thing\'" default-sort="defaultSort" default-sort-order="defaultSortOrder" columns="columns"></editable-things-list>',
                {
                    columns: [
                        {
                            prop: 'i',
                        },
                        {
                            prop: 'j',
                        },
                    ],
                    defaultSort: 'j',
                    defaultSortOrder: 'desc',
                },
            );
            resolveIndexCall();
            // j desc
            expectThings(['id1', 'id2', 'id3']);

            // i asc
            SpecHelper.click(elem, 'th[data-id="i"]');
            expectThings(['id1', 'id2', 'id3']);

            // i desc
            SpecHelper.click(elem, 'th[data-id="i"]');
            expectThings(['id3', 'id2', 'id1']);
        });

        it('should sort correctly when a function definition is used for a column', () => {
            things = [];
            for (let i = 1; i <= 3; i++) {
                things.push({
                    id: `id${i}`,
                    i,
                });
            }

            render('<editable-things-list klass-name="\'Thing\'" columns="columns"></editable-things-list>', {
                columns: [
                    {
                        id: 'ifunc',
                        prop(thing) {
                            return thing.i;
                        },
                    },
                ],
            });
            resolveIndexCall();

            // i asc
            SpecHelper.click(elem, 'th[data-id="ifunc"]');
            expectThings(['id1', 'id2', 'id3']);

            // i desc
            SpecHelper.click(elem, 'th[data-id="ifunc"]');
            expectThings(['id3', 'id2', 'id1']);
        });

        describe('emptyToEnd', () => {
            beforeEach(() => {
                things = [
                    {
                        id: 'id1',
                        prop: 'a',
                    },
                    {
                        id: 'id2',
                        prop: undefined,
                    },
                    {
                        id: 'id3',
                        prop: 'b',
                    },
                ];
            });

            it('should move empties to end by default', () => {
                render('<editable-things-list klass-name="\'Thing\'" columns="columns"></editable-things-list>', {
                    columns: [
                        {
                            prop: 'prop',
                        },
                    ],
                });
                scope.sort = {
                    column: 'prop',
                    descending: false,
                };
                resolveIndexCall();
                expectThings(['id1', 'id3', 'id2']);

                scope.sort = {
                    column: 'prop',
                    descending: true,
                };
                scope.$apply();
                expectThings(['id3', 'id1', 'id2']);
            });

            it('should work with a custom value', () => {
                render('<editable-things-list klass-name="\'Thing\'" columns="columns"></editable-things-list>', {
                    columns: [
                        {
                            prop: 'id',
                            callbacks: {
                                emptyToEnd(thing) {
                                    return thing.prop;
                                },
                            },
                        },
                    ],
                });
                scope.sort = {
                    column: 'id',
                    descending: false,
                };
                resolveIndexCall();
                expectThings(['id1', 'id3', 'id2']);

                scope.sort = {
                    column: 'id',
                    descending: true,
                };
                scope.$apply();
                expectThings(['id3', 'id1', 'id2']);
            });
        });

        describe('column#sortEmptyAs0', () => {
            it('should treat empty values as though they were zeroes', () => {
                things = [
                    {
                        id: 'id1',
                        prop: 1,
                    },
                    {
                        id: 'id2',
                        prop: undefined,
                    },
                    {
                        id: 'id3',
                        prop: 2,
                    },
                ];

                render('<editable-things-list klass-name="\'Thing\'" columns="columns"></editable-things-list>', {
                    columns: [
                        {
                            prop: 'prop',
                            sortEmptyAs0: true,
                        },
                    ],
                });
                scope.sort = {
                    column: 'prop',
                    descending: false,
                };
                resolveIndexCall();
                expectThings(['id2', 'id1', 'id3']);

                scope.sort = {
                    column: 'prop',
                    descending: true,
                };
                scope.$apply();
                expectThings(['id3', 'id1', 'id2']);
            });
        });
    });

    describe('editing', () => {
        it('should show the edit-directive, passing along all expected stuff', () => {
            things = [
                {
                    id: 'id1',
                },
            ];
            render(`<editable-things-list klass-name="'Thing'"
                edit-directive="'my-directive'"
                edit-directive-passthrough="{a: 1}"
                ></editable-things-list>`);
            SpecHelper.expectNoElement(elem, 'my-directive');
            resolveIndexCall();
            scope.visibleThing = things[0];
            scope.listMeta = 'listMeta';
            scope.containerViewModel = 'containerViewModel';
            scope.$apply();
            SpecHelper.expectNoElement(elem, '[name="results-wrapper"]');
            SpecHelper.expectNoElement(elem, 'front-royal-wrapper');
            SpecHelper.expectElement(elem, 'my-directive');

            assertPassedValue(things[0], 'thing');
            assertPassedValue(things, 'things');
            assertPassedValue(things, 'things-on-current-page');
            assertPassedValue('listMeta', 'list-meta');
            assertPassedValue('containerViewModel', 'container-view-model');
            assertPassedValue(
                {
                    a: 1,
                },
                'passthrough',
            );

            jest.spyOn(scope, 'showThing').mockImplementation(() => {});
            triggerCallback('go-back');
            expect(scope.showThing).toHaveBeenCalledWith(null);

            jest.spyOn(scope, 'onCreated').mockImplementation(() => {});
            triggerCallback('created', {
                $thing: 'newThing',
            });
            expect(scope.onCreated).toHaveBeenCalledWith('newThing');

            jest.spyOn(scope, 'onDestroyed').mockImplementation(() => {});
            triggerCallback('destroyed', {
                $thing: things[0],
            });
            expect(scope.onDestroyed).toHaveBeenCalledWith(things[0]);
        });

        it('should support selecting  a thing from the list', () => {
            const search = {};
            jest.spyOn($location, 'search').mockImplementation(() => search);
            things = [
                {
                    id: 'id1',
                },
            ];
            render('<editable-things-list klass-name="\'Thing\'" editing-is-open="isEditing"></editable-things-list>');
            resolveIndexCall();
            expect(scope.visibleThing).toBe(null);
            expect(parentScope.isEditing).toBe(false);
            search['thing-id'] = 'id1';
            scope.$apply();
            expect(scope.visibleThing).toBe(things[0]);
            expect(parentScope.isEditing).toBe(true);
        });

        it('should make a show call to find a missing thing', () => {
            const search = {
                'thing-id': 'id2',
            };
            jest.spyOn($location, 'search').mockImplementation(() => search);
            thingFromShowCall = {
                id: 'id2',
            };
            render('<editable-things-list klass-name="\'Thing\'" editing-is-open="isEditing"></editable-things-list>');
            expect(Thing.index).not.toHaveBeenCalled();
            SpecHelper.expectElement(elem, 'front-royal-spinner');
            resolveShowCall();
            expect(scope.visibleThing).toBe(thingFromShowCall);
            expect(parentScope.isEditing).toBe(true);
        });

        it('should not make a show call if configured not to do so', () => {
            const search = {
                'thing-id': 'id2',
            };
            jest.spyOn($location, 'search').mockImplementation(() => search);
            things = [
                {
                    id: 'id2',
                },
            ];
            render(
                '<editable-things-list klass-name="\'Thing\'" editing-is-open="isEditing" supports-show-requests="false"></editable-things-list>',
            );
            expect(Thing.index).toHaveBeenCalled();
            SpecHelper.expectElement(elem, 'front-royal-spinner');
            resolveIndexCall();
            expect(scope.visibleThing.id).toBe('id2');
            expect(parentScope.isEditing).toBe(true);
        });

        function getPassedValue(attr) {
            const myDirective = SpecHelper.expectElement(elem, 'my-directive');
            return scope.$eval(myDirective.attr(attr));
        }

        function triggerCallback(attr, locals) {
            const myDirective = SpecHelper.expectElement(elem, 'my-directive');
            return scope.$eval(myDirective.attr(attr), locals);
        }

        function assertPassedValue(expectedValue, attr) {
            const passedValue = getPassedValue(attr);
            expect(passedValue).toEqual(expectedValue);
        }
    });

    describe('exportData', () => {
        it('should include filtered records', () => {
            things = [
                {
                    id: 'id1',
                    some: 'thing',
                    client: '1',
                },
                {
                    id: 'id2',
                    some: 'thing else',
                    client: '2',
                },
            ];
            render('<editable-things-list filters="filters" klass-name="\'Thing\'"></editable-things-list>', {
                filters: [
                    {
                        value: {
                            client: '1',
                        },
                        server: false,
                    },
                ],
            });
            resolveIndexCall();
            expect(scope.exportData.filteredRecords).toEqual([things[0]]);
        });
    });

    describe('allowCreate', () => {
        it('should show the create button only if allowCreate is true', () => {
            render('<editable-things-list klass-name="\'Thing\'" allow-create="allowCreate"></editable-things-list>', {
                allowCreate: false,
            });
            resolveIndexCall();
            SpecHelper.expectNoElement(elem, '[name="create"]');
            parentScope.allowCreate = true;
            scope.$apply();
            jest.spyOn(scope, 'showThing').mockImplementation(() => {});
            SpecHelper.click(elem, '[name="create"]');
            expect(scope.showThing).toHaveBeenCalledWith('create');
        });
    });

    describe('creating, onCreated, onCreateCallback', () => {
        it('should work with an onCreateCallback', () => {
            render(
                '<editable-things-list klass-name="\'Thing\'" allow-create="allowCreate" on-create-callback="onCreateCallback"></editable-things-list>',
                {
                    allowCreate: false,
                    onCreateCallback: jest.fn(),
                },
            );
            const newThing = Thing.new();
            resolveIndexCall();

            // In the wild, the edit-directive will trigger the onCreated callback
            // (this is tested above in the 'editing' block).  Here, we just call it
            // directly
            jest.spyOn($location, 'search').mockImplementation(() => {});
            scope.onCreated(newThing);
            expect(_.last(scope.things)).toEqual(newThing);
            expect(parentScope.onCreateCallback).toHaveBeenCalledWith(newThing);
            expect($location.search).not.toHaveBeenCalled();
        });

        it('should work with no onCreateCallback', () => {
            render('<editable-things-list klass-name="\'Thing\'" allow-create="allowCreate" ></editable-things-list>', {
                allowCreate: false,
            });
            const newThing = Thing.new({
                id: 'asdasda',
            });
            resolveIndexCall();

            // In the wild, the edit-directive will trigger the onCreated callback
            // (this is tested above in the 'editing' block).  Here, we just call it
            // directly
            jest.spyOn($location, 'search').mockImplementation(() => {});
            scope.onCreated(newThing);
            expect(_.last(scope.things)).toEqual(newThing);
            expect($location.search).toHaveBeenCalledWith('thing-id', newThing.id);
        });
    });

    describe('onLoadCallback', () => {
        it('should work', () => {
            things = [
                {
                    id: 'id1',
                },
            ];
            render(
                '<editable-things-list klass-name="\'Thing\'" on-load-callback="onLoadCallback"></editable-things-list>',
                {
                    onLoadCallback: jest.fn(),
                },
            );
            expect(parentScope.onLoadCallback).not.toHaveBeenCalled();
            resolveIndexCall();
            expect(parentScope.onLoadCallback).toHaveBeenCalledWith(things);
        });
    });

    describe('customTip', () => {
        it('should work', () => {
            render('<editable-things-list klass-name="\'Thing\'" custom-tip="customTip"></editable-things-list>', {
                customTip: 'Do not push the red button',
            });
            resolveIndexCall();
            SpecHelper.expectElementText(elem, '.custom-tip-container', parentScope.customTip);
        });
    });

    describe('clicking a row, onItemClick', () => {
        it('should allow for overriding what happens when clicking on an item', () => {
            things = [
                {
                    id: 'id1',
                },
            ];
            render('<editable-things-list klass-name="\'Thing\'" on-item-click="onItemClick"></editable-things-list>', {
                onItemClick: jest.fn(),
            });
            resolveIndexCall();
            SpecHelper.click(elem, 'tbody tr', 0);
            expect(parentScope.onItemClick).toHaveBeenCalled();
        });

        it('should call showThing by default', () => {
            things = [
                {
                    id: 'id1',
                },
            ];
            render();
            resolveIndexCall();
            jest.spyOn(scope, 'showThing').mockImplementation(() => {});
            SpecHelper.click(elem, 'tbody tr', 0);
            expect(scope.showThing).toHaveBeenCalledWith(things[0]);
        });
    });

    describe('showExportCsv', () => {
        it('should work', () => {
            render(
                '<editable-things-list klass-name="\'Thing\'" show-export-csv="showExportCsv"></editable-things-list>',
                {
                    showExportCsv: false,
                },
            );
            resolveIndexCall();
            SpecHelper.expectNoElement(elem, '.export-link');
            parentScope.showExportCsv = true;
            scope.$apply();
            SpecHelper.expectElement(elem, '.export-link');
        });
    });

    describe('exportCSV, csvExportColumns, column#csvOnly', () => {
        let dataPassedToPapa;

        beforeEach(() => {
            jest.spyOn(Papa, 'unparse').mockImplementation(a => {
                dataPassedToPapa = a;
                return a;
            });
            things = [
                {
                    id: 'id1',
                    prop: 'a',
                },
                {
                    id: 'id2',
                    prop: 'b',
                },
            ];
        });

        it('should call exportCSV when .export-link is clicked', () => {
            render(
                '<editable-things-list klass-name="\'Thing\'" has-unsaved-changes="hasUnsavedChanges" columns="[]"></editable-things-list>',
                {
                    hasUnsavedChanges: true,
                },
            );
            jest.spyOn(scope, 'exportCSV').mockImplementation(() => {});
            resolveIndexCall();
            SpecHelper.click(elem, '.export-link');
            expect(scope.exportCSV).toHaveBeenCalled();
        });

        it('should show confirm if hasUnsavedChanges', () => {
            render(
                '<editable-things-list klass-name="\'Thing\'" has-unsaved-changes="hasUnsavedChanges" columns="[]"></editable-things-list>',
                {
                    hasUnsavedChanges: true,
                },
            );
            jest.spyOn($window, 'confirm').mockReturnValue(false);
            resolveIndexCall();
            scope.exportCSV();
            expect($window.confirm).toHaveBeenCalledWith(
                'You have unsaved changes. Are you sure you would like to proceed?',
            );
            $window.confirm.mockClear();

            parentScope.hasUnsavedChanges = false;
            scope.$apply();
            scope.exportCSV();
            expect($window.confirm).not.toHaveBeenCalled();
        });

        it('should convert header columns from html to strings', () => {
            render('<editable-things-list klass-name="\'Thing\'" columns="columns"></editable-things-list>', {
                columns: [
                    {
                        prop: 'id',
                        label: 'ID',
                    },
                    {
                        prop: 'prop',
                        label: '<h1>bang</h1>',
                    },
                ],
            });
            resolveIndexCall();
            scope.exportCSV();
            expect(dataPassedToPapa[0]).toEqual(['ID', 'bang']);
        });

        it('should add a row for each filtered record', () => {
            render('<editable-things-list klass-name="\'Thing\'" columns="columns"></editable-things-list>', {
                columns: [
                    {
                        prop: 'id',
                        label: 'ID',
                        type: 'text',
                    },
                    {
                        prop: 'prop',
                        label: '<h1>bang</h1>',
                        type: 'text',
                    },
                ],
            });
            jest.spyOn(scope, 'getFormattedColumnValue').mockImplementation(column => `value for ${column.prop}`);
            resolveIndexCall();
            scope.exportData.filteredRecords = [things[0]];
            scope.exportCSV();
            // one header row and one thing row
            expect(dataPassedToPapa.length).toEqual(2);
            expect(dataPassedToPapa[1]).toEqual(['value for id', 'value for prop']);
        });

        it('should respect csvExportColumns', () => {
            const columns = [
                {
                    prop: 'id',
                    label: 'ID',
                    type: 'text',
                },
                {
                    prop: 'prop',
                    label: '<h1>bang</h1>',
                    type: 'text',
                },
            ];
            render(
                '<editable-things-list klass-name="\'Thing\'" columns="columns" csv-export-columns="csvExportColumns"></editable-things-list>',
                {
                    csvExportColumns: [columns[0]],
                    columns,
                },
            );
            resolveIndexCall();
            scope.exportData.filteredRecords = [things[0]];
            scope.exportCSV();

            // one header row and one thing row
            expect(dataPassedToPapa.length).toEqual(2);
            expect(dataPassedToPapa[0]).toEqual(['ID']);
            expect(dataPassedToPapa[1]).toEqual([things[0].id]);
        });

        it('should hide columns marked as csvOnly from the web view', () => {
            render('<editable-things-list klass-name="\'Thing\'" columns="columns"></editable-things-list>', {
                columns: [
                    {
                        prop: 'id',
                        label: 'ID',
                    },
                    {
                        prop: 'prop',
                        label: '<h1>bang</h1>',
                        csvOnly: true,
                    },
                ],
            });
            resolveIndexCall();
            scope.exportCSV();
            expect(dataPassedToPapa[0]).toEqual(['ID', 'bang']);
            SpecHelper.expectNoElement(elem, 'tbody td[data-id="prop"]');
        });
    });

    describe('things', () => {
        it('should be accessible to parentScope', () => {
            things = [
                {
                    id: 'id1',
                },
            ];
            render('<editable-things-list klass-name="\'Thing\'" things="childThings"></editable-things-list>');
            resolveIndexCall();
            expect(parentScope.childThings).toEqual(things);
        });
    });

    describe('thingsOnCurrentPage', () => {
        let expectedThingsOnCurrentPage;
        let mockItemsPerPageFilter;

        beforeEach(() => {
            things = [];
            for (let i = 1; i <= 9; i++) {
                things.push({
                    id: `id${i}`,
                    i,
                });
            }
            expectedThingsOnCurrentPage = things.slice(0, 2);
            mockItemsPerPageFilter = jest.fn(() => expectedThingsOnCurrentPage);
            const $filter = $injector.get('$filter');
            const mockFilterService = filter => {
                if (filter === 'itemsPerPage') {
                    return mockItemsPerPageFilter;
                }
                return $filter(filter);
            };
            const originalInjectorGet = $injector.get;
            jest.spyOn($injector, 'get').mockImplementation(service => {
                if (service === '$filter') {
                    return mockFilterService;
                }
                return originalInjectorGet(service);
            });
        });

        it('should be caclulated by itemsPerPage filter from paginationService when pagination instance isRegistered', () => {
            render('<editable-things-list per-page="perPage" klass-name="\'Thing\'"></editable-things-list>', {
                perPage: 2,
            });
            scope.sort = {
                column: 'i',
                descending: false,
            };
            resolveIndexCall();

            const PaginationService = $injector.get('paginationService');
            jest.spyOn(PaginationService, 'isRegistered').mockReturnValue(true);

            const thingsOnCurrentPage = scope.thingsOnCurrentPage;

            expect(mockItemsPerPageFilter).toHaveBeenCalledWith(
                scope.filteredThings,
                scope.perPage,
                scope.paginationId,
            );
            expect(PaginationService.isRegistered).toHaveBeenCalledWith(scope.paginationId);
            expect(thingsOnCurrentPage).toEqual(expectedThingsOnCurrentPage);
        });

        it('should fallback to calculating value using currentPage, perPage, and the things directly when pagination instance is not registered with paginationService', () => {
            render('<editable-things-list per-page="perPage" klass-name="\'Thing\'"></editable-things-list>', {
                perPage: 2,
            });
            scope.sort = {
                column: 'i',
                descending: false,
            };
            resolveIndexCall();

            const PaginationService = $injector.get('paginationService');
            jest.spyOn(PaginationService, 'isRegistered').mockReturnValue(false);
            expect(scope.currentPage).toEqual(1);

            const thingsOnCurrentPage = scope.thingsOnCurrentPage;

            expect(PaginationService.isRegistered).toHaveBeenCalledWith(scope.paginationId);
            expect(mockItemsPerPageFilter).not.toHaveBeenCalled();
            expect(thingsOnCurrentPage).toEqual(expectedThingsOnCurrentPage);
        });
    });

    describe('$location.search() watcher', () => {
        it('should ignore id changes that are not relevant to the current path', () => {
            let path = 'startingPath';
            const search = {};
            jest.spyOn($location, 'path').mockImplementation(() => path);
            jest.spyOn($location, 'search').mockImplementation(() => search);
            things = [
                {
                    id: 'id1',
                },
            ];
            render('<editable-things-list klass-name="\'Thing\'" editing-is-open="isEditing"></editable-things-list>');
            resolveIndexCall();
            expect(parentScope.isEditing).toBe(false);
            expect(scope.visibleThing).toBe(null);
            path = 'differentPath';
            search['thing-id'] = 'id1';
            scope.$apply();
            expect(scope.visibleThing).toBe(null);
            expect(parentScope.isEditing).toBe(false);
        });

        it('should create a new thing', () => {
            const search = {};
            jest.spyOn($location, 'search').mockImplementation(() => search);
            things = [
                {
                    id: 'id1',
                },
            ];
            render('<editable-things-list klass-name="\'Thing\'" editing-is-open="isEditing"></editable-things-list>');
            resolveIndexCall();
            expect(scope.visibleThing).toBe(null);
            expect(parentScope.isEditing).toBe(false);
            search['thing-id'] = 'create';
            scope.$apply();
            expect(scope.visibleThing).not.toBe(null);
            expect(scope.visibleThing.id).toBeUndefined();
            expect(parentScope.isEditing).toBe(true);
        });

        describe('embedded', () => {
            // NOTE: the custom embedded behavior inside of showThing is tested
            // in the showThing block

            it('should ignore id queryparam', () => {
                things = [
                    {
                        id: 'id1',
                    },
                ];
                jest.spyOn($location, 'search').mockReturnValue({
                    id: 'id1',
                });
                render('<editable-things-list klass-name="\'Thing\'" embedded="true"></editable-things-list>');
                resolveIndexCall();
                expect(scope.visibleThing).toBe(null);

                // I don't quite understand it, but in the code we're setting showSpinner to false
                // so I figured I'd toss this in here
                SpecHelper.expectNoElement(elem, 'front-royal-spinner');
            });
        });
    });

    describe('with Lesson klass', () => {
        beforeEach(() => {
            $injector.get('LessonFixtures');
        });

        it('should have special handling for integration with RecentlyEditedLessons', () => {
            const lessons = [Lesson.fixtures.getInstance()];
            jest.spyOn(RecentlyEditedLessons.prototype, 'lessons', 'get').mockReturnValue(lessons);
            render('<editable-things-list klass-name="\'Lesson\'" filters="filters"></editable-things-list>', {
                filters: [
                    {
                        recentlyViewed: true,
                    },
                ],
            });
            expect(Lesson.index).not.toHaveBeenCalled();
            expect(scope.things).toEqual(lessons);
        });
        it('should not use RecentlyEditedLessons if no filter setup', () => {
            // eslint-disable-next-line no-multi-assign
            const lessons = (things = [Lesson.fixtures.getInstance()]);
            render('<editable-things-list klass-name="\'Lesson\'" filters="filters"></editable-things-list>', {
                filters: [{}],
            });
            resolveIndexCall();
            expect(scope.things).toEqual(lessons);
        });
    });

    describe('with Playlist klass', () => {
        it('should have a star', () => {
            const Playlist = $injector.get('Playlist');
            $injector.get('PlaylistFixtures');

            things = [
                Playlist.fixtures.getInstance({
                    programs_included_in: ['mba'],
                }),
                Playlist.fixtures.getInstance({
                    programs_included_in: ['emba', 'mba'],
                }),
                Playlist.fixtures.getInstance({
                    programs_included_in: ['emba'],
                }),
                Playlist.fixtures.getInstance({
                    programs_included_in: ['the_business_certificate'],
                }),
            ];
            Playlist.expect('index').returns(things);
            render("<editable-things-list klass-name=\"'Playlist'\" columns='columns'></editable-things-list>", {
                columns: [
                    {
                        prop: 'title',
                    },
                ],
            });
            Playlist.flush('index');

            // Which stars are shown is dependent on which programs each playlist is in.
            // These assertions line up with the values of programs_included_in setup above
            SpecHelper.expectElement(elem, `tbody tr[data-id="${things[0].id}"] .fa.fa-star`);
            SpecHelper.expectNoElement(elem, `tbody tr[data-id="${things[0].id}"] .far.fa-star`);

            SpecHelper.expectElement(elem, `tbody tr[data-id="${things[1].id}"] .fa.fa-star`);
            SpecHelper.expectElement(elem, `tbody tr[data-id="${things[1].id}"] .far.fa-star`);

            SpecHelper.expectNoElement(elem, `tbody tr[data-id="${things[2].id}"] .fa.fa-star`);
            SpecHelper.expectElement(elem, `tbody tr[data-id="${things[2].id}"] .far.fa-star`);

            SpecHelper.expectNoElement(elem, `tbody tr[data-id="${things[3].id}"] .fa.fa-star`);
            SpecHelper.expectNoElement(elem, `tbody tr[data-id="${things[3].id}"] .far.fa-star`);
        });
    });

    describe('header classes', () => {
        it('should add classes from column config', () => {
            render("<editable-things-list klass-name=\"'Thing'\" columns='columns'></editable-things-list>", {
                columns: [
                    {
                        prop: 'a',
                        classes: 'my-class',
                    },
                    {
                        prop: 'b',
                    },
                ],
            });
            resolveIndexCall();
            SpecHelper.expectHasClass(elem, 'thead th[data-id=a]', 'my-class');
            SpecHelper.expectDoesNotHaveClass(elem, 'thead th[data-id=b]', 'my-class');
        });
    });

    describe('headerSpanClasses', () => {
        it('should add sortable classes', () => {
            render("<editable-things-list klass-name=\"'Thing'\" columns='columns'></editable-things-list>", {
                columns: [
                    {
                        prop: 'a',
                        sortable: false,
                    },
                    {
                        prop: 'b',
                    },
                ],
            });
            jest.spyOn(scope, 'sortClasses').mockImplementation(columnProp => `sort-${columnProp}`);
            resolveIndexCall();
            expect(scope.sortClasses).not.toHaveBeenCalledWith('a');
            expect(scope.sortClasses).toHaveBeenCalledWith('b');
            SpecHelper.expectHasClass(elem, 'thead th[data-id=a] > span', 'not-sortable');
            SpecHelper.expectDoesNotHaveClass(elem, 'thead th[data-id=a] > span', 'sort-a');

            SpecHelper.expectDoesNotHaveClass(elem, 'thead th[data-id=b] > span', 'not-sortable');
            SpecHelper.expectHasClass(elem, 'thead th[data-id=b] > span', 'sort-b');
        });

        it('should add sortable classes when using a function as the prop definition', () => {
            render("<editable-things-list klass-name=\"'Thing'\" columns='columns'></editable-things-list>", {
                columns: [
                    {
                        id: 'id',
                        prop(thing) {
                            return thing.id;
                        },
                    },
                ],
            });
            jest.spyOn(scope, 'sortClasses').mockImplementation(columnProp => `sort-${columnProp}`);
            resolveIndexCall();
            expect(scope.sortClasses).toHaveBeenCalledWith('id');
            SpecHelper.expectDoesNotHaveClass(elem, 'thead th[data-id=id] > span', 'not-sortable');
            SpecHelper.expectHasClass(elem, 'thead th[data-id=id] > span', 'sort-id');
        });
    });

    describe('columnClasses', () => {
        // NOTE: the adding of text-center is tested with the individual column types below
        it('should add classes from column config', () => {
            things = [
                {
                    id: 'id1',
                },
            ];
            render("<editable-things-list klass-name=\"'Thing'\" columns='columns'></editable-things-list>", {
                columns: [
                    {
                        prop: 'a',
                        classes: 'my-class',
                    },
                    {
                        prop: 'b',
                    },
                ],
            });
            resolveIndexCall();
            SpecHelper.expectHasClass(elem, 'tbody td[data-id=a]', 'my-class');
            SpecHelper.expectDoesNotHaveClass(elem, 'tbody td[data-id=b]', 'my-class');
        });
    });

    describe('onDestroyed', () => {
        it('should remove the thing from the list', () => {
            things = [
                {
                    id: 'id1',
                },
                {
                    id: 'id2',
                },
            ];
            render();
            resolveIndexCall();
            // in the wild, onDestroyed would be called from the edit-directive.  That
            // is tested above in the edit-directive block.  Here we just call it directly.
            // (It can also be called from the event observer tested in 'destroy watcher')
            scope.onDestroyed(things[0]);
            expect(scope.things).toEqual([things[1]]);
        });
    });

    describe('destroy watcher', () => {
        it('should call onDestroyed when admin:klassNameDestroyed is broadcasted', () => {
            render();
            resolveIndexCall();
            jest.spyOn(scope, 'onDestroyed').mockImplementation(() => {});
            // in the wild, onDestroyed would be called from the edit-directive.  That
            // is tested above in the edit-directive block.  Here we just call it directly
            scope.$broadcast('admin:ThingDestroyed', 'thing');
            expect(scope.onDestroyed).toHaveBeenCalledWith('thing');
        });
    });

    describe('showThing', () => {
        it('should work with create', () => {
            render();
            jest.spyOn($location, 'search').mockReturnValue({});
            scope.showThing('create');
            expect($location.search).toHaveBeenCalledWith('thing-id', 'create');
        });

        it('should not do anything on create if !allowCreate', () => {
            render('<editable-things-list klass-name="\'Thing\'" allow-create="false"></editable-things-list>');
            jest.spyOn($location, 'search').mockReturnValue({});
            scope.showThing('create');
            expect($location.search).not.toHaveBeenCalled();
        });

        it('should work with a thing', () => {
            render();
            jest.spyOn($location, 'search').mockReturnValue({});
            scope.showThing({
                id: 'someId',
            });
            expect($location.search).toHaveBeenCalledWith('thing-id', 'someId');
        });

        it('should not do anything with a thing if allowEdit is false', () => {
            render('<editable-things-list klass-name="\'Thing\'" allow-edit="false"></editable-things-list>');
            jest.spyOn($location, 'search').mockReturnValue({});
            scope.showThing({
                id: 'someId',
            });
            expect($location.search).toHaveBeenCalledWith('thing-id', null);
        });

        it('should remove the existing id', () => {
            render();
            jest.spyOn($location, 'search').mockReturnValue({});
            scope.showThing();
            expect($location.search).toHaveBeenCalledWith('thing-id', null);
        });
    });

    describe('column cell rendering', () => {
        describe('types', () => {
            it('should work with checkIfTrue', () => {
                things = [
                    {
                        id: 'id1',
                        prop: true,
                    },
                    {
                        id: 'id2',
                        prop: false,
                    },
                    {
                        id: 'id3',
                        prop: null,
                    },
                ];
                render('<editable-things-list klass-name="\'Thing\'" columns="columns"><editable-things-list>', {
                    columns: [
                        {
                            prop: 'prop',
                            type: 'checkIfTrue',
                        },
                    ],
                });
                resolveIndexCall();
                const td1 = SpecHelper.expectElement(elem, 'tbody tr[data-id="id1"] td[data-id="prop"]');
                expect(td1.hasClass('text-center')).toBe(true);
                SpecHelper.expectElement(td1, 'i.fa.fa-check');

                const td2 = SpecHelper.expectElement(elem, 'tbody tr[data-id="id2"] td[data-id="prop"]');
                expect(td2.hasClass('text-center')).toBe(true);
                SpecHelper.expectNoElement(td2, 'i.fa.fa-check');

                const td3 = SpecHelper.expectElement(elem, 'tbody tr[data-id="id3"] td[data-id="prop"]');
                expect(td3.hasClass('text-center')).toBe(true);
                SpecHelper.expectNoElement(td3, 'i.fa.fa-check');
            });

            it('should work with acceptedRejected', () => {
                things = [
                    {
                        id: 'id1',
                        prop: 'accepted',
                    },
                    {
                        id: 'id2',
                        prop: 'rejected',
                    },
                    {
                        id: 'id3',
                        prop: null,
                    },
                ];
                render('<editable-things-list klass-name="\'Thing\'" columns="columns"><editable-things-list>', {
                    columns: [
                        {
                            prop: 'prop',
                            type: 'acceptedRejected',
                        },
                    ],
                });
                resolveIndexCall();
                const td1 = SpecHelper.expectElement(elem, 'tbody tr[data-id="id1"] td[data-id="prop"]');
                expect(td1.hasClass('text-center')).toBe(true);
                SpecHelper.expectElement(td1, 'i.fa.fa-check');

                const td2 = SpecHelper.expectElement(elem, 'tbody tr[data-id="id2"] td[data-id="prop"]');
                expect(td2.hasClass('text-center')).toBe(true);
                SpecHelper.expectElement(td2, 'i.fa.fa-remove');

                const td3 = SpecHelper.expectElement(elem, 'tbody tr[data-id="id3"] td[data-id="prop"]');
                expect(td3.hasClass('text-center')).toBe(true);
                SpecHelper.expectNoElement(td3, 'i.fa.fa-check');
                SpecHelper.expectNoElement(td3, 'i.fa.fa-remove');
            });

            it('should work with custom type and a template', () => {
                things = [
                    {
                        id: 'id1',
                    },
                ];
                render('<editable-things-list klass-name="\'Thing\'" columns="columns"><editable-things-list>', {
                    columns: [
                        {
                            prop: 'prop',
                            type: 'custom',
                            template: '<div>{{thing.id}}</div>',
                        },
                    ],
                });
                resolveIndexCall();
                const td1 = SpecHelper.expectElement(elem, 'tbody tr[data-id="id1"] td[data-id="prop"]');
                SpecHelper.expectElementText(td1, 'div', 'id1');
            });

            it('should work with custom type and a templateUrl', () => {
                const $templateCache = $injector.get('$templateCache');
                $templateCache.put('/path/to/template', '<div>{{thing.id}}</div>');
                things = [
                    {
                        id: 'id1',
                    },
                ];
                render('<editable-things-list klass-name="\'Thing\'" columns="columns"><editable-things-list>', {
                    columns: [
                        {
                            prop: 'prop',
                            type: 'custom',
                            templateUrl: '/path/to/template',
                        },
                    ],
                });
                resolveIndexCall();
                const td1 = SpecHelper.expectElement(elem, 'tbody tr[data-id="id1"] td[data-id="prop"]');
                SpecHelper.expectElementText(td1, 'div', 'id1');
            });

            it('should call getFormattedColumnValue with any other type', () => {
                things = [
                    {
                        id: 'id1',
                    },
                ];
                render('<editable-things-list klass-name="\'Thing\'" columns="columns"><editable-things-list>', {
                    columns: [
                        {
                            prop: 'prop',
                        },
                    ],
                });
                jest.spyOn(scope, 'getFormattedColumnValue').mockReturnValue('formatted');
                resolveIndexCall();

                SpecHelper.expectElementText(elem, 'tbody tr[data-id="id1"] td[data-id="prop"]', 'formatted');
                expect(scope.getFormattedColumnValue).toHaveBeenCalledWith(parentScope.columns[0], things[0], 'html');
            });
        });

        it('should work with a function definition for the prop', () => {
            things = [
                {
                    id: 'id1',
                    subthing: {
                        a: 'b',
                    },
                },
            ];
            render('<editable-things-list klass-name="\'Thing\'" columns="columns"><editable-things-list>', {
                columns: [
                    {
                        id: 'my-col',
                        prop(thing) {
                            return thing.subthing.a;
                        },
                        type: 'text',
                    },
                ],
            });
            resolveIndexCall();
            SpecHelper.expectElementText(elem, 'tbody tr[data-id="id1"] td[data-id="my-col"]', 'b');
        });
    });

    describe('getFormattedColumnValue', () => {
        it('should work with time type', () => {
            const amDateFormat = $injector.get('$filter')('amDateFormat');
            const date = new Date();
            things = [
                {
                    id: 'id1',
                    prop: date.getTime() / 1000,
                },
            ];
            render('<editable-things-list klass-name="\'Thing\'" columns="columns"><editable-things-list>', {
                columns: [
                    {
                        prop: 'prop',
                        type: 'time',
                    },
                ],
            });
            resolveIndexCall();
            expect(scope.getFormattedColumnValue(parentScope.columns[0], things[0], 'html')).toEqual(
                amDateFormat(date, 'MM/DD/YY HH:mm'),
            );
        });

        it('should work with perc100 type', () => {
            things = [
                {
                    id: 'id1',
                    prop: 42.6122,
                },
            ];
            render('<editable-things-list klass-name="\'Thing\'" columns="columns"><editable-things-list>', {
                columns: [
                    {
                        prop: 'prop',
                        type: 'perc100',
                    },
                ],
            });
            resolveIndexCall();
            expect(scope.getFormattedColumnValue(parentScope.columns[0], things[0], 'html')).toEqual('42.6122%');
        });

        it('should work with perc1 type', () => {
            things = [
                {
                    id: 'id1',
                    prop: 0.426122,
                },
            ];
            render('<editable-things-list klass-name="\'Thing\'" columns="columns"><editable-things-list>', {
                columns: [
                    {
                        prop: 'prop',
                        type: 'perc1',
                    },
                ],
            });
            resolveIndexCall();
            expect(scope.getFormattedColumnValue(parentScope.columns[0], things[0], 'html')).toEqual('43%');
            expect(scope.getFormattedColumnValue(parentScope.columns[0], things[0], 'csv')).toEqual('42.6122%');
        });

        it('should work with commaSeparatedList type', () => {
            things = [
                {
                    id: 'id1',
                    prop: ['a', 'b', 'c'],
                },
            ];
            render('<editable-things-list klass-name="\'Thing\'" columns="columns"><editable-things-list>', {
                columns: [
                    {
                        prop: 'prop',
                        type: 'commaSeparatedList',
                    },
                ],
            });
            resolveIndexCall();
            expect(scope.getFormattedColumnValue(parentScope.columns[0], things[0], 'html')).toEqual('a, b, c');
        });

        // In web, checkIfTrue does not hit getFormattedColumnValue, but when preparing
        // csv, it does
        it('should work with checkIfTrue', () => {
            things = [
                {
                    id: 'id1',
                    prop: true,
                },
                {
                    id: 'id2',
                    prop: false,
                },
                {
                    id: 'id3',
                    prop: null,
                },
            ];
            render('<editable-things-list klass-name="\'Thing\'" columns="columns"><editable-things-list>', {
                columns: [
                    {
                        prop: 'prop',
                        type: 'checkIfTrue',
                    },
                ],
            });
            resolveIndexCall();
            expect(scope.getFormattedColumnValue(parentScope.columns[0], things[0], 'html')).toEqual(true);
            expect(scope.getFormattedColumnValue(parentScope.columns[0], things[1], 'html')).toEqual(false);
            expect(scope.getFormattedColumnValue(parentScope.columns[0], things[2], 'html')).toEqual(null);
        });

        // In web, acceptedRejected does not hit getFormattedColumnValue, but when preparing
        // csv, it does
        it('should work with acceptedRejected', () => {
            things = [
                {
                    id: 'id1',
                    prop: 'accepted',
                },
                {
                    id: 'id2',
                    prop: 'rejected',
                },
                {
                    id: 'id3',
                    prop: null,
                },
            ];
            render('<editable-things-list klass-name="\'Thing\'" columns="columns"><editable-things-list>', {
                columns: [
                    {
                        prop: 'prop',
                        type: 'acceptedRejected',
                    },
                ],
            });
            resolveIndexCall();
            expect(scope.getFormattedColumnValue(parentScope.columns[0], things[0], 'html')).toEqual('accepted');
            expect(scope.getFormattedColumnValue(parentScope.columns[0], things[1], 'html')).toEqual('rejected');
            expect(scope.getFormattedColumnValue(parentScope.columns[0], things[2], 'html')).toEqual(null);
        });

        it('should work with custom type that does not have getFormattedColumnValueForCSV', () => {
            things = [
                {
                    id: 'id1',
                },
            ];
            render('<editable-things-list klass-name="\'Thing\'" columns="columns"><editable-things-list>', {
                columns: [
                    {
                        prop: 'id',
                        type: 'custom',
                    },
                ],
            });
            resolveIndexCall();
            expect(scope.getFormattedColumnValue(parentScope.columns[0], things[0], 'csv')).toEqual('id1');
        });

        it('should work with custom type that has getFormattedColumnValueForCSV', () => {
            things = [
                {
                    id: 'id1',
                },
            ];
            render('<editable-things-list klass-name="\'Thing\'" columns="columns"><editable-things-list>', {
                columns: [
                    {
                        prop: 'id',
                        type: 'custom',
                        callbacks: {
                            getFormattedColumnValueForCSV(thing) {
                                return `formatted for csv: ${thing.id}`;
                            },
                        },
                    },
                ],
            });
            resolveIndexCall();
            expect(scope.getFormattedColumnValue(parentScope.columns[0], things[0], 'csv')).toEqual(
                'formatted for csv: id1',
            );
        });

        it('should work text type', () => {
            things = [
                {
                    id: 'id1',
                    prop: 'a',
                },
            ];
            render('<editable-things-list klass-name="\'Thing\'" columns="columns"><editable-things-list>', {
                columns: [
                    {
                        prop: 'prop',
                        type: 'text',
                    },
                ],
            });
            resolveIndexCall();
            expect(scope.getFormattedColumnValue(parentScope.columns[0], things[0], 'html')).toEqual('a');
        });

        it('should work with dot syntax', () => {
            things = [
                {
                    id: 'id1',
                    subthing: {
                        a: 'b',
                    },
                },
            ];
            render('<editable-things-list klass-name="\'Thing\'" columns="columns"><editable-things-list>', {
                columns: [
                    {
                        prop: 'subthing.a',
                        type: 'custom',
                    },
                ],
            });
            resolveIndexCall();
            expect(scope.getFormattedColumnValue(parentScope.columns[0], things[0], 'csv')).toEqual('b');
        });
    });

    describe('$destroy', () => {
        it('should unset the id query param', () => {
            things = [
                {
                    id: 'id1',
                    subthing: {
                        a: 'b',
                    },
                },
            ];
            render();
            jest.spyOn($location, 'search');
            scope.showThing(things[0]);
            expect($location.search).toHaveBeenCalledWith('thing-id', things[0].id);
            $location.search.mockClear();
            scope.$destroy();
            expect($location.search).toHaveBeenCalledWith('thing-id', null);
        });
    });

    function expectThings(expectedIds, opts = {}) {
        const actualIds = [];
        elem.find('tbody tr').each(function () {
            actualIds.push($(this).attr('data-id'));
        });
        if (opts.ignoreOrder) {
            expect(actualIds.sort()).toEqual(expectedIds.sort());
        } else {
            expect(actualIds).toEqual(expectedIds);
        }
    }
});
