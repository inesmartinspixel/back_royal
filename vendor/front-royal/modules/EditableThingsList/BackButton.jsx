import React from 'react';

function BackButton(props) {
    return (
        <button
            className="unstyled-button"
            name="back-to-list"
            onClick={() => {
                props.editableThingsListViewModel.goBack();
            }}
        >
            <i className="fa fa-arrow-left" />
            {props.label}
        </button>
    );
}
export default BackButton;
