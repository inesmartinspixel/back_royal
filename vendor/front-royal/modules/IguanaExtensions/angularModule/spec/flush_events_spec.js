import 'AngularSpecHelper';
import 'IguanaExtensions/angularModule';

describe('EventLogger.EventLogger', () => {
    let $injector;
    let Iguana;
    let MyModel;
    let EventLogger;
    let SpecHelper;
    let EventBundle;
    let Event;

    beforeEach(() => {
        angular.mock.module('SpecHelper', 'FrontRoyal.iguanaExtensions');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                EventLogger = $injector.get('EventLogger');
                Iguana = $injector.get('Iguana');
                EventBundle = $injector.get('EventLogger.EventBundle');
                Event = $injector.get('EventLogger.Event');

                // Ensure config is initialized so events can be flushed
                SpecHelper.stubConfig();

                MyModel = Iguana.subclass(function () {
                    this.collection = 'things';
                });

                jest.spyOn(MyModel, 'saveWithoutInstantiating');
                jest.spyOn(Event.prototype, 'asJson').mockReturnValue('eventJson');
                MyModel.expect('save');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    afterEach(() => {
        EventLogger.destroy();
    });

    it('should flush events when creating anything', () => {
        const evt = EventLogger.log('something happened', {
            label: 'ok',
        });
        MyModel.create();
        expect(MyModel.saveWithoutInstantiating).toHaveBeenCalled();
        expect(MyModel.saveWithoutInstantiating.mock.calls[0][2]).toEqual({
            event_bundle: EventBundle.new({
                events: [evt.asJson()],
            }).asJson(),
        });
    });

    it('should flush events when saving anything', () => {
        const evt = EventLogger.log('something happened', {
            label: 'ok',
        });

        MyModel.expect('update').toBeCalledWith({}, {});
        const model = MyModel.new({
            id: 'id',
        });
        model.save();
        expect(MyModel.saveWithoutInstantiating).toHaveBeenCalled();
        expect(MyModel.saveWithoutInstantiating.mock.calls[0][2]).toEqual({
            event_bundle: EventBundle.new({
                events: [evt.asJson()],
            }).asJson(),
        });
    });

    it('should not add event bundles to event bundles', () => {
        EventBundle.expect('save');
        jest.spyOn(EventBundle, 'saveWithoutInstantiating');
        EventLogger.log('something happened', {
            label: 'ok',
        });
        EventBundle.create();
        expect(EventBundle.saveWithoutInstantiating).toHaveBeenCalled();

        // no metadata
        expect(EventBundle.saveWithoutInstantiating.mock.calls[0][2]).toBeUndefined();
    });
});
