import 'EventLogger/angularModule';

export default angular.module('FrontRoyal.iguanaExtensions', ['Iguana', 'EventLogger']);
