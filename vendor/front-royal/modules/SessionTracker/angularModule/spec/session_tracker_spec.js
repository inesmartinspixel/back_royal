import 'AngularSpecHelper';
import 'SessionTracker/angularModule';
import ClientStorage from 'ClientStorage';
import moment from 'moment-timezone';

describe('SessionTracker', () => {
    let $injector;
    let SessionTracker;

    beforeEach(() => {
        angular.mock.module('sessionTracker', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SessionTracker = $injector.get('SessionTracker');
        });
    });

    afterEach(() => {
        ClientStorage.removeItem(SessionTracker.clientStorageKey('identifier'));
        ClientStorage.removeItem(SessionTracker.clientStorageKey('anotherIdentifier'));
        ClientStorage.removeItem(SessionTracker.clientStorageKey('shouldBeRemoved'));
    });

    describe('pingCurrentSession', () => {
        it('should create a new session if there is none', () => {
            expect(SessionTracker._currentSessions.identifier).toBeUndefined();
            expect(SessionTracker.pingCurrentSession('identifier', 1000).id).not.toBeUndefined();
        });

        it('should ping an existing, non-expired session', () => {
            const sessionId = SessionTracker.pingCurrentSession('identifier', 1000).id;
            expect(SessionTracker.pingCurrentSession('identifier', 1000).id).toEqual(sessionId);
        });

        it('should not ping an existing session with a different identifier', () => {
            const sessionId = SessionTracker.pingCurrentSession('identifier', 1000).id;
            expect(SessionTracker.pingCurrentSession('anotherIdentifier', 1000).id).not.toEqual(sessionId);
        });

        it('should not ping an existing session for a different user', () => {
            const $rootScope = $injector.get('$rootScope');
            const sessionId = SessionTracker.pingCurrentSession('identifier', 1000).id;
            $rootScope.currentUser = { id: 'userId' };
            const newSessionId = SessionTracker.pingCurrentSession('identifier', 1000).id;
            expect(newSessionId).not.toEqual(sessionId);
            expect(SessionTracker.pingCurrentSession('identifier', 1000).id).toEqual(newSessionId);
            $rootScope.currentUser.id = 'differentUser';
            expect(SessionTracker.pingCurrentSession('identifier', 1000).id).not.toEqual(newSessionId);
        });

        it('should create a new session if the current one is expired', () => {
            const sessionId = SessionTracker.pingCurrentSession('identifier', 1000).id;
            jest.spyOn(SessionTracker, '_now').mockReturnValue(moment().add(1005, 'milliseconds').toDate());
            expect(SessionTracker.pingCurrentSession('identifier', 1000).id).not.toEqual(sessionId);
        });

        it('should pull a non-expired session from client storage', () => {
            const sessionId = SessionTracker.pingCurrentSession('identifier', 1000).id;

            // remove it from the cache, so it will have to be pulled from local storage
            SessionTracker._currentSessions = {};
            expect(SessionTracker.pingCurrentSession('identifier', 1000).id).toEqual(sessionId);
        });

        it('should create a new session if the one in client storage is expired', () => {
            const sessionId = SessionTracker.pingCurrentSession('identifier', 1).id;

            // remove it from the cache, so it will have to be pulled from local storage
            SessionTracker._currentSessions = {};
            jest.spyOn(SessionTracker, '_now').mockReturnValue(moment().add(1005, 'milliseconds').toDate());
            expect(SessionTracker.pingCurrentSession('identifier', 1000).id).not.toEqual(sessionId);
        });

        it('should clear expired sessions from client storage', () => {
            SessionTracker.pingCurrentSession('identifier', 99999999999);
            SessionTracker.pingCurrentSession('shouldBeRemoved', 1000);

            SessionTracker._localeStorageRead = false;
            jest.spyOn(SessionTracker, '_now').mockReturnValue(moment().add(1005, 'milliseconds').toDate());
            SessionTracker.pingCurrentSession('anotherIdentifier', 1);

            expect(ClientStorage.getItem(SessionTracker.clientStorageKey('identifier'))).not.toBeUndefined();
            expect(ClientStorage.getItem(SessionTracker.clientStorageKey('shouldBeRemoved'))).toBeUndefined();
        });

        it('should not create a new session even if the server timestamp is ahead of the client timestamp', () => {
            jest.spyOn(SessionTracker, '_now').mockImplementation(() => Date.now() + 1000 * 60 * 60);
            const id = SessionTracker.pingCurrentSession('anotherIdentifier', 10000).id;
            expect(SessionTracker.pingCurrentSession('anotherIdentifier', 10000).id).toEqual(id);
        });
    });
});
