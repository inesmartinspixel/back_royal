import { useRef, useContext } from 'react';
import AngularContext from 'AngularContext';

export default function useTranslationHelper(prefix, $injector) {
    const ref = useRef(null);

    // Sometimes it's easier to pass the injector in.  Usually,
    // it's easier to get it from the context.
    const $injectorFromContext = useContext(AngularContext);
    if (!$injector) {
        $injector = $injectorFromContext;
    }
    if (ref.current === null) {
        const TranslationHelper = $injector.get('TranslationHelper');
        ref.current = new TranslationHelper(prefix);
    }

    return ref.current;
}
