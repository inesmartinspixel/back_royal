import './scripts/content_item_editor_module';

import './scripts/content_item_editor_lists';
import './scripts/content_item_save_publish_dir';
import './scripts/locale_pack_selectize_dir';
import './scripts/content_item_image_upload_dir';
import './scripts/content_item_edit_locale_dir';
import './scripts/entity_metadata_form_dir';
import './scripts/duplicate_content_item_dir';
import 'ContentItemEditor/locales/content_item_editor/content_item_edit_locale-en.json';
