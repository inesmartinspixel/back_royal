import 'AngularSpecHelper';
import 'ContentItemEditor/angularModule';
import 'ContentItem/angularModule/spec/_mock/mock_is_content_item';
import setSpecLocales from 'Translation/setSpecLocales';
import contentItemEditLocaleLocales from 'ContentItemEditor/locales/content_item_editor/content_item_edit_locale-en.json';

setSpecLocales(contentItemEditLocaleLocales);

describe('ContentItem::ContentItemEditLocale', () => {
    let elem;
    let scope;
    let SpecHelper;
    let $injector;
    let contentItem;
    let ContentItem;
    let enItem;
    let esItem;
    let zhItem;
    let Locale;
    let anotherEsItem;
    let contentItemEditorLists;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.ContentItemEditor', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            Locale = $injector.get('Locale');
            contentItemEditorLists = $injector.get('contentItemEditorLists');

            Locale.editorLocalesWithoutEnglish = [Locale.spanish, Locale.chinese]; // future-proof this spec

            ContentItem = $injector.get('MockIsContentItem');
            contentItem = ContentItem.getInstance({
                locale: 'es',
                locale_pack: null,
            });

            enItem = ContentItem.getInstance({
                title: 'English',
                locale: 'en',
            });
            esItem = ContentItem.getInstance({
                title: 'Spanish',
                locale: 'es',
            });
            zhItem = ContentItem.getInstance({
                title: 'Chinese',
                locale: 'zh',
            });
            anotherEsItem = ContentItem.getInstance({
                title: 'Spanish',
                locale: 'es',
            });
        });
    });

    describe('with English content item', () => {
        beforeEach(() => {
            contentItem.locale = 'en';
            contentItem.locale_pack = null;
            contentItem.ensureLocalePack(); // add a new empty locale pack
        });

        it('should show spinner until everything is loaded', () => {
            render();
            SpecHelper.expectElement(elem, 'front-royal-spinner');
            SpecHelper.expectNoElement(elem, '.body');
            flushContentItemLoad();
            SpecHelper.expectNoElement(elem, 'front-royal-spinner');
            SpecHelper.expectElement(elem, '.body');
        });

        it('should allow for changing locale only if there are no linked items', () => {
            renderAndFlush();

            // add Spanish, locale selection should be disabled
            SpecHelper.updateSelectize(elem, '[name="linked_content_item"]:eq(0)', esItem.id);
            SpecHelper.expectElementDisabled(elem, '[name="locale"]', true);

            // unset Spanish
            SpecHelper.updateSelectize(elem, '[name="linked_content_item"]:eq(0)', null);
            SpecHelper.expectElementDisabled(elem, '[name="locale"]', false);
            SpecHelper.updateSelectize(elem, '[name="locale"]', 'es');

            expect(contentItem.locale).toEqual('es');
            expect(contentItem.locale_pack).toBe(null);
        });

        it('should allow for setting translations', () => {
            renderAndFlush();
            SpecHelper.expectElements(elem, '[name="linked_content_item"]', 2); // one for Spanish, one for Chinese

            // add Spanish
            SpecHelper.updateSelectize(elem, '[name="linked_content_item"]:eq(0)', esItem.id);
            expect(contentItem.locale_pack.contentItemForLocale('es')).not.toBeUndefined();
            expect(contentItem.locale_pack.contentItemForLocale('es').id).toEqual(esItem.id);

            // add Chinese
            SpecHelper.updateSelectize(elem, '[name="linked_content_item"]:eq(1)', zhItem.id);
            expect(contentItem.locale_pack.contentItemForLocale('zh').id).toEqual(zhItem.id);
        });

        it('should allow for unsetting translations', () => {
            renderAndFlush();
            SpecHelper.expectElements(elem, '[name="linked_content_item"]', 2); // one for Spanish, one for Chinese

            // add Spanish
            SpecHelper.updateSelectize(elem, '[name="linked_content_item"]:eq(0)', esItem.id);
            expect(contentItem.locale_pack.contentItemIdForLocale('es')).toEqual(esItem.id); // sanity check

            // unset Spanish
            SpecHelper.updateSelectize(elem, '[name="linked_content_item"]:eq(0)', null);
            expect(contentItem.locale_pack.contentItemForLocale('es')).toBeUndefined();
        });

        it('should allow for changing locale only if there are no linked items', () => {
            renderAndFlush();
            SpecHelper.expectElements(elem, '[name="linked_content_item"]', 2); // one for Spanish, one for Chinese

            // add Spanish
            SpecHelper.updateSelectize(elem, '[name="linked_content_item"]:eq(0)', esItem.id);
            expect(contentItem.locale_pack.contentItemIdForLocale('es')).toEqual(esItem.id); // sanity check

            // unset Spanish
            SpecHelper.updateSelectize(elem, '[name="linked_content_item"]:eq(0)', null);
            expect(contentItem.locale_pack.contentItemForLocale('es')).toBeUndefined();
        });

        it('should show links to translations', () => {
            renderAndFlush();
            jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});
            // add Spanish
            SpecHelper.updateSelectize(elem, '[name="linked_content_item"]:eq(0)', esItem.id);
            SpecHelper.click(elem, '.translation-selectors button', 0);
            expect(scope.loadUrl).toHaveBeenCalledWith(esItem.editorUrl, '_blank');
        });
    });

    describe('with non-English content item', () => {
        beforeEach(() => {
            contentItem = esItem;
            expect(contentItem.locale_pack).toBeUndefined();
        });

        it('should disable locale select if there are linked items', () => {
            linkContentItemAndRender();
            SpecHelper.expectElementDisabled(elem, '[name="locale"]', true);
        });

        it('should allow for changing locale to english only if there are no linked items', () => {
            renderAndFlush();
            expect(contentItem.locale_pack).toBeUndefined(); // sanity check
            SpecHelper.expectElementDisabled(elem, '[name="locale"]', false);
            SpecHelper.expectNoElement(elem, '.translation-selectors');

            // We have to load up lessons once we switch to English
            expectContentItemIndex();
            SpecHelper.updateSelectize(elem, '[name="locale"]', 'en');
            flushContentItemLoad();
            SpecHelper.expectElements(elem, '.translation-selectors');
            expect(contentItem.locale_pack).not.toBe(null);

            // do not include the version of this item that is currently in the
            // db as a linkable item
            expect(scope.contentItemsById[scope.contentItem.id]).toBeUndefined();
        });

        it('should allow for changing locale to non-english only if there are no linked items', () => {
            renderAndFlush();
            expect(contentItem.locale_pack).toBeUndefined(); // sanity check
            SpecHelper.expectElementDisabled(elem, '[name="locale"]', false);
            SpecHelper.expectNoElement(elem, '.translation-selectors');

            // We should not load up lessons if switching to non-English
            jest.spyOn(ContentItem, 'index').mockImplementation(() => {});
            SpecHelper.updateSelectize(elem, '[name="locale"]', 'zh');
            expect(ContentItem.index).not.toHaveBeenCalled();
            SpecHelper.expectNoElement(elem, '.translation-selectors');
            expect(contentItem.locale_pack).toBe(null);
        });

        it('should show link to english translation', () => {
            linkContentItemAndRender();
            jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});

            SpecHelper.click(elem, '.english-translation button.link');
            expect(scope.loadUrl).toHaveBeenCalledWith(enItem.editorUrl, '_blank');
        });

        it('should allow for unlinking', () => {
            linkContentItemAndRender();
            jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});

            SpecHelper.click(elem, '.english-translation button.remove');
            expect(contentItem.locale_pack).toBe(null);
        });

        function linkContentItemAndRender() {
            contentItem.ensureLocalePack();
            contentItem.locale_pack.content_items.push({
                locale: 'en',
                id: enItem.id,
                title: enItem.title,
            });
            renderAndFlush();
        }
    });

    function renderAndFlush() {
        render();

        // We only load linkable items if the main item is english
        if (contentItem.locale === 'en') {
            flushContentItemLoad();

            // Copy the instances of these that are loaded on the
            // scope so we can access them easily in all specs
            enItem = _.findWhere(scope.contentItemsById, {
                id: enItem.id,
            });
            esItem = _.findWhere(scope.contentItemsById, {
                id: esItem.id,
            });
            zhItem = _.findWhere(scope.contentItemsById, {
                id: zhItem.id,
            });
            anotherEsItem = _.findWhere(scope.contentItemsById, {
                id: anotherEsItem.id,
            });
        }
    }

    let contentItemLoadCallback;

    function flushContentItemLoad() {
        expect(contentItemEditorLists.load).toHaveBeenCalledWith('MockIsContentItem', ['es', 'zh'], {
            locale_pack_id: [null, contentItem.localePackId],
        });
        contentItemLoadCallback([enItem, esItem, zhItem, anotherEsItem]);
        scope.$digest();
    }

    function expectContentItemIndex() {
        jest.spyOn($injector.get('contentItemEditorLists'), 'load').mockReturnValue({
            onComplete(cb) {
                contentItemLoadCallback = cb;
            },
        });
    }

    function render() {
        // We only load linkable items if the main item is english
        if (contentItem.locale === 'en') {
            expectContentItemIndex();
        }
        const renderer = SpecHelper.renderer();
        renderer.scope.contentItem = contentItem;
        renderer.render('<content-item-edit-locale content-item="contentItem"></content-item-edit-locale>');
        elem = renderer.elem;
        scope = renderer.childScope;
    }
});
