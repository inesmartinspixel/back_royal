import 'AngularSpecHelper';
import 'ContentItemEditor/angularModule';
import 'Playlists/angularModule/spec/_mock/fixtures/playlists';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';

describe('ContentItemSavePublishDir', () => {
    let SpecHelper;
    let Stream;
    let Playlist;
    let EntityMetadata;
    let $timeout;
    let $window;
    let $location;
    let stream;
    let playlist;
    let scope;
    let elem;

    beforeEach(() => {
        // look for a note about NoUnhandledRejectionExceptions use below (error handling)
        angular.mock.module('FrontRoyal.ContentItemEditor', 'SpecHelper', 'NoUnhandledRejectionExceptions');

        angular.mock.inject($injector => {
            $injector.get('StreamFixtures');
            $injector.get('PlaylistFixtures');
            $injector.get('MockIguana');
            SpecHelper = $injector.get('SpecHelper');
            EntityMetadata = $injector.get('EntityMetadata');
            Stream = $injector.get('Lesson.Stream');
            Playlist = $injector.get('Playlist');
            $timeout = $injector.get('$timeout');
            $window = $injector.get('$window');
            $location = $injector.get('$location');

            stream = Stream.fixtures.getInstance();
            playlist = Playlist.fixtures.getInstance();
            SpecHelper.stubCurrentUser('editor');
            SpecHelper.stubConfig();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('save', () => {
        beforeEach(() => {
            render();
        });

        it('should happen when save is clicked', () => {
            expectButtonsEnabled();
            EntityMetadata.expect('create');
            Stream.expect('save');
            buttons().save.click();
            EntityMetadata.flush('create');
            expectButtonsDisabled();
            Stream.flush('save');
            // when the save comes back, we should flash a "Saved" message on the button
            expectButtonsDisabled();
            SpecHelper.expectEqual('Saved', buttons().save.text(), 'saved button text');

            // after a timeout, remove the message and re-enable
            $timeout.flush();
            expectButtonsEnabled();
            SpecHelper.expectEqual('Save Course', SpecHelper.trimText(buttons().save.text()), 'saved button text');
        });

        it('should override timeout', () => {
            EntityMetadata.expect('create');
            Stream.expect('save');
            jest.spyOn(stream, 'save');
            buttons().save.click();
            EntityMetadata.flush('create');
            const hasTimeoutProp = Object.prototype.hasOwnProperty.call(stream.save.mock.calls[0][1], 'timeout');
            expect(hasTimeoutProp).toBe(true);
            expect(stream.save.mock.calls[0][1].timeout).toBeUndefined();
        });

        it('should handle errors', () => {
            // see also: NoUnhandledRejectionExceptions module use above
            EntityMetadata.expect('create');
            Stream.expect('save').fails('error');
            buttons().save.click();
            EntityMetadata.flush('create');
            Stream.flush('save');
            expectButtonsEnabled();
        });

        it('should disable whole screen while saving', () => {
            SpecHelper.expectNoElement(elem, '.modal');
            scope.preventSubmit = true;
            scope.$digest();
            SpecHelper.expectElement(elem, '.modal');
            scope.preventSubmit = false;
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.modal');
        });
    });

    describe('delete', () => {
        beforeEach(() => {
            render();
        });
        it('should work when button is clicked', () => {
            expectButtonsEnabled();
            Stream.expect('destroy');

            jest.spyOn($window, 'confirm').mockReturnValue(true);
            buttons().delete.click();
            expect($window.confirm).toHaveBeenCalled();
            expectButtonsDisabled();

            jest.spyOn($location, 'url').mockImplementation(() => {});
            Stream.flush('destroy');
            expect($location.url).toHaveBeenCalledWith('/editor?section=courses');
        });

        it('should handle errors', () => {
            // see also: NoUnhandledRejectionExceptions module use above
            Stream.expect('destroy').fails('error');
            jest.spyOn($window, 'confirm').mockReturnValue(true);
            buttons().delete.click();
            Stream.flush('destroy');
            expectButtonsEnabled();
        });
    });

    describe('duplicate', () => {
        it('should not show when contentItem is not a playlist', () => {
            render(stream, 'Stream');
            SpecHelper.expectNoElement(elem, '#duplicate-playlist');
        });

        it('should show when contentItem is a playlist', () => {
            render(playlist, 'Playlist');
            SpecHelper.expectElement(elem, '#duplicate-playlist');
        });
    });

    describe('publishing', () => {
        beforeEach(() => {
            SpecHelper.stubCurrentUser('admin');
            render();
        });

        it('should only allow one of the publishing checkboxes to be checked', () => {
            stream.published_at = 12345;
            expect(scope.metadata).toEqual({
                should_publish: false,
                should_unpublish: false,
            });
            SpecHelper.checkCheckbox(elem, '[data-id="publish_on_save"] input');
            expect(scope.metadata).toEqual({
                should_publish: true,
                should_unpublish: false,
            });
            SpecHelper.checkCheckbox(elem, '[ng-model="metadata.should_unpublish"]');
            expect(scope.metadata).toEqual({
                should_publish: false,
                should_unpublish: true,
            });
            SpecHelper.checkCheckbox(elem, '[ng-model="metadata.should_publish"]');
            expect(scope.metadata).toEqual({
                should_publish: true,
                should_unpublish: false,
            });
        });

        it('should set should_publish flag in metadata', () => {
            Stream.expect('save').toBeCalledWith(
                stream,
                {
                    should_publish: true,
                    should_unpublish: false,
                },
                {
                    timeout: undefined,
                },
            );
            EntityMetadata.expect('create');
            SpecHelper.checkCheckbox(elem, '[data-id="publish_on_save"] input');
            buttons().save.click();
            EntityMetadata.flush('create');
            Stream.flush('save');
        });

        it('should set should_unpublish flag in metadata', () => {
            // should_unpublish should be hidden unless the stream is currently published
            SpecHelper.expectNoElement(elem, '[data-id="unpublish_on_save"] input');
            stream.published_at = 12345;
            scope.$digest();
            SpecHelper.expectElement(elem, '[data-id="unpublish_on_save"] input');

            Stream.expect('save').toBeCalledWith(
                stream,
                {
                    should_publish: false,
                    should_unpublish: true,
                },
                {
                    timeout: undefined,
                },
            );
            EntityMetadata.expect('create');
            SpecHelper.checkCheckbox(elem, '[data-id="unpublish_on_save"] input');
            buttons().save.click();
            EntityMetadata.flush('create');
            Stream.flush('save');
        });
    });

    function buttons() {
        return {
            save: SpecHelper.expectElement(elem, '[data-id="save"]'),
            delete: SpecHelper.expectElement(elem, '[data-id="delete"]'),
        };
    }

    function expectButtonsEnabled() {
        expectButtonsDisabled(false);
    }

    function expectButtonsDisabled(value) {
        value = value !== false;
        SpecHelper.expectEqual(value, buttons().save.prop('disabled'), 'save disabled');
        SpecHelper.expectEqual(value, buttons().delete.prop('disabled'), 'delete disabled');
    }

    function render(contentItem, itemHeader) {
        if (!contentItem) {
            contentItem = stream;
        }

        if (!itemHeader) {
            itemHeader = 'Course';
        }

        const renderer = SpecHelper.renderer();
        renderer.scope.contentItem = contentItem;
        renderer.scope.itemHeader = itemHeader;
        renderer.render(
            `<content-item-save-publish content-item="contentItem" item-header="${itemHeader}" go-back="goBack()"></content-item-save-publish>`,
        );
        scope = renderer.childScope;
        scope.contentItem = contentItem;
        scope.itemHeader = itemHeader;

        scope.goBack = () => {
            $location.url('/editor?section=courses');
        };

        elem = renderer.elem;
        $timeout.flush();
    }
});
