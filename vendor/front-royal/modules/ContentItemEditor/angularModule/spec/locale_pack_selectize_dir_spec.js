import 'AngularSpecHelper';
import 'ContentItemEditor/angularModule';
import 'ContentItem/angularModule/spec/_mock/mock_is_content_item';

describe('ContentItem::LocalePackSelectize', () => {
    let elem;
    let scope;
    let SpecHelper;
    let $injector;
    let ContentItem;
    let contentItems;
    let getCurrentValue;
    let contentItemEditorLists;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.ContentItemEditor', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            contentItemEditorLists = $injector.get('contentItemEditorLists');

            ContentItem = $injector.get('MockIsContentItem');
            contentItems = [ContentItem.getInstance(), ContentItem.getInstance(), ContentItem.getInstance()];
            _.invoke(contentItems, 'ensureLocalePack');
        });
    });

    it('should show a spinner until content items load', () => {
        render();
        SpecHelper.expectElement(elem, 'front-royal-spinner');
        flushContentItemLoad();
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
    });

    it('should support adding a locale pack', () => {
        renderAndFlush();
        SpecHelper.updateSelectize(elem, 'selectize', contentItems[0].localePackId);
        expect(getCurrentValue()).toEqual([contentItems[0].localePackId]);

        SpecHelper.updateSelectize(elem, 'selectize', contentItems[1].localePackId);
        expect(getCurrentValue()).toEqual([contentItems[0].localePackId, contentItems[1].localePackId]);
    });

    it('should support removing a locale pack', () => {
        renderAndFlush({
            localePackIds: [contentItems[0].localePackId, contentItems[1].localePackId],
        });

        SpecHelper.click(elem, '.icon.remove', 1);
        expect(getCurrentValue()).toEqual([contentItems[0].localePackId]);

        SpecHelper.click(elem, '.icon.remove');
        expect(getCurrentValue()).toEqual([]);
    });

    it('should support opening a content item', () => {
        renderAndFlush({
            localePackIds: [contentItems[0].localePackId],
        });
        jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});
        SpecHelper.click(elem, '.icon.link', 0);
        expect(scope.loadUrl).toHaveBeenCalledWith(contentItems[0].editorUrl, '_blank');
    });

    it('should allow for configuring selectize config', () => {
        renderAndFlush({
            config: {
                some: 'value',
            },
        });
        expect(elem.find('selectize').isolateScope().config).toEqual({
            maxItems: 1,
            labelField: 'titleWithTagAndLocales',
            valueField: 'localePackId',
            sortField: 'titleWithTag',
            searchField: 'titleWithTag',
            some: 'value',
        });
    });

    let loadCallback;

    function flushContentItemLoad() {
        loadCallback(contentItems);
        scope.$digest();
    }

    function renderAndFlush(opts) {
        render(opts);

        flushContentItemLoad();

        // Copy the instances of these that are loaded on the
        // scope so we can access them easily in all specs
        contentItems = scope.allContentItems;
    }

    function render(opts) {
        jest.spyOn(contentItemEditorLists, 'load').mockReturnValue({
            onLoad(cb) {
                loadCallback = cb;
            },
        });
        const renderer = SpecHelper.renderer();
        opts = opts || [];
        _.extend(
            renderer.scope,
            {
                localePackIds: [],
                placeholder: null,
                config: {},
            },
            opts,
        );

        getCurrentValue = () => renderer.scope.localePackIds;
        renderer.render(
            `<locale-pack-selectize ng-model="localePackIds" klass-name="MockIsContentItem" placeholder="${renderer.scope.placeholder}" config="config"></locale-pack-selectize>`,
        );
        elem = renderer.elem;
        scope = renderer.childScope;
        expect(contentItemEditorLists.load).toHaveBeenCalledWith('MockIsContentItem', 'en', undefined);
    }
});
