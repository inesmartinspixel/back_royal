import 'AngularSpecHelper';
import 'ContentItemEditor/angularModule';
import 'ContentItem/angularModule/spec/_mock/mock_is_content_item';
import 'ContentItem/angularModule/spec/_mock/mock_s3_asset';

describe('ContentItemImageUploadDir', () => {
    let SpecHelper;
    let elem;
    let scope;
    let uploadedImages;
    let MockS3Asset;
    let ContentItem;
    let Upload;
    let DialogModal;
    let uploadsStarted;
    let origFileReader;
    let $window;
    let FileReader;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.ContentItemEditor', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            MockS3Asset = $injector.get('MockS3Asset');
            ContentItem = $injector.get('MockIsContentItem');
            Upload = $injector.get('Upload');
            DialogModal = $injector.get('DialogModal');

            FileReader = function () {};
            FileReader.prototype.readAsDataURL = function () {
                this.onload({
                    target: {
                        result: 'dataUrl',
                    },
                });
            };
            $window = $injector.get('$window');
            origFileReader = $window.FileReader;
            $window.FileReader = FileReader;
            render();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
        $window.FileReader = origFileReader;
    });

    it('should support uploading an image', () => {
        scope.allowedType = () => true;
        let successCallback;
        const upload = {
            error() {
                return upload;
            },
            success(callback) {
                successCallback = callback;
                return upload;
            },
        };
        jest.spyOn(Upload, 'upload').mockReturnValue(upload);

        const files = [
            {
                name: 'image1',
                size: 2 * 1024 * 1024,
            },
        ];

        // upload button should be shown and spinner should not be here
        SpecHelper.expectElementNotHidden(elem, 'span[name="uploader"]');
        SpecHelper.expectNoElement(elem, '.front-royal-spinner');

        // mock the start of an image upload
        scope.onFileSelect(files);
        scope.$digest();

        expect(Upload.upload).toHaveBeenCalledWith({
            url: scope.contentItem.imageUploadUrl(),
            data: {
                images: files,
            },
            arrayKey: '[]',
            timeout: undefined,
        });

        // since the expected value is undefined, this can't be checked
        // in toHaveBeenCalledWith above
        expect(Upload.upload.mock.calls[0][0].hasOwnProperty('timeout')).toBe(true);
        expect(Upload.upload.mock.calls[0][0].timeout).toBeUndefined();

        // upload button should be hidden and spinner should be shown
        SpecHelper.expectElementHidden(elem, 'span[name="uploader"]');
        SpecHelper.expectElement(elem, '.front-royal-spinner');

        // startUpload should have been called with dataUrls
        SpecHelper.expectEqual(1, uploadsStarted.length, 'uploads started length');
        expect(uploadsStarted[0].fileName).toEqual('image1');
        expect(uploadsStarted[0].dataUrl).toEqual('dataUrl');

        // mock completion of an image upload
        const data = {
            contents: {
                images: [
                    MockS3Asset.get({
                        file_file_name: 'image1.JPG',
                    }),
                ],
            },
        };

        // // when onImageUploaded is called (upon return from server),
        // // the imageUploadedCallback should be called with the fileName and the s3Asset
        successCallback(data);
        scope.$digest();
        SpecHelper.expectEqual(1, uploadedImages.length, 'uploaded images length');
        SpecHelper.expectEqual(data.contents.images[0], uploadedImages[0].s3Asset, 'uploaded image s3Asset');
        SpecHelper.expectEqual('image1', uploadedImages[0].fileName, 'uploaded image filename');

        // upload button should be shown and spinner should not be here
        SpecHelper.expectElementNotHidden(elem, 'span[name="uploader"]');
        SpecHelper.expectNoElement(elem, '.front-royal-spinner');
    });

    it('should allow deleting an image', () => {
        scope.contentItem.image = {};
        scope.$apply();

        // when there is an image, remove should be enabled
        SpecHelper.expectElementDisabled(elem, '[name="remove"]', false);
        SpecHelper.click(elem, '[name="remove"]');

        // when there is an image key, but no image is set, remove should be hidden
        expect(scope.contentItem.image).toBe(null);
        SpecHelper.expectNoElement(elem, '[name="remove"]', true);

        // if there is no image key at all, remove button should be hidden
        delete scope.contentItem.image;
        scope.$apply();
        SpecHelper.expectNoElement(elem, '[name="remove"]');
    });

    it('should support a user-facing modal warning when filesize is too large', () => {
        scope.allowedType = () => true;

        jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
        jest.spyOn(Upload, 'upload').mockImplementation(() => {});

        const files = [
            {
                name: 'file1',
                size: 4 * 1024 * 1024,
            },
            {
                name: 'file2',
                size: 2 * 1024 * 1024,
            },
            {
                name: 'file3',
                size: 5 * 1024 * 1024,
            },
        ];

        // mock the start of an image upload
        scope.onFileSelect(files);
        expectErrorResponse(
            "'file1' exceeds the maximum upload size of 2.5 MB<br>'file3' exceeds the maximum upload size of 2.5 MB",
        );

        // make sure it works again
        scope.onFileSelect(files);
        expectErrorResponse(
            "'file1' exceeds the maximum upload size of 2.5 MB<br>'file3' exceeds the maximum upload size of 2.5 MB",
        );
    });

    it('should support a user-facing modal warning when file is wrong type', () => {
        jest.spyOn(scope, 'allowedType').mockReturnValue(false);

        jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
        jest.spyOn(Upload, 'upload').mockImplementation(() => {});

        const files = [
            {
                name: 'file1',
                size: 1,
            },
        ];

        // mock the start of an image upload
        scope.onFileSelect(files);
        expectErrorResponse("'file1' is not an allowed image type (jpg, png, gif, and svg are allowed).");

        // make sure it works again
        scope.onFileSelect(files);
        expectErrorResponse("'file1' is not an allowed image type (jpg, png, gif, and svg are allowed).");
    });

    it('should re-activate on failure', () => {
        scope.allowedType = () => true;
        let errorCallback;
        const upload = {
            error(callback) {
                errorCallback = callback;
                return upload;
            },
            success() {
                return upload;
            },
        };
        jest.spyOn(Upload, 'upload').mockReturnValue(upload);

        const files = [
            {
                name: 'file1',
                size: 1,
            },
            {
                name: 'file2',
                size: 1,
            },
        ];

        // upload button should be shown and spinner should not be here
        SpecHelper.expectElementNotHidden(elem, 'span[name="uploader"]');
        SpecHelper.expectNoElement(elem, '.front-royal-spinner');

        // mock the start of an image upload
        scope.onFileSelect(files);
        scope.$digest();

        // upload button should be hidden and spinner should be shown
        SpecHelper.expectElementHidden(elem, 'span[name="uploader"]');
        SpecHelper.expectElement(elem, '.front-royal-spinner');

        // oops. there's a server error
        errorCallback();
        scope.$digest();

        // upload button should be shown and spinner should not be here
        SpecHelper.expectElementNotHidden(elem, 'span[name="uploader"]');
        SpecHelper.expectNoElement(elem, '.front-royal-spinner');
    });

    function expectErrorResponse(content) {
        expect(DialogModal.alert).toHaveBeenCalledWith({
            content,
            classes: ['server-error-modal'],
            title: 'Invalid Image',
        });
        expect(Upload.upload).not.toHaveBeenCalled();
        DialogModal.alert.mockClear();
    }

    it('should support transclusion', () => {
        render(
            '<content-item-image-upload content-item="contentItem" ><div transcluded-content></div></content-item-image-upload>',
        );
        SpecHelper.expectElement(elem, '[transcluded-content]');
        SpecHelper.expectNoElement(elem, '.default');
    });

    function render(
        html = '<content-item-image-upload content-item="contentItem" uploaded="imageUploadedCallback($$fileName, $$s3Asset)" start-upload="startUploadCallback($$fileName, $$dataUrl)" ng-disabled="shouldBeDisabled"></content-item-image-upload>',
    ) {
        const renderer = SpecHelper.renderer();
        uploadedImages = [];
        uploadsStarted = [];
        renderer.scope.contentItem = ContentItem.getInstance();
        renderer.scope.imageUploadedCallback = (fileName, s3Asset) => {
            uploadedImages.push({
                fileName,
                s3Asset,
            });
        };
        renderer.scope.startUploadCallback = (fileName, dataUrl) => {
            uploadsStarted.push({
                fileName,
                dataUrl,
            });
        };
        renderer.scope.shouldBeDisabled = false;

        renderer.render(html);
        elem = renderer.elem;
        scope = renderer.childScope;
    }
});
