import 'AngularSpecHelper';
import 'ContentItemEditor/angularModule';
import 'ContentItem/angularModule/spec/_mock/mock_is_content_item';

describe('ContentItemEditor::entityMetadataForm', () => {
    let elem;
    let scope;
    let SpecHelper;
    let $injector;
    let contentItem;
    let ContentItem;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.ContentItemEditor', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            SpecHelper = $injector.get('SpecHelper');
            ContentItem = $injector.get('MockIsContentItem');
            contentItem = ContentItem.getInstance({
                title: 'content_item_title',
                description: 'content_item_description',
                utmCampaign: 'content_item_campaign',
                entity_metadata: {
                    title: 'entity_title',
                    description: 'entity_description',
                    canonical_url: 'entity_url',
                    tweet_template: 'entity_tweet_template',
                },
            });

            SpecHelper.stubCurrentUser('admin');

            SpecHelper.stubConfig({
                social_name: 'some_twitter_account',
            });

            render();
        });
    });

    describe('access control', () => {
        it('should only be accesible to admins', () => {
            SpecHelper.expectElement(elem, '> div.panel'); // sanity check
            SpecHelper.stubCurrentUser('super_editor');
            render();
            SpecHelper.expectNoElement(elem, '> div.panel');
        });
    });

    describe('Title locking', () => {
        it('should allow for locking and unlocking of SEO title', () => {
            expect(scope.contentItem.entity_metadata.title).toEqual('entity_title'); // sanity check
            expect(scope.seoTitleLocked).toBeUndefined(); // sanity check
            SpecHelper.expectElementEnabled(elem, 'input[name="seo_title"]');
            SpecHelper.click(elem, 'button[name="edit_seo_title"]');
            SpecHelper.expectElementDisabled(elem, 'input[name="seo_title"]');
            expect(scope.contentItem.entity_metadata.title).toEqual('content_item_title');
            expect(scope.seoTitleLocked).toBe(true);
        });
    });

    describe('Description locking', () => {
        it('should allow for locking and unlocking of SEO description', () => {
            expect(scope.contentItem.entity_metadata.description).toEqual('entity_description'); // sanity check
            expect(scope.seoDescriptionLocked).toBeUndefined(); // sanity check
            SpecHelper.expectElementEnabled(elem, 'textarea[name="seo_description"]');
            SpecHelper.click(elem, 'button[name="edit_seo_description"]');
            SpecHelper.expectElementDisabled(elem, 'textarea[name="seo_description"]');
            expect(scope.contentItem.entity_metadata.description).toEqual('content_item_description');
            expect(scope.seoDescriptionLocked).toBe(true);
        });
    });

    describe('URL locking', () => {
        it('should allow for locking and unlocking of canonical URL', () => {
            expect(scope.contentItem.entity_metadata.canonical_url).toEqual('entity_url'); // sanity check
            expect(scope.seoCanonicalUrlLocked).toBeUndefined(); // sanity check
            SpecHelper.expectElementEnabled(elem, 'input[name="seo_canonical_url"]');
            SpecHelper.click(elem, 'button[name="edit_seo_canonical_url"]');
            SpecHelper.expectElementDisabled(elem, 'input[name="seo_canonical_url"]');
            expect(scope.contentItem.entity_metadata.canonical_url).toEqual(
                `/urlPrefix/entitytitle/${scope.contentItem.id}`,
            );
            expect(scope.seoCanonicalUrlLocked).toBe(true);
        });
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.scope.contentItem = contentItem;
        renderer.render(
            '<entity-metadata-form content-item="contentItem" url-prefix="urlPrefix"></entity-metadata-form>',
        );
        elem = renderer.elem;
        scope = renderer.childScope;
    }
});
