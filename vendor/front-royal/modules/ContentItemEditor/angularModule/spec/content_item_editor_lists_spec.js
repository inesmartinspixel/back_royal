import 'AngularSpecHelper';
import 'ContentItemEditor/angularModule';
import 'ContentItem/angularModule/spec/_mock/mock_is_content_item';

describe('contentItemEditorLists', () => {
    let SpecHelper;
    let ContentItem;
    let enContentItems;
    let esContentItems;
    let contentItemEditorLists;
    let $timeout;
    let $q;
    let callback;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.ContentItemEditor', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            ContentItem = $injector.get('MockIsContentItem');
            $timeout = $injector.get('$timeout');
            $q = $injector.get('$q');

            enContentItems = [ContentItem.getInstance(), ContentItem.getInstance(), ContentItem.getInstance()];

            esContentItems = [ContentItem.getInstance(), ContentItem.getInstance(), ContentItem.getInstance()];

            callback = jest.fn();
            contentItemEditorLists = $injector.get('contentItemEditorLists');
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('load', () => {
        beforeEach(() => {});

        it('should make single request if nothing is loaded for a particular locale', () => {
            // index will be called twice
            expectIndexCalls(enContentItems, esContentItems);

            // make en request
            contentItemEditorLists.load('MockIsContentItem', 'en').onLoad(callback);
            assertIndexCallMade('en');
            assertCallbackCalled(enContentItems);

            // make es request
            callback.mockClear();
            contentItemEditorLists.load('MockIsContentItem', 'es').onLoad(callback);
            assertIndexCallMade('es');
            assertCallbackCalled(esContentItems);
        });

        it('should return current data and make no request if data is fresh', () => {
            // index will be called only once
            expectIndexCalls(enContentItems);

            // make en request
            contentItemEditorLists.load('MockIsContentItem', 'en').onLoad(callback);
            assertIndexCallMade('en');
            assertCallbackCalled(enContentItems);

            // second en request does not need to hit server
            contentItemEditorLists.load('MockIsContentItem', 'en').onLoad(callback);
            assertNoIndexCallMade();
            assertCallbackCalled(enContentItems);
        });

        it('should return current data and load up more if data is stale', () => {
            let now = new Date('2016/01/01 12:00');
            const lastLoadAt = now;
            jest.spyOn(contentItemEditorLists, 'now').mockImplementation(() => now);

            const newItem = ContentItem.getInstance();
            const updatedItem = ContentItem.new(enContentItems[0].asJson());
            updatedItem.title = 'new title';
            const updates = [newItem, updatedItem];

            // index will be called twice, the second time with some updates
            expectIndexCalls(enContentItems, updates);

            // make initial request
            contentItemEditorLists.load('MockIsContentItem', 'en').onLoad(callback);
            assertIndexCallMade('en');
            assertCallbackCalled(enContentItems);

            // make second request
            now = new Date(now.getTime() + 11 * 1000);
            contentItemEditorLists.load('MockIsContentItem', 'en').onLoad(callback);

            // initially, we just return the cached values
            // at the same time, we make another index call
            // to get the updated items
            const itemsWithUpdates = _.chain(enContentItems).without(enContentItems[0]).union(updates).value();
            assertIndexCallMade('en', lastLoadAt);

            // the callback was triggered twice.  Once before the callback was made
            // and once after the callback was made
            assertCallbackCalled(enContentItems, itemsWithUpdates);
        });

        it('should not call the onLoad handler again if the update returns nothing', () => {
            let now = new Date('2016/01/01 12:00');
            const lastLoadAt = now;
            jest.spyOn(contentItemEditorLists, 'now').mockImplementation(() => now);

            // index will be called twice, the second time no updates will be returned
            expectIndexCalls(enContentItems, []);

            // make initial request
            contentItemEditorLists.load('MockIsContentItem', 'en').onLoad(callback);
            assertIndexCallMade('en');
            assertCallbackCalled(enContentItems);

            // make second request
            now = new Date(now.getTime() + 11 * 1000);
            contentItemEditorLists.load('MockIsContentItem', 'en').onLoad(callback);

            // Since the update returns nothing, the callback
            // should just be called once
            assertIndexCallMade('en', lastLoadAt);
            assertCallbackCalled(enContentItems);
        });

        describe('onComplete', () => {
            it('should only be triggered when all data is loaded', () => {
                let now = new Date('2016/01/01 12:00');
                const lastLoadAt = now;
                jest.spyOn(contentItemEditorLists, 'now').mockImplementation(() => now);

                const newItem = ContentItem.getInstance();
                const updatedItem = ContentItem.new(enContentItems[0].asJson());
                updatedItem.title = 'new title';
                const updates = [newItem, updatedItem];

                // index will be called twice, the second time with some updates
                expectIndexCalls(enContentItems, updates);

                // make initial request
                contentItemEditorLists.load('MockIsContentItem', 'en').onComplete(callback);
                assertIndexCallMade('en');
                assertCallbackCalled(enContentItems);

                // make second request
                now = new Date(now.getTime() + 11 * 1000);
                contentItemEditorLists.load('MockIsContentItem', 'en').onComplete(callback);

                // initially, we just return the cached values
                // at the same time, we make another index call
                // to get the updated items
                const itemsWithUpdates = _.chain(enContentItems).without(enContentItems[0]).union(updates).value();
                assertIndexCallMade('en', lastLoadAt);

                // the callback was only triggered once, after all
                // data was loaded
                assertCallbackCalled(itemsWithUpdates);
            });

            it('should work when a check for updates does not return any new results', () => {
                let now = new Date('2016/01/01 12:00');
                const lastLoadAt = now;
                jest.spyOn(contentItemEditorLists, 'now').mockImplementation(() => now);

                // index will be called twice, the second time no updates will be returned
                expectIndexCalls(enContentItems, []);

                // make initial request
                contentItemEditorLists.load('MockIsContentItem', 'en').onLoad(callback);
                assertIndexCallMade('en');
                assertCallbackCalled(enContentItems);

                // make second request
                now = new Date(now.getTime() + 11 * 1000);
                contentItemEditorLists.load('MockIsContentItem', 'en').onComplete(callback);

                // Since the update returns nothing, the callback
                // should just be called once
                assertIndexCallMade('en', lastLoadAt);
                assertCallbackCalled(enContentItems);
            });
        });

        describe('with multiple locales', () => {
            it('should load only those that are not loaded and return result', () => {
                // index will be called twice
                expectIndexCalls(enContentItems, esContentItems);

                // cache the english results beforehand
                contentItemEditorLists.load('MockIsContentItem', 'en').onLoad(callback);
                assertIndexCallMade('en');
                assertCallbackCalled(enContentItems);

                // now make the call for english and spanish and ensure that only spanish is loaded
                contentItemEditorLists.load('MockIsContentItem', ['en', 'es']).onLoad(callback);
                $timeout.flush();
                assertIndexCallMade('es');
                // it is called once with just the english items, then again
                // with all the items
                assertCallbackCalled(enContentItems, enContentItems.concat(esContentItems));
            });

            it('should work when an update is necessary', () => {
                let now = new Date('2016/01/01 12:00');
                const lastLoadAt = now;
                jest.spyOn(contentItemEditorLists, 'now').mockImplementation(() => now);

                const newItem = ContentItem.getInstance();
                const updatedItem = ContentItem.new(enContentItems[0].asJson());
                updatedItem.title = 'new title';
                const updates = [newItem, updatedItem];

                // index will be called twice, the second time with some updates
                expectIndexCalls(enContentItems, updates);

                // make initial request
                contentItemEditorLists.load('MockIsContentItem', 'en').onLoad(callback);
                assertIndexCallMade('en');
                assertCallbackCalled(enContentItems);

                // make second request
                now = new Date(now.getTime() + 11 * 1000);
                contentItemEditorLists.load('MockIsContentItem', ['en']).onLoad(callback);

                // initially, we just return the cached values
                // at the same time, we make another index call
                // to get the updated items
                const itemsWithUpdates = _.chain(enContentItems).without(enContentItems[0]).union(updates).value();
                $timeout.flush();
                assertIndexCallMade('en', lastLoadAt);
                assertCallbackCalled(enContentItems, itemsWithUpdates);
            });

            it('should filter', () => {
                // index will be called twice
                expectIndexCalls(enContentItems, esContentItems);
                _.invoke(enContentItems, 'ensureLocalePack');

                // cache the english results beforehand
                contentItemEditorLists.load('MockIsContentItem', 'en').onLoad(callback);
                assertIndexCallMade('en');
                assertCallbackCalled(enContentItems);

                // now make the call for english and spanish and ensure that only spanish is loaded
                contentItemEditorLists
                    .load('MockIsContentItem', ['en', 'es'], {
                        locale_pack_id: [enContentItems[0].localePackId],
                    })
                    .onLoad(callback);
                $timeout.flush();
                assertIndexCallMade('es');

                // it is called first when the English items are loaded,
                // then again when all items are loaded
                assertCallbackCalled([enContentItems[0]], [enContentItems[0]]);
            });

            describe('onComplete', () => {
                it('should work when an update is necessary', () => {
                    let now = new Date('2016/01/01 12:00');
                    const lastLoadAt = now;
                    jest.spyOn(contentItemEditorLists, 'now').mockImplementation(() => now);

                    const newItem = ContentItem.getInstance();
                    const updatedItem = ContentItem.new(enContentItems[0].asJson());
                    updatedItem.title = 'new title';
                    const updates = [newItem, updatedItem];

                    // index will be called twice, the second time with some updates
                    expectIndexCalls(enContentItems, updates);

                    // make initial request
                    contentItemEditorLists.load('MockIsContentItem', 'en').onComplete(callback);
                    assertIndexCallMade('en');
                    assertCallbackCalled(enContentItems);

                    // make second request (An array of one locale, as far as
                    // the code is concerned, is the same as mutliple locales)
                    now = new Date(now.getTime() + 11 * 1000);
                    contentItemEditorLists.load('MockIsContentItem', ['en']).onComplete(callback);

                    // initially, we just return the cached values
                    // at the same time, we make another index call
                    // to get the updated items
                    const itemsWithUpdates = _.chain(enContentItems).without(enContentItems[0]).union(updates).value();
                    $timeout.flush();
                    assertIndexCallMade('en', lastLoadAt);
                    assertCallbackCalled(itemsWithUpdates);
                });

                it('should not be triggered until all locales are complete', () => {
                    // index will be called twice
                    expectIndexCalls(enContentItems, esContentItems);

                    contentItemEditorLists.load('MockIsContentItem', ['en', 'es']).onComplete(callback);
                    $timeout.flush();

                    // it is called just once with all the items
                    assertCallbackCalled(enContentItems.concat(esContentItems));
                });
            });
        });

        describe('with extra filters', () => {
            it('should filter by an array of locale_pack_id', () => {
                expectIndexCalls(enContentItems);
                _.invoke(enContentItems, 'ensureLocalePack');

                contentItemEditorLists
                    .load('MockIsContentItem', 'en', {
                        locale_pack_id: [enContentItems[0].localePackId],
                    })
                    .onLoad(callback);
                assertIndexCallMade('en');
                assertCallbackCalled([enContentItems[0]]);
            });

            it('should work with null localePackId', () => {
                expectIndexCalls(enContentItems);
                enContentItems[0].ensureLocalePack();
                enContentItems[1].ensureLocalePack();

                contentItemEditorLists
                    .load('MockIsContentItem', 'en', {
                        locale_pack_id: [enContentItems[0].localePackId, null],
                    })
                    .onLoad(callback);
                assertIndexCallMade('en');
                assertCallbackCalled([enContentItems[0], enContentItems[2]]);
            });
        });

        function assertCallbackCalled(firstCall, secondCall) {
            expect(_.pluck(callback.mock.calls[0][0], 'id').sort()).toEqual(
                _.pluck(firstCall, 'id').sort(),
                'Unexpected result from first call',
            );

            if (secondCall) {
                expect(callback.mock.calls.length).toBe(2, 'Unexpected number of calls');
                expect(_.pluck(callback.mock.calls[1][0], 'id').sort()).toEqual(
                    _.pluck(secondCall, 'id').sort(),
                    'Unexpected result from second call',
                );
            } else {
                expect(callback.mock.calls.length).toBe(1, 'Unexpected number of calls');
            }
            callback.mockClear();
        }

        function expectIndexCalls(...args) {
            let returnValues = _.map(args, contentItems =>
                $q.resolve({
                    result: contentItems,
                }),
            );

            // We add a bunch of empty returns because this leads
            // to very clear assertion failures, rather than bizarre NoMethodErrors
            // where there is an unexpected request.  Assumes that we are using
            // assertNoIndexCallMade in all the right places
            const emptyReturn = $q.resolve({
                result: [],
            });
            returnValues = returnValues.concat([emptyReturn, emptyReturn, emptyReturn, emptyReturn]);

            let i = -1;
            jest.spyOn(ContentItem, 'index').mockImplementation(() => {
                i++;
                return returnValues[i];
            });
        }

        function assertIndexCallMade(locale, updatedSince) {
            const filters = {
                published: false,
                in_users_locale_or_en: null,
                updated_since: 0,
                locale,
            };

            if (updatedSince) {
                filters.updated_since = updatedSince.getTime() / 1000;
            }
            expect(ContentItem.index).toHaveBeenCalledWith(
                {
                    filters,
                },
                { httpQueueOptions: { shouldQueue: false } },
            );
            $timeout.flush();
            ContentItem.index.mockClear();
        }

        function assertNoIndexCallMade() {
            expect(ContentItem.index).not.toHaveBeenCalled();
            $timeout.flush();
        }
    });
});
