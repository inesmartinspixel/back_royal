import 'AngularSpecHelper';
import 'ContentItemEditor/angularModule';
import 'ContentItem/angularModule/spec/_mock/mock_is_content_item';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

describe('ContentItemEditor::DuplicateContentItemDir', () => {
    let elem;
    let SpecHelper;
    let scope;
    let DialogModal;
    let contentItem;
    let ContentItem;
    let $q;
    let resolve;
    let duplicatedContentItem;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.ContentItemEditor', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $q = $injector.get('$q');
            $injector.get('LessonFixtures');
            DialogModal = $injector.get('DialogModal');
            ContentItem = $injector.get('MockIsContentItem');
            contentItem = ContentItem.getInstance({
                title: 'original',
            });

            jest.spyOn(DialogModal, 'hideAlerts').mockImplementation(() => {});
            render();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.scope.contentItem = contentItem;
        renderer.render('<duplicate-content-item content-item="contentItem"></duplicate-content-item>');
        elem = renderer.elem;
        scope = renderer.childScope;

        duplicatedContentItem = ContentItem.getInstance();
        const promise = $q((_resolve, _reject) => {
            resolve = () => {
                _resolve({
                    result: duplicatedContentItem,
                });
                scope.$digest();
            };
            () => {
                _reject();
                scope.$digest();
            };
        });
        jest.spyOn(contentItem, 'duplicate').mockReturnValue(promise);
    }

    it('should have a working cancel button', () => {
        SpecHelper.click(elem, '.cancel');
        expect(DialogModal.hideAlerts).toHaveBeenCalled();
    });

    describe('save', () => {
        it('should save duplicate, hide alert, and navigate to to new location on save', () => {
            jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
            SpecHelper.click(elem, '.submit');
            expect(contentItem.duplicate).toHaveBeenCalled();
            expect(DialogModal.hideAlerts).not.toHaveBeenCalled();
            expect(scope.loadRoute).not.toHaveBeenCalled();
            SpecHelper.expectElementDisabled(elem, 'button');
            resolve();
            scope.$digest();
            expect(DialogModal.hideAlerts).toHaveBeenCalled();
            expect(scope.loadRoute).toHaveBeenCalledWith(duplicatedContentItem.editorUrl);
        });

        it('should have reasonable defaults', () => {
            SpecHelper.click(elem, '.submit');
            expect(contentItem.duplicate).toHaveBeenCalledWith(
                {
                    title: `Duplicated from '${contentItem.title}'`,
                    locale: undefined,
                },
                {
                    in_same_locale_pack: false,
                },
            );
        });
    });

    describe('title', () => {
        it('should allow for setting title', () => {
            SpecHelper.updateInput(elem, 'input[name="title"]', 'new title');
            SpecHelper.click(elem, '.submit');
            expect(contentItem.duplicate).toHaveBeenCalledWith(
                {
                    title: 'new title',
                    locale: undefined,
                },
                {
                    in_same_locale_pack: false,
                },
            );
        });
    });

    describe('isTranslation', () => {
        it('should set a default locale when isTranslation is set', () => {
            expect(scope.dupeOptions.newItemLocale).toBeUndefined();
            expect(scope.dupeOptions.isTranslation).toBe(false);
            SpecHelper.toggleCheckbox(elem, '[name="is-translation"]');
            expect(scope.dupeOptions.newItemLocale).not.toBeUndefined();
            expect(scope.dupeOptions.newItemLocale).not.toEqual(contentItem.locale);
            expect(scope.dupeOptions.isTranslation).toBe(true);
        });

        it('should show translation options if checkbox is selected', () => {
            SpecHelper.toggleCheckbox(elem, '[name="is-translation"]');
            SpecHelper.expectElement(elem, '[name="locale"]');
        });

        it('should filter out original contentItem locale', () => {
            contentItem.locale = 'es';
            scope.$digest();

            const index = _.findIndex(scope.localeOptions, {
                key: 'es',
            });
            expect(index).toBe(-1);
        });

        it('should show warning message if a translation already exists for locale', () => {
            contentItem.locale = 'en';
            contentItem.addLocalePackFixture();

            SpecHelper.toggleCheckbox(elem, '[name="is-translation"]');
            SpecHelper.updateSelectize(elem, '[name="locale"]', 'es');

            const title = contentItem.locale_pack.contentItemForLocale(scope.dupeOptions.newItemLocale).title;
            SpecHelper.expectElementText(elem, '.warning-message', `This translation will replace ${title}`);
        });

        it('should send inSameLocalePack through to duplicate method as expected', () => {
            SpecHelper.toggleCheckbox(elem, '[name="is-translation"]');
            SpecHelper.click(elem, '.submit');
            expect(contentItem.duplicate).toHaveBeenCalledWith(
                {
                    title: `Duplicated from '${contentItem.title}'`,
                    locale: scope.dupeOptions.newItemLocale,
                },
                {
                    in_same_locale_pack: true,
                },
            );
        });
    });
});
