import {
    formattedBrandName,
    getBrandName,
    getBrandEmail,
    getBrandStyleClass,
    setupBrandNameProperties,
    setupBrandEmailProperties,
    setupScopeProperties,
    setupStyleHelpers,
    SMARTLY_DOMAIN,
    QUANTIC_DOMAIN,
    SMARTLY_DOMAIN_STAGING,
    QUANTIC_DOMAIN_STAGING,
} from './AppBrandMixin';

export {
    formattedBrandName,
    getBrandName,
    getBrandEmail,
    getBrandStyleClass,
    setupBrandNameProperties,
    setupBrandEmailProperties,
    setupScopeProperties,
    setupStyleHelpers,
    SMARTLY_DOMAIN,
    QUANTIC_DOMAIN,
    SMARTLY_DOMAIN_STAGING,
    QUANTIC_DOMAIN_STAGING,
};
