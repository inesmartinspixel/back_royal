import { DefaultLocale } from 'Translation';

/*
    This module contains exported functions that can be used to make it easier for our angular
    directives to display the appropriate brand name/logo in the UI.

    NOTE: This mixin assumes that a `currentUser` property will be defined on the scope; however,
        it's not necessary for the mixin to function.
*/

export const SMARTLY_DOMAIN = 'smart.ly';
export const QUANTIC_DOMAIN = 'quantic.edu';
export const SMARTLY_DOMAIN_STAGING = `staging.${SMARTLY_DOMAIN}`;
export const QUANTIC_DOMAIN_STAGING = `staging.${QUANTIC_DOMAIN}`;

/*
 *  Returns the formatted brand name for the given `user`. If no `user` is found,
 *  but a `config` has been passed in, the `config` will be used to determine what
 *  brand name should be returned.
 *  @param `format` - The format of the returned brand name. Supported values are
 *      `long`, `standard`, and `short`.
 *  @param `brand` - The target brand name. Supported values are
 *      `quantic`, `smartly`, and `miyamiya`. Defaults to `quantic`.
 *  @param `locale` - The target locale for any special-case handling.
 *      See `Locale.availablePrefLocales` `code` properties for supported values.
 */
export function formattedBrandName(format, brand = 'quantic', locale = DefaultLocale) {
    // NOTE: Since these are brand names, we don't really care about localizing these, except
    // for the Miya Miya special handling
    if (brand === 'quantic') {
        if (format === 'abbr') {
            return 'Q';
        }
        if (format === 'long') {
            return 'Quantic School of Business and Technology';
        }
        return 'Quantic'; // the standard and short names are the same for Quantic
    }
    if (brand === 'miyamiya') {
        // Miya Miya doesn't support any formats, but should default to English when not in Arabic
        return locale === 'ar' ? 'مية مية' : 'Miya Miya';
    }
    if (format === 'abbr') {
        return 'S';
    }
    if (['long', 'standard'].includes(format)) {
        return 'Smartly Institute'; // the long and standard names are the same for Smartly
    }
    return 'Smartly';
}

/*
 *  Returns the formatted brand name for the given `user`. If no `user` is found,
 *  but a `config` has been passed in, the `config` will be used to determine what
 *  brand name should be returned.
 *  @param `config` - App config that will be used as a fallback to determine the
 *      appropriate brand name value if no `user` is passed in.
 *  @param `format` - The format of the returned brand name. Supported values are
 *      `long`, `standard`, and `short`.
 *  @param `opts` - Object that supports the following properties:
 *      `forceMiyaMiya` - A function that, when true, will result in the Miya Miya
 *          brand name being returned for the property defined on the scope. This
 *          option is most useful when you don't care about the user's state.
 *      `miyaMiya` - When true, will return the Miya Miya brand name if the currentUser
 *          is a Miya Miya user. Defaults to true.
 */
export function getBrandName(user, config = undefined, format = 'short', opts = {}) {
    // By default, we assume Miya Miya branding and check to see
    // if the user is a Miya Miya user.
    opts.miyaMiya = opts.miyaMiya !== false;

    let brand = 'smartly';

    // Sometimes you may want to force the Miya Miya brand name to be returned.
    // In other contexts, you may only want to show the Miya Miya brand name
    // if the user is a Miya Miya user, falling back to a different brand name
    // if they're not a Miya Miya user.
    if ((opts.forceMiyaMiya && opts.forceMiyaMiya()) || (opts.miyaMiya && user?.isMiyaMiya)) {
        brand = 'miyamiya';
    } else if (user?.shouldSeeQuanticBranding || (!user && (config?.isQuantic() || !config))) {
        brand = 'quantic';
    }
    return formattedBrandName(format, brand, user?.pref_locale);
}

/*
 *  Returns the email address for the given `username`, branded according to the `user`.
 *  If no `user` is present, but a `config` has been passed in, the `config` will be used
 *  to determine what brand email should be returned.
 *  @param `config` - App config that will be used as a fallback to determine the
 *      appropriate brand email value if no `user` is passed in.
 */
export function getBrandEmail(user, username, config = undefined) {
    return user?.shouldSeeQuanticBranding || (!user && config?.isQuantic())
        ? `${username}@${QUANTIC_DOMAIN}`
        : `${username}@${SMARTLY_DOMAIN}`;
}

/*
 *  Adds several properties to the scope so you an easily display the appropriate
 *  brand name in the UI. The properties that are added are as follows:
 *      `brandNameAbbr` - Returns the abbreviated version of the brand name.
 *      `brandNameShort` - Returns the shortened version of the brand name.
 *      `brandNameStandard` - Returns the standard version of the brand name.
 *      `brandNameLong` - Returns the long version of the brand name.
 *  @param `config` - App config that will be used as a fallback to determine the
 *      appropriate brand name values if no `currentUser` is found on the scope.
 *  @param `opts` - Object that supports the following properties:
 *      `forceMiyaMiya` - A function that, when true, will result in the Miya Miya
 *          brand name being returned for the property defined on the scope. This
 *          option is most useful when you don't care about the user's state.
 *      `miyaMiya` - When true, will return the Miya Miya brand name if the currentUser
 *          is a Miya Miya user. Defaults to true.
 */
export function setupBrandNameProperties($injector, scope, config = undefined, opts = {}) {
    ['abbr', 'short', 'standard', 'long'].forEach(format => {
        const prop = `brandName${format.charAt(0).toUpperCase()}${format.slice(1)}`;
        Object.defineProperty(scope, prop, {
            get() {
                return getBrandName(scope.currentUser, config, format, opts);
            },
        });
    });
}

/*
 *  Adds properties to the scope so you can easily display the proper email
 *  address with the appropriate domain based on the app brand.
 *  @param `usernames` - An array of email usernames. Each username will have
 *      a camel-cased scope property created for it. For example, if usernames
 *      is `['support']`, then a `brandSupportEmail` scope property will be added.
 */
export function setupBrandEmailProperties($injector, scope, usernames) {
    usernames.forEach(username => {
        const prop = `brand${username.charAt(0).toUpperCase()}${username.slice(1)}Email`;
        Object.defineProperty(scope, prop, {
            get() {
                return getBrandEmail(scope.currentUser, username);
            },
        });
    });
}

/*
 *  Adds an arbitrary number of properties to the scope for convenient access.
 *  @param `props` - An array of `prop` objects where each `prop` object has
 *      a `prop` attribute, representing the name of the property on the scope
 *      that can be referenced, a `quantic` attribute, which is the return value
 *      when the relevant brand is Quantic, and a `fallback` attribute, which is
 *      either the raw return value when the brand is not Quantic or a function
 *      that will return the value when the brand is not Quantic.
 *  @param `config` - App config that will be used as a fallback to determine the
 *      appropriate value to return if no `currentUser` is found on the scope.
 */
export function setupScopeProperties($injector, scope, props, config = undefined) {
    props.forEach(prop => {
        Object.defineProperty(scope, prop.prop, {
            get() {
                if (scope.currentUser?.shouldSeeQuanticBranding || (!scope.currentUser && config?.isQuantic())) {
                    return prop.quantic;
                }
                if (typeof prop.fallback === 'function') {
                    return prop.fallback();
                }
                return prop.fallback;
            },
        });
    });
}

/*
 *  Returns a string that can be useful in the view in conjunction with the `ngClass`
 *  directive when you want to customize the styling of a specific part of the UI depending
 *  on whether the relevant brand is Quantic or Smartly.
 *  @param `config` - App config that will be used as a fallback to determine the
 *      appropriate value to return if no `user` is passed in.
 */
export function getBrandStyleClass(user, config = undefined) {
    return user?.shouldSeeQuanticBranding || (!user && config?.isQuantic()) ? 'quantic' : 'smartly';
}

/*
 *  Adds a method to the scope called `brandStyleClass` that's useful in the view
 *  in conjunction with the `ngClass` directive when you want to customize the styling
 *  of a specific part of the UI depending on whether the relevant brand is Quantic or
 *  Smartly.
 *  @param `config` - App config that will be used as a fallback to determine the
 *      appropriate value to return if no `currentUser` is found on the scope.
 */
export function setupStyleHelpers($injector, scope, config = undefined) {
    scope.brandStyleClass = function brandStyleClass() {
        return getBrandStyleClass(scope.currentUser, config);
    };
}
