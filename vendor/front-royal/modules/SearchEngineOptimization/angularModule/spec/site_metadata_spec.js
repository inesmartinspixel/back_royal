import 'AngularSpecHelper';
import 'SearchEngineOptimization/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import locales from 'SearchEngineOptimization/locales/search_engine_optimization/site_metadata_mixin-en.json';

setSpecLocales(locales);

describe('SiteMetadata.SiteMetadata', () => {
    let $injector;
    let $window;
    let SiteMetadata;
    let SpecHelper;
    let user;
    let contentItem;
    let $translate;
    let $rootScope;

    beforeEach(() => {
        angular.mock.module('SiteMetadata', 'SpecHelper');

        angular.mock.inject([
            '$injector',

            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                SiteMetadata = $injector.get('SiteMetadata');
                $window = $injector.get('$window');
                $translate = $injector.get('$translate');
                $rootScope = $injector.get('$rootScope');

                contentItem = {
                    title: 'content_item_title',
                    description: 'content_item_description',
                    utmCampaign: 'content_item_campaign',
                    entity_metadata: {
                        title: 'entity_title',
                        description: 'entity_description',
                        canonical_url: 'entity_url',
                        tweet_template: 'entity_tweet_template',
                    },
                };

                SpecHelper.stubConfig();
                user = {};
                $rootScope.currentUser = user;
            },
        ]);
    });

    describe('smartlyShareInfo', () => {
        it('should work', () => {
            expect(SiteMetadata.smartlyShareInfo()).not.toBeUndefined();
        });
    });

    describe('smartlyDemoShareInfo', () => {
        it('should work', () => {
            expect(SiteMetadata.smartlyDemoShareInfo()).not.toBeUndefined();
        });
    });

    describe('blueOceanShareInfo', () => {
        it('should work', () => {
            expect(SiteMetadata.blueOceanShareInfo()).not.toBeUndefined();
        });
    });

    describe('contentDefaultShareInfo', () => {
        it('should work', () => {
            expect(SiteMetadata.contentDefaultShareInfo(user, contentItem)).not.toBeUndefined();
        });

        describe('when !isQuantic', () => {
            beforeEach(() => {
                SpecHelper.stubConfig({
                    is_quantic: 'false',
                });
            });

            it('should use the content item entries instead of the entity meta data', () => {
                const shareInfo = SiteMetadata.contentDefaultShareInfo(user, contentItem);

                expect(shareInfo.title).toEqual('content_item_title');
                expect(shareInfo.description).toEqual('content_item_description');

                expect(shareInfo.facebook.title).toEqual('content_item_title');
                expect(shareInfo.facebook.description).toEqual('content_item_description');

                expect(shareInfo.twitter.description).not.toEqual('entity_tweet_template');
            });
        });

        describe('when isQuantic', () => {
            it('should use the entity_metadata instead of the content item data', () => {
                const shareInfo = SiteMetadata.contentDefaultShareInfo(user, contentItem);

                expect(shareInfo.url).toEqual('https://quantic.edu/entity_url');

                expect(shareInfo.title).toEqual('entity_title');
                expect(shareInfo.description).toEqual('entity_description');

                expect(shareInfo.facebook.title).toEqual('entity_title');
                expect(shareInfo.facebook.description).toEqual('entity_description');

                expect(shareInfo.twitter.description).toEqual('entity_tweet_template');
            });

            // NOTE: delete this test once we have translated entity_metadata (if ever)
            it('should use the content item entries instead of the (untranslated) entity meta data', () => {
                $translate.preferredLanguage('es');

                const shareInfo = SiteMetadata.contentDefaultShareInfo(user, contentItem);

                expect(shareInfo.title).toEqual('content_item_title');
                expect(shareInfo.description).toEqual('content_item_description');

                expect(shareInfo.facebook.title).toEqual('content_item_title');
                expect(shareInfo.facebook.description).toEqual('content_item_description');

                expect(shareInfo.twitter.description).not.toEqual('entity_tweet_template');

                $translate.preferredLanguage('en');
            });
        });
    });

    describe('contentCompletedShareInfo', () => {
        it('should work', () => {
            expect(SiteMetadata.contentCompletedShareInfo(user, contentItem)).not.toBeUndefined();
        });
    });

    describe('defaultShareInfo', () => {
        it('should work', () => {
            expect(SiteMetadata.defaultShareInfo).not.toBeUndefined();
        });
    });

    describe('seoCanonicalUrlFromTitle', () => {
        it('should work', () => {
            expect(SiteMetadata.seoCanonicalUrlFromTitle('stream', 'title', 'id')).not.toBeUndefined();
        });

        it('should compute a URL without the title for content items with non-Latin titles', () => {
            expect(SiteMetadata.seoCanonicalUrlFromTitle('course', '数据收集', 'id')).toBe('/course/id');
        });

        it('should compute a URL with a title for content items with latin based titles', () => {
            expect(SiteMetadata.seoCanonicalUrlFromTitle('course', 'test title', 'id')).toBe('/course/test-title/id');
        });
    });

    describe('updateHeaderMetadata', () => {
        it('should work', () => {
            const opts = {
                title: 'title!!!!',
                description: 'description!!!!',
                canonicalUrl: 'canonicalUrl',
                image: 'image',
            };

            // create dummy elements
            $('head').append('<meta name="description" content="">');
            $('head').append('<link href="" rel="canonical" />');
            $('head').append('<meta property="og:image" content=""/>');

            SiteMetadata.updateHeaderMetadata(opts);

            // check a few of the places that should be set by this
            expect($window.document.title).toEqual(opts.title);
            expect($('meta[name="description"]').attr('content')).toEqual(opts.description);
            expect($('link[rel="canonical"]').attr('href')).toEqual(`https://quantic.edu/${opts.canonicalUrl}`);
            expect($('meta[property="og:image"]').attr('content')).toEqual(opts.image);

            // remove dummy elements
            $('meta[name="description"]').remove();
            $('link[rel="canonical"]').remove();
            $('meta[property="og:image"]').remove();
        });
    });
});
