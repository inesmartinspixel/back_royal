import angularModule from 'Playlists/angularModule/scripts/playlists_module';
import template from 'Playlists/angularModule/views/playlist_map.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('playlistMap', [
    '$injector',

    function factory($injector) {
        const ContentAccessHelper = $injector.get('ContentAccessHelper');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                playlists: '<',
                streams: '<',
                // NOTE: hasConcentrations really means if the user has a relevant_cohort (refer to student_dashboard.html
                // and student_dashboard_dir.js to see how hasConcentrations gets passed down into this directive). Institution
                // users fall into this category, as they have playlists attached to their institution that show up in the playlist
                // map, but since they have no relevant_cohort, the bulk of this directive's logic is irrelevant.
                hasConcentrations: '<',
                currentUser: '<',
                activePlaylist: '<',
                recommendedPlaylist: '<',
                relevantCohort: '<',
                activatePlaylist: '<',
            },
            link(scope) {
                //----------------------------
                // Navigation
                //----------------------------

                scope.selectPlaylist = playlist => {
                    if (ContentAccessHelper.canLaunch(playlist) && scope.activatePlaylist) {
                        scope.scrollToTop();

                        // Note: We changed the binding for activatePlaylist from '&' to '=' and called the method slightly differently.
                        // We did this in order to be able to test. This seems to work just fine, but there are some things we don't
                        // understand here. So, if you change something with this callback be weary of dragons.
                        scope.activatePlaylist(playlist);
                    }
                };

                //----------------------------
                // Display Helpers
                //----------------------------

                scope.scrollToTop = () => {
                    // Scroll to top of the container so the user is not left looking at the list of
                    // streams after selecting a playlist
                    const appMainContainer = document.getElementById('app-main-container');
                    appMainContainer.scrollTop = 0;
                };

                //----------------------------
                // Watches
                //----------------------------

                scope.$watchGroup(
                    [
                        'playlists',
                        'hasConcentrations',
                        'activePlaylist',
                        'recommendedPlaylist',
                        'relevantCohort',
                        'currentUser.pastDueForIdVerification',
                    ],
                    () => {
                        if (!scope.playlists) {
                            return;
                        }

                        const playlists = scope.playlists;
                        const requiredPlaylists = !scope.relevantCohort
                            ? playlists
                            : scope.relevantCohort.getRequiredPlaylists(playlists);
                        const requiredPlaylistsByLocalePackId = _.indexBy(requiredPlaylists, 'localePackId');
                        const specializationPlaylists = !scope.relevantCohort
                            ? []
                            : scope.relevantCohort.getSpecializationPlaylists(playlists);

                        function createPlaylistInfo(playlist) {
                            return {
                                playlist,
                                active: scope.activePlaylist && playlist.id === scope.activePlaylist.id,
                                recommended: scope.recommendedPlaylist && playlist.id === scope.recommendedPlaylist.id,
                                locked: !ContentAccessHelper.canLaunch(playlist),
                                complete: playlist.complete,
                            };
                        }

                        // See comment above in the isolate scope attributes configuration section for an explanation of hasConcentrations.
                        if (!scope.hasConcentrations) {
                            scope.playlistCollectionInfos = [
                                {
                                    title: '', // in general, if there's only one playlist collection, we don't need a title
                                    playlistInfos: _.sortBy(playlists, 'title').map(playlist =>
                                        createPlaylistInfo(playlist),
                                    ), // simply use alphabetical order in this case
                                    expanded: true,
                                },
                            ];
                        } else {
                            // create the required playlist infos
                            let numUnlockedRequiredPlaylists = 0;

                            if (scope.relevantCohort) {
                                scope.playlistCollectionInfos = _.map(
                                    scope.relevantCohort.playlist_collections,
                                    playlistCollection => {
                                        const playlistCollectionInfo = {
                                            title: playlistCollection.title,
                                            playlistInfos: _.map(
                                                playlistCollection.required_playlist_pack_ids,
                                                playlistPackId =>
                                                    createPlaylistInfo(requiredPlaylistsByLocalePackId[playlistPackId]),
                                            ),
                                            expanded: _.contains(
                                                playlistCollection.required_playlist_pack_ids,
                                                scope.activePlaylist && scope.activePlaylist.localePackId,
                                            ),
                                        };
                                        numUnlockedRequiredPlaylists += _.filter(
                                            playlistCollectionInfo.playlistInfos,
                                            playlistInfo => playlistInfo.locked === false,
                                        ).length;
                                        return playlistCollectionInfo;
                                    },
                                );
                            }

                            if (!scope.activePlaylist) {
                                const firstPlaylistCollectionInfo = _.first(scope.playlistCollectionInfos);
                                firstPlaylistCollectionInfo.expanded = true;
                            }

                            // Display message if any of the playlists are locked
                            scope.numUnlockedRequiredPlaylists = numUnlockedRequiredPlaylists;

                            scope.hasLockedRequiredPlaylists =
                                scope.numUnlockedRequiredPlaylists <
                                _.reduce(
                                    scope.playlistCollectionInfos,
                                    (memo, playlistCollectionInfo) =>
                                        memo + playlistCollectionInfo.playlistInfos.length,
                                    0,
                                );

                            if (!scope.hasLockedRequiredPlaylists) {
                                //--------------------
                                // Exams
                                //--------------------

                                // In this logic, our main goal is to get the midterm and final exams for the user's relevant cohort.
                                // To do this, first we index all of the streams in the available playlists by their stream locale pack id
                                // and index all of the available streams by their locale pack id. Then we go through each of the required
                                // streams in the relevant cohort schedule and find the exam streams that aren't in the available playlists,
                                // which should leave us with the midterm and final exams (if they exist). It's important to note that this
                                // logic is fairly implicit in the sense that it relies on exam streams to always be required in the schedule
                                // and that the midterm and final exams are not going to be included in the playlists.
                                const playlistStreamsLocalePackIdsMap = {};
                                _.chain(scope.playlists)
                                    .pluck('streamLocalePackIds')
                                    .flatten()
                                    .uniq()
                                    .each(streamLocalePackId => {
                                        playlistStreamsLocalePackIdsMap[streamLocalePackId] = true;
                                    });
                                const streamsByLocalePackId = _.indexBy(scope.streams, 'localePackId');
                                scope.examsNotIncludedInPlaylists = _.chain(
                                    scope.relevantCohort.getRequiredStreamPackIdsFromPeriods(),
                                )
                                    .map(streamLocalePackId => {
                                        const stream = streamsByLocalePackId[streamLocalePackId];
                                        if (stream.exam && !playlistStreamsLocalePackIdsMap[streamLocalePackId]) {
                                            return stream;
                                        }
                                        return undefined;
                                    })
                                    .compact()
                                    .value();
                            }

                            //--------------------
                            // Specializations
                            //--------------------

                            if (specializationPlaylists.length > 0) {
                                scope.specializationPlaylistInfos = _.sortBy(specializationPlaylists, playlist => {
                                    let index = -1;
                                    if (scope.relevantCohort && scope.relevantCohort.specialization_playlist_pack_ids) {
                                        index = scope.relevantCohort.specialization_playlist_pack_ids.indexOf(
                                            playlist.localePackId,
                                        );
                                    }
                                    return index > -1 ? index : 9999;
                                }).map(playlist => createPlaylistInfo(playlist));

                                scope.numRequiredSpecializations = scope.relevantCohort.num_required_specializations;
                                scope.numSpecializationsComplete = _.filter(
                                    scope.specializationPlaylistInfos,
                                    playlistInfo => playlistInfo.complete,
                                ).length;
                            }

                            // setup the locked message if you have any locked required playlists
                            if (scope.hasLockedRequiredPlaylists) {
                                scope.playlistsLockedMessageKey = ContentAccessHelper.playlistsLockedReasonKey(
                                    scope.currentUser,
                                );
                            } else {
                                scope.playlistsLockedMessageKey = undefined;
                            }
                        }
                    },
                );
            },
        };
    },
]);
