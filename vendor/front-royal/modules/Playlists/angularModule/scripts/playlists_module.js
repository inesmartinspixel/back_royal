import 'ContentItem/angularModule';
import 'ImageFadeInOnLoad/angularModule';
import 'Lessons/angularModule';

export default angular.module('FrontRoyal.Playlists', [
    'FrontRoyal.Lessons',
    'FrontRoyal.ContentItem',
    'imageFadeInOnLoad',
]);
