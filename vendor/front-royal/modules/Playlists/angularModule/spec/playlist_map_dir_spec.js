import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Playlists/angularModule';
import 'Playlists/angularModule/spec/_mock/fixtures/playlists';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import setSpecLocales from 'Translation/setSpecLocales';
import playlistMapLocales from 'Playlists/locales/playlists/playlist_map-en.json';
import studentDashboardLearningBoxLocales from 'Lessons/locales/lessons/stream/student_dashboard_learning_box-en.json';

setSpecLocales(playlistMapLocales, studentDashboardLearningBoxLocales);

describe('FrontRoyal.Playlists.PlaylistMapDir', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let Playlist;
    let Cohort;
    let CohortApplication;
    let playlists;
    let activatePlaylist;
    let currentUser;
    let streams;
    let examStreams;
    let Stream;
    let $location;
    let ContentAccessHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                $location = $injector.get('$location');
                SpecHelper = $injector.get('SpecHelper');
                Playlist = $injector.get('Playlist');
                Stream = $injector.get('Lesson.Stream');
                Cohort = $injector.get('Cohort');
                CohortApplication = $injector.get('CohortApplication');
                ContentAccessHelper = $injector.get('ContentAccessHelper');

                $injector.get('CareerProfileFixtures');
                $injector.get('CohortFixtures');
                $injector.get('CohortApplicationFixtures');
                $injector.get('PlaylistFixtures');
                $injector.get('StreamFixtures');

                SpecHelper.stubConfig();

                playlists = [
                    Playlist.fixtures.getInstance(),
                    Playlist.fixtures.getInstance(),
                    Playlist.fixtures.getInstance(),
                    Playlist.fixtures.getInstance(),
                    Playlist.fixtures.getInstance(),
                ];

                streams = _.chain(playlists).pluck('stream_entries').flatten().value();
                // these two streams are used for the Exams section in the playlist map
                examStreams = [
                    Stream.fixtures.getInstance({
                        exam: true,
                    }),
                    Stream.fixtures.getInstance({
                        exam: true,
                    }),
                ];
                streams = streams.concat(examStreams);

                activatePlaylist = jest.fn();

                currentUser = SpecHelper.stubCurrentUser('learner');

                Object.defineProperty(currentUser, 'programType', {
                    value: 'mba',
                    configurable: true,
                });

                currentUser.mba_content_lockable = true;

                setupCohort();

                currentUser.cohort_applications.push(
                    CohortApplication.fixtures.getInstance({
                        status: 'pending',
                    }),
                );
                jest.spyOn($location, 'url').mockImplementation(() => {});
            },
        ]);
    });

    function setupCohort(overrides = {}) {
        currentUser.relevant_cohort = Cohort.fixtures.getInstance(
            angular.extend(
                {
                    playlists,
                    program_type: 'emba',
                },
                overrides,
            ),
        );

        // This is needed to ensure that there are some exams streams in the cohort.
        // These exam streams should not come from playlists as they here to reperesent
        // the midterm and final exams.
        currentUser.relevant_cohort.examPeriods[0].stream_entries = [
            {
                locale_pack_id: examStreams[0].localePackId,
                required: true,
            },
            {
                locale_pack_id: examStreams[1].localePackId,
                required: true,
            },
        ];

        // tricky: within cohorts.js fixtures, we merge the specializations into the available playlists
        // array, to mimic what happens on the server. Fetch the $$playlists from the fixture so that we
        // have all of the specializations within the playlists array at our disposal in tests
        playlists = Cohort.fixtures.getPlaylistsForCohort(currentUser.relevant_cohort);
    }

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.activePlaylist = opts.activePlaylist;
        renderer.scope.activatePlaylist = activatePlaylist;
        renderer.scope.playlists = playlists;
        renderer.scope.streams = streams;
        renderer.scope.currentUser = currentUser;
        renderer.scope.relevantCohort = currentUser.relevant_cohort;
        renderer.scope.hasConcentrations = !!currentUser.relevant_cohort;
        renderer.render(
            '<playlist-map current-user="currentUser" playlists="playlists" streams="streams" relevant-cohort="relevantCohort" has-concentrations="hasConcentrations" active-playlist="activePlaylist" activate-playlist="activatePlaylist"></playlist-map>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();

        jest.spyOn(scope, 'scrollToTop').mockImplementation(() => {});
        $location.url.mockClear();
    }

    describe('selectPlaylist', () => {
        let playlist;

        beforeEach(() => {
            jest.spyOn(ContentAccessHelper, 'canLaunch').mockReturnValue(true);
            playlist = playlists[0];
        });

        describe('when user !canLaunch playlist', () => {
            beforeEach(() => {
                ContentAccessHelper.canLaunch.mockReturnValue(false);
            });

            it('should do nothing', () => {
                render();
                jest.spyOn(scope, 'scrollToTop');
                scope.selectPlaylist(playlist);
                expect(ContentAccessHelper.canLaunch).toHaveBeenCalledWith(playlist);
                expect(scope.scrollToTop).not.toHaveBeenCalled();
                expect(scope.activatePlaylist).not.toHaveBeenCalled();
            });
        });

        describe('when user canLaunch playlist', () => {
            beforeEach(() => {
                ContentAccessHelper.canLaunch.mockReturnValue(true);
            });

            it('should activatePlaylist', () => {
                render();
                scope.selectPlaylist(playlist);
                expect(ContentAccessHelper.canLaunch).toHaveBeenCalledWith(playlist);
                expect(scope.activatePlaylist).toHaveBeenCalledWith(playlist);
            });

            it('should scrollToTop', () => {
                render();
                jest.spyOn(scope, 'scrollToTop').mockImplementation(() => {});
                scope.selectPlaylist(playlist);
                expect(ContentAccessHelper.canLaunch).toHaveBeenCalledWith(playlist);
                expect(scope.scrollToTop).toHaveBeenCalled();
            });
        });
    });

    describe('when relevantCohort.supportsCompactPlaylistMap', () => {
        beforeEach(() => {
            setupCohort({
                playlist_collections: [
                    {
                        title: 'Foo',
                        required_playlist_pack_ids: [playlists[0].localePackId, playlists[1].localePackId],
                    },
                    {
                        title: 'Bar',
                        required_playlist_pack_ids: [
                            playlists[2].localePackId,
                            playlists[3].localePackId,
                            playlists[4].localePackId,
                        ],
                    },
                ],
            });
            jest.spyOn(currentUser.relevant_cohort, 'supportsCompactPlaylistMap', 'get').mockReturnValue(true);
        });

        it('should display the playlist collections on the cohort in order', () => {
            render();
            SpecHelper.expectElements(elem, '.map-container .playlist-collection', 2);
            SpecHelper.expectElementText(
                elem,
                '.map-container .playlist-collection:eq(0) .playlist-collection-header .playlist-collection-title',
                'Foo',
            );
            SpecHelper.expectElementText(
                elem,
                '.map-container .playlist-collection:eq(1) .playlist-collection-header .playlist-collection-title',
                'Bar',
            );
        });

        it('should expand the first playlist collection by default if no activePlaylist is present', () => {
            render();
            const firstPlaylistCollectionElem = SpecHelper.expectElement(
                elem,
                '.map-container .playlist-collection:eq(0)',
            );
            SpecHelper.expectElementHasClass(firstPlaylistCollectionElem, 'expanded');
            SpecHelper.expectElements(
                firstPlaylistCollectionElem,
                '.playlist-collection-playlists-container playlist-box',
                2,
            );

            // other playlists should not be expanded by default
            const secondPlaylistCollectionElem = SpecHelper.expectElement(
                elem,
                '.map-container .playlist-collection:eq(1)',
            );
            SpecHelper.expectElementDoesNotHaveClass(secondPlaylistCollectionElem, 'expanded');
            SpecHelper.expectNoElement(secondPlaylistCollectionElem, '.playlist-collection-playlists-container');
        });

        it('should expand the playlist collection that contains the activePlaylist', () => {
            // set the activePlaylist to a playlist from the second playlist collection
            const playlistLocalePackId =
                currentUser.relevant_cohort.playlist_collections[1].required_playlist_pack_ids[0];
            const activePlaylist = currentUser.relevant_cohort.$$playlists.find(
                playlist => playlist.localePackId === playlistLocalePackId,
            );
            expect(activePlaylist).toBeDefined();

            render({
                activePlaylist,
            });
            expect(scope.activePlaylist).toBeDefined(); // sanity check

            // the activePlaylist is contained in the second playlist collection, not the first playlist collection,
            // so the first playlist collection should be collapsed
            SpecHelper.expectNoElement(
                elem,
                '.map-container .playlist-collection:eq(0) .playlist-collection-playlists-container',
            );

            // the second playlist collection contains the activePlaylist, so it should be expanded
            const secondPlaylistCollectionElem = SpecHelper.expectElement(
                elem,
                '.map-container .playlist-collection:eq(1)',
            );
            SpecHelper.expectElementHasClass(secondPlaylistCollectionElem, 'expanded');
            SpecHelper.expectElements(
                secondPlaylistCollectionElem,
                '.playlist-collection-playlists-container playlist-box',
                3,
            );
        });

        it('should support expanding and collapsing each playlist collection', () => {
            render();

            // expand the second playlist collection
            const secondPlaylistCollectionElem = SpecHelper.expectElement(
                elem,
                '.map-container .playlist-collection:eq(1)',
            );
            SpecHelper.click(secondPlaylistCollectionElem, '.playlist-collection-toggle-container i.fa-chevron-down');
            SpecHelper.expectElements(
                secondPlaylistCollectionElem,
                '.playlist-collection-playlists-container playlist-box',
                3,
            );
            SpecHelper.expectElementHasClass(secondPlaylistCollectionElem, 'expanded');

            // collapse the second playlist collection
            SpecHelper.click(secondPlaylistCollectionElem, '.playlist-collection-toggle-container i.fa-chevron-up');
            SpecHelper.expectNoElement(secondPlaylistCollectionElem, '.playlist-collection-playlists-container');
            SpecHelper.expectElementDoesNotHaveClass(secondPlaylistCollectionElem, 'expanded');

            // collapse the first playlist collection
            const firstPlaylistCollectionElem = SpecHelper.expectElement(
                elem,
                '.map-container .playlist-collection:eq(0)',
            );
            SpecHelper.click(firstPlaylistCollectionElem, '.playlist-collection-toggle-container i.fa-chevron-up');
            SpecHelper.expectNoElement(firstPlaylistCollectionElem, '.playlist-collection-playlists-container');
            SpecHelper.expectElementDoesNotHaveClass(firstPlaylistCollectionElem, 'expanded');
        });

        it('should selectPlaylist when clicking on a playlist', () => {
            render();
            jest.spyOn(scope, 'selectPlaylist').mockImplementation(() => {});
            SpecHelper.click(
                elem,
                '.map-container .playlist-collection:eq(0) .playlist-collection-playlists-container playlist-box:eq(0)',
            );
            expect(scope.selectPlaylist).toHaveBeenCalledWith(
                scope.playlistCollectionInfos[0].playlistInfos[0].playlist,
            );
        });
    });

    describe('when !relevantCohort.supportsCompactPlaylistMap', () => {
        beforeEach(() => {
            jest.spyOn(currentUser.relevant_cohort, 'supportsCompactPlaylistMap', 'get').mockReturnValue(false);
        });

        describe('when playlists unlocked', () => {
            beforeEach(() => {
                currentUser.mba_content_lockable = false;
                render();
            });

            it('should show all playlists', () => {
                expect(scope.numUnlockedRequiredPlaylists).toEqual(5); // normal required playlists
                SpecHelper.expectElements(elem, '.playlist-col-unlocked', scope.numUnlockedRequiredPlaylists);
                SpecHelper.expectElements(
                    elem,
                    '.playlist-col-specialization .playlist-box',
                    scope.relevantCohort.specialization_playlist_pack_ids.length,
                ); // specialization playlists
                SpecHelper.expectNoElement(elem, '.playlist-col-locked');
            });

            it('should selectPlaylist when clicking on a playlist', () => {
                jest.spyOn(scope, 'selectPlaylist').mockImplementation(() => {});
                SpecHelper.click(elem, '.playlist-box:eq(2)');
                expect(scope.selectPlaylist).toHaveBeenCalledWith(playlists[2]);
            });
        });

        describe('when playlists locked', () => {
            it('should show all playlists for mba and a message about why they are locked', () => {
                jest.spyOn(ContentAccessHelper, 'playlistsLockedReasonKey').mockReturnValue(
                    'reason_message_playlist_requires_id_verification',
                );
                Object.defineProperty(currentUser, 'programType', {
                    value: 'mba',
                });
                render();
                SpecHelper.expectElements(elem, '.playlist-box', 7); // required 5 + specializations 2 = 7
                SpecHelper.expectElements(elem, '.playlist-col-unlocked', 1);
                SpecHelper.expectElements(elem, '.playlist-col-locked', 4);
                SpecHelper.expectElementText(
                    elem,
                    '.locked-message',
                    'To unlock, complete your identity verification.',
                );
                expect(ContentAccessHelper.playlistsLockedReasonKey).toHaveBeenCalledWith(scope.currentUser);
            });

            it('should re-validate and update reason on ID verification change', () => {
                Object.defineProperty(currentUser, 'pastDueForIdVerification', {
                    value: true,
                    configurable: true,
                });

                Object.defineProperty(currentUser, 'programType', {
                    value: 'mba',
                });
                render();
                SpecHelper.expectElementText(
                    elem,
                    '.locked-message',
                    'To unlock, complete your identity verification.',
                );

                Object.defineProperty(scope.currentUser, 'pastDueForIdVerification', {
                    value: false,
                    configurable: true,
                });
                scope.$digest();
                SpecHelper.expectElementText(
                    elem,
                    '.locked-message',
                    'The full curriculum unlocks upon commencement of the EMBA program.',
                );
            });
        });

        describe('exams section', () => {
            it('should show the proper number of playlists (but no exams) when locked', () => {
                render();
                SpecHelper.expectElements(elem, '.playlist-col-unlocked', 1);
                SpecHelper.expectElements(elem, '.playlist-col-locked', 4);
                SpecHelper.expectNoElement(elem, '.playlist-col-exam');
                SpecHelper.expectElement(elem, '.map-extras-header'); // specializations
            });

            it('should show the proper number of exams when unlocked', () => {
                Object.defineProperty(currentUser, 'acceptedCohortApplication', {
                    value: true,
                    configurable: true,
                });

                render();
                expect(scope.numUnlockedRequiredPlaylists).toEqual(5); // normal required playlists
                SpecHelper.expectElements(elem, '.playlist-col-unlocked', scope.numUnlockedRequiredPlaylists); // normal playlists
                SpecHelper.expectElements(elem, '.playlist-col-specialization .playlist-box', 2); // specialization playlists
                SpecHelper.expectNoElement(elem, '.playlist-col-locked');
                SpecHelper.expectElements(elem, '.playlist-col-exam', 2); // there are 2 exams streams that are not in the playlists
                SpecHelper.expectElement(elem, '[translate-once="playlists.playlist_map.exams_title"]');
            });
        });

        describe('specializations', () => {
            it('should show the proper number of playlists (including specializations) when locked', () => {
                render();
                SpecHelper.expectElements(elem, '.playlist-col-unlocked', 1);
                SpecHelper.expectElements(elem, '.playlist-col-locked', 4);
                SpecHelper.expectElements(
                    elem,
                    '.playlist-col-specialization',
                    scope.relevantCohort.specialization_playlist_pack_ids.length,
                );
                SpecHelper.expectElement(elem, '.map-extras-header');
            });

            it('should show the proper number of playlists when unlocked', () => {
                Object.defineProperty(currentUser, 'acceptedCohortApplication', {
                    value: true,
                    configurable: true,
                });

                render();
                expect(scope.numUnlockedRequiredPlaylists).toEqual(5); // normal required playlists
                SpecHelper.expectElements(elem, '.playlist-col-unlocked', scope.numUnlockedRequiredPlaylists);
                SpecHelper.expectNoElement(elem, '.playlist-col-locked');
                SpecHelper.expectElements(
                    elem,
                    '.playlist-col-specialization',
                    scope.relevantCohort.specialization_playlist_pack_ids.length,
                ); // there are 2 specializations in cohorts.js
                SpecHelper.expectElement(elem, '[translate-once="playlists.playlist_map.specializations_title"]');
                SpecHelper.expectElementText(elem, '.map-extras-subtitle', '2 OR MORE REQUIRED • 0 COMPLETE');
            });

            it('should allow selecting a specialization playlist', () => {
                Object.defineProperty(currentUser, 'acceptedCohortApplication', {
                    value: true,
                    configurable: true,
                });

                render();
                SpecHelper.click(elem, '.playlist-col-specialization:eq(1) playlist-box');
                expect(scope.activatePlaylist).toHaveBeenCalledWith(playlists[playlists.length - 1]);
            });
        });
    });
});
