import 'AngularSpecHelper';
import 'Playlists/angularModule';
import 'Playlists/angularModule/spec/_mock/fixtures/playlists';

describe('Playlist::PlaylistProgressIcon', () => {
    let Playlist;
    let playlist;
    let SpecHelper;
    let Stream;
    let renderer;
    let elem;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            'MockIguana',
            'PlaylistFixtures',
            'StreamFixtures',
            $injector => {
                Playlist = $injector.get('Playlist');
                Stream = $injector.get('Lesson.Stream');
                Stream.setAdapter('Iguana.Mock.Adapter');
                SpecHelper = $injector.get('SpecHelper');

                playlist = Playlist.fixtures.getInstance();
            },
        ]);
    });

    describe('playlist combinations', () => {
        // helper method to test that a playlist with X streams (Y of them completed) renders with Z segments with appropriate filled state
        function testPlaylistLength(numStreams, numCompleted, expectedSegments, expectedCompleted) {
            it(`should render a playlist with ${numStreams} streams (${numCompleted} completed) with ${expectedSegments} segments`, () => {
                // create list of streams with appropriate length and complete status
                const playlistStreams = [];
                for (let i = 0; i < numStreams; i++) {
                    const stream = Stream.fixtures.getInstance();
                    // set the appropriate number of streams to be complete
                    if (i < numCompleted) {
                        Object.defineProperty(stream.lesson_streams_progress, 'complete', {
                            value: true,
                        });
                    }
                    playlistStreams.push(stream);
                }

                // create playlist with these streams
                const segmentPlaylist = Playlist.fixtures.getInstance({
                    streams: playlistStreams,
                });

                render({
                    playlist: segmentPlaylist,
                });

                // ensure appropriate # of segments with correct state
                SpecHelper.expectElements(elem, '.section > path', expectedSegments);
                SpecHelper.expectElements(elem, '.section > path.filled', expectedCompleted);
                SpecHelper.expectElements(elem, '.section > path:not(.filled)', expectedSegments - expectedCompleted);
            });
        }

        // normal numbers of segments (1-10) with all combinations of completeness
        for (let segments = 1; segments <= 10; segments++) {
            for (let numCompleted = 0; numCompleted <= segments; numCompleted++) {
                testPlaylistLength(segments, numCompleted, segments, numCompleted);
            }
        }

        // screwy playlist with no streams (is this even possible?)
        // Note that the playlist logic is such that it will be marked as complete, since "all" streams are complete
        testPlaylistLength(0, 1, 1, 1);

        // playlist with >10 streams
        testPlaylistLength(12, 0, 10, 0);

        // playlist with >10 and 1 complete - show one segment complete
        testPlaylistLength(20, 1, 10, 1);

        // playlist with >10 and all but 1 complete - show only 9 complete
        testPlaylistLength(20, 19, 10, 9);
    });

    describe('locking', () => {
        it('should hide the arc when the playlist is locked', () => {
            render({
                locked: true,
            });

            SpecHelper.expectNoElement(elem, '.playlist-arc');
        });
    });

    function render(options) {
        renderer = SpecHelper.renderer();

        angular.extend(
            renderer.scope,
            {
                playlist,
                locked: false,
            },
            options,
        );

        renderer.render(
            '<playlist-map-content-item-icon content-item="playlist" locked="locked"></playlist-map-content-item-icon>',
        );
        elem = renderer.elem;
    }
});
