import 'Lessons/angularModule/spec/_mock/fixtures/streams';

angular.module('FrontRoyal.Playlists').factory('PlaylistFixtures', [
    '$injector',
    $injector => {
        const Playlist = $injector.get('Playlist');
        const Stream = $injector.get('Lesson.Stream');
        let counter = 0;
        $injector.get('StreamProgressFixtures');
        $injector.get('StreamFixtures');
        const guid = $injector.get('guid');

        // FIXME: we could potentially DRY this up with all content items
        Playlist.prototype.addLocalePackFixture = function () {
            if (this.locale !== 'en') {
                throw new Error('We are assuming an en locale');
            }
            this.locale_pack = $injector.get('LocalePack').new({
                id: guid.generate(),
                content_items: [
                    {
                        id: this.id,
                        title: this.title,
                        locale: 'en',
                    },
                    {
                        id: guid.generate(),
                        title: 'Spanish',
                        locale: 'es',
                    },
                    {
                        id: guid.generate(),
                        title: 'Chinese',
                        locale: 'zh',
                    },
                ],
            });
            return this;
        };

        Playlist.fixtures = {
            getStreamsForPlaylist(playlist) {
                return _.chain(playlist.streamLocalePackIds)
                    .map(localePackId => Stream.getCachedForLocalePackId(localePackId))
                    .value();
            },

            getStreamsForPlaylists(playlists) {
                return _.chain(playlists)
                    .pluck('streamLocalePackIds')
                    .flatten()
                    .uniq()
                    .map(localePackId => Stream.getCachedForLocalePackId(localePackId))
                    .value();
            },

            sampleAttrs(overrides = {}) {
                const streams = overrides.streams || [
                    Stream.fixtures.getInstance(),
                    Stream.fixtures.getInstance(),
                    Stream.fixtures.getInstance(),
                ];
                delete overrides.streams;

                const streamEntries = streams.map(stream => {
                    stream.ensureLocalePack();
                    return {
                        stream_id: stream.id,
                        locale_pack_id: stream.localePackId,
                        description: `${stream.title} Description`,
                    };
                });

                const id = String(Math.random());

                return angular.extend(
                    {
                        id,
                        title: `My Playlist ${(counter += 1)}`,
                        tag: 'TAG',
                        description: 'This is a really good one',
                        stream_entries: streamEntries,
                        locale: 'en',
                        image: {
                            formats: {
                                '200x150': {
                                    url: 'NA',
                                },
                                original: {
                                    url: 'NA',
                                },
                            },
                        },
                        author: {
                            name: 'Cool Brent',
                        },
                        entity_metadata: {
                            title: 'askjhsdf',
                            description: 'sdjfhsdf',
                            canonical_url: 'askdjhs',
                        },

                        version_id: 'version_id',

                        created_at: 0,
                        updated_at: 0,
                    },
                    overrides,
                );
            },

            getInstance(overrides) {
                const playlist = Playlist.new(this.sampleAttrs(overrides));
                playlist.ensureLocalePack();
                // a promise is created when caching this playlist.  flush it
                // now so it's not interfering with other attempts to deal
                // with timeouts in tests
                $injector.get('$timeout').flush();
                return playlist;
            },
        };

        return {};
    },
]);
