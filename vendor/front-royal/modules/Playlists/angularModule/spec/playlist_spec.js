import 'AngularSpecHelper';
import 'Playlists/angularModule';
import 'Playlists/angularModule/spec/_mock/fixtures/playlists';

describe('Playlist::Playlist', () => {
    let Playlist;
    let $rootScope;
    let playlist;
    let SpecHelper;
    let Stream;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            'MockIguana',
            'PlaylistFixtures',
            'StreamFixtures',
            $injector => {
                Playlist = $injector.get('Playlist');
                Stream = $injector.get('Lesson.Stream');
                Stream.setAdapter('Iguana.Mock.Adapter');
                $rootScope = $injector.get('$rootScope');
                SpecHelper = $injector.get('SpecHelper');

                playlist = Playlist.fixtures.getInstance();
            },
        ]);
    });

    describe('complete', () => {
        it('should be true if all streams are complete', () => {
            playlist.streams.forEach(stream => {
                stream.ensureStreamProgress().complete = true;
            });
            expect(playlist.complete).toBe(true);
        });

        it('should be false if not all streams are complete', () => {
            playlist.streams.slice(0, 2).forEach(stream => {
                stream.ensureStreamProgress().complete = true;
            });
            expect(playlist.complete).toBe(false);
        });
    });

    describe('percentComplete', () => {
        it('should return value based on minutes of client-side lessons completed', () => {
            playlist.streams.forEach(stream => {
                stream.lessons.forEach(lesson => {
                    lesson.ensureLessonProgress().complete = true;
                });
            });
            expect(playlist.percentComplete).toBe(100);
        });

        it('should handle no lessons completed', () => {
            // Not calling ensureLessonProgress on any lessons
            expect(playlist.percentComplete).toBe(0);
        });

        it('should handle completed streams with lessons incomplete', () => {
            playlist.streams.forEach(stream => {
                stream.ensureStreamProgress().complete = true;
            });
            expect(playlist.percentComplete).toBe(100);
        });
    });

    describe('approxPlaylistMinutes', () => {
        const hardcodedComingSoonEstimate = 45;
        const playlistFixtureTimeEstimate = (3 * 3 * 3 * 21) / 60.0;

        it('should estimate playlist minutes', () => {
            expect(playlist.approxPlaylistMinutes).toBeCloseTo(playlistFixtureTimeEstimate);
        });

        it('should be zero for no streams', () => {
            playlist = Playlist.fixtures.getInstance({
                streams: [],
            });
            expect(playlist.approxPlaylistMinutes).toEqual(0);
        });

        it('should estimate playlist minutes with coming soon stream', () => {
            const comingSoonStream = Stream.fixtures.getInstance();
            comingSoonStream.coming_soon = true;
            playlist = Playlist.fixtures.getInstance({
                streams: [
                    Stream.fixtures.getInstance(),
                    Stream.fixtures.getInstance(),
                    Stream.fixtures.getInstance(),
                    comingSoonStream,
                ],
            });
            expect(playlist.approxPlaylistMinutes).toBeCloseTo(
                playlistFixtureTimeEstimate + hardcodedComingSoonEstimate,
            );
        });
    });

    describe('caching', () => {
        it('should clear cache if user changes', () => {
            jest.spyOn(Playlist, 'resetCache').mockImplementation(() => {});
            const user = SpecHelper.stubCurrentUser();
            $rootScope.$apply();

            $rootScope.currentUser = angular.copy(user);
            $rootScope.$apply();

            $rootScope.currentUser = angular.copy(user);
            $rootScope.$apply();
            expect(Playlist.resetCache).toHaveBeenCalledTimes(2);
        });

        describe('caching instances', () => {
            it('should cache an instance that is created with an id', () => {
                const playlist = Playlist.fixtures.getInstance();
                expect(Playlist._playlistCache[playlist.id].playlist).toBe(playlist);
            });
            it('should not cache an instance that is created without an id', () => {
                Playlist.resetCache();
                Playlist.new();
                expect(Playlist._playlistCache).toEqual({});
            });
        });

        describe('getCachedForLocalePackId', () => {
            beforeEach(() => {
                Playlist.resetCache();
            });

            it('should error if the requested playlist is not available', () => {
                expect(() => {
                    Playlist.getCachedForLocalePackId('asdasdasda');
                }).toThrow(new Error('No cached playlist found for locale pack.'));
            });

            it('should not return a playlist that has been uncached', () => {
                playlist = Playlist.fixtures.getInstance();
                playlist.ensureLocalePack();
                expect(Playlist.getCachedForLocalePackId(playlist.localePackId)).toBe(playlist);
                Playlist.resetCache();
                expect(Playlist.getCachedForLocalePackId(playlist.localePackId, false)).toBeUndefined();
            });

            it('should return a playlist and not rebuld the cache on a second request', () => {
                playlist = Playlist.fixtures.getInstance();
                playlist.ensureLocalePack();

                jest.spyOn(Playlist, '_rebuildPlaylistCacheByLocalePackId');
                expect(Playlist.getCachedForLocalePackId(playlist.localePackId)).toBe(playlist);
                expect(Playlist.getCachedForLocalePackId(playlist.localePackId)).toBe(playlist);
                expect(Playlist._rebuildPlaylistCacheByLocalePackId.mock.calls.length).toBe(1);
            });
        });
    });
});
