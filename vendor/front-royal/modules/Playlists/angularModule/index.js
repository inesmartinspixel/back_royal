import './scripts/playlists_module';

import './scripts/playlist_map_dir';
import './scripts/playlist_map_content_item_icon_dir';
import './scripts/stream_entry';
import './scripts/playlist';
import 'Playlists/locales/playlists/playlist_dashboard-en.json';
import 'Playlists/locales/playlists/playlist_library-en.json';
import 'Playlists/locales/playlists/playlist_link_box-en.json';
import 'Playlists/locales/playlists/playlist_map-en.json';
