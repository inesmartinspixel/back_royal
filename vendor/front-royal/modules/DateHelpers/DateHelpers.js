import casperMode from 'casperMode';
import moment from 'moment-timezone';

const ADMIN_REF_TIMEZONE = 'America/Los_Angeles';
const CAPSER_REF_TIMEZONE = 'America/New_York';
const tzForOffsetting = 'America/New_York';

/*
    This method is useful for when you have a date that was
    created in the wrong timezone.  For example, since datetimepicker
    does not have timezone support, when a user clicks on
    2016/01/01 12:00, datetimepicker returns that time in the local
    time zone.  However, we might want 2016/01/01 12:00 pacific time.
    We can't just convert the date, because we don't want to convert
    12:00 ET to 9:00 PT, we want to shift 12:00 ET to 12:00 PT.
*/
export function shiftDateToTimezone(newDate, targetTimezone) {
    const formattedDate = moment(newDate).format('YYYY-MM-DD HH:mm');
    const dateInTargetTimezone = moment.tz(formattedDate, targetTimezone);

    return dateInTargetTimezone.toDate();
}

// For many of our dates we actually cut off on the day prior if the time is before 11pm
export function shiftMonthDayForThreshold(date, threshold) {
    if (threshold === undefined) {
        // if this default threshold value ever changes, be sure to update
        // the deadline_with_threshold method in cohort.rb
        threshold = 23;
    }

    const momentous = moment(date);

    // If the time is before 11pm in your timezone, show the previous day
    if (momentous.hour() < threshold) {
        momentous.subtract(1, 'day');
    }

    return momentous.toDate();
}

// If you add days across daylight savings time, you can end up with a new
// date that is not at the time you expect.  This method makes sure that
// you are always adding dates following the rules of the default timezone
export function addInDefaultTimeZone(date, val, unit) {
    // we don't want to unexpectedly change the time zone on a moment
    // object that gets passed in, so unlike moment.add(), this method
    // always returns a clone rather than updating the original object
    const m = moment(date).clone();
    m.tz(tzForOffsetting).add(val, unit);

    // if you pass in moment, you get moment back.  If you pass in a
    // date, you get a date back.
    return date._isAMomentObject ? m : m.toDate();
}

// timezoneDescriptor is generally null, but can be set to adminRefTimezone if you
// want to see Pacific Time in the editor
export function formattedUserFacingDateTime(date, timezoneDescriptor) {
    return _getTimeStringWithAbbr(date, 'lll', timezoneDescriptor);
}

export function formattedUserFacingMonthDay(date, useDateThreshold) {
    const momentous = _getMomentUserFacingMonthDay(date, useDateThreshold);
    return _amDateFormat(momentous, 'M/D');
}

export function formattedUserFacingDay(date, useDateThreshold) {
    const momentous = _getMomentUserFacingMonthDay(date, useDateThreshold);
    return _amDateFormat(momentous, 'D');
}

export function formattedUserFacingMonthLong(date, useDateThreshold) {
    const momentous = _getMomentUserFacingMonthDay(date, useDateThreshold);
    return _amDateFormat(momentous, 'MMMM');
}

export function formattedUserFacingMonthDayLong(date, useDateThreshold) {
    const momentous = _getMomentUserFacingMonthDay(date, useDateThreshold);
    return _amDateFormat(momentous, 'MMMM D');
}

export function formattedUserFacingMonthDayShort(date, useDateThreshold) {
    const momentous = _getMomentUserFacingMonthDay(date, useDateThreshold);
    return _amDateFormat(momentous, 'MMM D');
}

export function formattedUserFacingMonthDayYearShort(date, useDateThreshold) {
    const momentous = _getMomentUserFacingMonthDay(date, useDateThreshold);
    return _amDateFormat(momentous, 'M/D/YY');
}

export function formattedUserFacingMonthDayYearLong(date, useDateThreshold) {
    const momentous = _getMomentUserFacingMonthDay(date, useDateThreshold);
    return _amDateFormat(momentous, 'MMMM D, YYYY');
}

export function formattedUserFacingMonthYearLong(date, useDateThreshold) {
    const momentous = _getMomentUserFacingMonthDay(date, useDateThreshold);
    return _amDateFormat(momentous, 'MMMM YYYY');
}

export function formattedDateRange(startDate, endDate) {
    const startMoment = moment(startDate);
    const endMoment = moment(endDate);
    let range = startMoment.format('MMMM D');

    if (startMoment.format('M') === endMoment.format('M')) {
        // same month and same year
        range += `–${endMoment.format('D, YYYY')}`;
    } else if (startMoment.format('YY') === endMoment.format('YY')) {
        // different month, same year
        range += `–${endMoment.format('MMMM D, YYYY')}`;
    } else {
        // different month and year
        range += `, ${startMoment.format('YYYY')}–${endMoment.format('MMMM D, YYYY')}`;
    }

    return range;
}

function _getMomentUserFacingMonthDay(date, useDateThreshold) {
    if (!date) {
        return '';
    }
    if (useDateThreshold !== false) {
        useDateThreshold = true;
    }

    if (useDateThreshold) {
        return moment(this.shiftMonthDayForThreshold(date));
    }
    return moment(date);
}

function _getTimeStringWithAbbr(date, format, timezoneDescriptor) {
    if (!date) {
        return '';
    }

    let inTimeZone;
    if (casperMode()) {
        // Default to Eastern timezone if we are in Casper specs. See casper_helper.js
        inTimeZone = _applyTimezone(date, CAPSER_REF_TIMEZONE);
    } else if (timezoneDescriptor === 'adminRefTimezone') {
        inTimeZone = _applyTimezone(date, ADMIN_REF_TIMEZONE);
    } else {
        inTimeZone = _applyTimezone(date, moment.tz.guess());
    }
    return _amDateFormat(inTimeZone, `${format} z`);
}

// adapted from angular-moment.js
function _isUndefinedOrNull(val) {
    return val === undefined || val === null;
}

// adapted from angular-moment.js #amDateFormat
function _amDateFormat(value, format) {
    if (_isUndefinedOrNull(value)) {
        return '';
    }

    const momentous = moment(value);
    if (!momentous.isValid()) {
        return '';
    }

    return momentous.format(format);
}

// adapted from angular-moment.js#amTimezone
function _applyTimezone(date, timezone) {
    const momentous = moment(date);

    if (momentous.tz) {
        return momentous.tz(timezone);
    }
    return momentous;
}
