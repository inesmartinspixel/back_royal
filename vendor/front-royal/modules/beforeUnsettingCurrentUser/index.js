export default async function beforeUnsettingCurrentUser(injector) {
    const EventLogger = injector.get('EventLogger');
    const frontRoyalStore = injector.get('frontRoyalStore');

    await EventLogger.tryToSaveBuffer(true);
    await frontRoyalStore.beforeUnsettingCurrentUser();
}
