export default angular
    .module('reverseFilter', [])
    .filter('reverse', () => items => (items ? items.slice().reverse() : items));
