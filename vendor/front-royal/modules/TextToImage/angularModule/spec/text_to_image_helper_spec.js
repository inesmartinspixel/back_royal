import 'AngularSpecHelper';
import 'TextToImage/angularModule';

describe('TextToImage', () => {
    let $injector;
    let TextToImageHelper;
    let textToImageHelper;

    beforeEach(() => {
        angular.mock.module('TextToImage', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                TextToImageHelper = $injector.get('TextToImageHelper');
            },
        ]);

        textToImageHelper = new TextToImageHelper();
    });

    describe('isASCII', () => {
        it('should return true when all characters passed in are ascii', () => {
            const isAscii = textToImageHelper.isASCII('Some Ascii Text');
            expect(isAscii).toBe(true);
        });

        it('should return false when some characters passed in are non-ascii', () => {
            const isAscii = textToImageHelper.isASCII('Some 保留所有权利 Text');
            expect(isAscii).toBe(false);
        });

        it('should return false when all characters passed in are non-ascii', () => {
            const isAscii = textToImageHelper.isASCII('保留所有权利');
            expect(isAscii).toBe(false);
        });
    });
});
