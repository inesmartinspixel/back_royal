export default async function flushStoredSentryErrors(injector) {
    const frontRoyalStore = injector.get('frontRoyalStore');
    const ErrorLogService = injector.get('ErrorLogService');

    return frontRoyalStore.retryAfterHandledError(async db => {
        const sentryErrors = await db.sentryErrors.toArray();
        const promises = sentryErrors.map(async sentryError => {
            try {
                await ErrorLogService.logSerializedSentryError(sentryError);
            } catch (err) {
                // no-op.
                // logToSentry should never error.  We capture
                // all exceptions internally there.  However, if we've missed
                // something and it does fail, we don't want to leave the
                // record in sentryErrors, because we would continuously
                // call logToSentry on every flush.  So, we have the
                // try/catch just as a backstop.
            }

            await db.sentryErrors.delete(sentryError.id);
        });
        return Promise.all(promises);
    });
}
