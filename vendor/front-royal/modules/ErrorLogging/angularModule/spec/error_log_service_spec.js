import moment from 'moment-timezone';
import * as Sentry from '@sentry/browser';
import 'AngularSpecHelper';
import 'ErrorLogging/angularModule';
import 'Users/angularModule';
import 'PrioritizedInterceptors/angularModule';
import 'IguanaSuperModelAndAClassAbove';
import 'EventLogger/angularModule';

describe('Error Logging: Exception Handling', () => {
    let $rootScope;
    let ErrorLogService;
    let $timeout;
    let $injector;

    describe('error interception', () => {
        let EventLogger;

        beforeEach(() => {
            angular
                .module('fakeModule', ['FrontRoyal.ErrorLogService'], () => {})
                .provider('$exceptionHandler', {
                    $get: ['ErrorLogService', _ErrorLogService => _ErrorLogService],
                });

            angular.mock.module('fakeModule', 'FrontRoyal.ErrorLogService', 'EventLogger', 'Iguana', 'SpecHelper');

            angular.mock.module(($provide, ErrorLogServiceProvider) => {
                const origService = ErrorLogServiceProvider.$get();
                ErrorLogService = jest.fn().mockImplementation((a, b) => {
                    origService(a, b);
                });
                $provide.value('ErrorLogService', ErrorLogService);
            });

            angular.mock.inject([
                '$injector',
                _$injector => {
                    $injector = _$injector;
                    $rootScope = $injector.get('$rootScope');
                    EventLogger = $injector.get('EventLogger');
                },
            ]);

            jest.spyOn(EventLogger.prototype, 'log');
        });

        it('should intercept error handling', () => {
            $rootScope.$apply(() => {
                // throw TypeError intentionally within digest
                const num = 12345;
                num.substr(1, 1);
            });
            expect(ErrorLogService).toHaveBeenCalled();
            expect(EventLogger.prototype.log).toHaveBeenCalled();
        });

        it('should not notify for known status 406 errors', () => {
            $rootScope.$apply(() => {
                // throw TypeError intentionally within digest
                throw new Error('Possibly unhandled rejection - blah blah blah {"status":406}');
            });
            expect(ErrorLogService).toHaveBeenCalled();
            expect(EventLogger.prototype.log).not.toHaveBeenCalled();
        });

        it('should not notify for known status 401 errors', () => {
            $rootScope.$apply(() => {
                // throw TypeError intentionally within digest
                throw new Error('Possibly unhandled rejection - blah blah blah {"status":401}');
            });
            expect(ErrorLogService).toHaveBeenCalled();
            expect(EventLogger.prototype.log).not.toHaveBeenCalled();
        });

        it('should not notify for known status 409 errors', () => {
            $rootScope.$apply(() => {
                // throw TypeError intentionally within digest
                throw new Error('Possibly unhandled rejection - blah blah blah {"status":409}');
            });
            expect(ErrorLogService).toHaveBeenCalled();
            expect(EventLogger.prototype.log).not.toHaveBeenCalled();
        });
    });

    describe('notify', () => {
        let EventLogger;
        let scope;
        let SpecHelper;

        function mockSentry() {
            SpecHelper.stubConfig({
                sentry_dsn: 'some_dsn',
            });

            scope = {};
            scope.setTag = jest.fn();
            scope.setFingerprint = jest.fn();
            scope.setLevel = jest.fn();
            scope.setExtra = jest.fn();

            jest.spyOn(Sentry, 'init').mockImplementation(() => {});
            jest.spyOn(Sentry, 'captureException').mockImplementation(() => {});
            jest.spyOn(Sentry, 'setUser').mockImplementation(() => {});
            jest.spyOn(Sentry, 'withScope');
            jest.spyOn(Sentry.Hub.prototype, 'pushScope').mockReturnValue(scope); // allows us to test withScope mechanics
        }

        beforeEach(() => {
            angular
                .module('fakeModule', ['Iguana'], () => {})
                .config(IguanaProvider => {
                    const uri = `${window.ENDPOINT_ROOT}/api`;
                    IguanaProvider.setAdapter('Iguana.Adapters.RestfulIdStyle');
                    IguanaProvider.setBaseUrl(uri);
                });

            angular.mock.module(
                'fakeModule',
                'FrontRoyal.ErrorLogService',
                'FrontRoyal.Users',
                'EventLogger',
                'Iguana',
                'SpecHelper',
            );

            angular.mock.inject([
                '$injector',
                _$injector => {
                    $injector = _$injector;
                    SpecHelper = $injector.get('SpecHelper');
                    ErrorLogService = $injector.get('ErrorLogService');
                    EventLogger = $injector.get('EventLogger');
                    $timeout = $injector.get('$timeout');
                    $rootScope = $injector.get('$rootScope');

                    $rootScope.currentUser = {
                        email: 'someuser@email.com',
                        id: 1,
                    };

                    EventLogger.disabled = true;
                },
            ]);
        });

        it('should log to Sentry', () => {
            mockSentry();

            const e = new Error('message');
            const result = ErrorLogService.notify(e, 'cause', {
                extra: 'ok',
            });
            $timeout.flush(); // flush the promise
            assertSentryCalled(e);
            expect(result).toBe(true);
        });

        it('should store serialized sentry errors that can be logged later frontRoyalStore is enabled', () => {
            const frontRoyalStore = $injector.get('frontRoyalStore');
            const $q = $injector.get('$q');
            const savedRecords = [];
            const db = {
                sentryErrors: {
                    put: record => {
                        savedRecords.push(record);
                    },
                },

                // since we're mocking out config and retryAfterHandledError,
                // we run into this.  Not really relevant to this spec, just need
                // to make it stop erroring
                configRecords: {
                    put: jest.fn(),
                },
            };
            jest.spyOn(frontRoyalStore, 'retryAfterHandledError').mockImplementation(fn => {
                fn(db);

                // This returns a native promise, but since we're testing
                // things with angular helpers, we use $q
                return $q.resolve();
            });

            jest.spyOn(frontRoyalStore, 'flush').mockImplementation();

            frontRoyalStore.enabled = true;
            mockSentry();

            const e = new Error('message');
            const result = ErrorLogService.notify(e, 'cause', {
                extra: 'ok',
            });
            expect(result).toBe(true);

            // Assert that the sentryErrors have been saved to the database
            expect(savedRecords.length).toBe(1);
            const record = savedRecords[0];
            expect(record.id).not.toBeUndefined();

            // Assert that these records work when passed into logSerializedSentryError,
            // which is what flushStoredSentryErrors is going to do with them
            ErrorLogService.logSerializedSentryError(record);
            $timeout.flush();
            assertSentryCalled(e);
            expect(frontRoyalStore.flush).toHaveBeenCalled();
        });

        function assertSentryCalled(e) {
            // init
            expect(Sentry.init).toHaveBeenCalled();
            expect(Sentry.init.mock.calls[0][0].dsn).toEqual('some_dsn');

            // meta
            expect(Sentry.withScope).toHaveBeenCalled();
            expect(scope.setLevel).toHaveBeenCalledWith('error');
            expect(scope.setFingerprint).toHaveBeenCalledWith(['message']);
            expect(scope.setExtra).toHaveBeenCalledWith('cause', 'cause');
            expect(scope.setExtra).toHaveBeenCalledWith('extra', 'ok');
            expect(Sentry.setUser).toHaveBeenCalledWith({
                email: 'someuser@email.com',
                id: 1,
            });

            // logging
            expect(Sentry.captureException).toHaveBeenCalled();
            expect(Sentry.captureException.mock.calls[0][0].message).toEqual(e.message);
            expect(Sentry.captureException.mock.calls[0][0].stack).toEqual(e.stack);
        }

        it('should support setting the level', () => {
            mockSentry();

            const e = new Error('message');
            ErrorLogService.notify(e, 'cause', {
                some: 'info',
                level: 'warning',
            });
            $timeout.flush(); // flush the promise

            // meta
            expect(scope.setLevel).toHaveBeenCalledWith('warning');
            expect(scope.setFingerprint).toHaveBeenCalledWith(['message']);
            expect(scope.setExtra).toHaveBeenCalledWith('cause', 'cause');
            expect(scope.setExtra).toHaveBeenCalledWith('some', 'info');

            // logging
            expect(Sentry.captureException).toHaveBeenCalledWith(e);
        });

        it('should support an `extra` property on the error', () => {
            // the date format only has millisecond accuracy, so we set
            // the start to one ms ago for use in our later comparison
            const start = moment().utc().subtract(1, 'millisecond').toDate();
            mockSentry();

            const e = new Error('message');
            e.extra = {
                some: 'info',
            };
            ErrorLogService.notify(e);
            $timeout.flush(); // flush the promise

            // meta
            expect(scope.setLevel).toHaveBeenCalledWith('error');
            expect(scope.setFingerprint).toHaveBeenCalledWith(['message']);
            expect(scope.setExtra).toHaveBeenCalledWith('cause', undefined);
            expect(scope.setExtra).toHaveBeenCalledWith('some', 'info');
            expect(scope.setExtra).toHaveBeenCalledWith('offline', false);

            let clientTimestamp;
            scope.setExtra.mock.calls.forEach(c => {
                if (c[0] === 'clientTimestamp') {
                    clientTimestamp = moment.utc(c[1], 'YYYY-MM-DDTHH:mm:ss.SSS').toDate();
                }
            });

            // clientTimestamp should have been set to now()
            // since we started this spec (>= because we don't
            // have millisecond accuracy here)
            expect(clientTimestamp >= start).toBe(true);
            expect(clientTimestamp <= moment().utc().toDate()).toBe(true);

            // logging
            expect(Sentry.captureException).toHaveBeenCalledWith(e);
        });

        it('should log to EventLogger', () => {
            const e = new Error('ok');
            jest.spyOn(EventLogger.prototype, 'log');
            const payload = {
                a: 'b',
            };
            const result = ErrorLogService.notify(e, 'my cause', payload);

            expect(EventLogger.prototype.log).toHaveBeenCalledWith('error', {
                label: 'ok',
                message: 'ok',
            });

            expect(result).toBe(false);
        });

        it('should truncate message on error events in log to EventLogger', () => {
            let message = '';
            for (let i = 0; i < 1029; i++) {
                message = `${message}.`;
            }
            const e = new Error(message);
            jest.spyOn(EventLogger.prototype, 'log');
            ErrorLogService.notify(e);
            expect(EventLogger.prototype.log.mock.calls[0][1].message.length).toBe(1024);
            expect(EventLogger.prototype.log.mock.calls[0][1].label.length).toBe(256);
        });

        it('should cap the maximum number of a given error, and a log an error when limit is hit', () => {
            mockSentry();
            jest.spyOn(EventLogger.prototype, 'log');
            let result;

            const payload = {};

            function logError() {
                const e = new Error('ok');
                // eslint-disable-next-line no-shadow
                const result = ErrorLogService.notify(e, 'my cause', payload);
                return result;
            }

            // log 10 times
            for (let i = 0; i < 10; i++) {
                result = logError();
                expect(result).toBe(true);
                expect(_.last(EventLogger.prototype.log.mock.calls)[0]).toBe('error');
                expect(_.last(EventLogger.prototype.log.mock.calls)[1].message).toBe('ok');
            }

            // 10 total calls
            expect(EventLogger.prototype.log.mock.calls.length).toEqual(10);

            // 11th time, error will be a special error and original error suppressed
            result = logError();
            expect(result).toBe(true);
            expect(_.last(EventLogger.prototype.log.mock.calls)[0]).toBe('error');
            expect(_.last(EventLogger.prototype.log.mock.calls)[1].message).toBe(
                'Error already logged 10 times, further errors suppressed: ok',
            );
            expect(EventLogger.prototype.log.mock.calls.length).toEqual(11);

            // 11th time, no errors logged at all
            result = logError();
            expect(EventLogger.prototype.log.mock.calls.length).toEqual(11);
        });

        it('should work with a string passed in', () => {
            jest.spyOn(EventLogger.prototype, 'log');
            ErrorLogService.notify('my message');

            expect(EventLogger.prototype.log).toHaveBeenCalledWith('error', {
                label: 'my message',
                message: 'my message',
            });
        });
    });
});
