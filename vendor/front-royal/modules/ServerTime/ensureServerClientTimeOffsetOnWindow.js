import getServerClientTimeOffset from './getServerClientTimeOffset';

// The earlier this is called during pageload, the better.  This is because
// the smaller that window.performance.now() is, the more precise we
// can be about the server time
export default function ensureServerClientTimeOffsetOnWindow() {
    if (!window.serverClientTimeOffset) {
        if (!window.serverGeneratedTimestamp) {
            throw new Error('Expected to find serverGeneratedTimestamp on window');
        }

        window.serverClientTimeOffset = getServerClientTimeOffset(
            window.serverGeneratedTimestamp,
            window.performance.now(),
        );
    }

    return window.serverClientTimeOffset;
}
