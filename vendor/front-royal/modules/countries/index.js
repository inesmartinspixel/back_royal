import { sortBy } from 'lodash/fp';

// FIXME: https://trello.com/c/OVFLbiZx/924-feat-add-localization-support-for-countries-dropdowns
const countries = sortBy('text')([
    {
        text: '',
        value: '',
        telPrefix: '',
    },
    {
        text: 'Afghanistan',
        value: 'AF',
        telPrefix: '+93',
    },
    {
        text: 'Albania',
        value: 'AL',
        telPrefix: '+355',
    },
    {
        text: 'Algeria',
        value: 'DZ',
        telPrefix: '+213',
    },
    {
        text: 'Andorra',
        value: 'AD',
        telPrefix: '+376',
    },
    {
        text: 'Angola',
        value: 'AO',
        telPrefix: '+244',
    },
    {
        text: 'Antigua and Barbuda',
        value: 'AG',
        telPrefix: '+1',
    },
    {
        text: 'Argentina',
        value: 'AR',
        telPrefix: '+54',
    },
    {
        text: 'Armenia',
        value: 'AM',
        telPrefix: '+374',
    },
    {
        text: 'Australia',
        value: 'AU',
        telPrefix: '+61',
    },
    {
        text: 'Austria',
        value: 'AT',
        telPrefix: '+43',
    },
    {
        text: 'Azerbaijan',
        value: 'AZ',
        telPrefix: '+994',
    },
    {
        text: 'Bahamas, The',
        value: 'BS',
        telPrefix: '+1',
    },
    {
        text: 'Bahrain',
        value: 'BH',
        telPrefix: '+973',
    },
    {
        text: 'Bangladesh',
        value: 'BD',
        telPrefix: '+880',
    },
    {
        text: 'Barbados',
        value: 'BB',
        telPrefix: '+1',
    },
    {
        text: 'Belarus',
        value: 'BY',
        telPrefix: '+375',
    },
    {
        text: 'Belgium',
        value: 'BE',
        telPrefix: '+32',
    },
    {
        text: 'Belize',
        value: 'BZ',
        telPrefix: '+501',
    },
    {
        text: 'Benin',
        value: 'BJ',
        telPrefix: '+229',
    },
    {
        text: 'Bhutan',
        value: 'BT',
        telPrefix: '+975',
    },
    {
        text: 'Bolivia',
        value: 'BO',
        telPrefix: '+591',
    },
    {
        text: 'Bosnia and Herzegovina',
        value: 'BA',
        telPrefix: '+387',
    },
    {
        text: 'Botswana',
        value: 'BW',
        telPrefix: '+267',
    },
    {
        text: 'Brazil',
        value: 'BR',
        telPrefix: '+55',
    },
    {
        text: 'Brunei',
        value: 'BN',
        telPrefix: '+673',
    },
    {
        text: 'Bulgaria',
        value: 'BG',
        telPrefix: '+359',
    },
    {
        text: 'Burkina Faso',
        value: 'BF',
        telPrefix: '+226',
    },
    {
        text: 'Burundi',
        value: 'BI',
        telPrefix: '+257',
    },
    {
        text: 'Cambodia',
        value: 'KH',
        telPrefix: '+855',
    },
    {
        text: 'Cameroon',
        value: 'CM',
        telPrefix: '+237',
    },
    {
        text: 'Canada',
        value: 'CA',
        telPrefix: '+1',
    },
    {
        text: 'Cape Verde',
        value: 'CV',
        telPrefix: '+238',
    },
    {
        text: 'Central African Republic',
        value: 'CF',
        telPrefix: '+236',
    },
    {
        text: 'Chad',
        value: 'TD',
        telPrefix: '+235',
    },
    {
        text: 'Chile',
        value: 'CL',
        telPrefix: '+56',
    },
    {
        text: "China, People's Republic of",
        value: 'CN',
        telPrefix: '+86',
    },
    {
        text: 'Colombia',
        value: 'CO',
        telPrefix: '+57',
    },
    {
        text: 'Comoros',
        value: 'KM',
        telPrefix: '+269',
    },
    {
        text: 'Congo, (Congo Kinshasa)',
        value: 'CD',
        telPrefix: '+243',
    },
    {
        text: 'Congo, (Congo Brazzaville)',
        value: 'CG',
        telPrefix: '+242',
    },
    {
        text: 'Costa Rica',
        value: 'CR',
        telPrefix: '+506',
    },
    {
        text: "Cote d'Ivoire (Ivory Coast)",
        value: 'CI',
        telPrefix: '+225',
    },
    {
        text: 'Croatia',
        value: 'HR',
        telPrefix: '+385',
    },
    {
        text: 'Cuba',
        value: 'CU',
        telPrefix: '+53',
    },
    {
        text: 'Cyprus',
        value: 'CY',
        telPrefix: '+357',
    },
    {
        text: 'Czech Republic',
        value: 'CZ',
        telPrefix: '+420',
    },
    {
        text: 'Denmark',
        value: 'DK',
        telPrefix: '+45',
    },
    {
        text: 'Djibouti',
        value: 'DJ',
        telPrefix: '+253',
    },
    {
        text: 'Dominica',
        value: 'DM',
        telPrefix: '+1',
    },
    {
        text: 'Dominican Republic',
        value: 'DO',
        telPrefix: '+1',
    },
    {
        text: 'Ecuador',
        value: 'EC',
        telPrefix: '+593',
    },
    {
        text: 'Egypt',
        value: 'EG',
        telPrefix: '+20',
    },
    {
        text: 'El Salvador',
        value: 'SV',
        telPrefix: '+503',
    },
    {
        text: 'Equatorial Guinea',
        value: 'GQ',
        telPrefix: '+240',
    },
    {
        text: 'Eritrea',
        value: 'ER',
        telPrefix: '+291',
    },
    {
        text: 'Estonia',
        value: 'EE',
        telPrefix: '+372',
    },
    {
        text: 'Ethiopia',
        value: 'ET',
        telPrefix: '+251',
    },
    {
        text: 'Fiji',
        value: 'FJ',
        telPrefix: '+679',
    },
    {
        text: 'Finland',
        value: 'FI',
        telPrefix: '+358',
    },
    {
        text: 'France',
        value: 'FR',
        telPrefix: '+33',
    },
    {
        text: 'Gabon',
        value: 'GA',
        telPrefix: '+241',
    },
    {
        text: 'Gambia, The',
        value: 'GM',
        telPrefix: '+220',
    },
    {
        text: 'Georgia',
        value: 'GE',
        telPrefix: '+995',
    },
    {
        text: 'Germany',
        value: 'DE',
        telPrefix: '+49',
    },
    {
        text: 'Ghana',
        value: 'GH',
        telPrefix: '+233',
    },
    {
        text: 'Greece',
        value: 'GR',
        telPrefix: '+30',
    },
    {
        text: 'Grenada',
        value: 'GD',
        telPrefix: '+1',
    },
    {
        text: 'Guatemala',
        value: 'GT',
        telPrefix: '+502',
    },
    {
        text: 'Guinea',
        value: 'GN',
        telPrefix: '+224',
    },
    {
        text: 'Guinea-Bissau',
        value: 'GW',
        telPrefix: '+245',
    },
    {
        text: 'Guyana',
        value: 'GY',
        telPrefix: '+592',
    },
    {
        text: 'Haiti',
        value: 'HT',
        telPrefix: '+509',
    },
    {
        text: 'Honduras',
        value: 'HN',
        telPrefix: '+504',
    },
    {
        text: 'Hungary',
        value: 'HU',
        telPrefix: '+36',
    },
    {
        text: 'Iceland',
        value: 'IS',
        telPrefix: '+354',
    },
    {
        text: 'India',
        value: 'IN',
        telPrefix: '+91',
    },
    {
        text: 'Indonesia',
        value: 'ID',
        telPrefix: '+62',
    },
    {
        text: 'Iran',
        value: 'IR',
        telPrefix: '+98',
    },
    {
        text: 'Iraq',
        value: 'IQ',
        telPrefix: '+964',
    },
    {
        text: 'Ireland',
        value: 'IE',
        telPrefix: '+353',
    },
    {
        text: 'Israel',
        value: 'IL',
        telPrefix: '+972',
    },
    {
        text: 'Italy',
        value: 'IT',
        telPrefix: '+39',
    },
    {
        text: 'Jamaica',
        value: 'JM',
        telPrefix: '+1',
    },
    {
        text: 'Japan',
        value: 'JP',
        telPrefix: '+81',
    },
    {
        text: 'Jordan',
        value: 'JO',
        telPrefix: '+962',
    },
    {
        text: 'Kazakhstan',
        value: 'KZ',
        telPrefix: '+7',
    },
    {
        text: 'Kenya',
        value: 'KE',
        telPrefix: '+254',
    },
    {
        text: 'Kiribati',
        value: 'KI',
        telPrefix: '+686',
    },
    {
        text: 'Korea, North',
        value: 'KP',
        telPrefix: '+850',
    },
    {
        text: 'Korea, South',
        value: 'KR',
        telPrefix: '+82',
    },
    {
        text: 'Kuwait',
        value: 'KW',
        telPrefix: '+965',
    },
    {
        text: 'Kyrgyzstan',
        value: 'KG',
        telPrefix: '+996',
    },
    {
        text: 'Laos',
        value: 'LA',
        telPrefix: '+856',
    },
    {
        text: 'Latvia',
        value: 'LV',
        telPrefix: '+371',
    },
    {
        text: 'Lebanon',
        value: 'LB',
        telPrefix: '+961',
    },
    {
        text: 'Lesotho',
        value: 'LS',
        telPrefix: '+266',
    },
    {
        text: 'Liberia',
        value: 'LR',
        telPrefix: '+231',
    },
    {
        text: 'Libya',
        value: 'LY',
        telPrefix: '+218',
    },
    {
        text: 'Liechtenstein',
        value: 'LI',
        telPrefix: '+423',
    },
    {
        text: 'Lithuania',
        value: 'LT',
        telPrefix: '+370',
    },
    {
        text: 'Luxembourg',
        value: 'LU',
        telPrefix: '+352',
    },
    {
        text: 'Macedonia',
        value: 'MK',
        telPrefix: '+389',
    },
    {
        text: 'Madagascar',
        value: 'MG',
        telPrefix: '+261',
    },
    {
        text: 'Malawi',
        value: 'MW',
        telPrefix: '+265',
    },
    {
        text: 'Malaysia',
        value: 'MY',
        telPrefix: '+60',
    },
    {
        text: 'Maldives',
        value: 'MV',
        telPrefix: '+960',
    },
    {
        text: 'Mali',
        value: 'ML',
        telPrefix: '+223',
    },
    {
        text: 'Malta',
        value: 'MT',
        telPrefix: '+356',
    },
    {
        text: 'Marshall Islands',
        value: 'MH',
        telPrefix: '+692',
    },
    {
        text: 'Mauritania',
        value: 'MR',
        telPrefix: '+222',
    },
    {
        text: 'Mauritius',
        value: 'MU',
        telPrefix: '+230',
    },
    {
        text: 'Mexico',
        value: 'MX',
        telPrefix: '+52',
    },
    {
        text: 'Micronesia',
        value: 'FM',
        telPrefix: '+691',
    },
    {
        text: 'Moldova',
        value: 'MD',
        telPrefix: '+373',
    },
    {
        text: 'Monaco',
        value: 'MC',
        telPrefix: '+377',
    },
    {
        text: 'Mongolia',
        value: 'MN',
        telPrefix: '+976',
    },
    {
        text: 'Montenegro',
        value: 'ME',
        telPrefix: '+382',
    },
    {
        text: 'Morocco',
        value: 'MA',
        telPrefix: '+212',
    },
    {
        text: 'Mozambique',
        value: 'MZ',
        telPrefix: '+258',
    },
    {
        text: 'Myanmar (Burma)',
        value: 'MM',
        telPrefix: '+95',
    },
    {
        text: 'Namibia',
        value: 'NA',
        telPrefix: '+264',
    },
    {
        text: 'Nauru',
        value: 'NR',
        telPrefix: '+674',
    },
    {
        text: 'Nepal',
        value: 'NP',
        telPrefix: '+977',
    },
    {
        text: 'Netherlands',
        value: 'NL',
        telPrefix: '+31',
    },
    {
        text: 'New Zealand',
        value: 'NZ',
        telPrefix: '+64',
    },
    {
        text: 'Nicaragua',
        value: 'NI',
        telPrefix: '+505',
    },
    {
        text: 'Niger',
        value: 'NE',
        telPrefix: '+227',
    },
    {
        text: 'Nigeria',
        value: 'NG',
        telPrefix: '+234',
    },
    {
        text: 'Norway',
        value: 'NO',
        telPrefix: '+47',
    },
    {
        text: 'Oman',
        value: 'OM',
        telPrefix: '+968',
    },
    {
        text: 'Pakistan',
        value: 'PK',
        telPrefix: '+92',
    },
    {
        text: 'Palau',
        value: 'PW',
        telPrefix: '+680',
    },
    {
        text: 'Panama',
        value: 'PA',
        telPrefix: '+507',
    },
    {
        text: 'Papua New Guinea',
        value: 'PG',
        telPrefix: '+675',
    },
    {
        text: 'Paraguay',
        value: 'PY',
        telPrefix: '+595',
    },
    {
        text: 'Peru',
        value: 'PE',
        telPrefix: '+51',
    },
    {
        text: 'Philippines',
        value: 'PH',
        telPrefix: '+63',
    },
    {
        text: 'Poland',
        value: 'PL',
        telPrefix: '+48',
    },
    {
        text: 'Portugal',
        value: 'PT',
        telPrefix: '+351',
    },
    {
        text: 'Qatar',
        value: 'QA',
        telPrefix: '+974',
    },
    {
        text: 'Romania',
        value: 'RO',
        telPrefix: '+40',
    },
    {
        text: 'Russia',
        value: 'RU',
        telPrefix: '+7',
    },
    {
        text: 'Rwanda',
        value: 'RW',
        telPrefix: '+250',
    },
    {
        text: 'Saint Kitts and Nevis',
        value: 'KN',
        telPrefix: '+1',
    },
    {
        text: 'Saint Lucia',
        value: 'LC',
        telPrefix: '+1',
    },
    {
        text: 'Saint Vincent and the Grenadines',
        value: 'VC',
        telPrefix: '+1',
    },
    {
        text: 'Samoa',
        value: 'WS',
        telPrefix: '+685',
    },
    {
        text: 'San Marino',
        value: 'SM',
        telPrefix: '+378',
    },
    {
        text: 'Sao Tome and Principe',
        value: 'ST',
        telPrefix: '+239',
    },
    {
        text: 'Saudi Arabia',
        value: 'SA',
        telPrefix: '+966',
    },
    {
        text: 'Senegal',
        value: 'SN',
        telPrefix: '+221',
    },
    {
        text: 'Serbia',
        value: 'RS',
        telPrefix: '+381',
    },
    {
        text: 'Seychelles',
        value: 'SC',
        telPrefix: '+248',
    },
    {
        text: 'Sierra Leone',
        value: 'SL',
        telPrefix: '+232',
    },
    {
        text: 'Singapore',
        value: 'SG',
        telPrefix: '+65',
    },
    {
        text: 'Slovakia',
        value: 'SK',
        telPrefix: '+421',
    },
    {
        text: 'Slovenia',
        value: 'SI',
        telPrefix: '+386',
    },
    {
        text: 'Solomon Islands',
        value: 'SB',
        telPrefix: '+677',
    },
    {
        text: 'Somalia',
        value: 'SO',
        telPrefix: '+252',
    },
    {
        text: 'South Africa',
        value: 'ZA',
        telPrefix: '+27',
    },
    {
        text: 'Spain',
        value: 'ES',
        telPrefix: '+34',
    },
    {
        text: 'Sri Lanka',
        value: 'LK',
        telPrefix: '+94',
    },
    {
        text: 'Sudan',
        value: 'SD',
        telPrefix: '+249',
    },
    {
        text: 'Suriname',
        value: 'SR',
        telPrefix: '+597',
    },
    {
        text: 'Swaziland',
        value: 'SZ',
        telPrefix: '+268',
    },
    {
        text: 'Sweden',
        value: 'SE',
        telPrefix: '+46',
    },
    {
        text: 'Switzerland',
        value: 'CH',
        telPrefix: '+41',
    },
    {
        text: 'Syria',
        value: 'SY',
        telPrefix: '+963',
    },
    {
        text: 'Tajikistan',
        value: 'TJ',
        telPrefix: '+992',
    },
    {
        text: 'Tanzania',
        value: 'TZ',
        telPrefix: '+255',
    },
    {
        text: 'Thailand',
        value: 'TH',
        telPrefix: '+66',
    },
    {
        text: 'Timor-Leste (East Timor)',
        value: 'TL',
        telPrefix: '+670',
    },
    {
        text: 'Togo',
        value: 'TG',
        telPrefix: '+228',
    },
    {
        text: 'Tonga',
        value: 'TO',
        telPrefix: '+676',
    },
    {
        text: 'Trinidad and Tobago',
        value: 'TT',
        telPrefix: '+1',
    },
    {
        text: 'Tunisia',
        value: 'TN',
        telPrefix: '+216',
    },
    {
        text: 'Turkey',
        value: 'TR',
        telPrefix: '+90',
    },
    {
        text: 'Turkmenistan',
        value: 'TM',
        telPrefix: '+993',
    },
    {
        text: 'Tuvalu',
        value: 'TV',
        telPrefix: '+688',
    },
    {
        text: 'Uganda',
        value: 'UG',
        telPrefix: '+256',
    },
    {
        text: 'Ukraine',
        value: 'UA',
        telPrefix: '+380',
    },
    {
        text: 'United Arab Emirates',
        value: 'AE',
        telPrefix: '+971',
    },
    {
        text: 'United Kingdom',
        value: 'GB',
        telPrefix: '+44',
    },
    {
        text: 'United States (USA)',
        value: 'US',
        telPrefix: '+1',
    },
    {
        text: 'Uruguay',
        value: 'UY',
        telPrefix: '+598',
    },
    {
        text: 'Uzbekistan',
        value: 'UZ',
        telPrefix: '+998',
    },
    {
        text: 'Vanuatu',
        value: 'VU',
        telPrefix: '+678',
    },
    {
        text: 'Vatican City',
        value: 'VA',
        telPrefix: '+379',
    },
    {
        text: 'Venezuela',
        value: 'VE',
        telPrefix: '+58',
    },
    {
        text: 'Vietnam',
        value: 'VN',
        telPrefix: '+84',
    },
    {
        text: 'Yemen',
        value: 'YE',
        telPrefix: '+967',
    },
    {
        text: 'Zambia',
        value: 'ZM',
        telPrefix: '+260',
    },
    {
        text: 'Zimbabwe',
        value: 'ZW',
        telPrefix: '+263',
    },
    {
        text: 'Taiwan',
        value: 'TW',
        telPrefix: '+886',
    },
    {
        text: 'Nagorno-Karabakh',
        value: 'AZ',
        telPrefix: '+994',
    },
    {
        text: 'Northern Cyprus',
        value: 'CY',
        telPrefix: '+357',
    },
    {
        text: 'Pridnestrovie (Transnistria)',
        value: 'MD',
        telPrefix: '+373',
    },
    {
        text: 'Christmas Island',
        value: 'CX',
        telPrefix: '+61',
    },
    {
        text: 'Cocos (Keeling) Islands',
        value: 'CC',
        telPrefix: '+61',
    },
    {
        text: 'Heard Island and McDonald Islands',
        value: 'HM',
        telPrefix: '+334',
    },
    {
        text: 'Norfolk Island',
        value: 'NF',
        telPrefix: '+672',
    },
    {
        text: 'New Caledonia',
        value: 'NC',
        telPrefix: '+687',
    },
    {
        text: 'French Polynesia',
        value: 'PF',
        telPrefix: '+689',
    },
    {
        text: 'Mayotte',
        value: 'YT',
        telPrefix: '+269',
    },
    {
        text: 'Saint Pierre and Miquelon',
        value: 'PM',
        telPrefix: '+508',
    },
    {
        text: 'Wallis and Futuna',
        value: 'WF',
        telPrefix: '+681',
    },
    {
        text: 'French Southern and Antarctic Lands',
        value: 'TF',
        telPrefix: '+262',
    },
    {
        text: 'Bouvet Island',
        value: 'BV',
        telPrefix: '+47',
    },
    {
        text: 'Cook Islands',
        value: 'CK',
        telPrefix: '+682',
    },
    {
        text: 'Niue',
        value: 'NU',
        telPrefix: '+683',
    },
    {
        text: 'Tokelau',
        value: 'TK',
        telPrefix: '+690',
    },
    {
        text: 'Guernsey',
        value: 'GG',
        telPrefix: '+44',
    },
    {
        text: 'Isle of Man',
        value: 'IM',
        telPrefix: '+44',
    },
    {
        text: 'Jersey',
        value: 'JE',
        telPrefix: '+44',
    },
    {
        text: 'Anguilla',
        value: 'AI',
        telPrefix: '+1',
    },
    {
        text: 'Bermuda',
        value: 'BM',
        telPrefix: '+1',
    },
    {
        text: 'British Indian Ocean Territory',
        value: 'IO',
        telPrefix: '+246',
    },
    {
        text: 'British Virgin Islands',
        value: 'VG',
        telPrefix: '+1',
    },
    {
        text: 'Cayman Islands',
        value: 'KY',
        telPrefix: '+1',
    },
    {
        text: 'Falkland Islands (Islas Malvinas)',
        value: 'FK',
        telPrefix: '+500',
    },
    {
        text: 'Gibraltar',
        value: 'GI',
        telPrefix: '+350',
    },
    {
        text: 'Montserrat',
        value: 'MS',
        telPrefix: '+1',
    },
    {
        text: 'Pitcairn Islands',
        value: 'PN',
        telPrefix: '+64',
    },
    {
        text: 'Saint Helena',
        value: 'SH',
        telPrefix: '+290',
    },
    {
        text: 'South Georgia & South Sandwich Islands',
        value: 'GS',
        telPrefix: '+500',
    },
    {
        text: 'Turks and Caicos Islands',
        value: 'TC',
        telPrefix: '+1',
    },
    {
        text: 'Northern Mariana Islands',
        value: 'MP',
        telPrefix: '+1',
    },
    {
        text: 'Puerto Rico',
        value: 'PR',
        telPrefix: '+1',
    },
    {
        text: 'American Samoa',
        value: 'AS',
        telPrefix: '+1',
    },
    {
        text: 'Guam',
        value: 'GU',
        telPrefix: '+1',
    },
    {
        text: 'Midway Islands, Wake Island',
        value: 'UM',
        telPrefix: '+1',
    },
    {
        text: 'U.S. Virgin Islands',
        value: 'VI',
        telPrefix: '+1',
    },
    {
        text: 'Hong Kong',
        value: 'HK',
        telPrefix: '+852',
    },
    {
        text: 'Macau',
        value: 'MO',
        telPrefix: '+853',
    },
    {
        text: 'Faroe Islands',
        value: 'FO',
        telPrefix: '+298',
    },
    {
        text: 'Greenland',
        value: 'GL',
        telPrefix: '+299',
    },
    {
        text: 'French Guiana',
        value: 'GF',
        telPrefix: '+594',
    },
    {
        text: 'Guadeloupe, Saint Barthelemy, Saint Martin',
        value: 'GP',
        telPrefix: '+590',
    },
    {
        text: 'Martinique',
        value: 'MQ',
        telPrefix: '+596',
    },
    {
        text: 'Reunion',
        value: 'RE',
        telPrefix: '+262',
    },
    {
        text: 'Aland',
        value: 'AX',
        telPrefix: '+358',
    },
    {
        text: 'Aruba',
        value: 'AW',
        telPrefix: '+297',
    },
    {
        text: 'Netherlands Antilles',
        value: 'AN',
        telPrefix: '+599',
    },
    {
        text: 'Svalbard',
        value: 'SJ',
        telPrefix: '+47',
    },
    {
        text: 'Ascension',
        value: 'AC',
        telPrefix: '+247',
    },
    {
        text: 'Tristan da Cunha',
        value: 'TA',
        telPrefix: '+290',
    },
    {
        text: 'Antarctic Territory',
        value: 'AQ',
        telPrefix: '+672',
    },
    {
        text: 'Palestine',
        value: 'PS',
        telPrefix: '+970',
    },
]);

export default countries;
