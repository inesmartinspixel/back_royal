import 'AngularSpecHelper';
import 'LearnerContentCache/angularModule';

describe('LearnerContentCacheInterceptor', () => {
    let SpecHelper;
    let LearnerContentCacheInterceptor;
    let LearnerContentCache;

    beforeEach(() => {
        angular.mock.module('SpecHelper', 'FrontRoyal.Users');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                LearnerContentCacheInterceptor = $injector.get('LearnerContentCacheInterceptor');
                LearnerContentCache = $injector.get('LearnerContentCache');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('response', () => {
        it('should update LearnerContentCache updated_at', () => {
            LearnerContentCache.contentViewsRefreshUpdatedAt = 0;

            LearnerContentCacheInterceptor.response({
                data: {
                    meta: {
                        push_messages: {
                            content_views_refresh_updated_at: 12345,
                        },
                    },
                },
                config: {
                    method: 'PUT',
                    url: '/not_a_progress_call.json',
                },
            });

            expect(LearnerContentCache.contentViewsRefreshUpdatedAt).toBe(12345);
        });
    });
});
