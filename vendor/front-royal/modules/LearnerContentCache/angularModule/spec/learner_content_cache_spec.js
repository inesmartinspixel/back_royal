import 'AngularSpecHelper';
import 'LearnerContentCache/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import { DisconnectedError } from 'FrontRoyalStore';

describe('LearnerContentCache', () => {
    let SpecHelper;
    let $rootScope;
    let LearnerContentCache;
    let StudentDashboard;
    let user;
    let streams;
    let Stream;
    let Lesson;
    let $q;
    let StreamProgress;
    let LessonProgress;
    let $timeout;
    let offlineModeManager;

    beforeEach(() => {
        angular.mock.module('SpecHelper', 'FrontRoyal.Users');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                $rootScope = $injector.get('$rootScope');
                LearnerContentCache = $injector.get('LearnerContentCache');
                StudentDashboard = $injector.get('StudentDashboard');
                Stream = $injector.get('Lesson.Stream');
                Lesson = $injector.get('Lesson');
                $injector.get('StreamFixtures');
                $injector.get('LessonFixtures');
                $q = $injector.get('$q');
                StreamProgress = $injector.get('Lesson.StreamProgress');
                $injector.get('StreamProgressFixtures');
                LessonProgress = $injector.get('LessonProgress');
                $injector.get('LessonProgressFixtures');
                $timeout = $injector.get('$timeout');
                offlineModeManager = $injector.get('offlineModeManager');
            },
        ]);

        user = SpecHelper.stubCurrentUser('learner');

        streams = [
            Stream.fixtures.getInstance({
                updated_at: 0,
                lesson_streams_progress: null,
                favorite: null,
                lessons: [
                    Lesson.fixtures.getInstance({
                        updated_at: 1,
                        lesson_progress: null,
                    }),
                    Lesson.fixtures.getInstance({
                        updated_at: 1,
                        lesson_progress: null,
                    }),
                ],
            }),
            Stream.fixtures.getInstance({
                updated_at: 3,
                lesson_streams_progress: null,
            }),
        ];
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('ensureStudentDashboard', () => {
        beforeEach(mockRejectInOfflineMode);

        it('should call into StudentDashboard.index once', () => {
            mockShoveProgress();
            jest.spyOn(StudentDashboard, 'index').mockReturnValue($q.when());
            let response1;
            let response2;
            LearnerContentCache.ensureStudentDashboard().then(r => {
                response1 = r;
            });
            LearnerContentCache.ensureStudentDashboard().then(r => {
                response2 = r;
            });
            expect(response2).toEqual(response1);
            expect(StudentDashboard.index).toHaveBeenCalled();
            expect(StudentDashboard.index.mock.calls.length).toBe(1);
        });

        it('should call into StudentDashboard.index each time if viewAs is supplied', () => {
            jest.spyOn(StudentDashboard, 'index').mockReturnValue($q.when());
            $rootScope.viewAs = 'SOMETHING';
            LearnerContentCache.ensureStudentDashboard();
            LearnerContentCache.ensureStudentDashboard();
            expect(StudentDashboard.index).toHaveBeenCalled();
            expect(StudentDashboard.index.mock.calls.length).toBe(2);
        });

        it('should shove progress data on', () => {
            jest.spyOn($rootScope.currentUser, 'hasPlaylists', 'get').mockReturnValue(true);
            StudentDashboard.expect('index')
                .toBeCalledWith({
                    user_id: $rootScope.currentUser.id,
                    do_not_include_progress: true,
                    filters: {},
                })
                .returns({
                    lesson_streams: streams,
                    available_playlists: [],
                });

            const progressResult = {
                favoriteStreamsSet: {},
            };
            const streamProgress = StreamProgress.fixtures.getInstance({
                locale_pack_id: streams[0].localePackId,
            });
            progressResult.streamProgress = [streamProgress];

            const lessonWithProgress = streams[0].lessons[0];
            const lessonProgress = LessonProgress.fixtures.getInstance({
                locale_pack_id: lessonWithProgress.localePackId,
            });
            progressResult.lessonProgress = [lessonProgress];

            progressResult.favoriteStreamsSet[streams[1].localePackId] = true;

            jest.spyOn(user.progress, 'getAllProgress').mockReturnValue($q.when(progressResult));

            let response;
            LearnerContentCache.ensureStudentDashboard().then(_response => {
                response = _response;
            });

            StudentDashboard.flush('index');
            const streamsResult = response.result[0].lesson_streams;
            // first stream has progress, second does not
            expect(streamsResult[0].lesson_streams_progress).toEqual(streamProgress);
            expect(streamsResult[1].lesson_streams_progress).toBeUndefined();

            // second stream is favorited. first is not
            expect(streamsResult[0].favorite).toBe(false);
            expect(streamsResult[1].favorite).toBe(true);

            // one lesson has some progress
            expect(streamsResult[0].lessons[0].lesson_progress).toEqual(lessonProgress);
            expect(streamsResult[0].lessons[1].lesson_progress).toEqual(lessonProgress);
            expect(streamsResult[1].lessons[0].lesson_progress).toEqual(lessonProgress);
        });

        it('should not blow up if current user is unset while request is in flight', () => {
            const expectedResponse = {};
            jest.spyOn(StudentDashboard, 'index').mockImplementation(() => {
                $rootScope.currentUser = null;
                return $q.resolve(expectedResponse);
            });
            let receivedResponse;
            LearnerContentCache.ensureStudentDashboard().then(_response => {
                receivedResponse = _response;
            });
            $timeout.flush();
            expect(receivedResponse).toEqual(expectedResponse);
        });

        it('should get a change in the progress even if content is still cached', () => {
            jest.spyOn($rootScope.currentUser, 'hasPlaylists', 'get').mockReturnValue(true);
            StudentDashboard.expect('index')
                .toBeCalledWith({
                    user_id: $rootScope.currentUser.id,
                    do_not_include_progress: true,
                    filters: {},
                })
                .returns({
                    lesson_streams: streams,
                    available_playlists: [],
                });

            const progressResult = {
                favoriteStreamsSet: {},
            };

            jest.spyOn(user.progress, 'getAllProgress').mockImplementation(() => $q.when(progressResult));

            let response;
            LearnerContentCache.ensureStudentDashboard().then(_response => {
                response = _response;
            });

            StudentDashboard.flush('index');
            const streamsResult = response.result[0].lesson_streams;
            // initially there is no progress
            expect(streamsResult[0].lesson_streams_progress).toBeUndefined();

            // but later there is
            const streamProgress = StreamProgress.fixtures.getInstance({
                locale_pack_id: streams[0].localePackId,
            });
            progressResult.streamProgress = [streamProgress];
            LearnerContentCache.ensureStudentDashboard().then(_response => {
                response = _response;
            });
            $timeout.flush();
            expect(streamsResult[0].lesson_streams_progress).toEqual(streamProgress);
        });

        it('should request get_has_available_incomplete_streams if no playlists', () => {
            jest.spyOn($rootScope.currentUser, 'hasPlaylists', 'get').mockReturnValue(false);
            StudentDashboard.expect('index').toBeCalledWith({
                get_has_available_incomplete_streams: true,
                user_id: $rootScope.currentUser.id,
                do_not_include_progress: true,
                filters: {},
            });

            const progressResult = {
                streamProgress: [],
                favoriteStreamsSet: {},
                lessonProgress: [],
            };

            jest.spyOn(user.progress, 'getAllProgress').mockImplementation(() => $q.when(progressResult));

            LearnerContentCache.ensureStudentDashboard();

            StudentDashboard.flush('index');
        });

        it('should try again on a second request if the first fails because the user entered offline mode', () => {
            mockShoveProgress();
            const disconnectedError = new DisconnectedError();
            const result = 'result';
            offlineModeManager.rejectInOfflineMode.mockReturnValue($q.reject(disconnectedError));
            let response1;
            let response2;
            LearnerContentCache.ensureStudentDashboard().catch(r => {
                response1 = r;
            });
            $timeout.flush();
            jest.spyOn(StudentDashboard, 'index').mockReturnValue($q.resolve(result));
            mockRejectInOfflineMode();
            LearnerContentCache.ensureStudentDashboard().then(r => {
                response2 = r;
            });
            $timeout.flush();
            expect(response1).toBe(disconnectedError);
            expect(response2).toBe(result);
        });
    });

    describe('clear', () => {
        it('should reset the underlying promise', () => {
            jest.spyOn(StudentDashboard, 'index').mockReturnValue($q.when());
            LearnerContentCache.ensureStudentDashboard();
            LearnerContentCache.ensureStudentDashboard();
            expect(StudentDashboard.index.mock.calls.length).toBe(1);
            LearnerContentCache.clear();
            LearnerContentCache.ensureStudentDashboard();
            expect(StudentDashboard.index.mock.calls.length).toBe(2);
        });

        it('should clear on currentUser watches', () => {
            mockShoveProgress();
            $rootScope.currentUser.relevant_cohort = {
                id: '123',
            };
            $rootScope.$digest();
            jest.spyOn(StudentDashboard, 'index').mockReturnValue($q.when());

            LearnerContentCache.ensureStudentDashboard();
            expect(StudentDashboard.index.mock.calls.length).toBe(1);
            $rootScope.currentUser.relevant_cohort = {
                id: 'something-different',
            };
            $rootScope.$digest();
            LearnerContentCache.ensureStudentDashboard();
            expect(StudentDashboard.index.mock.calls.length).toBe(2);

            $rootScope.currentUser.pref_locale = 'something-different';
            $rootScope.$digest();
            LearnerContentCache.ensureStudentDashboard();
            expect(StudentDashboard.index.mock.calls.length).toBe(3);

            $rootScope.currentUser.favorite_lesson_stream_locale_packs = [
                {
                    id: 'something-different',
                },
            ];
            $rootScope.$digest();
            LearnerContentCache.ensureStudentDashboard();
            expect(StudentDashboard.index.mock.calls.length).toBe(4);
        });
    });

    function mockShoveProgress() {
        jest.spyOn(LearnerContentCache, '_shoveProgressIntoContent').mockImplementation(response => response);
    }

    function mockRejectInOfflineMode() {
        jest.spyOn(offlineModeManager, 'rejectInOfflineMode').mockImplementation(fn => {
            return fn();
        });
    }
});
