import angularModule from 'LearnerContentCache/angularModule/scripts/learner_content_cache_module';

angularModule.factory('LearnerContentCacheInterceptor', [
    '$injector',

    $injector => {
        function setContentViewsRefreshedAt(response) {
            const LearnerContentCache = $injector.get('LearnerContentCache');
            let contentViewsRefreshUpdatedAt;
            try {
                contentViewsRefreshUpdatedAt = response.data.meta.push_messages.content_views_refresh_updated_at;
            } catch (e) {
                return;
            }

            if (
                contentViewsRefreshUpdatedAt &&
                LearnerContentCache.contentViewsRefreshUpdatedAt &&
                contentViewsRefreshUpdatedAt > LearnerContentCache.contentViewsRefreshUpdatedAt
            ) {
                LearnerContentCache.clear();
            }

            LearnerContentCache.contentViewsRefreshUpdatedAt = contentViewsRefreshUpdatedAt;
        }

        return {
            response(response) {
                setContentViewsRefreshedAt(response);

                return response;
            },
        };
    },
]);

angularModule.config([
    '$httpProvider',
    $httpProvider => {
        $httpProvider.interceptors.push('LearnerContentCacheInterceptor');
    },
]);
