import angularModule from 'LearnerContentCache/angularModule/scripts/learner_content_cache_module';
import DisconnectedError from 'FrontRoyalStore/DisconnectedError';

angularModule.factory('LearnerContentCache', [
    '$injector',
    $injector => {
        const $rootScope = $injector.get('$rootScope');
        const StudentDashboard = $injector.get('StudentDashboard');
        const Locale = $injector.get('Locale');
        const offlineModeManager = $injector.get('offlineModeManager');

        // LearnerContentCache will also clear with `currentUser.programType` changes via `CurrentUserInterceptor`
        // this is probably a bit more efficient than a $rootScope.$watch on a derived property value
        $rootScope.$watchGroup(['currentUser', 'currentUser.pref_locale', 'currentUser.relevant_cohort.id'], () => {
            // eslint-disable-next-line no-use-before-define
            LearnerContentCache.clear();
        });

        // since favoriting a stream can change the list of streams that comes back from the
        // student dashboard call, we need to clear when it changes (we also clear out the
        // progress cache in that case.  See UserProgressLoader)
        $rootScope.$watch(
            'currentUser.favorite_lesson_stream_locale_packs',
            () => {
                // eslint-disable-next-line no-use-before-define
                LearnerContentCache.clear();
            },
            true,
        ); // deep watch since the whole collection can be replaced with a clone

        const LearnerContentCache = {
            clear() {
                this._studentDashboardPromise = null;
            },

            ensureStudentDashboard() {
                const viewAs = $rootScope.viewAs;
                const self = this;
                const user = $rootScope.currentUser;

                // First, we ensure that we have a fresh version of all of the content.  (See
                // LearnerContentCacheInterceptor to see how we bust this cache.). Then, we
                // grab a fresh version of the progress from the currentUser.progress.getAllProgress and
                // we shove it into the content.  We do this over again even if the content
                // is already cached, since the progress can go out of date separately from the content.

                // If we were starting from scratch,
                // we would probably not shove progress into content this way.
                // Instead we would keep the content and
                // the progress separate on the client.  But we have a lot of code that relies
                // on the progress being embedded in the content, and it is easy to deal with that way.

                let promise = self._studentDashboardPromise;
                if (!promise || viewAs) {
                    const params = {
                        user_id: user.id,
                        do_not_include_progress: true,
                        filters: {},
                    };

                    if (!user.hasPlaylists) {
                        params.get_has_available_incomplete_streams = true;
                    }

                    // load content
                    if (viewAs) {
                        params.filters.view_as = viewAs;
                        params.filters.in_locale_or_en = Locale.activeCode;
                        params.filters.user_can_see = null;
                        params.filters.in_users_locale_or_en = null;
                        promise = StudentDashboard.index(params); // Do not store this promise.  We don't expect to re-use it
                    } else {
                        // If we enter offline mode and that causes this request to fail, then
                        // we can just leave the LearnerContentCache empty.  We don't need it to
                        // be populated as long as we're in offline mode, and it will be repopulated
                        // the next time ensureStudentDashboard is called after we've come out
                        // of offline mode.  So, we unset the promise after a failure.
                        self._studentDashboardPromise = offlineModeManager
                            .rejectInOfflineMode(() => StudentDashboard.index(params))
                            .catch(err => {
                                self._studentDashboardPromise = null;
                                throw err;
                            });
                        promise = self._studentDashboardPromise;
                    }
                }

                return (
                    promise
                        // add the progress into the content
                        .then(studentDashboardResponse => {
                            // if the current user changed while content was loading, do
                            // not attempt to shove progress on/
                            // See https://trello.com/c/hFv8MgQx, issue with `t.currentUser.progress`
                            if (!user || user !== $rootScope.currentUser) {
                                return studentDashboardResponse;
                            }
                            return self._shoveProgressIntoContent(studentDashboardResponse, user);
                        })
                );
            },

            preloadStudentDashboard() {
                this.ensureStudentDashboard().catch(err => {
                    // ensureStudentDashboard can throw a DisconnectedError if
                    // we enter offline mode while it is in flight.  Since preloadStudentDashboard
                    // is just a fire-and-forget call, we can ignore that error.
                    if (err.constructor !== DisconnectedError) {
                        throw err;
                    }
                });
            },

            _shoveProgressIntoContent(studentDashboardResponse, user) {
                return user.progress.getAllProgress().then(response => {
                    const streamProgressByLocalePackId = _.indexBy(response.streamProgress, 'locale_pack_id');
                    const lessonProgressByLocalePackId = _.indexBy(response.lessonProgress, 'locale_pack_id');

                    _.each(studentDashboardResponse.result[0].lesson_streams, stream => {
                        stream.lesson_streams_progress = streamProgressByLocalePackId[stream.localePackId];
                        stream.favorite = !!response.favoriteStreamsSet[stream.localePackId];

                        // FIXME: this is a bit scary, at least until we've unified all content into a single
                        // cache.  Maybe we could get away with stream progress never referencing stream() at all
                        // and not rely on the embedded relationship?  Right now, I don't think it creates any practical
                        // problems in the wild to remove this, but that's just an accident.  It is because we only ever
                        // complete a stream after the player has been launched, and in that case stream_progress.stream()
                        // will be set when the full content is loaded.  So, removing this just breaks a casper spec right
                        // now where we're manually completing a stream on the stream dashboard.
                        if (stream.lesson_streams_progress) {
                            stream.lesson_streams_progress.$$embeddedIn = stream;
                        }

                        _.each(stream.lessons, lesson => {
                            lesson.lesson_progress = lessonProgressByLocalePackId[lesson.localePackId];
                        });
                    });
                    return studentDashboardResponse;
                });
            },
        };

        return LearnerContentCache;
    },
]);
