import 'AngularSpecHelper';
import 'TimeoutHelpers/angularModule';

describe('resolvableTimeout', () => {
    let resolvableTimeout;

    beforeEach(() => {
        angular.mock.module('timeoutHelpers');

        angular.mock.inject([
            '$injector',
            $injector => {
                resolvableTimeout = $injector.get('resolvableTimeout');
            },
        ]);
    });

    it('should resolve a timeout that has not yet been triggered', () => {
        const callback = jest.fn();
        const promise = resolvableTimeout(callback);
        expect(callback).not.toHaveBeenCalled();
        resolvableTimeout.resolve(promise);
        expect(callback.mock.calls.length).toBe(1);
        expect(resolvableTimeout.flush).toThrow(new Error('No deferred tasks to be flushed'));
        expect(callback.mock.calls.length).toBe(1);
    });

    it('should do nothing when resolving a timeout that has already been triggered', () => {
        const callback = jest.fn();
        const promise = resolvableTimeout(callback);
        expect(callback).not.toHaveBeenCalled();
        resolvableTimeout.flush();
        expect(callback.mock.calls.length).toBe(1);
        resolvableTimeout.resolve(promise);
        expect(callback.mock.calls.length).toBe(1);
    });
});
