import { keyBy } from 'lodash/fp';

import meetupMarker from 'images/student_network_events_meetup_marker.png';
import conferenceMarker from 'images/student_network_events_conference_marker.png';
import companyVisitMarker from 'images/student_network_events_company_visit_marker.png';
import specialEventMarker from 'images/student_network_events_special_event_marker.png';
import admissionsEventMarker from 'images/student_network_events_admissions_event_marker.png';
import careerFairMarker from 'images/student_network_events_career_fair_marker.png';
import bookClubMarker from 'images/student_network_events_book_club_icon.png';
import onlineEventMarker from 'images/student_network_events_online_event_icon.png';

// During the build process, if Webpack comes across a live `require` statement, but the passed in value
// is an expression rather than a hardcoded string, Webpack will construct a regex based on the expression
// and require all assets matching that regex. This could potentially lead to the loading of unnecessary
// assets because we would relying on a regex. To avoid this, we've placed the `require` statements here
// in this service with hardcoded string values. See https://webpack.js.org/guides/dependency-management/
const EVENT_TYPE_CONFIGS = [
    {
        key: 'meetup',
        mappable: true,
        eventTypeFiltersOrder: 1,
        icon: {
            src: meetupMarker,
            width: 23,
            height: 23,
        },
    },
    {
        key: 'conference',
        mappable: true,
        eventTypeFiltersOrder: 2,
        icon: {
            src: conferenceMarker,
            width: 23,
            height: 23,
        },
    },
    {
        key: 'company_visit',
        mappable: true,
        eventTypeFiltersOrder: 3,
        icon: {
            src: companyVisitMarker,
            width: 23,
            height: 23,
        },
    },
    {
        key: 'special_event',
        mappable: true,
        eventTypeFiltersOrder: 4,
        icon: {
            src: specialEventMarker,
            width: 23,
            height: 23,
        },
    },
    {
        key: 'admissions_event',
        mappable: true,
        eventTypeFiltersOrder: 5,
        visibleInEventTypeFiltersOnlyWhenAny: true,
        icon: {
            src: admissionsEventMarker,
            width: 23,
            height: 23,
        },
    },
    {
        key: 'career_fair',
        mappable: true,
        eventTypeFiltersOrder: 6,
        visibleInEventTypeFiltersOnlyWhenAny: true,
        icon: {
            src: careerFairMarker,
            width: 23,
            height: 23,
        },
    },
    {
        key: 'book_club',
        mappable: false,
        icon: {
            src: bookClubMarker,
            width: 18,
            height: 20,
        },
    },
    {
        key: 'online_event',
        mappable: false,
        icon: {
            src: onlineEventMarker,
            width: 25,
            height: 24,
        },
    },
];

const EVENT_TYPE_CONFIGS_MAP = keyBy('key')(EVENT_TYPE_CONFIGS);

export { EVENT_TYPE_CONFIGS, EVENT_TYPE_CONFIGS_MAP };
