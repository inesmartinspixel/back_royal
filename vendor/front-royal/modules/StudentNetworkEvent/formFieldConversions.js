import { createFormFieldConversions } from 'FrontRoyalForm';
import { without, some } from 'lodash/fp';
import moment from 'moment-timezone';
import { mappable } from './helpers';

function convertTimestampWithZone(name) {
    const entry = {};
    entry[name] = {
        fromForm: values => (values.date_tbd ? null : values[name] && values[name] / 1000),
        toForm: values => {
            const momentous = moment(values[name] ? values[name] * 1000 : undefined);
            return values.timezone ? momentous.tz(values.timezone) : momentous;
        },
    };
    return entry;
}

const [toForm, fromForm] = createFormFieldConversions({
    // Date related fields
    ...convertTimestampWithZone('start_time'),
    ...convertTimestampWithZone('end_time'),
    date_tbd_description: {
        fromForm: values => (values.date_tbd && values.date_tbd_description) || null,
    },

    // The record has place_id and place_details, but we need to be able to
    // bind a single value to the SingleLocationInput, so we combine those
    // into `place`.
    //
    // If the current event type is not mappable, then we unset the location before
    // saving.  This is better than unsetting it right when someone changes the event
    // type, because we don't clear the location if someone accidentally picked an
    // unmappable type.
    place_id: {
        fromForm: values => (mappable(values) && values.place && values.place.id) || null,
    },
    place_details: {
        fromForm: values => (mappable(values) && values.place && values.place.details) || {},
    },
    location_name: {
        fromForm: values => (mappable(values) && values.location_name) || null,
    },
    timezone: {
        fromForm: values => (mappable(values) && values.timezone) || null,
    },
    place: {
        toForm: values =>
            values.place_id && {
                id: values.place_id,
                details: values.place_details,
            },
    },

    // Before using this pattern in another situation, see the
    // comment in StudentNetworkEventPreview.jsx, where we have to
    // convert this in a different way for use in the preview
    image: {
        fromForm: values => {
            if (values.image) {
                return values.image.file || values.image.s3_asset;
            }
            return null;
        },
        toForm: values => {
            const image = values.image;
            return {
                s3_asset: image,

                // see FileInput for why we return a function here
                getUrl: () => image && image.formats.original.url,
            };
        },
    },

    /*
        When converting from a record to an object for use in a form, we
        combine all of the visible_to columns into a single array that we
        can bind to a single select input.

        Then, when converting back to an object to send over the api,
        we split it back out into mutliple columns.
    */
    visible_to: {
        toForm: values => {
            let list = [];

            [
                'visible_to_current_degree_students',
                'visible_to_graduated_degree_students',
                'visible_to_non_degree_users',
            ].forEach(col => {
                if (values[col]) {
                    list.push(col);
                }
            });

            if (some(values.visible_to_accepted_degree_students_in_cohorts)) {
                list = list.concat(values.visible_to_accepted_degree_students_in_cohorts);
            }
            return list;
        },
    },
    visible_to_current_degree_students: {
        fromForm: values => values.visible_to.includes('visible_to_current_degree_students'),
    },
    visible_to_graduated_degree_students: {
        fromForm: values => values.visible_to.includes('visible_to_graduated_degree_students'),
    },
    visible_to_non_degree_users: {
        fromForm: values => values.visible_to.includes('visible_to_non_degree_users'),
    },
    visible_to_accepted_degree_students_in_cohorts: {
        fromForm: values =>
            without([
                'visible_to_current_degree_students',
                'visible_to_graduated_degree_students',
                'visible_to_non_degree_users',
            ])(values.visible_to),
    },
});

export { toForm, fromForm };
