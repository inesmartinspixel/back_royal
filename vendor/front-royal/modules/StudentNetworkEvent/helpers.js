import { EVENT_TYPE_CONFIGS_MAP } from './constants';

export function icon(studentNetworkEvent) {
    return studentNetworkEvent.event_type && eventTypeIcon(studentNetworkEvent.event_type);
}

export function eventTypeIcon(eventType) {
    return eventType && EVENT_TYPE_CONFIGS_MAP[eventType].icon;
}

export function eventTypeIsMappable(eventType) {
    return eventType ? EVENT_TYPE_CONFIGS_MAP[eventType].mappable : false;
}

export function mappable(studentNetworkEvent) {
    return studentNetworkEvent.event_type && eventTypeIsMappable(studentNetworkEvent.event_type);
}
