import * as Yup from 'yup';
import { eventTypeIsMappable } from './helpers';

// If the event type is unmappable, we don't care what the
// place is set to.  It will be unset when the form is saved
const requiredIfMappable = Yup.string().when('event_type', {
    is: eventType => eventTypeIsMappable(eventType),
    then: Yup.string().required(),
    otherwise: Yup.string().nullable(),
});

const validationSchema = Yup.object().shape({
    title: Yup.string().required(),
    description: Yup.string().required(),
    event_type: Yup.string().required(),
    start_time: Yup.date().when('date_tbd', {
        is: false,
        then: Yup.date().required(),
        otherwise: Yup.date().nullable(),
    }),
    end_time: Yup.date().when('date_tbd', {
        is: false,
        then: Yup.date()
            .required()
            .test('end after start', 'End time must be after start time', function (endTime) {
                return endTime > this.parent.start_time;
            }),
        otherwise: Yup.date().nullable(),
    }),
    date_tbd_description: Yup.string().when('date_tbd', {
        is: true,
        then: Yup.string().required(),
        otherwise: Yup.string().nullable(),
    }),
    external_rsvp_url: Yup.string()
        .nullable()
        .test('external_rsvp_url_undefined', 'External RSVP URL must be blank if RSVP Not Required', function (
            externalRsvpUrl,
        ) {
            return this.parent.rsvp_status === 'not_required'
                ? externalRsvpUrl === undefined || externalRsvpUrl === null
                : true;
        })
        .test('external_rsvp_url_defined', 'External RSVP URL must not be blank if RSVP Requied', function (
            externalRsvpUrl,
        ) {
            return this.parent.rsvp_status === 'required'
                ? !!externalRsvpUrl && typeof externalRsvpUrl === 'string'
                : true;
        }),
    place: requiredIfMappable,
    location_name: requiredIfMappable,
    timezone: requiredIfMappable,
    visible_to: Yup.array().when('published', {
        is: true,
        then: Yup.array().required(),
        otherwise: Yup.array().notRequired(),
    }),
});

export default validationSchema;
