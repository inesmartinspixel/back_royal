import validationSchema from './validationSchema';
import { TRANSACTION_TYPE_PAYMENT } from './constants';

describe('requiredIfPayment', () => {
    it('should be required if payment type', () => {
        expect(() => {
            validationSchema.validateSyncAt('provider', {
                provider: null,
                transaction_type: TRANSACTION_TYPE_PAYMENT,
            });
        }).toThrow();
    });

    it('should allow null if not payment type', () => {
        expect(() => {
            validationSchema.validateSyncAt('provider', {
                provider: null,
                transaction_type: 'notPayment',
            });
        }).not.toThrow();
    });

    it('should allow undefined if not payment type', () => {
        expect(() => {
            validationSchema.validateSyncAt('provider', {
                provider: undefined,
                transaction_type: 'notPayment',
            });
        }).not.toThrow();
    });

    it('should disallow a value if not payment type', () => {
        expect(() => {
            validationSchema.validateSyncAt('provider', {
                provider: '',
                transaction_type: 'notPayment',
            });
        }).toThrow();
    });
});

describe('amount_refunded', () => {
    it('should be valid if less than or equal to amount', () => {
        expect(() => {
            validationSchema.validateSyncAt('amount_refunded', {
                amount: 42,
                refunds: [
                    {
                        amount: 41,
                    },
                    {
                        amount: 1,
                    },
                ],
            });
        }).not.toThrow();
    });
    it('should be invalid if greater than amount', () => {
        expect(() => {
            validationSchema.validateSyncAt('amount_refunded', {
                amount: 42,
                refunds: [
                    {
                        amount: 42,
                    },
                    {
                        amount: 1,
                    },
                ],
            });
        }).toThrow();
    });
});
