import { toForm, fromForm } from './formFieldConversions';

import { TYPES_WITH_POSITIVE_AMOUNTS } from './constants';

describe('amount', () => {
    describe('fromForm', () => {
        it('should convert to negative for expected types', () => {
            const result = fromForm({
                amount: 42,
                transaction_type: 'nonPositive',
            }).amount;
            expect(result).toBe(-42);
        });
        it('should preserve positive for expected types', () => {
            const result = fromForm({
                amount: 42,
                transaction_type: TYPES_WITH_POSITIVE_AMOUNTS[0],
            }).amount;
            expect(result).toBe(42);
        });
    });
    describe('toForm', () => {
        it('should convert negative to positive', () => {
            const result = toForm({
                amount: -42,
            }).amount;
            expect(result).toBe(42);
        });
        it('should leave positive as-is', () => {
            const result = toForm({
                amount: 42,
            }).amount;
            expect(result).toBe(42);
        });
    });
});

describe('transaction_time', () => {
    describe('toForm', () => {
        it('should convert to date', () => {
            const date = new Date();
            const result = toForm({
                transaction_time: date.getTime() / 1000,
            }).transaction_time;
            expect(result).toEqual(date);
        });
    });
    describe('fromForm', () => {
        it('should convert to timestamp', () => {
            const date = new Date();
            const result = fromForm({
                transaction_time: date,
            }).transaction_time;
            expect(result).toEqual(date.getTime() / 1000);
        });
    });
});
