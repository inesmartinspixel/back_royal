import selectedProviderOption from './selectedProviderOption';

function hasUnselectableProvider(billingTransaction) {
    const selectedOption = selectedProviderOption(billingTransaction);
    return selectedOption ? !selectedOption.selectableByAdmins : false;
}

export default hasUnselectableProvider;
