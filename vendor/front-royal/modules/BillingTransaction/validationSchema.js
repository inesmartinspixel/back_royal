import * as Yup from 'yup';
import 'YupExtensions/nonNegative';
import { validationSchema as refundValidationSchema } from 'Refund';
import { reduce } from 'lodash/fp';
import { TRANSACTION_TYPE_PAYMENT } from './constants';

const requiredIfPayment = Yup.string().when('transaction_type', {
    is: TRANSACTION_TYPE_PAYMENT,
    then: Yup.string().required(),
    otherwise: Yup.string().nullable().oneOf([null, undefined], 'Null'),
});

const validationSchema = Yup.object().shape({
    transaction_type: Yup.string().required(),
    provider: requiredIfPayment,
    provider_transaction_id: requiredIfPayment,
    transaction_time: Yup.date().required(),
    amount: Yup.number().positive().required(),
    amount_refunded: Yup.number()
        .nonNegative()
        .nullable()
        .test(
            'lessThanAmount',
            'Total amount refunded must be less than or equal to amount',
            function lessThanAmountTest() {
                const amountRefunded = reduce((sum, refund) => sum + refund.amount, 0)(this.parent.refunds);
                if (!amountRefunded) {
                    return true;
                }
                return this.parent.amount && this.parent.amount >= amountRefunded;
            },
        ),
    refunds: Yup.array().of(refundValidationSchema),
});

export default validationSchema;
