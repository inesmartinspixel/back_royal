import { map } from 'lodash/fp';
import { createFormFieldConversions, convertTimestamp } from 'FrontRoyalForm';
import { toForm as refundToForm, fromForm as refundFromForm } from 'Refund';
import { TYPES_WITH_POSITIVE_AMOUNTS } from './constants';

const [toForm, fromForm] = createFormFieldConversions({
    ...convertTimestamp('transaction_time'),
    amount: {
        fromForm: values => {
            if (values.amount && values.amount < 0) {
                throw new Error('Expecting positive input');
            }

            if (TYPES_WITH_POSITIVE_AMOUNTS.includes(values.transaction_type)) {
                return values.amount;
            }
            return -values.amount;
        },
        toForm: values => (values.amount ? Math.abs(values.amount) : null),
    },
    refunds: {
        fromForm(values) {
            return map(refund => refundFromForm(refund))(values.refunds);
        },
        toForm(values) {
            return map(refund => refundToForm(refund))(values.refunds);
        },
    },
});

export { toForm, fromForm };
