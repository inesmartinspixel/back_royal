import paginationControlsTemplate from 'Pagination/angularModule/views/pagination_controls.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const paginationModule = angular.module('Pagination', []);

cacheAngularTemplate(paginationModule, 'Pagination/pagination_controls.html', paginationControlsTemplate);

export default paginationModule;
