import 'ShimLibrariesForSpecs';

import 'IguanaSuperModelAndAClassAbove';
import 'iguana/dist/iguana-mock';
import 'EventLogger/angularModule';
import 'Translation/angularModule';
import 'angular-mocks';
// import 'FrontRoyalApiErrorHandler/angularModule';
import 'angular-translate';
import 'Injector/angularModule';
import 'AngularHttpQueueAndRetry/angularModule';
import 'Users/angularModule/spec/_mock/fixtures/users';

import moment from 'moment-timezone';

const SpecHelperModule = angular.module('SpecHelper', [
    'Iguana',
    'Injector',
    'pascalprecht.translate',
    'EventLogger',
    'HttpQueueAndRetry',
    'FrontRoyal.Users.Fixtures',
    // 'FrontRoyal.ApiErrorHandler'
]);

SpecHelperModule.config(() => {
    // call this in config so that it happens before
    // any other providers are initialized
    window.localStorage.clear();
    window.loadTranslations();
});

const stubModule = angular.module('NoUnhandledRejectionExceptions', []);
stubModule.config($qProvider => {
    $qProvider.errorOnUnhandledRejections(false);
});

window.loadTranslations = () => {
    angular.mock.module(($translateProvider, $provide) => {
        $provide.value('missingTranslationHandler', (translationId, $uses) => {
            if (window.IGNORE_TRANSLATION_ERRORS) {
                return;
            }
            throw new Error(`Missing translation "${translationId}" for lang "${$uses}."`);
        });

        // This is how we actually get the locales in unit tests.
        //
        // In module specs, you have to call SpecHelper.stubLocales to put specific locale strings
        // onto window.Smartly.locales.modules
        // See lib/node/TranslationBuildTools/README.md for an overview of how locales are built
        let currentTranslations = $translateProvider.translations('en') || {};
        currentTranslations = angular.merge(currentTranslations, window.Smartly?.locales?.modules?.en || {});
        $translateProvider.translations('en', currentTranslations);

        // Needed for certain tests
        currentTranslations = $translateProvider.translations('es') || {};
        currentTranslations = angular.merge(currentTranslations, window.Smartly?.locales?.modules?.es || {});
        $translateProvider.translations('es', currentTranslations);

        // Here, we tell $translateProvider to use a fake loader. This is because the loading
        // of our locales is asynchronous, and makes an XHR to fetch the file(s). Since we
        // don't want  any outstanding XHRs in our specs, we override the loader specified in
        // `vendor/common/components/translation/scripts/translation_module.js` to simply return
        // a resolved promise.
        //
        // See also: https://angular-translate.github.io/docs/#/guide/22_unit-testing-with-angular-translate

        $provide.factory('fakeLoader', $q => () => {
            const deferred = $q.defer();
            deferred.resolve({});
            return deferred.promise;
        });
        $translateProvider.useLoader('fakeLoader');
        $translateProvider.forceAsyncReload(false);
    });
};

SpecHelperModule.directive('editThing', [
    '$injector',
    () => ({
        restrict: 'E',

        scope: {
            thing: '<',
            goBack: '&',
            created: '&',
            destroyed: '&',
        },

        link(scope) {
            scope.save = () => {
                if (!scope.thing.id) {
                    scope.thing.id = 'created';
                    scope.created({
                        $thing: scope.thing,
                    });
                }
            };

            scope.destroy = () => {
                scope.destroyed({
                    $thing: scope.thing,
                });
            };
        },
    }),
]);

SpecHelperModule.factory('SpecHelper', [
    '$injector',
    $injector => {
        const injector = $injector.get('injector');
        const $rootScope = $injector.get('$rootScope');
        const $templateCache = $injector.get('$templateCache');
        const $compile = $injector.get('$compile');
        const $document = $injector.get('$document');
        const EventLogger = injector.get('EventLogger', { optional: true });
        const $window = $injector.get('$window');
        const $q = $injector.get('$q');

        // this causes trouble during auto-reload
        if (EventLogger) {
            jest.spyOn(EventLogger.prototype, '_onUnload').mockImplementation(() => {});
        }

        $injector.get('MockIguana');

        // mock out MathJax which is currently lazy-loaded by app.js
        $window.MathJax = {
            Hub: {
                Queue() {},
            },
        };

        // mock out Modernizr
        $window.Modernizr = {
            touchevents: false,
        };

        // Mock out moment.locale
        jest.spyOn(moment, 'locale').mockImplementation(() => 'en');

        function Renderer() {
            const parentScope = $rootScope.$new();
            this.scope = parentScope;
            this.parentScope = this.scope;
            // eslint-disable-next-line no-use-before-define
            SpecHelper.renderers.push(this);
            this.elems = [];
        }

        /*
        // *** Example Renderer usage

        // create the renderer
        var renderer = SpecHelper.renderer();
        // mock out the template on an internal directive so we can encapsulate
        // the behavior of this one
        renderer.mockTemplate(
            'components/my_component/views/some_other_template.html',
            '<div></div>');
        // render my directive
        renderer.render('<preview-lesson lesson-id="'+lessonAttrs.id+'" />');
        // grab the rendered element
        elem = renderer.elem;
        // if my directive does not have an isolate scope, then I'll be interested in renderer.scope
        scope = renderer.scope;
        // if my directive has an isolate scope, then I can get that isolate scope with renderer.childScope
        scope = renderer.childScope;
    */

        Renderer.prototype = {
            render(html, parentNode) {
                const elem = angular.element(html);
                let compiled;
                if (parentNode) {
                    parentNode.append(elem);
                    compiled = $compile(parentNode);
                } else {
                    compiled = $compile(elem);
                }
                compiled(this.parentScope);
                this.parentScope.$digest();

                this.childScope = this.parentScope.$$childHead;
                this.elem = parentNode || elem;
                this.elems.push(elem);

                if (parentNode) {
                    this.elems.push(parentNode);
                }

                return this;
            },

            mockTemplate(path, html) {
                if (!html) {
                    html = '<div></div>';
                }
                $templateCache.put(path, html);
            },

            mockTemplates(...args) {
                for (let i = 0; i < args.length; i++) {
                    this.mockTemplate(args[i]);
                }
            },

            cleanup() {
                if (this.scope) {
                    this.scope.$destroy();
                }
                if (this.parentScope) {
                    this.parentScope.$destroy();
                }
                if (this.childScope) {
                    this.childScope.$destroy();
                }
                // I checked, and calling remove on a parent node
                // does in fact call it on all children
                this.elems.forEach(elem => {
                    elem.remove();
                });
                this.elems = [];
                // eslint-disable-next-line no-multi-assign
                this.elem = this.parentScope = this.childScope = this.scope = null;

                injector.get('DialogModal', { optional: true })?.removeAlerts(true);
            },
        };

        const SpecHelper = {
            id: Math.random(),

            renderers: [],

            cleanup() {
                try {
                    $rootScope.$destroy();
                } catch (err) {
                    // I do not know what sometimes leads to this error. Hopefully safe
                    // to ignore
                    if (!err.message.match(/Cannot read property '\$\$nextSibling' of null/)) {
                        throw err;
                    }
                }

                this.cleanupCreatedRenderers();
                this.renderers = null;
                EventLogger?.destroy();
            },

            cleanupCreatedRenderers() {
                this.renderers.forEach(renderer => {
                    renderer.cleanup();
                });
                this.renderers = [];
            },

            renderer() {
                return new Renderer();
            },

            render(html, parentNode) {
                return new Renderer().render(html, parentNode);
            },

            compile(element) {
                return new Renderer().render(element);
            },

            inDom(block) {
                $($document[0].body).append('<div class="stage"></div>');
                let stage = $.find('.stage');
                expect(stage.length).toBe(1);
                stage = $(stage);

                try {
                    block(stage);
                } finally {
                    stage.remove();
                }
                stage = null;
            },

            mockCapabilities() {
                // Phantom does support localStorage, but since
                // we don't have Modernizr, Capabilities does
                // not know it
                const Capabilities = $injector.get('Capabilities');
                angular.forEach(
                    {
                        touchEnabled: false,
                        localStorage: true,
                    },
                    (val, key) => {
                        Object.defineProperty(Capabilities, key, {
                            value: val,
                        });
                    },
                );
                return Capabilities;
            },

            stubEventLogging() {
                // don't send any events
                jest.spyOn(EventLogger.prototype, 'tryToSaveBuffer').mockImplementation(() => {});
            },

            stubEventLoggingHarder() {
                // make sure event logger creates no timeouts
                jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
                jest.spyOn(EventLogger.prototype, '_clearBufferAndScheduleNextClear').mockImplementation(() => {});
            },

            stubGoogleMaps() {
                $window.google = {
                    maps: {
                        event: {
                            addListenerOnce: jest.fn(),
                            addListener: jest.fn(),
                            trigger: jest.fn(),
                            removeListener: jest.fn(),
                        },
                        LatLng: jest.fn(),
                        Map: () => ({
                            setOptions: jest.fn(),
                            setCenter: jest.fn(),
                            getZoom: jest.fn().mockReturnValue(3),
                            getCenter: jest.fn().mockReturnValue({
                                equals: jest.fn().mockReturnValue(true),
                            }),
                            getProjection: jest.fn().mockReturnValue({
                                fromLatLngToPoint: jest.fn().mockReturnValue({
                                    x: 0,
                                    y: 0,
                                }),
                                fromPointToLatLng: jest.fn(),
                            }),
                            panToBounds: jest.fn(),
                        }),
                        Marker: jest.fn(),
                        Point: () => ({
                            x: 0,
                            y: 0,
                        }),
                        LatLngBounds: jest.fn(),
                    },
                };
            },

            // If a directive that you're testing makes use of a `<ng-map></ng-map>` directive,
            // calling this method will stub out the `ngMap` module, so that you don't have to
            // worry about any errors from the `ngMap` service in your specs.
            stubNgMap() {
                angular.module('ngMap', []);
                this.stubGoogleMaps();
            },

            stubStudentNetworkMapViewModel() {
                const StudentNetworkMapViewModel = $injector.get('StudentNetworkMapViewModel');

                this.stubGoogleMaps();
                const map = new $window.google.maps.Map();

                const GoogleOverlayView = $injector.get('GoogleOverlayView');
                jest.spyOn(GoogleOverlayView, 'onGoogleApiLoaded').mockImplementation(() => {});

                StudentNetworkMapViewModel.onGoogleApiLoaded();

                return new StudentNetworkMapViewModel(map);
            },

            stubConfig(overrides) {
                const config = {
                    free_mba_groups: 'SMARTER,MBA',
                    default_page_metadata: {
                        default_title: 'default_title',
                        default_description: 'default_description',
                        default_canonical_url: 'default_canonical_url',
                    },
                    join_config: {
                        default: {
                            signup_code: 'FREEMBA',
                        },
                        default_demo: {
                            signup_code: 'FREEDEMO',
                        },
                    },
                    force_idp_initiated_login_urls: {},
                    stripe_publishable_key: 'stripe_publishable_key',
                    stripe_image: 'http://path/to/image',
                    domain: 'quantic.edu',
                    quantic_domain: 'quantic.edu',
                    smartly_domain: 'smart.ly',
                    is_quantic: 'true',
                };
                angular.extend(config, overrides || {});

                const ConfigFactory = $injector.get('ConfigFactory');
                const Config = $injector.get('Config');
                ConfigFactory._config = Config.new(config);
                ConfigFactory.promise = $q.when(ConfigFactory._config);
                ConfigFactory._setServerTime(Date.now(), 0);
            },

            stubStripeCheckout() {
                // stub the Stripe Checkout API
                // this will also prevent RouteAssetLoader from trying to load the stripe checkout script
                $window.StripeCheckout = {
                    configure() {
                        return {
                            open() {},
                            close() {},
                        };
                    },
                };
            },

            stubFramePreloading() {
                const Frame = $injector.get('Lesson.FrameList.Frame');
                jest.spyOn(Frame.prototype, 'preloadAssets').mockImplementation(() => $injector.get('$q').when());
            },

            expectElement(parentEl, selector) {
                if (!parentEl.find) {
                    throw new Error('First argument to expectElement should be a parent element');
                }
                const el = parentEl.find(selector);
                this.expectEqual(1, el.length, `element count for "${selector}".`);
                return el;
            },

            expectHasClass(parentEl, selector, className, val) {
                if (angular.isUndefined(val)) {
                    val = true;
                }
                const el = this.expectElement(parentEl, selector);
                return this.expectEqual(
                    val,
                    el.get(0).classList.contains(className),
                    `element "${selector}" has class "${className}"`,
                );
            },

            expectDoesNotHaveClass(parentEl, selector, className) {
                this.expectHasClass(parentEl, selector, className, false);
            },

            expectElementHasClass(elem, className, message, val) {
                if (angular.isUndefined(val)) {
                    val = true;
                }
                message = message || `element has class "${className}"`;
                if (!elem[0]) {
                    throw new Error('No element provided to expectElementHasClass');
                }
                return this.expectEqual(val, elem[0].classList.contains(className), message);
            },

            expectElementDoesNotHaveClass(elem, className, message) {
                return this.expectElementHasClass(elem, className, message, false);
            },

            expectHasStyle(parentEl, selector, styleName, val) {
                if (angular.isUndefined(val)) {
                    val = true;
                }
                const el = this.expectElement(parentEl, selector);
                this.expectEqual(val, el.css(styleName), `element "${selector}" has style "${styleName}"`);
            },

            expectCheckboxChecked(parentEl, selector, checked) {
                if (angular.isUndefined(checked)) {
                    checked = true;
                }
                const el = this.expectElement(parentEl, selector);

                this.expectEqual(checked, el.is(':checked'), `element "${selector}" checked`);
                return el;
            },

            expectCheckboxUnchecked(parentEl, selector) {
                return this.expectCheckboxChecked(parentEl, selector, false);
            },

            expectElementAttr(parentEl, selector, attr, value) {
                const el = this.expectElement(parentEl, selector);
                SpecHelper.expectEqual(value, el.attr(attr), `element "${selector}" attr = "${attr}"`);
            },

            expectElementDisabled(parentEl, selector, disabled, index) {
                if (angular.isUndefined(disabled)) {
                    disabled = true;
                }
                const el = this.expectNthElement(parentEl, selector, index || 0);
                const isDisabled = el.attr('disabled') === 'disabled' || el.attr('disabled') === '';
                SpecHelper.expectEqual(disabled, isDisabled, `element "${selector}" disabled`);
                return el;
            },

            expectElementEnabled(parentEl, selector, index) {
                return this.expectElementDisabled(parentEl, selector, false, index);
            },

            expectElementHidden(parentEl, selector) {
                const el = this.expectElement(parentEl, selector);
                SpecHelper.expectEqual(true, el.get(0).classList.contains('ng-hide'), `element "${selector}" hidden`);
            },

            expectElementNotHidden(parentEl, selector) {
                const el = this.expectElement(parentEl, selector);
                SpecHelper.expectEqual(false, el.get(0).classList.contains('ng-hide'), `element "${selector}" hidden`);
            },

            expectElementRequired(parentEl, selector, required, index) {
                if (angular.isUndefined(required)) {
                    required = true;
                }
                const el = this.expectNthElement(parentEl, selector, index || 0);
                const isRequired = el.attr('required') === 'required' || el.attr('required') === '';
                SpecHelper.expectEqual(required, isRequired, `element "${selector}" required`);
                return el;
            },

            expectElementNotRequired(parentEl, selector, required, index) {
                return this.expectElementRequired(parentEl, selector, false, index);
            },

            expectNoElement(parentEl, selector) {
                this.expectElements(parentEl, selector, 0);
            },

            expectNoElementImgSrc(parentEl, selector, srcPath) {
                this.expectElementImgSrc(parentEl, selector, srcPath, false);
            },

            expectElements(parentEl, selector, size) {
                const el = parentEl.find(selector);
                if (angular.isDefined(size)) {
                    this.expectEqual(size, el.length, `element count for: "${selector}".`);
                } else {
                    this.expectEqual(true, el.length > 0, `at least one element found for: "${selector}".`);
                }

                return el;
            },

            expectElementImgSrc(parentEl, selector, srcPath, shouldMatch) {
                shouldMatch = angular.isDefined(shouldMatch) ? shouldMatch : true;
                const el = parentEl.find(selector);
                this.expectEqual(
                    shouldMatch,
                    !!(el.attr('src') && el.attr('src').match(new RegExp(srcPath.split('.')[0]))),
                );
            },

            expectElementText(parentEl, selector, text, index) {
                let el;
                if (angular.isDefined(index)) {
                    const els = this.expectElements(parentEl, selector);
                    if (index === 'last') {
                        index = els.length - 1;
                    }
                    el = els.eq(index);
                } else {
                    el = SpecHelper.expectElement(parentEl, selector);
                }
                SpecHelper.expectEqual(
                    text.trim(),
                    el
                        .text()
                        .replace(/\n*\s+/g, ' ')
                        .trim(),
                    `text for: ${selector}`,
                );
                return el;
            },

            trimText(elemOrText) {
                if (typeof elemOrText === 'object') {
                    elemOrText = elemOrText.text();
                }

                return elemOrText.trim().replace(/\n*\s+/g, ' ');
            },

            expectElementPlaceholder(parentEl, selector, placeholder, index) {
                let el;
                if (angular.isDefined(index)) {
                    const els = this.expectElements(parentEl, selector);
                    if (index === 'last') {
                        index = els.length - 1;
                    }
                    el = els.eq(index);
                } else {
                    el = SpecHelper.expectElement(parentEl, selector);
                }
                SpecHelper.expectEqual(
                    placeholder.trim(),
                    el
                        .attr('placeholder')
                        .replace(/\n*\s+/g, ' ')
                        .trim(),
                    `placeholder for: ${selector}`,
                );
                return el;
            },

            expectElementTitle(parentEl, selector, title, index) {
                let el;
                if (angular.isDefined(index)) {
                    const els = this.expectElements(parentEl, selector);
                    if (index === 'last') {
                        index = els.length - 1;
                    }
                    el = els.eq(index);
                } else {
                    el = SpecHelper.expectElement(parentEl, selector);
                }
                SpecHelper.expectEqual(
                    title.trim(),
                    el
                        .attr('title')
                        .replace(/\n*\s+/g, ' ')
                        .trim(),
                    `title for: ${selector}`,
                );
                return el;
            },

            expectNthElement(parentEl, selector, index) {
                const el = this.expectElements(parentEl, selector).eq(index);
                SpecHelper.expectEqual(true, el.length === 1, `element found at index ${index} for : ${selector}`);
                return el;
            },

            stubWindowPerformance(val) {
                jest.spyOn($window.performance, 'now').mockReturnValue(val);
            },

            stubCurrentUser(role, lesson, lessonAccess, overrides) {
                $injector.get('UserFixtures');
                const User = $injector.get('User');
                role = role || 'learner';
                $rootScope.currentUser = User.fixtures.getInstanceWithRole(role, overrides);
                if (lesson && lessonAccess) {
                    $rootScope.currentUser.lessonPermissions[lesson.id] = lessonAccess;
                }
                $rootScope.currentUser.ghostMode = false;
                return $rootScope.currentUser;
            },

            stubNoCurrentUser() {
                // eslint-disable-next-line no-shadow
                const $rootScope = $injector.get('$rootScope');
                $rootScope.currentUser = undefined;
            },

            stubLessonProgress() {
                return $injector.get('LessonProgress');
            },

            click(parentEl, selector, index) {
                let el;
                SpecHelper.expectElementEnabled(parentEl, selector, index);
                if (angular.isDefined(index)) {
                    el = this.expectNthElement(parentEl, selector, index);
                } else {
                    el = this.expectElement(parentEl, selector);
                }
                el.click();
                return el;
            },

            mousedown(parentEl, selector, index) {
                let el;
                if (angular.isDefined(index)) {
                    el = this.expectNthElement(parentEl, selector, index);
                } else {
                    el = this.expectElement(parentEl, selector);
                }
                el.mousedown();
            },

            clickKey(key) {
                const keyCode = {
                    '1': 49,
                    ESC: 27,
                    ENT: 13,
                    SPACE: 32,
                }[key];

                if (!keyCode) {
                    throw new Error(`Do not know key code for "${key}". Please add it.`);
                }

                /* eslint-disable */
                const e = $window.jQuery.Event('keydown');
                /* eslint-enable */

                e.which = keyCode;
                $document.trigger(e);
            },

            updateTextInput(elem, selector, newValue, index) {
                let input;
                if (angular.isDefined(index)) {
                    const inputs = this.expectElements(elem, selector);
                    if (index === 'last') {
                        index = inputs.length - 1;
                    }
                    input = inputs.eq(index);
                } else {
                    input = SpecHelper.expectElement(elem, selector);
                }

                SpecHelper.expectEqual(1, input.length, 'inputs size');

                // modify the value
                input.val(newValue);
                input.change();
                try {
                    $injector.get('$timeout').flush();
                } catch (err) {
                    // I haven't confirmed this, but my guess is that we end up
                    // with a timeout to flush only when certain modules are active.
                    // Maybe the click logging in EventLogger?
                    if (err.message !== 'No deferred tasks to be flushed') {
                        throw err;
                    }
                }

                return input;
            },

            updateNoApplyTextInput(elem, selector, newValue, index) {
                let input;
                if (angular.isDefined(index)) {
                    const inputs = this.expectElements(elem, selector);
                    if (index === 'last') {
                        index = inputs.length - 1;
                    }
                    input = inputs.eq(index);
                } else {
                    input = SpecHelper.expectElement(elem, selector);
                }

                SpecHelper.expectEqual(1, input.length, 'inputs size');

                // modify the value
                input.val(newValue);
                input.keyup();

                return input;
            },

            expectTextInputVal(parentEl, selector, text, index) {
                let el;
                if (angular.isDefined(index)) {
                    const els = this.expectElements(parentEl, selector);
                    if (index === 'last') {
                        index = els.length - 1;
                    }
                    el = els.eq(index);
                } else {
                    el = SpecHelper.expectElement(parentEl, selector);
                }
                SpecHelper.expectEqual(text.trim(), el.val().trim(), `text for: ${selector}`);
                return el;
            },

            updateTextArea(elem, selector, newValue, index) {
                return this.updateTextInput(elem, selector, newValue, index);
            },

            updateSelect(elem, selector, newValue) {
                return this.updateInput(elem, selector, newValue);
            },

            updateMultiSelect(elem, selector, newValue) {
                const multiSelect = SpecHelper.expectElement(elem, selector);
                if (multiSelect.find('select').length > 0) {
                    this.updateSelect(multiSelect, 'select', newValue);
                } else {
                    const scope = multiSelect.isolateScope();
                    const value = scope.convertValueToHashKey(newValue);
                    this.updateSelectize(multiSelect, 'selectize', value);
                }
            },

            updateSelectize(elem, selector, newValue) {
                const selectizeEl = this.expectElement(elem, selector);
                const selectize = selectizeEl[0].selectize;

                if (newValue === null) {
                    selectize.clear();
                    return;
                }

                const values = Array.isArray(newValue) ? newValue : [newValue];

                _.each(values, val => {
                    SpecHelper.assertHasSelectizeOptionValue(elem, selector, val);
                });

                selectize.setValue(newValue);
                if (elem.scope()) {
                    elem.scope().$digest();
                }
            },

            // this calls the createItem API and sets the new value to the selected value
            selectizeCreateItem(elem, selector, inputValue) {
                const selectize = this.expectElement(elem, selector);
                selectize[0].selectize.createItem(inputValue);
            },

            selectizeOnFocus(elem, selector) {
                const selectize = this.expectElement(elem, selector);
                selectize[0].selectize.onFocus();
            },

            selectizeOnBlur(elem, selector) {
                // called onBlur wasn't correctly simulating a blur. when we dove into selectize code
                // we found that they trigger a blur event
                const selectize = this.expectElement(elem, selector);
                selectize[0].selectize.trigger('blur');
            },

            assertSelectValue(elem, selector, expectedValue) {
                const select = this.expectElement(elem, selector);
                const ctrl = select.controller('ngModel');
                this.expectEqual(true, !!ctrl, `ngModel controller exists on ${selector}`);
                expect(ctrl.$modelValue).toEqual(expectedValue);
            },

            assertSelectizeValue(elem, selector, expectedValue) {
                const selectize = this.expectElement(elem, selector);
                const values = [];
                if (angular.isUndefined(expectedValue)) {
                    expectedValue = [];
                } else {
                    expectedValue = _.isArray(expectedValue) ? expectedValue : [expectedValue];
                }
                selectize
                    .next()
                    .find('.selectize-input [data-value]')
                    .each(function () {
                        values.push($(this).attr('data-value'));
                    });
                expect(values).toEqual(expectedValue);
            },

            assertHasSelectizeOptionValue(elem, selector, expectedValue) {
                const actualValues = this._getSelectizeOptionValues(elem, selector);
                this.expectEqual(
                    true,
                    _.contains(actualValues, expectedValue),
                    `Expected '${expectedValue}' to exist in ${actualValues.join(',')}`,
                );
            },

            _getSelectizeOptionValues(elem, selector) {
                const selectize = this.expectElement(elem, selector)[0].selectize;
                return _.pluck(selectize.options, selectize.settings.valueField);
            },

            assertSelectizeOptionValues(elem, selector, expectedOptionValues) {
                const values = this._getSelectizeOptionValues(elem, selector);
                expect(values).toEqual(expectedOptionValues);
            },

            forceFormValidity(scope, form) {
                Object.keys(form).forEach((key, i) => {
                    const input = form[i];
                    if (input && input.$setValidity) {
                        _.each(input.$validators, (val, k) => {
                            input.$setValidity(k, true);
                        });
                    }
                    scope.$digest();
                });
            },

            assertSelectOptions(elem, selector, expectedOptionValues) {
                const select = this.expectElement(elem, selector);
                const values = [];
                select.find('option').each(function () {
                    const val = $(this).val();
                    values.push(val);
                });
                expect(values).toEqual(expectedOptionValues);
            },

            assertRowValues(elem, selector, expectedValues) {
                const scope = elem.scope();
                if (!scope) {
                    throw new Error(`Invalid value for element scope: ${scope}`);
                }
                scope.$digest();
                const colIndexMap = {};

                // This looks complex, but it allows us to set up our assertion without knowing
                // which column is which, and to move columns around without breaking the spec
                _.each(expectedValues, (expectedVal, key) => {
                    let colIndex;
                    // < 10 is arbitrary here.  Just needs to be greater than
                    // the number of columns
                    const headers = elem.find('th');
                    for (let i = 0; i < 10; i++) {
                        // in editable things list, the class is now attached to a span
                        // inside of the th element
                        if (headers.eq(i).hasClass(key) || headers.eq(i).find(`> span.${key}`).length > 0) {
                            colIndex = i;
                            break;
                        }
                    }
                    if (angular.isUndefined(colIndex)) {
                        throw new Error(`No column for ${key}`);
                    }
                    expect(elem.find(selector).eq(colIndex).text().trim()).toEqual(
                        expectedVal.trim(),
                        `Unexpected value for ${key}`,
                    );
                    colIndexMap[key] = colIndex;
                });

                return colIndexMap;
            },

            updateDateInput(elem, selector, newValue) {
                if (newValue.getTime) {
                    throw new Error('Pass in a string like "2015-01-01"');
                }
                return this.updateInput(elem, selector, newValue);
            },

            updateAdminDatetimePicker(elem, selector, newValue) {
                if (!newValue.getTime) {
                    throw new Error('Pass in a date');
                }
                return this.updateInput(elem, selector, newValue);
            },

            updateInput(elem, selector, newValue) {
                const select = this.expectElement(elem, selector);
                const ctrl = select.controller('ngModel');
                this.expectEqual(true, !!ctrl, `ngModel controller exists on ${selector}`);
                ctrl.$setViewValue(newValue);
                if (elem.scope()) {
                    elem.scope().$digest();
                }
                return select;
            },

            expectInputValue(elem, selector, value) {
                const select = this.expectElement(elem, selector);
                const ctrl = select.controller('ngModel');
                this.expectEqual(true, !!ctrl, `ngModel controller exists on ${selector}`);
                this.expectEqual(value, ctrl.$viewValue);
            },

            uploadWithContentItemImageUpload(elem, selector, s3Asset, fileName) {
                const obj = SpecHelper.startUploadWithContentItemImageUpload(elem, selector, s3Asset, fileName);
                return obj.getResponseFromServer();
            },

            startUploadWithContentItemImageUpload(elem, selector, s3Asset, fileName = 'fileName.png') {
                const guid = $injector.get('guid');
                s3Asset = s3Asset || {
                    id: 1,
                    file_file_name: fileName,
                    formats: {
                        small: {
                            url: `https://small/${guid.generate()}/${fileName}`,
                            width: 565,
                            height: 275,
                        },
                        original: {
                            url: `https://original/${guid.generate()}/${fileName}`,
                        },
                    },
                };
                const uploadElem = SpecHelper.expectElement(elem, selector);
                SpecHelper.expectEqual(false, uploadElem.attr('disabled') === 'disabled', 'uploader disabled');
                if (uploadElem.scope().startUpload) {
                    uploadElem.scope().startUpload({
                        $$fileName: fileName.split('.')[0],
                        $$dataUrl: 'dataUrl',
                    });
                    uploadElem.scope().$apply();
                }
                return {
                    getResponseFromServer(overrideFileName) {
                        const _fileName = overrideFileName || fileName;
                        uploadElem.scope().uploaded({
                            $$s3Asset: s3Asset,
                            $$fileName: _fileName.split('.')[0],
                        });
                        // for some reason, this has to be apply.  digest is
                        // not good enough
                        uploadElem.scope().$apply();
                        return s3Asset;
                    },
                    getErrorFromServer() {
                        if (uploadElem.scope().error) {
                            uploadElem.scope().error();
                            uploadElem.scope().$apply();
                        }
                    },
                };
            },

            toggleRadio(elem, selector, index) {
                let el;
                if (angular.isDefined(index)) {
                    const radios = this.expectElements(elem, selector);
                    if (index === 'last') {
                        index = radios.length - 1;
                    }
                    el = radios.eq(index);
                } else {
                    el = SpecHelper.expectElement(elem, selector);
                }
                const ngModelController = el.controller('ngModel');
                let val;
                if (el.attr('ng-value')) {
                    val = el.scope().$eval(el.attr('ng-value'));
                } else {
                    val = el.val();
                }
                ngModelController.$setViewValue(val);
                $injector.get('$timeout').flush();
                return el;
            },

            toggleCheckbox(elem, selector, index) {
                let el;
                if (angular.isDefined(index)) {
                    const inputs = this.expectElements(elem, selector);
                    if (index === 'last') {
                        index = inputs.length - 1;
                    }
                    el = inputs.eq(index);
                } else {
                    el = SpecHelper.expectElement(elem, selector);
                }
                el.click();
                el.trigger('change');

                return el;
            },

            uncheckCheckbox(elem, selector, index) {
                let checkbox;
                if (angular.isDefined(index)) {
                    checkbox = this.expectNthElement(elem, selector, index);
                } else {
                    checkbox = this.expectElement(elem, selector);
                }

                if (checkbox.prop('checked')) {
                    SpecHelper.toggleCheckbox(elem, selector, index);
                }
            },

            checkCheckbox(elem, selector, index) {
                let checkbox;
                if (angular.isDefined(index)) {
                    checkbox = this.expectNthElement(elem, selector, index);
                } else {
                    checkbox = this.expectElement(elem, selector);
                }
                if (!checkbox.prop('checked')) {
                    SpecHelper.toggleCheckbox(elem, selector, index);
                }
            },

            setCheckboxValue(elem, value) {
                if (elem.prop('checked') !== value) {
                    elem.click();
                }
            },

            selectOptionByValue(elem, selector, value) {
                // Warning, this is based on the value in the html, not
                // the value linked to ngModel.   Confusing, I know
                const el = SpecHelper.expectElement(elem, selector);
                el.val(value);
                el.trigger('change');
            },

            selectOptionByLabel(elem, selector, label) {
                const el = SpecHelper.expectElement(elem, selector);
                const options = el.find('option');
                let value;
                angular.forEach(options, opt => {
                    if ($(opt).text().trim() === label || $(opt).text() === label) {
                        value = $(opt).val();
                    }
                });
                SpecHelper.expectEqual(true, angular.isDefined(value), `Found option with label "${label}".`);
                this.selectOptionByValue(elem, selector, value);
            },

            expectEqualObjects(expected, actual) {
                // eslint-disable-next-line no-shadow
                const SpecHelper = this;
                const expectedkeys = [];
                const actualkeys = [];

                if (
                    (_.isArray(expected) && !_.isArray(actual)) ||
                    typeof expected !== typeof actual ||
                    typeof expected === 'string'
                ) {
                    expect(actual).toEqual(expected);
                    return;
                }

                angular.forEach(expected, (value, key) => {
                    expectedkeys.push(key);
                });
                angular.forEach(actual, (value, key) => {
                    actualkeys.push(key);
                });
                expect({
                    objectKeys: actualkeys.sort(),
                }).toEqual({
                    objectKeys: expectedkeys.sort(),
                });

                angular.forEach(expected, (value, key) => {
                    const actualValue = actual[key];
                    if (_.isArray(value) && _.isArray(actualValue)) {
                        expect(`length of ${key} (${actualValue.length})`).toEqual(
                            `length of ${key} (${value.length})`,
                        );
                        angular.forEach(value, (el, i) => {
                            SpecHelper.expectEqualObjects(el, actualValue[i]);
                        });
                    } else if (typeof value === 'object') {
                        SpecHelper.expectEqualObjects(value, actualValue);
                    } else {
                        const a = {};
                        a[key] = actualValue;
                        const e = {};
                        e[key] = value;
                        expect(a).toEqual(e);
                    }
                });
            },

            callNextInQueue(el) {
                const nextFunctionInQueue = el.queue()[1];
                if (!nextFunctionInQueue) {
                    throw new Error('There is no function in the queue');
                }
                el.queue().splice(1, 1);
                nextFunctionInQueue.apply(el, [() => {}]);
            },

            assertMatchesFilters(el, filters, has) {
                has = !!has;
                angular.forEach(filters, filter => {
                    const actual = {};
                    actual[`matches:${filter}`] = $(el).is(filter);
                    const expected = {};
                    expected[`matches:${filter}`] = has;
                    expect(actual).toEqual(expected);
                });
            },

            stubWindowImage(block) {
                const origImage = $window.Image;
                const createdImages = [];
                $window.Image = function () {
                    createdImages.push(this);
                    this.load = function (options) {
                        if (!options) {
                            options = {};
                        }
                        this.width = options.width || 37;
                        this.height = options.height || 49;
                        if (this.onload) {
                            this.onload();
                        }
                    };
                };

                try {
                    block(createdImages);
                } finally {
                    $window.Image = origImage;
                }
            },

            expectEqual(expected, actual, label) {
                if (!label) {
                    expect(actual).toEqual(expected);
                } else {
                    const wrappedExpected = {};
                    wrappedExpected[label] = expected;
                    const wrappedActual = {};
                    wrappedActual[label] = actual;
                    expect(wrappedActual).toEqual(wrappedExpected);
                }
            },

            expectVeryClose(expected, actual, discrepency = 0.01, label) {
                if (!label) {
                    label = `difference < ${discrepency}`;
                }
                SpecHelper.expectEqual(Math.abs(expected - actual) < discrepency, true, label);
            },

            stubDirective(directiveName) {
                $injector.get(`${directiveName}Directive`)[0].restrict = '';
            },

            dateToQueryString(date) {
                // 2014-12-31T05:00:00.000Z
                return [
                    date.getUTCFullYear(),
                    '-',
                    String.padNumber(date.getUTCMonth() + 1, 2),
                    '-',
                    String.padNumber(date.getUTCDate(), 2),
                    'T',
                    String.padNumber(date.getUTCHours(), 2),
                    ':',
                    String.padNumber(date.getUTCMinutes(), 2),
                    ':',
                    String.padNumber(date.getUTCSeconds(), 2),
                    '.',
                    String.padNumber(date.getUTCMilliseconds(), 3),
                    'Z',
                ].join('');
            },

            disableHttpQueue() {
                jest.spyOn($injector.get('HttpQueueAndRetry').prototype, 'shouldQueue').mockReturnValue(false);
            },

            verifyNoNewTimeouts(fn) {
                const browser = $injector.get('$browser');
                const start = browser.deferredFns.length;
                fn();
                const finish = browser.deferredFns.length;

                if (start !== finish) {
                    /* eslint-disable */
                    dump(browser.deferredFns[browser.deferredFns.length - 2].fn.toString());
                    dump(browser.deferredFns[browser.deferredFns.length - 1].fn.toString());
                    /* eslint-enable */

                    throw new Error(
                        `Expected no new timeouts to be created but ${finish - start} timeout(s) was created.`,
                    );
                }
            },

            mockTranslatableLessonExportSetPrepare() {
                const TranslatableLessonExportSet = $injector.get('TranslatableLessonExportSet');
                // eslint-disable-next-line no-shadow
                const $q = $injector.get('$q');
                let resolve;
                let reject;
                const promise = $q((_resolve, _reject) => {
                    resolve = _resolve;
                    reject = _reject;
                });
                jest.spyOn(TranslatableLessonExportSet.prototype, '_prepare').mockReturnValue(promise);
                return {
                    resolve,
                    reject,
                };
            },

            submitForm(elem) {
                // the chrome runner requires dispatching this event explicitly
                // rather than relying on the underlying type=submit button click
                // this might be avoidable with a timeout or whatnot, but this is cleaner
                elem.find('form').triggerHandler('submit');
            },
        };

        // in tests, we don't want to bother with this lazy loading. It just
        // confuses things.  Some tests do not include this module, so we have
        // to catch it
        try {
            SpecHelper.stubDirective('lazyLoadedPanelBody');
            // eslint-disable-next-line no-empty
        } catch (e) {}

        return SpecHelper;
    },
]);
