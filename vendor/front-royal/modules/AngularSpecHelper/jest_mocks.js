jest.mock('FrontRoyalGsap', () => ({
    TweenLite: {
        set: () => {},
        to: () => {},
    },
    TweenMax: {
        fromTo: () => {},
        set: () => {},
        to: () => {},
    },
    // See also https://greensock.com/forums/topic/7592-unit-testing-with-gsap/
    TimelineMax: class {
        constructor() {
            this.to = jest.fn().mockReturnThis();
            this.fromTo = jest.fn().mockReturnThis();
            this.from = jest.fn().mockReturnThis();
            this.set = jest.fn();
        }
    },
    Power1: {},
    Power2: {},
    Bounce: {},
    CSSPlugin: {},
    AttrPlugin: {},
    ScrollToPlugin: {},
}));
