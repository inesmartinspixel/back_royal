export default angular.module('FrontRoyal.Institutions', []).factory('Institution', [
    'Iguana',
    Iguana =>
        Iguana.subclass(function () {
            this.setCollection('institutions');
            this.alias('Institution');
            this.setIdProperty('id');

            this.setCallback('after', 'copyAttrsOnInitialize', function () {
                this.playlist_pack_ids = this.playlist_pack_ids || [];
                this.reports_viewer_ids = this.reports_viewer_ids || [];
                this.users = this.users || [];
                this.external = this.external === undefined ? true : this.external;
            });

            this.extend({
                QUANTIC_ID: '85fec419-8dc5-45a5-afbd-0cc285a595b9',
            });

            Object.defineProperty(this.prototype, 'createdAt', {
                get() {
                    return new Date(this.created_at * 1000);
                },
            });

            Object.defineProperty(this.prototype, 'updatedAt', {
                get() {
                    return new Date(this.updated_at * 1000);
                },
            });

            Object.defineProperty(this.prototype, 'groupNamesString', {
                get() {
                    return _.pluck(this.groups, 'name').join(', ');
                },
            });

            return {};
        }),
]);
