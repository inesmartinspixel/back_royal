import UAParser from 'ua-parser-js';

function getUserAgentParser() {
    return new UAParser();
}

// Returns an object containing to parsed User-Agent info
export function getUserAgentInfo() {
    return getUserAgentParser().getResult();
}

// Returns an object containing to parsed User-Agent info
export function getSnakeCasedObject() {
    // parse various user agent properties
    const agentInfo = getUserAgentInfo();

    // agentInfo is guaranteed to have the top-level properties
    // and could theoretically change out from under a loaded version of the app
    if (agentInfo) {
        return {
            browser_name: agentInfo.browser.name,
            browser_major: agentInfo.browser.major,
            browser_version: agentInfo.browser.version,
            device_model: agentInfo.device.model,
            device_type: agentInfo.device.type,
            device_vendor: agentInfo.device.vendor,
            os_name: agentInfo.os.name,
            os_version: agentInfo.os.version,
            user_agent: agentInfo.ua,
        };
    }

    return null;
}

// Determines if the User-Agent device OS is Android
export function isAndroidDevice() {
    return getUserAgentParser().getOS().name === 'Android';
}

// Determines if the User-Agent device OS is iOS
export function isiOSDevice() {
    return getUserAgentParser().getOS().name === 'iOS';
}

export function isMobileDevice() {
    return getUserAgentParser().getDevice().type === 'mobile';
}

export function isMobileOrTabletDevice() {
    return isMobileDevice() || getUserAgentParser().getDevice().type === 'tablet';
}

// Determines if the User-Agent is Safari or is an iOS device
export function isSafariOrIsiOSDevice() {
    return getUserAgentParser().getBrowser().name === 'Safari' || isiOSDevice();
}

export function isMobileSafari() {
    return getUserAgentParser().getBrowser().name === 'Mobile Safari';
}

export function isChrome() {
    return getUserAgentParser().getBrowser().name === 'Chrome';
}

export function isFirefox() {
    return getUserAgentParser().getBrowser().name === 'Firefox';
}

// Determines if the User-Agent is Internet Explorer
export function isIE() {
    return getUserAgentParser().getBrowser().name === 'IE';
}

export function isEdge() {
    return getUserAgentParser().getBrowser().name === 'Edge';
}

export function isFacebookBrowser() {
    const ua = getUserAgentParser().getResult().ua;
    return ua.indexOf('FBAN') > -1 || ua.indexOf('FBAV') > -1 || ua.indexOf('FBIOS') > -1;
}

export function isInstagramBrowser() {
    const ua = getUserAgentParser().getResult().ua;
    return ua.indexOf('Instagram') > -1;
}
