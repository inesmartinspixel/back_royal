import UAParser from 'ua-parser-js';
import * as userAgentHelper from './index';

jest.mock('ua-parser-js', () => class MockUaParser {});

describe('userAgentHelper', () => {
    beforeEach(() => {
        UAParser.prototype.getResult = jest.fn();
        UAParser.prototype.getOS = jest.fn();
        UAParser.prototype.getBrowser = jest.fn();
        UAParser.prototype.getDevice = jest.fn();
    });

    describe('getUserAgentInfo', () => {
        it('should getResult', () => {
            userAgentHelper.getUserAgentInfo();
            expect(UAParser.prototype.getResult).toHaveBeenCalled();
        });
    });

    describe('isAndroidDevice', () => {
        it('should getOS', () => {
            UAParser.prototype.getOS.mockReturnValue({});
            userAgentHelper.isAndroidDevice();
            expect(UAParser.prototype.getOS).toHaveBeenCalled();
        });

        it("should return false if OS name is not 'Android'", () => {
            UAParser.prototype.getOS.mockReturnValue({
                name: 'iOS',
            });
            expect(userAgentHelper.isAndroidDevice()).toBe(false);
        });

        it("should return true if OS name is 'Android'", () => {
            UAParser.prototype.getOS.mockReturnValue({
                name: 'Android',
            });
            expect(userAgentHelper.isAndroidDevice()).toBe(true);
        });
    });

    describe('isMobileDevice', () => {
        it('should getDevice', () => {
            UAParser.prototype.getDevice.mockReturnValue({});
            userAgentHelper.isMobileDevice();
            expect(UAParser.prototype.getDevice).toHaveBeenCalled();
        });

        it("should return false if device type is not 'mobile'", () => {
            UAParser.prototype.getDevice.mockReturnValue({
                type: 'tablet',
            });
            expect(userAgentHelper.isMobileDevice()).toBe(false);
        });

        it("should return true if device type is 'mobile'", () => {
            UAParser.prototype.getDevice.mockReturnValue({
                type: 'mobile',
            });
            expect(userAgentHelper.isMobileDevice()).toBe(true);
        });
    });

    describe('isiOSDevice', () => {
        it('should getOS', () => {
            UAParser.prototype.getOS.mockReturnValue({});
            userAgentHelper.isiOSDevice();
            expect(UAParser.prototype.getOS).toHaveBeenCalled();
        });

        it("should return false if OS name is not 'iOS'", () => {
            UAParser.prototype.getOS.mockReturnValue({
                name: 'Android',
            });
            expect(userAgentHelper.isiOSDevice()).toBe(false);
        });

        it("should return true if OS name is 'iOS'", () => {
            UAParser.prototype.getOS.mockReturnValue({
                name: 'iOS',
            });
            expect(userAgentHelper.isiOSDevice()).toBe(true);
        });
    });

    describe('isIE', () => {
        it('should getBrowser', () => {
            UAParser.prototype.getBrowser.mockReturnValue({});
            userAgentHelper.isIE();
            expect(UAParser.prototype.getBrowser).toHaveBeenCalled();
        });

        it("should return false if browser name is not 'IE'", () => {
            UAParser.prototype.getBrowser.mockReturnValue({
                name: 'Chrome',
            });
            expect(userAgentHelper.isIE()).toBe(false);
        });

        it("should return true if browser name is 'IE'", () => {
            UAParser.prototype.getBrowser.mockReturnValue({
                name: 'IE',
            });
            expect(userAgentHelper.isIE()).toBe(true);
        });
    });
});
