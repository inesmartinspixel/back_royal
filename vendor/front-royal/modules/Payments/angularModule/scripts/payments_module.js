import 'Cohorts/angularModule';
import 'SafeApply/angularModule';
import 'Translation/angularModule';
import 'EventLogger/angularModule';
import 'Navigation/angularModule';
import 'Status/angularModule';
import 'Users/angularModule';

export default angular.module('FrontRoyal.Payments', [
    'FrontRoyal.Cohorts',
    'FrontRoyal.Navigation',
    'FrontRoyal.Users',
    'FrontRoyal.Status',
    'EventLogger',
    'Translation',
    'safeApply',
]);
