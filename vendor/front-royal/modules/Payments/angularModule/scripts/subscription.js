import angularModule from 'Payments/angularModule/scripts/payments_module';

angularModule.factory('Subscription', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');

        return Iguana.subclass(function () {
            this.setCollection('subscriptions');
            this.alias('Subscription');

            Object.defineProperty(this.prototype, 'planName', {
                get() {
                    return this.plan.name;
                },
            });

            Object.defineProperty(this.prototype, 'hasActiveAutoRenewal', {
                get() {
                    return !this.cancel_at_period_end;
                },
            });
        });
    },
]);
