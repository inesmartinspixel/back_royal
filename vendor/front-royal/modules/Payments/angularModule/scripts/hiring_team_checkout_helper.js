import angularModule from 'Payments/angularModule/scripts/payments_module';

angularModule.factory('Payments.HiringTeamCheckoutHelper', [
    '$injector',

    $injector => {
        const StripeCheckoutHelper = $injector.get('Payments.StripeCheckoutHelper');
        const $rootScope = $injector.get('$rootScope');
        const TranslationHelper = $injector.get('TranslationHelper');
        const translationHelper = new TranslationHelper('payments.hiring_team_checkout_helper');
        const HiringTeam = $injector.get('HiringTeam');

        const HiringTeamCheckoutHelper = StripeCheckoutHelper.subclass(function () {
            Object.defineProperty(this.prototype, 'hiringTeam', {
                get() {
                    // owner is set in the superclass.  Aliasing here for readability
                    return this.owner;
                },
            });

            Object.defineProperty(this.prototype, 'currentPlanId', {
                get() {
                    return this.hiringTeam.primarySubscription && this.hiringTeam.primarySubscription.stripe_plan_id;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'canEditPaymentInfo', {
                get() {
                    return $rootScope.currentUser.isHiringTeamOwner;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'ownerEmail', {
                get() {
                    return $rootScope.currentUser.hiring_team.email;
                },
                configurable: true,
            });

            return {
                subscribeToLegacyPlan(stripePlanId) {
                    if (this.currentPlanId && this.currentPlanId === stripePlanId) {
                        throw new Error('Hiring Team already has a subscription for this plan.');
                    }

                    // The second argument is showNoCardConfirm.  We set it to true here so
                    // we can ask them to confirm before switching plans.
                    return this._subscribe(stripePlanId, {
                        metaForSaveCall: {
                            cancel_current_subscription: !!this.currentPlanId,
                        },

                        // Assume they have a valid card if they already have at least one subscription.
                        // If not, we'll handle the error later
                        requiresCardInfo: !_.any(this.hiringTeam.subscriptions),
                        showNoCardConfirm: true,
                    });
                },

                subscribeToPayPerPostPlan(stripePlanId, openPositiondId) {
                    // The second argument is showNoCardConfirm.  We do not need to show
                    // a confirm here, like we do when switching plans in subscribeToLegacyPlan
                    return this._subscribe(stripePlanId, {
                        metaForSaveCall: {
                            open_position_id: openPositiondId,
                        },

                        // If the hiring team does not already have a card on file, and this is the owner,
                        // then we want to pop up the payment modal.
                        // If this isn't the owner, then we won't show the popup.
                        // If there is no card or the card fails when we try to charge it, the call to the subscriptions api will fail
                        // and we will handle the error.
                        requiresCardInfo:
                            !$rootScope.currentUser.ownsHiringTeamThatSeemsToHavePaymentSource &&
                            this.canEditPaymentInfo,
                        showNoCardConfirm: false,
                    });
                },

                subscribeToUnlimitedWithSourcingPlan() {
                    const stripePlan = this.hiringTeam.stripePlanForHiringPlan(
                        HiringTeam.HIRING_PLAN_UNLIMITED_WITH_SOURCING,
                    );
                    return this._subscribe(stripePlan.id, {
                        requiresCardInfo: !$rootScope.currentUser.hiringTeamSeemsToHavePaymentSource,
                        showNoCardConfirm: true,
                    });
                },

                modifyPaymentDetails($super) {
                    return $super({
                        name: translationHelper.get('smartly_talent'),
                    });
                },

                _subscribe(stripePlanId, opts) {
                    const self = this;

                    const stripePlan = this.hiringTeam.getPlanForId(stripePlanId);

                    if (!stripePlan) {
                        throw new Error(`No plan found for "${stripePlanId}"`);
                    }

                    // see also HiringTeam::SubscriptionConcern.is_pay_per_post_plan?
                    let panelLabel;
                    let description;
                    if (stripePlan.product_id.match(/per_post/)) {
                        panelLabel = translationHelper.get('publish');
                        description = translationHelper.get('plan_description_pay_per_post');
                    } else if (stripePlan.product_id.match(/unlimited/)) {
                        panelLabel = `${translationHelper.get('subscribe')} - `;
                        description = translationHelper.get('plan_description_unlimited');
                    } else if (stripePlan.usage_type === 'metered') {
                        panelLabel = translationHelper.get('subscribe');
                        description = translationHelper.get('plan_description_metered');
                    } else {
                        throw 'Unrecognized stripe plan';
                    }

                    return self.getTokenIfNeededAndCreateSubscription({
                        ...opts,

                        // the following stuff is ignored if `requiresCardInfo` is false
                        attrs: {
                            stripe_plan_id: stripePlanId,
                            hiring_team_id: this.hiringTeam.id,
                        },
                        checkoutModalOptions: {
                            name: stripePlan.product_name,
                            description,
                            amount: stripePlan.usage_type === 'metered' ? 0 : stripePlan.amount,
                            panelLabel,
                        },
                    });
                },
            };
        });

        return HiringTeamCheckoutHelper;
    },
]);
