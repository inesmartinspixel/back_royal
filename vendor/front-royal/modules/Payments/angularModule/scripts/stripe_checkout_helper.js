import angularModule from 'Payments/angularModule/scripts/payments_module';
/*
    Adds scope methods for Stripe Checkout
*/

angularModule.factory('Payments.StripeCheckoutHelper', [
    '$injector',

    $injector => {
        const RouteAssetLoader = $injector.get('Navigation.RouteAssetLoader');
        const ConfigFactory = $injector.get('ConfigFactory');
        const $window = $injector.get('$window');
        const $rootScope = $injector.get('$rootScope');
        const Subscription = $injector.get('Subscription');
        const safeApply = $injector.get('safeApply');
        const DialogModal = $injector.get('DialogModal');
        const HttpQueue = $injector.get('HttpQueue');
        const ApiErrorHandler = $injector.get('ApiErrorHandler');
        const TranslationHelper = $injector.get('TranslationHelper');
        const translationHelper = new TranslationHelper('payments.stripe_checkout_helper');
        const SuperModel = $injector.get('SuperModel');
        const $q = $injector.get('$q');
        const $http = $injector.get('$http');

        const StripeCheckoutHelper = SuperModel.subclass(function () {
            // overridden in hiringTeamCheckoutHelper
            Object.defineProperty(this.prototype, 'canEditPaymentInfo', {
                get() {
                    throw new Error('Subclasses of StripeCheckoutHelper have to implement canEditPaymentInfo');
                },
                configurable: true,
            });

            // NOTE: only hiringTeamCheckoutHelper implements this. Since canEditPaymentInfo is always
            // true for users, they never call `ownerEmail`
            Object.defineProperty(this.prototype, 'ownerEmail', {
                get() {
                    throw new Error('Subclasses of StripeCheckoutHelper have to implement ownerEmail');
                },
                configurable: true,
            });

            this.extend({
                // cache promises and handlers
                configurePromise: null,
                handler: null,

                //---------------------------------------------------------------
                // Generic Pass-through methods that wrap the Stripe Checkout API
                //---------------------------------------------------------------
                configure(owner, forceConfigure = false) {
                    // if not already configured/configuring...
                    if (!StripeCheckoutHelper.configurePromise || forceConfigure) {
                        // we need the config for the stripe publishable key...
                        StripeCheckoutHelper.configurePromise = ConfigFactory.getConfig().then(frontRoyalConfig => {
                            // configure stripe, allowing overrides
                            const options = {
                                key: frontRoyalConfig.stripe_publishable_key,
                                image: owner.shouldSeeQuanticBranding
                                    ? frontRoyalConfig.stripe_image_quantic
                                    : frontRoyalConfig.stripe_image_smartly || frontRoyalConfig.stripe_image, // FIXME: https://trello.com/c/X5DWN7Xk
                                locale: 'auto',
                            };

                            // load stripe if necessary, then configure
                            return RouteAssetLoader.loadStripeCheckoutDependencies().then(() => {
                                StripeCheckoutHelper.handler = $window.StripeCheckout.configure(options);
                                return StripeCheckoutHelper.handler;
                            });
                        });

                        // Close checkout modal on page navigation, e.g.: if hitting back button
                        $window.addEventListener('popstate', () => {
                            if (StripeCheckoutHelper.handler) {
                                StripeCheckoutHelper.handler.close();
                            }
                        });
                    }

                    return StripeCheckoutHelper.configurePromise;
                },

                open(options) {
                    if (!StripeCheckoutHelper.handler) {
                        throw new Error('You must configure Stripe Checkout before using it!');
                    }

                    // See https://stripe.com/docs/checkout#integration-custom
                    //
                    // "You can prevent Checkout's popup from being blocked by calling handler.open when
                    //  the user clicks on an element on the page. Do not call handler.open in a callback.
                    //  This design indicates to the browser that the user is explicitly requesting the popup.
                    //  Otherwise, mobile devices and some versions of Internet Explorer will block the popup
                    //  and prevent users from checking out"
                    StripeCheckoutHelper.handler.open(options);
                },
            });

            return {
                initialize(owner) {
                    this.owner = owner;
                    if (!this.owner) {
                        throw new Error('No owner');
                    }
                    this.stripeProcessing = false;
                    StripeCheckoutHelper.configure(this.owner); // eagerly configure
                },

                //---------------------------------------------------------------
                // More specific methods that do the work to handle EMBA registration and hiring plan interactions
                //---------------------------------------------------------------
                modifyPaymentDetails(options = {}) {
                    // See https://stripe.com/docs/recipes/updating-customer-cards for example
                    const name = options.name;
                    const panelLabel = translationHelper.get('stripe_panel_label_update_card');

                    if (!name) {
                        throw new Error('Need more arguments for modifyPaymentDetails');
                    }

                    return this._open({
                        name,
                        panelLabel,
                    }).then(token => {
                        this.stripeProcessing = true;
                        return $http
                            .put(
                                `${$window.ENDPOINT_ROOT}/api/subscriptions/modify_payment_details.json`,
                                {
                                    owner_id: this.owner.id,
                                    source: token && token.id,
                                },
                                {
                                    'FrontRoyal.ApiErrorHandler': {
                                        skip: true,
                                    },
                                },
                            )
                            .catch(response => {
                                // Bring back the UI behind the modal.  If we do not do this, and the user closes
                                // the stripe modal, then we will be stuck with a perpetual spinner.
                                this._setDoneProcessing();
                                return this._handleErrorResponse(response, 'modifyPaymentDetails', options);
                            })
                            .finally(() => this._setDoneProcessing());
                    });
                },

                getTokenIfNeededAndCreateSubscription(options) {
                    const self = this;

                    // stripe_plan_id and owner_id should be defined in the attrs
                    const attrs = options.attrs || {};
                    const coupon = options.coupon;
                    const requiresCardInfo = options.requiresCardInfo;
                    const showNoCardConfirm = options.showNoCardConfirm;
                    const metaForSaveCall = options.metaForSaveCall || {};
                    const checkoutModalOptions = options.checkoutModalOptions; // only needed if requiresCardInfo=true

                    if (requiresCardInfo && !checkoutModalOptions) {
                        throw new Error('Not enough arguments for launching stripe checkout.');
                    }

                    // if we require card payment, show the Checkout UI
                    // if we otherwise have requested the confirmation dialog, show that
                    let promise = $q.when();
                    if (requiresCardInfo) {
                        promise = this._open(checkoutModalOptions);
                    } else if (showNoCardConfirm) {
                        promise = $q((resolve, reject) => {
                            DialogModal.confirm({
                                text: translationHelper.get('are_you_sure_switch_plans'),
                                confirmCallback() {
                                    resolve();
                                },
                                cancelCallback() {
                                    reject();
                                },
                            });
                        });
                    }

                    return promise.then(token => {
                        const couponId = coupon && coupon.id;
                        if (couponId) {
                            metaForSaveCall.coupon_id = couponId;
                        }
                        if (token) {
                            metaForSaveCall.source = token.id;
                        }

                        return self
                            ._createSubscription(attrs, metaForSaveCall)
                            .catch(response =>
                                this._handleErrorResponse(response, 'getTokenIfNeededAndCreateSubscription', options),
                            );
                    });
                },

                //---------------------------------------------------------------
                // Private methods
                //---------------------------------------------------------------

                _createSubscription(attrs, meta = {}) {
                    this.stripeProcessing = true;
                    safeApply($rootScope);

                    // We don't have to explicitly set the subscription on the user,
                    // because it is sent down in the metadata and handled by the push message
                    // interceptor
                    return Subscription.create(attrs, meta, {
                        'FrontRoyal.ApiErrorHandler': {
                            skip: true,
                        },
                    }).finally(() => this._setDoneProcessing());
                },

                _open(options) {
                    // options that should be passed in:
                    // name, description, amount, panelLabel

                    options = _.extend(
                        {
                            email: this.owner.email,
                            allowRememberMe: false,
                            closed: null, // just leaving this here in case we have need for it in the future
                        },
                        options,
                    );

                    return $q((resolve, reject) => {
                        options.token = resolve; // this is the callback that StripeCheckoutHelper will trigger when the modal is closed

                        // Even though the StripeCheckoutHelper has already been configured at this point, we want to force it
                        // to reconfigure itself since the user's program could have changed from a Quantic program to a Smartly
                        // program, which means that the branding on the modal may need to be updated.
                        StripeCheckoutHelper.configure(this.owner, true)
                            .then(() => StripeCheckoutHelper.open(options))
                            .catch(reject);
                    });
                },

                _setDoneProcessing() {
                    this.stripeProcessing = false;
                    safeApply($rootScope);
                },

                _handleErrorResponse(response, retryMeth, options) {
                    if (response.status === 402) {
                        // FIXME: if someone were paying attention to the response here,
                        // we might want to wait until the modal was dismissed before rejecting
                        // this promise.  See HiringRelationshipViewModel#save for an example
                        // of that.

                        // Sometimes we send down a translation key for a generic card error.  Sometimes
                        // we pass through a specific error message from stripe (at least, the server-side code
                        // supports this.  I'm not sure this ever actually happens anymore, though.)
                        const message = response.data.message_key
                            ? translationHelper.get(response.data.message_key)
                            : response.data.message;
                        let message2;
                        let shouldOpenCheckoutModal = false;

                        // If the current user cannot edit their payment info, tell them
                        // to contact the owner
                        if (!this.canEditPaymentInfo) {
                            message2 = translationHelper.get('please_contact_your_owner', {
                                ownerEmail: this.ownerEmail,
                            });
                        } else {
                            message2 = response.data.has_default_source
                                ? translationHelper.get('please_try_another_card')
                                : '';

                            // After a card failure, open up the checkout modal so the user
                            // has a chance to add a new card
                            shouldOpenCheckoutModal = true;
                        }

                        return $q((resolve, reject) => {
                            DialogModal.alert({
                                content: `<p>${message}</p><p>${message2}</p>`,
                                classes: ['server-error-modal', 'small'],
                                title: translationHelper.get('payment_failed_title'),
                                close: () => {
                                    if (shouldOpenCheckoutModal) {
                                        // retryMeth is either modifyPaymentDetails or getTokenIfNeededAndCreateSubscription.
                                        // If getTokenIfNeededAndCreateSubscription, then we always want to set requiresCardInfo=true
                                        // so that we popup the stripe checkout helper after a card error. (We also set it in the
                                        // modifyPaymentDetails case, but it is ignored thete)
                                        this[retryMeth]({
                                            ...options,
                                            requiresCardInfo: true,
                                        }).then(resolve, reject);
                                    } else {
                                        reject();
                                    }
                                },
                            });
                            HttpQueue.unfreezeAfterError(response.config);
                        });
                    }

                    if (response.config) {
                        response.config['FrontRoyal.ApiErrorHandler'].skip = false;
                    }

                    return ApiErrorHandler.onResponseError(response);
                },
            };
        });

        return StripeCheckoutHelper;
    },
]);
