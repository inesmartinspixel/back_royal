import angularModule from 'Payments/angularModule/scripts/payments_module';

angularModule.factory('Coupon', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');

        return Iguana.subclass(function () {
            this.setCollection('coupons');
            this.alias('Coupon');
        });
    },
]);
