import angularModule from 'Payments/angularModule/scripts/payments_module';
import { getBrandName } from 'AppBrandMixin';

angularModule.factory('Payments.CohortRegistrationCheckoutHelper', [
    '$injector',

    $injector => {
        const StripeCheckoutHelper = $injector.get('Payments.StripeCheckoutHelper');
        const EventLogger = $injector.get('EventLogger');
        const $rootScope = $injector.get('$rootScope');
        const TranslationHelper = $injector.get('TranslationHelper');
        // translationHelper = new TranslationHelper('payments.stripe_checkout_helper');

        const CohortRegistrationCheckoutHelper = StripeCheckoutHelper.subclass(function () {
            Object.defineProperty(this.prototype, 'currentUser', {
                get() {
                    // owner is set in the superclass.  Aliasing here for readability
                    return this.owner;
                },
            });

            Object.defineProperty(this.prototype, 'canEditPaymentInfo', {
                get() {
                    // all students are allowed to edit their own payment info
                    return true;
                },
                configurable: true,
            });

            return {
                initialize($super) {
                    $super($rootScope.currentUser);
                },

                registerForCohort(stripePlanId, coupon) {
                    const self = this;

                    const currentUser = this.currentUser;
                    const cohortApplication = currentUser && currentUser.lastCohortApplication;
                    if (!currentUser || !cohortApplication || cohortApplication.registered) {
                        return;
                    }

                    EventLogger.allowEmptyLabel('cohort:register-start');
                    EventLogger.log('cohort:register-start', {
                        cohort_id: cohortApplication.cohort_id,
                    });

                    const stripePlan = cohortApplication.getPlanForId(stripePlanId);
                    const translationHelper = new TranslationHelper('settings.tuition_and_registration');

                    self.getTokenIfNeededAndCreateSubscription({
                        metaForSaveCall: {
                            cohort_application: _.pick(cohortApplication.asJson(), 'id', 'shareable_with_classmates'),
                        },

                        // don't use Stripe at all if there's no tuition. we call into the same endpoint, but it does not actually
                        // create a Stripe subscription. It will just validate the application state and update registration.
                        requiresCardInfo:
                            !cohortApplication.hasFullScholarship &&
                            cohortApplication.total_num_required_stripe_payments !== 0,

                        // the following stuff is all ignored if requiresCardInfo is false
                        attrs: {
                            stripe_plan_id: stripePlanId,
                            user_id: currentUser.id,
                        },
                        coupon,
                        checkoutModalOptions: stripePlanId && {
                            name: currentUser.relevant_cohort.programTitle,
                            description: this._descriptionForStripeModal(stripePlan),
                            amount: cohortApplication.getPlanPaymentPerIntervalForDisplay(stripePlan, coupon) * 100,
                            panelLabel: translationHelper.get('stripe_panel_label_pay_tuition'), // TODO: is it still ok to call this tuition in paid cert case?
                        },
                    });
                },

                modifyPaymentDetails($super) {
                    const translationHelper = new TranslationHelper('settings.tuition_and_registration');
                    return $super({
                        name: translationHelper.get('stripe_name', {
                            brandName: getBrandName(this.currentUser),
                        }),
                    });
                },

                _descriptionForStripeModal(stripePlan) {
                    const translationHelper = new TranslationHelper('settings.tuition_and_registration');

                    let descriptionText = '';
                    if (stripePlan.frequency === 'monthly') {
                        descriptionText = translationHelper.get('stripe_description_monthly_tuition');
                    } else if (stripePlan.frequency === 'bi_annual') {
                        descriptionText = translationHelper.get('stripe_description_biannual_tuition');
                    } else if (stripePlan.frequency === 'once') {
                        descriptionText = translationHelper.get('stripe_description_once_tuition');
                    } else {
                        $injector
                            .get('ErrorLogService')
                            .notify(
                                `Encountered a stripe plan with an unknown frequency: ${JSON.stringify(stripePlan)}`,
                            );
                    }

                    return descriptionText;
                },
            };
        });

        return CohortRegistrationCheckoutHelper;
    },
]);
