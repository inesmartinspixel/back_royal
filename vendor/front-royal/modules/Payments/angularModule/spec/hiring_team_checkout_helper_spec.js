import 'AngularSpecHelper';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';
import 'Payments/angularModule';
import hiringTeamCheckoutHelperLocales from 'Payments/locales/payments/hiring_team_checkout_helper-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(hiringTeamCheckoutHelperLocales);

describe('FrontRoyal.Payments.HiringTeamCheckoutHelper', () => {
    let $injector;
    let SpecHelper;
    let HiringTeamCheckoutHelper;
    let HiringTeam;
    let instance;
    let hiringTeam;
    let currentUser;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Payments', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                HiringTeamCheckoutHelper = $injector.get('Payments.HiringTeamCheckoutHelper');
                HiringTeam = $injector.get('HiringTeam');
                $injector.get('HiringTeamFixtures');
                currentUser = SpecHelper.stubCurrentUser();
                SpecHelper.stubConfig();
                SpecHelper.stubStripeCheckout();

                setupInstance(null);
            },
        ]);
    });

    function setupInstance(hiringPlan) {
        hiringTeam = HiringTeam.fixtures.getInstance({
            hiring_plan: hiringPlan,
        });
        currentUser.hiring_team = hiringTeam;
        instance = new HiringTeamCheckoutHelper(currentUser.hiring_team);
    }

    describe('subscribeToPayPerPostPlan', () => {
        let plan;
        let ownsHiringTeamThatSeemsToHavePaymentSource;

        beforeEach(() => {
            plan = _.detect(
                hiringTeam.stripe_plans,
                plan => plan.product_metadata.available_for_hiring_plan === HiringTeam.HIRING_PLAN_PAY_PER_POST,
            );
            ownsHiringTeamThatSeemsToHavePaymentSource = true;
            jest.spyOn(currentUser, 'ownsHiringTeamThatSeemsToHavePaymentSource', 'get').mockImplementation(
                () => ownsHiringTeamThatSeemsToHavePaymentSource,
            );
        });

        it('should work', () => {
            const openPositionId = '1234';
            jest.spyOn(hiringTeam, 'primarySubscription', 'get').mockReturnValue(null);

            jest.spyOn(instance, 'getTokenIfNeededAndCreateSubscription').mockImplementation(() => {});
            instance.subscribeToPayPerPostPlan(plan.id, openPositionId);

            expect(instance.getTokenIfNeededAndCreateSubscription).toHaveBeenCalledWith({
                metaForSaveCall: {
                    open_position_id: openPositionId,
                },
                requiresCardInfo: false,
                showNoCardConfirm: false,
                attrs: {
                    stripe_plan_id: plan.id,
                    hiring_team_id: hiringTeam.id,
                },
                checkoutModalOptions: {
                    name: plan.product_name,
                    description: 'Pay per Post - Monthly',
                    amount: 2000,
                    panelLabel: 'Publish',
                },
            });
        });

        it('should set requiresCardInfo=false if ownsHiringTeamThatSeemsToHavePaymentSource', () => {
            ownsHiringTeamThatSeemsToHavePaymentSource = true;
            hiringTeam.hiring_plan = 'pay_per_post';
            jest.spyOn(instance, 'canEditPaymentInfo', 'get').mockReturnValue(true);
            jest.spyOn(instance, '_subscribe').mockImplementation(() => {});
            instance.subscribeToPayPerPostPlan(plan.id, 1234);
            expect(instance._subscribe.mock.calls[0][1].requiresCardInfo).toBe(false);
        });

        it('should set requiresCardInfo=false if not ownsHiringTeamThatSeemsToHavePaymentSource', () => {
            ownsHiringTeamThatSeemsToHavePaymentSource = false;
            jest.spyOn(instance, 'canEditPaymentInfo', 'get').mockReturnValue(false);
            jest.spyOn(instance, '_subscribe').mockImplementation(() => {});
            instance.subscribeToPayPerPostPlan(plan.id, 1234);
            expect(instance._subscribe.mock.calls[0][1].requiresCardInfo).toBe(false);
        });
    });

    describe('subscribeToUnlimitedWithSourcingPlan', () => {
        let plan;

        beforeEach(() => {
            plan = _.detect(
                hiringTeam.stripe_plans,
                plan =>
                    plan.product_metadata.available_for_hiring_plan === HiringTeam.HIRING_PLAN_UNLIMITED_WITH_SOURCING,
            );
        });

        it('should work', () => {
            const openPositionId = '1234';
            jest.spyOn(hiringTeam, 'primarySubscription', 'get').mockReturnValue(null);

            jest.spyOn(instance, 'getTokenIfNeededAndCreateSubscription').mockImplementation(() => {});
            instance.subscribeToPayPerPostPlan(plan.id, openPositionId);

            expect(instance.getTokenIfNeededAndCreateSubscription).toHaveBeenCalledWith({
                metaForSaveCall: {
                    open_position_id: openPositionId,
                },
                requiresCardInfo: false,
                showNoCardConfirm: false,
                attrs: {
                    stripe_plan_id: plan.id,
                    hiring_team_id: hiringTeam.id,
                },
                checkoutModalOptions: {
                    name: plan.product_name,
                    description: 'Post + Source Unlimited',
                    amount: 24900,
                    panelLabel: 'Subscribe - ',
                },
            });
        });

        it('should set requiresCardInfo=true if not hiringTeamSeemsToHavePaymentSource', () => {
            jest.spyOn(currentUser, 'hiringTeamSeemsToHavePaymentSource', 'get').mockReturnValue(false);
            jest.spyOn(instance, '_subscribe');
            instance.subscribeToUnlimitedWithSourcingPlan();
            expect(instance._subscribe).toHaveBeenCalledWith(plan.id, {
                requiresCardInfo: true,
                showNoCardConfirm: true,
            });
        });
    });

    describe('subscribeToLegacyPlan', () => {
        beforeEach(() => {
            setupInstance(HiringTeam.HIRING_PLAN_LEGACY);
        });

        it('should work with metered plan', () => {
            const plan = _.findWhere(hiringTeam.stripe_plans, {
                usage_type: 'metered',
            });
            jest.spyOn(hiringTeam, 'primarySubscription', 'get').mockReturnValue(null);

            jest.spyOn(instance, 'getTokenIfNeededAndCreateSubscription').mockImplementation(() => {});
            instance.subscribeToLegacyPlan(plan.id);

            expect(instance.getTokenIfNeededAndCreateSubscription).toHaveBeenCalledWith({
                metaForSaveCall: {
                    cancel_current_subscription: false,
                },
                requiresCardInfo: true,
                showNoCardConfirm: true,
                attrs: {
                    stripe_plan_id: plan.id,
                    hiring_team_id: hiringTeam.id,
                },
                checkoutModalOptions: {
                    name: plan.product_name,
                    description: 'Pro plan – Pay per match',
                    amount: 0,
                    panelLabel: 'Subscribe',
                },
            });
        });

        it('should work with unlimited plan', () => {
            const plan = _.findWhere(hiringTeam.stripe_plans, {
                usage_type: 'licensed',
            });
            jest.spyOn(hiringTeam, 'primarySubscription', 'get').mockReturnValue(null);

            jest.spyOn(instance, 'getTokenIfNeededAndCreateSubscription').mockImplementation(() => {});
            instance.subscribeToLegacyPlan(plan.id);

            expect(instance.getTokenIfNeededAndCreateSubscription).toHaveBeenCalledWith({
                metaForSaveCall: {
                    cancel_current_subscription: false,
                },
                requiresCardInfo: true,
                showNoCardConfirm: true,
                attrs: {
                    stripe_plan_id: plan.id,
                    hiring_team_id: hiringTeam.id,
                },
                checkoutModalOptions: {
                    name: plan.product_name,
                    description: 'Post + Source Unlimited',
                    amount: plan.amount,
                    panelLabel: 'Subscribe - ',
                },
            });
        });

        it('should error if already subscribed to this plan', () => {
            const plan = _.findWhere(hiringTeam.stripe_plans, {
                usage_type: 'metered',
            });
            jest.spyOn(hiringTeam, 'primarySubscription', 'get').mockReturnValue({
                stripe_plan_id: plan.id,
            });

            expect(() => {
                instance.subscribeToLegacyPlan(plan.id);
            }).toThrow(new Error('Hiring Team already has a subscription for this plan.'));
        });

        it('should cancel current subscription if changing plans', () => {
            jest.spyOn(hiringTeam, 'primarySubscription', 'get').mockReturnValue({
                stripe_plan_id: hiringTeam.stripe_plans[0].id,
            });

            jest.spyOn(instance, 'getTokenIfNeededAndCreateSubscription').mockImplementation(() => {});
            instance.subscribeToLegacyPlan(hiringTeam.stripe_plans[1].id);

            expect(instance.getTokenIfNeededAndCreateSubscription.mock.calls[0][0].metaForSaveCall).toEqual({
                cancel_current_subscription: true,
            });
        });
    });

    describe('modifyPaymentDetails', () => {
        it('should work', () => {
            const superclass = $injector.get('Payments.StripeCheckoutHelper');
            jest.spyOn(superclass.prototype, 'modifyPaymentDetails').mockImplementation(() => {});

            const plan = hiringTeam.stripe_plans[0];
            jest.spyOn(hiringTeam, 'primarySubscription', 'get').mockReturnValue({
                stripe_plan_id: plan.id,
            });
            instance.modifyPaymentDetails();

            expect(superclass.prototype.modifyPaymentDetails).toHaveBeenCalledWith({
                name: 'Smartly Talent',
            });
        });
    });
});
