import 'AngularSpecHelper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import 'Payments/angularModule';
import tuitionAndRegistrationLocales from 'Settings/locales/settings/tuition_and_registration-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(tuitionAndRegistrationLocales);

describe('FrontRoyal.Payments.CohortRegistrationCheckoutHelper', () => {
    let $injector;
    let SpecHelper;
    let instance;
    let promoted;
    let Cohort;
    let CohortApplication;
    let currentUser;
    let CohortRegistrationCheckoutHelper;
    let EventLogger;
    let planPaymentPerInterval;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Payments', 'SpecHelper', 'NoUnhandledRejectionExceptions');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                Cohort = $injector.get('Cohort');
                $injector.get('CohortFixtures');
                CohortRegistrationCheckoutHelper = $injector.get('Payments.CohortRegistrationCheckoutHelper');
                CohortApplication = $injector.get('CohortApplication');
                EventLogger = $injector.get('EventLogger');
                const StripeCheckoutHelper = $injector.get('Payments.StripeCheckoutHelper');
                const guid = $injector.get('guid');

                const cohort = Cohort.fixtures.getInstance({
                    program_type: 'emba',
                    id: 123,
                    registration_deadline_days_offset: -4,
                    start_date: new Date('2017/07/01').getTime() / 1000,
                });

                promoted = CohortApplication.new({
                    id: guid.generate(),
                    status: 'pre_accepted',
                    program_type: 'emba',
                    cohort_id: cohort.id,
                    applied_at: new Date().getTime() / 1000,
                    cohort,
                    cohort_title: cohort.title,
                    stripe_plans: [
                        {
                            id: 'monthly_plan',
                            frequency: 'monthly',
                            amount: 80000,
                        },
                    ],
                    scholarship_level: {
                        name: 'No Scholarship',
                        coupons: {
                            monthly_plan: {
                                id: 'none',
                                amount_off: 0,
                                percent_off: undefined,
                            },
                        },
                        standard: {
                            monthly_plan: {
                                id: 'none',
                                amount_off: 0,
                                percent_off: undefined,
                            },
                        },
                        early: {
                            monthly_plan: {
                                id: 'none',
                                amount_off: 0,
                                percent_off: undefined,
                            },
                        },
                    },
                });
                currentUser = SpecHelper.stubCurrentUser('learner');
                currentUser.relevant_cohort = promoted.cohort;
                currentUser.cohort_applications.push(promoted);

                planPaymentPerInterval = 250;
                jest.spyOn(promoted, 'getPlanPaymentPerIntervalForDisplay').mockReturnValue(planPaymentPerInterval);
                jest.spyOn(promoted, 'getNumIntervalsForPlan').mockReturnValue(12);
                jest.spyOn(StripeCheckoutHelper, 'configure').mockImplementation(() => {});
                jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});

                SpecHelper.stubConfig();
                SpecHelper.stubStripeCheckout();
            },
        ]);
    });

    describe('registerForCohort', () => {
        let stripePlanId;
        let coupon;
        let cohortApplication;

        beforeEach(() => {
            instance = new CohortRegistrationCheckoutHelper();
            stripePlanId = 'monthly_plan';
            coupon = {
                id: 'myCoupon',
            };
            jest.spyOn(instance, 'getTokenIfNeededAndCreateSubscription').mockImplementation(() => {});
            cohortApplication = currentUser.lastCohortApplication;
        });

        it('should set attrs and coupon', () => {
            const options = registerForCohortAndGetOptions();
            expect(options.attrs).toEqual({
                stripe_plan_id: stripePlanId,
                user_id: currentUser.id,
            });
            expect(options.coupon).toEqual(coupon);
        });

        it('should set checkoutModalOptions', () => {
            jest.spyOn(instance, '_descriptionForStripeModal').mockReturnValue('descriptionText');

            const options = registerForCohortAndGetOptions();

            const stripePlan = cohortApplication.getPlanForId(stripePlanId);
            const checkoutModalOptions = options.checkoutModalOptions;
            expect(instance._descriptionForStripeModal).toHaveBeenCalledWith(stripePlan);
            expect(cohortApplication.getPlanPaymentPerIntervalForDisplay).toHaveBeenCalledWith(stripePlan, coupon);
            expect(checkoutModalOptions.panelLabel).toEqual('Pay Tuition – ');
            expect(checkoutModalOptions.description).toEqual('descriptionText');
            expect(checkoutModalOptions.name).toEqual(currentUser.relevant_cohort.programTitle);
            expect(checkoutModalOptions.amount).toEqual(planPaymentPerInterval * 100);
        });

        it('should set shareable_with_classmates', () => {
            cohortApplication.shareable_with_classmates = 'shareable_with_classmates';
            const options = registerForCohortAndGetOptions();
            expect(options.requiresCardInfo).toBe(true);
            expect(options.metaForSaveCall).toEqual({
                cohort_application: {
                    id: cohortApplication.id,
                    shareable_with_classmates: cohortApplication.shareable_with_classmates,
                },
            });
        });

        it('should set requiresCardInfo to true if total_num_required_stripe_payments is null', () => {
            jest.spyOn(promoted, 'hasFullScholarship', 'get').mockReturnValue(false);
            cohortApplication.total_num_required_stripe_payments = null;

            const options = registerForCohortAndGetOptions();
            expect(options.requiresCardInfo).toBe(true);
        });

        it('should set requiresCardInfo to true if total_num_required_stripe_payments > 0', () => {
            jest.spyOn(promoted, 'hasFullScholarship', 'get').mockReturnValue(false);
            cohortApplication.total_num_required_stripe_payments = 12;

            const options = registerForCohortAndGetOptions();
            expect(options.requiresCardInfo).toBe(true);
        });

        it('should set requiresCardInfo to false if there is a full scholarship', () => {
            jest.spyOn(promoted, 'hasFullScholarship', 'get').mockReturnValue(true);
            cohortApplication.total_num_required_stripe_payments = null;

            const options = registerForCohortAndGetOptions();
            expect(options.requiresCardInfo).toBe(false);
        });

        it('should set requiresCardInfo to false if there are no required stripe payments', () => {
            jest.spyOn(promoted, 'hasFullScholarship', 'get').mockReturnValue(false);
            cohortApplication.total_num_required_stripe_payments = 0;

            const options = registerForCohortAndGetOptions();
            expect(options.requiresCardInfo).toBe(false);
        });

        it('should log events', () => {
            instance.registerForCohort(stripePlanId, coupon);
            expect(EventLogger.prototype.log).toHaveBeenCalledWith('cohort:register-start', {
                cohort_id: cohortApplication.cohort_id,
            });
        });

        it('should never allow re-registration', () => {
            cohortApplication.registered = true;
            instance.registerForCohort(stripePlanId, coupon);
            expect(instance.getTokenIfNeededAndCreateSubscription).not.toHaveBeenCalled();
            expect(EventLogger.prototype.log).not.toHaveBeenCalled();
        });

        function registerForCohortAndGetOptions() {
            instance.registerForCohort(stripePlanId, coupon);
            expect(instance.getTokenIfNeededAndCreateSubscription).toHaveBeenCalled();
            const options = instance.getTokenIfNeededAndCreateSubscription.mock.calls[0][0];
            return options;
        }
    });

    describe('modifyPaymentDetails', () => {
        it('should set name and panelLabel', () => {
            const StripeCheckoutHelper = $injector.get('Payments.StripeCheckoutHelper');
            jest.spyOn(StripeCheckoutHelper.prototype, 'modifyPaymentDetails').mockImplementation(() => {});
            instance = new CohortRegistrationCheckoutHelper();
            instance.modifyPaymentDetails();
            expect(StripeCheckoutHelper.prototype.modifyPaymentDetails).toHaveBeenCalledWith({
                name: 'Quantic Executive MBA',
            });
        });
    });

    describe('_descriptionForStripeModal', () => {
        it('should work', () => {
            instance = new CohortRegistrationCheckoutHelper();
            const ErrorLogService = $injector.get('ErrorLogService');
            jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
            expect(
                instance._descriptionForStripeModal({
                    frequency: 'monthly',
                }),
            ).toEqual('Monthly Tuition Plan');

            expect(
                instance._descriptionForStripeModal({
                    frequency: 'bi_annual',
                }),
            ).toEqual('Bi-annual Tuition Plan');

            expect(
                instance._descriptionForStripeModal({
                    frequency: 'once',
                }),
            ).toEqual('Single Payment');
            expect(ErrorLogService.notify).not.toHaveBeenCalled();

            expect(
                instance._descriptionForStripeModal({
                    frequency: 'unexpected',
                }),
            ).toEqual('');
            expect(ErrorLogService.notify).toHaveBeenCalledWith(
                'Encountered a stripe plan with an unknown frequency: {"frequency":"unexpected"}',
            );
        });
    });
});
