import 'AngularSpecHelper';
import 'Payments/angularModule';
import stripeCheckoutHelperLocales from 'Payments/locales/payments/stripe_checkout_helper-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(stripeCheckoutHelperLocales);

describe('FrontRoyal.Payments.StripeCheckoutHelper', () => {
    let $injector;
    let SpecHelper;
    let $window;
    let $rootScope;
    let $q;
    let StripeCheckoutHelper;
    let Subscription;
    let instance;
    let $timeout;
    let owner;
    let DialogModal;
    let $http;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Payments', 'SpecHelper', 'NoUnhandledRejectionExceptions');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $window = $injector.get('$window');
                $rootScope = $injector.get('$rootScope');
                $q = $injector.get('$q');
                StripeCheckoutHelper = $injector.get('Payments.StripeCheckoutHelper');
                Subscription = $injector.get('Subscription');
                $timeout = $injector.get('$timeout');
                DialogModal = $injector.get('DialogModal');
                $http = $injector.get('$http');

                SpecHelper.stubConfig({
                    stripe_publishable_key: 'test_key',
                    stripe_image_quantic: 'test_image_quantic',
                });
                SpecHelper.stubStripeCheckout();

                // reset the cached data on stripe checkout helper so it always runs from scratch
                StripeCheckoutHelper.configurePromise = null;
                StripeCheckoutHelper.handler = null;
                owner = SpecHelper.stubCurrentUser();
            },
        ]);
    });

    describe('configure', () => {
        it('should configure properly', done => {
            jest.spyOn($window.StripeCheckout, 'configure');

            StripeCheckoutHelper.configure(owner).then(() => {
                expect($window.StripeCheckout.configure).toHaveBeenCalledWith({
                    key: 'test_key',
                    image: 'test_image_quantic',
                    locale: 'auto',
                });

                expect(StripeCheckoutHelper.handler).toBeDefined();
                done();
            });

            $rootScope.$digest();
        });

        it('should prevent open if not configured first', () => {
            expect(() => {
                StripeCheckoutHelper.open();
            }).toThrow(new Error('You must configure Stripe Checkout before using it!'));
        });

        it('should call open successfully if configured', done => {
            jest.spyOn(StripeCheckoutHelper, 'open').mockImplementation(() => {});
            StripeCheckoutHelper.configure(owner).then(() => {
                StripeCheckoutHelper.open();
                done();
            });

            $rootScope.$digest();
            expect(StripeCheckoutHelper.open).toHaveBeenCalled();
        });
    });

    describe('modifyPaymentDetails', () => {
        let openDeferred;
        let putDeferred;
        let options;

        beforeEach(() => {
            instance = new StripeCheckoutHelper(owner);
            setup();
        });

        function setup() {
            options = {
                name: 'name',
                panelLabel: 'panelLabel',
            };
            openDeferred = $q.defer();
            putDeferred = $q.defer();

            jest.spyOn(instance, '_open').mockReturnValue(openDeferred.promise);
            jest.spyOn($http, 'put').mockReturnValue(putDeferred.promise);
            jest.spyOn(instance, '_setDoneProcessing');

            instance.modifyPaymentDetails(options);
            openDeferred.resolve({
                id: 'tokenId',
            });
            $timeout.flush();
        }

        it('should hit modify_payment_details endpoint', () => {
            expect(instance.stripeProcessing).toBe(true);
            expect($http.put).toHaveBeenCalledWith(
                `${$window.ENDPOINT_ROOT}/api/subscriptions/modify_payment_details.json`,
                {
                    source: 'tokenId',
                    owner_id: owner.id,
                },
                {
                    'FrontRoyal.ApiErrorHandler': {
                        skip: true,
                    },
                },
            );

            putDeferred.resolve({});
            $timeout.flush();
            expect(instance._setDoneProcessing).toBeCalled();
        });

        it('should handle error from our server', () => {
            jest.spyOn(instance, '_handleErrorResponse');
            putDeferred.reject({
                foo: 'bar',
            });
            $timeout.flush();

            expect(instance._handleErrorResponse).toBeCalledWith(
                {
                    foo: 'bar',
                },
                'modifyPaymentDetails',
                options,
            );
        });
    });

    describe('_createSubscription', () => {
        beforeEach(() => {
            instance = new StripeCheckoutHelper(owner);
        });

        it('should create the subscription', () => {
            const attrs = {
                some: 'attrs',
            };
            const meta = {
                some: 'meta',
            };
            const subscription = Subscription.new(attrs);
            Subscription.expect('create').toBeCalledWith(subscription, meta, {
                'FrontRoyal.ApiErrorHandler': {
                    skip: true,
                },
            });

            instance._createSubscription(attrs, meta).then(angular.noop, () => {
                throw new Error('promise should not have rejected');
            });

            expect(instance.stripeProcessing).toBe(true);
            // flush the update
            Subscription.flush('create');
            expect(instance.stripeProcessing).toBe(false);
        });
    });

    describe('_open', () => {
        it('should open the stripe modal and resolve with the token', () => {
            instance = new StripeCheckoutHelper(owner);

            // spy on the class method
            jest.spyOn(StripeCheckoutHelper, 'open').mockImplementation(options => {
                options.token({
                    id: 'tokenId',
                });
            });

            const options = {
                name: 'name',
                description: 'description',
                amount: 'amount',
                panelLabel: 'panelLabel',
            };
            let token = false;
            instance._open(options).then(_token => {
                token = _token;
            });

            $timeout.flush(); // flush the configure call

            expect(StripeCheckoutHelper.open).toHaveBeenCalled();
            const optionsPassedThrough = StripeCheckoutHelper.open.mock.calls[0][0];
            _.each(options, (val, key) => {
                expect(optionsPassedThrough[key]).toEqual(val);
            });
            expect(optionsPassedThrough.email).toEqual(owner.email);
            expect(optionsPassedThrough.allowRememberMe).toEqual(false);

            $timeout.flush();
            expect(token.id).toEqual('tokenId');
        });
    });

    describe('getTokenIfNeededAndCreateSubscription', () => {
        it('should work with requiresCardInfo=true', () => {
            instance = new StripeCheckoutHelper(owner);
            const checkoutModalOptions = {
                some: 'options',
            };
            const metaForSaveCall = {
                some: 'meta',
            };
            const attrs = {
                some: 'attrs',
            };

            jest.spyOn(instance, '_open').mockReturnValue(
                $q.when({
                    id: 'tokenId',
                }),
            );
            jest.spyOn(instance, '_createSubscription').mockImplementation(() => {});

            const options = {
                requiresCardInfo: true,
                attrs,
                coupon: {
                    id: 'myCoupon',
                },
                metaForSaveCall,
                checkoutModalOptions,
            };
            instance.getTokenIfNeededAndCreateSubscription(options);

            expect(instance._open).toHaveBeenCalledWith(checkoutModalOptions);
            $timeout.flush();
            expect(instance._createSubscription).toHaveBeenCalledWith(
                attrs,
                _.extend(metaForSaveCall, {
                    coupon_id: 'myCoupon',
                    source: 'tokenId',
                }),
            );
        });

        it('should work with requiresCardInfo=false', () => {
            instance = new StripeCheckoutHelper(owner);
            const metaForSaveCall = {
                some: 'meta',
            };

            jest.spyOn(instance, '_open').mockImplementation(() => {});
            jest.spyOn(instance, '_createSubscription').mockImplementation(() => {});

            const options = {
                requiresCardInfo: false,
                metaForSaveCall,
            };
            instance.getTokenIfNeededAndCreateSubscription(options);

            expect(instance._open).not.toHaveBeenCalled();
            $timeout.flush();
            expect(instance._createSubscription).toHaveBeenCalledWith({}, _.extend(metaForSaveCall));
        });

        it('should work with requiresCardInfo=false and showNoCardConfirm=true', () => {
            SpecHelper.stubEventLogging();
            instance = new StripeCheckoutHelper(owner);

            const metaForSaveCall = {
                some: 'meta',
            };

            jest.spyOn(instance, '_open').mockImplementation(() => {});
            jest.spyOn(instance, '_createSubscription').mockImplementation(() => {});
            jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {});

            const options = {
                requiresCardInfo: false,
                showNoCardConfirm: true,
                metaForSaveCall,
            };
            instance.getTokenIfNeededAndCreateSubscription(options);

            expect(instance._open).not.toHaveBeenCalled();
            $timeout.flush();

            expect(instance._open).not.toHaveBeenCalled();
            expect(DialogModal.confirm).toHaveBeenCalled();

            const callbackFunction = DialogModal.confirm.mock.calls[0][0].confirmCallback;
            expect(callbackFunction).toBeDefined();

            callbackFunction.apply();
            $timeout.flush();

            expect(instance._createSubscription).toHaveBeenCalledWith({}, _.extend(metaForSaveCall));
        });

        it('should handle errors from _createSubscription', () => {
            instance = new StripeCheckoutHelper(owner);
            const metaForSaveCall = {
                some: 'meta',
            };

            jest.spyOn(instance, '_createSubscription').mockReturnValue(
                $q.reject({
                    some: 'response',
                }),
            );
            jest.spyOn(instance, '_handleErrorResponse').mockImplementation(() => {});

            const options = {
                metaForSaveCall,
            };
            instance.getTokenIfNeededAndCreateSubscription(options);
            $timeout.flush();
            expect(instance._handleErrorResponse).toHaveBeenCalledWith(
                {
                    some: 'response',
                },
                'getTokenIfNeededAndCreateSubscription',
                {
                    metaForSaveCall: {
                        some: 'meta',
                    },
                },
            );
        });
    });

    describe('_handleErrorResponse', () => {
        let HttpQueue;

        beforeEach(() => {
            HttpQueue = $injector.get('HttpQueue');
            jest.spyOn(HttpQueue.prototype, 'unfreezeAfterError').mockImplementation(() => {});
            instance = new StripeCheckoutHelper(owner);
        });

        describe('402 error', () => {
            let canEditPaymentInfoSpy;

            beforeEach(() => {
                canEditPaymentInfoSpy = jest.spyOn(instance, 'canEditPaymentInfo', 'get').mockReturnValue(true);
            });

            it('should be handled when a message is passed down', () => {
                jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
                instance._handleErrorResponse({
                    status: 402,
                    data: {
                        message: 'Your card was declined.',
                        has_default_source: true,
                    },
                });
                expect(DialogModal.alert).toHaveBeenCalled();

                expect(DialogModal.alert.mock.calls[0][0].content).toEqual(
                    '<p>Your card was declined.</p><p>Please contact your card provider or try another payment source.</p>',
                );
                expect(HttpQueue.prototype.unfreezeAfterError).toHaveBeenCalled();
            });

            it('should be handled when a message_key is passed down', () => {
                jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
                instance._handleErrorResponse({
                    status: 402,
                    data: {
                        message_key: 'there_was_an_issue_charging_to_card',
                        has_default_source: true,
                    },
                });
                expect(DialogModal.alert).toHaveBeenCalled();

                expect(DialogModal.alert.mock.calls[0][0].content).toEqual(
                    '<p>There was an issue charging the provided card.</p><p>Please contact your card provider or try another payment source.</p>',
                );
                expect(HttpQueue.prototype.unfreezeAfterError).toHaveBeenCalled();
            });

            it('should be handled when there is no default_source', () => {
                jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
                instance._handleErrorResponse({
                    status: 402,
                    data: {
                        message_key: 'there_is_no_payment_source',
                        has_default_source: false,
                    },
                });
                expect(DialogModal.alert).toHaveBeenCalled();

                expect(DialogModal.alert.mock.calls[0][0].content).toEqual(
                    '<p>There is no payment source configured on your account.</p><p></p>',
                );
                expect(HttpQueue.prototype.unfreezeAfterError).toHaveBeenCalled();
            });

            it('should call passed in retryMeth if canEditPaymentInfo', () => {
                let closeModal;
                jest.spyOn(DialogModal, 'alert').mockImplementation(opts => {
                    closeModal = opts.close;
                });

                // When the error is observed, the DialogModal should pop up
                const promise = instance._handleErrorResponse(
                    {
                        status: 402,
                        data: {
                            message_key: 'there_was_an_issue_charging_to_card',
                            has_default_source: true,
                        },
                    },
                    'retryMeth',
                    {
                        optionPassedThrough: true,
                    },
                );
                expect(DialogModal.alert).toHaveBeenCalled();

                // When the modal is closed, the retryMeth should be called with the provided options.
                instance.retryMeth = jest.fn().mockReturnValue($q.when('finalReturnValue'));
                closeModal();
                expect(instance.retryMeth.mock.calls[0][0].requiresCardInfo).toBe(true);
                expect(instance.retryMeth.mock.calls[0][0].optionPassedThrough).toBe(true);

                // Make sure that we resolved with the value from the retry
                let finalReturnValue;
                promise.then(result => {
                    finalReturnValue = result;
                });
                $timeout.flush();
                expect(finalReturnValue).toEqual('finalReturnValue');
            });

            it('should be handled when !canEditPaymentInfo', () => {
                canEditPaymentInfoSpy.mockReturnValue(false);
                jest.spyOn(instance, 'ownerEmail', 'get').mockReturnValue('billbob@mycompany.com');

                let closeModal;
                jest.spyOn(DialogModal, 'alert').mockImplementation(opts => {
                    closeModal = opts.close;
                });
                const promise = instance._handleErrorResponse(
                    {
                        status: 402,
                        data: {
                            message_key: 'there_was_an_issue_charging_to_card',
                            has_default_source: true,
                        },
                    },
                    'retryMeth',
                    {
                        optionPassedThrough: true,
                    },
                );
                expect(DialogModal.alert).toHaveBeenCalled();

                // When the modal is closed, the retryMeth should be called with the provided options.
                instance.retryMeth = jest.fn();
                const rejected = jest.fn();
                promise.catch(rejected);
                closeModal();
                $timeout.flush();
                expect(instance.retryMeth).not.toHaveBeenCalled();
                expect(rejected).toHaveBeenCalled();
            });
        });

        it('should not special-handle other errors when saving the subscription', () => {
            const ApiErrorHandler = $injector.get('ApiErrorHandler');

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            jest.spyOn(ApiErrorHandler, 'onResponseError').mockImplementation(() => {});
            instance._handleErrorResponse({
                status: 999,
            });
            expect(DialogModal.alert).not.toHaveBeenCalled();
            expect(ApiErrorHandler.onResponseError).toHaveBeenCalled();
            expect(HttpQueue.prototype.unfreezeAfterError).not.toHaveBeenCalled();
        });
    });
});
