import './scripts/payments_module';

import './scripts/coupon';
import './scripts/stripe_checkout_helper';
import './scripts/subscription';
import './scripts/hiring_team_checkout_helper';
import './scripts/cohort_registration_checkout_helper';
import 'Payments/locales/payments/hiring_team_checkout_helper-en.json';
import 'Payments/locales/payments/stripe_checkout_helper-en.json';
