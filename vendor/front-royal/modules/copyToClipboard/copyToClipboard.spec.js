import copyToClipboard from './copyToClipboard';

describe('copyToClipboard', () => {
    it('should work', () => {
        jest.spyOn(window.document, 'addEventListener');
        document.execCommand = jest.fn();
        jest.spyOn(window.document, 'removeEventListener').mockImplementation(() => {});

        copyToClipboard('foo');

        const copyListener = expect.anything();
        expect(window.document.addEventListener).toHaveBeenCalledWith('copy', copyListener, {
            once: true,
        });
        expect(document.execCommand).toHaveBeenCalledWith('copy');
    });
});
