export default function (text, successCallback, errorCallback) {
    // Note: The copy event is not supported in IE11, and possibly not in iOS Safari
    // See https://developer.mozilla.org/en-US/docs/Web/Events/copy
    window.document.addEventListener(
        'copy',
        e => {
            e.clipboardData.setData('text/plain', text);
            e.preventDefault();
        },
        {
            once: true,
        },
    );

    let supported;
    try {
        supported = document.execCommand('copy');
        successCallback();
    } catch (err) {
        if (errorCallback) {
            errorCallback(err);
        }
    }

    // execCommand returns false if unsupported or disabled
    // See https://developer.mozilla.org/en-US/docs/Web/API/Document/execCommand
    if (!supported && errorCallback) {
        errorCallback(new Error('execCommand not supported'));
    }
}
