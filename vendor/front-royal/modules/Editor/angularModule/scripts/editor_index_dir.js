import angularModule from 'Editor/angularModule/scripts/editor_module';
import template from 'Editor/angularModule/views/editor_index.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('editorIndex', [
    '$injector',

    function factory($injector) {
        const $location = $injector.get('$location');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const $rootScope = $injector.get('$rootScope');
        const isMobileMixin = $injector.get('isMobileMixin');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                isMobileMixin.onLink(scope);
                AppHeaderViewModel.setBodyBackground('beige');
                AppHeaderViewModel.showAlternateHomeButton = false;

                scope.mobileState = {
                    expanded: false,
                };

                // We hide the app header on mobile on this screen
                scope.$watch('isMobile', () => {
                    AppHeaderViewModel.toggleVisibility(!scope.isMobile);
                });

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                //--------------------------
                // Navigation Helpers
                //--------------------------

                scope.gotoSection = section => {
                    scope.section = section;
                    $location.search('section', section);
                    $location.search('id', undefined);
                    scope.mobileState.expanded = false;
                };

                scope.isSectionActive = section => {
                    if (section === scope.section || (section === 'lessons' && !scope.section)) {
                        return true;
                    }
                };

                Object.defineProperty(scope, 'sectionNavigationTitle', {
                    get() {
                        if (!scope.section) {
                            return 'LESSONS';
                        }
                        if (scope.section === 'contentTopics') {
                            return 'CONTENT TOPICS';
                        }
                        return scope.section.toUpperCase();
                    },
                });

                const initialSection = $location.search().section;
                if (initialSection) {
                    scope.gotoSection(initialSection);
                }
            },
        };
    },
]);
