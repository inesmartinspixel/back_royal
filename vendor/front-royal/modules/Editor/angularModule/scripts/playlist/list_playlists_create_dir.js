import angularModule from 'Editor/angularModule/scripts/editor_module';
import template from 'Editor/angularModule/views/playlist/list_playlists_create.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('listPlaylistsCreate', [
    '$injector',

    function factory($injector) {
        const ngToast = $injector.get('ngToast');
        const Playlist = $injector.get('Playlist');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                playlist: '=thing',
                goBack: '&',
                created: '&',
            },
            link(scope) {
                scope.save = () => {
                    Playlist.create({
                        title: scope.newPlaylistTitle,
                    }).then(response => {
                        scope.created({
                            $thing: response.result,
                        });

                        ngToast.create({
                            content: 'Playlist saved',
                            className: 'success',
                        });
                    });
                };
            },
        };
    },
]);
