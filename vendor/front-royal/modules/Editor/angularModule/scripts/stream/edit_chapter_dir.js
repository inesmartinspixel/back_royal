import angularModule from 'Editor/angularModule/scripts/editor_module';
import template from 'Editor/angularModule/views/stream/edit_chapter.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('editChapter', [
    '$injector',

    function factory($injector) {
        const $window = $injector.get('$window');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                stream: '<',
                chapter: '<',
                lessons: '<',
                otherLessons: '<',
                streamLessonLookupById: '<',
            },
            link(scope) {
                NavigationHelperMixin.onLink(scope);

                //----------------------------
                // Watches
                //----------------------------

                // lookup map
                let lessonsById = {};
                scope.$watch('lessons', () => {
                    lessonsById = {};
                    if (!scope.lessons) {
                        return;
                    }
                    scope.lessons.forEach(lesson => {
                        lessonsById[lesson.id] = lesson;
                    });
                });

                // watch for new lesson selection
                scope.$watch('addLesson', newVal => {
                    const lessonId = newVal || undefined;
                    if (!lessonId) {
                        return;
                    }
                    scope.chapter.addLesson(lessonsById[lessonId]);
                    scope.addLesson = undefined; // clear the selectize
                });

                // list of remaining lessons
                scope.$watch('otherLessons', () => {
                    scope.lessonOptions = [];
                    if (!scope.otherLessons) {
                        return;
                    }

                    scope.otherLessons.forEach(lesson => {
                        scope.lessonOptions.push({
                            value: lesson.id,
                            text: lesson.titleWithTag,
                        });
                    });
                });

                //----------------------------
                // Misc Scope Methods
                //----------------------------

                // remove the chapter from the stream, along with this directive element
                scope.removeChapter = () => {
                    if (!$window.confirm(`You sure you want to delete "${scope.chapter.title}"?`)) {
                        return;
                    }

                    scope.chapter.remove();
                };

                // remove the lesson from the chapter
                scope.removeLesson = lesson => {
                    scope.chapter.removeLesson(lesson);
                    scope.addLesson = ''; // not sure selection is triggered without this
                };

                scope.openLessonEditor = lesson => {
                    scope.loadUrl(lesson.editorUrl, '_blank');
                };
            },
        };
    },
]);
