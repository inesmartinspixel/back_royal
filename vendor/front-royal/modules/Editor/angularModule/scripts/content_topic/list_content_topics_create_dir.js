import angularModule from 'Editor/angularModule/scripts/editor_module';
import template from 'Editor/angularModule/views/content_topic/list_content_topics_create.html';
import labelsTemplate from 'Editor/angularModule/views/content_topic/list_content_topics_custom_labels.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('listContentTopicsCreate', [
    '$injector',

    function factory($injector) {
        const ngToast = $injector.get('ngToast');
        const ContentTopic = $injector.get('ContentTopic');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                thing: '=thing',
                goBack: '&',
                created: '&',
            },
            link(scope) {
                scope.labelsTemplate = labelsTemplate;

                if (!scope.thing) {
                    scope.thing = ContentTopic.new({
                        locales: {
                            en: scope.newContentTopicName,
                        },
                    });
                }

                scope.save = () =>
                    scope.thing.save().then(response => {
                        scope.created({
                            $thing: response.result,
                        });

                        scope.goBack();

                        ngToast.create({
                            content: 'Content Topic saved',
                            className: 'success',
                        });
                    });
            },
        };
    },
]);
