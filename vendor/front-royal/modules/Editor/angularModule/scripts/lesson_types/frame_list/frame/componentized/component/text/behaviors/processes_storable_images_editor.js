import angularModule from 'Editor/angularModule/scripts/editor_module';

angular
    .module('FrontRoyal.Editor')
    .factory('Lesson.FrameList.Frame.Componentized.Component.Text.Behaviors.ProcessesStorableImagesEditor', [
        '$injector',
        $injector => {
            const AModuleAbove = $injector.get('AModuleAbove');

            function processStorableImages(text) {
                const replaced = text.replace(
                    /(<img )([^src]+) src="([^"]+)"([^>]*>)/g,
                    (s, a, b, c, d) => `${a}${b} storable-image="${c}"${d}`,
                );
                return replaced;
            }

            /* eslint-enable new-cap */

            return new AModuleAbove({
                included(TextEditorViewModel) {
                    // set up a callback to be run whenever a TextModel is initialized
                    TextEditorViewModel.setCallback('after', 'initialize', function () {
                        const editorViewModel = this;

                        this.model.on('behavior_added:ProcessesStorableImages', () => {
                            editorViewModel.formatters.add('processStorableImages', processStorableImages);
                        });
                    });
                },
            });
        },
    ]);
