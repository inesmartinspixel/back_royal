import angularModule from 'Editor/angularModule/scripts/editor_module';

angularModule.factory(
    'Lesson.FrameList.Frame.Componentized.Component.MultipleChoiceMessage.MultipleChoiceMessageEditorViewModel',

    [
        'Lesson.FrameList.Frame.Componentized.Component.ComponentEditorViewModel',
        'Lesson.FrameList.Frame.Componentized.Component.Mixins.IsMessageEditorViewModel',

        (ComponentEditorViewModel, IsMessageEditorViewModel) =>
            ComponentEditorViewModel.subclass(function () {
                this.include(IsMessageEditorViewModel);
                this.setModel(
                    'Lesson.FrameList.Frame.Componentized.Component.MultipleChoiceMessage.MultipleChoiceMessageModel',
                );

                // the answer that this message is linked to directly.  Note that
                // the message will also be displayed when an equivalent answer (i.e.
                // same text or image) is selected, validated, etc.
                Object.defineProperty(this.prototype, 'primaryAnswer', {
                    get() {
                        return this.model.answerMatcher.answer;
                    },
                });

                return {
                    initialize($super, model) {
                        $super(model);
                        this.onInitializeMessageEditorViewModel();

                        this.model.on('.answerMatcher:remove', () => {
                            this.model.remove();
                        });

                        this.model.on(
                            '.answerMatcher.answer:text_or_image_change',
                            this._onAnswerContentChanged.bind(this),
                        );
                    },

                    // If an answer is changed such that it has the same content
                    // as another one, then there is the possibility that two
                    // messages could apply to the same answer, the message that was
                    // initially added to it and the message that was added to
                    // another answer which is now equivalent.  Clean that sh up.
                    //
                    // Note: very similar functionality is copy-pasted in SelectAnswerNavigatorEditorViewModel
                    _onAnswerContentChanged() {
                        if (this._conflictingMessageExists()) {
                            this.model.remove();
                        }
                    },

                    _conflictingMessageExists() {
                        const messages = this.frame.componentsForType(this.model.constructor);

                        for (const message of messages) {
                            if (
                                message !== this.model &&
                                message.challenge === this.model.challenge &&
                                message.appliesToAnswer(this.primaryAnswer)
                            ) {
                                return true;
                            }
                        }

                        return false;
                    },
                };
            }),
    ],
);
