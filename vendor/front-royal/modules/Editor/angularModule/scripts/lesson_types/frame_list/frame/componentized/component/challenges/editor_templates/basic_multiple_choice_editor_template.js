import angularModule from 'Editor/angularModule/scripts/editor_module';

angular
    .module('FrontRoyal.Editor')
    .factory('Lesson.FrameList.Frame.Componentized.Component.Challenges.EditorTemplates.BasicMultipleChoice', [
        'AModuleAbove',
        'Lesson.FrameList.Frame.Componentized.Component.Challenges.EditorTemplates.ChallengesEditorTemplateHelper',
        'Lesson.FrameList.Frame.Componentized.Component.Challenge.MultipleChoiceChallenge.MultipleChoiceChallengeModel',

        (AModuleAbove, ChallengesEditorTemplateHelper, MultipleChoiceChallengeModel) =>
            new AModuleAbove({
                included(ChallengesEditorViewModel) {
                    ChallengesEditorViewModel.addTemplate('basic_multiple_choice', 'Multiple Choice', function () {
                        const templateHelper = new ChallengesEditorTemplateHelper(this);

                        this.supportsNoIncorrectAnswers = {
                            ImmediateValidation: false,
                        };
                        this.supportsHasBranching = true;

                        this.setConfig({
                            newChallengeType: MultipleChoiceChallengeModel,
                            disallowAddAndReorderChallenges: true,
                            allowSetContextImages: true,
                            supportsCheckMany: {
                                answerListSkin: {
                                    true: 'checkboxes',
                                    false: 'buttons',
                                },
                            },
                            userDefinedOptions: [
                                {
                                    type: 'checkboxSetProperty',
                                    property: 'checkMany',
                                    title: 'Check Many',
                                },
                                {
                                    type: 'checkboxSetProperty',
                                    property: 'hasBranching',
                                    title: 'Branching',
                                    disabled: () => this.lesson.isPractice,
                                },
                                {
                                    type: 'checkboxSetProperty',
                                    property: 'noIncorrectAnswers',
                                    title: 'No Incorrect Answers',
                                },
                            ],
                        });

                        const answerListEditorViewModel = templateHelper.setupSharedAnswerList(
                            this.checkMany ? 'checkboxes' : 'buttons',
                            2,
                        );

                        // don't set the challengeTemplate here.  the checkMany listener handles
                        // toggling the challenge template
                        // templateHelper.setChallengeTemplate('basic_multiple_choice');
                        templateHelper.setupSharedText();

                        answerListEditorViewModel.setUserDefinedOptions('RandomizeAnswerOrder', 'forceSingleColumn');

                        templateHelper.setupExactlyOneChallenge();
                    });
                },
            }),
    ]);
