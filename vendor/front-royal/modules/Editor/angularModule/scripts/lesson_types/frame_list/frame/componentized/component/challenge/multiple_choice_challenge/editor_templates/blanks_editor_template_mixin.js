import angularModule from 'Editor/angularModule/scripts/editor_module';
/*
    Despite the name, note that this mixin is also used for the matching editor template

    It seems like what this is really for is any situation where each challenge is associated with a single correct answer, which should no be deletable.
    It also kind of assumes that you're using sequential or consumable mixin, and that that is handling the usage of ResetAnswersOnActivated
*/
angular
    .module('FrontRoyal.Editor')
    .factory(
        'Lesson.FrameList.Frame.Componentized.Component.Challenge.MultipleChoiceChallenge.EditorTemplates.BlanksMixin',
        [
            () => ({
                apply() {
                    return function () {
                        angular.forEach(
                            {
                                CompleteOnCorrect: {},
                                ClearMessagesOnAnswerSelect: {},
                                DisallowMultipleSelect: {},
                                FlashIncorrectStyling: {},
                                ImmediateValidation: {},
                                ShowCorrectStyling: {},
                            },
                            (val, key) => {
                                this.model.behaviors[key] = val;
                            },
                        );

                        this.setConfig({
                            disableCorrectAnswerSelect: true,
                        });

                        this.model.validator.behaviors.HasAllExpectedAnswers = {};
                        this.model.validator.behaviors.HasNoUnexpectedAnswers = {};

                        // make sure any answerLists, whether shared or not, show the randomize
                        // answer order option and the listen for it's answer list to be set
                        function setAnswerListConfig(answerList) {
                            if (answerList) {
                                answerList.editorViewModel.setUserDefinedOptions(
                                    'RandomizeAnswerOrder',
                                    'forceSingleColumn',
                                );
                            }
                        }
                        setAnswerListConfig(this.model.answerList);
                        this.model.on('set:answerList', setAnswerListConfig);

                        function disallowCorrectAnswerDelete(newCorrectAnswers, oldCorrectAnswers) {
                            oldCorrectAnswers.forEach(answer => {
                                answer.editorViewModel.setConfig({
                                    disallowDelete: false,
                                });
                            });
                            newCorrectAnswers.forEach(answer => {
                                answer.editorViewModel.setConfig({
                                    disallowDelete: true,
                                });
                            });
                        }

                        disallowCorrectAnswerDelete(this.correctAnswers, []);
                        this.model.on('set:correctAnswers', disallowCorrectAnswerDelete);

                        this.model.on('remove', function () {
                            // Be defensive in case there's no correct answer to remove (incomplete frame perhaps, already removed?)
                            if (this.editorViewModel && this.editorViewModel.correctAnswer) {
                                this.editorViewModel.correctAnswer.remove();
                            }
                        });
                    };
                },
            }),
        ],
    );
