import angularModule from 'Editor/angularModule/scripts/editor_module';

angular
    .module('FrontRoyal.Editor')
    .factory(
        'Lesson.FrameList.Frame.Componentized.Component.Challenge.MultipleChoiceChallenge.EditorTemplates.BlanksOnImage',
        [
            'AModuleAbove',
            'Lesson.FrameList.Frame.Componentized.Component.Challenge.MultipleChoiceChallenge.EditorTemplates.BlanksMixin',

            (AModuleAbove, BlanksMixin) =>
                new AModuleAbove({
                    included(MultipleChoiceChallengeEditorViewModel) {
                        MultipleChoiceChallengeEditorViewModel.addTemplate(
                            'blanks_on_image',
                            'Blanks On Image',
                            BlanksMixin.apply(),
                        );
                    },
                }),
        ],
    );
