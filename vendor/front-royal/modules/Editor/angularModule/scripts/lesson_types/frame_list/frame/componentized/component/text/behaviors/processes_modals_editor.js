import angularModule from 'Editor/angularModule/scripts/editor_module';

angular
    .module('FrontRoyal.Editor')
    .factory('Lesson.FrameList.Frame.Componentized.Component.Text.Behaviors.ProcessesModalsEditor', [
        '$injector',
        $injector => {
            const AModuleAbove = $injector.get('AModuleAbove');
            const ErrorLogService = $injector.get('ErrorLogService');

            function processModals(text) {
                let currentIndex = 0;
                const modals = this.model.modals;
                if (modals && modals.length > 0) {
                    text = text.replace(/\[\[(.*?)\]\]/g, (match, contents) => {
                        // ensure we have a matching modal entry
                        const modalContent = modals[currentIndex];
                        if (modalContent === undefined) {
                            return;
                        }

                        const str = `<modal-popup modal-container=".frame-container > .frame" index="${currentIndex}" event-type="lesson:modal-click">\
                                <span label>${contents}</span>\
                                <span content>\
                                    <cf-ui-component view-model="modalViewModel(${currentIndex})"></cf-ui-component>\
                                </span>\
                            </modal-popup>`;
                        currentIndex++;
                        return str;
                    });
                }
                return text;
            }

            function SingleChangeError(message) {
                this.name = 'SingleChangeError';
                this.message = message || '';
            }
            SingleChangeError.prototype = Error.prototype;

            return new AModuleAbove({
                included(TextEditorViewModel) {
                    // set up a callback to be run whenever a TextModel is initialized
                    TextEditorViewModel.setCallback('after', 'initialize', function () {
                        const editorViewModel = this;

                        this.model.on('behavior_added:ProcessesModals', () => {
                            editorViewModel.$$modalKeys = editorViewModel.getModalKeysFromText();

                            editorViewModel.model.on('set:text', (newText, oldText) => {
                                editorViewModel.updateModals.bind(editorViewModel)();

                                // log if we detect a duplicate modal_id anamoly
                                // see: https://trello.com/c/DpBqezxE
                                if (
                                    oldText &&
                                    _.uniq(editorViewModel.$$modalKeys).length < editorViewModel.$$modalKeys.length
                                ) {
                                    $injector
                                        .get('ErrorLogService')
                                        .notify('Text change generated duplicate modals.', null, {
                                            textDetails: {
                                                oldText,
                                                newText,
                                                modalKeys: editorViewModel.$$modalKeys,
                                                recentFormats: editorViewModel.$$recentFormats,
                                            },
                                        });
                                }
                            });

                            editorViewModel.formatters.add('modals', processModals);
                        });

                        // right now, there's no way in the editor to remove this. Can add
                        // this (and test it) later if necessary
                        // this.on('behavior_removed:ProcessesModals', function() {
                        //                     this.off('set:text', onSetText);
                        //                 });
                    });
                },

                updateModals() {
                    this.updateModalKeys(this.getModalKeysFromText());
                },

                getModalKeysFromText() {
                    let modalKeys = this.model.text ? this.model.text.match(/\[\[(.*?)\]\]/g) : undefined;
                    if (!modalKeys) {
                        modalKeys = [];
                    }
                    angular.forEach(modalKeys, (modalKey, i) => {
                        modalKeys[i] = modalKey.replace(/[\[\]]/g, '');
                    });

                    return modalKeys;
                },

                addModal() {
                    const TextEditorViewModel = this.constructor;
                    // We have to make sure to call setup() first before
                    // setting the text on the new modal.  Otherwise the
                    // textEditorViewModel will try to format the text before
                    // all the behaviors are setup.
                    const modal = TextEditorViewModel.addComponentTo(this.frame).setup().model;
                    return modal;
                },

                updateModalKeys(newModalKeys) {
                    // don't worry if this isn't immediately supplied. build off initial text parsing.
                    if (!this.$$modalKeys) {
                        this.$$modalKeys = newModalKeys;
                    }

                    const origModalKeys = this.$$modalKeys.slice(0); // clone modalKeys array

                    // This should never happen, but we have some bug causing us to
                    // get messed up modals.  This should hopefully let editors
                    // at least recover when in that state by ensuring there is
                    // one modal for each of the original modal keys when we start this process.
                    // See https://trello.com/c/6piQl5WG/969-bug-modal-will-not-process#
                    if (_.size(origModalKeys) !== _.size(this.model.modals)) {
                        ErrorLogService.notify('modal keys do not agree with modals', null, {
                            origModalKeys: _.clone(origModalKeys),
                            modals: _.invoke(this.model.modals, 'asJson'),
                            recentFormats: _.clone(this.$$recentFormats) || null,
                        });
                        while (_.size(origModalKeys) > this.model.modals) {
                            origModalKeys.pop();
                        }

                        while (_.size(origModalKeys) < this.model.modals) {
                            this.addModal();
                        }
                    }

                    if (_.isEqual(newModalKeys, origModalKeys)) {
                        return;
                    }

                    // cache data
                    this._saveDataForModals();

                    // In most cases, only one modal will change at a time, either an
                    // addition, a removal, or a change.  In those cases, we can be smart
                    // about preserving the existing confusers, messages, etc.
                    try {
                        // we can add a single modal
                        if (newModalKeys.length > origModalKeys.length) {
                            this._addSingleModal(newModalKeys, origModalKeys);
                        }

                        // we can remove a single modal
                        else if (newModalKeys.length < origModalKeys.length) {
                            this._removeSingleModal(newModalKeys, origModalKeys);
                        }

                        // we can change a single modal
                        else {
                            this._changeSingleModal(newModalKeys, origModalKeys);
                        }

                        // If there is more than once change, then an error will be thrown. In
                        // that case we have to blow away existing questions entirely and
                        // start over from scratch.
                    } catch (e) {
                        if (e.name === 'SingleChangeError') {
                            this.$$modalKeys = [];
                            this.model.modals = [];
                            const oneByOne = [];
                            angular.forEach(newModalKeys, modalKey => {
                                oneByOne.push(modalKey);
                                this.updateModalKeys(oneByOne);
                            });
                        } else {
                            throw e;
                        }
                    }

                    this.$$modalKeys = newModalKeys.slice(0);
                },

                _addSingleModal(newModalKeys, origModalKeys) {
                    const additions = [];
                    let origIndex = 0;
                    angular.forEach(newModalKeys, (newModalKey, i) => {
                        // use origIndex to follow along in the original array,
                        // checking that each newModalKey matches up with the existing modal
                        if (origModalKeys[origIndex] !== newModalKey) {
                            // when we find one that doesn't match up, we assume it needs
                            // to be added, and we step ahead by one in the original array
                            additions.push({
                                index: i,
                                modal: newModalKey,
                            });
                        } else {
                            origIndex += 1;
                        }
                    });
                    if (additions.length !== 1) {
                        throw new SingleChangeError('Expecting to add exactly one modal');
                    }

                    const newModalKey = additions[0].modal;
                    const index = additions[0].index;

                    let modal;
                    const cachedModal = this.$$cachedModalData && this.$$cachedModalData[newModalKey];
                    if (cachedModal) {
                        if (cachedModal.frame() !== this.frame) {
                            this.frame.addComponent(cachedModal);
                        }
                        modal = cachedModal;
                    } else {
                        modal = this.addModal();
                        modal.text = newModalKey;
                    }

                    if (!this.model.modals) {
                        this.model.modals = [];
                    }

                    this.model.modals.splice(index, 0, modal);
                },

                _removeSingleModal(newModalKeys, origModalKeys) {
                    const removals = [];
                    let newIndex = 0;
                    angular.forEach(origModalKeys, (origModal, i) => {
                        // use newIndex to follow along in the new array,
                        // checking that each origModal matches up with the new modal
                        if (newModalKeys[newIndex] !== origModal) {
                            // when we find one that doesn't match up, we assume it needs
                            // to be removed, and we do not step ahead our counter in the new array
                            removals.push({
                                index: i,
                            });
                        } else {
                            newIndex += 1;
                        }
                    });
                    if (removals.length !== 1) {
                        throw new SingleChangeError('Expecting to remove exactly one modal');
                    }

                    this.model.modals[removals[0].index].remove();
                },

                _changeSingleModal(newModalKeys, origModalKeys) {
                    const changes = [];
                    angular.forEach(origModalKeys, (origModal, i) => {
                        const newModalKey = newModalKeys[i];
                        if (newModalKey !== origModal) {
                            changes.push({
                                modal: newModalKey,
                                index: i,
                            });
                        }
                    });

                    if (changes.length !== 1) {
                        throw new SingleChangeError('Expecting to change exactly one modal');
                    }

                    const newModalKey = changes[0].modal;
                    const i = changes[0].index;

                    const cachedModal = this.$$cachedModalData && this.$$cachedModalData[newModalKey];
                    if (cachedModal) {
                        if (cachedModal.frame() !== this.frame) {
                            this.frame.addComponent(cachedModal);
                        }
                        this.model.modals[i] = cachedModal;
                    }
                },

                // save modal data in a cache for later re-use
                _saveDataForModals() {
                    if (!this.$$cachedModalData) {
                        this.$$cachedModalData = {};
                    }

                    angular.forEach(this.$$modalKeys, (modalKey, i) => {
                        this.$$cachedModalData[modalKey] = this.model.modals[i];
                    });
                },
            });
        },
    ]);
