import angularModule from 'Editor/angularModule/scripts/editor_module';
import template from 'Editor/angularModule/views/lesson_types/frame_list/frame/componentized/component/component_overlay/component_overlay_editor.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('cfComponentOverlayEditor', [
    'Lesson.FrameList.Frame.Componentized.Component.Image.ImageModel',
    'Lesson.FrameList.Frame.Componentized.Component.ComponentOverlay.ComponentOverlayEditorViewModel',
    'Lesson.FrameList.Frame.Componentized.Component.EditorDirHelper',

    (ImageModel, ComponentOverlayEditorViewModel, EditorDirHelper) =>
        EditorDirHelper.getOptions({
            templateUrl,
            link(scope, element) {
                EditorDirHelper.link(scope, element);
            },
        }),
]);
