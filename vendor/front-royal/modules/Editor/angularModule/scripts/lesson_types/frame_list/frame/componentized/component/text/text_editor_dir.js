import angularModule from 'Editor/angularModule/scripts/editor_module';
import template from 'Editor/angularModule/views/lesson_types/frame_list/frame/componentized/component/text/text_editor.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('cfTextEditor', [
    'Lesson.FrameList.Frame.Componentized.Component.EditorDirHelper',
    '$injector',
    (EditorDirHelper) /* , $injector */ =>
        EditorDirHelper.getOptions({
            templateUrl,
            link(scope, element) {
                EditorDirHelper.link(scope, element);

                scope.options = scope.options || {};

                Object.defineProperty(scope, 'textareaRows', {
                    get() {
                        return this.skin === 'inline' ? 1 : 4;
                    },
                });

                Object.defineProperty(scope, 'activeModal', {
                    get() {
                        if (
                            scope._activeModal &&
                            scope.model.editorViewModel.$$modalKeys.includes(scope._activeModal)
                        ) {
                            return scope._activeModal;
                        }
                        return scope.model.editorViewModel.$$modalKeys[0];
                    },
                    set(activeModal) {
                        scope._activeModal = activeModal;
                    },
                });

                Object.defineProperty(scope, 'activeModalIndex', {
                    get() {
                        if (scope.activeModal && scope.model.editorViewModel.$$modalKeys.includes(scope.activeModal)) {
                            return scope.model.editorViewModel.$$modalKeys.indexOf(scope.activeModal);
                        }
                        return -1;
                    },
                });

                const softMaxLengthWarningPercent = 0.75;
                const DEFAULT_MAX_LENGTH = 9999;

                scope.checkTextLength = () => {
                    // Make sure that this is specific enough not to catch the
                    // modal editors inside of here
                    const counter = element.find('> .cf-text-editor > .tlm-counter');

                    const maxLength =
                        +scope.editorViewModel.maxRecommendedTextLength() > 0
                            ? +scope.editorViewModel.maxRecommendedTextLength()
                            : DEFAULT_MAX_LENGTH;
                    const yellowThreshold = maxLength * softMaxLengthWarningPercent;
                    const validation = scope.editorViewModel.validateTextLength();
                    const textLength = validation.textLength;
                    const unformattedLength = validation.unformattedLength;

                    if (textLength === 0 && unformattedLength > 0) {
                        // If the text is being formatted, do not show anything
                        counter.text('Calculating ...');
                    } else {
                        counter.text(`${maxLength - textLength} / ${maxLength} remaining`);
                    }

                    const elemClassList = element.get(0).classList;
                    const counterClassList = counter.get(0).classList;

                    if (textLength >= maxLength) {
                        elemClassList.add('tlm-over');
                        elemClassList.remove('tlm-warning');
                        counterClassList.add('tlm-over');
                        counterClassList.remove('tlm-warning');
                    } else if (textLength >= yellowThreshold) {
                        elemClassList.add('tlm-warning');
                        elemClassList.remove('tlm-over');
                        counterClassList.add('tlm-warning');
                        counterClassList.remove('tlm-over');
                    } else {
                        elemClassList.remove('tlm-warning');
                        elemClassList.remove('tlm-over');
                        counterClassList.remove('tlm-warning');
                        counterClassList.remove('tlm-over');
                    }
                };

                // detect outside changes to softMaxLength
                scope.$watch('softMaxLength', scope.checkTextLength);
                scope.$watch('model.formatted_text', scope.checkTextLength);
            },
        }),
]);
