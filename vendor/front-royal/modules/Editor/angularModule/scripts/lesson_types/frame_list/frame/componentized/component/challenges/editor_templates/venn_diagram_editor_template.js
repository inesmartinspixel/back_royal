import angularModule from 'Editor/angularModule/scripts/editor_module';

angular
    .module('FrontRoyal.Editor')
    .factory('Lesson.FrameList.Frame.Componentized.Component.Challenges.EditorTemplates.VennDiagram', [
        'AModuleAbove',
        'Lesson.FrameList.Frame.Componentized.Component.Challenges.EditorTemplates.ChallengesEditorTemplateHelper',
        'Lesson.FrameList.Frame.Componentized.Component.Challenge.MultipleChoiceChallenge.MultipleChoiceChallengeModel',
        'MaxTextLengthConfig',

        (AModuleAbove, ChallengesEditorTemplateHelper, MultipleChoiceChallengeModel, MaxTextLengthConfig) =>
            new AModuleAbove({
                included(ChallengesEditorViewModel) {
                    ChallengesEditorViewModel.addTemplate('venn_diagram', 'Venn Diagram', function () {
                        const templateHelper = new ChallengesEditorTemplateHelper(this);

                        this.setConfig({
                            newChallengeType: MultipleChoiceChallengeModel,
                            disallowAddAndReorderChallenges: true,
                            maxRecommendedTextLengthWithoutImage: MaxTextLengthConfig.VENN_DIAGRAM_TEXT,
                            maxRecommendedTextLengthWithImage: MaxTextLengthConfig.VENN_DIAGRAM_TEXT,
                        });

                        this.model.sharedContentForInteractiveImage = undefined;
                        templateHelper.setChallengeTemplate('venn_diagram');
                        templateHelper.setupSharedText();

                        templateHelper.setupExactlyOneChallenge();
                        const minAnswers = 3;
                        const answerListEditorViewModel = templateHelper.setupSharedAnswerList(
                            'venn_diagram',
                            minAnswers,
                        );
                        templateHelper.setupVennDiagramHeaders(answerListEditorViewModel);

                        const answerAddedListener = answerListEditorViewModel.model.answers.on('childAdded', answer => {
                            answer.editorViewModel.setConfig({
                                disallowDelete: true,
                            });
                        });

                        templateHelper.teardownSharedContentForImage();
                        templateHelper.teardownSharedComponentOverlay();

                        // clean up afterwards
                        const removeTemplateListener = this.model.on('set:editor_template', () => {
                            // undo all the configs set on the challenges editor view model
                            this.setConfig({
                                newChallengeType: undefined,
                                disallowAddAndReorderChallenges: undefined,
                                maxRecommendedTextLengthWithoutImage: MaxTextLengthConfig.TEXT_WITHOUT_IMAGE,
                                maxRecommendedTextLengthWithImage: MaxTextLengthConfig.TEXT_WITH_IMAGE,
                            });

                            answerListEditorViewModel.model.answers.forEach(answerModel => {
                                answerModel.editorViewModel.setConfig({
                                    disallowDelete: false,
                                });
                            });

                            // thought about removing the headers, but nice to keep them so you
                            // can switch away and back

                            answerAddedListener.cancel();

                            removeTemplateListener.cancel();
                        });
                    });
                },
            }),
    ]);
