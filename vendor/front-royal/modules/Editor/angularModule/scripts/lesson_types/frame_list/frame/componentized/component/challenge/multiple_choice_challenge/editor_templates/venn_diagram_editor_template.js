import angularModule from 'Editor/angularModule/scripts/editor_module';

angular
    .module('FrontRoyal.Editor')
    .factory(
        'Lesson.FrameList.Frame.Componentized.Component.Challenge.MultipleChoiceChallenge.EditorTemplates.VennDiagram',
        [
            'AModuleAbove',
            'MaxTextLengthConfig',

            (AModuleAbove, MaxTextLengthConfig) =>
                new AModuleAbove({
                    included(MultipleChoiceChallengeEditorViewModel) {
                        MultipleChoiceChallengeEditorViewModel.addTemplate('venn_diagram', 'Venn Diagram', function () {
                            this.model.behaviors = {
                                ReadyToValidateWhenAnswerIsSelected: {},
                                CompleteOnCorrect: {},
                                ResetAnswersOnActivated: {},
                                DisallowMultipleSelect: {},
                                ShowCorrectStyling: {},
                                FlashIncorrectStyling: {},
                                ClearMessagesOnAnswerSelect: {},
                            };

                            this.model.validator.behaviors.HasAllExpectedAnswers = {};
                            this.model.validator.behaviors.HasNoUnexpectedAnswers = {};

                            let answerListConfigListener;

                            // and the listen for it's answer list to be set
                            function setAnswerListConfig(answerList) {
                                if (answerList) {
                                    // prevent adding addditional answers since venn_diagram only supports 3
                                    answerList.editorViewModel.setConfig({
                                        disallowAddAnswers: true,
                                    });
                                }
                            }
                            answerListConfigListener = this.model.on('set:answerList', setAnswerListConfig, true);

                            const answerAddedListener = this.model.on('.answerList.answers:childAdded', answer => {
                                answer.editorViewModel.setConfig({
                                    maxRecommendedTextLength: MaxTextLengthConfig.VENN_DIAGRAM_ANSWER,
                                });
                            });

                            // clean up afterwards
                            const removeTemplateListener = this.model.on('set:editor_template', () => {
                                // stop applying config to answer list
                                answerListConfigListener.cancel();
                                if (this.model.answerList) {
                                    this.model.answerList.editorViewModel.setConfig({
                                        disallowAddAnswers: undefined,
                                    });

                                    this.model.answerList.answers.forEach(answer => {
                                        answer.editorViewModel.setConfig({
                                            maxRecommendedTextLength: undefined,
                                        });
                                    });
                                }

                                removeTemplateListener.cancel();
                                answerAddedListener.cancel();
                            });
                        });
                    },
                }),
        ],
    );
