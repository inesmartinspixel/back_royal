import angularModule from 'Editor/angularModule/scripts/editor_module';

angular
    .module('FrontRoyal.Editor')
    .factory('Lesson.FrameList.Frame.Componentized.Component.Text.Behaviors.ProcessesInlineImagesEditor', [
        '$injector',
        $injector => {
            const AModuleAbove = $injector.get('AModuleAbove');
            const ImageModel = $injector.get('Lesson.FrameList.Frame.Componentized.Component.Image.ImageModel');

            // FIXME: these two functions are duplicated in ProcessesInlineImages
            function imagesByLabel(frame) {
                const map = {};
                frame.componentsForType(ImageModel).forEach(image => {
                    if (image.label) {
                        map[image.label] = image;
                    }
                });

                return map;
            }

            function searchTextForImages(text, fn) {
                return text.replace(/!\[(.*?)\]/g, fn);
            }

            function processInlineImages(text) {
                const imageMap = imagesByLabel(this.frame);

                const splitOnSpecialBlock = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.Text.TextHelpers',
                ).splitOnSpecialBlock;

                // ProcessesInlineImages requires that markdown runs after it, so we know that it
                // has not run yet
                const isMarkdownFormatted = false;

                // pre-process images so markdown will build them for us
                // (eg: "some ![image] => some <img alt="" src="http://someimage.png">)
                text = splitOnSpecialBlock(text, isMarkdownFormatted, (textBlock, inSpecialBlock) => {
                    if (inSpecialBlock) {
                        return textBlock;
                    }

                    // Prevent Maruku metadata parsing.
                    // see also: https://github.com/jekyll/jekyll/issues/127
                    textBlock = textBlock.replace(':', '\\:');
                    return searchTextForImages(textBlock, (match, contents) => {
                        let str = '';
                        const image = imageMap[contents];
                        if (image) {
                            const imageUrl = image.urlForContext('inline');
                            str = `![](${imageUrl})`;
                        }
                        return str;
                    });
                });

                return text;
            }

            return new AModuleAbove({
                included(TextEditorViewModel) {
                    // set up a callback to be run whenever a TextModel is initialized
                    TextEditorViewModel.setCallback('after', 'initialize', function () {
                        const editorViewModel = this;

                        this.model.on('behavior_added:ProcessesInlineImages', () => {
                            if (!editorViewModel.model.includesBehavior('ProcessesMarkdown')) {
                                throw new Error('ProcessesInlineImages requires ProcessesMarkdown');
                            }

                            editorViewModel.formatters.add('inlineImages', processInlineImages);
                        });
                    });
                },
            });
        },
    ]);
