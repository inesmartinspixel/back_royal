import angularModule from 'Editor/angularModule/scripts/editor_module';

angular
    .module('FrontRoyal.Editor')
    .factory(
        'Lesson.FrameList.Frame.Componentized.Component.Challenge.MultipleChoiceChallenge.EditorTemplates.BasicMultipleChoice',
        [
            'AModuleAbove',

            AModuleAbove =>
                new AModuleAbove({
                    included(MultipleChoiceChallengeEditorViewModel) {
                        function applyTemplate(model, checkMany) {
                            model.behaviors.CompleteOnCorrect = {};
                            model.behaviors.ResetAnswersOnActivated = {};
                            model.behaviors.FlashIncorrectStyling = {};
                            model.behaviors.ClearMessagesOnAnswerSelect = {};
                            // ImmediateValidation and ShowCorrectStyling are handled by the
                            // NoIncorrectAnswers mixin

                            if (!checkMany) {
                                model.behaviors.DisallowMultipleSelect = {};
                            } else {
                                model.behaviors.DisallowMultipleSelect = undefined;
                            }

                            // validator behaviors (i.e. HasAllExpectedAnswers) are handled
                            // by NoIncorrectAnswers mixin
                        }

                        MultipleChoiceChallengeEditorViewModel.addTemplate(
                            'basic_multiple_choice',
                            'Multiple Choice',
                            function () {
                                applyTemplate(this.model, false);

                                // make sure there is at most one correct answer
                                this.correctAnswers = this.correctAnswers.length > 0 ? [this.correctAnswers[0]] : [];
                            },
                        );

                        MultipleChoiceChallengeEditorViewModel.addTemplate('check_many', 'Check Many', function () {
                            applyTemplate(this.model, true);
                            // FIXME: if we decide we want to support checkMany frames with
                            // no correct answers (i.e. any answer is correct), then remove
                            // the validator completely.  Code will have to change in challenges
                            // and the challenges continue button to support that.
                        });
                    },
                }),
        ],
    );
