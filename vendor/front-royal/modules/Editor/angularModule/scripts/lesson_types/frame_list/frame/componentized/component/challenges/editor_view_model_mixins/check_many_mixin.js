import angularModule from 'Editor/angularModule/scripts/editor_module';

angular
    .module('FrontRoyal.Editor')
    .factory('Lesson.FrameList.Frame.Componentized.Component.Challenges.ChallengesEditorViewModel.CheckManyMixin', [
        '$injector',
        $injector => {
            const AModuleAbove = $injector.get('AModuleAbove');

            return new AModuleAbove({
                included(ChallengesEditorViewModel) {
                    Object.defineProperty(ChallengesEditorViewModel.prototype, 'checkMany', {
                        get() {
                            if (this.model.challenges && this.model.challenges[0]) {
                                return this.model.challenges[0].editor_template === 'check_many';
                            }
                            return false;
                        },
                        set(value) {
                            this.model.challenges.forEach(challengeModel => {
                                challengeModel.editorViewModel.applyTemplate(
                                    value ? 'check_many' : 'basic_multiple_choice',
                                );
                            });
                            // cant be both branching and checkmany at the same time
                            if (!!value && this.supportsHasBranching) {
                                this.hasBranching = false;
                            }
                            this.sharedAnswerList.skin = this.config.supportsCheckMany.answerListSkin[value];
                        },
                    });

                    ChallengesEditorViewModel.supportConfigOption('supportsCheckMany');

                    // when the editor view model is created, determine the current value of check many
                    // start listening for challenges to be added
                    //  - when a challenge is added, check the current value of checkMany and apply to the challenge
                    // when checkMany is set, loop through existing challenges and update them
                    ChallengesEditorViewModel.onConfigChange('supportsCheckMany', function (configValue) {
                        if (configValue) {
                            this._checkManyListener = this.model.challenges.on('childAdded', challenge => {
                                const challengeEditorViewModel = challenge.editorViewModel;
                                challengeEditorViewModel.applyTemplate(
                                    this.checkMany ? 'check_many' : 'basic_multiple_choice',
                                );
                            });
                        } else if (this._checkManyListener) {
                            this._checkManyListener.cancel();
                            this._checkManyListener = null;
                        }
                    });
                },
            });
        },
    ]);
