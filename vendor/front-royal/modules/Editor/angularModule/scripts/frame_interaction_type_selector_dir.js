import angularModule from 'Editor/angularModule/scripts/editor_module';

angularModule.directive('frameInteractionTypeSelector', [
    '$injector',
    $injector => {
        const TextImageInteractiveEditorViewModel = $injector.get(
            'Lesson.FrameList.Frame.Componentized.Component.Layout.TextImageInteractive.TextImageInteractiveEditorViewModel',
        );
        const ChallengesEditorViewModel = $injector.get(
            'Lesson.FrameList.Frame.Componentized.Component.Challenges.ChallengesEditorViewModel',
        );

        return {
            restrict: 'E',
            scope: {
                frameViewModel: '<',
            },
            template:
                '<select ng-model="interactionTypeOptions" name="interaction_type" ng-options="item as item.title for item in interactionTypes"></select>',
            link(scope) {
                Object.defineProperty(scope, 'frame', {
                    get() {
                        return this.frameViewModel.frame;
                    },
                });

                Object.defineProperty(scope, 'lesson', {
                    get() {
                        return this.frame.lesson();
                    },
                });

                Object.defineProperty(scope, 'interactionTypeOptions', {
                    get() {
                        let i;
                        let interactionTypeOptions;

                        const mainUiComponentEditorViewModel = this.frame.mainUiComponentEditorViewModel;
                        if (mainUiComponentEditorViewModel) {
                            const editorTemplate = mainUiComponentEditorViewModel.activeTemplate;
                            for (i = 0; i < this.interactionTypes.length; i++) {
                                interactionTypeOptions = scope.interactionTypes[i];
                                if (interactionTypeOptions.editorTemplate === editorTemplate) {
                                    return interactionTypeOptions;
                                }
                            }
                        }
                    },
                    set(options) {
                        if (options.action !== 'applyTemplate') {
                            throw new Error('Unexpected action');
                        }

                        this.frameViewModel.playerViewModel.activeFrame = undefined;
                        const newFrame = this.frame.swapMainUiComponentEditorTemplate(options.editorTemplate);
                        this.frameViewModel.playerViewModel.activeFrame = newFrame;
                        newFrame.expandExtraPanelsInitially();

                        return options;
                    },
                });

                scope.interactionTypes = [];

                scope.$watch('frameViewModel', frameViewModel => {
                    if (!frameViewModel) {
                        return;
                    }

                    // add all of the componentized editor templates to the list
                    // since we don't decorate_frame_json in practice lessons, we disallow polling.  We could
                    // bring it back, but seemed like we wouldn't want it
                    _.compact([
                        TextImageInteractiveEditorViewModel.templates.no_interaction,
                        ChallengesEditorViewModel.templates.this_or_that,
                        ChallengesEditorViewModel.templates.fill_in_the_blanks,
                        ChallengesEditorViewModel.templates.blanks_on_image,
                        ChallengesEditorViewModel.templates.matching,
                        ChallengesEditorViewModel.templates.basic_multiple_choice,
                        ChallengesEditorViewModel.templates.multiple_card_multiple_choice,
                        frameViewModel.lesson.isPractice
                            ? null
                            : ChallengesEditorViewModel.templates.multiple_choice_poll,
                        ChallengesEditorViewModel.templates.venn_diagram,
                        ChallengesEditorViewModel.templates.compose_blanks,
                        ChallengesEditorViewModel.templates.compose_blanks_on_image,
                        ChallengesEditorViewModel.templates.image_hotspot,
                    ]).forEach(editorTemplate => {
                        scope.interactionTypes.push({
                            action: 'applyTemplate',
                            title: editorTemplate.title,
                            editorTemplate,
                        });
                    });
                });

                scope.interactionTypes = _.sortBy(scope.interactionTypes, 'title');
            },
        };
    },
]);
