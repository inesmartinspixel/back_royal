import angularModule from 'Editor/angularModule/scripts/editor_module';

angularModule.factory('FrameScreenshotter', [
    '$injector',
    $injector => {
        const SuperModel = $injector.get('SuperModel');
        const Lesson = $injector.get('Lesson');
        const sequence = $injector.get('sequence');
        const $q = $injector.get('$q');
        const guid = $injector.get('guid');
        const $rootScope = $injector.get('$rootScope');
        const $compile = $injector.get('$compile');
        const $timeout = $injector.get('$timeout');

        return SuperModel.subclass(() => ({
            initialize(logProgress, existingScreenshotDetails, lessonLimit, lessonFilters, frameFilter) {
                this.id = guid.generate();
                this.logProgress = logProgress;
                this.progressMessage = 'Not started';
                this.screenshottedLessonDetails = [];
                this.lessonFilters = lessonFilters || {};
                this.lessonLimit = lessonLimit;
                this.frameFilter = frameFilter || (() => false);
                if (_.any(existingScreenshotDetails)) {
                    this.lessonIds = _.chain(existingScreenshotDetails).pluck('lessonId').uniq().compact().value();
                    this.frameIdsToScreenshot = _.chain(existingScreenshotDetails)
                        .pluck('frameId')
                        .uniq()
                        .compact()
                        .value();
                    this.lessonCount = this.lessonIds.length;
                }
            },

            exec() {
                const self = this;

                return this._loadLessonIds()
                    .then(() => self._screenshotMoreLessons())
                    .then(() => {
                        self.complete = true;
                    });
            },

            _loadLessonIds() {
                if (this.lessonIds) {
                    return $q.when();
                }
                const self = this;
                this.progressMessage = 'Loading lesson list';
                if (self.logProgress) {
                    console.log(self.progressMessage);
                }

                return Lesson.index(
                    {
                        filters: _.extend(
                            {
                                published: true,
                            },
                            this.lessonFilters,
                        ),
                        limit: this.lessonLimit,
                        'fields[]': ['id'],
                    },
                    {
                        // this request could take a long time. Do not time it out
                        timeout: undefined,
                    },
                ).then(response => {
                    self.lessonIds = _.pluck(response.result, 'id');
                    self.lessonCount = self.lessonIds.length;
                });
            },

            _loadLessons(ids) {
                return Lesson.index(
                    {
                        filters: {
                            published: true,
                            id: ids,
                        },
                        fields: 'ALL',
                        'except[]': ['lesson_progress', 'version_history', 'practice_frames'],
                    },
                    {
                        // this request could take a long time. Do not time it out
                        timeout: undefined,
                    },
                ).then(response => response.result);
            },

            _screenshotMoreLessons() {
                const self = this;

                const ids = self.lessonIds.splice(0, 10);
                if (!_.any(ids)) {
                    return $q.when();
                }

                return self
                    ._loadLessons(ids)
                    .then(lessons => self._screenshotLessonBatch(lessons))
                    .then(() => self._screenshotMoreLessons());
            },

            _screenshotLessonBatch(lessons) {
                const self = this;
                return sequence(lessons, lesson =>
                    self._screenshotLesson(lesson).then(() => {
                        self.screenshottedLessonDetails.push({
                            title: lesson.title,
                            id: lesson.id,
                            versionId: lesson.version_id,
                        });
                        self.progressMessage = `${self.screenshottedLessonDetails.length} of ${self.lessonCount} lessons screenshotted.`;
                    }),
                );
            },

            _screenshotLesson(lesson) {
                if (lesson.frames.length === 0) {
                    return $q.when();
                }

                /*
                    1. Put the lesson on the screen
                    2. Whenever a new frame becomes visible, set this.visibleFrameDetails
                    3. Once visibleFrameDetails is set, the bot will take a screenshot
                            and then call onScreenshotTaken
                    4. onScreenshotTaken will trigger either going to the next frame (and
                        back to #2), or onto the next lesson (by triggering onLessonComplete)
                */
                const self = this;

                // Put the lesson on the screen
                const result = this._showLesson(lesson);
                const scope = result.scope;
                const elem = result.elem;

                // Each time a new frame renders, tell the casper script, wait
                // for it to take a screenshot, then move on to the next frame
                this._watchForFrameChanges(scope);

                return $q(resolve => {
                    self.onLessonComplete = () => {
                        scope.playerViewModel.destroy();
                        scope.$destroy();
                        elem.remove();
                        resolve();
                    };
                });
            },

            _showLesson(lesson) {
                const self = this;
                const playerViewModel = lesson.createPlayerViewModel({
                    logProgress: false,
                });
                playerViewModel.preloadAllImages();
                self.playerViewModel = playerViewModel;
                playerViewModel.showStartScreen = false;
                const el = $('<div>').html('<show-lesson player-view-model="playerViewModel"></show-lesson>');
                const scope = $rootScope.$new();
                scope.playerViewModel = playerViewModel;
                $compile(el.contents())(scope);
                $('bot').append(el);
                return {
                    scope,
                    elem: el,
                };
            },

            _watchForFrameChanges(scope) {
                const self = this;
                const playerViewModel = scope.playerViewModel;

                scope.$on('frame_visible', () => {
                    // We have to wait for images to actually render on the screen
                    self._waitForImagesVisible()
                        .then(() => self._waitForTextFormatted(playerViewModel))
                        .then(() => self._waitForBackgroundImages(playerViewModel))
                        .then(() => {
                            self._announceFrameVisible(playerViewModel);
                        });
                });
            },

            _announceFrameVisible(playerViewModel) {
                const lesson = playerViewModel.lesson;
                const self = this;
                self.visibleFrameDetails = {
                    lessonId: lesson.id,
                    frameIndex: playerViewModel.activeFrameIndex,
                    paddedIndex: String.padNumber(playerViewModel.activeFrameIndex, 2),
                    frameId: playerViewModel.activeFrame.id,
                };

                $q(resolve => {
                    // The script will call onScreenshotTaken once it has
                    // take the screenshot
                    self.onScreenshotTaken = resolve;
                }).then(() => self._showNextFrame());

                if (playerViewModel.activeFrameIndex % 5 === 1) {
                    self.progressMessage = `Screenshotting "${lesson.title}": Took ${playerViewModel.activeFrameIndex} of ${playerViewModel.lesson.frames.length} screenshots.`;
                    if (self.logProgress) {
                        console.log(self.progressMessage);
                    }
                }
            },

            _waitForBackgroundImages(playerViewModel) {
                const activeFrame = playerViewModel.activeFrame;
                const answers = activeFrame.componentsForType('Answer.SelectableAnswer.SelectableAnswerModel');
                const imageAnswerCount = _.chain(answers).pluck('image').compact().size().value();

                // with image answers, the images are in background images, so _waitForImagesVisible does not
                // work.  In those cases, just wait a while
                if (imageAnswerCount > 0) {
                    return $timeout(imageAnswerCount * 250);
                }
                return $q.when();
            },

            _waitForTextFormatted(playerViewModel) {
                return playerViewModel.activeFrame.preloadAssets();
            },

            _waitForImagesVisible(start) {
                const self = this;
                start = start || new Date();
                return $q(resolve => {
                    if (new Date() - start > 1000 || self._checkAllImagesRendered()) {
                        resolve();
                    } else {
                        $timeout().then(self._waitForImagesVisible.bind(self, start)).then(resolve);
                    }
                });
            },

            _checkAllImagesRendered() {
                let result = true;
                $('bot img').each((i, img) => {
                    if ($(img).height() === 0) {
                        result = false;
                    }
                });
                return result;
            },

            _showNextFrame() {
                this.visibleFrameDetails = undefined;
                // do not use gotoNext() so we make sure to ignore frame navigation
                let nextFrameIndex = this.playerViewModel.activeFrameIndex + 1;
                let nextFrame = this.playerViewModel.lesson.frames[nextFrameIndex];
                let frameToActivate;

                while (nextFrame && !frameToActivate) {
                    if (!this.frameIdsToScreenshot || _.include(this.frameIdsToScreenshot, nextFrame.id)) {
                        frameToActivate = this.frameFilter(nextFrame) ? undefined : nextFrame;
                    }

                    if (!frameToActivate) {
                        nextFrameIndex++;
                        nextFrame = this.playerViewModel.lesson.frames[nextFrameIndex];
                    }
                }

                if (frameToActivate) {
                    this.playerViewModel.activeFrame = frameToActivate;
                } else {
                    this.onLessonComplete();
                }
            },
        }));
    },
]);
