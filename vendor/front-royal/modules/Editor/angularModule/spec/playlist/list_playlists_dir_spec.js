import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Playlists/angularModule/spec/_mock/fixtures/playlists';

describe('Editor::Playlist::ListPlaylists', () => {
    let SpecHelper;
    let Playlist;
    let EntityMetadata;
    let playlists;
    let elem;
    let scope;
    let $location;
    let contentItemEditorLists;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject($injector => {
            $injector.get('PlaylistFixtures');
            $injector.get('MockIguana');
            SpecHelper = $injector.get('SpecHelper');
            Playlist = $injector.get('Playlist');
            $location = $injector.get('$location');
            EntityMetadata = $injector.get('EntityMetadata');
            SpecHelper.stubDirective('editPlaylist');
            contentItemEditorLists = $injector.get('contentItemEditorLists');
            $timeout = $injector.get('$timeout');

            playlists = [
                Playlist.fixtures.getInstance(),
                Playlist.fixtures.getInstance(),
                Playlist.fixtures.getInstance(),
            ];
        });
        SpecHelper.stubDirective('dirPaginationControls');

        SpecHelper.stubConfig({
            social_name: 'some_twitter_account',
        });

        render();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should display playlists', () => {
        const links = SpecHelper.expectElements(elem, 'td[data-id="title"]', playlists.length);

        for (let i = 0; i < playlists.length; i++) {
            expect(links[i].textContent.trim()).toEqual(playlists[i].title);
        }
    });

    it('should follow a playlist link when clicked on', () => {
        jest.spyOn($location, 'url').mockImplementation(() => {});
        SpecHelper.click(elem, 'button[data-id="edit"]', 0);
        expect($location.url).toHaveBeenCalledWith(`/editor/playlist/${playlists[0].id}/edit`);
    });

    it('should support creating a new playlist', () => {
        SpecHelper.click(elem, 'button[name="create"]');
        SpecHelper.updateTextInput(elem, '[name="title"]', 'New Playlist');
        const createButton = SpecHelper.expectElementEnabled(elem, 'button[name="save"]');

        const playlist = playlists[0];
        Playlist.expect('create')
            .toBeCalledWith({
                title: 'New Playlist',
                __iguana_type: 'Playlist',
            })
            .returns(playlist);

        jest.spyOn($location, 'url').mockImplementation(() => {});
        EntityMetadata.expect('save').returns({
            result: {
                entity_metadata: {
                    title: 'stuff',
                    description: 'things',
                    canonical_url: 'whatever',
                },
            },
            meta: {},
        });

        createButton.click();

        Playlist.flush('create');
        EntityMetadata.flush('save');

        expect($location.url).toHaveBeenCalledWith(`/editor/playlist/${playlist.id}/edit`);
    });

    it('should load other locales if requested', () => {
        SpecHelper.updateSelectize(elem, '[name="locale"]', 'es');
        flushLoad();
        expect(contentItemEditorLists.load).toHaveBeenCalledWith('Playlist', 'es');
    });

    function flushLoad() {
        loadCallback(playlists);
        scope.$digest();
    }

    let loadCallback;

    function render() {
        const renderer = SpecHelper.renderer();
        const obj = {
            onLoad(cb) {
                loadCallback = cb;
                return obj;
            },
        };

        jest.spyOn(contentItemEditorLists, 'load').mockReturnValue(obj);
        renderer.render('<list-playlists></list-playlists>');
        scope = renderer.childScope;
        elem = renderer.elem;

        SpecHelper.expectElements(elem, 'front-royal-spinner:eq(0)');

        // pretend that 'index' call to the api is returning
        $timeout.flush();
        flushLoad();
        expect(contentItemEditorLists.load).toHaveBeenCalledWith('Playlist', 'en');
        SpecHelper.expectNoElement(renderer.elem, 'front-royal-spinner');

        return renderer;
    }
});
