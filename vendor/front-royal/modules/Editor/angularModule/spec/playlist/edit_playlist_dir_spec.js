import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Playlists/angularModule/spec/_mock/fixtures/playlists';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import 'ContentItem/angularModule/spec/_mock/mock_s3_asset';

describe('Editor::Playlist::EditPlaylistDir', () => {
    let $injector;
    let SpecHelper;
    let MockS3Asset;
    let Playlist;
    let Stream;
    let $timeout;
    let playlist;
    let scope;
    let elem;
    let playlistStreams;
    let contentItemEditorLists;
    let allStreams;
    let otherStreams;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            $injector.get('PlaylistFixtures');
            $injector.get('StreamFixtures');
            $injector.get('MockIguana');
            SpecHelper = $injector.get('SpecHelper');
            MockS3Asset = $injector.get('MockS3Asset');
            Stream = $injector.get('Lesson.Stream');
            Playlist = $injector.get('Playlist');
            $timeout = $injector.get('$timeout');
            contentItemEditorLists = $injector.get('contentItemEditorLists');

            playlistStreams = [Stream.fixtures.getInstance(), Stream.fixtures.getInstance()];

            otherStreams = [Stream.fixtures.getInstance()];

            allStreams = playlistStreams.concat(otherStreams);

            playlist = Playlist.fixtures.getInstance({
                streams: playlistStreams,
            });

            SpecHelper.stubCurrentUser('editor');
            SpecHelper.stubConfig();

            SpecHelper.stubDirective('contentTopicsEditor');
            SpecHelper.stubDirective('contentItemSavePublish');
            SpecHelper.stubDirective('entityMetadataForm');

            SpecHelper.stubDirective('itemGroupsEditor');
            SpecHelper.stubDirective('contentItemEditLocale');
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('basic editing', () => {
        beforeEach(() => {
            render();
        });

        it('should have have an editable title input', () => {
            SpecHelper.updateTextInput(elem, 'input[name="title"]', 'new Title');
            SpecHelper.expectEqual('new Title', playlist.title, 'updated title');
        });

        it('should have have an editable tag input', () => {
            SpecHelper.updateTextInput(elem, 'input[name="tag"]', 'new Tag');
            SpecHelper.expectEqual('new Tag', playlist.tag, 'updated tag');
        });

        it('should have have an editable textarea input', () => {
            SpecHelper.updateTextInput(elem, 'textarea[name="description"]', 'new Description');
            SpecHelper.expectEqual('new Description', playlist.description, 'updated description');
        });

        it('should support uploading an image', () => {
            const imageUploader = SpecHelper.expectElement(elem, '.course-image-uploader');
            SpecHelper.expectEqual('imageUploaded($$s3Asset)', imageUploader.attr('uploaded'), 'uploaded callback');

            // pretend that an image was uploaded with content-item-image-upload
            const s3Asset = MockS3Asset.get();
            scope.imageUploaded(s3Asset);
            scope.$digest();

            SpecHelper.expectEqual(s3Asset, playlist.image, 'uploaded image on playlist');
        });

        it('should support editing stream list', () => {
            SpecHelper.updateSelectize(elem, '.stream-entries selectize', [otherStreams[0].localePackId]);
            expect(playlist.streamLocalePackIds).toEqual(
                _.pluck(playlistStreams.concat([otherStreams[0]]), 'localePackId'),
            );
        });
    });

    function flushStreamLoad() {
        loadCallback(allStreams);
        scope.$digest();
    }

    let loadCallback;

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.scope.playlistId = playlist.id;

        Playlist.resetCache();

        jest.spyOn(contentItemEditorLists, 'load').mockReturnValue({
            onLoad(cb) {
                loadCallback = cb;
            },
        });

        Playlist.expect('show')
            .toBeCalledWith(playlist.id, {
                filters: {
                    published: false,
                    in_users_locale_or_en: null,
                },
            })
            .returns(playlist);

        renderer.render('<edit-playlist playlist-id="{{playlistId}}"></edit-playlist>');
        scope = renderer.childScope;
        Playlist.flush('show');
        flushStreamLoad();
        scope.playlist = playlist;
        elem = renderer.elem;
        $timeout.flush();
        expect(contentItemEditorLists.load).toHaveBeenCalledWith('Lesson.Stream', 'en', undefined);
    }
});
