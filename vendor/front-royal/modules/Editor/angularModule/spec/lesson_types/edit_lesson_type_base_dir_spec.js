import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import editLessonActionsFormTemplate from 'Editor/angularModule/views/lesson_types/edit_lesson_actions_form.html';
import stubSpecLocale from 'Translation/stubSpecLocale';

stubSpecLocale('lessons.models.lesson.lesson_player_view_model.lesson_x_of_x');

describe('Editor.EditLessonTypeBaseDir', () => {
    let SpecHelper;
    let expectedLesson;
    let scope;
    let elem;
    let Lesson;
    let Config;
    let EntityMetadata;
    let $window;
    let renderer;
    let $injector;
    let DialogModal;

    beforeEach(() => {
        // we have to create a new app so we can add a fake directive to it that
        // descends from the base
        const testApp = angular.module('EditLessonTypeBaseDirTestApp', ['FrontRoyal.Editor', 'ooDirective']);
        testApp.ooDirective('lessonTypeEditor', [
            'editLessonTypeBase',
            function factory(editLessonTypeBase) {
                return editLessonTypeBase.subclass({
                    template: editLessonActionsFormTemplate,
                });
            },
        ]);

        // look for a note about NoUnhandledRejectionExceptions use below (error handling)
        angular.mock.module('EditLessonTypeBaseDirTestApp', 'SpecHelper', 'NoUnhandledRejectionExceptions');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;

                $window = $injector.get('$window');

                SpecHelper = $injector.get('SpecHelper');
                Lesson = $injector.get('Lesson');
                $injector.get('LessonFixtures');
                Config = $injector.get('Config');
                EntityMetadata = $injector.get('EntityMetadata');
                DialogModal = $injector.get('DialogModal');

                expectedLesson = Lesson.fixtures.getInstance();
                Config.setAdapter('Iguana.Mock.Adapter');
                Config.expect('index', [], {
                    result: {},
                });

                SpecHelper.stubCurrentUser('super_editor');

                render();
            },
        ]);
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.playerViewModel = expectedLesson.createPlayerViewModel({
            editorMode: true,
        });
        jest.spyOn(renderer.scope.playerViewModel, 'linkToWindow').mockImplementation(() => {});
        jest.spyOn(renderer.scope.playerViewModel, 'delinkFromWindow').mockImplementation(() => {});
        renderer.scope.playerViewModel.lesson.author = {
            id: 1,
            email: 'blarg@blarg.com',
        };
        renderer.scope.playerViewModel.lesson.entity_metadata = EntityMetadata.new({
            title: 'blah',
            description: 'blah',
            canonical_url: 'blah',
        });
        renderer.render('<lesson-type-editor player-view-model="playerViewModel" />');
        elem = renderer.elem;
        scope = renderer.childScope;
    }

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('saving', () => {
        it('should alert save warnings', () => {
            EntityMetadata.expect('create');
            Lesson.expect('update')
                .toBeCalledWith(
                    angular.extend(expectedLesson.asJson(), {}),
                    {},
                    {
                        timeout: undefined,
                    },
                )
                .returnsMeta({
                    save_warnings: ['a warning'],
                });

            const saveButton = angular.element(elem).find('button[data-id="save"]');
            expect(saveButton.length).toBe(1);
            saveButton.click();
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            Lesson.flush('update');
            expect(DialogModal.alert).toHaveBeenCalledWith({
                title: 'Lesson Warnings',
                content:
                    '<p>Your lesson has been saved, but you cannot pin until the following issues are fixed:</p><ul><li>a warning</li></ul>',
            });
        });

        it('should not alert if no save warnings', () => {
            EntityMetadata.expect('create');
            Lesson.expect('update')
                .toBeCalledWith(
                    angular.extend(expectedLesson.asJson(), {}),
                    {},
                    {
                        timeout: undefined,
                    },
                )
                .returnsMeta({});

            const saveButton = angular.element(elem).find('button[data-id="save"]');
            expect(saveButton.length).toBe(1);
            saveButton.click();
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            Lesson.flush('update');
            expect(DialogModal.alert).not.toHaveBeenCalled();
        });

        it('should toggle save button to disabled during save', () => {
            // see also: NoUnhandledRejectionExceptions module use above

            jest.spyOn($window, 'alert').mockImplementation(() => {});

            // FIXME: Iguana should somehow let us call this without hacking this version in
            EntityMetadata.expect('create');
            Lesson.expect('update')
                .toBeCalledWith(
                    angular.extend(expectedLesson.asJson(), {}),
                    {},
                    {
                        timeout: undefined,
                    },
                )
                .returns({
                    result: expectedLesson,
                });

            const saveButton = angular.element(elem).find('button[data-id="save"]');
            expect(saveButton.length).toBe(1);
            expect(saveButton.prop('disabled')).toBe(false);
            saveButton.click();
            expect(saveButton.prop('disabled')).toBe(true);
            Lesson.flush('update');
            expect(saveButton.prop('disabled')).toBe(false);

            // do it again with a failure ...
            Lesson.expect('update').fails({
                message: 'catastrophic system failure!',
            });
            saveButton.click();
            expect(saveButton.prop('disabled')).toBe(true);
            Lesson.flush('update');
            expect(saveButton.prop('disabled')).toBe(false);
        });

        it('should allow saving, editing of all fields if lesson editor', () => {
            renderer.scope.playerViewModel = expectedLesson.createPlayerViewModel({
                editorMode: true,
            });
            renderer.scope.playerViewModel.lesson.author = {
                id: 1,
                email: 'blarg@blarg.com',
            };
            scope.currentUser.roles = [
                {
                    name: 'editor',
                    resource_id: null,
                },
                {
                    name: 'lesson_editor',
                    resource_type: 'Lesson',
                    resource_id: scope.playerViewModel.lesson.id,
                },
            ];
            renderer.render('<lesson-type-editor player-view-model="playerViewModel" />');

            const saveButton = angular.element(elem).find('button[data-id="save"]');
            expect(saveButton.prop('disabled')).toBe(false);

            const previewButton = angular.element(elem).find('button[data-id="preview"]');
            expect(previewButton.prop('disabled')).toBe(false);

            const duplicateButton = angular.element(elem).find('button[data-id="duplicate"]');
            expect(duplicateButton.prop('disabled')).toBe(false);
        });

        it('should display a toast upon save complete', () => {
            const $filter = $injector.get('$filter');
            const ngToast = $injector.get('ngToast');

            expectedLesson.updated_at = 12345; // seconds

            Lesson.expect('update')
                .toBeCalledWith(
                    angular.extend(expectedLesson.asJson(), {}),
                    {},
                    {
                        timeout: undefined,
                    },
                )
                .returns({
                    result: expectedLesson,
                });
            EntityMetadata.expect('create');
            const saveButton = angular.element(elem).find('button[data-id="save"]');

            jest.spyOn(ngToast, 'create').mockImplementation(() => {});

            saveButton.click();
            Lesson.flush('update');
            EntityMetadata.flush('create');

            expect(ngToast.create).toHaveBeenCalledWith({
                content: `Lesson successfully saved at ${$filter('amDateFormat')(
                    expectedLesson.updated_at * 1000,
                    'h:mm:ss a',
                )}`,
            });
        });

        it('should defer consecutive saves into a single save if already saving', () => {
            jest.spyOn(expectedLesson, 'save');

            const saveButton = angular.element(elem).find('button[data-id="save"]');
            EntityMetadata.expect('create');
            Lesson.expect('update');
            saveButton.click();

            EntityMetadata.expect('create');
            Lesson.expect('update');
            saveButton.click();

            EntityMetadata.expect('create');
            Lesson.expect('update');
            saveButton.click();

            EntityMetadata.expect('create');
            Lesson.expect('update');
            saveButton.click();
            Lesson.flush('update');
            EntityMetadata.flush('create');

            expect(expectedLesson.save).toHaveBeenCalled();
            expect(expectedLesson.save.mock.calls.length).toBe(2);
        });
    });

    describe('permission enforcement', () => {
        it('should not allow general editing if lesson reviewer', () => {
            renderer.scope.playerViewModel = expectedLesson.createPlayerViewModel({
                editorMode: true,
            });
            renderer.scope.playerViewModel.lesson.author = {
                id: 1,
                email: 'blarg@blarg.com',
            };
            scope.currentUser.roles = [
                {
                    name: 'editor',
                    resource_id: null,
                },
                {
                    name: 'reviewer',
                    resource_type: 'Lesson',
                    resource_id: scope.playerViewModel.lesson.id,
                },
            ];
            renderer.render('<lesson-type-editor player-view-model="playerViewModel" />');

            const saveButton = angular.element(elem).find('button[data-id="save"]');
            expect(saveButton.prop('disabled')).toBe(true);

            const previewButton = angular.element(elem).find('button[data-id="preview"]');
            expect(previewButton.prop('disabled')).toBe(false);

            const duplicateButton = angular.element(elem).find('button[data-id="duplicate"]');
            expect(duplicateButton.prop('disabled')).toBe(true);
        });

        it('should only allow previewing if lesson previewer', () => {
            renderer.scope.playerViewModel = expectedLesson.createPlayerViewModel({
                editorMode: true,
            });
            renderer.scope.playerViewModel.lesson.author = {
                id: 1,
                email: 'blarg@blarg.com',
            };
            scope.currentUser.roles = [
                {
                    name: 'editor',
                    resource_id: null,
                },
                {
                    name: 'previewer',
                    resource_type: 'Lesson',
                    resource_id: scope.playerViewModel.lesson.id,
                },
            ];
            renderer.render('<lesson-type-editor player-view-model="playerViewModel" />');

            const saveButton = angular.element(elem).find('button[data-id="save"]');
            expect(saveButton.prop('disabled')).toBe(true);

            const previewButton = angular.element(elem).find('button[data-id="preview"]');
            expect(previewButton.prop('disabled')).toBe(false);

            const duplicateButton = angular.element(elem).find('button[data-id="duplicate"]');
            expect(duplicateButton.prop('disabled')).toBe(true);
        });
    });

    describe('preview', () => {
        it('should preview a lesson', () => {
            jest.spyOn(scope.lesson, 'resetKeyTerms').mockImplementation(() => {});
            const originalPlayerViewModel = scope.playerViewModel;
            elem.find('button[data-id="preview"]').click();
            expect(scope.previewMode).toBe(true);

            // a new playerViewModel should have been created
            expect(scope.playerViewModel).not.toBe(originalPlayerViewModel);
            expect(scope.playerViewModel.editorMode).toBe(false);
            expect(scope.playerViewModel.previewMode).toBe(true);
            expect(scope.lesson.resetKeyTerms).toHaveBeenCalled();
        });

        it('should respond to editLesson:exitPreviewMode event', () => {
            jest.spyOn(scope, 'preview').mockImplementation(() => {});
            expect(scope.previewMode).toBe(false); // sanity check
            scope.$emit('editLesson:exitPreviewMode');
            expect(scope.preview).not.toHaveBeenCalled(); // nothing should happen when previewMod=false

            scope.previewMode = true;
            scope.$emit('editLesson:exitPreviewMode');
            expect(scope.preview).toHaveBeenCalled();
        });
    });

    it('should recreate playerViewModel when playerViewModel is destroyed', () => {
        const originalPlayerViewModel = scope.playerViewModel;
        scope.playerViewModel.destroy();
        scope.$digest();
        expect(scope.playerViewModel).not.toBeUndefined();
        expect(scope.playerViewModel).not.toBe(originalPlayerViewModel);
        expect(scope.playerViewModel.lesson).toBe(originalPlayerViewModel.lesson);
        expect(scope.playerViewModel.editorMode).toBe(true);
    });

    it('should link playerViewModel to the window', () => {
        render();

        expect(scope.playerViewModel.linkToWindow).toHaveBeenCalled();

        scope.playerViewModel.previewMode = true;
        scope.$digest();
        expect(scope.playerViewModel.delinkFromWindow).toHaveBeenCalled();

        scope.playerViewModel.linkToWindow.mockClear();
        scope.playerViewModel.previewMode = false;
        scope.playerViewModel.editorMode = true;
        scope.$digest();
        expect(scope.playerViewModel.linkToWindow).toHaveBeenCalled();
    });
});
