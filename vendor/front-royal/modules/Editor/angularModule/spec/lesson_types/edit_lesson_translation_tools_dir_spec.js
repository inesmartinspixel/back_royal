import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Users/angularModule/spec/_mock/fixtures/users';

describe('Editor.EditLessonTranslationToolsDir', () => {
    let lesson;
    let scope;
    let elem;
    let SpecHelper;
    let Lesson;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject($injector => {
            Lesson = $injector.get('Lesson');
            $injector.get('LessonFixtures');
            SpecHelper = $injector.get('SpecHelper');
            SpecHelper.stubDirective('contentTopicsEditor');
            SpecHelper.stubDirective('contentItemEditLocale');
            lesson = Lesson.fixtures.getInstance();
            Lesson.setAdapter('Iguana.Mock.Adapter');
            SpecHelper.stubCurrentUser('learner');
            render();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('translatableLessonExportSet', () => {
        it('should allow for exporting', () => {
            // hide exportElem until contentItemEditLocaleLoaded
            SpecHelper.expectNoElement(elem, 'translatable-lesson-export-set');
            scope.contentItemEditLocaleLoaded = true;
            scope.$digest();
            const exportElem = SpecHelper.expectElement(elem, 'translatable-lesson-export-set');
            SpecHelper.expectNoElement(exportElem, 'front-royal-spinner');
            SpecHelper.expectNoElement(exportElem, '[name="download-links"]');

            // clicking to export should show set translatableLessonExportSet
            SpecHelper.mockTranslatableLessonExportSetPrepare();
            SpecHelper.click(elem, '[name="export"]');
            expect(scope.translatableLessonExportSet).not.toBeUndefined();
            expect(scope.translatableLessonExportSet.lessons).toEqual([lesson]);
            expect(scope.translatableLessonExportSet.formats).toEqual(['oneColumnTextOnly', 'oneColumnWithFrames']);
            const exportScope = exportElem.isolateScope();
            expect(exportScope).not.toBeUndefined();
            expect(exportScope.translatableLessonExportSet).toBe(scope.translatableLessonExportSet);
        });
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.scope.playerViewModel = lesson.createPlayerViewModel();
        renderer.render('<edit-lesson-translation-tools player-view-model="playerViewModel" />');
        elem = renderer.elem;
        scope = renderer.childScope;
    }
});
