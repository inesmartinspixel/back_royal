import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Lessons/angularModule/spec/_helpers/componentized_spec_helper';

describe('cf-image-list-editor', () => {
    let frame;
    let elem;
    let scope;
    let Componentized;
    let SpecHelper;
    let ComponentizedSpecHelper;
    let $window;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                ComponentizedSpecHelper = $injector.get('ComponentizedSpecHelper');
                $injector.get('ComponentizedFixtures');
                frame = Componentized.fixtures.getInstance();
                $window = $injector.get('$window');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should show editors for each available image', () => {
        const images = [frame.addImage(), frame.addImage()];
        render();
        ComponentizedSpecHelper.assertEditorElementPresent(elem, images[0]);
        ComponentizedSpecHelper.assertEditorElementPresent(elem, images[1]);
    });

    it('should support uploading an image', () => {
        render();
        const uploader = SpecHelper.startUploadWithContentItemImageUpload(elem, '.content-item-image-upload');

        const image = _.last(frame.components);
        SpecHelper.expectEqual('ImageModel', image.type);
        SpecHelper.expectEqual('dataUrl', image.$$dataUrl);
        expect(frame.lesson().blockSaving).toBe(true);

        const s3Asset = uploader.getResponseFromServer();
        SpecHelper.expectEqual(s3Asset, image.image);
        expect(frame.lesson().blockSaving).toBe(false);
    });

    it('should delete images if upload fails', () => {
        render();

        // upload one image successfully so we can make sure it does not end up getting deleted
        SpecHelper.uploadWithContentItemImageUpload(elem, '.content-item-image-upload', undefined, 'firstImage.png');
        const firstImage = _.last(frame.components);

        const uploader = SpecHelper.startUploadWithContentItemImageUpload(elem, '.content-item-image-upload:eq(1)');
        const secondImage = _.last(frame.components);
        SpecHelper.expectEqual('ImageModel', secondImage.type);
        SpecHelper.expectEqual('dataUrl', secondImage.$$dataUrl);
        expect(frame.lesson().blockSaving).toBe(true);

        jest.spyOn(firstImage, 'remove').mockImplementation(() => {});
        jest.spyOn(secondImage, 'remove').mockImplementation(() => {});
        uploader.getErrorFromServer();
        expect(firstImage.remove).not.toHaveBeenCalled();
        expect(secondImage.remove).toHaveBeenCalled();
        expect(frame.lesson().blockSaving).toBe(false);
    });

    it('should alert and delete images if response comes back with unkown filename', () => {
        render();

        const uploader1 = SpecHelper.startUploadWithContentItemImageUpload(
            elem,
            '.content-item-image-upload',
            undefined,
            'image1',
        );
        const image1 = _.last(frame.components);
        expect(frame.lesson().blockSaving).toBe(true);

        const uploader2 = SpecHelper.startUploadWithContentItemImageUpload(
            elem,
            '.content-item-image-upload:eq(1)',
            undefined,
            'image2',
        );
        const image2 = _.last(frame.components);
        expect(frame.lesson().blockSaving).toBe(true);

        jest.spyOn(image1, 'remove').mockImplementation(() => {});
        jest.spyOn(image2, 'remove').mockImplementation(() => {});
        jest.spyOn($window, 'alert').mockImplementation(() => {});
        uploader1.getResponseFromServer('anotherFileName.png');
        expect(image1.remove).toHaveBeenCalled();
        expect(image2.remove).toHaveBeenCalled();
        expect($window.alert).toHaveBeenCalled();
        expect(frame.lesson().blockSaving).toBe(false);

        // the second response should not alert again
        $window.alert.mockClear();
        uploader2.getResponseFromServer('anotherFileName.png');
        expect($window.alert).not.toHaveBeenCalled();
        expect(frame.lesson().blockSaving).toBe(false);
    });

    it('should support deleting an image', () => {
        const image = frame.addImage();
        render();
        jest.spyOn(image, 'remove').mockImplementation(() => {});
        SpecHelper.click(elem, '[name="remove"]');
        expect(image.remove).toHaveBeenCalled();
    });

    it('should disable remove while uploading', () => {
        const image = frame.addImage();
        render();
        SpecHelper.expectElementEnabled(elem, '.remove-button:eq(0)');
        image.$$dataUrl = 'asdasdas';
        image.image = undefined;
        scope.$digest();
        SpecHelper.expectElementDisabled(elem, '.remove-button:eq(0)');
    });

    function render(html = '<cf-image-list-editor frame-view-model="frameViewModel"></cf-image-list-editor>') {
        const renderer = SpecHelper.renderer();
        renderer.scope.mainImageTarget = {};
        renderer.scope.frameViewModel = frame.createFrameViewModel();
        renderer.render(html);
        elem = renderer.elem;
        scope = renderer.scope;
    }
});
