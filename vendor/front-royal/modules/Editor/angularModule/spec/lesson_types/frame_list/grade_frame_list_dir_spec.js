import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

describe('Lessons::GradeFrameListDir', () => {
    let elem;
    let SpecHelper;
    let lesson;
    let scope;
    let DialogModal;
    let grader;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $injector.get('LessonFixtures');
            $timeout = $injector.get('$timeout');
            DialogModal = $injector.get('DialogModal');
            const FrameList = $injector.get('Lesson.FrameList');
            lesson = FrameList.fixtures.getInstance();
            jest.spyOn(DialogModal, 'hideAlerts').mockImplementation(() => {});

            render();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should show the grade', () => {
        Object.defineProperty(grader, 'letterGrade', {
            value: 'Grade',
        });
        scope.$digest();
        SpecHelper.expectElementText(elem, '.letter-grade', 'Grade');
    });

    it('should show the generalMessage', () => {
        assertGeneralMessage('A+', "You're practically perfect in every way.");
        assertGeneralMessage('A-', 'Good job. Try to eliminate potential problems to get to an A+!');
        assertGeneralMessage(
            'B-',
            'Almost there! Eliminate critical issues and potential problems to reach B or higher before pinning.',
        );
    });

    it('should show issue lists for each type', () => {
        grader.notes = [];
        grader.potentialProblems = [];
        grader.criticalIssues = [];
        scope.$digest();
        SpecHelper.expectElements(elem, '.issue-list', 0);

        grader.criticalIssues = [{}];
        scope.$digest();
        SpecHelper.expectElements(elem, '.issue-list', 1);
        SpecHelper.expectElement(elem, '.issue-list.critical_issues');
        SpecHelper.expectElementText(elem, '.issue-list.critical_issues h3', 'Critical Issues ?');

        grader.potentialProblems = [{}];
        scope.$digest();
        SpecHelper.expectElements(elem, '.issue-list', 2);
        SpecHelper.expectElement(elem, '.issue-list.potential_problems');
        SpecHelper.expectElementText(elem, '.issue-list.potential_problems h3', 'Potential Problems ?');

        grader.notes = [{}];
        scope.$digest();
        SpecHelper.expectElements(elem, '.issue-list', 3);
        SpecHelper.expectElement(elem, '.issue-list.notes');
        SpecHelper.expectElementText(elem, '.issue-list.notes h3', 'Notes ?');
    });

    it('should show issues in the issue list', () => {
        grader.notes = [
            {
                message: 'a message',
            },
        ];
        scope.$digest();
        SpecHelper.expectElements(elem, '.issue-list.notes li', 1);
        SpecHelper.expectElementText(elem, '.issue-list.notes li', 'a message');
    });

    function assertGeneralMessage(grade, message) {
        Object.defineProperty(grader, 'letterGrade', {
            value: grade,
        });
        scope.$digest();
        SpecHelper.expectElementText(elem, '.general-message', message);
    }

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.scope.lesson = lesson;
        renderer.render('<grade-frame-list lesson="lesson" ></grade-frame-list>');
        elem = renderer.elem;
        scope = renderer.childScope;
        $timeout.flush(); // flush the timeout on grader
        grader = scope.grader;
    }
});
