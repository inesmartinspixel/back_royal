import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';

describe('cf-user-defined-options', () => {
    let frame;
    let elem;
    let model;
    let editorViewModel;
    let Componentized;
    let SpecHelper;
    let ComponentModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                ComponentModel = $injector.get('Lesson.FrameList.Frame.Componentized.Component.ComponentModel');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                frame = Componentized.fixtures.getInstance();
            },
        ]);

        ComponentModel.supportBehavior('MyBehavior');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('toggleBehavior', () => {
        beforeEach(() => {
            render([
                {
                    type: 'toggleBehavior',
                    behavior: 'MyBehavior',
                    title: 'My Behavior',
                    valueWhenOn: {
                        a: 'b',
                    },
                },
            ]);
        });

        it('should show labels', () => {
            SpecHelper.expectElementText(elem, '.option-label', 'My Behavior');
        });

        it('should toggle a behavior on and off', () => {
            expect(model.behaviors.MyBehavior).toBeUndefined();
            SpecHelper.toggleCheckbox(elem, 'input');
            expect(model.behaviors.MyBehavior).toEqual({
                a: 'b',
            });
            SpecHelper.toggleCheckbox(elem, 'input');
            expect(model.behaviors.MyBehavior).toBe(false);
        });

        describe('with defaultValue', () => {
            let options;

            beforeEach(() => {
                options = [
                    {
                        type: 'toggleBehavior',
                        defaultValue: {
                            a: 'default',
                        },
                        behavior: 'MyBehavior',
                        title: 'My Behavior',
                        valueWhenOn: {
                            a: 'b',
                        },
                    },
                ];
            });

            it('should set a default value if current value is undefined', () => {
                render(options, {
                    behaviors: {
                        MyBehavior: undefined,
                    },
                });
                expect(model.behaviors.MyBehavior.a).toBe('default');
            });
            it('should not set a default value if current value is false', () => {
                render(options, {
                    behaviors: {
                        MyBehavior: false,
                    },
                });
                expect(model.behaviors.MyBehavior).toBe(false);
            });
            it('should not set a default value if current value is set', () => {
                render(options, {
                    behaviors: {
                        MyBehavior: {
                            a: 'previouslySet',
                        },
                    },
                });
                expect(model.behaviors.MyBehavior.a).toBe('previouslySet');
            });
        });
    });

    describe('radioSetProperty', () => {
        beforeEach(() => {
            render([
                {
                    type: 'radioSetProperty',
                    property: 'myProperty',
                    radios: [
                        ['this', 'This'],
                        ['that', 'That'],
                    ],
                },
            ]);
        });

        it('should show labels', () => {
            SpecHelper.expectElementText(elem, '.option-label', 'This', 0);
            SpecHelper.expectElementText(elem, '.option-label', 'That', 1);
        });

        it('should toggle property', () => {
            expect(editorViewModel.myProperty).toBeUndefined();

            SpecHelper.toggleRadio(elem, 'input', 0);
            expect(editorViewModel.myProperty).toEqual('this');

            SpecHelper.toggleRadio(elem, 'input', 1);
            expect(editorViewModel.myProperty).toEqual('that');
        });
    });

    describe('checkboxSetProperty', () => {
        beforeEach(() => {
            render([
                {
                    type: 'checkboxSetProperty',
                    property: 'myProperty',
                    title: 'Title',
                },
            ]);
        });

        it('should show labels', () => {
            SpecHelper.expectElementText(elem, '.option-label', 'Title', 0);
        });

        it('should toggle property', () => {
            expect(editorViewModel.myProperty).toBeUndefined();

            SpecHelper.toggleCheckbox(elem, 'input');
            expect(editorViewModel.myProperty).toEqual(true);

            SpecHelper.toggleCheckbox(elem, 'input');
            expect(editorViewModel.myProperty).toEqual(false);
        });
    });

    describe('inputGetterSetter', () => {
        let prop;

        it('should create an input and bind it to the provided getter and setter', () => {
            prop = 'initial value';
            render([
                {
                    type: 'inputGetterSetter',
                    title: 'Property',
                    get() {
                        return prop;
                    },
                    set(val) {
                        prop = val;
                        return val;
                    },
                    inputType: 'text',
                },
            ]);
            const input = SpecHelper.expectElement(elem, 'input[type="text"]');
            expect(input.val()).toBe('initial value');
            SpecHelper.updateTextInput(elem, 'input', 'some text');
            expect(prop).toBe('some text');
        });

        describe('with number input', () => {
            it('should add number-specific attributes', () => {
                prop = 1;
                render([
                    {
                        type: 'inputGetterSetter',
                        title: 'Property',
                        get() {
                            return prop;
                        },
                        set(val) {
                            prop = val;
                            return val;
                        },
                        inputType: 'number',
                        min: 1,
                        max: 42,
                        step: 13,
                    },
                ]);
                const input = SpecHelper.expectElement(elem, 'input[type="number"]');
                expect(input.attr('min')).toBe('1');
                expect(input.attr('max')).toBe('42');
                expect(input.attr('step')).toBe('13');
            });
        });
    });

    function render(options, modelOptions) {
        const renderer = SpecHelper.renderer();
        model = frame.addVanillaComponent(modelOptions);
        editorViewModel = renderer.scope.editorViewModel = frame.editorViewModelFor(model);
        renderer.scope.options = options;
        renderer.render(
            '<cf-user-defined-options editor-view-model="editorViewModel" options="options"></cf-user-defined-options>',
        );
        elem = renderer.elem;
    }
});
