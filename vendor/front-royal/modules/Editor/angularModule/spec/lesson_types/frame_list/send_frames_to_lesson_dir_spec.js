import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

describe('Lessons.SendFramesToLessonDir', () => {
    let renderer;
    let scope;
    let sampleLesson;
    let otherLessons;
    let allLessons;
    let elem;
    let Lesson;
    let FrameList;
    let Frame;
    let playerViewModel;
    let SpecHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject($injector => {
            $injector.get('LessonFixtures');
            $injector.get('MockIguana');
            Lesson = $injector.get('Lesson');
            FrameList = $injector.get('Lesson.FrameList');
            Frame = $injector.get('Lesson.FrameList.Frame');
            SpecHelper = $injector.get('SpecHelper');
            jest.spyOn(Frame.prototype, 'getKeyTerms').mockReturnValue([]);

            sampleLesson = FrameList.fixtures.getInstance();
            SpecHelper.stubEventLogging();

            otherLessons = [
                FrameList.fixtures.getInstance(),
                FrameList.fixtures.getInstance(),
                FrameList.fixtures.getInstance(),
            ];
            allLessons = otherLessons.concat(sampleLesson);

            renderer = SpecHelper.renderer();
            playerViewModel = sampleLesson.createPlayerViewModel({
                editorMode: true,
            });
            renderer.scope.playerViewModel = playerViewModel;

            Lesson.expect('index').returns(allLessons);
            renderer.render('<send-frames-to-lesson player-view-model="playerViewModel"></send-frames-to-lesson>');
            Lesson.flush('index');

            elem = renderer.elem;
            scope = renderer.childScope;

            scope.$digest();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('target lessons', () => {
        it('should pull a list of all lessons upon load, excluding the current lesson', () => {
            const mapToId = lesson => lesson.id;
            // we return all lessons, but post-filtering should only contain other lessons
            expect(scope.lessons.map(mapToId)).toEqual(otherLessons.map(mapToId));
        });

        it('should pull a list of all lessons upon load but not excluide the current lesson if an older version', () => {
            scope.playerViewModel.lesson.old_version = true;
            scope.lessons = allLessons; // force refresh
            scope.$digest();
            const mapToId = lesson => lesson.id;
            expect(scope.lessons.map(mapToId)).toEqual(allLessons.map(mapToId));
        });

        it('should initially display a dropdown of all lessons to select target lesson', () => {
            SpecHelper.expectElement(elem, 'selectize[name="target_lessons"]');
        });
    });

    describe('copyFrames', () => {
        beforeEach(() => {
            scope.playerViewModel.lesson.old_version = false;
            scope.targetLesson = otherLessons[0];
            scope.$digest();
        });

        it('should display an options menu when a target lesson is set', () => {
            SpecHelper.expectElement(elem, '.options-container');
            SpecHelper.expectElements(elem, '.options-container input[type="radio"][name="mode"]', 2);
            SpecHelper.expectElement(elem, '.options-container select[name="start_index"]');
            SpecHelper.expectElement(elem, '.options-container select[name="end_index"]');
            SpecHelper.expectElement(elem, 'input[type="checkbox"][name="prune_source"]');
            SpecHelper.expectElement(elem, 'button[type="button"][name="copy_frames"]');
        });

        it('should not display the move checkbox if the source lesson is not editable', () => {
            scope.playerViewModel.lesson.old_version = true;
            scope.$digest();
            SpecHelper.expectElement(elem, '.options-container');
            SpecHelper.expectElements(elem, '.options-container input[type="radio"][name="mode"]', 2);
            SpecHelper.expectElement(elem, '.options-container select[name="start_index"]');
            SpecHelper.expectElement(elem, '.options-container select[name="end_index"]');
            SpecHelper.expectNoElement(elem, 'input[type="checkbox"][name="prune_source"]');
            SpecHelper.expectElement(elem, 'button[type="button"][name="copy_frames"]');
        });

        it('should copy the active frame if in active mode', () => {
            expect(scope.mode).toEqual('active'); // default

            const spy = jest.spyOn(scope.targetLesson, 'importFrames');

            FrameList.expect('update');
            SpecHelper.click(elem, 'button[type="button"][name="copy_frames"]');
            FrameList.flush('update');

            expect(spy).toHaveBeenCalledWith([scope.playerViewModel.activeFrame]);
            expect(scope.targetLesson).toBeUndefined();
            expect(scope.complete).toBe(true);
        });

        it('should copy the frame range if in ranged mode', () => {
            expect(scope.mode).toEqual('active'); // default
            SpecHelper.toggleRadio(elem, '.options-container input[type="radio"][name="mode"]', 1);
            expect(scope.mode).toEqual('range');

            SpecHelper.selectOptionByValue(elem, '.options-container select[name="start_index"]', 'number:0');
            SpecHelper.selectOptionByValue(elem, '.options-container select[name="end_index"]', 'number:1');
            expect(scope.startIndex).toEqual(0);
            expect(scope.endIndex).toEqual(1);
            const frames = [scope.playerViewModel.lesson.frames[0], scope.playerViewModel.lesson.frames[1]];

            const spy = jest.spyOn(scope.targetLesson, 'importFrames');

            FrameList.expect('update');
            SpecHelper.click(elem, 'button[type="button"][name="copy_frames"]');
            FrameList.flush('update');

            expect(spy).toHaveBeenCalledWith(frames);
            expect(scope.targetLesson).toBeUndefined();
            expect(scope.complete).toBe(true);
        });

        it('should prune the original lesson of the sent frames if pruning is enabled', () => {
            expect(scope.pruneSource).toEqual(false); // default
            SpecHelper.toggleCheckbox(elem, 'input[type="checkbox"][name="prune_source"]');
            scope.$apply();
            expect(scope.pruneSource).toEqual(true);

            jest.spyOn(scope.playerViewModel, 'removeFrames').mockImplementation(() => {});

            const expectedFrame = scope.playerViewModel.activeFrame;

            FrameList.expect('update');
            SpecHelper.click(elem, 'button[type="button"][name="copy_frames"]');
            FrameList.flush('update');

            expect(scope.playerViewModel.removeFrames).toHaveBeenCalledWith([expectedFrame]);
        });
    });
});
