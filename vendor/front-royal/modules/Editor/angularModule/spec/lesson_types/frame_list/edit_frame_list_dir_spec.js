import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Users/angularModule/spec/_mock/fixtures/users';
import setSpecLocales from 'Translation/setSpecLocales';
import lessonPlayerViewModelLocales from 'Lessons/locales/lessons/models/lesson/lesson_player_view_model-en.json';

setSpecLocales(lessonPlayerViewModelLocales);

/* eslint-disable lodash-fp/prefer-constant */
describe('Lessons.EditFrameListDir', () => {
    let $injector;
    let renderer;
    let scope;
    let sampleLesson;
    let elem;
    let FrameList;
    let Frame;
    let FrameListPlayerViewModel;
    let playerViewModel;
    let SpecHelper;
    let locationMock;
    let Config;
    let EntityMetadata;
    let currentUser;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        locationMock = {
            url() {},
            hash() {},
            search() {
                return {};
            },
            path() {
                return '';
            },
        };
        angular.mock.module($provide => {
            $provide.value('$location', locationMock);
        });

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;

            FrameList = $injector.get('Lesson.FrameList');
            FrameListPlayerViewModel = $injector.get('Lesson.FrameList.FrameListPlayerViewModel');
            Frame = $injector.get('Lesson.FrameList.Frame');
            SpecHelper = $injector.get('SpecHelper');
            Config = $injector.get('Config');
            EntityMetadata = $injector.get('EntityMetadata');
            $injector.get('LessonFixtures');
            $injector.get('UserFixtures');
            $injector.get('MockIguana');
            SpecHelper.stubDirective('contentTopicsEditor');
            SpecHelper.stubDirective('contentItemEditLocale');

            SpecHelper.stubEventLogging();
            SpecHelper.stubConfig();

            currentUser = SpecHelper.stubCurrentUser('super_editor');

            sampleLesson = FrameList.fixtures.getInstance();
            render();
        });
    });
    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('initialization / cleanup', () => {
        it('should set the body background to white', () => {
            const bodyElem = $('body');
            SpecHelper.expectElementHasClass(bodyElem, 'bg-white');
        });

        it('should include an info panel', () => {
            expect(elem.find(playerViewModel.activeFrame.infoPanelDirectiveName).length).toBe(1);
        });

        it('should cleanup playerViewModel on destroy', () => {
            jest.spyOn(scope.playerViewModel, 'destroy').mockImplementation(() => {});
            scope.$destroy();
            expect(scope.playerViewModel.destroy).toHaveBeenCalled();
        });
    });

    describe('thumbnails', () => {
        it('should show a list of frame thumbnails on the left side', () => {
            expect(elem.find('.edit-frame-list-thumbnails').length).toBe(1);
        });

        it('should show the active frame and change when a thumbnail is clicked', () => {
            const fakeDirectiveName = Frame.prototype.directiveName;
            expect(elem.find(fakeDirectiveName).length).toBe(1);
            expect(scope.playerViewModel.activeFrame).toBe(sampleLesson.frames[0]);
            expect(scope.activeFrameViewModel.frame).toBe(sampleLesson.frames[0]);

            elem.find('.frame_thumbnail').eq(1).click();
            expect(scope.playerViewModel.activeFrame).toBe(sampleLesson.frames[1]);
            expect(scope.activeFrameViewModel.frame).toBe(sampleLesson.frames[1]);
        });
    });

    describe('navigation tabs', () => {
        describe('frame tab', () => {
            it('should be visible to editors', () => {
                SpecHelper.stubCurrentUser('editor', sampleLesson, 'lesson_editor');
                SpecHelper.click(elem, '[data-id="frame_tab"]');
                SpecHelper.expectElement(elem, 'fake-info-panel-name');
            });

            it('should be visible to reviewers', () => {
                SpecHelper.stubCurrentUser('editor', sampleLesson, 'reviewer');
                render();
                SpecHelper.click(elem, '[data-id="frame_tab"]');
                SpecHelper.expectElement(elem, 'fake-info-panel-name');
            });

            it('should not be visible to editors without lesson permissions', () => {
                SpecHelper.stubCurrentUser('editor');
                render();
                SpecHelper.expectNoElement(elem, '[data-id="frame_tab"]');
            });
        });

        describe('comments tab', () => {
            it('should be visible to editors', () => {
                SpecHelper.stubCurrentUser('editor', sampleLesson, 'lesson_editor');
                SpecHelper.click(elem, '[data-id="comments_tab"]');
                SpecHelper.expectElement(elem, 'frame-author-comments');
            });

            it('should be visible to reviewers', () => {
                SpecHelper.stubCurrentUser('editor', sampleLesson, 'reviewer');
                render();
                SpecHelper.click(elem, '[data-id="comments_tab"]');
                SpecHelper.expectElement(elem, 'frame-author-comments');
            });

            it('should not be visible to editors without lesson permissions', () => {
                SpecHelper.stubCurrentUser('editor');
                render();
                SpecHelper.expectNoElement(elem, '[data-id="comments_tab"]');
            });
        });

        describe('module tab', () => {
            it('should be visible to editors regardless of lesson access', () => {
                SpecHelper.stubCurrentUser('editor');
                SpecHelper.click(elem, '[data-id="module_tab"]');
                SpecHelper.expectElement(elem, 'textarea[name="lesson_title"]');
            });

            it('should not be available as a tab, rather an isolated pane if not an editor', () => {
                SpecHelper.stubCurrentUser('editor');
                render();
                SpecHelper.expectNoElement(elem, '[data-id="module_tab"]');
                SpecHelper.expectElement(elem, 'textarea[name="lesson_title"]');
            });
        });
    });

    describe('preloading', () => {
        it('should preload all images', () => {
            jest.spyOn(FrameListPlayerViewModel.prototype, 'preloadAllImages').mockImplementation(() => {});
            render();
            expect(FrameListPlayerViewModel.prototype.preloadAllImages).toHaveBeenCalled();
        });
    });

    describe('duplicate', () => {
        it('should launch duplicate modal when Duplicate is clicked', () => {
            const DialogModal = $injector.get('DialogModal');
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            SpecHelper.click(elem, '[data-id="duplicate"]');
            expect(DialogModal.alert).toHaveBeenCalledWith({
                content: '<duplicate-content-item content-item="contentItem"></duplicate-content-item>',
                scope: {
                    contentItem: sampleLesson,
                },
                classes: ['overflow-visible'],
                size: 'small',
            });
        });
    });

    describe('pinning', () => {
        it('should launch saveAndPin dialog when save and pin is clicked', () => {
            const DialogModal = $injector.get('DialogModal');
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            SpecHelper.click(elem, '[data-id="save_and_pin"]');
            expect(DialogModal.alert).toHaveBeenCalledWith({
                content:
                    '<pin-and-publish-lesson lesson="lesson" should-publish="shouldPublish"></pin-and-publish-lesson>',
                scope: {
                    lesson: sampleLesson,
                    shouldPublish: false,
                },
                classes: ['pin-and-publish-modal'],
            });
        });
    });

    describe('tabs', () => {
        it('should show appropriate tabs based on editability of lesson', () => {
            SpecHelper.expectElementNotHidden(elem, '.frame_label');
            SpecHelper.expectElement(elem, '.tab_count_3');
            jest.spyOn(currentUser, 'canEditLesson').mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectElementHidden(elem, '.frame_label');
            SpecHelper.expectElement(elem, '.tab_count_2');
        });
    });

    describe('description', () => {
        it('should add description items', () => {
            scope.playerViewModel.lesson.description = ['stuff.'];
            scope.addDescriptionItem();
            scope.$digest();
            SpecHelper.expectEqual(scope.playerViewModel.lesson.description, ['stuff.', '']);
        });

        it('should remove description items', () => {
            scope.playerViewModel.lesson.description = ['stuff', 'things'];
            scope.removeDescriptionItem(1);
            scope.$digest();
            SpecHelper.expectEqual(scope.playerViewModel.lesson.description, ['stuff']);
            scope.removeDescriptionItem(0);
            scope.$digest();
            SpecHelper.expectEqual(scope.playerViewModel.lesson.description, []);
        });
    });

    describe('seo updating', () => {
        it('should update seo title and canonical url from title if both are locked', () => {
            scope.playerViewModel.seoTitleLocked = true;
            scope.playerViewModel.seoCanonicalUrlLocked = true;
            scope.$digest();
            scope.playerViewModel.lesson.title = 'cats';
            scope.$digest();
            SpecHelper.expectEqual(scope.playerViewModel.lesson.entity_metadata.title, 'cats');
            SpecHelper.expectEqual(
                scope.playerViewModel.lesson.entity_metadata.canonical_url,
                `/lesson/cats/${scope.playerViewModel.lesson.id}`,
            );
        });

        it('should not update seo title and canonical url from title if both are unlocked', () => {
            scope.playerViewModel.seoTitleLocked = false;
            scope.playerViewModel.seoCanonicalUrlLocked = false;
            scope.$digest();
            scope.playerViewModel.lesson.entity_metadata.title = 'dogs';
            scope.playerViewModel.lesson.entity_metadata.canonical_url = 'dogs.com';
            scope.playerViewModel.lesson.title = 'cats';
            scope.$digest();
            SpecHelper.expectEqual(scope.playerViewModel.lesson.entity_metadata.title, 'dogs');
            SpecHelper.expectEqual(scope.playerViewModel.lesson.entity_metadata.canonical_url, 'dogs.com');
        });

        it('should update only seo title from title if just seo_title is locked', () => {
            scope.playerViewModel.seoTitleLocked = true;
            scope.playerViewModel.seoCanonicalUrlLocked = false;
            scope.$digest();
            scope.playerViewModel.lesson.entity_metadata.title = 'dogs';
            scope.playerViewModel.lesson.entity_metadata.canonical_url = 'dogs.com';
            scope.playerViewModel.lesson.title = 'cats';
            scope.$digest();
            SpecHelper.expectEqual(scope.playerViewModel.lesson.entity_metadata.title, 'cats');
            SpecHelper.expectEqual(scope.playerViewModel.lesson.entity_metadata.canonical_url, 'dogs.com');
        });

        it('should update seo description from description if seo_description is locked', () => {
            scope.playerViewModel.seoDescriptionLocked = true;
            scope.$digest();
            scope.playerViewModel.lesson.description = ['all about cats.', 'other stuff'];
            scope.$digest();
            SpecHelper.expectEqual(
                scope.playerViewModel.lesson.entity_metadata.description,
                'all about cats. other stuff',
            );
        });

        it('should not update seo description from description if seo_description is unlocked', () => {
            scope.playerViewModel.seoDescriptionLocked = false;
            scope.$digest();
            scope.playerViewModel.lesson.entity_metadata.description = ['all about dogs'];
            scope.playerViewModel.lesson.description = ['all about cats'];
            scope.$digest();
            SpecHelper.expectEqual(scope.playerViewModel.lesson.entity_metadata.description, ['all about dogs']);
        });

        it('should not revert back to original values if unlocked and then locked again', () => {
            scope.playerViewModel.lesson.title = 'cats';
            scope.playerViewModel.seoTitleLocked = true;
            scope.$digest();
            SpecHelper.expectEqual(scope.playerViewModel.lesson.entity_metadata.title, 'cats');
            scope.playerViewModel.seoTitleLocked = false;
            scope.playerViewModel.lesson.entity_metadata.title = 'dogs';
            scope.$digest();
            SpecHelper.expectEqual(scope.playerViewModel.lesson.entity_metadata.title, 'dogs');
            scope.playerViewModel.seoTitleLocked = true;
            scope.$digest();
            SpecHelper.expectEqual(scope.playerViewModel.lesson.entity_metadata.title, 'cats');
        });
    });

    describe('editor pref toggling', () => {
        it('should allow for the enabling or disabling of message hint hovers', () => {
            const ClientStorage = $injector.get('ClientStorage');
            jest.spyOn(ClientStorage, 'setItem').mockImplementation(() => {});

            SpecHelper.toggleCheckbox(elem, '.show-message-icons input[type="checkbox"].ng-not-empty');

            expect(ClientStorage.setItem).toHaveBeenCalledWith('showEditorMessageIcons', false);

            SpecHelper.toggleCheckbox(elem, '.show-message-icons input[type="checkbox"].ng-empty');

            expect(ClientStorage.setItem).toHaveBeenCalledWith('showEditorMessageIcons', true);
        });

        it('should allow for the enabling or disabling of correct/incorrect answer icons', () => {
            const ClientStorage = $injector.get('ClientStorage');
            jest.spyOn(ClientStorage, 'setItem').mockImplementation(() => {});

            SpecHelper.toggleCheckbox(elem, '.show-correct-incorrect-icons input[type="checkbox"].ng-not-empty');

            expect(ClientStorage.setItem).toHaveBeenCalledWith('showCorrectIncorrectIcons', false);

            SpecHelper.toggleCheckbox(elem, '.show-correct-incorrect-icons input[type="checkbox"].ng-empty');

            expect(ClientStorage.setItem).toHaveBeenCalledWith('showCorrectIncorrectIcons', true);
        });
    });

    function render() {
        Config.expect('index');
        sampleLesson.entity_metadata = EntityMetadata.new({
            title: 'askjhsdf',
            description: 'sdjfhsdf',
            canonical_url: 'www.awesome/com',
        });
        playerViewModel = sampleLesson.createPlayerViewModel({
            editorMode: true,
        });

        renderer = SpecHelper.renderer();
        renderer.scope.playerViewModel = playerViewModel;
        renderer.render('<edit-frame-list player-view-model="playerViewModel" />');
        elem = renderer.elem;
        scope = renderer.childScope;
    }
});
