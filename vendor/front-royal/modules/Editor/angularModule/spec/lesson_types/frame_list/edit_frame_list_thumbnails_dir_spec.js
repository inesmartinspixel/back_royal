import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

describe('Lessons.EditFrameListThumbnailsDir', () => {
    let renderer;
    let scope;
    let sampleLesson;
    let otherLessons;
    let elem;
    let Lesson;
    let FrameList;
    let Frame;
    let playerViewModel;
    let SpecHelper;
    let DialogModal;
    let $q;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject($injector => {
            $injector.get('LessonFixtures');
            $injector.get('MockIguana');
            Lesson = $injector.get('Lesson');
            FrameList = $injector.get('Lesson.FrameList');
            Frame = $injector.get('Lesson.FrameList.Frame');
            SpecHelper = $injector.get('SpecHelper');
            DialogModal = $injector.get('DialogModal');
            $q = $injector.get('$q');
            $timeout = $injector.get('$timeout');
            SpecHelper.stubEventLogging();
            SpecHelper.stubCurrentUser('editor');

            sampleLesson = FrameList.fixtures.getInstance().reifyAllFrames();

            otherLessons = [
                Lesson.fixtures.getInstance(),
                Lesson.fixtures.getInstance(),
                Lesson.fixtures.getInstance(),
            ];
            const allLessons = otherLessons.push(sampleLesson);

            renderer = SpecHelper.renderer();
            playerViewModel = sampleLesson.createPlayerViewModel({
                editorMode: true,
            });
            renderer.scope.playerViewModel = playerViewModel;

            Lesson.expect('index').returns(allLessons);
            render();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('hasBranching', () => {
        it('should be false by default', () => {
            SpecHelper.stubCurrentUser('super_editor');
            scope.$digest();

            const branchingIndicatorOverlays = angular.element(renderer.elem).find('.frame_branch_overlay');
            expect(branchingIndicatorOverlays.length).toBe(0);
        });

        it('should display a branching icon when a frame is points to another frame', () => {
            SpecHelper.stubCurrentUser('super_editor');
            render();

            const frame = sampleLesson.frames[0];
            const pointedToframe = sampleLesson.frames[1];
            const navigationPointers = {};
            navigationPointers[frame.id] = {
                to: [pointedToframe.id],
                from: [],
            };

            jest.spyOn(playerViewModel.lesson, 'createNavigationPointers').mockReturnValue(navigationPointers);
            scope.$digest();

            SpecHelper.expectElements(renderer.elem, '.icons-group.branching .icon.branching', 1);
            SpecHelper.expectElements(renderer.elem, '.icons-group.branching .icon.frame', 1);
        });
    });

    describe('hasMerging', () => {
        it('should be false by default', () => {
            SpecHelper.stubCurrentUser('super_editor');
            scope.$digest();

            const mergingIndicatorOverlays = angular.element(renderer.elem).find('.frame_merge_overlay');
            expect(mergingIndicatorOverlays.length).toBe(0);
        });

        it('should display a merging icon when a frame is pointed to by another frame', () => {
            SpecHelper.stubCurrentUser('super_editor');
            render();

            const frame = sampleLesson.frames[0];
            const pointedFromframe = sampleLesson.frames[1];
            const navigationPointers = {};
            navigationPointers[frame.id] = {
                from: [pointedFromframe.id],
                to: [],
            };
            jest.spyOn(playerViewModel.lesson, 'createNavigationPointers').mockReturnValue(navigationPointers);
            scope.$digest();

            SpecHelper.expectElements(renderer.elem, '.icons-group.merging .icon.merging', 1);
            SpecHelper.expectElements(renderer.elem, '.icons-group.merging .icon.frame', 1);
        });
    });

    describe('thumbnail display', () => {
        it('should have proper styles as an editor user', () => {
            SpecHelper.stubCurrentUser('editor');
            scope.currentUser.roles = [
                {
                    name: 'editor',
                    resource_id: null,
                },
                {
                    name: 'lesson_editor',
                    resource_type: 'Lesson',
                    resource_id: sampleLesson.id,
                },
            ];
            scope.$digest();
            const thumbnails = angular.element(renderer.elem).find('.frame_thumbnail_container .frame_thumbnail');
            expect(thumbnails.length).toBe(sampleLesson.frames.length);
        });

        it('should have proper styles as an non-editor user', () => {
            SpecHelper.stubCurrentUser('editor');
            scope.currentUser.roles = [
                {
                    name: 'editor',
                    resource_id: null,
                },
                {
                    name: 'reviewer',
                    resource_type: 'Lesson',
                    resource_id: sampleLesson.id,
                },
            ];
            scope.$digest();
            const sortableThumbnails = angular.element(renderer.elem).find('ul[sortable]');
            expect(sortableThumbnails.length).toBe(0);
        });

        it('should render the active frame as selected', () => {
            const index = sampleLesson.frames.indexOf(playerViewModel.activeFrame);
            expect(elem.find('.frame_thumbnail_container').eq(index).get(0).classList.contains('selected')).toBe(true);
        });

        it('should add exceeds_frame_list styling', () => {
            const currentUser = SpecHelper.stubCurrentUser('editor');
            jest.spyOn(currentUser, 'canEditLesson').mockReturnValue(true);
            const invalidFrameIds = {};
            const validateTextLengths = (Frame.prototype.validateTextLengths = jest.fn());
            const frames = sampleLesson.frames;
            validateTextLengths.mockImplementation(function () {
                const frame = this;
                return $q(resolve => {
                    resolve({
                        valid: !invalidFrameIds[frame.id],
                    });
                });
            });

            render();
            $timeout.flush(); // flush the promises

            // rendering should have called validateTextLengths once for each frame,
            // and since none of them are invalid, exceeds_frame_list should not show up
            SpecHelper.expectNoElement(elem, '.exceeds_frame_list');
            expect(validateTextLengths.mock.calls.length).toBe(frames.length);

            invalidFrameIds[frames[0].id] = true;
            validateTextLengths.mockClear();
            scope.$digest();

            // even though one frame is now invalid, we should not call
            // validateTextLengths again until the active frame changes
            expect(validateTextLengths).not.toHaveBeenCalled();

            // changing the active frame should trigger a new check
            // of the text lengths
            scope.playerViewModel.activeFrameIndex = scope.playerViewModel.activeFrameIndex + 1;
            scope.$digest();
            $timeout.flush(); // flush the promises

            // now the first frame should show the exceeds_frame_list
            // class
            SpecHelper.expectElement(elem, '.frame_thumbnail_container.exceeds_text_limit:eq(0)');
        });

        it('should add contains_search_term styling', () => {
            const currentUser = SpecHelper.stubCurrentUser('editor');
            jest.spyOn(currentUser, 'canEditLesson').mockReturnValue(true);

            sampleLesson.frames[0].text_content = 'something';

            render();
            $timeout.flush(); // flush the promises

            // by default, should not be set
            expect(scope.searchTerm).toBeUndefined();
            SpecHelper.expectNoElement(elem, '.contains_search_term');

            // specify a searchterm
            SpecHelper.updateInput(elem, '.quick-search input', 'soMethIng'); // case insensitive
            $timeout.flush(); // flush the debounce promise

            // now the first frame should show the contains_search_term class
            SpecHelper.expectElements(elem, '.contains_search_term', 1);
        });
    });

    describe('thumbnail actions', () => {
        let canEditLessonSpy;

        beforeEach(() => {
            const currentUser = SpecHelper.stubCurrentUser('editor');
            canEditLessonSpy = jest.spyOn(currentUser, 'canEditLesson').mockReturnValue(true);
            render();
        });

        it('should add show and hide actions as appropriate', () => {
            SpecHelper.expectNoElement(elem, '.show_actions');
            scope.showActions = true;
            scope.$digest();
            SpecHelper.expectElement(elem, '.show_actions');
        });

        it('should insert a frame after the current frame when button is pressed', () => {
            jest.spyOn(sampleLesson, 'addFrame');
            const expectedIndex = playerViewModel.activeFrameIndex + 1;
            clickButton('add_frame');
            expect(sampleLesson.addFrame).toHaveBeenCalledWith(null, expectedIndex);
        });

        it('should duplicate a frame when button is pressed', () => {
            jest.spyOn(sampleLesson, 'duplicateFrame').mockImplementation(() => {});
            clickButton('duplicate_frame');
            expect(sampleLesson.duplicateFrame).toHaveBeenCalledWith(playerViewModel.activeFrame);
        });

        it('should render a modal menu when send frames button is pressed', () => {
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            clickButton('send_frames');
            expect(DialogModal.alert).toHaveBeenCalled();
            const modalArgs = _.last(DialogModal.alert.mock.calls)[0];
            expect(modalArgs.content).toEqual(
                '<send-frames-to-lesson player-view-model="playerViewModel"></send-frames-to-lesson>',
            );
            expect(modalArgs.scope.playerViewModel).toEqual(playerViewModel);
        });

        it('should remove a frame when button is pressed', () => {
            jest.spyOn(playerViewModel, 'removeFrame').mockImplementation(() => {});
            clickButton('remove_frame');
            expect(playerViewModel.removeFrame).toHaveBeenCalledWith(playerViewModel.activeFrame);
        });

        it('should disable the frame remove button if no frames exist', () => {
            SpecHelper.expectElementEnabled(elem, 'button[name="remove_frame"]'); // sanity check
            scope.lesson.frames = [];
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, 'button[name="remove_frame"]');
        });

        it('should be able to remove the last frame', () => {
            scope.lesson.frames = [scope.lesson.frames[0]];
            scope.$digest();
            expect(() => {
                clickButton('remove_frame');
            }).not.toThrowError();
        });

        it('should be sortable if user is editor', () => {
            const currentUser = SpecHelper.stubCurrentUser('editor');
            jest.spyOn(currentUser, 'canEditLesson').mockReturnValue(true);
            render();
            SpecHelper.expectElements(elem, 'ul[sortable]', 1);
        });

        it('should not be sortable if user is not editor', () => {
            SpecHelper.stubCurrentUser('editor');
            canEditLessonSpy.mockReturnValue(false);
            scope.currentUser.roles = [
                {
                    name: 'editor',
                    resource_id: null,
                },
                {
                    name: 'previewer',
                    resource_type: 'Lesson',
                    resource_id: sampleLesson.id,
                },
            ];
            scope.$digest();
            const sortableThumbnails = angular.element(renderer.elem).find('ul[sortable]');
            expect(sortableThumbnails.length).toBe(0);
        });

        it('should dirty a parent form if provided', () => {
            renderer.render(
                '<form name="editForm"><edit-frame-list-thumbnails player-view-model="playerViewModel" /></form>',
            );
            elem = renderer.elem;
            scope = renderer.scope;

            jest.spyOn(playerViewModel, 'removeFrame').mockImplementation(() => {});
            clickButton('remove_frame');
            expect(scope.editForm.$dirty).toBe(true);
        });
    });

    function clickButton(name) {
        const button = elem.find(`[name="${name}"]`);
        expect(button.length).toBe(1);
        button.click();
    }

    function render() {
        renderer.render(
            '<form name="editForm"><edit-frame-list-thumbnails player-view-model="playerViewModel" /></form>',
        );
        elem = renderer.elem;
        scope = elem.find('edit-frame-list-thumbnails').isolateScope();
    }

    // I wanted to test the window resizing and frame dropping, but failed in both cases
});
