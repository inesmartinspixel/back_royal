import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

describe('Editor.EditLessonDir', () => {
    let translatableLessonExportSet;
    let scope;
    let elem;
    let SpecHelper;
    let Lesson;
    let lesson;
    let TranslatableLessonExportSet;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject($injector => {
            Lesson = $injector.get('Lesson');
            $injector.get('LessonFixtures');
            SpecHelper = $injector.get('SpecHelper');
            SpecHelper.stubDirective('contentTopicsEditor');
            SpecHelper.stubDirective('contentItemEditLocale');
            lesson = Lesson.fixtures.getInstance();
            TranslatableLessonExportSet = $injector.get('TranslatableLessonExportSet');
            Lesson.setAdapter('Iguana.Mock.Adapter');

            translatableLessonExportSet = new TranslatableLessonExportSet([lesson], ['copyright']);
            render();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('translatableLessonExport', () => {
        it('should show status message while preparing and then display links', () => {
            // when there is no translatableLessonExportSet,
            // no status message should show
            SpecHelper.expectNoElement(elem, '.status-message');
            SpecHelper.expectNoElement(elem, '[name="download-links"]');

            // when translatableLessonExportSet is set,
            // prepare should be called and status message should show
            const resolve = SpecHelper.mockTranslatableLessonExportSetPrepare().resolve;
            scope.translatableLessonExportSet = translatableLessonExportSet;
            scope.$digest();
            SpecHelper.expectElement(elem, '.status-message');
            SpecHelper.expectNoElement(elem, '[name="download-links"]');

            // when prepared, links should show up
            resolve();
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.status-message');
            SpecHelper.expectElement(elem, '[name="download-links"]');

            // Clicking on links should call appropriate methods
            jest.spyOn(scope.translatableLessonExportSet, 'downloadTranslatableArtifacts').mockImplementation(() => {});
            SpecHelper.click(elem, '[name="download"]');
            expect(scope.translatableLessonExportSet.downloadTranslatableArtifacts).toHaveBeenCalled();
        });

        it('should show hdie show links if more than two files have been created', () => {
            translatableLessonExportSet = new TranslatableLessonExportSet(
                [lesson, lesson, lesson],
                ['copyright'],
                'filename.zip',
            );
            render();
            const resolve = SpecHelper.mockTranslatableLessonExportSetPrepare().resolve;
            scope.translatableLessonExportSet = translatableLessonExportSet;
            resolve();
            scope.$digest();

            SpecHelper.expectNoElement(elem, '[name="download-links"] [name="open"]');
            SpecHelper.expectElement(elem, '[name="download-links"] [name="download"]');
        });
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.scope.translatableLessonExportSet = undefined;
        renderer.render(
            '<translatable-lesson-export-set translatable-lesson-export-set="translatableLessonExportSet" />',
        );
        elem = renderer.elem;
        scope = renderer.childScope;
    }
});
