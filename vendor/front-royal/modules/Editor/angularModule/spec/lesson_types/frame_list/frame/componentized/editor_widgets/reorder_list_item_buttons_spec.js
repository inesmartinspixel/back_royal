import 'AngularSpecHelper';
import 'Editor/angularModule';

describe('cf-reorder-list-item-buttons', () => {
    let elem;
    let parentScope;
    let scope;
    let SpecHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);

        render();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should display up and down ordering buttons', () => {
        render();
        SpecHelper.expectElement(elem, 'button[name="moveUp"]');
        SpecHelper.expectElement(elem, 'button[name="moveDown"]');
    });

    it('should disable up button when item is first', () => {
        render();
        scope.item = 'guid_1';
        parentScope.collection = ['guid_1', 'guid_2'];
        scope.$apply();
        const up = SpecHelper.expectElement(elem, 'button[name="moveUp"]');
        SpecHelper.expectEqual(true, up.prop('disabled'), 'up button disabled');
    });

    it('should disable down button when item is last', () => {
        render();
        scope.item = 'guid_2';
        parentScope.collection = ['guid_1', 'guid_2'];
        scope.$apply();
        const down = SpecHelper.expectElement(elem, 'button[name="moveDown"]');
        SpecHelper.expectEqual(true, down.prop('disabled'), 'up button disabled');
    });

    it('should disable both buttons when item is not in list', () => {
        render();
        scope.item = 'guid_3';
        parentScope.collection = ['guid_1', 'guid_2'];
        scope.$apply();
        const up = SpecHelper.expectElement(elem, 'button[name="moveUp"]');
        SpecHelper.expectEqual(true, up.prop('disabled'), 'up button disabled');
        const down = SpecHelper.expectElement(elem, 'button[name="moveDown"]');
        SpecHelper.expectEqual(true, down.prop('disabled'), 'up button disabled');
    });

    it('should add a move method on the scope', () => {
        render();

        scope.item = 'guid_1';
        parentScope.collection = ['guid_1', 'guid_2'];
        scope.$apply();

        expect(scope.move).not.toBeUndefined();

        scope.move(1);
        scope.$digest();
        expect(scope.collection.indexOf(scope.item)).toEqual(1);

        scope.move(1);
        scope.$digest();
        expect(scope.collection.indexOf(scope.item)).toEqual(1); // constrained

        scope.move(-1);
        scope.$digest();
        expect(scope.collection.indexOf(scope.item)).toEqual(0);

        scope.move(-1);
        scope.$digest();
        expect(scope.collection.indexOf(scope.item)).toEqual(0); // constrained
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<cf-reorder-list-item-buttons item="item" collection="collection"></cf-component-editor>');
        elem = renderer.elem;
        parentScope = renderer.scope;
        scope = parentScope.$$childHead;
    }
});
