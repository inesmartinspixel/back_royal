import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';

describe('cf-image-selector', () => {
    let frame;
    let viewModel;
    let Componentized;
    let SpecHelper;
    let $timeout;
    let elem;
    let scope;
    let model;
    let anotherImage;
    let ImageOptions;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                $timeout = $injector.get('$timeout');
                ImageOptions = $injector.get('cfImageSelectorImageOptions');

                frame = Componentized.fixtures.getInstance();
                viewModel = getViewModel();

                anotherImage = frame.addImage();
                anotherImage.label = 'another image';
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should initially show (No Image) as selected', () => {
        renderWithNoImageSelected();
        const firstOption = SpecHelper.expectElement(elem, 'select option:eq(0)');
        expect(firstOption.text()).toEqual('(No Image)');
        expect(firstOption.attr('selected')).toEqual('selected');
    });

    it('should allow for selecting an another image', () => {
        render();
        expect(scope.imageModel).not.toBe(anotherImage);
        expect(scope.imageModel).not.toBeUndefined();
        SpecHelper.selectOptionByLabel(elem, 'select', '(1) another image');
        expect(scope.imageModel).toBe(anotherImage);
    });
    it('should allow for selecting no image', () => {
        render();
        expect(scope.imageModel).not.toBeUndefined();
        expect(elem.find('select option:eq(0)').attr('selected')).not.toEqual('selected'); // sanity check: None Option should not be selected
        SpecHelper.selectOptionByLabel(elem, 'select', '(No Image)');
        expect(scope.imageModel).toBeUndefined();
    });
    it('should delete the empty image component when switching to an another image', () => {
        render();
        const originalModel = scope.imageModel;
        originalModel.image = undefined;
        jest.spyOn(originalModel, 'remove').mockImplementation(() => {});
        SpecHelper.selectOptionByLabel(elem, 'select', '(1) another image');
        $timeout.flush();
        expect(originalModel.remove).toHaveBeenCalled();
    });
    it('should not delete a non-empty image component when switching to an another image', () => {
        render();
        const originalModel = scope.imageModel;
        originalModel.image = {
            formats: {
                original: {
                    url: 'stubImage',
                },
            },
        };
        jest.spyOn(originalModel, 'remove').mockImplementation(() => {});
        SpecHelper.selectOptionByLabel(elem, 'select', '(1) another image');
        $timeout.flush();
        expect(originalModel.remove).not.toHaveBeenCalled();
    });
    it('should be possible to disable the select', () => {
        render();
        SpecHelper.expectElementEnabled(elem, 'select');
        scope.disabled = true;
        scope.$digest();
        SpecHelper.expectElementDisabled(elem, 'select');
    });

    describe('with images in other frames', () => {
        let otherFrames;
        let image;

        beforeEach(() => {
            render();
            otherFrames = [Componentized.fixtures.getInstance(), Componentized.fixtures.getInstance()];
            otherFrames.forEach(otherFrame => {
                otherFrame.$$image = otherFrame.addImage({
                    label: `image for ${otherFrame.id}`,
                });
                frame.lesson().addFrame(otherFrame);
            });
            image = model;
            scope.$apply();
        });

        it('should show all images', () => {
            expect(elem.find('select').text()).toEqual(
                '(No Image)' +
                    '(1) another image' +
                    `(1) ${image.label}` +
                    `(2) ${otherFrames[0].$$image.label}` +
                    `(3) ${otherFrames[1].$$image.label}`,
            );
        });

        it('should only show duplicate images once', () => {
            const image1 = otherFrames[0].addImage({
                label: 'duplicate',
            });
            const image2 = otherFrames[1].addImage({
                label: 'duplicate',
            });
            image1.image.formats.original.url = 'duplicate/url.jpg?11111'; // stuff after the ? should be ignored
            image2.image.formats.original.url = 'duplicate/url.jpg?22222'; // stuff after the ? should be ignored
            // destroy existing scope so ImageCache is refreshed
            scope.$destroy();
            render();

            expect(elem.find('select').text()).toEqual(
                '(No Image)' +
                    '(1) another image' +
                    `(1) ${image.label}` +
                    '(2,3) duplicate' +
                    `(2) ${otherFrames[0].$$image.label}` +
                    `(3) ${otherFrames[1].$$image.label}`,
            );
        });

        it('should only regenerate the list when necessary', () => {
            jest.spyOn(ImageOptions.prototype, '_generateOptions');
            const oldScope = scope;

            // rendering another instance of the directive should not regenerate list
            render();
            expect(ImageOptions.prototype._generateOptions.mock.calls.length).toBe(0);

            // digest should not regenerate the list
            scope.$apply();
            expect(ImageOptions.prototype._generateOptions.mock.calls.length).toBe(0);

            // adding an image should regenerate the list
            const newImage = frame.addImage();
            scope.$digest();
            expect(ImageOptions.prototype._generateOptions.mock.calls.length).toBe(1);

            // removing an image should regenerate the list
            newImage.remove();
            scope.$digest();
            expect(ImageOptions.prototype._generateOptions.mock.calls.length).toBe(2);

            // reordering the frame list should regenerate the list
            frame.lesson().frames.$$sortableKey = 'original';
            scope.$digest();
            expect(ImageOptions.prototype._generateOptions.mock.calls.length).toBe(2);
            frame.lesson().frames.$$sortableKey = 'changed';
            scope.$digest();
            expect(ImageOptions.prototype._generateOptions.mock.calls.length).toBe(3);

            // rendering a different frame should regenerate the list
            oldScope.$destroy();
            scope.$destroy();
            render(otherFrames[0]);
            expect(ImageOptions.prototype._generateOptions.mock.calls.length).toBe(4);
            expect(elem.find('select').text()).toEqual(
                '(No Image)' +
                    `(2) ${otherFrames[0].$$image.label}` +
                    '(1) another image' +
                    `(1) ${image.label}` +
                    `(3) ${otherFrames[1].$$image.label}`,
            );

            // changing an image label should regenerate the list
            otherFrames[0].$$image.label = 'New Label';
            scope.$digest();
            expect(ImageOptions.prototype._generateOptions.mock.calls.length).toBe(5);
            expect(elem.find('select').text()).toEqual(
                '(No Image)' +
                    '(2) New Label' +
                    '(1) another image' +
                    `(1) ${image.label}` +
                    `(3) ${otherFrames[1].$$image.label}`,
            );
        });

        it('should destroy the cache on destroy', () => {
            jest.spyOn(ImageOptions, 'destroy').mockImplementation(() => {});
            scope.$destroy();
            expect(ImageOptions.destroy).toHaveBeenCalled();
        });

        it('should duplicate an image from another frame when it is selected', () => {
            const imageCopiedFromOtherFrame = otherFrames[0].$$image;
            SpecHelper.selectOptionByLabel(elem, 'select', `(2) ${otherFrames[0].$$image.label}`);
            const image = scope.imageModel;
            expect(image.frame()).toBe(frame);
            expect(image.id).not.toBe(imageCopiedFromOtherFrame.id);
            expect(image.image).toEqual(imageCopiedFromOtherFrame.image);
            expect(image.label).toEqual(imageCopiedFromOtherFrame.label);
        });
    });

    function render(_frame = frame) {
        const renderer = SpecHelper.renderer();
        renderer.scope.imageModel = viewModel.model;
        renderer.scope.frameViewModel = _frame.createFrameViewModel();
        renderer.scope.disabled = false;
        renderer.render(
            '<cf-image-selector name="name" frame-view-model="frameViewModel" image-model="imageModel" disabled="disabled"></cf-image-selector>',
        );
        elem = renderer.elem;
        scope = renderer.childScope;
        model = viewModel.model;
        SpecHelper.expectElement(elem, 'select[name="name"]'); // check that the name was copied down to the select box
    }

    function renderWithNoImageSelected() {
        const renderer = SpecHelper.renderer();
        const frame = Componentized.fixtures.getInstance();
        renderer.scope.frameViewModel = frame.createFrameViewModel();
        renderer.render(
            '<cf-image-selector name="name" frame-view-model="frameViewModel" image-model="imageModel" ></cf-image-selector>',
        );
        elem = renderer.elem;
        scope = renderer.childScope;
        model = viewModel.model;
        SpecHelper.expectElement(elem, 'select[name="name"]'); // check that the name was copied down to the select box
    }

    function getViewModel() {
        viewModel = getModel().createViewModel(frame.createFrameViewModel());
        return viewModel;
    }

    function getModel() {
        return frame.addImage();
    }
});
