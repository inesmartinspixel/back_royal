import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';

describe('cf-context-image-editor', () => {
    let frame;
    let elem;
    let scope;
    let Componentized;
    let SpecHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                frame = Componentized.fixtures.getInstance();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    // it('should show entries for each available image', () => {
    //     const images = [frame.addImage(), frame.addImage()];
    //     //render();
    //     //var select = SpecHelper.expectElement(elem, '[name="context_image_select"]');
    // });

    it('should respect allow-top-image', () => {
        render();
        scope.allowTopImage = false;
        scope.$digest();
        SpecHelper.expectNoElement(elem, '.context-image-select.first');
        scope.allowTopImage = true;
        scope.$digest();
        SpecHelper.expectElement(elem, '.context-image-select.first');
    });

    it('should respect allow-bottom-image', () => {
        render();
        scope.allowBottomImage = false;
        scope.$digest();
        SpecHelper.expectNoElement(elem, '.context-image-select.second');
        scope.allowBottomImage = true;
        scope.$digest();
        SpecHelper.expectElement(elem, '.context-image-select.second');
    });

    describe('context image sizing', () => {
        it('should support assignment of size via allowContextImageSizing if a firstContextImage or secondContextImage exists', () => {
            const image = frame.addImage();
            render(
                '<cf-context-image-editor title="something" frame-view-model="frameViewModel" set-context-image-on="mainImageTarget" allow-context-image-sizing="true" allow-top-image="true" allow-bottom-image="true"></cf-context-image-editor>',
            );
            SpecHelper.expectNoElement(elem, 'select[name="context_image_size_select_first"]');
            SpecHelper.expectNoElement(elem, 'select[name="context_image_size_select_second"]');

            SpecHelper.selectOptionByLabel(elem, 'select[name="context_image_select_first"]', `(1) ${image.label}`);

            const sizeSelector = SpecHelper.expectElement(elem, 'select[name="context_image_size_select_first"]');
            SpecHelper.expectElementText(sizeSelector, 'option:eq(0)', 'Tall');
            SpecHelper.expectElementText(sizeSelector, 'option:eq(1)', 'Short');

            SpecHelper.selectOptionByLabel(elem, 'select[name="context_image_select_first"]', '(No Image)');
            SpecHelper.selectOptionByLabel(elem, 'select[name="context_image_select_second"]', `(1) ${image.label}`);

            SpecHelper.expectElement(elem, 'select[name="context_image_size_select_second"]');
            SpecHelper.expectElementText(sizeSelector, 'option:eq(0)', 'Tall');
            SpecHelper.expectElementText(sizeSelector, 'option:eq(1)', 'Short');
        });
        it('should default to not supporting image sizing', () => {
            render(
                '<cf-context-image-editor title="something" frame-view-model="frameViewModel" set-context-image-on="mainImageTarget"></cf-context-image-editor>',
            );
            SpecHelper.expectNoElement(elem, 'select[name="context_image_size_select"]');
        });
    });

    function render(
        html = '<cf-context-image-editor title="something" set-context-image-on="mainImageTarget" frame-view-model="frameViewModel"></cf-context-image-editor>',
    ) {
        const renderer = SpecHelper.renderer();
        renderer.scope.mainImageTarget = {
            mainImage: '',
        };
        renderer.scope.frameViewModel = frame.createFrameViewModel();
        renderer.render(html);
        elem = renderer.elem;
        scope = renderer.childScope;
    }
});
