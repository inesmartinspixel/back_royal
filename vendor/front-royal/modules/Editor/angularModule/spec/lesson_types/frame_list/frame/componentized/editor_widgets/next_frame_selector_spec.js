import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';

describe('cf-next-frame-selector', () => {
    let frame;
    let anotherFrame;
    let elem;
    let scope;
    let Componentized;
    let SpecHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                frame = Componentized.fixtures.getInstance();
                anotherFrame = Componentized.fixtures.getInstance();
                frame.lesson().addFrame(anotherFrame);
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    // Note: the basic usage of this is tested in a few places where we actually use it,
    // so not bothering to test all functionality here

    describe('defaultFrame', () => {
        it('should show the default option as "Next Frame" when defaultFrame is undefined', () => {
            render();
            SpecHelper.expectElementText(elem, 'select option:eq(0)', '--- Next frame ---');
        });

        it('should show the default option as the default frame when there is one', () => {
            render();
            scope.defaultFrame = anotherFrame;
            scope.$digest();
            SpecHelper.expectElementText(elem, 'select option:eq(0)', `--- ${anotherFrame.label} ---`);
        });
    });

    function render(
        html = '<cf-next-frame-selector\
                frame="frame" \
                default-frame="defaultFrame"\
                ng-model="editorViewModel.ensureSelectableAnswerNavigatorFor(challenge, answer, \'gotoLastSelectedWhenMovingOnFromFrame\').next_frame_id">\
            </cf-next-frame-selector>',
    ) {
        const renderer = SpecHelper.renderer();
        renderer.scope.frame = frame;
        renderer.scope.frameViewModel = frame.createFrameViewModel();
        renderer.render(html);
        elem = renderer.elem;
        scope = renderer.scope;
    }
});
