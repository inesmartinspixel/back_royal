import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Users/angularModule/spec/_mock/fixtures/users';

describe('Lessons.FrameAuthorComments', () => {
    let frame;
    let elem;
    let scope;
    let playerViewModel;
    let Componentized;
    let SpecHelper;
    let renderer;
    let $rootScope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'FrontRoyal.Lessons', 'SpecHelper', 'FrontRoyal.Users');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                $rootScope = $injector.get('$rootScope');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
                frame = Componentized.fixtures.getInstance();
            },
        ]);

        SpecHelper.stubCurrentUser('super_editor');

        render();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('adding', () => {
        it('should allow adding of author comments', () => {
            const commentInput = SpecHelper.expectElement(elem, 'textarea[name="author_comment"]');
            expect(frame.$$author_comment).toBeUndefined();
            expect(commentInput.length).toBe(1);
            const ngModelController = commentInput.controller('ngModel');
            ngModelController.$setViewValue('new comment');
            renderer.scope.$digest();

            elem.find('button[name="add_comment"]').click();

            expect(frame.author_comments.length).toBe(1);
            expect(frame.author_comments[0].text).toBe('new comment');
            expect(frame.author_comments[0].author_name).toBe('Cool Brent'); // from users.js mock
        });

        it('should not allow adding of author comments if editor is previewer', () => {
            renderer.scope.currentUser.roles = [
                {
                    name: 'editor',
                    resource_id: null,
                },
                {
                    name: 'previewer',
                    resource_type: 'Lesson',
                    resource_id: playerViewModel.lesson.id,
                },
            ];
            renderer.scope.$digest();

            const commentInput = elem.find('textarea[name="author_comment"]');
            expect(commentInput.length).toBe(0);
        });

        it('should not allow adding of empty comments', () => {
            const commentInput = elem.find('textarea[name="author_comment"]');
            const ngModelController = commentInput.controller('ngModel');
            ngModelController.$setViewValue('');
            renderer.scope.$digest();

            elem.find('button[name="add_comment"]').click();

            expect(frame.author_comments).toBeUndefined();
        });

        it('should be disabled while an exiting save is occuring', () => {
            const commentInput = SpecHelper.expectElement(elem, 'textarea[name="author_comment"]');
            expect(frame.$$author_comment).toBeUndefined();
            expect(commentInput.length).toBe(1);
            const ngModelController = commentInput.controller('ngModel');
            ngModelController.$setViewValue('new comment');
            renderer.scope.$digest();

            SpecHelper.expectElementEnabled(elem, 'button[name="add_comment"]');
            frame.lesson().$$saving = true;
            renderer.scope.$digest();
            SpecHelper.expectElementDisabled(elem, 'button[name="add_comment"]');
        });
    });

    describe('deleting', () => {
        it('should allow deleting of comments by author', () => {
            elem.find('button[name="toggle_edit_comment"]').click();

            const commentInput = elem.find('textarea[name="author_comment"]');

            // start out with a single comment
            frame.author_comments = [
                {
                    text: 'old comment',
                    timestamp: new Date().getTime(),
                    author_id: 12345,
                },
            ];

            // add a comment
            const ngModelController = commentInput.controller('ngModel');
            ngModelController.$setViewValue('new comment');
            renderer.scope.$digest();

            elem.find('button[name="add_comment"]').click();

            expect(frame.author_comments.length).toBe(2);
            expect(frame.author_comments[0].text).toBe('old comment');
            expect(frame.author_comments[1].text).toBe('new comment');

            // now delete the old comment
            elem.find('button[name="remove_comment"]').eq(0).click();
            expect(frame.author_comments.length).toBe(1);
            expect(frame.author_comments[0].text).toBe('new comment');

            // delete the new comment too
            elem.find('button[name="remove_comment"]').eq(0).click();
            expect(frame.author_comments.length).toBe(0);
        });

        it('should not allow deleting of comments by anyone other than author', () => {
            elem.find('button[name="toggle_edit_comment"]').click();

            // two comments, one by someone else and one by me
            frame.author_comments = [
                {
                    text: 'old comment by someone else',
                    timestamp: new Date().getTime(),
                    author_id: 67890,
                },
                {
                    text: 'old comment by me',
                    timestamp: new Date().getTime(),
                    author_id: renderer.scope.currentUser.id,
                },
            ];
            renderer.scope.$digest();

            // check that the delete button for only the comment by me is clickable
            expect(frame.author_comments.length).toBe(2);
            expect(frame.author_comments[0].text).toBe('old comment by someone else');
            expect(frame.author_comments[1].text).toBe('old comment by me');

            SpecHelper.expectElementDisabled(elem, 'button[name="remove_comment"]:eq(0)');
            SpecHelper.expectElementEnabled(elem, 'button[name="remove_comment"]:eq(1)');
        });

        it('should allow admin to delete any comments', () => {
            SpecHelper.stubCurrentUser('admin');
            render();

            elem.find('button[name="toggle_edit_comment"]').click();

            // two comments, one by someone else and one by me
            frame.author_comments = [
                {
                    text: 'comment by a learner',
                    timestamp: new Date().getTime(),
                    author_id: 67890,
                },
                {
                    text: 'comment by an editor',
                    timestamp: new Date().getTime(),
                    author_id: 67891,
                },
            ];
            renderer.scope.$digest();

            expect(frame.author_comments.length).toBe(2);
            expect(frame.author_comments[0].text).toBe('comment by a learner');
            expect(frame.author_comments[1].text).toBe('comment by an editor');

            // both comments should deleteable for an admin
            SpecHelper.expectElementEnabled(elem, 'button[name="remove_comment"]:eq(0)');
            SpecHelper.expectElementEnabled(elem, 'button[name="remove_comment"]:eq(1)');
        });

        it('should be disabled while an exiting save is occuring', () => {
            SpecHelper.stubCurrentUser('admin');
            render();

            elem.find('button[name="toggle_edit_comment"]').click();

            // two comments, one by someone else and one by me
            frame.author_comments = [
                {
                    text: 'comment by self',
                    timestamp: new Date().getTime(),
                    author_id: renderer.scope.currentUser.id,
                },
            ];
            renderer.scope.$digest();
            SpecHelper.expectElementEnabled(elem, 'button[name="remove_comment"]:eq(0)');
            frame.lesson().$$saving = true;
            renderer.scope.$digest();
            SpecHelper.expectElementDisabled(elem, 'button[name="remove_comment"]:eq(0)');
        });
    });

    describe('completion', () => {
        it('should allow toggling completion of comments by author', () => {
            elem.find('button[name="toggle_edit_comment"]').click();

            // start out with a single comment
            frame.author_comments = [
                {
                    text: 'old comment',
                    timestamp: new Date().getTime(),
                    author_id: renderer.scope.currentUser.id,
                    completed: false,
                },
            ];
            renderer.scope.$digest();

            const todoCheckbox = SpecHelper.expectElementEnabled(elem, 'input[name="completed"]');
            expect(todoCheckbox.prop('checked')).toBe(false);

            SpecHelper.toggleCheckbox(elem, 'input[name="completed"]');
            renderer.scope.$digest();
            expect(frame.author_comments[0].completed).toBe(true);
        });

        it('should not allow toggling completion of comments by anyone other than author', () => {
            elem.find('button[name="toggle_edit_comment"]').click();

            // start out with a single comment
            frame.author_comments = [
                {
                    text: 'old comment',
                    timestamp: new Date().getTime(),
                    author_id: 12345,
                    completed: false,
                },
            ];
            renderer.scope.$digest();

            SpecHelper.expectElementDisabled(elem, 'input[name="completed"]');
        });

        it('should hide edit comments button from user who cannot edit the lesson', () => {
            jest.spyOn(renderer.scope.currentUser, 'canEditLesson').mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectNoElement(elem, 'button[name="toggle_edit_comment"]');
        });

        it('should allow admin to toggle completion of any comments', () => {
            SpecHelper.stubCurrentUser('admin');
            render();

            elem.find('button[name="toggle_edit_comment"]').click();

            // start out with a single comment
            frame.author_comments = [
                {
                    text: 'old comment',
                    timestamp: new Date().getTime(),
                    author_id: 12345,
                    completed: false,
                },
            ];
            renderer.scope.$digest();

            const todoCheckbox = SpecHelper.expectElementEnabled(elem, 'input[name="completed"]');
            expect(todoCheckbox.prop('checked')).toBe(false);

            SpecHelper.toggleCheckbox(elem, 'input[name="completed"]');
            renderer.scope.$digest();
            expect(frame.author_comments[0].completed).toBe(true);
        });
    });

    function render() {
        renderer = SpecHelper.renderer();
        playerViewModel = frame.lesson().createPlayerViewModel();
        playerViewModel.activeFrame = frame;
        renderer.scope.playerViewModel = playerViewModel;
        renderer.scope.currentUser = $rootScope.currentUser;
        renderer.render(
            '<frame-author-comments frame="playerViewModel.activeFrame" current-user="::currentUser"></frame-author-comments>',
        );
        elem = renderer.elem;
        renderer.scope.$digest();
        scope = renderer.scope;
    }
});
