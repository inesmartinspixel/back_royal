import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';
import 'Lessons/angularModule/spec/_helpers/componentized_spec_helper';

describe('Componentized.ComponentizedInfoPanel', () => {
    let frame;
    let elem;
    let playerViewModel;
    let Componentized;
    let SpecHelper;
    let ComponentEditorViewModel;
    let ComponentizedSpecHelper;
    let renderer;
    let Lesson;
    let Frame;
    let FrameList;
    let $window;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                ComponentEditorViewModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ComponentEditorViewModel',
                );
                $injector.get('ComponentizedFixtures');
                ComponentizedSpecHelper = $injector.get('ComponentizedSpecHelper');
                frame = Componentized.fixtures.getInstance();
                Lesson = $injector.get('Lesson');
                FrameList = $injector.get('Lesson.FrameList');
                $window = $injector.get('$window');
                Frame = $injector.get('Lesson.FrameList.Frame');

                // override text_content so we can use a fake mainUiComponent
                Object.defineProperty(ComponentEditorViewModel.prototype, 'text_content', {
                    value: 'main text',
                });

                SpecHelper.stubEventLogging();
            },
        ]);

        SpecHelper.stubCurrentUser('super_editor');

        render();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should show the editor for the current main content component', () => {
        render();
        ComponentizedSpecHelper.assertEditorElementPresent(elem, frame.mainUiComponent);
    });

    it('should allow for reverting a frame back to English', () => {
        const lesson = frame.lesson();
        Object.defineProperty(Frame.prototype, 'mainUiComponent', {
            value: {
                id: 'mainUiComponentId',
            },
        });
        jest.spyOn(Frame.prototype, 'logInfo').mockReturnValue({}); // mocking shit

        const enLesson = FrameList.fixtures.getInstance();
        enLesson.frames[0] = Componentized.new(
            _.extend(frame.asJson(), {
                id: 'en_id',
                marker: 'this is English',
            }),
        );

        lesson.locale = 'es';
        lesson.ensureLocalePack().content_items.push({
            locale: 'en',
            id: enLesson.id,
        });

        render();
        Lesson.expect('index')
            .toBeCalledWith({
                filters: {
                    locale_pack_id: lesson.localePackId,
                    locale: 'en',
                    published: false,
                    in_users_locale_or_en: false,
                },
                'fields[]': ['frames', 'lesson_type'],
            })
            .returns([enLesson]);
        jest.spyOn($window, 'confirm').mockReturnValue(true);

        SpecHelper.click(elem, '[name="revert-to-english"]');
        expect($window.confirm).toHaveBeenCalled();
        Lesson.flush('index');
        expect(playerViewModel.activeFrame.marker).toEqual('this is English'); // english was copied over
        expect(playerViewModel.activeFrame.id).toEqual(frame.id); // but id did not change
    });

    function render() {
        renderer = SpecHelper.renderer();
        playerViewModel = frame.lesson().createPlayerViewModel();
        playerViewModel.activeFrame = frame;
        renderer.scope.frameViewModel = playerViewModel.activeFrameViewModel;
        renderer.render('<componentized-info-panel frame-view-model="frameViewModel"></componentized-info-panel>');
        elem = renderer.elem;
        ComponentEditorViewModel.supportConfigOption('a');
        renderer.childScope.mainUiComponentTemplates = [
            {
                title: 'Default template',
                identifier: 'default_template',
                EditorViewModel: ComponentEditorViewModel,
                config: {
                    a: 'b',
                },
            },
        ];
        renderer.scope.$digest();
    }
});
