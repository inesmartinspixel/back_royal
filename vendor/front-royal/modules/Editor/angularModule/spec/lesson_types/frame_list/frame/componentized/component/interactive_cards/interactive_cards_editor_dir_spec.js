import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/componentized_fixtures';

describe('cf-interactive-cards-editor', () => {
    let frame;
    let elem;
    let Componentized;
    let SpecHelper;
    let viewModel;
    let frameViewModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Lessons', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Componentized = $injector.get('Lesson.FrameList.Frame.Componentized');
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('ComponentizedFixtures');
            },
        ]);

        viewModel = getViewModel();

        Object.defineProperty(frame, 'editor_template', {
            get: _.constant(''),
            configurable: true,
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.scope.editorViewModel = frame.editorViewModelFor(viewModel.model);
        renderer.scope.frameViewModel = frameViewModel;
        renderer.render(
            '<cf-interactive-cards-editor editor-view-model="editorViewModel" frame-view-model="frameViewModel"></cf-interactive-cards-editor>',
        );
        elem = renderer.elem;
    }

    function getViewModel(options) {
        const model = getModel(options);
        return frameViewModel.viewModelFor(model);
    }

    function getModel() {
        frame = Componentized.fixtures.getInstance();
        frameViewModel = frame.createFrameViewModel();
        frameViewModel.playerViewModel = {
            activeFrameViewModel: frameViewModel,
            logInfo() {},
            log() {},
        };
        return frame.addInteractiveCards().addDefaultReferences();
    }

    describe('multiple_card_multiple_choice', () => {
        beforeEach(() => {
            jest.spyOn(frame, 'editor_template', 'get').mockReturnValue('multiple_card_multiple_choice');
        });

        it('should show the proper controls when text', () => {
            jest.spyOn(viewModel.model.overlays[0], 'contentType', 'get').mockReturnValue('text');
            render();
            SpecHelper.expectElement(elem, '[name="force-card-height"]');
            SpecHelper.expectElement(elem, '[name="wide"]');
            SpecHelper.expectElement(elem, '.interactive-cards-row:eq(0) cf-has-text-or-image-editor');
            SpecHelper.expectElement(elem, '.interactive-cards-row:eq(0) [name="background-color"]');
        });

        it('should show the proper controls when image', () => {
            jest.spyOn(viewModel.model.overlays[0], 'contentType', 'get').mockReturnValue('image');
            render();
            SpecHelper.expectElement(elem, '[name="force-card-height"]');
            SpecHelper.expectElement(elem, '[name="wide"]');
            SpecHelper.expectElement(elem, '.interactive-cards-row:eq(0) cf-has-text-or-image-editor');
            SpecHelper.expectElement(elem, '.interactive-cards-row:eq(0) [name="background-color"]');
        });
    });

    it('should show the proper controls in compose_blanks_on_image', () => {
        jest.spyOn(frame, 'editor_template', 'get').mockReturnValue('compose_blanks_on_image');
        render();
        SpecHelper.expectNoElement(elem, '[name="force-card-height"]');
        SpecHelper.expectNoElement(elem, '[name="wide"]');
        SpecHelper.expectElement(
            elem,
            '.interactive-cards-row:eq(0) cf-image-selector',
            viewModel.model.overlays.length,
        );
        SpecHelper.expectNoElement(elem, '[name="background-color"]');
    });
});
