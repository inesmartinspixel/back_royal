import 'EventLogger/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Users/angularModule/spec/_mock/fixtures/users';
import setSpecLocales from 'Translation/setSpecLocales';
import lessonPlayerViewModelLocales from 'Lessons/locales/lessons/models/lesson/lesson_player_view_model-en.json';
import showFramesPlayerLocales from 'Lessons/locales/lessons/lesson/show_frames_player-en.json';
import previousButtonDesktopLocales from 'Navigation/locales/navigation/previous_button_desktop-en.json';
import componentizedLocales from 'Lessons/locales/lessons/lesson/frame_list/frame/componentized-en.json';
import continueButtonLocales from 'Lessons/locales/lessons/lesson/frame_list/frame/componentized/component/continue_button-en.json';

setSpecLocales(lessonPlayerViewModelLocales);
setSpecLocales(showFramesPlayerLocales);
setSpecLocales(previousButtonDesktopLocales);
setSpecLocales(componentizedLocales);
setSpecLocales(continueButtonLocales);

angular.module('FrontRoyal.Lessons').factory('ComponentizedEditorIntegrationSpecHelper', [
    '$injector',
    'SpecHelper',
    'Lesson.FrameList',
    'EventLogger',
    '$timeout',
    '$location',
    'LessonFixtures',
    'UserFixtures',
    ($injector, SpecHelper, FrameList, EventLogger, $timeout, $location) => {
        SpecHelper.stubEventLogging();
        SpecHelper.stubFramePreloading();
        SpecHelper.stubConfig();

        SpecHelper.stubDirective('feedbackSidebar');

        // we can end up on unexpected frames if the query params get set
        jest.spyOn($location, 'search').mockReturnValue({});

        return {
            renderEditFrameList(frameOrLesson) {
                SpecHelper.cleanupCreatedRenderers();
                let frame;
                let lesson;
                if (frameOrLesson && frameOrLesson.isA(FrameList)) {
                    lesson = frameOrLesson;
                } else if (frameOrLesson) {
                    frame = frameOrLesson;
                }

                SpecHelper.stubCurrentUser('super_editor');
                SpecHelper.stubLessonProgress();

                if (!lesson) {
                    const frames = frame ? [frame.asJson()] : [];
                    lesson = FrameList.fixtures.getInstance({
                        frames,
                    });
                }

                const renderer = SpecHelper.renderer();
                renderer.scope.playerViewModel = lesson.createPlayerViewModel({
                    editorMode: true,
                });

                // stub out front-royal-footer so that the continue button
                // does not get removed from the screen, since there is no
                // app-shell here
                SpecHelper.stubDirective('frontRoyalFooterContent');

                renderer.render('<edit-frame-list player-view-model="playerViewModel" />');
                return renderer;
            },

            renderShowLesson(frameOrLesson, opts) {
                SpecHelper.cleanupCreatedRenderers();
                let frame;
                let lesson;
                if (frameOrLesson && frameOrLesson.frames) {
                    lesson = frameOrLesson;
                } else if (frameOrLesson) {
                    frame = frameOrLesson;
                }

                if (!lesson) {
                    const frames = frame ? [frame.asJson()] : [];
                    lesson = FrameList.fixtures.getInstance({
                        frames,
                    });
                }

                const renderer = SpecHelper.renderer();
                renderer.scope.playerViewModel = lesson.createPlayerViewModel(
                    angular.extend(
                        {
                            editorMode: false,
                            skipStartScreen: true,
                        },
                        opts,
                    ),
                );

                // stub out front-royal-footer so that the continue button
                // does not get removed from the screen, since there is no
                // app-shell here
                SpecHelper.stubDirective('frontRoyalFooterContent');

                // prevent progress api calls
                jest.spyOn(renderer.scope.playerViewModel, '_logLessonStart');
                renderer.render('<show-lesson player-view-model="playerViewModel" />');

                // it takes two timeouts to get the activeFrame on the screen
                $timeout.flush();
                $timeout.flush();
                return renderer;
            },

            expectElementInFrameContainer(elem, selector) {
                const container = SpecHelper.expectElement(elem, '.frame-container');
                return SpecHelper.expectElement(container, selector);
            },

            setMainTextAndAssertOnScreen(
                elem,
                selector = 'textarea[name="text"]',
                text = `Main text ${Math.random()}`,
                screenText = text,
            ) {
                SpecHelper.updateTextArea(elem, selector, text);
                const textEl = this.expectElementInFrameContainer(elem, '.section.text');
                SpecHelper.expectEqual(screenText.trim(), SpecHelper.trimText(textEl), 'main text contents');
            },

            setMainImageAndAssertOnScreen(elem) {
                const s3Asset = SpecHelper.uploadWithContentItemImageUpload(
                    elem,
                    '.cf-image-list-editor [add-new-image] .content-item-image-upload',
                );

                // click the image that was just created in order to make it the main image
                SpecHelper.selectOptionByLabel(elem, 'select[name="context_image_select_first"]', '(1) fileName');

                const img = SpecHelper.expectElements(elem, '.frame-container .section.context-image-first img', 1);
                SpecHelper.expectEqual(s3Asset.formats.original.url, img.attr('src'), 'image href');
            },

            assertInstructions(elem, expectedInstructions) {
                const instructions = $injector.get('Navigation.AppHeader.AppHeaderViewModel').frameInstructions;
                SpecHelper.expectHasClass(elem, '.cf-continue-button', 'ng-hide');
                SpecHelper.expectEqual(expectedInstructions, instructions);
            },

            assertContinueButtonShown(elem) {
                SpecHelper.expectDoesNotHaveClass(elem, '.cf-continue-button', 'ng-hide');
                SpecHelper.expectElementText(elem, '.cf-continue-button', 'Continue');
            },
        };
    },
]);
