/*
    Descends from InfoPanelSpecHelper, but renders the entire editor instead of just
    the info panel, allowing for testing of the integration between the info panel
    and the frame preview.
*/
angular.module('LessonSpecHelpers').factory('EditorIntegrationSpecHelper', [
    'InfoPanelSpecHelper',
    'SpecHelper',
    'AClassAbove',
    '$injector',
    'LessonFixtures',
    (InfoPanelSpecHelper, SpecHelper, AClassAbove, $injector) => {
        const Helper = InfoPanelSpecHelper.Helper.subclass(() => ({
            initialize(frame) {
                this.renderer = null;
                this.frame = frame;

                // override elem and scope so that they still point to the info panel
                Object.defineProperty(this, 'elem', {
                    get() {
                        return this.renderer ? this.renderer.elem.find(this.frame.infoPanelDirectiveName) : null;
                    },
                });

                Object.defineProperty(this, 'scope', {
                    get() {
                        return this.renderer ? this.elem.isolateScope() : null;
                    },
                });

                Object.defineProperty(this, 'framePreviewElem', {
                    get() {
                        return this.renderer ? this.renderer.elem.find(`.${this.frame.frame_type}`) : null;
                    },
                });

                Object.defineProperty(this, 'framePreviewScope', {
                    get() {
                        return this.renderer ? this.framePreviewElem.isolateScope() : null;
                    },
                });

                this.render();
            },

            render() {
                this.renderer = SpecHelper.renderer();
                this.renderer.scope.playerViewModel = this.frame.lesson().createPlayerViewModel({
                    editorMode: true,
                });
                this.renderer.scope.playerViewModel.activeFrame = this.frame;
                this.renderer.render('<edit-frame-list player-view-model="playerViewModel"></edit-frame-list>');
            },
        }));

        return {
            Helper,

            getHelper(frame, moduleNames = []) {
                const HelperKlass = Helper.subclass(function () {
                    angular.forEach(moduleNames, moduleName => {
                        const module = $injector.get(moduleName);
                        this.include(module);
                    });
                });

                return new HelperKlass(frame);
            },
        };
    },
]);
