import 'AngularSpecHelper';
import 'Editor/angularModule';

describe('Editor.ListContentTopicsDir', () => {
    let SpecHelper;
    let guid;
    let renderer;
    let elem;
    let ContentTopic;
    let expectedContentTopics;
    let ngToast;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                guid = $injector.get('guid');
                ContentTopic = $injector.get('ContentTopic');
                ngToast = $injector.get('ngToast');
                $timeout = $injector.get('$timeout');

                expectedContentTopics = [
                    {
                        id: guid.generate(),
                        locales: {
                            en: 'topic 1',
                        },
                    },
                    {
                        id: guid.generate(),
                        locales: {
                            en: 'topic 2',
                        },
                    },
                ];
            },
        ]);

        SpecHelper.stubCurrentUser('super_editor'); // grant edit global privs
        SpecHelper.stubDirective('dirPaginationControls');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        ContentTopic.expect(
            'index',
            [
                {
                    filters: {},
                },
            ],
            {
                result: expectedContentTopics,
            },
        );

        renderer = SpecHelper.renderer();
        renderer.render('<list-content-topics></list-content-topics>');

        $timeout.flush();

        SpecHelper.expectElement(renderer.elem, 'front-royal-spinner');
        ContentTopic.flush('index');
        SpecHelper.expectNoElement(renderer.elem, 'front-royal-spinner');

        elem = renderer.elem;
        return renderer;
    }

    it('should list content topics', () => {
        render();
        SpecHelper.expectElements(elem, 'tr', expectedContentTopics.length + 1); // header row
    });

    it('should support creating a content topic', () => {
        render();

        SpecHelper.click(elem, 'button[name="create"]');

        const newTopic = {
            id: guid.generate(),
            locales: {
                en: 'New Topic',
            },
        };

        // update the input
        SpecHelper.expectElementDisabled(elem, 'button[name="save"]', true);
        SpecHelper.updateTextInput(elem, '.front-royal-form-container input[placeholder="en"]', newTopic.locales.en);
        const createButton = SpecHelper.expectElementEnabled(elem, 'button[name="save"]');

        // expect proper calls
        ContentTopic.expect('create').returns({
            result: newTopic,
        });
        expectedContentTopics.push(newTopic);

        createButton.click();
        ContentTopic.flush('create');

        SpecHelper.expectElements(elem, 'tr', expectedContentTopics.length + 1);
    });

    it('should support deleting a content topic', () => {
        render();

        ContentTopic.expect('destroy');

        expectedContentTopics.splice(0, 1);

        SpecHelper.expectElement(elem, 'button[name="delete"]:eq(0)').click();

        ContentTopic.flush('destroy');

        SpecHelper.expectElements(elem, 'tr', expectedContentTopics.length + 1);
    });

    it('should show toast after update', () => {
        render();

        jest.spyOn(ngToast, 'create').mockImplementation(() => {});
        ContentTopic.expect('save');

        SpecHelper.expectElement(elem, 'button[name="save"]:eq(0)').click();
        ContentTopic.flush('save');
        expect(ngToast.create).toHaveBeenCalledWith({
            content: 'Content topic updated',
            className: 'success',
        });
    });
});
