import 'AngularSpecHelper';
import 'Editor/angularModule';

describe('Editor::ContentTopicEditorDir', () => {
    let elem;
    let SpecHelper;
    let item;
    let contentTopics;
    let ContentTopic;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper', 'guid');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            ContentTopic = $injector.get('ContentTopic');

            const guid = $injector.get('guid');
            contentTopics = [
                {
                    id: guid.generate(),
                    name: 'topic 1',
                    locales: {
                        en: 'topic 1',
                    },
                },
                {
                    id: guid.generate(),
                    name: 'topic 2',
                    locales: {
                        en: 'topic 2',
                    },
                },
            ];

            item = {
                locale_pack: {
                    content_topics: [contentTopics[0]],
                },
                locale: 'en',
            };

            ContentTopic.expect('index').returns(contentTopics);

            render();
        });
    });

    afterAll(() => {
        SpecHelper.cleanup();
    });

    it('should add a topic to the item', () => {
        // sanity check
        expect(item.locale_pack.content_topics).toEqual([contentTopics[0]]);

        SpecHelper.assertSelectizeValue(elem, '[name="topics"]', contentTopics[0].id);

        SpecHelper.updateSelectize(elem, '[name="topics"]', [contentTopics[0].id, contentTopics[1].id]);

        expect(item.locale_pack.content_topics.length).toBe(2);
        expect(item.locale_pack.content_topics[0].locales.en).toEqual(contentTopics[0].locales.en);
        expect(item.locale_pack.content_topics[1].locales.en).toEqual(contentTopics[1].locales.en);
        // I refactored the expectation a bit because it was failing due to the absence of Iguana properties
        // expect(item.locale_pack.content_topics).toEqual([
        //     contentTopics[0],
        //     contentTopics[1]
        // ]);

        SpecHelper.assertSelectizeValue(elem, '[name="topics"]', [contentTopics[0].id, contentTopics[1].id]);
    });

    // I don't know how to test adding a new option through the
    // selectize input

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.scope.item = item;
        renderer.render('<content-topics-editor item="item"></content-topics-editor>');
        elem = renderer.elem;
        SpecHelper.expectElement(elem, 'front-royal-spinner');
        SpecHelper.expectNoElement(elem, 'selectize');
        ContentTopic.flush('index');
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
        SpecHelper.expectElement(elem, 'selectize');
    }
});
