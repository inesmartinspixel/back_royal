import 'AngularSpecHelper';
import 'Editor/angularModule';

describe('ooDirective', () => {
    let scope;
    let elem;
    let compiled;
    let app;

    function render($rootScope, $compile, html) {
        scope = $rootScope.$new();
        elem = angular.element(html);
        compiled = $compile(elem);
        compiled(scope);
        scope.$digest();
        return {
            scope,
            elem,
        };
    }

    beforeEach(() => {
        const name = `ooDirectiveTestApp${Math.random()}`;
        // FrontRoyal is just here so we have SpecHelper.
        // Ideally SpecHelper should be separated out.
        app = angular.module(name, ['ooDirective']);
        angular.mock.module(name);
    });

    afterEach(() => {
        if (scope) {
            scope.$destroy();
        }
        if (elem) {
            $(elem).remove();
        }
    });

    // it('should understand how di works with directive controllers', function() {
    //     app.directive('test', ['$injectable', function() {
    //
    //         return {
    //             controller: function($scope, $element, $attrs, $transclude) {
    //                 angular.forEach(arguments, function(i){
    //                     console.log(i);
    //                     console.log('***************');
    //                 })
    //             }
    //         };
    //
    //     });
    //
    //     inject(function($rootScope, $compile){
    //         var scope;
    //         scope = render($rootScope, $compile, '<div test=""></div>').scope;
    //     });
    // });

    it('should create a directive with options set on created klass', () => {
        app.ooDirective('test', directiveBase =>
            directiveBase.subclass({
                require: 'something',
            }),
        );

        angular.mock.inject(testDirective => {
            expect(testDirective[0].require).toBe('something'); // set in the factory
            expect(testDirective[0].restrict).toBe('EA'); // default in angular
        });
    });

    it('should inherit the options from a superclass', () => {
        app.ooDirective('super', directiveBase =>
            directiveBase.subclass({
                require: 'something',
            }),
        );

        app.ooDirective('sub', [
            'super',
            superDirective =>
                superDirective.subclass({
                    template: 'template',
                }),
        ]);

        angular.mock.inject((superDirective, subDirective) => {
            expect(superDirective[0].template).toBeUndefined(); // not set in the super class
            expect(superDirective[0].require).toBe('something'); // set in the super class
            expect(superDirective[0].restrict).toBe('EA'); // default in angular

            expect(subDirective[0].template).toBe('template'); // set in the sub class
            expect(subDirective[0].require).toBe('something'); // set in the super class
            expect(subDirective[0].restrict).toBe('EA'); // default in angular
        });
    });

    it('should subclass a controller, passing the scope through', () => {
        app.ooDirective('super', directiveBase =>
            directiveBase.subclass({
                controller: directiveBase.extendController({
                    initialize(scope) {
                        scope.setInSuperInit = true;
                        scope.setInSuperPrototype = this.setInSuperPrototype;
                        scope.overriddenInSub = this.overriddenInSub;
                    },
                    setInSuperPrototype: true,
                    overriddenInSub: false,
                }),
            }),
        );

        app.ooDirective('sub', [
            'super',
            superDirective =>
                superDirective.subclass({
                    controller: superDirective.extendController({
                        initialize($super, scope) {
                            $super(scope);
                            scope.setInSubInit = true;
                            scope.overriddenInSub = this.overriddenInSub;
                        },
                        overriddenInSub: true,
                    }),
                }),
        ]);

        angular.mock.inject(($rootScope, $compile) => {
            let scope;
            scope = render($rootScope, $compile, '<div super=""></div>').scope;
            expect(scope.setInSuperInit).toBe(true);
            expect(scope.setInSubInit).toBeUndefined();
            expect(scope.setInSuperPrototype).toBe(true);
            expect(scope.overriddenInSub).toBe(false);

            scope = render($rootScope, $compile, '<div sub=""></div>').scope;
            expect(scope.setInSuperInit).toBe(true);
            expect(scope.setInSubInit).toBe(true);
            expect(scope.setInSuperPrototype).toBe(true);
            expect(scope.overriddenInSub).toBe(true);
        });
    });

    it('should subclass a controller, passing the elem through', () => {
        app.ooDirective('super', directiveBase =>
            directiveBase.subclass({
                controller: directiveBase.extendController({
                    initialize(scope, elem) {
                        elem.data('setInSuperInit', true);
                        elem.data('setInSuperPrototype', this.setInSuperPrototype);
                        elem.data('overriddenInSub', this.overriddenInSub);
                    },
                    setInSuperPrototype: true,
                    overriddenInSub: false,
                }),
            }),
        );

        app.ooDirective('sub', [
            'super',
            superDirective =>
                superDirective.subclass({
                    controller: superDirective.extendController({
                        initialize($super, scope, elem) {
                            $super(scope, elem);
                            elem.data('setInSubInit', true);
                            elem.data('overriddenInSub', this.overriddenInSub);
                        },
                        overriddenInSub: true,
                    }),
                }),
        ]);

        angular.mock.inject(($rootScope, $compile) => {
            let elem;
            elem = render($rootScope, $compile, '<div super=""></div>').elem;
            expect(elem.data('setInSuperInit')).toBe(true);
            expect(elem.data('setInSubInit')).toBeUndefined();
            expect(elem.data('setInSuperPrototype')).toBe(true);
            expect(elem.data('overriddenInSub')).toBe(false);

            elem = render($rootScope, $compile, '<div sub=""></div>').elem;
            expect(elem.data('setInSuperInit')).toBe(true);
            expect(elem.data('setInSubInit')).toBe(true);
            expect(elem.data('setInSuperPrototype')).toBe(true);
            expect(elem.data('overriddenInSub')).toBe(true);
        });
    });

    it('should subclass a controller, passing the attrs through', () => {
        app.ooDirective('super', directiveBase =>
            directiveBase.subclass({
                controller: directiveBase.extendController({
                    initialize(scope, elem, attrs) {
                        attrs.$set('class', 'ok');
                        attrs.$set('set-in-super-init', 'true');
                        attrs.$set('set-in-super-prototype', this.setInSuperPrototype);
                        attrs.$set('overridden-in-sub', this.overriddenInSub);
                    },
                    setInSuperPrototype: 'true',
                    overriddenInSub: 'false',
                }),
            }),
        );

        app.ooDirective('sub', [
            'super',
            superDirective =>
                superDirective.subclass({
                    controller: superDirective.extendController({
                        initialize($super, scope, elem, attrs) {
                            $super(scope, elem, attrs);
                            attrs.$set('set-in-sub-init', 'true');
                            attrs.$set('overridden-in-sub', this.overriddenInSub);
                        },
                        overriddenInSub: 'true',
                    }),
                }),
        ]);

        angular.mock.inject(($rootScope, $compile) => {
            let elem;
            elem = render($rootScope, $compile, '<div super=""></div>').elem;
            expect(elem.attr('set-in-super-init')).toBe('true');
            expect(elem.attr('set-in-sub-init')).toBeUndefined();
            expect(elem.attr('set-in-super-prototype')).toBe('true');
            expect(elem.attr('overridden-in-sub')).toBe('false');

            elem = render($rootScope, $compile, '<div sub=""></div>').elem;
            expect(elem.attr('set-in-super-init')).toBe('true');
            expect(elem.attr('set-in-sub-init')).toBe('true');
            expect(elem.attr('set-in-super-prototype')).toBe('true');
            expect(elem.attr('overridden-in-sub')).toBe('true');
        });
    });
});
