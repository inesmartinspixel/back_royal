import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

describe('Editor.ListLessonsDir', () => {
    let expectedLessons;
    let renderer;
    let scope;
    let Lesson;
    let $location;
    let $timeout;
    let SpecHelper;
    let elem;
    let DialogModal;
    let $window;
    let FrameList;
    let EntityMetadata;
    let RecentlyEditedLessons;
    let contentItemEditorLists;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Lesson = $injector.get('Lesson');
                SpecHelper = $injector.get('SpecHelper');
                $location = $injector.get('$location');
                $timeout = $injector.get('$timeout');
                DialogModal = $injector.get('DialogModal');
                $window = $injector.get('$window');
                EntityMetadata = $injector.get('EntityMetadata');
                FrameList = $injector.get('Lesson.FrameList');
                $injector.get('MockIguana');
                $injector.get('LessonFixtures');
                RecentlyEditedLessons = $injector.get('RecentlyEditedLessons');
                contentItemEditorLists = $injector.get('contentItemEditorLists');
                $timeout = $injector.get('$timeout');

                expectedLessons = [
                    Lesson.fixtures.getInstance(),
                    Lesson.fixtures.getInstance(),
                    Lesson.fixtures.getInstance(),
                ];

                Lesson.setAdapter('Iguana.Mock.Adapter');
            },
        ]);
        SpecHelper.stubCurrentUser('super_editor'); // grant edit global privs
        SpecHelper.stubDirective('dirPaginationControls');

        SpecHelper.stubConfig({
            social_name: 'some_twitter_account',
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should default to sorting by tag', () => {
        render();
        SpecHelper.expectElement(elem, 'th .tag.sort-asc');
    });

    it('should show recently edited lessons', () => {
        const lessons = [Lesson.fixtures.getInstance()];
        Object.defineProperty(RecentlyEditedLessons.prototype, 'lessons', {
            get() {
                return lessons;
            },
        });
        render();
        SpecHelper.expectElement(elem, '#recents-panel');
    });

    describe('creating', () => {
        it('should support creating a new lesson', () => {
            render();

            SpecHelper.click(elem, 'button[name="create"]');
            SpecHelper.updateTextInput(elem, '[name="title"]', 'New Stream');
            const createButton = SpecHelper.expectElementEnabled(elem, 'button[name="save"]');

            const expectedLesson = expectedLessons[0];
            FrameList.expect('create').returns(expectedLesson);

            jest.spyOn($location, 'url').mockImplementation(() => {});
            EntityMetadata.expect('save').returns({
                result: {
                    entity_metadata: {
                        title: 'stuff',
                        description: 'things',
                        canonical_url: 'whatever',
                    },
                },
                meta: {},
            });

            createButton.click();

            FrameList.flush('create');
            EntityMetadata.flush('save');

            expect($location.url).toHaveBeenCalledWith(`/editor/lesson/${expectedLesson.id}/edit`);
        });
    });

    describe('archiving', () => {
        it('should immediately archive a lesson that is not in a stream on click', () => {
            render();
            const tr = elem.find('tr:eq(1)');
            const lesson = tr.scope().thing; // editable-thing-list
            lesson.stream_titles = [];
            scope.$digest();
            Lesson.expect('save');

            SpecHelper.expectNoElement(tr, '.actions front-royal-spinner');

            SpecHelper.click(tr, '[data-id="archive"]');
            SpecHelper.expectEqual(true, lesson.archived);
            SpecHelper.expectNoElement(tr, '[data-id="archive"]');

            Lesson.flush('save');
            SpecHelper.expectNoElement(tr, '.actions front-royal-spinner');
            SpecHelper.expectNoElement(tr, '[data-id="archive"]');
            SpecHelper.expectElement(tr, '[data-id="unarchive"]');
        });

        it('should immediated unarchive a lesson on click', () => {
            render(true);
            const tr = elem.find('tr:eq(1)');
            const lesson = tr.scope().thing; // editable-thing-list
            lesson.archived = true;
            scope.$digest();

            Lesson.expect('save');
            SpecHelper.expectNoElement(tr, '.actions front-royal-spinner');

            SpecHelper.click(tr, '[data-id="unarchive"]');
            SpecHelper.expectEqual(false, lesson.archived);
            SpecHelper.expectNoElement(tr, '[data-id="unarchive"]');

            Lesson.flush('save');
            SpecHelper.expectNoElement(tr, '.actions front-royal-spinner');
            SpecHelper.expectNoElement(tr, '[data-id="unarchive"]');
            SpecHelper.expectElement(tr, '[data-id="archive"]');
        });

        it('should show a dialog alert when archiving a lesson that is in a stream', () => {
            render();
            const tr = elem.find('tr:eq(1)');
            const lesson = tr.scope().thing; // editable-thing-list
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            lesson.stream_titles = ['I am in a stream'];
            jest.spyOn(lesson, 'save').mockImplementation(() => {});

            SpecHelper.click(tr, '[data-id="archive"]');

            expect(DialogModal.alert).toHaveBeenCalled();
            expect(lesson.save).not.toHaveBeenCalled();
        });

        it('should allow for deleting an archived lesson', () => {
            render(true);
            const tr = elem.find('tr:eq(1)');
            const lesson = tr.scope().thing; // editable-thing-list
            Lesson.expect('destroy');

            // setting showDeleteButton to true should
            // show the delete button next to archived lessons
            jest.spyOn($window, 'confirm').mockReturnValue(true);

            SpecHelper.click(tr, '[data-id="delete"]');
            expect($window.confirm).toHaveBeenCalled();

            Lesson.flush('destroy');
            SpecHelper.expectNoElement(elem, `tr[data-id="${lesson.id}"]`);
        });

        it('should hide archive/unarchive buttons if user does not have edit permissions', () => {
            render(true);
            const tr = elem.find('tr:eq(1)');
            SpecHelper.stubCurrentUser('editor'); // take away edit global privs
            scope.$digest();

            SpecHelper.expectNoElement(tr, '[data-id="archive"]');
            SpecHelper.expectNoElement(tr, '[data-id="unarchive"]');
        });
    });

    describe('sorting', () => {
        describe('by published_at', () => {
            beforeEach(() => {
                expectedLessons[0].published_at = 2;
                expectedLessons[1].published_at = undefined;
                expectedLessons[2].published_at = 1;
                render();
            });

            it('should work', () => {
                SpecHelper.click(elem, 'th[data-id="sortablePublishedAt"]');
                // ascending, with rows with no published_at at the end
                SpecHelper.expectEqual([1, 2, undefined], listOfPublishedAt());

                SpecHelper.click(elem, 'th[data-id="sortablePublishedAt"]');
                // descending, with rows with no published_at STILL at the end
                SpecHelper.expectEqual([2, 1, undefined], listOfPublishedAt());
            });

            function listOfPublishedAt() {
                return elem
                    .find('tr')
                    .toArray()
                    .slice(1)
                    .map(el => $(el).scope().thing.published_at); // editable-thing-list
            }
        });

        describe('by published_at', () => {
            beforeEach(() => {
                expectedLessons[0].modified_at = 2;
                expectedLessons[1].modified_at = undefined;
                expectedLessons[2].modified_at = 1;
                render();
            });

            it('should work', () => {
                SpecHelper.click(elem, 'th[data-id="sortableModifiedAt"]');
                // ascending, with rows with no modified_at at the end
                SpecHelper.expectEqual([1, 2, undefined], listOfModifiedAt());

                SpecHelper.click(elem, 'th[data-id="sortableModifiedAt"]');
                // descending, with rows with no modified_at STILL at the end
                SpecHelper.expectEqual([2, 1, undefined], listOfModifiedAt());
            });

            function listOfModifiedAt() {
                return elem
                    .find('tr')
                    .toArray()
                    .slice(1)
                    .map(el => $(el).scope().thing.modified_at); // editable-thing-list
            }
        });
    });

    it('should display lessons', () => {
        render();
        const links = SpecHelper.expectElements(elem, 'td[data-id="title"]', expectedLessons.length);

        for (let i = 0; i < expectedLessons.length; i++) {
            expect(links[i].textContent.trim()).toEqual(expectedLessons[i].title);
        }
    });

    it('should follow a lesson link when clicked on', () => {
        const renderer = render();
        jest.spyOn($location, 'url').mockImplementation(() => {});
        SpecHelper.click(renderer.elem, 'button[data-id="edit"]', 0);
        expect($location.url).toHaveBeenCalledWith(`/editor/lesson/${expectedLessons[0].id}/edit`);
    });

    it('should allow for full-text searching of content', () => {
        render();

        Lesson.expect(
            'index',
            [
                {
                    filters: {
                        published: false,
                        archived: false,
                        in_users_locale_or_en: false,
                        content_search: 'SEARCH TEXT',
                        locale: 'en',
                    },
                    'fields[]': Lesson.fieldsForEditorList,
                },
            ],
            {
                result: expectedLessons, // doesn't matter
            },
        );

        SpecHelper.expectElements(elem, '.content-search-container .faux-button:eq(0).selected'); // content by default
        SpecHelper.updateTextInput(elem, '.content-search-container input[type="text"]', 'SEARCH TEXT');
        SpecHelper.click(elem, '.content-search-container button');

        Lesson.flush('index');
    });

    it('should allow for full-text searching of comments', () => {
        render();

        Lesson.expect(
            'index',
            [
                {
                    filters: {
                        published: false,
                        archived: false,
                        in_users_locale_or_en: false,
                        comments_search: 'SEARCH TEXT',
                        locale: 'en',
                    },
                    'fields[]': Lesson.fieldsForEditorList,
                },
            ],
            {
                result: expectedLessons, // doesn't matter
            },
        );

        SpecHelper.expectElements(elem, '.content-search-container .faux-button:eq(0).selected'); // content by default
        SpecHelper.click(elem, '.content-search-container .faux-button:eq(1)'); // comments
        SpecHelper.updateTextInput(elem, '.content-search-container input[type="text"]', 'SEARCH TEXT');
        SpecHelper.click(elem, '.content-search-container button');

        Lesson.flush('index');
    });

    it('should load other locales if requested', () => {
        render();
        SpecHelper.updateSelectize(elem, '[name="locale"]', 'es');
        flushLoad();
        expect(contentItemEditorLists.load).toHaveBeenCalledWith('Lesson', 'es');
    });

    function flushLoad() {
        loadCallback(expectedLessons);
        scope.$digest();
    }

    let loadCallback;

    function render(archived) {
        renderer = SpecHelper.renderer();
        const obj = {
            onLoad(cb) {
                loadCallback = cb;
                return obj;
            },
        };

        jest.spyOn(contentItemEditorLists, 'load').mockReturnValue(obj);
        renderer.render('<list-lessons></list-lessons>');
        scope = renderer.childScope;
        elem = renderer.elem;

        SpecHelper.expectElements(elem, 'front-royal-spinner:eq(0)');

        // pretend that 'index' call to the api is returning
        $timeout.flush();
        flushLoad();
        expect(contentItemEditorLists.load).toHaveBeenCalledWith('Lesson', 'en');
        SpecHelper.expectNoElement(renderer.elem, 'front-royal-spinner');

        if (archived) {
            expectedLessons.forEach(lesson => {
                lesson.archived = true;
            });
            Lesson.expect(
                'index',
                [
                    {
                        filters: {
                            published: false,
                            archived: true,
                            in_users_locale_or_en: false,
                            locale: 'en',
                        },
                        'fields[]': Lesson.fieldsForEditorList,
                    },
                ],
                {
                    result: expectedLessons,
                },
            );
            SpecHelper.click(elem, 'button[name="lesson-archive-toggle"]');
            scope.$digest();
            Lesson.flush('index');
            SpecHelper.expectNoElement(renderer.elem, 'front-royal-spinner');
        }

        return renderer;
    }
});
