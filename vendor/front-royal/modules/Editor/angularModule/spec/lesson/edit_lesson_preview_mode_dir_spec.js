import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

describe('Editor.EditLessonPreviewModeDir', () => {
    let SpecHelper;
    let elem;
    let scope;
    let $rootScope;
    let lesson;
    let playerViewModel;
    let AppHeaderViewModel;
    let Capabilities;
    let $timeout;
    let DialogModal;
    let ErrorLogService;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                Capabilities = SpecHelper.mockCapabilities();
                $rootScope = $injector.get('$rootScope');
                $timeout = $injector.get('$timeout');
                AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
                DialogModal = $injector.get('DialogModal');
                const Lesson = $injector.get('Lesson');
                $injector.get('LessonFixtures');
                lesson = Lesson.fixtures.getInstance();
                playerViewModel = lesson.createPlayerViewModel();
                ErrorLogService = $injector.get('ErrorLogService');

                jest.spyOn(ErrorLogService, 'notify');

                $rootScope.currentUser = {
                    email: 'user@example.com',
                    copyAttrs() {},
                };

                render();
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('showScreen toggling', () => {
        it('should toggle screens when clicking the buttons', () => {
            assertToggling();
        });

        it('should save the value of showScreen across instances of the directive', () => {
            SpecHelper.selectOptionByLabel(elem, 'select', 'Show Both');

            render();
            SpecHelper.expectElementNotHidden(elem, '.monitor');
            SpecHelper.expectElementNotHidden(elem, '.phone');
        });

        it('should display a warning without localStorage', () => {
            Object.defineProperty(Capabilities, 'localStorage', {
                value: false,
            });
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            render();
            expect(DialogModal.alert).toHaveBeenCalledWith({
                title: 'Preview Error',
                content:
                    'Preview mode will not work properly without local storage enabled. If using Safari, please disable Private Browsing mode.',
                classes: ['server-error-modal'],
            });
        });

        function assertToggling() {
            SpecHelper.expectElementNotHidden(elem, '.monitor');
            SpecHelper.expectElementHidden(elem, '.phone');

            SpecHelper.selectOptionByLabel(elem, 'select', 'Show Both');
            SpecHelper.expectElementNotHidden(elem, '.monitor');
            SpecHelper.expectElementNotHidden(elem, '.phone');

            SpecHelper.selectOptionByLabel(elem, 'select', 'Show iPhone 5');
            SpecHelper.expectElementHidden(elem, '.monitor');
            SpecHelper.expectElementNotHidden(elem, '.phone');
        }
    });

    it('should leave preview mode when Edit Lesson button is clicked', () => {
        jest.spyOn(scope, '$emit').mockImplementation(() => {});
        SpecHelper.click(elem, '[data-id="preview"]');
        expect(scope.$emit).toHaveBeenCalledWith('editLesson:exitPreviewMode');
    });

    describe('onIframeLoad', () => {
        it('should call registerSharedPreviewScope on each iframe', () => {
            const registerSharedPreviewScope1 = mockIframeInitialized('iframe-desktop');
            const registerSharedPreviewScope2 = mockIframeInitialized('iframe-iphone');
            SpecHelper.expectElement(elem, '.loading');
            scope.onIframeLoad('iframe-desktop');
            scope.onIframeLoad('iframe-iphone');
            expect(registerSharedPreviewScope1).toHaveBeenCalledWith(
                scope,
                $rootScope.currentUser,
                $injector.get('ConfigFactory'),
            );
            expect(registerSharedPreviewScope2).toHaveBeenCalledWith(
                scope,
                $rootScope.currentUser,
                $injector.get('ConfigFactory'),
            );
            SpecHelper.expectNoElement(elem, '.loading');
        });

        it('should try again to call registerSharedPreviewScope on each iframe if it is not available the first time', () => {
            // iframes load before they are initialized
            scope.onIframeLoad('iframe-desktop');
            scope.onIframeLoad('iframe-iphone');
            $timeout.flush(0);

            // Desktop iframe is initialized, during the first
            // 100ms delay.  After the delay, registerSharedPreviewScope
            // is called on that iframe
            const registerSharedPreviewScope1 = mockIframeInitialized('iframe-desktop');
            $timeout.flush(100);
            expect(registerSharedPreviewScope1).toHaveBeenCalled();

            // After another 100ms delay, the iphone iframe is initialized,
            // and registerSharedPreviewScope is called on that one.
            const registerSharedPreviewScope2 = mockIframeInitialized('iframe-iphone');
            $timeout.flush(100);
            expect(registerSharedPreviewScope2).toHaveBeenCalled();
        });

        it('should show retry button after failing for a second and allow for retrying', () => {
            assertFailAndRetry();
        });

        it('should log an error if retry fails', () => {
            // assertFailAndRetry mocks out waiting through
            // 10 delay cycles for the iframe to be initialized,
            // then giving up and replacing the iframe on the screen.
            assertFailAndRetry();

            // Mock out waiting through 10 more delay cycles
            // for the swapped iframe to load
            scope.onIframeLoad('iframe-desktop');
            for (let i = 0; i <= 10; i++) {
                $timeout.flush(100);
            }

            // Now send an error message
            expect(ErrorLogService.notify).toHaveBeenCalledWith('Preview iframes failed to load twice in a row');
        });

        function assertFailAndRetry() {
            scope.onIframeLoad('iframe-desktop');
            // the iframe is not initialized. Retry 10 times waiting
            // for it to initialize
            for (let i = 0; i <= 10; i++) {
                $timeout.flush(100);
            }

            SpecHelper.click(elem, '.loading .slowly button');

            // clicking retry should remove the iframes for a moment and make new ones
            SpecHelper.expectNoElement(elem, 'iframe');
            $timeout.flush();
            SpecHelper.expectElements(elem, 'iframe', 2);

            // Error message should not be called until we retry
            expect(ErrorLogService.notify).not.toHaveBeenCalled();
        }

        function mockIframeInitialized(id) {
            const iframe = scope.getIframe(id);
            iframe.contentWindow = { registerSharedPreviewScope: jest.fn() };
            return iframe.contentWindow.registerSharedPreviewScope;
        }
    });

    it('should hide the app header and replace it on destroy', () => {
        jest.spyOn(AppHeaderViewModel, 'toggleVisibility').mockImplementation(() => {});
        render();
        expect(AppHeaderViewModel.toggleVisibility).toHaveBeenCalledWith(false);
        AppHeaderViewModel.toggleVisibility.mockClear();
        scope.$destroy();
        expect(AppHeaderViewModel.toggleVisibility).toHaveBeenCalledWith(true);
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.scope.playerViewModel = playerViewModel;
        renderer.render('<edit-lesson-preview-mode player-view-model="playerViewModel"></edit-lesson-preview-mode>');
        elem = renderer.elem;
        scope = renderer.childScope;
        const iframes = {
            'iframe-desktop': {},
            'iframe-iphone': {},
        };
        scope.getIframe = id => iframes[id];
        $timeout.flush(); // render iframes
    }
});
