import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Users/angularModule/spec/_mock/fixtures/users';

describe('Editor::PinAndPublishLessonDir', () => {
    let elem;
    let SpecHelper;
    let lesson;
    let scope;
    let Lesson;
    let currentUser;
    let DialogModal;
    let ngToast;
    let $rootScope;
    let User;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        // look for a note about NoUnhandledRejectionExceptions use below (error handling)
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper', 'NoUnhandledRejectionExceptions');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $injector.get('LessonFixtures');
            DialogModal = $injector.get('DialogModal');
            ngToast = $injector.get('ngToast');
            $rootScope = $injector.get('$rootScope');
            Lesson = $injector.get('Lesson');
            lesson = Lesson.fixtures.getInstance();
            User = $injector.get('User');
            $injector.get('UserFixtures');
        });
    });

    beforeEach(() => {
        angular.mock.inject($injector => {
            currentUser = User.fixtures.getInstance();
            $rootScope.currentUser = currentUser;
            jest.spyOn(DialogModal, 'hideAlerts').mockImplementation(() => {});
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});

            Lesson.prototype.grade = () => $injector.get('$q').when();

            render();
        });
    });

    afterAll(() => {
        SpecHelper.cleanup();
    });

    it('should include a grade', () => {
        SpecHelper.expectElement(elem, 'grade-frame-list');
    });

    it('should allow for setting the pinned properties and saving', () => {
        SpecHelper.updateTextInput(elem, '[name="pinned_title"]', 'Title');
        SpecHelper.updateTextInput(elem, '[name="pinned_description"]', 'Description');
        Lesson.expect('update').toBeCalledWith(lesson, {
            should_publish: false,
            should_pin: true,
            pinned_title: 'Title',
            pinned_description: 'Description',
        });
        SpecHelper.click(elem, '.submit');
        expect(DialogModal.hideAlerts).not.toHaveBeenCalled();
        SpecHelper.expectElement(elem, 'front-royal-spinner');
        Lesson.flush('update');
        expect(DialogModal.hideAlerts).toHaveBeenCalled();
        expect(ngToast.create).toHaveBeenCalledWith({
            content: 'Save & Pin successful',
            className: 'success',
        });
    });

    it('should require a version title', () => {
        SpecHelper.expectElementDisabled(elem, '.submit');
        SpecHelper.updateTextInput(elem, '[name="pinned_title"]', 'Title');
        SpecHelper.expectElementEnabled(elem, '.submit');
    });

    describe('publishing', () => {
        it('should hide publishing options if no publishing permissions', () => {
            SpecHelper.expectNoElement(elem, '.should-publish-wrapper');
            jest.spyOn(currentUser, 'canPublish').mockReturnValue(true);
            scope.$digest();
            SpecHelper.expectElement(elem, '.should-publish-wrapper');
        });

        it('should allow publishing', () => {
            jest.spyOn(currentUser, 'canPublish').mockReturnValue(true);
            scope.$digest();
            SpecHelper.checkCheckbox(elem, '[name="should_publish"]');
            SpecHelper.updateTextInput(elem, '[name="pinned_title"]', 'Title');
            SpecHelper.updateTextInput(elem, '[name="pinned_description"]', 'Description');

            Lesson.expect('update').toBeCalledWith(lesson, {
                should_publish: true,
                should_pin: true,
                pinned_title: 'Title',
                pinned_description: 'Description',
            });
            SpecHelper.click(elem, '.submit');
            Lesson.flush('update');
        });

        it('should check box by default if shouldPublish is true', () => {
            jest.spyOn(currentUser, 'canPublish').mockReturnValue(true);
            render(true);
            SpecHelper.expectCheckboxChecked(elem, '[name="should_publish"]');
        });
    });

    it('should have a working cancel button', () => {
        SpecHelper.click(elem, '.cancel');
        expect(DialogModal.hideAlerts).toHaveBeenCalled();
    });

    it('should hide alert on save failure', () => {
        // see also: NoUnhandledRejectionExceptions module use above

        SpecHelper.updateTextInput(elem, '[name="pinned_title"]', 'Title');
        SpecHelper.updateTextInput(elem, '[name="pinned_description"]', 'Description');
        Lesson.expect('save').fails({});
        SpecHelper.click(elem, '.submit');
        expect(DialogModal.hideAlerts).not.toHaveBeenCalled();
        Lesson.flush('save');
        expect(DialogModal.hideAlerts).toHaveBeenCalled();
        expect(ngToast.create).not.toHaveBeenCalled();
    });

    function render(shouldPublish) {
        const renderer = SpecHelper.renderer();
        renderer.scope.lesson = lesson;
        renderer.scope.shouldPublish = shouldPublish || false;
        renderer.render(
            '<pin-and-publish-lesson lesson="lesson" should-publish="shouldPublish"></pin-and-publish-lesson>',
        );
        elem = renderer.elem;
        scope = renderer.childScope;
    }
});
