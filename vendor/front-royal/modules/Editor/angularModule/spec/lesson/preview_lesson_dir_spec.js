import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import stubSpecLocale from 'Translation/stubSpecLocale';
import setSpecLocales from 'Translation/setSpecLocales';
import contentAccessHelperLocales from 'Lessons/locales/lessons/shared/content_access_helper-en.json';

stubSpecLocale('lessons.models.lesson.lesson_player_view_model.lesson_x_of_x');
setSpecLocales(contentAccessHelperLocales);

describe('Editor.PreviewLessonDir', () => {
    let SpecHelper;
    let scope;
    let $window;
    let sharedPreviewScope;
    let lesson;
    let playerViewModel;
    let safeApply;
    let safeApplySpy;
    let EntityMetadata;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Authentication', 'FrontRoyal.Editor', 'SpecHelper');

        angular.mock.module($provide => {
            safeApplySpy = jest.fn();
            // eslint-disable-next-line no-shadow
            safeApply = scope => {
                safeApplySpy(scope);
                scope.$apply();
            };

            $provide.value('safeApply', safeApply);
        });

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                $window = $injector.get('$window');
                const Lesson = $injector.get('Lesson');
                $injector.get('LessonFixtures');
                EntityMetadata = $injector.get('EntityMetadata');
                lesson = Lesson.fixtures.getInstance();
                playerViewModel = lesson.createPlayerViewModel();
                const ValidationResponder = $injector.get('ValidationResponder');

                ValidationResponder.initializePreviewMode();
                SpecHelper.stubEventLogging();
            },
        ]);
        SpecHelper.stubConfig();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('without sharedPreviewScope set', () => {
        it('should add a show-lesson directive once the playerViewModel is set', () => {
            expect(() => {
                render();
            }).toThrow(new Error('Cannot initialize lesson preview without a shared preview scope'));
        });
    });

    describe('with sharedPreviewScope set', () => {
        beforeEach(() => {
            setSharedPreviewScope();
            render();
            safeApplySpy.mockClear();
        });

        it('should call safeApply when the sharedPreviewScope is digested', () => {
            safeApplySpy.mockClear();
            expect(safeApplySpy).not.toHaveBeenCalled();
            sharedPreviewScope.$digest();
            expect(safeApplySpy).toHaveBeenCalled();
        });

        it('should stop responding to sharedPreviewScope digests after destroy', () => {
            scope.$destroy();
            sharedPreviewScope.$digest();
            expect(safeApplySpy).not.toHaveBeenCalled();
        });

        it('should call onPreviewDigest', () => {
            jest.spyOn(sharedPreviewScope, 'onPreviewDigest').mockImplementation(() => {});
            scope.$apply();
            expect(sharedPreviewScope.onPreviewDigest).toHaveBeenCalled();
        });

        it('should digest on window unloading', () => {
            jest.spyOn(sharedPreviewScope, 'onPreviewDigest').mockImplementation(() => {});
            $($window).trigger('unload');
            expect(sharedPreviewScope.onPreviewDigest).toHaveBeenCalled();
        });
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.render('<preview-lesson></preview-lesson>');
        scope = renderer.childScope;
    }

    function setSharedPreviewScope() {
        // We don't want to create a real scope here
        // with $rootScope.$new() because then it would be
        // connected with the scope of our directive.  In the
        // wild, the sharedPreviewScope will live in a different
        // window and so the digest cycles of the two will
        // be unrelated.

        playerViewModel.lesson.entity_metadata = EntityMetadata.new({
            title: 'askjhsdf',
            description: 'sdjfhsdf',
        });
        sharedPreviewScope = {
            playerViewModel,
            onPreviewDigest() {},
            $watch(callback) {
                this.callback = callback;
                return () => {
                    this.callback = undefined;
                };
            },
            $digest() {
                if (this.callback) {
                    this.callback();
                }
            },
            $apply() {
                if (this.callback) {
                    this.callback();
                }
            },
        };
        $window.registerSharedPreviewScope(sharedPreviewScope, undefined, $injector.get('ConfigFactory'));
        sharedPreviewScope.$digest();
    }
});
