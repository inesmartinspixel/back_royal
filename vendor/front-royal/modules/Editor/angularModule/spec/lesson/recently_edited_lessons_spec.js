import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

describe('Editor.RecentlyEditedLessons', () => {
    let SpecHelper;
    let $injector;
    let RecentlyEditedLessons;
    let ClientStorage;
    let Lesson;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                RecentlyEditedLessons = $injector.get('RecentlyEditedLessons');
                ClientStorage = $injector.get('ClientStorage');
                Lesson = $injector.get('Lesson');
                $injector.get('LessonFixtures');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('lessons', () => {
        it('should return the list of cached lessons', () => {
            const cached = setClientStorage();
            const recentlyEditedLessons = new RecentlyEditedLessons();
            expect(recentlyEditedLessons.lessons.length).toBe(cached.length);
            expect(recentlyEditedLessons.lessons[0].asJson()).toEqual(cached[0].lesson);
        });
        it('should remove any lessons that are too old', () => {
            const cached = setClientStorage([
                {
                    lastOpened: new Date().getTime() - (RecentlyEditedLessons.AGE_LIMIT + 1),
                    lesson: _.pick(Lesson.fixtures.getInstance().asJson(), Lesson.fieldsForEditorList),
                },
                {
                    lastOpened: new Date().getTime(),
                    lesson: _.pick(Lesson.fixtures.getInstance().asJson(), Lesson.fieldsForEditorList),
                },
            ]);
            const recentlyEditedLessons = new RecentlyEditedLessons();
            expect(recentlyEditedLessons.lessons.length).toBe(1);
            expect(recentlyEditedLessons.lessons[0].asJson()).toEqual(cached[1].lesson);
        });
    });

    describe('update', () => {
        it('should update lessons that are in cache', () => {
            const lesson = Lesson.fixtures.getInstance();
            setClientStorage([
                {
                    lastOpened: new Date().getTime(),
                    lesson: _.pick(lesson.asJson(), Lesson.fieldsForEditorList),
                },
            ]);
            const recentlyEditedLessons = new RecentlyEditedLessons();
            lesson.title = 'New Title';
            recentlyEditedLessons.update('en', [lesson]);
            expect(recentlyEditedLessons.lessons[0].title).toEqual('New Title');
        });
        it('should remove lessons that are in cache but not passed in, but only if locale matches', () => {
            const lesson = Lesson.fixtures.getInstance();
            setClientStorage();
            const recentlyEditedLessons = new RecentlyEditedLessons();
            recentlyEditedLessons.update('fakeLocale', []);
            expect(recentlyEditedLessons.lessons.length).toBe(1);
            recentlyEditedLessons.update('en', [lesson]);
            expect(recentlyEditedLessons.lessons.length).toBe(0);
        });
        it('should save', () => {
            const lesson = Lesson.fixtures.getInstance();
            setClientStorage();
            const recentlyEditedLessons = new RecentlyEditedLessons();
            jest.spyOn(recentlyEditedLessons, '_save').mockImplementation(() => {});
            recentlyEditedLessons.update('en', [lesson]);
            expect(recentlyEditedLessons._save).toHaveBeenCalled();
        });
    });

    describe('add', () => {
        it('should replace an entry that is already in the cache', () => {
            const lesson = Lesson.fixtures.getInstance();
            const origLastOpened = 1;
            setClientStorage([
                {
                    lastOpened: origLastOpened,
                    lesson: _.pick(lesson.asJson(), Lesson.fieldsForEditorList),
                },
            ]);

            const recentlyEditedLessons = new RecentlyEditedLessons();
            recentlyEditedLessons.add(lesson);
            expect(recentlyEditedLessons.lessons.length).toBe(1);
            expect(recentlyEditedLessons.$$lessonEntries[0].lastOpened).not.toBe(1);
        });
        it('should add a new entry to the cache', () => {
            const lesson = Lesson.fixtures.getInstance();
            setClientStorage();
            const recentlyEditedLessons = new RecentlyEditedLessons();
            recentlyEditedLessons.add(lesson);
            expect(recentlyEditedLessons.lessons.length).toBe(2);
            expect(recentlyEditedLessons.lessons[0]).toBe(lesson);
        });
        it('should enforce MAX_LENGTH', () => {
            const recentlyEditedLessons = new RecentlyEditedLessons();
            for (let i = 0; i < RecentlyEditedLessons.MAX_LENGTH + 3; i++) {
                recentlyEditedLessons.add(Lesson.fixtures.getInstance());
            }
            expect(recentlyEditedLessons.lessons.length).toBe(RecentlyEditedLessons.MAX_LENGTH);
        });
        it('should save', () => {
            const recentlyEditedLessons = new RecentlyEditedLessons();
            jest.spyOn(recentlyEditedLessons, '_save').mockImplementation(() => {});
            recentlyEditedLessons.add(Lesson.fixtures.getInstance());
            expect(recentlyEditedLessons._save).toHaveBeenCalled();
        });
    });

    describe('_save', () => {
        it('should save to localStorage', () => {
            const lesson = Lesson.fixtures.getInstance();
            const recentlyEditedLessons = new RecentlyEditedLessons();
            recentlyEditedLessons.add(lesson);
            const lastOpened = recentlyEditedLessons.$$lessonEntries[0].lastOpened;

            jest.spyOn(ClientStorage, 'setItem').mockImplementation(() => {});
            recentlyEditedLessons._save();
            expect(ClientStorage.setItem).toHaveBeenCalledWith(
                RecentlyEditedLessons.CSKEY,
                angular.toJson([
                    {
                        lastOpened,
                        lesson: _.pick(lesson.asJson(), Lesson.fieldsForEditorList),
                    },
                ]),
            );
        });
    });

    function setClientStorage(
        val = [
            {
                lastOpened: new Date().getTime(),
                lesson: _.pick(Lesson.fixtures.getInstance().asJson(), Lesson.fieldsForEditorList),
            },
        ],
    ) {
        jest.spyOn(ClientStorage, 'getItem').mockReturnValue(JSON.stringify(val));
        return val;
    }
});
