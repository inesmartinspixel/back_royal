import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Users/angularModule/spec/_mock/fixtures/users';

describe('Editor::UnpublishLessonDir', () => {
    let elem;
    let SpecHelper;
    let lesson;
    let scope;
    let Lesson;
    let currentUser;
    let DialogModal;
    let ngToast;

    beforeEach(() => {
        // look for a note about NoUnhandledRejectionExceptions use below (error handling)
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper', 'NoUnhandledRejectionExceptions');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $injector.get('LessonFixtures');
            DialogModal = $injector.get('DialogModal');
            ngToast = $injector.get('ngToast');
            const $rootScope = $injector.get('$rootScope');
            Lesson = $injector.get('Lesson');
            lesson = Lesson.fixtures.getInstance();

            const User = $injector.get('User');
            $injector.get('UserFixtures');
            currentUser = User.fixtures.getInstance();
            $rootScope.currentUser = currentUser;
            jest.spyOn(DialogModal, 'hideAlerts').mockImplementation(() => {});
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});

            render();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for setting the pinned properties and saving', () => {
        Lesson.expect('update').toBeCalledWith(lesson, {
            should_unpublish: true,
        });
        SpecHelper.click(elem, '.submit');
        expect(DialogModal.hideAlerts).not.toHaveBeenCalled();
        SpecHelper.expectElement(elem, 'front-royal-spinner');
        Lesson.flush('update');
        expect(DialogModal.hideAlerts).toHaveBeenCalled();
        expect(ngToast.create).toHaveBeenCalledWith({
            content: 'Unpublish successful',
            className: 'success',
        });
    });

    it('should show stream titles', () => {
        SpecHelper.expectElementText(elem, '.courses', 'This lesson is not in any courses.');
        lesson.stream_titles = ['a stream'];
        scope.$digest();
        SpecHelper.expectElementText(
            elem,
            '.courses',
            'This lesson will be removed from the following courses: a stream',
        );
    });

    it('should have a working cancel button', () => {
        SpecHelper.click(elem, '.cancel');
        expect(DialogModal.hideAlerts).toHaveBeenCalled();
    });

    it('should hide alert on save failure', () => {
        // see also: NoUnhandledRejectionExceptions module use above

        Lesson.expect('save').fails({});
        SpecHelper.click(elem, '.submit');
        expect(DialogModal.hideAlerts).not.toHaveBeenCalled();
        Lesson.flush('save');
        expect(DialogModal.hideAlerts).toHaveBeenCalled();
        expect(ngToast.create).not.toHaveBeenCalled();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.scope.lesson = lesson;
        renderer.render('<unpublish-lesson lesson="lesson"></unpublish-lesson>');
        elem = renderer.elem;
        scope = renderer.childScope;
    }
});
