import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';

describe('Editor.ArchiveLessonDir', () => {
    let lesson;
    let elem;
    let SpecHelper;
    let DialogModal;
    let $injector;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');
        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            $injector.get('LessonFixtures');
            SpecHelper = $injector.get('SpecHelper');
            DialogModal = $injector.get('DialogModal');
        });
        DialogModal.hideAlerts = jest.fn(); // necessary for beforeAll use
    });

    beforeEach(() => {
        lesson = $injector.get('Lesson').fixtures.getInstance();
        render();
        jest.spyOn(lesson, 'save').mockReturnValue({
            then() {},
        });
    });

    afterAll(() => {
        SpecHelper.cleanup();
    });

    it('should archive the lesson when archive is clicked', () => {
        SpecHelper.click(elem, '[data-id="archive"]');
        expect(lesson.archived).toBe(true);
        expect(lesson.save).toHaveBeenCalled();
        expect(DialogModal.hideAlerts).toHaveBeenCalled();
    });

    it('should close the modal when close is clicked', () => {
        SpecHelper.click(elem, '[data-id="cancel"]');
        expect(lesson.archived).toBe(false);
        expect(lesson.save).not.toHaveBeenCalled();
        expect(DialogModal.hideAlerts).toHaveBeenCalled();
    });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.scope.lesson = lesson;
        renderer.render('<archive-lesson lesson="lesson"/>');
        elem = renderer.elem;
        return renderer;
    }
});
