import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Users/angularModule/spec/_mock/fixtures/users';

describe('Editor.EditLessonDir', () => {
    let expectedLesson;
    let scope;
    let elem;
    let $location;
    let Lesson;
    let $filter;
    let SpecHelper;
    let updatedAt;
    let RecentlyEditedLessons;
    let EventLogger;
    let $timeout;
    let $injector;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                $location = $injector.get('$location');
                Lesson = $injector.get('Lesson');
                $injector.get('LessonFixtures');
                SpecHelper = $injector.get('SpecHelper');
                $location = $injector.get('$location');
                $filter = $injector.get('$filter');
                SpecHelper.stubDirective('contentTopicsEditor');
                SpecHelper.stubDirective('contentItemEditLocale');
                RecentlyEditedLessons = $injector.get('RecentlyEditedLessons');
                EventLogger = $injector.get('EventLogger');
                $timeout = $injector.get('$timeout');
            },
        ]);

        updatedAt = new Date('01/06/2015 17:03:35');
        Lesson.setAdapter('Iguana.Mock.Adapter');

        SpecHelper.stubCurrentUser('learner');
        SpecHelper.stubDirective('modalPopup'); // this works around a problem with transcluding the modal popup contents into the DOM
    });

    beforeEach(() => {
        jest.spyOn(EventLogger.instance, 'log').mockImplementation(() => {});
        expectedLesson = Lesson.fixtures.getInstance({
            version_id: '7', // lessons loaded up for the editor will have a version_id included
            updated_at: updatedAt.getTime() / 1000,

            version_history: [
                {
                    last_editor: {
                        name: 'John Doe',
                    },
                    updated_at: updatedAt.getTime() / 1000,
                    version_id: '7',
                },
                {
                    last_editor: {
                        name: 'John Doe',
                    },
                    updated_at: updatedAt.getTime() / 1000 - 1,
                    version_id: '6',
                },
                {
                    last_editor: {
                        name: 'John Doe',
                    },
                    updated_at: updatedAt.getTime() / 1000 - 2,
                    version_id: '5',
                },
                {
                    last_editor: {
                        name: 'John Doe',
                    },
                    updated_at: updatedAt.getTime() / 1000 - 3,
                    version_id: '4',
                },
                {
                    last_editor: {
                        name: 'John Doe',
                    },
                    updated_at: updatedAt.getTime() / 1000 - 4,
                    version_id: '3',
                },
                {
                    last_editor: {
                        name: 'John Doe',
                    },
                    updated_at: updatedAt.getTime() / 1000 - 5,
                    version_id: '2',
                },
                {
                    last_editor: {
                        name: 'John Doe',
                    },
                    updated_at: updatedAt.getTime() / 1000 - 6,
                    version_id: '1',
                },
            ],
        });
    });

    afterAll(() => {
        SpecHelper.cleanup();
    });

    it('should show the editor for the lesson once it is loaded up', () => {
        expectedLesson.editorDirectiveName = 'test-directive';
        render(false);
        SpecHelper.expectElement(elem, 'front-royal-spinner');
        Lesson.flush('show');
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
        expect(scope.playerViewModel.lesson.asJson()).toEqual(expectedLesson.asJson());
        SpecHelper.expectElement(elem, 'test-directive');
    });

    it('should set the editorMode to true on the lesson', () => {
        render(true);
        expect(scope.playerViewModel.editorMode).toBe(true);
    });

    it('should load up a pinned version', () => {
        jest.spyOn($location, 'search').mockReturnValue({
            version: '42',
        });

        Lesson.expect(
            'show',
            [
                expectedLesson.id,
                {
                    filters: {
                        published: false,
                        version_id: '42',
                    },
                    'except[]': ['lesson_progress'],
                },
            ],
            {
                result: expectedLesson,
            },
        );

        SpecHelper.render(`<edit-lesson lesson-id="${expectedLesson.id}" />`);
        Lesson.flush('show');
    });

    it('should add lesson to recently edited ones', () => {
        jest.spyOn(RecentlyEditedLessons.prototype, 'add').mockImplementation(() => {});
        render(true);
        expect(RecentlyEditedLessons.prototype.add).toHaveBeenCalled();
    });

    it('should log ping events on mousedown', () => {
        render(true);
        expect(EventLogger.instance.log).toHaveBeenCalled(); // check fo initial ping
        EventLogger.instance.log.mockClear();
        elem.mousedown();
        elem.mousedown();
        $timeout.flush(59 * 1000);
        elem.mousedown();
        elem.mousedown();
        expect(EventLogger.instance.log).not.toHaveBeenCalled();
        $timeout.flush(1 * 1000);
        expect(EventLogger.instance.log.mock.calls.length).toEqual(1);
        expect(EventLogger.instance.log).toHaveBeenCalledWith('lesson:editor:ping', scope.playerViewModel.logInfo());
    });

    it('should log ping events on keydown', () => {
        render(true);
        expect(EventLogger.instance.log).toHaveBeenCalled(); // check fo initial ping
        EventLogger.instance.log.mockClear();
        elem.trigger('keydown');
        elem.trigger('keydown');
        $timeout.flush(59 * 1000);
        elem.trigger('keydown');
        elem.trigger('keydown');
        expect(EventLogger.instance.log).not.toHaveBeenCalled();
        $timeout.flush(1 * 1000);
        expect(EventLogger.instance.log.mock.calls.length).toEqual(1);
        expect(EventLogger.instance.log).toHaveBeenCalledWith('lesson:editor:ping', scope.playerViewModel.logInfo());
    });

    // see https://trello.com/c/hF2p1RkR
    it('should ping right away when coming back after a delay', () => {
        render(true);
        expect(EventLogger.instance.log).toHaveBeenCalled(); // check fo initial ping

        // the user goes away for a while
        $timeout.flush(300 * 1000);
        EventLogger.instance.log.mockClear();

        // Once the user comes back and clicks, we should ping
        elem.mousedown();
        expect(EventLogger.instance.log).toHaveBeenCalledWith('lesson:editor:ping', scope.playerViewModel.logInfo());
        EventLogger.instance.log.mockClear();

        // The user does some more stuff, but we shouldn't ping for another 60 seconds
        elem.mousedown();
        elem.mousedown();
        expect(EventLogger.instance.log).not.toHaveBeenCalled();

        // After 60 seconds, we should ping
        $timeout.flush(61 * 1000);
        expect(EventLogger.instance.log).toHaveBeenCalled();

        // If the user does not do anything else, we should not ping anymore
        EventLogger.instance.log.mockClear();
        $timeout.flush(120 * 1000);
        expect(EventLogger.instance.log).not.toHaveBeenCalled();
    });

    it('should ping and then destroy ping listeners on scope destroy', () => {
        render(true);
        expect(EventLogger.instance.log).toHaveBeenCalled(); // check fo initial ping
        EventLogger.instance.log.mockClear();
        scope.$destroy();
        expect(EventLogger.instance.log).toHaveBeenCalled(); // should be pinged during the destroy
        EventLogger.instance.log.mockClear();
        elem.mousedown();
        expect(EventLogger.instance.log).not.toHaveBeenCalled();
        $timeout.flush(61 * 1000);
        expect(EventLogger.instance.log).not.toHaveBeenCalled();
    });

    /*
        Since we have frames in the wild that existed before we implemented
        pre-rendered text formatting, we need to make sure that loading up
        a lesson automagically formats all the text in that lesson, so that
        if it is saved (and published), it will have formatted text.
    */
    describe('text formatting', () => {
        it('should format all text', () => {
            const FrameList = $injector.get('Lesson.FrameList');
            expectedLesson = FrameList.fixtures.getInstance({}, false, true);
            const textComponents = [
                expectedLesson.frames[0].reify().addText(),
                expectedLesson.frames[1].reify().addText(),
            ];
            expectedLesson = expectedLesson.asJson();

            const TextEditorViewModel = $injector.get(
                'Lesson.FrameList.Frame.Componentized.Component.Text.TextEditorViewModel',
            );
            jest.spyOn(TextEditorViewModel.prototype, 'formatText').mockImplementation(() => {});
            jest.spyOn(FrameList.prototype, 'createPlayerViewModel').mockImplementation(() => {}); // all kinds of things blow up if we create lesson play, so mock it out

            expect(_.chain(textComponents).pluck('formatted_text').uniq().value()).toEqual([undefined]);
            render(true);
            $timeout.flush();
            expect(TextEditorViewModel.prototype.formatText.mock.calls.length >= 2).toBe(true);
        });
    });

    describe('edit_lesson_main_form', () => {
        beforeEach(() => {
            renderEditLessonMainForm();
            jest.spyOn(scope.currentUser, 'canEditLesson').mockReturnValue(true);
            scope.$digest();
        });

        it('should allow for setting test flag', () => {
            SpecHelper.checkCheckbox(elem, '[name="test"]');
            expect(expectedLesson.test).toBe(true);
            SpecHelper.uncheckCheckbox(elem, '[name="test"]');
            expect(expectedLesson.test).toBe(false);
        });

        it('should show version history', () => {
            SpecHelper.expectElements(elem, '.version-row', 6);
            SpecHelper.expectElementText(
                elem,
                '[data-id="version_history_0"]',
                `John Doe at ${$filter('amDateFormat')(updatedAt, 'l h:mm:ss a')} (viewing this version)`,
            );

            SpecHelper.expectHasClass(elem, '.version-row:eq(0)', 'active');
            SpecHelper.expectDoesNotHaveClass(elem, '.version-row:eq(1)', 'active');

            SpecHelper.click(elem, '[data-id="version_history"] .show-more-history');
            SpecHelper.expectElements(elem, '.version-row', expectedLesson.version_history.length);

            scope.launchVersion = jest.fn();
            scope.restoreVersion = jest.fn();

            // Because we've stubbed the modalPopup directive, we can just directly click the popover contents that are
            // already rendered directly into the DOM at this point

            SpecHelper.click(elem, '.version-row:eq(1) .view-old-version');
            expect(scope.launchVersion).toHaveBeenCalled();

            SpecHelper.click(elem, '.version-row:eq(1) .restore-old-version');
            expect(scope.restoreVersion).toHaveBeenCalled();
        });

        it('should show version info for old versions', () => {
            expectedLesson.old_version = true;
            scope.$digest();
            SpecHelper.expectNoElement(elem, '[data-id="version_history"]');
            SpecHelper.expectEqual(
                'This is an old version saved by',
                elem.find('#history-panel').text().trim().slice(0, 31),
            );
        });

        it('should show published status', () => {
            SpecHelper.expectElementText(
                elem,
                '[data-id="published_status"]',
                'Last Published This lesson is not published.',
            );

            expectedLesson.version_history[0].was_published = true;
            expectedLesson.published_version_id = 'something';
            expectedLesson.published_at = updatedAt.getTime() / 1000;
            scope.$digest();
            SpecHelper.expectElementText(
                elem,
                '[data-id="published_status"]',
                `Last Published at ${$filter('amDateFormat')(updatedAt, 'l h:mm:ss a')}`,
            );

            // if the most recent version is the published version, then it should be red
            SpecHelper.expectNoElement(elem, '[data-id="published_status"] .most-recent-version');
            expectedLesson.published_version_id = expectedLesson.version_id;
            scope.$digest();
            SpecHelper.expectElement(elem, '[data-id="published_status"] .most-recent-version');
        });

        it('should support setting assessment flag', () => {
            SpecHelper.checkCheckbox(elem, '[name="assessment"]');
            expect(expectedLesson.assessment).toBe(true);
            SpecHelper.uncheckCheckbox(elem, '[name="assessment"]');
            expect(expectedLesson.assessment).toBe(false);
        });

        describe('practice lessons', () => {
            it('should call ensurePracticeLesson and open it', () => {
                Object.defineProperty(expectedLesson, 'supportsPractice', {
                    value: true,
                });
                scope.$digest();
                jest.spyOn(expectedLesson, 'openPracticeEditor').mockImplementation(() => {});
                SpecHelper.click(elem, '[name="open_practice_lesson"]');
                expect(expectedLesson.openPracticeEditor).toHaveBeenCalled();
            });

            it('should hide practice link if supportsPractice != true', () => {
                expect(expectedLesson.supportsPractice).not.toBe(true);
                SpecHelper.expectNoElement(elem, '[name="open_practice_lesson"]');
            });
        });

        describe('pinning history', () => {
            it('should show checkmarks on published versions', () => {
                expectedLesson = Lesson.fixtures.getInstance({
                    version_id: '4',
                    published_version_id: '3',
                    version_history: [
                        {
                            version_id: '4',
                            created_at: 0,
                            updated_at: 0,
                        },
                        {
                            pinned: true,
                            was_published: true,
                            version_id: '3',
                            created_at: 0,
                            updated_at: 0,
                        },
                        {
                            pinned: true,
                            version_id: '2',
                            created_at: 0,
                            updated_at: 0,
                        },
                        {
                            pinned: true,
                            was_published: true,
                            version_id: '1',
                            created_at: 0,
                            updated_at: 0,
                        },
                    ],
                    published_at: 0,
                });
                renderEditLessonMainForm();
                SpecHelper.expectElements(elem, '.version-row', expectedLesson.version_history.length);
                SpecHelper.expectElements(elem, '.version-row .pinned-icon', 3);
                SpecHelper.expectElements(elem, '.version-row .published-checkmark', 2);
                SpecHelper.expectElements(elem, '.version-row .currently-published', 1);
            });
        });

        describe('publishing', () => {
            beforeEach(() => {
                jest.spyOn(scope.currentUser, 'canPublish').mockReturnValue(true);
                scope.$digest();
            });

            it('should hide publish buttons unless user has publish access', () => {
                SpecHelper.expectElement(elem, '[data-id="publish_actions"]');
                scope.currentUser = {
                    // eslint-disable-next-line lodash-fp/prefer-constant
                    canPublish() {
                        return false;
                    },
                };
                scope.$digest();
                SpecHelper.expectNoElement(elem, '[data-id="publish_actions"]');
            });
            it('should hide publish buttons unless lesson is editable', () => {
                SpecHelper.expectElement(elem, '[data-id="publish_actions"]');
                Object.defineProperty(scope.lesson, 'editable', {
                    value: false,
                });
                scope.$digest();
                SpecHelper.expectNoElement(elem, '[data-id="publish_actions"]');
            });
            it('should support publishing', () => {
                scope.showPinAndPublishDialog = jest.fn();
                SpecHelper.click(elem, '[data-id="publish_actions"] .publish');
                expect(scope.showPinAndPublishDialog).toHaveBeenCalledWith(true);
            });
            it('should support unpublishing', () => {
                // unpublish button should be hidden unless lesson is published
                SpecHelper.expectNoElement(elem, '[data-id="publish_actions"] .unpublish');
                expectedLesson.published_at = 12345;
                scope.$digest();
                SpecHelper.expectElement(elem, '[data-id="publish_actions"] .unpublish');

                scope.showUnpublishDialog = jest.fn();

                SpecHelper.click(elem, '[data-id="publish_actions"] .unpublish');
                expect(scope.showUnpublishDialog).toHaveBeenCalled();
            });
        });

        function renderEditLessonMainForm() {
            const renderer = SpecHelper.render(
                '<div><div ng-include="\'Editor/edit_lesson_main_form.html\'"></div></div>',
            );
            renderer.scope.lesson = expectedLesson;
            elem = renderer.elem;
            scope = renderer.scope;

            // this actually happens in the editLessonTypeBase directive. Sucks
            // that we have to do it explicitly here as well
            scope.maxVersionsToShow = {
                n: 6,
            };
            scope.$digest();
        }
    });

    function render(flush) {
        Lesson.expect(
            'show',
            [
                expectedLesson.id,
                {
                    filters: {
                        published: false,
                    },
                    'except[]': ['lesson_progress'],
                },
            ],
            {
                result: expectedLesson,
            },
        );

        const renderer = SpecHelper.render(`<edit-lesson lesson-id="${expectedLesson.id}" />`);
        elem = renderer.elem;
        scope = renderer.childScope;
        if (flush) {
            Lesson.flush('show');
        }
    }
});
