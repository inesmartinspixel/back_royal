import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';

describe('Editor::Stream::ListStreamsDir', () => {
    let SpecHelper;
    let Stream;
    let EntityMetadata;
    let streams;
    let elem;
    let scope;
    let $location;
    let contentItemEditorLists;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject($injector => {
            $injector.get('StreamFixtures');
            $injector.get('MockIguana');
            SpecHelper = $injector.get('SpecHelper');
            Stream = $injector.get('Lesson.Stream');
            $location = $injector.get('$location');
            EntityMetadata = $injector.get('EntityMetadata');
            SpecHelper.stubDirective('editStream');
            contentItemEditorLists = $injector.get('contentItemEditorLists');
            $timeout = $injector.get('$timeout');

            streams = [Stream.fixtures.getInstance(), Stream.fixtures.getInstance(), Stream.fixtures.getInstance()];

            SpecHelper.stubConfig({
                social_name: 'some_twitter_account',
            });

            render();
        });
        SpecHelper.stubDirective('dirPaginationControls');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should display streams', () => {
        const links = SpecHelper.expectElements(elem, 'td[data-id="title"]', streams.length);

        for (let i = 0; i < streams.length; i++) {
            expect(links[i].textContent.trim()).toEqual(streams[i].title);
        }
    });

    it('should follow a stream link when clicked on', () => {
        jest.spyOn($location, 'url').mockImplementation(() => {});
        SpecHelper.click(elem, 'button[data-id="edit"]', 0);
        expect($location.url).toHaveBeenCalledWith(`/editor/course/${streams[0].id}/edit`);
    });

    it('should support creating a new stream', () => {
        SpecHelper.click(elem, 'button[name="create"]');
        SpecHelper.updateTextInput(elem, '[name="title"]', 'New Stream');
        const createButton = SpecHelper.expectElementEnabled(elem, 'button[name="save"]');

        const stream = streams[0];
        Stream.expect('create').returns(stream);

        jest.spyOn($location, 'url').mockImplementation(() => {});
        EntityMetadata.expect('save').returns({
            result: {
                entity_metadata: {
                    title: 'stuff',
                    description: 'things',
                    canonical_url: 'whatever',
                },
            },
            meta: {},
        });

        createButton.click();

        Stream.flush('create');
        EntityMetadata.flush('save');

        expect($location.url).toHaveBeenCalledWith(`/editor/course/${stream.id}/edit`);
    });

    it('should load other locales if requested', () => {
        SpecHelper.updateSelectize(elem, '[name="locale"]', 'es');
        flushLoad();
        expect(contentItemEditorLists.load).toHaveBeenCalledWith('Lesson.Stream', 'es');
    });

    let loadCallback;

    function flushLoad() {
        loadCallback(streams);
        scope.$digest();
    }

    function render() {
        const renderer = SpecHelper.renderer();
        const obj = {
            onLoad(cb) {
                loadCallback = cb;
                return obj;
            },
        };

        jest.spyOn(contentItemEditorLists, 'load').mockReturnValue(obj);
        renderer.render('<list-streams></list-streams>');
        scope = renderer.childScope;
        elem = renderer.elem;

        SpecHelper.expectElements(elem, 'front-royal-spinner:eq(0)');

        // pretend that 'index' call to the api is returning
        $timeout.flush();
        flushLoad();
        expect(contentItemEditorLists.load).toHaveBeenCalledWith('Lesson.Stream', 'en');
        SpecHelper.expectNoElement(renderer.elem, 'front-royal-spinner');

        return renderer;
    }
});
