import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';
import 'ContentItem/angularModule/spec/_mock/mock_s3_asset';

describe('Editor::Stream::EditStreamDir', () => {
    let SpecHelper;
    let Lesson;
    let MockS3Asset;
    let Stream;
    let $timeout;
    let stream;
    let streams;
    let scope;
    let elem;
    let otherLessons;
    let contentItemEditorLists;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject($injector => {
            $injector.get('StreamFixtures');
            $injector.get('LessonFixtures');
            $injector.get('MockIguana');
            SpecHelper = $injector.get('SpecHelper');
            Lesson = $injector.get('Lesson');
            contentItemEditorLists = $injector.get('contentItemEditorLists');
            MockS3Asset = $injector.get('MockS3Asset');
            Stream = $injector.get('Lesson.Stream');
            $timeout = $injector.get('$timeout');

            stream = Stream.fixtures.getInstance();
            SpecHelper.stubCurrentUser('editor');
            SpecHelper.stubConfig();

            SpecHelper.stubDirective('contentTopicsEditor');
            SpecHelper.stubDirective('contentItemSavePublish');
            SpecHelper.stubDirective('entityMetadataForm');
            SpecHelper.stubDirective('itemGroupsEditor');
            SpecHelper.stubDirective('contentItemEditLocale');
        });

        setupStreams();
    });

    function setupStreams(requestedStreams) {
        streams = requestedStreams || [
            Stream.fixtures.getInstance(),
            Stream.fixtures.getInstance(),
            Stream.fixtures.getInstance(),
        ];
    }

    afterEach(() => {
        SpecHelper.cleanup();
    });

    describe('basic editing', () => {
        beforeEach(() => {
            render();
        });

        it('should have have an editable title input', () => {
            SpecHelper.updateTextInput(elem, 'input[name="title"]', 'new Title');
            SpecHelper.expectEqual('new Title', stream.title, 'updated title');
        });

        it('should have have an editable textarea input', () => {
            SpecHelper.updateTextInput(elem, 'textarea[name="description"]', 'new Description');
            SpecHelper.expectEqual('new Description', stream.description, 'updated description');
        });

        it('should support uploading an image', () => {
            const imageUploader = SpecHelper.expectElement(elem, '.course-image-uploader');
            SpecHelper.expectEqual('imageUploaded($$s3Asset)', imageUploader.attr('uploaded'), 'uploaded callback');

            // pretend that an image was uploaded with content-item-image-upload
            const s3Asset = MockS3Asset.get();
            scope.imageUploaded(s3Asset);
            scope.$digest();

            SpecHelper.expectEqual(s3Asset, stream.image, 'uploaded image on stream');
        });

        it('should support setting the exam flag', () => {
            SpecHelper.checkCheckbox(elem, '[name="exam"]');
            expect(stream.exam).toBe(true);
            SpecHelper.uncheckCheckbox(elem, '[name="exam"]');
            expect(stream.exam).toBe(false);
        });

        it('should support setting the some exam-only values only if exam is true', () => {
            SpecHelper.expectElementDisabled(elem, '[name="time_limit_hours"]');
            SpecHelper.expectElementText(elem, '.exam-group .help-block', 'Only available for exam courses.');
            stream.exam = true;
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.time-limit-group .help-block');
            SpecHelper.updateTextInput(elem, '[name="time_limit_hours"]', 42);
            expect(stream.time_limit_hours).toBe(42);
        });
    });

    describe('chapter support', () => {
        beforeEach(() => {
            render();
        });

        it('should have a list of the chapters on the stream', () => {
            SpecHelper.expectElements(elem, '.edit_chapter', stream.chapters.length);
        });

        it('should support adding a chapter', () => {
            expect(stream.chapters.length).toBe(2); // sanity check
            SpecHelper.click(elem, '[data-id="add_chapter"]');
            scope.$digest();
            expect(stream.chapters.length).toBe(3);
            SpecHelper.expectElements(elem, '.edit_chapter', 3);
        });

        it('should populate a list of other-lessons', () => {
            expect(scope.otherLessons).not.toBeUndefined();
            expect(scope.otherLessons.length).toEqual(scope.lessons.length - scope.stream.lessons.length);
        });

        it('should populate a lookup of stream lessons', () => {
            expect(scope.streamLessonLookupById).not.toBeUndefined();
            for (let i = scope.stream.lessons.length - 1; i >= 0; i--) {
                const lesson = scope.stream.lessons[i];
                const lessonId = lesson.id;
                expect(scope.streamLessonLookupById[lessonId]).toEqual(lesson);
            }
        });
    });

    describe('related courses', () => {
        beforeEach(() => {
            render();
            expect(scope.stream.related_stream_ids).toEqual([]);
        });

        it('should be able to add a related course to the course info', () => {
            expect(scope.stream.related_stream_ids).toEqual([]);
            const streamId = scope.streams[scope.streams.length - 1].id;
            SpecHelper.updateSelectize(elem, '[name="addRelatedStream"]', streamId);
            expect(scope.stream.related_stream_ids).toEqual([streamId]);
        });

        it('should be able to delete related courses', () => {
            const streamId = scope.streams[scope.streams.length - 1].id;
            SpecHelper.updateSelectize(elem, '[name="addRelatedStream"]', streamId);
            SpecHelper.click(elem, '[name="removeRelatedStream"]');
            expect(scope.stream.related_stream_ids).toEqual([]);
        });
    });

    describe('recommended courses', () => {
        beforeEach(() => {
            render();
            expect(scope.stream.recommended_stream_ids).toEqual([]);
        });

        it('should be able to add a recommended course to the course info', () => {
            const streamId = scope.streams[scope.streams.length - 1].id;
            SpecHelper.updateSelectize(elem, '[name="addRecommendedStream"]', streamId);
            expect(scope.stream.recommended_stream_ids).toEqual([streamId]);
        });

        it('should be able to delete recommended courses', () => {
            const streamId = scope.streams[scope.streams.length - 1].id;
            SpecHelper.updateSelectize(elem, '[name="addRecommendedStream"]', streamId);
            SpecHelper.click(elem, '[name="removeRecommendedStream"]');
            expect(scope.stream.recommended_stream_ids).toEqual([]);
        });
    });

    describe('translatableLessonExportSet', () => {
        beforeEach(render);

        it('should allow for exporting all lessons', () => {
            // hide exportElem until contentItemEditLocaleLoaded
            SpecHelper.expectNoElement(elem, 'translatable-lesson-export-set');
            scope.contentItemEditLocaleLoaded = true;
            scope.$digest();
            const exportElem = SpecHelper.expectElement(elem, 'translatable-lesson-export-set');
            SpecHelper.expectNoElement(exportElem, 'front-royal-spinner');
            SpecHelper.expectNoElement(exportElem, '[name="download-links"]');

            // clicking to export should show set translatableLessonExportSet
            SpecHelper.mockTranslatableLessonExportSetPrepare();
            SpecHelper.click(elem, '[name="export-all-lessons"]');
            expect(scope.translatableLessonExportSet).not.toBeUndefined();
            expect(scope.translatableLessonExportSet.lessonIds).toEqual(stream.lessonIds);
            expect(scope.translatableLessonExportSet.formats).toEqual(['oneColumnTextOnly', 'oneColumnWithFrames']);
            const exportScope = exportElem.isolateScope();
            expect(exportScope).not.toBeUndefined();
            expect(exportScope.translatableLessonExportSet).toBe(scope.translatableLessonExportSet);
        });
    });

    const loadCallbacks = {};

    function flushLoad(klassName, contentItems) {
        loadCallbacks[klassName](contentItems);
        scope.$digest();
    }

    // function render() {
    //     var renderer = SpecHelper.renderer();

    //     jest.spyOn(contentItemEditorLists, 'load').mockReturnValue({
    //         onLoad: function(cb) {
    //             loadCallback = cb;
    //         }
    //     });

    function render() {
        const renderer = SpecHelper.renderer();
        renderer.scope.streamId = stream.id;

        otherLessons = [Lesson.fixtures.getInstance(), Lesson.fixtures.getInstance(), Lesson.fixtures.getInstance()];

        jest.spyOn(contentItemEditorLists, 'load').mockImplementation(klassName => ({
            onLoad(cb) {
                loadCallbacks[klassName] = cb;
            },
        }));

        Stream.resetCache();
        Stream.expect('show')
            .toBeCalledWith(stream.id, {
                filters: {
                    published: false,
                    in_users_locale_or_en: null,
                    user_can_see: null,
                },
                'except[]': ['lesson_streams_progress'],
                include_progress: false,
            })
            .returns(stream);

        const allLessons = stream.lessons.slice(0, 999).concat(otherLessons);
        // Lesson.expect('index').returns(allLessons);
        renderer.render('<edit-stream stream-id="{{streamId}}"></edit-stream>');
        scope = renderer.childScope;
        Stream.flush('show');
        scope.$digest();
        flushLoad('Lesson', allLessons);
        flushLoad('Lesson.Stream', streams);
        scope.stream = stream;
        elem = renderer.elem;
        $timeout.flush();
    }
});
