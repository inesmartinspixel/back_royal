import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Lessons/angularModule/spec/_mock/fixtures/lessons';
import 'Lessons/angularModule/spec/_mock/fixtures/streams';

describe('Editor::Stream::EditChapterDir', () => {
    let SpecHelper;
    let Stream;
    let Lesson;
    let $window;
    let stream;
    let chapter;
    let elem;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject($injector => {
            $injector.get('LessonFixtures');
            $injector.get('StreamFixtures');
            $injector.get('MockIguana');
            SpecHelper = $injector.get('SpecHelper');
            Stream = $injector.get('Lesson.Stream');
            Lesson = $injector.get('Lesson');
            $window = $injector.get('$window');
            render();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should display and edit a title', () => {
        const selector = 'input[name="chapter_name"]';
        SpecHelper.expectTextInputVal(elem, selector, chapter.title);
        SpecHelper.updateTextInput(elem, selector, 'A New Title');
        SpecHelper.expectEqual('A New Title', chapter.title, 'chapter title');
    });

    it('should have a list of the lessons in the chapter', () => {
        SpecHelper.expectElements(elem, '.lesson.for_lessons', chapter.lesson_hashes.length);
    });

    // it('should be able to add a lesson to the chapter and stream', function() {
    //     // TODO: not sure how to properly test selectize atm
    // });

    it('should be able to remove a lesson from the chapter and stream', () => {
        const origLength = chapter.lessonIds.length;
        SpecHelper.click(elem, '.for_lessons .lesson-controls-container > button.remove-btn', 0);
        scope.$digest();
        expect(chapter.lessonIds.length).toEqual(origLength - 1);
    });

    it('should be able to toggle a lesson as coming soon', () => {
        const lesson = chapter.lessons[0];
        expect(lesson.comingSoon).toEqual(false);
        SpecHelper.click(elem, '.for_lessons .lesson-controls-container > button.coming-soon-btn', 0);
        scope.$digest();
        expect(lesson.comingSoon).toEqual(true);
    });

    it('should have the appropriate re-ordering controls', () => {
        SpecHelper.expectElement(elem, '.chapter-sessions-header-container > cf-reorder-list-item-buttons');
        SpecHelper.expectElements(
            elem,
            '.lesson > span.lesson-controls-container > cf-reorder-list-item-buttons',
            chapter.lessons.length,
        );
    });

    it('should be able to toggle chapter pending', () => {
        expect(chapter.pending).toBe(false);
        SpecHelper.expectElements(elem, '.edit_chapter.available', 1);
        SpecHelper.expectNoElement(elem, '.edit_chapter.pending');
        SpecHelper.click(elem, '.pending-delete-container > button[name="toggle_pending"]');
        scope.$digest();
        expect(chapter.pending).toBe(true);
        SpecHelper.expectNoElement(elem, '.edit_chapter.available', 0);
        SpecHelper.expectElements(elem, '.edit_chapter.pending', 1);
    });

    it('should be able to remove itself from the stream', () => {
        expect(scope.stream.chapters.indexOf(chapter)).toEqual(0);

        jest.spyOn($window, 'confirm').mockReturnValue(true);
        SpecHelper.click(elem, '.pending-delete-container > button[name="remove_chapter"]');
        expect($window.confirm).toHaveBeenCalled();

        scope.$digest();
        expect(scope.stream.chapters.indexOf(chapter)).toEqual(-1);
    });

    it('should open a lesson when clicked', () => {
        jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});
        SpecHelper.click(elem, '.for_lessons .lesson-controls-container > button.link-btn', 0);
        expect(scope.loadUrl).toHaveBeenCalledWith(chapter.lessons[0].editorUrl, '_blank');
    });

    function render() {
        stream = Stream.fixtures.getInstance();
        chapter = stream.chapters[0];

        const otherLessons = [
            Lesson.fixtures.getInstance(),
            Lesson.fixtures.getInstance(),
            Lesson.fixtures.getInstance(),
        ];

        const allLessons = stream.lessons.slice(0, 999).concat(otherLessons);

        const streamLessonLookupById = {};
        stream.lessons.forEach(lesson => {
            streamLessonLookupById[lesson.id] = lesson;
        });

        const renderer = SpecHelper.renderer();
        renderer.scope.chapter = chapter;
        renderer.scope.stream = stream;
        renderer.scope.lessons = allLessons;
        renderer.scope.otherLessons = otherLessons;
        renderer.scope.streamLessonLookupById = streamLessonLookupById;

        renderer.render(
            '<edit-chapter ' +
                'chapter="chapter" ' +
                'stream="stream" ' +
                'lessons="lessons" ' +
                'other-lessons="otherLessons" ' +
                'stream-lesson-lookup-by-guid="streamLessonLookupById" ' +
                '></edit-chapter>',
        );

        scope = renderer.childScope;
        elem = renderer.elem;
    }
});
