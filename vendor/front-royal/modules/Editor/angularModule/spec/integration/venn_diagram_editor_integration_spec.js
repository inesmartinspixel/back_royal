import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Editor/angularModule/spec/_helpers/_typescomponentized_editor_integration_spec_helper';
import locales from 'Lessons/locales/lessons/lesson/frame_list/frame/componentized/component/answer_list/answer_list_venn_diagram-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(locales);

describe('Componentized VennDiagram integration', () => {
    let elem;
    let lesson;
    let SpecHelper;
    let ComponentizedEditorIntegrationSpecHelper;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                ComponentizedEditorIntegrationSpecHelper = $injector.get('ComponentizedEditorIntegrationSpecHelper');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for creation of a new venn_diagram frame', () => {
        assertCreatesVennDiagramFrame();
        renderPlayer();
        playFrame();
    });

    it('should allow for editing an existing venn_diagram frame', () => {
        const frame = assertCreatesVennDiagramFrame();
        renderEditor(frame);
        // assertEditable();
    });

    function assertCreatesVennDiagramFrame() {
        renderEditor();
        SpecHelper.click(elem, 'button[name="add_frame"]');
        SpecHelper.selectOptionByLabel(elem, 'select[name="interaction_type"]', 'Venn Diagram');

        assertEditable();

        return lesson.frames[0];
    }

    function assertEditable() {
        // we should be able to update the main text
        ComponentizedEditorIntegrationSpecHelper.setMainTextAndAssertOnScreen(elem, '[name="main_text"] textarea');

        // we should be able to update the two header text areas
        setHeaderTextAndAssertOnScreen(elem, '[name="headers"] textarea', 0);
        setHeaderTextAndAssertOnScreen(elem, '[name="headers"] textarea', 1);

        // we should see 3 answers
        SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 3);

        // and be able to update them
        const answerText = 'Updated answer text';
        SpecHelper.updateTextArea(elem, '[name="shared_answers"] [name="answer"] textarea', answerText, 0);
        const vennDiagramButtons = SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 3);
        vennDiagramButtons.eq(0).click();
        SpecHelper.expectElementText(elem, '.frame-container #venn_diagram_messages', answerText, 0);

        // setup a correct answer
        SpecHelper.selectOptionByLabel(elem, 'select[name="correct_answer"]', '2: BLANK'); // account for indexing
    }

    function playFrame() {
        SpecHelper.expectElementText(
            elem,
            '.frame-container .message',
            'Place me in one of the three positions above.',
            0,
        );

        // clicking the wrong answer, and then 'check answer', should show incorrect styling
        const vennDiagramButtons = SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 3);
        vennDiagramButtons.eq(0).click();
        SpecHelper.expectElementText(elem, '.frame-container .message', 'Updated answer text', 0);
        SpecHelper.click(elem, '.cf-continue-button button'); // click 'check answer'
        const vennDiagramMessage = SpecHelper.expectElement(elem, '.frame-container #venn_diagram_messages');
        SpecHelper.expectEqual(
            true,
            vennDiagramMessage.get(0).classList.contains('incorrect'),
            'button has incorrect class',
        );
        SpecHelper.expectEqual(
            false,
            vennDiagramMessage.get(0).classList.contains('correct'),
            'button has correct class',
        );
        $timeout.flush();

        // // back to preview mode, click the correct answer, and then 'check answer' should show correct styling and move on to next challenge
        // ComponentizedEditorIntegrationSpecHelper.togglePreviewMode(elem);
        // vennDiagramButtons = SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 3);
        // vennDiagramButtons.eq(1).click();
        // SpecHelper.click(elem, '.cf-continue-button'); // click 'check answer'
        // vennDiagramMessage = SpecHelper.expectElement(elem, '.frame-container #venn_diagram_messages');
        // SpecHelper.expectEqual(true, vennDiagramMessage.get(1).classList.contains('correct'), 'button has correct class');
        // $timeout.flush();
    }

    function renderEditor(frame) {
        const renderer = ComponentizedEditorIntegrationSpecHelper.renderEditFrameList(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
    }

    function renderPlayer() {
        const frame = lesson.frames[0];
        const json = frame.asJson();

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderShowLesson(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        // check that rendering the frame did not change it
        SpecHelper.expectEqualObjects(frame.asJson(), json);
    }

    function setHeaderTextAndAssertOnScreen(
        _elem,
        selector = 'textarea[name="text"]',
        index,
        text = `Header text ${Math.random()}`,
        screenText = text,
    ) {
        SpecHelper.updateTextArea(_elem, selector, text, index);
        // check that venn_diagram_headers exist in the parent elem
        const textEl = ComponentizedEditorIntegrationSpecHelper.expectElementInFrameContainer(
            _elem,
            `[name="venn_diagram_header_${index}"]`,
        );
        SpecHelper.expectEqual(screenText.trim(), SpecHelper.trimText(textEl), 'header text contents');
    }
});
