import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Editor/angularModule/spec/_helpers/_typescomponentized_editor_integration_spec_helper';

describe('Componentized BlanksOnImage integration', () => {
    let elem;
    let lesson;
    let SpecHelper;
    let ComponentizedEditorIntegrationSpecHelper;
    let $timeout;
    let $httpBackend;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                ComponentizedEditorIntegrationSpecHelper = $injector.get('ComponentizedEditorIntegrationSpecHelper');
                $httpBackend = $injector.get('$httpBackend');
                // backend definition common for all tests
                $httpBackend.expectGET('/sounds/button_correct.mp3').respond({});

                // since there is no width or height on the elements in the test,
                // resizeHandle blows up
                SpecHelper.stubDirective('resizeHandle');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for creating a new sequential blanks_on_image frame, playing it, reloading it, and editing it again', () => {
        createsBlanksOnImageFrame();
        addTwoSequentialChallenges();

        // play it in the player
        renderPlayer();
        playSequentialFrame();

        // reload it in the editor and make sure it's still editable
        renderEditor(lesson.frames[0]);
        addTwoSequentialChallenges();
    });

    it('should allow for creating a new consumable blanks_on_image frame, playing it, reloading it, and editing it again', () => {
        createsBlanksOnImageFrame();
        addTwoConsumableChallenges();

        // play it in the player
        renderPlayer();
        playConsumableFrame();

        // reload it in the editor and make sure it's still editable
        renderEditor(lesson.frames[0]);
        addTwoConsumableChallenges();
    });

    it('should allow for switching from consumable to sequential', () => {
        createsBlanksOnImageFrame();
        addTwoConsumableChallenges();

        // switch to sequential
        SpecHelper.toggleRadio(elem, 'input[type="radio"][value="sequential"]');

        // the correct answer from each blank should have been copied over.  We're just gonna
        // check here that there are the right number of answers.  The specifics are tested
        // in the unit test
        const answerInputs = elem.find('.cf-multiple-choice-challenge-editor .cf-answer-list-editor textarea');

        SpecHelper.expectEqual(getBlankTexts().length, answerInputs.length, 'answer count');

        addTwoSequentialChallenges();
    });

    it('should allow for switching from sequential to consumable', () => {
        createsBlanksOnImageFrame();
        addTwoSequentialChallenges();

        // get all of the answer texts from the answer inputs in the info panel
        const answerInputs = elem.find('.cf-multiple-choice-challenge-editor .cf-answer-list-editor textarea');
        const answerTextsBeforeSwitching = [];
        angular.forEach(answerInputs, input => {
            answerTextsBeforeSwitching.push($(input).val());
        });

        // switch to consumable
        SpecHelper.toggleRadio(elem, 'input[type="radio"][value="consumable"]');

        // now there is only one answer list, and it is on the screen.  Check that
        // we have a button for each answer that existed before.
        const answerButtons = elem.find('.frame-container .cf_answer_list button');
        const answerTextsAfterSwitching = [];
        angular.forEach(answerButtons, button => {
            answerTextsAfterSwitching.push($(button).find('.button_label').text());
        });

        SpecHelper.expectEqual(
            answerTextsBeforeSwitching.sort(),
            answerTextsAfterSwitching.sort().map(e => e.replace(/\s+/g, ' ').trim()),
        );

        // check that we can edit it
        addTwoConsumableChallenges();
    });

    it('should allow for image answers', () => {
        createsBlanksOnImageFrame();

        SpecHelper.toggleRadio(elem, 'input[type="radio"][value="sequential"]');

        // set challenge to an image
        SpecHelper.click(
            elem,
            '[name="challenges_list"] .challenge_row [name="proxy-editor"] [name="toggle_content_type"]',
        );

        // challenge on screen should have image
        SpecHelper.expectElement(elem, '.frame-container .image_blank');

        // answer have image
        SpecHelper.expectElement(elem, '.frame-container .cf_answer_list button.image');

        // add a confuser
        SpecHelper.click(elem, '[name="add_answer"]');

        // clicking the wrong answer should show incorrect styling
        const answerButtons = SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 2);
        answerButtons.eq(1).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(1).get(0).classList.contains('incorrect'),
            'button has incorrect class',
        );
        $timeout.flush();

        // clicking the correct answer should show correct styling and move on to next challenge
        answerButtons.eq(0).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button has correct class',
        );
        $timeout.flush();
    });

    it('should work with multiple interactive cards', () => {
        const frame = createsBlanksOnImageFrame();
        addTwoConsumableChallenges();

        // create a new interactive card
        SpecHelper.click(elem, '[name="add_interactive_card"]');
        // we have to flush so that the slick handler gets set up before adding
        // a challenge
        $timeout.flush();

        // add 2 challenges to the new interactive card
        const newChallengesPanel = SpecHelper.expectElement(elem, '[id="challenges-panel-2"]');
        SpecHelper.click(newChallengesPanel, '[name="add_challenge"]');
        SpecHelper.click(newChallengesPanel, '[name="add_challenge"]');

        // play it in the player
        renderPlayer();
        $timeout.flush(); // flush to setup slick
        playFrameWithMultipleInteractiveCards([0, 2], [3, 4]);

        // // re-order the cards
        renderEditor(frame);
        const firstChallengeEditorOnFirstCard = elem
            .find('[id="challenges-panel-1"] .challenge_editor > [component-id]')
            .eq(0);
        const firstChallengeEditorOnSecondCard = elem
            .find('[id="challenges-panel-2"] .challenge_editor > [component-id]')
            .eq(0);
        SpecHelper.click(elem, '.interactive-cards-row [name="moveDown"]', 0);
        // challenge editors should have swapped interactive card panels
        SpecHelper.expectElement(
            elem,
            `[id="challenges-panel-2"] .challenge_editor [component-id="${firstChallengeEditorOnFirstCard.attr(
                'component-id',
            )}"]`,
        );
        SpecHelper.expectElement(
            elem,
            `[id="challenges-panel-1"] .challenge_editor [component-id="${firstChallengeEditorOnSecondCard.attr(
                'component-id',
            )}"]`,
        );

        // play it inthe player, expecting the cards to be re-ordered
        renderPlayer();
        $timeout.flush(); // flush to setup slick
        playFrameWithMultipleInteractiveCards([3, 4], [0, 2]);
    });

    function getBlankTexts() {
        const texts = [];
        angular.forEach(elem.find('.cf-multiple-choice-challenge-editor'), el => {
            texts.push($(el).find('[name="challenge-overlay-blank"] textarea').val());
        });
        return texts;
    }

    function createsBlanksOnImageFrame() {
        renderEditor();
        SpecHelper.click(elem, 'button[name="add_frame"]');
        SpecHelper.selectOptionByLabel(elem, 'select[name="interaction_type"]', 'Blanks on Image');

        // before the resize handle gets added, we need to make sure the
        // overlaid components have width and height.  In real life, this
        // is defined in css

        const wrapper = elem.find('.overlay_component_wrapper');
        wrapper.width('50px');
        wrapper.height('50px');

        // flush the timeout in positionable that creates the resize handle
        $timeout.flush();

        return lesson.frames[0];
    }

    function addTwoSequentialChallenges() {
        SpecHelper.toggleRadio(elem, 'input[type="radio"][value="sequential"]');

        let challengeEditors = elem.find('.cf-multiple-choice-challenge-editor');
        const initialChallengeCount = challengeEditors.length;

        // we should see a challenge blank
        SpecHelper.expectElements(elem, '.cf-challenge-overlay-blank', initialChallengeCount);

        // If there is more than one challenge, activate the first one
        if (initialChallengeCount > 1) {
            SpecHelper.click(elem, '[name="challenges_list"] .challenge_row', 0);
        }

        // and be able to update it
        const blankText = `blank text 0 ${Math.random()}`;
        SpecHelper.updateTextArea(
            elem,
            '[name="challenges_list"] .challenge_row .cf-text-editor textarea',
            blankText,
            0,
        );
        SpecHelper.expectElementText(elem, '.frame-container .cf-challenge-overlay-blank', blankText, 0);

        // we should see at least one answer button for the existing challenge, and
        // it should have the same text as our blank
        const initialAnswerCount = elem.find('.frame-container .cf_answer_list button').length;
        SpecHelper.expectElementText(elem, '.frame-container .cf_answer_list button', blankText, 0);

        // but we should not be able to update the correct answer
        challengeEditors = elem.find('.cf-multiple-choice-challenge-editor');
        const challengeEditor = challengeEditors.eq(0);
        SpecHelper.expectElementDisabled(challengeEditor, '[name="answer"] textarea', true, 0);

        // and we should not be able to delete the correct answer
        SpecHelper.expectElementDisabled(
            challengeEditor.find('.cf-answer-list-editor [name="answer"]').eq(0),
            '[name="remove"]',
        );

        // we should be able to add a confuser and edit it
        const newAnswerText = `answer text ${Math.random()}`;
        SpecHelper.click(challengeEditor, '[name="add_answer"]');
        const answerIndex = challengeEditor.find('[name="answer"]').length - 1;
        SpecHelper.updateTextArea(challengeEditor, '[name="answer"] textarea', newAnswerText, answerIndex);
        SpecHelper.expectElementEnabled(challengeEditor.find('[name="answer"]').eq(answerIndex), '[name="remove"]');
        SpecHelper.expectElementText(
            elem,
            '.frame-container .cf_answer_list .button_label',
            newAnswerText,
            answerIndex,
        );
        SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', initialAnswerCount + 1);

        // we should be able to add a new challenge
        SpecHelper.click(elem, '[name="add_challenge"]');
        $timeout.flush();

        // there should be as many challenge editors as blanks
        SpecHelper.expectElements(elem, '.cf-multiple-choice-challenge-editor', initialChallengeCount + 1);

        // there should be one answer for the new challenge
        SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 1);

        // see https://trello.com/c/286PBj5j/163-bug-blanks-on-image-after-reloading-correct-answer-select-is-not-locked
        $(elem)
            .find('select[name="correct_answer"]')
            .each((index, select) => {
                expect($(select).attr('disabled')).toEqual('disabled');
            });
    }

    function addTwoConsumableChallenges() {
        SpecHelper.toggleRadio(elem, 'input[type="radio"][value="consumable"]');

        SpecHelper.expectElement(elem, '[name="main_text"] textarea').val();
        const challengeEditors = elem.find('.cf-multiple-choice-challenge-editor');
        const initialChallengeCount = challengeEditors.length;

        // we should see a challenge blank
        SpecHelper.expectElements(elem, '.cf-challenge-overlay-blank', initialChallengeCount);

        // If there is more than one challenge, activate the first one
        if (initialChallengeCount > 1) {
            SpecHelper.click(elem, '[name="challenges_list"] .challenge_row', 0);
        }
        const initialAnswerCount = elem.find('.frame-container .cf_answer_list button').length;
        SpecHelper.expectEqual(true, initialAnswerCount > 0, 'at least one answer');
        SpecHelper.expectEqual(true, initialAnswerCount >= initialChallengeCount, 'at least as many answers as blanks');

        // and be able to update it
        const blankText = `blank text 0 ${Math.random()}`;
        SpecHelper.updateTextArea(
            elem,
            '[name="challenges_list"] .challenge_row .cf-text-editor textarea',
            blankText,
            0,
        );
        SpecHelper.expectElementText(elem, '.frame-container .cf-challenge-overlay-blank', blankText, 0);

        // updating the first blank should change the text on the first answer
        SpecHelper.expectElementText(elem, '.frame-container .cf_answer_list button', blankText, 0);

        // we should not be able to update the first answer (No we shouldn't)
        let answerIndex = 0;
        const answerListEditor = SpecHelper.expectElement(elem, '[name="shared_answers"]');
        SpecHelper.expectElementDisabled(answerListEditor, '[name="answer"] textarea');

        // but we should not be able to delete the correct answer
        SpecHelper.expectElementDisabled(answerListEditor.find('[name="answer"]').eq(answerIndex), '[name="remove"]');

        // we should be able to add a confuser and edit it
        const answer2Text = `answer text ${Math.random()}`;
        answerIndex = initialAnswerCount;
        SpecHelper.click(answerListEditor, '[name="add_answer"]');
        SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', initialAnswerCount + 1);
        SpecHelper.updateTextArea(answerListEditor, '[name="answer"] textarea', answer2Text, answerIndex);
        SpecHelper.expectElementText(
            elem,
            '.frame-container .cf_answer_list button .button_label',
            answer2Text,
            answerIndex,
        );
        SpecHelper.expectElementEnabled(answerListEditor.find('[name="answer"]').eq(answerIndex), '[name="remove"]');

        // we should be able to add a new challenge by updating the text
        SpecHelper.click(elem, '[name="add_challenge"]');

        // there should be as many challenge editors as blanks
        SpecHelper.expectElements(elem, '.cf-multiple-choice-challenge-editor', initialChallengeCount + 1);

        // another answer should have been added
        SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', initialAnswerCount + 2);
    }

    function playSequentialFrame() {
        // 'Select an answer for each blank' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, 'Select an answer for each blank');

        // first blank should be selected
        const blanks = SpecHelper.expectElements(elem, '.cf-challenge-overlay-blank', 2);
        SpecHelper.expectEqual(true, blanks.eq(0).get(0).classList.contains('selected'), 'blank 0 selected');
        SpecHelper.expectEqual(false, blanks.eq(1).get(0).classList.contains('selected'), 'blank 1 selected');

        // clicking the wrong answer should show incorrect styling
        let answerButtons = SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 2);
        answerButtons.eq(1).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(1).get(0).classList.contains('incorrect'),
            'button has incorrect class',
        );
        $timeout.flush();

        // first blank should still be selected
        SpecHelper.expectEqual(true, blanks.eq(0).get(0).classList.contains('selected'), 'blank 0 selected');
        SpecHelper.expectEqual(false, blanks.eq(1).get(0).classList.contains('selected'), 'blank 1 selected');

        // clicking the correct answer should show correct styling and move on to next challenge
        answerButtons.eq(0).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button has correct class',
        );
        $timeout.flush();

        // now second blank should be selected
        SpecHelper.expectEqual(false, blanks.eq(0).get(0).classList.contains('selected'), 'blank 0 selected');
        SpecHelper.expectEqual(true, blanks.eq(0).get(0).classList.contains('completed'), 'blank 0 selected');
        SpecHelper.expectEqual(true, blanks.eq(1).get(0).classList.contains('selected'), 'blank 1 selected');

        // clicking the correct answer should show correct styling and finish the frame
        answerButtons = SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 1);
        SpecHelper.expectEqual(
            false,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button has correct class',
        );
        answerButtons.eq(0).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button has correct class',
        );

        $timeout.flush();

        // 'continue' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertContinueButtonShown(elem);
    }

    function playConsumableFrame() {
        // 'Select an answer for each blank' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, 'Select an answer for each blank');

        playThroughTwoConsumableChallenges();

        // frame should be finished and continue button should be enabled
        $timeout.flush();

        // 'continue' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertContinueButtonShown(elem);
    }

    function playFrameWithMultipleInteractiveCards(correctAnswersForCard1, correctAnswersForCard2) {
        // 'Select an answer for each blank' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, 'Select an answer for each blank');

        SpecHelper.expectElement(elem, '.cf-interactive-cards .card.active');
        playThroughTwoConsumableChallenges(correctAnswersForCard1);

        $timeout.flush();
        elem.find('.cf-interactive-cards .carousel').trigger('transitionend');

        // once challenges are complete, first card should no longer be active
        SpecHelper.expectElementDoesNotHaveClass(elem, '.card.active');

        // still no continue button, since frame is not completed
        SpecHelper.expectNoElement(elem, 'button.cf-continue-button:enabled');

        // and now the second card should be active
        SpecHelper.expectElement(elem, '.cf-interactive-cards .card.active');
        playThroughTwoConsumableChallenges(correctAnswersForCard2);

        $timeout.flush();

        // 'continue' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertContinueButtonShown(elem);
    }

    function playThroughTwoConsumableChallenges(correctAnswerIndexes = [0, 2]) {
        // first blank should be selected
        const blanks = SpecHelper.expectElements(elem, '.card.active .cf-challenge-overlay-blank', 2);
        SpecHelper.expectElementHasClass(blanks.eq(0), 'selected', 'blank 0 selected');
        SpecHelper.expectElementDoesNotHaveClass(blanks.eq(1), 'selected', 'blank 1 selected');

        // clicking the wrong answer should show incorrect styling
        const answerButtons = elem.find('.frame-container .cf_answer_list button');
        answerButtons.eq(1).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(1).get(0).classList.contains('incorrect'),
            'button has incorrect class',
        );
        $timeout.flush();

        // first blank should still be selected
        SpecHelper.expectElementHasClass(blanks.eq(0), 'selected', 'blank 0 selected');
        SpecHelper.expectElementDoesNotHaveClass(blanks.eq(1), 'selected', 'blank 1 selected');

        // // clicking the correct answer should show correct styling and move on to next challenge
        answerButtons.eq(correctAnswerIndexes[0]).click();
        SpecHelper.expectElementHasClass(
            answerButtons.eq(correctAnswerIndexes[0]),
            'correct',
            'first correct answer button has correct class after clicking',
        );
        $timeout.flush();

        // selected answer should still have correct class, and be disabled
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(correctAnswerIndexes[0]).get(0).classList.contains('correct'),
            'button 0 has correct class after flushing',
        );
        SpecHelper.expectEqual(
            'disabled',
            answerButtons.eq(correctAnswerIndexes[0]).attr('disabled'),
            'selected correct answer disabled',
        );

        // now second blank should be selected
        SpecHelper.expectEqual(false, blanks.eq(0).get(0).classList.contains('selected'), 'blank 0 selected');
        SpecHelper.expectEqual(true, blanks.eq(0).get(0).classList.contains('completed'), 'blank 0 selected');
        SpecHelper.expectEqual(true, blanks.eq(1).get(0).classList.contains('selected'), 'blank 1 selected');

        // clicking the correct answer should show correct styling
        answerButtons.eq(correctAnswerIndexes[1]).click();
        SpecHelper.expectElementHasClass(
            answerButtons.eq(correctAnswerIndexes[1]),
            'correct',
            'second correct answer button has correct class after clicking',
        );
    }

    function renderEditor(frame) {
        let json;
        if (frame) {
            json = frame.asJson();
        }

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderEditFrameList(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        frame = lesson.frames[0];
        // check that re-rendering the frame did not change it
        if (json) {
            SpecHelper.expectEqualObjects(frame.asJson(), json);
        }
    }

    function renderPlayer() {
        const frame = lesson.frames[0];
        const json = frame.asJson();

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderShowLesson(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        // check that rendering the frame did not change it
        SpecHelper.expectEqualObjects(frame.asJson(), json);
    }
});
