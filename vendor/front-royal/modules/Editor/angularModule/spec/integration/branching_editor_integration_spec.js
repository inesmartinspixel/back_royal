import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Editor/angularModule/spec/_helpers/_typescomponentized_editor_integration_spec_helper';

describe('Componentized Branching integration', () => {
    let elem;
    let lesson;
    let playerViewModel;
    let LessonProgress;
    let SpecHelper;
    let ComponentizedEditorIntegrationSpecHelper;
    let $timeout;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                ComponentizedEditorIntegrationSpecHelper = $injector.get('ComponentizedEditorIntegrationSpecHelper');
                LessonProgress = $injector.get('LessonProgress');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for branching', () => {
        assertPlaysBranchingToLastFrame();
        const frames = lesson.frames;

        // frame 4 should finish the lesson
        // (which leaves us where the activeFrame has been unset
        // but the store has not yet been flushed, so there is no
        // active frame but we're not yet on the finish screen)
        assertOnFrame(frames[3]);
        playNoInteractionFrame();
        expect(playerViewModel.activeFrame).toBeUndefined();

        // make sure re-loading the frame does not mess anything up
        renderEditor(lesson);
    });

    it('should keep track of actual path followed and use that for back button', () => {
        const frames = assertPlaysBranchingToLastFrame({
            logProgress: true,
        }).frames;

        // mock out server-side persisted history
        playerViewModel.lesson.ensureLessonProgress().frame_history = [frames[0].id, frames[2].id, frames[1].id];

        // back button should got back to 1 and then 2 and then 0
        // (event though frame 1 also navigates to frame 4)
        assertOnFrame(frames[3]);
        assertGotoPrev(frames[1]);
        assertGotoPrev(frames[2]);
        assertGotoPrev(frames[0]);
    });

    function assertPlaysBranchingToLastFrame(opts) {
        renderEditor();
        const frames = [];
        frames.push(createsMultipleChoiceFrame());
        frames.push(createsNoInteractionFrame());
        frames.push(createsNoInteractionFrame());
        frames.push(createsNoInteractionFrame());

        setupBranching();
        LessonProgress.expect('save');
        renderPlayer(opts);

        // clicking the first answer on the branching frame should go to
        // frame frame 3
        playBranchingFrame();
        assertOnFrame(frames[2]);

        // frame 3 should forward to frame 2
        playNoInteractionFrame();
        assertOnFrame(frames[1]);

        // frame 2 should forward to frame 4
        playNoInteractionFrame();
        assertOnFrame(frames[3]);

        return lesson;
    }

    function assertGotoPrev(frame) {
        playerViewModel.gotoPrev();
        // two timeouts to move on to the next frame
        $timeout.flush();
        $timeout.flush();
        assertOnFrame(frame);
    }

    function assertOnFrame(frame) {
        SpecHelper.expectElement(elem, `[frame-guid="${frame.id}"]`);
    }

    function setupBranching() {
        gotoFrameInEditor(0);
        SpecHelper.toggleCheckbox(elem, 'input[type="checkbox"][name="hasBranching"]');

        // Give the answers text so that they don't both match the same
        SpecHelper.updateTextArea(elem, '[name="answer"] textarea', 'Answer 1', 0);
        SpecHelper.updateTextArea(elem, '[name="answer"] textarea', 'Answer 2', 1);

        // link to first answer to frame 3 and the second to frame 4
        SpecHelper.selectOptionByLabel(elem, '.panel[name="hasBranching"] cf-next-frame-selector:eq(0) select', '3: ');
        SpecHelper.selectOptionByLabel(elem, '.panel[name="hasBranching"] cf-next-frame-selector:eq(1) select', '4: ');

        // goto frame 3 and set the next frame to frame 2
        gotoFrameInEditor(2);
        SpecHelper.selectOptionByLabel(elem, '.panel[name="options"] cf-next-frame-selector select', '2: ');

        // goto frame 2 and set the next frame to frame 4
        gotoFrameInEditor(1);
        SpecHelper.selectOptionByLabel(elem, '.panel[name="options"] cf-next-frame-selector select', '4: ');
    }

    function createsMultipleChoiceFrame() {
        SpecHelper.click(elem, 'button[name="add_frame"]');
        SpecHelper.selectOptionByLabel(elem, 'select[name="interaction_type"]', 'Multiple Choice');
        return _.last(lesson.frames);
    }

    function createsNoInteractionFrame() {
        SpecHelper.click(elem, 'button[name="add_frame"]');
        SpecHelper.selectOptionByLabel(elem, 'select[name="interaction_type"]', 'No Interaction');
        return _.last(lesson.frames);
    }

    function gotoFrameInEditor(index) {
        SpecHelper.click(elem, '.frame_thumbnail', index);
    }

    function playBranchingFrame() {
        // clicking an answer should not show correct styling, but should show continue button
        const answerButtons = SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 2);
        answerButtons.eq(0).click();
        $timeout.flush();

        // 'continue' should be displayed
        SpecHelper.click(elem, '.cf-continue-button button');
        // two timeouts to move on to the next frame
        $timeout.flush();
        $timeout.flush();
    }

    function playNoInteractionFrame() {
        SpecHelper.click(elem, '.cf-continue-button button');
        // two timeouts to move on to the next frame
        $timeout.flush();
        $timeout.flush();
    }

    function renderEditor(existingLesson) {
        if (elem) {
            elem.remove();
        }
        let json;
        let firstFrame;
        if (existingLesson) {
            firstFrame = existingLesson.frames[0];
            json = firstFrame.asJson();
        }

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderEditFrameList(existingLesson);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        firstFrame = lesson.frames[0];

        // check that re-rendering the frame did not change it
        if (json) {
            SpecHelper.expectEqualObjects(firstFrame.asJson(), json);
        }

        return lesson;
    }

    function renderPlayer(opts) {
        if (elem) {
            elem.remove();
        }
        const renderer = ComponentizedEditorIntegrationSpecHelper.renderShowLesson(lesson, opts);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        playerViewModel = elem.find('[player-view-model]').scope().playerViewModel;
    }
});
