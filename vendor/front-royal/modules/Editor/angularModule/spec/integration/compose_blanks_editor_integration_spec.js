import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Editor/angularModule/spec/_helpers/_typescomponentized_editor_integration_spec_helper';
import stubSpecLocale from 'Translation/stubSpecLocale';

stubSpecLocale('lessons.lesson.frame_list.frame.componentized.component.layout.text_image_interactive.give_me_a_hint');

describe('Componentized ComposeBlanks integration', () => {
    let elem;
    let lesson;
    let SpecHelper;
    let ComponentizedEditorIntegrationSpecHelper;
    let $timeout;
    let SoundManager;
    let SoundConfig;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                ComponentizedEditorIntegrationSpecHelper = $injector.get('ComponentizedEditorIntegrationSpecHelper');
                SoundManager = $injector.get('SoundManager');
                SoundConfig = $injector.get('SoundConfig');
            },
        ]);

        jest.spyOn(SoundManager, 'playUrl').mockImplementation(() => {});
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for creating a new compose_blanks frame, playing it, reloading it, and editing it again', () => {
        createsComposeBlanksFrame();
        addTwoChallenges();

        // play it in the player
        renderPlayer();
        playFrame();

        // reload it in the editor and make sure it's still editable
        renderEditor(lesson.frames[0]);
        addTwoChallenges();
    });

    function createsComposeBlanksFrame() {
        renderEditor();
        SpecHelper.click(elem, 'button[name="add_frame"]');
        SpecHelper.selectOptionByLabel(elem, 'select[name="interaction_type"]', 'Compose Blanks');
        return lesson.frames[0];
    }

    function addTwoChallenges() {
        let mainText = SpecHelper.expectElement(elem, '[name="main_text"] textarea').val();

        const matches = mainText.match(/\[/g);
        const initialChallengeBlankCount = matches ? matches.length : 0;

        // there should be as many challenge editors as blanks
        SpecHelper.expectElements(elem, '.cf-user-input-challenge-editor', initialChallengeBlankCount);

        // we should be able to add a challenge by adding a blank
        mainText = `${mainText}and [a blank] and`;
        ComponentizedEditorIntegrationSpecHelper.setMainTextAndAssertOnScreen(
            elem,
            '[name="main_text"] textarea',
            mainText,
            mainText.replace(/\[([^\]]+)\]/g, '$1'),
        );

        // there should be as many challenge editors as blanks
        SpecHelper.expectElements(elem, '.cf-user-input-challenge-editor', initialChallengeBlankCount + 1);

        // we should be able to add a second challenge by updating the text
        mainText = `${mainText}and [another] and`;
        ComponentizedEditorIntegrationSpecHelper.setMainTextAndAssertOnScreen(
            elem,
            '[name="main_text"] textarea',
            mainText,
            mainText.replace(/\[([^\]]+)\]/g, '$1'),
        );

        // there should be as many challenge editors as blanks
        SpecHelper.expectElements(elem, '.cf-user-input-challenge-editor', initialChallengeBlankCount + 2);
    }

    function playFrame() {
        // 'Type an answer in each blank' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, 'Type an answer in each blank');

        // first blank should be selected
        const blanks = SpecHelper.expectElements(elem, '.section.text .blank', 2);
        SpecHelper.expectEqual(true, blanks.eq(0).get(0).classList.contains('selected'), 'blank 0 selected');
        SpecHelper.expectEqual(false, blanks.eq(1).get(0).classList.contains('selected'), 'blank 1 selected a');

        // entering the wrong text should show incorrect styling
        SpecHelper.updateNoApplyTextInput(blanks.eq(0), 'input', 'x');
        SpecHelper.expectEqual(
            true,
            blanks.eq(0).get(0).classList.contains('incorrect'),
            'blank has incorrect class after typing incorrect answer',
        );
        // $timeout.flush();

        // first blank should still be selected
        SpecHelper.expectEqual(true, blanks.eq(0).get(0).classList.contains('selected'), 'blank 0 selected');
        SpecHelper.expectEqual(false, blanks.eq(1).get(0).classList.contains('selected'), 'blank 1 selected b ');

        // entering part of the correct answer should show incomplete styling
        SpecHelper.updateNoApplyTextInput(blanks.eq(0), 'input', 'a bl');
        SpecHelper.expectEqual(
            true,
            blanks.eq(0).get(0).classList.contains('incomplete'),
            'blank has incomplete class after typing incomplete answer',
        );
        SpecHelper.expectEqual(
            false,
            blanks.eq(0).get(0).classList.contains('incorrect'),
            'blank has incorrect class after typing incomplete answer',
        );

        // entering the correct answer should show complete styling
        SpecHelper.updateNoApplyTextInput(blanks.eq(0), 'input', 'a blank');
        expect(SoundManager.playUrl).toHaveBeenCalledWith(SoundConfig.VALIDATE_CORRECT);
        SpecHelper.expectEqual(
            false,
            blanks.eq(0).get(0).classList.contains('incomplete'),
            'blank has incomplete class after typing complete answer',
        );
        SpecHelper.expectEqual(
            false,
            blanks.eq(0).get(0).classList.contains('incorrect'),
            'blank has incorrect class after typing complete answer',
        );

        // now second blank should be selected
        $timeout.flush();
        SpecHelper.expectEqual(false, blanks.eq(0).get(0).classList.contains('selected'), 'blank 0 selected');
        SpecHelper.expectEqual(true, blanks.eq(0).get(0).classList.contains('completed'), 'blank 0 selected');
        SpecHelper.expectEqual(true, blanks.eq(1).get(0).classList.contains('selected'), 'blank 1 selected c ');

        // no continue button until challenge is completed
        SpecHelper.expectNoElement(elem, 'button.cf-continue-button:enabled');

        // finishing all challenges should finish the frame
        SoundManager.playUrl.mockClear();
        SpecHelper.updateNoApplyTextInput(blanks.eq(1), 'input', 'another');
        expect(SoundManager.playUrl).toHaveBeenCalledWith(SoundConfig.VALIDATE_CORRECT);

        $timeout.flush();

        // 'continue' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertContinueButtonShown(elem);
    }

    function renderEditor(frame) {
        let json;
        if (frame) {
            json = frame.asJson();
        }

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderEditFrameList(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        frame = lesson.frames[0];
        // check that re-rendering the frame did not change it
        if (json) {
            SpecHelper.expectEqualObjects(frame.asJson(), json);
        }
    }

    function renderPlayer() {
        const renderer = ComponentizedEditorIntegrationSpecHelper.renderShowLesson(lesson.frames[0]);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
    }
});
