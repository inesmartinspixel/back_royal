import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Editor/angularModule/spec/_helpers/_typescomponentized_editor_integration_spec_helper';

describe('Componentized MultipleCardMultipleChoice integration', () => {
    let elem;
    let lesson;
    let SpecHelper;
    let ComponentizedEditorIntegrationSpecHelper;
    let $timeout;
    let $httpBackend;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                ComponentizedEditorIntegrationSpecHelper = $injector.get('ComponentizedEditorIntegrationSpecHelper');
                $httpBackend = $injector.get('$httpBackend');
                // backend definition common for all tests
                $httpBackend.expectGET('/sounds/button_correct.mp3').respond({});

                // since there is no width or height on the elements in the test,
                // resizeHandle blows up
                SpecHelper.stubDirective('resizeHandle');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    /*
        There could be other tests here, but since all the logic
        is essentially shared with blanks_on_image, we'll assume
        that if those tests pass than this stuff is working
    */
    it('should work with multiple interactive cards', () => {
        const frame = createsMultipleChoiceWithMultipleCardsFrame();
        addConsumableChallenge();

        // create a new interactive card
        SpecHelper.click(elem, '[name="add_interactive_card"]');
        // we have to flush so that the slick handler gets set up before adding
        // a challenge
        $timeout.flush();

        // add a challenge to the new interactive card
        const newChallengesPanel = SpecHelper.expectElement(elem, '[id="challenges-panel-2"]');
        SpecHelper.click(newChallengesPanel, '[name="add_challenge"]');

        // add challenge button should disappear now that there is one challenge
        SpecHelper.expectNoElement(newChallengesPanel, '[name="add_challenge"]');

        // play it in the player
        renderPlayer();
        $timeout.flush(); // flush to setup slick
        playFrameWithMultipleInteractiveCards(0, 2);

        // // re-order the cards
        renderEditor(frame);
        const firstChallengeEditorOnFirstCard = elem
            .find('[id="challenges-panel-1"] .challenge_editor > [component-id]')
            .eq(0);
        const firstChallengeEditorOnSecondCard = elem
            .find('[id="challenges-panel-2"] .challenge_editor > [component-id]')
            .eq(0);
        SpecHelper.click(elem, '.interactive-cards-row [name="moveDown"]', 0);
        // challenge editors should have swapped interactive card panels
        SpecHelper.expectElement(
            elem,
            `[id="challenges-panel-2"] .challenge_editor [component-id="${firstChallengeEditorOnFirstCard.attr(
                'component-id',
            )}"]`,
        );
        SpecHelper.expectElement(
            elem,
            `[id="challenges-panel-1"] .challenge_editor [component-id="${firstChallengeEditorOnSecondCard.attr(
                'component-id',
            )}"]`,
        );

        // play it inthe player, expecting the cards to be re-ordered
        renderPlayer();
        $timeout.flush(); // flush to setup slick
        playFrameWithMultipleInteractiveCards(2, 0);
    });

    function createsMultipleChoiceWithMultipleCardsFrame() {
        renderEditor();
        SpecHelper.click(elem, 'button[name="add_frame"]');
        SpecHelper.selectOptionByLabel(elem, 'select[name="interaction_type"]', 'Multiple Choice w Multiple Cards');

        // before the resize handle gets added, we need to make sure the
        // overlaid components have width and height.  In real life, this
        // is defined in css

        const wrapper = elem.find('.overlay_component_wrapper');
        wrapper.width('50px');
        wrapper.height('50px');

        // flush the timeout in positionable that creates the resize handle
        $timeout.flush();

        return lesson.frames[0];
    }

    function addConsumableChallenge() {
        SpecHelper.toggleRadio(elem, 'input[type="radio"][value="consumable"]');

        SpecHelper.expectElement(elem, '[name="main_text"] textarea').val();
        const challengeEditors = elem.find('.cf-multiple-choice-challenge-editor');
        const initialChallengeCount = challengeEditors.length;

        // we should not see aany challenge blank
        SpecHelper.expectHasClass(elem, '.cf-challenge-overlay-blank:eq(0)', 'hide');

        // If there is more than one challenge, activate the first one
        if (initialChallengeCount > 1) {
            SpecHelper.click(elem, '[name="challenges_list"] .challenge_row', 0);
        }
        const initialAnswerCount = elem.find('.frame-container .cf_answer_list button').length;
        SpecHelper.expectEqual(true, initialAnswerCount > 0, 'at least one answer');
        SpecHelper.expectEqual(true, initialAnswerCount >= initialChallengeCount, 'at least as many answers as blanks');

        // we should not be able to update the invisible blank
        SpecHelper.expectNoElement(elem, '[name="challenges_list"] .challenge_row .cf-text-editor');

        // we should be able to update the correct answer
        let answerIndex = 0;
        const answerListEditor = SpecHelper.expectElement(elem, '[name="shared_answers"]');
        SpecHelper.expectElementEnabled(answerListEditor, '[name="answer"] textarea');

        // and we should not be able to delete the correct answer
        SpecHelper.expectElementDisabled(answerListEditor.find('[name="answer"]').eq(answerIndex), '[name="remove"]');

        // we should be able to add a confuser and edit it
        const answer2Text = `answer text ${Math.random()}`;
        answerIndex = initialAnswerCount;
        SpecHelper.click(answerListEditor, '[name="add_answer"]');
        SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', initialAnswerCount + 1);
        SpecHelper.updateTextArea(answerListEditor, '[name="answer"] textarea', answer2Text, answerIndex);
        SpecHelper.expectElementText(
            elem,
            '.frame-container .cf_answer_list button .button_label',
            answer2Text,
            answerIndex,
        );
        SpecHelper.expectElementEnabled(answerListEditor.find('[name="answer"]').eq(answerIndex), '[name="remove"]');

        // we should not be able to add a new challenge since there is
        // one on this card already
        SpecHelper.expectNoElement(elem, '[name="add_challenge"]');
    }

    function playFrameWithMultipleInteractiveCards(correctAnswerForCard1, correctAnswerForCard2) {
        // 'Select an answer for each blank' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, 'Select an answer');

        SpecHelper.expectElement(elem, '.cf-interactive-cards .card.active');
        playThroughConsumableChallenge(correctAnswerForCard1);

        $timeout.flush();
        elem.find('.cf-interactive-cards .carousel').trigger('transitionend');

        // once challenges are complete, first card should no longer be active
        SpecHelper.expectElementDoesNotHaveClass(elem, '.card.active');

        // still no continue button, since frame is not completed
        SpecHelper.expectNoElement(elem, 'button.cf-continue-button:enabled');

        // and now the second card should be active
        SpecHelper.expectElement(elem, '.cf-interactive-cards .card.active');
        playThroughConsumableChallenge(correctAnswerForCard2);

        $timeout.flush();

        // 'continue' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertContinueButtonShown(elem);
    }

    function playThroughConsumableChallenge(correctAnswerIndex) {
        // clicking the wrong answer should show incorrect styling
        const answerButtons = elem.find('.frame-container .cf_answer_list button');
        answerButtons.eq(1).click();
        SpecHelper.expectElementHasClass(answerButtons.eq(1), 'incorrect', 'button has incorrect class');
        $timeout.flush();

        // clicking the correct answer should show correct styling and move on to next challenge
        answerButtons.eq(correctAnswerIndex).click();
        SpecHelper.expectElementHasClass(
            answerButtons.eq(correctAnswerIndex),
            'correct',
            'first correct answer button has correct class after clicking',
        );
        $timeout.flush();

        // selected answer should still have correct class, and be disabled
        SpecHelper.expectElementHasClass(
            answerButtons.eq(correctAnswerIndex),
            'correct',
            'button 0 has correct class after flushing',
        );
        SpecHelper.expectEqual(
            'disabled',
            answerButtons.eq(correctAnswerIndex).attr('disabled'),
            'selected correct answer disabled',
        );
    }

    function renderEditor(frame) {
        let json;
        if (frame) {
            json = frame.asJson();
        }

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderEditFrameList(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        frame = lesson.frames[0];
        // check that re-rendering the frame did not change it
        if (json) {
            SpecHelper.expectEqualObjects(frame.asJson(), json);
        }
    }

    function renderPlayer() {
        const frame = lesson.frames[0];
        const json = frame.asJson();

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderShowLesson(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        // check that rendering the frame did not change it
        SpecHelper.expectEqualObjects(frame.asJson(), json);
    }
});
