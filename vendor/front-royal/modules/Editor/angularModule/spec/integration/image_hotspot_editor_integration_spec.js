import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Editor/angularModule/spec/_helpers/_typescomponentized_editor_integration_spec_helper';

describe('Componentized ImageHotspot integration', () => {
    let elem;
    let lesson;
    let SpecHelper;
    let ComponentizedEditorIntegrationSpecHelper;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                ComponentizedEditorIntegrationSpecHelper = $injector.get('ComponentizedEditorIntegrationSpecHelper');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for creating a new standard image_hotspot frame, playing it, reloading it, and editing it again', () => {
        createsImageHotspotFrame();
        editStandardMode();

        // play it in the player
        renderPlayer();
        playStandardFrame();

        // reload it in the editor and make sure it's still editable
        renderEditor(lesson.frames[0]);
        editStandardMode();
    });

    it('should allow for creating a new check-many image_hotspot frame, playing it, reloading it, and editing it again', () => {
        createsImageHotspotFrame();

        // switch to check many and setup challenge
        toggleCheckMany();
        editCheckManyMode();

        // play it in the player
        renderPlayer();
        playCheckManyFrame();

        // // reload it in the editor and make sure it's still editable
        // renderEditor(lesson.frames[0]);
        // editCheckManyMode();
    });

    it('should allow for switching from standard to check-many challenges', () => {
        createsImageHotspotFrame();
        editStandardMode();

        // switch to check many
        toggleCheckMany();

        // edit check-many mode
        editCheckManyMode();

        // play it in the player
        renderPlayer();
        playCheckManyFrame();
    });

    it('should allow for switching from standard to check-many challenges', () => {
        createsImageHotspotFrame();

        // switch to check many
        toggleCheckMany();

        editCheckManyMode();

        // switch back to standard
        toggleCheckMany();

        // edit standard mode
        editStandardMode();

        // play it in the player
        renderPlayer();
        playStandardFrame();
    });

    function toggleCheckMany() {
        SpecHelper.toggleCheckbox(elem, 'input[type="checkbox"][name="checkMany"]', 0);
    }

    function createsImageHotspotFrame() {
        renderEditor();
        SpecHelper.click(elem, 'button[name="add_frame"]');
        SpecHelper.selectOptionByLabel(elem, 'select[name="interaction_type"]', 'Image Hotspot');
        return lesson.frames[0];
    }

    function editStandardMode() {
        SpecHelper.expectElement(elem, '[name="main_text"] textarea').val();

        // there should initially be one challenge
        SpecHelper.expectElements(elem, '.cf-multiple-choice-challenge-editor', 1);

        // initially, there might be one or two answers (depending if we already added a second)
        const answerCount = elem.find('[name="shared_answers"] [name="answer"] textarea').length;
        SpecHelper.expectEqual(true, answerCount > 0, 'at least one answer');
        // add one more if there is only one
        if (answerCount === 1) {
            SpecHelper.click(elem, '[name="add_answer"]');
        }
        SpecHelper.expectElements(elem, '[name="shared_answers"] [name="answer"] textarea', 2);

        // updating answer values should be reflected in buttons for current challenge
        const correctAnswerText = 'Correct Answer';
        SpecHelper.updateTextArea(elem, '[name="shared_answers"] [name="answer"] textarea', correctAnswerText, 0);
        SpecHelper.expectElementText(elem, '.frame-container .cf_answer_list button', correctAnswerText, 0);

        const randomText = `answer text ${Math.random()}`;
        SpecHelper.updateTextArea(elem, '[name="answer"] textarea', randomText, 1);
        SpecHelper.expectElementText(elem, '.frame-container .cf_answer_list button', randomText, 1);

        // there should be a button to add answers
        SpecHelper.expectElements(elem, '[name="add_answer"]', 1);

        // there should not be a button to add challenges
        SpecHelper.expectElements(elem, '[name="add_challenge"]', 0);

        // the challenges component should render as a single select
        SpecHelper.expectElements(elem, '[name="correct_answer"]', 1);
        SpecHelper.expectElements(elem, '[name="correct_answers"][multiple]', 0);

        // set the correct answer to the first one
        SpecHelper.selectOptionByLabel(elem, '[name="correct_answer"]', `1: ${correctAnswerText}`);
    }

    function editCheckManyMode() {
        // make sure check-many is selected
        SpecHelper.expectCheckboxChecked(elem, 'input[type="checkbox"][name="checkMany"]:eq(0)');

        SpecHelper.expectElement(elem, '[name="main_text"] textarea').val();

        // there should initially be one challenge
        SpecHelper.expectElements(elem, '.cf-multiple-choice-challenge-editor', 1);

        // initially, there might be one or two answers (depending if we already added a second)
        const answerCount = elem.find('[name="shared_answers"] [name="answer"] textarea').length;
        SpecHelper.expectEqual(true, answerCount > 0, 'at least one answer');
        // add one more if there is only one
        if (answerCount === 1) {
            SpecHelper.click(elem, '[name="add_answer"]');
        }
        SpecHelper.expectElements(elem, '[name="shared_answers"] [name="answer"] textarea', 2);

        // updating answer values should be reflected in buttons for current challenge
        SpecHelper.updateTextArea(elem, '[name="shared_answers"] [name="answer"] textarea', 'Answer 1', 0);
        SpecHelper.expectElementText(elem, '.frame-container .cf_answer_list button', 'Answer 1', 0);

        const randomText = `answer text ${Math.random()}`;
        SpecHelper.updateTextArea(elem, '[name="answer"] textarea', randomText, 1);
        SpecHelper.expectElementText(elem, '.frame-container .cf_answer_list button', randomText, 1);

        // there should be a button to add answers
        SpecHelper.expectElements(elem, '[name="add_answer"]', 1);

        // there should not be a button to add challenges
        SpecHelper.expectElements(elem, '[name="add_challenge"]', 0);

        // the challenges component should render as a multiple select
        SpecHelper.expectElements(elem, '[name="correct_answer"]', 0);
        SpecHelper.expectElements(elem, '[name="correct_answers"]', 1);

        // set both answers to be required
        const checkboxes = SpecHelper.expectElements(elem, '[name="correct_answers"] input', 2);
        checkboxes.each(function () {
            SpecHelper.setCheckboxValue($(this), true);
        });
    }

    function playStandardFrame() {
        // 'Select an answer' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, 'Select an answer');

        // clicking the wrong answer should show incorrect styling
        const answerButtons = SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 2);
        answerButtons.eq(1).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(1).get(0).classList.contains('incorrect'),
            'button has incorrect class',
        );
        $timeout.flush();

        // clicking the correct answer should show correct styling and move on to next challenge
        answerButtons.eq(0).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button has correct class',
        );
        $timeout.flush();

        // 'continue' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertContinueButtonShown(elem);
    }

    function playCheckManyFrame() {
        // 'Select an answer' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, 'Select all answers that apply');

        // we should have 2 buttons
        SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 2);

        // both answers should display correct styling when clicked
        const answerButtons = SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 2);

        answerButtons.eq(0).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button has correct class',
        );
        $timeout.flush();

        // 'Select an answer' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, 'Select all answers that apply');

        answerButtons.eq(1).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(1).get(0).classList.contains('correct'),
            'button has correct class',
        );
        $timeout.flush();

        // 'continue' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertContinueButtonShown(elem);
    }

    function renderEditor(frame) {
        let json;
        if (frame) {
            json = frame.asJson();
        }

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderEditFrameList(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        frame = lesson.frames[0];

        // check that re-rendering the frame did not change it
        if (json) {
            SpecHelper.expectEqualObjects(frame.asJson(), json);
        }
    }

    function renderPlayer() {
        const renderer = ComponentizedEditorIntegrationSpecHelper.renderShowLesson(lesson.frames[0]);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
    }
});
