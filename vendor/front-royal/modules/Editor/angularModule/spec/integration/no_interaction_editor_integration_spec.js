import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Editor/angularModule/spec/_helpers/_typescomponentized_editor_integration_spec_helper';

describe('Componentized NoInteraction integration', () => {
    let elem;
    let lesson;
    let SpecHelper;
    let ComponentizedEditorIntegrationSpecHelper;
    let AlwaysReadyContinueButton;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                ComponentizedEditorIntegrationSpecHelper = $injector.get('ComponentizedEditorIntegrationSpecHelper');
                AlwaysReadyContinueButton = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.ContinueButton.AlwaysReadyContinueButton.AlwaysReadyContinueButtonModel',
                );
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for creation of a new no_interaction frame', () => {
        assertCreatesNoInteractionFrame();
        renderPlayer();
        assertPlaysFrame();
    });

    it('should allow for editing an existing no_interaction frame', () => {
        const frame = assertCreatesNoInteractionFrame();
        const json = frame.asJson();
        const reloadedFrame = renderEditor(frame);
        // check that re-rendering the frame did not change it
        SpecHelper.expectEqualObjects(reloadedFrame.asJson(), json);
    });

    function assertCreatesNoInteractionFrame() {
        renderEditor();
        SpecHelper.click(elem, 'button[name="add_frame"]');
        SpecHelper.selectOptionByLabel(elem, 'select[name="interaction_type"]', 'No Interaction');

        ComponentizedEditorIntegrationSpecHelper.setMainTextAndAssertOnScreen(elem);
        ComponentizedEditorIntegrationSpecHelper.setMainImageAndAssertOnScreen(elem);

        return lesson.frames[0];
    }

    function assertPlaysFrame() {
        // there should be a continue button
        const button = SpecHelper.expectElement(elem, '.cf-continue-button');

        const model = button.scope().model;
        SpecHelper.expectEqual(AlwaysReadyContinueButton, model.constructor, 'continue button type');
    }

    function renderEditor(frame) {
        const renderer = ComponentizedEditorIntegrationSpecHelper.renderEditFrameList(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        return lesson.frames[0];
    }

    function renderPlayer() {
        const frame = lesson.frames[0];
        const json = frame.asJson();

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderShowLesson(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        // check that rendering the frame did not change it
        SpecHelper.expectEqualObjects(frame.asJson(), json);
    }
});
