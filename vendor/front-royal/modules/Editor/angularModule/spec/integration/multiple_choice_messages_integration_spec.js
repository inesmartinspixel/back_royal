import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Editor/angularModule/spec/_helpers/_typescomponentized_editor_integration_spec_helper';

describe('Componentized Multiple Choice Messages integration', () => {
    let elem;
    let lesson;
    let scope;
    let SpecHelper;
    let ComponentizedEditorIntegrationSpecHelper;
    let ChallengesModel;
    let MultipleChoiceChallengeModel;
    let AnswerListModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                ComponentizedEditorIntegrationSpecHelper = $injector.get('ComponentizedEditorIntegrationSpecHelper');
                ChallengesModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.Challenges.ChallengesModel',
                );
                MultipleChoiceChallengeModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.Challenge.MultipleChoiceChallenge.MultipleChoiceChallengeModel',
                );
                AnswerListModel = $injector.get(
                    'Lesson.FrameList.Frame.Componentized.Component.AnswerList.AnswerListModel',
                );
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for adding messages to a frame and previewing immediately', () => {
        createFrameAndAddMessages();
        assertMessagesShown('wrong!', 'correct!');
    });

    it('should allow for editing messages on an existing frame', () => {
        const frame = createFrameAndAddMessages();
        renderEditor(frame);
        SpecHelper.updateTextArea(elem, '.messages textarea', 'correct edited!', 0);
        SpecHelper.updateTextArea(elem, '.messages textarea', 'wrong edited!', 1);

        assertMessagesShown('wrong edited!', 'correct edited!');
    });

    it('should apply the same message to identical answers', () => {
        const frame = renderEditor();
        setupMultipleChoiceFrame(frame);

        // edit the answers so they both have the same message
        SpecHelper.updateTextArea(elem, '[name="shared_answers"] [name="answer"] textarea', 'answer', 0);
        SpecHelper.updateTextArea(elem, '[name="shared_answers"] [name="answer"] textarea', 'answer', 1);

        const messagesEl = SpecHelper.expectElement(elem, '.messages');
        // add a message to one of the answers
        SpecHelper.click(messagesEl, 'button[name="add_message"]', 0);
        SpecHelper.updateTextArea(messagesEl, 'textarea', 'a message', 0);

        // assert that it is on the other message:
        // the add_message button should have disappeared
        SpecHelper.expectNoElement(messagesEl, 'button[name="add_message"]');
        // and the text input should have the same value
        const inputs = SpecHelper.expectElements(messagesEl, 'textarea', 2);
        SpecHelper.expectEqual('a message', inputs.eq(1).val(), 'Value for second input');
    });

    function createFrameAndAddMessages() {
        const frame = renderEditor();
        setupMultipleChoiceFrame(frame);
        addMessages();
        return frame;
    }

    function addMessages() {
        const messagesEl = SpecHelper.expectElement(elem, '.messages');
        // add a message to the correct answer
        SpecHelper.click(messagesEl, 'button[name="add_message"]', 0);
        SpecHelper.updateTextArea(messagesEl, 'textarea', 'correct!');
        // // add a message to the incorrect answer
        SpecHelper.click(messagesEl, 'button[name="add_message"]');
        SpecHelper.updateTextArea(messagesEl, 'textarea', 'wrong!', 1);
    }

    function assertMessagesShown(wrongMessage, correctMessage) {
        renderPlayer();

        // click the incorrect answer
        SpecHelper.click(elem, '.cf_answer_list button', 1);
        let slideMessage = SpecHelper.expectElement(elem, '.slide-message-display');
        SpecHelper.expectElementText(slideMessage, '.message-element', wrongMessage);
        SpecHelper.expectEqual(
            true,
            slideMessage.get(0).classList.contains('incorrect'),
            'slide message has "incorrect" class',
        );

        // click the correct answer
        SpecHelper.click(elem, '.cf_answer_list button', 0);
        slideMessage = SpecHelper.expectElement(elem, '.slide-message-display');
        SpecHelper.expectElementText(slideMessage, '.message-element', correctMessage);
        SpecHelper.expectEqual(
            true,
            slideMessage.get(0).classList.contains('correct'),
            'slide message has "correct" class',
        );
    }

    function renderEditor(frame) {
        const renderer = ComponentizedEditorIntegrationSpecHelper.renderEditFrameList(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        scope = renderer.scope;
        if (!frame) {
            SpecHelper.click(elem, 'button[name="add_frame"]');
        }
        return frame || lesson.frames[0];
    }

    function renderPlayer() {
        const frame = lesson.frames[0];
        const json = frame.asJson();

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderShowLesson(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        // check that rendering the frame did not change it
        SpecHelper.expectEqualObjects(frame.asJson(), json);
    }

    function setupMultipleChoiceFrame(frame) {
        // we don't want to use any of the actual editor templates.  We just
        // want to create a generic multiple choice frame, so we'll do it
        // by hand
        const challengesHelper = ChallengesModel.EditorViewModel.addComponentTo(frame).setup();
        const challenges = challengesHelper.model;
        const challengeHelper = MultipleChoiceChallengeModel.EditorViewModel.addComponentTo(frame).setup();
        const challenge = challengeHelper.model;
        challenges.challenges.push(challenge);
        challenge.behaviors.ImmediateValidation = {};
        challenge.behaviors.DisallowMultipleSelect = {};
        challenge.validator.behaviors.HasAllExpectedAnswers = {};
        challenge.validator.behaviors.HasNoUnexpectedAnswers = {};

        const answerListHelper = AnswerListModel.EditorViewModel.addComponentTo(frame).setup();
        const answerList = answerListHelper.model;
        answerList.skin = 'buttons';
        answerList.editorViewModel.addAnswer();
        answerList.editorViewModel.addAnswer();
        challenges.editorViewModel.setConfig({
            supportsSharedAnswerList: true,
        });

        challenges.editorViewModel.sharedAnswerList = answerList;
        answerListHelper.model.answers[0].text.text = 'Correct Answer';
        answerListHelper.model.answers[1].text.text = 'Incorrect Answer';
        challengeHelper.correctAnswer = answerList.answers[0];
        challenge.answerList = answerListHelper.model;
        frame.mainUiComponent = challenges;
        scope.$digest();
    }
});
