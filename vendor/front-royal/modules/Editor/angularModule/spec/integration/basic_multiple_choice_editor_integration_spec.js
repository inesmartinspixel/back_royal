import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Editor/angularModule/spec/_helpers/_typescomponentized_editor_integration_spec_helper';
import stubSpecLocale from 'Translation/stubSpecLocale';

stubSpecLocale(
    'lessons.lesson.frame_list.frame.componentized.component.answer_list.answer_list_buttons.select_multiple_answers',
);

describe('Componentized BasicMultipleChoice integration', () => {
    let elem;
    let lesson;
    let SpecHelper;
    let ComponentizedEditorIntegrationSpecHelper;
    let $timeout;
    let $injector;
    let $httpBackend;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                $httpBackend = $injector.get('$httpBackend');
                ComponentizedEditorIntegrationSpecHelper = $injector.get('ComponentizedEditorIntegrationSpecHelper');
            },
        ]);

        $httpBackend.whenGET('sounds/button_scaling_click_1.mp3').respond({});
        $httpBackend.whenGET('sounds/button_scaling_click_2.mp3').respond({});
        $httpBackend.whenGET('sounds/button_select.mp3').respond({});
        $httpBackend.whenGET('sounds/button_incorrect.mp3').respond({});
        $httpBackend.whenGET('sounds/button_correct.mp3').respond({});
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for creating a new standard basic_multiple_choice frame, playing it, reloading it, and editing it again', () => {
        createsMultipleChoiceFrame();
        editStandardMode();

        // play it in the player
        renderPlayer();
        playStandardFrame();

        // reload it in the editor and make sure it's still editable
        renderEditor(lesson.frames[0]);
        editStandardMode();
    });

    it('should allow for creating a new check-many basic_multiple_choice frame, playing it, reloading it, and editing it again', () => {
        createsMultipleChoiceFrame();

        // switch to check many and setup challenge
        toggleCheckMany();
        editCheckManyMode();

        // play it in the player
        renderPlayer();
        playCheckManyFrame();

        // reload it in the editor and make sure it's still editable
        renderEditor(lesson.frames[0]);
        editCheckManyMode();
    });

    it('should allow for switching from standard to check-many challenges', () => {
        createsMultipleChoiceFrame();
        editStandardMode();

        // switch to check many
        toggleCheckMany();

        // edit check-many mode
        editCheckManyMode();

        // play it in the player
        renderPlayer();
        playCheckManyFrame();
    });

    it('should allow for switching from standard to check-many challenges', () => {
        createsMultipleChoiceFrame();

        // switch to check many
        toggleCheckMany();

        editCheckManyMode();

        // switch back to standard
        toggleCheckMany();

        // edit standard mode
        editStandardMode();

        // play it in the player
        renderPlayer();
        playStandardFrame();
    });

    it('should allow for creating a frame with no_incorrect_answers', () => {
        createsMultipleChoiceFrame();
        editStandardMode();
        toggleNoIncorrectAnswers();
        renderPlayer();
        playNoIncorrectAnswerFrame();
        // reload it in the editor and make sure nothing changed,and that
        // No Incorrect Answers is stil checked
        renderEditor(lesson.frames[0]);
        SpecHelper.expectCheckboxChecked(elem, 'input[type="checkbox"][name="noIncorrectAnswers"]');
    });

    it('should allow for creating a frame with no_incorrect_answers and check_many', () => {
        createsMultipleChoiceFrame();
        editStandardMode();
        toggleNoIncorrectAnswers();
        toggleCheckMany();
        renderPlayer();
        playNoIncorrectAnswerFrame(true);
        // reload it in the editor and make sure nothing changed,and that
        // No Incorrect Answers is stil checked
        renderEditor(lesson.frames[0]);
        SpecHelper.expectCheckboxChecked(elem, 'input[type="checkbox"][name="noIncorrectAnswers"]');
    });

    function createsMultipleChoiceFrame() {
        renderEditor();
        SpecHelper.click(elem, 'button[name="add_frame"]');
        SpecHelper.selectOptionByLabel(elem, 'select[name="interaction_type"]', 'Multiple Choice');
        return lesson.frames[0];
    }

    function toggleCheckMany() {
        SpecHelper.toggleCheckbox(elem, 'input[type="checkbox"][name="checkMany"]', 0);
    }

    function toggleNoIncorrectAnswers() {
        SpecHelper.toggleCheckbox(elem, 'input[type="checkbox"][name="noIncorrectAnswers"]');
    }

    function editStandardMode() {
        // make sure check-many isn't selected
        SpecHelper.expectCheckboxUnchecked(elem, 'input[type="checkbox"][name="checkMany"]:eq(0)');

        SpecHelper.expectElement(elem, '[name="main_text"] textarea').val();

        // there should initially be one challenge
        SpecHelper.expectElements(elem, '.cf-multiple-choice-challenge-editor', 1);

        // there should initially be two answers
        SpecHelper.expectElements(elem, '[name="shared_answers"] [name="answer"] textarea', 2);

        // updating answer values should be reflected in buttons for current challenge
        const correctAnswerText = 'Correct Answer';
        SpecHelper.updateTextArea(elem, '[name="shared_answers"] [name="answer"] textarea', correctAnswerText, 0);
        SpecHelper.expectElementText(elem, '.frame-container .cf_answer_list button', correctAnswerText, 0);

        const randomText = `answer text ${Math.random()}`;
        SpecHelper.updateTextArea(elem, '[name="answer"] textarea', randomText, 1);
        SpecHelper.expectElementText(elem, '.frame-container .cf_answer_list button', randomText, 1);

        // there should be a button to add answers
        SpecHelper.expectElements(elem, '[name="add_answer"]', 1);

        // there should not be a button to add challenges
        SpecHelper.expectElements(elem, '[name="add_challenge"]', 0);

        // the challenges component should render as a single select
        SpecHelper.expectElements(elem, '[name="correct_answer"]', 1);
        SpecHelper.expectElements(elem, '[name="correct_answers"]', 0);

        // set the correct answer to the first one
        SpecHelper.selectOptionByLabel(elem, '[name="correct_answer"]', `1: ${correctAnswerText}`);
    }

    function editCheckManyMode() {
        // make sure check-many is selected
        SpecHelper.expectCheckboxChecked(elem, 'input[type="checkbox"][name="checkMany"]:eq(0)');

        SpecHelper.expectElement(elem, '[name="main_text"] textarea').val();

        // there should initially be one challenge
        SpecHelper.expectElements(elem, '.cf-multiple-choice-challenge-editor', 1);

        // there should initially be two answers
        SpecHelper.expectElements(elem, '[name="shared_answers"] [name="answer"] textarea', 2);

        // updating answer values should be reflected in buttons for current challenge
        SpecHelper.updateTextArea(elem, '[name="shared_answers"] [name="answer"] textarea', 'Answer 1', 0);
        SpecHelper.expectElementText(elem, '.frame-container .cf_answer_list button', 'Answer 1', 0);

        const randomText = `answer text ${Math.random()}`;
        SpecHelper.updateTextArea(elem, '[name="answer"] textarea', randomText, 1);
        SpecHelper.expectElementText(elem, '.frame-container .cf_answer_list button', randomText, 1);

        // there should be a button to add answers
        SpecHelper.expectElements(elem, '[name="add_answer"]', 1);
        // there should not be a button to add challenges
        SpecHelper.expectElements(elem, '[name="add_challenge"]', 0);

        // the challenges component should render as a multiple select
        SpecHelper.expectElements(elem, '[name="correct_answer"]', 0);
        SpecHelper.expectElements(elem, '[name="correct_answers"]', 1);

        // set both answers to be required
        const checkboxes = SpecHelper.expectElements(elem, '[name="correct_answers"] input', 2);
        checkboxes.each(function () {
            SpecHelper.setCheckboxValue($(this), true);
        });
    }

    function playStandardFrame() {
        // 'Select an answer' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, 'Select an answer');

        // clicking the wrong answer should show incorrect styling
        const answerButtons = SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 2);

        answerButtons.eq(1).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(1).get(0).classList.contains('incorrect'),
            'button has incorrect class',
        );
        $timeout.flush();

        // clicking the correct answer should show correct styling and move on to next challenge
        answerButtons.eq(0).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button has correct class',
        );
        $timeout.flush();

        // 'continue' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertContinueButtonShown(elem);
    }

    function playNoIncorrectAnswerFrame(checkMany) {
        // 'Select an answer' should be displayed
        const instructions = !checkMany ? 'Select an answer' : 'Select all answers that apply';
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, instructions);

        // clicking an answer should not show correct styling, but should show continue button
        const answerButtons = SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 2);
        answerButtons.eq(0).click();
        SpecHelper.expectEqual(
            false,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button has correct class',
        );
        SpecHelper.expectEqual(
            false,
            answerButtons.eq(0).get(0).classList.contains('incorrect'),
            'button has incorrect class',
        );
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(0).get(0).classList.contains('selected'),
            'button has selected class',
        );
        $timeout.flush();

        // 'continue' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertContinueButtonShown(elem);

        // if checkMany, then selecting two answers should work
        if (checkMany) {
            answerButtons.eq(1).click();
            $timeout.flush();

            [0, 1].forEach(i => {
                SpecHelper.expectEqual(
                    false,
                    answerButtons.eq(i).get(0).classList.contains('correct'),
                    'button has correct class',
                );
                SpecHelper.expectEqual(
                    false,
                    answerButtons.eq(i).get(0).classList.contains('incorrect'),
                    'button has incorrect class',
                );
                SpecHelper.expectEqual(
                    true,
                    answerButtons.eq(i).get(0).classList.contains('selected'),
                    'button has selected class',
                );
            });

            // unselect before moving on
            answerButtons.eq(1).click();
            $timeout.flush();
        }

        // clicking again should unselect the answer
        answerButtons.eq(0).click();
        SpecHelper.expectEqual(
            false,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button has correct class',
        );
        SpecHelper.expectEqual(
            false,
            answerButtons.eq(0).get(0).classList.contains('incorrect'),
            'button has incorrect class',
        );
        SpecHelper.expectEqual(
            false,
            answerButtons.eq(0).get(0).classList.contains('selected'),
            'button has selected class',
        );
        $timeout.flush();
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, instructions);

        // selecting an answer and clicking continue should finish the frame
        answerButtons.eq(0).click();
        const frameViewModel = elem.find('.show-frames-player').scope().playerViewModel.activeFrameViewModel;
        jest.spyOn(frameViewModel, 'gotoNextFrame').mockImplementation(() => {});
        SpecHelper.click(elem, '.cf-continue-button button');
        expect(frameViewModel.gotoNextFrame).toHaveBeenCalled();
        $timeout.flush();
    }

    function playCheckManyFrame() {
        // 'Select an answer' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, 'Select all answers that apply');

        // we should have 2 buttons with the check-many icon styling
        SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 2);

        // both answers should display correct styling when clicked
        const answerButtons = SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 2);

        answerButtons.eq(0).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button has correct class',
        );
        $timeout.flush();

        SpecHelper.expectNoElement(elem, 'button.cf-continue-button:enabled');

        answerButtons.eq(1).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(1).get(0).classList.contains('correct'),
            'button has correct class',
        );
        $timeout.flush();

        // 'continue' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertContinueButtonShown(elem);
    }

    function renderEditor(frame) {
        if (elem) {
            elem.remove();
        }
        let json;
        if (frame) {
            json = frame.asJson();
        }

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderEditFrameList(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        frame = lesson.frames[0];
        scope = renderer.scope;
        scope.$digest();

        // check that re-rendering the frame did not change it
        if (json) {
            SpecHelper.expectEqualObjects(frame.asJson(), json);
        }
    }

    function renderPlayer() {
        if (elem) {
            elem.remove();
        }
        const renderer = ComponentizedEditorIntegrationSpecHelper.renderShowLesson(lesson.frames[0]);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        scope = renderer.scope;
    }
});
