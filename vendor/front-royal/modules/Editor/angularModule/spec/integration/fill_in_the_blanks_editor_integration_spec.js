import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Editor/angularModule/spec/_helpers/_typescomponentized_editor_integration_spec_helper';

describe('Componentized FillInTheBlanks integration', () => {
    let elem;
    let lesson;
    let SpecHelper;
    let ComponentizedEditorIntegrationSpecHelper;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                ComponentizedEditorIntegrationSpecHelper = $injector.get('ComponentizedEditorIntegrationSpecHelper');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for creating a new sequential fill_in_the_blanks frame, playing it, reloading it, and editing it again', () => {
        createsFillInTheBlanksFrame();
        addTwoSequentialChallenges();

        // play it in the player
        renderPlayer();
        playSequentialFrame();

        // reload it in the editor and make sure it's still editable
        renderEditor(lesson.frames[0]);
        addTwoSequentialChallenges();
    });

    it('should allow for creating a new consumable fill_in_the_blanks frame, playing it, reloading it, and editing it again', () => {
        createsFillInTheBlanksFrame();
        addTwoConsumableChallenges();

        // play it in the player
        renderPlayer();
        playConsumableFrame();

        // reload it in the editor and make sure it's still editable
        renderEditor(lesson.frames[0]);
        addTwoConsumableChallenges();
    });

    it('should allow for switching from consumable to sequential', () => {
        createsFillInTheBlanksFrame();
        addTwoConsumableChallenges();

        // switch to sequential
        SpecHelper.toggleRadio(elem, 'input[type="radio"][value="sequential"]');

        // the correct answer from each blank should have been copied over.  We're just gonna
        // check here that there are the right number of answers.  The specifics are tested
        // in the unit test
        const answerInputs = elem.find('.cf-multiple-choice-challenge-editor .cf-answer-list-editor textarea');

        SpecHelper.expectEqual(getBlankTexts().length, answerInputs.length, 'answer count');

        addTwoSequentialChallenges();
    });

    it('should allow for switching from sequential to consumable', () => {
        createsFillInTheBlanksFrame();
        addTwoSequentialChallenges();

        // get all of the answer texts from the answer inputs in the info panel
        const answerInputs = elem.find('.cf-multiple-choice-challenge-editor .cf-answer-list-editor textarea');
        const answerTextsBeforeSwitching = [];
        angular.forEach(answerInputs, input => {
            answerTextsBeforeSwitching.push($(input).val());
        });

        // switch to consumable
        SpecHelper.toggleRadio(elem, 'input[type="radio"][value="consumable"]');

        // now there is only one answer list, and it is on the screen.  Check that
        // we have a button for each answer that existed before.
        const answerButtons = elem.find('.frame-container .cf_answer_list button');
        const answerTextsAfterSwitching = [];
        angular.forEach(answerButtons, button => {
            answerTextsAfterSwitching.push($(button).find('.button_label').text().trim());
        });

        SpecHelper.expectEqual(answerTextsBeforeSwitching.sort(), answerTextsAfterSwitching.sort());

        // check that we can edit it
        addTwoConsumableChallenges();
    });

    function getBlankTexts() {
        const regex = /([^\!\[]|^)\[([^\[\]]+)\]/g;
        const mainText = SpecHelper.expectElement(elem, '[name="main_text"] textarea').val();
        const matches = mainText.match(regex) || [];
        return matches.map(match => match.replace(/[\[\]]/g, '')); // remove brackets
    }

    function createsFillInTheBlanksFrame() {
        renderEditor();
        SpecHelper.click(elem, 'button[name="add_frame"]');
        SpecHelper.selectOptionByLabel(elem, 'select[name="interaction_type"]', 'Fill in the Blanks');
        return lesson.frames[0];
    }

    function replaceBlanksWithUnderscores(mainText) {
        return mainText.replace(/\[(.+?)\]/g, (match, p1) => p1.replace(/./g, '_'));
    }

    function addTwoSequentialChallenges() {
        SpecHelper.toggleRadio(elem, 'input[type="radio"][value="sequential"]');

        let mainText = SpecHelper.expectElement(elem, '[name="main_text"] textarea').val();

        const matches = mainText.match(/\[/g);
        const initialChallengeBlankCount = matches ? matches.length : 0;

        // there should be as many challenge editors as blanks
        SpecHelper.expectElements(elem, '.cf-multiple-choice-challenge-editor', initialChallengeBlankCount);

        // we should be able to add a challenge by adding a blank
        mainText = `${mainText}and [a blank] and`;
        ComponentizedEditorIntegrationSpecHelper.setMainTextAndAssertOnScreen(
            elem,
            '[name="main_text"] textarea',
            mainText,
            replaceBlanksWithUnderscores(mainText),
        );

        // there should be as many challenge editors as blanks
        const challengeEditors = SpecHelper.expectElements(
            elem,
            '.cf-multiple-choice-challenge-editor',
            initialChallengeBlankCount + 1,
        );

        // we should see one answer button for the newly created challenge, and
        // it should have the same text as our blank
        $timeout.flush();
        SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 1);
        SpecHelper.expectElementText(elem, '.frame-container .cf_answer_list button', 'a blank');

        // and be able to update it inside the first challenge's section
        const answerText = `answer text ${Math.random()}`;
        const challengeEditor = challengeEditors.eq(initialChallengeBlankCount);
        SpecHelper.updateTextArea(challengeEditor, '[name="answer"] textarea', answerText, 0);
        SpecHelper.expectElementText(elem, '.frame-container .cf_answer_list button', answerText, 0);

        // but we should not be able to delete the correct answer
        SpecHelper.expectElementDisabled(challengeEditor, '[name="remove"]');

        // we should be able to add a condfuser and edit it
        const answer2Text = `answer text ${Math.random()}`;
        SpecHelper.click(challengeEditor, '[name="add_answer"]');
        SpecHelper.updateTextArea(challengeEditor, '[name="answer"] textarea', answer2Text, 1);
        SpecHelper.expectElementEnabled(challengeEditor.find('[name="answer"]').eq(1), '[name="remove"]');
        SpecHelper.expectElementText(elem, '.frame-container .cf_answer_list button', answer2Text, 1);

        // we should be able to add a second challenge by updating the text
        mainText = `${mainText}and [another] and`;
        ComponentizedEditorIntegrationSpecHelper.setMainTextAndAssertOnScreen(
            elem,
            '[name="main_text"] textarea',
            mainText,
            replaceBlanksWithUnderscores(mainText),
        );

        // there should be as many challenge editors as blanks
        SpecHelper.expectElements(elem, '.cf-multiple-choice-challenge-editor', initialChallengeBlankCount + 2);
    }

    function addTwoConsumableChallenges() {
        SpecHelper.toggleRadio(elem, 'input[type="radio"][value="consumable"]');

        let mainText = SpecHelper.expectElement(elem, '[name="main_text"] textarea').val();

        const matches = mainText.match(/\[/g);
        const initialChallengeBlankCount = matches ? matches.length : 0;
        const initialAnswerCount = elem.find('.frame-container .cf_answer_list button').length;

        SpecHelper.expectEqual(
            true,
            initialAnswerCount >= initialChallengeBlankCount,
            'at least as many answers as blanks',
        );

        // there should be as many challenge editors as blanks
        SpecHelper.expectElements(elem, '.cf-multiple-choice-challenge-editor', initialChallengeBlankCount);

        // we should be able to add a challenge by adding a blank
        mainText = `${mainText}and [a blank] and`;
        ComponentizedEditorIntegrationSpecHelper.setMainTextAndAssertOnScreen(
            elem,
            '[name="main_text"] textarea',
            mainText,
            replaceBlanksWithUnderscores(mainText),
        );

        // there should be as many challenge editors as blanks
        SpecHelper.expectElements(elem, '.cf-multiple-choice-challenge-editor', initialChallengeBlankCount + 1);

        // we should see one answer button for the newly created challenge, and
        // it should have the same text as our blank
        SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', initialAnswerCount + 1);
        SpecHelper.expectElementText(
            elem,
            '.frame-container .cf_answer_list button .button_label',
            'a blank',
            initialAnswerCount,
        );

        // and be able to update it inside the sharedAnswerList section
        const answerText = `answer text ${Math.random()}`;
        const answerListEditor = SpecHelper.expectElement(elem, '[name="shared_answers"]');
        SpecHelper.updateTextArea(answerListEditor, '[name="answer"] textarea', answerText, initialAnswerCount);
        SpecHelper.expectElementText(
            elem,
            '.frame-container .cf_answer_list button .button_label',
            answerText,
            initialAnswerCount,
        );

        // but we should not be able to delete the correct answer
        SpecHelper.expectElementDisabled(
            answerListEditor.find('[name="answer"]').eq(initialAnswerCount),
            '[name="remove"]',
        );

        // we should be able to add a confuser and edit it
        const answer2Text = `answer text ${Math.random()}`;
        SpecHelper.click(answerListEditor, '[name="add_answer"]');
        SpecHelper.updateTextArea(answerListEditor, '[name="answer"] textarea', answer2Text, initialAnswerCount + 1);
        SpecHelper.expectElementText(
            elem,
            '.frame-container .cf_answer_list button .button_label',
            answer2Text,
            initialAnswerCount + 1,
        );
        SpecHelper.expectElementEnabled(
            answerListEditor.find('[name="answer"]').eq(initialAnswerCount + 1),
            '[name="remove"]',
        );

        // we should be able to add a second challenge by updating the text
        mainText = `${mainText}and [another] and`;
        ComponentizedEditorIntegrationSpecHelper.setMainTextAndAssertOnScreen(
            elem,
            '[name="main_text"] textarea',
            mainText,
            replaceBlanksWithUnderscores(mainText),
        );

        // there should be as many challenge editors as blanks
        SpecHelper.expectElements(elem, '.cf-multiple-choice-challenge-editor', initialChallengeBlankCount + 2);

        // another answer should have been added
        SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', initialAnswerCount + 3);
    }

    function playSequentialFrame() {
        // 'Select an answer for each blank' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, 'Select an answer for each blank');

        // first blank should be selected
        let blanks = SpecHelper.expectElements(elem, '.section.text .blank', 2);
        SpecHelper.expectEqual(true, blanks.eq(0).get(0).classList.contains('selected'), 'blank 0 selected');
        SpecHelper.expectEqual(false, blanks.eq(1).get(0).classList.contains('selected'), 'blank 1 selected');

        // clicking the wrong answer should show incorrect styling
        let answerButtons = SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 2);
        answerButtons.eq(1).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(1).get(0).classList.contains('incorrect'),
            'button has incorrect class',
        );
        $timeout.flush();

        // first blank should still be selected
        SpecHelper.expectEqual(true, blanks.eq(0).get(0).classList.contains('selected'), 'blank 0 selected');
        SpecHelper.expectEqual(false, blanks.eq(1).get(0).classList.contains('selected'), 'blank 1 selected');

        // clicking the correct answer should show correct styling and move on to next challenge
        answerButtons.eq(0).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button has correct class',
        );
        $timeout.flush();

        blanks = SpecHelper.expectElements(elem, '.section.text .blank', 2);

        // now second blank should be selected
        SpecHelper.expectEqual(false, blanks.eq(0).get(0).classList.contains('selected'), 'blank 0 selected');
        SpecHelper.expectEqual(true, blanks.eq(0).get(0).classList.contains('completed'), 'blank 0 selected');
        SpecHelper.expectEqual(true, blanks.eq(1).get(0).classList.contains('selected'), 'blank 1 selected');

        // clicking the correct answer should show correct styling and finish the frame
        answerButtons = SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 1);
        SpecHelper.expectEqual(
            false,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button has correct class',
        );
        answerButtons.eq(0).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button has correct class',
        );

        $timeout.flush();

        // 'continue' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertContinueButtonShown(elem);
    }

    function playConsumableFrame() {
        // 'Select an answer for each blank' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, 'Select an answer for each blank');

        // first blank should be selected
        let blanks = SpecHelper.expectElements(elem, '.section.text .blank', 2);
        SpecHelper.expectEqual(true, blanks.eq(0).get(0).classList.contains('selected'), 'blank 0 selected');
        SpecHelper.expectEqual(false, blanks.eq(1).get(0).classList.contains('selected'), 'blank 1 selected');

        // clicking the wrong answer should show incorrect styling
        const answerButtons = elem.find('.frame-container .cf_answer_list button');
        answerButtons.eq(1).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(1).get(0).classList.contains('incorrect'),
            'button has incorrect class',
        );
        $timeout.flush();

        // first blank should still be selected
        SpecHelper.expectEqual(true, blanks.eq(0).get(0).classList.contains('selected'), 'blank 0 selected');
        SpecHelper.expectEqual(false, blanks.eq(1).get(0).classList.contains('selected'), 'blank 1 selected');

        // clicking the correct answer should show correct styling and move on to next challenge
        answerButtons.eq(0).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button 0 has correct class after clicking',
        );
        $timeout.flush();

        blanks = SpecHelper.expectElements(elem, '.section.text .blank', 2);

        // selected answer should still have correct class, and be disabled
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button 0 has correct class after flushing',
        );
        SpecHelper.expectEqual('disabled', answerButtons.eq(0).attr('disabled'), 'selected correct answer disabled');

        // now second blank should be selected
        SpecHelper.expectEqual(false, blanks.eq(0).get(0).classList.contains('selected'), 'blank 0 selected');
        SpecHelper.expectEqual(true, blanks.eq(0).get(0).classList.contains('completed'), 'blank 0 selected');
        SpecHelper.expectEqual(true, blanks.eq(1).get(0).classList.contains('selected'), 'blank 1 selected');

        // clicking the correct answer should show correct styling and finish the frame
        answerButtons.eq(2).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(2).get(0).classList.contains('correct'),
            'button 2 has correct class after clicking',
        );

        $timeout.flush();

        // 'continue' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertContinueButtonShown(elem);
    }

    function renderEditor(frame) {
        let json;
        if (frame) {
            json = frame.asJson();
        }

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderEditFrameList(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        frame = lesson.frames[0];
        // check that re-rendering the frame did not change it
        if (json) {
            SpecHelper.expectEqualObjects(frame.asJson(), json);
        }
    }

    function renderPlayer() {
        const renderer = ComponentizedEditorIntegrationSpecHelper.renderShowLesson(lesson.frames[0]);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
    }
});
