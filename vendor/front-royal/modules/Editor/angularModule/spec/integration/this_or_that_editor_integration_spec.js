import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Editor/angularModule/spec/_helpers/_typescomponentized_editor_integration_spec_helper';
import locales from 'Lessons/locales/lessons/lesson/frame_list/frame/componentized/component/tile_prompt_board-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(locales);

describe('Componentized ThisOrThat integration', () => {
    let elem;
    let lesson;
    let SpecHelper;
    let ComponentizedEditorIntegrationSpecHelper;
    let $timeout;
    let $httpBackend;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                ComponentizedEditorIntegrationSpecHelper = $injector.get('ComponentizedEditorIntegrationSpecHelper');
                $httpBackend = $injector.get('$httpBackend');
                // backend definition common for all tests
                $httpBackend.expectGET('/sounds/button_correct.mp3').respond({});
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for creating a new matching frame, playing it, reloading it, and editing it again', () => {
        createMatchingFrame();
        addTwoChallenges();

        // play it in the player
        renderPlayer();
        playFrame();

        // reload it in the editor and make sure it's still editable
        renderEditor(lesson.frames[0]);
        addTwoChallenges();
    });

    function createMatchingFrame() {
        renderEditor();
        SpecHelper.click(elem, 'button[name="add_frame"]');
        SpecHelper.selectOptionByLabel(elem, 'select[name="interaction_type"]', 'This or That?');

        return lesson.frames[0];
    }

    let isFirstPass = true;
    let answerText0;
    let answerText1;

    function addTwoChallenges() {
        SpecHelper.expectElement(elem, '[name="main_text"] textarea').val();
        const challengeEditors = elem.find('.cf-multiple-choice-challenge-editor');
        const initialChallengeCount = challengeEditors.length;

        // we should see a challenge button
        SpecHelper.expectElements(elem, '.tile:not(.fake-tile) .cf-tile-prompt', initialChallengeCount);

        // If there is more than one challenge, activate the first one
        if (initialChallengeCount > 1) {
            SpecHelper.click(elem, '.challenge_row', 0);
        }
        const initialAnswerCount = elem.find('.frame-container .cf_answer_list button').length;
        SpecHelper.expectEqual(true, initialAnswerCount === 2, 'two answers');

        // and be able to update it
        const buttonText = `button text 0 ${Math.random()}`;
        SpecHelper.updateTextArea(
            elem,
            '[name="challenges_list"] .challenge_row .cf-text-editor textarea',
            buttonText,
            0,
        );
        SpecHelper.expectElementText(
            elem,
            '.frame-container .tile:not(.fake-tile) .cf-tile-prompt .prompt.text',
            buttonText,
            0,
        );

        if (isFirstPass) {
            // we should be able to update the first answer
            answerText0 = `answer text ${Math.random()}`;
            answerText1 = `answer text ${Math.random()}`;

            const answerListEditor = SpecHelper.expectElement(elem, '[name="shared_answers"]');

            // setup answer 1
            SpecHelper.updateTextArea(answerListEditor, '[name="answer"] textarea', answerText0, 0);
            SpecHelper.expectElementText(elem, '.frame-container .cf_answer_list button .button_label', answerText0, 0);
            SpecHelper.expectNoElement(answerListEditor, '[name="font-style-editor"]');

            // setup answer 2
            SpecHelper.updateTextArea(answerListEditor, '[name="answer"] textarea', answerText1, 1);
            SpecHelper.expectElementText(elem, '.frame-container .cf_answer_list button .button_label', answerText1, 1);
            SpecHelper.expectNoElement(answerListEditor, '[name="font-style-editor"]');

            // we should not be able to add or remove answers
            SpecHelper.expectElementDisabled(answerListEditor.find('[name="answer"]').eq(0), '[name="remove"]');
            SpecHelper.expectElementDisabled(answerListEditor.find('[name="answer"]').eq(1), '[name="remove"]');
            SpecHelper.expectNoElement(elem, '[name="add_answer"]');

            // set first challenge correct answer
            SpecHelper.selectOptionByLabel(elem, 'select[name="correct_answer"]', `1: ${answerText0}`); // account for indexing
        }

        // we should be able to add a new challenge by updating the text
        SpecHelper.click(elem, '[name="add_challenge"]');

        // there should be as many challenge editors as challenges
        const challengeElems = SpecHelper.expectElements(
            elem,
            '.cf-multiple-choice-challenge-editor',
            initialChallengeCount + 1,
        );

        // set second challenge correct answer
        SpecHelper.selectOptionByLabel(challengeElems.eq(1), 'select[name="correct_answer"]', `2: ${answerText1}`); // account for indexing

        isFirstPass = false;
    }

    function playFrame() {
        // 'Select an answer for each card' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, 'Select an answer for each card');

        // first challenge button should be selected
        const tilePrompts = SpecHelper.expectElements(elem, '.tile:not(.fake-tile) .cf-tile-prompt', 2);

        SpecHelper.expectEqual(true, tilePrompts.eq(0).get(0).classList.contains('selected'), 'tile prompt 0 selected');
        SpecHelper.expectEqual(
            false,
            tilePrompts.eq(1).get(0).classList.contains('selected'),
            'tile prompt 1 selected',
        );

        // clicking the wrong answer should show incorrect styling
        const answerButtons = elem.find('.frame-container .cf_answer_list button');
        answerButtons.eq(1).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(1).get(0).classList.contains('incorrect'),
            'button has incorrect class',
        );
        $timeout.flush();

        // first tile prompt should still be selected
        SpecHelper.expectEqual(true, tilePrompts.eq(0).get(0).classList.contains('selected'), 'tile prompt 0 selected');
        SpecHelper.expectEqual(
            false,
            tilePrompts.eq(1).get(0).classList.contains('selected'),
            'tile prompt 1 selected',
        );

        // clicking the correct answer should show correct styling and move on to next challenge
        answerButtons.eq(0).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button 0 has correct class after clicking',
        );
        $timeout.flush();

        // now second tile prompt should be selected
        SpecHelper.expectEqual(
            false,
            tilePrompts.eq(0).get(0).classList.contains('selected'),
            'tile prompt 0 selected',
        );
        SpecHelper.expectEqual(
            true,
            tilePrompts.eq(0).get(0).classList.contains('completed'),
            'tile prompt 0 selected',
        );
        SpecHelper.expectEqual(true, tilePrompts.eq(1).get(0).classList.contains('selected'), 'tile prompt 1 selected');

        // clicking the correct answer should show correct styling and finish the frame
        answerButtons.eq(1).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(1).get(0).classList.contains('correct'),
            'button 1 has correct class after clicking',
        );

        $timeout.flush();

        // 'continue' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertContinueButtonShown(elem);
    }

    function renderEditor(frame) {
        let json;
        if (frame) {
            json = frame.asJson();
        }

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderEditFrameList(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        frame = lesson.frames[0];
        // check that re-rendering the frame did not change it
        if (json) {
            SpecHelper.expectEqualObjects(frame.asJson(), json);
        }
    }

    function renderPlayer() {
        const frame = lesson.frames[0];
        const json = frame.asJson();

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderShowLesson(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        // check that rendering the frame did not change it
        SpecHelper.expectEqualObjects(frame.asJson(), json);
    }
});
