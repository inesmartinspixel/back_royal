import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Editor/angularModule/spec/_helpers/_typescomponentized_editor_integration_spec_helper';

describe('Componentized Matching integration', () => {
    let elem;
    let lesson;
    let SpecHelper;
    let ComponentizedEditorIntegrationSpecHelper;
    let $timeout;
    let $httpBackend;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                ComponentizedEditorIntegrationSpecHelper = $injector.get('ComponentizedEditorIntegrationSpecHelper');
                $httpBackend = $injector.get('$httpBackend');
                // backend definition common for all tests
                $httpBackend.expectGET('/sounds/button_correct.mp3').respond({});
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for creating a new matching frame, playing it, reloading it, and editing it again', () => {
        createMatchingFrame();
        addTwoChallenges();

        // FIXME: the following causes Jest 25.1.0 / JSDom 15 to hang, for unknown reasons

        // // play it in the player
        // renderPlayer();
        // playFrame();
        // // reload it in the editor and make sure it's still editable
        // renderEditor(lesson.frames[0]);
        // addTwoChallenges();
    });

    function createMatchingFrame() {
        renderEditor();
        SpecHelper.click(elem, 'button[name="add_frame"]');
        SpecHelper.selectOptionByLabel(elem, 'select[name="interaction_type"]', 'Matching');

        return lesson.frames[0];
    }

    function addTwoChallenges() {
        SpecHelper.expectElement(elem, '[name="main_text"] textarea').val();
        const challengeEditors = elem.find('.cf-multiple-choice-challenge-editor');
        const initialChallengeCount = challengeEditors.length;

        // we should see a challenge button
        SpecHelper.expectElements(elem, '.cf-matching-challenge-button', initialChallengeCount);

        // If there is more than one challenge, activate the first one
        if (initialChallengeCount > 1) {
            SpecHelper.click(elem, '.challenge_row', 0);
        }
        const initialAnswerCount = elem.find('.frame-container .cf_answer_list button').length;
        SpecHelper.expectEqual(true, initialAnswerCount > 0, 'at least one answer');
        SpecHelper.expectEqual(
            true,
            initialAnswerCount === initialChallengeCount,
            'exactly as many answers as challenge buttons',
        );

        // and be able to update it
        const buttonText = `button text 0 ${Math.random()}`;
        SpecHelper.updateTextArea(
            elem,
            '[name="challenges_list"] .challenge_row .cf-text-editor textarea',
            buttonText,
            0,
        );
        SpecHelper.expectElementText(elem, '.frame-container .cf-matching-challenge-button', buttonText, 0);

        // we should be able to update the first answer
        const answerIndex = 0;
        const answerText = `answer text ${Math.random()}`;
        const answerListEditor = SpecHelper.expectElement(elem, '[name="shared_answers"]');
        SpecHelper.updateTextArea(answerListEditor, '[name="answer"] textarea', answerText, answerIndex);
        SpecHelper.expectElementText(
            elem,
            '.frame-container .cf_answer_list button .button_label',
            answerText,
            answerIndex,
        );

        // but we should not be able to delete the correct answer
        SpecHelper.expectElementDisabled(answerListEditor.find('[name="answer"]').eq(answerIndex), '[name="remove"]');

        // and we should not be able to add a confuse
        SpecHelper.expectNoElement(elem, '[name="add_answer"]');

        // we should be able to add a new challenge by updating the text
        SpecHelper.click(elem, '[name="add_challenge"]');

        // there should be as many challenge editors as challenges
        SpecHelper.expectElements(elem, '.cf-multiple-choice-challenge-editor', initialChallengeCount + 1);

        // another answer should have been added
        SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', initialAnswerCount + 1);
    }

    function playFrame() {
        // 'Match each prompt with an answer' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, 'Match each prompt with an answer');

        // first challenge button should be selected
        const challengeButtons = SpecHelper.expectElements(elem, '.cf-matching-challenge-button button', 2);
        SpecHelper.expectEqual(
            true,
            challengeButtons.eq(0).get(0).classList.contains('selected'),
            'challenge button 0 selected',
        );
        SpecHelper.expectEqual(
            false,
            challengeButtons.eq(1).get(0).classList.contains('selected'),
            'challenge button 1 selected',
        );

        // clicking the wrong answer should show incorrect styling
        const answerButtons = elem.find('.frame-container .cf_answer_list button');
        answerButtons.eq(1).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(1).get(0).classList.contains('incorrect'),
            'button has incorrect class',
        );
        $timeout.flush();

        // first challenge button should still be selected
        SpecHelper.expectEqual(
            true,
            challengeButtons.eq(0).get(0).classList.contains('selected'),
            'challenge button 0 selected',
        );
        SpecHelper.expectEqual(
            false,
            challengeButtons.eq(1).get(0).classList.contains('selected'),
            'challenge button 1 selected',
        );

        // clicking the correct answer should show correct styling and move on to next challenge
        answerButtons.eq(0).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button 0 has correct class after clicking',
        );
        $timeout.flush();

        // selected answer should still have correct class, and be disabled
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button 0 has correct class after flushing',
        );
        SpecHelper.expectEqual('disabled', answerButtons.eq(0).attr('disabled'), 'selected correct answer disabled');

        // now second challenge button should be selected
        SpecHelper.expectEqual(
            false,
            challengeButtons.eq(0).get(0).classList.contains('selected'),
            'challenge button 0 selected',
        );
        SpecHelper.expectEqual(
            true,
            challengeButtons.eq(0).get(0).classList.contains('completed'),
            'challenge button 0 selected',
        );
        SpecHelper.expectEqual(
            true,
            challengeButtons.eq(1).get(0).classList.contains('selected'),
            'challenge button 1 selected',
        );

        // clicking the correct answer should show correct styling and finish the frame
        answerButtons.eq(1).click();
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(1).get(0).classList.contains('correct'),
            'button 1 has correct class after clicking',
        );

        $timeout.flush();

        // 'continue' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertContinueButtonShown(elem);
    }

    function renderEditor(frame) {
        let json;
        if (frame) {
            json = frame.asJson();
        }

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderEditFrameList(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        frame = lesson.frames[0];
        // check that re-rendering the frame did not change it
        if (json) {
            SpecHelper.expectEqualObjects(frame.asJson(), json);
        }
    }

    function renderPlayer() {
        const frame = lesson.frames[0];
        const json = frame.asJson();

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderShowLesson(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        // check that rendering the frame did not change it
        SpecHelper.expectEqualObjects(frame.asJson(), json);
    }
});
