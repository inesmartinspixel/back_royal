import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Editor/angularModule/spec/_helpers/_typescomponentized_editor_integration_spec_helper';
import locales from 'Lessons/locales/lessons/lesson/frame_list/frame/componentized/component/answer_list/answer_list_buttons-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(locales);

describe('Componentized MultipleChoicePoll integration', () => {
    let elem;
    let lesson;
    let SpecHelper;
    let ComponentizedEditorIntegrationSpecHelper;
    let $timeout;
    let $injector;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                ComponentizedEditorIntegrationSpecHelper = $injector.get('ComponentizedEditorIntegrationSpecHelper');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for creating a new standard multiple_choice_poll frame, playing it, reloading it, and editing it again', () => {
        createsFrame();
        editFrame();

        // play it in the player
        renderPlayer();
        playFrame();

        // reload it in the editor and make sure it's still editable
        renderEditor(lesson.frames[0]);
        editFrame();
    });

    function createsFrame() {
        renderEditor();
        SpecHelper.click(elem, 'button[name="add_frame"]');
        SpecHelper.selectOptionByLabel(elem, 'select[name="interaction_type"]', 'Poll');
        return lesson.frames[0];
    }

    function editFrame() {
        SpecHelper.expectElement(elem, '[name="main_text"] textarea').val();

        // there should initially be one challenge
        SpecHelper.expectElements(elem, '.cf-multiple-choice-challenge-editor', 1);

        // there should initially be two answers
        SpecHelper.expectElements(elem, '[name="shared_answers"] [name="answer"] textarea', 2);

        // updating answer values should be reflected in buttons for current challenge
        SpecHelper.updateTextArea(elem, '[name="shared_answers"] [name="answer"] textarea', 'Answer 1', 0);
        SpecHelper.expectElementText(elem, '.frame-container .cf_answer_list button .button_text', 'Answer 1', 0);

        const randomText = `answer text ${Math.random()}`;
        SpecHelper.updateTextArea(elem, '[name="answer"] textarea', randomText, 1);
        SpecHelper.expectElementText(elem, '.frame-container .cf_answer_list button .button_text', randomText, 1);

        // there should be a button to add answers
        SpecHelper.expectElements(elem, '[name="add_answer"]', 1);

        // there should not be a button to add challenges
        SpecHelper.expectElements(elem, '[name="add_challenge"]', 0);
    }

    function playFrame() {
        // 'Select an answer' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, 'Vote to reveal poll results');

        // clicking an answer should not show correct styling, but should show continue button
        const answerButtons = SpecHelper.expectElements(elem, '.frame-container .cf_answer_list button', 2);
        answerButtons.eq(0).click();
        SpecHelper.expectEqual(
            false,
            answerButtons.eq(0).get(0).classList.contains('correct'),
            'button has correct class',
        );
        SpecHelper.expectEqual(
            false,
            answerButtons.eq(0).get(0).classList.contains('incorrect'),
            'button has incorrect class',
        );
        SpecHelper.expectEqual(
            true,
            answerButtons.eq(0).get(0).classList.contains('selected'),
            'button has selected class',
        );

        // buttons should be disabled
        SpecHelper.expectElementDisabled(elem, '.frame-container .cf_answer_list button');
        $timeout.flush();

        // 'continue' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertContinueButtonShown(elem);
    }

    function renderEditor(frame) {
        if (elem) {
            elem.remove();
        }
        let json;
        if (frame) {
            json = frame.asJson();
        }

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderEditFrameList(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        frame = lesson.frames[0];

        // check that re-rendering the frame did not change it
        if (json) {
            SpecHelper.expectEqualObjects(frame.asJson(), json);
        }
    }

    function renderPlayer() {
        if (elem) {
            elem.remove();
        }
        const renderer = ComponentizedEditorIntegrationSpecHelper.renderShowLesson(lesson.frames[0]);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
    }
});
