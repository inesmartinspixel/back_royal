import 'AngularSpecHelper';
import 'Editor/angularModule';
import 'Editor/angularModule/spec/_helpers/_typescomponentized_editor_integration_spec_helper';
import stubSpecLocale from 'Translation/stubSpecLocale';

stubSpecLocale('lessons.lesson.frame_list.frame.componentized.component.layout.text_image_interactive.give_me_a_hint');

describe('Componentized ComposeBlanksOnImage integration', () => {
    let elem;
    let lesson;
    let SpecHelper;
    let ComponentizedEditorIntegrationSpecHelper;
    let $timeout;
    let $httpBackend;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                ComponentizedEditorIntegrationSpecHelper = $injector.get('ComponentizedEditorIntegrationSpecHelper');
                $httpBackend = $injector.get('$httpBackend');
                // backend definition common for all tests
                $httpBackend.expectGET('/sounds/button_correct.mp3').respond({});

                // since there is no width or height on the elements in the test,
                // resizeHandle blows up
                SpecHelper.stubDirective('resizeHandle');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for creating a new compose_blanks_on_image frame, playing it, reloading it, and editing it again', () => {
        createsComposeBlanksOnImageFrame();
        addTwoChallenges();

        // play it in the player
        renderPlayer();
        playFrame();

        // reload it in the editor and make sure it's still editable
        renderEditor(lesson.frames[0]);
        addTwoChallenges();
    });

    function createsComposeBlanksOnImageFrame() {
        renderEditor();
        SpecHelper.click(elem, 'button[name="add_frame"]');
        SpecHelper.selectOptionByLabel(elem, 'select[name="interaction_type"]', 'Compose Blanks on Image');

        // flush the timeout in positionable that creates the resize handle
        $timeout.flush();

        return lesson.frames[0];
    }

    function addTwoChallenges() {
        SpecHelper.expectElement(elem, '[name="main_text"] textarea').val();
        let challengeEditors = elem.find('.cf-user-input-challenge-editor');
        const initialChallengeCount = challengeEditors.length;

        // we should see a challenge blank
        SpecHelper.expectElements(elem, '.cf-challenge-overlay-blank', initialChallengeCount);

        // If there is more than one challenge, activate the first one
        if (initialChallengeCount > 1) {
            SpecHelper.click(elem, '[name="challenges_list"] .challenge_row', 0);
        }

        // and be able to update it
        const blankText = `blank text 0 ${Math.random()}`;
        SpecHelper.updateTextArea(
            elem,
            '[name="challenges_list"] .challenge_row .cf-text-editor textarea',
            blankText,
            0,
        );
        const overlayBlank = elem.find('.frame-container .cf-challenge-overlay-blank').eq(0);

        SpecHelper.expectEqual(blankText, SpecHelper.trimText(overlayBlank.find('.complete_container').text()));
        // we should be able to add a new challenge
        SpecHelper.click(elem, '[name="add_challenge"]');

        // there should be as many challenge editors as blanks
        SpecHelper.expectElements(elem, '.cf-user-input-challenge-editor', initialChallengeCount + 1);

        // Update blanks in preparation for playing the frame
        // Use the text 'a blank' and 'another' so we can reuse tests from compose_blanks_editor_integration
        challengeEditors = elem.find('.cf-user-input-challenge-editor');
        SpecHelper.updateTextArea(challengeEditors.eq(0), '.cf-text-editor textarea', 'a blank', 0);
        SpecHelper.updateTextArea(challengeEditors.eq(1), '.cf-text-editor textarea', 'another', 0);

        // Verify new values were set
        const overlayBlanks = elem.find('.frame-container .cf-challenge-overlay-blank');
        SpecHelper.expectEqual('a blank', SpecHelper.trimText(overlayBlanks.eq(0).find('.complete_container').text()));
        SpecHelper.expectEqual('another', SpecHelper.trimText(overlayBlanks.eq(1).find('.complete_container').text()));
    }

    function playFrame() {
        // 'Type an answer in each blank' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertInstructions(elem, 'Type an answer in each blank');

        // first blank should be selected
        const blanks = SpecHelper.expectElements(elem, '.cf-challenge-overlay-blank', 2);
        SpecHelper.expectEqual(true, blanks.eq(0).get(0).classList.contains('selected'), 'blank 0 selected');
        SpecHelper.expectEqual(false, blanks.eq(1).get(0).classList.contains('selected'), 'blank 1 selected');

        // entering the wrong text should show incorrect styling
        SpecHelper.updateNoApplyTextInput(blanks.eq(0), 'textarea', 'x');
        SpecHelper.expectEqual(
            true,
            blanks.eq(0).find('.user_input').get(0).classList.contains('incorrect'),
            'blank has incorrect class after typing incorrect answer',
        );

        // first blank should still be selected
        SpecHelper.expectEqual(true, blanks.eq(0).get(0).classList.contains('selected'), 'blank 0 selected');
        SpecHelper.expectEqual(false, blanks.eq(1).get(0).classList.contains('selected'), 'blank 1 selected');

        // entering part of the correct answer should show incomplete styling
        SpecHelper.updateNoApplyTextInput(blanks.eq(0), 'textarea', 'a bl');
        SpecHelper.expectEqual(
            true,
            blanks.eq(0).find('.user_input').get(0).classList.contains('incomplete'),
            'blank has incomplete class after typing incomplete answer',
        );
        SpecHelper.expectEqual(
            false,
            blanks.eq(0).find('.user_input').get(0).classList.contains('incorrect'),
            'blank has incorrect class after typing incomplete answer',
        );

        // entering the correct answer should show complete styling
        SpecHelper.updateNoApplyTextInput(blanks.eq(0), 'textarea', 'a blank');
        SpecHelper.expectEqual(
            false,
            blanks.eq(0).find('.user_input').get(0).classList.contains('incomplete'),
            'blank has incomplete class after typing complete answer',
        );
        SpecHelper.expectEqual(
            false,
            blanks.eq(0).find('.user_input').get(0).classList.contains('incorrect'),
            'blank has incorrect class after typing complete answer',
        );
        $timeout.flush();

        // now second blank should be selected
        SpecHelper.expectEqual(false, blanks.eq(0).get(0).classList.contains('selected'), 'blank 0 selected');
        SpecHelper.expectEqual(true, blanks.eq(0).get(0).classList.contains('completed'), 'blank 0 completed');
        SpecHelper.expectEqual(true, blanks.eq(1).get(0).classList.contains('selected'), 'blank 1 selected');

        // continue button should still be disabled
        SpecHelper.expectNoElement(elem, 'button.cf-continue-button:enabled');

        // finishing all challenges should finish the frame
        SpecHelper.updateNoApplyTextInput(blanks.eq(1), 'textarea', 'another');

        // 'continue' should be displayed
        ComponentizedEditorIntegrationSpecHelper.assertContinueButtonShown(elem);
    }

    // Note: copied these from other integration tests - pull out into helpers?
    function renderEditor(frame) {
        let json;
        if (frame) {
            json = frame.asJson();
        }

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderEditFrameList(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        frame = lesson.frames[0];
        // check that re-rendering the frame did not change it
        if (json) {
            SpecHelper.expectEqualObjects(frame.asJson(), json);
        }
    }

    function renderPlayer() {
        const frame = lesson.frames[0];
        const json = frame.asJson();

        const renderer = ComponentizedEditorIntegrationSpecHelper.renderShowLesson(frame);
        lesson = renderer.scope.playerViewModel.lesson;
        elem = renderer.elem;
        // check that rendering the frame did not change it
        SpecHelper.expectEqualObjects(frame.asJson(), json);
    }
});
