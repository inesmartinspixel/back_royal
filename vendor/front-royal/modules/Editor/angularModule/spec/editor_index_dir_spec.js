import 'AngularSpecHelper';
import 'Editor/angularModule';

describe('Editor::IndexDir', () => {
    let elem;
    let scope;
    let SpecHelper;
    let $location;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('FrontRoyal.Editor', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $location = $injector.get('$location');
            SpecHelper.stubDirective('listLessons');
            SpecHelper.stubDirective('listStreams');
            SpecHelper.stubDirective('listPlaylists');
            SpecHelper.stubDirective('listContentTopics');
            SpecHelper.stubCurrentUser('admin');
        });
    });

    beforeEach(() => {
        render();
    });

    afterAll(() => {
        SpecHelper.cleanup();
    });

    it('should have working sections', () => {
        expectLessonsActive();
        clickStreamsSection();
        expectStreamsActive();
        clickLessonsSection();
        expectLessonsActive();
        clickPlaylistsSection();
        expectPlaylistsActive();
        clickContentTopicsSection();
        expectContentTopicsActive();
    });

    it('should hide some sections unless admin', () => {
        SpecHelper.stubCurrentUser('editor');
        render();
        SpecHelper.expectElementNotHidden(elem, '.nav-item[name="lessons"]');
        SpecHelper.expectElementHidden(elem, '.nav-item[name="courses"]');
        SpecHelper.expectElementHidden(elem, '.nav-item[name="playlists"]');
        SpecHelper.expectElementHidden(elem, '.nav-item[name="content_topics"]');
    });

    describe('integration with url bar', () => {
        it('should read stream_id query param', () => {
            jest.spyOn($location, 'search').mockReturnValue({
                section: 'courses',
            });
            render();
            expect(scope.section).toBe('courses');
        });

        it('should write stream_id query param', () => {
            jest.spyOn($location, 'search').mockReturnValue({});
            clickStreamsSection();
            expect($location.search).toHaveBeenCalledWith('section', 'courses');
        });
    });

    function clickStreamsSection() {
        streamsSection().click();
    }

    function clickLessonsSection() {
        lessonsSection().click();
    }

    function clickPlaylistsSection() {
        playlistsSection().click();
    }

    function clickContentTopicsSection() {
        contentTopicsSection().click();
    }

    function expectLessonsActive() {
        expectOnlyOneSectionActive();
        SpecHelper.expectEqual(true, lessonsSection().get(0).classList.contains('active'), 'lessons section active');
        SpecHelper.expectElement(elem, 'list-lessons');
    }

    function expectStreamsActive() {
        expectOnlyOneSectionActive();
        SpecHelper.expectEqual(true, streamsSection().get(0).classList.contains('active'), 'streams section active');
        SpecHelper.expectEqual(1, elem.find('list-streams').length, 'edit streams exists');
    }

    function expectPlaylistsActive() {
        expectOnlyOneSectionActive();
        SpecHelper.expectEqual(
            true,
            playlistsSection().get(0).classList.contains('active'),
            'playlists Ssction active',
        );
        SpecHelper.expectEqual(1, elem.find('list-playlists').length, 'edit playlists exists');
    }

    function expectContentTopicsActive() {
        expectOnlyOneSectionActive();
        SpecHelper.expectEqual(
            true,
            contentTopicsSection().get(0).classList.contains('active'),
            'content topics section active',
        );
        SpecHelper.expectEqual(1, elem.find('list-content-topics').length, 'edit content topics exists');
    }

    function expectOnlyOneSectionActive() {
        SpecHelper.expectElements(elem, '.nav-item.active', 1);
    }

    function lessonsSection() {
        const section = elem.find('.nav-item').eq(0);
        SpecHelper.expectEqual('Lessons', section.text().trim(), 'lessons section text');
        return section;
    }

    function streamsSection() {
        const section = elem.find('.nav-item').eq(1);
        SpecHelper.expectEqual('Courses', section.text().trim(), 'streams section text');
        return section;
    }

    function playlistsSection() {
        const section = elem.find('.nav-item').eq(2);
        SpecHelper.expectEqual('Playlists', section.text().trim(), 'playlists section text');
        return section;
    }

    function contentTopicsSection() {
        const section = elem.find('.nav-item').eq(3);
        SpecHelper.expectEqual('Content Topics', section.text().trim(), 'content topics section text');
        return section;
    }

    function render() {
        const renderer = SpecHelper.render('<editor-index></editor-index>');
        elem = renderer.elem;
        scope = renderer.scope;
    }
});
