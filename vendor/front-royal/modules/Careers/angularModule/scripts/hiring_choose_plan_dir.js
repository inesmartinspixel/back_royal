import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/hiring_choose_plan.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import illustrationPostAJobDesktop from 'images/employers/illustration-post-a-job-desktop.png';
import onlyTheBestCandidatesIllustration from 'vectors/how_it_works/employer/only-the-best-candidates-illustration.svg';
import searchCandidateNetwork from 'images/employers-landing/search-candidate-network@2x.png';
import aidScholarship from 'vectors/executive-mba/aid-scholarship.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('hiringChoosePlan', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const HiringTeam = $injector.get('HiringTeam');

        return {
            scope: {},
            restrict: 'E',
            templateUrl,
            link(scope) {
                NavigationHelperMixin.onLink(scope);

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.$watchGroup(
                    ['currentUser.usingUnlimitedWithSourcingHiringPlan', 'currentUser.hiring_team.$$saving'],
                    () => {
                        if (scope.currentUser) {
                            scope.showPlans =
                                !scope.currentUser.usingUnlimitedWithSourcingHiringPlan ||
                                scope.currentUser.hiring_team.$$saving;
                        }
                    },
                );

                // This UI was built before we had official names for the plans.
                // Instead of going back and replacing `post` and `source` with the
                // official names in translate keys and everything, I'm keeping a separate
                // planMap and plans array, and ensuring that they agree.
                const planMap = {
                    post: HiringTeam.HIRING_PLAN_PAY_PER_POST,
                    source: HiringTeam.HIRING_PLAN_UNLIMITED_WITH_SOURCING,
                };
                scope.plans = ['post', 'source'];

                if (!_.isEqual(_.keys(planMap).sort(), scope.plans.sort())) {
                    throw new Error('planMap does not match plans');
                }

                scope.costForPlan = function (plan) {
                    if (!$rootScope.currentUser.hiring_team) {
                        return null;
                    }
                    const hiringPlan = planMap[plan];
                    const cents = scope.currentUser.hiring_team.stripePlanForHiringPlan(hiringPlan).amount;
                    return cents / 100;
                };

                scope.marketingSections = [
                    {
                        key: 'section_one',
                        image: illustrationPostAJobDesktop,
                        bullets: ['one', 'two', 'three'],
                    },
                    {
                        key: 'section_two',
                        image: onlyTheBestCandidatesIllustration,
                        bullets: ['one', 'two', 'three'],
                    },
                    {
                        key: 'section_three',
                        image: searchCandidateNetwork,
                        bullets: ['one', 'two', 'three'],
                    },
                    {
                        key: 'section_four',
                        image: aidScholarship,
                        bullets: ['one', 'two', 'three'],
                    },
                ];

                scope.selectPlan = plan =>
                    ({
                        post: () => {
                            scope.loadRoute('hiring/positions/?action=create');
                        },
                        source: () => {
                            const hiringTeam = scope.currentUser.hiring_team;
                            hiringTeam.hiring_plan = HiringTeam.HIRING_PLAN_UNLIMITED_WITH_SOURCING;
                            hiringTeam.save().then(() => {
                                // If the user is already accepted, then this will forward
                                // them to their new homepage.  If not, then they will stay here,
                                $rootScope.goHome();
                            });
                        },
                    }[plan]());
            },
        };
    },
]);
