import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_hiring_manager_profile.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('editHiringManagerProfile', [
    '$injector',
    function factory($injector) {
        const TranslationHelper = $injector.get('TranslationHelper');
        const $rootScope = $injector.get('$rootScope');
        const $q = $injector.get('$q');
        const scrollHelper = $injector.get('scrollHelper');
        const scopeTimeout = $injector.get('scopeTimeout');
        const $location = $injector.get('$location');
        const ClientStorage = $injector.get('ClientStorage');
        const HiringApplication = $injector.get('HiringApplication');

        return {
            restrict: 'E',
            scope: {
                stepNames: '<?', // Tests will pass this in
                user: '<?', // used in editor to override
            },
            templateUrl,
            link(scope) {
                const translationHelper = new TranslationHelper('careers.edit_hiring_manager_profile');

                //-----------------------------
                // Profile Data
                //-----------------------------

                scope.proxy = {};

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        if (scope.user) {
                            return scope.user;
                        }
                        return $rootScope.currentUser;
                    },
                });

                scope.$watch('currentUser', () => {
                    if (scope.currentUser && scope.currentUser.hiring_application) {
                        // Since scope.hiringApplication is a copy of the currentUser's hiring_application, it acts
                        // as a proxy, allowing us to easily throw away changes if the user dirties the form and then
                        // navigates away to another step without mucking up the hiring_application on the currentUser.
                        // NOTE: Since this $watch on the currentUser only triggers if the REFERENCE to currentUser changes,
                        // this line should theoretically only run once, which is when this directive first renders.
                        scope.hiringApplication = HiringApplication.new(scope.currentUser.hiring_application.asJson());
                    }
                });

                const settingsUrl = '/settings/profile';
                const hiringManagerEditorUrl = '/admin/careers/hiring_applications';
                scope.isHiringManagerEditor = $location.path().includes(hiringManagerEditorUrl);

                //-----------------------------
                // Multi-Step Form Config
                //-----------------------------

                scope.steps = $injector.get('HIRING_MANAGER_FORM_STEPS');
                scope.stepNames = _.chain(scope.stepNames || _.pluck(scope.steps, 'stepName'))
                    .reject(stepName => scope.isHiringManagerEditor && stepName === 'preview')
                    .value();
                scope.stepsInfo = _.map(scope.stepNames, section => {
                    // return a template that passes along necessary model info
                    // NOTE: all of these directives will use the shared scope
                    const directiveName = `${section.snakeCase('-')}-form`;

                    const templateStr = `<${directiveName}></${directiveName}>`;

                    return {
                        key: section,
                        template: templateStr,
                        title: translationHelper.get(section),
                        hasForm: true,
                    };
                });

                //-----------------------------
                // Step Navigation
                //-----------------------------

                function scrollToContentTop() {
                    // don't need to scroll to top in the editor
                    if (scope.isHiringManagerEditor) {
                        return;
                    }

                    scopeTimeout(scope, () => {
                        const mainBox = $('.main-box');
                        const isFixed = mainBox.css('position') === 'fixed';
                        scrollHelper.scrollToTop(false, isFixed ? mainBox : undefined);
                    });
                }

                // see also: form_helper.js::supportForm
                const formListener = scope.$on('formHelperCurrentForm', (evt, formController) => {
                    scope.currentForm = formController;
                });

                function updateLocationForIndex(index) {
                    if (scope.isHiringManagerEditor) {
                        $location.search('page', index + 1);
                    } else {
                        $location.url(`${settingsUrl}?page=${index + 1}`);
                        scrollToContentTop();
                    }
                }

                function gotoFormStep(index) {
                    scope.proxy.mobileNavExpanded = false;

                    if (scope.currentForm && scope.currentForm.$dirty) {
                        // throw away changes if moving away from a dirty form step
                        scope.hiringApplication = HiringApplication.new(scope.currentUser.hiring_application.asJson());
                        updateLocationForIndex(index);
                    } else {
                        updateLocationForIndex(index);
                    }
                }

                scope.$on('gotoFormStep', (ev, index) => {
                    gotoFormStep(index);
                });

                scope.$on('$destroy', () => {
                    formListener();
                });

                //-----------------------------
                // External Section-Navigation
                //-----------------------------

                // FIXME: expand logic once section sub-nav is available (https://trello.com/c/VYthcEB2)
                // var externalNavListener = $rootScope.$on('viewCareersProfileSection', function(ev, section) {
                // });
                // scope.$on('$destroy', externalNavListener);

                //-----------------------------
                // Form Submission
                //-----------------------------

                scope.save = () => {
                    scope.currentForm.$setSubmitted(true);

                    if (!scope.currentForm.$valid) {
                        return $q.reject('invalid');
                    }

                    ClientStorage.setItem(`saved_${scope.currentForm.$name}`, true);

                    return scope.hiringApplication.save().then(() => {
                        scope.currentForm.$setPristine();

                        // We still want scope.hiringApplication to act as a proxy (see the $watch on currentUser above),
                        // so we reset scope.currentUser.hiring_application to a new HiringApplication Iguana model, which
                        // ensures that scope.currentUser.hiring_application isn't a reference to scope.hiringApplication,
                        // thus maintaining it's behavior as a proxy.
                        scope.currentUser.hiring_application = HiringApplication.new(scope.hiringApplication.asJson());

                        // see admin_edit_hiring_application_dir.js and settings_dir.js for where we listen to this
                        scope.$emit('savedHiringManagerProfile');
                    });
                };

                scope.saveAndNext = nextStepCallback => {
                    // Save the user (or do nothing), then save the hiring manager profile, then reset
                    scope.save().then(() => {
                        if (scope.isHiringManagerEditor) {
                            let index = +$location.search().page;
                            updateLocationForIndex(index++);
                        } else if (nextStepCallback) {
                            nextStepCallback();
                            scrollToContentTop();
                        } else {
                            scope.loadRoute('/careers/card');
                        }
                    });
                };
            },
        };
    },
]);
