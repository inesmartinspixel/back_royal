import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/sourcing_upsell.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import searchCandidateNetwork from 'images/employers-landing/search-candidate-network@2x.png';
import sharedHiringRelationshipCheck from 'vectors/shared-hiring-relationship-check.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('sourcingUpsell', [
    '$injector',

    function factory($injector) {
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const $rootScope = $injector.get('$rootScope');
        const HiringTeamCheckoutHelper = $injector.get('Payments.HiringTeamCheckoutHelper');
        const HiringTeam = $injector.get('HiringTeam');
        const DialogModal = $injector.get('DialogModal');
        const TranslationHelper = $injector.get('TranslationHelper');

        return {
            scope: {},
            restrict: 'E',
            templateUrl,
            link(scope) {
                NavigationHelperMixin.onLink(scope);

                const translationHelper = new TranslationHelper('careers.sourcing_upsell');

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.$watch('currentUser.hiring_team', hiringTeam => {
                    if (!scope.currentUser) {
                        return;
                    }
                    scope.stripePlan = hiringTeam
                        ? hiringTeam.stripePlanForHiringPlan(HiringTeam.HIRING_PLAN_UNLIMITED_WITH_SOURCING)
                        : null;

                    // Since we've hardcoded the trial period in the copy, let's send a warning
                    // in case it is ever wrong
                    if (scope.stripePlan && scope.stripePlan.trial_period_days !== 14) {
                        $injector
                            .get('ErrorLogService')
                            .notifyInProd(
                                'Text on sourcing upsell page does not match trial_period_days on stripe plan.',
                            );
                    }

                    scope.checkoutHelper = new HiringTeamCheckoutHelper(scope.currentUser.hiring_team);
                });

                scope.getStartedImg = searchCandidateNetwork;
                scope.hiringPlanScreen =
                    scope.currentUser && scope.currentUser.usingUnlimitedWithSourcingHiringPlan ? 'pay' : 'start';

                scope.sourceCandidates = () => {
                    scope.hiringPlanScreen = 'pay';
                };

                scope.subscribe = () => {
                    const switchingFromPayPerPostPlan = scope.currentUser.usingPayPerPostHiringPlan;
                    scope.checkoutHelper.subscribeToUnlimitedWithSourcingPlan().then(() => {
                        if (switchingFromPayPerPostPlan) {
                            DialogModal.alert({
                                title: translationHelper.get('plan_updated'),
                                content: `
                                    <div class="sourcing-upsell-plan-upgraded">
                                        <img image-fade-in-on-load src="${sharedHiringRelationshipCheck}">
                                        <div translate-once="careers.sourcing_upsell.upgrade_success"></div>
                                    </div>
                                    `,
                                classes: ['medium-small'],
                            });
                        }
                    });
                };
            },
        };
    },
]);
