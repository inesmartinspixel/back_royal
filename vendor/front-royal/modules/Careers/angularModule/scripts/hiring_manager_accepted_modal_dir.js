import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/hiring_manager_accepted_modal.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('hiringManagerAcceptedModal', [
    '$injector',
    function factory($injector) {
        const DialogModal = $injector.get('DialogModal');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');

        return {
            restrict: 'E',
            scope: {
                message: '<',
                buttonText: '<',
            },
            templateUrl,

            link(scope) {
                NavigationHelperMixin.onLink(scope);

                scope.action = () => {
                    DialogModal.hideAlerts();
                    scope.loadRoute('/hiring/browse-candidates');
                };
            },
        };
    },
]);
