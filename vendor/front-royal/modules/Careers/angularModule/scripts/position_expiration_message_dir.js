import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/position_expiration_message.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('positionExpirationMessage', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const dateHelper = $injector.get('dateHelper');
        return {
            scope: {
                position: '<',
            },
            restrict: 'E',
            templateUrl,

            link(scope) {
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                // This can update on changes to the position or the subscription.  Rather than
                // watching a bunch of individual things, just checking these two things on each digest.  Should
                // be fast enough
                scope.$watch(() => {
                    scope.expirationMessageLocaleSuffix =
                        scope.currentUser && scope.currentUser.expirationMessageLocaleSuffix(scope.position);
                    scope.expirationDate =
                        scope.currentUser &&
                        dateHelper.formattedUserFacingMonthDayShort(
                            scope.currentUser.expirationCancellationOrRenewalDateForPosition(scope.position),
                            /*
                                We do not use the date threshold here, even though that could mean that a position
                                expires or gets renewed in the morning before someone comes into work and they
                                get annoyed.

                                We went back and forth on this and weren't totally sure.  The one good argument for it
                                is that if we did use the threshold, we might sometimes show that a position will expire
                                or renew yesterday, which would be strange.
                            */
                            false,
                        );
                });
            },
        };
    },
]);
