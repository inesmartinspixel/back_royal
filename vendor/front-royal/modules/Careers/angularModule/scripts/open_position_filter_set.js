import angularModule from 'Careers/angularModule/scripts/careers_module';
/*
    OpenPositionFilterSet is responsible for loading up batches of
    positions for a candidate, along with the corresponding relationships
    and interests, until no more are left.
*/
angularModule.factory('OpenPositionFilterSet', [
    '$injector',

    function factory($injector) {
        const SuperModel = $injector.get('SuperModel');
        const $rootScope = $injector.get('$rootScope');
        const OpenPosition = $injector.get('OpenPosition');
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
        const listOffsetHelper = $injector.get('listOffsetHelper');

        const OpenPositionFilterSet = SuperModel.subclass(function () {
            Object.defineProperty(this.prototype, 'any', {
                get() {
                    return this._initialResultsLoaded ? _.any(this.results) : false;
                },
            });

            Object.defineProperty(this.prototype, 'noAdditionalResults', {
                get() {
                    return this.noMoreAvailable && !this.any;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'searchingForMoreWhenEmpty', {
                get() {
                    return (!!this._loadingPromise || !this._initialResultsLoaded) && !this.any;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'searchingForMore', {
                get() {
                    return !!this._loadingPromise;
                },
            });

            Object.defineProperty(this.prototype, 'hasAttemptedToFetchInitialResults', {
                get() {
                    return this.searchingForMore || this._initialResultsLoaded;
                },
            });

            Object.defineProperty(this.prototype, 'noMoreAvailable', {
                get() {
                    return !!this._allDataLoaded;
                },
            });

            return {
                initialize(filters, options = {}) {
                    this.sourceFilters = filters; // this is just used in specs.  Probably kind of a silly way to do it
                    this.careersNetworkViewModel = CareersNetworkViewModel.get('candidate');
                    this.filters = angular.copy(filters);
                    this.offset = options.offset || 0;

                    // minLength acts as a threshold to help determine when an API call should be made to retrieve more profiles.
                    // Should the curent number of profiles dips below the value of minLength, more profiles will be retrieved, if necessary.
                    this.minLength = options.minLength || 5;

                    // lets the server know the max number of profiles it should return
                    this.serverLimit = options.serverLimit || 10;

                    this.results = [];
                    this._initialResultsLoaded = false;
                },

                cloneFilters() {
                    return angular.copy(this.filters);
                },

                setOffset(offset, action, listLimit) {
                    this.offset = listOffsetHelper.setOffset(offset, action, listLimit);
                },

                // resets the offset value to 0 or increments/decrements the offset value using the listLimit based on
                // the action provided and then attempts to make an API call to retrieve the profiles if necessary
                getResultsRelativeToOffset(offset, action, listLimit) {
                    this.setOffset(offset, action, listLimit);
                    return this.ensureResultsPreloaded();
                },

                // Users of this FilterSet are expected to remove things from the collection.
                // If the collection gets shorter than the minLength, then filterSet will
                // make another request to make sure it has enough.
                ensureResultsPreloaded() {
                    const self = this;

                    if (self._loadingPromise) {
                        return self._loadingPromise;
                    }

                    if (self.results.length - self.offset < self.minLength && !self._allDataLoaded) {
                        self.maxTotalCount = 100;

                        self._loadingPromise = OpenPosition.index({
                            filters: this._buildFiltersForApiRequest(),
                            limit: self.serverLimit,
                            max_total_count: self.maxTotalCount,
                            offset: self.results.length,
                            candidate_id: $rootScope.currentUser.id,
                            sort: 'RECOMMENDED_FOR_CANDIDATE',
                        }).then(response => {
                            // We don't want to decrement the initialTotalCount since we load more results under
                            // the hood, so only cache total_count when loading the initial results
                            if (response.meta && !self._initialResultsLoaded) {
                                self.initialTotalCount = response.meta.total_count;
                                self.initialTotalCountIsMin = self.initialTotalCount === self.maxTotalCount;

                                // totalCount can be mutated by the client
                                self.totalCount = response.meta.total_count;
                            }

                            self._initialResultsLoaded = true;
                            self._loadingPromise = null;
                            self.results = self.results.concat(response.result);

                            if (response.result.length < self.serverLimit) {
                                self._allDataLoaded = true;
                            }

                            // Calling ensureResultsPreloaded again after a request completes
                            // allows us to catch up if the offset has moved ahead while we were loading.
                            // The UI should prevent the user from banging on the next button anyhow, but
                            // if not, this makes sure we eventually load what we need to.
                            self.ensureResultsPreloaded();
                        });
                    }

                    return self._loadingPromise;
                },

                _buildFiltersForApiRequest() {
                    const filters = _.extend({}, this.filters, {
                        featured: true,
                        archived: false,
                        hiring_manager_might_be_interested_in: $rootScope.currentUser.id,
                        candidate_has_acted_on: false,
                    });

                    return filters;
                },
            };
        });

        return OpenPositionFilterSet;
    },
]);
