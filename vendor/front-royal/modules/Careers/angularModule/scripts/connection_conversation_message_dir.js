import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/connection_conversation_message.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('connectionConversationMessage', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const TranslationHelper = $injector.get('TranslationHelper');
        const OpenPosition = $injector.get('OpenPosition');
        const $filter = $injector.get('$filter');

        return {
            scope: {
                message: '<',
                hiringRelationshipViewModel: '<',
            },
            restrict: 'E',
            templateUrl,

            link(scope, elem) {
                const translationHelper = new TranslationHelper('careers.connection_conversation');

                scope.openPosition = scope.message.metadata && OpenPosition.new(scope.message.metadata.openPosition);

                scope.$watch('hiringRelationshipViewModel.conversationViewModel', conversationViewModel => {
                    scope.conversationViewModel = conversationViewModel;
                });

                if (scope.openPosition) {
                    scope.salary =
                        $filter('number')(scope.openPosition.salary) ||
                        ` ${translationHelper.get('open_to_discussion')}`;
                    scope.company = scope.message.metadata && scope.message.metadata.senderCompanyName;
                }

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'receiptMessage', {
                    get() {
                        const displayName = scope.hiringRelationshipViewModel.theirDisplayNameForConversation;

                        if (scope.hiringRelationshipViewModel.messageIsUnreadByAllOfThem(scope.message)) {
                            return translationHelper.get('receipt_unread', {
                                name: displayName,
                            });
                        }
                        return translationHelper.get('receipt_read', {
                            name: displayName,
                        });
                    },
                });

                if (scope.message.sender_id === scope.currentUser.id) {
                    $(elem).addClass('sent-by-me');
                }
            },
        };
    },
]);
