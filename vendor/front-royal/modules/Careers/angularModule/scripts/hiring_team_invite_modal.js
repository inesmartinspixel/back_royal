import angularModule from 'Careers/angularModule/scripts/careers_module';

angularModule.factory('HiringTeamInviteModal', [
    '$injector',
    $injector => {
        const DialogModal = $injector.get('DialogModal');
        const TranslationHelper = $injector.get('TranslationHelper');

        return {
            open() {
                const self = this;
                DialogModal.alert({
                    title: new TranslationHelper('careers.invite_teammate').get('invite_coworker'),
                    content: '<invite-teammate-form on-finish="onFinish"></invite-teammate-form>',
                    classes: ['team-inviate-modal'],
                    blurTargetSelector: 'div[ng-controller]',
                    scope: {
                        onFinish() {
                            self.close();
                        },
                    },
                });
            },

            close() {
                DialogModal.hideAlerts();
            },
        };
    },
]);
