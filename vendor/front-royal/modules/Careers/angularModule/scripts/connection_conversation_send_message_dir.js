import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/connection_conversation_send_message.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('connectionConversationSendMessage', [
    '$injector',

    function factory($injector) {
        return {
            scope: {
                hiringRelationshipViewModel: '<',
            },
            restrict: 'E',
            templateUrl,

            link(scope) {
                scope.$rootScope = $injector.get('$rootScope');

                Object.defineProperty(scope, 'placeholderLocale', {
                    get() {
                        let placeholder = 'careers.connection_conversation.send_a_message_to_x';

                        // customize message if you haven't sent a message yet
                        if (
                            scope.hiringRelationshipViewModel &&
                            !scope.hiringRelationshipViewModel.hasMessageFromUs &&
                            !scope.hiringRelationshipViewModel.hiringRelationship.open_position_id
                        ) {
                            placeholder +=
                                scope.hiringRelationshipViewModel.role === 'candidate'
                                    ? '_candidate'
                                    : '_hiring_manager';
                        } else if (
                            scope.hiringRelationshipViewModel &&
                            !scope.hiringRelationshipViewModel.hasMessageFromUs &&
                            scope.hiringRelationshipViewModel.hiringRelationship.open_position_id &&
                            scope.hiringRelationshipViewModel.role === 'candidate'
                        ) {
                            placeholder += '_candidate_with_position';
                        }

                        return placeholder;
                    },
                });

                scope.$watch('$rootScope.currentUser', currentUser => {
                    scope.currentUser = currentUser;
                });
            },
        };
    },
]);
