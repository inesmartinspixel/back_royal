import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/candidate_list.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('candidateList', [
    '$injector',

    function factory($injector) {
        const HiringRelationshipViewModel = $injector.get('HiringRelationshipViewModel');
        const CareerProfile = $injector.get('CareerProfile');
        const CandidatePositionInterest = $injector.get('CandidatePositionInterest');
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
        const $location = $injector.get('$location');
        const $rootScope = $injector.get('$rootScope');
        const $filter = $injector.get('$filter');

        return {
            scope: {
                // ngModel can be an array of one of three types: HiringRelationshipViewModel, CareerProfile, or CandidatePositionInterest
                ngModel: '=', // sends back up modified object
                listLimit: '<',
                offset: '<',
                noMoreAvailable: '<',
                getProfiles: '&',
                afterItemRemoval: '&?',
                showHideButton: '<?',
                unsupportedDeepLink: '<?',
                unsupportedDeepLinkKey: '<?',
                candidatePositionInterests: '<?',
                showPagination: '<?',
                showCompactCards: '<?',
                showNewStatus: '<?',
                adminMode: '<?',
                showFreemiumLimiting: '<?',
                totalCount: '<',
                showActions: '<?',
                showingProfiles: '=?', // two-way bound so parent can read it
            },
            restrict: 'E',
            templateUrl,
            link(scope) {
                // This directive can be rendered during a logout, which will
                // lead to an error when instantiating a CareersNetworkViewModel
                // without a currentUser
                if ($rootScope.currentUser) {
                    scope.careersNetworkViewModel = CareersNetworkViewModel.get('hiringManager');
                }
                scope.showHideButton = angular.isDefined(scope.showHideButton) ? scope.showHideButton : true;
                scope.showPagination = angular.isDefined(scope.showPagination) ? scope.showPagination : true;
                scope.showNewStatus = angular.isDefined(scope.showNewStatus) ? scope.showNewStatus : true;
                scope.showActions = angular.isDefined(scope.showActions) ? scope.showActions : true;

                Object.defineProperty(scope, 'sharedByEmail', {
                    get() {
                        return $location.search().sharedByEmail;
                    },
                });

                scope.$watch('showFreemiumLimiting', showFreemiumLimiting => {
                    scope.blurAfterFirstNCandidates = showFreemiumLimiting ? 3 : null;
                });

                // FIXME: If we are showing a CandidatePositionInterest list then ensure that we sort curated positions
                // first (represented by adminStatusPriority), followed by hiring_manager_priority then created_at DESC.
                // The server sends them to us in this order, but we are messing with that ordering when creating the different
                // interestGroup buckets in the directive above. We should ideally fix this so we don't duplicate this ordering
                // logic, but that much of a refactor felt out of scope for the interest curation changes.
                if (scope.ngModel && scope.ngModel.length > 0) {
                    if (scope.ngModel[0].isA(CandidatePositionInterest)) {
                        scope.candidatePositionInterests = scope.ngModel;
                        scope.orderBy = [
                            'anonymizedPriority',
                            'adminStatusPriority',
                            'hiring_manager_priority',
                            '-created_at',
                        ];
                    } else if (scope.ngModel[0].isA(HiringRelationshipViewModel)) {
                        // If ngModel is a collection of hiring relationship view-models, ensure that deep linked
                        // profiles are first in the list. The ordering of the other profiles should remain the same.
                        scope.orderBy = '_deepLinked';
                    }
                }

                // if the user performs enough actions on the last page so that no more profiles are visible,
                // decrement the offset to automatically navigate the user back a page
                scope.$watch(
                    () => scope.offset === scope.ngModel && scope.ngModel.length,
                    val => {
                        // only get the previous page of profiles if there are any
                        if (val && scope.ngModel && scope.ngModel.length > 0) {
                            scope.getProfiles({
                                action: 'previous',
                            });
                        }
                    },
                );

                function setLimitedModels() {
                    scope.limitedModels =
                        scope.ngModel &&
                        $filter('orderBy')(scope.ngModel, scope.orderBy).slice(
                            scope.offset,
                            scope.offset + scope.listLimit,
                        );
                    scope.showingProfiles = scope.limitedModels && scope.limitedModels.length > 0;
                }

                scope.$watchCollection('ngModel', setLimitedModels);
                scope.$watchCollection('orderBy', setLimitedModels);
                scope.$watchGroup(['listLimit', 'offset'], setLimitedModels);

                // if item is a hiring relationship view model, return it's carrer profile
                scope.getCareerProfile = item => {
                    if (item.isA(HiringRelationshipViewModel)) {
                        return item.hiringRelationship.career_profile;
                    }
                    if (item.isA(CareerProfile)) {
                        return item;
                    }
                    if (item.isA(CandidatePositionInterest)) {
                        return item.career_profile;
                    }
                    throw 'Unsupported ngModel class';
                };

                // passed down to candidate-list-actions
                scope.removeItem = item => {
                    const index = scope.ngModel.indexOf(item);

                    // it's possible this item has already been removed, e.g.: position_review_dir.js could have rebuilt
                    // the interestsGroups due to the candidatePositionInterestUpdate event, in which case the ngModel won't
                    // have the position to remove anymore.
                    if (index >= 0) {
                        scope.ngModel.splice(index, 1);
                    }

                    if (scope.afterItemRemoval) {
                        scope.afterItemRemoval({
                            item,
                        });
                    }
                };

                scope.getCandidatePositionInterest = careerProfile => {
                    if (!scope.candidatePositionInterests) {
                        return;
                    }

                    return _.find(
                        scope.candidatePositionInterests,
                        interest => interest.career_profile.id === careerProfile.id,
                    );
                };

                scope.internalGetProfiles = (offset, action, listLimit) => {
                    scope.getProfiles({
                        offset,
                        action,
                        listLimit,
                    });
                };

                scope.blurCard = index => scope.blurAfterFirstNCandidates && index >= scope.blurAfterFirstNCandidates;
            },
        };
    },
]);
