import angularModule from 'Careers/angularModule/scripts/careers_module';
import { setupBrandNameProperties } from 'AppBrandMixin';
import template from 'Careers/angularModule/views/preview_profile_status_message.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('previewProfileStatusMessage', [
    '$injector',
    function factory($injector) {
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const CareerProfile = $injector.get('CareerProfile');
        const $rootScope = $injector.get('$rootScope');
        const Cohort = $injector.get('Cohort');

        return {
            restrict: 'E',
            scope: {
                // Certain locales make reference to the blue dots displayed in the progress-nav,
                // but if the progress-nav isn't being shown in the UI, then we don't want to use
                // those locales, so this directive needs to know if the progress-nav is being shown
                // in the UI so that it can display the appropriate locale to the user.
                withProgressNav: '<?',
            },
            templateUrl,

            link(scope) {
                NavigationHelperMixin.onLink(scope);
                setupBrandNameProperties($injector, scope);

                // convenience getter
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.withProgressNav = scope.withProgressNav || false;

                // need to be responsive to push messages (i.e.: if we're accepted, rejected, etc. by an admin)
                // and changes to the status fields
                scope.$watchGroup(
                    [
                        'currentUser.can_edit_career_profile',
                        'currentUser.career_profile.interested_in_joining_new_company',
                        'currentUser.isRejectedOrExpelled',
                        'currentUser.career_profile.last_calculated_complete_percentage',
                        'currentUser.isCareerNetworkOnly',
                        'currentUser.lastCohortApplicationStatus',
                        'currentUser.programType',
                        'currentUser.isPending',
                    ],
                    (newVals, oldVals) => {
                        // be defensive
                        if (!scope.currentUser || !scope.currentUser.career_profile) {
                            return;
                        }

                        // if they have access to the career network...
                        if (scope.currentUser.can_edit_career_profile) {
                            // ...and their profile is complete...
                            if (scope.currentUser.career_profile.last_calculated_complete_percentage === 100) {
                                // ...and their profile is set to active...
                                if (
                                    _.contains(
                                        CareerProfile.ACTIVE_INTEREST_LEVELS,
                                        scope.currentUser.career_profile.interested_in_joining_new_company,
                                    )
                                ) {
                                    scope.status = 'active';

                                    // ...and their profile is not set to active
                                } else {
                                    scope.status = 'inactive';
                                }

                                // ...and their profile is not complete...
                                // ..and pre-accepted/accepted to a degree program
                            } else if (
                                Cohort.isDegreeProgram(scope.currentUser.programType) &&
                                _.contains(['accepted', 'pre_accepted'], scope.currentUser.lastCohortApplicationStatus)
                            ) {
                                if (scope.withProgressNav) {
                                    scope.status = 'accepted_degree_incomplete_profile';
                                } else {
                                    scope.status = 'accepted_degree_incomplete_profile_alt';
                                }
                            } else {
                                scope.status = 'incomplete';
                            }
                            // or, if they don't have access to the career network...
                            // ...then they're either already accepted or rejected to a career_network_only cohort
                        } else if (scope.currentUser.isCareerNetworkOnly) {
                            scope.status = 'no_cn_access_cno';
                            // ...or rejected to a learning program
                        } else if (scope.currentUser.isRejectedOrExpelled) {
                            scope.status = 'no_cn_access';
                            // ...or accepted to a cohort but don't have access to the career network
                        } else if (
                            _.contains(['accepted', 'pre_accepted'], scope.currentUser.lastCohortApplicationStatus)
                        ) {
                            if (Cohort.isDegreeProgram(scope.currentUser.programType)) {
                                if (scope.currentUser.career_profile.last_calculated_complete_percentage !== 100) {
                                    if (scope.withProgressNav) {
                                        scope.status = 'accepted_degree_incomplete_profile';
                                    } else {
                                        scope.status = 'accepted_degree_incomplete_profile_alt';
                                    }
                                } else {
                                    scope.status = 'reviewing_degree';
                                }
                            } else {
                                scope.status = 'reviewing_non_degree';
                            }
                            // ...or waiting for someone to review their profile
                        } else if (scope.currentUser.lastCohortApplicationStatus === 'deferred') {
                            scope.status = 'deferred';
                        } else if (scope.currentUser.isPending) {
                            scope.status = 'applied';
                        } else {
                            const cu = scope.currentUser;
                            $injector
                                .get('ErrorLogService')
                                .notify('Unexpected situation in determining profile preview status', undefined, {
                                    // watch vals
                                    watchOldVals: oldVals,
                                    watchNewVals: newVals,

                                    // programType debugging
                                    programType: cu.programType,
                                    pendingAcceptedOrPreAcceptedCohortApplicationProgramType: cu.pendingAcceptedOrPreAcceptedCohortApplication
                                        ? cu.pendingAcceptedOrPreAcceptedCohortApplication.program_type
                                        : undefined,
                                    pendingProgramTypeSelected: cu.$$pendingProgramTypeSelected,
                                });
                            scope.status = 'no_cn_access';
                        }
                    },
                );
            },
        };
    },
]);
