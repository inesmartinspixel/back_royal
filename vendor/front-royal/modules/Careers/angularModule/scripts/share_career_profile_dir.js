import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/share_career_profile.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import sharedHiringRelationshipCheck from 'vectors/shared-hiring-relationship-check.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('shareCareerProfile', [
    '$injector',
    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const $timeout = $injector.get('$timeout');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                careerProfile: '<',
                onCloseCallback: '&',
            },
            link(scope) {
                scope.sharedHiringRelationshipCheck = sharedHiringRelationshipCheck;

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'currentUserHasAtLeastOneHiringTeammate', {
                    get() {
                        return !!(
                            $rootScope.currentUser &&
                            $rootScope.currentUser.hiring_teammate_emails &&
                            $rootScope.currentUser.hiring_teammate_emails.length > 0
                        );
                    },
                });

                scope.formProxy = {
                    targetEmail: undefined,
                    message: undefined,
                    emailOptions: _.chain(scope.currentUser.hiring_teammate_emails)
                        .map(email => ({
                            label: email,
                            value: email,
                        }))
                        .sortBy('label')
                        .value(),
                };
                scope.form_errors = {};

                scope.share = () => {
                    if (scope.shareCandidateForm.$valid && !scope.formSubmitting) {
                        scope.formSubmitting = true;

                        const $window = $injector.get('$window');
                        const $http = $injector.get('$http');

                        $http
                            .get(`${$window.ENDPOINT_ROOT}/api/share_career_profile.json`, {
                                params: {
                                    record: {
                                        id: scope.careerProfile.id,
                                        email: scope.formProxy.targetEmail,
                                        message: scope.formProxy.message,
                                    },
                                },
                                // using the JQLike serializer prevents encoding errors on ';' (and other chars?)
                                paramSerializer: '$httpParamSerializerJQLike',
                            })
                            .then(() => {
                                scope.submitted = true;
                            })
                            .then(() => $timeout(1750))
                            .then(scope.onCloseCallback);
                    }
                };

                scope.openHiringTeamInviteModal = () => {
                    scope.onCloseCallback({
                        openHiringTeamInviteModal: true,
                    });
                };
            },
        };
    },
]);
