import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/candidate_connections.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('candidateConnections', [
    '$injector',

    function factory($injector) {
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const $location = $injector.get('$location');
        const TranslationHelper = $injector.get('TranslationHelper');

        return {
            scope: {},
            restrict: 'E',
            templateUrl,

            link(scope) {
                scope.$location = $location;
                const translationHelper = new TranslationHelper('careers.careers');

                scope.$watch('$location.search().connectionId', connectionId => {
                    if (!connectionId) {
                        AppHeaderViewModel.setTitleHTML(translationHelper.get('careers_title'));
                    }
                    // otherwise, we defer to connection_conversation_dir.js to set the title
                });
            },
        };
    },
]);
