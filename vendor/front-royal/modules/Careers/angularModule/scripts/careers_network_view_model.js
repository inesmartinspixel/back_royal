import angularModule from 'Careers/angularModule/scripts/careers_module';
import casperMode from 'casperMode';

angularModule.factory('CareersNetworkViewModel', [
    '$injector',
    $injector => {
        const SuperModel = $injector.get('SuperModel');
        const $rootScope = $injector.get('$rootScope');
        const HiringRelationship = $injector.get('HiringRelationship');
        const HiringRelationshipViewModel = $injector.get('HiringRelationshipViewModel');
        const $q = $injector.get('$q');
        const DialogModal = $injector.get('DialogModal');
        const TranslationHelper = $injector.get('TranslationHelper');
        const ClientStorage = $injector.get('ClientStorage');
        const OpenPosition = $injector.get('OpenPosition');
        const $timeout = $injector.get('$timeout');
        const HiringApplication = $injector.get('HiringApplication');
        const CandidatePositionInterest = $injector.get('CandidatePositionInterest');
        const $location = $injector.get('$location');
        const $interval = $injector.get('$interval');
        const EventLogger = $injector.get('EventLogger');

        EventLogger.allowEmptyLabel('hiring_manager:saw_hiring_billing_modal');

        const CareersNetworkViewModel = SuperModel.subclass(function () {
            this.extend({
                instances: {},

                /*
                    Just in case someone was BOTH a hiring manager and a candidate,
                    this would create two separate view models for them, one for use
                    on hiring manager pages, and one for use on candidate pages.
                */
                get(role) {
                    if (!this.instances[role]) {
                        if (!_.contains(['hiringManager', 'candidate'], role)) {
                            throw new Error('Unexpected role');
                        }
                        this.instances[role] = new CareersNetworkViewModel(role);
                    }
                    return this.instances[role];
                },

                removeInstance(instance) {
                    let role;
                    _.each(this.instances, (_instance, _role) => {
                        if (_instance === instance) {
                            role = _role;
                        }
                    });

                    delete this.instances[role];
                },
            });

            function destroy() {
                _.each(CareersNetworkViewModel.instances, viewModel => {
                    viewModel.destroy();
                });
                CareersNetworkViewModel.instances = {};
            }

            // If you switch teams, we just need to totally reset the cache
            function resetCache() {
                _.invoke(CareersNetworkViewModel.instances, 'resetCache');
            }

            function onHiringRelationshipUpdate() {
                // when changes get pushed down, some hiring relationships may have
                // become hidden (i.e. if a teammate accepted one of your pending relationships)
                _.invoke(CareersNetworkViewModel.instances, 'removeHiddenRelationships');
                _.invoke(CareersNetworkViewModel.instances, 'calculateNumNotifications');
            }

            $rootScope.$watch('currentUser', destroy);
            $rootScope.$watchGroup(
                [
                    'currentUser.hiring_team_id',
                    'currentUser.hiring_team.has_full_access',
                    'currentUser.hasCareersNetworkAccess',
                ],
                resetCache,
            );
            $rootScope.$on('$destroy', destroy);
            $rootScope.$on('hiringRelationshipUpdate', onHiringRelationshipUpdate);

            return {
                initialize(role, options) {
                    const self = this;
                    options = options || {};
                    this.preload = options.preload !== false;
                    this.user = $rootScope.currentUser;
                    this.translationHelper = new TranslationHelper('careers.careers_network_view_model');
                    if (!this.user) {
                        throw new Error('No current user when initializing CareersNetworkViewModel.');
                    }
                    this.role = role; // hiringManager or candidate
                    this.featuredPositionsUrl = '/hiring/positions';
                    this.resetCache();
                    this._setupWatchers();
                    if (this.preload) {
                        this._setupIntervals();
                    }

                    this._relationshipChangeDetector = new HiringRelationship.UnloadedChangeDetector().onChangeDetected(
                        alreadyUpdatedTo => {
                            self.onUnloadedHiringRelationshipChange(alreadyUpdatedTo);
                        },
                    );

                    this._openPositionChangeDetector = new OpenPosition.UnloadedChangeDetector().onChangeDetected(
                        () => {
                            self._reloadOpenPositions();
                        },
                    );

                    this._candidatePositionInterestChangeDetector = new CandidatePositionInterest.UnloadedChangeDetector().onChangeDetected(
                        () => {
                            self._reloadCandidatePositionInterests();
                        },
                    );
                },

                resetCache() {
                    const self = this;

                    self.hiringRelationships = [];
                    self.hiringRelationshipViewModels = [];
                    self._hiringRelationshipViewModelsByConnectionId = {};

                    // _dependentHiringRelationshipListLoadPromises contains the load promises for specifically
                    // requested lists that are required to finish in order to get the available actions for the
                    // current list of connections that may be displayed in the UI. In contrast,
                    // _independentHiringRelationshipListLoadPromises contains the load promises for specifically
                    // requested lists that are NOT required to finish in order to get the available actions for
                    // the hiring relationships. See ensureHiringRelationshipsLoaded.
                    self._dependentHiringRelationshipListLoadPromises = {};
                    self._independentHiringRelationshipListLoadPromises = {};

                    self._openPositionsPromise = null;
                    self.openPositions = null;

                    self._candidatePositionInterestPromise = null;
                    self.candidatePositionInterests = null;

                    // Give other stuff time to do it's thing before we start preloading,
                    // so that the initial page can get on the screen as soon as possible.
                    // `preload` is always true in the wild, but generally false in specs in
                    // order to simplify things
                    $timeout.cancel(this._preloadTimeout);

                    // We do not make the request for "candidates" who have no careers
                    // access.  This is not critical, but the request is unnecessary.
                    if (self.preload && (self.user.hasCareersNetworkAccess || self.role === 'hiringManager')) {
                        const delay = casperMode() ? 0 : 10000;
                        this._preloadTimeout = $timeout(() => {
                            self.setFeaturedPositionsNumNotifications();
                            self._preloadHiringRelationships();
                        }, delay);
                    }
                },

                /*
                    Loads the hiring relationships for the specific list requested via the listName.
                    @param listName - The list that the loaded hiring relationships are for.
                    @param independent - Since the available actions for a list of connections are only determined after all
                        _dependentHiringRelationshipListLoadPromises have finished, the current list of connections in the UI may have finished loading,
                        but the available actions may not have rendered because another list is being loaded in the background. If that's the case, then
                        the list that's loading in the background is independent, i.e. we don't need to wait for it to finish in order for the available
                        actions to be decided for the currently displayed list. This param acts as a flag to determine if the promise to load up the hiring
                        relationships for the requested list should be added to a list of promises that are required to finish (dependent) in order to
                        determine what actions should be made available for the current list of connections that may be displayed in the UI or if the promise
                        should be added to a list of promises that are NOT required to finish (independent) in order to determine what actions should be
                        made available to the current list of connections displayed in the UI. By default, hiring relationship list load promises are dependent.
                */
                ensureHiringRelationshipsLoaded(listName, independent) {
                    /*
                        Each listName maps to some filters to send up in the index call.

                        See _loadHiringRelationshipsForListName for all the supported values of listName

                        Notes on caching of hiring relationships:  We keep objects of
                        _dependentHiringRelationshipListLoadPromises and
                        _independentHiringRelationshipListLoadPromises, whose keys are the listNames
                        passed in here. When there is a a resolved promise in that object, it indicates
                        that we have already loaded up all hiring relationships for that list.

                        We monitor for changes to lastRelationshipUpdatedAt. If it
                        changes due to activity outside of this client, then we load up any recent
                        updates, which will once again make it reliable that anything that is in
                        _dependentHiringRelationshipListLoadPromises and
                        _independentHiringRelationshipListLoadPromises is updated and ready to go.
                    */

                    if (!listName) {
                        throw new Error('Must pass in a listName');
                    }

                    // We should not try to load relationships for a hiring manager that does
                    // not have a team (which can only be unaccepted hiring managers).
                    if (this.role === 'hiringManager' && !this.user.hiring_team_id) {
                        return $q.when();
                    }

                    // If we don't already have a promise for this listName, then send out a request
                    let promise = this._getExistingHiringRelationshipListLoadPromiseForListName(listName);
                    if (!promise) {
                        // The list that is returned from the initial request
                        // is not meaningful, since it can change due to changes
                        // in the client (i.e. a relationship can move from featured
                        // to myCandidates).  So just return true to indicate that things are all
                        // done loading.
                        promise = this._loadHiringRelationshipsForListName(listName).then(() => true);

                        const promiseMap = independent
                            ? '_independentHiringRelationshipListLoadPromises'
                            : '_dependentHiringRelationshipListLoadPromises';
                        this[promiseMap][listName] = promise;
                    }

                    return promise;
                },

                getMyHiringRelationshipViewModelForConnectionId(connectionId, checkApiIfNotFound) {
                    const self = this;
                    return this._getHiringRelationshipViewModelsForConnectionIdAndProperty(
                        connectionId,
                        'isMyRelationship',
                        () => {
                            // If there is no relationship already cached, go look for one over the api
                            if (checkApiIfNotFound) {
                                return self._loadHiringRelationships({
                                    CONNECTION_ID: connectionId,
                                });
                            }
                        },
                    );
                },

                // This checks for the one relationship either for me or my team where hiring_manager_status = accepted
                getHiringRelationshipViewModelAcceptedByTeam(connectionId, checkApiIfNotFound) {
                    const self = this;
                    // Since you can only have a conversation when hiring_manager_status=accepted, and two people
                    // from the same team cannot both have hiring_manager_status=accepted for the same connection,
                    // we can be sure that there is only one relationship at most to be returned from this method.
                    return this._getHiringRelationshipViewModelsForConnectionIdAndProperty(
                        connectionId,
                        'acceptedByHiringManager',
                        () => {
                            // If there is no relationship already cached, go look for one over the api
                            if (checkApiIfNotFound) {
                                return self._loadHiringRelationships({
                                    CONNECTION_ID: connectionId,
                                });
                            }
                        },
                    );
                },

                // This initializes a hiringRelationshipViewModel, but does not save it over the api
                // or add it to the list of hiring relationships on the CareersNetworkViewModel.  When and if
                // it is saved, it will be automatically added to the CareersNetworkViewModel.
                getOrInitializeHiringRelationshipViewModelForCareerProfile(careerProfile, opts) {
                    if (this.role !== 'hiringManager') {
                        throw new Error('This is only for hiring managers.');
                    }
                    opts = opts || {};
                    const foundWithSearchId = opts.foundWithSearchId;
                    const checkApiIfNotFound = !!opts.checkApiIfNotFound;
                    const self = this;
                    return self
                        .getMyHiringRelationshipViewModelForConnectionId(careerProfile.user_id, checkApiIfNotFound)
                        .then(hiringRelationshipViewModel => {
                            if (!hiringRelationshipViewModel) {
                                hiringRelationshipViewModel = self._initializeHiringRelationshipViewModel(
                                    careerProfile,
                                    foundWithSearchId,
                                );
                            }
                            return hiringRelationshipViewModel;
                        });
                },

                calculateNumNotifications() {
                    const self = this;

                    this.connectionsNumNotifications = _.chain(this.hiringRelationshipViewModels)
                        .reject('closed')
                        .select(vm => {
                            // If the user is a hiring manager and on a team, we need to account
                            // for unread messages within "dropped-in" conversations, i.e. the
                            // user is participating in a conversation within a teammate's hiring
                            // relationship
                            // see https://trello.com/c/d4Avmm9A

                            // Add hiring relationships with unread messages to the notification count
                            if (vm.hasUnread) {
                                return true;
                            }

                            // Add new matches for hiring managers, or new requests for candidates
                            // to the notification count
                            if (vm.newMatch || vm.newRequest) {
                                return true;
                            }

                            // add a notification for candidates if they have not yet sent a message in a
                            // conversation
                            if (self.role === 'candidate') {
                                return vm.matched && !vm.hasMessageFromUs;
                            }
                        })
                        .size()
                        .value();
                },

                destroy() {
                    _.each(this.watchCancelers, cancel => {
                        cancel();
                    });
                    $timeout.cancel(this._preloadTimeout);
                    $interval.cancel(this._resetInterval);
                    this._relationshipChangeDetector.destroy();
                    this._openPositionChangeDetector.destroy();
                    this._candidatePositionInterestChangeDetector.destroy();
                    CareersNetworkViewModel.removeInstance(this);
                },

                // This method is written with the assumption that the relationships (yours or teammates')
                // needed to properly determine which actions to show are already loaded.
                getAvailableActionsWithEverythingPreloaded(careerProfile, options) {
                    const self = this;
                    options = options || {};
                    const connectionId = careerProfile.user_id;

                    const teammateHasAccepted = !!_.findWhere(
                        self._hiringRelationshipViewModelsByConnectionId[connectionId],
                        {
                            ourStatus: 'accepted',
                            role: 'hiringTeammate',
                        },
                    );

                    return self
                        .getMyHiringRelationshipViewModelForConnectionId(connectionId)
                        .then(myHiringRelationshipViewModel =>
                            self._getAvailableActionsForProfile(
                                myHiringRelationshipViewModel,
                                teammateHasAccepted,
                                options.viewingTeammateRelationship,
                            ),
                        );
                },

                getAvailableActionsForReviewedPosition(careerProfile, options) {
                    const self = this;
                    options = options || {};
                    const connectionId = careerProfile.user_id;
                    let teammateHasAccepted;

                    return self
                        .getHiringRelationshipViewModelAcceptedByTeam(connectionId, true)
                        .then(acceptedHiringRelationshipViewModel => {
                            if (
                                acceptedHiringRelationshipViewModel &&
                                !acceptedHiringRelationshipViewModel.isMyRelationship
                            ) {
                                teammateHasAccepted = true;
                            }

                            if (
                                !acceptedHiringRelationshipViewModel ||
                                !acceptedHiringRelationshipViewModel.isMyRelationship
                            ) {
                                return self.getMyHiringRelationshipViewModelForConnectionId(connectionId, true);
                            }
                            if (acceptedHiringRelationshipViewModel.isMyRelationship) {
                                return acceptedHiringRelationshipViewModel;
                            }
                        })
                        .then(myHiringRelationshipViewModel =>
                            self._getAvailableActionsForProfile(
                                myHiringRelationshipViewModel,
                                teammateHasAccepted,
                                options.viewingTeammateRelationship,
                            ),
                        )
                        .then(actions => {
                            // We never show pass as an option in reviewed positions
                            actions.pass = false;
                            return actions;
                        });
                },

                _getAvailableActionsForProfile(
                    myHiringRelationshipViewModel,
                    teammateHasAccepted,
                    viewingTeammateRelationship,
                ) {
                    // if there is no relationship, it should look the same as it does
                    // when there is a pending relationship
                    const myStatus = myHiringRelationshipViewModel ? myHiringRelationshipViewModel.myStatus : 'pending';
                    const acceptedByMyTeam = teammateHasAccepted || myStatus === 'accepted';
                    const oneClickReachOutEnabled = this.user.pref_one_click_reach_out || false;

                    return {
                        // We always hide the pass button when viewing a teammates' relationship,
                        // since it is unclear what it would do
                        pass: !viewingTeammateRelationship && !acceptedByMyTeam && myStatus !== 'rejected',
                        save: !acceptedByMyTeam && myStatus !== 'saved_for_later',
                        like: !acceptedByMyTeam && oneClickReachOutEnabled && myStatus !== 'saved_for_later',
                        invite: !acceptedByMyTeam,
                    };
                },

                setFeaturedPositionsNumNotifications() {
                    const self = this;

                    if (self.role === 'hiringManager') {
                        if ($location.path() === self.featuredPositionsUrl) {
                            self.featuredPositionsNumNotifications = 0;
                        } else if (!self.user.hasSeenFeaturedPositionsPage) {
                            // 1 if we have not yet visited the featured positions page
                            self.featuredPositionsNumNotifications = 1;
                        } else {
                            this._calculateFeaturedPositionsNumNotificationsFromCandidatePositionInterests();
                        }
                    } else {
                        // undefined if not a hiringManager
                        self.featuredPositionsNumNotifications = undefined;
                    }
                },

                bumpLastRelationshipUpdatedAt(hiringRelationship) {
                    if (hiringRelationship.updated_at > this.lastRelationshipUpdatedAt) {
                        this.lastRelationshipUpdatedAt = hiringRelationship.updated_at;
                    }
                },

                nagIfNecessary() {
                    // Only candidates should be nagged and only when the last time they were nagged was an hour or more ago
                    // or if the careersLastNaggedAt value is somehow set to a date in the future.
                    const now = Date.now();
                    const careersLastNaggedAt = ClientStorage.getItem('careersLastNaggedAt');

                    // they should be nagged if they were previously nagged an hour or more ago or
                    // if the last time they were nagged is somehow set to a date in the future
                    const millisecondsInOneHour = 3600000;
                    const shouldBeNagged =
                        !careersLastNaggedAt ||
                        careersLastNaggedAt <= now - millisecondsInOneHour ||
                        careersLastNaggedAt > now;

                    if (shouldBeNagged) {
                        if (this.connectionsNumNotifications > 0) {
                            this._nag(); // nag the user about connections in the Careers network that need their attention
                        }
                    }
                },

                showHiringBillingModal(careerProfile, action) {
                    EventLogger.log('hiring_manager:saw_hiring_billing_modal', {
                        candidate_id: careerProfile && careerProfile.user_id,
                        action,
                    });

                    DialogModal.alert({
                        content: '<hiring-billing in-modal="true"></hiring-billing>',
                        classes: ['hiring-billing-modal'],
                    });
                },

                showHiringBillingModalOnAction(careerProfile, action) {
                    // some career profiles will be anonymized for freemium hiring managers who
                    // do not yet have a subscription
                    if (careerProfile.anonymized) {
                        this.showHiringBillingModal(careerProfile, action);
                        return true;
                    }
                    return false;
                },

                showShareModal(careerProfile, onFinish) {
                    // determine proper callbacks
                    const onCloseCallback = openHiringTeamInviteModal => {
                        DialogModal.removeAlerts();

                        // the hiring manager can opt to invite a coworker from the share modal
                        // instead of sharing the career profile with an existing teammate
                        if (openHiringTeamInviteModal) {
                            const HiringTeamInviteModal = $injector.get('HiringTeamInviteModal');
                            HiringTeamInviteModal.open();
                        } else if (onFinish) {
                            onFinish();
                        }
                    };

                    DialogModal.alert({
                        content:
                            '<share-career-profile career-profile="careerProfile" on-close-callback="onCloseCallback(openHiringTeamInviteModal)" class="careers-share-modal"></share-career-profile>',
                        classes: ['overflow-visible', 'share-card', 'blue', 'small'],
                        hideCloseButton: true,
                        closeOnOutsideClick: true,
                        blurTargetSelector: 'div[ng-controller]',
                        scope: {
                            careerProfile,
                            onCloseCallback,
                        },
                    });
                },

                showFullProfile(params) {
                    this.getOrInitializeHiringRelationshipViewModelForCareerProfile(params.careerProfile).then(
                        hiringRelationshipViewModel => {
                            const classes = !params.showInvite && !params.showActions ? ['single-column'] : [];

                            DialogModal.alert({
                                content:
                                    // eslint-disable-next-line prefer-template
                                    '<div class="card-list-item"><candidate-list-card career-profile="careerProfile" on-finish="onFinish" show-full-profile="showFullProfile"></candidate-list-card></div>' +
                                    (params.showInvite || params.showActions ? '<div class="modal-rail">' : '') +
                                    (params.showInvite
                                        ? '<invite-to-interview-form hiring-relationship-view-model="hiringRelationshipViewModel" on-finish="onFinish" candidate-position-interest="candidatePositionInterest" stays-narrow="staysNarrow"></invite-to-interview-form>'
                                        : '') +
                                    (params.showActions
                                        ? '<candidate-full-card-actions ng-model="hiringRelationshipViewModel" candidate-position-interest="candidatePositionInterest" on-finish="onFinish" show-hide-button="showHideButton"></candidate-full-card-actions>'
                                        : '') +
                                    (params.showInvite || params.showActions ? '</div>' : ''),
                                classes: ['career-profile-modal'].concat(classes),
                                blurTargetSelector: 'div[ng-controller]',
                                scope: {
                                    careerProfile: params.careerProfile,
                                    hiringRelationshipViewModel,
                                    candidatePositionInterest: params.candidatePositionInterest,
                                    showHideButton: params.showHideButton,
                                    staysNarrow: true,
                                    showFullProfile: true,
                                    onFinish() {
                                        DialogModal.removeAlerts();
                                        if (params.onFinish) {
                                            params.onFinish();
                                        }
                                    },
                                },
                            });
                        },
                    );
                },

                openInviteModal(careerProfile, params, onFinish, onClosed) {
                    const hasNotYetLikedFirstCandidate = this.hasNotYetLikedFirstCandidate;

                    this.getOrInitializeHiringRelationshipViewModelForCareerProfile(careerProfile, {
                        foundWithSearchId: params.foundWithSearchId,
                    }).then(hiringRelationshipViewModel => {
                        DialogModal.alert({
                            content:
                                '<invite-to-interview-form hiring-relationship-view-model="hiringRelationshipViewModel" candidate-position-interest="candidatePositionInterest" on-finish="onFinish" show-invite-sent-checkmark="showInviteSentCheckmark"></invite-to-interview-form>',
                            size: 'normal',
                            classes: ['no-title'],
                            blurTargetSelector: '#sp-page',
                            scope: {
                                onFinish(result) {
                                    if (!hasNotYetLikedFirstCandidate) {
                                        DialogModal.removeAlerts();
                                    }
                                    if (onFinish) {
                                        onFinish(result);
                                    }
                                },
                                hiringRelationshipViewModel,
                                candidatePositionInterest: params.candidatePositionInterest,
                                showInviteSentCheckmark: !hasNotYetLikedFirstCandidate,
                            },
                            close: onClosed,
                        });
                    });
                },

                // open positions for hiring managers
                ensureOpenPositions() {
                    const self = this;

                    if (!self._openPositionsPromise) {
                        const filters = {};

                        if (self.role === 'hiringManager') {
                            if (self.user.hiring_team_id) {
                                filters.hiring_team_id = self.user.hiring_team_id;
                            } else {
                                filters.hiring_manager_id = self.user.id;
                            }
                        } else if (self.role === 'candidate') {
                            throw new Error('Only hiring managers use this method now');
                        }

                        self._openPositionsPromise = OpenPosition.index({
                            filters,
                        }).then(response => {
                            self.openPositions = response.result;
                            return self.openPositions;
                        });
                    }

                    return self._openPositionsPromise;
                },

                loadRecommendedPositions(extraParams) {
                    const self = this;

                    if (self.role !== 'candidate') {
                        throw new Error('Only candidates use this method');
                    }

                    // Note: If this logic changes then be sure to reassess the
                    // push message logic in api_crud_controller_base.rb#add_num_recommended_positions_to_push_messages
                    return OpenPosition.index({
                        filters: {
                            recommended_for: self.user.id,
                            featured: true,
                            archived: false,
                            hiring_manager_might_be_interested_in: self.user.id,
                            candidate_has_acted_on: false,
                        },
                        candidate_id: self.user.id,
                        sort: 'RECOMMENDED_FOR_CANDIDATE',
                        ...extraParams,
                    }).then(response => response.result || []);
                },

                // We use this method because when we edit open positions
                // in a form, we edit a clone so that unsaved changes will
                // not be left in the cache.  When the changes to a clone are
                // saved, the clone should be passed in here, and we will
                // include it in the openPositions cache.
                commitOpenPosition(openPosition) {
                    // I don't think this method can ever be called before
                    // there are cached open positions, but if there aren't
                    // any, then we don't need to do this.
                    if (!this.openPositions) {
                        return;
                    }

                    // Replace the existing openPosition in this.openPositions with the updated
                    // openPosition that was passed in
                    const index = _.findIndex(this.openPositions, op => op.id === openPosition.id);

                    // If an existing openPosition was found, replace it
                    if (index !== -1) {
                        this.openPositions[index] = openPosition;
                    } else {
                        // Otherwise, unshift the openPosition onto the beginning of the array
                        this.openPositions.unshift(openPosition);
                    }
                },

                ensureCandidatePositionInterests() {
                    if (!this._candidatePositionInterestPromise) {
                        this._candidatePositionInterestPromise = this._loadCandidatePositionInterests();
                    }
                    return this._candidatePositionInterestPromise;
                },

                saveCandidatePositionInterest(candidatePositionInterest) {
                    const self = this;
                    if (self.role === 'hiringManager') {
                        $rootScope.$broadcast('candidatePositionInterestUpdate');
                        return candidatePositionInterest.save().then(
                            response => $q.when(response),
                            response => {
                                if (response.status === 409) {
                                    DialogModal.alert({
                                        content: self.translationHelper.get('interest_save_conflict'),
                                    });
                                    self.onCandidatePositionInterestConflict();
                                    return $q.when(response);
                                }
                                throw new Error('An unexpected status code passed through');
                            },
                        );
                    }
                    $rootScope.$broadcast('candidatePositionInterestUpdate');
                    return candidatePositionInterest.save();
                },

                removeHiddenRelationships() {
                    const self = this;
                    _.each(self.hiringRelationshipViewModels, hiringRelationshipViewModel => {
                        if (hiringRelationshipViewModel.myStatus === 'hidden') {
                            self._removeHiringRelationshipFromCache(hiringRelationshipViewModel.hiringRelationship.id);
                        }
                    });
                },

                // this is tested in the CareersNetworkInterceptor spec
                onUnloadedHiringRelationshipChange(alreadyUpdatedTo) {
                    const self = this;
                    const oldHiringRelationshipsById =
                        self.hiringRelationships && _.indexBy(self.hiringRelationships, 'id');
                    let newHiringRelationships;

                    // Load up any changes
                    return self
                        ._loadUpdatedRelationships(alreadyUpdatedTo)
                        .then(_newHiringRelationships => {
                            newHiringRelationships = _newHiringRelationships;
                            // Load up any deletions
                            return self._loadHiddenRelationships(alreadyUpdatedTo);
                        })
                        .then(() =>
                            self._notifyOfHiringRelationshipChages(newHiringRelationships, oldHiringRelationshipsById),
                        );
                },

                onCandidatePositionInterestConflict() {
                    this._reloadCandidatePositionInterests();
                    $rootScope.$broadcast('candidatePositionInterestConflict');
                },

                // When a hiring manager reviews and accepts a candidate position interest for a candidate,
                // an accepted hiring relationship gets created between the hiring manager and the canddiate.
                // When this happens, we want to hide all of the other position interests from this candidate
                // for the other positions related to this hiring manager. We only care about doing this when
                // jthe hiring manager has ACCEPTED the hiring relationship because if they reject or save the
                // candidate for later, the other candidate position interests are still considered to be valid.
                // The server has analogous logic. We could reduce code duplication here by having the server
                // send down side effects and looking at those, but it seemed simpler for now to duplicate the
                // rules here. If the logic gets more complex, we can reconsider.
                hideRelatedCandidatePositionInterests(hiringRelationshipViewModel) {
                    if (this.role !== 'hiringManager') {
                        throw new Error('Only for hiring managers!');
                    }

                    if (hiringRelationshipViewModel.ourStatus !== 'accepted') {
                        return $q.when([]);
                    }

                    return this.ensureCandidatePositionInterests()
                        .then(() => this.ensureOpenPositions())
                        .then(() => {
                            const relatedInterests = _.chain(this.candidatePositionInterests)
                                .where({
                                    candidate_id: hiringRelationshipViewModel.connectionId,
                                })
                                .select(relatedInterest => !relatedInterest.hiddenOrReviewedByHiringManager)
                                .value();

                            // On the server, these related interests become hidden from the hiring manager,
                            // so we remove them from the candidatePositionInterests master collection to
                            // emulate this on the client.
                            this.candidatePositionInterests = _.without(
                                this.candidatePositionInterests,
                                ...relatedInterests,
                            );

                            return relatedInterests;
                        });
                },

                _loadCandidatePositionInterests() {
                    const self = this;

                    const args = {};
                    const filters = {};
                    if (self.role === 'candidate') {
                        filters.candidate_id = self.user.id;
                        args['except[]'] = ['hiring_manager_status', 'admin_status'];
                    } else if (self.role === 'hiringManager') {
                        // this logic should be in line with SendHiringManagerTrackingEmailsJob
                        if (self.user.hiring_team_id) {
                            filters.hiring_team_id = self.user.hiring_team_id;
                        } else {
                            filters.hiring_manager_id = self.user.id;
                        }
                        filters.candidate_status = 'accepted';
                        filters.hiring_manager_status_not = 'hidden';
                        args['fields[]'] = ['HIRING_MANAGER_REVIEWER_FIELDS'];
                    }

                    args.filters = filters;

                    return CandidatePositionInterest.index(args).then(response => {
                        self.candidatePositionInterests = response.result;
                        return self.candidatePositionInterests;
                    });
                },

                // This immediately creates a hiring relationship, saving it over the api.  It is different
                // from getOrInitializeHiringRelationshipViewModelForCareerProfile because it does not
                // require that a career_profile be passed in.  Since it is being saved immediately, the
                // career profile comes down from the api.
                //
                // NOTE: If we run into a multiple connections error because a teammate has already liked
                // this candidate, then we will be forwarded to the conversation for the candidate.  See
                // _handleSaveError near (targetStatus === 'pending') and https://trello.com/c/gzdhD7bn
                _getOrCreatePendingHiringRelationshipViewModelForConnectionId(
                    connectionId,
                    foundWithSearchId,
                    checkApiIfNotFound,
                ) {
                    const self = this;
                    return self
                        .getMyHiringRelationshipViewModelForConnectionId(connectionId, checkApiIfNotFound)
                        .then(hiringRelationshipViewModel => {
                            if (!hiringRelationshipViewModel) {
                                return self._createPendingHiringRelationship(connectionId, foundWithSearchId);
                            }
                        })
                        .then(() => self.getMyHiringRelationshipViewModelForConnectionId(connectionId));
                },

                _createPendingHiringRelationship(candidateId, foundWithSearchId) {
                    const hiringRelationshipViewModel = this._initializeHiringRelationshipViewModel(
                        candidateId,
                        foundWithSearchId,
                    );
                    return hiringRelationshipViewModel.save();
                },

                _calculateFeaturedPositionsNumNotificationsFromCandidatePositionInterests() {
                    const self = this;

                    self.ensureCandidatePositionInterests().then(candidatePositionInterests => {
                        self.featuredPositionsNumNotifications = _.chain(candidatePositionInterests)
                            .where({
                                hiring_manager_status: 'unseen',
                                candidate_status: 'accepted',
                            })
                            .size()
                            .value();
                    });
                },

                _initializeHiringRelationshipViewModel(careerProfileOrConnectionId, foundWithSearchId) {
                    if (this.role !== 'hiringManager') {
                        throw new Error('This is only for hiring managers.');
                    }

                    let careerProfile;
                    let candidateId;
                    // You can pass in either a candidate id or a career profile here.  However,
                    // if you pass in only a candidate id, you will not be able to display this
                    // connection until you save it and pull down the career profile.
                    if (typeof careerProfileOrConnectionId === 'string') {
                        candidateId = careerProfileOrConnectionId;
                    } else {
                        careerProfile = careerProfileOrConnectionId;
                        candidateId = careerProfile.user_id;
                    }

                    const hiringRelationship = HiringRelationship.new({
                        candidate_id: candidateId,
                        hiring_manager_id: this.user.id,
                        hiring_manager_status: 'pending',
                        career_profile: careerProfile,
                        hiring_application: this.user.hiring_application,
                    });
                    const hiringRelationshipViewModel = new HiringRelationshipViewModel(
                        hiringRelationship,
                        null,
                        'hiringManager',
                        this.user,
                    );
                    hiringRelationshipViewModel.foundWithSearchId = foundWithSearchId;
                    hiringRelationshipViewModel.addToCareersNetworkViewModelOnSave = this;
                    return hiringRelationshipViewModel;
                },

                _removeHiringRelationshipFromCache(hiringRelationshipId) {
                    let hiringRelationshipViewModel;
                    this.hiringRelationshipViewModels = _.reject(this.hiringRelationshipViewModels, vm => {
                        if (vm.hiringRelationship.id === hiringRelationshipId) {
                            hiringRelationshipViewModel = vm;
                            return true;
                        }
                    });

                    if (!hiringRelationshipViewModel) {
                        return;
                    }

                    // remove from this._hiringRelationshipViewModelsByConnectionId, making sure to preserve
                    // other relationships (i.e. from teammates) with the same connection
                    let hiringRelationshipViewModelsForConnectionId = this._hiringRelationshipViewModelsByConnectionId[
                        hiringRelationshipViewModel.connectionId
                    ];
                    hiringRelationshipViewModelsForConnectionId = _.reject(
                        hiringRelationshipViewModelsForConnectionId,
                        vm => vm.hiringRelationship.id === hiringRelationshipId,
                    );
                    if (_.any(hiringRelationshipViewModelsForConnectionId)) {
                        this._hiringRelationshipViewModelsByConnectionId[
                            hiringRelationshipViewModel.connectionId
                        ] = hiringRelationshipViewModelsForConnectionId;
                    } else {
                        delete this._hiringRelationshipViewModelsByConnectionId[
                            hiringRelationshipViewModel.connectionId
                        ];
                    }

                    // remove from this.hiringRelationships
                    this.hiringRelationships = _.reject(
                        this.hiringRelationships,
                        hiringRelationship => hiringRelationship.id === hiringRelationshipId,
                    );
                },

                _getHiringRelationshipViewModelsForConnectionIdAndProperty(connectionId, property, onNotFound) {
                    const self = this;
                    return self
                        ._getHiringRelationshipViewModelsForConnectionId(connectionId)
                        .then(hiringRelationshipViewModels => {
                            // filter out teammate's relationships
                            const hiringRelationshipViewModel = _.select(hiringRelationshipViewModels, property).first;

                            // See getOrCreatePendingHiringRelationshipViewModelForSharedCareerProfile.
                            // In that case, we need to check the api to see if we already have a relationship with this person
                            if (!hiringRelationshipViewModel) {
                                return onNotFound();
                            }
                        })
                        .then(
                            () => _.select(self._hiringRelationshipViewModelsByConnectionId[connectionId], property)[0],
                        );
                },

                _getHiringRelationshipViewModelsForConnectionId(connectionId) {
                    const self = this;
                    return this._waitForDependentHiringRelationshipListLoadRequestsToFinish().then(
                        () => self._hiringRelationshipViewModelsByConnectionId[connectionId],
                    );
                },

                _preloadHiringRelationships() {
                    // This must return everything we need for calculateNumNotifications.
                    // We also have it return everything needed for the initial load of
                    //  the my candidates page connections list.  This is not critical,
                    //  but it generally shouldn't add much to the results and it means
                    //  that we don't need to make another request when hitting that page,
                    //  and don't need to deal with the UI issues that would come from
                    //  having the "Connected" section loaded but not other sections.
                    // For accepted hiring managers this means preloading teamCandidates
                    // For everyone else, this means preloading just myCandidates
                    if (this.role === 'hiringManager') {
                        return this.ensureHiringRelationshipsLoaded('teamCandidates');
                    }
                    return this.ensureHiringRelationshipsLoaded('myCandidates');
                },

                _ensureHiringRelationshipViewModels() {
                    const self = this;

                    self._hiringRelationshipViewModelsById = self._hiringRelationshipViewModelsById || {};

                    self.hiringRelationshipViewModels = _.map(self.hiringRelationships, hiringRelationship => {
                        if (self._hiringRelationshipViewModelsById[hiringRelationship.id]) {
                            // If we already have a view model for this hiring relationship, replace
                            // the underlying model with the one that was just pulled from the server
                            self._hiringRelationshipViewModelsById[
                                hiringRelationship.id
                            ].hiringRelationship = hiringRelationship;
                        } else {
                            // If we don;t have a view model yet, create one
                            self._hiringRelationshipViewModelsById[
                                hiringRelationship.id
                            ] = new HiringRelationshipViewModel(hiringRelationship, self);
                        }
                        return self._hiringRelationshipViewModelsById[hiringRelationship.id];
                    });
                    self._hiringRelationshipViewModelsByConnectionId = _.groupBy(
                        self.hiringRelationshipViewModels,
                        'connectionId',
                    );
                    self.calculateNumNotifications();
                },

                _loadHiringRelationshipsForListName(listName) {
                    let filters;

                    if (listName === 'teamCandidates' && this.role === 'hiringManager') {
                        filters = {
                            OUR_STATUS: ['accepted', 'saved_for_later'],
                            CONNECTION_STATUS: ['accepted', 'pending', 'hidden'],
                        };
                    } else if (listName === 'closed' && this.role === 'hiringManager') {
                        filters = {
                            closed: true,
                        };
                    } else if (listName === 'featured' && this.role === 'hiringManager') {
                        filters = {
                            hiring_manager_status: 'pending',
                        };
                    } else if (listName === 'rejected' && this.role === 'hiringManager') {
                        // This should be the only place that we specify the individual hiring manager in the filter.
                        // Since we give all accepted hiring managers a team we decided to simplify things by getting
                        // rid of the "my" vs "team" relationship loading. However, we realized that we still needed to
                        // load only an individual hiring manager's rejected relationships since we still show candidates
                        // in the Candidates tab for a hiring manager even if a teammate has rejected them, so it did not
                        // make sense to show teammates' rejected relationships in the hidden tab.
                        // See https://trello.com/c/oXVVYMkK
                        filters = {
                            hiring_manager_id: this.user.id,
                            hiring_manager_status: 'rejected',
                            candidate_status: 'hidden',
                            closed: false,
                        };
                    } else if (listName === 'saved' && this.role === 'hiringManager') {
                        filters = {
                            hiring_manager_status: 'saved_for_later',
                            open_position_id: null, // must be null to ensure these candidates have been saved_for_later from the Candidates page
                        };
                    } else if (listName === 'myCandidates' && this.role === 'candidate') {
                        filters = {
                            hiring_manager_status: ['accepted'],
                            candidate_status: ['accepted', 'pending', 'hidden'],
                            closed: false,
                        };
                    } else if (listName === 'closed' && this.role === 'candidate') {
                        filters = {
                            rejected_by_candidate_or_closed: true,
                        };
                    } else {
                        throw new Error(`Unexpected list "${listName}"`);
                    }

                    return this._loadHiringRelationships(filters);
                },

                _getHiringRelationshipIndexFilters(filters) {
                    const formattedFilters = {};

                    if (this.role === 'hiringManager') {
                        formattedFilters.hiring_team_id = this.user.hiring_team_id;
                    } else if (this.role === 'candidate') {
                        formattedFilters.candidate_id = this.user.id;
                    }

                    // OUR_STATUS: Set this if you want results with a particular status.
                    // If this is not set, then we will load up all statuses other than hidden.
                    const ourStatusProp = this.role === 'hiringManager' ? 'hiring_manager_status' : 'candidate_status';
                    if (filters.OUR_STATUS) {
                        formattedFilters[ourStatusProp] = filters.OUR_STATUS;
                    } else {
                        const statusNotProp = `${ourStatusProp}_not`;
                        formattedFilters[statusNotProp] = 'hidden';
                    }
                    delete filters.OUR_STATUS;

                    // CONNECTION_STATUS: Set this if you want results with a particular.
                    const theirStatusProp =
                        this.role === 'hiringManager' ? 'candidate_status' : 'hiring_manager_status';
                    if (filters.CONNECTION_STATUS) {
                        formattedFilters[theirStatusProp] = filters.CONNECTION_STATUS;
                    }
                    delete filters.CONNECTION_STATUS;

                    // CONNECTION_ID: Set this if you want relationships with a
                    // specific connection
                    if (filters.CONNECTION_ID) {
                        const connectionIdProp = this.role === 'hiringManager' ? 'candidate_id' : 'hiring_manager_id';
                        formattedFilters[connectionIdProp] = filters.CONNECTION_ID;
                    }
                    delete filters.CONNECTION_ID;

                    // We always set viewable_by_hiring_manager to true
                    if (this.role === 'hiringManager') {
                        formattedFilters.viewable_by_hiring_manager = true;
                    }

                    // any other filters that have been passed in
                    // just get merged on as-is
                    _.extend(formattedFilters, filters);

                    return formattedFilters;
                },

                _getHiringRelationshipIndexParams(filters, extraParams) {
                    const params = _.extend(
                        {
                            filters: this._getHiringRelationshipIndexFilters(filters),
                        },
                        extraParams,
                    );

                    if (this.role === 'hiringManager') {
                        params['except[]'] = ['hiring_application'];
                    } else if (this.role === 'candidate') {
                        params['except[]'] = ['career_profile'];
                    }

                    // If we are not searching for the relationships for
                    // the current user, then we DO need the hiring applications, but
                    // in order the save data over the wire, we don't want to duplicate
                    // them
                    if (this.role === 'hiringManager' && params.filters.hiring_manager_id !== this.user.id) {
                        params.hiring_applications_in_meta = true;
                    }

                    return params;
                },

                _loadHiringRelationships(filters, extraParams) {
                    const self = this;

                    const params = this._getHiringRelationshipIndexParams(filters, extraParams);

                    return HiringRelationship.index(params).then(response =>
                        self._onHiringRelationshipsLoaded(response.result, response.meta),
                    );
                },

                _reloadOpenPositions() {
                    this._openPositionsPromise = null;
                    this.ensureOpenPositions();
                },

                _reloadCandidatePositionInterests() {
                    this._candidatePositionInterestPromise = null;

                    // this method will reload the interests and set the notification count
                    this._calculateFeaturedPositionsNumNotificationsFromCandidatePositionInterests();
                },

                _getExistingHiringRelationshipListLoadPromiseForListName(listName) {
                    return (
                        this._dependentHiringRelationshipListLoadPromises[listName] ||
                        this._independentHiringRelationshipListLoadPromises[listName]
                    );
                },

                // See ensureHiringRelationshipsLoaded for an explanation of _dependentHiringRelationshipListLoadPromises vs _independentHiringRelationshipListLoadPromises
                _waitForDependentHiringRelationshipListLoadRequestsToFinish() {
                    return $q.all(_.values(this._dependentHiringRelationshipListLoadPromises));
                },

                // separate method for specs
                _onHiringRelationshipsLoaded(hiringRelationships, meta) {
                    const self = this;

                    // if any relationships have been pulled down that are hidden
                    // from me, remove them (this is used when pushing down changes. See
                    // onLastRelationshipUpdatedAtPushedDown)
                    hiringRelationships = _.reject(hiringRelationships, hiringRelationship => {
                        const prop = self.role === 'hiringManager' ? 'hiring_manager_status' : 'candidate_status';
                        const myStatus = hiringRelationship[prop];
                        if (myStatus === 'hidden') {
                            self._removeHiringRelationshipFromCache(hiringRelationship.id);
                            return true;
                        }
                    });

                    // make a new array where any of the incoming relationships replace existing ones
                    // with the same id
                    self.hiringRelationships = self.hiringRelationships
                        ? _.uniq(hiringRelationships.concat(self.hiringRelationships), 'id')
                        : hiringRelationships;

                    const hiringApplicationsFromMeta = _.chain(meta && meta.hiring_applications)
                        .map(attrs => HiringApplication.new(attrs))
                        .indexBy('user_id')
                        .value();

                    // Instead of loading up a copy of the hiring_application or
                    // career_profile with each relationship, just re-use the one
                    // from the current user.
                    _.each(self.hiringRelationships, hr => {
                        if (hr.hiring_manager_id === self.user.id) {
                            hr.hiring_application = self.user.hiring_application;
                        } else if (hiringApplicationsFromMeta[hr.hiring_manager_id]) {
                            hr.hiring_application = hiringApplicationsFromMeta[hr.hiring_manager_id];
                        }

                        if (hr.candidate_id === self.user.id) {
                            hr.career_profile = self.user.career_profile;
                        }
                    });

                    // We don't know if you've liked a candidate or not until we load up some
                    // relationships here.  That means if you have never liked someone before,
                    // you hit the browse page and like someone real fast before the CareersNetworkViewModel
                    // has preloaded, you will never see that modal popup.  This is low impact
                    // and low likelihood, so I"m leaving it.
                    self.hasNotYetLikedFirstCandidate = meta && meta.has_liked_candidate === false;
                    self._ensureHiringRelationshipViewModels();

                    // tell the world that hiring relationships have changed
                    $rootScope.$broadcast('hiringRelationshipUpdate');
                    return hiringRelationships;
                },

                _notifyOfHiringRelationshipChages(newHiringRelationships, oldHiringRelationshipsById) {
                    if (!oldHiringRelationshipsById) {
                        return;
                    }

                    const hiringRelationshipViewModelsById = _.indexBy(
                        this.hiringRelationshipViewModels,
                        vm => vm.hiringRelationship.id,
                    );

                    const changedHiringRelationship = _.chain(newHiringRelationships)
                        .sortBy('updated_at')
                        .reverse()
                        .detect(newHiringRelationship => {
                            const oldHiringRelationship = oldHiringRelationshipsById[newHiringRelationship.id];

                            const hiringRelationshipViewModel =
                                hiringRelationshipViewModelsById[newHiringRelationship.id];

                            // notify candidates of requests when they come in
                            return (
                                (hiringRelationshipViewModel.role === 'candidate' && !oldHiringRelationship) ||
                                // notify hiring managers of matches when they come in
                                (hiringRelationshipViewModel.role === 'hiringManager' &&
                                    oldHiringRelationship &&
                                    !oldHiringRelationship.matched &&
                                    newHiringRelationship.matched)
                            );

                            // Do nothing if the role is hiringTeammate
                        })
                        .value();

                    if (changedHiringRelationship) {
                        this._showModalForHiringRelationship(changedHiringRelationship);
                    }
                },

                _showModalForHiringRelationship(hiringRelationship) {
                    const hiringRelationshipViewModel = _.findWhere(this.hiringRelationshipViewModels, {
                        hiringRelationshipId: hiringRelationship.id,
                    });

                    const title =
                        this.role === 'hiringManager'
                            ? this.translationHelper.get('a_match')
                            : this.translationHelper.get('getting_attention');
                    const content =
                        this.role === 'hiringManager'
                            ? '<hiring-manager-connection-match-modal message="message" button-text="buttonText" connection-id="connectionId" connection-avatar-src="connectionAvatarSrc"></hiring-manager-connection-match-modal>'
                            : '<candidate-connection-match-modal message="message" button-text="buttonText" connection-id="connectionId" connection-avatar-src="connectionAvatarSrc"></candidate-connection-match-modal>';
                    const message =
                        this.role === 'hiringManager'
                            ? this.translationHelper.get('also_interested', {
                                  name: hiringRelationshipViewModel.connectionName,
                              })
                            : this.translationHelper.get('employer_liked');
                    const buttonText =
                        this.role === 'hiringManager'
                            ? this.translationHelper.get('send_message')
                            : this.translationHelper.get('check_it_out');
                    const classes = ['blue', 'avatar-badge'];
                    const blurTargetSelector = 'div[ng-controller]';

                    DialogModal.alert({
                        content,
                        title,
                        scope: {
                            message,
                            buttonText,
                            connectionId: hiringRelationshipViewModel && hiringRelationshipViewModel.connectionId,
                            connectionAvatarSrc:
                                hiringRelationshipViewModel && hiringRelationshipViewModel.connectionAvatarSrc,
                        },
                        size: 'small',
                        classes,
                        blurTargetSelector,
                    });
                },

                _nag() {
                    const self = this;
                    let classes;
                    let blurTargetSelector;

                    const title = self.translationHelper.get('careers_needs_attention');
                    const content =
                        '<candidate-connection-match-modal message="message" button-text="buttonText"></candidate-connection-match-modal>';
                    const message = self.translationHelper.get('waiting_your_response');
                    const buttonText = self.translationHelper.get('go_to_careers');

                    DialogModal.alert({
                        content,
                        title,
                        scope: {
                            message,
                            buttonText,
                        },
                        size: 'small',
                        classes,
                        blurTargetSelector,
                    });

                    ClientStorage.setItem('careersLastNaggedAt', Date.now());
                },

                _displayNewlyAccepted() {
                    const self = this;
                    const hiringPlan = this.user.hiringPlan;

                    const message = self.translationHelper.get('start_reviewing_now');
                    const buttonText = self.translationHelper.get('review_candidates');

                    if (hiringPlan === 'unlimited_w_sourcing') {
                        // When accepted take the unlimited-with-sourcing hiring manager directly
                        // to their home. Assuming they require subscriptions they'll
                        // go to the browse-candidates page, which has messaging
                        // and UI for actually subscribing
                        $location.url('/home');
                    } else if (hiringPlan === 'pay_per_post') {
                        // Do nothing if pay_per_post since those teams can already make positions
                        // before being accepted
                    } else if (hiringPlan === 'legacy') {
                        // In the old hiring pricing model we showed a modal if a
                        // hiring manager was accepted while in the app. We migrated
                        // those hiring managers' plans to 'legacy'. Continue
                        // to do this logic just for them.
                        DialogModal.alert({
                            content:
                                '<hiring-manager-accepted-modal message="message" button-text="buttonText"></hiring-manager-accepted-modal>',
                            title: self.translationHelper.get('youre_in'),
                            scope: {
                                message,
                                buttonText,
                            },
                            size: 'small',
                            classes: ['blue'],
                            blurTargetSelector: 'div[ng-controller]',
                        });
                    }
                },

                _loadUpdatedRelationships(updatedSince) {
                    return this._loadHiringRelationships({
                        updated_since: updatedSince,
                    });
                },

                _loadHiddenRelationships(updatedSince) {
                    return this._loadHiringRelationships(
                        {
                            updated_since: updatedSince,
                            OUR_STATUS: 'hidden',
                        },
                        {
                            'fields[]': ['id', 'hiring_manager_status', 'candidate_status'],
                        },
                    );
                },

                _setupWatchers() {
                    const self = this;
                    // Create watchers to listen for an updated timestamp coming in from the push_messages
                    self.watchCancelers = [
                        $rootScope.$watch('currentUser.avatar_url', (newAvatar, oldAvatar) => {
                            if (newAvatar !== oldAvatar) {
                                self._refreshExistingData();
                            }
                        }),
                        $rootScope.$watch('currentUser.hasSeenFeaturedPositionsPage', (newValue, oldValue) => {
                            if (oldValue !== newValue) {
                                self.setFeaturedPositionsNumNotifications();
                            }
                        }),
                        $rootScope.$watch(
                            () => $location.path(),
                            (newValue, oldValue) => {
                                if (oldValue !== newValue) {
                                    self.setFeaturedPositionsNumNotifications();
                                }
                            },
                        ),
                    ];

                    if (self.role === 'hiringManager') {
                        self.watchCancelers.push(
                            $rootScope.$watch('currentUser.hiring_application.status', (newStatus, oldStatus) => {
                                if (newStatus === 'accepted' && oldStatus !== newStatus) {
                                    // don't display the newly accepted prompt if the hiring manager has already begun swiping,
                                    // since it would be redundant and unnecessary explanation
                                    const neverSwiped = _.every(
                                        self.hiringRelationshipViewModels || [],
                                        viewModel => !viewModel.consideredByHiringManager,
                                    );
                                    if (neverSwiped) {
                                        self._displayNewlyAccepted();
                                    }
                                }
                            }),
                        );
                    }
                },

                // We have seen enough little edge cases with caching that we thing
                // it likely that there will be new ones in the future.  Just in case,
                // clear the cache every hour so someone will eventually get new data
                // even if there is some issue and they never refresh the page.
                _setupIntervals() {
                    const self = this;

                    const ONE_MINUTE = 1000 * 60;

                    this._resetInterval = $interval(() => {
                        self.resetCache();
                    }, ONE_MINUTE * 60);
                },

                // handles any situation where we think we can preserve the data but
                // rebuild updated fields off of derived data (user, etc)
                _refreshExistingData() {
                    const self = this;

                    // rebuild avatars in conversation messages
                    _.chain(self.hiringRelationships)
                        .pluck('conversation')
                        .compact()
                        .each(conversation => {
                            if (conversation.messages) {
                                conversation.messages.forEach(message => {
                                    if (message.sender.id === self.user.id) {
                                        message.sender.avatar_url = self.user.avatar_url;
                                    }
                                });
                            }
                        });

                    // Update the proxy fields on the HiringApplication or CareerProfile
                    if (self.user.hiring_application) {
                        self.user.hiring_application.avatar_url = self.user.avatar_url;
                    }
                    if (self.user.career_profile) {
                        self.user.career_profile.avatar_url = self.user.avatar_url;
                    }
                },
            };
        });

        return CareersNetworkViewModel;
    },
]);
