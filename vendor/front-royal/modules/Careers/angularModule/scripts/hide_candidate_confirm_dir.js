import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/hide_candidate_confirm.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('hideCandidateConfirm', [
    '$injector',
    function factory() {
        return {
            restrict: 'E',
            scope: {
                hideCandidate: '&',
                currentUser: '<',
            },
            templateUrl,

            link(scope) {
                // save user has seen hide candidate confirm
                scope.currentUser.has_seen_hide_candidate_confirm = true;
                if (!scope.currentUser.ghostMode) {
                    scope.currentUser.save();
                }
            },
        };
    },
]);
