import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/candidate_full_card_actions.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('candidateFullCardActions', [
    '$injector',
    function factory($injector) {
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
        const EventLogger = $injector.get('EventLogger');
        const $location = $injector.get('$location');
        const $rootScope = $injector.get('$rootScope');
        const isMobileMixin = $injector.get('isMobileMixin');
        const DialogModal = $injector.get('DialogModal');
        const $translate = $injector.get('$translate');
        const HiringRelationshipViewModel = $injector.get('HiringRelationshipViewModel');
        const CareerProfile = $injector.get('CareerProfile');

        EventLogger.allowEmptyLabel([
            'candidate_list_card:clicked_share_link',
            'candidate_list_card:clicked_full_profile',
        ]);

        return {
            restrict: 'E',
            scope: {
                ngModel: '<',
                onFinish: '<',
                candidatePositionInterest: '<?',
                disableActions: '<?',
                showHideButton: '<?',
            },
            templateUrl,

            link(scope) {
                isMobileMixin.onLink(scope);

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.careersNetworkViewModel = CareersNetworkViewModel.get('hiringManager');

                // get hiringRelationshipViewModel
                if (scope.ngModel.isA(HiringRelationshipViewModel)) {
                    scope.hiringRelationshipViewModel = scope.ngModel;
                    scope.careerProfile = scope.hiringRelationshipViewModel.hiringRelationship.career_profile;
                } else if (scope.ngModel.isA(CareerProfile)) {
                    scope.careerProfile = scope.ngModel;
                    scope.careersNetworkViewModel
                        .getOrInitializeHiringRelationshipViewModelForCareerProfile(scope.careerProfile)
                        .then(hiringRelationshipViewModel => {
                            scope.hiringRelationshipViewModel = hiringRelationshipViewModel;
                        });
                } else {
                    throw 'Unsupported ngModel class';
                }

                // some locale keys
                if (scope.candidatePositionInterest) {
                    scope.hideLocaleKey = 'careers.candidate_list_card.pass';
                    scope.hideToolTipKey = 'pass_for_position';
                } else {
                    scope.hideLocaleKey = 'careers.candidate_list_card.hide';
                    scope.hideToolTipKey = 'hide_from_browse';
                }

                const interestStatus =
                    scope.candidatePositionInterest && scope.candidatePositionInterest.hiring_manager_status;

                // get showActions
                scope.careersNetworkViewModel
                    .getAvailableActionsWithEverythingPreloaded(scope.careerProfile)
                    .then(showActions => {
                        scope.showSave = showActions.save;
                        scope.showLike = showActions.like && !scope.candidatePositionInterest;
                        // only show pass button in full profile context because we show it in candidate_list_actions_dir.js
                        scope.showHide =
                            (showActions.pass && !interestStatus) ||
                            (scope.showHideButton &&
                                _.contains(['unseen', 'pending', 'saved_for_later'], interestStatus));
                    });

                scope.hasBadge =
                    _.contains(['invited', 'accepted'], interestStatus) ||
                    (scope.hiringRelationshipViewModel &&
                        scope.hiringRelationshipViewModel.hiringRelationship.hiring_manager_status === 'accepted');
                scope.showShare =
                    _.contains(['unseen', 'pending', 'saved_for_later', 'rejected'], interestStatus) ||
                    (!scope.hasBadge && !scope.candidatePositionInterest);

                scope.logEvent = (name, payload) => {
                    EventLogger.log(
                        name,
                        angular.extend(payload || {}, {
                            candidate_id: scope.careerProfile.user_id,
                        }),
                    );
                };

                // share action
                scope.share = () => {
                    if (scope.careersNetworkViewModel.showHiringBillingModalOnAction(scope.careerProfile, 'share')) {
                        return;
                    }

                    scope.logEvent('candidate_list_card:clicked_share_link');
                    scope.careersNetworkViewModel.showShareModal(scope.careerProfile);
                };

                // main action
                scope.updateStatus = status => {
                    if (
                        scope.careersNetworkViewModel.showHiringBillingModalOnAction(
                            scope.careerProfile,
                            `updateStatus:${status}`,
                        )
                    ) {
                        return;
                    }

                    // if you're trying to hide a candidate from the positions context, $location.search().action will be "review"
                    // don't confirm the action in that case
                    if (
                        status === 'rejected' &&
                        !scope.currentUser.has_seen_hide_candidate_confirm &&
                        !$location.search().action
                    ) {
                        DialogModal.alert({
                            content:
                                '<hide-candidate-confirm hide-candidate="hideCandidate()" current-user="currentUser"></hide-candidate-confirm>',
                            title: $translate.instant('careers.hide_candidate_confirm.title'),
                            size: 'small',
                            classes: ['center'],
                            scope: {
                                hideCandidate() {
                                    scope.updateStatus(status);
                                    DialogModal.hideAlerts();
                                },
                                currentUser: scope.currentUser,
                            },
                        });
                    } else {
                        scope.careersNetworkViewModel
                            .getOrInitializeHiringRelationshipViewModelForCareerProfile(scope.careerProfile)
                            .then(hiringRelationshipViewModel => {
                                // If we are rejecting/saving/liking a candidate within the /positions context,
                                // we'll need access to the corresponding candidatePositionInterest
                                const meta = {};
                                if (scope.candidatePositionInterest) {
                                    scope.candidatePositionInterest.hiring_manager_status =
                                        status === 'invite' ? 'invited' : status;
                                    meta.candidatePositionInterest = scope.candidatePositionInterest;
                                }

                                // If we are rejecting this candidate in the positions context then only
                                // update the candidatePositionInterest since updating the hiringRelationship would
                                // mean rejecting them completely, rather than rejecting them just for this position.
                                if (scope.candidatePositionInterest && status === 'rejected') {
                                    scope.careersNetworkViewModel.saveCandidatePositionInterest(
                                        scope.candidatePositionInterest,
                                    );
                                } else {
                                    // Otherwise, update the hiringRelationshipViewModel,
                                    // passing in the candidatePositionInterest
                                    hiringRelationshipViewModel.updateStatus(status);
                                    hiringRelationshipViewModel.save(meta);
                                }
                            });

                        scope.onFinish();
                    }
                };
            },
        };
    },
]);
