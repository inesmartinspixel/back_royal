import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/hiring_positions.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('hiringPositions', [
    '$injector',

    function factory($injector) {
        const $location = $injector.get('$location');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const $rootScope = $injector.get('$rootScope');
        const $route = $injector.get('$route');
        const scopeTimeout = $injector.get('$timeout');
        const OpenPosition = $injector.get('OpenPosition');
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
        const TranslationHelper = $injector.get('TranslationHelper');
        const HiringTeam = $injector.get('HiringTeam');

        return {
            scope: {},
            restrict: 'E',
            templateUrl,

            link(scope, elem) {
                AppHeaderViewModel.setTitleHTML(new TranslationHelper('navigation.app_menu').get('post').toUpperCase());

                scope.careersNetworkViewModel = CareersNetworkViewModel.get('hiringManager');

                scope.addPosition = () => {
                    $location.search('action', 'create');
                    $location.search('list', null);
                };

                scope.tabs = [
                    {
                        role: 'hiringManager',
                        name: 'openPositions',
                        class: 'open-positions',
                        translate: {
                            hiringManager: 'careers.positions.open',
                        },
                    },
                    {
                        role: 'hiringManager',
                        name: 'archivedPositions',
                        class: 'archived-positions',
                        translate: {
                            hiringManager: 'careers.positions.closed',
                        },
                    },
                ];

                scope.$location = $location;

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.$watch('currentUser.hiring_team', hiringTeam => {
                    scope.stripePlan = hiringTeam
                        ? hiringTeam.stripePlanForHiringPlan(HiringTeam.HIRING_PLAN_PAY_PER_POST)
                        : null;
                });

                if (
                    !scope.currentUser.canCreateFeaturedPositions &&
                    !scope.currentUser.has_drafted_open_position &&
                    scope.currentUser.hiring_team
                ) {
                    $location.search().action = 'create';
                }

                Object.defineProperty(scope, 'currentTab', {
                    get() {
                        const search = $location.search();
                        let list = search.list;
                        if (
                            !angular.isDefined(search.action) &&
                            (!list || !_.contains(['openPositions', 'archivedPositions'], list))
                        ) {
                            $location.search('list', 'openPositions');
                            list = 'openPositions';
                        }
                        return list;
                    },
                    set(val) {
                        if (!_.contains(['archivedPositions'], val)) {
                            val = 'openPositions';
                        }
                        $location.search('list', val);
                        return val;
                    },
                });

                Object.defineProperty(scope, 'currentRouteDirective', {
                    get() {
                        return $route && $route.current && $route.current.$$route && $route.current.$$route.directive;
                    },
                    configurable: true, // for tests
                });

                // Don't animate back animation for this directive
                AppHeaderViewModel.animateBackTransition = false;
                scope.$on('$destroy', () => {
                    AppHeaderViewModel.animateBackTransition = false;

                    // Wait a beat to remove this param as it will trigger a change to currentTab which causes
                    // some jank
                    scopeTimeout(() => {
                        $location.search('list', null);
                    }, 0);
                });

                function getPosition() {
                    return scope.careersNetworkViewModel.ensureOpenPositions().then(openPositions =>
                        _.findWhere(openPositions, {
                            id: scope.positionId,
                        }),
                    );
                }

                function setPosition() {
                    // create a clone so we are not changing the object that is
                    // cached in careersNetworkViewModel until the form gets submitted.
                    // Also, prefill the location info with the hiring manager comany's
                    // location (unless the position already has the location info set)
                    // to reduce inconsistencies with the locations
                    getPosition().then(position => {
                        scope.position = position
                            ? OpenPosition.new(
                                  _.extend(position.asJson(), {
                                      place_id: position.place_id || $rootScope.currentUser.hiring_application.place_id,
                                      place_details: _.isEmpty(position.place_details)
                                          ? $rootScope.currentUser.hiring_application.place_details
                                          : position.place_details,
                                  }),
                              )
                            : null;
                    });
                }

                function reloadInterests() {
                    scope.position = null;
                    scope.careersNetworkViewModel.ensureCandidatePositionInterests().then(() => {
                        setPosition();
                    });
                }

                const interestConflictListener = $rootScope.$on('candidatePositionInterestConflict', () => {
                    reloadInterests();
                });
                scope.$on('$destroy', () => {
                    interestConflictListener();
                });

                scope.$watch('currentUser.hasFullHiringManagerAccess', (newVal, oldVal) => {
                    if (angular.isDefined(oldVal) && newVal !== oldVal) {
                        reloadInterests();
                    }
                });

                // There is also potentially a page param in the search() object.
                // We don't need to take any action when that changes
                scope.$watchGroup(
                    ['$location.search().action', '$location.search().positionId', '$location.search().list'],
                    () => {
                        const searchObject = $location.search();

                        // Everytime the 'action' query param changes we need to reset scope.position to null.
                        // We have to do this because scope.position could still be set from the previous action
                        // like 'review'. And due to the asynchronous nature of this code the position-form directive
                        // element will render before we reset the position on the scope to a clone of the position
                        // that's cached in the careersNetworkViewModel.
                        scope.position = null;
                        scope.action = searchObject.action;
                        scope.positionId = searchObject.positionId;

                        if (scope.positionId) {
                            getPosition()
                                .then(openPosition => {
                                    if (!openPosition) {
                                        $location.search('positionId', null);
                                    } else if (scope.action === 'review') {
                                        return scope.careersNetworkViewModel.ensureCandidatePositionInterests();
                                    }
                                })
                                .then(() => {
                                    setPosition();
                                });
                            if (scope.action === 'edit' || scope.action === 'create') {
                                elem.addClass('page-with-just-main-box-800');
                            } else {
                                elem.removeClass('page-with-just-main-box-800');
                            }
                        } else if (scope.action === 'create') {
                            // Without this timeout, we consistently get 10-digest errors in the browser.
                            // I guess there is just too much of a domino effect of different changes.  The timeout
                            // breaks the loop so we're not doing so much in one cycle.
                            $injector
                                .get('$timeout')()
                                .then(() => {
                                    // instantiate a new OpenPosition Iguana object and ensure that we prefill
                                    // the location info with the hiring manager company's location to reduce
                                    // inconsistencies with the locations
                                    scope.position = OpenPosition.new({
                                        hiring_manager_id: $rootScope.currentUser.id,
                                        hiring_application: $rootScope.currentUser.hiring_application,
                                        place_id: $rootScope.currentUser.hiring_application.place_id,
                                        place_details: $rootScope.currentUser.hiring_application.place_details,
                                    });
                                    elem.addClass('page-with-just-main-box-800');
                                });
                        } else {
                            elem.addClass('page-with-just-main-box-800');
                        }
                    },
                );

                // If the user presses back from an edit form, we want them to end up going back
                // to the positions list they came from.  The normal back button behavior ignores
                // query params, so this won't work by default.
                scope.$watch('$location.search().action', (newAction, oldAction) => {
                    if (oldAction !== newAction) {
                        $rootScope.pushBackButtonHistory($location.$$url, scope.currentRouteDirective);
                    }
                });

                scope.$watch('position', position => {
                    AppHeaderViewModel.showAlternateHomeButton = !!position;
                });

                scope.$watch('currentUser.hasSeenFeaturedPositionsPage', () => {
                    if (
                        scope.currentUser &&
                        !scope.currentUser.hasSeenFeaturedPositionsPage &&
                        !scope.currentUser.ghostMode
                    ) {
                        scope.currentUser.hasSeenFeaturedPositionsPage = true;
                        scope.currentUser.save();
                    }
                });
            },
        };
    },
]);
