import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/liked_first_candidate_modal.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('likedCandidateModal', [
    '$injector',
    function factory($injector) {
        const DialogModal = $injector.get('DialogModal');

        return {
            restrict: 'E',
            scope: {},
            templateUrl,

            link(scope) {
                scope.dismiss = () => {
                    DialogModal.hideAlerts();
                };
            },
        };
    },
]);
