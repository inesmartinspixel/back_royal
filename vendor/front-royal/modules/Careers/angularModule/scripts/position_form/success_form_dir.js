import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/position_form/success_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import congrats from 'vectors/congrats.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('successForm', [
    function factory() {
        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                scope.congratsSrc = congrats;

                // If the position is not featured (because we just saved a
                // draft) or the position is archived, then this success page
                // makes no sense
                if (!scope.position.featured || scope.position.archived || !scope.position.id) {
                    scope.loadRoute('/hiring/positions');
                }
            },
        };
    },
]);
