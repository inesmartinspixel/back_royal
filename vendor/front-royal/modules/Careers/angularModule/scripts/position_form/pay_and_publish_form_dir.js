import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/position_form/pay_and_publish_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

('use strict');

angularModule.directive('payAndPublishForm', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                // be defensive against someone refreshing on this page
                if (!scope.position.id || scope.currentUser.subscriptionForPosition(scope.position)) {
                    scope.loadRoute('/hiring/positions');
                }
            },
        };
    },
]);
