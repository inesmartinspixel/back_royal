import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/position_form/add_position_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';
import moment from 'moment-timezone';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('addPositionForm', [
    '$injector',

    function factory($injector) {
        const FormHelper = $injector.get('FormHelper');
        const TranslationHelper = $injector.get('TranslationHelper');
        const dateHelper = $injector.get('dateHelper');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                scope.positionDescriptorOptions = $injector.get('CAREERS_POSITION_DESCRIPTOR_KEYS');
                scope.perkOptions = ['bonus', 'equity', 'remote', 'relocation'];

                const propsValuesMap = {
                    position_descriptors: scope.positionDescriptorOptions,
                    perks: scope.perkOptions,
                };

                const propsRequiredFieldsMap = {
                    position_descriptors: {
                        min: 1,
                    },
                };

                FormHelper.supportForm(scope, scope.add_position);

                FormHelper.supportCheckboxGroups(scope, scope.position, propsValuesMap, propsRequiredFieldsMap);

                Object.defineProperty(scope, 'formattedAutoExpirationDate', {
                    get() {
                        return dateHelper.formattedUserFacingMonthDayYearLong(scope.autoExpirationDateViewValue, false);
                    },
                });

                scope.maxSkills = 5;

                scope.$watch('position', () => {
                    if (scope.position && !scope.position.archived && !scope.hiringManager.usingPayPerPostHiringPlan) {
                        scope.position.auto_expiration_date =
                            scope.position.auto_expiration_date || moment().add(60, 'days').format('YYYY-MM-DD');
                    }
                });

                const minExpirationDateDiff = 1;
                const maxExpirationDateDiff = 120;
                scope.minExpirationDate = moment().add(minExpirationDateDiff, 'days').format('YYYYMMDD');
                scope.maxExpirationDate = moment().add(maxExpirationDateDiff, 'days').format('YYYYMMDD');

                scope.$watch('autoExpirationDateViewValue', () => {
                    if (scope.autoExpirationDateViewValue) {
                        scope.showInvalidAutoExpirationDate =
                            scope.autoExpirationDateViewValue < scope.minExpirationDate ||
                            scope.autoExpirationDateViewValue > scope.maxExpirationDate;
                    } else {
                        scope.showInvalidAutoExpirationDate = false;
                    }
                });

                const translationHelper = new TranslationHelper('careers.position_form');

                function yearsArray(yearsFrom) {
                    return _.chain(_.range(yearsFrom, 11))
                        .map(year => ({
                            value: year,
                            label: `${year} ${translationHelper.get(year === 1 ? 'year' : 'years')}`,
                        }))
                        .value();
                }

                scope.yearsMinOptions = scope.yearsMaxOptions = yearsArray(1);

                scope.$watchCollection('position.desired_years_experience', () => {
                    const min =
                        scope.position &&
                        scope.position.desired_years_experience &&
                        scope.position.desired_years_experience.min;
                    if (min && min !== '') {
                        scope.yearsMaxOptions = yearsArray(min + 1);
                    } else {
                        scope.yearsMaxOptions = scope.yearsMinOptions;
                    }
                });

                scope.willSponsorVisaFieldValueConfigs = [
                    {
                        value: true,
                        localeKey: 'yes',
                    },
                    {
                        value: false,
                        localeKey: 'no',
                    },
                ];
            },
        };
    },
]);
