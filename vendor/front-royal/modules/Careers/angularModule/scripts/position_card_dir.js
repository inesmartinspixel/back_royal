import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/position_card.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('positionCard', [
    '$injector',

    function factory($injector) {
        const $sce = $injector.get('$sce');
        const TranslationHelper = $injector.get('TranslationHelper');

        return {
            scope: {
                position: '<',
                mini: '<?',
                showRecommendedBadge: '<?',
            },
            restrict: 'E',
            templateUrl,

            link(scope) {
                const translationHelper = new TranslationHelper('careers.position_card');

                // Some external positions have poorly formatted short descriptions.
                // We may want to tweak this more in the future but for now, we'll
                // consider anything that starts with an ellipsis to be unreadable
                // for our purposes.
                // We implicitly trust that any short_description on internal positions
                // is properly formatted.
                scope.shortDescriptionReadable = !!(
                    scope.position.short_description &&
                    (!scope.position.external || !/^\.\.\./.test(scope.position.short_description))
                );

                scope.getReadableShortDescription = () => {
                    if (scope.shortDescriptionReadable) {
                        return !scope.position.external
                            ? scope.position.short_description
                            : $sce.trustAsHtml(scope.position.short_description);
                    }
                    return translationHelper.get('fallback_short_description');
                };
            },
        };
    },
]);
