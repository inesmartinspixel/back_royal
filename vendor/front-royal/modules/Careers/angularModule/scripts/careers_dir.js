import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/careers.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('careers', [
    '$injector',
    function factory($injector) {
        const $location = $injector.get('$location');
        const $rootScope = $injector.get('$rootScope');
        const SiteMetadata = $injector.get('SiteMetadata');
        const isMobileMixin = $injector.get('isMobileMixin');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const TranslationHelper = $injector.get('TranslationHelper');
        const ConfigFactory = $injector.get('ConfigFactory');
        const RouteResolvers = $injector.get('Navigation.RouteResolvers');
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');

        return {
            restrict: 'E',
            scope: {
                _section: '@section',
            },
            templateUrl,

            link(scope) {
                scope.careersNetworkViewModel = CareersNetworkViewModel.get('candidate');

                // convenience getter
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'sectionNavigationTitle', {
                    get() {
                        // build up the section navigation title here
                        const parts = [translationHelper.get('careers_title')];

                        // note: don't try localize the section if not supplied or if it's the special section 'apply', which doesn't show the navigation at all
                        if (scope._section && scope._section !== 'apply') {
                            // section matches the locale entries, except for preview)

                            let sectionLocale = scope._section;

                            if (sectionLocale === 'card') {
                                sectionLocale = 'profile';
                            }

                            parts.push(translationHelper.get(sectionLocale));
                        }
                        return parts.join(' – ');
                    },
                });

                NavigationHelperMixin.onLink(scope);
                isMobileMixin.onLink(scope);

                // We hide the app header on mobile on this screen
                scope.$watch('isMobile', () => {
                    AppHeaderViewModel.toggleVisibility(!scope.isMobile);
                });

                // Setup localization keys
                var translationHelper = new TranslationHelper('careers.careers');

                AppHeaderViewModel.setTitleHTML(translationHelper.get('careers_title'));
                AppHeaderViewModel.setBodyBackground('beige');
                AppHeaderViewModel.showAlternateHomeButton = false;
                $injector.get('scrollHelper').scrollToTop();

                // Default title
                SiteMetadata.updateHeaderMetadata();

                scope.config = ConfigFactory.getSync();

                //----------------------------------------------------
                // Special Handling for Removal from Career Network
                //----------------------------------------------------

                function checkSectionAndCareerNetworkAccess() {
                    // prevent error on logout
                    if (!scope.currentUser) {
                        return;
                    }

                    // if you haven't applied or have access to careers, the only section you can access is the apply section
                    // Redirect you there if you're not already there.
                    if (!scope.currentUser.hasAppliedOrHasAccessToCareers && scope._section !== 'apply') {
                        scope.gotoSection('apply');
                        return;
                    }

                    // if you don't have career network access (and weren't already sent to apply), then the only other place
                    // you can be is the card section. Redirect you there if you're not already there.
                    if (!scope.currentUser.hasCareersNetworkAccess && scope._section !== 'card') {
                        scope.gotoSection('card');
                        return;
                    }

                    // if you don't have a complete career profile, then you can't go to positions
                    if (!scope.currentUser.hasCompleteCareerProfile && scope._section === 'positions') {
                        scope.gotoSection('card');
                    }
                }

                scope.canAccessCareersSection = section =>
                    RouteResolvers.canAccessCareersSection(scope.currentUser, section);

                // edge case: if a user sitting on a section like 'positions' and gets removed from the career network,
                // we need to watch for that and proactively move them back to the card section
                scope.$watchGroup(
                    [
                        'currentUser.hasCareersNetworkAccess',
                        'currentUser.hasAppliedOrHasAccessToCareers',
                        'currentUser.hasCompleteCareerProfile',
                    ],
                    checkSectionAndCareerNetworkAccess,
                );

                //-------------------------
                // Navigation / Actions
                //-------------------------

                scope.mobileState = {
                    expanded: false,
                };

                scope.gotoSection = section => {
                    // don't allow section navigation to invalid places
                    // reuse the RouteResolver here to know whether we can go there
                    if (!scope.canAccessCareersSection(section)) {
                        return;
                    }

                    $location.url(`/careers/${section}`);
                };

                // This is a terrible hack, probably, but we need to
                // break the digest so we don't get a 10 digest error.
                // This pushes the rendering of the particular section
                // into a subsequent digest.
                scope.$watch('_section', section => {
                    $injector
                        .get('$timeout')()
                        .then(() => {
                            scope.section = section;
                        });
                });

                scope.$on('$destroy', () => {
                    AppHeaderViewModel.toggleVisibility(true);
                });
            },
        };
    },
]);
