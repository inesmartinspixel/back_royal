import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_career_profile.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('editCareerProfile', [
    '$injector',
    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const TranslationHelper = $injector.get('TranslationHelper');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const DialogModal = $injector.get('DialogModal');
        const scopeTimeout = $injector.get('scopeTimeout');
        const SiteMetadata = $injector.get('SiteMetadata');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const scrollHelper = $injector.get('scrollHelper');
        const $location = $injector.get('$location');
        const CareerProfile = $injector.get('CareerProfile');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
        const ClientStorage = $injector.get('ClientStorage');
        const ConfigFactory = $injector.get('ConfigFactory');
        const FixViewportOnBlur = $injector.get('FixViewportOnBlur');
        const $q = $injector.get('$q');

        return {
            restrict: 'E',
            scope: {
                steps: '<',
                initialStep: '<?',
                user: '<?', // the user whose profile to edit (defaults to currentUser if not provided),
                stepsProgressMap: '<?',
                goToStep: '&?',
            },
            templateUrl,

            link(scope, elem) {
                NavigationHelperMixin.onLink(scope);
                scope.proxy = {};
                scope.helper = EditCareerProfileHelper;
                const translationHelper = new TranslationHelper('careers.edit_career_profile');

                AppHeaderViewModel.setBodyBackground('beige');
                AppHeaderViewModel.showAlternateHomeButton = false;

                // the applicants editor doesn't need to scroll...
                if (!EditCareerProfileHelper.isApplicantEditor()) {
                    scrollHelper.scrollToTop();
                }

                // Default title
                SiteMetadata.updateHeaderMetadata();
                ConfigFactory.getConfig().then(config => {
                    scope.gdprAppliesToUser = config.gdprAppliesToUser();
                });

                //-----------------------------
                // Profile Data
                //-----------------------------

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        if (scope.user) {
                            return scope.user;
                        }
                        return $rootScope.currentUser;
                    },
                });

                scope.$watch('currentUser', () => {
                    if (scope.currentUser && scope.currentUser.career_profile) {
                        // Since scope.careerProfile is a copy of the currentUser's career_profile, it acts as a proxy,
                        // allowing us to easily throw away changes if the user dirties the form and then navigates away
                        // to another step without mucking up the career_profile on the currentUser.
                        // NOTE: Since this $watch on the currentUser only triggers if the REFERENCE to currentUser changes,
                        // this line should theoretically only run once, which is when this directive first renders.
                        scope.careerProfile = CareerProfile.new(scope.currentUser.career_profile.asJson());

                        scope.$watch('steps', setupFormSteps);
                    }
                });

                //-----------------------------
                // Multi-Step Form Config
                //-----------------------------

                function getCurrentPage() {
                    return parseInt($location.search().page, 10) || 1;
                }

                function setupFormSteps(newValue, oldValue) {
                    if (
                        !scope.careerProfile ||
                        !scope.steps ||
                        (scope.stepsInfo && JSON.stringify(newValue) === JSON.stringify(oldValue))
                    ) {
                        return;
                    }

                    // it's important to re-build steps in an async manner such that the multi-form-container is re-initialized
                    scope.stepsInfo = undefined;
                    scopeTimeout(scope, () => {
                        scope.stepsInfo = _.chain(scope.steps)
                            .pluck('stepName')
                            .map(section => {
                                // return a template that passes along necessary model info
                                // NOTE: all of these directives will use the shared scope
                                const directiveName = `${section.replace(/_/g, '-').snakeCase('-')}-form`;
                                const templateStr = `<${directiveName}></${directiveName}>`;
                                const title = translationHelper.get(section);

                                return {
                                    key: section,
                                    template: templateStr,
                                    title,
                                    hasForm: true,
                                    data: {
                                        stepName: section,
                                    },
                                };
                            })
                            .value();
                    });
                }

                //-----------------------------
                // Step Navigation
                //-----------------------------

                // see also: form_helper.js::supportForm
                const formListener = scope.$on('formHelperCurrentForm', (evt, formController) => {
                    scope.currentForm = formController;
                });
                scope.$on('$destroy', formListener);

                function scrollToContentTop() {
                    scopeTimeout(scope, () => {
                        const mainBox = $('.main-box');
                        const isFixed = mainBox.css('position') === 'fixed';
                        scrollHelper.scrollToTop(false, isFixed ? mainBox : undefined);
                    });
                }

                function updateLocationForIndex(index) {
                    if (EditCareerProfileHelper.isApplicantEditor()) {
                        $location.search('page', index + 1);
                    } else {
                        $location.url(`${EditCareerProfileHelper.getRootUrl()}?page=${index + 1}`);
                        scrollToContentTop();
                    }
                }

                // on scope for testing
                scope.gotoFormStep = index => {
                    scope.proxy.mobileNavExpanded = false;

                    if (getCurrentPage() - 1 === index) {
                        return;
                    }

                    updateLocationForIndex(index);
                };

                // This has to support the case of the user clicking the "Save and Next" button,
                // clicking one of the nav buttons, or clicking back on the browser.
                scope.$on('$locationChangeStart', event => {
                    // if navigating away from currentForm, delete invalidFields
                    delete scope.invalidFields;

                    // NOTE: we check for dirtied forms beyond just `currentForm`
                    // because we have sub-components that might be registering forms for
                    // their own validation, etc - which could be dirtied as well
                    if (elem.find('form.ng-dirty').length > 0) {
                        // Surprisingly, $location.url() already has the new location at this point,
                        // even though we can still cancel the navigation with event.preventDefault().
                        // We could also grab the target location from the second argument to the $locationChangeStart
                        // callback, but that would give us the full url with the hostname and all, which
                        // is less convenient.
                        const targetLocation = $location.url();
                        event.preventDefault();

                        DialogModal.confirm({
                            text: translationHelper.get('unsaved_changes_confirm'),
                            confirmCallback() {
                                scope.careerProfile = CareerProfile.new(scope.currentUser.career_profile.asJson());
                                scope.currentForm.$setPristine(true);
                                $location.url(targetLocation);
                            },
                        });
                    }
                });

                // HACK: in Facebook in-app browser, we need to guard against viewport bugs
                scope.$on('$locationChangeSuccess', () => {
                    FixViewportOnBlur.installBlurListenersOnLink(scope, elem);
                });
                FixViewportOnBlur.installBlurListenersOnLink(scope, elem);

                scope.$on('gotoFormStep', (ev, index) => {
                    scope.gotoFormStep(index);
                });

                scope.$on('gotoEditProfileSection', (ev, section) => {
                    const index = _.findIndex(scope.steps, step => step.stepName === section);
                    scope.gotoFormStep(index);
                });

                // set the initial page if provided directly
                if (scope.initialStep) {
                    $location.search({
                        page: scope.initialStep,
                    });
                }

                //-----------------------------
                // Form Submission
                //-----------------------------

                function showValidity() {
                    scope.currentForm.$setSubmitted(true);
                    return scope.currentForm.$valid;
                }

                let programTypePriorToSave;

                scope.save = () => scope.saveForm().catch(angular.noop);

                scope.saveForm = () => {
                    if (!showValidity()) {
                        scope.invalidFields =
                            (scope.currentForm.$error.required &&
                                scope.currentForm.$error.required
                                    .concat(scope.currentForm.$error['required-range'])
                                    .filter(f => typeof f === 'object')) ||
                            scope.currentForm.$error['required-range'];
                        return $q.reject();
                    }
                    delete scope.invalidFields;

                    ClientStorage.setItem(`saved_${scope.currentForm.$name}`, true);

                    // I started to use the ProfileCompletionHelper here to check for completion, but then I realized
                    // that a user can't save a step if it isn't valid in the first place.
                    scope.helper.logEventForApplication(`completed-${scope.currentForm.$name}`);

                    return scope.careerProfile.save().then(() => {
                        // After the save is successful, we need to set the form back to a pristine state so the form's
                        // $dirty state also gets set to false. Otherwise, even if the save is successfully complete,
                        // the confirmation dialog modal may pop up if the user tries to navigate to a different section
                        // because the form's $dirty state wasn't reset.
                        scope.currentForm.$setPristine();

                        if (scope.currentUser) {
                            // we haven't reset scope.currentUser.career_profile to a copy of the careerProfile proxy yet,
                            // so we can still get the program_type value prior to the save from scope.currentUser.career_profile
                            programTypePriorToSave = scope.currentUser.career_profile.program_type;

                            // We still want scope.careerProfile to act as a proxy (see the $watch on currentUser above),
                            // so we reset scope.currentUser.career_profile to a COPY of scope.careerProfile, which ensures
                            // that scope.currentUser.career_profile isn't a reference to scope.careerProfile, thus maintaining
                            // it's behavior as a proxy.
                            scope.currentUser.career_profile = CareerProfile.new(scope.careerProfile.asJson());
                        }

                        // see admin_edit_career_profile_dir.js and settings_dir.js for where we listen to this
                        scope.$emit('savedCareerProfile');

                        // see mba_application_questions_form_dir.js, emba_application_questions_form_dir.js, and program_choice_form_dir.js
                        scope.$broadcast('savedCareerProfile', programTypePriorToSave);
                    });
                };

                scope.saveAndNext = nextStepCallback => {
                    let currentFormWasIncompletePriorToSave;
                    if (EditCareerProfileHelper.isCareerProfile()) {
                        currentFormWasIncompletePriorToSave = scope.currentFormIsIncomplete();
                    }

                    scope
                        .saveForm()
                        .then(() => {
                            // When we're in the career profile context, if the user has any incomplete form steps,
                            // navigate them to the next incomplete form step from the current form step or back
                            // to the first incomplete form step if no incomplete form steps can be found after
                            // the current form step.
                            if (EditCareerProfileHelper.isCareerProfile()) {
                                const nextIncompleteStepIndex = scope.getNextIncompleteFormStepIndex();
                                if (nextIncompleteStepIndex > -1) {
                                    scope.gotoFormStep(nextIncompleteStepIndex);
                                    return;
                                }
                                if (currentFormWasIncompletePriorToSave) {
                                    scope.gotoFormStep(scope.steps.length - 1);
                                    return;
                                }
                            }

                            if (nextStepCallback) {
                                nextStepCallback();
                                scrollToContentTop();
                            } else if (EditCareerProfileHelper.isApplication()) {
                                scope.loadRoute(
                                    `${EditCareerProfileHelper.getRootUrl()}?page=${scope.steps.length}` - 1,
                                );
                            }
                        })
                        .catch(angular.noop);
                };

                scope.currentFormIsIncomplete = () => {
                    if (!scope.stepsProgressMap) {
                        throw new Error(
                            'scope.stepsProgressMap must be defined to determine if the form was incomplete prior to save.',
                        );
                    }

                    const currentPage = getCurrentPage();
                    const currentStep = scope.steps[currentPage - 1];
                    return scope.stepsProgressMap[currentStep.stepName] === 'incomplete';
                };

                scope.getNextIncompleteFormStepIndex = () => {
                    if (!scope.stepsProgressMap) {
                        throw new Error(
                            'scope.stepsProgressMap must be defined to determine the next incomplete form step.',
                        );
                    }

                    const currentPage = getCurrentPage(); // 1-indexed

                    // If the user isn't on the last page, check if any of the form steps after
                    // the currentPage are incomplete and take them to the first one that we find.
                    for (let i = currentPage; i < scope.steps.length; i++) {
                        if (scope.stepsProgressMap[scope.steps[i].stepName] === 'incomplete') {
                            return i;
                        }
                    }

                    // Otherwise, check if any of the form steps before the currentPage are incomplete.
                    for (let i = 0; i < currentPage - 1; i++) {
                        if (scope.stepsProgressMap[scope.steps[i].stepName] === 'incomplete') {
                            return i;
                        }
                    }

                    return -1;
                };

                // If users are reapplyingOrEditingApplication then they have the option to cancel reapplying and go back to the application status
                scope.cancel = () => {
                    scope.currentUser.reapplyingOrEditingApplication = false;
                    $injector.get('$location').path('/settings/application_status');
                };
            },
        };
    },
]);
