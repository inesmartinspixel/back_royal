import angularModule from 'Careers/angularModule/scripts/careers_module';

angularModule.factory('PositionsWithInterestsHelper', [
    '$injector',

    $injector => {
        const SuperModel = $injector.get('SuperModel');

        const PositionsWithInterestsHelper = SuperModel.subclass(function () {
            this.extend({
                // Loads the data necessary for the PositionsWithInterestsHelper service and returns a promise that,
                // when resolved, returns a new instance of the class.
                loadPositionsAndInterests(careersNetworkViewModel) {
                    return careersNetworkViewModel
                        .ensureOpenPositions()
                        .then(() => careersNetworkViewModel.ensureCandidatePositionInterests())
                        .then(() => new PositionsWithInterestsHelper(careersNetworkViewModel));
                },
            });

            return {
                initialize(careersNetworkViewModel) {
                    this.careersNetworkViewModel = careersNetworkViewModel;
                    this.caches = {
                        interestsByPositionId: {},
                        numReviewed: {},
                        numUnreviewed: {},
                        numNewCandidates: {},
                    };
                },

                hideRelatedCandidatePositionInterests(hiringRelationshipViewModel) {
                    return this.careersNetworkViewModel
                        .hideRelatedCandidatePositionInterests(hiringRelationshipViewModel)
                        .then(relatedInterests => {
                            _.each(relatedInterests, relatedInterest => {
                                const position = _.findWhere(this.careersNetworkViewModel.openPositions, {
                                    id: relatedInterest.open_position_id,
                                });
                                this.clearInterestCachesForPosition(position);
                            });
                        });
                },

                interests(position) {
                    if (!this.caches.interestsByPositionId[position.id]) {
                        this.caches.interestsByPositionId[position.id] = _.chain(
                            this.careersNetworkViewModel.candidatePositionInterests,
                        )
                            .where({
                                open_position_id: position.id,
                            })
                            .sortBy('created_at')
                            .value();
                    }
                    return this.caches.interestsByPositionId[position.id];
                },

                numInterests(position) {
                    return this.interests(position).length;
                },

                numReviewed(position) {
                    if (!this.caches.numReviewed[position.id]) {
                        this.caches.numReviewed[position.id] = _.chain(this.interests(position))
                            .where({
                                reviewedByHiringManager: true,
                            })
                            .size()
                            .value();
                    }
                    return this.caches.numReviewed[position.id];
                },

                numUnreviewed(position) {
                    if (!this.caches.numUnreviewed[position.id]) {
                        this.caches.numUnreviewed[position.id] = _.chain(this.interests(position))
                            .where({
                                reviewedByHiringManager: false,
                            })
                            .size()
                            .value();
                    }
                    return this.caches.numUnreviewed[position.id];
                },

                numNewCandidates(position) {
                    if (!this.caches.numNewCandidates[position.id]) {
                        this.caches.numNewCandidates[position.id] = _.chain(this.interests(position))
                            .where({
                                hiring_manager_status: 'unseen',
                            })
                            .size()
                            .value();
                    }
                    return this.caches.numNewCandidates[position.id];
                },

                hasNewCandidates(position) {
                    return this.numNewCandidates(position) > 0;
                },

                sortOpenPositions(filter) {
                    let positions = this.careersNetworkViewModel.openPositions;
                    if (filter) {
                        positions = _.filter(this.careersNetworkViewModel.openPositions, filter);
                    }
                    const sortedList = _.groupBy(positions, position => {
                        // we only want new messages from connections that are ongoing
                        // conversations where both parties have sent at least one message
                        if (this.hasNewCandidates(position)) {
                            return 'unseenInterests';
                        }
                        if (this.numInterests(position) > 0) {
                            return 'seenInterests';
                        }
                        return 'remaining';
                    });

                    return (
                        _.sortBy(sortedList.unseenInterests, position => this.numNewCandidates(position)).reverse() ||
                        []
                    )
                        .concat(
                            _.sortBy(sortedList.seenInterests, position => this.numInterests(position)).reverse() || [],
                        )
                        .concat(_.sortBy(sortedList.remaining, 'created_at').reverse() || []);
                },

                // clears all of the candidate poition interest related cached values for the passed in position
                clearInterestCachesForPosition(position) {
                    Object.keys(this.caches).forEach(key => {
                        delete this.caches[key][position.id];
                    });
                },
            };
        });

        return PositionsWithInterestsHelper;
    },
]);
