import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/cover_letter.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import sharedHiringRelationshipCheck from 'vectors/shared-hiring-relationship-check.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('coverLetter', [
    '$injector',

    function factory($injector) {
        const dateHelper = $injector.get('dateHelper');
        const $timeout = $injector.get('$timeout');
        const DialogModal = $injector.get('DialogModal');

        return {
            scope: {
                coverLetter: '<?',
                avatarUrl: '<?',
                submit: '<?',
                hiringApplication: '<?',
            },
            restrict: 'E',
            templateUrl,
            link(scope) {
                scope.sharedHiringRelationshipCheck = sharedHiringRelationshipCheck;

                scope.createdAt =
                    scope.coverLetter &&
                    dateHelper.formattedUserFacingMonthDayYearLong(scope.coverLetter.created_at, false);

                scope.showToggle =
                    scope.coverLetter && scope.coverLetter.content && scope.coverLetter.content.length > 400;

                scope.showForm = !!scope.submit;

                if (scope.showForm && scope.coverLetter) {
                    scope.content = scope.coverLetter.content;
                }

                scope.formSubmit = () => {
                    scope.showCheck = true;

                    scope.submit({
                        content: scope.content,
                        created_at: Date.now(),
                    });

                    $timeout(() => {
                        DialogModal.hideAlerts();
                    }, 1750);
                };

                scope.minlength = 100;
            },
        };
    },
]);
