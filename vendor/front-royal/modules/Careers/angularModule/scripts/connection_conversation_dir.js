import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/connection_conversation.html';
import conversationEmptyTemplate from 'Careers/angularModule/views/connection_conversation_empty.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import matchAndChat from 'images/match_and_chat.png';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('connectionConversation', [
    '$injector',

    function factory($injector) {
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const TranslationHelper = $injector.get('TranslationHelper');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const $rootScope = $injector.get('$rootScope');

        return {
            scope: {
                hiringRelationshipViewModel: '<',
                role: '<',
            },
            restrict: 'E',
            templateUrl,

            link(scope) {
                scope.matchAndChat = matchAndChat;
                scope.conversationEmptyTemplate = conversationEmptyTemplate;

                const translationHelper = new TranslationHelper('careers.connection_conversation');
                scope.declineKey =
                    scope.role === 'hiringManager' ||
                    (scope.role === 'candidate' && !scope.hiringRelationshipViewModel.newRequest)
                        ? 'careers.connection_conversation.close'
                        : 'careers.connection_conversation.decline';
                scope.backButtonKey =
                    scope.role === 'hiringManager'
                        ? 'careers.connection_conversation.back_to_tracker'
                        : 'careers.connection_conversation.back_to_connections';

                scope.$watch('hiringRelationshipViewModel.conversationViewModel', conversationViewModel => {
                    scope.conversationViewModel = conversationViewModel;
                    scope.conversation = conversationViewModel && conversationViewModel.conversation;
                });

                // on a pushed update, the actual conversation object could be swapped out.  So only
                // watch for the id the change.  When it does, initialize the visible messages to be all
                // messages.  Any messages that come in while we're on this page will be hidden until
                // the user presses tap-to-show
                scope.$watch('hiringRelationshipViewModel.conversation.id', showAllMessages);

                scope.$watch('showMessagesUpTo', filterMessages);
                scope.$watchCollection('conversation.messages', filterMessages);

                // this has to be a separate watch because the conversation can change
                // when the hiringRelationshipViewModel
                // does not (due to the CareersNetworkViewModel pulling new data)
                scope.$watch('hiringRelationshipViewModel.conversationViewModel.conversation', setConversationAndBadge);

                scope.$watch('hiringRelationshipViewModel', setTitleHTML);

                scope.showAllMessages = showAllMessages;
                scope.openLink = url => {
                    NavigationHelperMixin.loadUrl(url, '_blank');
                };

                // When new messages are sent while we are on this page, we do not show
                // them immediately.  Instead we show a "Tap to show" button and hide the
                // new messages until the button is pressed.  If THIS user sends a message,
                // however, we want to show it right away.
                function filterMessages() {
                    const messages = scope.conversation && scope.conversation.messages;
                    scope.visibleMessages = _.select(
                        messages,
                        message =>
                            message.created_at <= scope.showMessagesUpTo ||
                            message.sender_id === $rootScope.currentUser.id,
                    );
                }

                function markAllAsReadIfNecessary() {
                    const hiringRelationshipViewModel = scope.hiringRelationshipViewModel;
                    if (hiringRelationshipViewModel && hiringRelationshipViewModel.conversationViewModel) {
                        hiringRelationshipViewModel.markAllAsRead();
                    }
                }

                function setConversationAndBadge(conversation) {
                    if (!scope.hiringRelationshipViewModel) {
                        scope.conversation = null;
                        scope.badge = null;
                        return;
                    }
                    scope.conversation = conversation;

                    if (scope.hiringRelationshipViewModel.closed) {
                        scope.badge = {
                            class: 'declined',
                            localeKey: 'careers.connection_conversation.closed',
                        };
                    } else if (scope.hiringRelationshipViewModel.newMatch) {
                        scope.badge = {
                            class: 'new-match',
                            localeKey: 'careers.connection_conversation.new_match',
                        };
                    } else if (scope.hiringRelationshipViewModel.newRequest) {
                        scope.badge = {
                            class: 'new-request',
                            localeKey: 'careers.connection_conversation.new_request',
                        };
                    } else if (scope.hiringRelationshipViewModel.newInstantMatch) {
                        scope.badge = {
                            class: 'new-instant-match',
                            localeKey: 'careers.connection_conversation.new_match',
                        };
                    } else if (scope.hiringRelationshipViewModel.rejectedByCandidate) {
                        scope.badge = {
                            class: 'declined',
                            localeKey: 'careers.connection_conversation.declined',
                        };
                    } else {
                        scope.badge = {
                            class: 'connected',
                            localeKey: 'careers.connection_conversation.connected',
                        };
                    }
                }

                function setTitleHTML(hiringRelationshipViewModel) {
                    if (hiringRelationshipViewModel) {
                        // set up proper title
                        AppHeaderViewModel.setTitleHTML(
                            translationHelper.get('connections_employer_conversation_title', {
                                name: scope.hiringRelationshipViewModel.connectionName,
                                company: scope.hiringRelationshipViewModel.connectionCompany,
                            }),
                        );
                    }
                }

                function showAllMessages() {
                    if (scope.conversation) {
                        scope.showMessagesUpTo = scope.conversation.messages[0].created_at;
                        markAllAsReadIfNecessary();
                    } else {
                        scope.showMessagesUpTo = null;
                    }
                }
            },
        };
    },
]);
