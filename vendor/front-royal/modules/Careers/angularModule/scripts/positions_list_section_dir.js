import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/positions_list_section.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('positionsListSection', [
    '$injector',

    function factory($injector) {
        const safeApply = $injector.get('safeApply');
        const $window = $injector.get('$window');
        const scrollHelper = $injector.get('scrollHelper');
        const $timeout = $injector.get('$timeout');
        const $rootScope = $injector.get('$rootScope');

        return {
            scope: {
                expandedSections: '<',
                sectionName: '<',
                positions: '<',
                stickyHeaders: '<',
                selectedTeamMemberId: '<?',
                searchText: '<?',
                positionsWithInterestsHelper: '<?',
            },
            restrict: 'E',
            templateUrl,

            link(scope, elem) {
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                //------------------------
                // Filtering candidates
                //------------------------
                function hasSelectedTeamMember(position) {
                    if (!scope.selectedTeamMemberId || scope.selectedTeamMemberId === 'all') {
                        return true;
                    }
                    return position.hiring_manager_id === scope.selectedTeamMemberId;
                }

                function matchesSearchText(position) {
                    return !scope.searchText || position.searchableText.includes(scope.searchText.toLowerCase());
                }

                function updateFiltering() {
                    const oldLength = (scope.positions || []).length;
                    scope.filteredPositions = _.chain(scope.positions)
                        .filter(matchesSearchText)
                        .filter(hasSelectedTeamMember)
                        .value();

                    const newLength = (scope.positions || []).length;

                    // trigger sticky calculation if it changed
                    if (oldLength !== newLength) {
                        $timeout(updateSticky);
                    }
                }

                scope.$watchGroup(['selectedTeamMemberId', 'searchText'], updateFiltering);
                scope.$watchCollection('positions', updateFiltering);
                scope.$watch('expandedSections[sectionName].expanded', expanded => {
                    scope.expanded = expanded;
                });

                updateFiltering();

                //------------------------
                // View Helpers
                //------------------------

                scope.clickRow = () => {
                    const entry = scope.expandedSections && scope.expandedSections[scope.sectionName];

                    if (scope.stickyBottom) {
                        // first, we always open the element, then we scroll to it
                        entry.expanded = true;
                        $timeout(() => {
                            scrollHelper.scrollToElement($(elem), true, -51, $(scrollableContainer), 1000);
                        });
                    } else {
                        // toggle expansion state
                        entry.expanded = !entry.expanded;
                        // it's possible we move a section by doing this, so trigger the update functions after re-render
                        $timeout(() => {
                            $(scrollableContainer).trigger('scroll');
                        });
                    }
                };

                //------------------------
                // Sticky Scrolling Helpers
                //------------------------

                // for webkit browsers (Chrome, Safari) we seem to have to take into account the scrollbar when using
                // position: fixed; Detect it here as reliably as we can. Tricky because IE/Edge tries to impersonate webkit.
                const ua = window.navigator.userAgent;
                scope.isWebkit = ua.includes('AppleWebKit/') && !ua.includes('Edge') && !ua.includes('MSIE');

                // get the scrollable main container element
                var scrollableContainer = scrollHelper.container();

                // cache the sticky values so we don't unnecessarily trigger digests / DOM updates
                let lastStickyBottomValue;

                let lastStickyTopValue;

                function updateSticky() {
                    const offset = $(elem).offset(); // offset of the header element from the top of the viewport
                    const sectionHeaderHeight = 50; // matches the CSS
                    const appHeaderHeight = 75; // matches the CSS
                    const windowWidth = $window.innerWidth;

                    const offsetFromBottom = offset.top - $(window).height() + sectionHeaderHeight;
                    const offsetFromTop = offset.top - appHeaderHeight;
                    const stickyEnabled = windowWidth >= 800 && scope.stickyHeaders; // we disable sticky features when we're narrower than 800px

                    const stickyBottom = offsetFromBottom > 0 && stickyEnabled;
                    const stickyTop = offsetFromTop < 0 && stickyEnabled;
                    let changed = false;

                    // update sticky bottom if necessary
                    if (stickyBottom !== lastStickyBottomValue) {
                        $(elem).toggleClass('sticky-bottom', stickyBottom);
                        lastStickyBottomValue = stickyBottom;
                        changed = true;
                    }

                    // update sticky top if necessary
                    if (stickyTop !== lastStickyTopValue) {
                        $(elem).toggleClass('sticky-top', stickyTop);
                        lastStickyTopValue = stickyTop;
                        changed = true;
                    }

                    // update scope and apply if anything changed
                    if (changed) {
                        scope.stickyBottom = stickyBottom;
                        safeApply(scope);
                    }
                }

                // setup listeners
                $(scrollableContainer).on(`scroll.${scope.id}`, updateSticky);
                $($window).on(`resize.${scope.$id}`, updateSticky);

                // initially setup sticky classes
                $timeout(updateSticky);

                // cleanup listeners
                scope.$on('$destroy', () => {
                    $($window).off(`resize.${scope.$id}`);
                    $(scrollableContainer).off(`scroll.${scope.id}`);
                });
            },
        };
    },
]);
