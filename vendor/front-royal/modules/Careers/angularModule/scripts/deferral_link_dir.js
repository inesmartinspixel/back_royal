import angularModule from 'Careers/angularModule/scripts/careers_module';
import { setupBrandEmailProperties } from 'AppBrandMixin';
import template from 'Careers/angularModule/views/deferral_link.html';
import cacheAngularTemplate from 'cacheAngularTemplate';
import moment from 'moment-timezone';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('deferralLink', [
    '$injector',

    function factory($injector) {
        const TranslationHelper = $injector.get('TranslationHelper');
        const DeferralLink = $injector.get('DeferralLink');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const dateHelper = $injector.get('dateHelper');
        const Cohort = $injector.get('Cohort');
        const CohortApplication = $injector.get('CohortApplication');
        const $rootScope = $injector.get('$rootScope');
        const ErrorLogService = $injector.get('ErrorLogService');
        const User = $injector.get('User');

        return {
            scope: {
                id: '@',
            },
            restrict: 'E',
            templateUrl,
            controllerAs: 'controller',

            link(scope) {
                NavigationHelperMixin.onLink(scope);
                setupBrandEmailProperties($injector, scope, ['mba', 'emba']);
                const translationHelper = new TranslationHelper('careers.deferral_link');
                scope.loadingDeferralLink = true;

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.proxyModel = {
                    toCohortId: undefined,
                    billingConfirmation: undefined,
                };

                scope.$watch('proxyModel.toCohortId', id => {
                    if (id) {
                        const upcomingCohort = scope.upcomingCohorts.find(cohort => cohort.id === id);
                        scope.billingConfirmationText = translationHelper.get('billing_confirmation', {
                            trialEndDate: dateHelper.formattedUserFacingMonthDayYearLong(upcomingCohort.trialEndDate),
                        });
                    }
                });

                DeferralLink.show(scope.id).then(response => {
                    scope.deferralLink = response.result;

                    if (!scope.deferralLink?.active) {
                        scope.gotoHome();
                        return;
                    }

                    const fromCohortApplication = scope.deferralLink.from_cohort_application;
                    const fromCohort = response.meta.from_cohort;

                    scope.upcomingCohorts = response.meta.upcoming_cohorts.map(cohortJson => Cohort.new(cohortJson));
                    scope.cohortOptions = scope.upcomingCohorts.map(cohort => {
                        return {
                            id: cohort.id,
                            label: `${cohort.name} starting ${dateHelper.formattedUserFacingMonthDayYearLong(
                                cohort.start_date * 1000,
                            )}`,
                        };
                    });

                    scope.shouldShowBillingConfirmation =
                        !!scope.currentUser.primarySubscription ||
                        User.willNeedSubscription(scope.currentUser, fromCohort, fromCohortApplication);

                    // Note: For now we'll only be using this for Quantic MBA and EMBA programs. If that ever changes
                    // we'll need to make this logic smarter.
                    const programType = fromCohortApplication.program_type;
                    if (programType === 'emba') {
                        scope.supportEmail = scope.brandEmbaEmail;
                    } else if (programType === 'mba') {
                        scope.supportEmail = scope.brandMbaEmail;
                    } else {
                        ErrorLogService.notify('Trying to show support email for an unknown program type');
                    }

                    scope.loadingDeferralLink = false;
                });

                scope.useDeferralLink = (deferralLink, toCohortId) => {
                    scope.usingDeferralLink = true;
                    scope.deferralLink.used = true;
                    deferralLink
                        .save({
                            to_cohort_id: toCohortId,
                        })
                        .then(response => {
                            scope.toCohortApplication = CohortApplication.new(response.meta.to_cohort_application);
                            scope.calendarEventStartDate = moment(scope.toCohortApplication.cohortStartDate)
                                .startOf('day')
                                .toDate();
                            scope.calendarEventEndDate = moment(scope.toCohortApplication.cohortStartDate)
                                .add(1, 'days')
                                .startOf('day')
                                .toDate();
                            scope.calendarEventDescription = translationHelper.get('calendar_event_description', {
                                cohortTitle: scope.toCohortApplication.cohort_title,
                            });
                            scope.usingDeferralLink = false;
                        });
                };
            },
        };
    },
]);
