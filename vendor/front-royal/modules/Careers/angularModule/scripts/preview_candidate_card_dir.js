import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/preview_candidate_card.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('previewCandidateCard', [
    '$injector',
    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
        const $location = $injector.get('$location');

        return {
            restrict: 'E',
            scope: {},
            templateUrl,

            link(scope) {
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.$on('gotoEditProfileSection', (ev, section) => {
                    const steps = EditCareerProfileHelper.getStepsForCareersForm(scope.currentUser);
                    const index = _.findIndex(steps, step => step.stepName === section);
                    $location.url(`/settings/my-profile?page=${index + 1}`);
                });
            },
        };
    },
]);
