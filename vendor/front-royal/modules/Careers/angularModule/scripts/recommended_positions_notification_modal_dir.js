import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/recommended_positions_notification_modal.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('recommendedPositionsNotificationModal', [
    '$injector',
    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');

        return {
            restrict: 'E',
            scope: {
                onFinish: '<',
            },
            templateUrl,

            link(scope) {
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.setting = scope.currentUser.notify_candidate_positions_recommended;

                scope.save = function () {
                    scope.currentUser.notify_candidate_positions_recommended = scope.setting;
                    scope.currentUser.save().then(() => {
                        scope.onFinish();
                    });
                };
            },
        };
    },
]);
