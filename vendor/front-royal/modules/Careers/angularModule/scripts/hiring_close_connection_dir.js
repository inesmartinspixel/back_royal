import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/hiring_close_connection.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('hiringCloseConnection', [
    '$injector',
    function factory($injector) {
        const DialogModal = $injector.get('DialogModal');
        const FormHelper = $injector.get('FormHelper');

        return {
            restrict: 'E',
            scope: {
                hiringRelationshipViewModel: '<',
            },
            templateUrl,

            link(scope) {
                scope.closedInfo = {
                    reasons: [],
                    feedback: undefined,
                };

                scope.reasonKeys = [
                    'never_connected_or_interviewed',
                    'hired_someone_else',
                    'experience_level_too_senior',
                    'experience_level_too_junior',
                    'experience_not_relevant',
                    'interview_performance_or_fit',
                ];

                const propsValuesMap = {
                    reasons: scope.reasonKeys,
                };

                const propsRequiredFieldsMap = {
                    reasons: {
                        min: 1,
                    },
                };

                FormHelper.supportCheckboxGroups(scope, scope.closedInfo, propsValuesMap, propsRequiredFieldsMap);

                scope.decline = () => {
                    scope.hiringRelationshipViewModel.hiringRelationship.hiring_manager_closed_info = scope.closedInfo;
                    scope.hiringRelationshipViewModel.close();
                    scope.hiringRelationshipViewModel.navigateToTracker();

                    scope.declining = true;
                    scope.hiringRelationshipViewModel.save().then(() => {
                        scope.declining = false;
                        DialogModal.hideAlerts();
                    });
                };
            },
        };
    },
]);
