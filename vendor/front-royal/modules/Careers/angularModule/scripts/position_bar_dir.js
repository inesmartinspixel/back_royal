import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/position_bar.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('positionBar', [
    '$injector',

    function factory($injector) {
        const $location = $injector.get('$location');
        const $rootScope = $injector.get('$rootScope');

        return {
            scope: {
                position: '<',
                positionsWithInterestsHelper: '<',
            },
            restrict: 'E',
            templateUrl,

            link(scope) {
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.editPosition = () => {
                    $location.search('positionId', scope.position.id);
                    $location.search('action', 'edit');
                };

                scope.reviewCandidates = () => {
                    $location.search('positionId', scope.position.id);
                    $location.search('action', 'review');
                };
            },
        };
    },
]);
