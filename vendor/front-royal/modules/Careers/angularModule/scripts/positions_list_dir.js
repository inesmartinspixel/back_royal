import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/positions_list.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import files from 'vectors/files.svg';
import archive from 'vectors/archive.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('positionsList', [
    '$injector',

    function factory($injector) {
        const $location = $injector.get('$location');
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const TranslationHelper = $injector.get('TranslationHelper');
        const PositionsWithInterestsHelper = $injector.get('PositionsWithInterestsHelper');

        return {
            scope: {
                archived: '<',
            },
            restrict: 'E',
            templateUrl,

            link(scope) {
                scope.files = files;
                scope.archive = archive;

                NavigationHelperMixin.onLink(scope);

                const careersNetworkViewModel = CareersNetworkViewModel.get('hiringManager');

                PositionsWithInterestsHelper.loadPositionsAndInterests(careersNetworkViewModel).then(
                    _positionsWithInterestsHelper => {
                        scope.positionsWithInterestsHelper = _positionsWithInterestsHelper;
                        scope.setupWatchers();
                    },
                );

                // on scope for easier testing
                scope.setupWatchers = () => {
                    // Watch for the hiring manager archiving a position. If that happens, we want the UI to reflect
                    // the appropriate data changes.
                    // NOTE: we are just watching archived here, but the length of this
                    // array can give us insight into inserts/deletes as well
                    scope.$watchCollection(
                        () => _.pluck(careersNetworkViewModel.openPositions, 'archived'),
                        () => {
                            // iterate through positions and associate them with their
                            // respective interests
                            _.each(careersNetworkViewModel.openPositions, position => {
                                scope.positionsWithInterestsHelper.clearInterestCachesForPosition(position);
                            });

                            // it's important that we call getPositionLists after we call clearInterestCachesForPosition
                            // for each of the openPositions on the careersNetworkViewModel so that the appropriate values
                            // is calculated.
                            getPositionLists();
                        },
                    );
                };

                scope.addOrEditPosition = positionId => {
                    if (positionId) {
                        $location.search('positionId', positionId);
                    } else {
                        $location.search('action', 'create');
                    }
                    $location.search('list', null);
                };

                scope.setSearchFocused = searchFocused => {
                    scope.searchFocused = searchFocused;
                };

                // Each of these sections has a listName, indicating
                // which list needs to be sent into PositionsViewModal.ensureHiringRelationshipsLoaded
                // in order to load up the relationships for that list.
                //
                // The 'myCandidates' list will be preloaded by the PositionsViewModal,
                // (see comments in _preloadHiringRelationships).
                //
                // On the 'archived' page for either hiring managers or candidates,
                // the 'rejectedOrarchived' list will be loaded
                //
                // NOTE: Right now, we expect that each tab (each instance of positions-list)
                // will only include one listName.  But, things should mostly work
                // even if there were multiple lists on one page.  The UI will show whatever
                // it has after each list loads, so it's not the perfect UI, but it
                // mostly works.  We may want to actually use this at some point and
                // fix up the UI.

                scope.expandedSections = {
                    // open positions with no candidats or all candidates reviewed
                    open: {
                        expanded: true,
                        loaded: false,
                        loading: false,
                        listName: 'myPositions',
                        archived: false,
                        roles: ['hiringManager'],
                    },

                    // positions that were created via the Invite to Connect modal
                    draft: {
                        expanded: false,
                        loaded: false,
                        loading: false,
                        listName: 'myPositions',
                        archived: false,
                        roles: ['hiringManager'],
                    },

                    // archived (inactive) positions
                    archived: {
                        expanded: true,
                        loaded: false,
                        loading: false,
                        listName: 'archivedPositions',
                        archived: true,
                        roles: ['hiringManager'],
                    },

                    // positions that were created via the Invite to Connect modal and then archived
                    archived_draft: {
                        expanded: false,
                        loaded: false,
                        loading: false,
                        listName: 'archivedPositions',
                        archived: true,
                        roles: ['hiringManager'],
                    },
                };

                // only hiring manager gets the fancy sticky headers
                scope.stickyHeaders = scope.role === 'hiringManager';

                // default search text is none
                scope.search = {
                    text: '',
                };

                // Group positions into lists
                function getPositionLists() {
                    const positionLists = _.groupBy(careersNetworkViewModel.openPositions, position => {
                        if (!scope.archived && !position.archived && !position.featured) {
                            return 'draft';
                        }
                        if (scope.archived && position.archived && !position.featured) {
                            return 'archived_draft';
                        }
                        if (scope.archived && position.archived) {
                            return 'archived';
                        }
                        if ((!scope.archived && position.archived) || (scope.archived && !position.archived)) {
                            return 'ignore';
                        }
                        if (!scope.archived) {
                            return 'open';
                        }
                        $injector
                            .get('ErrorLogService')
                            .notifyInProd('Do not know what list this position should be in.', null, {
                                extra: {
                                    archived: scope.archived,
                                    position,
                                },
                            });
                        return 'ignore'; // default to ignore ones we don't understand
                    });

                    delete positionLists.ignore;

                    _.each(positionLists, (list, key) => {
                        positionLists[key] = scope.positionsWithInterestsHelper.sortOpenPositions(position =>
                            _.contains(list, position),
                        );
                    });

                    // test if there are no positions
                    scope.noPositions = !_.chain(positionLists).values().flatten().any().value();

                    // test if there are only draft positions
                    const positionListsGroups = Object.keys(positionLists);
                    scope.draftOnlyPositions =
                        positionListsGroups.length === 1 && positionListsGroups.indexOf('draft') === 0;

                    if (scope.draftOnlyPositions) {
                        scope.expandedSections.draft.expanded = true;
                    }

                    scope.positionLists = positionLists;
                }

                const connectionsTranslationHelper = new TranslationHelper('careers.connections');
                const initialTeamMemberOptions = [
                    {
                        id: 'all',
                        name: connectionsTranslationHelper.get('show_all_team_members'),
                    },
                ];

                function rebuildFilters() {
                    if (!scope.positionLists) {
                        return;
                    }

                    rebuildTeamMemberOptions();
                }

                scope.proxy = {};

                function rebuildTeamMemberOptions() {
                    // reset filter
                    scope.proxy.selectedTeamMemberId = 'all';

                    // get open position ids from current list of connections
                    const teamMemberOptions = _.chain(scope.positionLists)
                        .values()
                        .flatten()
                        .pluck('hiring_application')
                        .map(ha => {
                            if (ha.hiring_manager_id !== careersNetworkViewModel.user.id) {
                                return {
                                    id: ha.user_id,
                                    name: ha.name,
                                };
                            }
                        })
                        .compact()
                        .uniq('id')
                        .sortBy('name')
                        .value();

                    if (_.any(teamMemberOptions)) {
                        scope.teamMemberOptions = initialTeamMemberOptions.concat(teamMemberOptions);
                    } else {
                        scope.teamMemberOptions = null;
                    }
                }

                scope.$watch('positionLists', rebuildFilters);
            },
        };
    },
]);
