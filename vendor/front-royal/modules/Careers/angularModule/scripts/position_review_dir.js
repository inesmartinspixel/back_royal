import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/position_review.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import usergroup from 'vectors/usergroup.svg';
import threeCandidateCards from 'vectors/three_candidate_cards.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('positionReview', [
    '$injector',

    function factory($injector) {
        const $location = $injector.get('$location');
        const listOffsetHelper = $injector.get('listOffsetHelper');
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const $rootScope = $injector.get('$rootScope');
        const DialogModal = $injector.get('DialogModal');
        const TranslationHelper = $injector.get('TranslationHelper');
        const scopeTimeout = $injector.get('scopeTimeout');
        const dateHelper = $injector.get('dateHelper');
        const PositionsWithInterestsHelper = $injector.get('PositionsWithInterestsHelper');

        return {
            scope: {
                position: '<',
            },
            restrict: 'E',
            templateUrl,

            link(scope) {
                scope.usergroup = usergroup;

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                NavigationHelperMixin.onLink(scope);

                const careersNetworkViewModel = CareersNetworkViewModel.get('hiringManager');

                // get open positions for next/prev buttons
                PositionsWithInterestsHelper.loadPositionsAndInterests(careersNetworkViewModel).then(
                    _positionsWithInterestsHelper => {
                        scope.positionsWithInterestsHelper = _positionsWithInterestsHelper;
                        scope.openPositions = scope.positionsWithInterestsHelper.sortOpenPositions(
                            position => !position.archived && position.featured,
                        );
                        scope.showFirstPositionReviewModal();
                    },
                );

                function updateHasSeenFirstPositionReviewModal() {
                    if (!scope.currentUser.ghostMode) {
                        scope.currentUser.has_seen_first_position_review_modal = true;
                        scope.currentUser.save();
                    }
                }

                scope.showFirstPositionReviewModal = () => {
                    // FIXME: we can gut this when the pay-per-post hiring plan is out
                    // https://trello.com/c/XqkS7esI
                    if (
                        scope.currentUser.usingLegacyHiringPlan &&
                        scope.positionsWithInterestsHelper.numInterests(scope.position) > 0 &&
                        !scope.currentUser.has_seen_first_position_review_modal &&
                        !scope.currentUser.hasFullHiringManagerAccess
                    ) {
                        const translationHelper = new TranslationHelper('careers.position_review');

                        DialogModal.alert({
                            title: translationHelper.get('candidates_are_interested_in_company', {
                                companyName: scope.currentUser.hiring_application.company_name,
                            }),
                            content:
                                `<p><img src="${threeCandidateCards}" /></p>` +
                                '<p translate-once="careers.position_review.review_up_to_three"></p>' +
                                '<button class="flat blue" translate-once="careers.position_review.see_your_candidates" ng-click="closeModal()"></button>',
                            classes: ['center', 'welcome-to-smartly-talent'],
                            hideCloseButton: true,
                            blurTargetSelector: '[ng-controller]',
                            scope: {
                                closeModal() {
                                    updateHasSeenFirstPositionReviewModal();
                                    DialogModal.hideAlerts();
                                },
                            },
                        });
                    } else if (
                        !scope.currentUser.has_seen_first_position_review_modal &&
                        scope.currentUser.hasFullHiringManagerAccess
                    ) {
                        // save this to true if the user has a subscription
                        updateHasSeenFirstPositionReviewModal();
                    }
                };

                scope.$watchGroup(['position', 'openPositions'], () => {
                    if (!scope.position || !scope.openPositions) {
                        return;
                    }

                    scope.currentPositionIndex =
                        scope.openPositions.length > 2
                            ? _.findIndex(scope.openPositions, {
                                  id: scope.position.id,
                              })
                            : -1;

                    if (scope.currentPositionIndex > -1) {
                        scope.nextPosition = scope.openPositions[scope.currentPositionIndex + 1]
                            ? scope.openPositions[scope.currentPositionIndex + 1]
                            : _.first(scope.openPositions);
                        scope.previousPosition = scope.openPositions[scope.currentPositionIndex - 1]
                            ? scope.openPositions[scope.currentPositionIndex - 1]
                            : _.last(scope.openPositions);
                    }
                    scope.rerenderingCard = true;
                    scopeTimeout(
                        scope,
                        () => {
                            scope.rerenderingCard = false;
                        },
                        1,
                    );
                });

                scope.traversePositions = positionId => {
                    $location.search('positionId', positionId);
                };

                scope.goBack = () => {
                    scope.loadRoute('/hiring/positions');
                    $location.search('list', 'openPositions');
                };

                scope.editPosition = () => {
                    $location.search('positionId', scope.position.id);
                    $location.search('action', 'edit');
                };

                scope.addPosition = () => {
                    $location.search('positionId', null);
                    $location.search('action', 'create');
                };

                function getInterestGroups() {
                    scope.interestGroups = _.groupBy(
                        scope.positionsWithInterestsHelper.interests(scope.position),
                        interest => {
                            if (!interest.reviewedByHiringManager) {
                                return 'all_interested';
                            }
                            if (interest.hiring_manager_status === 'saved_for_later') {
                                return 'saved';
                            }
                            if (
                                interest.hiring_manager_status === 'invited' ||
                                interest.hiring_manager_status === 'accepted'
                            ) {
                                return 'connected';
                            }
                            if (interest.hiring_manager_status === 'rejected') {
                                return 'passed';
                            }
                        },
                    );

                    scope.tabs = [
                        {
                            name: 'all_interested',
                            class: 'positions-all_interested',
                            translate: {
                                hiringManager: 'careers.position_review.all_interested',
                                values: {
                                    num: scope.interestGroups.all_interested
                                        ? scope.interestGroups.all_interested.length
                                        : 0,
                                },
                            },
                        },
                        {
                            name: 'saved',
                            class: 'positions-saved',
                            translate: {
                                hiringManager: 'careers.position_review.saved',
                                values: {
                                    num: scope.interestGroups.saved ? scope.interestGroups.saved.length : 0,
                                },
                            },
                        },
                        {
                            name: 'connected',
                            class: 'positions-connected',
                            translate: {
                                hiringManager: 'careers.position_review.connected',
                                values: {
                                    num: scope.interestGroups.connected ? scope.interestGroups.connected.length : 0,
                                },
                            },
                        },
                        {
                            name: 'passed',
                            class: 'positions-passed',
                            translate: {
                                hiringManager: 'careers.position_review.passed',
                                values: {
                                    num: scope.interestGroups.passed ? scope.interestGroups.passed.length : 0,
                                },
                            },
                        },
                    ];
                }

                scope.currentTab = 'all_interested';

                // pagination
                scope.listLimit = 10;
                scope.offset = 0;

                scope.getProfilesRelativeToOffset = (offset, action, listLimit) => {
                    scope.offset = listOffsetHelper.setOffset(offset, action, listLimit);
                };

                // set unseen, non-anonymized interests to pending and add a $new flag
                function setUnseenToNew() {
                    _.chain(scope.positionsWithInterestsHelper.interests(scope.position))
                        .where({
                            hiring_manager_status: 'unseen',
                            anonymized: false,
                        })
                        .each(interest => {
                            interest.$new = true;

                            // if a teammate views your unseen interests, leave them as unseen
                            if (scope.currentUser.id === scope.position.hiring_manager_id) {
                                interest.hiring_manager_status = 'pending';
                                careersNetworkViewModel.saveCandidatePositionInterest(interest);
                            }
                        });
                }

                // don't watch for this until after unseen interests have been updated
                let interestUpdateListener;

                scope.$watchGroup(['position', 'positionsWithInterestsHelper'], () => {
                    if (!scope.position || !scope.positionsWithInterestsHelper) {
                        return;
                    }

                    getInterestGroups();
                    setUnseenToNew();

                    if (interestUpdateListener) {
                        interestUpdateListener();
                    }
                    interestUpdateListener = scope.$on('candidatePositionInterestUpdate', getInterestGroups);

                    scope.$on('hiringRelationship:updateStatus', (evt, hiringRelationshipViewModel) => {
                        scope.positionsWithInterestsHelper.hideRelatedCandidatePositionInterests(
                            hiringRelationshipViewModel,
                        );
                    });
                });

                // reset offset when changing tabs
                scope.$watch('currentTab', () => {
                    scope.offset = 0;

                    // if user has "one-at-a-time" option set, and we're on the all or saved tab, only show one at a time
                    if (
                        !scope.currentUser.pref_positions_candidate_list &&
                        _.contains(['all_interested', 'saved'], scope.currentTab)
                    ) {
                        scope.listLimit = 1;
                    } else {
                        scope.listLimit = 10;
                    }
                });

                scope.$watch('position.created_at', createdAt => {
                    scope.formattedCreatedAt =
                        createdAt && dateHelper.formattedUserFacingMonthDayShort(1000 * createdAt, false);
                });
            },
        };
    },
]);
