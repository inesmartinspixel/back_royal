import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/hiring_manager_card.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import hiringWebsiteIcon from 'vectors/hiring_website_icon.svg';
import locationPinIcon from 'vectors/location_pin_icon.svg';
import industryIcon from 'vectors/industry_icon.svg';
import departmentIcon from 'vectors/department_icon.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('hiringManagerCard', [
    '$injector',
    function factory($injector) {
        const EventLogger = $injector.get('EventLogger');
        const TranslationHelper = $injector.get('TranslationHelper');

        EventLogger.allowEmptyLabel(['hiring_manager_card:clicked_close_view_all']);
        EventLogger.allowEmptyLabel(['hiring_manager_card:clicked_view_all']);
        return {
            restrict: 'E',
            scope: {
                hiringApplication: '<',
            },
            templateUrl,

            link(scope) {
                scope.hiringWebsiteIcon = hiringWebsiteIcon;
                scope.locationPinIcon = locationPinIcon;
                scope.industryIcon = industryIcon;
                scope.departmentIcon = departmentIcon;

                const optionTranslationhelper = new TranslationHelper('careers.field_options');

                scope.userSectionHeight = 59;
                // when there's no fun fact, we straighten out the bottom blue background on the card
                scope.userSectionHeightOffset = scope.hiringApplication.fun_fact ? 10 : 0;

                let roleKeys = [];
                const getRoleKeys = () => {
                    const roleKeysOriginal = $injector.get('CAREERS_AREA_KEYS');
                    _.each(roleKeysOriginal, (value, key) => {
                        roleKeys.push(key);

                        if (Array.isArray(value)) {
                            // An array value for the key denotes subitems so add those as well
                            roleKeys = _.union(roleKeys, value);
                        }
                    });
                };
                getRoleKeys();

                scope.getRoleOptionLabel = option => {
                    let translatedOption;

                    // At one time we allowed custom roles so check that the role is
                    // in the set of predefined role keys
                    if (roleKeys.includes(option)) {
                        translatedOption = optionTranslationhelper.get(option);
                    }
                    return translatedOption || option;
                };

                scope.getRolesClasses = () => {
                    if (scope.hiringApplication && scope.hiringApplication.role_descriptors) {
                        const count = scope.hiringApplication.role_descriptors.length;
                        if (count === 1) {
                            return 'single';
                        }
                        if (count === 2) {
                            return 'double';
                        }
                        if (count > 3) {
                            return 'more';
                        }
                    }
                };

                scope.logEvent = (name, payload) => {
                    EventLogger.log(
                        name,
                        angular.extend(
                            {
                                hiring_manager_id: scope.hiringApplication.user_id,
                            },
                            payload || {},
                        ),
                    );
                };

                // Kind of silly, since there's only one page, but
                // parallel with candidate_card
                scope.logEvent('hiring_manager_card:viewed_page', {
                    label: 'page 1',
                });

                Object.defineProperty(scope, 'userInfo', {
                    get() {
                        if (!scope.hiringApplication) {
                            return {};
                        }
                        return {
                            displayName: scope.hiringApplication.name,
                            subtext1: scope.hiringApplication.job_role
                                ? optionTranslationhelper.get(scope.hiringApplication.job_role)
                                : null,
                            subtext2: null,
                            avatarUrl: scope.hiringApplication.avatar_url,
                        };
                    },
                });

                scope.closeViewAll = () => {
                    scope.logEvent('hiring_manager_card:clicked_close_view_all');
                    scope.viewAllOpen = false;
                };

                scope.openViewAll = () => {
                    // don't launch if we have fewer than 4 roles
                    if (scope.hiringApplication.role_descriptors.length <= 3) {
                        return;
                    }
                    scope.logEvent('hiring_manager_card:clicked_view_all');
                    scope.viewAllOpen = true;
                };
            },
        };
    },
]);
