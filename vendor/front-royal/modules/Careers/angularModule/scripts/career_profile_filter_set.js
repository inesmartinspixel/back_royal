import angularModule from 'Careers/angularModule/scripts/careers_module';
/*
    CareerProfileFilterSet is instantiated with a list of filters and then
    is responsible for loading up batches of profiles that match those
    filters whenever necessary, until no more are left.
*/
angularModule.factory('CareerProfileFilterSet', [
    '$injector',

    function factory($injector) {
        const SuperModel = $injector.get('SuperModel');
        const CareerProfile = $injector.get('CareerProfile');
        const $rootScope = $injector.get('$rootScope');
        const CareerProfileSearch = $injector.get('CareerProfileSearch');
        const areaKeys = $injector.get('CAREERS_AREA_KEYS');
        const listOffsetHelper = $injector.get('listOffsetHelper');

        // build a child -> parent lookup for buildRolesFilter use
        const parentLookup = {};
        _.each(areaKeys, (parent, parentKey) => {
            _.each(parent, childKey => {
                parentLookup[childKey] = parentKey;
            });
        });

        const CareerProfileFilterSet = SuperModel.subclass(function () {
            Object.defineProperty(this.prototype, 'any', {
                get() {
                    return this._initialResultsLoaded ? _.any(this.careerProfiles) : false;
                },
            });

            Object.defineProperty(this.prototype, 'noAdditionalCandidates', {
                get() {
                    return this.noMoreAvailable && !this.any;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'searchingForMoreWhenEmpty', {
                get() {
                    return (!!this._loadingPromise || !this._initialResultsLoaded) && !this.any;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'searchingForMore', {
                get() {
                    return !!this._loadingPromise;
                },
            });

            Object.defineProperty(this.prototype, 'hasAttemptedToFetchInitialResults', {
                get() {
                    return !!(this.searchingForMore || this._initialResultsLoaded);
                },
            });

            Object.defineProperty(this.prototype, 'noMoreAvailable', {
                get() {
                    return !!this._allDataLoaded;
                },
            });

            // careerProfileFilterSet.any can be false for a short period
            // of time, between hitting the end of the `careerProfiles` and loading
            // the next 10 profiles. In this case, we don't want to destroy whatever
            // directive is being used to display the career profiles (e.g. candidateStack
            // or the candidateList) and then rerender the element to the DOM.
            // see https://trello.com/c/9HBuG3qt
            Object.defineProperty(this.prototype, 'showDisplayElement', {
                get() {
                    return this.any || this.searchingForMoreWhenEmpty;
                },
                configurable: true,
            });

            this.extend({
                buildRolesFilter(preferredPrimaryAreasOfInterest) {
                    if (!_.any(preferredPrimaryAreasOfInterest)) {
                        return undefined;
                    }

                    // In the client UI, we only set "preferred_primary_areas_of_interest" (which should
                    // be called "preferred_roles").  This value
                    // is then used to generate a "roles" object, which includes 4 different values.  That
                    // "roles" object is what is actually sent up to the server and used for filtering.  This
                    // works this way because knowledge of the hierarchy of roles only exists on the client, and
                    // that is required to convert a single preferred role into the rules needed for querying.
                    const roles = {
                        preferred_primary_areas_of_interest: preferredPrimaryAreasOfInterest,
                    };
                    // For primary_areas_of_interest, include the preferred area itself.
                    // if it is parent record, include all children.
                    // if it's a child record, include parent.
                    roles.primary_areas_of_interest = _.chain(preferredPrimaryAreasOfInterest)
                        .map(preferredArea => [
                            preferredArea,
                            areaKeys[preferredArea] /* arr of children of parent */,
                            parentLookup[preferredArea] /* str of parent of child */,
                        ])
                        .flatten()
                        .compact()
                        .unique()
                        .value();

                    roles.preferred_work_experience_roles = _.clone(preferredPrimaryAreasOfInterest);
                    // For work_experience_roles, include the preferred area itself
                    // if it is parent record, include all children.
                    // if it is parent record, include all children.
                    // if it's a child record, DO NOT include parent.
                    roles.work_experience_roles = _.chain(preferredPrimaryAreasOfInterest)
                        .map(preferredArea => [preferredArea, areaKeys[preferredArea] /* arr of children of parent */])
                        .flatten()
                        .compact()
                        .unique()
                        .value();

                    return roles;
                },
            });

            return {
                initialize(filters, sort, options = {}) {
                    this.sourceFilters = filters; // this is just used in specs.  Probably kind of a silly way to do it
                    this.filters = CareerProfileSearch.cloneFilters(filters);
                    this.sort = sort || ['BEST_SEARCH_MATCH_FOR_HM'];
                    this.offset = options.offset || 0;

                    // minLength acts as a threshold to help determine when an API call should be made to retrieve more profiles.
                    // Should the curent number of profiles dips below the value of minLength, more profiles will be retrieved, if necessary.
                    this.minLength = options.minLength || 5;

                    // lets the server know the max number of profiles it should return
                    this.serverLimit = options.serverLimit || 10;

                    this.careerProfiles = [];
                    this._initialResultsLoaded = false;
                },

                cloneFilters() {
                    return CareerProfileSearch.cloneFilters(this.filters);
                },

                setOffset(offset, action, listLimit) {
                    this.offset = listOffsetHelper.setOffset(offset, action, listLimit);
                },

                // resets the offset value to 0 or increments/decrements the offset value using the listLimit based on
                // the action provided and then attempts to make an API call to retrieve the profiles if necessary
                getProfilesRelativeToOffset(offset, action, listLimit) {
                    this.setOffset(offset, action, listLimit);
                    return this.ensureCareerProfilesPreloaded();
                },

                // Users of CareerProfileFilterSet are expected to remove things from `careerProfiles`.
                // If `careerProfiles` gets shorter than the minLength, then careerProfileFilterSet will
                // make another request to make sure it has enough.
                ensureCareerProfilesPreloaded() {
                    const self = this;

                    if (self._loadingPromise) {
                        return self._loadingPromise;
                    }

                    if (self.careerProfiles.length - self.offset < self.minLength && !self._allDataLoaded) {
                        const maxTotalCount = 100;

                        self._loadingPromise = CareerProfile.index({
                            view: 'career_profiles',
                            filters: this._buildFiltersForApiRequest(),
                            limit: self.serverLimit,
                            max_total_count: maxTotalCount,
                            offset: self.careerProfiles.length,
                            sort: self.sort,
                            direction: 'desc',
                            save_search: !this._initialResultsLoaded,
                        }).then(response => {
                            // We don't want to decrement the initialTotalCount since we load more results under
                            // the hood, so only cache total_count when loading the initial results
                            if (response.meta && !self._initialResultsLoaded) {
                                self.initialTotalCount = response.meta.total_count;
                                self.initialTotalCountIsMin = self.initialTotalCount === maxTotalCount;
                            }

                            self._initialResultsLoaded = true;
                            self._loadingPromise = null;
                            self.careerProfiles = self.careerProfiles.concat(response.result);
                            self._userIds = _.pluck(self.careerProfiles, 'user_id');

                            if (response.meta && response.meta.career_profile_search) {
                                const attrs = response.meta.career_profile_search;
                                self.searchId = attrs.id;

                                if ($rootScope.currentUser && $rootScope.currentUser.hiring_application) {
                                    let recentSearches =
                                        $rootScope.currentUser.hiring_application.recent_career_profile_searches;
                                    const existingSearch = _.find(recentSearches, {
                                        id: attrs.id,
                                    });

                                    if (existingSearch) {
                                        existingSearch.copyAttrs(attrs);
                                        const cloned = _.without(recentSearches, existingSearch);
                                        cloned.unshift(existingSearch);
                                        $rootScope.currentUser.hiring_application.recent_career_profile_searches = cloned;
                                    } else {
                                        const search = CareerProfileSearch.new(attrs);
                                        recentSearches.unshift(search);
                                    }

                                    // reset this to the array that is now on the hiring application.  In case it was
                                    // cloned above.
                                    recentSearches =
                                        $rootScope.currentUser.hiring_application.recent_career_profile_searches;
                                    while (recentSearches.length > 3) {
                                        recentSearches.pop();
                                    }
                                }
                            }

                            if (response.result.length < self.serverLimit) {
                                self._allDataLoaded = true;
                            }

                            // Calling ensureCareerProfilesPreloaded again after a request completes
                            // allows us to catch up if the offset has moved ahead while we were loading.
                            // The UI should prevent the user from banging on the next button anyhow, but
                            // if not, this makes sure we eventually load what we need to.
                            self.ensureCareerProfilesPreloaded();
                        });
                    }

                    return self._loadingPromise;
                },

                _addRolesFilter(filters) {
                    const preferredPrimaryAreasOfInterest = filters.preferred_primary_areas_of_interest;
                    filters.roles = CareerProfileFilterSet.buildRolesFilter(preferredPrimaryAreasOfInterest);
                    delete filters.preferred_primary_areas_of_interest;
                },

                _buildFiltersForApiRequest() {
                    const filters = _.extend({}, this.filters, {
                        has_candidate_relationship: false,
                        available_for_relationships_with_hiring_manager: $rootScope.currentUser.id,
                    });

                    this._addRolesFilter(filters);
                    return filters;
                },
            };
        });

        return CareerProfileFilterSet;
    },
]);
