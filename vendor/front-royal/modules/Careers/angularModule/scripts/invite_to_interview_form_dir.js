import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/invite_to_interview_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import sharedHiringRelationshipCheck from 'vectors/shared-hiring-relationship-check.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('inviteToInterviewForm', [
    '$injector',

    function factory($injector) {
        const $timeout = $injector.get('$timeout');
        const OpenPosition = $injector.get('OpenPosition');
        const guid = $injector.get('guid');
        const TranslationHelper = $injector.get('TranslationHelper');
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
        const $q = $injector.get('$q');

        return {
            scope: {
                onFinish: '<',
                hiringRelationshipViewModel: '<',
                candidatePositionInterest: '<?',

                // a boolean to set when the form will be contained in a
                // narrow column such as in the showFullProfile DialogModal
                staysNarrow: '<?',

                // This boolean attribute controls whether or not the blue checkmark should
                // be shown. This directive is shown inside of a modal and there may be another
                // modal that pops up after the hiring relationship is successfully saved to
                // take this modal's place. After the hiring relationship is saved, we typically
                // show a big checkmark in this directive's UI to indicate that the request
                // was successful; however, we don't want this checkmark to appear if another
                // modal pops up afterwards because it makes for a jarring UX.
                // Default: true
                showInviteSentCheckmark: '<?',
            },
            restrict: 'E',
            templateUrl,

            link(scope) {
                scope.sharedHiringRelationshipCheck = sharedHiringRelationshipCheck;

                scope.showInviteSentCheckmark = angular.isDefined(scope.showInviteSentCheckmark)
                    ? scope.showInviteSentCheckmark
                    : true;

                scope.proxy = {};
                let placeholderOpenPosition;

                const translationHelper = new TranslationHelper('careers.invite_to_interview');
                scope.titlePlaceholder = translationHelper.get('role_placeholder');

                scope.proxy.messageBody = translationHelper.get(
                    scope.candidatePositionInterest ? 'message_prefill_interest' : 'message_prefill',
                    {
                        // always show nickname or name regardless of hiring manager's showPhotosNames setting
                        // this is so that messages sent are more personal, a request from the hiring team
                        recipientName:
                            scope.hiringRelationshipViewModel.connectionProfile &&
                            (scope.hiringRelationshipViewModel.connectionProfile.nickname ||
                                scope.hiringRelationshipViewModel.connectionProfile.name),
                        senderName: scope.hiringRelationshipViewModel.currentUser.nickname,
                        senderCompany: scope.hiringRelationshipViewModel.hiringManagerCompany,
                    },
                );

                // If this form was introduced from within the /positions context,
                // we want the submit button to use a different locale
                scope.proxy.submitButtonText = translationHelper.get(
                    scope.candidatePositionInterest ? 'connect_with' : 'invite_to_connect',
                    {
                        recipientName: scope.hiringRelationshipViewModel.connectionNickname,
                    },
                );

                const careersNetworkViewModel = CareersNetworkViewModel.get('hiringManager');

                careersNetworkViewModel.ensureOpenPositions().then(openPositions => {
                    scope.openPositions = _.chain(openPositions)
                        .where({
                            archived: false,
                            // clone all the objects so that unsaved changes to not get
                            // persisted in the careersNetworkViewModel
                        })
                        .map(openPosition => OpenPosition.new(openPosition.asJson()))
                        .value();

                    // If a candidatePositionInterest was passed in the params object, we
                    // need to get the associated OpenPosition.
                    if (scope.candidatePositionInterest) {
                        scope.openPosition = _.findWhere(scope.openPositions, {
                            id: scope.candidatePositionInterest.open_position_id,
                        });
                    }

                    if (scope.openPosition) {
                        scope.proxy.openPositionId = scope.openPosition.id;
                    } else if (_.any(scope.openPositions)) {
                        scope.openPosition = _.chain(scope.openPositions).sortBy('last_invitation_at').last().value();
                        scope.proxy.openPositionId = scope.openPosition.id;
                    } else {
                        // before an openPosition has been selected, we need somewhere
                        // for the form to bind to, in case the user fills out other
                        // inputs before entering a title and triggering createOpenPosition()
                        scope.openPosition = placeholderOpenPosition = OpenPosition.new();
                    }
                });

                /*
                        Special selectize handling:

                        Since we are enabling create on a single-select selectize,
                        we want to empty out the selection on focus.  This makes it
                        more clear to the user that she can type in a new value. Without
                        this she has to figure out to press backspace and then start typing.

                        Really this should be general wherever we use selectize and enable
                        create on a single-select.  But that's a bit scary because when doing
                        this you have to handle the fact that the ngModel gets switched to
                        undefined.  (See watch on proxy.openPositionId below and onTitleSelectizeBlur)
                    */
                scope.onTitleSelectizeFocus = function () {
                    const selectize = this;
                    selectize.clear();
                };

                scope.onTitleSelectizeBlur = function () {
                    const selectize = this;
                    if (!selectize.getValue() && scope.openPosition) {
                        selectize.setValue(scope.openPosition.id);
                    }
                };

                scope.createOpenPosition = title => {
                    const attrs = {
                        id: guid.generate(),
                        hiring_manager_id: scope.hiringRelationshipViewModel.hiringRelationship.hiring_manager_id,
                        title,
                    };
                    if (placeholderOpenPosition) {
                        _.extend(attrs, placeholderOpenPosition);
                        placeholderOpenPosition = null;
                    }
                    const openPosition = OpenPosition.new(attrs);
                    return openPosition;
                };

                scope.$watch('proxy.openPositionId', id => {
                    // This may get unset while the user is interacting with
                    // the selectize input.  We want to keep the rest of the
                    // form filled out in that case.
                    if (id) {
                        scope.openPosition = _.findWhere(scope.openPositions, {
                            id,
                        });
                    }
                });

                scope.invite = () => {
                    let promise;
                    let result;
                    let meta;

                    meta = {};
                    const openPosition = scope.openPosition;
                    const candidatePositionInterest = scope.candidatePositionInterest;

                    // only update candidatePositionInterest if one was provided
                    // NOTE: this has to come before addMessage so that the hiringRelationship:updateStatus
                    // event will be called after the candidate position interest is marked as
                    // reviewed.  See position_list_dir.
                    if (candidatePositionInterest) {
                        candidatePositionInterest.hiring_manager_status = 'invited';
                        meta.candidatePositionInterest = candidatePositionInterest;
                    }

                    // We serialize the open position in the message so it can be displayed wherever
                    // the message is displayed, and then we also set up an association between the
                    // hiring relationship and the open position.  It is intentional that this is denormalized
                    // in the message.  Even if the position is edited later, we would not want the message to
                    // change.
                    scope.hiringRelationshipViewModel.addMessage(
                        scope.proxy.messageBody,
                        {
                            senderCompanyName: scope.hiringRelationshipViewModel.hiringManagerCompany,

                            // We used to just call openPosition.asJson() here, but there is too
                            // much stuff in there: https://trello.com/c/56q5gp8x/3210-bug-we-are-logging-too-much-stuff-in-messages-and-hiring-applications
                            openPosition: _.pick(openPosition, [
                                'id',
                                'available_interview_times',
                                'company',
                                'title',
                                'salary',
                                'bonus',
                                'equity',
                                'job_post_url',
                                'description',
                                'hiring_manager_id',
                                'place_id',
                                'place_details',
                            ]),
                        },
                        {
                            // The candidate_position_interest_id is only used on the server to determine if there's a cover letter
                            // attached to the candidate position interest that it should include in the conversation's JSON returned
                            // from the server. If we see that the candidatePositionInterest has no cover letter, then we shouldn't
                            // bother setting this attribute on the conversation.
                            candidate_position_interest_id:
                                candidatePositionInterest && candidatePositionInterest.hasCoverLetter
                                    ? candidatePositionInterest.id
                                    : undefined,
                        },
                    );

                    scope.hiringRelationshipViewModel.hiringRelationship.open_position_id = openPosition.id;

                    // only update featured positions if savePosition checkbox is selected
                    if (!openPosition.featured || (openPosition.featured && scope.proxy.savePosition)) {
                        careersNetworkViewModel.commitOpenPosition(openPosition);
                        meta.openPosition = openPosition;
                    }

                    // If not a featured position, and no candidatePositionInterest
                    // was provided, this will simply save the VM
                    promise = scope.hiringRelationshipViewModel.save(meta);

                    promise
                        .then(() => {
                            result = true;
                            let promise;
                            if (scope.showInviteSentCheckmark) {
                                scope.inviteSent = true;
                                promise = $timeout(1750);
                            } else {
                                promise = $q.when();
                            }
                            return promise;
                        })
                        .catch(() => {
                            // after an error (like if there is a conflict
                            // with a teammate), we are not showing the completed
                            // element, so no need to timeout.  Just close
                            result = false;
                        })
                        .then(() => {
                            // regardless of whether the save succeeded or failed
                            // we call the same callback.  If anyone outside of
                            // here was ever interested, then we could have two
                            // callbacks or somehow expose a promise or something.
                            scope.onFinish(result);
                        });
                };

                scope.positionSelectizeConfig = {
                    create: scope.createOpenPosition,
                    onFocus: scope.onTitleSelectizeFocus,
                    onBlur: scope.onTitleSelectizeBlur,
                    maxItems: 1,
                    valueField: 'id',
                    labelField: 'title',
                    sortField: 'title',
                    searchField: ['title'],
                    placeholder: scope.titlePlaceholder,
                };
            },
        };
    },
]);
