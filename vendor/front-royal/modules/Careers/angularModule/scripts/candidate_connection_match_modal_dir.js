import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/candidate_connection_match_modal.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('candidateConnectionMatchModal', [
    '$injector',
    function factory($injector) {
        const DialogModal = $injector.get('DialogModal');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');

        return {
            restrict: 'E',
            scope: {
                message: '<',
                buttonText: '<',
                connectionId: '<',
                connectionAvatarSrc: '<',
            },
            templateUrl,

            link(scope) {
                NavigationHelperMixin.onLink(scope);

                scope.action = () => {
                    DialogModal.hideAlerts();
                    if (scope.connectionId) {
                        scope.loadRoute(`/careers/connections?connectionId=${scope.connectionId}`);
                    } else {
                        scope.loadRoute('/careers/connections');
                    }
                };
            },
        };
    },
]);
