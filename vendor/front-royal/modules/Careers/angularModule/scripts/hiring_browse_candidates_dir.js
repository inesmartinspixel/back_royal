import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/hiring_browse_candidates.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import telescope from 'vectors/telescope.svg';
import magicwand from 'vectors/magicwand.svg';
import usergroup from 'vectors/usergroup.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('hiringBrowseCandidates', [
    '$injector',

    function factory($injector) {
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const TranslationHelper = $injector.get('TranslationHelper');
        const CareerProfileFilterSet = $injector.get('CareerProfileFilterSet');
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
        const $rootScope = $injector.get('$rootScope');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const isMobileMixin = $injector.get('isMobileMixin');
        const CareerProfile = $injector.get('CareerProfile');
        const listOffsetHelper = $injector.get('listOffsetHelper');
        const EventLogger = $injector.get('EventLogger');
        const HttpQueue = $injector.get('HttpQueue');
        const $location = $injector.get('$location');
        const $q = $injector.get('$q');
        const ErrorLogService = $injector.get('ErrorLogService');
        const HasLocation = $injector.get('HasLocation');

        EventLogger.allowEmptyLabel(['hiring_manager:all_candidates_reviewed']);

        return {
            scope: {},
            restrict: 'E',
            templateUrl,
            link(scope) {
                scope.telescope = telescope;
                scope.magicwand = magicwand;
                scope.usergroup = usergroup;

                const translationHelper = new TranslationHelper('careers.hiring_browse_candidates');
                const fieldsTranslationHelper = new TranslationHelper('careers.field_options');
                const skillsTranslationHelper = new TranslationHelper('careers.edit_career_profile.select_skills_form');
                const candidateListCardTranslationHelper = new TranslationHelper('careers.candidate_list_card');
                scope.careersNetworkViewModel = CareersNetworkViewModel.get('hiringManager');

                AppHeaderViewModel.setTitleHTML(translationHelper.get('browse_candidates_title'));
                NavigationHelperMixin.onLink(scope);
                isMobileMixin.onLink(scope);

                // An object to map the various collections of career profiles for the tabs
                // where usesCareerProfileFilterSet on the associated tab config is false
                scope.careerProfilesMap = {};
                scope.hiringRelationshipViewModelsMap = {};

                // a local variable to keep track of the current list of filtered career profiles
                let careerProfilesOrHiringRelationshipViewModels;

                // a local flag to determine if a shared candidate has already been processed
                let processedDeepLinkedCareerProfile = false;

                // returns config object for a tab with default values for properties
                // that control various behaviors and processing for the UI
                function getTabConfig(tabName, listName, opts = {}) {
                    return _.extend(
                        {
                            tabName, // The tab's name. It should match the associated tab config's property name.
                            listName, // The list name for the tab (see CareersNetworkViewModel#_loadHiringRelationshipsForListName for list of possible values)
                            usesCareerProfileFilterSet: false, // controls whether the tab uses the CareerProfileFilterSet service
                            supportsHidingCandidates: true, // controls support for the 'hide' action on candidate cards
                            shouldShowAllReviewedMessageOnEmpty: false, // controls whether the "All Candidates Reviewed" message should be shown when the tab is empty
                            shouldShowNoCandidatesForActionMessageOnEmpty: false, // controls whether the "You haven't {{action}} any candidates" message should be shown when the tab is empty
                            supportsPreloadingHiringRelationshipsOnInit: false, // controls whether the tab should have its candidates preloaded to calculate its candidate count
                            supportsAllCandidatesReviewedEvent: true, // controls whether the tab should create the 'hiring_manager:all_candidates_reviewed' event when the tab's last candidate is reviewed
                            supportsDeepLinking: false, // controls whether the tab should allow deep linkingto a candidate's profile
                            derivesCandidatesFromHiringRelationshipViewModels: false, // controls whether the tab should use the hiring relationship view-models to display the candidates
                            loading: true, // controls visibility of the spinner when loading hiring relationships for tab
                            disabledWhenNoFullAccess: false, // hidden and saved tabs are hidden to users who do not have full access (so we don't have to worry about anonymizing existing relationships or deciding what actions they can take)
                            disabledWhenNoAcceptedApplication: true, // pending hiring managers can only see browse tab, and then the whole page is blurred out
                            // Tabs where usesCareerProfileFilterSet is false get their respective candidates by filtering through the hiring relationship
                            // view-models on the careers network view-model. Different filtering logic can be configured for a tab by providing a different
                            // filter function property in the opts param.
                            filter(viewModel) {
                                // in the common case, it's enough to filter by the hiring_manager_status
                                // on the hiringRelationship attached to the hiring relationship view-model
                                return this.usesCareerProfileFilterSet
                                    ? false
                                    : viewModelIsForHiringManagerAndMatchesHiringManagerStatus(
                                          viewModel,
                                          opts.hiringManagerStatus,
                                      );
                            },
                        },
                        opts,
                    );
                }

                // verifies that the passed in hiring relationship view-model is for the hiring manager and that the
                // hiring_manager_status on the attached hiringRelationship matches the passed in hiringManagerStatus
                function viewModelIsForHiringManagerAndMatchesHiringManagerStatus(viewModel, hiringManagerStatus) {
                    return (
                        viewModel.role === 'hiringManager' &&
                        viewModel.hiringRelationship.hiring_manager_status === hiringManagerStatus
                    );
                }

                // set the configs for the available lists in this directive
                scope.tabConfigs = {
                    all: getTabConfig('all', 'all', {
                        usesCareerProfileFilterSet: true,
                        supportsAllCandidatesReviewedEvent: false,
                        loading: false,
                        disabledWhenNoAcceptedApplication: false,
                    }),
                    featured: getTabConfig('featured', 'featured', {
                        shouldShowAllReviewedMessageOnEmpty: true,
                        derivesCandidatesFromHiringRelationshipViewModels: true,
                        supportsDeepLinking: true,
                        filter(viewModel) {
                            // The featured tab is unique in that it is the only tab that supportsDeepLinking. As a feature of the deep linking support,
                            // if the hiring manager deep links to a specific candidate's career profile the profile should be included in the featured
                            // tab's list of profiles regardless of the state of the hiring relationship. As such, we need to include _deepLinked hiring
                            // relationship view-models in the filtered list of candidates.
                            return (
                                viewModel._deepLinked ||
                                viewModelIsForHiringManagerAndMatchesHiringManagerStatus(viewModel, 'pending')
                            );
                        },
                    }),
                    saved: getTabConfig('saved', 'saved', {
                        supportsPreloadingHiringRelationshipsOnInit: true,
                        shouldShowNoCandidatesForActionMessageOnEmpty: true,
                        derivesCandidatesFromHiringRelationshipViewModels: true,
                        disabledWhenNoFullAccess: true,
                        filter(viewModel) {
                            // the 'saved' list should show candidates that the hiring manager only
                            // saved_for_later from the Candidates area, not from Positions
                            return (
                                !viewModel.hasOpenPositionId &&
                                viewModelIsForHiringManagerAndMatchesHiringManagerStatus(viewModel, 'saved_for_later')
                            );
                        },
                    }),
                    hidden: getTabConfig('hidden', 'rejected', {
                        hiringManagerStatus: 'rejected',
                        supportsHidingCandidates: false,
                        shouldShowNoCandidatesForActionMessageOnEmpty: true,
                        disabledWhenNoFullAccess: true,
                    }),
                };

                // derive tabs from the keys of the tabConfigs object defined on the scope
                const tabs = Object.keys(scope.tabConfigs);

                // generate the candidatesTabs that get passed along to the tabs directive
                scope.candidatesTabs = _.map(tabs, tabName => ({
                    name: tabName,
                    class: `${tabName}-candidates`,

                    translate: {
                        hiringManager: `careers.hiring_browse_candidates.${tabName}`,
                        values: {
                            count: getTabCount(tabName),
                        },
                    },
                }));

                Object.defineProperty(scope, 'totalCount', {
                    get() {
                        const plus = scope.careerProfileFilterSet.initialTotalCountIsMin ? '+' : '';
                        return scope.currentTabConfig.usesCareerProfileFilterSet
                            ? scope.careerProfileFilterSet.initialTotalCount + plus
                            : scope.currentTabCount;
                    },
                });

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'currentUserCanBrowseProfiles', {
                    get() {
                        // a legacy, pending user hasSourcingAccess but !hasAcceptedHiringManagerAccess.  once we get
                        // rid of HIRING_PLAN_LEGACY we can probably simplify this
                        return !!(
                            scope.currentUser &&
                            scope.currentUser.hasSourcingAccess &&
                            scope.currentUser.hasAcceptedHiringManagerAccess
                        );
                    },
                });

                Object.defineProperty(scope, 'currentTab', {
                    get() {
                        // Derive currentTab from the 'tab' query param. If it's not a valid tab,
                        // fallback to the 'all' tab and reset the 'tab' query param in the URL accordingly.
                        let requestedTab = $location.search().tab;
                        if (!_.contains(tabs, requestedTab)) {
                            requestedTab = 'all';
                            $location.search('tab', requestedTab);
                        }
                        return requestedTab;
                    },
                    set(newTab) {
                        // currentTab is derived from the 'tab' query param, so when currentTab gets set,
                        // just update the 'tab' query param in the URL. Also, replace the history so the
                        // hiring manager doesn't have to sift through all of the different tabs that they
                        // visited on the Candidates page when they press the browser's back button.
                        $location.search('tab', newTab).replace();
                    },
                });

                Object.defineProperty(scope, 'currentTabConfig', {
                    get() {
                        return scope.tabConfigs[scope.currentTab];
                    },
                });

                Object.defineProperty(scope, 'currentTabCount', {
                    get() {
                        return _.findWhere(scope.candidatesTabs, {
                            name: scope.currentTab,
                        }).translate.values.count;
                    },
                });

                // convenience property that returns the career profiles for the currentTab
                Object.defineProperty(scope, 'careerProfilesOrHiringRelationshipViewModels', {
                    get() {
                        // if the currentTab usesCareerProfileFilterSet, return the career profiles
                        // from the careerProfileFilterSet or an empty array if the careerProfileFilterSet
                        // hasn't been created yet
                        if (scope.currentTabConfig.usesCareerProfileFilterSet) {
                            return scope.careerProfileFilterSet ? scope.careerProfileFilterSet.careerProfiles : [];
                        }
                        // otherwise, return the career profiles or hiring relationship view-models for the currentTab
                        return careerProfilesOrHiringRelationshipViewModels;
                    },
                    configurable: true,
                });

                Object.defineProperty(scope, 'noCandidatesForCurrentFilters', {
                    get() {
                        return (
                            scope.hasAppliedFilters &&
                            scope.careerProfilesOrHiringRelationshipViewModels &&
                            scope.careerProfilesOrHiringRelationshipViewModels.length === 0
                        );
                    },
                    configurable: true,
                });

                Object.defineProperty(scope, 'showAllReviewedMessage', {
                    get() {
                        return (
                            scope.currentTabConfig.shouldShowAllReviewedMessageOnEmpty &&
                            !scope.currentTabConfig.loading &&
                            !scope.unsupportedDeepLink &&
                            unfilteredListOfProfilesForCurrentTabIsEmpty()
                        );
                    },
                    configurable: true,
                });

                Object.defineProperty(scope, 'showNoCandidatesForActionMessage', {
                    get() {
                        return (
                            scope.currentTabConfig.shouldShowNoCandidatesForActionMessageOnEmpty &&
                            !scope.currentTabConfig.loading &&
                            unfilteredListOfProfilesForCurrentTabIsEmpty()
                        );
                    },
                    configurable: true,
                });

                Object.defineProperty(scope, 'showAdjustPreferencesMessage', {
                    get() {
                        return (
                            (scope.currentTabConfig.shouldShowAllReviewedMessageOnEmpty ||
                                scope.currentTabConfig.shouldShowNoCandidatesForActionMessageOnEmpty) &&
                            scope.noCandidatesForCurrentFilters &&
                            !scope.showNoCandidatesForActionMessage &&
                            !scope.showAllReviewedMessage
                        );
                    },
                    configurable: true,
                });

                Object.defineProperty(scope, 'showingSomeResults', {
                    get() {
                        return (
                            scope.careerProfilesOrHiringRelationshipViewModels &&
                            scope.careerProfilesOrHiringRelationshipViewModels.length > 0
                        );
                    },
                });

                function unfilteredListOfProfilesForCurrentTabIsEmpty() {
                    const candidates = scope.currentTabConfig.derivesCandidatesFromHiringRelationshipViewModels
                        ? scope.hiringRelationshipViewModelsMap[scope.currentTab]
                        : scope.careerProfilesMap[scope.currentTab];
                    return candidates && candidates.length === 0;
                }

                function sanitizeUrlSearchParams(removeIdSearchParam) {
                    const searchParams = $location.search();
                    _.each(_.keys(searchParams), searchParam => {
                        if (!_.contains(['tab', 'id'], searchParam)) {
                            $location.search(searchParam, null);
                        }
                    });
                    if (removeIdSearchParam || !scope.currentTabConfig.supportsDeepLinking) {
                        $location.search('id', null);
                    }
                }

                // invoke immediately to ensure the URL isn't mucked up unnecessarily
                sanitizeUrlSearchParams();

                // Note: we used to present a welcome modal for users that were accepted with complete profiles
                // visiting browse for the first time. We don't do that now, instead opting to just let them explore.
                // For now, we just set the has_seen_welcome flag proactively, but later we may opt to bring back a different
                // welcome screen.
                scope.$watchGroup(['currentUser.hiring_application.complete', 'currentUserCanBrowseProfiles'], () => {
                    const application = scope.currentUser && scope.currentUser.hiring_application;
                    if (
                        scope.currentUserCanBrowseProfiles &&
                        application &&
                        application.complete &&
                        !application.has_seen_welcome
                    ) {
                        application.has_seen_welcome = true;
                        application.save();
                    }
                });

                // We hide the app header on mobile on this screen
                scope.$watch('isMobile', () => {
                    AppHeaderViewModel.toggleVisibility(!scope.isMobile);
                });

                scope.$on('$destroy', () => {
                    AppHeaderViewModel.toggleVisibility(true);
                    resetDeepLinkedCareerProfile();
                    $location.search('tab', null);
                });

                scope.mobileState = {
                    expanded: false,
                };
                scope.$watch('mobileState.expanded', expanded => {
                    if (
                        !expanded &&
                        scope.careerProfileFilterSet &&
                        scope.currentTabConfig.usesCareerProfileFilterSet
                    ) {
                        scope.filters = scope.careerProfileFilterSet.cloneFilters();
                    }
                });

                scope.disableTab = name => {
                    if (!scope.currentUser) {
                        // keep the all tab enabled after logging out to prevent sending
                        // a warning to sentry
                        return name !== 'all';
                    }

                    const tabConfig = scope.tabConfigs[name];
                    if (!scope.currentUser.hasAcceptedHiringManagerAccess) {
                        return tabConfig.disabledWhenNoAcceptedApplication;
                    }
                    if (!scope.currentUser.hasFullHiringManagerAccess) {
                        return tabConfig.disabledWhenNoFullAccess;
                    }
                    return false;
                };

                scope.$watchGroup(['currentTab', 'currentUser.hasFullHiringManagerAccess'], () => {
                    if (scope.disableTab(scope.currentTab)) {
                        if (scope.currentTab === 'all') {
                            ErrorLogService.notifyInProd('What am I supposed to do if the all tab is disabled?');
                            return;
                        }
                        scope.currentTab = 'all';
                    }
                });

                scope.yearsExperienceKeys = $injector.get('CAREERS_YEARS_EXPERIENCE_KEYS');
                scope.typesOfInterestKeys = $injector.get('CAREERS_POSITION_DESCRIPTOR_KEYS');
                scope.levelOfInterestKeys = _.without($injector.get('NEW_COMPANY_INTEREST_LEVELS'), 'not_interested');

                // NOTE: we are omitting language keys here and deferring to keyword match
                // see https://trello.com/c/GlVfdxr1
                scope.skillsKeys = _.flatten(
                    _.values(_.chain(_.omit($injector.get('CAREERS_SKILLS_KEYS'), 'languages')).value()),
                ).map(translationKey => ({
                    value: translationKey,
                    translation: skillsTranslationHelper.get(translationKey),
                }));

                scope.inSchoolOptions = [
                    {
                        value: null,
                        label: translationHelper.get('in_or_out_of_school'),
                    },
                    {
                        value: true,
                        label: translationHelper.get('in_school'),
                    },
                    {
                        value: false,
                        label: translationHelper.get('out_of_school'),
                    },
                ];

                scope.placesPlaceholder = translationHelper.get('office_location');
                scope.rolesPlaceholder = translationHelper.get('role');
                scope.yearsExperiencePlaceholder = translationHelper.get('years_experience');
                scope.skillsPlaceholder = translationHelper.get('skills');
                scope.keywordSearchPlaceholder = translationHelper.get('keyword_search');
                scope.typesOfInterestPlaceholder = translationHelper.get('job_types');
                scope.levelOfInterestPlaceholder = translationHelper.get('job_search_stage');

                scope.listLimit = 10; // used by all of the tabs
                scope.offset = 0; // used only by tabs where usesCareerProfileFilterSet is false
                const preloadedParams = {
                    // The serverLimit should be twice the value of the listLimit to ensure that the
                    // next page of profiles can be completely filled out if the user navigates to it.
                    serverLimit: scope.listLimit * 2,

                    // By making minLength a little larger than twice the listLimit, it ensures that the next page
                    // always has enough profiles to be filled, if possible. This means more frequent trips to the
                    // server for more profiles, but prevents the UI from attaching profiles to the bottom of the
                    // list in a jarring way and then suddenly updating the counter upon doing so.
                    minLength: scope.listLimit * 2 + 1,
                    offset: 0,
                };

                scope.proxy = {};

                function preloadCareerProfilesForCareerProfileFilterSet() {
                    if (scope.currentTabConfig.usesCareerProfileFilterSet && scope.careerProfileFilterSet) {
                        scope.careerProfileFilterSet.ensureCareerProfilesPreloaded();
                    }
                }

                // NOTE: gets called immediately due to the $watch on the currentUser
                scope.applyFilters = () => {
                    if (scope.currentUserCanBrowseProfiles) {
                        scope.hasAppliedFilters = scope.hasSelectedFilters;

                        if (scope.currentTabConfig.usesCareerProfileFilterSet) {
                            createNewCareerProfileFilterSetIfAppropriate();
                            preloadCareerProfilesForCareerProfileFilterSet();
                        } else {
                            careerProfilesOrHiringRelationshipViewModels = getFilteredCareerProfilesOrHiringRelationshipViewModelsForTab(
                                scope.currentTab,
                            );

                            // If the user has applied filters, send them back to the first page of the list.
                            // Otherwise, the list offset may get out of sync if the user has moved to a different
                            // page of the results, applies different filters, and a smaller list of profiles gets returned.
                            scope.setOffset(scope.offset, 'first', scope.listLimit);

                            updateTabCount(scope.currentTab);
                        }
                    } else {
                        scope.careerProfileFilterSet = null;
                    }

                    setMobileHeader();
                    scope.mobileState.expanded = false;
                };

                // creates a new CareerProfileFilterSet derived from scope.filters and sets the careerProfileFilterSet
                // on the scope, but only if currentUserCanBrowseProfiles
                function createNewCareerProfileFilterSetIfAppropriate() {
                    if (scope.currentUserCanBrowseProfiles) {
                        // set these two here instead of directly on scope.filters because these inputs come
                        // from <organization-autocomplete> which returns an object rather than a string
                        scope.filters.company_name =
                            scope.proxy.professionalOrganization && scope.proxy.professionalOrganization.text !== ''
                                ? scope.proxy.professionalOrganization.text
                                : undefined;
                        scope.filters.school_name =
                            scope.proxy.educationalOrganization && scope.proxy.educationalOrganization.text !== ''
                                ? scope.proxy.educationalOrganization.text
                                : undefined;
                        const sort = defaultFiltersSelected()
                            ? ['PRIORITIZE_HIRING_INDEX', 'BEST_SEARCH_MATCH_FOR_HM']
                            : ['BEST_SEARCH_MATCH_FOR_HM'];
                        scope.careerProfileFilterSet = new CareerProfileFilterSet(scope.filters, sort, preloadedParams);
                    }
                }

                // ensure that a company_name aded to filters from a saved career_profile_search updates
                // the input
                scope.$watchGroup(['scope.proxy.professionalOrganization', 'filters.company_name'], () => {
                    const companyName = scope.filters && scope.filters.company_name;
                    const professionalOrganization = scope.proxy.professionalOrganization;
                    if (professionalOrganization && companyName) {
                        scope.proxy.professionalOrganization.text = companyName;
                    }
                });

                scope.resetFilters = filters => {
                    scope.proxy.showAdvancedFilters = false;

                    _.each(filters || scope.filters, (val, key) => {
                        if (key === 'only_local') {
                            scope.filters[key] = false;
                        } else if (key === 'keyword_search' || key === 'in_school') {
                            scope.filters[key] = null;
                        } else if (key === 'company_name') {
                            scope.proxy.professionalOrganization = undefined;
                            scope.filters[key] = undefined;
                        } else if (key === 'school_name') {
                            scope.proxy.educationalOrganization = undefined;
                            scope.filters[key] = undefined;
                        } else {
                            scope.filters[key] = [];
                        }
                    });
                };

                // resets the filters and updates the mobile header to be "No Preferences Selected"
                function resetFiltersAndUpdateMobileHeader() {
                    scope.resetFilters();
                    setMobileHeader();
                }

                // careerProfileFilterSet.careerProfiles is manipulated via the candidateList
                // directives for tabs where usesCareerProfileFilterSet is true
                scope.$watch(
                    'careerProfileFilterSet.careerProfiles.length',
                    preloadCareerProfilesForCareerProfileFilterSet,
                );

                // by default, don't show advanced filters
                scope.proxy.showAdvancedFilters = false;

                // requires a deep watch because scope.fitlers has properties mapped to arrays, which
                // won't trigger this listener when changed for a regular $watch or a $watchCollection
                scope.$watch('filters', setHasSelectedFilters, true);

                // NOTE: This gets triggered upon initialization of this directive
                scope.$watchGroup(
                    ['currentUser', 'currentUser.hasFullHiringManagerAccess', 'currentUserCanBrowseProfiles'],
                    () => {
                        // Reset the loading states when ping causes this to trigger so the placeholder
                        // UI is correct. See scope.tabConfigs and getTabConfig() to figure out what
                        // needs to be set.
                        _.each(scope.tabConfigs, (tabConfig, key) => {
                            if (key !== 'all') {
                                tabConfig.loading = true;
                            }
                        });

                        if (!scope.filters) {
                            scope.filters = {
                                only_local: false,
                                places: [],
                                industries: [], // advanced
                                preferred_primary_areas_of_interest: [],
                                years_experience: [],
                                skills: [], // advanced
                                keyword_search: undefined,
                                company_name: undefined, // advanced
                                school_name: undefined, // advanced
                                in_school: null, // advanced
                                employment_types_of_interest: [],
                                levels_of_interest: [],
                            };
                        }

                        // Upon initialization of this directive, use the hiring manager's most recent
                        // career profile search in preparation for preloading the career profiles for
                        // the careerProfileFilterSet.
                        // NOTE: Even if usesCareerProfileFilterSet is false for the currentTab, the UI
                        // still depends on careerProfileFilterSet being set on the scope.
                        extendFiltersWithMostRecentCareerProfileSearch();
                        createNewCareerProfileFilterSetIfAppropriate();

                        if (scope.currentTabConfig.usesCareerProfileFilterSet) {
                            scope.applyFilters();
                        } else {
                            // since this listener gets triggered upon intialization of this directive,
                            // if the currentTab doesn't use the career profile filter set, reset the
                            // filters so the filters are empty and ensure the mobile header is set so
                            // the "No Preferences Selected" header shows up on mobile
                            resetFiltersAndUpdateMobileHeader();
                            ensureHiringRelationshipsLoadedForTab(scope.currentTab);
                        }

                        // lastly after all other more important API calls have been made, ensure the hiring
                        // relationships are loaded for that tabs that require a count
                        ensureHiringRelationshipsLoadedForTabCountsOnInit();
                    },
                );

                function extendFiltersWithMostRecentCareerProfileSearch() {
                    if (scope.currentUser) {
                        const search = scope.currentUser.hiring_application.recent_career_profile_searches[0];
                        if (search) {
                            _.extend(scope.filters, search.cloneFilters());

                            if (scope.filters.company_name) {
                                scope.proxy.professionalOrganization = {
                                    text: scope.filters.company_name,
                                };
                            }

                            if (scope.filters.school_name) {
                                scope.proxy.educationalOrganization = {
                                    text: scope.filters.school_name,
                                };
                            }
                        }
                    }

                    // if one or more advanced filters aren't empty, show them
                    if (
                        scope.currentTabConfig.usesCareerProfileFilterSet &&
                        (!_.isEmpty(scope.filters.skills) ||
                            !_.isEmpty(scope.filters.industries) ||
                            !_.isEmpty(scope.filters.levels_of_interest) ||
                            scope.filters.company_name ||
                            scope.filters.school_name ||
                            scope.filters.in_school)
                    ) {
                        scope.proxy.showAdvancedFilters = true;
                    }
                }

                scope.$watch('proxy.showAdvancedFilters', () => {
                    if (scope.proxy.showAdvancedFilters === false) {
                        // pick out the advanced filters and reset them
                        scope.resetFilters(
                            _.pick(
                                scope.filters,
                                'skills',
                                'industries',
                                'company_name',
                                'school_name',
                                'in_school',
                                'levels_of_interest',
                            ),
                        );
                    }
                });

                function setHasSelectedFilters() {
                    // Note: this assumes that all filters are lists
                    scope.hasSelectedFilters = _.chain(scope.filters)
                        .map((val, key) => {
                            if (key === 'only_local' || key === 'keyword_search' || key === 'in_school') {
                                return val || undefined;
                            }
                            // the values of all other filters are lists of
                            // desired options
                            return _.any(val);
                        })
                        .any()
                        .value();
                }

                function getFilterSummary(key) {
                    let summary;

                    if (
                        (key === 'keyword_search' || key === 'company_name' || key === 'school_name') &&
                        scope.filters[key]
                    ) {
                        summary = `"${scope.filters[key]}"`;
                    } else if (_.any(scope.filters[key])) {
                        if (key === 'skills') {
                            summary = scope.filters[key][0];
                        } else if (key === 'levels_of_interest') {
                            summary = candidateListCardTranslationHelper.get(`status_${scope.filters[key][0]}`);
                        } else if (key === 'places') {
                            const firstPlace = scope.filters[key][0];
                            summary = HasLocation.localizedLocationOrLocationString(firstPlace);
                        } else {
                            summary = fieldsTranslationHelper.get(scope.filters[key][0]);
                        }

                        const moreCount = scope.filters[key].length - 1;
                        if (moreCount > 0) {
                            summary += translationHelper.get(
                                `plus_n_${key}`,
                                {
                                    count: moreCount,
                                },
                                undefined,
                                'messageformat',
                            );
                        }
                    }

                    return summary;
                }

                function defaultFiltersSelected() {
                    const defaultKeys = [
                        'only_local',
                        'places',
                        'preferred_primary_areas_of_interest',
                        'industries',
                        'years_experience',
                        'skills',
                        'keyword_search',
                        'company_name',
                        'school_name',
                        'in_school',
                        'employment_types_of_interest',
                        'levels_of_interest',
                    ];

                    return _.chain(scope.filters)
                        .map((val, key) => {
                            if (defaultKeys.includes(key)) {
                                if (key === 'only_local' || key === 'keyword_search') {
                                    return !val; // false or undefined
                                }
                                if (Array.isArray(val)) {
                                    return val === [] || val.length === 0;
                                }
                            } else {
                                return true; // ignore any extraneous filters
                            }
                        })
                        .all()
                        .value();
                }

                function setMobileHeader() {
                    const mobileHeaders = _.compact([
                        getFilterSummary('places'),
                        getFilterSummary('preferred_primary_areas_of_interest'),
                        getFilterSummary('industry'),
                        getFilterSummary('years_experience'),
                        getFilterSummary('skills'),
                        getFilterSummary('keyword_search'),
                        getFilterSummary('company_name'),
                        getFilterSummary('school_name'),
                        getFilterSummary('in_school'),
                        getFilterSummary('employment_types_of_interest'),
                        getFilterSummary('levels_of_interest'),
                    ]);

                    if (_.any(mobileHeaders)) {
                        scope.selectedFiltersSummary = mobileHeaders.join(', ');
                    } else {
                        scope.selectedFiltersSummary = translationHelper.get('no_preferences_selected');
                    }
                }

                // create a map of the filters that are suppored on the client and their repsective
                // properties on the appropriate model (hiringRelationshipViewModel or careerProfile)
                // for use in getFilteredCareerProfilesOrHiringRelationshipViewModelsForTab
                const supportedClientSideFiltersMap = {
                    preferred_primary_areas_of_interest: 'primary_areas_of_interest',
                    keyword_search: 'searchableText',
                };

                // Returns the filtered hiring relationship view-models or career profiles for the currentTab.
                // If the requested tab is not the currentTab, it will return the unfiltered hiring relationship
                // view-models or career profiles. Configure the derivesCandidatesFromHiringRelationshipViewModels
                // tab config property to inform this function if the returned models should be hiring relationship
                // view-models or career profiles.
                function getFilteredCareerProfilesOrHiringRelationshipViewModelsForTab(tabName) {
                    // create a regex for the keyword_search filter, but only if necessary
                    const regex =
                        scope.hasSelectedFilters && scope.filters.keyword_search
                            ? new RegExp(scope.filters.keyword_search, 'i')
                            : null;

                    // reset careerProfiles by filtering through the unfiltered list of career profiles for the tab
                    if (scope.hasSelectedFilters && scope.currentTab === tabName) {
                        const filteredHiringRelationshipViewModels = _.filter(
                            scope.hiringRelationshipViewModelsMap[tabName],
                            hiringRelationshipViewModel =>
                                _.keys(supportedClientSideFiltersMap).every(filterKey => {
                                    // First check if the filter actually needs processing or not.
                                    // If processing isn't required, return true to proceed onto the next filter.
                                    if (!scope.filters[filterKey] || scope.filters[filterKey].length === 0) {
                                        return true;
                                    }
                                    if (filterKey === 'keyword_search' && scope.filters[filterKey]) {
                                        return regex.test(
                                            hiringRelationshipViewModel[supportedClientSideFiltersMap[filterKey]],
                                        );
                                    }
                                    // at this point we assume the filter is an array and check it against the careerProfile attached to the hiringRelationshipViewModel
                                    return (
                                        _.intersection(
                                            hiringRelationshipViewModel.careerProfile[
                                                supportedClientSideFiltersMap[filterKey]
                                            ],
                                            scope.filters[filterKey],
                                        ).length > 0
                                    );
                                }),
                        );

                        // appropriately return either the filtered hiring relationship view-models or their career profiles
                        return scope.tabConfigs[tabName].derivesCandidatesFromHiringRelationshipViewModels
                            ? filteredHiringRelationshipViewModels
                            : _.pluck(filteredHiringRelationshipViewModels, 'careerProfile');
                    }

                    // otherwise, just return the appropriate unfiltered list
                    return getUnfilteredCareerProfilesOrHiringRelationshipViewModelsForTab(tabName);
                }

                //--------------------------
                // Tab Helpers
                //--------------------------

                scope.setOffset = (offset, action, listLimit) => {
                    scope.offset = listOffsetHelper.setOffset(offset, action, listLimit);
                };

                // Callback function that gets passed down into the candidateList scope to be
                // executed after a profile gets removed. We can't rely on changes to the length of the careerProfiles
                // array to know that a candidate was reviewed because the hiring manager can change the length of the
                // careerProfiles array via filtering, which certainly does not mean that a candidate was reviewed.
                // So, the only way to be certain that a profile was reviewed is for the candidateList
                // directives to execute this callback when it's appropriate.
                scope.afterCandidateReviewed = item => {
                    if (scope.currentTabConfig.supportsDeepLinking && item && item._deepLinked) {
                        resetDeepLinkedCareerProfile();
                    }
                    // log a 'hiring_manager:all_candidates_reviewed' event if the last candidate in the UNFILTERED
                    // list of career profiles has been reviewed and the currentTab supportsAllCandidatesReviewedEvent
                    if (
                        scope.currentTabConfig.supportsAllCandidatesReviewedEvent &&
                        scope.careerProfilesMap[scope.currentTab].length === 0
                    ) {
                        EventLogger.log('hiring_manager:all_candidates_reviewed');
                    }
                };

                scope.$watch('currentTab', (newTab, oldTab) => {
                    // handle changes to the currentTab
                    if (newTab && oldTab && newTab !== oldTab) {
                        // if there happen to be query params in the URL for a deep linked candidate,
                        // remove them so they don't muck up the UI in the new tab
                        sanitizeUrlSearchParams(true);
                        scope.unsupportedDeepLink = false;

                        // the contents of this conditional only need to be run if currentUserCanBrowseProfiles
                        if (scope.currentUserCanBrowseProfiles) {
                            // update the offsets to 0 so they're sent to the first page of results
                            scope.setOffset(scope.offset, 'first', scope.listLimit);
                            scope.careerProfileFilterSet.setOffset(scope.offset, 'first', scope.listLimit);

                            // ensure that the filters and the mobile header from the previous tab aren't
                            // carried over to the new tab
                            resetFiltersAndUpdateMobileHeader();

                            resetDeepLinkedCareerProfile();

                            // if the newTab usesCareerProfileFilterSet, extend the filters with the
                            // filters from a recent career profile search so the filters properly
                            // reflects the candidates displayed in the UI
                            if (scope.tabConfigs[newTab].usesCareerProfileFilterSet) {
                                extendFiltersWithMostRecentCareerProfileSearch();
                                // if the current careerProfileFilterSet hasn't attempted to fetch any career profiles yet,
                                // then apply the filters so the career profiles start loading
                                if (!scope.careerProfileFilterSet.hasAttemptedToFetchInitialResults) {
                                    scope.applyFilters();
                                }
                            } else {
                                ensureHiringRelationshipsLoadedForTab(newTab);
                            }
                        }
                    }
                });

                // This is aannoying, but it's not enough to watch just the length of the careerProfiles because the 'all' tab
                // apparently triggers the careerProfiles length listener before the hiring relationship view-model gets created
                // on the careers network view-model, so we need both listeners to handle this scenario.
                scope.$watch('careerProfilesOrHiringRelationshipViewModels.length', updateTabCountsOnChange);
                scope.$watch('careersNetworkViewModel.hiringRelationshipViewModels.length', updateTabCountsOnChange);

                function updateTabCountsOnChange(newLength, oldLength) {
                    if (
                        scope.currentUserCanBrowseProfiles &&
                        angular.isDefined(newLength) &&
                        angular.isDefined(oldLength) &&
                        newLength !== oldLength
                    ) {
                        updateTabCounts();
                    }
                }

                // Returns early and does nothing if the tabConfig associated with the passed in tabName usesCareerProfileFilterSet,
                // otherwise ensures the hiring relationships are loaded for the tab associated with the tabName and then updates
                // the tab counts
                function ensureHiringRelationshipsLoadedForTab(tabName, independent) {
                    const tabConfig = scope.tabConfigs[tabName];
                    if (tabConfig.usesCareerProfileFilterSet || !scope.currentUserCanBrowseProfiles) {
                        return $q.when();
                    }

                    independent = independent || false;
                    return scope.careersNetworkViewModel
                        .ensureHiringRelationshipsLoaded(tabConfig.listName, independent)
                        .then(() => {
                            if (scope.currentUserCanBrowseProfiles) {
                                return getOrInitializeHiringRelationshipViewModelForDeepLinkedCareerProfile(
                                    tabName,
                                ).then(() => {
                                    setUnfilteredCareerProfilesAndHiringRelationshipViewModelsForTab(tabName);
                                    scope.tabConfigs[tabName].loading = false;
                                    resetFilteredCandidates(tabName);
                                    updateTabCounts();
                                });
                            }
                            scope.careerProfilesMap[tabName] = scope.hiringRelationshipViewModelsMap[tabName] = null;
                            scope.tabConfigs[tabName].loading = false;
                            resetFilteredCandidates(tabName);

                            return $q.when();
                        });
                }

                // Ensures the hiring relationships are loaded for tabs where supportsPreloadingHiringRelationshipsOnInit
                // is true. This logic should be run after all other necessary initial API calls have been triggered.
                // As such, these API calls are independent of the necessary API calls for the current list so we pass
                // true as the second arg to indicate that they are independent.
                function ensureHiringRelationshipsLoadedForTabCountsOnInit() {
                    if (scope.currentUserCanBrowseProfiles) {
                        _.each(tabs, tab => {
                            if (scope.tabConfigs[tab].supportsPreloadingHiringRelationshipsOnInit) {
                                ensureHiringRelationshipsLoadedForTab(tab, true);
                            }
                        });
                    }
                }

                // if tabName matches currentTab, set the list of filtered careerProfiles to the list of unfiltered career profiles for the tab
                function resetFilteredCandidates(tabName) {
                    if (scope.currentTab === tabName) {
                        careerProfilesOrHiringRelationshipViewModels = getUnfilteredCareerProfilesOrHiringRelationshipViewModelsForTab(
                            tabName,
                        );
                    }
                }

                scope.unsupportedDeepLink = false;

                // If the hiring manager has requested to view a shared candidate, which is done by supplying
                // a id in the query params of the URL, get the career profile associated with the id query param
                // and then gets or creates a pending hiring relationship view-model for the shared career profile.
                function getOrInitializeHiringRelationshipViewModelForDeepLinkedCareerProfile(tabName) {
                    if (scope.tabConfigs[tabName].supportsDeepLinking) {
                        const deepLinkedCareerProfileId = $location.search().id;

                        // if we haven't already processed the shared candidate and the query params
                        // indicate that the hiring manager is deep linking to a specific candidate,
                        // ensure that a hiring relationship view-model for the candidate is present
                        if (!processedDeepLinkedCareerProfile && deepLinkedCareerProfileId) {
                            let careerProfile;
                            let myViewModel;
                            // first, get the profile for the deep linked candidate, skipping the FrontRoyal.ApiErrorHandler
                            // because the candidateList has its own error messaging for deep linked profiles...
                            return (
                                CareerProfile.show(
                                    deepLinkedCareerProfileId,
                                    {
                                        view: 'career_profiles',
                                        deep_link: true,
                                    },
                                    {
                                        'FrontRoyal.ApiErrorHandler': {
                                            skip: true,
                                        },
                                    },
                                )
                                    // ... then attempt to get the hiring relationship view-model between this hiring manager and the candidate
                                    // making sure to check the API if it's not initially found (otherwise, the actions on the candidate card
                                    // may be the wrong actions...
                                    .then(response => {
                                        careerProfile = response.result;
                                        return scope.careersNetworkViewModel.getMyHiringRelationshipViewModelForConnectionId(
                                            careerProfile.user_id,
                                            true,
                                        );
                                    })
                                    // ... then if we found a view-model, we can set hiringRelationshipViewModelForDeepLinkedCareerProfile
                                    // on the scope, otherwise, the hiring relationship doesn't exist or it's hidden. If that's the case,
                                    // we need to check hiring relationships for the hiring manager's teammates to see if any teammates have
                                    // a connection with the deep linked candidate since teammate connections are visible to the hiring manager...
                                    .then(myHiringRelationshipViewModel => {
                                        myViewModel = myHiringRelationshipViewModel;
                                        return (
                                            myViewModel ||
                                            scope.careersNetworkViewModel.ensureHiringRelationshipsLoaded(
                                                'teamCandidates',
                                            )
                                        );
                                    })
                                    // ... then after we've loaded up the hiring relationships for the teamCandidates, we need to attempt
                                    // to get the accepted hiring relationship view-model between the teammate the deep linked candidate...
                                    .then(
                                        () =>
                                            myViewModel ||
                                            scope.careersNetworkViewModel.getHiringRelationshipViewModelAcceptedByTeam(
                                                careerProfile.user_id,
                                            ),
                                    )
                                    .then(
                                        viewModel =>
                                            viewModel ||
                                            scope.careersNetworkViewModel.getOrInitializeHiringRelationshipViewModelForCareerProfile(
                                                careerProfile,
                                            ),
                                    )
                                    // ... then we can set hiringRelationshipViewModelForDeepLinkedCareerProfile on the scope and the _deepLinked
                                    // flag on the hiring relationship view-model as well, which is required for the candidateList directive
                                    .then(viewModel => {
                                        if (viewModel) {
                                            scope.hiringRelationshipViewModelForDeepLinkedCareerProfile = viewModel;
                                            scope.hiringRelationshipViewModelForDeepLinkedCareerProfile._deepLinked = true;
                                        }
                                    })
                                    // if at any point any of the previous steps fail, set the unsupportedDeepLink flag on the scope
                                    .catch(response => {
                                        HttpQueue.unfreezeAfterError(response.config);
                                        scope.unsupportedDeepLink = true;

                                        if (response.status === 404) {
                                            scope.unsupportedDeepLinkKey =
                                                'careers.hiring_browse_candidates.inactive_deep_link_error_message';
                                        } else {
                                            scope.unsupportedDeepLinkKey =
                                                'careers.hiring_browse_candidates.default_deep_link_error_message';
                                        }
                                    })
                            );
                        }
                    }
                    processedDeepLinkedCareerProfile = true;
                    return $q.when();
                }

                // Sets the unfilterd list of hiring relationship view-models and unfiltered
                // list of career profiles for the tab associated with the passed in tabName
                function setUnfilteredCareerProfilesAndHiringRelationshipViewModelsForTab(tabName) {
                    // set the hiring relationship view-models for the tab
                    scope.hiringRelationshipViewModelsMap[tabName] = _.chain(allHiringRelationshipViewModels())
                        // tabs may require a different filtering process depending on its needs,
                        // so we delegate the filtering process to the filter method that should be
                        // attached to the tab config associated with the tabName that's passed in
                        .filter(vm => scope.tabConfigs[tabName].filter(vm))
                        .sortBy(vm => vm.hiringRelationship.hiring_manager_priority)
                        .value();

                    scope.careerProfilesMap[tabName] = _.pluck(
                        scope.hiringRelationshipViewModelsMap[tabName],
                        'careerProfile',
                    );
                }

                function allHiringRelationshipViewModels() {
                    return _.uniq(
                        scope.careersNetworkViewModel.hiringRelationshipViewModels.concat(
                            _.compact([scope.hiringRelationshipViewModelForDeepLinkedCareerProfile]),
                        ),
                    );
                }

                function resetDeepLinkedCareerProfile() {
                    $location.search('id', null);

                    if (scope.hiringRelationshipViewModelForDeepLinkedCareerProfile) {
                        delete scope.hiringRelationshipViewModelForDeepLinkedCareerProfile._deepLinked;
                        delete scope.hiringRelationshipViewModelForDeepLinkedCareerProfile;
                    }
                }

                // updates the count for each tab
                function updateTabCounts() {
                    _.each(tabs, updateTabCount);
                }

                // updates the count for the tab associated with the passed in tabName
                function updateTabCount(tabName) {
                    // tabs where usesCareerProfileFilterSet is true get their count from the careerProfileFilterSet
                    if (scope.tabConfigs[tabName].usesCareerProfileFilterSet) {
                        return;
                    }
                    const candidateTab = _.findWhere(scope.candidatesTabs, {
                        name: tabName,
                    });
                    candidateTab.translate.values.count = getTabCount(tabName);
                }

                // Returns the filtered profile count for the tab that matches the passed in tabName.
                // If the passed in tabName is the currentTab, the count for the filtered list of
                // candidates is returned, otherwise the count for the unfiltered candidates is returned.
                function getTabCount(tabName) {
                    // filter the hiringRelationshipViewModels on the careersNetworkViewModel using the configured filter function on the tab config
                    setUnfilteredCareerProfilesAndHiringRelationshipViewModelsForTab(tabName);
                    const candidates =
                        scope.currentTab === tabName
                            ? careerProfilesOrHiringRelationshipViewModels
                            : getUnfilteredCareerProfilesOrHiringRelationshipViewModelsForTab(tabName);
                    return candidates && candidates.length;
                }

                // gets the unfiltered career profiles or hiring relationship
                // view-models for the tab associated with the passed in tabName
                function getUnfilteredCareerProfilesOrHiringRelationshipViewModelsForTab(tabName) {
                    return scope.tabConfigs[tabName].derivesCandidatesFromHiringRelationshipViewModels
                        ? scope.hiringRelationshipViewModelsMap[tabName]
                        : scope.careerProfilesMap[tabName];
                }
            },
        };
    },
]);
