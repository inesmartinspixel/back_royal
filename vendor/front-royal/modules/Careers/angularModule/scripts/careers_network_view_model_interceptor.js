import angularModule from 'Careers/angularModule/scripts/careers_module';

angularModule.factory('CareersNetworkViewModelInterceptor', [
    '$injector',

    $injector => {
        // The subscriptions_controller will push down an
        // open position when a subscription is created for it, so that
        // we can have access to the `subscription_id` in the client
        function handlePushedOpenPositions(response) {
            const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
            const OpenPosition = $injector.get('OpenPosition');

            let openPositionAttrs;
            try {
                openPositionAttrs = response.data.meta.open_positions;
            } catch (e) {}

            if (!openPositionAttrs) {
                return;
            }

            const careersNetworkViewModel = CareersNetworkViewModel.get('hiringManager');

            _.each(openPositionAttrs, attrs => {
                const openPosition = OpenPosition.new(attrs);
                careersNetworkViewModel.commitOpenPosition(openPosition);
            });
        }

        return {
            response(response) {
                handlePushedOpenPositions(response);
                return response;
            },
        };
    },
]);

angularModule.config([
    '$httpProvider',
    $httpProvider => {
        $httpProvider.interceptors.push('CareersNetworkViewModelInterceptor');
    },
]);
