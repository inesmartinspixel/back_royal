import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/hiring_manager_contact_owner.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import iconWarningWhite from 'vectors/icon-warning-white.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('hiringManagerContactOwner', [
    '$injector',

    function factory($injector) {
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const isMobileMixin = $injector.get('isMobileMixin');
        const $rootScope = $injector.get('$rootScope');

        return {
            scope: {},
            restrict: 'E',
            templateUrl,

            link(scope) {
                scope.iconWarningWhite = iconWarningWhite;

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                isMobileMixin.onLink(scope);

                scope.$watch('isMobile', val => {
                    AppHeaderViewModel.toggleVisibility(!val);
                });

                scope.$on('$destroy', () => {
                    AppHeaderViewModel.toggleVisibility(true);
                });
            },
        };
    },
]);
