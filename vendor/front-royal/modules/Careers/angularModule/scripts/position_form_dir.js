import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/position_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import hireWithConfidence from 'vectors/how_it_works/employer/hire-with-confidence.svg';
import congrats from 'vectors/congrats.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('positionForm', [
    '$injector',

    function factory($injector) {
        const TranslationHelper = $injector.get('TranslationHelper');
        const $location = $injector.get('$location');
        const scopeTimeout = $injector.get('scopeTimeout');
        const scrollHelper = $injector.get('scrollHelper');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
        const $rootScope = $injector.get('$rootScope');
        const HiringTeamCheckoutHelper = $injector.get('Payments.HiringTeamCheckoutHelper');
        const HiringTeam = $injector.get('HiringTeam');
        const DialogModal = $injector.get('DialogModal');
        const $q = $injector.get('$q');

        return {
            scope: {
                position: '<',
                hiringManager: '<',

                // Used to override default behavior after saving open position (e.g. In the admin pages)
                saveCallback: '&?',
            },
            restrict: 'E',
            templateUrl,

            link(scope) {
                scope.$location = $location;

                NavigationHelperMixin.onLink(scope);

                // The hiring team can be initially null if the user did not
                // get one assigned when they signed up because their domain was
                // already attached to a team
                scope.$watch('hiringManager.hiring_team', hiringTeam => {
                    if (hiringTeam) {
                        scope.hiringTeamCheckoutHelper = new HiringTeamCheckoutHelper(hiringTeam);
                        scope.stripePlan = hiringTeam.stripePlanForHiringPlan(HiringTeam.HIRING_PLAN_PAY_PER_POST);
                    } else {
                        scope.hiringTeamCheckoutHelper = null;
                        scope.stripePlan = null;
                    }
                });

                // see also: form_helper.js::supportForm
                const formListener = scope.$on('formHelperCurrentForm', (evt, formController) => {
                    scope.currentForm = formController;
                });
                scope.$on('$destroy', formListener);

                scope.navSteps = [
                    {
                        stepName: 'choose_plan',
                    },
                    {
                        stepName: 'add_position',
                    },
                    {
                        stepName: 'pay_and_publish',
                    },
                ];

                scope.gotoStep = index => {
                    if (index === 0) {
                        scope.loadRoute('hiring/plan');
                    } else {
                        if ($location.search().page === index || !scope.currentForm.$valid) {
                            return;
                        }

                        $location.search('page', index);
                    }
                };

                scope.stepsProgressMap = {
                    choose_plan: 'complete',
                    add_position: 'incomplete',
                    pay_and_publish: 'incomplete',
                };

                const translationHelper = new TranslationHelper('careers.position_form');
                const positionsTranslationHelper = new TranslationHelper('careers.positions');

                scope.previewTextKey = !scope.hiringManager.canCreateFeaturedPositions
                    ? 'careers.position_form.preview_card_will_appear'
                    : 'careers.position_form.preview_card';

                // We do not show this if there is no hiring team because in that case we will be showing
                // them another warning message.  See hiring-lock-dialog
                if (
                    scope.hiringManager.usingLegacyHiringPlan &&
                    !scope.hiringManager.canCreateFeaturedPositions &&
                    !scope.hiringManager.has_drafted_open_position &&
                    scope.hiringManager.hiring_team
                ) {
                    DialogModal.alert({
                        title: translationHelper.get('welcome_to_smartly_talent'),
                        content:
                            `<p><img src="${hireWithConfidence}" /></p>` +
                            '<p translate-once="careers.position_form.post_your_first_position"></p>' +
                            '<button class="flat blue" translate-once="careers.position_form.add_position" ng-click="closeModal()"></button>',
                        classes: ['center', 'welcome-to-smartly-talent'],
                        hideCloseButton: true,
                        blurTargetSelector: '[ng-controller]',
                        scope: {
                            closeModal() {
                                DialogModal.hideAlerts();
                            },
                        },
                    });
                }

                scope.hasPostedPosition = scope.hiringManager.has_drafted_open_position;

                scope.isDraftedPosition =
                    scope.position.drafted_from_positions ||
                    (scope.position.draftedFromInvite && !scope.position.featured);

                const careersNetworkViewModel = CareersNetworkViewModel.get('hiringManager');

                scope.cancel = () => {
                    if (!scope.cancelCallback) {
                        $location.search('positionId', null);
                        $location.search('page', null);
                    } else {
                        scope.cancelCallback();
                    }
                };

                //-----------------------------
                // Multi-Step Form Config
                //-----------------------------

                const action = $location.search().action;

                const stepsSubtitles = {
                    add_position: scope.position.draftedFromInvite
                        ? positionsTranslationHelper.get('draft_message')
                        : undefined,
                    success: translationHelper.get('success_subtitle'),
                };

                let steps;
                let currentPage = $location.search().page;
                currentPage = currentPage ? currentPage.toString() : null;
                if ($rootScope.currentUser.hasAdminAccess) {
                    steps = ['add_position'];
                }

                // If they are going to have to pay, or if they just refreshed on the success screen,
                // then we need all three steps
                else if (publishWillRequirePayment() || currentPage === '3') {
                    steps = ['add_position', 'pay_and_publish', 'success'];
                } else {
                    steps = ['add_position', 'success'];
                }

                function publishWillRequirePayment() {
                    return (
                        scope.hiringManager.postingJobsRequiresPayment &&
                        !scope.hiringManager.subscriptionForPosition(scope.position)
                    );
                }

                scope.stepsInfo = _.map(steps, section => {
                    // return a template that passes along necessary model info
                    // NOTE: all of these directives will use the shared scope
                    const directiveName = `${section.replace(/_/g, '-').snakeCase('-')}-form`;

                    const templateStr = `<${directiveName}></${directiveName}>`;
                    const sectionKey = action !== 'create' && section === 'add_position' ? 'update_job_post' : section;
                    const title = translationHelper.get(sectionKey);

                    let buttonTextKey;

                    if (section === 'pay_and_publish') {
                        buttonTextKey = 'pay_and_publish';
                    } else if (section === 'success') {
                        buttonTextKey = 'continue';
                    } else if (publishWillRequirePayment()) {
                        buttonTextKey = 'continue';
                    } else if (scope.position.id && scope.position.archived) {
                        buttonTextKey = 'save';
                    } else if (scope.position.id && scope.position.featured) {
                        buttonTextKey = 'update_job_post';
                    } else if (!scope.position.id || !scope.position.featured) {
                        buttonTextKey = scope.hiringManager.canCreateFeaturedPositions ? 'publish' : 'save';
                    }

                    const buttonText = buttonTextKey ? translationHelper.get(buttonTextKey) : '';
                    const subtitle = stepsSubtitles[section];

                    return {
                        key: section,
                        template: templateStr,
                        title,
                        hasForm: true,
                        data: {
                            subtitle,
                            buttonText,
                            section,
                        },
                    };
                });

                // When a user clicks the renew button on the positions bar from the
                // closed section of the positions screen, and the position does not
                // currently have a subscription, the user ends up here and should
                // be on the pay_and_publish page
                scope.$watchGroup(['$location.search().startPage', 'stepsInfo'], () => {
                    const startPage = $location.search().startPage;
                    if (startPage) {
                        const step = _.findWhere(scope.stepsInfo, {
                            key: startPage,
                        });
                        $location.search('page', scope.stepsInfo.indexOf(step) + 1);
                        $location.search('startPage', null);
                    }
                });

                scope.scrollToContentTop = () => {
                    scopeTimeout(scope, () => {
                        const mainBox = $('.main-box');
                        const isFixed = mainBox.css('position') === 'fixed';
                        scrollHelper.scrollToTop(false, isFixed ? mainBox : undefined);
                    });
                };

                function showFirstPostModal() {
                    $location.search('action', 'review');
                    $location.search('page', null);

                    DialogModal.alert({
                        title: translationHelper.get('congrats_first_post'),
                        content:
                            `<p><img src="${congrats}" /></p>` +
                            '<p translate-once="careers.position_form.our_team_is_reviewing"></p>' +
                            '<button class="flat blue" translate-once="careers.position_form.view_your_position" ng-click="viewPosition()"></button>' +
                            '<button class="flat grey" translate-once="careers.position_form.add_another_position" ng-click="addPosition()"></button>',
                        classes: ['center', 'welcome-to-smartly-talent'],
                        hideCloseButton: true,
                        blurTargetSelector: '[ng-controller]',
                        scope: {
                            addPosition() {
                                DialogModal.hideAlerts();
                                $location.search('action', 'create');
                                $location.search('positionId', null);
                                $location.search('list', null);
                            },
                            viewPosition() {
                                DialogModal.hideAlerts();
                            },
                        },
                    });

                    // return a promise that never resolves.  The user will click one
                    // of the buttons in the modal and that will be handled by addPosition() or
                    // viewPosition().  We never need to go to the next step
                    return $q(() => {});
                }

                // on scope for testing purposes
                scope.savePosition = saveAsDraft => {
                    let showFirstPositionCongrats;

                    if (saveAsDraft) {
                        scope.position.drafted_from_positions = true;

                        if (!$rootScope.currentUser.hasAdminAccess && !scope.hiringManager.has_drafted_open_position) {
                            scope.hiringManager.has_drafted_open_position = true;
                            scope.hiringManager.save();

                            // The messaging in showFirstPostModal says that we are reviewing your application, so we
                            // only need to show it if !canCreateFeaturedPositions
                            showFirstPositionCongrats = !scope.hiringManager.canCreateFeaturedPositions;
                        }
                    } else if (!scope.position.archived) {
                        scope.position.featured = true;
                    }

                    return scope.position.save().then(() => {
                        // after creating a position, set the id in the url
                        $location.search('positionId', scope.position.id);
                        if (!scope.saveCallback) {
                            careersNetworkViewModel.commitOpenPosition(scope.position);

                            // If the user can't create featured positions, redirect them to the
                            // reivew page so that they are walked through the entire process of
                            // a position as a new user. Accepted users should know how it works
                            if (showFirstPositionCongrats) {
                                return showFirstPostModal();
                            }
                        } else {
                            scope.saveCallback();
                        }
                    });
                };

                scope.submit = (currentSection, nextStep, opts) => {
                    opts = opts || {};

                    // If overriding the create behavior then assume we don't want the scrolling logic either
                    if (!scope.saveCallback) {
                        const _nextStep = nextStep;
                        nextStep = () => {
                            _nextStep();
                            scope.scrollToContentTop();
                        };
                    }

                    if (currentSection === 'add_position') {
                        // This is hit when navigating from the form to the
                        // payment page
                        let saveAsDraft = opts.saveAsDraft || false;
                        if (!scope.hiringManager.canCreateFeaturedPositions || publishWillRequirePayment()) {
                            saveAsDraft = true;
                        }

                        // We always go to the next step after saving.  If that is the
                        // success step, and we are saving a draft, then the success directive
                        // will automatically navigate back to the positions page
                        scope.savePosition(saveAsDraft).then(nextStep);
                    } else if (currentSection === 'pay_and_publish') {
                        scope.hiringTeamCheckoutHelper
                            .subscribeToPayPerPostPlan(scope.stripePlan.id, scope.position.id)
                            .then(() => {
                                // FIXME: this may not be required since the server sets the position to
                                // featured anyway.
                                // See also: HiringTeam::SubscriptionConcern#handle_create_subscription_request
                                scope.position.featured = true;
                                nextStep();
                            });
                    } else if (currentSection === 'success') {
                        if (!scope.saveCallback) {
                            scope.loadRoute('/hiring/positions');
                        }
                    }
                };

                // We don't want this to change immediately upon clicking the save button, so we
                // watch $$saving instead of watching `position.featured`. (This is just so that it isn't awkward
                // in the UI when the draft button disappears becuase you pressed the publish button)
                scope.$watchGroup(
                    [
                        'position',
                        'position.$$saving',
                        'hiringManager.canCreateFeaturedPositions',
                        'hiringManager.postingJobsRequiresPayment',
                    ],
                    () => {
                        // Do not update this while saving or if the position is not set.
                        // Wait until the saving is done.
                        if (!scope.position || scope.position.$$saving) {
                            return;
                        }
                        scope.showSaveAsDraftButton =
                            !scope.position.featured &&
                            !scope.position.archived &&
                            scope.hiringManager.canCreateFeaturedPositions &&
                            !scope.hiringManager.postingJobsRequiresPayment;
                    },
                );
            },
        };
    },
]);
