import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/connections_list.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import inbox from 'vectors/inbox.svg';
import archive from 'vectors/archive.svg';
import usergroup from 'vectors/usergroup.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('connectionsList', [
    '$injector',

    function factory($injector) {
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
        const TranslationHelper = $injector.get('TranslationHelper');
        const HiringTeamInviteModal = $injector.get('HiringTeamInviteModal');
        const listOffsetHelper = $injector.get('listOffsetHelper');

        return {
            scope: {
                closed: '<',
                role: '<',
            },
            restrict: 'E',
            templateUrl,

            link(scope) {
                scope.inbox = inbox;
                scope.archive = archive;
                scope.usergroup = usergroup;

                scope.openInviteTeammateModal = () => {
                    HiringTeamInviteModal.open();
                };

                // selectize flips out if this is ever undefined
                scope.openPositionOptions = [];
                scope.teamMemberOptions = [];
                scope.proxy = {
                    selectedOpenPositionId: 'all',
                    selectedTeamMemberId: 'all',
                    currentTab: scope.closed ? 'closed' : 'connected',
                };

                scope.hasHiringRole = scope.role === 'hiringManager' || scope.role === 'hiringTeammate';

                // Each of these sections has a listName, indicating
                // which list needs to be sent into careersNetworkViewModel.ensureHiringRelationshipsLoaded
                // in order to load up the relationships for that list.
                //
                // The 'myCandidates' list will be preloaded by the careersNetworkViewModel,
                // (see comments in _preloadHiringRelationships).
                //
                // On the 'Closed' page for either hiring managers or candidates,
                // the 'rejectedOrClosed' list will be loaded
                //
                // NOTE: Right now, we expect that each tab (each instance of connections-list)
                // will only include one listName.  But, things should mostly work
                // even if there were multiple lists on one page.  The UI will show whatever
                // it has after each list loads, so it's not the perfect UI, but it
                // mostly works.  We may want to actually use this at some point and
                // fix up the UI.
                scope.expandedSections = {
                    // matched (and not closed)
                    connected: {
                        expanded: true,
                        loaded: false,
                        loading: false,
                        listName: 'myCandidates',
                        closed: false,
                        roles: ['hiringManager', 'hiringTeammate', 'candidate'],
                    },

                    closed: {
                        expanded: true,
                        loaded: false,
                        loading: false,
                        listName: 'closed',
                        closed: true,
                        roles: ['hiringManager', 'hiringTeammate', 'candidate'],
                    },

                    declined: {
                        expanded: true,
                        loaded: false,
                        loading: false,
                        listName: 'closed',
                        closed: true,
                        roles: ['candidate'],
                    },

                    // requests to which the candidate has not responded
                    new: {
                        expanded: true,
                        loaded: false,
                        loading: false,
                        listName: 'myCandidates',
                        closed: false,
                        roles: ['candidate'],
                    },
                };

                // pagination
                scope.listLimit = 200;
                scope.offset = 0;

                scope.getProfilesRelativeToOffset = (offset, action, listLimit) => {
                    scope.offset = listOffsetHelper.setOffset(offset, action, listLimit);
                };

                scope.careersNetworkViewModel = CareersNetworkViewModel.get(
                    scope.role === 'hiringTeammate' ? 'hiringManager' : scope.role,
                );

                if (scope.role === 'hiringManager') {
                    scope.setSearchFocused = searchFocused => {
                        scope.searchFocused = searchFocused;
                    };
                }

                // only hiring manager gets the fancy sticky headers
                scope.stickyHeaders = scope.role === 'hiringManager';

                // default search text is none
                scope.search = {
                    text: '',
                };

                function removeTeammateDuplicates(vm, list, uniqueConnections) {
                    // if this vm hasn't been logged, log it
                    if (!uniqueConnections.includes(vm.connectionId)) {
                        uniqueConnections.push(vm.connectionId);
                        return 'saved_for_later';
                    }

                    const originalIndex = _.findIndex(list, {
                        connectionId: vm.connectionId,
                    });

                    // create an array with the original's teammate avatar or add to it
                    if (!list[originalIndex].teammateAvatarSrcs) {
                        list[originalIndex].teammateAvatarSrcs = [list[originalIndex].teammateAvatarSrc].concat([
                            vm.teammateAvatarSrc,
                        ]);
                    } else if (!_.contains(list[originalIndex].teammateAvatarSrcs, vm.teammateAvatarSrc)) {
                        list[originalIndex].teammateAvatarSrcs.push(vm.teammateAvatarSrc);
                    }

                    // create an array with the original's teammate name or add to it
                    if (!list[originalIndex].teammateNames) {
                        list[originalIndex].teammateNames = [
                            list[originalIndex].hiringRelationship.hiring_manager_display_name,
                        ].concat([vm.hiringRelationship.hiring_manager_display_name]);
                    } else if (
                        !_.contains(
                            list[originalIndex].teammateNames,
                            vm.hiringRelationship.hiring_manager_display_name,
                        )
                    ) {
                        list[originalIndex].teammateNames.push(vm.hiringRelationship.hiring_manager_display_name);
                    }

                    // create an array with the teammate hiring manager id's or add to it
                    if (!list[originalIndex].teammateHiringManagerIds) {
                        list[originalIndex].teammateHiringManagerIds = [vm.hiringRelationship.hiring_manager_id];
                    } else if (
                        !_.contains(
                            list[originalIndex].teammateHiringManagerIds,
                            vm.hiringRelationship.hiring_manager_id,
                        )
                    ) {
                        list[originalIndex].teammateHiringManagerIds.push(vm.hiringRelationship.hiring_manager_id);
                    }

                    return 'ignore';
                }

                function getViewModelLists() {
                    const allHiringRelationshipViewModels = scope.careersNetworkViewModel.hiringRelationshipViewModels;
                    const filteredHiringRelationshipViewModels =
                        !scope.closed && scope.hasHiringRole
                            ? _.where(allHiringRelationshipViewModels, {
                                  role: scope.role,
                              })
                            : allHiringRelationshipViewModels;
                    const uniqueConnections = [];

                    const viewModelLists = _.groupBy(filteredHiringRelationshipViewModels, vm => {
                        if (scope.role !== vm.role && vm.role === 'hiringTeammate' && scope.closed && vm.closed) {
                            return 'closed_team';
                        }
                        if (scope.role !== vm.role) {
                            return 'ignore';
                        }
                        if (scope.closed && vm.closed) {
                            return 'closed';
                        }
                        if (
                            scope.closed &&
                            scope.careersNetworkViewModel.role === 'candidate' &&
                            vm.rejectedByCandidate
                        ) {
                            return 'declined';
                        }
                        if (
                            scope.closed &&
                            scope.careersNetworkViewModel.role === 'hiringManager' &&
                            vm.hiringRelationship.candidate_status === 'rejected'
                        ) {
                            return 'closed';
                        }
                        if (
                            scope.closed &&
                            scope.careersNetworkViewModel.role === 'hiringManager' &&
                            vm.hiringRelationship.status === 'rejected_by_hiring_manager'
                        ) {
                            return 'ignore';
                        }
                        if (
                            !scope.closed &&
                            scope.careersNetworkViewModel.role === 'hiringManager' &&
                            vm.hiringRelationship.status === 'rejected_by_hiring_manager'
                        ) {
                            return 'ignore';
                        }
                        if (scope.closed !== vm.closed) {
                            return 'ignore';
                        }
                        if (
                            scope.careersNetworkViewModel.role === 'hiringManager' &&
                            vm.hiringRelationship.status === 'waiting_on_hiring_manager'
                        ) {
                            return 'ignore';
                        }
                        if (
                            scope.careersNetworkViewModel.role === 'hiringManager' &&
                            vm.hiringRelationship.status === 'saved_for_later'
                        ) {
                            if (scope.role === 'hiringTeammate') {
                                return removeTeammateDuplicates(vm, allHiringRelationshipViewModels, uniqueConnections);
                            }
                            return 'saved_for_later';
                        }
                        if (
                            scope.careersNetworkViewModel.role === 'candidate' &&
                            vm.hiringRelationship.candidate_status === 'hidden'
                        ) {
                            return 'ignore';
                        }
                        if (
                            scope.careersNetworkViewModel.role === 'hiringManager' &&
                            vm.hiringRelationship.candidate_status === 'rejected'
                        ) {
                            return 'ignore';
                        }
                        if (
                            scope.careersNetworkViewModel.role === 'candidate' &&
                            vm.hiringRelationship.candidate_status === 'rejected'
                        ) {
                            return 'ignore';
                        }
                        if (
                            scope.careersNetworkViewModel.role === 'candidate' &&
                            vm.hiringRelationship.status === 'waiting_on_candidate'
                        ) {
                            return 'new';
                        }
                        if (scope.careersNetworkViewModel.role === 'candidate' && vm.matched && vm.newInstantMatch) {
                            return 'new';
                        }
                        if (
                            scope.careersNetworkViewModel.role === 'candidate' &&
                            vm.matched &&
                            vm.needsResponseFromMe
                        ) {
                            return 'new';
                        }
                        if (scope.careersNetworkViewModel.role === 'candidate' && vm.matched) {
                            return 'connected';
                        }
                        if (scope.careersNetworkViewModel.role === 'hiringManager' && vm.matched) {
                            return 'connected';
                        }
                        if (
                            scope.careersNetworkViewModel.role === 'hiringManager' &&
                            !vm.matched &&
                            vm.hiringRelationship.hiring_manager_status === 'accepted'
                        ) {
                            return 'liked';
                        }
                        $injector
                            .get('ErrorLogService')
                            .notifyInProd('Do not know what list this relationship should be in.', null, {
                                extra: {
                                    closed: scope.closed,
                                    role: scope.role,
                                    hiringRelationship: {
                                        id: vm.hiringRelationship.id,
                                        hiring_manager_status: vm.hiringRelationship.hiring_manager_status,
                                        candidate_status: vm.hiringRelationship.candidate_status,
                                        closed: vm.hiringRelationship.closed,
                                        status: vm.hiringRelationship.status,
                                    },
                                },
                            });
                        return 'ignore'; // default to ignore ones we don't understand
                    });
                    delete viewModelLists.ignore;

                    _.each(viewModelLists, (list, key) => {
                        // group list
                        const sortedList = _.groupBy(list, vm => {
                            // we only want new messages from connections that are ongoing
                            // conversations where both parties have sent at least one message
                            if (vm.hasUnread && !vm.newMatch && !vm.newRequest && !vm.newInstantMatch) {
                                return 'newMessages';
                            }
                            if (vm.newMatch && !vm.newRequest) {
                                return 'newMatches';
                            }
                            if (vm.newRequest || vm.newInstantMatch) {
                                return 'newRequests';
                            }
                            return 'remaining';
                        });

                        // concat groups in a certain order, also sorting them by activty inside each group
                        viewModelLists[key] = (_.sortBy(sortedList.newMessages, 'msSinceLastActivity') || [])
                            .concat(_.sortBy(sortedList.newMatches, 'msSinceLastActivity') || [])
                            .concat(_.sortBy(sortedList.newRequests, 'msSinceLastActivity') || [])
                            .concat(_.sortBy(sortedList.remaining, 'msSinceLastActivity') || []);
                    });

                    scope.noCandidates = !_.chain(viewModelLists).values().flatten().any().value();
                    scope.viewModelLists = viewModelLists;
                    updateFiltering();
                }
                scope.$watch('viewModelLists', () => {
                    if (!_.isEmpty(scope.viewModelLists) && !scope.closed) {
                        scope.tabs = [
                            {
                                name: 'connected',
                                class: 'candidates-connected',
                                translate: {
                                    hiringManager: 'careers.connections_list.connected',
                                    values: {
                                        num: scope.viewModelLists.connected ? scope.viewModelLists.connected.length : 0,
                                    },
                                },
                            },
                            {
                                name: 'saved_for_later',
                                class: 'candidates-saved',
                                translate: {
                                    hiringManager: 'careers.connections_list.saved',
                                    values: {
                                        num: scope.viewModelLists.saved_for_later
                                            ? scope.viewModelLists.saved_for_later.length
                                            : 0,
                                    },
                                },
                            },
                            {
                                name: 'liked',
                                class: 'candidates-pending',
                                translate: {
                                    hiringManager: 'careers.connections_list.pending',
                                    values: {
                                        num: scope.viewModelLists.liked ? scope.viewModelLists.liked.length : 0,
                                    },
                                },
                            },
                        ];
                    } else if (!_.isEmpty(scope.viewModelLists) && scope.closed) {
                        scope.tabs = [
                            {
                                name: 'closed',
                                class: 'candidates-closed',
                                translate: {
                                    hiringManager: 'careers.connections_list.closed',
                                    values: {
                                        num: scope.viewModelLists.closed ? scope.viewModelLists.closed.length : 0,
                                    },
                                },
                            },
                            {
                                name: 'closed_team',
                                class: 'candidates-closed_team',
                                translate: {
                                    hiringManager: 'careers.connections_list.closed_team',
                                    values: {
                                        num: scope.viewModelLists.closed_team
                                            ? scope.viewModelLists.closed_team.length
                                            : 0,
                                    },
                                },
                            },
                        ];
                    }
                });

                function ensureHiringRelationshipsLoaded() {
                    if (scope.role === 'candidate') {
                        // Find any section that is expanded and not
                        // yet loaded and load it
                        _.chain(scope.expandedSections)
                            .values()
                            .where({
                                expanded: true,
                                closed: scope.closed,
                                loaded: false,
                                loading: false,
                            })
                            .select(entry => _.contains(entry.roles, scope.role))
                            .each(entry => {
                                entry.loading = true;
                            })
                            .pluck('listName')
                            .uniq()
                            .each(listName => {
                                scope.careersNetworkViewModel.ensureHiringRelationshipsLoaded(listName).then(() => {
                                    // mark each section related to this list as loaded
                                    _.chain(scope.expandedSections)
                                        .where({
                                            listName,
                                        })
                                        .each(entry => {
                                            entry.loaded = true;
                                            entry.loading = false;
                                        });
                                    getViewModelLists();
                                });
                            });
                    } else if (scope.hasHiringRole) {
                        if (scope.closed) {
                            scope.careersNetworkViewModel.ensureHiringRelationshipsLoaded('closed').then(() => {
                                getViewModelLists();
                            });
                        } else {
                            scope.careersNetworkViewModel.ensureHiringRelationshipsLoaded('teamCandidates').then(() => {
                                getViewModelLists();
                            });
                        }
                    }
                }

                const connectionsTranslationHelper = new TranslationHelper('careers.connections');
                const initialOpenPositionOptions = [
                    {
                        id: 'all',
                        title: connectionsTranslationHelper.get('show_all_positions'),
                    },
                ];
                const initialTeamMemberOptions = [
                    {
                        id: 'all',
                        name: connectionsTranslationHelper.get('show_all_team_members'),
                    },
                ];

                function rebuildFilters() {
                    if (!scope.hasHiringRole || !scope.viewModelLists) {
                        return;
                    }
                    rebuildOpenPositionOptions();
                    rebuildTeamMemberOptions();
                }

                function rebuildOpenPositionOptions() {
                    // reset filter
                    scope.proxy.selectedOpenPositionId = 'all';

                    scope.openPositionOptions = initialOpenPositionOptions;

                    // get open position ids from current list of connections
                    const openPositionIdsInRelationships = _.chain(scope.viewModelLists[scope.proxy.currentTab])
                        .values()
                        .flatten()
                        .pluck('hiringRelationship')
                        .filter('open_position_id')
                        .indexBy('open_position_id')
                        .value();

                    // push those open positions to the selectize options
                    scope.careersNetworkViewModel.ensureOpenPositions().then(openPositions => {
                        const filteredOptions = _.chain(openPositions)
                            .select(openPosition => openPositionIdsInRelationships[openPosition.id])
                            .sortBy('title')
                            .value();
                        scope.openPositionOptions = initialOpenPositionOptions.concat(filteredOptions);
                    });
                }

                function rebuildTeamMemberOptions() {
                    // reset filter
                    scope.proxy.selectedTeamMemberId = 'all';

                    // get open position ids from current list of connections
                    const teamMemberOptions = _.chain(scope.viewModelLists)
                        .values()
                        .flatten()
                        .pluck('hiringRelationship')
                        .map(hr => {
                            if (hr.hiring_manager_id !== scope.careersNetworkViewModel.user.id) {
                                return {
                                    id: hr.hiring_manager_id,
                                    name: hr.hiring_manager_display_name,
                                };
                            }
                        })
                        .compact()
                        .uniq('id')
                        .sortBy('name')
                        .value();

                    if (_.any(teamMemberOptions)) {
                        scope.teamMemberOptions = initialTeamMemberOptions.concat(teamMemberOptions);
                    } else {
                        scope.teamMemberOptions = null;
                    }
                }

                scope.$watch('viewModelLists', rebuildFilters);
                scope.$watch('proxy.currentTab', () => {
                    rebuildFilters();
                    updateFiltering();
                });

                const expandedWatchKeys = _.map(
                    scope.expandedSections,
                    (entry, key) => `expandedSections.${key}.expanded`,
                );
                scope.$watchGroup(expandedWatchKeys, ensureHiringRelationshipsLoaded);
                scope.$watch('closed', ensureHiringRelationshipsLoaded);

                // Listen for hiringRelationship updates on the viewmodel - see hiring_relationship_view_model.js
                // NOTE: We intentionally call ensureHiringRelationshipsLoaded here. In a situation where a ping's
                //  payload contains changes to the hiring manager's hiring team's has_full_access AND hiringRelationship
                //  changes, careersNetworkViewModel will reset the cache and also broadcast a 'hiringRelationshipUpdate'
                //  event. At that point, we've blown away the hiringRelationshipViewModels in careersNetworkViewModel and
                //  won't preload them again for an arbitrary amount of time. If we were to call getViewModelLists() before
                //  loading relationships again, the UI would be blank until the next poll comes through 10 seconds later.
                const statusListener = scope.$on('hiringRelationshipUpdate', () => {
                    ensureHiringRelationshipsLoaded();
                });
                scope.$on('$destroy', statusListener);

                //------------------------
                // Filtering candidates
                //------------------------
                function matchesSearchText(vm) {
                    return !scope.search.text || vm.searchableText.includes(scope.search.text.toLowerCase());
                }

                function hasSelectedOpenPosition(vm) {
                    if (scope.proxy.selectedOpenPositionId === 'all') {
                        return true;
                    }
                    return (
                        vm.hasOpenPositionId &&
                        vm.hiringRelationship.open_position_id === scope.proxy.selectedOpenPositionId
                    );
                }

                function hasSelectedHiringManager(vm) {
                    if (!scope.proxy.selectedTeamMemberId || scope.proxy.selectedTeamMemberId === 'all') {
                        return true;
                    }
                    return (
                        vm.hiringRelationship.hiring_manager_id === scope.proxy.selectedTeamMemberId ||
                        (vm.teammateHiringManagerIds &&
                            vm.teammateHiringManagerIds.includes(scope.proxy.selectedTeamMemberId))
                    );
                }

                function updateFiltering() {
                    scope.filteredViewModels =
                        scope.viewModelLists && scope.viewModelLists[scope.proxy.currentTab]
                            ? _.chain(scope.viewModelLists[scope.proxy.currentTab])
                                  .filter(matchesSearchText)
                                  .filter(hasSelectedOpenPosition)
                                  .filter(hasSelectedHiringManager)
                                  .value()
                            : [];

                    (scope.tabs || []).forEach(tab => {
                        if (tab.name === scope.proxy.currentTab) {
                            tab.translate.values.num = scope.filteredViewModels.length;
                        } else {
                            tab.translate.values.num = scope.viewModelLists[tab.name]
                                ? scope.viewModelLists[tab.name].length
                                : 0;
                        }
                    });
                }

                scope.$watchGroup(
                    ['proxy.selectedOpenPositionId', 'search.text', 'proxy.selectedTeamMemberId'],
                    updateFiltering,
                );
            },
        };
    },
]);
