import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/candidate_list.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('candidateListDemo', [
    '$injector',

    function factory($injector) {
        const CareerProfileList = $injector.get('CareerProfileList');

        return {
            scope: {},
            restrict: 'E',
            templateUrl,

            link(scope) {
                const params = {
                    filters: {
                        name: 'hiring_index', // only grab hiring-index list
                        career_profiles_limit: 5, // Limit to 5 for brevity (display only - no interaction)
                    },
                };

                CareerProfileList.index(params).then(response => {
                    const list = _.first(response.result);
                    scope.ngModel = list ? list.career_profiles : [];
                    scope.limitedModels = scope.ngModel;
                });

                // Necessary for this template, this is a function from candidate_list_dir that checks
                // if item is a hiring relationship.
                scope.getCareerProfile = item => item;

                scope.disableActions = true;
            },
        };
    },
]);
