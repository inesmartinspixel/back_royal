import angularModule from 'Careers/angularModule/scripts/careers_module';

angularModule.factory('HiringApplication', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');
        const ProfileCompletionHelper = $injector.get('ProfileCompletionHelper');
        const HasLocation = $injector.get('HasLocation');

        const profileCompletionHelper = new ProfileCompletionHelper($injector.get('HIRING_MANAGER_FORM_STEPS'));

        return Iguana.subclass(function () {
            this.setCollection('hiring_applications');
            this.alias('HiringApplication');
            this.embedsMany('recent_career_profile_searches', 'CareerProfileSearch');
            this.include(HasLocation);

            this.VALID_STATUSES = ['pending', 'rejected', 'accepted'];

            this.setCallback('before', 'save', function () {
                this._beforeSave();
            });

            this.setCallback('after', 'copyAttrsOnInitialize', function () {
                this.recent_career_profile_searches = this.recent_career_profile_searches || [];
            });

            Object.defineProperty(this.prototype, 'statusOrderKey', {
                get() {
                    return (
                        {
                            accepted: 1,
                            pending: 2,
                            rejected: 3,
                        }[this.status] || 4
                    );
                },
                configurable: true, // for specs
            });

            Object.defineProperty(this.prototype, 'complete', {
                get() {
                    return this.percentComplete() === 100;
                },
                configurable: true, // for specs
            });

            return {
                percentComplete(stepName) {
                    return profileCompletionHelper.getPercentComplete(this, stepName);
                },

                getStepsProgressMap() {
                    return profileCompletionHelper.getStepsProgressMap(this);
                },

                getStepProgress(step) {
                    return profileCompletionHelper.getStepProgress(step, this);
                },

                _beforeSave() {
                    // set last_calculated_complete_percentage
                    this.last_calculated_complete_percentage = this.percentComplete();
                },
            };
        });
    },
]);
