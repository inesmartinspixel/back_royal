import angularModule from 'Careers/angularModule/scripts/careers_module';

angularModule.factory('HiringTeam', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');

        const HiringTeam = Iguana.subclass(function () {
            this.setCollection('hiring_teams');
            this.alias('HiringTeam');
            this.setIdProperty('id');
            this.embedsMany('subscriptions', 'Subscription');

            this.setCallback('after', 'copyAttrsOnInitialize', function () {
                this.hiring_manager_ids = this.hiring_manager_ids || [];
                this.subscriptions = this.subscriptions || [];
            });

            this.extend({
                HIRING_PLAN_LEGACY: 'legacy',
                HIRING_PLAN_PAY_PER_POST: 'pay_per_post',
                HIRING_PLAN_UNLIMITED_WITH_SOURCING: 'unlimited_w_sourcing',
                hiringPlans() {
                    return [
                        this.HIRING_PLAN_LEGACY,
                        this.HIRING_PLAN_PAY_PER_POST,
                        this.HIRING_PLAN_UNLIMITED_WITH_SOURCING,
                    ];
                },
            });

            Object.defineProperty(this.prototype, 'memberCount', {
                get() {
                    return this.hiring_manager_ids.length;
                },
            });

            Object.defineProperty(this.prototype, 'noHiringManagerAccessDueToPastDuePayment', {
                get() {
                    return !!this.primarySubscription && !this.has_full_access;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasPaymentTrouble', {
                get() {
                    return this.subscriptions && _.find(this.subscriptions, subscription => subscription.past_due);
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'activeStripePlan', {
                get() {
                    return this.primarySubscription && this.getPlanForId(this.primarySubscription.stripe_plan_id);
                },
            });

            Object.defineProperty(this.prototype, 'usingPayPerPostHiringPlan', {
                get() {
                    return this.hiring_plan === HiringTeam.HIRING_PLAN_PAY_PER_POST;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'usingUnlimitedWithSourcingHiringPlan', {
                get() {
                    return this.hiring_plan === HiringTeam.HIRING_PLAN_UNLIMITED_WITH_SOURCING;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'usingLegacyHiringPlan', {
                get() {
                    return this.hiring_plan === HiringTeam.HIRING_PLAN_LEGACY;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'primarySubscription', {
                get() {
                    // See comments on server-side primary_subscription definition
                    return this.usingPayPerPostHiringPlan ? null : this.subscriptions[0];
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'seemsToHavePaymentSource', {
                get() {
                    if (this.usingPayPerPostHiringPlan) {
                        // You have to make a payment to set hiring plan to pay per post.
                        // But, all the subscriptions could have run out by now, so we can't
                        // just check for a subscription like in other cases.
                        return true;
                    }
                    return _.any(this.subscriptions);
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'seemsToHaveStripeAccount', {
                get() {
                    return this.seemsToHavePaymentSource;
                },
                configurable: true,
            });

            // A hiring team should be considered disabled for non owners if there is no hiring_plan,
            // or the hiring_plan is `unlimited_w_sourcing` and `has_full_access` is false.
            Object.defineProperty(this.prototype, 'disabledForNonOwners', {
                get() {
                    return !this.hiring_plan || (this.usingUnlimitedWithSourcingHiringPlan && !this.has_full_access);
                },
                configurable: true,
            });

            return {
                getPlanForId(stripePlanId) {
                    return _.findWhere(this.stripe_plans, {
                        id: stripePlanId,
                    });
                },

                stripePlanForHiringPlan(hiringPlan) {
                    return _.detect(
                        this.stripe_plans,
                        stripePlan => stripePlan.product_metadata.available_for_hiring_plan === hiringPlan,
                    );
                },
            };
        });

        return HiringTeam;
    },
]);
