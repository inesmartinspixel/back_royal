import angularModule from 'Careers/angularModule/scripts/careers_module';
import defaultImageUrl from 'vectors/graduationcap.svg';

angularModule.factory('EducationExperience', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');
        const TranslationHelper = $injector.get('TranslationHelper');
        const educationExperienceDetailTranslationHelper = new TranslationHelper(
            'careers.edit_career_profile.education_experience_detail',
        );
        const enrollmentTranslationHelper = new TranslationHelper('lessons.stream.student_dashboard_enrollment');

        return Iguana.subclass(function () {
            this.alias('EducationExperience');
            this.setIdProperty('id');
            this.embedsMany('transcripts', 'S3TranscriptAsset');

            this.extend({
                // Alteration of this list needs to be reflected in `CareerProfile::TranscriptRequirementHelper::DEGREE_LEVELS`
                degreeOptions: [
                    'associate_of_applied_arts',
                    'associate_of_applied_science',
                    'associate_of_arts',
                    'associate_of_engineering',
                    'associate_of_political_science',
                    'associate_of_science',
                    'bachelor_of_architecture',
                    'bachelor_of_arts',
                    'bachelor_of_arts_2',
                    'bachelor_of_business_administration',
                    'bachelor_of_commerce',
                    'bachelor_of_engineering',
                    'bachelor_of_fine arts',
                    'bachelor_of_science',
                    'bachelor_of_science_intl',
                    'doctor_of_education',
                    'doctor_of_medicine',
                    'doctor_of_philosophy',
                    'doctor_of_philosophy_2',
                    'doctor_of_veterinary_medicine',
                    'higher_national_diploma',
                    'juris_doctor',
                    'master_of_advanced_studies',
                    'master_of_advanced_studies_2',
                    'master_of_arts',
                    'master_of_arts_2',
                    'master_of_business_administration',
                    'master_of_engineering',
                    'master_of_laws',
                    'master_of_philosophy',
                    'master_of_research',
                    'master_of_science',
                    'master_of_science_2',
                    'master_of_studies',
                    'master_of_technology',
                ],
            });
            this.degreeOptionsSet = {};
            _.each(this.degreeOptions, key => {
                this.degreeOptionsSet[key] = true;
            });

            this.setCallback('after', 'copyAttrsOnInitialize', function () {
                this.transcripts = this.transcripts || [];
                this.$$transcript_waived = !!this.transcript_waiver;
            });

            Object.defineProperty(this.prototype, 'imageUrl', {
                get() {
                    return this.educational_organization && this.educational_organization.image_url
                        ? this.educational_organization.image_url
                        : defaultImageUrl;
                },
            });

            Object.defineProperty(this.prototype, 'orgName', {
                get() {
                    return this.educational_organization && this.educational_organization.text;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'degreeAndMajorString', {
                get() {
                    return _.compact([this.degreeName, this.major]).join(', ');
                },
            });

            Object.defineProperty(this.prototype, 'degreeAndOrgNameString', {
                get() {
                    // Degree is required in the application form, but I do see some in the db
                    // without it, so being defensive here
                    return this.degreeName ? `${this.degreeName} - ${this.orgName}` : this.orgName;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'degreeAndOrgNameWithTranscriptRequirementString', {
                get() {
                    const key = this.official_transcript_required
                        ? 'degree_official_transcript_required'
                        : 'degree_official_transcript_not_required';
                    return enrollmentTranslationHelper.get(key, {
                        degreeAndOrgNameString: this.degreeAndOrgNameString,
                    });
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'degreeName', {
                get() {
                    return this.constructor.degreeOptionsSet[this.degree]
                        ? educationExperienceDetailTranslationHelper.get(this.degree)
                        : this.degree;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'degreeProgram', {
                get() {
                    return this.degree_program;
                },
                set(val) {
                    this.degree_program = val;
                },
            });

            Object.defineProperty(this.prototype, 'willNotComplete', {
                get() {
                    return this.will_not_complete;
                },
                set(val) {
                    this.will_not_complete = val;
                },
            });

            // this determines if the education experience is empty in the context of the education form.
            // if any of the form fields change, we should verify that this property still works as expected
            Object.defineProperty(this.prototype, 'isEmpty', {
                get() {
                    return (
                        (!this.educational_organization || !this.educational_organization.text) &&
                        !this.degree &&
                        !this.major &&
                        !this.minor &&
                        !this.gpa &&
                        angular.isUndefined(this.willNotComplete)
                    );
                },
            });

            Object.defineProperty(this.prototype, 'transcriptUploaded', {
                get() {
                    return this.transcripts.length > 0;
                },
                configurable: true,
            });

            // See also education_experience.rb
            Object.defineProperty(this.prototype, 'transcriptRequired', {
                get() {
                    return (
                        this.degreeProgram &&
                        !this.will_not_complete &&
                        this.graduation_year <= new Date().getFullYear()
                    );
                },
                configurable: true,
            });

            // See also education_experience.rb
            Object.defineProperty(this.prototype, 'transcriptUploadedOrWaived', {
                get() {
                    return !!(this.transcriptUploaded || this.$$transcript_waived);
                },
                configurable: true,
            });

            // See also education_experience.rb
            Object.defineProperty(this.prototype, 'transcriptApprovedOrWaived', {
                get() {
                    return this.transcript_approved || this.$$transcript_waived;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'transcriptInReview', {
                get() {
                    return !this.transcript_approved && this.transcriptUploaded;
                },
                configurable: true,
            });

            return {};
        });
    },
]);
