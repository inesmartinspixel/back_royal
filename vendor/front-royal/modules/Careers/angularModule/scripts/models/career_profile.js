import angularModule from 'Careers/angularModule/scripts/careers_module';

angularModule.factory('CareerProfile', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');
        const ProfileCompletionHelper = $injector.get('ProfileCompletionHelper');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
        const EducationExperience = $injector.get('EducationExperience');
        const WorkExperience = $injector.get('WorkExperience');
        const HasLocation = $injector.get('HasLocation');
        const $translate = $injector.get('$translate');
        const $rootScope = $injector.get('$rootScope');
        const ErrorLogService = $injector.get('ErrorLogService');
        const ClientStorage = $injector.get('ClientStorage');

        return Iguana.subclass(function () {
            this.setCollection('career_profiles');
            this.alias('CareerProfile');
            this.setIdProperty('id');
            this.embedsMany('education_experiences', 'EducationExperience');
            this.embedsMany('work_experiences', 'WorkExperience');

            // Since the filters for index calls can get really long,
            // we use a post to avoid having get urls that go beyond browser
            // limits.
            this.overrideAction('index', {
                method: 'POST',
                path: 'index',
            });
            this.include(HasLocation);

            this.setCallback('before', 'save', function () {
                this._beforeSave();
            });

            this.extend({
                TESTS: ['gmat', 'sat', 'act', 'gre_verbal', 'gre_quantitative', 'gre_analytical'],
                ACTIVE_INTEREST_LEVELS: _.reject(
                    $injector.get('NEW_COMPANY_INTEREST_LEVELS'),
                    interestLevel => interestLevel === 'not_interested',
                ),
            });

            this.defineSetter('program_type', function (val) {
                // We saw a user mysteriously switch back to MBA after
                // converting to EMBA. We struggled to reproduce the issue,
                // but if it happens again, we'd like to to know how/when.
                // See also: https://trello.com/c/hAsMiFFJ
                // Update: The logic here is not totally correct.  If the user has been
                // rejected, then they should now be allowed to change program types
                // again.  (I didn't bother to update this logic because it has only happened
                // once and it is really just affecting logging to help engineers.  It might make
                // sense in the future to update this logic though.)  See https://trello.com/c/RUUexX7Q
                if (
                    val !== this.program_type &&
                    this.program_type === 'emba' &&
                    ClientStorage.getItem('converted_to_emba')
                ) {
                    ErrorLogService.notifyInProd(
                        'Unexpected program_type change after converted_to_emba on career_profile',
                        undefined,
                        {
                            level: 'warn',
                            career_profile_id: this.id,
                        },
                    );
                }

                this.writeKey('program_type', val);
            });

            // During the Quantic transition we are only allowing
            // Quantic degree programs to be (re)applied to
            Object.defineProperty(this.prototype, 'programTypeIfAllowed', {
                get() {
                    const currentUser = $rootScope.currentUser;

                    // This should only trigger in the admin, in which
                    // case our checks below won't be applicable
                    if (currentUser.id !== this.user_id) {
                        return this.program_type;
                    }

                    if (!['mba', 'emba'].includes(this.program_type)) {
                        return null;
                    }

                    return this.program_type;
                },
            });

            Object.defineProperty(this.prototype, 'complete', {
                get() {
                    return this.percentComplete() === 100;
                },
                configurable: true, // for specs
            });

            // Gets the initials of the user (e.g. 'John Doe' => 'J. D.')
            Object.defineProperty(this.prototype, 'userInitials', {
                get() {
                    const namesArray = this.name.split(' ');
                    return _.map(namesArray, name => `${name[0]}. `)
                        .join('')
                        .trim();
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'longLabel', {
                get() {
                    const candidateEmail = this.email ? ` [${this.email}]` : ' ';
                    const cohortName = this.candidate_cohort_name === null ? '' : ` (${this.candidate_cohort_name})`;
                    // Example: Johnny Appleseed [johnny.appleseed@foobar.com] (MBA1)
                    return this.name + candidateEmail + cohortName;
                },
                configurable: true, // for specs
            });

            Object.defineProperty(this.prototype, 'hasAtLeastOneTestScore', {
                get() {
                    const self = this;
                    return !!_.detect(this.TESTS, test => self.hasScore(test));
                },
            });

            Object.defineProperty(this.prototype, 'hasGraduationDateForAccepedCohort', {
                get() {
                    return !!this.candidate_cohort_name && _.contains(['mba', 'emba'], this.program_type);
                },
            });

            Object.defineProperty(this.prototype, 'labelForAcceptedCohortInEducation', {
                get() {
                    if (this.candidate_cohort_name) {
                        return {
                            mba: 'MBA',
                            emba: 'Executive MBA',
                            the_business_certificate: 'Fundamentals of Business Certificate',
                            marketing_basics_certificate: 'Marketing Basics Certificate',
                            accounting_basics_certificate: 'Accounting Basics Certificate',
                            finance_basics_certificate: 'Finance Basics Certificate',
                            microeconomics_basics_certificate: 'Microeconomics Basics Certificate',
                            statistics_basics_certificate: 'Statistics Basics Certificate',
                        }[this.program_type];
                    }
                    return undefined;
                },
            });

            Object.defineProperty(this.prototype, 'hasAcceptedCohortShownInEducation', {
                get() {
                    return !!this.labelForAcceptedCohortInEducation;
                },
            });

            Object.defineProperty(this.prototype, 'hasMBAOrEMBACohort', {
                get() {
                    return !!this.candidate_cohort_name && _.contains(['mba', 'emba'], this.program_type);
                },
            });

            Object.defineProperty(this.prototype, 'isMBACohort', {
                get() {
                    return !!this.candidate_cohort_name && this.program_type === 'mba';
                },
            });

            Object.defineProperty(this.prototype, 'isEMBACohort', {
                get() {
                    return !!this.candidate_cohort_name && this.program_type === 'emba';
                },
            });

            Object.defineProperty(this.prototype, 'TESTS', {
                get() {
                    return this.constructor.TESTS;
                },
            });

            Object.defineProperty(this.prototype, 'workExperiences', {
                get() {
                    // not an empty one
                    return _.filter(this.work_experiences, w => !!w.orgName);
                },
            });

            Object.defineProperty(this.prototype, 'educationExperiences', {
                get() {
                    // not an empty one
                    return _.filter(this.education_experiences, e => !!e.orgName);
                },
            });

            Object.defineProperty(this.prototype, 'formalEducationExperiences', {
                get() {
                    return _.filter(this.education_experiences, e => e.degreeProgram);
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasLoneEmptyFormalEducationExperience', {
                get() {
                    // if any of the form fields change in education_experience_detail, we should verify that the `isEmpty` property still works as expected
                    return this.formalEducationExperiences.length === 1 && this.formalEducationExperiences[0].isEmpty;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasUploadedTranscripts', {
                get() {
                    return (
                        this.educationExperiencesIndicatingTranscriptRequired.filter(e => e.transcriptUploaded).length >
                        0
                    );
                },
                configurable: true,
            });

            // See also career_profile.rb
            Object.defineProperty(this.prototype, 'missingTranscripts', {
                get() {
                    return (
                        this.educationExperiencesIndicatingTranscriptRequired.filter(e => !e.transcriptUploadedOrWaived)
                            .length > 0
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'missingTranscriptApprovals', {
                get() {
                    return (
                        this.educationExperiencesIndicatingTranscriptRequired.filter(e => !e.transcript_approved)
                            .length > 0
                    );
                },
                configurable: true,
            });

            // See also career_profile.rb
            Object.defineProperty(this.prototype, 'educationExperiencesIndicatingTranscriptRequired', {
                get() {
                    const educationExperiences = this.education_experiences || [];
                    return educationExperiences.filter(e => e.transcriptRequired);
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'numRequiredTranscripts', {
                get() {
                    return this.educationExperiencesIndicatingTranscriptRequired.length;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'numRequiredTranscriptsUploaded', {
                get() {
                    return this.educationExperiencesIndicatingTranscriptRequired.filter(e => e.transcriptUploaded)
                        .length;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'numRequiredTranscriptsApproved', {
                get() {
                    return this.educationExperiencesIndicatingTranscriptRequired.filter(e => !!e.transcript_approved)
                        .length;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'numRequiredTranscriptsWaived', {
                get() {
                    return this.educationExperiencesIndicatingTranscriptRequired.filter(e => !!e.transcript_waiver)
                        .length;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'numRequiredTranscriptsNotWaived', {
                get() {
                    return this.educationExperiencesIndicatingTranscriptRequired.filter(e => !e.transcript_waiver)
                        .length;
                },
                configurable: true,
            });

            // See also career_profile.rb
            Object.defineProperty(this.prototype, 'indicatesUserShouldProvideTranscripts', {
                get() {
                    return this.educationExperiencesIndicatingTranscriptRequired.length > 0;
                },
                configurable: true,
            });

            // A user's career profile should indicate that the user needs to upload
            // english language proficiency documents if the user is not a native english
            // speaker, has not earned an accredited degree in english, and does not
            // have sufficient english work experience.
            //
            // Duplicated in career_profile.rb (indicates_user_should_upload_english_language_proficiency_documents?)
            Object.defineProperty(this.prototype, 'indicatesUserShouldUploadEnglishLanguageProficiencyDocuments', {
                get() {
                    // The native_english_speaker and earned_accredited_degree_in_english columns
                    // are nullable in the database, so we need to explicitly check if these values
                    // are set to false. Likewise, the sufficient_english_work_experience column is
                    // nullable, so we need to explicitly check if this value is not true in order
                    // to treat NULL the same as false.
                    return (
                        this.native_english_speaker === false &&
                        this.earned_accredited_degree_in_english === false &&
                        this.sufficient_english_work_experience !== true
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'currentFullTimeWorkExperiences', {
                get() {
                    return _.where(this.fullTimeWorkExperiences, {
                        end_date: null,
                    });
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'featuredWorkExperience', {
                get() {
                    return _.findWhere(this.work_experiences, {
                        featured: true,
                    });
                },
                set(workExperience) {
                    _.each(this.work_experiences, workExp => {
                        if (workExp.featured) {
                            workExp.featured = false;
                        }
                    });
                    if (workExperience) {
                        workExperience.featured = true;
                    }
                },
            });

            Object.defineProperty(this.prototype, 'fullTimeWorkExperiences', {
                get() {
                    return _.where(this.workExperiences, {
                        employment_type: 'full_time',
                    }).sort((w1, w2) => {
                        // We want the featured one at the top of the list
                        if (w1.featured && !w2.featured) {
                            return -1;
                        }
                        if (!w1.featured && w2.featured) {
                            return 1;
                        }

                        // I guess we should move the most recently started ones
                        // above less-recently started ones
                        if (w1.start_date > w2.start_date) {
                            return -1;
                        }
                        if (w1.start_date < w2.start_date) {
                            return 1;
                        }
                        return 0;
                    });
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'partTimeWorkExperiences', {
                get() {
                    return _.where(this.workExperiences, {
                        employment_type: 'part_time',
                    });
                },
            });

            Object.defineProperty(this.prototype, '_showPhotosAndNames', {
                get() {
                    return !$rootScope.currentUser || $rootScope.currentUser.pref_show_photos_names;
                },
            });

            Object.defineProperty(this.prototype, 'sanitizedAvatarSrc', {
                get() {
                    return this._showPhotosAndNames ? this.avatar_url : null;
                },
            });

            Object.defineProperty(this.prototype, 'sanitizedName', {
                get() {
                    return this._showPhotosAndNames ? this.name : this.userInitials;
                },
            });

            Object.defineProperty(this.prototype, 'mostRecentPositionText', {
                get() {
                    // grab the featured work experience
                    const workExperience = this.featuredWorkExperience;

                    if (workExperience && workExperience.orgName) {
                        const position = `${workExperience.job_title} @ ${workExperience.orgName}`;

                        // if there's an end-date, it means the featured work experience is in the past
                        if (workExperience.end_date) {
                            return `${$translate.instant('careers.candidate_list_card.formerly')} ${position}`;
                        }
                        return position;
                    }
                    return '';
                },
            });

            Object.defineProperty(this.prototype, 'mostRecentEducationText', {
                get() {
                    // filter out non-degree programs and grab the most recent education experience
                    const educationExperience = _.first(
                        _.filter(this.educationExperiences, e => e.degreeProgram && e.orgName),
                    );

                    return educationExperience
                        ? `${educationExperience.degreeName}, ${educationExperience.orgName}`
                        : '';
                },
            });

            Object.defineProperty(this.prototype, 'openToAllLocations', {
                get() {
                    return (
                        this.locations_of_interest &&
                        this.locations_of_interest.length > 0 &&
                        this.locations_of_interest[0] === 'flexible'
                    );
                },
            });

            Object.defineProperty(this.prototype, 'openToSomeAdditionalLocations', {
                get() {
                    return (
                        this.locations_of_interest &&
                        this.locationsWithoutCurrentLocation.length > 0 &&
                        this.locations_of_interest[0] !== 'none' &&
                        this.locations_of_interest[0] !== 'flexible'
                    );
                },
            });

            Object.defineProperty(this.prototype, 'openToNoAdditionalLocations', {
                get() {
                    return (
                        this.locations_of_interest &&
                        this.locations_of_interest.length > 0 &&
                        this.locations_of_interest[0] === 'none'
                    );
                },
            });

            Object.defineProperty(this.prototype, 'locationsWithoutCurrentLocation', {
                get() {
                    const that = this;
                    return _.chain(this.locations_of_interest || [])
                        .map(location => $translate.instant(`careers.field_options.${location}`))
                        .filter(location => location !== that.locationString)
                        .value();
                },
            });

            Object.defineProperty(this.prototype, 'resumeUrl', {
                get() {
                    if (
                        this.resume &&
                        this.resume.formats &&
                        this.resume.formats.original &&
                        this.resume.formats.original.url
                    ) {
                        return this.resume.formats.original.url;
                    }
                    return false;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'githubUrl', {
                get() {
                    return this.github_profile_url || false;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'deepLinkUrl', {
                get() {
                    // NOTE: the path and query params of this URL should correspond
                    // with the CareerProfile#deep_link_url method on the server
                    const $window = $injector.get('$window');
                    return `${$window.ENDPOINT_ROOT}/hiring/browse-candidates?tab=featured&id=${this.id}`;
                },
            });

            Object.defineProperty(this.prototype, 'salaryRangeForEventPayload', {
                get() {
                    // This mapping comes from FrontRoyal.Careers module and careerPreferencesForm directive
                    const salaryRangeMapping = {
                        prefer_not_to_disclose: 0,
                        less_than_40000: 0,
                        '40000_to_49999': 40000,
                        '50000_to_59999': 50000,
                        '60000_to_69999': 60000,
                        '70000_to_79999': 70000,
                        '80000_to_89999': 80000,
                        '90000_to_99999': 90000,
                        '100000_to_119999': 100000,
                        '120000_to_149999': 120000,
                        '150000_to_199999': 150000,
                        over_200000: 200000,
                    };

                    return salaryRangeMapping[this.salary_range] || 0;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'currentSalaryRange', {
                get() {
                    if (this.salary_range && this.salary_range !== 'prefer_not_to_disclose') {
                        return {
                            lower: parseInt(this.salary_range.split('_').shift(), 10) || null,
                            upper: parseInt(this.salary_range.split('_').pop(), 10) || null,
                        };
                    }
                    return null;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'appliedAt', {
                get() {
                    if (!this.$$appliedAt) {
                        this.$$appliedAt = new Date(this.applied_at * 1000);
                    }
                    return this.$$appliedAt;
                },
            });

            const definePropertyForTestScore = (object, propertyName, serverProperty) => {
                Object.defineProperty(object, propertyName, {
                    get() {
                        const parsedScore = parseFloat(this[serverProperty]);

                        if (Number.isNaN(parsedScore)) {
                            this[serverProperty] = null;
                        } else {
                            this[serverProperty] = parsedScore;
                        }

                        return this[serverProperty];
                    },
                    set(val) {
                        this[serverProperty] = val && val.toString();
                    },
                });
            };

            // the column types for these values are text, but we changed the input field
            // to a number type, so we have to return a number instead of a string for
            // use in education_form_dir
            definePropertyForTestScore(this.prototype, 'scoreOnGMAT', 'score_on_gmat');
            definePropertyForTestScore(this.prototype, 'scoreOnSAT', 'score_on_sat');
            definePropertyForTestScore(this.prototype, 'scoreOnGreVerbal', 'score_on_gre_verbal');
            definePropertyForTestScore(this.prototype, 'scoreOnGreQuantitative', 'score_on_gre_quantitative');
            definePropertyForTestScore(this.prototype, 'scoreOnGreAnalytical', 'score_on_gre_analytical');
            definePropertyForTestScore(this.prototype, 'scoreOnACT', 'score_on_act');

            return {
                percentComplete(stepName) {
                    // regenerate the helpers, since the steps might need to change if the program_type has changed
                    const profileCompletionHelper = new ProfileCompletionHelper(
                        EditCareerProfileHelper.getStepsForCareerProfile(this),
                    );
                    return profileCompletionHelper.getPercentComplete(this, stepName);
                },

                interestedInEmploymentType(type) {
                    return _.contains(this.employment_types_of_interest, type);
                },

                hasScore(test) {
                    const val = this.getScore(test);
                    return val !== null && val !== undefined;
                },

                getScore(test) {
                    const val = this[`score_on_${test}`];
                    return val;
                },

                // Removes all peer recommendations that don't have an email.
                sanitizePeerRecommendations() {
                    this.peer_recommendations = _.filter(
                        this.peer_recommendations,
                        peerRecommendation => peerRecommendation.email,
                    );
                },

                addDummyEducationExperienceIfNecessary() {
                    // If the career profile has no formal education experiences, add a dummy education experience.
                    // NOTE: used to populate the educationExperienceDetail directive with a blank education experience on the educationForm.
                    if (!_.any(this.formalEducationExperiences)) {
                        this.education_experiences.push(
                            EducationExperience.new({
                                degreeProgram: true,
                            }),
                        ); // dummy education experience
                    }
                },

                addDummyWorkExperienceIfNecessary() {
                    // If the career profile has no work experiences, create a dummy work experience.
                    // NOTE: used to populate the workExperienceDetail directive with a blank work experience on the workForm.
                    if (!_.any(this.work_experiences)) {
                        this.work_experiences = [
                            WorkExperience.new({
                                employment_type: 'full_time',
                                featured: false,
                            }),
                        ]; // dummy work experience
                    }
                },

                asJson($super) {
                    const json = $super();

                    // filter out empty work and education experiences that were added as dummies
                    const hasFields = requiredKeys => element =>
                        _.chain(element).keys().intersection(requiredKeys).value().length === requiredKeys.length;

                    json.education_experiences = _.filter(
                        json.education_experiences,
                        hasFields(['educational_organization', 'graduation_year', 'major']),
                    );
                    json.work_experiences = _.filter(
                        json.work_experiences,
                        hasFields(['professional_organization', 'job_title', 'start_date']),
                    );

                    return json;
                },

                _beforeSave() {
                    const percentComplete = this.percentComplete();
                    this.last_calculated_complete_percentage = percentComplete;

                    // Ensure all peer recommendations that don't have an email are
                    // removed since the server expects only valid peer recommendations.
                    this.sanitizePeerRecommendations();
                },

                hasSharedStudentNetworkInterests(careerProfile) {
                    function mapInterests(interests) {
                        return interests && _.map(interests, interest => interest.text.toLowerCase());
                    }

                    const thisInterests = mapInterests(this.student_network_interests);
                    const profileInterests = mapInterests(careerProfile.student_network_interests);

                    return _.intersection(thisInterests, profileInterests).length > 0;
                },

                hasSameCompany(careerProfile) {
                    return (
                        (this.featuredWorkExperience && this.featuredWorkExperience.professional_organization.id) ===
                        (careerProfile.featuredWorkExperience &&
                            careerProfile.featuredWorkExperience.professional_organization.id)
                    );
                },

                hasSameRole(careerProfile) {
                    return (
                        (this.featuredWorkExperience && this.featuredWorkExperience.role) ===
                        (careerProfile.featuredWorkExperience && careerProfile.featuredWorkExperience.role)
                    );
                },

                hasSharedSchools(careerProfile) {
                    function mapSchools(educationExperiences) {
                        return (
                            educationExperiences &&
                            _.map(
                                educationExperiences,
                                experience =>
                                    experience.educational_organization && experience.educational_organization.id,
                            )
                        );
                    }

                    const thisSchools = mapSchools(this.education_experiences);
                    const profileSchools = mapSchools(careerProfile.education_experiences);

                    return _.intersection(thisSchools, profileSchools).length > 0;
                },
            };
        });
    },
]);
