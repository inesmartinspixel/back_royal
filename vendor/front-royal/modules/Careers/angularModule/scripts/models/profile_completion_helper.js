import angularModule from 'Careers/angularModule/scripts/careers_module';
import casperMode from 'casperMode';

angularModule.factory('ProfileCompletionHelper', [
    '$injector',
    $injector => {
        const SuperModel = $injector.get('SuperModel');
        const ClientStorage = $injector.get('ClientStorage');

        return SuperModel.subclass(() => ({
            /**
             * Merges all of the requiredFields values from the elements in the formStepsAndRequiredFields
             * parameter into a single object for use in other helper methods
             * @param formStepsAndRequiredFields - an array of objects with properties called stepName (string) and requiredFields (object)
             *      that represent the profile steps and their respective required fields
             */
            initialize(formStepsAndRequiredFields) {
                this.setSteps(formStepsAndRequiredFields);
            },

            // internal helper that checks the completeness of all required fields on a model
            _getComponentPercentComplete(model, requiredFields) {
                const keys = _.keys(requiredFields);
                const self = this;
                if (keys.length > 0) {
                    const completed = _.filter(keys, fieldKey => self._fieldComplete(model, requiredFields, fieldKey))
                        .length;

                    return Math.round((completed / keys.length) * 100);
                }
                // component has no required fields, so just return 100 signifying completeness
                return 100;
            },

            _fieldComplete(model, requiredFields, fieldKey) {
                const val = model[fieldKey];
                const field = requiredFields[fieldKey];
                const self = this;

                if (typeof field === 'object') {
                    // recurse to check the completeness of each of the fields within this object
                    const completed = _.filter(
                        val,
                        v => self._getComponentPercentComplete(v, field.requiredFields) === 100,
                    );
                    return _.size(completed) >= field.min;
                }
                if (Array.isArray(val) && val.length > 0) {
                    return val.length >= field.min;
                }
                if (typeof val === 'boolean') {
                    return true;
                }
                return !!val;
            },

            /**
             * Calculates percent complete for entire profile or a single profile section
             * @param model - a model of the current profile
             * @param stepName (optional) - the name of the profile step to calculate the
             *      percent complete (if undefined, the profile % complete will be calculated)
             * @param omissions (optional) - an array of keys to exclude from the calculation
             * @return the percent complete
             */
            getPercentComplete(model, stepName, omissions) {
                let requiredFields;

                if (stepName) {
                    const step = _.findWhere(this.formStepsAndRequiredFields, {
                        stepName,
                    });
                    requiredFields = step.requiredFields;
                } else {
                    requiredFields = _.omit(this.requiredModelFields, omissions);
                }

                // find all simple mandatory fields
                return this._getComponentPercentComplete(model, requiredFields);
            },

            /**
             * Determines if each profile section is complete
             * NOTE: expects each step to be an object with properties called stepName and requiredFields
             * @param model - a model of the current profile
             * @return an object that maps the step names to their necessary classes
             */
            getStepsProgressMap(model) {
                const self = this;
                const progressStateStepsMap = {};
                _.each(self.formStepsAndRequiredFields, step => {
                    progressStateStepsMap[step.stepName] = self.getStepProgress(step, model);
                });
                return progressStateStepsMap;
            },

            // returns a string indicating current progress of the step
            getStepProgress(step, model) {
                // We mark the submit_application step as incomplete if they haven't submitted
                // their application yet to create the appearance that submitting their application
                // is a required action when completing their application.
                if (step.stepName === 'submit_application') {
                    if (ClientStorage.getItem('reapplyingOrEditingApplication') === 'true') {
                        return 'complete';
                    }
                    return 'incomplete';
                }

                if (_.isEmpty(step.requiredFields)) {
                    if (ClientStorage.getItem(`saved_${step.stepName}`)) {
                        return 'complete';
                    }
                    return 'none';
                }
                if (this._getComponentPercentComplete(model, step.requiredFields) === 100) {
                    // a special case where a required field could be prefilled by us or if they've
                    // completed the step and have cleared their localStorage (e.g. in Network,
                    // pref_student_network_privacy is prefilled by us). Also, we don't save the pages
                    // in casperMode, so we can't rely on localStorage for correct values in this case.
                    // So if this step is complete, we show a checkmark as the default, which is a more
                    // accurate UI
                    if (!casperMode() && step.requiresVisit && !ClientStorage.getItem(`saved_${step.stepName}`)) {
                        return 'none';
                    }
                    return 'complete';
                }
                return 'incomplete';
            },

            setSteps(formStepsAndRequiredFields) {
                this.formStepsAndRequiredFields = formStepsAndRequiredFields;
                const requiredFieldsArray = _.pluck(formStepsAndRequiredFields, 'requiredFields');
                const requiredModelFields = {};
                // merge each step's required fields together into a single object
                _.each(requiredFieldsArray, requiredFields => {
                    _.extend(requiredModelFields, requiredFields);
                });
                this.requiredModelFields = requiredModelFields;
            },
        }));
    },
]);
