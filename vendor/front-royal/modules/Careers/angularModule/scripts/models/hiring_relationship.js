import angularModule from 'Careers/angularModule/scripts/careers_module';

angularModule.factory('HiringRelationship', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');
        const ErrorLogService = $injector.get('ErrorLogService');
        const UnloadedChangeDetector = $injector.get('UnloadedChangeDetector');

        return Iguana.subclass(function () {
            this.setCollection('hiring_relationships');
            this.alias('HiringRelationship');
            this.embedsOne('career_profile', 'CareerProfile');
            this.embedsOne('hiring_application', 'HiringApplication');
            this.embedsOne('conversation', 'Mailbox.Conversation');

            this.setCallback('after', 'copyAttrsOnInitialize', function () {
                this.hiring_manager_closed_info = this.hiring_manager_closed_info || {};
            });

            this.setCallback('before', 'save', function () {
                this._beforeSave();
            });

            this.UnloadedChangeDetector = UnloadedChangeDetector.createDetectorKlass('hiring_relationship');

            Object.defineProperty(this.prototype, 'status', {
                get() {
                    // sometimes we initialize a new hiring relationship without a status
                    if (!this.hiring_manager_status || !this.candidate_status) {
                        return null;
                    }

                    if (this.hiring_manager_closed) {
                        return 'closed_by_hiring_manager';
                    }
                    if (this.candidate_closed) {
                        return 'closed_by_candidate';
                    }

                    const statusMap = {
                        hidden: 'hidden_from_hiring_manager',
                        pending: 'waiting_on_hiring_manager',
                        rejected: 'rejected_by_hiring_manager',
                        saved_for_later: 'saved_for_later',
                        accepted: {
                            pending: 'waiting_on_candidate',
                            accepted: 'matched',
                            rejected: 'rejected_by_candidate',
                            hired: 'hired',
                            hidden: 'hidden_from_candidate',
                        },
                        hired: 'hired',
                    };

                    const step1 = statusMap[this.hiring_manager_status];
                    let status;

                    if (step1 && typeof step1 === 'string') {
                        return step1;
                    }

                    if (step1) {
                        status = step1[this.candidate_status];
                    }

                    if (status) {
                        return status;
                    }
                    ErrorLogService.notify('invalid hiring_relationship status', undefined, {
                        hiring_relationship_id: this.id,
                        hiring_manager_status: this.hiring_manager_status,
                        candidate_status: this.candidate_status,
                    });
                    return 'invalid';
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'closed', {
                get() {
                    return !!(this.hiring_manager_closed || this.candidate_closed);
                },
            });

            // Returns the hiring relationship status in human readable form.
            // NOTE: This is intended to be used as a suffix by adding it to the end
            // of a user's name, which is why it has prepended whitespace and the text
            // is in parentheses.
            // FIXME: localize these if they're ever used outside of the admin interface.
            Object.defineProperty(this.prototype, 'humanReadableStatus', {
                get() {
                    if (!this.status) {
                        return '';
                    }

                    return (
                        {
                            hired: ' (Hired)',
                            matched: ' (Matched)',
                            saved_for_later: ' (Saved for Later)',
                            rejected_by_candidate: ' (Rejected by Candidate)',
                            rejected_by_hiring_manager: ' (Rejected by Hiring Manager)',
                            waiting_on_candidate: ' (Waiting on Candidate)',
                            waiting_on_hiring_manager: ' (Waiting on Hiring Manager)',
                            hidden_from_hiring_manager: ' (Hidden from Hiring Manager)',
                            hidden_from_candidate: ' (Hidden from Candidate)',
                            closed_by_hiring_manager: ' (Closed by Hiring Manager)',
                            closed_by_candidate: ' (Closed by Candidate)',
                        }[this.status] || ''
                    );
                },
            });

            // Gets the hiring relationship status from the humanReadableStatus getter
            // and removes any prepended whitespace and returns the text between the parentheses.
            Object.defineProperty(this.prototype, 'humanReadableStatusTrimmed', {
                get() {
                    // retrieve the relationship status and remove any prepended whitespace
                    const status = this.humanReadableStatus.trim();
                    const matches = status.match(/([^()]+)/); // get the text between the parentheses
                    if (matches) {
                        return matches[0];
                    }
                    return status;
                },
            });

            Object.defineProperty(this.prototype, 'isVisibleToCandidate', {
                get() {
                    return this.candidate_status !== 'hidden';
                },
            });

            Object.defineProperty(this.prototype, 'matched', {
                get() {
                    return this.candidate_status === 'accepted' && this.hiring_manager_status === 'accepted';
                },
            });

            Object.defineProperty(this.prototype, 'pending', {
                get() {
                    return this.candidate_status === 'pending' || this.hiring_manager_status === 'pending';
                },
            });

            Object.defineProperty(this.prototype, 'candidateCohortName', {
                get() {
                    return this.career_profile.candidate_cohort_name;
                },
            });

            Object.defineProperty(this.prototype, 'candidateEmail', {
                get() {
                    return this.career_profile.email;
                },
            });

            Object.defineProperty(this.prototype, 'longLabel', {
                get() {
                    return this.career_profile.longLabel + this.humanReadableStatus;
                },
            });

            Object.defineProperty(this.prototype, 'hiringTeamId', {
                get() {
                    return this.hiring_application && this.hiring_application.hiring_team_id;
                },
            });

            this.extend({
                statusOptions: [
                    {
                        text: 'Pending',
                        value: 'pending',
                    },
                    {
                        text: 'Accepted',
                        value: 'accepted',
                    },
                    {
                        text: 'Rejected',
                        value: 'rejected',
                    },
                ],
            });

            return {
                markAsHired(user) {
                    if (user.id === this.hiring_manager_id) {
                        this.hiring_manager_status = 'hired';
                    } else if (user.id === this.candidate_id) {
                        this.candidate_status = 'hired';
                    } else {
                        throw new Error('User is not a party to this hiring relationship');
                    }
                },

                _beforeSave() {
                    // there are places in the UI that rely on this, and
                    // that we want to update immediately before a save request
                    // returns.  See, for example, /careers/connections
                    this.updated_at = Date.now() / 1000;
                },
            };
        });
    },
]);
