import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/connection_bar_button.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('connectionBarButton', [
    '$injector',

    function factory($injector) {
        $injector = $injector;
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');

        return {
            scope: {
                hiringRelationshipViewModel: '<',
                viewProfile: '&',
            },
            restrict: 'E',
            templateUrl,

            link(scope) {
                scope.newKey = () => {
                    let key;

                    if (scope.hiringRelationshipViewModel.newRequest) {
                        key = 'new_request';
                    } else if (scope.hiringRelationshipViewModel.newInstantMatch) {
                        key = 'new_match';
                    } else if (scope.hiringRelationshipViewModel.hasUnread) {
                        key = 'new_message';
                    }

                    if (key) {
                        return `careers.connection_bar_button.${key}`;
                    }
                    return false;
                };

                scope.statusKey = status => {
                    let key;

                    if (status === 'accepted' && scope.hiringRelationshipViewModel.hasOpenPositionId) {
                        key = 'invited_to_connect';
                    } else if (status === 'accepted') {
                        key = 'liked';
                    }

                    if (key) {
                        return `careers.connection_bar_button.${key}`;
                    }
                    return false;
                };

                scope.openResume = url => {
                    NavigationHelperMixin.loadUrl(url, '_blank');
                };

                // We want to be able to bind-once all of the properties in the view,
                // but if the hiring relationship gets updated, we need to be able
                // to re-bind all of the stuff inside.  So we create this proxy
                // object the gets re-built each time lastActivityAt changes
                const emptyArray = [];
                scope.$watch('hiringRelationshipViewModel.lastActivityAt', () => {
                    if (scope.hiringRelationshipViewModel) {
                        scope.proxies = [
                            {
                                viewModel: scope.hiringRelationshipViewModel,
                            },
                        ];
                    } else {
                        scope.proxies = emptyArray;
                    }
                });
            },
        };
    },
]);
