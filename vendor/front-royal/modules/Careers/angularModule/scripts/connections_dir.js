import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/connections.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('connections', [
    '$injector',

    function factory($injector) {
        const $location = $injector.get('$location');
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
        const $rootScope = $injector.get('$rootScope');
        const $route = $injector.get('$route');
        const scopeTimeout = $injector.get('$timeout');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');

        return {
            scope: {
                role: '<',
            },
            restrict: 'E',
            templateUrl,

            link(scope) {
                NavigationHelperMixin.onLink(scope);

                scope.tabs = [
                    {
                        name: 'activeConnections',
                        class: 'active-connections',
                        translate: {
                            hiringManager: 'careers.connections.my_candidates',
                            candidate: 'careers.connections.student_connections',
                        },
                    },
                    {
                        role: 'hiringManager',
                        name: 'teamConnections',
                        class: 'team-connections',
                        translate: {
                            hiringManager: 'careers.connections.team_candidates',
                        },
                    },
                    {
                        name: 'closedConnections',
                        class: 'closed-connections',
                        translate: {
                            hiringManager: 'careers.connections.archive',
                            candidate: 'careers.connections.archive',
                        },
                    },
                ];

                scope.$location = $location;
                scope.careersNetworkViewModel = CareersNetworkViewModel.get(scope.role);

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'currentTab', {
                    get() {
                        let list = $location.search().list;
                        if (!list || !_.contains(['activeConnections', 'closedConnections', 'teamConnections'], list)) {
                            $location.search('list', 'activeConnections');
                            list = 'activeConnections';
                        }
                        return list;
                    },
                    set(val) {
                        if (!_.contains(['closedConnections', 'teamConnections'], val)) {
                            val = 'activeConnections';
                        }
                        $location.search('list', val);
                        return val;
                    },
                });

                Object.defineProperty(scope, 'currentRouteDirective', {
                    get() {
                        return $route && $route.current && $route.current.$$route && $route.current.$$route.directive;
                    },
                    configurable: true, // for tests
                });

                // Don't animate back animation for this directive
                AppHeaderViewModel.animateBackTransition = false;
                scope.$on('$destroy', () => {
                    AppHeaderViewModel.animateBackTransition = false;

                    // Wait a beat to remove this param as it will trigger a change to currentTab which causes
                    // some jank
                    scopeTimeout(() => {
                        $location.search('list', undefined);
                    }, 0);
                });

                scope.$watch('$location.search().connectionId', connectionId => {
                    scope.connectionId = connectionId;
                    if (connectionId) {
                        // It is possible that there could be two relationships with this connection, one for
                        // me and one for a teammate.  But, there can only be one hiring_manager_status = accepted
                        // relationship, so by restricting to that we can be sure to get just one.
                        scope.careersNetworkViewModel
                            .getHiringRelationshipViewModelAcceptedByTeam(connectionId, true)
                            .then(hiringRelationshipViewModel => {
                                // Note: this ensures that the only conversations you can view are those with a match or those that are a new request for the candidate
                                // Otherwise, you could construct the URL for a connection that you've merely liked as a hiring manager and start sending messages
                                if (
                                    hiringRelationshipViewModel &&
                                    (hiringRelationshipViewModel.matched ||
                                        hiringRelationshipViewModel.newRequest ||
                                        hiringRelationshipViewModel.rejectedByCandidate)
                                ) {
                                    scope.connectionViewModel = hiringRelationshipViewModel;

                                    // set up the back button (with animation disabled)
                                    AppHeaderViewModel.showAlternateHomeButton = true;
                                }

                                // This supports the case of someone following a shared link for a candidate who is in
                                // the connection pending state.  See https://trello.com/c/gzdhD7bn
                                else if (hiringRelationshipViewModel && hiringRelationshipViewModel.connectionPending) {
                                    hiringRelationshipViewModel.showUpdateModal();
                                    $location.search('connectionId', null);
                                } else {
                                    $location.search('connectionId', null);
                                }
                            });
                    } else {
                        // otherwise set up standard home button
                        AppHeaderViewModel.showAlternateHomeButton = false;
                        scope.connectionViewModel = null;
                    }

                    // ensure we always add a history entry so the back button will work properly
                    $rootScope.pushBackButtonHistory($location.$$url, scope.currentRouteDirective);
                });
            },
        };
    },
]);
