import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/manage_position.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

('use strict');

angularModule.directive('managePosition', [
    '$injector',

    function factory($injector) {
        const dateHelper = $injector.get('dateHelper');
        const $rootScope = $injector.get('$rootScope');
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');

        return {
            scope: {
                position: '<',
                closeModal: '<',
            },
            restrict: 'E',
            templateUrl,

            link(scope) {
                const careersNetworkViewModel = CareersNetworkViewModel.get('hiringManager');

                // We might be working on a clone here (see hiring_positions_dir.js#setPosition),
                // so once we make a save we need to update the position in the cache in careersNetworkViewModel
                function savePosition(meta) {
                    scope.position.save(meta);
                    careersNetworkViewModel.commitOpenPosition(scope.position);
                    scope.closeModal();
                }

                const renewConfig = {
                    color: 'blue',
                    action() {
                        // renew position
                        scope.position.archived = false;
                        scope.position.manually_archived = false;
                        savePosition({
                            new_value_for_cancel_at_period_end: false,
                        });
                    },
                    textKey: 'renew',
                    captionKey: 'enable_renewal_date',
                };

                const buttonLookup = {
                    cancel: {
                        color: 'red',
                        action() {
                            // cancel renewal
                            savePosition({
                                new_value_for_cancel_at_period_end: true,
                            });
                        },
                        textKey: 'cancel_renewal',
                        captionKey: 'cancel_renewal_date',
                    },
                    cancelClose: {
                        color: 'red',
                        action() {
                            // cancel renewal and close position
                            scope.position.archived = true;
                            savePosition({
                                new_value_for_cancel_at_period_end: true,
                            });
                        },
                        textKey: 'cancel_and_close',
                        captionKey: 'cancel_and_close_immediately',
                    },
                    renew: {
                        ...renewConfig,
                        textKey: 'renew',
                    },
                    reopen_and_renew: {
                        ...renewConfig,
                        textKey: 'reopen_and_renew',
                    },
                    close: {
                        color: 'red',
                        action() {
                            // close position
                            scope.position.archived = true;
                            savePosition();
                        },
                        textKey: 'close',
                        captionKey: 'close_immediately',
                    },
                    leaveClosed: {
                        color: 'red',
                        action() {
                            scope.closeModal();
                        },
                        textKey: 'leave_closed',
                        captionKey: 'allow_position_to_remain_closed',
                    },
                };

                scope.$watch(
                    () => $rootScope.currentUser,
                    currentUser => {
                        scope.currentUser = currentUser;
                    },
                );

                let hasActiveAutoRenewal = false;

                // we watch subscriptions because that is what can change the return value of hasActiveAutoRenewal
                scope.$watchGroup(['position', 'currentUser.hiring_team.subscriptions'], () => {
                    hasActiveAutoRenewal = scope.currentUser.hasActiveAutoRenewal(scope.position);

                    // If a position is manually archived but still has a subscription that has
                    // not yet been canceled, then this modal can be opened with the renew button
                    // in the "closed" tab of the positions page.
                    if (scope.position.manually_archived && !hasActiveAutoRenewal) {
                        scope.buttons = [buttonLookup.reopen_and_renew, buttonLookup.leaveClosed];
                    } else if (scope.position.archived) {
                        // This is an invalid state. do nothing. We will log to sentry below
                        // This is invalid because you should never be able to open
                        // this directive with an archived position (see position-bar.html)
                    } else if (hasActiveAutoRenewal) {
                        scope.buttons = [buttonLookup.cancel, buttonLookup.cancelClose];
                    } else if (!scope.position.archived) {
                        scope.buttons = [buttonLookup.renew, buttonLookup.close];
                    }

                    if (!scope.buttons) {
                        $injector.get('ErrorLogService').notifyInProd('Position in unmanageable state', null, {
                            positionId: scope.position.id,
                            hasActiveAutoRenewal,
                            manually_archived: scope.position.manually_archived,
                            archived: scope.position.archived,
                            featured: scope.position.featured,
                            updated_at: scope.position.updated_at,
                        });
                    }

                    const subscription = scope.currentUser.subscriptionForPosition(scope.position);
                    const renewalDate = new Date(1000 * subscription.current_period_end);
                    scope.renewalDate = dateHelper.formattedUserFacingMonthDayShort(renewalDate, false);
                });
            },
        };
    },
]);
