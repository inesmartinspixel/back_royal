import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/hiring_lock_dialog.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('hiringLockDialog', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');

        return {
            scope: {
                blurTargetSelector: '<',
                unlockWhenReviewing: '<',
            },
            restrict: 'E',
            templateUrl,

            link(scope) {
                $rootScope.$watch('currentUser', currentUser => {
                    scope.currentUser = currentUser;
                    scope.weAlreadyHaveATeamTranslateValues = {
                        domain: currentUser && currentUser.emailDomain,
                    };
                });
            },
        };
    },
]);
