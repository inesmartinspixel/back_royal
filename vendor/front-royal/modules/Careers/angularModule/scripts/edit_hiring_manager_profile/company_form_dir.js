import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_hiring_manager_profile/company_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('companyForm', [
    '$injector',
    function factory($injector) {
        const FormHelper = $injector.get('FormHelper');
        const TranslationHelper = $injector.get('TranslationHelper');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                const optionTranslationhelper = new TranslationHelper('careers.field_options');

                FormHelper.supportForm(scope, scope.company);

                scope.yearOptions = [];
                for (let i = new Date().getUTCFullYear(); i >= 1800; i--) {
                    scope.yearOptions.push({
                        value: i,
                        label: i,
                    });
                }

                function toSelectOptions(optionKeys) {
                    return optionKeys.map(translationKey => {
                        if (translationKey === 'yes') {
                            return {
                                label: optionTranslationhelper.get(translationKey),
                                value: true,
                            };
                        }
                        if (translationKey === 'no') {
                            return {
                                label: optionTranslationhelper.get(translationKey),
                                value: false,
                            };
                        }
                        return {
                            label: optionTranslationhelper.get(translationKey),
                            value: translationKey,
                        };
                    });
                }

                scope.sizeOptions = toSelectOptions($injector.get('CAREERS_EMPLOYEE_COUNT_KEYS'));
                scope.revenueOptions = toSelectOptions($injector.get('CAREERS_ANNUAL_REVENUE_KEYS'));
                scope.fundingOptions = toSelectOptions($injector.get('CAREERS_FUNDING_KEYS'));
                scope.industryOptions = toSelectOptions($injector.get('CAREERS_INDUSTRY_KEYS'));
                scope.recruitingServicesOptions = toSelectOptions($injector.get('CAREERS_YES_NO_KEYS'));
            },
        };
    },
]);
