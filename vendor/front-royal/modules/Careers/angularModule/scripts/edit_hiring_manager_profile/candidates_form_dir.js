import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_hiring_manager_profile/candidates_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('candidatesForm', [
    '$injector',
    function factory($injector) {
        const FormHelper = $injector.get('FormHelper');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                // Add the 'any' key to the list of available salary range options. This particular key should only
                // be included for hiring managers, which is why it's not included in CAREERS_BASE_SALARY_RANGE_KEYS.
                scope.defaultAndDominantSalaryRangeValue = 'any';
                scope.salaryRangeOptions = [scope.defaultAndDominantSalaryRangeValue].concat(
                    $injector.get('CAREERS_BASE_SALARY_RANGE_KEYS'),
                );
                scope.whereOptions = ['remote', 'relocate'];
                scope.positionOptions = $injector.get('CAREERS_POSITION_DESCRIPTOR_KEYS');

                FormHelper.supportForm(scope, scope.candidates);

                const propsRequiredFieldsMap = _.findWhere(scope.steps, {
                    stepName: 'candidates',
                }).requiredFields;

                const propsValuesMap = {
                    where_descriptors: scope.whereOptions,
                    position_descriptors: scope.positionOptions,
                    salary_ranges: scope.salaryRangeOptions,
                };

                const propsDefaultAndDominantValuesMap = {
                    salary_ranges: scope.defaultAndDominantSalaryRangeValue,
                };

                FormHelper.supportCheckboxGroups(
                    scope,
                    scope.hiringApplication,
                    propsValuesMap,
                    propsRequiredFieldsMap,
                    propsDefaultAndDominantValuesMap,
                    propsDefaultAndDominantValuesMap,
                );
            },
        };
    },
]);
