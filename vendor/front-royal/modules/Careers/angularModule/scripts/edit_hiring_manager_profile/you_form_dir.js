import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_hiring_manager_profile/you_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('youForm', [
    '$injector',
    function factory($injector) {
        const TranslationHelper = $injector.get('TranslationHelper');
        const FormHelper = $injector.get('FormHelper');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                const translationHelper = new TranslationHelper('careers.edit_hiring_manager_profile.you_form');
                FormHelper.supportForm(scope, scope.you);

                scope.placeholderFunFact = translationHelper.get('did_you_know_placeholder');

                scope.roleOptions = [
                    'senior_management',
                    'hiring_manager',
                    'hr_or_internal_recruiter',
                    'contract_recruiter',
                    'job_role_other',
                ].map(translationKey => ({
                    value: translationKey,
                    label: translationHelper.get(translationKey),
                }));
            },
        };
    },
]);
