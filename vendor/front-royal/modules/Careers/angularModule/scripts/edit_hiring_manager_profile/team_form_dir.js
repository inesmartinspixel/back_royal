import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_hiring_manager_profile/team_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('teamForm', [
    '$injector',
    function factory($injector) {
        const TranslationHelper = $injector.get('TranslationHelper');
        const FormHelper = $injector.get('FormHelper');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                const translationHelper = new TranslationHelper('careers.edit_hiring_manager_profile.team_form');
                FormHelper.supportForm(scope, scope.team);

                scope.teamNamePlaceholder = translationHelper.get('team_name_placeholder');
                scope.rolesPlaceholder = translationHelper.get('role_example');

                scope.hiringOptions = ['own_team', 'another_team', 'multiple_teams'];
            },
        };
    },
]);
