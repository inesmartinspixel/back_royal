import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_hiring_manager_profile/preview_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import emptyHiringProfile from 'vectors/empty_hiring_profile.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('previewForm', [
    '$injector',
    function factory($injector) {
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                scope.emptyHiringProfile = emptyHiringProfile;

                NavigationHelperMixin.onLink(scope); // used in directive template
            },
        };
    },
]);
