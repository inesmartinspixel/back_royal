import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/register_to_view.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('registerToView', [
    '$injector',
    function factory($injector) {
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                callback: '&',
            },
            link(scope) {
                NavigationHelperMixin.onLink(scope);

                scope.register = () => {
                    if (scope.callback) {
                        scope.callback();
                    }
                    scope.loadUrl('/hiring');
                };
            },
        };
    },
]);
