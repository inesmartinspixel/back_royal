import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_career_profile/education_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('educationForm', [
    '$injector',
    function factory($injector) {
        const FormHelper = $injector.get('FormHelper');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
        const Cohort = $injector.get('Cohort');
        const TranslationHelper = $injector.get('TranslationHelper');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                scope.helper = EditCareerProfileHelper;
                FormHelper.supportForm(scope, scope.education);

                scope.helper.logEventForApplication('viewed-education');
                scope.satMaxOptions = ['1600', '2400'];
                scope.careerProfile.sat_max_score = scope.careerProfile.sat_max_score || _.first(scope.satMaxOptions);

                // special messaging for cohorts that support early career applicants
                scope.earlyCareerApplicant = EditCareerProfileHelper.supportsEarlyCareerApplicants(scope.currentUser);
                scope.programTypeSupportsRequiredEducation = scope.helper.supportsRequiredEducation(scope.currentUser);

                const educationStep = _.findWhere(scope.steps, {
                    stepName: 'education',
                });

                scope.learningProgramType = scope.helper.isDegreeProgram(scope.currentUser) ? 'degree' : 'certificate';

                Object.defineProperty(scope, 'showHasNoFormalEducationCheckbox', {
                    get() {
                        return (
                            !!scope.careerProfile &&
                            !scope.helper.formalEducationIsRequired(scope.currentUser, scope.careerProfile) &&
                            !_.any(scope.careerProfile.formalEducationExperiences) &&
                            !scope.helper.highestLevelEducationRequiresFormalEducation(scope.careerProfile)
                        );
                    },
                    configurable: true,
                });

                scope.$watch(
                    'helper.formalEducationIsRequired(currentUser, careerProfile)',
                    formalEducationIsRequired => {
                        if (formalEducationIsRequired) {
                            scope.careerProfile.addDummyEducationExperienceIfNecessary();
                        } else if (scope.careerProfile.hasLoneEmptyFormalEducationExperience) {
                            const index = _.findIndex(
                                scope.careerProfile.education_experiences,
                                ee => ee.isDegreeProgram,
                            );
                            scope.careerProfile.education_experiences.splice(index, 1);
                        }
                    },
                );

                scope.$watchCollection('careerProfile.education_experiences', () => {
                    if (!scope.showHasNoFormalEducationCheckbox) {
                        scope.careerProfile.has_no_formal_education = false;
                    }
                });

                // special messaging for cohorts that support document upload
                scope.willProvideDocuments =
                    Cohort.supportsDocumentUpload(scope.careerProfile.program_type) && scope.helper.isApplication();

                scope.highestLevelEducationOptions = _.map($injector.get('CAREERS_HIGHEST_EDUCATION_KEYS'), key => ({
                    label: new TranslationHelper('careers.field_options').get(key),
                    value: key,
                }));

                Object.defineProperty(scope, 'showEnglishLanguageProficiency', {
                    get() {
                        return (
                            Cohort.requiresEnglishLanguageProficiency(scope.careerProfile.program_type) &&
                            !scope.helper.isCareerProfile()
                        );
                    },
                    configurable: true,
                });

                scope.englishLangueProficiencyFieldValueConfigs = [
                    {
                        value: true,
                        localeKey: 'yes',
                    },
                    {
                        value: false,
                        localeKey: 'no',
                    },
                ];

                scope.englishLangueProficiencyFieldConfigs = [
                    {
                        modelProp: 'native_english_speaker',
                        inputType: 'radio',
                        fieldIsVisible() {
                            return !scope.helper.isCareerProfile();
                        },
                        shouldRemoveFormGroupBorder() {
                            return !!isNextFieldVisible('earned_accredited_degree_in_english');
                        },
                    },
                    {
                        modelProp: 'earned_accredited_degree_in_english',
                        inputType: 'radio',
                        fieldIsVisible() {
                            return (
                                !scope.helper.isCareerProfile() && scope.careerProfile.native_english_speaker === false
                            );
                        },
                        shouldRemoveFormGroupBorder() {
                            return !!isNextFieldVisible('sufficient_english_work_experience');
                        },
                    },
                    {
                        modelProp: 'sufficient_english_work_experience',
                        inputType: 'radio',
                        fieldIsVisible() {
                            return (
                                !scope.helper.isCareerProfile() &&
                                scope.careerProfile.earned_accredited_degree_in_english === false
                            );
                        },
                        showSubtext() {
                            return scope.careerProfile.sufficient_english_work_experience === false;
                        },
                        shouldRemoveFormGroupBorder() {
                            return !!isNextFieldVisible('english_work_experience_description');
                        },
                    },
                    {
                        modelProp: 'english_work_experience_description',
                        inputType: 'text',
                        fieldIsVisible() {
                            return (
                                !scope.helper.isCareerProfile() &&
                                scope.careerProfile.sufficient_english_work_experience === true
                            );
                        },
                    },
                ];

                function isNextFieldVisible(modelProp) {
                    return _.find(
                        scope.englishLangueProficiencyFieldConfigs,
                        fieldConfig => fieldConfig.modelProp === modelProp && fieldConfig.fieldIsVisible(),
                    );
                }

                // for data integrity purposes
                scope.$watch('careerProfile.native_english_speaker', () => {
                    if (scope.careerProfile.native_english_speaker) {
                        scope.careerProfile.earned_accredited_degree_in_english = null;
                    } else if (shouldRequireEarnedAccreditedDegreeField()) {
                        educationStep.requiredFields.earned_accredited_degree_in_english = true;
                    }
                });

                scope.$watch('careerProfile.earned_accredited_degree_in_english', (newVal, oldVal) => {
                    // If a user changes this, we should null out sufficient_english_work_experience
                    // and english_work_experience_description in case they were populated. We don't
                    // want to sync these values with Airtable if the user changes their mind.
                    if (newVal !== oldVal && !!newVal) {
                        scope.careerProfile.english_work_experience_description = null;
                        scope.careerProfile.sufficient_english_work_experience = null;
                    }
                });

                function shouldRequireEarnedAccreditedDegreeField() {
                    const earnedAccreditedDegreeFieldConfig = _.findWhere(scope.englishLangueProficiencyFieldConfigs, {
                        modelProp: 'earned_accredited_degree_in_english',
                    });
                    return (
                        earnedAccreditedDegreeFieldConfig.fieldIsVisible() &&
                        scope.helper.isDegreeProgram(scope.currentUser)
                    );
                }

                // Input validation handling for score_on_sat and sat_max_score.
                scope.$watchGroup(
                    [
                        'earlyCareerApplicant',
                        "education['sat-max-score']",
                        'careerProfile.scoreOnSAT',
                        'careerProfile.sat_max_score',
                    ],
                    () => {
                        if (scope.earlyCareerApplicant && scope.education['sat-max-score']) {
                            const satMaxScoreAsInt = parseInt(scope.careerProfile.sat_max_score);

                            // sat_max_score shouldn't be less than score_on_sat if scoreOnSatIsValid
                            if (
                                scope.careerProfile.scoreOnSAT &&
                                satMaxScoreAsInt !== 2400 &&
                                scope.careerProfile.scoreOnSAT > satMaxScoreAsInt &&
                                scope.scoreOnSatIsValid
                            ) {
                                scope.education['sat-max-score'].$setValidity('satMaxScore', false);
                            } else {
                                scope.education['sat-max-score'].$setValidity('satMaxScore', true);
                            }
                        }
                    },
                );
            },
        };
    },
]);
