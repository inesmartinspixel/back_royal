import angularModule from 'Careers/angularModule/scripts/careers_module';

angularModule.factory('CandidatePositionInterest', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');
        const UnloadedChangeDetector = $injector.get('UnloadedChangeDetector');

        return Iguana.subclass(function () {
            this.setCollection('candidate_position_interests');
            this.alias('CandidatePositionInterest');
            this.embedsOne('career_profile', 'CareerProfile');

            this.UnloadedChangeDetector = UnloadedChangeDetector.createDetectorKlass('candidate_position_interest');

            Object.defineProperty(this.prototype, 'createdAt', {
                get() {
                    if (!this.$$createdAt && this.created_at) {
                        this.$$createdAt = new Date(this.created_at * 1000);
                    }
                    return this.$$createdAt;
                },
            });

            Object.defineProperty(this.prototype, 'reviewedByHiringManager', {
                get() {
                    // See also candidate_position_interest.rb#reviewed_hiring_manager_statuses
                    return _.contains(
                        ['saved_for_later', 'invited', 'accepted', 'rejected'],
                        this.hiring_manager_status,
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hiddenOrReviewedByHiringManager', {
                get() {
                    // See also candidate_position_interest.rb#reviewed_hiring_manager_statuses
                    return this.hiring_manager_status === 'hidden' || this.reviewedByHiringManager;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'adminStatusPriority', {
                get() {
                    // See also admin_status_priority ordering in candidate_position_interest_controller.rb
                    if (this.admin_status === 'outstanding') {
                        return 0;
                    }
                    if (this.admin_status === 'reviewed') {
                        return 1;
                    }
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'anonymized', {
                get() {
                    return this.career_profile && this.career_profile.anonymized;
                },
            });

            Object.defineProperty(this.prototype, 'anonymizedPriority', {
                get() {
                    // hiring managers that are not in good standing will have
                    // some interests anonymized.  We move those to the bottom
                    if (this.anonymized) {
                        return 1;
                    }
                    return 0;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasCoverLetter', {
                get() {
                    return !!(this.cover_letter && this.cover_letter.content);
                },
                configurable: true,
            });
        });
    },
]);
