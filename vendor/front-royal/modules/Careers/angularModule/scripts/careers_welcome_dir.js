import angularModule from 'Careers/angularModule/scripts/careers_module';
import { setupBrandNameProperties } from 'AppBrandMixin';
import template from 'Careers/angularModule/views/careers_welcome.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import smartlyTalent from 'vectors/smartly-talent.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('careersWelcome', [
    '$injector',

    function factory($injector) {
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const $rootScope = $injector.get('$rootScope');
        const DialogModal = $injector.get('DialogModal');
        const $translate = $injector.get('$translate');
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
        const OpenPosition = $injector.get('OpenPosition');
        const $q = $injector.get('$q');

        return {
            scope: {},
            restrict: 'E',
            templateUrl,

            link(scope) {
                scope.smartlyTalent = smartlyTalent;

                const careersNetworkViewModel = CareersNetworkViewModel.get('candidate');

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'hasConfirmedProfile', {
                    get() {
                        return !!scope.currentUser.career_profile.last_confirmed_at_by_student;
                    },
                    configurable: true,
                });

                NavigationHelperMixin.onLink(scope);
                setupBrandNameProperties($injector, scope);

                if (!scope.currentUser.has_seen_careers_welcome) {
                    DialogModal.alert({
                        title: $translate.instant('careers.careers_welcome.welcome_to_career_network', {
                            brandName: scope.brandNameShort,
                        }),
                        content:
                            '<h3 translate-once="careers.careers_welcome.next_steps"></h3>' +
                            '<ol>' +
                            '<li translate-once="careers.careers_welcome.confirm_profile_w_link" translate-compile></li>' +
                            '<li translate-once="careers.careers_welcome.featured_positions" translate-compile></li>' +
                            '</ol>' +
                            '<p class="complete-setup"><button class="flat blue" ng-click="getStarted()" translate-once="careers.careers_welcome.get_started"></button></p>',
                        scope: {
                            getStarted() {
                                if (!scope.currentUser.ghostMode) {
                                    scope.currentUser.has_seen_careers_welcome = true;
                                    scope.currentUser.save();
                                }
                                DialogModal.hideAlerts();
                            },
                            loadRoute(route) {
                                scope.loadRoute(route);
                                DialogModal.hideAlerts();
                            },
                        },
                        classes: ['next-steps'],
                        hideCloseButton: true,
                        blurTargetSelector: 'div[ng-controller]',
                    });
                }

                // Note: If we ever disable HTTP queueing this would be better
                // written as a $q.all, but since we do HTTP queueing I'm going
                // to attempt to grab recommended first then fallback to grabbing
                // the top two sorted by RECOMMENDED_FOR_CANDIDATE. Another option
                // would have been to fallback in the server code but I would have
                // needed to add a query to every call, and maybe some other work,
                // to do that.
                scope.loadingRecommended = true;
                careersNetworkViewModel
                    .loadRecommendedPositions({
                        limit: 2,
                        max_total_count: 2,
                    })
                    .then(recommendedPositions => {
                        if (recommendedPositions.length === 0) {
                            return OpenPosition.index({
                                filters: {
                                    featured: true,
                                    archived: false,
                                    hiring_manager_might_be_interested_in: scope.currentUser.id,
                                    candidate_has_acted_on: false,
                                },
                                limit: 2,
                                max_total_count: 2,
                                candidate_id: scope.currentUser.id,
                                sort: 'RECOMMENDED_FOR_CANDIDATE',
                            }).then(response => response.result);
                        }
                        return $q.when(recommendedPositions);
                    })
                    .then(recommendedOrFallbackPositions => {
                        scope.positions = recommendedOrFallbackPositions;
                        scope.loadingRecommended = false;
                    });
            },
        };
    },
]);
