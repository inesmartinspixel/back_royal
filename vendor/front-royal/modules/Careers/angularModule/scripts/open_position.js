import angularModule from 'Careers/angularModule/scripts/careers_module';

angularModule.factory('OpenPosition', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');
        const HasLocation = $injector.get('HasLocation');
        const UnloadedChangeDetector = $injector.get('UnloadedChangeDetector');
        const DialogModal = $injector.get('DialogModal');
        const $rootScope = $injector.get('$rootScope');
        const $location = $injector.get('$location');

        return Iguana.subclass(function () {
            this.setCollection('open_positions');
            this.alias('OpenPosition');
            this.include(HasLocation);

            // Since the filters for index calls can get really long,
            // we use a post to avoid having get urls that go beyond browser
            // limits.
            this.overrideAction('index', {
                method: 'POST',
                path: 'index',
            });

            this.setCallback('after', 'copyAttrsOnInitialize', function () {
                this.skills = this.skills || [];
                this.perks = this.perks || [];
                this.position_descriptors = this.position_descriptors || [];
                this.desired_years_experience = this.desired_years_experience || {
                    min: null,
                    max: null,
                };
                this.featured = this.featured || false;
                this.archived = this.archived || false;
            });

            this.UnloadedChangeDetector = UnloadedChangeDetector.createDetectorKlass('open_position');

            this.extend({
                // eslint-disable-next-line lodash-fp/prefer-identity
                mapClientSortToServerSort(clientSort) {
                    return clientSort;
                },
            });

            Object.defineProperty(this.prototype, 'bonus', {
                get() {
                    return _.contains(this.perks, 'bonus');
                },
                set(val) {
                    this.perks = this.perks || [];
                    if (val && !this.bonus) {
                        this.perks.push('bonus');
                    } else if (!val) {
                        this.perks = _.without(this.perks, 'bonus');
                    }
                },
            });

            Object.defineProperty(this.prototype, 'equity', {
                get() {
                    return _.contains(this.perks, 'equity');
                },
                set(val) {
                    this.perks = this.perks || [];
                    if (val && !this.equity) {
                        this.perks.push('equity');
                    } else if (!val) {
                        this.perks = _.without(this.perks, 'equity');
                    }
                },
            });

            Object.defineProperty(this.prototype, 'searchableText', {
                get() {
                    if (!this.$$searchableText) {
                        const $translate = $injector.get('$translate');
                        this.$$searchableText = _.flatten([
                            this.title,
                            this.locationString,
                            this.description,
                            $translate.instant(`careers.field_options.${this.role}`),
                            _.map(this.skills, s => s.text),
                            _.map(this.position_descriptors, v => $translate.instant(`careers.position_card.${v}`)),
                            _.map(this.perks, v => $translate.instant(`careers.position_card.${v}`)),
                        ])
                            .join(' ')
                            .toLowerCase();
                    }
                    return this.$$searchableText;
                },
            });

            Object.defineProperty(this.prototype, 'author', {
                get() {
                    return this.hiring_application && this.hiring_application.name;
                },
            });

            Object.defineProperty(this.prototype, 'draftedFromInvite', {
                get() {
                    // the only way to have a title and not a role is if it was drafted from the invite modal
                    return this.title && !this.role && !this.featured && !this.drafted_from_positions;
                },
            });

            Object.defineProperty(this.prototype, 'lastCuratedAt', {
                get() {
                    if (!this.$$lastCuratedAt && this.last_curated_at) {
                        this.$$lastCuratedAt = new Date(this.last_curated_at * 1000);
                    }
                    return this.$$lastCuratedAt;
                },
                set(date) {
                    this.last_curated_at = date.getTime() / 1000;
                    this.$$lastCuratedAt = new Date(this.last_curated_at * 1000);
                },
            });

            Object.defineProperty(this.prototype, 'curationEmailLastTriggeredAt', {
                get() {
                    if (!this.$$curationEmailLastTriggeredAt && this.curation_email_last_triggered_at) {
                        this.$$curationEmailLastTriggeredAt = new Date(this.curation_email_last_triggered_at * 1000);
                    }
                    return this.$$curationEmailLastTriggeredAt;
                },
                set(date) {
                    this.curation_email_last_triggered_at = date.getTime() / 1000;
                    this.$$curationEmailLastTriggeredAt = new Date(this.curation_email_last_triggered_at * 1000);
                },
            });

            Object.defineProperty(this.prototype, 'postedAt', {
                get() {
                    if (!this.$$postedAt && this.posted_at) {
                        this.$$postedAt = new Date(this.posted_at * 1000);
                    }
                    return this.$$postedAt;
                },
            });

            Object.defineProperty(this.prototype, 'hiringManagerLastSeenAt', {
                get() {
                    if (!this.$$hiringManagerLastSeenAt && this.hiring_manager_last_seen_at) {
                        this.$$hiringManagerLastSeenAt = new Date(this.hiring_manager_last_seen_at * 1000);
                    }
                    return this.$$hiringManagerLastSeenAt;
                },
            });

            Object.defineProperty(this.prototype, 'status', {
                get() {
                    if (this.archived) {
                        return 'archived';
                    }
                    if (this.featured) {
                        return 'featured';
                    }
                    return 'drafted';
                },
            });

            Object.defineProperty(this.prototype, 'showManage', {
                get() {
                    // if the position is archived, we show the renew button
                    // and not the manage button (there might be a subscription in
                    // this case, if the user clicked "Cancel and Close" to
                    // set subscriptipn.cancel_at_period_end = true and position.archived = true)
                    if (this.archived) {
                        return false;
                    }

                    // If the position is featured and has a subscription, we show
                    // the manage button
                    return !!$rootScope.currentUser.subscriptionForPosition(this);
                },
            });

            Object.defineProperty(this.prototype, 'hasInstantApply', {
                get() {
                    // Stubbed out. ATM all open positions are internal, but these two tickets
                    // will be changing that in the near-term:
                    // https://trello.com/c/5cH9Mhwu/3213-feat-use-ziprecruiter-api-to-bolster-job-search-results-in-featured-positions
                    // https://trello.com/c/ysJO9KJN/3172-feat-enable-3rd-party-jobs-in-career-network-featured-positions
                    return !this.external;
                },
                configurable: true,
            });

            // We have internal OpenPositions with placeDetails which we use to construct
            // a nice and readable representation of the location. We have some external
            // positions (Zip Recruiter) where we rely on an external API to provide us
            // with a location string.
            Object.defineProperty(this.prototype, 'formattedLocationStringOrExternalLocation', {
                get() {
                    if (!this.external) {
                        return this.locationString;
                    }
                    return this.location;
                },
                configurable: true,
            });

            return {
                toggleArchived() {
                    this.archived = !this.archived;
                    return this.save();
                },

                manage() {
                    DialogModal.alert({
                        content: '<manage-position position="position" close-modal="closeModal"></manage-position>',
                        scope: {
                            position: this,
                            closeModal() {
                                DialogModal.hideAlerts();
                            },
                        },
                        classes: ['manage-position'],
                    });
                },

                renew() {
                    const subscription = $rootScope.currentUser.subscriptionForPosition(this);
                    if (subscription) {
                        this.manage();
                    } else {
                        $location.search('positionId', this.id);
                        $location.search('action', 'edit');
                        $location.search('startPage', 'pay_and_publish');
                    }
                },
            };
        });
    },
]);
