import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/hiring_manager_sorry.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('hiringManagerSorry', [
    '$injector',

    function factory($injector) {
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const isMobileMixin = $injector.get('isMobileMixin');

        return {
            scope: {},
            restrict: 'E',
            templateUrl,

            link(scope) {
                isMobileMixin.onLink(scope);

                scope.$watch('isMobile', val => {
                    AppHeaderViewModel.toggleVisibility(!val);
                });

                scope.$on('$destroy', () => {
                    AppHeaderViewModel.toggleVisibility(true);
                });
            },
        };
    },
]);
