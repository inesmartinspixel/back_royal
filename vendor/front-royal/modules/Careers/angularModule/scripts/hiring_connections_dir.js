import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/hiring_connections.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('hiringConnections', [
    '$injector',

    function factory($injector) {
        const AppHeaderViewModel = $injector.get('Navigation.AppHeader.AppHeaderViewModel');
        const $location = $injector.get('$location');
        const $rootScope = $injector.get('$rootScope');
        const TranslationHelper = $injector.get('TranslationHelper');

        return {
            scope: {},
            restrict: 'E',
            templateUrl,

            link(scope, elem) {
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.$location = $location;
                const translationHelper = new TranslationHelper('navigation.app_menu');

                scope.$watch('$location.search().connectionId', connectionId => {
                    if (!connectionId) {
                        // The tracker does not have a white-backgrounded box taking up the
                        // main part of the screen. Instead it has a few elements that lay
                        // on top of the default grey background.
                        elem.removeClass('page-with-just-main-box-800');
                        AppHeaderViewModel.setTitleHTML(translationHelper.get('inbox').toUpperCase());
                        // otherwise, we defer to connection_conversation_dir.js to set the title
                    } else {
                        // the connection conversation needs to be on a white background
                        elem.addClass('page-with-just-main-box-800');
                    }
                });
            },
        };
    },
]);
