import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/career_profile_status_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('careerProfileStatusForm', [
    '$injector',
    function factory($injector) {
        const TranslationHelper = $injector.get('TranslationHelper');
        const $rootScope = $injector.get('$rootScope');
        const FormHelper = $injector.get('FormHelper');
        const ngToast = $injector.get('ngToast');
        const DialogModal = $injector.get('DialogModal');

        return {
            restrict: 'E',
            scope: {},
            templateUrl,

            link(scope) {
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'careerProfile', {
                    get() {
                        return scope.currentUser && scope.currentUser.career_profile;
                    },
                });

                scope.$watch('careerProfile.interested_in_joining_new_company', val => {
                    scope.selectedInterestLevel = _.findWhere(scope.careerProfileActiveInterestLevels, {
                        value: val,
                    });
                });

                FormHelper.supportForm(scope, scope.status_form);

                const translationHelper = new TranslationHelper('careers.preview_candidate_card');

                const interestLevels = $injector.get('NEW_COMPANY_INTEREST_LEVELS');
                scope.careerProfileActiveInterestLevels = _.map(interestLevels, interestLevel => ({
                    value: interestLevel,
                    name: translationHelper.get(`${interestLevel}_label`),
                    caption: translationHelper.get(`${interestLevel}_caption`),
                }));

                scope.save = () => {
                    if (scope.currentUser.can_edit_career_profile && scope.currentUser.career_profile.complete) {
                        scope.careerProfile.interested_in_joining_new_company = scope.selectedInterestLevel.value;
                        const meta = {
                            confirmed_by_student: true,
                        };
                        scope.careerProfile.save(meta).then(() => {
                            scope.status_form.$setPristine();

                            ngToast.create({
                                content: translationHelper.get('content_and_status_confirmed'),
                                className: 'success',
                            });

                            if (
                                _.contains([null, 'never'], scope.currentUser.notify_candidate_positions_recommended) &&
                                scope.selectedInterestLevel.value !== 'not_interested'
                            ) {
                                DialogModal.alert({
                                    content:
                                        '<recommended-positions-notification-modal on-finish="onFinish"></recommended-positions-notification-modal>',
                                    classes: ['medium-small'],
                                    scope: {
                                        onFinish() {
                                            DialogModal.hideAlerts();
                                        },
                                    },
                                });
                            }
                        });
                    }
                };
            },
        };
    },
]);
