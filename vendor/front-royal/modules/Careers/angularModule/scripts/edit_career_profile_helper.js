import angularModule from 'Careers/angularModule/scripts/careers_module';
import * as userAgentHelper from 'userAgentHelper';

angularModule.service('EditCareerProfileHelper', [
    '$injector',
    function ($injector) {
        const $location = $injector.get('$location');
        const $rootScope = $injector.get('$rootScope');
        const EventLogger = $injector.get('EventLogger');

        // --------------------------
        // Tests for which context the user is filling out a profile in
        // --------------------------

        this.careerProfileUrl = '/settings/my-profile';
        this.applicationUrl = '/settings/application';
        this.applicantUrl = '/admin/users/applicants';

        this.isApplication = function () {
            // NOTE: cannot use indexOf because this should not match
            // the settings/application_status page. See https://trello.com/c/x98UKImS/2289-bug-cannot-active-incomplete-profile
            return $location.path() === this.applicationUrl;
        };

        this.isMbaApplication = function () {
            return this.isApplication() && $rootScope.currentUser && $rootScope.currentUser.isMBA;
        };

        this.isEmbaApplication = function () {
            return this.isApplication() && $rootScope.currentUser && $rootScope.currentUser.isEMBA;
        };

        this.isApplicantEditor = function () {
            return $location.path().includes(this.applicantUrl);
        };

        this.isCareerProfile = function () {
            return $location.path().includes(this.careerProfileUrl);
        };

        this.getRootUrl = function () {
            if (this.isCareerProfile()) {
                return this.careerProfileUrl;
            }
            if (this.isApplication()) {
                return this.applicationUrl;
            }
            if (this.isApplicantEditor()) {
                return this.applicantUrl;
            }
            throw new Error('Where are we then?');
        };

        // --------------------------
        // Tests for which features are supported/required for the profile
        // --------------------------

        // NOTE: This method returns false if the Cohort.js dependency is not yet loaded
        this.getProgramTypeConfigValue = (user, callback) => {
            let Cohort;
            try {
                Cohort = $injector.get('Cohort');
            } catch (e) {
                return false;
            }
            return user && user.programType && Cohort[callback](user.programType);
        };

        this.isDegreeProgram = function (user) {
            return this.getProgramTypeConfigValue(user, 'isDegreeProgram');
        };

        this.requiresEnglishLanguageProficiency = user => {
            if (
                user &&
                this.getProgramTypeConfigValue(user, 'requiresEnglishLanguageProficiency') &&
                user.programType === 'the_business_certificate' &&
                user.career_profile &&
                user.career_profile.created_at * 1000 < new Date(2019, 1, 23).getTime()
            ) {
                return false;
            }
            return this.getProgramTypeConfigValue(user, 'requiresEnglishLanguageProficiency');
        };

        this.supportsEarlyCareerApplicants = function (user) {
            return this.getProgramTypeConfigValue(user, 'supportsEarlyCareerApplicants');
        };

        this.supportsRequiredResume = function (user) {
            return (
                !userAgentHelper.isMobileDevice() &&
                !this.isApplicantEditor() &&
                this.getProgramTypeConfigValue(user, 'supportsRequiredResume')
            );
        };

        this.supportsRequiredLinkedInProfile = function (user) {
            return this.getProgramTypeConfigValue(user, 'supportsRequiredLinkedInProfile');
        };

        this.supportsLongTermGoalField = function (user) {
            return this.getProgramTypeConfigValue(user, 'supportsLongTermGoalField');
        };

        this.supportsSubmitToStatusPage = function (user) {
            return this.getProgramTypeConfigValue(user, 'supportsSubmitToStatusPage');
        };

        this.supportsMobilePhoneNumber = function (user) {
            return this.getProgramTypeConfigValue(user, 'supportsMobilePhoneNumber');
        };

        this.supportsPeerRecommendations = function (user) {
            return this.getProgramTypeConfigValue(user, 'supportsPeerRecommendations');
        };

        this.supportsRequiredEducation = function (user) {
            return this.getProgramTypeConfigValue(user, 'supportsRequiredEducation');
        };

        this.supportsNetworkAccess = function (user) {
            return this.getProgramTypeConfigValue(user, 'supportsNetworkAccess');
        };

        this.supportsPersonalSummaryAnythingElseField = function (user) {
            return this.getProgramTypeConfigValue(user, 'supportsPersonalSummaryAnythingElseField');
        };

        this.supportsEarlyDecisionConsideration = user =>
            user && user.relevant_cohort && user.relevant_cohort.supportsEarlyDecisionConsideration;

        this.highestLevelEducationRequiresFormalEducation = careerProfile => {
            const highestLevelEducation =
                careerProfile && careerProfile.survey_highest_level_completed_education_description;
            return !!highestLevelEducation && highestLevelEducation !== 'high_school';
        };

        this.formalEducationIsRequired = (user, careerProfile) =>
            this.supportsRequiredEducation(user) || this.highestLevelEducationRequiresFormalEducation(careerProfile);

        // --------------------------
        // Locales
        // --------------------------

        this.workFormFullTimeWorkHistoryKey = function (user) {
            return this.getProgramTypeConfigValue(user, 'workFormFullTimeWorkHistoryKey');
        };

        this.workFormPartTimeWorkHistoryKey = function (user) {
            return this.getProgramTypeConfigValue(user, 'workFormPartTimeWorkHistoryKey');
        };

        // --------------------------
        // Event logging helper
        // --------------------------

        this.logEventForApplication = function (eventName, force, payload = {}) {
            if (!eventName) {
                throw new Error('Calling edit_career_profile_helper#logEventForApplication without an eventName');
            }

            if (this.isApplication() || force) {
                const type = `application-form:${eventName}`;
                EventLogger.allowEmptyLabel([type]);

                EventLogger.log(
                    type,
                    _.extend(
                        {
                            program_type: $rootScope.currentUser && $rootScope.currentUser.programType,
                        },
                        payload,
                    ),
                    {
                        // currently only interested in this particular event. if we want to expand on
                        // this, we need to make sure that the event name is coherent when truncated to
                        // 40-chars (in order to meet FB event name guidelines) in EventLogger
                        cordovaFacebook: eventName === 'submit-application',
                    },
                );
            }
        };

        // --------------------------
        // Helpers to return a set of required steps for each career profile context
        // --------------------------

        // The steps and required fields for the CareerProfile to be active in the career network
        // This is a subset of the required fields for the various user-facing forms, since we collect
        // some "required" data in those forms that isn't necessary for a good looking card.
        this.getStepsForCareerProfile = careerProfile => {
            let steps = angular.copy($injector.get('CAREER_PROFILE_FORM_STEPS'));

            steps = handleLegacySkills(steps, careerProfile);
            // We do not add workAndEducation here because those are not required to have a "complete"
            // career profile and as such are not included in the calculation of last_calculated_complete_percentage

            // don't need this for the profile itself
            const howDidYouHearAboutUsStep = _.findWhere(steps, {
                stepName: 'personal_summary',
            });
            delete howDidYouHearAboutUsStep.requiredFields.how_did_you_hear_about_us;

            return steps;
        };

        // Tweaks to the steps and required fields for the CareerProfile in the
        // Applicant Editor context
        this.getStepsForApplicantEditorForm = function (user) {
            let steps = angular.copy($injector.get('CAREER_PROFILE_FORM_STEPS'));
            const userProgram = user.programType;

            const basicInfoStep = _.findWhere(steps, {
                stepName: 'basic_info',
            });
            delete basicInfoStep.requiredFields.birthdate;

            steps = this.addWorkToRequiredFields(steps);
            steps = this.addEducationToRequiredFields(steps, user);
            steps = handleLegacySkills(steps, user && user.career_profile);
            steps = addQuestionsStepIfApplicable(userProgram, steps, steps.length, user && user.career_profile);

            return steps;
        };

        // Tweaks to the steps and required fields for the CareerProfile in the
        // CareerProfile context
        this.getStepsForCareersForm = function (user) {
            let steps = angular.copy($injector.get('CAREER_PROFILE_FORM_STEPS'));

            steps = this.addWorkToRequiredFields(steps);
            steps = this.addEducationToRequiredFields(steps, user);
            steps = handleLegacySkills(steps, user && user.career_profile);

            // Add student_network_settings step before submit_application
            if (this.supportsNetworkAccess(user)) {
                steps.push({
                    stepName: 'student_network_settings',
                    requiredFields: {
                        pref_student_network_privacy: true,
                    },
                    requiresVisit: true,
                });
            }

            steps?.push({
                stepName: 'preview_profile',
            });

            return steps;
        };

        // Tweaks to the steps and required fields for the CareerProfile in the
        // Mba Application context
        this.getStepsForApplicationForm = function (user) {
            let steps = angular.copy($injector.get('CAREER_PROFILE_FORM_STEPS'));

            const userProgram = user && user.programType;
            const pendingCohortApplication = user && user.pendingCohortApplication;

            // FIXME: Once we force users to upgrade their clients to support the new certificate program types,
            // we want to remove the conditional wrapping this bit of logic so that we always add the Program
            // Choice step regardless of the program type. See https://trello.com/c/0C1D0SaE.
            if (
                !_.contains(
                    [
                        'marketing_basics_certificate',
                        'accounting_basics_certificate',
                        'finance_basics_certificate',
                        'microeconomics_basics_certificate',
                        'statistics_basics_certificate',
                    ],
                    userProgram,
                )
            ) {
                // Add Program choice step as first step
                const stepName =
                    !!user.lastCohortApplication &&
                    user.reapplyingOrEditingApplication &&
                    !(user.isRejected || user.isExpelled)
                        ? 'program_lockdown'
                        : 'program_choice';
                const programChoiceStep = {
                    stepName,
                    requiredFields: {
                        programTypeIfAllowed: true,
                    },
                };
                steps.unshift(programChoiceStep);
            }

            // Add basic info requirements
            const basicInfoStep = _.findWhere(steps, {
                stepName: 'basic_info',
            });
            basicInfoStep.requiredFields.birthdate = true;
            basicInfoStep.requiredFields.nickname = true;

            // for non-degree applicants, move the avatar_url requirement to the career_preferences form
            if (!_.contains(['mba', 'emba'], userProgram)) {
                const moreAboutYouStep = _.findWhere(steps, {
                    stepName: 'more_about_you',
                });
                delete moreAboutYouStep.requiredFields.avatar_url;
                const careerPreferencesStep = _.findWhere(steps, {
                    stepName: 'career_preferences',
                });
                careerPreferencesStep.requiredFields.avatar_url = true;
            }

            // Remove certain steps from the cohort application
            if (userProgram === 'emba' || userProgram === 'mba') {
                _.each(['more_about_you', 'select_skills', 'career_preferences'], stepName => {
                    const stepIndex = _.findIndex(steps, step => step.stepName === stepName);
                    steps.splice(stepIndex, 1);
                });
            }

            steps = this.addWorkToRequiredFields(steps);
            steps = this.addEducationToRequiredFields(steps, user);

            const resumeAndLinksStep = _.findWhere(steps, {
                stepName: 'resume_and_links',
            });

            // conditionally require the resume for certain students
            if (this.supportsRequiredResume(user)) {
                resumeAndLinksStep.requiredFields.resume = true;
            }

            // conditionally require linkedin for certain students
            if (this.supportsRequiredLinkedInProfile(user)) {
                resumeAndLinksStep.requiredFields.li_profile_url = true;
            }

            // conditionally require long term goal for certain students
            if (this.supportsLongTermGoalField(user)) {
                const personalSummaryStep = _.findWhere(steps, {
                    stepName: 'personal_summary',
                });
                personalSummaryStep.requiredFields.long_term_goal = true;
            }

            // Add student_network_settings step before submit_application
            if (this.supportsNetworkAccess(user)) {
                steps.push({
                    stepName: 'student_network_settings',
                    requiredFields: {
                        pref_student_network_privacy: true,
                    },
                    requiresVisit: true,
                });
            }

            // insert the (e)mba_application_questions step between the
            // student_network_settings and submit_application steps
            steps = addQuestionsStepIfApplicable(userProgram, steps, steps.length, user && user.career_profile);

            steps = this.addSubmitApplicationStep(steps, user);

            // If an application has been retargeted, then it is always considered complete.
            // This prevents people from having to go back and add things to their application
            // because we decidede to switch them to a different program type.
            if (pendingCohortApplication && pendingCohortApplication.retargeted_from_program_type) {
                steps = _.map(steps, step =>
                    _.extend({}, step, {
                        requiredFields: {},
                    }),
                );
            }

            return steps;
        };

        // --------------------------
        // Internal helpers to adjust required fields, etc.
        // --------------------------

        this.addSubmitApplicationStep = function (steps, user) {
            const submitApplicationStep = {
                stepName: 'submit_application',
            };

            // If emba application, require consider_early_decision
            if (this.supportsEarlyDecisionConsideration(user)) {
                if (
                    this.isApplicantEditor() ||
                    (user.relevant_cohort && !user.relevant_cohort.lastAdmissionRoundDeadlineHasPassed())
                ) {
                    submitApplicationStep.requiredFields = {
                        consider_early_decision: true,
                    };
                }
            }

            steps.push(submitApplicationStep);
            return steps;
        };

        this.addWorkToRequiredFields = function (steps) {
            const workStep = _.findWhere(steps, {
                stepName: 'work',
            });

            // full-time work experience is always required, regardless of program type
            // (see https://trello.com/c/oYNJo6mh)
            workStep.requiredFields.fullTimeWorkExperiences = {
                min: 1,
                requiredFields: {
                    professional_organization: true,
                    job_title: true,
                    start_date: true,
                },
            };

            // Since featuredWorkExperience is derived from fullTimeWorkExperiences, it should be
            // required if and only if fullTimeWorkExperiences is required.
            workStep.requiredFields.featuredWorkExperience = true;

            workStep.requiredFields.fullTimeWorkExperiences.requiredFields.responsibilities = {
                min: 1,
            };
            workStep.requiredFields.survey_years_full_time_experience = true;
            workStep.requiredFields.survey_most_recent_role_description = true;
            return steps;
        };

        this.addEducationToRequiredFields = function (steps, user) {
            const educationStep = _.findWhere(steps, {
                stepName: 'education',
            });

            if (
                this.supportsRequiredEducation(user) ||
                this.highestLevelEducationRequiresFormalEducation(user.career_profile)
            ) {
                // education experience
                educationStep.requiredFields.education_experiences = {
                    min: 1,
                    requiredFields: {
                        educational_organization: true,
                        degree: true,
                        graduation_year: true,
                        major: true,
                    },
                };
            } else if (!this.highestLevelEducationRequiresFormalEducation(user.career_profile)) {
                educationStep.requiredFields.has_no_formal_education = true;
            }

            if (this.isDegreeProgram(user) && !this.isCareerProfile()) {
                // English Language Proficiency
                educationStep.requiredFields.native_english_speaker = true;
            }

            educationStep.requiredFields.survey_highest_level_completed_education_description = true;
            return steps;
        };

        function addQuestionsStepIfApplicable(programType, steps, spliceIntoIndex, careerProfile) {
            // if user selected one of the MBA program types,
            // insert mba_application_questions step after education and work
            let questionsStep;
            if (programType === 'emba') {
                questionsStep = {
                    stepName: 'emba_application_questions',
                    requiredFields: {
                        short_answer_why_pursuing: true,
                        short_answer_greatest_achievement: true,
                        short_answer_ask_professional_advice: true,
                    },
                };
            } else if (programType === 'mba') {
                questionsStep = {
                    stepName: 'mba_application_questions',
                    requiredFields: {
                        short_answer_why_pursuing: true,
                        short_answer_use_skills_to_advance: true,
                        short_answer_greatest_achievement: true,
                        short_answer_ask_professional_advice: true,
                    },
                };
            } else if (programType === 'the_business_certificate') {
                const requiredFields =
                    careerProfile && careerProfile.created_at * 1000 < new Date(2019, 1, 23).getTime()
                        ? {}
                        : {
                              short_answer_why_pursuing: true,
                              short_answer_greatest_achievement: true,
                              short_answer_ask_professional_advice: true,
                          };
                questionsStep = {
                    stepName: 'cert_application_questions',
                    requiredFields,
                };
            }

            // if we have a questions step, add it
            if (questionsStep) {
                steps.splice(spliceIntoIndex, 0, questionsStep);
            }

            return steps;
        }

        function handleLegacySkills(steps, careerProfile) {
            // At some point along the way we wanted to require users to add at least 4 skills
            // and at least 2 awards/interests. We updated the locale strings, but never
            // actually enforced this in the code. As a result, we have to grandfather in users
            // that have complete career profiles, but don't meet the minimum requirements for
            // skills and awards and interests.
            if (careerProfile && careerProfile.created_at * 1000 < new Date(2017, 6, 1).getTime()) {
                const moreAboutYouStep = _.findWhere(steps, {
                    stepName: 'more_about_you',
                });
                moreAboutYouStep.requiredFields.awards_and_interests.min = 1;

                const selectSkillsStep = _.findWhere(steps, {
                    stepName: 'select_skills',
                });
                selectSkillsStep.requiredFields.skills.min = 1;
            }

            return steps;
        }
    },
]);
