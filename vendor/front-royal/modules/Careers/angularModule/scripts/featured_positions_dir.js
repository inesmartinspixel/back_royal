import angularModule from 'Careers/angularModule/scripts/careers_module';
import { setupBrandNameProperties } from 'AppBrandMixin';
import { getZipRecruiterJobs } from 'ZipRecruiterSearch';
import template from 'Careers/angularModule/views/featured_positions.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('featuredPositions', [
    '$injector',

    function factory($injector) {
        const $filter = $injector.get('$filter');
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
        const $location = $injector.get('$location');
        const CandidatePositionInterest = $injector.get('CandidatePositionInterest');
        const DialogModal = $injector.get('DialogModal');
        const $rootScope = $injector.get('$rootScope');
        const roleKeys = $injector.get('CAREERS_AREA_KEYS');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const TranslationHelper = $injector.get('TranslationHelper');
        const $q = $injector.get('$q');
        const listOffsetHelper = $injector.get('listOffsetHelper');
        const HasLocation = $injector.get('HasLocation');
        const OpenPosition = $injector.get('OpenPosition');
        const OpenPositionFilterSet = $injector.get('OpenPositionFilterSet');
        const $window = $injector.get('$window');
        const safeApply = $injector.get('safeApply');
        const EventLogger = $injector.get('EventLogger');
        const injector = $injector.get('injector');

        return {
            scope: {},
            restrict: 'E',
            templateUrl,
            link(scope) {
                // used in 'instructions' translated string in template
                NavigationHelperMixin.onLink(scope);
                setupBrandNameProperties($injector, scope);

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'disableFilters', {
                    get() {
                        return (
                            (scope.noPositions && !scope.hasFilters) ||
                            scope.currentTabConfig.loading ||
                            !scope.currentUser.hasActiveCareerProfile
                        );
                    },
                });

                scope.$on('$destroy', () => {
                    $location.search('list', null);
                });

                // reset filters which also resets selectedFilterOptions
                scope.resetFilters = () => {
                    scope.filters = {
                        role: [],
                        places: [],
                        position_descriptors: [],
                        industry: [],
                        years_experience: [],
                    };
                };
                scope.resetFilters(); // immediately reset

                // Setup pagination
                scope.listLimit = 10; // used by all of the tabs
                scope.offset = 0; // used only by tabs where usesOpenPositionFilterSet is false
                const preloadedParams = {
                    // The serverLimit should be twice the value of the listLimit to ensure that the
                    // next page of profiles can be completely filled out if the user navigates to it.
                    serverLimit: scope.listLimit * 2,

                    // By making minLength a little larger than twice the listLimit, it ensures that the next page
                    // always has enough profiles to be filled, if possible. This means more frequent trips to the
                    // server for more profiles, but prevents the UI from attaching profiles to the bottom of the
                    // list in a jarring way and then suddenly updating the counter upon doing so.
                    minLength: scope.listLimit * 2 + 1,
                    offset: 0,
                };

                scope.createNewOpenPositionFilterSet = () => {
                    scope.openPositionFilterSet = new OpenPositionFilterSet(scope.filters, preloadedParams);
                    return scope.openPositionFilterSet.ensureResultsPreloaded().catch(() => {
                        throw new Error('Error creating new open position filter set');
                    });
                };

                // getZipRecruiterJobs is an ES6 module, while
                // this file is not. As such, we need a wrapper function
                // so we can properly mock this out in specs.
                scope._getZipRecruiterJobs = () => {
                    return getZipRecruiterJobs(injector, _.clone(scope.filters))
                        .then(response => {
                            scope.externalPositions = response;
                        })
                        .catch(err => {
                            $injector
                                .get('ErrorLogService')
                                .notifyInProd('Error fetching Zip Recruiter Jobs', err.message, {
                                    extra: {
                                        filters: scope.filters,
                                    },
                                });
                            scope.externalPositions = [];
                        })
                        .finally(() => {
                            // getZipRecruiterJobs returns a native Promise,
                            // so we need to safeApply here.
                            safeApply(scope);
                        });
                };

                scope.mergeInternalAndExternalPositions = () => {
                    const internalPositions = scope.openPositionFilterSet?.results || [];
                    const externalPositions = scope.externalPositions || [];

                    const openTabConfig = scope.tabConfigs.open;

                    const recommendedInternalPositions = internalPositions.filter(position => position.recommended);
                    const nonRecommendedInternalPositions = internalPositions.filter(position => !position.recommended);

                    // When we have no selected roles, we want to prioritize external
                    // positions over non-recommended internal positions. This is
                    // because we use the user's `primary_areas_of_interest` to fetch
                    // external jobs, so they're likely to be more relevant than non-recommended
                    // internal positions when we don't have filters selected.
                    if (_.isEmpty(scope.filters.role)) {
                        openTabConfig.initialPositions = [
                            ...recommendedInternalPositions,
                            ...externalPositions,
                            ...nonRecommendedInternalPositions,
                        ];
                    }
                    // When we have selected roles, we want to prioritize all internal
                    // positions over external positions
                    else {
                        openTabConfig.initialPositions = [...internalPositions, ...externalPositions];
                    }

                    // These positions are not going to be filtered (see
                    // applyFilters below), so filteredPositions is just the
                    // same as initialPositions
                    openTabConfig.filteredPositions = openTabConfig.initialPositions;
                };

                scope.fetchInternalAndExternalPositions = () => {
                    const openTabConfig = scope.tabConfigs.open;
                    openTabConfig.loading = true;
                    openTabConfig.initialPositions = null;
                    openTabConfig.filteredPositions = null;

                    scope.updateTabNum('open');

                    // prettier-ignore
                    scope.createNewOpenPositionFilterSet()
                        .then(() => scope._getZipRecruiterJobs())
                        .finally(() => {
                            // Reset pagination in case we got here by
                            // applying filters
                            scope.offset = 0;
                            openTabConfig.loading = false;

                            scope.mergeInternalAndExternalPositions();
                        });
                };

                scope.applyFilters = () => {
                    if (scope.currentTabConfig.usesOpenPositionFilterSet) {
                        scope.fetchInternalAndExternalPositions();
                    } else {
                        scope.filterClientPaginatedPositions();
                    }
                };

                /**
                 * Sets or increments the display value for a given tabConfig
                 *
                 * @param {*} tabName
                 * @param {*} newValue - The new value to set the tab number to, or nothing for an ellipsis.
                 */
                scope.updateTabNum = (tabName, newValue) => {
                    const tabConfig = scope.tabConfigs[tabName];

                    if (!tabConfig) {
                        return;
                    }

                    if (angular.isDefined(newValue)) {
                        tabConfig.translate.values.num = newValue;

                        // Check if we need to display the "100+" style text, which is applicable
                        // to a filterSet using the maxTotalCount strategy.
                        if (
                            tabConfig.usesOpenPositionFilterSet &&
                            tabConfig.translate.values.num === scope.openPositionFilterSet?.maxTotalCount
                        ) {
                            tabConfig.translate.values.num += '+';
                        }
                    } else {
                        tabConfig.translate.values.num = '…';
                    }
                };

                // Setup initial tabConfigs
                scope.tabConfigs = {
                    recommended: {
                        name: 'recommended',
                        class: 'positions-recommended',
                        translate: {
                            candidate: 'careers.featured_positions.recommended',
                            values: {
                                num: '…',
                            },
                        },
                        loading: false,
                        initialPositions: [],
                        filteredPositions: [],
                    },
                    open: {
                        name: 'open',
                        class: 'positions-all',
                        translate: {
                            candidate: 'careers.featured_positions.all',
                            values: {
                                num: '…',
                            },
                        },
                        usesOpenPositionFilterSet: true,
                        loading: false,
                        initialPositions: [],
                        filteredPositions: [],
                    },
                    accepted: {
                        name: 'accepted',
                        class: 'positions-underReview',
                        translate: {
                            candidate: 'careers.featured_positions.under_review',
                            values: {
                                num: '…',
                            },
                        },
                        loading: false,
                        initialPositions: [],
                        filteredPositions: [],
                    },
                    connected: {
                        name: 'connected',
                        class: 'positions-connected',
                        translate: {
                            candidate: 'careers.featured_positions.connected',
                            values: {
                                num: '…',
                            },
                        },
                        loading: false,
                        initialPositions: [],
                        filteredPositions: [],
                    },
                    rejected: {
                        name: 'rejected',
                        class: 'positions-closed',
                        translate: {
                            candidate: 'careers.featured_positions.closed',
                            values: {
                                num: '…',
                            },
                        },
                        loading: false,
                        initialPositions: [],
                        filteredPositions: [],
                    },
                };

                // generate the positionTabs that get passed along to the tabs directive
                scope.positionTabs = _.map(scope.tabConfigs, tabConfig => ({
                    name: tabConfig.name,
                    class: tabConfig.class,
                    translate: tabConfig.translate,
                }));

                /**
                 * currentList/Tabs
                 */
                const tabKeys = Object.keys(scope.tabConfigs);

                Object.defineProperty(scope, 'currentTab', {
                    get() {
                        // Derive currentTab from the 'list' query param. If it's not a valid tab,
                        // fallback to the 'open' tab and reset the 'list' query param in the URL accordingly.
                        let requestedTab = $location.search().list;
                        if (!_.contains(tabKeys, requestedTab)) {
                            requestedTab = 'recommended';
                            $location.search('list', requestedTab); // named 'list' for legacy purposes
                        }
                        return requestedTab;
                    },
                    set(newTab) {
                        // currentTab is derived from the 'list' query param, so when currentTab gets set,
                        // just update the 'list' query param in the URL. Also, replace the history so the
                        // candidate doesn't have to sift through all of the different tabs that they
                        // visited on the Featured Positions page when they press the browser's back button.
                        $location.search('list', newTab).replace(); // named 'list' for legacy purposes
                    },
                });

                Object.defineProperty(scope, 'currentTabConfig', {
                    get() {
                        return scope.tabConfigs[scope.currentTab] || {};
                    },
                });

                Object.defineProperty(scope, 'activePositions', {
                    get() {
                        return scope.currentTabConfig.filteredPositions || [];
                    },
                    configurable: true,
                });

                Object.defineProperty(scope, 'totalCount', {
                    get() {
                        // If the total_count from the server was the min then use that min with a plus sign.
                        // If the total_count from the server was not the min then use the current totalCount stored
                        //  on the openPositionFilterSet.
                        // If not using the filter set then simply return the length of the activePositions array.
                        if (
                            scope.currentTabConfig.usesOpenPositionFilterSet &&
                            scope.openPositionFilterSet.initialTotalCountIsMin
                        ) {
                            return `${scope.openPositionFilterSet.initialTotalCount}+`;
                        }

                        if (
                            scope.currentTabConfig.usesOpenPositionFilterSet &&
                            !scope.openPositionFilterSet.initialTotalCountIsMin
                        ) {
                            return (
                                (scope.openPositionFilterSet?.totalCount || 0) + (scope.externalPositions?.length || 0)
                            );
                        }

                        return scope.activePositions.length;
                    },
                });

                Object.defineProperty(scope, 'noMoreAvailable', {
                    get() {
                        if (scope.currentTabConfig.usesOpenPositionFilterSet) {
                            return scope.openPositionFilterSet.noMoreAvailable;
                        }
                        return true;
                    },
                });

                Object.defineProperty(scope, 'noPositions', {
                    get() {
                        if (scope.currentTabConfig.usesOpenPositionFilterSet) {
                            return (
                                scope.openPositionFilterSet?.initialTotalCount === 0 &&
                                (!scope.externalPositions || scope.externalPositions.length === 0)
                            );
                        }
                        return scope.activePositions.length === 0;
                    },
                });

                Object.defineProperty(scope, 'hasFilters', {
                    get() {
                        return _.chain(scope.filters).values().flatten().any().value();
                    },
                    configurable: true,
                });

                // if the user performs enough actions on the last page so that no more positions are visible,
                // decrement the offset to automatically navigate the user back a page
                scope.$watch(
                    () => scope.offset === scope.activePositions.length,
                    val => {
                        // only get the previous page of positions if there are any
                        if (val && scope.activePositions.length > 0) {
                            scope.getOpenPositionsRelativeToOffset(scope.offset, 'previous', scope.listLimit);
                        }
                    },
                );

                // Ensure that we fetch more results if enough positions are liked or rejected such that
                // we need to load more
                scope.$watch('openPositionFilterSet.results.length', () => {
                    if (!scope.openPositionFilterSet) {
                        return;
                    }

                    scope.openPositionFilterSet.ensureResultsPreloaded();
                });

                scope.$watchGroup(['openPositionFilterSet.results', 'externalPositions'], () => {
                    scope.mergeInternalAndExternalPositions();
                });

                scope.$watchGroup(
                    ['tabConfigs.open.loading', 'openPositionFilterSet.totalCount', 'externalPositions.length'],
                    () => {
                        if (!scope.tabConfigs.open.loading) {
                            // Note that we can't actually use allOpenPositions for a count here, since
                            // it won't contain any server paginated OpenPositions
                            const totalInternalPositions = scope.openPositionFilterSet?.totalCount || 0;
                            const totalExternalPositions = scope.externalPositions?.length || 0;
                            scope.updateTabNum('open', totalInternalPositions + totalExternalPositions);
                        }
                    },
                );

                scope.$watch('currentTab', (newTab, oldTab) => {
                    if (!oldTab || newTab === oldTab || !scope.currentUser.hasActiveCareerProfile) {
                        return;
                    }

                    // Reset the offset when switching tabs
                    scope.setOffset(scope.offset, 'first', preloadedParams.listLimit);

                    // Reset the filters when switching tabs if any were set
                    if (scope.hasFilters) {
                        scope.resetFilters();

                        // If the tab being switched from is still loading then we don't need to
                        // do any resetting to it
                        const oldTabConfig = scope.tabConfigs[oldTab];
                        if (!oldTabConfig.loading) {
                            if (oldTabConfig.usesOpenPositionFilterSet) {
                                scope.fetchInternalAndExternalPositions();
                            } else {
                                oldTabConfig.filteredPositions = oldTabConfig.initialPositions;
                                scope.updateTabNum(oldTab, scope.tabConfigs[oldTab].initialPositions.length);
                            }
                        }
                    }
                });

                // If there are no recommended positions then remove the recommended
                // tab and forward the user to the open tab if necessary
                scope.$watchGroup(
                    ['tabConfigs.recommended.initialPositions.length', 'tabConfigs.recommended.loading'],
                    values => {
                        const length = values[0];
                        const loading = values[1];
                        if (!loading && length === 0) {
                            scope.positionTabs = _.reject(scope.positionTabs, tab => tab.name === 'recommended');
                            if (scope.currentTab === 'recommended') {
                                scope.currentTab = 'open';
                            }
                        }
                    },
                );

                // We decided to only set recommended_positions_seen_ids when an email notification is sent, but it is still
                // a possibility that in the future we set it when viewing featured positions. So let's
                // just leave this here commented out for now.
                // scope.$watchGroup(['currentTab', 'offset', 'tabConfigs.recommended.loading'], () => {
                //     if (!scope.currentUser.ghostMode && scope.currentTab === 'recommended') {
                //         scope.currentUser.recommended_positions_seen_ids = scope.currentUser.recommended_positions_seen_ids || [];

                //         const currentlyViewingIds = scope.currentTabConfig.filteredPositions
                //             .slice(scope.offset, scope.offset + scope.listLimit)
                //             .map(position => position.id);
                //         const alreadySeenIds = scope.currentUser.recommended_positions_seen_ids;
                //         const missingIds = _.difference(currentlyViewingIds, alreadySeenIds);

                //         if (missingIds.length > 0) {
                //             scope.currentUser.recommended_positions_seen_ids = alreadySeenIds.concat(missingIds);
                //             scope.currentUser.save();
                //         }
                //     }
                // });

                scope.$watch(
                    'filters',
                    (newFilters, oldFilters) => {
                        // If no currentTab set or user does not have an active career profile then don't
                        // try to set or apply filters
                        if (!scope.currentTabConfig || !scope.currentUser.hasActiveCareerProfile) {
                            return;
                        }

                        scope.setSelectedFilterOptions();

                        if (angular.isDefined(newFilters) && !_.isEqual(newFilters, oldFilters)) {
                            scope.applyFilters();
                        }
                    },
                    true,
                );

                scope.setOffset = (offset, action, listLimit) => {
                    scope.offset = listOffsetHelper.setOffset(offset, action, listLimit);
                };

                scope.getOpenPositionsRelativeToOffset = (offset, action, listLimit) => {
                    scope.offset = listOffsetHelper.setOffset(offset, action, listLimit);

                    if (scope.currentTabConfig.usesOpenPositionFilterSet) {
                        scope.openPositionFilterSet.getResultsRelativeToOffset(offset, action, listLimit);
                    }
                };

                scope.initializeClientPaginatedList = (list, key) => {
                    const tabConfig = scope.tabConfigs[key];

                    // The user could have liked or hidden a position while the positions for the
                    // client paginated lists were being loaded, so be sure to use concat to catch
                    // this edge case.
                    tabConfig.initialPositions = tabConfig.initialPositions.concat(list);
                    tabConfig.filteredPositions = tabConfig.initialPositions;
                    scope.updateTabNum(key, tabConfig.initialPositions.length);
                    tabConfig.loading = false;
                };

                scope.interests = [];
                const careersNetworkViewModel = CareersNetworkViewModel.get('candidate');

                // Initial loading of all tabs
                if (scope.currentUser.hasActiveCareerProfile) {
                    // Make a call to grab the applicable recommended positions
                    scope.tabConfigs.recommended.loading = true;
                    careersNetworkViewModel.loadRecommendedPositions().then(recommendedPositions => {
                        scope.initializeClientPaginatedList(recommendedPositions, 'recommended');
                    });

                    // Make sure we get the open tab positions first
                    scope.fetchInternalAndExternalPositions();

                    // Set the clientPaginated tabs to loading
                    _.each(['accepted', 'connected', 'rejected'], tab => {
                        scope.tabConfigs[tab].loading = true;
                    });

                    // Grab the interests and hiring relationships in parallel, then get the
                    // open positions for non-paginated tabs, then assemble the non-paginated lists
                    $q.all([
                        careersNetworkViewModel.ensureCandidatePositionInterests(),
                        careersNetworkViewModel.ensureHiringRelationshipsLoaded('myCandidates'),
                        careersNetworkViewModel.ensureHiringRelationshipsLoaded('closed'),
                    ])
                        .then(responses => {
                            scope.interests = responses[0];

                            const openPositionIdsFromInterests = _.pluck(scope.interests, 'open_position_id');
                            const openPositionIdsFromRelationships = _.pluck(
                                careersNetworkViewModel.hiringRelationships,
                                'open_position_id',
                            );

                            return OpenPosition.index({
                                filters: {
                                    featured: true,
                                    archived: false,
                                    hiring_manager_might_be_interested_in: scope.currentUser.id,
                                    candidate_has_acted_on: true,
                                    id: _.chain(openPositionIdsFromInterests.concat(openPositionIdsFromRelationships))
                                        .compact()
                                        .uniq()
                                        .value(),
                                },
                            });
                        })
                        .then(response =>
                            createClientPaginatedLists(response.result, scope.interests, careersNetworkViewModel),
                        )
                        .then(clientPaginatedLists => {
                            _.each(clientPaginatedLists, (list, key) => scope.initializeClientPaginatedList(list, key));
                        })
                        .catch(e => {
                            throw e;
                        });
                }

                /**
                 *
                 * @param {*} positionsFromInterestsOrRelationships
                 * @param {*} interests
                 * @param {*} _careersNetworkViewModel
                 *
                 * @returns Object containing a list of accepted, connected, and rejected positions
                 */
                function createClientPaginatedLists(
                    positionsFromInterestsOrRelationships,
                    interests,
                    _careersNetworkViewModel,
                ) {
                    // Attach an interest if one exists to each position for efficient grouping below
                    const interestsByOpenPositionId = _.indexBy(interests, 'open_position_id');
                    _.each(positionsFromInterestsOrRelationships, position => {
                        position.$interest = interestsByOpenPositionId[position.id];
                    });

                    // make three groups of positions: open, liked, and rejected
                    // - open and rejected are available to scope.currentTab/scope.tabConfigs
                    // - liked gets split into two more groups below
                    const positionLists = _.groupBy(positionsFromInterestsOrRelationships, position => {
                        const interest = position.$interest;
                        if (interest) {
                            if (interest.candidate_status === 'accepted' && !interest.hiring_manager_rejected) {
                                return 'liked';
                            }
                            if (interest.candidate_status === 'accepted' && interest.hiring_manager_rejected) {
                                position.$closedByEmployer = true;
                                return 'rejected';
                            }
                            if (interest.candidate_status === 'rejected') {
                                return 'rejected';
                            }
                        } else {
                            // Note: There shouldn't be any "open" ones passed to this function anymore since those
                            // are now server paginated
                            return 'open';
                        }
                    });

                    const acceptedHiringRelationshipPromises = [];

                    (positionLists.liked || []).forEach(position => {
                        acceptedHiringRelationshipPromises.push(
                            _careersNetworkViewModel.getMyHiringRelationshipViewModelForConnectionId(
                                position.hiring_manager_id,
                            ),
                        );
                    });

                    // NOTE: this would get more complicated if hiring managers could see teammates' positions
                    return $q.all(acceptedHiringRelationshipPromises).then(hiringRelationshipViewModels => {
                        // get rid of undefined relationships so findWhere doesn't iterate over them uneccessarily
                        hiringRelationshipViewModels = _.filter(
                            hiringRelationshipViewModels,
                            vm => vm && vm.matched === true,
                        );

                        // Attach a hiringRelationshipViewModel if one exists to each position for efficient grouping below
                        const hiringRelationshipViewModelsByConnectionId = _.indexBy(
                            hiringRelationshipViewModels,
                            'connectionId',
                        );
                        _.each(positionLists.liked, position => {
                            position.$hiringRelationshipViewModel =
                                hiringRelationshipViewModelsByConnectionId[position.hiring_manager_id];
                        });

                        // split liked into two groups: those you have connections with, and those you don't
                        // - connected and accepted are available to scope.currentTab/scope.tabConfigs
                        const likedPositionLists = _.groupBy(positionLists.liked, position => {
                            const hrvm = position.$hiringRelationshipViewModel;
                            if (hrvm) {
                                // If the hiring relationship that was attached is not for this specific position
                                // then hide the position. This scenario can happen if a candidate likes multiple
                                // positions from a hiring manager then that hiring manager creates
                                // a relationship with the candidate for one of the positions.
                                if (hrvm.openPositionId !== position.id) {
                                    return 'hidden';
                                }
                                if (hrvm.connectionId === position.hiring_manager_id && hrvm.closedByMe) {
                                    position.$closedByMe = true;
                                    return 'rejected';
                                }
                                if (hrvm.connectionId === position.hiring_manager_id && hrvm.closedByConnection) {
                                    position.$closedByEmployer = true;
                                    return 'rejected';
                                }
                                if (hrvm.connectionId === position.hiring_manager_id) {
                                    return 'connected';
                                }
                            } else {
                                return 'accepted';
                            }
                        });

                        return {
                            accepted: likedPositionLists.accepted || [],
                            connected: likedPositionLists.connected || [],
                            rejected: (positionLists.rejected || []).concat(likedPositionLists.rejected || []),
                        };
                    });
                }

                scope.yearsExperienceKeys = $injector.get('CAREERS_YEARS_EXPERIENCE_KEYS');
                scope.typeOptions = $injector.get('CAREERS_POSITION_DESCRIPTOR_KEYS');

                // this relies on the values in yearsExperienceKeys to be in the following format: 0_1_years
                // if that ever changes, this will need to be revisited
                const yearsExperienceLookup = {};
                scope.yearsExperienceKeys.forEach(key => {
                    const values = key.split('_');
                    const start = parseInt(values[0], 10);
                    const stop = values[1] === 'plus' ? 10 : parseInt(values[1], 10);
                    yearsExperienceLookup[key] = _.range(start, stop + 1); // range ends one less that the stop number
                });

                scope.filterClientPaginatedPositions = () => {
                    // reset pagination
                    scope.offset = 0;

                    scope.setSelectedFilterOptions();

                    // assign new object so we can manipulate it from here before we use it
                    const proxyFilters = _.extend({}, scope.filters);

                    // add child roles when parent is selected
                    if (proxyFilters.role) {
                        proxyFilters.role.forEach(role => {
                            if (roleKeys[role]) {
                                proxyFilters.role = roleKeys[role].concat(proxyFilters.role);
                            }
                        });
                    }

                    // truly empty the filters because just setting a value of '' isn't enough
                    _.each(proxyFilters, (value, key) => {
                        if (_.isEmpty(value)) {
                            delete proxyFilters[key];
                        }
                    });

                    // finally filter
                    if (scope.currentTabConfig.initialPositions.length > 0) {
                        scope.currentTabConfig.filteredPositions = $filter('filter')(
                            scope.currentTabConfig.initialPositions,
                            position =>
                                _.keys(proxyFilters).every(filter => {
                                    // if filters are present, it will exit once the predicate callback returns false; true otherwise
                                    if (filter === 'years_experience') {
                                        const desiredYearsExperience = _.range(
                                            position.desired_years_experience.min || 0,
                                            (position.desired_years_experience.max || 10) + 1,
                                        );
                                        const yearsExperienceArray = _.chain(
                                            _.map(proxyFilters.years_experience, key => yearsExperienceLookup[key]),
                                        )
                                            .flatten()
                                            .uniq()
                                            .value();
                                        return _.intersection(desiredYearsExperience, yearsExperienceArray).length > 0;
                                    }
                                    if (filter === 'industry') {
                                        // industry is a nested property
                                        return (
                                            position.hiring_application &&
                                            _.contains(proxyFilters[filter], position.hiring_application.industry)
                                        );
                                    }
                                    if (filter === 'places') {
                                        const isNearPlaceMap = _.map(
                                            proxyFilters[filter],
                                            place => position.distanceTo(place) <= 100,
                                        );
                                        return _.contains(isNearPlaceMap, true);
                                    }
                                    if (_.isArray(position[filter])) {
                                        // position.position_descriptors is an array
                                        return _.intersection(proxyFilters[filter], position[filter]).length > 0;
                                    }
                                    return _.contains(proxyFilters[filter], position[filter]);
                                }),
                        );

                        scope.updateTabNum(scope.currentTab, scope.currentTabConfig.filteredPositions.length);
                    }
                };

                /**
                 * Filtering
                 */
                const translationHelper = new TranslationHelper('careers.field_options');

                // parse filters into array of options
                scope.setSelectedFilterOptions = () => {
                    scope.selectedFilterOptions = [];

                    _.each(scope.filters, (value, key) => {
                        if (!_.isEmpty(value)) {
                            if (key === 'places') {
                                scope.selectedFilterOptions = scope.selectedFilterOptions.concat(
                                    _.map(value, option => ({
                                        value: option,
                                        label: HasLocation.locationString(option),
                                        filter: key,
                                    })),
                                );
                            } else {
                                scope.selectedFilterOptions = scope.selectedFilterOptions.concat(
                                    _.map(value, option => ({
                                        value: option,
                                        label: translationHelper.get(option),
                                        filter: key,
                                    })),
                                );
                            }
                        }
                    });
                };

                // remove a single option
                scope.removeSelectedFilterOption = option => {
                    // remove from selectedFilterOptions
                    const optionIndex = scope.selectedFilterOptions.indexOf(option);
                    scope.selectedFilterOptions.splice(optionIndex, 1);

                    // remove from filters
                    const filterIndex = scope.filters[option.filter].indexOf(option.value);
                    scope.filters[option.filter].splice(filterIndex, 1);
                };

                /**
                 * Moving cards from one list to another
                 */
                scope.removeCard = ($index, status, coverLetter) => {
                    // get the index of the position for the list of filtered positions
                    const filteredPositionsIndex = $index + scope.offset;

                    // cache the position
                    const position = scope.currentTabConfig.filteredPositions[filteredPositionsIndex];

                    // cache the interest values
                    const interestAttrs = {
                        candidate_id: careersNetworkViewModel.user.id,
                        open_position_id: position.id,
                    };

                    // check to see if this interest exists or not
                    let interest = _.findWhere(scope.interests, interestAttrs);
                    if (!interest) {
                        if (coverLetter) {
                            interestAttrs.cover_letter = coverLetter;
                        }
                        interest = CandidatePositionInterest.new(interestAttrs);
                        scope.interests.push(interest);
                        position.$interest = interest;
                    } else if (coverLetter) {
                        interest.cover_letter = coverLetter;
                    }
                    interest.candidate_status = status;
                    careersNetworkViewModel.saveCandidatePositionInterest(interest);

                    // if there's a list to add to, add to it
                    if (scope.tabConfigs[status].initialPositions) {
                        scope.tabConfigs[status].initialPositions.push(position);
                    } else {
                        // if there's no list, create it with this single item
                        scope.tabConfigs[status].initialPositions = [position];
                    }

                    // Remove the position from current tab of filtered positions
                    scope.currentTabConfig.filteredPositions.splice(filteredPositionsIndex, 1);

                    // Remove the position from the master list of unfiltered positions for the current tab
                    const unfilteredPositionsIndex = _.indexOf(scope.currentTabConfig.initialPositions, position);
                    if (unfilteredPositionsIndex !== -1) {
                        scope.currentTabConfig.initialPositions.splice(unfilteredPositionsIndex, 1);
                    }

                    // If the user is on the recommended tab then also remove the position from the
                    // open tab since we load recommended positions there as well, and vice versa.
                    if (scope.currentTabConfig.name === 'recommended') {
                        const openPositionIndex = _.findIndex(
                            scope.tabConfigs.open.initialPositions,
                            p => p.id === position.id,
                        );
                        scope.tabConfigs.open.initialPositions.splice(openPositionIndex, 1);
                    } else if (scope.currentTabConfig.name === 'open' && position.recommended) {
                        const recommendedPositionIndex = _.findIndex(
                            scope.tabConfigs.recommended.initialPositions,
                            p => p.id === position.id,
                        );
                        scope.tabConfigs.recommended.initialPositions.splice(recommendedPositionIndex, 1);
                        scope.updateTabNum('recommended', scope.tabConfigs.recommended.initialPositions.length);
                    }

                    // Update the tab number in the new tab
                    // Note: If the new tab uses the filter set then we need to check if
                    // the initialTotalCountIsMin to know if we should increment the totalCount or just
                    // leave it as maxTotalCount with a plus sign. Ditto for the current tab logic below.
                    const newTabConfig = scope.tabConfigs[status];
                    if (newTabConfig.usesOpenPositionFilterSet && !scope.openPositionFilterSet.initialTotalCountIsMin) {
                        scope.openPositionFilterSet.totalCount += 1;
                        scope.updateTabNum(status, scope.openPositionFilterSet.totalCount);
                    } else if (!newTabConfig.usesOpenPositionFilterSet) {
                        scope.updateTabNum(status, scope.tabConfigs[status].initialPositions.length);
                    }

                    // Update the tab number in the current filtered tab
                    if (
                        scope.currentTabConfig.usesOpenPositionFilterSet &&
                        !scope.openPositionFilterSet.initialTotalCountIsMin
                    ) {
                        scope.openPositionFilterSet.totalCount -= 1;
                        scope.updateTabNum(scope.currentTab, scope.openPositionFilterSet.totalCount);
                    } else if (!scope.currentTabConfig.usesOpenPositionFilterSet) {
                        scope.updateTabNum(scope.currentTab, scope.currentTabConfig.filteredPositions.length);
                    }
                };

                scope.toggleFilters = function () {
                    scope.filtersOpen = !scope.filtersOpen;
                };

                /**
                 * Open a connection
                 */
                scope.openConnection = connectionId => {
                    $location.url(`/careers/connections?connectionId=${connectionId}`);
                };

                scope.shouldShowRecommendedBadge = position =>
                    position.recommended && (scope.currentTab === 'recommended' || scope.currentTab === 'open');

                /**
                 * Candidate Action Buttons
                 */
                scope.apply = ($index, position) => {
                    if (position.hasInstantApply) {
                        DialogModal.alert({
                            title: new TranslationHelper('careers.featured_positions').get('apply_now'),
                            content:
                                '<cover-letter cover-letter="coverLetter" submit="submit" hiring-application="hiringApplication"></cover-letter><position-card position="position"></position-card>',
                            scope: {
                                coverLetter: position.$interest && position.$interest.cover_letter,
                                position,
                                hiringApplication: position.hiring_application,
                                submit(coverLetter) {
                                    scope.removeCard($index, 'accepted', coverLetter);
                                },
                            },
                            size: 'medium-large',
                            classes: ['cover-letter'],
                        });
                    } else if (position.url) {
                        EventLogger.allowEmptyLabel('candidate:clicked_external_position_url');
                        EventLogger.log('candidate:clicked_external_position_url', {
                            external_position_id: position?.id,
                            external_position_url: position?.url,
                            external_position_title: position?.title,
                            external_position_industry: position?.industry_name,
                            external_position_location: position?.location,
                            external_position_company_name: position?.hiring_application?.company_name,
                        });
                        $window.open(position.url);
                    }
                };

                scope.viewCoverLetter = position => {
                    DialogModal.alert({
                        title: new TranslationHelper('careers.featured_positions').get('cover_letter'),
                        content:
                            '<cover-letter cover-letter="coverLetter"></cover-letter><position-card position="position"></position-card>',
                        scope: {
                            position,
                            coverLetter: position.$interest.cover_letter,
                        },
                        size: 'medium-large',
                        classes: ['cover-letter'],
                    });
                };
            },
        };
    },
]);
