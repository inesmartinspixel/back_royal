import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/candidate_list_actions.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('candidateListActions', [
    '$injector',
    function factory($injector) {
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
        const EventLogger = $injector.get('EventLogger');
        const $location = $injector.get('$location');
        const $rootScope = $injector.get('$rootScope');
        const HiringRelationshipViewModel = $injector.get('HiringRelationshipViewModel');
        const CandidatePositionInterest = $injector.get('CandidatePositionInterest');
        const CareerProfile = $injector.get('CareerProfile');
        const dateHelper = $injector.get('dateHelper');

        EventLogger.allowEmptyLabel([
            'candidate_list_card:clicked_share_link',
            'candidate_list_card:clicked_full_profile',
        ]);

        return {
            restrict: 'E',
            scope: {
                ngModel: '<',
                removeItem: '&',
                showHideButton: '<?',
                getCandidatePositionInterest: '&?',
                showNewStatus: '<?',
                disableActions: '<?',
                adminMode: '<?',
            },
            templateUrl,

            link(scope) {
                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.dateHelper = dateHelper;

                // some things require a Career Profile
                if (scope.ngModel.isA(HiringRelationshipViewModel)) {
                    scope.hiringRelationshipViewModel = scope.ngModel;
                    scope.careerProfile = scope.hiringRelationshipViewModel.hiringRelationship.career_profile;

                    // get teammate information if it exists
                    if (scope.hiringRelationshipViewModel.teammateNames) {
                        scope.teammates = _.map(scope.hiringRelationshipViewModel.teammateNames, (name, index) => ({
                            avatar: scope.hiringRelationshipViewModel.teammateAvatarSrcs[index],
                            name,
                        }));
                    } else if (scope.hiringRelationshipViewModel.role === 'hiringTeammate') {
                        scope.teammates = [
                            {
                                avatar: scope.hiringRelationshipViewModel.teammateAvatarSrc,
                                name: scope.hiringRelationshipViewModel.hiringRelationship.hiring_manager_display_name,
                            },
                        ];
                    }
                } else if (scope.ngModel.isA(CareerProfile)) {
                    scope.careerProfile = scope.ngModel;
                } else if (scope.ngModel.isA(CandidatePositionInterest)) {
                    scope.careerProfile = scope.ngModel.career_profile;
                    if (
                        scope.ngModel.hiring_manager_reviewer_name &&
                        scope.ngModel.hiring_manager_reviewer_name !== scope.currentUser.name
                    ) {
                        scope.teammates = [
                            {
                                avatar: scope.ngModel.hiring_manager_reviewer_avatar_url,
                                name: scope.ngModel.hiring_manager_reviewer_name,
                            },
                        ];
                    }
                } else {
                    throw 'Unsupported ngModel class';
                }

                // put on scope for testing
                scope.careersNetworkViewModel = CareersNetworkViewModel.get('hiringManager');

                // Use either the ngModel (if it is a CandidatePositionInterest) or the supplied callback
                // to get a corresponding interest, if it exists.
                if (scope.ngModel.isA(CandidatePositionInterest)) {
                    scope.candidatePositionInterest = scope.ngModel;
                } else {
                    scope.candidatePositionInterest = scope.getCandidatePositionInterest
                        ? scope.getCandidatePositionInterest({
                              $$careerProfile: scope.careerProfile,
                          })
                        : null;
                }

                // new interest/match
                scope.showNewStatus = angular.isDefined(scope.showNewStatus) ? scope.showNewStatus : true;
                scope.newMessage =
                    scope.hiringRelationshipViewModel &&
                    scope.hiringRelationshipViewModel.hasUnread &&
                    !scope.hiringRelationshipViewModel.newMatch &&
                    !scope.hiringRelationshipViewModel.newRequest;
                scope.new =
                    scope.showNewStatus &&
                    ((scope.candidatePositionInterest && scope.candidatePositionInterest.$new) ||
                        (scope.hiringRelationshipViewModel &&
                            (scope.hiringRelationshipViewModel.newMatch || scope.newMessage)));
                scope.newKey = scope.hiringRelationshipViewModel
                    ? scope.newMessage
                        ? 'new_message'
                        : 'new_match'
                    : 'new';

                // outstanding interest
                scope.outstanding =
                    scope.candidatePositionInterest && scope.candidatePositionInterest.admin_status === 'outstanding';

                if (scope.candidatePositionInterest) {
                    scope.inviteKey = 'connect_now';
                } else {
                    scope.inviteKey = 'invite_to_connect';
                }

                scope.careersNetworkViewModel
                    .getAvailableActionsWithEverythingPreloaded(scope.careerProfile)
                    .then(showActions => {
                        scope.showInvite = showActions.invite;
                        scope.showSave = showActions.save;
                        scope.showLike = showActions.like && !scope.candidatePositionInterest;
                        scope.showHide = showActions.pass && !interestStatus;
                        scope.showPass =
                            scope.showHideButton &&
                            _.contains(['unseen', 'pending', 'saved_for_later'], interestStatus);
                    });

                var interestStatus =
                    scope.candidatePositionInterest && scope.candidatePositionInterest.hiring_manager_status;

                scope.showBadge =
                    _.contains(['invited', 'accepted'], interestStatus) ||
                    (scope.hiringRelationshipViewModel &&
                        scope.hiringRelationshipViewModel.hiringRelationship.hiring_manager_status === 'accepted');
                scope.badgeKey =
                    interestStatus === 'invited' ||
                    (scope.hiringRelationshipViewModel && scope.hiringRelationshipViewModel.invitedPosition)
                        ? 'position'
                        : 'general_interest';

                // if there's an interest status that is one of these, or if there is not but there's no badge, show the share button
                scope.showShare =
                    _.contains(['unseen', 'pending', 'saved_for_later', 'rejected'], interestStatus) ||
                    (!scope.showBadge && !scope.candidatePositionInterest);

                // in certain conditions, show a button to open the conversation
                scope.showOpen =
                    _.contains(['invited', 'accepted'], interestStatus) ||
                    (scope.hiringRelationshipViewModel && scope.hiringRelationshipViewModel.matched);
                scope.openKey = interestStatus ? 'open_in_tracker' : 'open';

                scope.showShare =
                    _.contains(['pending', 'saved_for_later', 'rejected'], interestStatus) ||
                    (!scope.showBadge && !scope.candidatePositionInterest);

                // get the positionTitle from the ngModel depending on what type it is
                scope.$watch('ngModel', ngModel => {
                    if (scope.ngModel.isA(HiringRelationshipViewModel)) {
                        scope.positionTitle =
                            scope.hiringRelationshipViewModel.invitedPosition &&
                            scope.hiringRelationshipViewModel.invitedPosition.title;
                    } else if (scope.ngModel.isA(CareerProfile)) {
                        scope.careersNetworkViewModel
                            .getOrInitializeHiringRelationshipViewModelForCareerProfile(ngModel)
                            .then(hiringRelationshipViewModel => {
                                scope.positionTitle =
                                    hiringRelationshipViewModel.invitedPosition &&
                                    hiringRelationshipViewModel.invitedPosition.title;
                            });
                    } else if (scope.ngModel.isA(CandidatePositionInterest)) {
                        scope.careersNetworkViewModel
                            .getOrInitializeHiringRelationshipViewModelForCareerProfile(scope.careerProfile)
                            .then(hiringRelationshipViewModel => {
                                scope.positionTitle =
                                    hiringRelationshipViewModel.invitedPosition &&
                                    hiringRelationshipViewModel.invitedPosition.title;
                            });
                    } else {
                        throw 'Unsupported ngModel class';
                    }
                });

                /**
                 * Actions
                 */
                scope.openConnection = () => {
                    if (scope.disableActions) {
                        return;
                    }

                    $location.search('positionId', null);
                    $location.search('action', null);
                    scope.careersNetworkViewModel
                        .getOrInitializeHiringRelationshipViewModelForCareerProfile(scope.careerProfile)
                        .then(hiringRelationshipViewModel => {
                            hiringRelationshipViewModel.openConnection();
                        });
                };

                scope.openInviteModal = () => {
                    if (
                        scope.disableActions ||
                        scope.careersNetworkViewModel.showHiringBillingModalOnAction(
                            scope.careerProfile,
                            'openInviteModal',
                        )
                    ) {
                        return;
                    }

                    scope.careersNetworkViewModel.openInviteModal(
                        scope.careerProfile,
                        {
                            candidatePositionInterest: scope.candidatePositionInterest,
                        },
                        () => {
                            scope.onStatusChange();
                        },
                    );
                };

                scope.onStatusChange = () => {
                    scope.removeItem({
                        $$item: scope.ngModel,
                    });
                };

                scope.logEvent = (name, payload) => {
                    EventLogger.log(
                        name,
                        angular.extend(payload || {}, {
                            candidate_id: scope.careerProfile.user_id,
                        }),
                    );
                };

                scope.viewFullProfile = () => {
                    if (
                        scope.disableActions ||
                        scope.careersNetworkViewModel.showHiringBillingModalOnAction(
                            scope.careerProfile,
                            'viewFullProfile',
                        )
                    ) {
                        return;
                    }

                    scope.logEvent('candidate_list_card:clicked_full_profile');
                    scope.careersNetworkViewModel.showFullProfile({
                        showInvite: scope.showInvite && !scope.currentUser.hasAdminAccess,
                        showActions:
                            (scope.showSave || scope.showLike || scope.showHide || scope.showPass) &&
                            !scope.currentUser.hasAdminAccess,
                        showHideButton: scope.showHideButton,
                        careerProfile: scope.careerProfile,
                        onFinish() {
                            scope.onStatusChange();
                        },
                        candidatePositionInterest: scope.candidatePositionInterest
                            ? scope.candidatePositionInterest
                            : undefined,
                    });
                };

                scope.rejectInterest = () => {
                    if (
                        scope.disableActions ||
                        scope.careersNetworkViewModel.showHiringBillingModalOnAction(
                            scope.careerProfile,
                            'updateStatus:rejected',
                        )
                    ) {
                        return;
                    }

                    // We are rejecting a candidate within the /positions context, so
                    // we'll need access to the corresponding candidatePositionInterest
                    scope.candidatePositionInterest.hiring_manager_status = 'rejected';

                    // We are rejecting this candidate in the positions context only, so
                    // update the candidatePositionInterest since updating the hiringRelationship would
                    // mean rejecting them completely, rather than rejecting them just for this position.
                    scope.careersNetworkViewModel.saveCandidatePositionInterest(scope.candidatePositionInterest);

                    scope.onStatusChange();
                };

                //---------------------------
                // Admin Mode
                //---------------------------

                scope.adminModeViewModel = {
                    showSendToOutstanding: _.includes(['unreviewed', 'approved'], scope.adminMode),
                    showSendToApproved: _.includes(['unreviewed', 'outstanding'], scope.adminMode),
                    showSendToHidden: _.includes(['unreviewed', 'outstanding', 'approved'], scope.adminMode),
                    showSendToUnreviewed: scope.adminMode === 'hidden',
                    showPriorityInput: _.includes(['outstanding', 'approved'], scope.adminMode),
                    showCreatedAt: _.includes(['unreviewed', 'outstanding', 'approved'], scope.adminMode),
                    disableLaunchFullProfile: _.includes(['hidden', 'saved', 'connected', 'passed'], scope.adminMode),
                };

                scope.$watch(
                    'candidatePositionInterest.hiring_manager_priority',
                    val => {
                        if (Number.isInteger(val)) {
                            scope.tempInterestPriorityUserFacing = val + 1;
                        }
                    },
                    true,
                );

                scope.triggerInterestAdminAction = (action, priority) => {
                    scope.$emit('admin:interestAdminAction', {
                        action,
                        interest: scope.candidatePositionInterest,
                        group: scope.adminMode,
                        priority,
                    });
                };
            },
        };
    },
]);
