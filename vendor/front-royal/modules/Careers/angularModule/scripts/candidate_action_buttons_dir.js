import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/candidate_action_buttons.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('candidateActionButtons', [
    function factory() {
        return {
            restrict: 'E',
            scope: {
                // these properties can be set to
                // true  => button is enabled
                // false => button is hidden
                showHide: '<?',
                showApply: '<?',
                showView: '<?',
                hasInstantApply: '<?',

                // these properties are callbacks triggered when
                // each button is clicked
                apply: '&',
                hide: '&',
                viewCoverLetter: '&',
            },
            templateUrl,

            link(scope) {
                scope.applyLocale = scope.hasInstantApply ? 'apply_now' : 'learn_more';
            },
        };
    },
]);
