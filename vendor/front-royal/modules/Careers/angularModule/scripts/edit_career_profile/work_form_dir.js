import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_career_profile/work_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('workForm', [
    '$injector',
    function factory($injector) {
        const FormHelper = $injector.get('FormHelper');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
        const TranslationHelper = $injector.get('TranslationHelper');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                scope.helper = EditCareerProfileHelper;
                FormHelper.supportForm(scope, scope.work);

                scope.helper.logEventForApplication('viewed-work');

                function mapKeysToOptions(keys) {
                    return _.map(keys, key => ({
                        label: new TranslationHelper('careers.field_options').get(key),
                        value: key,
                    }));
                }

                scope.yearsExperienceOptions = mapKeysToOptions($injector.get('CAREERS_FULL_TIME_EXPERIENCE_KEYS'));
                scope.recentRolesOptions = mapKeysToOptions($injector.get('CAREERS_RECENT_ROLE_KEYS'));

                const workStep = _.findWhere(scope.steps, {
                    stepName: 'work',
                });
                // set required responsibilities to pass to work experience detail
                scope.requiredResponsibilities =
                    (workStep.requiredFields.fullTimeWorkExperiences.requiredFields.responsibilities &&
                        workStep.requiredFields.fullTimeWorkExperiences.requiredFields.responsibilities.min) ||
                    0;

                scope.proxy = {};

                scope.careerProfile.addDummyWorkExperienceIfNecessary();

                // deep watch the career profile for end_date changes on the work experiences so we can
                // change the featured work experience when appropriate.
                scope.$watch(
                    'careerProfile',
                    (newCareerProfile, oldCareerProfile) => {
                        const newEndDates = _.pluck(newCareerProfile.work_experiences, 'end_date');
                        const oldEndDates = _.pluck(oldCareerProfile.work_experiences, 'end_date');
                        if (!_.isEqual(newEndDates, oldEndDates)) {
                            /*
                                We want to force set the featured work experience ourselves if a new experience was NOT
                                added and 1 of the following 3 scenarios has occurred:

                                1) The user can't see the "Featured Work Experience" field to manually change it themselves,
                                    i.e. the number of current work experiences is less than 2.
                                2) The featured work experience has been made not current.
                                3) The "Featured Work Experience" field just became visible to the user, so they can
                                    manually set it themselves.

                                Basically, we want to be hands-on and actively setting the featured work experience in the
                                background if the user wasn't able to set it themselves before, and hands-off if the user
                                can see it.
                            */
                            const oldNullEndDates = _.filter(oldEndDates, endDate => endDate === null);
                            const currentFullTimeWorkExperiences = scope.careerProfile.currentFullTimeWorkExperiences;
                            if (
                                newEndDates.length <= oldEndDates.length &&
                                (currentFullTimeWorkExperiences.length < 2 ||
                                    (scope.proxy.featuredWorkExperience &&
                                        scope.proxy.featuredWorkExperience.end_date !== null) ||
                                    (oldNullEndDates.length === 1 && currentFullTimeWorkExperiences.length === 2))
                            ) {
                                setFeaturedWorkExperience();
                            }
                        }
                    },
                    true,
                );

                scope.$watch('proxy.featuredWorkExperience', handleProxyFeaturedWorkExperienceChange);

                function handleProxyFeaturedWorkExperienceChange() {
                    if (
                        !scope.proxy.featuredWorkExperience &&
                        scope.careerProfile.currentFullTimeWorkExperiences.length > 0 &&
                        scope.careerProfile.featuredWorkExperience &&
                        scope.careerProfile.featuredWorkExperience.end_date === null
                    ) {
                        // In the event that the career profile gets saved, proxy.featuredWorkExperience gets reset to undefined.
                        // In this instance we should be setting it to the currently featured work experience on the career profile.
                        scope.proxy.featuredWorkExperience = scope.careerProfile.featuredWorkExperience;
                    }
                    scope.careerProfile.featuredWorkExperience = scope.proxy.featuredWorkExperience;
                }

                setFeaturedWorkExperience(scope.careerProfile.featuredWorkExperience);

                // logic for setting featured work experience
                function setFeaturedWorkExperience(existingfeaturedWorkExperience) {
                    const currentFullTimeWorkExperiences = scope.careerProfile.currentFullTimeWorkExperiences;
                    if (existingfeaturedWorkExperience) {
                        scope.proxy.featuredWorkExperience = scope.careerProfile.featuredWorkExperience;
                    } else if (scope.careerProfile.work_experiences.length === 1) {
                        scope.proxy.featuredWorkExperience = _.first(scope.careerProfile.work_experiences);
                    } else if (currentFullTimeWorkExperiences.length > 0) {
                        // if the career profile has current work experiences, default the featured work experience
                        // to the current work experience with the most recent start_date
                        const sortedCurrentWorkExps = _.sortBy(
                            currentFullTimeWorkExperiences,
                            workExp => workExp.start_date,
                        );
                        scope.proxy.featuredWorkExperience = _.last(sortedCurrentWorkExps);
                    } else {
                        // if the career profile has no current work experiences, default the featured work
                        // experience to the work exp with the most recent end_date. If multiple work exps
                        // have the most recent end_date, default the featured work exp to the exp with the
                        // most recent start_date.
                        let sortedWorkExps = _.sortBy(
                            scope.careerProfile.work_experiences,
                            workExp => workExp.end_date,
                        );
                        const sameEndDateWorkExps = _.where(sortedWorkExps, {
                            end_date: sortedWorkExps.length > 0 && _.last(sortedWorkExps).end_date,
                        });
                        sortedWorkExps = _.sortBy(sameEndDateWorkExps, workExp => workExp.start_date);
                        scope.proxy.featuredWorkExperience = _.last(sortedWorkExps);
                    }
                }

                scope.moveWorkExperience = (workExperience, employmentType) => {
                    const index = scope.careerProfile.work_experiences.indexOf(workExperience);
                    scope.careerProfile.work_experiences.splice(index, 1);
                    workExperience.employment_type = employmentType === 'full_time' ? 'part_time' : 'full_time';
                    scope.careerProfile.work_experiences.push(workExperience);
                };
            },
        };
    },
]);
