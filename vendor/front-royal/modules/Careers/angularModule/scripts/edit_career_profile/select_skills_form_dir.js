import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_career_profile/select_skills_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('selectSkillsForm', [
    '$injector',
    function factory($injector) {
        const FormHelper = $injector.get('FormHelper');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                scope.selectSkillsStep = _.findWhere(scope.steps, {
                    stepName: 'select_skills',
                });

                scope.helper = EditCareerProfileHelper;
                scope.helper.logEventForApplication('viewed-select_skills');

                FormHelper.supportForm(scope, scope.select_skills);

                // Handle legacy skills
                scope.min = scope.selectSkillsStep.requiredFields.skills.min || 4;

                const initialSkills = _.clone(scope.careerProfile.skills);

                // Form validation / dirty handling for scareerProfile.skills.length
                scope.$watch('careerProfile.skills.length', (oldLength, newLength) => {
                    // if we're resetting the careerProfile after the form was dirtied, we don't want to re-dirty
                    if (oldLength !== newLength && !_.isEqual(initialSkills, scope.careerProfile.skills)) {
                        scope.select_skills.$setDirty(true);
                    }

                    // user a required field to validate the form
                    scope.skillsValid =
                        scope.careerProfile.skills && scope.careerProfile.skills.length >= scope.min ? 'true' : null;
                });
            },
        };
    },
]);
