import angularModule from 'Careers/angularModule/scripts/careers_module';
import 'FrontRoyalUiBootstrap/popover';
import template from 'Careers/angularModule/views/edit_career_profile/education_experience_detail.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import trashcanBeige from 'vectors/trashcan_beige.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('educationExperienceDetail', [
    '$injector',
    function factory($injector) {
        const FormHelper = $injector.get('FormHelper');
        const EducationExperience = $injector.get('EducationExperience');
        const Locale = $injector.get('Locale');
        const TranslationHelper = $injector.get('TranslationHelper');
        const $rootScope = $injector.get('$rootScope');

        return {
            restrict: 'E',
            scope: {
                ngModel: '=',
                min: '<?',
                max: '<?',
                degreeProgram: '<',
            },
            require: '?^form',
            templateUrl,
            link(scope, elem, attrs, formController) {
                scope.trashcanBeige = trashcanBeige;

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                const initialIds = scope.ngModel.map(value => value.id);

                scope.$watchCollection('ngModel', () => {
                    scope.educationExperiences = _.filter(
                        scope.ngModel,
                        experience => experience.degreeProgram === scope.degreeProgram,
                    );

                    getButtonKeys();

                    if (
                        scope.degreeProgram &&
                        scope.currentUser &&
                        scope.currentUser.career_profile &&
                        scope.currentUser.career_profile.indicatesUserShouldProvideTranscripts &&
                        !scope.currentUser.transcripts_verified &&
                        (scope.currentUser.isAccepted ||
                            scope.currentUser.isPreAccepted ||
                            scope.currentUser.isDeferredFromMbaOrEmba)
                    ) {
                        scope.educationExperiences.forEach(experience => {
                            experience.$$disableEdit = initialIds.includes(experience.id);
                        });
                    }
                });

                //---------------------------
                // Initialization
                //---------------------------

                const translationHelper = new TranslationHelper(
                    'careers.edit_career_profile.education_experience_detail',
                );

                scope.min = scope.min || 0;
                scope.organizationPlaceholderText = translationHelper.get('college');
                scope.institutionPlaceholderText = translationHelper.get('institution_name');

                // Add support for custom form validation and dirtying
                FormHelper.supportDirtyState(scope, formController);
                FormHelper.supportCollectionSizeValidation(scope, attrs, formController);

                // NOTE: this seems incorrect, but is not. These options are used
                // for educationExperience.willNotComplete. Selecting "yes" means
                // the program is or will be complete, hence the false value.
                // The opposite is true when selecting "no".
                scope.completeOptions = [
                    {
                        value: false,
                        stringValue: 'true',
                        label: translationHelper.get('yes'),
                    },
                    {
                        value: true,
                        stringValue: 'false',
                        label: translationHelper.get('no'),
                    },
                ];

                scope.yearOptions = [];

                for (let j = new Date().getUTCFullYear() + 5; j >= 1940; j--) {
                    scope.yearOptions.push(j);
                }

                scope.yearKey = graduated => {
                    if (graduated) {
                        if (scope.degreeProgram) {
                            return 'careers.edit_career_profile.education_experience_detail.grad_year';
                        }
                        return 'careers.edit_career_profile.education_experience_detail.completed_year';
                    }
                    return 'careers.edit_career_profile.education_experience_detail.last_year_attended';
                };

                scope.degreeConfig = angular.extend(FormHelper.getSelectizeConfigDefaults(), {
                    create: true,
                    placeholder: translationHelper.get('degree'),
                    plugins: ['inputMaxlength'],
                    inputMaxlength: 75,
                    onFocus() {
                        this.$wrapper.siblings('.degree-tooltip').addClass('show');
                    },
                    onBlur() {
                        this.$wrapper.siblings('.degree-tooltip').removeClass('show');
                    },
                });

                scope.degreeOptions = EducationExperience.degreeOptions.map(translationKey => ({
                    sort: translationHelper.get(translationKey),
                    value: translationKey,
                    label: translationHelper.get(translationKey),
                }));

                // find any user-provided options that aren't in the existing list and allow them to be visible
                const additionalOptions = _.chain(scope.ngModel)
                    .map('degree')
                    .difference(scope.degreeOptions)
                    .uniq()
                    .value();

                additionalOptions.forEach((option, i) => {
                    scope.degreeOptions.push({
                        sort: scope.degreeOptions.length + i,
                        value: option,
                        label: option,
                    });
                });

                function getButtonKeys() {
                    // If this is within the Formal Degree context, always
                    // display 'Add Another Degree', because we pre-create
                    // an empty experience if there are none to being with.
                    if (scope.degreeProgram) {
                        scope.addButtonKey =
                            scope.educationExperiences && scope.educationExperiences.length >= 1
                                ? 'careers.edit_career_profile.education_experience_detail.add_degree'
                                : 'careers.edit_career_profile.education_experience_detail.add_degree_first';
                    } else {
                        // If we have no non-degree programs, display 'Add a Program'
                        // If we have 1 or more non-degree programs, display 'Add Another Program'
                        scope.addButtonKey =
                            scope.educationExperiences && scope.educationExperiences.length >= 1
                                ? 'careers.edit_career_profile.education_experience_detail.add_education'
                                : 'careers.edit_career_profile.education_experience_detail.add_education_first';
                    }
                }

                //---------------------------
                // Collection CRUD
                //---------------------------

                scope.removeItem = educationExperience => {
                    const index = scope.ngModel.indexOf(educationExperience);
                    scope.ngModel.splice(index, 1);
                    scope.updateDirty();
                    scope.updateValidity();
                };

                scope.addItem = () => {
                    const experience = EducationExperience.new();
                    experience.educational_organization = {
                        text: '',
                        locale: Locale.activeCode,
                    };

                    experience.degreeProgram = scope.degreeProgram;

                    // this will update the reference so that our watch gets triggered
                    scope.ngModel = scope.ngModel.concat([experience]);

                    scope.updateDirty();
                    scope.updateValidity();
                };
            },
        };
    },
]);
