import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_career_profile/program_choice_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import exclamationPointInCircle from 'vectors/exclamation-point-in-circle.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('programChoiceForm', [
    '$injector',
    function factory($injector) {
        const FormHelper = $injector.get('FormHelper');
        const TranslationHelper = $injector.get('TranslationHelper');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
        const $rootScope = $injector.get('$rootScope');
        const ConfigFactory = $injector.get('ConfigFactory');
        const DialogModal = $injector.get('DialogModal');
        const Cohort = $injector.get('Cohort');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                //-------------------------
                // Initialization
                //-------------------------

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                scope.helper = EditCareerProfileHelper;
                const translationHelper = new TranslationHelper('careers.edit_career_profile.program_choice_form');

                // If the user is reapplying after being rejected from a Smartly (non-Quantic)
                // program, then they are not allowed to select the program_type they currently
                // have.  In that case, we initialize the program_type to null on the proxy
                // career_profile we're using for this form
                // See https://trello.com/c/QFVa6l5W
                scope.careerProfile.program_type = scope.careerProfile.programTypeIfAllowed;

                scope.programOptions = [
                    {
                        type: 'mba',
                        featured: true,
                        icon_class: 'mba',
                        title: translationHelper.get('mba_title'),
                        subtitle: translationHelper.get('mba_subtitle'),
                        bullets: [
                            translationHelper.get('mba_bullet_1'),
                            translationHelper.get('mba_bullet_2'),
                            translationHelper.get('mba_bullet_3'),
                        ],
                        footer: translationHelper.get('free_if_admitted'),
                    },
                    {
                        type: 'emba',
                        featured: true,
                        icon_class: 'emba',
                        title: translationHelper.get('emba_title'),
                        subtitle: translationHelper.get('emba_subtitle'),
                        bullets: [
                            translationHelper.get('emba_bullet_1'),
                            translationHelper.get('emba_bullet_2'),
                            translationHelper.get('emba_bullet_3'),
                            translationHelper.get('emba_bullet_4'),
                            translationHelper.get('emba_bullet_5'),
                        ],
                        footer: translationHelper.get('tuition_fees_scholarships'),
                        coming_soon: ConfigFactory.getSync().embaComingSoon(),
                    },
                    {
                        type: 'the_business_certificate',
                        featured: false,
                        title: translationHelper.get('free_certificates_title'),
                        subtitle: translationHelper.get('free_certificates_subtitle'),
                        pricing_subtitle: translationHelper.get('free_if_admitted'),
                    },
                    {
                        type: 'career_network_only',
                        featured: false,
                        title: translationHelper.get('career_network_only_title'),
                        subtitle: translationHelper.get('career_network_only_subtitle'),
                        pricing_subtitle: translationHelper.get('free_if_admitted'),
                    },
                ];

                if (!window.RUNNING_IN_TEST_MODE) {
                    // still nice to test this stuff
                    scope.programOptions = scope.programOptions.filter(o => ['mba', 'emba'].includes(o.type));
                }

                // If learner has already been accepted the career network, take away that option
                if (scope.currentUser?.hasCareersNetworkAccess) {
                    scope.programOptions = scope.programOptions.filter(o => o.type !== 'career_network_only');
                }

                // If the learner is in the experiment to hide non-mba program choices then
                // remove those choices
                // See https://trello.com/c/AF4bThbe
                if (scope.currentUser.experiment_ids.includes('h')) {
                    scope.hideNonMbaProgramOptions = true;
                    scope.programOptions = scope.programOptions.filter(option => ['mba', 'emba'].includes(option.type));
                }

                scope.showFeatured = scope.programOptions.some(o => o?.featured);
                scope.showNonFeatured = scope.programOptions.some(o => o?.featured === false);

                scope.helper.logEventForApplication('viewed-program_choice');

                //-------------------------
                // View Helpers
                //-------------------------

                FormHelper.supportForm(scope, scope.program_choice);

                const currentlySavedProgramType = _.clone(scope.careerProfile.program_type);

                // keep the selected attribute on each option synced to current selection
                scope.$watch('careerProfile.program_type', () => {
                    if (scope.currentUser) {
                        // used for any `programType` calculations on the user
                        scope.currentUser.$$pendingProgramTypeSelected = scope.careerProfile.program_type;

                        _.each(scope.programOptions, option => {
                            option.selected = option.type === scope.careerProfile.program_type;
                        });

                        // if they are already pending in a degree program and try to switch to a non-degree program, throw a warning
                        if (
                            _.contains(['mba', 'emba'], currentlySavedProgramType) &&
                            !_.contains(['mba', 'emba'], scope.careerProfile.program_type) &&
                            scope.currentUser.lastCohortApplication &&
                            scope.currentUser.lastCohortApplication.warn_if_switching_from_mba_emba
                        ) {
                            DialogModal.alert({
                                content:
                                    '<div class="center">' +
                                    `<img src="${exclamationPointInCircle}">` +
                                    '<p translate="careers.edit_career_profile.program_choice_form.program_change_warning" translate-values="{newProgram: newProgram, oldProgram: oldProgram}"></p>' +
                                    '<button class="flat coral" ng-click="keepProgram()" translate="careers.edit_career_profile.program_choice_form.keep_program" translate-values="{oldProgram: oldProgram}"></button><br><br>' +
                                    '<button class="unstyled-button coral" ng-click="closeModal()" translate="careers.edit_career_profile.program_choice_form.switch_program" translate-values="{newProgram: newProgram}"></button>' +
                                    '</div>',

                                hideCloseButton: true,
                                classes: ['program-change-warning'],
                                scope: {
                                    newProgram: Cohort.programChoiceTitle(scope.careerProfile.program_type),
                                    oldProgram: Cohort.programChoiceTitle(currentlySavedProgramType),
                                    keepProgram() {
                                        scope.careerProfile.program_type = currentlySavedProgramType;
                                        DialogModal.hideAlerts();
                                    },
                                    closeModal() {
                                        DialogModal.hideAlerts();
                                    },
                                },
                            });
                        }
                    }
                });

                scope.$on('$destroy', () => {
                    if (scope.currentUser) {
                        scope.currentUser.$$pendingProgramTypeSelected = undefined;
                    }
                });
            },
        };
    },
]);
