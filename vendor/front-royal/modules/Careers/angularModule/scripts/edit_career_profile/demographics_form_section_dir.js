import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_career_profile/demographics_form_section.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('demographicsFormSection', [
    '$injector',
    function factory($injector) {
        const FormHelper = $injector.get('FormHelper');
        const TranslationHelper = $injector.get('TranslationHelper');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                careerProfile: '<',
                gdprAppliesToUser: '<',
            },
            link(scope) {
                const fieldOptionsTranslationHelper = new TranslationHelper('careers.field_options');

                // Note: If you change this list also see the "Sex" Airtable column
                scope.sexOptions = ['male', 'female', 'no_identify'].map(translationKey => ({
                    value: translationKey,
                    label: fieldOptionsTranslationHelper.get(translationKey),
                }));

                // Note: If you change this list also see the "Race" Airtable column
                scope.raceKeys = [
                    'american_indian_or_alaska',
                    'asian',
                    'african_american',
                    'latin_american',
                    'arab',
                    'native_hawaiian_or_other',
                    'white',
                ];

                const propsValuesMap = {
                    race: scope.raceKeys,
                };
                FormHelper.supportCheckboxGroups(scope, scope.careerProfile, propsValuesMap);
            },
        };
    },
]);
