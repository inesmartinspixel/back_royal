import angularModule from 'Careers/angularModule/scripts/careers_module';
import { setupBrandNameProperties } from 'AppBrandMixin';
import template from 'Careers/angularModule/views/edit_career_profile/emba_application_questions_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('embaApplicationQuestionsForm', [
    '$injector',
    function factory($injector) {
        const FormHelper = $injector.get('FormHelper');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
        const answersTooShortHelper = $injector.get('answersTooShortHelper');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                scope.helper = EditCareerProfileHelper;
                scope.helper.logEventForApplication('viewed-emba_application_questions');

                setupBrandNameProperties($injector, scope);

                FormHelper.supportForm(scope, scope.emba_application_questions);

                scope.step = _.findWhere(scope.steps, {
                    stepName: 'emba_application_questions',
                });

                scope.shortAnswerModelProps = [
                    'short_answer_why_pursuing',
                    'short_answer_greatest_achievement',
                    'short_answer_ask_professional_advice',
                    'anything_else_to_tell_us',
                ];

                scope.shortAnswerFieldOverrides = {
                    short_answer_why_pursuing: {
                        localeValues: `{brandName: '${scope.brandNameLong}'}`,
                    },
                    anything_else_to_tell_us: {
                        maxCharLength: 500,
                        localeKey: 'careers.edit_career_profile.personal_summary_form.anything_else_to_tell_us',
                    },
                };

                scope.$on('savedCareerProfile', () => {
                    answersTooShortHelper.maybeShowModal(
                        scope.careerProfile,
                        _.without(scope.shortAnswerModelProps, 'anything_else_to_tell_us'),
                        100,
                    );
                });
            },
        };
    },
]);
