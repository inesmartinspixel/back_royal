import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_career_profile/preview_profile_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('previewProfileForm', [
    '$injector',
    function factory() {
        return {
            restrict: 'E',
            templateUrl,
            link() {},
        };
    },
]);
