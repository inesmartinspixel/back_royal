import angularModule from 'Careers/angularModule/scripts/careers_module';
import { setupBrandNameProperties, setupBrandEmailProperties } from 'AppBrandMixin';
import template from 'Careers/angularModule/views/edit_career_profile/program_lockdown_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('programLockdownForm', [
    '$injector',
    function factory($injector) {
        const FormHelper = $injector.get('FormHelper');
        const TranslationHelper = $injector.get('TranslationHelper');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
        const ConfigFactory = $injector.get('ConfigFactory');
        const DialogModal = $injector.get('DialogModal');
        const dateHelper = $injector.get('dateHelper');
        const Cohort = $injector.get('Cohort');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const ClientStorage = $injector.get('ClientStorage');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                Object.defineProperty(scope, 'canConvertToEMBA', {
                    get() {
                        return (
                            scope.currentUser.lastCohortApplication &&
                            scope.currentUser.lastCohortApplication.can_convert_to_emba
                        );
                    },
                });

                setupBrandNameProperties($injector, scope);
                setupBrandEmailProperties($injector, scope, ['admissions']);
                NavigationHelperMixin.onLink(scope);

                EditCareerProfileHelper.logEventForApplication('viewed-program_lockdown');
                FormHelper.supportForm(scope, scope.program_lockdown);
                const translationHelper = new TranslationHelper('careers.edit_career_profile.program_choice_form');

                scope.programOptions = [
                    {
                        type: 'emba',
                        featured: true,
                        icon_class: 'emba',
                        title: translationHelper.get('emba_title'),
                        subtitle: translationHelper.get('emba_subtitle'),
                        bullets: [
                            translationHelper.get('emba_bullet_1'),
                            translationHelper.get('emba_bullet_2'),
                            translationHelper.get('emba_bullet_3'),
                            translationHelper.get('emba_bullet_4'),
                            translationHelper.get('emba_bullet_5'),
                        ],
                        footer: translationHelper.get('tuition_fees_scholarships'),
                        coming_soon: ConfigFactory.getSync().embaComingSoon(),
                        updateApplication: true,
                    },
                ];

                scope.$watch('careerProfile.program_type', programType => {
                    if (programType) {
                        scope.programChoiceFullTitle = Cohort.programChoiceFullTitle(programType);
                        scope.programAppliedTitle = translationHelper.get(`${programType}_title`);
                        scope.programAppliedSubtitle = translationHelper.get(`${programType}_subtitle`);
                    }
                });

                Object.defineProperty(scope, 'showAdmissionDecisionMessage', {
                    get() {
                        return (
                            scope.currentUser.relevant_cohort &&
                            scope.currentUser.relevant_cohort.supportsAdmissionRounds
                        );
                    },
                });

                if (scope.showAdmissionDecisionMessage && scope.currentUser.lastCohortApplication) {
                    const applicableAdmissionRound = scope.currentUser.relevant_cohort.getApplicableAdmissionRound(
                        scope.currentUser.lastCohortApplication.appliedAt,
                    );

                    // we saw a user who was put into an MBA cohort by an admin after the last admission round
                    // so if that happens, use the decision date of the last admission round
                    if (applicableAdmissionRound) {
                        scope.decisionDate = dateHelper.formattedUserFacingMonthDayLong(
                            applicableAdmissionRound.decisionDate,
                        );
                    } else {
                        scope.decisionDate = dateHelper.formattedUserFacingMonthDayLong(
                            scope.currentUser.relevant_cohort.lastDecisionDate,
                        );
                    }
                }

                scope.onShortAnswerScholarshipSave = () => {
                    scope.save().then(() => {
                        DialogModal.removeAlerts();
                    });
                };

                scope.showShortAnswerScholarshipModal = () => {
                    if (
                        !scope.careerProfile.short_answer_scholarship &&
                        scope.currentUser.lastCohortApplication &&
                        scope.currentUser.lastCohortApplication.converted_pending_application_to_emba_upon_invitation
                    ) {
                        DialogModal.alert({
                            content:
                                '<short-answer-scholarship-modal career-profile="careerProfile" on-save="onSave"></short-answer-scholarship-modal>',
                            scope: {
                                careerProfile: scope.careerProfile,
                                onSave: scope.onShortAnswerScholarshipSave,
                            },
                        });
                    }
                };

                scope.updateApplication = programType => {
                    scope.updating = true;
                    scope.careerProfile.program_type = programType;
                    scope.careerProfile.save().then(() => {
                        ClientStorage.setItem('converted_to_emba', programType === 'emba');
                        scope.updating = false;
                        scope.showShortAnswerScholarshipModal();
                        scope.$emit('savedCareerProfile');
                    });
                };

                scope.close = () => {
                    scope.currentUser.reapplyingOrEditingApplication = false;
                    $injector.get('$location').path('/settings/application_status');
                };

                scope.editApplication = () => {
                    scope.$emit('gotoFormStep', 1);
                };
            },
        };
    },
]);
