import angularModule from 'Careers/angularModule/scripts/careers_module';
import countries from 'countries';
import template from 'Careers/angularModule/views/edit_career_profile/basic_info_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';
import moment from 'moment-timezone';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('basicInfoForm', [
    '$injector',
    function factory($injector) {
        const FormHelper = $injector.get('FormHelper');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
        const dateHelper = $injector.get('dateHelper');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                scope.helper = EditCareerProfileHelper;
                scope.dateHelper = dateHelper;

                // FIXME: account for EMBA as well
                scope.helper.logEventForApplication('viewed-basic_info');

                scope.countryOptions = _.map(countries, country => ({
                    value: country.value,
                    label: country.text,
                }));

                scope.careerProfile.country = scope.careerProfile.country || 'US';

                scope.$watch('careerProfile.place_details', placeDetails => {
                    if (!_.any(placeDetails)) {
                        return;
                    }

                    // Note that we only set these if the user has not explicity filled out their mailing address so as not
                    // to overrite their explicit values with inferred values from the Google Place
                    if (!scope.careerProfile.address_line_1) {
                        scope.careerProfile.city = scope.careerProfile.placeCity;
                        scope.careerProfile.state = scope.careerProfile.placeState;
                        scope.careerProfile.country = scope.careerProfile.placeCountry;
                    }
                });

                FormHelper.supportForm(scope, scope.basic_info);

                scope.minBirthday = moment().subtract(100, 'years').format('YYYYMMDD');
                scope.maxBirthday = moment().subtract(13, 'years').format('YYYYMMDD');
                scope.proxy = {};

                scope.$watch('proxy.birthdateViewValue', () => {
                    if (scope.proxy.birthdateViewValue) {
                        scope.invalidAge =
                            scope.proxy.birthdateViewValue < scope.minBirthday ||
                            scope.proxy.birthdateViewValue > scope.maxBirthday
                                ? moment().diff(scope.proxy.birthdateViewValue, 'years')
                                : undefined;
                    } else {
                        scope.invalidAge = undefined;
                    }

                    scope.showInvalidAge = angular.isDefined(scope.invalidAge);
                });
            },
        };
    },
]);
