import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_career_profile/short_answer_fields_section.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('shortAnswerFieldsSection', () => ({
    restrict: 'E',

    scope: {
        step: '<',
        modelProps: '<',
        careerProfile: '<',
        fieldOverrides: '<',
    },

    templateUrl,

    link(scope) {
        function getShortAnswerFieldConfig(modelProp, overrides = {}) {
            return _.extend(
                {
                    maxCharLength: 350,
                    localeKey: `careers.edit_career_profile.${scope.step.stepName}_form.${modelProp}`,
                },
                overrides,
                {
                    modelProp,
                    isRequired: !!scope.step.requiredFields[modelProp],
                },
            );
        }

        scope.shortAnswerFieldConfigs = _.map(scope.modelProps, field =>
            getShortAnswerFieldConfig(field, scope.fieldOverrides[field]),
        );
    },
}));
