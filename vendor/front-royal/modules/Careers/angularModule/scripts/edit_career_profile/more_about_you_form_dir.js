import angularModule from 'Careers/angularModule/scripts/careers_module';
import { setupBrandNameProperties } from 'AppBrandMixin';
import template from 'Careers/angularModule/views/edit_career_profile/more_about_you_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('moreAboutYouForm', [
    '$injector',
    function factory($injector) {
        const FormHelper = $injector.get('FormHelper');
        const TranslationHelper = $injector.get('TranslationHelper');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                scope.moreAboutYouStep = _.findWhere(scope.steps, {
                    stepName: 'more_about_you',
                });

                scope.helper = EditCareerProfileHelper;
                const translationHelper = new TranslationHelper('careers.edit_career_profile.more_about_you_form');

                scope.helper.logEventForApplication('viewed-more_about_you');

                setupBrandNameProperties($injector, scope);

                scope.awardsPlaceholder = translationHelper.get('add_an_award');

                scope.reasonForApplyingOptionKeys = [
                    'start_own_company',
                    'advance_in_current_company',
                    'get_better_job',
                    'expand_business_knowledge',
                    'more_effective_leader',
                ];

                scope.subjectAreaKeys = $injector
                    .get('CAREERS_SUBJECT_AREA_KEYS')
                    .concat(['management_and_leadership']);

                scope.attributeKeys = [
                    'creative',
                    'competitive',
                    'conscientious',
                    'loyal',
                    'resilient',
                    'optimistic',
                    'driven',
                    'flexible',
                    'balanced',
                    'persuasive',
                    'compassionate',
                    'courageous',
                    'motivating',
                    'meticulous',
                    'high_energy',
                ];

                scope.motivationKeys = [
                    'results',
                    'helping_others',
                    'career_advancement',
                    'innovation',
                    'fellowship',
                    'personal_growth',
                    'respect',
                    'security',
                ];

                scope.strengthKeys = [
                    'problem_solving',
                    'leadership',
                    'business_acumen',
                    'professionalism',
                    'entrepreneurial',
                    'relationship_building',
                    'work_ethic',
                    'design_aesthetic',
                    'communication',
                    'resourcefulness',
                ];

                FormHelper.supportForm(scope, scope.more_about_you);
                FormHelper.supportAutoSuggestOptions(scope);

                const propsValuesMap = {
                    race: scope.raceKeys,
                };
                FormHelper.supportCheckboxGroups(scope, scope.careerProfile, propsValuesMap);

                scope.getOptionsAndFilter = (type, searchText) =>
                    scope.getOptionsForType(type, searchText).then(options => {
                        // filter the response
                        const existingTexts = _.pluck(scope.careerProfile[type], 'text');

                        return _.reject(options, option => _.contains(existingTexts, option.text));
                    });

                scope.getOptionsForAwardsAndInterests = searchText =>
                    scope.getOptionsAndFilter('awards_and_interests', searchText);
            },
        };
    },
]);
