import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_career_profile/career_preferences_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('careerPreferencesForm', [
    '$injector',
    function factory($injector) {
        const FormHelper = $injector.get('FormHelper');
        const TranslationHelper = $injector.get('TranslationHelper');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
        const isMobileMixin = $injector.get('isMobileMixin');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                //---------------------------
                // Initialization
                //---------------------------

                isMobileMixin.onLink(scope);
                FormHelper.supportForm(scope, scope.career_preferences);

                scope.helper = EditCareerProfileHelper;
                scope.helper.logEventForApplication('viewed-career_preferences');

                // special messaging for cohorts that support early career applicants
                scope.earlyCareerApplicant = EditCareerProfileHelper.supportsEarlyCareerApplicants(scope.currentUser);

                //---------------------------
                // Checkboxes Support
                //---------------------------

                const propsRequiredFieldsMap = _.findWhere(scope.steps, {
                    stepName: 'career_preferences',
                }).requiredFields;

                scope.companySizeOptions = $injector.get('CAREERS_EMPLOYEE_COUNT_KEYS');
                scope.employmentTypeOptions = $injector.get('CAREERS_POSITION_DESCRIPTOR_KEYS');
                const locationsOfInterestKeys = $injector.get('CAREERS_LOCATIONS_OF_INTEREST_KEYS');
                scope.defaultAndDominantLocationsOfInterestOption = 'flexible';

                const propsValuesMap = {
                    company_sizes_of_interest: scope.companySizeOptions,
                    employment_types_of_interest: scope.employmentTypeOptions,
                    locations_of_interest: locationsOfInterestKeys,
                };

                const propsDefaultValuesMap = {
                    locations_of_interest: scope.defaultAndDominantLocationsOfInterestOption,
                };

                const propsDominantValuesMap = {
                    locations_of_interest: scope.defaultAndDominantLocationsOfInterestOption,
                };

                FormHelper.supportCheckboxGroups(
                    scope,
                    scope.careerProfile,
                    propsValuesMap,
                    propsRequiredFieldsMap,
                    propsDefaultValuesMap,
                    propsDominantValuesMap,
                );

                //---------------------------
                // Employer Preferences
                //---------------------------

                const employerPreferencesTranslationHelper = new TranslationHelper(
                    'careers.edit_career_profile.employer_preferences_form',
                );
                scope.areaPlaceholder = employerPreferencesTranslationHelper.get('choose_up_to_3');

                scope.cultureKeys = [
                    'best_in_class',
                    'fast_paced',
                    'hierarchical',
                    'merit_based',
                    'stable',
                    'entrepreneurial',
                    'results_oriented',
                    'harmonious',
                    'flat_structured',
                    'mission_driven',
                ];

                scope.industryKeys = $injector.get('CAREERS_INDUSTRY_KEYS');

                //---------------------------
                // Job Preferences
                //---------------------------

                const jobPreferencesTranslationHelper = new TranslationHelper(
                    'careers.edit_career_profile.job_preferences_form',
                );

                // Interested in Joining New Company
                scope.defaultNewCompanyInterestLevel = 'neutral';
                scope.careerProfile.interested_in_joining_new_company =
                    scope.careerProfile.interested_in_joining_new_company || scope.defaultNewCompanyInterestLevel;
                scope.newCompanyInterestLevels = [
                    'not_interested',
                    scope.defaultNewCompanyInterestLevel,
                    'interested',
                    'very_interested',
                ];

                // Locations of Interest
                const visibleLocationsOnMobileLimit = 10;
                scope.locationsOfInterest = locationsOfInterestKeys.slice(0, visibleLocationsOnMobileLimit);

                // We change how the locations of interest are displayed to the user based on the device's screen size.
                // If they're on a mobile device, then the first 10 options are shown with a button beneath them to show
                // all of the remaining available locations. If they're not on a mobile device, the groups get split into
                // two columns to save vertical space.
                scope.$watch('isMobile', () => {
                    scope.locationsOfInterest = locationsOfInterestKeys.slice(0, visibleLocationsOnMobileLimit);
                    if (!scope.isMobile) {
                        scope.locationsOfInterest = locationsOfInterestKeys;
                    }
                });

                scope.showAllLocationsOfInterest = () => {
                    scope.showingAllLocationsOfInterest = true;
                    scope.locationsOfInterest = locationsOfInterestKeys;
                };

                // Citizenship/Immigration status
                const authorizedOptions = [
                    'citizen',
                    'novisa',
                    'h1visa',
                    'f1visa',
                    'ead',
                    'tnvisa',
                    'j1visa',
                    'o1visa',
                    'l1visa',
                    'tneligible',
                    'e3eligible',
                    'h1b1eligible',
                ];
                scope.authorizedOptions = convertJobPreferenceKeysToValues(authorizedOptions);

                // Converts keys that require the jobPreferencesTranslationHelper to values in preparation for use
                // in the ngOptions attribute directive.
                function convertJobPreferenceKeysToValues(keys) {
                    return _.map(keys, translationKey => ({
                        value: translationKey,
                        label: jobPreferencesTranslationHelper.get(translationKey),
                    }));
                }

                scope.$watch('careerProfile.willing_to_relocate', (newVal, oldVal) => {
                    // If initializing or changing to false, always set to ['none']
                    if (newVal === false) {
                        scope.careerProfile.locations_of_interest = ['none'];
                    } else if (newVal === true) {
                        // If initializing to true, and no locations_of_interest, AKA
                        // ['none'], set to empty array to force user selection
                        if (newVal === oldVal && scope.careerProfile.locations_of_interest[0] === 'none') {
                            scope.careerProfile.locations_of_interest = [];
                        } else if (newVal !== oldVal) {
                            // If changing from false to true, always set to ['flexible']
                            scope.toggleCheckbox(
                                'locations_of_interest',
                                scope.defaultAndDominantLocationsOfInterestOption,
                            );
                        }
                    }
                });
            },
        };
    },
]);
