import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_career_profile/peer_recommendations.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('peerRecommendations', [
    '$injector',
    function factory() {
        return {
            restrict: 'E',
            templateUrl,
            scope: {
                careerProfile: '<',
            },
            link(scope) {
                const maxNumPeerRecommendations = 3;

                scope.careerProfile.peer_recommendations = scope.careerProfile.peer_recommendations || [];

                // Populates the peer_recommendations array on the careerProfile with empty
                // peer recommendations for each unoccupied index in the array.
                scope.addEmptyPeerRecommendations = () => {
                    for (let i = 1; i <= maxNumPeerRecommendations; i++) {
                        if (!scope.careerProfile.peer_recommendations[i - 1]) {
                            scope.careerProfile.peer_recommendations[i - 1] = {};
                        }
                    }
                };

                // invoke immediately
                scope.addEmptyPeerRecommendations();

                scope.$watch('careerProfile.peer_recommendations.length', () => {
                    // If the length of the peer_recommendations array ever becomes not equal to maxNumPeerRecommendations,
                    // then we need to call addEmptyPeerRecommendations to repopulate the array with empty peer recommendations.
                    // This can happen if the user saves the career profile without filling in the maxNumPeerRecommendations.
                    if (
                        scope.careerProfile &&
                        scope.careerProfile.peer_recommendations &&
                        scope.careerProfile.peer_recommendations.length !== maxNumPeerRecommendations
                    ) {
                        scope.addEmptyPeerRecommendations();
                    }
                });

                scope.$on('$destroy', () => {
                    // Ensure that all invalid peer recommendations are removed from the
                    // peer_recommendations array when this directive gets destroyed.
                    scope.careerProfile.sanitizePeerRecommendations();
                });
            },
        };
    },
]);
