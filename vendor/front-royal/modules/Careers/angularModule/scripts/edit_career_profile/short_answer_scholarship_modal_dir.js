import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_career_profile/short_answer_scholarship_modal.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('shortAnswerScholarshipModal', [
    '$injector',
    function factory() {
        return {
            restrict: 'E',
            templateUrl,
            scope: {
                careerProfile: '<',
                onSave: '<',
            },
            link(scope) {
                scope.savedShortAnswerScholarship = false;

                scope.save = () => {
                    if (scope.shortAnswerScholarship.$invalid) {
                        return;
                    }
                    scope.savedShortAnswerScholarship = true;
                    scope.onSave();
                };

                scope.$on('$destroy', () => {
                    // When this directive gets destroyed, the user may have entered some text for the short_answer_scholarship
                    // field, but may not have actually saved the form. If we see that this has happened, we want to revert any
                    // unsaved changes they may have made.
                    if (!scope.savedShortAnswerScholarship) {
                        scope.careerProfile.short_answer_scholarship = null;
                    }
                });
            },
        };
    },
]);
