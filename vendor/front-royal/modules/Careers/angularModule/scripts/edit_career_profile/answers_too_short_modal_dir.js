import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_career_profile/answers_too_short_modal.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('answersTooShortModal', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');

        return {
            restrict: 'E',
            scope: {
                onFinish: '<',
            },
            templateUrl,
            link(scope) {
                scope.closeModal = () => {
                    if (!$rootScope.currentUser.ghostMode) {
                        $rootScope.currentUser.has_seen_short_answer_warning = true;
                        $rootScope.currentUser.save();
                    }
                    scope.onFinish();
                };
            },
        };
    },
]);
