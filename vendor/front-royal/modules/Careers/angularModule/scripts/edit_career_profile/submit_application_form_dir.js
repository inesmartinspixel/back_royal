import angularModule from 'Careers/angularModule/scripts/careers_module';
import { setupBrandNameProperties } from 'AppBrandMixin';
import template from 'Careers/angularModule/views/edit_career_profile/submit_application_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';
import moment from 'moment-timezone';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('submitApplicationForm', [
    '$injector',
    function factory($injector) {
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const DialogModal = $injector.get('DialogModal');
        const TranslationHelper = $injector.get('TranslationHelper');
        const ngToast = $injector.get('ngToast');
        const $timeout = $injector.get('$timeout');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
        const ProfileCompletionHelper = $injector.get('ProfileCompletionHelper');
        const AuthFormHelperMixin = $injector.get('AuthFormHelperMixin');
        const FormHelper = $injector.get('FormHelper');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                //---------------------------
                // Mixins
                //---------------------------

                FormHelper.supportForm(scope, scope.submit_application);
                NavigationHelperMixin.onLink(scope);
                AuthFormHelperMixin.onLink(scope);
                setupBrandNameProperties($injector, scope);

                //---------------------------
                // Mixin Initialization
                //---------------------------

                const translationHelper = new TranslationHelper('careers.edit_career_profile.submit_application_form');

                const completionHelper = new ProfileCompletionHelper(
                    EditCareerProfileHelper.getStepsForApplicationForm(scope.currentUser),
                );

                //---------------------------
                // Scope Properties
                //---------------------------

                // This is a special case. We want to hide portions of the UI if the application is incomplete.
                // However, we want to show those portions of the UI if the applicaiton is complete aside from
                // consider_early_decision, which is set on this page. Since consider_early_decision is required
                // for EMBA applications, the application is technically incomplete until it is set.
                Object.defineProperty(scope, 'applicationCompleteWithoutConsiderEarlyDecision', {
                    get() {
                        return (
                            scope.currentUser &&
                            scope.currentUser.career_profile &&
                            completionHelper.getPercentComplete(scope.currentUser.career_profile, undefined, [
                                'consider_early_decision',
                            ]) === 100
                        );
                    },
                    configurable: true, // for specs
                });

                Object.defineProperty(scope, 'previousApplicationToRelevantCohort', {
                    get() {
                        return (
                            scope.currentUser &&
                            scope.currentUser.relevant_cohort &&
                            scope.currentUser.cohort_applications &&
                            _.findWhere(scope.currentUser.cohort_applications, {
                                cohort_id: scope.currentUser.relevant_cohort.id,
                            })
                        );
                    },
                    configurable: true, // for specs
                });

                Object.defineProperty(scope, 'hasAppliedToRelevantCohort', {
                    get() {
                        return !!scope.previousApplicationToRelevantCohort;
                    },
                    configurable: true, // specs
                });

                Object.defineProperty(scope, 'lastAdmissionRoundDeadlineHasPassed', {
                    get() {
                        const currentUser = scope.currentUser;
                        return (
                            currentUser &&
                            currentUser.relevant_cohort &&
                            currentUser.relevant_cohort.lastAdmissionRoundDeadlineHasPassed()
                        );
                    },
                    configurable: true, // specs
                });

                //---------------------------
                // Scope Initialization
                //---------------------------

                scope.helper.logEventForApplication('viewed-submit_application');
                scope.applicationComplete = getApplicationComplete(scope.currentUser.career_profile);

                // I tried putting this directly into the ng-click, but it would not work for some reason
                scope.toggleFullProfile = () => {
                    scope.showFullProfile = !scope.showFullProfile;
                };

                const considerEarlyDecisionOptionValues = [true, false];
                scope.considerEarlyDecisionOptions = _.map(considerEarlyDecisionOptionValues, value => ({
                    value,
                    localeSuffix: value ? 'yes' : 'no',
                }));

                scope.considerEarlyDecisionIsRequired = () =>
                    !_.contains(considerEarlyDecisionOptionValues, scope.careerProfile.consider_early_decision);

                // If emba application, require early decision
                if (scope.helper.supportsEarlyDecisionConsideration(scope.currentUser)) {
                    // Listen for consider_early_decision changes to update form validity
                    scope.$watch(
                        () => scope.careerProfile.consider_early_decision,
                        newVal => {
                            if (angular.isDefined(newVal)) {
                                scope.applicationComplete = getApplicationComplete(scope.careerProfile);
                            }
                        },
                    );
                }

                scope.$watch('currentUser.relevant_cohort', relevantCohort => {
                    scope.applicationDeadlineMessage = relevantCohort && relevantCohort.getApplicationDeadlineMessage();

                    // This is temporary. See https://trello.com/c/Vl9Nix5w
                    if (relevantCohort && !relevantCohort.supportsEarlyDecisionConsideration) {
                        scope.careerProfile.consider_early_decision = false;
                    }
                });

                //---------------------------
                // Submit Application
                //---------------------------

                scope.submitApplication = () => {
                    scope.submitting = true;
                    let promise;
                    const alreadyPending = !!scope.currentUser.hasPendingOrPreAcceptedCohortApplication;
                    const reapplyingToSameCohort = scope.hasAppliedToRelevantCohort;

                    // For program types that support early decision consideration, if the lastAdmissionRoundDeadlineHasPassed,
                    // then we want to silently set consider_early_decision to false, unless they've already set it.
                    if (
                        scope.helper.supportsEarlyDecisionConsideration(scope.currentUser) &&
                        !scope.careerProfile.consider_early_decision &&
                        scope.lastAdmissionRoundDeadlineHasPassed
                    ) {
                        scope.careerProfile.consider_early_decision = false;
                    }

                    // if we forsee that the user is most likely too experienced for the MBA, show them an option to switch to the EMBA
                    if (
                        scope.careerProfile.program_type === 'mba' &&
                        moment().diff(scope.careerProfile.birthdate, 'years') > 32 &&
                        !scope.currentUser.has_seen_mba_submit_popup &&
                        !alreadyPending &&
                        !reapplyingToSameCohort
                    ) {
                        DialogModal.alert({
                            content:
                                '<p translate-once="careers.edit_career_profile.submit_application_form.we_also_offer"></p>' +
                                '<br>' +
                                '<p class="center">' +
                                '<button type="button" class="flat blue" ng-click="changePrograms()" translate-once="careers.edit_career_profile.submit_application_form.switch_to_emba"></button>' +
                                '<span class="buttons_or" translate="careers.edit_career_profile.submit_application_form.or"></span>' +
                                '<button type="button" class="flat hollow blue" ng-click="submitApplication()" translate-once="careers.edit_career_profile.submit_application_form.submit_mba_application"></button>' +
                                '</p>',
                            title: translationHelper.get('you_are_about_to_apply'),
                            hideCloseButton: true,
                            scope: {
                                changePrograms: scope.changeProgramToEmba,
                                submitApplication: scope.keepProgramSubmitApplication,
                            },
                        });
                    } else {
                        // save career profile because the phone field is on this page
                        scope.careerProfile.save().then(() => {
                            // The unsaved changes confirmation dialog modal pops up when the route changes but we see that the
                            // form on the page is still dirty. At this point in the logic, the user has successfully saved
                            // their career profile, so they shouldn't have any unsaved changes, but the form may still dirty
                            // so we need to set the currentForm to pristine to prevent the unsaved changes confirmation dialog
                            // modal from popping up unexpectedly.
                            scope.currentForm.$setPristine();

                            scope.currentUser.career_profile = scope.careerProfile;
                            if (alreadyPending) {
                                // We don't actually have to update anything here, but the timeout
                                // is necessary because the toast won't reliably show up unless defer a digest
                                promise = $timeout().then(() => {
                                    ngToast.create({
                                        content: translationHelper.get('application_updated'),
                                        className: 'success',
                                    });
                                }, 10);
                            } else if (reapplyingToSameCohort) {
                                const cohortApplication = scope.previousApplicationToRelevantCohort;
                                cohortApplication.status = 'pending';
                                promise = cohortApplication
                                    .save({
                                        update_applied_at: true,
                                    })
                                    .then(() => {
                                        DialogModal.alert({
                                            content: '<thanks-for-applying-modal></thanks-for-applying-modal>',
                                            title: new TranslationHelper('careers.thanks_for_applying_modal').get(
                                                'title',
                                                {
                                                    brandName: scope.brandNameShort,
                                                },
                                            ),
                                            size: 'small',
                                            scope: {},
                                        });
                                    });
                            } else {
                                promise = scope.currentUser
                                    .applyToCohort(scope.currentUser.relevant_cohort)
                                    .then(() => {
                                        if (!EditCareerProfileHelper.supportsSubmitToStatusPage(scope.currentUser)) {
                                            DialogModal.alert({
                                                content: '<thanks-for-applying-modal></thanks-for-applying-modal>',
                                                title: new TranslationHelper('careers.thanks_for_applying_modal').get(
                                                    'title',
                                                    {
                                                        brandName: scope.brandNameShort,
                                                    },
                                                ),
                                                size: 'small',
                                                scope: {},
                                            });
                                        }
                                    });
                            }

                            return promise
                                .then(() => {
                                    // In case they were reapplyingOrEditingApplication, do not show the menu item anymore
                                    scope.currentUser.reapplyingOrEditingApplication = false;
                                    scope.loadRoute(
                                        alreadyPending ||
                                            EditCareerProfileHelper.supportsSubmitToStatusPage(scope.currentUser)
                                            ? '/settings/application_status'
                                            : '/dashboard',
                                    );
                                })
                                .finally(() => {
                                    scope.submitting = false;
                                });
                        });
                    }
                };

                //---------------------------
                // EMBA Upsell Modal
                //---------------------------

                function logEmbaUpsellDecision(programType) {
                    scope.helper.logEventForApplication('emba_upsell_modal_selection', true, {
                        selection_details: programType,
                    });
                }

                function dismissUpsellModal() {
                    scope.currentUser.has_seen_mba_submit_popup = true;
                    scope.currentUser.save();
                    DialogModal.hideAlerts();
                }

                // put on scope for testing, used in scope.submitApplication DialogModal
                scope.changeProgramToEmba = () => {
                    logEmbaUpsellDecision('emba');
                    scope.careerProfile.program_type = 'emba';
                    // console.log('A')
                    scope.careerProfile.save().then(() => {
                        // console.log('B')
                        scope.currentUser.career_profile.program_type = 'emba';
                        scope.$emit('savedCareerProfileWithStepChange', 'emba_application_questions');
                        dismissUpsellModal();
                    });
                };

                scope.keepProgramSubmitApplication = () => {
                    logEmbaUpsellDecision('mba');
                    dismissUpsellModal();
                    scope.submitApplication();
                };

                //---------------------------
                // Completion Tracking
                //---------------------------

                function getApplicationComplete(model) {
                    return (
                        scope.currentUser &&
                        scope.currentUser.career_profile &&
                        completionHelper.getPercentComplete(model) === 100
                    );
                }
            },
        };
    },
]);
