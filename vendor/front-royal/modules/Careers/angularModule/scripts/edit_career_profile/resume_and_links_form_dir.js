import angularModule from 'Careers/angularModule/scripts/careers_module';
import * as userAgentHelper from 'userAgentHelper';
import template from 'Careers/angularModule/views/edit_career_profile/resume_and_links_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import trashcanBeige from 'vectors/trashcan_beige.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('resumeAndLinksForm', [
    '$injector',
    function factory($injector) {
        const FormHelper = $injector.get('FormHelper');
        const TranslationHelper = $injector.get('TranslationHelper');
        const $window = $injector.get('$window');
        const frontRoyalUpload = $injector.get('frontRoyalUpload');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                scope.trashcanBeige = trashcanBeige;

                scope.helper = EditCareerProfileHelper;
                const translationHelper = new TranslationHelper('careers.edit_career_profile.resume_and_links_form');

                scope.helper.logEventForApplication('viewed-resume_and_links');

                scope.uploadString = translationHelper.get('upload');
                scope.fileName = scope.careerProfile.resume ? scope.careerProfile.resume.file_file_name : undefined;
                scope.uploadedAt = scope.careerProfile.resume ? scope.careerProfile.resume.file_updated_at : undefined;

                // We don't use the ng-model that ng-file-upload sets for our careerProfile form, but we
                // do use it for required validation and ng-file-upload sets it when uploading a file.
                // Therefore we need to initialize this proxyFile to something that validates to true when
                // first loading the page in order for required validation to work properly. See also removeResume
                // below.
                scope.proxyFile = scope.careerProfile.resume ? scope.careerProfile.resume : undefined;

                // We used to hide the resume upload section if the User-Agent was on an iOS devices, regardless of the device type
                // (see https://trello.com/c/GbHl1dnr). Since then, we've relaxed this rule a bit to allow iOS tablet devices to
                // upload a resume (see https://trello.com/c/L5VQxmG1).
                scope.hideResumeUpload =
                    $window.CORDOVA && userAgentHelper.isiOSDevice() && userAgentHelper.isMobileDevice();

                FormHelper.supportForm(scope, scope.resume_and_links);

                //-----------------------------
                // File upload / delete
                //-----------------------------

                scope.removeResume = () => {
                    scope.careerProfile.resume_id = null;
                    scope.fileName = undefined;
                    scope.proxyFile = undefined;
                };

                scope.onFileSelect = (file, errFiles) => {
                    if (!scope.currentUser) {
                        return;
                    }

                    scope.errMessage = null;
                    scope.uploading = true;

                    frontRoyalUpload
                        .handleNgfSelect(file, errFiles, file => ({
                            url: scope.currentUser.resumeUploadUrl(),
                            data: {
                                resume_file: file,
                                resume_file_name: file.name,
                            },
                            supportedFormatsForErrorMessage: '.pdf, .doc, .docx, .jpg, .png',
                        }))
                        .then(response => {
                            scope.careerProfile.resume_id = response.data.contents.resume.id;
                            scope.careerProfile.resume = response.data.contents.resume;
                            scope.fileName = response.data.contents.resume.file_file_name;
                            scope.uploadedAt = response.data.contents.resume.file_updated_at;
                        })
                        .catch(err => {
                            scope.errMessage = err && err.message;
                        })
                        .finally(() => {
                            scope.uploading = false;
                        });
                };
            },
        };
    },
]);
