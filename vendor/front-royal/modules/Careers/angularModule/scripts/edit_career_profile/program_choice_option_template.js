import angularModule from 'Careers/angularModule/scripts/careers_module';
import programChoiceOptionTemplate from 'Careers/angularModule/views/edit_career_profile/program_choice_option.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

cacheAngularTemplate(angularModule, 'Careers/program_choice_option.html', programChoiceOptionTemplate);
