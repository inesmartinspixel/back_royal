import angularModule from 'Careers/angularModule/scripts/careers_module';
import { setupBrandNameProperties } from 'AppBrandMixin';
import template from 'Careers/angularModule/views/edit_career_profile/personal_summary_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('personalSummaryForm', [
    '$injector',
    function factory($injector) {
        const FormHelper = $injector.get('FormHelper');
        const EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
        const TranslationHelper = $injector.get('TranslationHelper');

        return {
            restrict: 'E',
            templateUrl,
            link(scope) {
                const translationHelper = new TranslationHelper('careers.edit_career_profile.personal_summary_form');
                setupBrandNameProperties($injector, scope);

                FormHelper.supportForm(scope, scope.personal_summary);

                scope.helper.logEventForApplication('viewed-personal_summary');

                if (scope.currentUser.programType === 'career_network_only') {
                    scope.funFactDescKey = 'careers.edit_career_profile.personal_summary_form.fun_fact_desc';
                } else {
                    scope.funFactDescKey = 'careers.edit_career_profile.personal_summary_form.fun_fact_tell_us';
                }

                scope.placeholderFact = translationHelper.get('fun_fact_placeholder');

                // special messaging for cohorts that support early career applicants
                scope.earlyCareerApplicant = EditCareerProfileHelper.supportsEarlyCareerApplicants(scope.currentUser);

                scope.placeholderGoal = translationHelper.get('long_term_goal_placeholder');

                scope.howHearPlaceholder = translationHelper.get('how_hear_placeholder');

                scope.howHearOptionKeys = [
                    'financial_times',
                    'economist',
                    'tv_cnbc',
                    'tv_bloomberg',
                    'facebook',
                    'instagram',
                    'linkedin',
                    'friend',
                    'ivyexec',
                    'search_engine',
                ];
            },
        };
    },
]);
