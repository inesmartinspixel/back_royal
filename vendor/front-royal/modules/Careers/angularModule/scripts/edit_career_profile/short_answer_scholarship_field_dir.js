import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/edit_career_profile/short_answer_scholarship_field.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('shortAnswerScholarshipField', [
    '$injector',
    function factory() {
        return {
            restrict: 'E',
            templateUrl,
            scope: {
                careerProfile: '<',
                isRequired: '<',
            },
            link(scope) {
                scope.isRequired = scope.isRequired || false;
            },
        };
    },
]);
