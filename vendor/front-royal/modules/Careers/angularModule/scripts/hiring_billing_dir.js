import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/hiring_billing.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import sharedHiringRelationshipCheck from 'vectors/shared-hiring-relationship-check.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('hiringBilling', [
    '$injector',

    function factory($injector) {
        const $rootScope = $injector.get('$rootScope');
        const DialogModal = $injector.get('DialogModal');
        const NavigationHelperMixin = $injector.get('Navigation.NavigationHelperMixin');
        const HiringTeamCheckoutHelper = $injector.get('Payments.HiringTeamCheckoutHelper');
        const TranslationHelper = $injector.get('TranslationHelper');
        const translationHelper = new TranslationHelper('careers.hiring_billing');

        return {
            restrict: 'E',
            templateUrl,
            scope: {
                inModal: '<?',
            },
            link(scope) {
                scope.sharedHiringRelationshipCheck = sharedHiringRelationshipCheck;

                NavigationHelperMixin.onLink(scope);

                Object.defineProperty(scope, 'currentUser', {
                    get() {
                        return $rootScope.currentUser;
                    },
                });

                Object.defineProperty(scope, 'currentPlan', {
                    get() {
                        return scope.currentUser && scope.currentUser.hiringPlan;
                    },
                });

                Object.defineProperty(scope, 'saving', {
                    get() {
                        return (
                            (scope.checkoutHelper && scope.checkoutHelper.stripeProcessing) ||
                            (scope.currentUser.hiring_team &&
                                scope.currentUser.hiring_team.primarySubscription &&
                                scope.currentUser.hiring_team.primarySubscription.$$saving)
                        );
                    },
                });

                scope.$watchGroup(['currentUser.hiring_team', 'currentPlan'], () => {
                    const hiringTeam = scope.currentUser && scope.currentUser.hiring_team;
                    if (hiringTeam) {
                        scope.checkoutHelper = new HiringTeamCheckoutHelper(hiringTeam);
                        scope.stripePlan = hiringTeam.stripePlanForHiringPlan(scope.currentPlan);
                    }
                });

                scope.goToBilling = () => {
                    DialogModal.removeAlerts();
                    scope.loadRoute('/settings/billing');
                };

                // always start with plans
                scope.showScreen = 'plans';

                scope.$watchGroup(
                    [
                        'currentUser.hiring_team',
                        'currentUser.isHiringTeamOwner',
                        'currentUser.hiring_team.primarySubscription',
                    ],
                    () => {
                        scope.selectedPlan = scope.currentUser.hiring_team.activeStripePlan;
                        scope.selectedPlanType = scope.selectedPlan && scope.selectedPlan.usage_type;
                        scope.selectedPlanName =
                            scope.selectedPlanType && translationHelper.get(scope.selectedPlanType);
                        scope.stripePlans = _.indexBy(scope.currentUser.hiring_team.stripe_plans, 'usage_type');
                    },
                );

                function onSuccess() {
                    scope.showScreen = 'success';
                }

                scope.subscribe = usageType => {
                    const plan = scope.stripePlans[usageType];
                    scope.checkoutHelper.subscribeToLegacyPlan(plan.id).then(onSuccess);
                };

                scope.modifyPaymentDetails = () => {
                    scope.checkoutHelper.modifyPaymentDetails();
                };

                scope.getStarted = () => {
                    // If this is in a modal, then close it.
                    DialogModal.hideAlerts();
                    $rootScope.goHome();
                };

                scope.contactOwner = () => {
                    scope.loadUrl(`mailto:${scope.currentUser.hiring_team.email}`, '_blank');
                };

                scope.cancelPlan = () => {
                    DialogModal.confirm({
                        confirmCallback() {
                            scope.currentUser.hiring_team.primarySubscription.destroy();
                        },
                        title: translationHelper.get('cancel_plan'),
                        text: translationHelper.get('are_you_sure'),
                        cancelButtonText: translationHelper.get('go_back'),
                        confirmButtonText: translationHelper.get('cancel_plan'),
                        cancelButtonClass: 'grey',
                        confirmButtonClass: 'red',
                    });
                };
            },
        };
    },
]);
