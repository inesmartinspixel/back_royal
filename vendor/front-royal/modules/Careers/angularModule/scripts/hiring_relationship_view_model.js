import angularModule from 'Careers/angularModule/scripts/careers_module';

angularModule.factory('HiringRelationshipViewModel', [
    '$injector',

    function factory($injector) {
        const SuperModel = $injector.get('SuperModel');
        const DialogModal = $injector.get('DialogModal');
        const $location = $injector.get('$location');
        const TranslationHelper = $injector.get('TranslationHelper');
        const $translate = $injector.get('$translate');
        const $q = $injector.get('$q');
        const $rootScope = $injector.get('$rootScope');
        const ConversationViewModel = $injector.get('ConversationViewModel');
        const Conversation = $injector.get('Mailbox.Conversation');
        const HttpQueue = $injector.get('HttpQueue');
        const ApiErrorHandler = $injector.get('ApiErrorHandler');
        const translationHelper = new TranslationHelper('careers.hiring_relationship_view_model');
        const HiringRelationship = $injector.get('HiringRelationship');

        const HiringRelationshipViewModel = SuperModel.subclass(function () {
            this.extend({});

            Object.defineProperty(this.prototype, 'conversation', {
                get() {
                    return this.hiringRelationship.conversation;
                },
            });

            Object.defineProperty(this.prototype, 'conversationViewModel', {
                get() {
                    if (!this.conversation) {
                        return null;
                    }

                    // after a save, the conversation may have been swapped out
                    if (this._conversationViewModel && this._conversationViewModel.conversation !== this.conversation) {
                        this._conversationViewModel = null;
                    }

                    if (!this._conversationViewModel) {
                        this._conversationViewModel = new ConversationViewModel(
                            this.conversation,
                            this.currentUser,
                            this.careersNetworkViewModel,
                        );
                    }
                    return this._conversationViewModel;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'closed', {
                get() {
                    return this.hiringRelationship.closed;
                },
                configurable: true,
            });

            // false if this relationship belongs to your teammate
            Object.defineProperty(this.prototype, 'isMyRelationship', {
                get() {
                    return this.role === 'hiringManager' || this.role === 'candidate';
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasHiringRole', {
                get() {
                    return this.role === 'hiringManager' || this.role === 'hiringTeammate';
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'closable', {
                get() {
                    return (
                        this.isMyRelationship &&
                        !this.closed &&
                        this.ourStatus === 'accepted' &&
                        this.connectionStatus !== 'rejected'
                    );
                },
            });

            Object.defineProperty(this.prototype, 'closedProp', {
                get() {
                    if (!this.isMyRelationship) {
                        return null;
                    }
                    return this.role === 'hiringManager' ? 'hiring_manager_closed' : 'candidate_closed';
                },
            });

            Object.defineProperty(this.prototype, 'closedByMe', {
                get() {
                    if (!this.isMyRelationship) {
                        return null;
                    }
                    return !!this.hiringRelationship[this.closedProp];
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'closedByConnection', {
                get() {
                    return this.closed && !this.closedByMe;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'consideredByHiringManager', {
                get() {
                    return this.hiringRelationship.hiring_manager_status !== 'pending';
                },
            });

            Object.defineProperty(this.prototype, 'msSinceLastActivity', {
                get() {
                    return Date.now() - this.lastActivityAt;
                },
            });

            Object.defineProperty(this.prototype, 'lastActivityAt', {
                get() {
                    const vals = [1000 * this.hiringRelationship.updated_at];
                    if (this.conversation) {
                        vals.push(this.conversation.lastMessageAtTimestamp);
                    }
                    return _.max(vals);
                },
            });

            Object.defineProperty(this.prototype, 'careerProfile', {
                get() {
                    return this.hiringRelationship.career_profile;
                },
            });

            Object.defineProperty(this.prototype, 'hiringApplication', {
                get() {
                    return this.hiringRelationship.hiring_application;
                },
            });

            Object.defineProperty(this.prototype, 'ourAvatarSrc', {
                get() {
                    return this.ourProfile && this.ourProfile.avatar_url;
                },
            });

            Object.defineProperty(this.prototype, 'teammateAvatarSrc', {
                get() {
                    return this.role === 'hiringTeammate' && this.ourAvatarSrc;
                },
            });

            Object.defineProperty(this.prototype, 'connectionAvatarSrc', {
                get() {
                    return this._showPhotosAndNames ? this.connectionProfile.avatar_url : null;
                },
            });

            Object.defineProperty(this.prototype, 'connectionName', {
                get() {
                    return this._showPhotosAndNames
                        ? this.connectionProfile && this.connectionProfile.name
                        : this.connectionProfile.userInitials;
                },
            });

            Object.defineProperty(this.prototype, 'connectionNickname', {
                get() {
                    if (!this._showPhotosAndNames) {
                        return this.connectionProfile.userInitials;
                    }
                    return this.connectionProfile && (this.connectionProfile.nickname || this.connectionProfile.name);
                },
            });

            Object.defineProperty(this.prototype, 'connectionFormattedAddress', {
                get() {
                    return this.connectionProfile && this.connectionProfile.locationString;
                },
            });

            Object.defineProperty(this.prototype, 'connectionCompany', {
                get() {
                    if (this.hasHiringRole) {
                        return this.connectionProfile && this.connectionProfile.mostRecentPositionText;
                    }
                    return this.connectionProfile && this.connectionProfile.company_name;
                },
            });

            Object.defineProperty(this.prototype, 'hiringManagerCompany', {
                get() {
                    return (
                        this.hiringRelationship.hiring_application &&
                        this.hiringRelationship.hiring_application.company_name
                    );
                },
            });

            Object.defineProperty(this.prototype, 'resumeUrl', {
                get() {
                    if (this.hasHiringRole) {
                        if (
                            this.connectionProfile.resume &&
                            this.connectionProfile.resume.formats &&
                            this.connectionProfile.resume.formats.original &&
                            this.connectionProfile.resume.formats.original.url
                        ) {
                            return this.connectionProfile.resume.formats.original.url;
                        }
                        return false;
                    }
                    return false;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'matched', {
                get() {
                    return this.hiringRelationship.matched;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'rejectedByCandidate', {
                get() {
                    return this.hiringRelationship.candidate_status === 'rejected';
                },
                configurable: true,
            });

            // This is only used for when you go to /connections?connection-id=bob
            // and we need to figure out WHICH hiring relationship view model is the
            // one we should use to render the page.  Is it YOUR relationship with
            // bob, or your teammate's?  Since only one of those can have
            // hiring_manager_status === 'accepted', and only hiring_manager_status === 'accepted'
            // relationships can have conversations, we can use this property to
            // filter for the ONE relationship that is relevant on that page.
            Object.defineProperty(this.prototype, 'acceptedByHiringManager', {
                get() {
                    return this.hiringRelationship.hiring_manager_status === 'accepted';
                },
            });

            Object.defineProperty(this.prototype, 'connectionProfile', {
                get() {
                    if (this.hasHiringRole) {
                        return this.hiringRelationship.career_profile;
                    }
                    return this.hiringRelationship.hiring_application;
                },
            });

            Object.defineProperty(this.prototype, 'ourProfile', {
                get() {
                    if (this.role === 'candidate') {
                        return this.hiringRelationship.career_profile;
                    }
                    if (this.hasHiringRole) {
                        return this.hiringRelationship.hiring_application;
                    }
                    return undefined;
                },
            });

            Object.defineProperty(this.prototype, 'hasMessages', {
                get() {
                    return this.conversation && this.conversation.hasMessages;
                },
            });

            Object.defineProperty(this.prototype, 'hasUnread', {
                get() {
                    return this.conversationViewModel ? this.conversationViewModel.hasUnread : false;
                },
            });

            Object.defineProperty(this.prototype, 'hasMessageFromUs', {
                get() {
                    return _.any(this.messagesFromUs);
                },
            });

            Object.defineProperty(this.prototype, 'needsResponseFromMe', {
                get() {
                    return (
                        this.conversationViewModel &&
                        this.conversationViewModel.mostRecentMessage.sender_id !== this.ourId
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'newInstantMatch', {
                get() {
                    // this is "new" whether they have read the conversation or not
                    return (
                        this.role === 'candidate' &&
                        this.matchedByHiringManager &&
                        !this.closed &&
                        !this.rejectedByCandidate &&
                        !this.hasMessageFromUs
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'hasOpenPositionId', {
                get() {
                    return this.hiringRelationship && !!this.hiringRelationship.open_position_id;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'openPositionId', {
                get() {
                    return this.hiringRelationship && this.hiringRelationship.open_position_id;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'invitedPosition', {
                get() {
                    if (this.conversation && this.conversation.messages) {
                        const metadata = _.chain(this.conversation.messages)
                            .values()
                            .flatten()
                            .pluck('metadata')
                            .filter('openPosition')
                            .value();
                        return metadata[0] && metadata[0].openPosition;
                    }

                    return undefined;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'matchedByHiringManager', {
                get() {
                    return !!this.hiringRelationship.matched_by_hiring_manager;
                },
                configurable: true,
            });

            // There is more than one way to create a matched HiringRelationship.
            // Depending on how a HiringRelationship was created, newMatch can have
            // different meaning/purpose at the time the HiringRelationship is
            // considered matched.
            //
            // Invite to Interview (Browse):
            // If a candidate responds to an invite to interview, creating a matched
            //  HiringRelationship, newMatch is true ONLY when the hiring manager has
            //  sent a single message (the initial invite) AND the hiring manager has
            //  not read any messages from the candidate.
            //
            // Connect Now (Positions):
            // If a hiring manager creates a matched HiringRelationship by electing to connect
            //  now with a candidate who has expressed interest in one of the hiring manager's
            //  OpenPositions (matchedByHiringManager), newMatch is NEVER true.
            Object.defineProperty(this.prototype, 'newMatch', {
                get() {
                    // this is only true if you're a hiring manager or teammate
                    if (!this.hasHiringRole) {
                        return false;
                    }

                    // this is only true for matches
                    if (!this.matched) {
                        return false;
                    }

                    //----------------------------------------
                    // Matched by hiring manager (connect now)
                    //----------------------------------------
                    //
                    // newMatch should be false if the relationship was matchedByHiringManager
                    if (this.matchedByHiringManager) {
                        return false;
                    }

                    // this is always true if there is no conversation
                    if (!this.conversationViewModel) {
                        return true;
                    }

                    //-------------------------------------------------
                    // Matched by candidate (invite to Interview, like)
                    //-------------------------------------------------
                    //
                    // newMatch should be false if the hiring manager has sent more than one message,
                    // or the hiring manager has read any messages from the candidate
                    if (
                        this.conversationViewModel.messagesFrom(this.hiringRelationship.hiring_manager_id).length > 1 ||
                        !this.conversationViewModel.allUnreadFrom(this.hiringRelationship.candidate_id, this.ourId)
                    ) {
                        return false;
                    }
                    return true;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'newRequest', {
                get() {
                    return (
                        this.role === 'candidate' &&
                        this.hiringRelationship.hiring_manager_status === 'accepted' &&
                        this.hiringRelationship.candidate_status === 'pending'
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'connectionPending', {
                get() {
                    return (
                        this.hasHiringRole &&
                        this.hiringRelationship.hiring_manager_status === 'accepted' &&
                        this.hiringRelationship.candidate_status === 'pending'
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'ourId', {
                get() {
                    if (this.role === 'candidate') {
                        return this.hiringRelationship.candidate_id;
                    }

                    if (this.hasHiringRole) {
                        return this.hiringRelationship.hiring_manager_id;
                    }

                    return undefined;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'myStatus', {
                get() {
                    if (this.role === 'candidate') {
                        return this.hiringRelationship.candidate_status;
                    }

                    if (this.role === 'hiringManager') {
                        return this.hiringRelationship.hiring_manager_status;
                    }
                    return null;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'ourStatus', {
                get() {
                    if (this.role === 'candidate') {
                        return this.hiringRelationship.candidate_status;
                    }

                    if (this.hasHiringRole) {
                        return this.hiringRelationship.hiring_manager_status;
                    }

                    return undefined;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'status', {
                get() {
                    return this.hiringRelationship.status;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'connectionStatus', {
                get() {
                    if (this.hasHiringRole) {
                        return this.hiringRelationship.candidate_status;
                    }
                    return this.hiringRelationship.hiring_manager_status;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, '$$saving', {
                get() {
                    return this.hiringRelationship.$$saving || (this.conversation && this.conversation.$$saving);
                },
            });

            Object.defineProperty(this.prototype, '_showPhotosAndNames', {
                get() {
                    return (
                        !this.currentUser || this.currentUser.pref_show_photos_names || this.hiringRelationship.matched
                    );
                },
            });

            Object.defineProperty(this.prototype, 'searchableText', {
                get() {
                    if (!this.$$searchableText) {
                        const profile = this.connectionProfile;
                        this.$$searchableText = _.flatten([
                            profile.name,
                            profile.locationString,
                            profile.personal_fact,
                            _.map(
                                profile.workExperiences,
                                w => `${w.orgName} ${w.job_title} ${w.responsibilities.join(' ')}`,
                            ),
                            _.map(profile.educationExperiences, e => e.orgName),
                            _.map(profile.skills, s => s.text),
                            _.map(profile.awards_and_interests, s => s.text),
                            _.map(profile.top_personal_descriptors, v =>
                                $translate.instant(`careers.field_options.${v}`),
                            ),
                            _.map(profile.top_workplace_strengths, v =>
                                $translate.instant(`careers.field_options.${v}`),
                            ),
                            _.map(profile.primary_areas_of_interest, v =>
                                $translate.instant(`careers.field_options.${v}`),
                            ),
                            _.map(profile.job_sectors_of_interest, v =>
                                $translate.instant(`careers.field_options.${v}`),
                            ),
                        ])
                            .join(' ')
                            .toLowerCase();
                    }
                    return this.$$searchableText;
                },
            });

            Object.defineProperty(this.prototype, 'canSendMessage', {
                get() {
                    return !this.closed && this.ourStatus !== 'rejected';
                },
            });

            Object.defineProperty(this.prototype, 'ourParticipantId', {
                get() {
                    if (this.hasHiringRole) {
                        return this.hiringRelationship.hiring_manager_id;
                    }

                    if (this.role === 'candidate') {
                        return this.hiringRelationship.candidate_id;
                    }

                    return undefined;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'theirParticipantId', {
                get() {
                    if (this.hasHiringRole) {
                        return this.hiringRelationship.candidate_id;
                    }

                    if (this.role === 'candidate') {
                        return this.hiringRelationship.hiring_manager_id;
                    }

                    return undefined;
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'messagesFromMe', {
                get() {
                    if (!this.conversationViewModel) {
                        return [];
                    }

                    return this.conversationViewModel.messagesFrom(this.currentUser.id);
                },
            });

            Object.defineProperty(this.prototype, 'messagesFromUs', {
                get() {
                    const self = this;
                    return _.select(self.conversation && self.conversation.messages, message =>
                        self.messageIsSentByUs(message),
                    );
                },
            });

            Object.defineProperty(this.prototype, 'hasMultipleHiringTeammatesParticipatingInConversation', {
                get() {
                    const self = this;

                    // If two people who are not candidates have sent messages, then this is true
                    // (See comment in messageIsSentByUs about assuming that all participants who
                    // are not candidates are hiring teammates)
                    return (
                        _.chain(self.conversation && self.conversation.messages)
                            .pluck('sender_id')
                            .uniq()
                            .select(senderId => senderId !== self.hiringRelationship.candidate_id)
                            .value().length > 1
                    );
                },
                configurable: true,
            });

            Object.defineProperty(this.prototype, 'theirDisplayNameForConversation', {
                get() {
                    if (this.role === 'candidate' && this.hasMultipleHiringTeammatesParticipatingInConversation) {
                        return this.connectionCompany;
                    }
                    return this.connectionNickname;
                },
                configurable: true,
            });

            return {
                // In most cases, this is initialized with a careersNetworkViewModel.
                //
                // When calling getOrInitializeHiringRelationshipViewModelForCareerProfile, this will
                // be initialized with a currentUser but not a careersNetworkViewModel. In that
                // case addToCareersNetworkViewModelOnSave will be set.
                initialize(hiringRelationship, careersNetworkViewModel, role, currentUser) {
                    this.hiringRelationship = hiringRelationship;
                    this.hiringRelationshipId = hiringRelationship.id;

                    if (!careersNetworkViewModel && role) {
                        this.role = role;
                    } else if (
                        careersNetworkViewModel.role === 'candidate' &&
                        hiringRelationship.candidate_id === careersNetworkViewModel.user.id
                    ) {
                        this.role = 'candidate';
                    } else if (
                        careersNetworkViewModel.role === 'hiringManager' &&
                        hiringRelationship.hiring_manager_id === careersNetworkViewModel.user.id
                    ) {
                        this.role = 'hiringManager';
                    } else if (
                        careersNetworkViewModel.role === 'hiringManager' &&
                        hiringRelationship.hiringTeamId === careersNetworkViewModel.user.hiring_team_id
                    ) {
                        this.role = 'hiringTeammate';
                    } else {
                        throw new Error('Cannot determine role');
                    }

                    this.careersNetworkViewModel = careersNetworkViewModel;
                    this.currentUser = careersNetworkViewModel ? careersNetworkViewModel.user : currentUser;

                    this.connectionId = this.hasHiringRole
                        ? hiringRelationship.candidate_id
                        : hiringRelationship.hiring_manager_id;
                },

                startConversation(attrs = {}) {
                    if (!this.isMyRelationship) {
                        throw new Error('Only for participants in relationship.');
                    }
                    this.hiringRelationship.conversation = Conversation.new(attrs)
                        .addUser(this.currentUser.id, this.currentUser.name, this.currentUser.avatar_url)
                        .addUser(this.connectionId, this.connectionName, this.connectionAvatarSrc);

                    this.conversation.subject = 'NO SUBJECT'; // this has to be set

                    return this.conversationViewModel;
                },

                addMessage(body, metadata, conversationAttrs = {}) {
                    const notAccepted = this.ourStatus !== 'accepted';

                    if (!this.isMyRelationship && (notAccepted || !this.conversationViewModel)) {
                        throw new Error('Cannot send message in a non-active relationship that is not mine.');
                    }

                    if (notAccepted) {
                        this.updateStatus('accepted');
                    }

                    if (!this.conversationViewModel) {
                        this.startConversation(conversationAttrs);
                    }

                    return this.conversationViewModel.addMessage(body, metadata);
                },

                sendMessage(body) {
                    this.addMessage(body);
                    this.save();
                },

                showUpdateModal() {
                    const self = this;
                    if (this.role === 'candidate') {
                        DialogModal.alert({
                            content:
                                '<hiring-manager-card hiring-application="hiringApplication"></hiring-manager-card>',
                            classes: ['transparent', 'hiring-manager-card-modal'],
                            blurTargetSelector: 'div[ng-controller]',
                            scope: {
                                hiringApplication: this.connectionProfile,
                            },
                        });
                    } else {
                        this.careersNetworkViewModel.showFullProfile({
                            showInvite: false,
                            showActions: false,
                            careerProfile: self.connectionProfile,
                        });
                    }
                },

                openConnection() {
                    // If not on /connections go there, then set connectionId query param
                    let path;
                    if (this.hasHiringRole) {
                        path = '/hiring/tracker';
                    } else if (this.role === 'candidate') {
                        path = '/careers/connections';
                    }
                    if ($location.$$path !== path) {
                        $location.path(path);
                    }
                    $location.search('connectionId', this.connectionId);
                },

                updateStatus(status) {
                    if (!this.isMyRelationship) {
                        throw new Error('Only for participants in relationship.');
                    }
                    const prop = this.role === 'hiringManager' ? 'hiring_manager_status' : 'candidate_status';
                    this.hiringRelationship[prop] = status;
                    $rootScope.$broadcast('hiringRelationship:updateStatus', this);
                },

                decline() {
                    if (!this.isMyRelationship) {
                        throw new Error('Only for participants in relationship.');
                    }
                    if (this.role === 'candidate') {
                        const hiringRelationshipViewModel = this;

                        DialogModal.confirm({
                            text: translationHelper.get('are_you_sure', {
                                nickname: this.connectionNickname,
                            }),
                            confirmCallback() {
                                hiringRelationshipViewModel._rejectOrCloseViewModel();
                            },
                        });
                    } else if (this.role === 'hiringManager') {
                        DialogModal.alert({
                            content:
                                '<hiring-close-connection hiring-relationship-view-model="hiringRelationshipViewModel"></hiring-close-connection>',
                            scope: {
                                hiringRelationshipViewModel: this,
                            },
                            size: 'large',
                            hideCloseButton: false,
                            classes: ['blue', 'hiring-welcome-modal', 'no-title'],
                            blurTargetSelector: 'div[ng-controller]',
                        });
                    } else {
                        throw new Error(`Unexpected role: ${this.role}`);
                    }
                },

                navigateToTracker() {
                    $location.search('connectionId', null);
                },

                save(options) {
                    const self = this;
                    const metadata = {};
                    options = options || {};

                    const openPosition = options.openPosition;
                    if (openPosition) {
                        // It's not that important, but there is no need to send up a whole copy of the
                        // hiring application here
                        metadata.open_position = _.omit(openPosition.asJson(), 'hiring_application');
                    }

                    const candidatePositionInterest = options.candidatePositionInterest;
                    if (candidatePositionInterest) {
                        metadata.candidate_position_interest = candidatePositionInterest.asJson();
                        $rootScope.$broadcast('candidatePositionInterestUpdate');

                        // If we are inviting someone to interview, or 'connecting now' from positions,
                        // inviteToInterviewForm will explicity set open_position_id on the hiringRelationship.
                        //
                        // We also want to set open_position_id on the hiringRelationship if we are SAVING
                        // a candidate from positions.
                        if (
                            self.hiringRelationship.hiring_manager_status === 'saved_for_later' &&
                            !self.hiringRelationship.open_position_id
                        ) {
                            self.hiringRelationship.open_position_id = candidatePositionInterest.open_position_id;
                        }
                    }

                    // if this was created with getOrInitializeHiringRelationshipViewModelForCareerProfile,
                    // then there might be a foundWithSearchId to save
                    if (self.foundWithSearchId) {
                        metadata.found_with_search_id = self.foundWithSearchId;
                        self.foundWithSearchId = null;
                    }

                    const targetStatus = self.myStatus;
                    return self.hiringRelationship
                        .save(metadata, {
                            'FrontRoyal.ApiErrorHandler': {
                                skip: true,
                            },
                        })
                        .then(
                            response => {
                                // if this was created with getOrInitializeHiringRelationshipViewModelForCareerProfile,
                                // then we might need to add it to a careersNetworkViewModel now that it is saved
                                if (self.addToCareersNetworkViewModelOnSave) {
                                    self.careersNetworkViewModel = self.addToCareersNetworkViewModelOnSave;
                                }

                                self._showLikedFirstCandidateModalIfNecessary();

                                if (self.addToCareersNetworkViewModelOnSave) {
                                    // _onHiringRelationshipsLoaded changes the hasNotYetLikedFirstCandidate boolean attribute
                                    // on the careersNetworkViewModel and since _showLikedFirstCandidateModalIfNecessary depends
                                    // on the value of hasNotYetLikedFirstCandidate BEFORE the hiring relationship has been saved,
                                    // we need to call _onHiringRelationshipsLoaded AFTER _showLikedFirstCandidateModalIfNecessary.
                                    // Otherwise, the liked-first-candidate-modal may not show up.
                                    self.careersNetworkViewModel._onHiringRelationshipsLoaded([
                                        self.hiringRelationship,
                                    ]);
                                    self.addToCareersNetworkViewModelOnSave = null;
                                }

                                self._handlePushedUpdates(response.meta);

                                // bump the updated_at so careers network view model doesn't reload hiring relationships
                                // after saves made in this browser.
                                self.careersNetworkViewModel.bumpLastRelationshipUpdatedAt(self.hiringRelationship);

                                if (response.meta) {
                                    // An openPosition was passed in with the save
                                    if (response.meta.open_position) {
                                        options.openPosition.copyAttrs(response.meta.open_position);
                                    }
                                    // A candidatePositionInterest was passed in with the save
                                    if (response.meta.candidate_position_interest) {
                                        options.candidatePositionInterest.copyAttrs(
                                            response.meta.candidate_position_interest,
                                        );
                                    }
                                }

                                // broadcast event notifying any listeners of change
                                // see connections_list_dir.js
                                $rootScope.$broadcast('hiringRelationshipUpdate');
                            },
                            response => {
                                // On a multiple connections error, we should always have something pushed
                                // down, since the error was caused by us not knowing the state of the
                                // db.  Note that here we are getting the raw response from $http, so
                                // meta is in response.data.meta, rather than directly in response.meta
                                self._handlePushedUpdates(response.data && response.data.meta);
                                return self._handleSaveError(response, targetStatus);
                            },
                        );
                },

                _handlePushedUpdates(meta) {
                    // If there were side affects when saving this relationships (like teammates'
                    // relationships changing status)
                    if (meta) {
                        if (meta.hiring_relationships) {
                            // even if there was an error which means that we are not adding THIS
                            // hiring relationship to the careersNetworkViewModel, we should still
                            // update the relevant careersNetworkViewModel with the side affects.
                            const careersNetworkViewModel =
                                this.careersNetworkViewModel || this.addToCareersNetworkViewModelOnSave;
                            const hiringRelationships = _.map(meta.hiring_relationships, attrs =>
                                HiringRelationship.new(attrs),
                            );
                            careersNetworkViewModel._onHiringRelationshipsLoaded(hiringRelationships);
                        }

                        // The save had candidatePositionInterest side effects
                        if (meta.candidate_position_interests) {
                            this.careersNetworkViewModel.setFeaturedPositionsNumNotifications();
                        }
                    }
                },

                markAllAsRead() {
                    if (
                        this.currentUser.ghostMode ||
                        !this.conversationViewModel ||
                        !this.conversation.isRecipient(this.currentUser.id)
                    ) {
                        return $q.when();
                    }

                    if (this.conversationViewModel.markAllAsRead()) {
                        return this.save();
                    }
                    return $q.when();
                },

                close() {
                    if (!this.isMyRelationship) {
                        throw new Error('Only for participants in relationship.');
                    }
                    if (this.closedByMe) {
                        return $q.when();
                    }

                    // Trashing the messages isn't really necessary anymore, except to support the case where this
                    // hiring manager views this relationship on an old client after closing it on
                    // a new client.  Since we added the concept of closing a relationship, nothing
                    // in the UI pays attention to whether a message is marked as trashed (this used to
                    // be how we archived conversations)
                    if (this.conversationViewModel) {
                        this.conversationViewModel.updateAllReceipts({
                            trashed: true,
                        });
                    }

                    this.hiringRelationship[this.closedProp] = true;
                },

                messageIsUnreadByAllOfThem(message) {
                    if (!this.messageIsSentByUs(message)) {
                        return false;
                    }

                    const self = this;
                    const readReceipt = _.detect(
                        message.receipts,
                        receipt => !self._userIdIsUs(receipt.receiver_id) && receipt.is_read,
                    );
                    return !readReceipt;
                },

                messageIsSentByUs(message) {
                    return this._userIdIsUs(message.sender_id);
                },

                _userIdIsUs(id) {
                    if (this.role === 'candidate') {
                        return id === this.currentUser.id;
                    }
                    if (this.hasHiringRole) {
                        // I don't like to just assume here that any message not sent
                        // by the candidate is sent by a teammate, as it's not totally
                        // future proof.  But it is safe for now and I don't have a
                        // good way to confirm that a message is from a teammate.
                        return id !== this.hiringRelationship.candidate_id;
                    }
                },

                _handleSaveError(response, targetStatus) {
                    const self = this;
                    let errorType;
                    let hiringRelationshipJson;
                    let teammateName;
                    try {
                        hiringRelationshipJson = response.data.meta.hiring_relationship;
                        errorType = response.data.meta.error_type;
                        teammateName = response.data.meta.teammate_name;
                        // eslint-disable-next-line no-empty
                    } catch (e) {}

                    // FIXME: Now that we allow 409 through our error_log_service we could transition this
                    // 406 response to a 409 on the server and avoid the FrontRoyal.ApiErrorHandler.skip stuff.
                    if (
                        response.status === 406 &&
                        errorType === 'multiple_team_connections_to_candidate' &&
                        teammateName &&
                        hiringRelationshipJson
                    ) {
                        // revert the hiring relationship back to how the server says it is
                        self.hiringRelationship.copyAttrs(hiringRelationshipJson);
                        // tell the world that hiring relationships have changed
                        $rootScope.$broadcast('hiringRelationshipUpdate');

                        let reject;
                        const promise = $q((ignore, _reject) => {
                            reject = _reject;
                        });

                        // If we were trying to reject the candidate, there is no reason
                        // to show the error to the user.  We just hide the candidate
                        if (targetStatus === 'rejected') {
                            reject();
                        }

                        // See https://trello.com/c/gzdhD7bn.  If a user has been shared with us, then
                        // we will try to create a pending relationship.  If we hit a conflict in trying
                        // to create that relationship, then we can just go to the connection page for the
                        // user.
                        else if (targetStatus === 'pending') {
                            self.openConnection();
                            reject();
                        } else {
                            DialogModal.alert({
                                content: translationHelper.get('candidate_already_accepted_msg', {
                                    // NOTE: we decided to use full names here instead of nicknames.
                                    // Ori said:
                                    //      so in this case, you’re looking at a candidate for the first time
                                    //      all you know is their full name based on the card
                                    //      it’s always possible their nickname is slightly different
                                    //      so maybe full name would be safest in *this* scenario?
                                    //      I’m not sure it really matters
                                    //      haha
                                    //      teammate I kinda feel the same way
                                    teammateName,
                                    candidateName: self.connectionName,
                                }),
                                classes: ['server-error-modal', 'small'],
                                title: translationHelper.get('candidate_already_accepted'),
                                close: reject,
                            });
                        }
                        HttpQueue.unfreezeAfterError(response.config);

                        return promise;
                    }

                    if (response.status === 409 && errorType === 'interest_conflict') {
                        DialogModal.alert({
                            content: $translate.instant('careers.careers_network_view_model.interest_save_conflict'),
                        });
                        self.careersNetworkViewModel.onCandidatePositionInterestConflict();
                        HttpQueue.unfreezeAfterError(response.config);

                        return $q.when(response);
                    }
                    if (response.config) {
                        response.config['FrontRoyal.ApiErrorHandler'].skip = false;
                    }
                    return ApiErrorHandler.onResponseError(response);
                },

                _showLikedFirstCandidateModalIfNecessary() {
                    if (
                        this.role === 'hiringManager' &&
                        this.careersNetworkViewModel.hasNotYetLikedFirstCandidate &&
                        this.hiringRelationship.hiring_manager_status === 'accepted'
                    ) {
                        const $timeout = $injector.get('$timeout');
                        $timeout(() => {
                            DialogModal.alert({
                                content: '<liked-candidate-modal></liked-candidate-modal>',
                                title: new TranslationHelper('careers.liked_first_candidate_modal').get('title'),
                                size: 'small',
                                classes: ['dark', 'rounded', 'static', 'front-royal-modal', 'blue'],
                                hideCloseButton: true,
                                closeOnClick: false,
                                scope: {},
                                blurTargetSelector: 'div[ng-controller]',
                            });
                        }, 500);

                        this.careersNetworkViewModel.hasNotYetLikedFirstCandidate = false;
                    }
                },

                _rejectOrCloseViewModel() {
                    const hiringRelationshipViewModel = this;

                    if (hiringRelationshipViewModel.ourStatus === 'pending') {
                        hiringRelationshipViewModel.updateStatus('rejected');
                    } else {
                        hiringRelationshipViewModel.close();
                    }

                    // neither operation saves implicitly
                    hiringRelationshipViewModel.save();
                    hiringRelationshipViewModel.navigateToTracker();
                },
            };
        });

        return HiringRelationshipViewModel;
    },
]);
