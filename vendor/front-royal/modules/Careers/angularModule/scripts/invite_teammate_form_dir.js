import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/invite_teammate_form.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

import sharedHiringRelationshipCheck from 'vectors/shared-hiring-relationship-check.svg';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('inviteTeammateForm', [
    '$injector',

    function factory($injector) {
        const HiringTeamInvite = $injector.get('HiringTeamInvite');
        const $timeout = $injector.get('$timeout');
        const $rootScope = $injector.get('$rootScope');
        const DialogModal = $injector.get('DialogModal');
        const TranslationHelper = $injector.get('TranslationHelper');
        const HttpQueue = $injector.get('HttpQueue');
        const ApiErrorHandler = $injector.get('ApiErrorHandler');

        return {
            scope: {
                onFinish: '<',
            },
            restrict: 'E',
            templateUrl,

            link(scope) {
                scope.sharedHiringRelationshipCheck = sharedHiringRelationshipCheck;

                scope.emailDomain = $rootScope.currentUser.emailDomain;

                scope.invite = () => {
                    if (scope.disableForm) {
                        return undefined;
                    }

                    scope.disableForm = true;

                    return HiringTeamInvite.create(
                        {
                            inviter_id: $rootScope.currentUser.id,
                            invitee_email: scope.invitation.email,
                            invitee_name: scope.invitation.name,
                        },
                        {},
                        {
                            'FrontRoyal.ApiErrorHandler': {
                                skip: true,
                            },
                        },
                    )
                        .then(
                            () => {
                                scope.inviteSent = true;
                            },
                            response => {
                                let errorType;
                                let inviteeHiringTeamId;
                                let inviteeProvider;
                                try {
                                    errorType = response.data.meta.error_type;
                                    inviteeHiringTeamId = response.data.meta.invitee_hiring_team_id;
                                    inviteeProvider = response.data.meta.invitee_provider;
                                    // eslint-disable-next-line no-empty
                                } catch (e) {}

                                if (response.status === 406 && errorType === 'invitee_exists') {
                                    const translationHelper = new TranslationHelper('careers.invite_teammate');

                                    let localeKey;
                                    if (inviteeHiringTeamId !== $rootScope.currentUser.hiring_team_id) {
                                        localeKey = 'contact_smartly';
                                    } else if (inviteeProvider === 'hiring_team_invite') {
                                        localeKey = 'invitee_already_invited';
                                    } else {
                                        localeKey = 'invitee_already_on_team';
                                    }

                                    DialogModal.alert({
                                        content: translationHelper.get(`${localeKey}_msg`, {
                                            inviteeName: scope.invitation.name,
                                            inviteeEmail: scope.invitation.email,
                                        }),
                                        classes: ['server-error-modal', 'small'],
                                        title: translationHelper.get(localeKey),
                                    });
                                    HttpQueue.unfreezeAfterError(response.config);

                                    // prevent the onFinish from happening.  We've opened
                                    // a new modal and the user needs to close it
                                    throw response;
                                } else {
                                    if (response.config) {
                                        response.config['FrontRoyal.ApiErrorHandler'].skip = false;
                                    }
                                    return ApiErrorHandler.onResponseError(response);
                                }
                            },
                        )
                        .then(() => $timeout(1750))
                        .then(() => {
                            if (scope.onFinish) {
                                scope.onFinish();
                            }
                        });
                };
            },
        };
    },
]);
