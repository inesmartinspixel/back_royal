import angularModule from 'Careers/angularModule/scripts/careers_module';
import template from 'Careers/angularModule/views/tabs.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('tabs', [
    '$injector',

    function factory($injector) {
        const scrollHelper = $injector.get('scrollHelper');
        const scopeTimeout = $injector.get('scopeTimeout');
        const ErrorLogService = $injector.get('ErrorLogService');

        return {
            scope: {
                role: '<',
                tabs: '<',
                currentTab: '=', // get sent back up to parent directive
                disableTab: '&?',
            },
            restrict: 'E',
            templateUrl,

            link(scope, elem, attrs) {
                scope.changeTab = tabName => {
                    scope.currentTab = tabName;
                };

                scope.$watch('currentTab', currentTab => {
                    // we put this action in a timeout so we ensure the directive is fully rendered
                    // the error logging doesn't give us enough data on it's own. see: https://trello.com/c/6INiwhq5
                    scopeTimeout(
                        scope,
                        () => {
                            let tabsWidth;
                            let currentTabElem;
                            let currentTabWidth;
                            let offset;
                            try {
                                if (currentTab && attrs.class === 'tab-style-slash') {
                                    tabsWidth = $(elem).width();
                                    currentTabElem = $(`button[name="${currentTab}"]`);
                                    currentTabWidth = currentTabElem.width();
                                    offset = ((tabsWidth - currentTabWidth) / 2.0) * -1;
                                    scrollHelper.scrollToElement(currentTabElem, true, offset, '.tabs', 0.5, 'x');
                                }
                            } catch (e) {
                                ErrorLogService.notify(e, undefined, {
                                    tabsWidth,
                                    currentTab,
                                    currentTabElem: currentTabElem && currentTabElem.length,
                                    currentTabWidth,
                                    offset,
                                    tabs: elem && elem.find('.tabs') && elem.find('.tabs').length,
                                });
                            }
                        },
                        1,
                    );
                });
            },
        };
    },
]);
