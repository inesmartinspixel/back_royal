import angularModule from 'Careers/angularModule/scripts/careers_module';

angularModule.factory('HiringTeamInvite', [
    '$injector',
    $injector => {
        const Iguana = $injector.get('Iguana');

        return Iguana.subclass(function () {
            this.setCollection('hiring_team_invites');
            this.alias('HiringTeamInvite');

            /*
                HiringTeamInvite.create({
                    inviter_id: currentUser.id,
                    invitee_email: email,
                    invitee_name: name
                })
            */

            return {};
        });
    },
]);
