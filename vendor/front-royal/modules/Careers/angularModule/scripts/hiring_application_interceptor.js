import angularModule from 'Careers/angularModule/scripts/careers_module';

angularModule.factory('HiringApplicationInterceptor', [
    '$injector',

    $injector => {
        const $rootScope = $injector.get('$rootScope');

        return {
            response(response) {
                let updatedProperties;
                try {
                    updatedProperties = response.data.meta.push_messages.hiring_application;
                } catch (e) {}

                const currentUser = $rootScope.currentUser;

                if (currentUser && currentUser.hiring_application && updatedProperties) {
                    // We use copyAttrs so that iguana will instantiate embedded objects,
                    // like cohort_applications
                    currentUser.hiring_application.copyAttrs(updatedProperties);
                }

                return response;
            },
        };
    },
]);

angularModule.config([
    '$httpProvider',
    $httpProvider => {
        $httpProvider.interceptors.push('HiringApplicationInterceptor');
    },
]);
