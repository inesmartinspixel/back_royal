import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/careers_spec_helper';
import setSpecLocales from 'Translation/setSpecLocales';
import hiringCloseConnectionLocales from 'Careers/locales/careers/hiring_close_connection-en.json';

setSpecLocales(hiringCloseConnectionLocales);

describe('FrontRoyal.Careers.HiringCloseConnection', () => {
    let $injector;
    let SpecHelper;
    let CareersSpecHelper;
    let renderer;
    let elem;
    let scope;
    let OpenPosition;
    let hiringRelationshipViewModel;
    let $q;
    let DialogModal;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                OpenPosition = $injector.get('OpenPosition');
                $q = $injector.get('$q');
                DialogModal = $injector.get('DialogModal');
            },
        ]);

        const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
        hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithNoConversation();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.hiringRelationshipViewModel = hiringRelationshipViewModel;
        renderer.render(
            '<hiring-close-connection hiring-relationship-view-model="hiringRelationshipViewModel"></hiring-close-connection>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should set the closed_info properly for reasons and feedback', () => {
        render();

        let resolveSave;
        jest.spyOn(hiringRelationshipViewModel, 'save').mockReturnValue(
            $q(resolve => {
                resolveSave = resolve;
            }),
        );

        SpecHelper.checkCheckbox(elem, '[type="checkbox"]', 0);
        SpecHelper.checkCheckbox(elem, '[type="checkbox"]', 1);
        SpecHelper.updateTextInput(elem, '#message', 'Some feedback');

        scope.decline();
        resolveSave();

        expect(scope.hiringRelationshipViewModel.hiringRelationship.hiring_manager_closed_info).toEqual({
            reasons: [scope.reasonKeys[0], scope.reasonKeys[1]],
            feedback: 'Some feedback',
        });
    });

    it('should show spinner while close request is in flight', () => {
        render();

        let resolveSave;
        jest.spyOn(hiringRelationshipViewModel, 'save').mockReturnValue(
            $q(resolve => {
                resolveSave = resolve;
            }),
        );

        SpecHelper.click(elem, '[name="close-connection"]');
        SpecHelper.expectElement(elem, 'front-royal-spinner');
        resolveSave();
        scope.$digest();
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
    });

    it('should close the connection and return the user to the tracker screen', () => {
        jest.spyOn(DialogModal, 'hideAlerts').mockImplementation(() => {});
        render();
        jest.spyOn(scope.hiringRelationshipViewModel, 'navigateToTracker').mockImplementation(() => {});
        jest.spyOn(scope.hiringRelationshipViewModel, 'close').mockImplementation(() => {});

        let resolveSave;
        jest.spyOn(hiringRelationshipViewModel, 'save').mockReturnValue(
            $q(resolve => {
                resolveSave = resolve;
            }),
        );

        SpecHelper.click(elem, '[name="close-connection"]');

        expect(hiringRelationshipViewModel.navigateToTracker).toHaveBeenCalled();
        expect(scope.declining).toBe(true);
        expect(scope.hiringRelationshipViewModel.close).toHaveBeenCalled();

        resolveSave();
        scope.$digest(); // force an event loop to see the scope update after the promise resolves

        expect(scope.declining).toBe(false);
        expect(DialogModal.hideAlerts).toHaveBeenCalled();
    });

    describe('interview process initiated', () => {
        beforeEach(() => {
            jest.spyOn(hiringRelationshipViewModel, 'invitedPosition', 'get', 'get').mockReturnValue(
                OpenPosition.new(),
            );
        });

        it('should show message about emailing candidate on close', () => {
            render();
            SpecHelper.expectElementText(
                elem,
                '.info',
                'Closing the interview process with a candidate confirms that you are no longer interested in them. Our team will notify the candidate that they are no longer being considered for your company.',
            );
        });
    });

    describe('interview process not initiated', () => {
        beforeEach(() => {
            jest.spyOn(hiringRelationshipViewModel, 'invitedPosition', 'get', 'get').mockReturnValue(undefined);
        });

        it('should not show message about emailing candidate on close', () => {
            render();
            SpecHelper.expectElementText(
                elem,
                '.info',
                'Closing the interview process with a candidate confirms that you are no longer interested in them.',
            );
        });
    });
});
