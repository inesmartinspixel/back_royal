import 'AngularSpecHelper';
import 'Careers/angularModule';

describe('FrontRoyal.Careers.CareerProfileFilterSet', () => {
    let $injector;
    let SpecHelper;
    let CareerProfileFilterSet;
    let CareerProfile;
    let $rootScope;
    let HiringApplication;
    let CareerProfileSearch;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');
                CareerProfileFilterSet = $injector.get('CareerProfileFilterSet');
                HiringApplication = $injector.get('HiringApplication');
                CareerProfileSearch = $injector.get('CareerProfileSearch');
                $rootScope = $injector.get('$rootScope');
            },
        ]);

        SpecHelper.stubCurrentUser();
    });

    describe('getProfilesRelativeToOffset', () => {
        let careerProfileFilterSet;

        beforeEach(() => {
            careerProfileFilterSet = new CareerProfileFilterSet({});
            jest.spyOn(careerProfileFilterSet, 'ensureCareerProfilesPreloaded').mockImplementation(() => {});
        });

        it('should ensureCareerProfilesPreloaded', () => {
            careerProfileFilterSet.getProfilesRelativeToOffset(0, 'first', 10);
            expect(careerProfileFilterSet.ensureCareerProfilesPreloaded).toHaveBeenCalled();
        });

        it('should throw error if action not provided', () => {
            expect(() => {
                careerProfileFilterSet.getProfilesRelativeToOffset();
            }).toThrowError('Unsupported pagination control action: undefined');
        });

        it('should throw error if action is unsupported', () => {
            expect(() => {
                careerProfileFilterSet.getProfilesRelativeToOffset(0, 'foo', 10);
            }).toThrowError('Unsupported pagination control action: foo');
        });

        describe("when action is 'first'", () => {
            it('should set offset to 0', () => {
                careerProfileFilterSet.offset = 10;
                careerProfileFilterSet.getProfilesRelativeToOffset(careerProfileFilterSet.offset, 'first', 10);

                expect(careerProfileFilterSet.offset).toEqual(0);
            });
        });

        describe("when action is 'previous'", () => {
            it('should set offset to 0 if decrementing offset by listLimit results in negative number', () => {
                const listLimit = 15; // listLimit needs to be greater than offset in this case
                careerProfileFilterSet.offset = 10;
                careerProfileFilterSet.getProfilesRelativeToOffset(
                    careerProfileFilterSet.offset,
                    'previous',
                    listLimit,
                );

                expect(careerProfileFilterSet.offset).toEqual(0);
            });

            it('should decrement offset by listLimit', () => {
                const listLimit = 3;
                careerProfileFilterSet.offset = 10;
                careerProfileFilterSet.getProfilesRelativeToOffset(
                    careerProfileFilterSet.offset,
                    'previous',
                    listLimit,
                );

                expect(careerProfileFilterSet.offset).toEqual(7);
            });
        });

        describe("when action is 'next'", () => {
            it('should increment offset by listLimit', () => {
                const listLimit = 5;
                careerProfileFilterSet.offset = 10;
                careerProfileFilterSet.getProfilesRelativeToOffset(careerProfileFilterSet.offset, 'next', listLimit);

                expect(careerProfileFilterSet.offset).toEqual(15);
            });
        });
    });

    describe('ensureCareerProfilesPreloaded', () => {
        let careerProfileFilterSet;

        beforeEach(() => {
            careerProfileFilterSet = new CareerProfileFilterSet({});
        });

        it('should set the total count', () => {
            CareerProfile.expect('index').returnsMeta({
                total_count: 1337,
            });
            careerProfileFilterSet.ensureCareerProfilesPreloaded();
            CareerProfile.flush('index');
            expect(careerProfileFilterSet.initialTotalCount).toBe(1337);

            // mock _allDataLoaded to give the appearance that not that all possible data has been loaded
            // for the purpose of this test
            careerProfileFilterSet._allDataLoaded = false;

            // Emulating the meta return from a subsequent page load
            CareerProfile.expect('index').returnsMeta({
                total_count: 1327,
            });
            careerProfileFilterSet.ensureCareerProfilesPreloaded();
            CareerProfile.flush('index');
            expect(careerProfileFilterSet.initialTotalCount).toBe(1337);
        });

        it('should do nothing if stack has 5 profiles already', () => {
            careerProfileFilterSet.careerProfiles = [{}, {}, {}, {}, {}];
            jest.spyOn(CareerProfile, 'index').mockImplementation(() => {});
            careerProfileFilterSet.ensureCareerProfilesPreloaded();
            expect(CareerProfile.index).not.toHaveBeenCalled();
        });

        it('should clone the filters so changes do not affect subsequent loads', () => {
            const filters = {
                places: [
                    {
                        new: 'york',
                    },
                ],
                preferred_primary_areas_of_interest: [],
                industries: [],
                years_experience: [],
                only_local: false,
                keyword_search: false,
                skills: [],
                employment_types_of_interest: undefined,
                levels_of_interest: undefined,
                company_name: undefined,
                school_name: undefined,
                in_school: undefined,
            };
            careerProfileFilterSet = new CareerProfileFilterSet(filters);
            expect(careerProfileFilterSet.filters).toEqual(filters);
            expect(careerProfileFilterSet.filters).not.toBe(filters);
            filters.places.push({
                washington: 'dc',
            });
            expect(careerProfileFilterSet.filters.places).toEqual([
                {
                    new: 'york',
                },
            ]);
        });

        it('should do nothing if all profiles have been loaded from the server', () => {
            // mock making a request that returns no results, indicating that
            // all available profiles have been loaded already
            CareerProfile.expect('index').returns([]);
            careerProfileFilterSet.ensureCareerProfilesPreloaded();
            CareerProfile.flush('index');

            jest.spyOn(CareerProfile, 'index').mockImplementation(() => {});
            careerProfileFilterSet.ensureCareerProfilesPreloaded();
            expect(CareerProfile.index).not.toHaveBeenCalled();
        });

        it('should load more data if there are fewer than 5 profiles in the stack', () => {
            const careerProfiles = [
                {
                    id: 1,
                },
                {
                    id: 2,
                },
            ];
            CareerProfile.expect('index').returns(careerProfiles);
            careerProfileFilterSet.ensureCareerProfilesPreloaded();
            CareerProfile.flush('index');
            expect(_.pluck(careerProfileFilterSet.careerProfiles, 'id')).toEqual([1, 2]);
        });

        it('should do nothing if already loading', () => {
            CareerProfile.expect('index').returns([
                {
                    id: 1,
                },
            ]);
            const promise = careerProfileFilterSet.ensureCareerProfilesPreloaded();

            // calling again before first request returns should return the same promise
            expect(careerProfileFilterSet.ensureCareerProfilesPreloaded()).toBe(promise);
            CareerProfile.flush('index');

            // calling again after the first request returns should return a new promise
            CareerProfile.expect('index');
            expect(careerProfileFilterSet.ensureCareerProfilesPreloaded()).not.toBe(promise);
        });

        it('should add a new career_profile_search to currentUser', () => {
            setupUserWithExistingCareerProfileSearches();

            CareerProfile.expect('index').returnsMeta({
                career_profile_search: {
                    id: 'newSearch',
                },
            });
            careerProfileFilterSet.ensureCareerProfilesPreloaded();
            CareerProfile.flush('index');
            expect(careerProfileFilterSet.searchId).toEqual('newSearch');

            const recentSearches = $rootScope.currentUser.hiring_application.recent_career_profile_searches;

            // the oldest one should have been popped off the list
            expect(_.pluck(recentSearches, 'id')).toEqual(['newSearch', 'existing1', 'existing2']);
            const lastSearch = recentSearches[0];
            expect(lastSearch.isA(CareerProfileSearch)).toBe(true);
            expect(lastSearch.id).toEqual('newSearch');
        });

        it('should move an existing career_profile_search to the front of the list', () => {
            setupUserWithExistingCareerProfileSearches();

            CareerProfile.expect('index').returnsMeta({
                career_profile_search: {
                    id: 'existing2',
                    prop: 'newValue',
                },
            });
            careerProfileFilterSet.ensureCareerProfilesPreloaded();
            CareerProfile.flush('index');
            expect(careerProfileFilterSet.searchId).toEqual('existing2');

            const recentSearches = $rootScope.currentUser.hiring_application.recent_career_profile_searches;
            expect(_.pluck(recentSearches, 'id')).toEqual(['existing2', 'existing1', 'existing3']);
            const lastSearch = recentSearches[0];
            expect(lastSearch.isA(CareerProfileSearch)).toBe(true);
            expect(lastSearch.id).toEqual('existing2');
            expect(lastSearch.prop).toEqual('newValue');
            expect(recentSearches[1].id).toEqual('existing1');
        });

        it('should set initialTotalCountIsMin when totalCount is equal to max_total_count', () => {
            asserSetsInitialTotalCountIsMin(100, true);
        });

        it('should set initialTotalCountIsMin when totalCount is less than max_total_count', () => {
            asserSetsInitialTotalCountIsMin(42, false);
        });

        it('should set derivative role filters when a lower-level category is selected', () => {
            const filters = {
                preferred_primary_areas_of_interest: ['human_resources'],
            };
            const filtersForApiRequest = new CareerProfileFilterSet(filters)._buildFiltersForApiRequest();
            expect(filtersForApiRequest.roles.preferred_primary_areas_of_interest).toEqual(['human_resources']);
            expect(filtersForApiRequest.roles.primary_areas_of_interest).toEqual([
                'human_resources',
                'general_management',
            ]);
            expect(filtersForApiRequest.roles.preferred_work_experience_roles).toEqual(['human_resources']);
            expect(filtersForApiRequest.roles.work_experience_roles).toEqual(['human_resources']);
        });

        it('should set derivative role filters when a higher-level category is selected', () => {
            const areaKeys = $injector.get('CAREERS_AREA_KEYS');
            const generalManagementChildren = areaKeys.general_management;
            const filters = {
                preferred_primary_areas_of_interest: ['general_management'],
            };
            const expectedAreas = ['general_management'].concat(generalManagementChildren);

            const filtersForApiRequest = new CareerProfileFilterSet(filters)._buildFiltersForApiRequest();
            expect(filtersForApiRequest.roles.preferred_primary_areas_of_interest).toEqual(['general_management']);
            expect(filtersForApiRequest.roles.primary_areas_of_interest).toEqual(expectedAreas);
            expect(filtersForApiRequest.roles.preferred_work_experience_roles).toEqual(['general_management']);
            expect(filtersForApiRequest.roles.work_experience_roles).toEqual(expectedAreas);
        });

        function asserSetsInitialTotalCountIsMin(totalCount, initialTotalCountIsMin) {
            setupUserWithExistingCareerProfileSearches();

            CareerProfile.expect('index')
                .toBeCalledWith({
                    filters: careerProfileFilterSet._buildFiltersForApiRequest(),
                    limit: 10,
                    view: 'career_profiles',
                    max_total_count: 100,
                    offset: 0,
                    sort: careerProfileFilterSet.sort,
                    direction: 'desc',
                    save_search: !careerProfileFilterSet._initialResultsLoaded,
                })
                .returnsMeta({
                    total_count: totalCount,
                });
            careerProfileFilterSet.ensureCareerProfilesPreloaded();
            CareerProfile.flush('index');
            expect(careerProfileFilterSet.initialTotalCount).toEqual(totalCount);
            expect(careerProfileFilterSet.initialTotalCountIsMin).toEqual(initialTotalCountIsMin);
        }

        function setupUserWithExistingCareerProfileSearches() {
            const user = SpecHelper.stubCurrentUser();
            user.hiring_application = HiringApplication.new();
            $rootScope.currentUser.hiring_application.recent_career_profile_searches = [
                CareerProfileSearch.new({
                    id: 'existing1',
                    prop: 'oldValue',
                }),
                CareerProfileSearch.new({
                    id: 'existing2',
                    prop: 'oldValue',
                }),
                CareerProfileSearch.new({
                    id: 'existing3',
                    prop: 'oldValue',
                }),
            ];

            return user;
        }
    });

    describe('states', () => {
        it('should be set appropriately in different parts of the loading cycle', () => {
            const careerProfiles = [
                {
                    id: 1,
                },
                {
                    id: 2,
                },
                {
                    id: 3,
                },
                {
                    id: 4,
                },
            ];
            const careerProfileFilterSet = new CareerProfileFilterSet({}, undefined, {
                serverLimit: careerProfiles.length - 1,
            });

            // before initial load
            expect(careerProfileFilterSet.any).toBe(false);
            expect(careerProfileFilterSet.searchingForMore).toBe(false);
            expect(careerProfileFilterSet.searchingForMoreWhenEmpty).toBe(true);
            expect(careerProfileFilterSet.showDisplayElement).toBe(true);
            expect(careerProfileFilterSet.noMoreAvailable).toBe(false);
            expect(careerProfileFilterSet.hasAttemptedToFetchInitialResults).toBe(false);

            // during initial load
            CareerProfile.expect('index').returns(careerProfiles);
            careerProfileFilterSet.ensureCareerProfilesPreloaded();
            expect(careerProfileFilterSet.any).toBe(false);
            expect(careerProfileFilterSet.searchingForMore).toBe(true); /* changed */
            expect(careerProfileFilterSet.searchingForMoreWhenEmpty).toBe(true);
            expect(careerProfileFilterSet.showDisplayElement).toBe(true);
            expect(careerProfileFilterSet.noMoreAvailable).toBe(false);
            expect(careerProfileFilterSet.hasAttemptedToFetchInitialResults).toBe(true); /* changed */

            // during next preload that returns fewer profiles than the serverLimit (flushing
            // the first index leads to ensureCareerProfilesPreloaded being called again, triggering
            // the next index)
            CareerProfile.expect('index').returns([
                {
                    id: 5,
                },
            ]);
            CareerProfile.flush('index');
            expect(careerProfileFilterSet.any).toBe(true);
            expect(careerProfileFilterSet.searchingForMore).toBe(true); /* changed */
            expect(careerProfileFilterSet.searchingForMoreWhenEmpty).toBe(false);
            expect(careerProfileFilterSet.showDisplayElement).toBe(true);
            expect(careerProfileFilterSet.noMoreAvailable).toBe(false);
            expect(careerProfileFilterSet.hasAttemptedToFetchInitialResults).toBe(true);

            CareerProfile.flush('index');
            expect(careerProfileFilterSet.any).toBe(true);
            expect(careerProfileFilterSet.searchingForMore).toBe(false); /* changed */
            expect(careerProfileFilterSet.searchingForMoreWhenEmpty).toBe(false);
            expect(careerProfileFilterSet.showDisplayElement).toBe(true);
            expect(careerProfileFilterSet.noMoreAvailable).toBe(true); /* changed */
            expect(careerProfileFilterSet.hasAttemptedToFetchInitialResults).toBe(true);

            // preloading when there is still something in the stack
            careerProfileFilterSet.ensureCareerProfilesPreloaded();
            expect(careerProfileFilterSet.any).toBe(true);
            expect(careerProfileFilterSet.searchingForMore).toBe(false);
            expect(careerProfileFilterSet.searchingForMoreWhenEmpty).toBe(false);
            expect(careerProfileFilterSet.showDisplayElement).toBe(true);
            expect(careerProfileFilterSet.noMoreAvailable).toBe(true);
            expect(careerProfileFilterSet.hasAttemptedToFetchInitialResults).toBe(true);

            // after the stack has been cleaned out
            careerProfileFilterSet.careerProfiles = [];
            expect(careerProfileFilterSet.any).toBe(false); /* changed */
            expect(careerProfileFilterSet.searchingForMore).toBe(false);
            expect(careerProfileFilterSet.searchingForMoreWhenEmpty).toBe(false);
            expect(careerProfileFilterSet.showDisplayElement).toBe(false); /* changed */
            expect(careerProfileFilterSet.noMoreAvailable).toBe(true);
            expect(careerProfileFilterSet.hasAttemptedToFetchInitialResults).toBe(true);
        });
    });
});
