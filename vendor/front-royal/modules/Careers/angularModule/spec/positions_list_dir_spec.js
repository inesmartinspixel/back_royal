import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_application_fixtures';
import 'Careers/angularModule/spec/careers_spec_helper';
import positionsLocales from 'Careers/locales/careers/positions-en.json';
import positionBarLocales from 'Careers/locales/careers/position_bar-en.json';
import connectionsLocales from 'Careers/locales/careers/connections-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(positionsLocales, positionBarLocales, connectionsLocales);

describe('FrontRoyal.Careers.PositionsList', () => {
    let $injector;
    let SpecHelper;
    let CareersSpecHelper;
    let renderer;
    let elem;
    let scope;
    let $timeout;
    let OpenPosition;
    let careersNetworkViewModel;
    let $q;
    let HiringApplication;
    let CandidatePositionInterest;
    let CareersNetworkViewModel;
    let PositionsWithInterestsHelper;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                OpenPosition = $injector.get('OpenPosition');
                $q = $injector.get('$q');
                $timeout = $injector.get('$timeout');
                CandidatePositionInterest = $injector.get('CandidatePositionInterest');
                CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
                PositionsWithInterestsHelper = $injector.get('PositionsWithInterestsHelper');
                HiringApplication = $injector.get('HiringApplication');
                $injector.get('HiringApplicationFixtures');
            },
        ]);

        careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
    });

    function render(archived) {
        renderer = SpecHelper.renderer();
        renderer.scope.archived = archived || false;
        renderer.render('<positions-list archived="archived"></positions-list>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should use PositionsWithInterestsHelper to loadPositionsAndInterests and then setupWatchers', () => {
        const deferred = $q.defer();
        jest.spyOn(PositionsWithInterestsHelper, 'loadPositionsAndInterests').mockReturnValue(deferred.promise);
        jest.spyOn(CareersNetworkViewModel, 'get').mockReturnValue(careersNetworkViewModel);
        render();
        jest.spyOn(scope, 'setupWatchers');

        expect(PositionsWithInterestsHelper.loadPositionsAndInterests).toHaveBeenCalledWith(careersNetworkViewModel);
        expect(scope.positionsWithInterestsHelper).toBeUndefined();
        expect(scope.setupWatchers).not.toHaveBeenCalled();

        const mockPositionsWithInterestsHelper = new PositionsWithInterestsHelper(careersNetworkViewModel);
        deferred.resolve(mockPositionsWithInterestsHelper);
        scope.$digest(); // propogate promise to `then` callback function

        expect(scope.positionsWithInterestsHelper).toBe(mockPositionsWithInterestsHelper);
        expect(scope.setupWatchers).toHaveBeenCalled();
    });

    describe('position list generation', () => {
        it('should be triggered by a $watch and refresh data when positions inserted/deleted', () => {
            stubPositions();
            render();

            jest.spyOn(scope.positionsWithInterestsHelper, 'clearInterestCachesForPosition');
            jest.spyOn(scope.positionsWithInterestsHelper, 'sortOpenPositions');
            expect(scope.positionLists.open.length).toEqual(1);

            // add an open position
            careersNetworkViewModel.openPositions.push(
                OpenPosition.new({
                    id: 5,
                    role: 'marketing', // if excluded, throws a missing translation error
                    featured: true,
                    hiring_manager_id: careersNetworkViewModel.user.id,
                    hiring_application: HiringApplication.fixtures.getInstance(),
                    auto_expiration_date: '2019-01-01',
                }),
            );
            scope.$digest();

            careersNetworkViewModel.openPositions.forEach(openPosition => {
                expect(scope.positionsWithInterestsHelper.clearInterestCachesForPosition).toHaveBeenCalledWith(
                    openPosition,
                );
            });
            scope.positionsWithInterestsHelper.clearInterestCachesForPosition.mockClear();

            expect(scope.positionsWithInterestsHelper.sortOpenPositions).toHaveBeenCalled();
            scope.positionsWithInterestsHelper.sortOpenPositions.mockClear();

            expect(scope.positionLists.open.length).toEqual(2);

            // remove the position
            careersNetworkViewModel.openPositions.splice(4, 1);
            scope.$digest();

            careersNetworkViewModel.openPositions.forEach(openPosition => {
                expect(scope.positionsWithInterestsHelper.clearInterestCachesForPosition).toHaveBeenCalledWith(
                    openPosition,
                );
            });
            expect(scope.positionsWithInterestsHelper.sortOpenPositions).toHaveBeenCalled();
            expect(scope.positionLists.open.length).toEqual(1);
        });

        it('should fire a watch when a position is archived/unarchived', () => {
            stubPositions();
            render();

            // Get the position with associated interests in stubPositions()
            const position = _.findWhere(scope.positionLists.open, {
                id: 1,
            });
            jest.spyOn(scope.positionsWithInterestsHelper, 'clearInterestCachesForPosition');
            expect(scope.positionLists.open.length).toEqual(1);

            position.archived = true;
            scope.$digest();

            careersNetworkViewModel.openPositions.forEach(openPosition => {
                expect(scope.positionsWithInterestsHelper.clearInterestCachesForPosition).toHaveBeenCalledWith(
                    openPosition,
                );
            });
            expect(scope.positionLists.open).toBe(undefined);
            render(true);
            expect(scope.positionLists.archived.length).toEqual(2);
        });
    });

    describe('empty states', () => {
        it('should render expected message when empty and archived === true', () => {
            stubPositions([]);
            render(true);
            SpecHelper.expectElementText(elem, '.no-positions p', 'Your closed positions will appear here.');
        });

        it('should render expected message when empty and archived !== true', () => {
            stubPositions([]);
            render();
            SpecHelper.expectElementText(
                elem,
                '.no-positions p',
                'Add your open positions to find interested, pre-qualified candidates. We’ll send you a weekly candidate summary to review.',
            );
        });

        it('should render expected message when the only positions to list are drafts and drafts should be expanded', () => {
            stubPositions([
                OpenPosition.new({
                    role: 'marketing',
                    hiring_manager_id: careersNetworkViewModel.user.id,
                    hiring_application: HiringApplication.fixtures.getInstance(),
                }),
            ]);
            render();
            SpecHelper.expectElements(elem, 'position-bar', 1); // should be expanded
            SpecHelper.expectElementText(
                elem,
                '.no-positions p',
                'Add your open positions to find interested, pre-qualified candidates. We’ll send you a weekly candidate summary to review.',
            );
        });
    });

    describe('position status filtering', () => {
        it('should render positions that are open', () => {
            stubPositions();
            render();

            SpecHelper.expectElement(elem, '.search-filters button');
            SpecHelper.expectElements(elem, '[section-name="\'open\'"] position-bar', 1);

            // expose drafts so we can count them
            scope.expandedSections.draft.expanded = true;
            scope.$apply();
            $timeout.flush();
            SpecHelper.expectElements(elem, '[section-name="\'draft\'"] position-bar', 1);
        });

        it('should render positions that are archived', () => {
            stubPositions();
            render(true);

            SpecHelper.expectNoElement(elem, '.search-filters button');
            SpecHelper.expectElement(elem, '[section-name="\'archived\'"] position-bar', 1);

            // expose drafts so we can count them
            scope.expandedSections.archived_draft.expanded = true;
            scope.$apply();
            $timeout.flush();
            SpecHelper.expectElement(elem, '[section-name="\'archived_draft\'"] position-bar', 1);
        });
    });

    describe('teammates filter', () => {
        it('should include names current list of teammates', () => {
            const positions = [
                OpenPosition.new({
                    role: 'marketing',
                    hiring_manager_id: careersNetworkViewModel.user.id,
                    hiring_application: HiringApplication.fixtures.getInstance(),
                }),
            ];
            stubPositions(positions);
            render();

            expect(_.pluck(scope.teamMemberOptions, 'id')).toEqual(['all', positions[0].hiring_application.user_id]);
            expect(_.pluck(scope.teamMemberOptions, 'name')).toEqual([
                'All Team Members',
                positions[0].hiring_application.name,
            ]);
        });
    });

    function stubPositions(positions, interests) {
        if (!positions) {
            positions = [
                // one that is open
                OpenPosition.new({
                    id: 1,
                    role: 'marketing', // if excluded, throws a missing translation error
                    featured: true,
                    hiring_manager_id: careersNetworkViewModel.user.id,
                    hiring_application: HiringApplication.fixtures.getInstance(),
                    auto_expiration_date: '2019-01-01',
                }),
                // one that is drafted
                OpenPosition.new({
                    id: 2,
                    role: 'marketing',
                    hiring_manager_id: careersNetworkViewModel.user.id,
                    hiring_application: HiringApplication.fixtures.getInstance(),
                }),
                // one that is archived
                OpenPosition.new({
                    id: 3,
                    role: 'marketing',
                    featured: true,
                    archived: true,
                    hiring_manager_id: careersNetworkViewModel.user.id,
                    hiring_application: HiringApplication.fixtures.getInstance(),
                    auto_expiration_date: '2019-01-01',
                }),
                // one that is drafted and archived
                OpenPosition.new({
                    id: 4,
                    role: 'marketing',
                    archived: true,
                    hiring_manager_id: careersNetworkViewModel.user.id,
                    hiring_application: HiringApplication.fixtures.getInstance(),
                }),
            ];
        }

        if (!interests) {
            interests = [
                CandidatePositionInterest.new({
                    hiring_manager_id: careersNetworkViewModel.user.id,
                    open_position_id: 1,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'unseen',
                }),
                CandidatePositionInterest.new({
                    hiring_manager_id: careersNetworkViewModel.user.id,
                    open_position_id: 1,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'unseen',
                }),
                CandidatePositionInterest.new({
                    hiring_manager_id: careersNetworkViewModel.user.id,
                    open_position_id: 1,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'pending',
                }),
                CandidatePositionInterest.new({
                    hiring_manager_id: careersNetworkViewModel.user.id,
                    open_position_id: 1,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'accepted',
                }),
            ];
        }

        careersNetworkViewModel.openPositions = positions;
        jest.spyOn(CareersNetworkViewModel.prototype, 'ensureOpenPositions').mockReturnValue($q.when(positions));
        jest.spyOn(CareersNetworkViewModel.prototype, 'ensureCandidatePositionInterests').mockReturnValue(
            $q.when(interests),
        );
        jest.spyOn(CareersNetworkViewModel.prototype, 'ensureHiringRelationshipsLoaded').mockReturnValue($q.when());
    }
});
