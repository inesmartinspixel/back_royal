import 'AngularSpecHelper';
import 'Careers/angularModule';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(fieldOptionsLocales);

describe('HasLocation', () => {
    let $injector;
    let sanFran;
    let HasLocation;
    let user;
    let profile;
    let User;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                User = $injector.get('User');
                HasLocation = $injector.get('HasLocation');
            },
        ]);
        sanFran = JSON.parse(
            '{"locality":{"short":"SF","long":"San Francisco"},"administrative_area_level_2":{"short":"San Francisco County","long":"San Francisco County"},"administrative_area_level_1":{"short":"CA","long":"California"},"country":{"short":"US","long":"United States"},"formatted_address":"San Francisco, CA, USA","utc_offset":-420,"lat":37.7749295,"lng":-122.4194155}',
        );
    });

    describe('localizedLocationOrLocationString', () => {
        it('should return special localization for known location', () => {
            expect(HasLocation.localizedLocationOrLocationString(sanFran)).toEqual('San Francisco Bay Area');
        });

        it('should return locationString for unknown location', () => {
            const notSanFran = _.extend(sanFran, {
                lat: 42,
            });
            expect(HasLocation.localizedLocationOrLocationString(notSanFran)).toEqual('San Francisco, CA');
        });
    });

    describe('coordinates', () => {
        beforeEach(initializeProfile);

        it('should return undefined if no place_details', () => {
            profile.place_details = null;
            expect(profile.coordinates).toBeUndefined();
        });

        describe('when place_details', () => {
            beforeEach(() => {
                profile.place_details = {};
            });

            describe('when no lat or lng on place_details', () => {
                beforeEach(() => {
                    profile.place_details.lat = undefined;
                    profile.place_details.lng = undefined;
                });

                it('should return undefined if no lat or lng in the place_details', () => {
                    expect(profile.coordinates).toBeUndefined();
                    profile.place_details.lat = 'foo';
                    expect(profile.coordinates).toBeUndefined();
                    profile.place_details.lat = undefined;
                    profile.place_details.lng = 'foo';
                    expect(profile.coordinates).toBeUndefined();
                });
            });

            describe('when lat and lng are present on place_details', () => {
                beforeEach(() => {
                    profile.place_details.lat = 'foo';
                    profile.place_details.lng = 'bar';
                });

                it('should return an array containing the lat and the lng in the place_details', () => {
                    expect(profile.coordinates).toEqual(['foo', 'bar']);
                });
            });
        });
    });

    describe('formattedAdrAddress', () => {
        beforeEach(initializeProfile);

        it('should return undefined if no place_details', () => {
            profile.place_details = null;
            expect(profile.formattedAdrAddress).toBeUndefined();
        });

        describe('when place_details', () => {
            beforeEach(() => {
                profile.place_details = {};
            });

            describe('when no adr_address is present', () => {
                beforeEach(() => {
                    profile.place_details.adr_address = null;
                });

                it('should return undefined', () => {
                    expect(profile.formattedAdrAddress).toBeUndefined();
                });
            });

            describe('when adr_address is present', () => {
                function setAdrAdress(adrAddress, formattedAddress) {
                    formattedAddress = formattedAddress || 'dummy address';
                    profile.place_details = {
                        formatted_address: formattedAddress,
                        adr_address: adrAddress,
                    };
                }

                it('should cache the result and update when it changes', () => {
                    setAdrAdress('<span>This is a dummy address for testing purposes only</span>', 'dummy address 1');

                    expect(profile.formattedAdrAddress).toEqual(
                        '<span>This is a dummy address for testing purposes only</span>',
                    );
                    expect(profile.$$formattedAdrAddressCache['dummy address 1']).toEqual(
                        '<span>This is a dummy address for testing purposes only</span>',
                    );

                    setAdrAdress(
                        '<span>This is another dummy address for testing purposes only</span>',
                        'dummy address 2',
                    );

                    expect(profile.formattedAdrAddress).toEqual(
                        '<span>This is another dummy address for testing purposes only</span>',
                    );
                    expect(profile.$$formattedAdrAddressCache['dummy address 2']).toEqual(
                        '<span>This is another dummy address for testing purposes only</span>',
                    );
                });

                it('should wrap the street-address in a div and remove the trailing comma after the street-address', () => {
                    setAdrAdress(
                        '<span class="street-address">3000 K St NW Suite 275</span>, <span class="locality">Washington</span>, <span class="region">DC</span> <span class="postal-code">20007</span>',
                    );
                    expect(profile.formattedAdrAddress).toEqual(
                        '<div class="street-address-container"><span class="street-address">3000 K St NW Suite 275</span></div><span class="locality">Washington</span>, <span class="region">DC</span> <span class="postal-code">20007</span>',
                    );
                });

                it('should wrap the street-address in a div and remove the trailing comma after the street-address even if the country-name immediately follows it', () => {
                    // Normally, if the country-name isn't the last element in the HTML, we don't remove the comma that
                    // comes right before it because the address is most likely for a city-state, so the country-name
                    // element is probably being used as the city in this case. However, if a street-address is present
                    // and the country-name comes right after it, we still remove the trailing comma that comes after
                    // the street-address, which just so happens to be the preceding comma before the country-name,
                    // because if the street-address is present, we ALWAYS put the street-address on its own line
                    // and we don't want the trailing comma.
                    setAdrAdress(
                        '<span class="street-address">532 Upper Serangoon Rd</span>, <span class="country-name">Singapore</span> <span class="postal-code">534547</span>',
                    );
                    expect(profile.formattedAdrAddress).toEqual(
                        '<div class="street-address-container"><span class="street-address">532 Upper Serangoon Rd</span></div><span class="country-name">Singapore</span> <span class="postal-code">534547</span>',
                    );
                });

                it('should wrap the street-address and any characters before the street-address in a div and remove the trailing comma after the street-address', () => {
                    setAdrAdress(
                        '3/<span class="street-address">50 Murray St</span>, <span class="locality">Pyrmont</span> <span class="region">NSW</span> <span class="postal-code">2009</span>',
                    );
                    expect(profile.formattedAdrAddress).toEqual(
                        '<div class="street-address-container">3/<span class="street-address">50 Murray St</span></div><span class="locality">Pyrmont</span> <span class="region">NSW</span> <span class="postal-code">2009</span>',
                    );
                });

                it('should not wrap street-address in a div if no street-address is present', () => {
                    const adrAddress = '<span class="locality">London</span> <span class="postal-code">WC2N 4BG</span>';
                    setAdrAdress(adrAddress);
                    expect(profile.formattedAdrAddress).toEqual(adrAddress);
                });

                it('should remove the preceding comma before the country-name if the country-name is the last element in the HTML', () => {
                    setAdrAdress(
                        '<span class="locality">London</span> <span class="postal-code">WC2N 4BG</span>, <span class="country-name">UK</span>',
                    );
                    expect(profile.formattedAdrAddress).toEqual(
                        '<span class="locality">London</span> <span class="postal-code">WC2N 4BG</span><span class="country-name">UK</span>',
                    );
                });

                it('should not remove preceding comma before the country-name if the country-name is not the last element in the HTML', () => {
                    setAdrAdress(
                        '<span>This is a dummy address for testing purposes only</span>, <span class="country-name">Singapore</span> <span class="postal-code">534547</span>',
                    );
                    expect(profile.formattedAdrAddress).toEqual(
                        '<span>This is a dummy address for testing purposes only</span>, <span class="country-name">Singapore</span> <span class="postal-code">534547</span>',
                    );
                });

                it('should remove the preceding dash before the country-name if the country-name is the last element in the HTML', () => {
                    setAdrAdress(
                        'Near Nakheel Metro - <span class="region">Dubai</span> - <span class="country-name">United Arab Emirates</span>',
                    );
                    expect(profile.formattedAdrAddress).toEqual(
                        'Near Nakheel Metro - <span class="region">Dubai</span><span class="country-name">United Arab Emirates</span>',
                    );
                });

                it('should not remove preceding dash before the country-name if the country-name is not the last element in the HTML', () => {
                    setAdrAdress(
                        '<span>This is a dummy address for testing purposes only</span> - <span class="country-name">Singapore</span> <span class="postal-code">534547</span>',
                    );
                    expect(profile.formattedAdrAddress).toEqual(
                        '<span>This is a dummy address for testing purposes only</span> - <span class="country-name">Singapore</span> <span class="postal-code">534547</span>',
                    );
                });

                it("should remove the country-name and preceding comma if the country-name is 'US' and it's the last element in the HTML", () => {
                    setAdrAdress(
                        '<span class="locality">Harrisonburg</span>, <span class="region">VA</span> <span class="postal-code">22801</span>, <span class="country-name">US</span>',
                    );
                    expect(profile.formattedAdrAddress).toEqual(
                        '<span class="locality">Harrisonburg</span>, <span class="region">VA</span> <span class="postal-code">22801</span>',
                    );
                });

                it("should remove the country-name and preceding comma if the country-name is 'USA' and it's the last element in the HTML", () => {
                    setAdrAdress(
                        '<span class="locality">Harrisonburg</span>, <span class="region">VA</span> <span class="postal-code">22801</span>, <span class="country-name">USA</span>',
                    );
                    expect(profile.formattedAdrAddress).toEqual(
                        '<span class="locality">Harrisonburg</span>, <span class="region">VA</span> <span class="postal-code">22801</span>',
                    );
                });
            });
        });
    });

    function initializeProfile() {
        user = User.new({
            career_profile: {},
        });
        profile = user.career_profile;
    }
});
