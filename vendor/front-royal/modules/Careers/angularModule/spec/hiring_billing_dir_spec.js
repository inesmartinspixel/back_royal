import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';
import setSpecLocales from 'Translation/setSpecLocales';
import hiringBillingLocales from 'Careers/locales/careers/hiring_billing-en.json';

setSpecLocales(hiringBillingLocales);

describe('FrontRoyal.Careers.hiringBilling', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let currentUser;
    let Subscription;
    let HiringTeam;
    let userId;
    let DialogModal;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                HiringTeam = $injector.get('HiringTeam');
                $injector.get('HiringTeamFixtures');
                Subscription = $injector.get('Subscription');
                DialogModal = $injector.get('DialogModal');

                userId = 'userId';
                SpecHelper.stubConfig();
                SpecHelper.stubStripeCheckout();
                currentUser = SpecHelper.stubCurrentUser(null, null, null, {
                    id: userId,
                    hiring_team: HiringTeam.fixtures
                        .getInstance({
                            owner_id: userId,
                            title: 'Acme Products',
                        })
                        .asJson(),
                });
                currentUser.hiring_application = {};
                expect(currentUser.isHiringTeamOwner).toBe(true); // sanity check
            },
        ]);
    });

    function render(inModal) {
        renderer = SpecHelper.renderer();
        renderer.scope.inModal = angular.isDefined(inModal) ? inModal : false;
        renderer.render('<hiring-billing in-modal="inModal"></hiring-billing>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('non owner', () => {
        it('should show contact team owner copy when not a team owner', () => {
            currentUser.hiring_team.owner_id = 'teammate';
            render();
            SpecHelper.expectElement(elem, '.contact_team_owner');
        });

        it('should show owner email', () => {
            currentUser.hiring_team.owner_id = 'teammate';
            currentUser.hiring_team.email = 'teammate@mycompany.com';
            render(true);
            SpecHelper.expectElementText(
                elem,
                '.contact_team_owner',
                'Contact your team owner (teammate@mycompany.com) to update your plan or billing information.',
            );
        });
    });

    describe('modify', () => {
        it('should only show up for owners on full screen with ownsHiringTeamThatSeemsToHaveValidPaymentSource', () => {
            assertHidesModify(() => {
                scope.inModal = true;
            });

            assertHidesModify(() => {
                jest.spyOn(currentUser, 'ownsHiringTeamThatSeemsToHaveValidPaymentSource', 'get').mockReturnValue(
                    false,
                );
            });
        });

        it('should call modifyPaymentDetails', () => {
            const $q = $injector.get('$q');
            let resolve;
            renderWithModifyButton();

            jest.spyOn(scope.checkoutHelper, 'modifyPaymentDetails').mockReturnValue(
                $q(_resolve => {
                    resolve = _resolve;
                }),
            );

            SpecHelper.click(elem, '.modify button');
            expect(scope.checkoutHelper.modifyPaymentDetails).toHaveBeenCalled();
            resolve();
        });

        function assertHidesModify(fn) {
            renderWithModifyButton();
            SpecHelper.expectElement(elem, '.choose-plan .modify');

            fn();
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.choose-plan .modify');
        }

        function renderWithModifyButton() {
            currentUser.hiring_team.owner_id = currentUser.id;
            jest.spyOn(currentUser, 'ownsHiringTeamThatSeemsToHaveValidPaymentSource', 'get').mockReturnValue(true);
            render();
        }
    });

    describe('.trouble', () => {
        it('should only show up for owners on full screen with ownsHiringTeamThatSeemsToHaveBadPaymentSource', () => {
            assertHidesTrouble(() => {
                scope.inModal = true;
            });

            assertHidesTrouble(() => {
                jest.spyOn(currentUser, 'ownsHiringTeamThatSeemsToHaveBadPaymentSource', 'get').mockReturnValue(false);
            });
        });

        it('should call modifyPaymentDetails', () => {
            const $q = $injector.get('$q');
            let resolve;
            renderWithTroubleButton();

            jest.spyOn(scope.checkoutHelper, 'modifyPaymentDetails').mockReturnValue(
                $q(_resolve => {
                    resolve = _resolve;
                }),
            );

            SpecHelper.click(elem, '.trouble button');
            expect(scope.checkoutHelper.modifyPaymentDetails).toHaveBeenCalled();
            resolve();
        });

        function assertHidesTrouble(fn) {
            renderWithTroubleButton();
            SpecHelper.expectElement(elem, '.choose-plan .trouble');

            fn();
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.choose-plan .trouble');
        }

        function renderWithTroubleButton() {
            currentUser.hiring_team.owner_id = currentUser.id;
            jest.spyOn(currentUser, 'ownsHiringTeamThatSeemsToHaveBadPaymentSource', 'get').mockReturnValue(true);
            render();
        }
    });

    describe('.contact', () => {
        describe('.questions', () => {
            it('should show when in modal', () => {
                jest.spyOn(currentUser.hiring_team, 'primarySubscription', 'get').mockReturnValue(Subscription.new());
                render();
                SpecHelper.expectNoElement(elem, '.contact .questions');

                scope.inModal = true;
                scope.$digest();
                SpecHelper.expectElement(elem, '.contact .questions');
            });

            it('should show when no subscription', () => {
                jest.spyOn(currentUser.hiring_team, 'primarySubscription', 'get').mockReturnValue(Subscription.new());
                render();
                SpecHelper.expectNoElement(elem, '.contact .questions');

                jest.spyOn(currentUser.hiring_team, 'primarySubscription', 'get').mockReturnValue(null);
                scope.$digest();
                SpecHelper.expectElement(elem, '.contact .questions');
            });
        });

        describe('.contact_team_owner', () => {
            it('should show owner email', () => {
                currentUser.hiring_team.owner_id = 'teammate';
                currentUser.hiring_team.email = 'teammate@mycompany.com';
                render();
                SpecHelper.expectElementText(
                    elem,
                    'header .contact_team_owner',
                    'Contact your team owner (teammate@mycompany.com) to update your plan or billing information.',
                );
            });

            it('should hide if owner', () => {
                currentUser.hiring_team.owner_id = 'teammate';
                render();
                SpecHelper.expectElement(elem, 'header .contact_team_owner');

                currentUser.hiring_team.owner_id = currentUser.id;
                scope.$digest();
                SpecHelper.expectNoElement(elem, 'header .contact_team_owner');
            });
        });
    });

    describe('success', () => {
        it('should show the selected plan name', () => {
            render();
            const plans = _.indexBy(currentUser.hiring_team.stripe_plans, 'usage_type');
            scope.showScreen = 'success';

            // metered plan
            jest.spyOn(currentUser.hiring_team, 'primarySubscription', 'get').mockReturnValue(
                Subscription.new({
                    stripe_plan_id: plans.metered.id,
                }),
            );
            scope.$digest();
            SpecHelper.expectElementText(
                elem,
                '.success header',
                'Success! Acme Products is now on the Pay as you go Pro Plan.',
            );

            // licensed plan
            jest.spyOn(currentUser.hiring_team, 'primarySubscription', 'get').mockReturnValue(
                Subscription.new({
                    stripe_plan_id: plans.licensed.id,
                }),
            );
            scope.$digest();
            SpecHelper.expectElementText(
                elem,
                '.success header',
                'Success! Acme Products is now on the Unlimited Pro Plan.',
            );
        });

        it('should have a link back to billing', () => {
            render();
            scope.showScreen = 'success';
            scope.$digest();

            SpecHelper.expectNoElement(elem, '.success .link-to-billing');
            scope.inModal = true;
            scope.$digest();
            SpecHelper.expectElement(elem, '.success .link-to-billing');
        });

        it('should have a link back to home', () => {
            const $rootScope = $injector.get('$rootScope');
            render();
            scope.showScreen = 'success';
            scope.$digest();
            jest.spyOn($rootScope, 'goHome').mockImplementation(() => {});
            jest.spyOn(DialogModal, 'hideAlerts').mockImplementation(() => {});

            SpecHelper.click(elem, '.success .get-started');
            expect($rootScope.goHome).toHaveBeenCalled();
            expect(DialogModal.hideAlerts).toHaveBeenCalled();
        });
    });

    describe('with new hiring plans', () => {
        // FIXME: add specs once fully implemented

        it('should show correct pricing UI when unlimited-with-sourcing', () => {
            currentUser.hiring_team.hiring_plan = HiringTeam.HIRING_PLAN_UNLIMITED_WITH_SOURCING;
            jest.spyOn(currentUser.hiring_team, 'stripePlanForHiringPlan').mockReturnValue({
                amount: 1337,
            });
            render();
            SpecHelper.expectElementText(elem, '[name="current-plan-wrapper"] .program-title', 'Post + Source');
            SpecHelper.expectElementText(
                elem,
                '[name="current-plan-wrapper"] .program-subtitle span:eq(0)',
                'Unlimited elite candidates',
            );
            SpecHelper.expectElementText(
                elem,
                '[name="current-plan-wrapper"] .program-subtitle span:eq(1)',
                '$13.37/month',
            );
        });
    });

    describe('with legacy hiring plan', () => {
        beforeEach(() => {
            currentUser = SpecHelper.stubCurrentUser(null, null, null, {
                id: userId,
                hiring_application: {},
                hiring_team: HiringTeam.fixtures
                    .getInstance({
                        owner_id: userId,
                        title: 'Acme Products',
                        hiring_plan: HiringTeam.HIRING_PLAN_LEGACY,
                    })
                    .asJson(),
            });
        });

        describe('.choose-plan', () => {
            it('should only show heading in inModal', () => {
                render(true);
                SpecHelper.expectElement(elem, '.choose-plan header h2');

                render(false);
                SpecHelper.expectNoElement(elem, '.choose-plan header h2');
            });

            describe('.plan', () => {
                it('should show plan info on each button', () => {
                    render();
                    const plans = _.chain(currentUser.hiring_team.stripe_plans)
                        .select(plan => _.contains(['hiring_unlimited_300', 'hiring_per_match_20'], plan.id))
                        .indexBy('usage_type')
                        .value();
                    const planEls = SpecHelper.expectElements(elem, '.plan', 2);

                    // Since these elements are copied/pasted in the template, we should test
                    // each one explicitly
                    SpecHelper.expectElementText(planEls.eq(0), 'h4', 'Pay as you go');
                    SpecHelper.expectElementText(planEls.eq(0), '.price', `$${plans.metered.amount / 100}`);
                    SpecHelper.expectElementText(planEls.eq(0), '.per', 'per match');
                    SpecHelper.expectElementText(planEls.eq(0), 'button', 'Choose Plan');
                    SpecHelper.expectHasClass(planEls.eq(0), 'button', 'blue');
                    SpecHelper.expectDoesNotHaveClass(planEls.eq(0), 'button', 'beigemiddark');
                    SpecHelper.expectElementEnabled(planEls.eq(0), 'button');

                    SpecHelper.expectElementText(planEls.eq(1), 'h4', 'Unlimited');
                    SpecHelper.expectElementText(planEls.eq(1), '.price', `$${plans.licensed.amount / 100}`);
                    SpecHelper.expectElementText(planEls.eq(1), '.per', 'per month');
                    SpecHelper.expectElementText(planEls.eq(1), 'button', 'Choose Plan');
                    SpecHelper.expectHasClass(planEls.eq(1), 'button', 'blue');
                    SpecHelper.expectDoesNotHaveClass(planEls.eq(1), 'button', 'beigemiddark');
                    SpecHelper.expectElementEnabled(planEls.eq(0), 'button');
                });

                it('should make some changes on the selected plan', () => {
                    render();
                    const plans = _.indexBy(currentUser.hiring_team.stripe_plans, 'usage_type');
                    const planEls = SpecHelper.expectElements(elem, '.plan', 2);

                    // Since these elements are copied/pasted in the template, we should test
                    // each one explicitly
                    jest.spyOn(currentUser.hiring_team, 'primarySubscription', 'get').mockReturnValue(
                        Subscription.new({
                            stripe_plan_id: plans.metered.id,
                        }),
                    );
                    scope.$digest();
                    SpecHelper.expectElementText(planEls.eq(0), 'button', 'Your plan');
                    SpecHelper.expectHasClass(planEls.eq(0), 'button', 'beigemiddark');
                    SpecHelper.expectDoesNotHaveClass(planEls.eq(0), 'button', 'blue');
                    SpecHelper.expectElementDisabled(planEls.eq(0), 'button');

                    jest.spyOn(currentUser.hiring_team, 'primarySubscription', 'get').mockReturnValue(
                        Subscription.new({
                            stripe_plan_id: plans.licensed.id,
                        }),
                    );
                    scope.$digest();
                    SpecHelper.expectElementText(planEls.eq(1), 'button', 'Your plan');
                    SpecHelper.expectHasClass(planEls.eq(1), 'button', 'beigemiddark');
                    SpecHelper.expectDoesNotHaveClass(planEls.eq(1), 'button', 'blue');
                    SpecHelper.expectElementDisabled(planEls.eq(1), 'button');
                });

                it('should be Contact Owner for non-owner', () => {
                    currentUser.hiring_team.owner_id = 'teammate';
                    render();
                    const planEls = SpecHelper.expectElements(elem, '.plan', 2);
                    SpecHelper.expectElementText(planEls.eq(0), 'button span', 'Contact Owner');
                    SpecHelper.expectElementText(planEls.eq(1), 'button span', 'Contact Owner');
                });

                it('should allow for selecting a plan', () => {
                    const $q = $injector.get('$q');
                    const $timeout = $injector.get('$timeout');
                    let resolve;
                    render();

                    jest.spyOn(scope.checkoutHelper, 'subscribeToLegacyPlan').mockReturnValue(
                        $q(_resolve => {
                            resolve = _resolve;
                        }),
                    );
                    const plans = _.indexBy(currentUser.hiring_team.stripe_plans, 'usage_type');

                    SpecHelper.click(elem, '.plan button', 0);
                    expect(scope.checkoutHelper.subscribeToLegacyPlan).toHaveBeenCalledWith(plans.metered.id);

                    expect(scope.showScreen).toEqual('plans');
                    resolve();
                    $timeout.flush();
                    expect(scope.showScreen).toEqual('success');
                });
            });
        });

        describe('.to_cancel', () => {
            it('should hide on modal', () => {
                jest.spyOn(currentUser.hiring_team, 'primarySubscription', 'get').mockReturnValue(Subscription.new());
                render();
                SpecHelper.expectElement(elem, '.contact .to_cancel');

                scope.inModal = true;
                scope.$digest();
                SpecHelper.expectNoElement(elem, '.contact .to_cancel');
            });

            it('should hide when no subscription', () => {
                jest.spyOn(currentUser.hiring_team, 'primarySubscription', 'get').mockReturnValue(Subscription.new());
                render();
                SpecHelper.expectElement(elem, '.contact .to_cancel');

                jest.spyOn(currentUser.hiring_team, 'primarySubscription', 'get').mockReturnValue(null);
                scope.$digest();
                SpecHelper.expectNoElement(elem, '.contact .to_cancel');
            });

            it('should have a working cancel button', () => {
                jest.spyOn(currentUser.hiring_team, 'primarySubscription', 'get').mockReturnValue(Subscription.new());
                let confirmCallback;
                jest.spyOn(DialogModal, 'confirm').mockImplementation(opts => {
                    confirmCallback = opts.confirmCallback;
                });
                currentUser.hiring_team.hiring_plan = HiringTeam.HIRING_PLAN_UNLIMITED_WITH_SOURCING;
                jest.spyOn(currentUser.hiring_team.primarySubscription, 'destroy').mockImplementation();
                render();
                SpecHelper.click(elem, '[name="cancel-plan"]');

                // once the user confirms, we cancel the plan
                expect(currentUser.hiring_team.primarySubscription.destroy).not.toHaveBeenCalled();
                confirmCallback();
                expect(currentUser.hiring_team.primarySubscription.destroy).toHaveBeenCalled();
            });
        });
    });
});
