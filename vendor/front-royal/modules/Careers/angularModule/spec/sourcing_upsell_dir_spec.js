import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';
import setSpecLocales from 'Translation/setSpecLocales';
import sourcingUpsellLocales from 'Careers/locales/careers/sourcing_upsell-en.json';
import hiringChoosePlanLocales from 'Careers/locales/careers/hiring_choose_plan-en.json';

setSpecLocales(sourcingUpsellLocales, hiringChoosePlanLocales);

describe('FrontRoyal.Careers.sourcingUpsellDir', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let currentUser;
    let HiringTeam;
    let $q;
    let DialogModal;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');
        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                HiringTeam = $injector.get('HiringTeam');
                $q = $injector.get('$q');
                DialogModal = $injector.get('DialogModal');

                $injector.get('HiringTeamFixtures');
            },
        ]);

        SpecHelper.stubConfig();
        SpecHelper.stubStripeCheckout();

        currentUser = SpecHelper.stubCurrentUser();
        currentUser.hiring_team = HiringTeam.fixtures.getInstance();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<sourcing-upsell></sourcing-upsell>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should start at start screen if not unlimited-with-sourcing plan', () => {
        jest.spyOn(currentUser, 'hiringPlan', 'get').mockReturnValue(HiringTeam.HIRING_PLAN_PAY_PER_POST);
        render();
        expect(scope.hiringPlanScreen).toBe('start');
    });

    it('should start at pay screen if user has selected unlimited-with-sourcing plan', () => {
        jest.spyOn(currentUser, 'usingUnlimitedWithSourcingHiringPlan', 'get').mockReturnValue(true);
        render();
        expect(scope.hiringPlanScreen).toBe('pay');
    });

    it('should lead users through two steps and then payment', () => {
        jest.spyOn(currentUser, 'isHiringTeamOwner', 'get').mockReturnValue(true);
        render();
        SpecHelper.expectElement(elem, '.hiring-get-started');
        SpecHelper.expectNoElement(elem, '.hiring-total');

        SpecHelper.expectElement(elem, 'button.get-started');
        SpecHelper.expectNoElement(elem, '.contact_team_owner');

        SpecHelper.click(elem, 'button');

        SpecHelper.expectNoElement(elem, '.hiring-get-started');
        SpecHelper.expectElement(elem, '.hiring-total');

        jest.spyOn(scope.checkoutHelper, 'subscribeToUnlimitedWithSourcingPlan').mockImplementation(() =>
            $q(resolve => resolve()),
        );
        SpecHelper.click(elem, 'button[name="subscribe"]');
        expect(scope.checkoutHelper.subscribeToUnlimitedWithSourcingPlan).toHaveBeenCalled();
    });

    it('should show DialogModal after successful switch from pay_per_post to unlimited_w_sourcing plan', () => {
        jest.spyOn(currentUser, 'isHiringTeamOwner', 'get').mockReturnValue(true);
        jest.spyOn(currentUser, 'usingPayPerPostHiringPlan', 'get').mockReturnValue(true);
        render();
        SpecHelper.click(elem, 'button');

        const deferred = $q.defer();
        jest.spyOn(scope.checkoutHelper, 'subscribeToUnlimitedWithSourcingPlan').mockImplementation(
            () => deferred.promise,
        );
        jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});

        SpecHelper.click(elem, 'button[name="subscribe"]');
        expect(scope.checkoutHelper.subscribeToUnlimitedWithSourcingPlan).toHaveBeenCalled();
        expect(DialogModal.alert).not.toHaveBeenCalled();

        deferred.resolve();
        scope.$digest();
        expect(DialogModal.alert).toHaveBeenCalledWith({
            title: 'Plan Updated',
            content: expect.anything(), // there's a `require()` statement in there
            classes: ['medium-small'],
        });
    });

    it('should show spinner while stripeProcessing', () => {
        jest.spyOn(currentUser, 'usingUnlimitedWithSourcingHiringPlan', 'get').mockReturnValue(true);
        render();
        SpecHelper.expectElementEnabled(elem, '[name="subscribe"]');
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');

        scope.checkoutHelper.stripeProcessing = true;
        scope.$digest();
        SpecHelper.expectElementDisabled(elem, '[name="subscribe"]');
        SpecHelper.expectElement(elem, 'front-royal-spinner');

        scope.checkoutHelper.stripeProcessing = false;
        scope.$digest();
        SpecHelper.expectElementEnabled(elem, '[name="subscribe"]');
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
    });

    it('should disable button for non-team owner', () => {
        jest.spyOn(currentUser, 'isHiringTeamOwner', 'get').mockReturnValue(false);
        currentUser.hiring_team.email = 'blah@blah.com';
        render();
        SpecHelper.expectNoElement(elem, 'button.get-started');
        SpecHelper.expectElementText(
            elem,
            '.contact_team_owner',
            'Contact your team owner (blah@blah.com) to update your plan or billing information.',
        );
    });
});
