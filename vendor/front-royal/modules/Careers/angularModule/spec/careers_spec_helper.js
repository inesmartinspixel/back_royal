import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/candidate_position_interest_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/career_profile_lists';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_application_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_relationship_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/open_position_fixtures';
import 'Careers/angularModule/spec/careers_spec_helper';
import 'Mailbox/angularModule/spec/_mock/fixtures/message_fixtures';
import 'Mailbox/angularModule/spec/_mock/fixtures/conversation_fixtures';

angular.module('CareersSpecHelper', ['SpecHelper']).factory('CareersSpecHelper', [
    '$injector',
    $injector => {
        const $rootScope = $injector.get('$rootScope');
        const SpecHelper = $injector.get('SpecHelper');

        return {
            stubCareersNetworkViewModel(role, opts) {
                const HiringApplication = $injector.get('HiringApplication');
                $injector.get('HiringApplicationFixtures');
                const CareerProfile = $injector.get('CareerProfile');
                const Message = $injector.get('Mailbox.Message');
                $injector.get('CareerProfileFixtures');
                $injector.get('MessageFixtures');

                opts = opts || {};
                if (!_.has(opts, 'hiringRelationshipCount')) {
                    opts.hiringRelationshipCount = 6;
                }
                if (!_.has(opts, 'preload')) {
                    opts.preload = false;
                }
                if (!_.has(opts, 'otherUsers')) {
                    opts.otherUsers = [];
                }
                if (!_.has(opts, 'statusPairs')) {
                    opts.statusPairs = [
                        ['pending', 'hidden'], // waiting_on_hiring_manager
                        ['saved_for_later', 'hidden'], // saved_for_later
                        ['rejected', 'hidden'], // rejected_by_hiring_manager
                        ['accepted', 'pending'], // waiting_on_candidate
                        ['accepted', 'rejected'], // rejected_by_candidate
                        ['accepted', 'accepted'], // matched
                    ];
                }
                if (!_.has(opts, 'conversationOverrides')) {
                    opts.conversationOverrides = [
                        {
                            messages: [
                                {
                                    from: 'me',
                                    isRead: true,
                                },
                                {
                                    from: 'them',
                                    isRead: true,
                                },
                            ],
                        },
                    ];
                }
                if (!_.has(opts, 'addHiringRelationships')) {
                    opts.addHiringRelationships = true;
                }

                const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
                const HiringRelationship = $injector.get('HiringRelationship');
                const Conversation = $injector.get('Mailbox.Conversation');
                $injector.get('HiringRelationshipFixtures');
                $injector.get('ConversationFixtures');
                const user = $rootScope.currentUser || SpecHelper.stubCurrentUser();
                $rootScope.$apply();
                const guid = $injector.get('guid');

                const otherUsers = opts.otherUsers;
                let i = 0;
                while (otherUsers.length < opts.hiringRelationshipCount) {
                    i += 1;
                    otherUsers.push({
                        id: guid.generate(),
                        name: `Connection ${i}`,
                    });
                }

                if (role === 'hiringManager') {
                    user.hiring_application = user.hiring_application || HiringApplication.fixtures.getInstance();
                } else if (role === 'candidate') {
                    user.career_profile = user.career_profile || CareerProfile.fixtures.getInstance();
                }

                const hiringRelationships = [];

                const statusPairs = opts.statusPairs;
                for (i = 0; i < opts.hiringRelationshipCount; i++) {
                    const statusPair = statusPairs[i % statusPairs.length];

                    const hiringManager = role === 'hiringManager' ? user : otherUsers[i];
                    const candidate = role === 'candidate' ? user : otherUsers[i];
                    const hiringRelationship = HiringRelationship.fixtures.getInstance({
                        candidate_id: candidate.id,
                        candidate_display_name: candidate.name,
                        candidate_status: statusPair[1],
                        hiring_manager_id: opts.teammateId ? opts.teammateId : hiringManager.id,
                        hiring_manager_display_name: opts.teammateId ? 'teammate' : hiringManager.name,
                        hiring_manager_status: statusPair[0],
                        updated_at: i,
                        hiring_manager_email: opts.teammateId ? 'teammate@example.com' : hiringManager.email,
                        candidate_email: candidate.email,
                    });

                    // the request will not return a hiring application if the
                    // role is hiring manager (since we use the except param in the request)
                    if (!opts.teammateId && role !== 'hiringManager') {
                        hiringRelationship.hiring_application = HiringApplication.fixtures.getInstance({
                            id: guid.generate(),
                            user_id: hiringManager.id,
                            avatar_url: hiringManager.avatar_url || 'http://path/to/avatar',
                            name: hiringManager.name,
                            nickname: hiringManager.name,
                            email: hiringManager.email,
                        });
                    } else if (opts.hiringTeamId) {
                        hiringRelationship.hiring_application.hiring_team_id = opts.hiringTeamId;
                    }

                    // the request will not return a career profile if the
                    // role is candidate (since we use the except param in the request)
                    if (role !== 'candidate') {
                        hiringRelationship.career_profile = CareerProfile.fixtures.getInstance({
                            id: guid.generate(),
                            user_id: candidate.id,
                            avatar_url: candidate.avatar_url || 'http://path/to/avatar',
                            name: candidate.name,
                            nickname: candidate.name,
                            email: candidate.email,
                        });
                    }
                    hiringRelationships.push(hiringRelationship);

                    // for testing a candidate that has been saved for later by multiple teammates
                    if (opts.duplicateCandidate && opts.teammateId && statusPair[0] === 'saved_for_later') {
                        hiringRelationships.push(
                            HiringRelationship.fixtures.getInstance({
                                candidate_id: candidate.id,
                                candidate_display_name: candidate.name,
                                candidate_status: statusPair[1],
                                hiring_manager_id: `${opts.teammateId}2`,
                                hiring_manager_display_name: 'teammate 2',
                                hiring_manager_status: statusPair[0],
                                updated_at: i,
                                hiring_manager_email: 'teammate2@example.com',
                                candidate_email: candidate.email,
                            }),
                        );
                    }
                }

                // finds all matched hiring relationships
                const matchedRelationships = _.where(hiringRelationships, {
                    matched: true,
                });

                if (matchedRelationships.length > 0) {
                    matchedRelationships.forEach((matchedRelationship, j) => {
                        const otherUser = _.findWhere(otherUsers, {
                            id:
                                role === 'hiringManager'
                                    ? matchedRelationship.candidate_id
                                    : matchedRelationship.hiring_manager_id,
                        });

                        const conversationOverrides = opts.conversationOverrides[j];
                        if (conversationOverrides && conversationOverrides.messages) {
                            const userOrTeammate = opts.teammateId
                                ? {
                                      id: opts.teammateId,
                                  }
                                : user;
                            const conversation = Conversation.fixtures.getInstance(
                                {
                                    messages: [],
                                },
                                userOrTeammate,
                                otherUser,
                            );
                            const oneDay = 24 * 60 * 60 * 1000;

                            _.each(conversationOverrides.messages, (messageAttrs, k) => {
                                // eslint-disable-line
                                const isRead = messageAttrs.isRead || false;
                                const message = Message.fixtures.getInstance(
                                    {
                                        created_at: Date.now() - oneDay + 1000 * j + k,
                                        receipts: [
                                            {
                                                receiver_id: userOrTeammate.id,
                                                is_read: messageAttrs.from === 'me' ? true : isRead,
                                                trashed: false,
                                            },
                                            {
                                                receiver_id: otherUser.id,
                                                is_read: messageAttrs.from === 'me' ? isRead : true,
                                                trashed: false,
                                            },
                                        ],
                                    },
                                    messageAttrs.from === 'me' ? userOrTeammate : otherUser,
                                    messageAttrs.from === 'me' ? otherUser : userOrTeammate,
                                );

                                if (messageAttrs.hasOurReceipt === false) {
                                    message.receipts = _.reject(
                                        message.receipts,
                                        receipt => receipt.receiver_id === userOrTeammate.id,
                                    );
                                }

                                // If a user has "dropped in", we need to add a receipt for them to this message
                                if (messageAttrs.droppedIn && messageAttrs.droppedInUserId) {
                                    message.receipts.push({
                                        receiver_id: messageAttrs.droppedInUserId,
                                        is_read: messageAttrs.from === 'me' ? true : isRead,
                                        trashed: false,
                                    });
                                }
                                conversation.addMessage(message);
                            });
                            matchedRelationship.conversation = conversation;
                        }
                    });
                }

                CareersNetworkViewModel.instances[role] = new CareersNetworkViewModel(role, {
                    preload: opts.preload,
                });

                const viewModel = CareersNetworkViewModel.instances[role];

                if (opts.addHiringRelationships) {
                    viewModel._onHiringRelationshipsLoaded(hiringRelationships);
                }

                viewModel.testHelper = {
                    viewModel,
                    getHiringRelationshipViewModelWithNoConversation() {
                        return _.detect(this.viewModel.hiringRelationshipViewModels, vm => !vm.conversation);
                    },
                    getHiringRelationshipViewModelWithConversation() {
                        return _.detect(this.viewModel.hiringRelationshipViewModels, vm => vm.conversation);
                    },
                    getRelationshipsByStatus() {
                        return _.indexBy(this.viewModel.hiringRelationships, 'status');
                    },
                    getRelationshipViewModelsByStatus() {
                        return _.indexBy(
                            this.viewModel.hiringRelationshipViewModels,
                            vm => vm.hiringRelationship.status,
                        );
                    },
                };

                // When we first set this up, there would be a digest after the view model was created,
                // so there are still a bunch of specs relying on that.
                $rootScope.$digest();
                return viewModel;
            },
        };
    },
]);
