import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/careers_spec_helper';
import setSpecLocales from 'Translation/setSpecLocales';
import connectionsLocales from 'Careers/locales/careers/connections-en.json';
import connectionBarButtonLocales from 'Careers/locales/careers/connection_bar_button-en.json';
import candidateListLocales from 'Careers/locales/careers/candidate_list-en.json';

setSpecLocales(connectionsLocales, connectionBarButtonLocales, candidateListLocales);

describe('FrontRoyal.Careers.ConnectionsListSectionDir', () => {
    let $injector;
    let SpecHelper;
    let CareersSpecHelper;
    let renderer;
    let elem;
    let scope;
    let careersNetworkViewModel;
    let sectionName;
    let expandedSections;
    let hiringRelationshipViewModels;
    let stickyHeaders;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
            },
        ]);

        careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');

        // Except for the one spec where we test it directly, make all relationships
        // matches so they will be visible
        setupHiringRelationshipViewModels('accepted', 'accepted');

        hiringRelationshipViewModels = careersNetworkViewModel.hiringRelationshipViewModels;
        stickyHeaders = true;
        sectionName = 'connected';
        expandedSections = {
            connected: {
                expanded: true,
            },
        };
    });

    function setupHiringRelationshipViewModels(hiringManageStatus, candidateStatus) {
        _.each(careersNetworkViewModel.hiringRelationshipViewModels, vm => {
            vm.hiringRelationship.hiring_manager_status = hiringManageStatus;
            vm.hiringRelationship.candidate_status = candidateStatus;
        });
    }

    function render() {
        renderer = SpecHelper.renderer();
        angular.extend(renderer.scope, {
            sectionName,
            expandedSections,
            hiringRelationshipViewModels,
            stickyHeaders,
        });
        renderer.render(
            '<connections-list-section section-name="sectionName" expanded-sections="expandedSections" hiring-relationship-view-models="hiringRelationshipViewModels" sticky-headers="stickyHeaders"></connections-list-section>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('section display', () => {
        it('should render normally', () => {
            render();

            SpecHelper.expectElements(elem, 'connection-bar-button', hiringRelationshipViewModels.length);
        });
    });

    describe('viewProfile', () => {
        it('should call showUpdateModal', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');

            // Except for the one spec where we test it directly, make all relationships
            // rejectedByCandidate so the profile button will be visible
            setupHiringRelationshipViewModels('accepted', 'rejected');

            hiringRelationshipViewModels = careersNetworkViewModel.hiringRelationshipViewModels;
            const hiringRelationshipViewModel = hiringRelationshipViewModels[0];
            jest.spyOn(hiringRelationshipViewModel, 'showUpdateModal').mockImplementation(() => {});
            render();

            SpecHelper.click(elem, 'connection-bar-button [name="profile"]', 0);
            expect(hiringRelationshipViewModel.showUpdateModal).toHaveBeenCalled();
        });
    });
});
