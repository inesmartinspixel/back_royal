import 'AngularSpecHelper';
import 'Careers/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import coverLetterLocales from 'Careers/locales/careers/cover_letter-en.json';

setSpecLocales(coverLetterLocales);

describe('FrontRoyal.Careers.CoverLetter', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let DialogModal;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                DialogModal = $injector.get('DialogModal');
                $timeout = $injector.get('$timeout');
            },
        ]);
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.coverLetter = opts.coverLetter;
        renderer.scope.avatarUrl = opts.avatarUrl;
        renderer.scope.submit = opts.submit && jest.fn();
        renderer.scope.hiringApplication = opts.hiringApplication;
        renderer.render(
            '<cover-letter cover-letter="coverLetter" avatar-url="avatarUrl" submit="submit" hiring-application="hiringApplication"></cover-letter>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should show cover letter', () => {
        render({
            coverLetter: {
                content: 'foo',
                created_at: Date.now(),
            },
        });

        expect(scope.showForm).toBe(false);
        SpecHelper.expectElement(elem, '.content');
    });

    it('should show careers-avatar', () => {
        render({
            coverLetter: {
                content: 'foo',
                created_at: Date.now(),
            },
            avatarUrl: 'http://example.com',
        });

        SpecHelper.expectElement(elem, 'careers-avatar');
    });

    describe('form', () => {
        it('should show form', () => {
            render({
                submit: true,
            });

            expect(scope.showForm).toBe(true);
            SpecHelper.expectElement(elem, 'form');
        });

        it('should display intro text', () => {
            const hiringApplication = {
                name: 'Luke Skywalker',
                company_name: 'Rebel Alliance',
            };

            render({
                submit: true,
                hiringApplication,
            });

            SpecHelper.expectElementText(
                elem,
                '.intro-text',
                `Write a cover letter to ${hiringApplication.name} at ${hiringApplication.company_name}. What excites you about ${hiringApplication.company_name}? Why would you would be a good fit?`,
            );
        });

        it('should submit data', () => {
            render({
                submit: true,
            });

            const now = Date.now();
            jest.spyOn(Date, 'now').mockReturnValue(now);
            jest.spyOn(DialogModal, 'hideAlerts');

            scope.formSubmit();

            expect(scope.submit).toHaveBeenCalledWith({
                content: scope.content,
                created_at: now,
            });

            $timeout.flush(1749);
            expect(DialogModal.hideAlerts).not.toHaveBeenCalled();
            $timeout.flush(1);
            expect(DialogModal.hideAlerts).toHaveBeenCalled();
        });
    });
});
