import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/careers_spec_helper';
import setSpecLocales from 'Translation/setSpecLocales';
import connectionConversationLocales from 'Careers/locales/careers/connection_conversation-en.json';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';

setSpecLocales(connectionConversationLocales, candidateListCardLocales);

describe('FrontRoyal.Careers.ConnectionConversation', () => {
    let $injector;
    let SpecHelper;
    let CareersSpecHelper;
    let renderer;
    let elem;
    let scope;
    let careersNetworkViewModel;
    let HiringRelationship;
    let Message;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                HiringRelationship = $injector.get('HiringRelationship');
                Message = $injector.get('Mailbox.Message');

                $injector.get('MessageFixtures');
            },
        ]);

        SpecHelper.stubCurrentUser();
        careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
    });

    function render(hiringRelationshipViewModel, role) {
        renderer = SpecHelper.renderer();
        renderer.scope.hiringRelationshipViewModel = hiringRelationshipViewModel;
        renderer.scope.role = angular.isDefined(role) ? role : 'hiringManager';

        if (hiringRelationshipViewModel.conversationViewModel) {
            jest.spyOn(hiringRelationshipViewModel, 'markAllAsRead').mockImplementation(() => {});
        }

        renderer.render(
            '<connection-conversation hiring-relationship-view-model="hiringRelationshipViewModel" role="role"></connection-conversation>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();

        if (hiringRelationshipViewModel.conversationViewModel) {
            expect(hiringRelationshipViewModel.markAllAsRead).toHaveBeenCalled();
        }
    }

    describe('header', () => {
        it('should have expected content in header for hiring manager', () => {
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            render(hiringRelationshipViewModel);

            // header user details
            SpecHelper.expectElement(elem, '.header careers-avatar');
            SpecHelper.expectElementText(elem, '.primary-line:eq(0)', hiringRelationshipViewModel.connectionName);
            SpecHelper.expectElementText(elem, '.secondary-line', hiringRelationshipViewModel.connectionCompany);

            // header action buttons
            SpecHelper.expectNoElement(elem, '.buttons button.website');
            SpecHelper.expectElement(elem, '.buttons button.profile');
            SpecHelper.expectElement(elem, '.buttons button.resume');
            SpecHelper.expectElement(elem, '.close-relationship-link');
            SpecHelper.expectElementText(elem, '.close-relationship-link', 'Close');
        });

        it('should have expected content in header for candidate', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            render(hiringRelationshipViewModel, 'candidate');

            // header user details
            SpecHelper.expectNoElement(elem, '.header careers-avatar');
            SpecHelper.expectNoElement(elem, '.secondary-line');

            // if there is a logo, show it
            hiringRelationshipViewModel.hiringApplication.company_logo_url = '/path/to/logo';
            scope.$digest();
            SpecHelper.expectElement(elem, '.primary-line:eq(0) img');
            SpecHelper.expectNoElement(elem, '.primary-line:eq(0) .company-name');

            // if no logo, show company name
            hiringRelationshipViewModel.hiringApplication.company_logo_url = null;
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.primary-line:eq(0) img');
            SpecHelper.expectElementText(
                elem,
                '.primary-line:eq(0) .company-name',
                hiringRelationshipViewModel.hiringManagerCompany,
            );

            // header action buttons
            SpecHelper.expectElement(elem, '.buttons button.website');
            SpecHelper.expectElement(elem, '.buttons button.profile');
            SpecHelper.expectNoElement(elem, '.buttons button.resume');
            SpecHelper.expectElement(elem, '.close-relationship-link');
            SpecHelper.expectElementText(elem, '.close-relationship-link', 'Close');
        });

        it('should open resume url', () => {
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            jest.spyOn(hiringRelationshipViewModel, 'resumeUrl', 'get').mockReturnValue('http://example.com');
            render(hiringRelationshipViewModel);

            jest.spyOn(scope, 'openLink').mockImplementation(() => {});
            SpecHelper.click(elem, '.buttons button.resume');
            expect(scope.openLink).toHaveBeenCalled();
        });

        it('should allow for showing profile', () => {
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            render(hiringRelationshipViewModel);

            jest.spyOn(hiringRelationshipViewModel, 'showUpdateModal').mockImplementation(() => {});
            SpecHelper.click(elem, '[name="profile"]');
            expect(hiringRelationshipViewModel.showUpdateModal).toHaveBeenCalled();
        });

        it('should allow hiringManager to go back to tracker', () => {
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            render(hiringRelationshipViewModel);

            jest.spyOn(hiringRelationshipViewModel, 'navigateToTracker').mockImplementation(() => {});
            SpecHelper.expectElementTitle(elem, 'button.back', 'Back to Tracker');
            SpecHelper.click(elem, 'button.back');
            expect(hiringRelationshipViewModel.navigateToTracker).toHaveBeenCalled();
        });

        it('should allow candidate to go back to connections list', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            render(hiringRelationshipViewModel, 'candidate');

            jest.spyOn(hiringRelationshipViewModel, 'navigateToTracker').mockImplementation(() => {});
            SpecHelper.expectElementTitle(elem, 'button.back', 'Back to Connections');
            SpecHelper.click(elem, 'button.back');
            expect(hiringRelationshipViewModel.navigateToTracker).toHaveBeenCalled();
        });
    });

    describe('new conversation state', () => {
        it('should display an image and message for hiring manager', () => {
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithNoConversation();
            render(hiringRelationshipViewModel);
            SpecHelper.expectElement(elem, '.careers-placeholder-container img');
            SpecHelper.expectElementText(
                elem,
                '.careers-placeholder-container span.subtitle',
                'You have a match! Say hello.',
            );
            const textarea = SpecHelper.expectElement(elem, '.connection-conversation-send-message .new-message-box');
            expect(textarea.attr('placeholder')).toEqual('Hello, nice to meet you!');
        });

        it('should display an image and message for candidate', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithNoConversation();
            render(hiringRelationshipViewModel, 'candidate');
            SpecHelper.expectElement(elem, '.careers-placeholder-container img');
            SpecHelper.expectElementText(
                elem,
                '.careers-placeholder-container span.subtitle',
                `${hiringRelationshipViewModel.connectionNickname} liked your profile and has requested to connect!Reply to accept the connection and learn more about available positions.`,
            );
            const textarea = SpecHelper.expectElement(elem, '.connection-conversation-send-message .new-message-box');
            expect(textarea.attr('placeholder')).toEqual(
                `Hi ${hiringRelationshipViewModel.connectionNickname}, thanks for connecting! Can you share more about the roles you are looking to fill?`,
            );
        });
    });

    describe('newRequest and invitedPosition state', () => {
        it('should not display a tip for hiring manager', () => {
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            render(hiringRelationshipViewModel);
            SpecHelper.expectNoElement(elem, '.connection-conversation-send-message label:eq(0)');
        });

        it('should display a tip for candidate', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            Object.defineProperty(hiringRelationshipViewModel, 'newRequest', {
                value: true,
            });
            Object.defineProperty(hiringRelationshipViewModel, 'invitedPosition', {
                value: true,
            });
            render(hiringRelationshipViewModel, 'candidate');
            const label = SpecHelper.expectElement(elem, '.connection-conversation-send-message label:eq(0)');
            SpecHelper.expectElementText(
                elem,
                label,
                `Tip: provide at least 3 time slots to connect with ${hiringRelationshipViewModel.connectionNickname} at ${hiringRelationshipViewModel.hiringManagerCompany} (include time zone)`,
            );
        });
    });

    describe('conversation message send placeholder', () => {
        it('should display generic placeholder after a message is sent', () => {
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            render(hiringRelationshipViewModel);
            const textarea = SpecHelper.expectElement(elem, '.connection-conversation-send-message .new-message-box');
            expect(textarea.attr('placeholder')).toEqual(
                `Send message to ${hiringRelationshipViewModel.connectionName}`,
            );
        });

        it('should display generic placeholder after a message is sent', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            render(hiringRelationshipViewModel, 'candidate');
            const textarea = SpecHelper.expectElement(elem, '.connection-conversation-send-message .new-message-box');
            expect(textarea.attr('placeholder')).toEqual(
                `Send message to ${hiringRelationshipViewModel.connectionName}`,
            );
        });
    });

    describe('new messages available', () => {
        it('should show a bar that loads new messages if new messages are detected', () => {
            // Set up initial and updated view models
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();

            // Render with the initial view model
            render(hiringRelationshipViewModel);

            // Expect the correct number of messages and newMessages to be undefined
            const origMessageLength = hiringRelationshipViewModel.conversation.messages.length;
            SpecHelper.expectElements(elem, 'connection-conversation-message', origMessageLength);

            // update the conversation
            const message = Message.fixtures.getInstance(
                {},
                {
                    id: hiringRelationshipViewModel.otherParticipantId,
                },
                careersNetworkViewModel.user,
            );
            message.receipts.push({
                receiver_id: careersNetworkViewModel.user.user_id,
                is_read: false,
            });
            hiringRelationshipViewModel.conversationViewModel.conversation.addMessage(message);
            scope.$digest();

            SpecHelper.expectElement(elem, '.tap-to-show');
            SpecHelper.expectElements(elem, 'connection-conversation-message', origMessageLength);

            scope.hiringRelationshipViewModel.markAllAsRead.mockClear();
            SpecHelper.click(elem, '.tap-to-show');
            expect(scope.hiringRelationshipViewModel.markAllAsRead).toHaveBeenCalled();
            SpecHelper.expectElements(elem, 'connection-conversation-message', origMessageLength + 1);
        });

        it('should not show a bar if no new messages are detected', () => {
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            render(hiringRelationshipViewModel);
            SpecHelper.expectElements(
                elem,
                'connection-conversation-message',
                hiringRelationshipViewModel.conversation.messages.length,
            );
            expect(scope.newMessages).toBeFalsy();

            scope.conversationViewModel = angular.copy(hiringRelationshipViewModel.conversationViewModel);
            scope.$digest();

            expect(scope.newMessages).toBeFalsy();
        });

        it('should immediately show a new message from me', () => {
            // Set up initial and updated view models
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();

            // Render with the initial view model
            render(hiringRelationshipViewModel);

            // Expect the correct number of messages and newMessages to be undefined
            const origMessageLength = hiringRelationshipViewModel.conversation.messages.length;
            SpecHelper.expectElements(elem, 'connection-conversation-message', origMessageLength);

            // send the message
            scope.hiringRelationshipViewModel.markAllAsRead.mockClear();
            scope.hiringRelationshipViewModel.conversationViewModel.addMessage('a message from me');
            scope.$digest();

            SpecHelper.expectNoElement(elem, '.tap-to-show');
            SpecHelper.expectElements(elem, 'connection-conversation-message', origMessageLength + 1);
            expect(scope.hiringRelationshipViewModel.markAllAsRead).not.toHaveBeenCalled();

            // when the save call comes back, the whole conversation is gonna be swapped out, and the new
            // message will get an id and a created_at
            const json = hiringRelationshipViewModel.hiringRelationship.asJson();
            const messageJson = _.first(json.conversation.messages);
            expect(messageJson.id).not.toBeDefined(); // sanity check
            expect(messageJson.body).toEqual('a message from me'); // sanity check
            messageJson.id = 'newMessageId';
            messageJson.created_at = Date.now();
            careersNetworkViewModel._onHiringRelationshipsLoaded([HiringRelationship.new(json)]);

            scope.$digest();
            SpecHelper.expectNoElement(elem, '.tap-to-show');
            SpecHelper.expectElements(elem, 'connection-conversation-message', origMessageLength + 1);
            expect(scope.hiringRelationshipViewModel.markAllAsRead).not.toHaveBeenCalled();
        });

        it('should immediately show a new read status that gets pushed down', () => {
            // Set up initial and updated view models
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate', {
                conversationOverrides: [
                    {
                        messages: [
                            {
                                from: 'me',
                                isRead: false,
                            },
                        ],
                    },
                ],
            });
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();

            // Render with the initial view model
            render(hiringRelationshipViewModel);

            SpecHelper.expectElement(elem, '.unread'); // this is way down in the connection-conversation-message

            // Mock out a change getting pushed down into the careersNetworkViewModel, which will
            // totally swap out the underlying conversation
            const json = hiringRelationshipViewModel.hiringRelationship.asJson();
            const messageJson = _.first(json.conversation.messages);
            const receipt = _.detect(
                messageJson.receipts,
                receipt => receipt.receiver_id !== careersNetworkViewModel.user.id,
            );
            receipt.is_read = true;
            careersNetworkViewModel._onHiringRelationshipsLoaded([HiringRelationship.new(json)]);
            scope.$digest();

            SpecHelper.expectNoElement(elem, '.unread'); // this is way down in the connection-conversation-message
        });
    });

    describe('messages', () => {
        it('should render messages', () => {
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            render(hiringRelationshipViewModel);
            SpecHelper.expectElements(
                elem,
                'connection-conversation-message',
                hiringRelationshipViewModel.conversation.messages.length,
            );
        });

        it('should display correct time', () => {
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            const date = new Date('2016/12/09 12:00');
            _.each(hiringRelationshipViewModel.conversation.messages, message => {
                message.created_at = date.getTime() / 1000;
            });
            render(hiringRelationshipViewModel);
            SpecHelper.expectElementText(elem, 'connection-conversation-message:eq(0) .time', '12/09/2016');
        });

        it('should send a new message', () => {
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            render(hiringRelationshipViewModel);

            jest.spyOn(scope.hiringRelationshipViewModel, 'sendMessage').mockImplementation(() => {});

            const textarea = SpecHelper.expectElement(elem, '.connection-conversation-send-message .new-message-box');
            expect(textarea.attr('placeholder')).toEqual(
                `Send message to ${hiringRelationshipViewModel.connectionName}`,
            );
            SpecHelper.updateTextArea(elem, '.new-message-box', 'This is a new message');
            // SpecHelper.click(elem, '.send-message');
            SpecHelper.submitForm(elem);

            expect(scope.hiringRelationshipViewModel.sendMessage).toHaveBeenCalledWith('This is a new message');
        });

        it('should disable the send button if no text for new message', () => {
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            render(hiringRelationshipViewModel);

            jest.spyOn(scope.hiringRelationshipViewModel, 'sendMessage').mockImplementation(() => {});
            SpecHelper.updateTextArea(elem, '.new-message-box', '');
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, '.send-message');
        });

        it('should not error on rendering duplicate paragraphs', () => {
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            render(hiringRelationshipViewModel);
            const targetParagraphs = hiringRelationshipViewModel.conversation.messages[0].paragraphs;
            targetParagraphs.push(targetParagraphs[0]);
            expect(() => {
                scope.$digest();
            }).not.toThrow();
        });

        it('should show proper state for read and unread message receipts', () => {
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            jest.spyOn(hiringRelationshipViewModel, 'theirDisplayNameForConversation', 'get').mockReturnValue('them');
            const message = hiringRelationshipViewModel.addMessage('One more');

            // we don't set ids or create receipts on the client, normally the server does that
            message.id = 'new-message';
            message.created_at =
                _.chain(hiringRelationshipViewModel.conversation.messages).pluck('created_at').max().value() + 1;
            message.receipts.push({
                receiver_id: hiringRelationshipViewModel.connectionProfile.user_id,
            });
            const messagesFromMe = hiringRelationshipViewModel.messagesFromMe;
            expect(messagesFromMe[1]).not.toBeUndefined();
            const messagesFromThem = hiringRelationshipViewModel.conversationViewModel.messagesFromOthers;

            const receipts = _.map(messagesFromMe.slice(0, 2), message =>
                _.detect(message.receipts, receipt => receipt.receiver_id !== careersNetworkViewModel.user.id),
            );

            receipts[0].is_read = true;
            receipts[1].is_read = false;

            render(hiringRelationshipViewModel);
            scope.$digest();

            SpecHelper.expectElements(
                elem,
                'connection-conversation-message',
                hiringRelationshipViewModel.conversation.messages.length,
            );
            SpecHelper.expectNoElement(
                elem,
                `connection-conversation-message[message-id="${messagesFromThem[0].id}"] .receipt`,
            );
            SpecHelper.expectElementText(
                elem,
                `connection-conversation-message[message-id="${messagesFromMe[0].id}"] .receipt`,
                'Read by them',
            );
            SpecHelper.expectElementText(
                elem,
                `connection-conversation-message[message-id="${messagesFromMe[1].id}"] .receipt`,
                'Sent, unread by them',
            );
        });
    });

    describe('hiringManager badge', () => {
        let hiringRelationshipViewModel;

        beforeEach(() => {
            hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
        });

        it('should have CONNECTED badge', () => {
            jest.spyOn(hiringRelationshipViewModel, 'newMatch', 'get').mockReturnValue(false);
            render(hiringRelationshipViewModel);
            const badgeEl = SpecHelper.expectElementText(elem, '.connection-badge', 'CONNECTED');
            SpecHelper.expectElementHasClass(badgeEl, 'connected');
        });

        it('should have NEW MATCH badge', () => {
            jest.spyOn(hiringRelationshipViewModel, 'newMatch', 'get').mockReturnValue(true);
            render(hiringRelationshipViewModel);

            const badgeEl = SpecHelper.expectElementText(elem, '.connection-badge', 'NEW MATCH');
            SpecHelper.expectElementHasClass(badgeEl, 'new-match');
        });

        it('should have CLOSED badge', () => {
            jest.spyOn(hiringRelationshipViewModel, 'closed', 'get').mockReturnValue(true);
            render(hiringRelationshipViewModel);

            const badgeEl = SpecHelper.expectElementText(elem, '.connection-badge', 'CLOSED');
            SpecHelper.expectElementHasClass(badgeEl, 'declined');
        });
    });

    describe('candidate badge', () => {
        let hiringRelationshipViewModel;

        beforeEach(() => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
        });

        it('should have NEW REQUEST badge', () => {
            jest.spyOn(hiringRelationshipViewModel, 'newRequest', 'get').mockReturnValue(true);
            render(hiringRelationshipViewModel, 'candidate');

            const badge = SpecHelper.expectElement(elem, '.connection-badge');
            expect(badge.text()).toEqual('NEW REQUEST');
            SpecHelper.expectElementHasClass(badge, 'new-request');
        });

        it('should have NEW MATCH badge', () => {
            jest.spyOn(hiringRelationshipViewModel, 'newRequest', 'get').mockReturnValue(false);
            jest.spyOn(hiringRelationshipViewModel, 'newInstantMatch', 'get').mockReturnValue(true);
            render(hiringRelationshipViewModel, 'candidate');

            const badge = SpecHelper.expectElement(elem, '.connection-badge');
            expect(badge.text()).toEqual('NEW MATCH');
            SpecHelper.expectElementHasClass(badge, 'new-instant-match');
        });

        it('should have CLOSED badge', () => {
            jest.spyOn(hiringRelationshipViewModel, 'newRequest', 'get').mockReturnValue(false);
            jest.spyOn(hiringRelationshipViewModel, 'newInstantMatch', 'get').mockReturnValue(false);
            jest.spyOn(hiringRelationshipViewModel, 'closed', 'get').mockReturnValue(true);
            render(hiringRelationshipViewModel, 'candidate');

            const badge = SpecHelper.expectElement(elem, '.connection-badge');
            expect(badge.text()).toEqual('CLOSED');
            SpecHelper.expectElementHasClass(badge, 'declined');
        });

        it('should have DECLINED badge when rejected by candidate', () => {
            jest.spyOn(hiringRelationshipViewModel, 'newRequest', 'get').mockReturnValue(false);
            jest.spyOn(hiringRelationshipViewModel, 'newInstantMatch', 'get').mockReturnValue(false);
            jest.spyOn(hiringRelationshipViewModel, 'rejectedByCandidate', 'get').mockReturnValue(true);
            render(hiringRelationshipViewModel, 'candidate');

            const badge = SpecHelper.expectElement(elem, '.connection-badge');
            expect(badge.text()).toEqual('DECLINED');
            SpecHelper.expectElementHasClass(badge, 'declined');
        });
    });

    describe('close/decline', () => {
        it('should be close for candidate when active match', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            jest.spyOn(hiringRelationshipViewModel, 'decline').mockImplementation(() => {});
            render(hiringRelationshipViewModel, 'candidate');
            SpecHelper.expectElementText(elem, '.close-relationship-link', 'Close');
            SpecHelper.click(elem, '.close-relationship-link span');
            expect(hiringRelationshipViewModel.decline).toHaveBeenCalled();
        });

        it('should be decline for candidate when new request', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            jest.spyOn(hiringRelationshipViewModel, 'newRequest', 'get').mockReturnValue(true);
            jest.spyOn(hiringRelationshipViewModel, 'decline').mockImplementation(() => {});
            render(hiringRelationshipViewModel, 'candidate');
            SpecHelper.expectElementText(elem, '.close-relationship-link', 'Decline');
            SpecHelper.click(elem, '.close-relationship-link span');
            expect(hiringRelationshipViewModel.decline).toHaveBeenCalled();
        });

        it('should be close for hiringManager', () => {
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            jest.spyOn(hiringRelationshipViewModel, 'decline').mockImplementation(() => {});
            render(hiringRelationshipViewModel, 'hiringManager');
            SpecHelper.expectElementText(elem, '.close-relationship-link', 'Close');
            SpecHelper.click(elem, '.close-relationship-link span');
            expect(hiringRelationshipViewModel.decline).toHaveBeenCalled();
        });

        it('should not show send message textarea and instead display messaging disabled text', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            Object.defineProperty(hiringRelationshipViewModel, 'closed', {
                value: true,
            });
            render(hiringRelationshipViewModel, 'candidate');

            SpecHelper.expectNoElement(elem, '.connection-conversation-send-message');
            SpecHelper.expectNoElement(elem, '.close-relationship-link');
            SpecHelper.expectElement(elem, '.no-messaging');
            SpecHelper.expectElementText(
                elem,
                '.no-messaging',
                'This conversation is closed. Messaging is disabled. Send a note to talent@smart.ly if you would like to re-open this conversation.',
            );
        });

        it('should display correct messaging disabled text for a hiring manager', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            Object.defineProperty(hiringRelationshipViewModel, 'closed', {
                value: true,
            });
            render(hiringRelationshipViewModel, 'hiringManager');
            SpecHelper.expectElementText(
                elem,
                '.no-messaging',
                'This conversation is closed. Messaging is disabled. Send a note to hiring@smart.ly if you would like to re-open this conversation.',
            );
        });
    });
});
