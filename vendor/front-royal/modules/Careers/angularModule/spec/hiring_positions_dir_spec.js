import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';
import 'Careers/angularModule/spec/careers_spec_helper';
import setSpecLocales from 'Translation/setSpecLocales';
import hiringPositionsLocales from 'Careers/locales/careers/hiring_positions-en.json';
import positionsLocales from 'Careers/locales/careers/positions-en.json';

setSpecLocales(hiringPositionsLocales, positionsLocales);

describe('FrontRoyal.Careers.HiringPositions', () => {
    let $injector;
    let SpecHelper;
    let CareersSpecHelper;
    let renderer;
    let elem;
    let scope;
    let $rootScope;
    let $location;
    let $q;
    let careersNetworkViewModel;
    let User;
    let OpenPosition;
    let CandidatePositionInterest;
    let currentUser;
    let HiringTeam;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                $rootScope = $injector.get('$rootScope');
                $location = $injector.get('$location');
                $q = $injector.get('$q');
                User = $injector.get('User');
                OpenPosition = $injector.get('OpenPosition');
                HiringTeam = $injector.get('HiringTeam');
                CandidatePositionInterest = $injector.get('CandidatePositionInterest');
                $timeout = $injector.get('$timeout');

                $injector.get('HiringTeamFixtures');
            },
        ]);

        SpecHelper.stubDirective('positionsList');
        SpecHelper.stubDirective('positionForm');
        SpecHelper.stubDirective('positionReview');
        SpecHelper.stubDirective('candidateListCard');
        SpecHelper.stubDirective('candidateListActions');

        currentUser = SpecHelper.stubCurrentUser();
        currentUser.hasSeenFeaturedPositionsPage = true;
        currentUser.hiring_team = HiringTeam.fixtures.getInstance();

        careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<hiring-positions></hiring-positions>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('currentTab', () => {
        describe('get', () => {
            describe('when no action query param', () => {
                it('should be openPositions if list query param does not exist', () => {
                    jest.spyOn($location, 'search').mockImplementation(() => ({}));
                    render();
                    expect(scope.currentTab).toEqual('openPositions');
                });

                it('should be openPositions if list query param is not valid', () => {
                    jest.spyOn($location, 'search').mockImplementation(() => ({
                        list: 'foobar',
                    }));
                    render();
                    expect(scope.currentTab).toEqual('openPositions');
                });
            });

            describe('with action query param', () => {
                it('should not change currentTab', () => {
                    jest.spyOn($location, 'search').mockImplementation(() => ({
                        action: 'foo',
                        list: 'bar',
                    }));
                    render();
                    expect(scope.currentTab).toEqual('bar');
                });
            });
        });

        describe('set', () => {
            beforeEach(() => {
                render();
            });

            it('should set list and currentTab to openPositions if not archivedPositions', () => {
                scope.currentTab = 'foobar';
                expect($location.search().list).toEqual('openPositions');
                expect(scope.currentTab).toEqual('openPositions');
            });

            it('should set list and currentTab to archivedPositions', () => {
                scope.currentTab = 'archivedPositions';
                expect($location.search().list).toEqual('archivedPositions');
                expect(scope.currentTab).toEqual('archivedPositions');
            });
        });
    });

    describe('hasSeenFeaturedPositionsPage', () => {
        it('should update currentUser.hasSeenFeaturedPositionsPage', () => {
            currentUser.hasSeenFeaturedPositionsPage = false;
            User.expect('update');
            CandidatePositionInterest.expect('index');
            render();
            User.flush('update');
            expect(currentUser.hasSeenFeaturedPositionsPage).toEqual(true);
        });
    });

    describe('without positionId', () => {
        it('should have tabs for switching between positions lists', () => {
            Object.defineProperty(currentUser, 'hasAcceptedHiringManagerAccess', {
                get() {
                    return true;
                },
            });

            render();

            SpecHelper.expectElementText(elem, '.tabs button.open-positions', 'Open');
            SpecHelper.expectElementText(elem, '.tabs button.archived-positions', 'Closed');

            assertTabWithPositionsList('open-positions', 'openPositions', false, 'hiringManager');
            assertTabWithPositionsList('archived-positions', 'archivedPositions', true, 'hiringManager');
        });

        it('should render position-form when user !hasAcceptedHiringManagerAccess', () => {
            render();
            $timeout.flush();
            SpecHelper.expectElement(elem, 'position-form');
        });
    });

    function assertTabWithPositionsList(tab, searchParam, archived) {
        SpecHelper.click(elem, `.tabs button.${tab}`);
        SpecHelper.expectElements(elem, '.pane.open-positions', tab === 'open-positions' ? 1 : 0);
        SpecHelper.expectElements(elem, '.pane.archived-positions', tab === 'archived-positions' ? 1 : 0);

        SpecHelper.expectHasClass(elem, '.tabs button.open-positions', 'active', tab === 'open-positions');
        SpecHelper.expectHasClass(elem, '.tabs button.archived-positions', 'active', tab === 'archived-positions');

        expect($location.search().list).toEqual(searchParam);
        SpecHelper.expectElementAttr(elem, 'positions-list', 'archived', archived ? 'true' : 'false');
    }

    describe('with action', () => {
        beforeEach(() => {
            Object.defineProperty(currentUser, 'hasAcceptedHiringManagerAccess', {
                get() {
                    return true;
                },
            });
        });

        describe('create', () => {
            it('should render position form', () => {
                render();
                $location.search('action', 'create');
                $timeout.flush();
                SpecHelper.expectNoElement(elem, '.positions-container');
                SpecHelper.expectElement(elem, 'position-form');
            });

            it('should set location info on position based on company location', () => {
                currentUser.hiring_application = {
                    place_id: 'some_place_id',
                    place_details: {
                        formatted_address: 'Seattle, WA',
                    },
                };
                render();

                $location.search('action', 'create');
                $timeout.flush();

                SpecHelper.expectElement(elem, 'position-form');
                expect(scope.position.place_id).toEqual('some_place_id');
                expect(scope.position.place_details).toEqual({
                    formatted_address: 'Seattle, WA',
                });
            });

            // We had a bug where moving from page 1 to page 2 (the preview)
            // on the position form would reset the position.
            it('should not reset the position when the page changes', () => {
                render();
                $location.search('action', 'create');
                scope.$apply();

                const position = scope.position;
                $location.search('page', '2');
                scope.$apply();

                expect(scope.position).toBe(position);
            });
        });

        describe('for OpenPosition record', () => {
            it("should only ensureCandidatePositionInterests for 'review' action", () => {
                jest.spyOn(careersNetworkViewModel, 'ensureOpenPositions').mockReturnValue(
                    $q.when([
                        OpenPosition.new({
                            id: 'some_id',
                        }),
                    ]),
                );
                jest.spyOn(careersNetworkViewModel, 'ensureCandidatePositionInterests').mockReturnValue($q.when());
                render();

                // when no action
                expect(careersNetworkViewModel.ensureCandidatePositionInterests).not.toHaveBeenCalled();

                // when 'create' action
                $location.search('action', 'create');
                scope.$digest();
                expect(careersNetworkViewModel.ensureCandidatePositionInterests).not.toHaveBeenCalled();

                // when 'edit' action
                $location.search('action', 'edit');
                $location.search('positionId', 'some_id');
                scope.$digest();
                expect(careersNetworkViewModel.ensureCandidatePositionInterests).not.toHaveBeenCalled();

                // when 'review' action
                $location.search('action', 'review');
                $location.search('positionId', 'some_id');
                scope.$digest();
                expect(careersNetworkViewModel.ensureCandidatePositionInterests).toHaveBeenCalled();
            });

            it('should set location info on position based on existing location info on position', () => {
                currentUser.hiring_application = {
                    place_id: 'some_place_id',
                    place_details: {
                        formatted_address: 'Seattle, WA',
                    },
                };
                // mock out index call for open positions and ensure that the open position
                // returned has location info set
                OpenPosition.expect('index').returns([
                    OpenPosition.new({
                        id: 'some_id',
                        place_id: 'another_place_id',
                        place_details: {
                            formatted_address: 'Harrisonburg, VA',
                        },
                    }),
                ]);
                render();

                $location.search('positionId', 'some_id');
                $location.search('action', 'edit');
                scope.$apply();

                OpenPosition.flush('index');

                SpecHelper.expectElement(elem, 'position-form');
                expect(scope.position.place_id).toEqual('another_place_id');
                expect(scope.position.place_details).toEqual({
                    formatted_address: 'Harrisonburg, VA',
                });
            });

            it('should set location info on position based on company location if position location not present', () => {
                currentUser.hiring_application = {
                    place_id: 'some_place_id',
                    place_details: {
                        formatted_address: 'Seattle, WA',
                    },
                };
                // mock out index call for open positions and ensure that the open position
                // returned doesn't have location info already set
                OpenPosition.expect('index').returns([
                    OpenPosition.new({
                        id: 'some_id',
                    }),
                ]);
                render();

                $location.search('positionId', 'some_id');
                $location.search('action', 'edit');
                scope.$apply();

                OpenPosition.flush('index');

                SpecHelper.expectElement(elem, 'position-form');
                expect(scope.position.place_id).toEqual('some_place_id');
                expect(scope.position.place_details).toEqual({
                    formatted_address: 'Seattle, WA',
                });
            });
        });
    });

    describe('candidatePositionInterestConflict', () => {
        it('should reload interests and reset the position on interest conflict', () => {
            const positions = [
                OpenPosition.new({
                    id: 'open-position-id',
                }),
            ];

            jest.spyOn(careersNetworkViewModel, 'ensureOpenPositions').mockReturnValue($q.when(positions));

            const interests = [
                CandidatePositionInterest.new({
                    id: 'interest-id',
                    hiring_manager_status: 'pending',
                    open_position_id: 'open-position-id',
                }),
            ];

            const updatedInterests = _.clone(interests);
            updatedInterests[0].hiring_manager_status = 'rejected';
            jest.spyOn(careersNetworkViewModel, 'ensureCandidatePositionInterests')
                .mockReturnValueOnce($q.when(interests))
                .mockReturnValueOnce($q.when(updatedInterests));
            render();

            // go to 'review'
            $location.search('action', 'review');
            $location.search('positionId', 'open-position-id');
            scope.$digest();
            expect(careersNetworkViewModel.ensureCandidatePositionInterests).toHaveBeenCalled();
            expect(scope.position.id).toEqual(positions[0].id);
            const firstPosition = scope.position;

            // now trigger a conflict from the careers network view model
            $rootScope.$broadcast('candidatePositionInterestConflict');
            scope.$digest();
            expect(careersNetworkViewModel.ensureCandidatePositionInterests).toHaveBeenCalled();

            expect(scope.position).not.toBe(firstPosition); // position was rebuilt
            expect(scope.position.id).toEqual(positions[0].id);
        });
    });

    describe('has_full_access', () => {
        function assertHasFullAccessChange(from, to) {
            currentUser.hiring_team = HiringTeam.fixtures.getInstance();

            Object.defineProperty(currentUser, 'hasFullHiringManagerAccess', {
                value: from,
                configurable: true,
            });

            const positions = [
                OpenPosition.new({
                    id: 'open-position-id',
                }),
            ];

            jest.spyOn(careersNetworkViewModel, 'ensureOpenPositions').mockReturnValue($q.when(positions));

            const interests = [
                CandidatePositionInterest.new({
                    id: 'interest-id',
                    hiring_manager_status: 'pending',
                    open_position_id: 'open-position-id',
                }),
            ];

            const updatedInterests = _.clone(interests);
            updatedInterests[0].hiring_manager_status = 'rejected';
            jest.spyOn(careersNetworkViewModel, 'ensureCandidatePositionInterests')
                .mockReturnValueOnce($q.when(interests))
                .mockReturnValueOnce($q.when(updatedInterests));
            render();

            // go to 'review'
            $location.search('action', 'review');
            $location.search('positionId', 'open-position-id');
            scope.$digest();
            expect(careersNetworkViewModel.ensureCandidatePositionInterests).toHaveBeenCalled();
            expect(scope.position.id).toEqual(positions[0].id);
            const firstPosition = scope.position;

            // now change the user's standing
            Object.defineProperty(currentUser, 'hasFullHiringManagerAccess', {
                value: to,
                configurable: true,
            });
            scope.$digest();
            expect(careersNetworkViewModel.ensureCandidatePositionInterests).toHaveBeenCalled();

            expect(scope.position).not.toBe(firstPosition); // position was rebuilt
            expect(scope.position.id).toEqual(positions[0].id);
        }

        it('should reload interests if team goes from having full access to not having full access', () => {
            assertHasFullAccessChange(true, false);
        });

        it('should reload interests if team goes from not having full access to having full access', () => {
            assertHasFullAccessChange(false, true);
        });
    });
});
