import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/candidate_position_interest_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/open_position_fixtures';
import 'Careers/angularModule/spec/careers_spec_helper';

describe('FrontRoyal.Careers.PositionsWithInterestsHelper', () => {
    let $injector;
    let CareersSpecHelper;
    let PositionsWithInterestsHelper;
    let positionsWithInterestsHelper;
    let careersNetworkViewModel;
    let $q;
    let $timeout;
    let OpenPosition;
    let CandidatePositionInterest;
    let position;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                PositionsWithInterestsHelper = $injector.get('PositionsWithInterestsHelper');
                $q = $injector.get('$q');
                $timeout = $injector.get('$timeout');
                OpenPosition = $injector.get('OpenPosition');
                CandidatePositionInterest = $injector.get('CandidatePositionInterest');

                $injector.get('OpenPositionFixtures');
                $injector.get('CandidatePositionInterestFixtures');
            },
        ]);

        careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');

        position = OpenPosition.fixtures.getInstance({
            id: 1,
        });

        positionsWithInterestsHelper = new PositionsWithInterestsHelper(careersNetworkViewModel);
    });

    describe('loadPositionsAndInterests', () => {
        it('should ensureOpenPositions, ensureCandidatePositionInterests, and then return an instance of PositionsWithInterestsHelper', () => {
            const firstDeferred = $q.defer();
            const secondDeferred = $q.defer();
            jest.spyOn(careersNetworkViewModel, 'ensureOpenPositions').mockReturnValue(firstDeferred.promise);
            jest.spyOn(careersNetworkViewModel, 'ensureCandidatePositionInterests').mockReturnValue(
                secondDeferred.promise,
            );
            PositionsWithInterestsHelper.loadPositionsAndInterests(careersNetworkViewModel).then(
                _positionsWithInterestsHelper => {
                    expect(_positionsWithInterestsHelper).toBeInstanceOf(PositionsWithInterestsHelper);
                },
            );
            expect(careersNetworkViewModel.ensureOpenPositions).toHaveBeenCalled();
            firstDeferred.resolve();
            $timeout.flush(); // propogate promise to `then` callback
            expect(careersNetworkViewModel.ensureCandidatePositionInterests).toHaveBeenCalled();
            secondDeferred.resolve();
            $timeout.flush(); // propogate promise to `then` callback
        });
    });

    describe('hideRelatedCandidatePositionInterests', () => {
        it('should call hideRelatedCandidatePositionInterests on careersNetworkViewModel and then clear interest cache for related interests', () => {
            const openPositions = [
                OpenPosition.fixtures.getInstance({
                    id: 1,
                }),
                OpenPosition.fixtures.getInstance({
                    id: 2,
                }),
                OpenPosition.fixtures.getInstance({
                    id: 3,
                }),
            ];
            careersNetworkViewModel.openPositions = openPositions;

            const deferred = $q.defer();
            jest.spyOn(careersNetworkViewModel, 'hideRelatedCandidatePositionInterests').mockReturnValue(
                deferred.promise,
            );

            jest.spyOn(positionsWithInterestsHelper, 'clearInterestCachesForPosition');

            const mockHiringRelationshipViewModel = {};
            positionsWithInterestsHelper.hideRelatedCandidatePositionInterests(mockHiringRelationshipViewModel);
            expect(careersNetworkViewModel.hideRelatedCandidatePositionInterests).toHaveBeenCalledWith(
                mockHiringRelationshipViewModel,
            );

            deferred.resolve([
                CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: 1,
                }),
                CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: 2,
                }),
            ]);
            $timeout.flush(); // propogate promise to `then` callback

            expect(positionsWithInterestsHelper.clearInterestCachesForPosition).toHaveBeenCalledWith(openPositions[0]);
            expect(positionsWithInterestsHelper.clearInterestCachesForPosition).toHaveBeenCalledWith(openPositions[1]);
            expect(positionsWithInterestsHelper.clearInterestCachesForPosition).not.toHaveBeenCalledWith(
                openPositions[2],
            );
        });
    });

    describe('interests', () => {
        it('should cache and return the position interests for the passed in position order by their created_at values', () => {
            const now = new Date().getTime();
            careersNetworkViewModel.candidatePositionInterests = [
                CandidatePositionInterest.fixtures.getInstance({
                    created_at: now - 1000,
                    open_position_id: 1,
                }),
                CandidatePositionInterest.fixtures.getInstance({
                    created_at: now,
                    open_position_id: 1,
                }),
                CandidatePositionInterest.fixtures.getInstance({
                    created_at: now - 500,
                    open_position_id: 1,
                }),
                CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: 2,
                }),
                CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: 3,
                }),
            ];

            const interests = positionsWithInterestsHelper.interests(position);
            const expectedInterests = _.chain(careersNetworkViewModel.candidatePositionInterests)
                .where({
                    open_position_id: 1,
                })
                .sortBy('created_at')
                .value();
            expect(interests).toEqual(expectedInterests);
            expect(positionsWithInterestsHelper.caches.interestsByPositionId[position.id]).toEqual(expectedInterests);
        });
    });

    describe('numInterests', () => {
        it('should return number of interests for the position', () => {
            jest.spyOn(positionsWithInterestsHelper, 'interests').mockReturnValue([{}, {}, {}]); // we don't care about data stored in the interests that are returned, just how many there are
            expect(positionsWithInterestsHelper.numInterests(position)).toEqual(3);
        });
    });

    describe('numReviewed', () => {
        it('should cache and return the number of position interests for the passed in position that are reviewedByHiringManager', () => {
            const interests = [
                CandidatePositionInterest.fixtures.getInstance(),
                CandidatePositionInterest.fixtures.getInstance(),
                CandidatePositionInterest.fixtures.getInstance(),
                CandidatePositionInterest.fixtures.getInstance(),
            ];
            jest.spyOn(positionsWithInterestsHelper, 'interests').mockReturnValue(interests);
            jest.spyOn(interests[0], 'reviewedByHiringManager', 'get').mockReturnValue(true);
            jest.spyOn(interests[1], 'reviewedByHiringManager', 'get').mockReturnValue(false);
            jest.spyOn(interests[2], 'reviewedByHiringManager', 'get').mockReturnValue(true);
            jest.spyOn(interests[3], 'reviewedByHiringManager', 'get').mockReturnValue(true);
            expect(positionsWithInterestsHelper.numReviewed(position)).toEqual(3);
            expect(positionsWithInterestsHelper.caches.numReviewed[position.id]).toEqual(3);
        });
    });

    describe('numUnreviewed', () => {
        it('should cache and return the number of position interests for the passed in position that are !reviewedByHiringManager', () => {
            const interests = [
                CandidatePositionInterest.fixtures.getInstance(),
                CandidatePositionInterest.fixtures.getInstance(),
                CandidatePositionInterest.fixtures.getInstance(),
                CandidatePositionInterest.fixtures.getInstance(),
            ];
            jest.spyOn(positionsWithInterestsHelper, 'interests').mockReturnValue(interests);
            jest.spyOn(interests[0], 'reviewedByHiringManager', 'get').mockReturnValue(true);
            jest.spyOn(interests[1], 'reviewedByHiringManager', 'get').mockReturnValue(false);
            jest.spyOn(interests[2], 'reviewedByHiringManager', 'get').mockReturnValue(true);
            jest.spyOn(interests[3], 'reviewedByHiringManager', 'get').mockReturnValue(true);
            expect(positionsWithInterestsHelper.numUnreviewed(position)).toEqual(1);
            expect(positionsWithInterestsHelper.caches.numUnreviewed[position.id]).toEqual(1);
        });
    });

    describe('numNewCandidates', () => {
        it("should cache and return the number of position interests for the passed in position where the hiring_manager_status is 'unseen'", () => {
            const interests = [
                CandidatePositionInterest.fixtures.getInstance({
                    hiring_manager_status: 'unseen',
                }),
                CandidatePositionInterest.fixtures.getInstance({
                    hiring_manager_status: 'pending',
                }),
                CandidatePositionInterest.fixtures.getInstance({
                    hiring_manager_status: 'unseen',
                }),
                CandidatePositionInterest.fixtures.getInstance({
                    hiring_manager_status: 'unseen',
                }),
            ];
            jest.spyOn(positionsWithInterestsHelper, 'interests').mockReturnValue(interests);
            expect(positionsWithInterestsHelper.numNewCandidates(position)).toEqual(3);
            expect(positionsWithInterestsHelper.caches.numNewCandidates[position.id]).toEqual(3);
        });
    });

    describe('hasNewCandidates', () => {
        it('should be true if numNewCandidates is greater than 0', () => {
            jest.spyOn(positionsWithInterestsHelper, 'numNewCandidates').mockReturnValue(1);
            expect(positionsWithInterestsHelper.hasNewCandidates(position)).toBe(true);
            expect(positionsWithInterestsHelper.numNewCandidates).toHaveBeenCalledWith(position);
        });

        it('should be false if numNewCandidates is not greater than 0', () => {
            jest.spyOn(positionsWithInterestsHelper, 'numNewCandidates').mockReturnValue(0);
            expect(positionsWithInterestsHelper.hasNewCandidates(position)).toBe(false);
            expect(positionsWithInterestsHelper.numNewCandidates).toHaveBeenCalledWith(position);
        });
    });

    describe('sortOpenPositions', () => {
        it('should sort openPositions on careersNetworkViewModel by unseen interests first, seen interests, and then the remaining positions', () => {
            const positions = [
                OpenPosition.fixtures.getInstance(),
                OpenPosition.fixtures.getInstance(),
                OpenPosition.fixtures.getInstance({
                    created_at: 1,
                }),
                OpenPosition.fixtures.getInstance(),
                OpenPosition.fixtures.getInstance(),
                OpenPosition.fixtures.getInstance({
                    creatd_at: 2,
                }),
            ];
            careersNetworkViewModel.openPositions = positions;

            jest.spyOn(positionsWithInterestsHelper, 'hasNewCandidates').mockImplementation(position => {
                if (position === positions[0] || position === positions[3]) {
                    return true;
                }
                return false;
            });

            jest.spyOn(positionsWithInterestsHelper, 'numInterests').mockImplementation(position => {
                if (position === positions[1]) {
                    return 1;
                }
                if (position === positions[4]) {
                    return 2;
                }
                return 0;
            });

            jest.spyOn(positionsWithInterestsHelper, 'numNewCandidates').mockImplementation(position => {
                if (position === positions[0]) {
                    return 1;
                }
                if (position === positions[3]) {
                    return 2;
                }
                throw new Error('positionsWithInterestsHelper#numNewCandidates called with unexpected position');
            });

            const sortedOpenPositions = positionsWithInterestsHelper.sortOpenPositions();
            expect(sortedOpenPositions).toEqual([
                positions[3],
                positions[0],
                positions[4],
                positions[1],
                positions[5],
                positions[2],
            ]);
        });
    });

    describe('clearInterestCachesForPosition', () => {
        it('should delete entries for position in the caches', () => {
            const cacheKeys = Object.keys(positionsWithInterestsHelper.caches);
            cacheKeys.forEach(key => {
                positionsWithInterestsHelper.caches[key][position.id] = 'some_value';
            });

            positionsWithInterestsHelper.clearInterestCachesForPosition(position);

            cacheKeys.forEach(key => {
                expect(positionsWithInterestsHelper.caches[key][position.id]).toBeUndefined();
            });
        });
    });
});
