import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import setSpecLocales from 'Translation/setSpecLocales';
import jobPreferencesFormLocales from 'Careers/locales/careers/edit_career_profile/job_preferences_form-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import careersLocales from 'Careers/locales/careers/careers-en.json';

setSpecLocales(
    jobPreferencesFormLocales,
    fieldOptionsLocales,
    candidateListCardLocales,
    editCareerProfileLocales,
    careersLocales,
);

describe('FrontRoyal.Careers.CandidateCardPreview', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let CareerProfile;
    let user;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');
                $injector.get('CareerProfileFixtures');
            },
        ]);

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.showProfile = angular.isDefined(opts.showProfile) ? opts.showProfile : true;
        renderer.scope.fullProfileDisplayMode = opts.fullProfileDisplayMode || false;
        renderer.scope.allowEdit = opts.allowEdit || false;
        renderer.render(
            '<candidate-card-preview show-profile="showProfile" full-profile-display-mode="fullProfileDisplayMode" allow-edit="allowEdit"></candidate-card-preview>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('when showProfile', () => {
        beforeEach(() => {
            render({
                showProfile: true,
            });
        });

        it('should show the candidate-list-card and not the empty learner placeholder images', () => {
            SpecHelper.expectElement(elem, 'candidate-list-card');
            SpecHelper.expectNoElement(elem, '.empty-learner-img');
        });

        describe('when allowEdit', () => {
            beforeEach(() => {
                scope.allowEdit = true;
                scope.$digest();
            });

            it('should show instructions for the edit buttons', () => {
                SpecHelper.expectElementText(
                    elem,
                    '.sub-text',
                    'Use the edit buttons to navigate to relevant parts of your profile.',
                );

                scope.allowEdit = false;
                scope.$digest();
                SpecHelper.expectNoElement(elem, '.sub-text');
            });

            describe('edit profile button', () => {
                it('should be visible', () => {
                    SpecHelper.expectElement(elem, '.edit-profile-button');

                    scope.allowEdit = false;
                    scope.$digest();
                    SpecHelper.expectNoElement(elem, '.edit-profile-button');
                });

                it('should loadRoute /settings/my-profile when clicked', () => {
                    jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
                    SpecHelper.click(elem, '.edit-profile-button button');
                    expect(scope.loadRoute).toHaveBeenCalledWith('/settings/my-profile');
                });
            });
        });
    });

    describe('when !showProfile', () => {
        beforeEach(() => {
            render({
                showProfile: false,
            });
        });

        it('should show the empty learner profile placeholder images and not the candidate-list-card', () => {
            SpecHelper.expectElement(elem, '.empty-learner-img.mobile');
            SpecHelper.expectElement(elem, '.empty-learner-img.desktop');
            SpecHelper.expectNoElement(elem, 'candidate-list-card');
        });
    });
});
