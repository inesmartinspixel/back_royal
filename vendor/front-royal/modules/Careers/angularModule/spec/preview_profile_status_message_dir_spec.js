import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import setSpecLocales from 'Translation/setSpecLocales';
import previewProfileStatusMessageLocales from 'Careers/locales/careers/preview_profile_status_message-en.json';

setSpecLocales(previewProfileStatusMessageLocales);

describe('FrontRoyal.Careers.PreviewProfileStatusMessage', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let CareerProfile;
    let user;
    let Cohort;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');
                Cohort = $injector.get('Cohort');

                $injector.get('CareerProfileFixtures');
                $injector.get('CohortApplicationFixtures');
            },
        ]);

        SpecHelper.stubConfig();

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.withProgressNav = opts.withProgressNav || false;
        renderer.render(
            '<preview-profile-status-message with-progress-nav="withProgressNav"></preview-profile-status-message>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('with access to edit career profile', () => {
        beforeEach(() => {
            user.can_edit_career_profile = true;
        });

        describe('with complete career profile', () => {
            beforeEach(() => {
                user.career_profile.last_calculated_complete_percentage = 100;
            });

            it('should behave properly when interested_in_joining_new_company set to an active interest level', () => {
                user.career_profile.interested_in_joining_new_company = CareerProfile.ACTIVE_INTEREST_LEVELS[0];
                render();
                expect(scope.status).toEqual('active');
                SpecHelper.expectElementText(elem, '.title', 'Profile Active!');
                SpecHelper.expectElementText(
                    elem,
                    '.description',
                    'Looking good! Your profile is complete and active in the career network. Employers that are interested in connecting can reach out to you through messages in the Connections section.',
                );
            });

            it('should behave properly when interested_in_joining_new_company is not an active interest level', () => {
                user.career_profile.interested_in_joining_new_company = 'not_interested';
                render();
                expect(scope.status).toEqual('inactive');
                SpecHelper.expectElementText(elem, '.title', 'Profile Inactive');
                SpecHelper.expectElementText(
                    elem,
                    '.description',
                    'Your profile is complete, but currently deactivated. Activate your profile below to allow employers to match with you in the network.',
                );
            });
        });

        describe('with incomplete career profile', () => {
            beforeEach(() => {
                user.career_profile.last_calculated_complete_percentage = 99;
            });

            describe('when programType is for degreeProgram', () => {
                beforeEach(() => {
                    jest.spyOn(Cohort, 'isDegreeProgram').mockReturnValue(true);
                });

                _.each(['pre_accepted', 'accepted'], lastCohortApplicationStatus => {
                    describe(`when lastCohortApplicationStatus is ${lastCohortApplicationStatus}`, () => {
                        beforeEach(() => {
                            jest.spyOn(user, 'lastCohortApplicationStatus', 'get').mockReturnValue(
                                lastCohortApplicationStatus,
                            );
                        });

                        it('should set status to accepted_degree_incomplete_profile if withProgressNav', () => {
                            render({
                                withProgressNav: true,
                            });
                            expect(scope.withProgressNav).toBe(true);
                            expect(scope.status).toEqual('accepted_degree_incomplete_profile');
                            SpecHelper.expectElementText(elem, '.title', 'Profile Incomplete');
                            SpecHelper.expectElementText(
                                elem,
                                '.description',
                                'Congratulations on your acceptance! Please complete your profile as part of enrollment. Incomplete steps are marked with a blue circle below.',
                            );
                        });

                        it('should set status to accepted_degree_incomplete_profile_alt if !withProgressNav', () => {
                            render();
                            expect(scope.withProgressNav).toBe(false);
                            expect(scope.status).toEqual('accepted_degree_incomplete_profile_alt');
                            SpecHelper.expectElementText(elem, '.title', 'Profile Incomplete');
                            SpecHelper.expectElementText(
                                elem,
                                '.description',
                                'Congratulations on your acceptance! Please complete your profile as part of enrollment.',
                            );
                        });
                    });
                });
            });

            describe('when programType is not for degreeProgram', () => {
                beforeEach(() => {
                    jest.spyOn(Cohort, 'isDegreeProgram').mockReturnValue(false);
                });

                it("should set the status to 'incomplete'", () => {
                    render();
                    expect(scope.status).toEqual('incomplete');
                    SpecHelper.expectElementText(elem, '.title', 'Profile Incomplete');
                    SpecHelper.expectElementText(
                        elem,
                        '.description',
                        'Complete and activate your profile to allow employers to match with you in the network.',
                    );
                });
            });
        });
    });

    describe('with no access to edit career profile', () => {
        beforeEach(() => {
            user.can_edit_career_profile = false;
        });

        it('should behave properly when user isCareerNetworkOnly', () => {
            // We're mocking shouldSeeQuanticBranding here so that locale message says Smartly instead of Quantic, which is inline
            // with what we'd actually expect in the wild since the career_network_only program type is only for Smartly.
            jest.spyOn(user, 'shouldSeeQuanticBranding', 'get').mockReturnValue(false);
            jest.spyOn(user, 'isCareerNetworkOnly', 'get').mockReturnValue(true);
            render();
            expect(scope.status).toEqual('no_cn_access_cno');
            SpecHelper.expectElementText(elem, '.title', 'Career Network Access');
            SpecHelper.expectElementText(
                elem,
                '.description',
                'Thanks for applying to Smartly! Unfortunately, we don’t have enough opportunities that match your role and location preferences. We’ll notify you if and when that changes so you can gain access.',
            );
        });

        it('should behave properly when user isRejectedOrExpelled', () => {
            jest.spyOn(user, 'isRejectedOrExpelled', 'get').mockReturnValue(true);
            render();
            expect(scope.status).toEqual('no_cn_access');
            SpecHelper.expectElementText(elem, '.title', 'Career Network Access');
            SpecHelper.expectElementText(
                elem,
                '.description',
                'Thanks for applying to Quantic! Unfortunately, only accepted students can qualify for access to our career network. We appreciate your interest and wish you the best in your learning and career goals!',
            );
        });

        it('should behave properly with accepted degree program and a complete career profile', () => {
            jest.spyOn(user, 'lastCohortApplicationStatus', 'get').mockReturnValue('accepted');
            jest.spyOn(Cohort, 'isDegreeProgram').mockReturnValue(true);
            user.career_profile.last_calculated_complete_percentage = 100;
            render();
            expect(scope.status).toEqual('reviewing_degree');
            SpecHelper.expectElementText(elem, '.title', 'Congratulations!');
            SpecHelper.expectElementText(
                elem,
                '.description',
                'Your profile is now complete. After a brief review, it should be visible in our network within 1-2 weeks of the start of classes.',
            );
        });

        it('should behave properly with pre_accepted degree program and a complete career profile', () => {
            jest.spyOn(user, 'lastCohortApplicationStatus', 'get').mockReturnValue('pre_accepted');
            jest.spyOn(Cohort, 'isDegreeProgram').mockReturnValue(true);
            user.career_profile.last_calculated_complete_percentage = 100;
            render();
            expect(scope.status).toEqual('reviewing_degree');
            SpecHelper.expectElementText(elem, '.title', 'Congratulations!');
            SpecHelper.expectElementText(
                elem,
                '.description',
                'Your profile is now complete. After a brief review, it should be visible in our network within 1-2 weeks of the start of classes.',
            );
        });

        it('should behave properly with accepted degree program and an incomplete career profile and withProgressNav', () => {
            jest.spyOn(user, 'lastCohortApplicationStatus', 'get').mockReturnValue('accepted');
            jest.spyOn(Cohort, 'isDegreeProgram').mockReturnValue(true);
            user.career_profile.last_calculated_complete_percentage = 99;
            render({
                withProgressNav: true,
            });
            expect(scope.withProgressNav).toBe(true);
            expect(scope.status).toEqual('accepted_degree_incomplete_profile');
            SpecHelper.expectElementText(elem, '.title', 'Profile Incomplete');
            SpecHelper.expectElementText(
                elem,
                '.description',
                'Congratulations on your acceptance! Please complete your profile as part of enrollment. Incomplete steps are marked with a blue circle below.',
            );
        });

        it('should behave properly with pre_accepted degree program and an incomplete career profile and !withProgressNav', () => {
            jest.spyOn(user, 'lastCohortApplicationStatus', 'get').mockReturnValue('pre_accepted');
            jest.spyOn(Cohort, 'isDegreeProgram').mockReturnValue(true);
            user.career_profile.last_calculated_complete_percentage = 99;
            render();
            expect(scope.withProgressNav).toBe(false);
            expect(scope.status).toEqual('accepted_degree_incomplete_profile_alt');
            SpecHelper.expectElementText(elem, '.title', 'Profile Incomplete');
            SpecHelper.expectElementText(
                elem,
                '.description',
                'Congratulations on your acceptance! Please complete your profile as part of enrollment.',
            );
        });

        it('should behave properly with accepted non-degree program', () => {
            jest.spyOn(user, 'lastCohortApplicationStatus', 'get').mockReturnValue('accepted');
            jest.spyOn(Cohort, 'isDegreeProgram').mockReturnValue(false);
            render();
            expect(scope.status).toEqual('reviewing_non_degree');
            SpecHelper.expectElementText(elem, '.title', 'In Review');
            SpecHelper.expectElementText(
                elem,
                '.description',
                'Congratulations on your acceptance to the certificate program!At this time, we don’t have opportunities that match your interests, role or location preferences. We’ll notify you if and when that changes so you can gain access.',
            );
        });

        it('should behave properly with pre_accepted non-degree program', () => {
            jest.spyOn(user, 'lastCohortApplicationStatus', 'get').mockReturnValue('pre_accepted');
            jest.spyOn(Cohort, 'isDegreeProgram').mockReturnValue(false);
            render();
            expect(scope.status).toEqual('reviewing_non_degree');
            SpecHelper.expectElementText(elem, '.title', 'In Review');
            SpecHelper.expectElementText(
                elem,
                '.description',
                'Congratulations on your acceptance to the certificate program!At this time, we don’t have opportunities that match your interests, role or location preferences. We’ll notify you if and when that changes so you can gain access.',
            );
        });

        it('should behave properly when deferred', () => {
            jest.spyOn(user, 'lastCohortApplicationStatus', 'get').mockReturnValue('deferred');
            render();
            expect(scope.status).toEqual('deferred');
            SpecHelper.expectElementText(elem, '.title', 'Deferred');
            SpecHelper.expectElementText(
                elem,
                '.description',
                'Congratulations on your acceptance! Your profile will be activated after you join a new class.',
            );
        });

        it('should behave properly when user isPending', () => {
            jest.spyOn(user, 'isPending', 'get').mockReturnValue(true);
            render();
            expect(scope.status).toEqual('applied');
            SpecHelper.expectElementText(elem, '.title', 'Pending Application');
            SpecHelper.expectElementText(
                elem,
                '.description',
                "Your application to Quantic has been submitted! If you applied to the MBA, EMBA, or our certificate programs, your career profile will be reviewed at the same time as your program application.If you qualify, this is where you'll connect with hundreds of leading companies and innovative startups looking for candidates just like you!",
            );
        });
    });
});
