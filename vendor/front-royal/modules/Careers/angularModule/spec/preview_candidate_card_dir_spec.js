import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import setSpecLocales from 'Translation/setSpecLocales';
import previewCandidateCardLocales from 'Careers/locales/careers/preview_candidate_card-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import previewProfileStatusMessageLocales from 'Careers/locales/careers/preview_profile_status_message-en.json';
import careersLocales from 'Careers/locales/careers/careers-en.json';
import jobPreferencesFormLocales from 'Careers/locales/careers/edit_career_profile/job_preferences_form-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';

setSpecLocales(
    previewCandidateCardLocales,
    editCareerProfileLocales,
    previewProfileStatusMessageLocales,
    careersLocales,
    jobPreferencesFormLocales,
    fieldOptionsLocales,
    candidateListCardLocales,
);

describe('FrontRoyal.Careers.PreviewCandidateCard', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let CareerProfile;
    let Cohort;
    let user;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');
                Cohort = $injector.get('Cohort');

                $injector.get('CareerProfileFixtures');
                $injector.get('CohortFixtures');
            },
        ]);

        SpecHelper.stubConfig();

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
        user.relevant_cohort = Cohort.fixtures.getInstance({
            program_type: 'mba',
        });
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<preview-candidate-card></preview-candidate-card>');

        elem = renderer.elem;
    }

    it("should show the user's candidate card if profile is complete and user hasEditCareerProfileAccess", () => {
        jest.spyOn(user, 'hasEditCareerProfileAccess', 'get').mockReturnValue(true);
        Object.defineProperty(user.career_profile, 'complete', {
            value: true,
        });
        render();
        SpecHelper.expectElement(elem, 'candidate-list-card');
    });

    it("should not show the user's candidate card if profile is not complete", () => {
        Object.defineProperty(user.career_profile, 'complete', {
            value: false,
        });
        render();
        SpecHelper.expectNoElement(elem, 'candidate-list-card');
    });

    it("should not show the user's candidate card if profile complete but can_edit_career_profile is false", () => {
        Object.defineProperty(user.career_profile, 'complete', {
            value: true,
        });
        Object.defineProperty(user, 'can_edit_career_profile', {
            value: false,
        });
        render();
        SpecHelper.expectNoElement(elem, 'candidate-list-card');
    });
});
