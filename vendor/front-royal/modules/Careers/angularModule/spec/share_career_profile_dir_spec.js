import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import setSpecLocales from 'Translation/setSpecLocales';
import shareCareerProfileLocales from 'Careers/locales/careers/share_career_profile-en.json';

setSpecLocales(shareCareerProfileLocales);

describe('FrontRoyal.Careers.ShareCareerProfile', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let CareerProfile;
    let careerProfile;
    let currentUser;
    let $http;
    let $timeout;
    let $window;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');
                $http = $injector.get('$http');
                $timeout = $injector.get('$timeout');
                $window = $injector.get('$window');

                $injector.get('CareerProfileFixtures');
            },
        ]);

        careerProfile = CareerProfile.fixtures.getInstance();
        currentUser = SpecHelper.stubCurrentUser(undefined, undefined, undefined, {
            hiring_teammate_emails: ['foo@foo.com', 'bar@bar.com', 'qux@qux.com'],
        });
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.careerProfile = careerProfile;
        renderer.scope.onCloseCallback = expect.anything();
        renderer.render(
            '<share-career-profile career-profile="careerProfile" on-close-callback="onCloseCallback(openHiringTeamInviteModal)"></share-career-profile>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it("should show the candidate's avatar", () => {
        render();
        SpecHelper.expectElement(elem, 'careers-avatar');
    });

    describe('share', () => {
        beforeEach(() => {
            render();
        });

        describe('when !shareCandidateForm.$valid', () => {
            it('should not do anything', () => {
                expect(scope.shareCandidateForm.$invalid).toBe(true);
                jest.spyOn($http, 'get').mockImplementation(() => {});
                scope.share();
                expect($http.get).not.toHaveBeenCalled();
            });
        });

        describe('when shareCandidateForm.$valid', () => {
            beforeEach(() => {
                const $q = $injector.get('$q');
                jest.spyOn($http, 'get').mockReturnValue($q.when());
                scope.shareCandidateForm.$valid = jest.fn().mockReturnValue(true); // mock form validity
            });

            it('should GET share_career_profile, then set submitted to true, wait a little, then execute the onCloseCallback', () => {
                jest.spyOn(scope, 'onCloseCallback').mockImplementation(() => {});
                scope.share();
                expect(scope.submitted).not.toBe(true);
                expect(scope.onCloseCallback).not.toHaveBeenCalled();
                expect($http.get).toHaveBeenCalledWith(`${$window.ENDPOINT_ROOT}/api/share_career_profile.json`, {
                    params: {
                        record: {
                            id: scope.careerProfile.id,
                            email: undefined,
                            message: undefined,
                        },
                    },
                    paramSerializer: '$httpParamSerializerJQLike',
                });
                $timeout.flush(0);
                expect(scope.submitted).toBe(true);
                expect(scope.onCloseCallback).not.toHaveBeenCalled();
                $timeout.flush(1750);
                expect(scope.onCloseCallback).toHaveBeenCalled();
            });
        });
    });

    describe('when !currentUserHasAtLeastOneHiringTeammate', () => {
        beforeEach(() => {
            currentUser.hiring_teammate_emails = [];
            render();
            expect(scope.currentUserHasAtLeastOneHiringTeammate).toBe(false);
        });

        it('should NOT show the form fields', () => {
            SpecHelper.expectNoElement(elem, 'form[name="shareCandidateForm"] select[name="email"]');
            SpecHelper.expectNoElement(elem, 'form[name="shareCandidateForm"] textarea[name="message"]');
            SpecHelper.expectNoElement(elem, 'form[name="shareCandidateForm"] button[name="share-profile"]');
        });

        it('should have a title with some instructions explaining that they must first invite a coworker', () => {
            SpecHelper.expectElement(elem, '.title', 'Share This Profile');
            SpecHelper.expectElement(
                elem,
                '.title',
                "You don't have any teammates to share this profile with. Invite a new coworker to your team to share this profile with them.",
            );
        });
    });

    describe('when currentUserHasAtLeastOneHiringTeammate', () => {
        beforeEach(() => {
            render();
            expect(scope.currentUserHasAtLeastOneHiringTeammate).toBe(true);
        });

        it('should have a title with some instructions and a working link to openHiringTeamInviteModal', () => {
            SpecHelper.expectElementText(elem, '.title', 'Share This Profile');
            SpecHelper.expectElementText(
                elem,
                '.sub-title',
                'Select a teammate to share this profile with or invite a new coworker to your team.',
            );
            jest.spyOn(scope, 'openHiringTeamInviteModal');
            jest.spyOn(scope, 'onCloseCallback').mockImplementation(() => {});
            SpecHelper.click(elem, '.sub-title .invite-teammate');
            expect(scope.openHiringTeamInviteModal).toHaveBeenCalled();
            expect(scope.onCloseCallback).toHaveBeenCalledWith({
                openHiringTeamInviteModal: true,
            });
        });

        describe('when not submitted', () => {
            beforeEach(() => {
                expect(scope.submitted).toBeFalsy();
            });

            it('should have close button that triggers the onCloseCallback', () => {
                jest.spyOn(scope, 'onCloseCallback').mockImplementation(() => {});
                SpecHelper.click(elem, '.custom-close');
                expect(scope.onCloseCallback).toHaveBeenCalled();
            });

            it("should have a required select dropdown containing the user's hiring teammate emails in alphabetical order", () => {
                SpecHelper.expectElementRequired(elem, 'select[name="email"]');
                // each email in the hiring_teammate_emails array is a valid option (in alphabetical order),
                // plus the additional option that acts as a placeholder for the select element
                SpecHelper.expectElements(
                    elem,
                    'select[name="email"] option',
                    currentUser.hiring_teammate_emails.length + 1,
                );
                SpecHelper.expectElementText(elem, 'select[name="email"] option:eq(0)', 'Work Email Address');
                SpecHelper.expectElementText(elem, 'select[name="email"] option:eq(1)', 'bar@bar.com');
                SpecHelper.expectElementText(elem, 'select[name="email"] option:eq(2)', 'foo@foo.com');
                SpecHelper.expectElementText(elem, 'select[name="email"] option:eq(3)', 'qux@qux.com');
            });

            it('should have a textarea for an optional message with placeholder instructions', () => {
                SpecHelper.expectElementNotRequired(elem, 'textarea.message');
                SpecHelper.expectElementPlaceholder(
                    elem,
                    'textarea.message',
                    'Optional message to include with profile',
                );
            });

            it('should have a button to share the candidate', () => {
                SpecHelper.expectElementText(elem, 'button[name="share-profile"]', 'Share Profile');
            });

            it('should disable the share-profile button if form is $invalid and enable it if $valid', () => {
                expect(scope.shareCandidateForm.$invalid).toBe(true);
                SpecHelper.expectElementDisabled(elem, 'button[name="share-profile"]');
                SpecHelper.updateSelect(elem, 'select[name="email"]', 'bar@bar.com');
                expect(scope.shareCandidateForm.$valid).toBe(true);
                SpecHelper.expectElementEnabled(elem, 'button[name="share-profile"]');
            });
        });

        describe('when submitted', () => {
            it('should show shared-hiring-relationship-check SVG image with a message saying the profile was sent', () => {
                SpecHelper.expectNoElement(elem, 'img.relationship-check');
                SpecHelper.expectNoElement(elem, '.sent-message');
                scope.submitted = true;
                scope.$digest();
                SpecHelper.expectElement(elem, 'img.relationship-check');
                SpecHelper.expectElementText(elem, '.sent-message', 'Profile Sent');
            });
        });
    });
});
