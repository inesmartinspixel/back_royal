import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Careers/angularModule/spec/_mock/fixtures/open_position_fixtures';
import setSpecLocales from 'Translation/setSpecLocales';
import careersWelcomeLocales from 'Careers/locales/careers/careers_welcome-en.json';
import positionCardLocales from 'Careers/locales/careers/position_card-en.json';

setSpecLocales(careersWelcomeLocales, positionCardLocales);

describe('FrontRoyal.Careers.CareersWelcome', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let $q;
    let CareersNetworkViewModel;
    let careersNetworkViewModel;
    let OpenPosition;
    let CareerProfile;
    let User;
    let recommendedPositionsPromise;
    let currentUser;
    let DialogModal;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $q = $injector.get('$q');
                CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
                OpenPosition = $injector.get('OpenPosition');
                CareerProfile = $injector.get('CareerProfile');
                User = $injector.get('User');
                DialogModal = $injector.get('DialogModal');
            },
        ]);

        SpecHelper.stubConfig();
        $injector.get('OpenPositionFixtures');
        $injector.get('CareerProfileFixtures');

        currentUser = SpecHelper.stubCurrentUser();
        currentUser.has_seen_careers_welcome = true;
        currentUser.career_profile = CareerProfile.fixtures.getInstance();

        careersNetworkViewModel = CareersNetworkViewModel.get('candidate');
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<careers-welcome></careers-welcome>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    function setup(recommendedPositions) {
        recommendedPositions = recommendedPositions || [
            OpenPosition.fixtures.getInstance({
                id: 'recommended-1',
                title: 'Recommended 1',
            }),
            OpenPosition.fixtures.getInstance({
                id: 'recommended-2',
                title: 'Recommended 2',
            }),
        ];
        recommendedPositionsPromise = $q.when(recommendedPositions);
        jest.spyOn(CareersNetworkViewModel, 'get').mockReturnValue(careersNetworkViewModel);
        jest.spyOn(careersNetworkViewModel, 'loadRecommendedPositions').mockReturnValue(recommendedPositionsPromise);
    }

    it('should load top two recommended positions', () => {
        setup();
        render();
        expect(_.pluck(scope.positions, 'id')).toEqual(['recommended-1', 'recommended-2']);
    });

    it('should fallback to loading top two positions ordered by RECOMMENDED_FOR_CANDIDATE', () => {
        setup([]);
        const otherPositions = [
            OpenPosition.fixtures.getInstance({
                id: 'other-1',
                title: 'Other 1',
            }),
            OpenPosition.fixtures.getInstance({
                id: 'other-2',
                title: 'Other 2',
            }),
        ];
        OpenPosition.expect('index')
            .toBeCalledWith({
                filters: {
                    featured: true,
                    archived: false,
                    hiring_manager_might_be_interested_in: currentUser.id,
                    candidate_has_acted_on: false,
                },
                limit: 2,
                max_total_count: 2,
                candidate_id: currentUser.id,
                sort: 'RECOMMENDED_FOR_CANDIDATE',
            })
            .returns(otherPositions);
        render();
        OpenPosition.flush('index');
        expect(_.pluck(scope.positions, 'id')).toEqual(['other-1', 'other-2']);
    });

    it('should show spinner while loading', () => {
        setup([]);
        OpenPosition.expect('index').returns([]);
        render();
        expect(scope.loadingRecommended).toBe(true);
        SpecHelper.expectElement(elem, 'front-royal-spinner');
        OpenPosition.flush('index');
        expect(scope.loadingRecommended).toBe(false);
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
    });

    describe('careers welcome modal', () => {
        it('should show dialog modal if user has not seen careers welcome', () => {
            setup();
            currentUser.has_seen_careers_welcome = false;
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            User.expect('update');
            render();
            expect(DialogModal.alert).toHaveBeenCalled();
        });

        it('should not show dialog modal if user has seen careers welcome', () => {
            setup();
            currentUser.has_seen_careers_welcome = true;
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            render();
            expect(DialogModal.alert).not.toHaveBeenCalled();
        });
    });

    describe('confirm UI', () => {
        it('should show if not hasConfirmedProfile', () => {
            setup();
            render();
            jest.spyOn(scope, 'hasConfirmedProfile', 'get').mockReturnValue(false);
            jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
            scope.$digest();
            SpecHelper.click(elem, '[name="confirm-profile"]');
            expect(scope.loadRoute).toHaveBeenCalledWith('/careers/card');
        });

        it('should not show if hasConfirmedProfile', () => {
            setup();
            render();
            jest.spyOn(scope, 'hasConfirmedProfile', 'get').mockReturnValue(true);
            scope.$digest();
            SpecHelper.expectNoElement(elem, '[name="confirm-ui"]');
        });
    });
});
