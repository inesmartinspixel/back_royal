import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/open_position_fixtures';

describe('OpenPosition', () => {
    let OpenPosition;
    let position;
    let $injector;
    let DialogModal;
    let user;
    let SpecHelper;
    let $location;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;

                SpecHelper = $injector.get('SpecHelper');
                OpenPosition = $injector.get('OpenPosition');
                DialogModal = $injector.get('DialogModal');
                $location = $injector.get('$location');

                $injector.get('OpenPositionFixtures');
                $injector.get('MockIguana');

                OpenPosition.setAdapter('Iguana.Mock.Adapter');

                position = OpenPosition.fixtures.getInstance();
                user = SpecHelper.stubCurrentUser();
            },
        ]);
    });

    describe('manage', () => {
        it('should work', () => {
            jest.spyOn(DialogModal, 'alert');
            position.manage();
            expect(DialogModal.alert).toHaveBeenCalled();
            const closeModal = DialogModal.alert.mock.calls[0][0].scope.closeModal;
            jest.spyOn(DialogModal, 'hideAlerts');
            closeModal();
            expect(DialogModal.hideAlerts).toHaveBeenCalled();
        });
    });

    describe('renew', () => {
        it('should open manage modal if there is a subscription', () => {
            jest.spyOn(user, 'subscriptionForPosition').mockReturnValue(true);
            jest.spyOn(position, 'manage');
            position.renew();
            expect(position.manage).toHaveBeenCalled();
        });

        it('should redirect to pay_and_publish if there is no subscription', () => {
            jest.spyOn(user, 'subscriptionForPosition').mockReturnValue(false);
            jest.spyOn($location, 'search').mockImplementation(() => {});
            position.renew();
            expect($location.search.mock.calls[0][0]).toEqual('positionId', position.id);
            expect($location.search.mock.calls[1][0]).toEqual('action', 'edit');
            expect($location.search.mock.calls[2][0]).toEqual('startPage', 'pay_and_publish');
        });
    });
});
