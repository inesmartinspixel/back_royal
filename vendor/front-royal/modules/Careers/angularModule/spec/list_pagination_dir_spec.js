import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import candidateListLocales from 'Careers/locales/careers/candidate_list-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(candidateListLocales);

describe('FrontRoyal.Careers.listPaginationDir', () => {
    let $injector;
    let CareerProfile;
    let ngModel;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let getItemsSpy;
    let scrollHelper;
    let $q;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');
                $q = $injector.get('$q');
                scrollHelper = $injector.get('scrollHelper');
                $injector.get('CareerProfileFixtures');
            },
        ]);

        SpecHelper.stubCurrentUser(); // need to stub currentUser for candidateListCard directives
    });

    function render(opts = {}) {
        ngModel = [];
        for (let i = 0; i < (opts.numItems || 5); i++) {
            ngModel.push(CareerProfile.fixtures.getInstance());
        }
        getItemsSpy = jest.fn();

        renderer = SpecHelper.renderer();
        renderer.scope.getItems = getItemsSpy;
        renderer.scope.ngModel = opts.ngModel || ngModel;
        renderer.scope.listLimit = opts.listLimit || 4;
        renderer.scope.offset = angular.isDefined(opts.offset) ? opts.offset : 0;
        renderer.scope.noMoreAvailable = opts.noMoreAvailable || false;
        renderer.scope.bottomPagination = opts.bottomPagination || false;
        renderer.scope.totalCount = opts.totalCount || (angular.isDefined(opts.numItems) ? opts.numItems : 5);
        renderer.scope.persistWhenZero = opts.persistWhenZero || false;
        renderer.render(
            '<list-pagination ng-model="ngModel" get-items="getItems(action, listLimit, offset)" list-limit="listLimit" offset="offset" no-more-available="noMoreAvailable" bottom-pagination=bottomPagination total-count="totalCount" persist-when-zero="persistWhenZero"></list-pagination>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('numRemainingItems', () => {
        it('should be the difference in the number of ngModel and the offset', () => {
            render({
                numItems: 6,
                offset: 2,
            });
            expect(scope.numRemainingItems).toEqual(4);
        });
    });

    describe('numVisibleItems', () => {
        it('should return listLimit if there are more items relative to the offset that can fit on the page', () => {
            const listLimit = 3;
            render({
                numItems: 6,
                listLimit,
                offset: 2,
            });
            expect(scope.numVisibleItems).toEqual(listLimit);
        });
    });

    describe('onLastPage', () => {
        describe('when noMoreAvailable is false', () => {
            it('should be false', () => {
                render({
                    noMoreAvailable: false,
                });
                expect(scope.onLastPage).toBe(false);
            });
        });

        describe('when noMoreAvailable', () => {
            it('should be false if numRemainingItems is greater than listLimit', () => {
                const listLimit = 4;
                render({
                    noMoreAvailable: true,
                    listLimit,
                });
                jest.spyOn(scope, 'numRemainingItems', 'get').mockReturnValue(listLimit + 1);
                expect(scope.onLastPage).toBe(false);
            });

            it('should be true if numRemainingItems is equal to listLimit', () => {
                const listLimit = 4;
                render({
                    noMoreAvailable: true,
                    listLimit,
                });
                jest.spyOn(scope, 'numRemainingItems', 'get').mockReturnValue(listLimit);
                expect(scope.onLastPage).toBe(true);
            });

            it('should be true if numRemainingItems is less than listLimit', () => {
                const listLimit = 4;
                render({
                    noMoreAvailable: true,
                    listLimit,
                });
                jest.spyOn(scope, 'numRemainingItems', 'get').mockReturnValue(listLimit - 1);
                expect(scope.onLastPage).toBe(true);
            });
        });
    });

    describe('pagination counters', () => {
        it('should be hidden if ngModel.length is not greater than 0', () => {
            render({
                ngModel: [],
            });
            SpecHelper.expectNoElement(elem, '.current');
        });

        it('should say "0 - 0 of 0" if persistWhenZero', () => {
            render({
                ngModel: [],
                numItems: 0,
                persistWhenZero: true,
            });
            SpecHelper.expectElementText(elem, '.current', '0 - 0 of 0');
        });

        it('should set starting number to 1 greater than offset', () => {
            render({
                numItems: 5,
                listLimit: 5,
                offset: 0,
            });
            // starting number should be calculated as 1 (offset + 1)
            SpecHelper.expectElementText(elem, '.current', '1 - 5 of 5');
        });

        it('should set ending number based on offset and listLimit if there are more items than can be visible', () => {
            render({
                numItems: 10,
                listLimit: 5,
                offset: 3,
            });
            // ngModel.length is greater than the listLimit, so we use the listLimit to calculate the ending number
            SpecHelper.expectElementText(elem, '.current', '4 - 8 of 10');
        });

        it('should set ending number based on offset and ngModel.length if all items are visible', () => {
            render({
                numItems: 10,
                listLimit: 15,
                offset: 5,
            });
            // ngModel.length is less than the listLimit, so we use ngModel.length to calculate the ending number
            SpecHelper.expectElementText(elem, '.current', '6 - 10 of 10');
        });

        it('should not show an ending number before the starting number', () => {
            render({
                numItems: 20,
                totalCount: '100+',
                listLimit: 15,
                offset: 35,
            });
            SpecHelper.expectElementText(elem, '.current', '36 - 36 of 100+');
        });
    });

    describe('pagination controls', () => {
        it('should disable first and previous controls if on first page', () => {
            render();
            SpecHelper.expectElementDisabled(elem, '[name="first-page"]:eq(0)');
            SpecHelper.expectElementDisabled(elem, '[name="previous-page"]:eq(0)');
        });

        it('should disable next control if ngModel.length is less than listLimit', () => {
            const listLimit = ngModel.length + 1; // length needs to be greater than the length of ngModel
            render({
                ngModel,
                listLimit,
            });
            expect(scope.shouldDisableNext).toEqual(true);
            SpecHelper.expectElementDisabled(elem, '[name="next-page"]:eq(0)');
        });

        it('should disable next control if onLastPage', () => {
            const listLimit = 5;
            render({
                listLimit,
                noMoreAvailable: true,
            });
            jest.spyOn(scope, 'numRemainingItems', 'get').mockReturnValue(listLimit);
            expect(scope.onLastPage).toEqual(true);
            expect(scope.shouldDisableNext).toEqual(true);
            SpecHelper.expectElementDisabled(elem, '[name="next-page"]:eq(0)');
        });

        it('should go to first page of results', () => {
            const listLimit = 5;
            const offset = 10; // needs to be something other than 0 so button is enabled
            render({
                listLimit,
                offset,
            });
            SpecHelper.click(elem, '[name="first-page"]:eq(0)');
            expect(getItemsSpy).toHaveBeenCalledWith('first', listLimit, offset);
        });

        it('should go to previous page of results', () => {
            const listLimit = 5;
            const offset = 10; // needs to be something other than 0 so button is enabled
            render({
                listLimit,
                offset,
            });
            SpecHelper.click(elem, '[name="previous-page"]:eq(0)');
            expect(getItemsSpy).toHaveBeenCalledWith('previous', listLimit, offset);
        });

        it('should go to next page of results', () => {
            const listLimit = 5;
            render({
                listLimit,
            });
            SpecHelper.click(elem, '[name="next-page"]:eq(0)');
            expect(getItemsSpy).toHaveBeenCalledWith('next', listLimit, 0);
        });

        describe('at bottom', () => {
            it('should get items and scroll to top', () => {
                const numItems = 6;
                const listLimit = numItems - 1;
                render({
                    numItems,
                    listLimit,
                    bottomPagination: true,
                });
                jest.spyOn(scrollHelper, 'scrollToTop').mockReturnValue(true); // doesn't matter what it returns
                SpecHelper.click(elem, '[name="next-page"]:eq(0)');
                expect(scrollHelper.scrollToTop).toHaveBeenCalledWith(true, undefined);
                expect(getItemsSpy).toHaveBeenCalledWith('next', listLimit, 0);
            });
        });
    });

    describe('getItemsAndMaybeScrollToTop', () => {
        it('should disable "Next" pagination control if loading more career items', () => {
            render();

            const deferred = $q.defer();
            jest.spyOn(scope, 'getItems').mockReturnValue(deferred.promise);

            expect(scope.disableNext).toBeUndefined();
            expect(scope.shouldDisableNext).toBe(false);
            const args = {
                action: 'first',
                listLimit: 10,
            };

            scope.getItemsAndMaybeScrollToTop(args);
            expect(scope.disableNext).toBe(true);
            expect(scope.shouldDisableNext).toBe(true);
            expect(scope.getItems).toHaveBeenCalledWith(args);

            scope.$digest();
            SpecHelper.expectElementDisabled(elem, '[name="next-page"]:eq(0)');

            deferred.resolve();
            scope.$digest();

            expect(scope.disableNext).toBe(false);
            expect(scope.shouldDisableNext).toBe(false);
            SpecHelper.expectElementEnabled(elem, '[name="next-page"]:eq(0)');
        });
    });

    describe('disable', () => {
        it('should disable the buttons', () => {
            render();
            SpecHelper.expectElementEnabled(elem, '[name="next-page"]');
            scope.disable = true;
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, '[name="next-page"]');
        });
    });
});
