import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_application_fixtures';

describe('FrontRoyal.Careers.ProfileCompletionHelper', () => {
    let $injector;
    let ClientStorage;
    let profileCompletionHelper;
    let ProfileCompletionHelper;
    let formStepsAndRequiredFields;
    let HiringApplication;
    let hiringApplicationModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                ClientStorage = $injector.get('ClientStorage');
                ProfileCompletionHelper = $injector.get('ProfileCompletionHelper');
                HiringApplication = $injector.get('HiringApplication');
                $injector.get('HiringApplicationFixtures');
            },
        ]);

        formStepsAndRequiredFields = [
            {
                stepName: 'qux',
                requiredFields: {
                    quux: true,
                },
                requiresVisit: true,
            },
            {
                // name needs to be submit_application because it is expected in profileCompletionHelper
                stepName: 'submit_application',
                requiredFields: {
                    bar: true,
                },
            },
            {
                // step without required fields
                stepName: 'baz',
            },
        ];

        profileCompletionHelper = new ProfileCompletionHelper(formStepsAndRequiredFields);

        hiringApplicationModel = HiringApplication.fixtures.getInstance();
    });

    describe('getStepsProgressMap', () => {
        it('should getStepProgress for each step', () => {
            jest.spyOn(profileCompletionHelper, 'getStepProgress').mockImplementation(() => {});
            profileCompletionHelper.getStepsProgressMap(hiringApplicationModel);
            expect(profileCompletionHelper.getStepProgress.mock.calls.length).toEqual(
                formStepsAndRequiredFields.length,
            );
        });

        it('should return object map of steps and their progress', () => {
            jest.spyOn(profileCompletionHelper, 'getStepProgress')
                .mockReturnValueOnce('incomplete')
                .mockReturnValueOnce('complete')
                .mockReturnValueOnce('none');
            const stepsProgressMap = profileCompletionHelper.getStepsProgressMap(hiringApplicationModel);

            expect(stepsProgressMap).toEqual({
                qux: 'incomplete',
                submit_application: 'complete',
                baz: 'none',
            });
        });
    });

    describe('getStepProgress', () => {
        describe('when stepName is submit_application', () => {
            it('should return complete', () => {
                jest.spyOn(ClientStorage, 'getItem').mockReturnValue('true');
                expect(
                    profileCompletionHelper.getStepProgress(formStepsAndRequiredFields[1], hiringApplicationModel),
                ).toBe('complete');
                expect(ClientStorage.getItem).toHaveBeenCalledWith('reapplyingOrEditingApplication');
            });

            it('should return incomplete', () => {
                jest.spyOn(ClientStorage, 'getItem').mockReturnValue('false');
                expect(
                    profileCompletionHelper.getStepProgress(formStepsAndRequiredFields[1], hiringApplicationModel),
                ).toBe('incomplete');
                expect(ClientStorage.getItem).toHaveBeenCalledWith('reapplyingOrEditingApplication');
            });
        });

        describe('when stepName is NOT submit_application', () => {
            it('should return incomplete', () => {
                jest.spyOn(profileCompletionHelper, '_getComponentPercentComplete').mockReturnValue(0); // needs to return something other than 100, denoting incompletion
                expect(
                    profileCompletionHelper.getStepProgress(formStepsAndRequiredFields[0], hiringApplicationModel),
                ).toBe('incomplete');
            });

            it('should return none', () => {
                jest.spyOn(ClientStorage, 'getItem').mockReturnValue(false);
                expect(
                    profileCompletionHelper.getStepProgress(formStepsAndRequiredFields[2], hiringApplicationModel),
                ).toBe('none');
            });

            it('should return complete', () => {
                jest.spyOn(ClientStorage, 'getItem').mockImplementation(key => {
                    if (key === 'saved_qux') {
                        return true;
                    }
                });
                jest.spyOn(profileCompletionHelper, '_getComponentPercentComplete').mockReturnValue(100); // needs to return 100 to denote completion
                expect(
                    profileCompletionHelper.getStepProgress(formStepsAndRequiredFields[0], hiringApplicationModel),
                ).toBe('complete');
                expect(ClientStorage.getItem).toHaveBeenCalledWith(`saved_${formStepsAndRequiredFields[0].stepName}`);
            });

            it('should return complete when no requiredFields and ClientStorage has saved step key', () => {
                jest.spyOn(ClientStorage, 'getItem').mockReturnValue('true');
                expect(
                    profileCompletionHelper.getStepProgress(formStepsAndRequiredFields[2], hiringApplicationModel),
                ).toBe('complete');
                expect(ClientStorage.getItem).toHaveBeenCalledWith(`saved_${formStepsAndRequiredFields[2].stepName}`);
            });
        });
    });
});
