import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_application_fixtures';
import setSpecLocales from 'Translation/setSpecLocales';
import editHiringManagerProfileLocales from 'Careers/locales/careers/edit_hiring_manager_profile-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import candidatesFormLocales from 'Careers/locales/careers/edit_hiring_manager_profile/candidates_form-en.json';
import previewFormLocales from 'Careers/locales/careers/edit_hiring_manager_profile/preview_form-en.json';
import companyFormLocales from 'Careers/locales/careers/edit_hiring_manager_profile/company_form-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import teamFormLocales from 'Careers/locales/careers/edit_hiring_manager_profile/team_form-en.json';
import writeTextAboutLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/write_text_about-en.json';

setSpecLocales(
    editHiringManagerProfileLocales,
    editCareerProfileLocales,
    candidatesFormLocales,
    previewFormLocales,
    companyFormLocales,
    fieldOptionsLocales,
    teamFormLocales,
    writeTextAboutLocales,
);

describe('FrontRoyal.Careers.EditHiringManagerProfile', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let hiringApplication;
    let HiringApplication;
    let user;
    let ClientStorage;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                HiringApplication = $injector.get('HiringApplication');
                ClientStorage = $injector.get('ClientStorage');
                $injector.get('HiringApplicationFixtures');
                hiringApplication = HiringApplication.fixtures.getInstance();
                user = SpecHelper.stubCurrentUser();
                user.hiring_application = hiringApplication;
            },
        ]);

        SpecHelper.stubDirective('locationAutocomplete');
    });

    describe('hiringApplication', () => {
        it("should be a new Iguana model from the currentUser's hiring_application JSON so it's NOT a reference to currentUser.hiring_application", () => {
            hiringApplication = HiringApplication.fixtures.getInstance();
            const hiringApplicationJson = user.hiring_application.asJson();
            jest.spyOn(user.hiring_application, 'asJson').mockReturnValue(hiringApplicationJson);
            jest.spyOn(HiringApplication, 'new').mockReturnValue(hiringApplication);

            render();

            expect(HiringApplication.new).toHaveBeenCalledWith(hiringApplicationJson);
            expect(scope.hiringApplication).toBe(hiringApplication);
            expect(scope.hiringApplication).not.toBe(scope.currentUser.hiring_application);
        });
    });

    describe('$on gotoFormStep', () => {
        beforeEach(() => {
            render();
        });

        describe('when currentForm.$dirty', () => {
            beforeEach(() => {
                scope.currentForm.$setDirty();
            });

            it('should reset hiringApplication to a copy (not a reference) to currentUser.hiring_application', () => {
                hiringApplication = HiringApplication.fixtures.getInstance();
                const hiringApplicationJson = user.hiring_application.asJson();
                jest.spyOn(scope.currentUser.hiring_application, 'asJson').mockReturnValue(hiringApplicationJson);
                jest.spyOn(HiringApplication, 'new').mockReturnValue(hiringApplication);

                scope.$broadcast('gotoFormStep', 0);

                // references should be different since the hiringApplication on the scope
                // is now a copy of the hiring_application on the currentUser
                expect(HiringApplication.new).toHaveBeenCalledWith(hiringApplicationJson);
                expect(scope.hiringApplication).toBe(hiringApplication);
                expect(scope.hiringApplication).not.toBe(scope.currentUser.hiring_application);
            });
        });
    });

    describe('save', () => {
        it('should save the hiring application', () => {
            render();
            mockFormValidity();
            HiringApplication.expect('update');
            HiringApplication.expect('save').toBeCalledWith(scope.hiringApplication);
            SpecHelper.click(elem, '[name="save"]');
            HiringApplication.flush('update');
        });

        it('should set currentUser.hiring_application to a copy (not a reference) of scope.hiringApplication after save', () => {
            render();
            mockFormValidity();

            hiringApplication = HiringApplication.fixtures.getInstance();
            const hiringApplicationJson = scope.hiringApplication.asJson();
            jest.spyOn(scope.hiringApplication, 'asJson').mockReturnValue(hiringApplicationJson);
            jest.spyOn(HiringApplication, 'new').mockReturnValue(hiringApplication);

            HiringApplication.expect('update');
            HiringApplication.expect('save').toBeCalledWith(scope.hiringApplication);
            SpecHelper.click(elem, '[name="save"]');
            HiringApplication.flush('update');

            // references should be different since the hiring_application on the currentUser
            // is a copy of the hiringApplication on the scope
            expect(HiringApplication.new).toHaveBeenCalledWith(hiringApplicationJson);
            expect(scope.currentUser.hiring_application).toBe(hiringApplication);
            expect(scope.currentUser.hiring_application).not.toBe(scope.hiringApplication);
        });

        it('should set ClientStorage item for saved step', () => {
            render(['candidates']);
            mockFormValidity();
            jest.spyOn(ClientStorage, 'setItem').mockImplementation(() => {});

            HiringApplication.expect('update');
            scope.save();

            expect(ClientStorage.setItem).toHaveBeenCalledWith('saved_candidates', true);
        });
    });

    describe('saveAndNext', () => {
        it('should save and move on to next step if there is one', () => {
            render();
            jest.spyOn(scope, 'saveAndNext');
            mockFormValidity();
            HiringApplication.expect('update');
            HiringApplication.expect('save').toBeCalledWith(scope.hiringApplication).returns(scope.hiringApplication);
            SpecHelper.click(elem, '[name="save-and-next"]');
            expect(scope.saveAndNext).toHaveBeenCalled();
            HiringApplication.flush('update');
            SpecHelper.expectElement(elem, `form[name="${scope.stepsInfo[1].key}"]`);
        });

        it('should save and move on to preview from last step', () => {
            render(['candidates', 'preview']);
            mockFormValidity();
            HiringApplication.expect('save');
            SpecHelper.click(elem, 'button[name="save-and-next"]');
            HiringApplication.flush('save');
            SpecHelper.expectElement(elem, 'form[name="preview"]');
        });
    });

    it('should hide both save buttons on the preview form', () => {
        render(['preview']);
        SpecHelper.expectNoElement(elem, '[name="save"]');
        SpecHelper.expectNoElement(elem, '[name="save-and-next"]');
    });

    function mockFormValidity() {
        scope.currentForm.$valid = true; // mock validity of formController
        scope.$digest();
    }

    function render(stepNames) {
        renderer = SpecHelper.renderer();
        if (stepNames) {
            renderer.scope.stepNames = stepNames;
            renderer.render('<edit-hiring-manager-profile step-names="stepNames"></edit-hiring-manager-profile>');
        } else {
            renderer.render('<edit-hiring-manager-profile></edit-hiring-manager-profile>');
        }
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }
});
