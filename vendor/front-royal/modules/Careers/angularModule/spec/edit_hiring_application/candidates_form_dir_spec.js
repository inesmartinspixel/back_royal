import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_application_fixtures';
import setSpecLocales from 'Translation/setSpecLocales';
import candidatesFormLocales from 'Careers/locales/careers/edit_hiring_manager_profile/candidates_form-en.json';
import editHiringManagerProfileLocales from 'Careers/locales/careers/edit_hiring_manager_profile-en.json';

setSpecLocales(candidatesFormLocales, editHiringManagerProfileLocales);

describe('FrontRoyal.Careers.EditHiringApplication.CandidatesForm', () => {
    let $injector;
    let SpecHelper;
    let hiringApplication;
    let currentUser;
    let renderer;
    let scope;
    let elem;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('HiringApplicationFixtures');
                const HiringApplication = $injector.get('HiringApplication');

                hiringApplication = HiringApplication.fixtures.getInstance();
                currentUser = SpecHelper.stubCurrentUser();
                currentUser.hiring_application = hiringApplication;

                SpecHelper.stubDirective('locationAutocomplete');
            },
        ]);
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.stepNames = ['candidates'];
        renderer.render('<edit-hiring-manager-profile step-names="stepNames"></edit-hiring-manager-profile');
        elem = renderer.elem;
        scope = elem.find('candidates-form').scope();

        // edit-hiring-manager-profile will duplicate the hiringApplication, so
        // grab the copy
        hiringApplication = scope.hiringApplication;
        scope.$apply();
    }

    it('should support setting where_descriptors', () => {
        render();
        expect(hiringApplication.where_descriptors).toEqual([]);
        const optionKey = scope.whereOptions[1];
        SpecHelper.toggleCheckbox(elem, '[name="where-group"] input', 1);
        expect(hiringApplication.where_descriptors).toEqual([optionKey]);
        SpecHelper.toggleCheckbox(elem, '[name="where-group"] input', 1);
        expect(hiringApplication.where_descriptors).toEqual([]);
    });

    it('should support setting position_descriptors', () => {
        render();
        expect(hiringApplication.position_descriptors).toEqual([]);
        const optionKey = scope.positionOptions[1];
        SpecHelper.toggleCheckbox(elem, '[name="position-group"] input', 1);
        expect(hiringApplication.position_descriptors).toEqual([optionKey]);
        SpecHelper.toggleCheckbox(elem, '[name="position-group"] input', 1);
        expect(hiringApplication.position_descriptors).toEqual([]);
    });

    it("should default salary_ranges to 'any' if salary_ranges is empty", () => {
        expect(hiringApplication.salary_ranges).toEqual([]);
        render();
        expect(hiringApplication.salary_ranges).toEqual(['any']);
    });

    it('should support setting salary_ranges', () => {
        const any = 'any';
        expect(hiringApplication.salary_ranges).toEqual([]);
        render();
        expect(hiringApplication.salary_ranges).toEqual([any]); // it should default to 'any'
        SpecHelper.expectElement(elem, '[name="salary-ranges-group"] input[checked="checked"]');

        // test toggling 'any' checkbox
        const anyIndex = _.indexOf(scope.salaryRangeOptions, any);
        SpecHelper.toggleCheckbox(elem, '[name="salary-ranges-group"] input', anyIndex); // uncheck 'any'
        expect(hiringApplication.salary_ranges).toEqual([]);
        SpecHelper.expectNoElement(elem, '[name="salary-ranges-group"] input[checked="checked"]');
        SpecHelper.toggleCheckbox(elem, '[name="salary-ranges-group"] input', anyIndex); // check 'any'
        expect(hiringApplication.salary_ranges).toEqual([any]);
        SpecHelper.expectElement(elem, '[name="salary-ranges-group"] input[checked="checked"]');
    });

    it("should uncheck 'any' checkbox if another checkbox is checked and uncheck every other checkbox if 'any' checkbox is checked", () => {
        const any = 'any';
        expect(hiringApplication.salary_ranges).toEqual([]);
        render();
        expect(hiringApplication.salary_ranges).toEqual([any]); // it should default to 'any'
        SpecHelper.expectElement(elem, '[name="salary-ranges-group"] input[checked="checked"]');

        // test checking a checkbox other than 'any', when the 'any' checkbox is checked, unchecks the 'any' checkbox
        const otherOptionIndex = 1;
        const otherOption = scope.salaryRangeOptions[otherOptionIndex];
        SpecHelper.toggleCheckbox(elem, '[name="salary-ranges-group"] input', otherOptionIndex);
        expect(hiringApplication.salary_ranges).not.toEqual([any, otherOption]);
        expect(_.indexOf(hiringApplication.salary_ranges, any)).toEqual(-1); // 'any' should not be in salary_ranges
        expect(hiringApplication.salary_ranges).toEqual([otherOption]);
        SpecHelper.expectElement(elem, '[name="salary-ranges-group"] input[checked="checked"]');

        // test checking another checkbox just for good measure
        const anotherOptionIndex = 2;
        const anotherOption = scope.salaryRangeOptions[anotherOptionIndex];
        SpecHelper.toggleCheckbox(elem, '[name="salary-ranges-group"] input', anotherOptionIndex);
        expect(_.indexOf(hiringApplication.salary_ranges, any)).toEqual(-1); // 'any' should not be in salary_ranges
        expect(hiringApplication.salary_ranges).toEqual([otherOption, anotherOption]);
        SpecHelper.expectElements(elem, '[name="salary-ranges-group"] input[checked="checked"]');

        // test that checking the 'any' checkbox unchecks every other checkbox
        const anyIndex = _.indexOf(scope.salaryRangeOptions, any);
        SpecHelper.toggleCheckbox(elem, '[name="salary-ranges-group"] input', anyIndex); // check 'any'
        expect(hiringApplication.salary_ranges).toEqual([any]); // all other options should be removed from salary_ranges except 'any'
        SpecHelper.expectElement(elem, '[name="salary-ranges-group"] input[checked="checked"]');
    });
});
