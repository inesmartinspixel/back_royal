import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_application_fixtures';
import setSpecLocales from 'Translation/setSpecLocales';
import previewFormLocales from 'Careers/locales/careers/edit_hiring_manager_profile/preview_form-en.json';
import editHiringManagerProfileLocales from 'Careers/locales/careers/edit_hiring_manager_profile-en.json';
import hiringManagerCardLocales from 'Careers/locales/careers/hiring_manager_card-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';

setSpecLocales(previewFormLocales, editHiringManagerProfileLocales, hiringManagerCardLocales, fieldOptionsLocales);

describe('FrontRoyal.Careers.EditHiringApplication.PreviewForm', () => {
    let $injector;
    let SpecHelper;
    let hiringApplication;
    let currentUser;
    let renderer;
    let scope;
    let elem;
    let HiringApplication;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('HiringApplicationFixtures');
                HiringApplication = $injector.get('HiringApplication');

                hiringApplication = HiringApplication.fixtures.getInstance();
                currentUser = SpecHelper.stubCurrentUser();
                currentUser.hiring_application = hiringApplication;

                SpecHelper.stubDirective('locationAutocomplete');
            },
        ]);
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.stepNames = ['preview'];
        renderer.render('<edit-hiring-manager-profile step-names="stepNames"></edit-hiring-manager-profile');
        elem = renderer.elem;
        scope = elem.find('preview-form').scope();

        // edit-hiring-manager-profile will duplicate the hiringApplication, so
        // grab the copy
        hiringApplication = scope.hiringApplication;
        scope.$apply();
    }

    it('should show a card and browse button if profile complete', () => {
        Object.defineProperty(HiringApplication.prototype, 'complete', {
            value: true,
        });
        render();
        const card = SpecHelper.expectElement(elem, 'hiring-manager-card');
        expect(card.isolateScope().hiringApplication.id).toBe(hiringApplication.id);
        SpecHelper.expectElementText(elem, '[name="message"]', 'Your profile is complete!');
        SpecHelper.expectElement(elem, '.browse');
    });

    it('should show a blank card if profile incomplete', () => {
        Object.defineProperty(HiringApplication.prototype, 'complete', {
            value: false,
        });
        render();
        SpecHelper.expectNoElement(elem, 'hiring-manager-card');
        SpecHelper.expectElement(elem, '[name="card-placeholder"]');
        SpecHelper.expectElementText(
            elem,
            '[name="message"]',
            'Please complete all required profile fields to unlock preview.',
        );
    });
});
