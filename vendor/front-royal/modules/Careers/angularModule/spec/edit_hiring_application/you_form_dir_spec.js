import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_application_fixtures';
import setSpecLocales from 'Translation/setSpecLocales';
import youFormLocales from 'Careers/locales/careers/edit_hiring_manager_profile/you_form-en.json';
import editHiringManagerProfileLocales from 'Careers/locales/careers/edit_hiring_manager_profile-en.json';
import companyFormLocales from 'Careers/locales/careers/edit_hiring_manager_profile/company_form-en.json';
import writeTextAboutLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/write_text_about-en.json';

setSpecLocales(youFormLocales, editHiringManagerProfileLocales, companyFormLocales, writeTextAboutLocales);

describe('FrontRoyal.Careers.EditHiringApplication.YouForm', () => {
    let currentUser;
    let elem;
    let hiringApplication;
    let $injector;
    let renderer;
    let scope;
    let SpecHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('HiringApplicationFixtures');
                const HiringApplication = $injector.get('HiringApplication');

                hiringApplication = HiringApplication.fixtures.getInstance();
                currentUser = SpecHelper.stubCurrentUser();
                currentUser.hiring_application = hiringApplication;

                SpecHelper.stubDirective('locationAutocomplete');
                SpecHelper.stubDirective('uploadAvatar');
                render();
            },
        ]);
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.stepNames = ['you'];
        renderer.render('<edit-hiring-manager-profile step-names="stepNames"></edit-hiring-manager-profile');
        elem = renderer.elem;
        scope = elem.find('you-form').scope();

        // edit-hiring-manager-profile will duplicate the hiringApplication, so
        // grab the copy
        hiringApplication = scope.hiringApplication;
        scope.$apply();
    }

    it('should support setting avatar', () => {
        SpecHelper.expectElement(elem, 'upload-avatar');
    });

    it('should support setting job_title', () => {
        SpecHelper.updateTextInput(elem, 'write-text-about[name="job_title"] > input', 'Hiring Manager');
        expect(scope.hiringApplication.job_title).toEqual('Hiring Manager');
    });

    it('should support setting job_role', () => {
        SpecHelper.updateSelect(elem, '[name="job_role"]', scope.roleOptions[0].value);
        expect(hiringApplication.job_role).toEqual('senior_management');
    });

    it('should support setting phone', () => {
        SpecHelper.updateSelect(elem, 'select[name="countryCode"]', 'US');
        SpecHelper.updateInput(elem, 'input[name="phone"]', '(201) 555-5555');
        expect(scope.hiringApplication.phone).toBe('+12015555555');
    });

    it('should support setting fun_fact', () => {
        SpecHelper.updateTextInput(elem, 'write-text-about[name="fun_fact"] > input', 'This is a fun fact...');
        expect(hiringApplication.fun_fact).toEqual('This is a fun fact...');
    });

    it('should display correct did_you_know text', () => {
        SpecHelper.expectElementText(elem, '.form-group:eq(5) label', 'Did you know?');
        SpecHelper.expectElementText(
            elem,
            '.form-group:eq(5) p:eq(0)',
            'Optional. This will appear in your profile to help break the ice with matched candidates.',
        );
    });
});
