import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_application_fixtures';
import setSpecLocales from 'Translation/setSpecLocales';
import teamFormLocales from 'Careers/locales/careers/edit_hiring_manager_profile/team_form-en.json';
import editHiringManagerProfileLocales from 'Careers/locales/careers/edit_hiring_manager_profile-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import writeTextAboutLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/write_text_about-en.json';

setSpecLocales(teamFormLocales, editHiringManagerProfileLocales, fieldOptionsLocales, writeTextAboutLocales);

describe('FrontRoyal.Careers.EditHiringApplication.TeamForm', () => {
    let $injector;
    let SpecHelper;
    let hiringApplication;
    let currentUser;
    let renderer;
    let scope;
    let elem;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('HiringApplicationFixtures');
                const HiringApplication = $injector.get('HiringApplication');

                hiringApplication = HiringApplication.fixtures.getInstance();
                currentUser = SpecHelper.stubCurrentUser();
                currentUser.hiring_application = hiringApplication;

                SpecHelper.stubDirective('locationAutocomplete');
                render();
            },
        ]);
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.stepNames = ['team'];
        renderer.render('<edit-hiring-manager-profile step-names="stepNames"></edit-hiring-manager-profile');
        elem = renderer.elem;
        scope = elem.find('team-form').scope();

        // edit-hiring-manager-profile will duplicate the hiringApplication, so
        // grab the copy
        hiringApplication = scope.hiringApplication;
        scope.$apply();
    }

    it('should be wired to hiringApplication', () => {
        render();
        expect(scope.hiringApplication).toBeDefined();
    });

    it('should support setting hiring_for', () => {
        SpecHelper.toggleRadio(elem, '[name="hiring_for"]', 0);
        expect(hiringApplication.hiring_for).toEqual(scope.hiringOptions[0]);
        SpecHelper.toggleRadio(elem, '[name="hiring_for"]', 1);
        expect(hiringApplication.hiring_for).toEqual(scope.hiringOptions[1]);
    });

    it('should support setting team_name', () => {
        SpecHelper.updateTextInput(elem, '[name="team_name"] input', 'Meetings Dept.');
        expect(hiringApplication.team_name).toEqual('Meetings Dept.');
    });

    it('should support setting team_mission', () => {
        SpecHelper.updateTextInput(elem, '[name="team_mission"] input', 'Conduct all the meetings!');
        expect(hiringApplication.team_mission).toEqual('Conduct all the meetings!');
    });

    it('should support setting role_descriptors', () => {
        hiringApplication.role_descriptors = [];
        scope.$apply();
        SpecHelper.updateMultiSelect(elem, '[name="role_descriptors"] multi-select', 'marketing');
        expect(hiringApplication.role_descriptors).toEqual(['marketing']);
        SpecHelper.updateMultiSelect(elem, '[name="role_descriptors"] multi-select', 'sales');
        expect(hiringApplication.role_descriptors).toEqual(['marketing', 'sales']);
    });
});
