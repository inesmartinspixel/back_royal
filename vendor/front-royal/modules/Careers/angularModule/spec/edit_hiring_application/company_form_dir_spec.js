import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_application_fixtures';
import setSpecLocales from 'Translation/setSpecLocales';
import companyFormLocales from 'Careers/locales/careers/edit_hiring_manager_profile/company_form-en.json';
import editHiringManagerProfileLocales from 'Careers/locales/careers/edit_hiring_manager_profile-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';

setSpecLocales(companyFormLocales, editHiringManagerProfileLocales, fieldOptionsLocales);

describe('FrontRoyal.Careers.EditHiringApplication.CompanyForm', () => {
    let currentUser;
    let elem;
    let hiringApplication;
    let $injector;
    let renderer;
    let scope;
    let SpecHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $injector.get('HiringApplicationFixtures');
                const HiringApplication = $injector.get('HiringApplication');

                hiringApplication = HiringApplication.fixtures.getInstance();
                currentUser = SpecHelper.stubCurrentUser();
                currentUser.hiring_application = hiringApplication;

                SpecHelper.stubDirective('locationAutocomplete');
                render();
            },
        ]);
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.stepNames = ['company'];
        renderer.render('<edit-hiring-manager-profile step-names="stepNames"></edit-hiring-manager-profile');
        elem = renderer.elem;
        scope = elem.find('company-form').scope();

        // edit-hiring-manager-profile will duplicate the hiringApplication, so
        // grab the copy
        hiringApplication = scope.hiringApplication;
        scope.$apply();
    }

    it('should support setting location', () => {
        const el = SpecHelper.expectElement(elem, 'location-autocomplete');
        expect(el.attr('ng-model')).toEqual('hiringApplication.place_id');
        expect(el.attr('details-model')).toEqual('hiringApplication.place_details');
    });

    it('should support setting company_year', () => {
        expect(scope.yearOptions.length).toEqual(new Date().getUTCFullYear() - 1799);
        SpecHelper.updateSelect(elem, '[name="company_year"]', scope.yearOptions[0].value);
        expect(hiringApplication.company_year).toEqual(new Date().getUTCFullYear());
    });

    it('should support setting company_employee_count', () => {
        SpecHelper.updateSelect(elem, '[name="company_employee_count"]', scope.sizeOptions[0].value);
        expect(hiringApplication.company_employee_count).toEqual('1-24');
    });

    it('should support setting company_annual_revenue', () => {
        SpecHelper.updateSelect(elem, '[name="company_annual_revenue"]', scope.revenueOptions[0].value);
        expect(hiringApplication.company_annual_revenue).toEqual('lesson_than_1_million');
    });

    it('should support setting industry', () => {
        SpecHelper.updateSelect(elem, '[name="industry"]', scope.industryOptions[0].value);
        expect(hiringApplication.industry).toEqual('consulting');
    });

    it('should support setting funding', () => {
        SpecHelper.updateSelect(elem, '[name="funding"]', scope.fundingOptions[0].value);
        expect(hiringApplication.funding).toEqual('bootstrapped');

        SpecHelper.updateSelect(elem, '[name="funding"]', scope.fundingOptions[5].value);
        expect(hiringApplication.funding).toEqual('private');
    });

    it('should support setting website_url', () => {
        SpecHelper.updateTextInput(elem, '[name="website_url"]', 'foobar.com');
        expect(hiringApplication.website_url).toEqual('foobar.com');
    });
});
