import 'AngularSpecHelper';
import 'Careers/angularModule';

describe('FrontRoyal.Careers.OpenPositionFilterSet', () => {
    let $injector;
    let SpecHelper;
    let OpenPositionFilterSet;
    let OpenPosition;
    let user;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                OpenPosition = $injector.get('OpenPosition');
                OpenPositionFilterSet = $injector.get('OpenPositionFilterSet');
            },
        ]);

        user = SpecHelper.stubCurrentUser();
    });

    it('should build filters correctly', () => {
        const openPositionFilterSet = new OpenPositionFilterSet({});
        expect(openPositionFilterSet._buildFiltersForApiRequest()).toEqual({
            featured: true,
            archived: false,
            hiring_manager_might_be_interested_in: user.id,
            candidate_has_acted_on: false,
        });
    });

    describe('getResultsRelativeToOffset', () => {
        let openPositionFilterSet;

        beforeEach(() => {
            openPositionFilterSet = new OpenPositionFilterSet({});
            jest.spyOn(openPositionFilterSet, 'ensureResultsPreloaded').mockImplementation(() => {});
        });

        it('should ensureResultsPreloaded', () => {
            openPositionFilterSet.getResultsRelativeToOffset(0, 'first', 10);
            expect(openPositionFilterSet.ensureResultsPreloaded).toHaveBeenCalled();
        });

        it('should throw error if action not provided', () => {
            expect(() => {
                openPositionFilterSet.getResultsRelativeToOffset();
            }).toThrowError('Unsupported pagination control action: undefined');
        });

        it('should throw error if action is unsupported', () => {
            expect(() => {
                openPositionFilterSet.getResultsRelativeToOffset(0, 'foo', 10);
            }).toThrowError('Unsupported pagination control action: foo');
        });

        describe("when action is 'first'", () => {
            it('should set offset to 0', () => {
                openPositionFilterSet.offset = 10;
                openPositionFilterSet.getResultsRelativeToOffset(openPositionFilterSet.offset, 'first', 10);

                expect(openPositionFilterSet.offset).toEqual(0);
            });
        });

        describe("when action is 'previous'", () => {
            it('should set offset to 0 if decrementing offset by listLimit results in negative number', () => {
                const listLimit = 15; // listLimit needs to be greater than offset in this case
                openPositionFilterSet.offset = 10;
                openPositionFilterSet.getResultsRelativeToOffset(openPositionFilterSet.offset, 'previous', listLimit);

                expect(openPositionFilterSet.offset).toEqual(0);
            });

            it('should decrement offset by listLimit', () => {
                const listLimit = 3;
                openPositionFilterSet.offset = 10;
                openPositionFilterSet.getResultsRelativeToOffset(openPositionFilterSet.offset, 'previous', listLimit);

                expect(openPositionFilterSet.offset).toEqual(7);
            });
        });

        describe("when action is 'next'", () => {
            it('should increment offset by listLimit', () => {
                const listLimit = 5;
                openPositionFilterSet.offset = 10;
                openPositionFilterSet.getResultsRelativeToOffset(openPositionFilterSet.offset, 'next', listLimit);

                expect(openPositionFilterSet.offset).toEqual(15);
            });
        });
    });

    describe('ensureResultsPreloaded', () => {
        let openPositionFilterSet;

        beforeEach(() => {
            openPositionFilterSet = new OpenPositionFilterSet({});
        });

        it('should set the total count', () => {
            OpenPosition.expect('index').returnsMeta({
                total_count: 1337,
            });
            openPositionFilterSet.ensureResultsPreloaded();
            OpenPosition.flush('index');
            expect(openPositionFilterSet.initialTotalCount).toBe(1337);

            // mock _allDataLoaded to give the appearance that not that all possible data has been loaded
            // for the purpose of this test
            openPositionFilterSet._allDataLoaded = false;

            // Emulating the meta return from a subsequent page load
            OpenPosition.expect('index').returnsMeta({
                total_count: 1327,
            });
            openPositionFilterSet.ensureResultsPreloaded();
            OpenPosition.flush('index');
            expect(openPositionFilterSet.initialTotalCount).toBe(1337);
        });

        it('should do nothing if stack has 5 profiles already', () => {
            openPositionFilterSet.results = [{}, {}, {}, {}, {}];
            jest.spyOn(OpenPosition, 'index').mockImplementation(() => {});
            openPositionFilterSet.ensureResultsPreloaded();
            expect(OpenPosition.index).not.toHaveBeenCalled();
        });

        it('should clone the filters so changes do not affect subsequent loads', () => {
            const filters = {
                foo: true,
                bar: false,
                baz: [1, 2],
            };
            openPositionFilterSet = new OpenPositionFilterSet(filters);
            expect(openPositionFilterSet.filters).toEqual(filters);
            expect(openPositionFilterSet.filters).not.toBe(filters);
            filters.baz.push(3);
            expect(openPositionFilterSet.filters.baz).toEqual([1, 2]);
        });

        it('should do nothing if all profiles have been loaded from the server', () => {
            // mock making a request that returns no results, indicating that
            // all available profiles have been loaded already
            OpenPosition.expect('index').returns([]);
            openPositionFilterSet.ensureResultsPreloaded();
            OpenPosition.flush('index');

            jest.spyOn(OpenPosition, 'index').mockImplementation(() => {});
            openPositionFilterSet.ensureResultsPreloaded();
            expect(OpenPosition.index).not.toHaveBeenCalled();
        });

        it('should load more data if there are fewer than 5 positions in the stack', () => {
            const openPositions = [
                {
                    id: 1,
                },
                {
                    id: 2,
                },
            ];
            OpenPosition.expect('index').returns(openPositions);
            openPositionFilterSet.ensureResultsPreloaded();
            OpenPosition.flush('index');
            expect(_.pluck(openPositionFilterSet.results, 'id')).toEqual([1, 2]);
        });

        it('should do nothing if already loading', () => {
            OpenPosition.expect('index').returns([
                {
                    id: 1,
                },
            ]);
            const promise = openPositionFilterSet.ensureResultsPreloaded();

            // calling again before first request returns should return the same promise
            expect(openPositionFilterSet.ensureResultsPreloaded()).toBe(promise);
            OpenPosition.flush('index');

            // calling again after the first request returns should return a new promise
            OpenPosition.expect('index');
            expect(openPositionFilterSet.ensureResultsPreloaded()).not.toBe(promise);
        });

        it('should set initialTotalCountIsMin when totalCount is equal to max_total_count', () => {
            assertSetsInitialTotalCountIsMin(100, true);
        });

        it('should set initialTotalCountIsMin when totalCount is less than max_total_count', () => {
            assertSetsInitialTotalCountIsMin(42, false);
        });

        function assertSetsInitialTotalCountIsMin(totalCount, initialTotalCountIsMin) {
            user = SpecHelper.stubCurrentUser();

            OpenPosition.expect('index')
                .toBeCalledWith({
                    filters: openPositionFilterSet._buildFiltersForApiRequest(),
                    limit: 10,
                    max_total_count: 100,
                    offset: 0,
                    candidate_id: user.id,
                    sort: 'RECOMMENDED_FOR_CANDIDATE',
                })
                .returnsMeta({
                    total_count: totalCount,
                });
            openPositionFilterSet.ensureResultsPreloaded();
            OpenPosition.flush('index');
            expect(openPositionFilterSet.initialTotalCount).toEqual(totalCount);
            expect(openPositionFilterSet.initialTotalCountIsMin).toEqual(initialTotalCountIsMin);
        }
    });

    describe('states', () => {
        it('should be set appropriately in different parts of the loading cycle', () => {
            const openPositions = [
                {
                    id: 1,
                },
                {
                    id: 2,
                },
                {
                    id: 3,
                },
                {
                    id: 4,
                },
            ];
            const openPositionFilterSet = new OpenPositionFilterSet(
                {},
                {
                    serverLimit: openPositions.length - 1,
                },
            );

            // before initial load
            expect(openPositionFilterSet.any).toBe(false);
            expect(openPositionFilterSet.searchingForMore).toBe(false);
            expect(openPositionFilterSet.searchingForMoreWhenEmpty).toBe(true);
            expect(openPositionFilterSet.noMoreAvailable).toBe(false);
            expect(openPositionFilterSet.hasAttemptedToFetchInitialResults).toBe(false);

            // during initial load
            OpenPosition.expect('index').returns(openPositions);
            openPositionFilterSet.ensureResultsPreloaded();
            expect(openPositionFilterSet.any).toBe(false);
            expect(openPositionFilterSet.searchingForMore).toBe(true); /* changed */
            expect(openPositionFilterSet.searchingForMoreWhenEmpty).toBe(true);
            expect(openPositionFilterSet.noMoreAvailable).toBe(false);
            expect(openPositionFilterSet.hasAttemptedToFetchInitialResults).toBe(true); /* changed */

            // during next preload that returns fewer profiles than the serverLimit (flushing
            // the first index leads to ensureResultsPreloaded being called again, triggering
            // the next index)
            OpenPosition.expect('index').returns([
                {
                    id: 5,
                },
            ]);
            OpenPosition.flush('index');
            expect(openPositionFilterSet.any).toBe(true);
            expect(openPositionFilterSet.searchingForMore).toBe(true); /* changed */
            expect(openPositionFilterSet.searchingForMoreWhenEmpty).toBe(false);
            expect(openPositionFilterSet.noMoreAvailable).toBe(false);
            expect(openPositionFilterSet.hasAttemptedToFetchInitialResults).toBe(true);

            OpenPosition.flush('index');
            expect(openPositionFilterSet.any).toBe(true);
            expect(openPositionFilterSet.searchingForMore).toBe(false); /* changed */
            expect(openPositionFilterSet.searchingForMoreWhenEmpty).toBe(false);
            expect(openPositionFilterSet.noMoreAvailable).toBe(true); /* changed */
            expect(openPositionFilterSet.hasAttemptedToFetchInitialResults).toBe(true);

            // preloading when there is still something in the stack
            openPositionFilterSet.ensureResultsPreloaded();
            expect(openPositionFilterSet.any).toBe(true);
            expect(openPositionFilterSet.searchingForMore).toBe(false);
            expect(openPositionFilterSet.searchingForMoreWhenEmpty).toBe(false);
            expect(openPositionFilterSet.noMoreAvailable).toBe(true);
            expect(openPositionFilterSet.hasAttemptedToFetchInitialResults).toBe(true);

            // after the stack has been cleaned out
            openPositionFilterSet.results = [];
            expect(openPositionFilterSet.any).toBe(false); /* changed */
            expect(openPositionFilterSet.searchingForMore).toBe(false);
            expect(openPositionFilterSet.searchingForMoreWhenEmpty).toBe(false);
            expect(openPositionFilterSet.noMoreAvailable).toBe(true);
            expect(openPositionFilterSet.hasAttemptedToFetchInitialResults).toBe(true);
        });
    });
});
