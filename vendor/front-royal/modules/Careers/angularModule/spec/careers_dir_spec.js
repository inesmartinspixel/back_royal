import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import setSpecLocales from 'Translation/setSpecLocales';
import careersLocales from 'Careers/locales/careers/careers-en.json';

setSpecLocales(careersLocales);

describe('FrontRoyal.Careers.Careers', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let $rootScope;
    let $location;
    let CareerProfile;
    let StudentDashboard;
    let Cohort;
    let user;
    let $timeout;
    let CohortApplication;
    let CareersNetworkViewModel;
    let careersNetworkViewModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $rootScope = $injector.get('$rootScope');
                $location = $injector.get('$location');
                $timeout = $injector.get('$timeout');
                CareerProfile = $injector.get('CareerProfile');
                StudentDashboard = $injector.get('StudentDashboard');
                Cohort = $injector.get('Cohort');
                CohortApplication = $injector.get('CohortApplication');
                CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');

                $injector.get('CareerProfileFixtures');
                $injector.get('CohortFixtures');
                $injector.get('CohortApplicationFixtures');
            },
        ]);

        SpecHelper.stubConfig();
        SpecHelper.stubDirective('locationAutocomplete');
        SpecHelper.stubDirective('previewCandidateCard');
        SpecHelper.stubDirective('uploadAvatar');
        SpecHelper.stubDirective('featuredPositions');
        SpecHelper.stubDirective('careersWelcome');

        careersNetworkViewModel = {};
        jest.spyOn(CareersNetworkViewModel, 'get').mockReturnValue(careersNetworkViewModel);

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
        user.cohort_applications.push(
            CohortApplication.fixtures.getInstance({
                status: 'accepted',
            }),
        );
        user.can_edit_career_profile = true;
        user.mba_enabled = true;
        user.relevant_cohort = Cohort.fixtures.getInstance();
    });

    function render(section) {
        StudentDashboard.expect('index').toBeCalledWith({
            get_has_available_incomplete_streams: true,
            get_has_available_incomplete_playlists: true,
            user_id: $rootScope.currentUser.id,
            filters: {},
        });

        renderer = SpecHelper.renderer();
        renderer.scope.section = section;

        if (section) {
            renderer.render(`<careers section="${section}"></careers>`);
        } else {
            renderer.render('<careers></careers>');
        }

        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
        $timeout.flush();
    }

    it('should show apply if user is not flagged to edit profile and has not yet applied', () => {
        user.can_edit_career_profile = false;
        user.cohort_applications = [];
        render('positions');
        SpecHelper.expectElement(elem, 'partial-screen-dialog-modal');
        // Check button is in DOM
        SpecHelper.expectElement(elem, 'partial-screen-dialog-modal button');
    });

    describe('main-box', () => {
        it('should display appropriate content based on section', () => {
            render('card');
            SpecHelper.expectElement(elem, 'preview-candidate-card');
            scope.section = 'positions';
            scope.$apply();
            SpecHelper.expectElement(elem, 'featured-positions');
        });
    });

    describe('card section', () => {
        it('should work with no access', () => {
            jest.spyOn($location, 'url').mockImplementation(() => {});
            $rootScope.currentUser.can_edit_career_profile = false;
            render('card');
            assertNavigatedToSection(null);
            SpecHelper.expectHasClass(elem, '[name="welcome"]', 'disabled');
            SpecHelper.expectHasClass(elem, '[name="connections"]', 'disabled');
            SpecHelper.expectDoesNotHaveClass(elem, '[name="preview-profile"]', 'disabled');
            SpecHelper.expectHasClass(elem, '[name="preview-profile"]', 'active');

            // check that clicking other tabs does not change section
            SpecHelper.click(elem, '[name="connections"]');
            assertNavigatedToSection(null);
        });
    });

    describe('navigation', () => {
        beforeEach(() => {
            jest.spyOn($location, 'url').mockImplementation(() => {});
        });

        it('should use the passed in section', () => {
            Object.defineProperty(user, 'hasActiveCareerProfile', {
                value: true,
            });
            render('card');
            assertNavigatedToSection(null);
        });
        it('should set the appropriate nav section as active on load', () => {
            jest.spyOn($location, 'path').mockReturnValue('/careers/card');
            render('card');
            SpecHelper.expectHasClass(elem, '[name="preview-profile"]', 'active');
        });

        it('should activate the appropriate nav section when that section is clicked', () => {
            render('positions');
            assertNavigatedToSection(null);
            SpecHelper.expectDoesNotHaveClass(elem, '[name="preview-profile"]', 'active');
            SpecHelper.click(elem, '[name="preview-profile"]');
            assertNavigatedToSection('card');
        });

        it('should prevent navigation to positions when career profile is not complete', () => {
            jest.spyOn(user, 'hasCareersNetworkAccess', 'get').mockReturnValue(true);
            let complete = false;
            jest.spyOn(user, 'hasCompleteCareerProfile', 'get').mockImplementation(() => complete);
            render();

            SpecHelper.expectHasClass(elem, '[name="positions"]', 'disabled');

            complete = true;
            scope.$digest();
            SpecHelper.expectDoesNotHaveClass(elem, '[name="positions"]', 'disabled');
        });

        it('should automatically move you out of a non-card section if you lose access', () => {
            $rootScope.currentUser.cohort_applications[0].status = 'accepted';
            $rootScope.currentUser.can_edit_career_profile = true;

            render('positions');
            assertNavigatedToSection(false);

            $rootScope.currentUser.cohort_applications[0].status = 'accepted';
            $rootScope.currentUser.can_edit_career_profile = false; // changed
            scope.$apply();
            $timeout.flush();

            // check that we change sections automatically
            assertNavigatedToSection('card');
        });

        it('should automatically move you to apply if you lost all access with no application', () => {
            user.can_edit_career_profile = false;
            user.cohort_applications[0].status = 'pending';
            render('card');
            assertNavigatedToSection(null);

            user.cohort_applications = [];
            scope.$apply();
            $timeout.flush();

            // check that we change sections automatically
            assertNavigatedToSection('apply');
            SpecHelper.expectElement(elem, 'partial-screen-dialog-modal');
        });

        it('should automatically move you to edit if on positions and career profile is not complete', () => {
            let complete = false;
            jest.spyOn(user, 'hasCompleteCareerProfile', 'get').mockImplementation(() => complete);
            render('positions');
            assertNavigatedToSection('card');

            complete = true;
            render('positions');
            assertNavigatedToSection(null);
        });

        it('should show num_recommended_positions badge on featured positions section', () => {
            user.num_recommended_positions = 0;
            render();
            SpecHelper.expectNoElement(elem, '[name="positions"] > .notification');
            user.num_recommended_positions = 13;
            scope.$digest();
            SpecHelper.expectElementText(elem, '[name="positions"] > .notification', '13');
        });

        it('should show connectionsNumNotifications badge on connections section', () => {
            careersNetworkViewModel.connectionsNumNotifications = 0;
            render();
            SpecHelper.expectNoElement(elem, '[name="connections"] > .notification');
            careersNetworkViewModel.connectionsNumNotifications = 14;
            scope.$digest();
            SpecHelper.expectElementText(elem, '[name="connections"] > .notification', '14');
        });
    });

    function assertNavigatedToSection(section) {
        if (section) {
            expect($location.url).toHaveBeenCalledWith(`/careers/${section}`);
        } else {
            // url() might have been called, but only with no args
            expect(_.chain($location.url.calls).pluck('length').max().value() <= 0).toBe(true);
        }
        $location.url.mockClear();
    }
});
