import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/open_position_fixtures';
import positionsLocales from 'Careers/locales/careers/positions-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import positionCardLocales from 'Careers/locales/careers/position_card-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(positionsLocales, fieldOptionsLocales, positionCardLocales);

describe('FrontRoyal.Careers.PositionsListSection', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let positions;
    let expandedSections;
    let OpenPosition;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                OpenPosition = $injector.get('OpenPosition');

                $injector.get('OpenPositionFixtures');

                SpecHelper.stubDirective('positionBar');
                SpecHelper.stubCurrentUser();
            },
        ]);

        const stubbedPositions = [
            {
                id: 1,
                title: 'title search',
                role: 'marketing',
                position_descriptors: ['permanent'],
                perks: ['remote'],
                featured: true,
                archived: false,
            },
            {
                id: 2,
                description: 'description search',
                role: 'marketing',
                position_descriptors: ['part_time'],
                perks: ['relocation'],
                skills: [
                    {
                        text: 'nunchucks',
                    },
                ],
                featured: true,
                archived: false,
            },
            {
                id: 3,
                title: 'Marketing Leader Extraordinaire',
                role: 'trading',
                position_descriptors: ['contract'],
                perks: ['equity'],
                featured: true,
                archived: false,
            },
            {
                id: 4,
                title: 'Marketing Leader Extraordinaire',
                role: 'marketing',
                position_descriptors: ['internship'],
                perks: ['bonus'],
                featured: true,
                archived: true,
            },
            {
                id: 5,
                title: 'Marketing Leader Extraordinaire',
                role: 'marketing',
                archived: true,
            },
            {
                id: 6,
                title: 'Marketing Leader Extraordinaire',
                role: 'marketing',
            },
        ];

        positions = _.map(stubbedPositions, (position, index) => {
            const newPosition = OpenPosition.new(position);
            jest.spyOn(newPosition, 'locationString', 'get').mockReturnValue(`${index} city, ST`);
            return newPosition;
        });

        expandedSections = {
            open: {
                expanded: true,
            },
        };
    });

    function render(params = {}) {
        renderer = SpecHelper.renderer();
        angular.extend(renderer.scope, {
            sectionName: params.sectionName || 'open',
            expandedSections: params.expandedSections || expandedSections,
            positions,
            stickyHeaders: params.stickyHeaders || true,
            searchText: params.searchText,
        });
        renderer.render(
            '<positions-list-section section-name="sectionName" expanded-sections="expandedSections" positions="positions" sticky-headers="stickyHeaders" search-text="searchText"></positions-list-section>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('section display', () => {
        it('should render normally', () => {
            render();
            SpecHelper.expectElements(elem, 'position-bar', positions.length);
        });

        it('should show new and total headers if open or archived', () => {
            render();

            scope.sectionName = 'open';
            scope.$digest();
            SpecHelper.expectElementText(elem, '.counts li:eq(0)', 'New');
            SpecHelper.expectElementText(elem, '.counts li:eq(1)', 'Total');

            scope.sectionName = 'archived';
            scope.$digest();
            SpecHelper.expectElementText(elem, '.counts li:eq(0)', 'New');
            SpecHelper.expectElementText(elem, '.counts li:eq(1)', 'Total');

            scope.sectionName = 'foo';
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.counts');
        });
    });

    describe('search', () => {
        it('should show everything with blank text filter', () => {
            render({
                searchText: '',
            });
            SpecHelper.expectElements(elem, 'position-bar', positions.length);
        });

        it('should filter by team member', () => {
            positions = [
                OpenPosition.fixtures.getInstance({
                    hiring_manager_id: 'me-id',
                }),
                OpenPosition.fixtures.getInstance({
                    hiring_manager_id: 'teammate-id',
                }),
            ];
            render();
            SpecHelper.expectElements(elem, 'position-bar', 2);

            scope.selectedTeamMemberId = 'all';
            scope.$digest();
            SpecHelper.expectElements(elem, 'position-bar', 2);
            SpecHelper.expectElementText(elem, '.caption', '2 positions');

            scope.selectedTeamMemberId = 'teammate-id';
            scope.$digest();
            SpecHelper.expectElements(elem, 'position-bar', 1);
            SpecHelper.expectElementText(elem, '.caption', '1 position');
        });

        const searches = [
            {
                field: 'title',
                text: 'title search',
            },
            {
                field: 'locationString',
                text: '2 city',
            },
            {
                field: 'description',
                text: 'description search',
            },
            {
                field: 'role',
                text: 'trading',
            },
            {
                field: 'skills',
                text: 'nunchucks',
            },
            {
                field: 'position descriptor',
                text: 'contract',
            },
            {
                field: 'perks',
                text: 'bonus',
            },
        ];

        searches.forEach(search => {
            it(`should filter by ${search.field}`, () => {
                render({
                    searchText: search.text,
                });
                SpecHelper.expectElements(elem, 'position-bar', 1);
            });
        });
    });
});
