import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/careers_spec_helper';
import setSpecLocales from 'Translation/setSpecLocales';
import connectionsLocales from 'Careers/locales/careers/connections-en.json';
import connectionConversationLocales from 'Careers/locales/careers/connection_conversation-en.json';

setSpecLocales(connectionsLocales, connectionConversationLocales);

describe('FrontRoyal.Careers.Connections', () => {
    let $injector;
    let SpecHelper;
    let CareersSpecHelper;
    let renderer;
    let elem;
    let scope;
    let $location;
    let careerNetworkViewModel;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                $location = $injector.get('$location');
            },
        ]);

        careerNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
        $location.search('list', null);
        $location.search('connectionId', null);

        SpecHelper.stubDirective('connectionsList');
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.role = opts.role ? opts.role : 'hiringManager';
        renderer.render('<connections role="role"></connections>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('with hiring manager role', () => {
        it('should have tabs for switching between connections lists', () => {
            render();

            SpecHelper.expectElementText(elem, '.tabs button.active-connections', 'My Candidates');
            SpecHelper.expectElementText(elem, '.tabs button.team-connections', 'Team Candidates');
            SpecHelper.expectElementText(elem, '.tabs button.closed-connections', 'Closed');

            assertTabWithConnectionsList('active-connections', 'activeConnections', false, 'hiringManager');
            // assert that the hiringManager role is being passed through
            SpecHelper.expectElementAttr(elem, 'connections-list', 'role', 'role');
            expect(scope.role).toEqual('hiringManager');

            assertTabWithConnectionsList('team-connections', 'teamConnections', false, 'hiringTeammate');
            // assert that the role is switched to hiringTeammate
            SpecHelper.expectElementAttr(elem, 'connections-list', 'role', "'hiringTeammate'");

            assertTabWithConnectionsList('closed-connections', 'closedConnections', true, 'hiringManager');
            // assert that the hiringManager role is being passed through
            SpecHelper.expectElementAttr(elem, 'connections-list', 'role', 'role');
            expect(scope.role).toEqual('hiringManager');
        });
    });

    function assertTabWithConnectionsList(tab, searchParam, closed) {
        SpecHelper.click(elem, `.tabs button.${tab}`);
        SpecHelper.expectElements(elem, '.pane.active-connections', tab === 'active-connections' ? 1 : 0);
        SpecHelper.expectElements(elem, '.pane.team-connections', tab === 'team-connections' ? 1 : 0);
        SpecHelper.expectElements(elem, '.pane.closed-connections', tab === 'closed-connections' ? 1 : 0);

        SpecHelper.expectHasClass(elem, '.tabs button.active-connections', 'active', tab === 'active-connections');
        SpecHelper.expectHasClass(elem, '.tabs button.team-connections', 'active', tab === 'team-connections');
        SpecHelper.expectHasClass(elem, '.tabs button.closed-connections', 'active', tab === 'closed-connections');

        expect($location.search().list).toEqual(searchParam);
        SpecHelper.expectElementAttr(elem, 'connections-list', 'closed', closed ? 'true' : 'false');
    }

    describe('with candidate role', () => {
        it('should render 2 tabs with appropriate titles', () => {
            careerNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            render({
                role: 'candidate',
            });

            // no team connections tab
            SpecHelper.expectNoElement(elem, '.tabs button.team-connections');

            SpecHelper.expectElement(elem, '.pane.active-connections');
            SpecHelper.expectNoElement(elem, '.pane.closed-connections');

            SpecHelper.expectElementText(elem, '.tabs button.active-connections', 'Open Connections');
            SpecHelper.expectHasClass(elem, '.tabs button.active-connections', 'active');
            SpecHelper.expectDoesNotHaveClass(elem, '.tabs button.closed-connections', 'active');
            expect($location.search().list).toEqual('activeConnections');

            SpecHelper.expectElementAttr(elem, 'connections-list', 'closed', 'false');
            expect(scope.role).toEqual('candidate');

            SpecHelper.click(elem, '.tabs button.closed-connections');
            SpecHelper.expectElementText(elem, '.tabs button.closed-connections', 'Closed');
            SpecHelper.expectNoElement(elem, '.pane.active-connections');

            SpecHelper.expectDoesNotHaveClass(elem, '.tabs button.active-connections', 'active');
            SpecHelper.expectHasClass(elem, '.tabs button.closed-connections', 'active');

            SpecHelper.expectElement(elem, '.pane.closed-connections');
            expect($location.search().list).toEqual('closedConnections');

            SpecHelper.expectElementAttr(elem, 'connections-list', 'closed', 'true');
            expect(scope.role).toEqual('candidate');
        });
    });

    describe('with connectionId', () => {
        it('should render the conversation for a matched relationship', () => {
            const hiringRelationshipViewModel = careerNetworkViewModel.testHelper.getRelationshipViewModelsByStatus()
                .matched;
            render();
            const HiringRelationship = $injector.get('HiringRelationship');
            HiringRelationship.expect('save');
            expectHiringRelationshipViewModel(hiringRelationshipViewModel);
            $location.search('connectionId', hiringRelationshipViewModel.connectionId);
            scope.$apply();
            SpecHelper.expectNoElement(elem, '.tabs');
            SpecHelper.expectElement(elem, 'connection-conversation');
            jest.spyOn($location, 'search').mockImplementation(() => {});
            hiringRelationshipViewModel.navigateToTracker();
            expect($location.search).toHaveBeenCalledWith('connectionId', null);
        });

        it('should render the conversation for a pending relationship', () => {
            careerNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            render({
                role: 'candidate',
            });
            const hiringRelationshipViewModel = careerNetworkViewModel.testHelper.getRelationshipViewModelsByStatus()
                .waiting_on_candidate;
            jest.spyOn(hiringRelationshipViewModel, 'showUpdateModal').mockImplementation(() => {});
            expectHiringRelationshipViewModel(hiringRelationshipViewModel);
            $location.search('connectionId', hiringRelationshipViewModel.connectionId);
            scope.$apply();
            SpecHelper.expectNoElement(elem, '.tabs');
            SpecHelper.expectElement(elem, 'connection-conversation');
            jest.spyOn($location, 'search').mockImplementation(() => {});
            hiringRelationshipViewModel.navigateToTracker();
            expect($location.search).toHaveBeenCalledWith('connectionId', null);
        });

        it('should render the conversation for a relationship rejected by candidate', () => {
            careerNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            render({
                role: 'candidate',
            });
            const hiringRelationshipViewModel = careerNetworkViewModel.testHelper.getRelationshipViewModelsByStatus()
                .rejected_by_candidate;
            jest.spyOn(hiringRelationshipViewModel, 'showUpdateModal').mockImplementation(() => {});
            expectHiringRelationshipViewModel(hiringRelationshipViewModel);
            $location.search('connectionId', hiringRelationshipViewModel.connectionId);
            scope.$apply();
            SpecHelper.expectNoElement(elem, '.tabs');
            SpecHelper.expectElement(elem, 'connection-conversation');
            jest.spyOn($location, 'search').mockImplementation(() => {});
            hiringRelationshipViewModel.navigateToTracker();
            expect($location.search).toHaveBeenCalledWith('connectionId', null);
        });

        it('should show the modal to a hiring manager for a connection pending relationship', () => {
            render();
            const hiringRelationshipViewModel = careerNetworkViewModel.testHelper.getRelationshipViewModelsByStatus()
                .waiting_on_candidate;
            expectHiringRelationshipViewModel(hiringRelationshipViewModel);
            jest.spyOn(hiringRelationshipViewModel, 'showUpdateModal').mockImplementation(() => {});
            $location.search('connectionId', hiringRelationshipViewModel.connectionId);

            scope.$apply();

            // no modal shown
            expect(hiringRelationshipViewModel.showUpdateModal).toHaveBeenCalled();

            // tabs should still be visible
            SpecHelper.expectElement(elem, '.tabs');

            // conversation should not be rendered
            SpecHelper.expectNoElement(elem, 'connection-conversation');
        });

        it('should not show the modal to a hiring manager for a pending relationship', () => {
            // Maybe we actually should show it in this case, theoretically, but I'm not sure how
            // we would get here, and this spec was already here from before, so I'm just leaving
            // it as-is
            render();
            const hiringRelationshipViewModel = careerNetworkViewModel.testHelper.getRelationshipViewModelsByStatus()
                .waiting_on_hiring_manager;
            expectHiringRelationshipViewModel(hiringRelationshipViewModel);
            jest.spyOn(hiringRelationshipViewModel, 'showUpdateModal').mockImplementation(() => {});
            $location.search('connectionId', hiringRelationshipViewModel.connectionId);

            scope.$apply();

            // no modal shown
            expect(hiringRelationshipViewModel.showUpdateModal).not.toHaveBeenCalled();

            // tabs should still be visible
            SpecHelper.expectElement(elem, '.tabs');

            // conversation should not be rendered
            SpecHelper.expectNoElement(elem, 'connection-conversation');
        });

        function expectHiringRelationshipViewModel(hiringRelationshipViewModel) {
            const $q = $injector.get('$q');
            jest.spyOn(careerNetworkViewModel, 'getHiringRelationshipViewModelAcceptedByTeam').mockReturnValue(
                $q.when(hiringRelationshipViewModel),
            );
        }
    });
});
