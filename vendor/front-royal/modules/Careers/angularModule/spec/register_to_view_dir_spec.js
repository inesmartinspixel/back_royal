import 'AngularSpecHelper';
import 'Careers/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import registerToViewLocales from 'Careers/locales/careers/register_to_view-en.json';

setSpecLocales(registerToViewLocales);

describe('FrontRoyal.Careers.RegisterToView', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);
        render();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.callback = jest.fn();
        renderer.render('<register-to-view callback="callback()"></register-to-view>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should display a message with a register buton navigating to home', () => {
        SpecHelper.expectElementText(elem, '.title', 'Create Your Account');
        SpecHelper.expectElementText(
            elem,
            '.sub-title',
            'Register to continue viewing profiles in the Smartly Talent network!',
        );
        const button = SpecHelper.expectElementText(elem, '.modal-action-button', 'Register');

        jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});
        button.click();

        expect(scope.loadUrl).toHaveBeenCalledWith('/hiring');
        expect(renderer.scope.callback).toHaveBeenCalled();
    });
});
