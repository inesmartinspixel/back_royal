import 'AngularSpecHelper';
import 'Careers/angularModule';
import inviteTeammateLocales from 'Careers/locales/careers/invite_teammate-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(inviteTeammateLocales);

describe('FrontRoyal.Careers.InviteTeammateFormDir', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let invitation;
    let DialogModal;
    let HttpQueue;
    let config;
    let $timeout;
    let $q;
    let onFinish;
    let HiringTeamInvite;
    let currentUser;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper', 'NoUnhandledRejectionExceptions');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $timeout = $injector.get('$timeout');
                $q = $injector.get('$q');
                HiringTeamInvite = $injector.get('HiringTeamInvite');
                DialogModal = $injector.get('DialogModal');
                HttpQueue = $injector.get('HttpQueue');
            },
        ]);

        onFinish = jest.fn();

        config = {
            'FrontRoyal.ApiErrorHandler': {
                skip: true,
            },
        };

        currentUser = SpecHelper.stubCurrentUser();

        invitation = {
            inviter_id: currentUser.id,
            invitee_email: `invitee@${currentUser.emailDomain}`,
            invitee_name: 'Invitee',
        };

        SpecHelper.stubDirective('requireDomain');
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.onFinish = onFinish;
        renderer.render('<invite-teammate-form on-finish="onFinish"></invite-teammate-form');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('invite sucess', () => {
        it('should create a hiring team invite', () => {
            let resolveCreate;
            jest.spyOn(HiringTeamInvite, 'create').mockReturnValue(
                $q(resolve => {
                    resolveCreate = resolve;
                }),
            );

            render();

            SpecHelper.updateTextInput(elem, '[name=name]', invitation.invitee_name);
            SpecHelper.updateTextInput(elem, '[name=email]', invitation.invitee_email);
            SpecHelper.submitForm(elem);

            expect(HiringTeamInvite.create).toHaveBeenCalledWith(invitation, {}, config);

            resolveCreate();
            $timeout.flush(0);

            SpecHelper.expectNoElement(elem, '.front-royal-form-container');
            SpecHelper.expectElement(elem, '.sent-message');

            $timeout.flush(1749);
            expect(scope.onFinish).not.toHaveBeenCalled();
            $timeout.flush(1);
            expect(scope.onFinish).toHaveBeenCalled();
        });
    });

    // see also: NoUnhandledRejectionExceptions module use above
    describe('invite failure', () => {
        beforeEach(() => {
            render();

            SpecHelper.updateTextInput(elem, '[name=name]', invitation.invitee_name);
            SpecHelper.updateTextInput(elem, '[name=email]', invitation.invitee_email);

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            jest.spyOn(HttpQueue.instance, 'unfreezeAfterError').mockImplementation(() => {});

            config.url = '/api/hiring_team_invites';
        });

        it('should special-handle invitee_exists error when invitee is already a user', () => {
            setupCreateFailure({
                inviteeHiringTeamId: 123,
                inviteeProvider: 'hiring_team_invite',
            });

            expect(DialogModal.alert).toHaveBeenCalled();
            assertDialogMessage({
                content: `There is already an account for ${invitation.invitee_email}. Please contact <a href="mailto:hiring@smart.ly">hiring@smart.ly</a> to add them to your team.`,
                title: 'Contact Smartly',
            });
            expect(HttpQueue.instance.unfreezeAfterError).toHaveBeenCalled();
        });

        it('should special-handle invitee_exists error when invitee is already invited', () => {
            setupCreateFailure({
                inviteeHiringTeamId: currentUser.hiring_team_id,
                inviteeProvider: 'hiring_team_invite',
            });

            expect(DialogModal.alert).toHaveBeenCalled();
            assertDialogMessage({
                content: `${invitation.invitee_name} has already been invited to your team.`,
                title: 'Coworker Already Invited',
            });
            expect(HttpQueue.instance.unfreezeAfterError).toHaveBeenCalled();
        });

        it('should special-handle invitee_exists error when invitee is already on the team', () => {
            setupCreateFailure({
                inviteeHiringTeamId: currentUser.hiring_team_id,
                inviteeProvider: 'email',
            });

            expect(DialogModal.alert).toHaveBeenCalled();
            assertDialogMessage({
                content: `${invitation.invitee_name} is already on your team.`,
                title: 'Coworker Already Exists',
            });
            expect(HttpQueue.instance.unfreezeAfterError).toHaveBeenCalled();
        });

        function setupCreateFailure(params) {
            HiringTeamInvite.expect('create').fails({
                status: 406,
                data: {
                    meta: {
                        error_type: 'invitee_exists',
                        invitee_hiring_team_id: params.inviteeHiringTeamId,
                        invitee_provider: params.inviteeProvider,
                    },
                },
                config,
            });
            SpecHelper.submitForm(elem);
            HiringTeamInvite.flush('create');
        }

        function assertDialogMessage(params) {
            const opts = DialogModal.alert.mock.calls[0][0];
            expect(_.pick(opts, ['content', 'classes', 'title'])).toEqual({
                content: params.content,
                classes: ['server-error-modal', 'small'],
                title: params.title,
            });
        }
    });
});
