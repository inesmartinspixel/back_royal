import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/candidate_position_interest_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Careers/angularModule/spec/_mock/fixtures/open_position_fixtures';
import 'Careers/angularModule/spec/careers_spec_helper';
import setSpecLocales from 'Translation/setSpecLocales';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';

setSpecLocales(candidateListCardLocales);

describe('FrontRoyal.Careers.CandidateListActions', () => {
    let $injector;
    let SpecHelper;
    let CareersSpecHelper;
    let renderer;
    let elem;
    let scope;
    let careerProfile;
    let $q;
    let $timeout;
    let userOverrides;
    let OpenPosition;
    let CandidatePositionInterest;
    let CareerProfile;
    let careersNetworkViewModel;
    let dateHelper;
    let User;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;

                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                OpenPosition = $injector.get('OpenPosition');
                CandidatePositionInterest = $injector.get('CandidatePositionInterest');
                CareerProfile = $injector.get('CareerProfile');
                $q = $injector.get('$q');
                $timeout = $injector.get('$timeout');
                dateHelper = $injector.get('dateHelper');
                User = $injector.get('User');

                $injector.get('CareerProfileFixtures');
                $injector.get('CandidatePositionInterestFixtures');
                $injector.get('User');

                careerProfile = CareerProfile.fixtures.getInstance();
            },
        ]);

        SpecHelper.stubCurrentUser(undefined, undefined, undefined, userOverrides);
        careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = opts.ngModel || careerProfile;
        renderer.scope.removeItem = jest.fn();
        renderer.scope.showHideButton = true;
        renderer.scope.getCandidatePositionInterest = opts.getCandidatePositionInterest || jest.fn();
        renderer.scope.adminMode = opts.adminMode;
        renderer.render(
            '<candidate-list-actions ng-model="ngModel" remove-item="removeItem()" show-hide-button="showHideButton" ' +
                'get-candidate-position-interest="getCandidatePositionInterest()" admin-mode="adminMode"></candidate-list-actions>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should show a badge when outstanding', () => {
        const getCandidatePositionInterest = jest.fn().mockReturnValue(
            CandidatePositionInterest.fixtures.getInstance({
                admin_status: 'outstanding',
                hiring_manager_status: 'unseen',
            }),
        );

        render({
            getCandidatePositionInterest,
        });
        expect(scope.outstanding).toBe(true);
        SpecHelper.expectElementText(elem, '.outstanding', 'Great Applicant'); // css makes it uppercase
    });

    describe('careerProfile', () => {
        it('should set career profile to the ngModel if it is a CareerProfile', () => {
            render();
            expect(scope.careerProfile).toBe(scope.ngModel);
        });

        it('should get career profile from the interest if ngModel is a CandidatePositionInterest', () => {
            const interest = CandidatePositionInterest.fixtures.getInstance();
            render({
                ngModel: interest,
            });
            expect(scope.careerProfile).toBe(interest.career_profile);
        });

        it('should get career profile from the hiring relationship if ngModel is a HiringRelationshipViewModel', () => {
            const hrvm = careersNetworkViewModel.hiringRelationshipViewModels[0];
            render({
                ngModel: hrvm,
            });
            expect(scope.careerProfile).toBe(hrvm.careerProfile);
        });
    });

    describe('candidatePositionInterest', () => {
        it('should set candidatePositionInterest to the ngModel if it is a CandidatePositionInterest', () => {
            const interest = CandidatePositionInterest.fixtures.getInstance();
            render({
                ngModel: interest,
            });
            expect(scope.candidatePositionInterest).toBe(interest);
        });

        it('should get candidatePositionInterest using the callback if ngModel is not a CandidatePositionInterest', () => {
            const interest = CandidatePositionInterest.fixtures.getInstance();
            const getCandidatePositionInterest = jest.fn().mockReturnValue(interest);
            render({
                getCandidatePositionInterest,
            });
            expect(scope.candidatePositionInterest.id).toEqual(interest.id);
        });

        it('should show a badge when reviewed by teammate', () => {
            const teammate = User.fixtures.getInstance({
                name: 'James Bond',
            });
            const interest = CandidatePositionInterest.fixtures.getInstance({
                hiring_manager_reviewer_id: teammate.id,
                hiring_manager_reviewer_name: teammate.name,
                hiring_manager_reviewer_avatar_url: teammate.avatar_url,
            });
            render({
                ngModel: interest,
            });
            expect(scope.teammates.length).toBe(1);
            SpecHelper.expectElementText(elem, '.badges', teammate.name);
            SpecHelper.expectElement(elem, '.badges careers-avatar');
        });
    });

    describe('positionTitle', () => {
        let hrvm;

        beforeEach(() => {
            $injector.get('OpenPositionFixtures');
            hrvm = careersNetworkViewModel.hiringRelationshipViewModels[0];
            jest.spyOn(hrvm, 'invitedPosition', 'get').mockReturnValue(
                OpenPosition.fixtures.getInstance({
                    title: 'foo',
                }),
            );
        });

        it('should set positionTitle if ngModel is a CareerProfile', () => {
            jest.spyOn(
                careersNetworkViewModel,
                'getOrInitializeHiringRelationshipViewModelForCareerProfile',
            ).mockReturnValue($q.when(hrvm));
            render();
            expect(scope.positionTitle).toBe('foo');
        });

        it('should set positionTitle if ngModel is a CandidatePositionInterest', () => {
            jest.spyOn(
                careersNetworkViewModel,
                'getOrInitializeHiringRelationshipViewModelForCareerProfile',
            ).mockReturnValue($q.when(hrvm));
            render({
                ngModel: CandidatePositionInterest.fixtures.getInstance(),
            });
            expect(scope.positionTitle).toBe('foo');
        });

        it('should set positionTitle if ngModel is a HiringRelationshipViewModel', () => {
            render({
                ngModel: hrvm,
            });
            expect(scope.positionTitle).toBe('foo');
        });
    });

    describe('actions', () => {
        beforeEach(() => {
            render();
        });

        it('should open conversation', () => {
            const viewModel = {
                openConnection: jest.fn(),
            };
            jest.spyOn(
                careersNetworkViewModel,
                'getOrInitializeHiringRelationshipViewModelForCareerProfile',
            ).mockReturnValue($q.when(viewModel));
            scope.openConnection();
            expect(
                careersNetworkViewModel.getOrInitializeHiringRelationshipViewModelForCareerProfile,
            ).toHaveBeenCalledWith(careerProfile);
            $timeout.flush('500'); // wait a half second for the promise
            expect(viewModel.openConnection).toHaveBeenCalled();
        });

        it('should open invite modal', () => {
            jest.spyOn(scope, 'openInviteModal').mockImplementation(() => {});
            SpecHelper.click(elem, 'button.invite');
            expect(scope.openInviteModal).toHaveBeenCalled();
        });

        it('should show full profile', () => {
            jest.spyOn(scope.careersNetworkViewModel, 'showFullProfile').mockImplementation(() => {});
            SpecHelper.click(elem, 'button.full-profile');
            expect(scope.careersNetworkViewModel.showFullProfile).toHaveBeenCalled();
        });

        describe('when anonymized', () => {
            beforeEach(() => {
                render();
                scope.careerProfile.anonymized = true;
                jest.spyOn(scope.careersNetworkViewModel, 'showHiringBillingModal').mockImplementation(() => {});
                jest.spyOn(scope, 'removeItem').mockImplementation(() => {});
            });

            it('should show billing modal when inviting', () => {
                SpecHelper.click(elem, '.invite');
                expect(scope.removeItem).not.toHaveBeenCalled();
                expect(scope.careersNetworkViewModel.showHiringBillingModal).toHaveBeenCalledWith(
                    scope.careerProfile,
                    'openInviteModal',
                );
            });
        });
    });

    describe('adminMode', () => {
        let interest;
        let getCandidatePositionInterest;

        beforeEach(() => {
            interest = CandidatePositionInterest.fixtures.getInstance({
                hiring_manager_priority: 13,
            });
            getCandidatePositionInterest = jest.fn().mockReturnValue(interest);
        });

        it('should show createdAt when showCreatedAt', () => {
            render({
                ngModel: interest,
                adminMode: 'unreviewed',
            });
            SpecHelper.expectElementText(
                elem,
                '.interest-created-at',
                `Created at: ${dateHelper.formattedUserFacingMonthDayYearShort(interest.createdAt, false)}`,
            );
        });

        it('should update hiring_manager_priority if it changes', () => {
            const interest = CandidatePositionInterest.fixtures.getInstance({
                hiring_manager_priority: 1,
            });
            const getCandidatePositionInterest = jest.fn().mockReturnValue(interest);
            render({
                adminMode: 'outstanding',
                getCandidatePositionInterest,
            });
            scope.candidatePositionInterest.hiring_manager_priority = 1337;
            scope.$digest();
            SpecHelper.expectTextInputVal(elem, '#interest-priority', '1338');
        });

        describe('adminModeViewModel', () => {
            it('should be correct for unreviewed', () => {
                render({
                    adminMode: 'unreviewed',
                });

                expect(scope.adminModeViewModel).toEqual({
                    showSendToOutstanding: true,
                    showSendToApproved: true,
                    showSendToHidden: true,
                    showSendToUnreviewed: false,
                    showPriorityInput: false,
                    showCreatedAt: true,
                    disableLaunchFullProfile: false,
                });
            });

            it('should be correct for outstanding', () => {
                render({
                    adminMode: 'outstanding',
                });

                expect(scope.adminModeViewModel).toEqual({
                    showSendToOutstanding: false,
                    showSendToApproved: true,
                    showSendToHidden: true,
                    showSendToUnreviewed: false,
                    showPriorityInput: true,
                    showCreatedAt: true,
                    disableLaunchFullProfile: false,
                });
            });

            it('should be correct for approved', () => {
                render({
                    adminMode: 'approved',
                });

                expect(scope.adminModeViewModel).toEqual({
                    showSendToOutstanding: true,
                    showSendToApproved: false,
                    showSendToHidden: true,
                    showSendToUnreviewed: false,
                    showPriorityInput: true,
                    showCreatedAt: true,
                    disableLaunchFullProfile: false,
                });
            });

            describe('reviewed states', () => {
                it('should be correct for saved', () => {
                    render({
                        adminMode: 'saved',
                    });

                    expect(scope.adminModeViewModel).toEqual({
                        showSendToOutstanding: false,
                        showSendToApproved: false,
                        showSendToHidden: false,
                        showSendToUnreviewed: false,
                        showPriorityInput: false,
                        showCreatedAt: false,
                        disableLaunchFullProfile: true,
                    });
                });

                it('should be correct for connected', () => {
                    render({
                        adminMode: 'connected',
                    });

                    expect(scope.adminModeViewModel).toEqual({
                        showSendToOutstanding: false,
                        showSendToApproved: false,
                        showSendToHidden: false,
                        showSendToUnreviewed: false,
                        showPriorityInput: false,
                        showCreatedAt: false,
                        disableLaunchFullProfile: true,
                    });
                });
                it('should be correct for passed', () => {
                    render({
                        adminMode: 'passed',
                    });

                    expect(scope.adminModeViewModel).toEqual({
                        showSendToOutstanding: false,
                        showSendToApproved: false,
                        showSendToHidden: false,
                        showSendToUnreviewed: false,
                        showPriorityInput: false,
                        showCreatedAt: false,
                        disableLaunchFullProfile: true,
                    });
                });
            });

            it('should be correct for hidden', () => {
                render({
                    adminMode: 'hidden',
                });

                expect(scope.adminModeViewModel).toEqual({
                    showSendToOutstanding: false,
                    showSendToApproved: false,
                    showSendToHidden: false,
                    showSendToUnreviewed: true,
                    showPriorityInput: false,
                    showCreatedAt: false,
                    disableLaunchFullProfile: true,
                });
            });
        });

        describe('triggerInterestAdminAction', () => {
            describe('setInterestPriority', () => {
                it('should set', () => {
                    const getCandidatePositionInterest = jest.fn().mockReturnValue(
                        CandidatePositionInterest.fixtures.getInstance({
                            hiring_manager_priority: 13,
                        }),
                    );
                    render({
                        adminMode: 'outstanding',
                        getCandidatePositionInterest,
                    });
                    expect(scope.tempInterestPriorityUserFacing).toBe(14);
                    SpecHelper.updateTextInput(elem, '#interest-priority', 101);

                    jest.spyOn(scope, '$emit').mockImplementation(() => {});
                    SpecHelper.click(elem, '[name="set-interest-priority"]');

                    expect(scope.$emit).toHaveBeenCalledWith('admin:interestAdminAction', {
                        action: 'setInterestPriority',
                        interest: scope.candidatePositionInterest,
                        group: scope.adminMode,
                        priority: 100,
                    });
                });

                it('should increment priority', () => {
                    render({
                        adminMode: 'outstanding',
                        getCandidatePositionInterest,
                    });

                    jest.spyOn(scope, '$emit').mockImplementation(() => {});
                    SpecHelper.click(elem, '[name="priority-up"]');

                    expect(scope.$emit).toHaveBeenCalledWith('admin:interestAdminAction', {
                        action: 'setInterestPriority',
                        interest: scope.candidatePositionInterest,
                        group: scope.adminMode,
                        priority: 'higherPriority',
                    });
                });

                it('should decrement priority', () => {
                    render({
                        adminMode: 'outstanding',
                        getCandidatePositionInterest,
                    });

                    jest.spyOn(scope, '$emit').mockImplementation(() => {});
                    SpecHelper.click(elem, '[name="priority-down"]');

                    expect(scope.$emit).toHaveBeenCalledWith('admin:interestAdminAction', {
                        action: 'setInterestPriority',
                        interest: scope.candidatePositionInterest,
                        group: scope.adminMode,
                        priority: 'lowerPriority',
                    });
                });
            });

            it('should sendToOutstanding', () => {
                render({
                    ngModel: interest,
                    adminMode: 'unreviewed',
                });

                jest.spyOn(scope, '$emit').mockImplementation(() => {});
                SpecHelper.click(elem, '[name="send-to-outstanding"]');
                expect(scope.$emit).toHaveBeenCalledWith('admin:interestAdminAction', {
                    action: 'sendToOutstanding',
                    interest: scope.candidatePositionInterest,
                    group: scope.adminMode,
                    priority: undefined,
                });
            });

            it('should sendToApproved', () => {
                render({
                    ngModel: interest,
                    adminMode: 'unreviewed',
                });

                jest.spyOn(scope, '$emit').mockImplementation(() => {});
                SpecHelper.click(elem, '[name="send-to-approved"]');
                expect(scope.$emit).toHaveBeenCalledWith('admin:interestAdminAction', {
                    action: 'sendToApproved',
                    interest: scope.candidatePositionInterest,
                    group: scope.adminMode,
                    priority: undefined,
                });
            });

            it('should sendToHidden', () => {
                interest.admin_status = 'outstanding';
                render({
                    ngModel: interest,
                    adminMode: 'outstanding',
                });

                jest.spyOn(scope, '$emit').mockImplementation(() => {});
                SpecHelper.click(elem, '[name="send-to-hidden"]');
                expect(scope.$emit).toHaveBeenCalledWith('admin:interestAdminAction', {
                    action: 'sendToHidden',
                    interest: scope.candidatePositionInterest,
                    group: scope.adminMode,
                    priority: undefined,
                });
            });

            it('should sendToUnreviewed', () => {
                render({
                    ngModel: interest,
                    adminMode: 'hidden',
                });

                jest.spyOn(scope, '$emit').mockImplementation(() => {});
                SpecHelper.click(elem, '[name="send-to-unreviewed"]');
                expect(scope.$emit).toHaveBeenCalledWith('admin:interestAdminAction', {
                    action: 'sendToUnreviewed',
                    interest: scope.candidatePositionInterest,
                    group: scope.adminMode,
                    priority: undefined,
                });
            });
        });
    });
});
