import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_application_fixtures';
import setSpecLocales from 'Translation/setSpecLocales';
import hiringManagerCardLocales from 'Careers/locales/careers/hiring_manager_card-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';

setSpecLocales(hiringManagerCardLocales, fieldOptionsLocales);

describe('FrontRoyal.Careers.HiringManagerCard', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let hiringApplication;
    let EventLogger;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                const HiringApplication = $injector.get('HiringApplication');
                EventLogger = $injector.get('EventLogger');

                $injector.get('HiringApplicationFixtures');
                hiringApplication = HiringApplication.fixtures.getInstance();
                jest.spyOn(EventLogger.instance, 'log').mockImplementation(() => {});
            },
        ]);

        render();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.hiringApplication = hiringApplication;
        renderer.render('<hiring-manager-card hiring-application="hiringApplication"></hiring-manager-card>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    function assertEventLogged(eventType, attrs) {
        expect(EventLogger.instance.log).toHaveBeenCalledWith(
            eventType,
            angular.extend(attrs || {}, {
                hiring_manager_id: hiringApplication.user_id,
            }),
        );
    }

    it('should log an event when viewed', () => {
        assertEventLogged('hiring_manager_card:viewed_page', {
            label: 'page 1',
        });
    });

    it('should display a link to the company website', () => {
        SpecHelper.expectElement(elem, `a[href="${hiringApplication.website_url}"][target="_blank"]`);
    });

    it('should display the company background values', () => {
        expect(hiringApplication.company_year).toEqual('2006'); // sanity
        SpecHelper.expectElementText(elem, '.detail.founded', 'FOUNDED 2006');

        expect(hiringApplication.funding).toEqual('public'); // sanity
        SpecHelper.expectElementText(elem, '.detail.funding', 'FUNDING Public');

        expect(hiringApplication.company_annual_revenue).toEqual('more_than_500_million'); // sanity
        SpecHelper.expectElementText(elem, '.detail.annual-revenue', 'ANNUAL REVENUE > $500 million');

        expect(hiringApplication.company_employee_count).toEqual('5000_plus'); // sanity
        SpecHelper.expectElementText(elem, '.detail.employees', 'EMPLOYEES 5000+');
    });

    it('should hide revenue when manager has opted to', () => {
        hiringApplication.company_annual_revenue = 'rather_not_specify';
        scope.$digest();
        SpecHelper.expectElement(elem, '.company-background.without-revenue');
        SpecHelper.expectNoElement(elem, '.detail.annual-revenue');
    });

    it('should display the mission statement', () => {
        expect(hiringApplication.team_mission).toEqual(
            'Enable easy building of sophisticated, scalable web applications.',
        ); // sanity
        SpecHelper.expectElementText(
            elem,
            '.mission-statement',
            'ACME products MISSION Enable easy building of sophisticated, scalable web applications.',
        );
    });

    it('should display the location and team values', () => {
        jest.spyOn(hiringApplication, 'locationString', 'get').mockReturnValue('Seattle, WA');
        render();

        SpecHelper.expectElementText(elem, '.location-and-team div:eq(0)', 'Seattle, WA');

        expect(hiringApplication.industry).toEqual('technology'); // sanity
        SpecHelper.expectElementText(elem, '.location-and-team div:eq(1)', 'Technology');

        expect(hiringApplication.team_name).toEqual('Core Services Team'); // sanity
        SpecHelper.expectElementText(elem, '.location-and-team div:eq(2)', 'Core Services Team');
    });

    it('should have avatar_url if set', () => {
        const avatarElem = elem.find('careers-avatar');
        expect(avatarElem.isolateScope().fullSrcUrl).toBe(
            `url(https://path/to/hiring_manager/avatar.png), url(${$injector.get('DEFAULT_PROFILE_PHOTO')})`,
        );
    });

    describe('roles section', () => {
        it('shoud display the truncated roles being hired for', () => {
            hiringApplication.role_descriptors = ['sales', 'marketing', 'ux_ui'];
            scope.$digest();

            SpecHelper.expectElementText(elem, '.hiring-roles label', "ROLES WE'RE HIRING");
            SpecHelper.expectElements(elem, '.hiring-roles div', 3); // truncated
            SpecHelper.expectElementText(elem, '.hiring-roles div:eq(0)', 'Sales / Business Development');
            SpecHelper.expectElementText(elem, '.hiring-roles div:eq(1)', 'Marketing');
            SpecHelper.expectElementText(elem, '.hiring-roles div:eq(2)', 'UX / UI');
        });

        describe('toggle view-all-overlay', () => {
            beforeEach(() => {
                SpecHelper.expectNoElement(elem, '.careers-card-view-all-overlay');
            });
            it('should work for roles', () => {
                SpecHelper.click(elem, '.view-all');
                SpecHelper.expectElement(elem, '.careers-card-view-all-overlay');
                assertEventLogged('hiring_manager_card:clicked_view_all');
            });

            it('shoud display the full list of roles being hired for', () => {
                expect(hiringApplication.role_descriptors).toEqual([
                    'sales',
                    'marketing',
                    'general_management',
                    'finance',
                ]); // sanity

                SpecHelper.click(elem, '.view-all');
                SpecHelper.expectElement(elem, '.careers-card-view-all-overlay');

                SpecHelper.expectElements(elem, '.careers-card-view-all-overlay .hiring-roles div', 4); // truncated
                SpecHelper.expectElementText(
                    elem,
                    '.careers-card-view-all-overlay .hiring-roles div:eq(0)',
                    'Sales / Business Development',
                );
                SpecHelper.expectElementText(
                    elem,
                    '.careers-card-view-all-overlay .hiring-roles div:eq(1)',
                    'Marketing',
                );
                SpecHelper.expectElementText(
                    elem,
                    '.careers-card-view-all-overlay .hiring-roles div:eq(2)',
                    'Management / Operations',
                );
                SpecHelper.expectElementText(elem, '.careers-card-view-all-overlay .hiring-roles div:eq(3)', 'Finance');
            });

            it('should close when x is clicked', () => {
                SpecHelper.click(elem, '.view-all');
                SpecHelper.expectElement(elem, '.careers-card-view-all-overlay');

                SpecHelper.click(elem, '.careers-card-view-all-overlay [name="close"]');
                SpecHelper.expectNoElement(elem, '.careers-card-view-all-overlay');
                assertEventLogged('hiring_manager_card:clicked_close_view_all');
            });
        });

        it('shoud hide the view all link if less than 4 roles', () => {
            hiringApplication.role_descriptors.pop();
            render();
            SpecHelper.expectNoElement(elem, '.view-all');
        });

        it('should not error on incomplete data', () => {
            hiringApplication.funding = null;
            const ErrorLogService = $injector.get('ErrorLogService');
            jest.spyOn(ErrorLogService, 'notify').mockImplementation(() => {});
            render();
            expect(ErrorLogService.notify).not.toHaveBeenCalled();
        });

        it('should not try to translate a custom value', () => {
            hiringApplication.role_descriptors = ['Foo'];
            scope.$digest();
            SpecHelper.expectElementText(elem, '.hiring-roles div:eq(0)', 'Foo');
        });
    });

    it('should not display industry and team name if not set', () => {
        hiringApplication.industry = undefined;
        hiringApplication.team_name = undefined;
        render();
        SpecHelper.expectElements(elem, '.location-and-team div', 1);
    });

    it('should not display subtext if not set', () => {
        hiringApplication.job_role = undefined;
        render();
        SpecHelper.expectNoElement(elem, '.subtext');
    });
});
