import 'AngularSpecHelper';
import 'Careers/angularModule';

describe('FrontRoyal.Careers.HiringApplication', () => {
    let $injector;
    let HiringApplication;
    let user;
    let User;
    let hiringApplication;
    let ProfileCompletionHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                HiringApplication = $injector.get('HiringApplication');
                $injector.get('MockIguana');
                User = $injector.get('User');
                ProfileCompletionHelper = $injector.get('ProfileCompletionHelper');
            },
        ]);

        user = User.new({
            hiring_application: {},
        });
        hiringApplication = user.hiring_application;
    });

    describe('before save callbacks', () => {
        it('should store last_calculated_complete_percentage', () => {
            jest.spyOn(hiringApplication, 'percentComplete').mockReturnValue(42);
            expect(hiringApplication.last_calculated_complete_percentage).not.toBe(42);
            HiringApplication.expect('save');
            hiringApplication.save();
            expect(hiringApplication.last_calculated_complete_percentage).toBe(42);
        });
    });

    describe('percent completed', () => {
        it('should get the percent completed for the entire hiring application', () => {
            expect(hiringApplication.percentComplete()).toBe(0);

            hiringApplication.name = 'John Doe';
            const valWithName = hiringApplication.percentComplete();
            expect(valWithName).toBeDefined();
            expect(valWithName).not.toBe(0);

            hiringApplication.company_year = '1942';
            const valWithNameAndBio = hiringApplication.percentComplete();
            expect(valWithNameAndBio).toBeDefined();
            expect(valWithNameAndBio).not.toBe(0);
            expect(valWithNameAndBio > valWithName).toBe(true);
        });

        it('should get the percent completed for a section of the hiring application', () => {
            expect(hiringApplication.percentComplete()).toBe(0);

            // update an optional field in the you section
            hiringApplication.avatar_url = 'https://path/to/avatar';
            const sectionPercentCompletewithAvatarUrl = hiringApplication.percentComplete('you');
            expect(sectionPercentCompletewithAvatarUrl).toBeDefined();
            expect(sectionPercentCompletewithAvatarUrl).toEqual(0);

            // update another field in the you section
            hiringApplication.nickname = 'Bob';
            const sectionPercentCompleteWithAvatarUrlAndNickname = hiringApplication.percentComplete('you');
            expect(sectionPercentCompleteWithAvatarUrlAndNickname).toBeDefined();
            expect(sectionPercentCompleteWithAvatarUrlAndNickname).not.toBe(0);
            expect(sectionPercentCompleteWithAvatarUrlAndNickname).toBeGreaterThan(sectionPercentCompletewithAvatarUrl);

            // compare section percent complete against hiringApplication percent complete
            const hiringApplicationPercentComplete = hiringApplication.percentComplete();
            expect(sectionPercentCompleteWithAvatarUrlAndNickname).toBeGreaterThan(hiringApplicationPercentComplete);
        });
    });

    describe('complete', () => {
        it('should be false if < 100', () => {
            jest.spyOn(ProfileCompletionHelper.prototype, 'getPercentComplete').mockReturnValue(99);
            expect(hiringApplication.complete).toBe(false);
            expect(ProfileCompletionHelper.prototype.getPercentComplete).toHaveBeenCalledWith(
                hiringApplication,
                undefined,
            );
        });

        it('should be true if 100', () => {
            jest.spyOn(ProfileCompletionHelper.prototype, 'getPercentComplete').mockReturnValue(100);
            expect(hiringApplication.complete).toBe(true);
            expect(ProfileCompletionHelper.prototype.getPercentComplete).toHaveBeenCalledWith(
                hiringApplication,
                undefined,
            );
        });
    });

    describe('getStepsProgressMap', () => {
        it('should getStepsProgressMap from ProfileCompletionHelper', () => {
            const stepProgressMap = {
                company: 'incomplete',
                team: 'incomplete',
                you: 'incomplete',
                candidates: 'incomplete',
                preview: 'none',
            };
            jest.spyOn(ProfileCompletionHelper.prototype, 'getStepsProgressMap').mockReturnValue(stepProgressMap);
            expect(hiringApplication.getStepsProgressMap()).toEqual(stepProgressMap);
            expect(ProfileCompletionHelper.prototype.getStepsProgressMap).toHaveBeenCalledWith(hiringApplication);
        });
    });

    describe('getStepProgress', () => {
        it('should getStepProgress from ProfileCompletionHelper', () => {
            // mock a step with an incomplete required field
            const step = {
                stepName: 'test',
                requiredFields: {
                    some_field: true,
                },
            };
            jest.spyOn(ProfileCompletionHelper.prototype, 'getStepProgress').mockReturnValue('incomplete');
            expect(hiringApplication.getStepProgress(step)).toEqual('incomplete');
            expect(ProfileCompletionHelper.prototype.getStepProgress).toHaveBeenCalledWith(step, hiringApplication);
        });
    });
});
