import 'AngularSpecHelper';
import 'Careers/angularModule';

describe('FrontRoyal.Careers.HiringRelationship', () => {
    let $injector;
    let HiringRelationship;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                $injector.get('MockIguana');
                HiringRelationship = $injector.get('HiringRelationship');
            },
        ]);
    });

    describe('closed', () => {
        it('should work', () => {
            const hiringRelationship = HiringRelationship.new();

            hiringRelationship.hiring_manager_closed = null;
            hiringRelationship.candidate_closed = null;
            expect(hiringRelationship.closed).toBe(false);

            hiringRelationship.hiring_manager_closed = true;
            hiringRelationship.candidate_closed = null;
            expect(hiringRelationship.closed).toBe(true);

            hiringRelationship.hiring_manager_closed = null;
            hiringRelationship.candidate_closed = true;
            expect(hiringRelationship.closed).toBe(true);

            hiringRelationship.hiring_manager_closed = true;
            hiringRelationship.candidate_closed = true;
            expect(hiringRelationship.closed).toBe(true);
        });
    });
});
