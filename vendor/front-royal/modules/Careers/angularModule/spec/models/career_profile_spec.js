import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';

describe('FrontRoyal.Careers.CareerProfile', () => {
    let $injector;
    let CareerProfile;
    let SpecHelper;
    let user;
    let profile;
    let User;
    let ProfileCompletionHelper;
    let EditCareerProfileHelper;
    let EducationExperience;
    let ErrorLogService;
    let ClientStorage;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');
                $injector.get('CareerProfileFixtures');
                User = $injector.get('User');
                $injector.get('MockIguana');
                ProfileCompletionHelper = $injector.get('ProfileCompletionHelper');
                EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                EducationExperience = $injector.get('EducationExperience');
                ErrorLogService = $injector.get('ErrorLogService');
                ClientStorage = $injector.get('ClientStorage');
            },
        ]);

        user = User.new({
            career_profile: {},
        });
        profile = user.career_profile;
    });

    describe('addDummyEducationExperienceIfNecessary', () => {
        let formalEducationExperiencesSpy;

        beforeEach(() => {
            formalEducationExperiencesSpy = jest.spyOn(profile, 'formalEducationExperiences', 'get');
        });

        describe('with formalEducationExperiences', () => {
            it('should not add a new dummy degreeProgram EducationExperience', () => {
                const educationExperiences = [
                    {
                        degreeProgram: true,
                    },
                ];
                profile.education_experiences = educationExperiences;
                profile.addDummyEducationExperienceIfNecessary();
                expect(profile.education_experiences.length).toEqual(educationExperiences.length);
                expect(formalEducationExperiencesSpy).toHaveBeenCalled();
            });
        });

        describe('with no formalEducationExperiences', () => {
            beforeEach(() => {
                profile.education_experiences = [
                    {
                        degreeProgram: false,
                    },
                ];
            });

            it('should add a new dummy degreeProgram EducationExperience', () => {
                profile.addDummyEducationExperienceIfNecessary();
                expect(profile.education_experiences.length).toEqual(2); // one non degree program exists, now we have one of each
                expect(profile.education_experiences[1].degreeProgram).toBe(true);
                expect(formalEducationExperiencesSpy).toHaveBeenCalled();
            });
        });
    });

    describe('addDummyWorkExperienceIfNecessary', () => {
        describe('with work_experiences', () => {
            it('should not create a dummy WorkExperience', () => {
                const workExperiences = [{}, {}, {}];
                profile.work_experiences = workExperiences;
                profile.addDummyWorkExperienceIfNecessary();
                expect(profile.work_experiences.length).toEqual(workExperiences.length);
            });
        });

        describe('with no work_experiences', () => {
            beforeEach(() => {
                profile.work_experiences = [];
            });

            it('should create a dummy WorkExperience', () => {
                profile.addDummyWorkExperienceIfNecessary();
                expect(profile.work_experiences).not.toEqual([]);
                expect(profile.work_experiences.length).toEqual(1);
            });

            describe('dummy WorkExperience', () => {
                it('should be full_time', () => {
                    profile.addDummyWorkExperienceIfNecessary();
                    expect(profile.work_experiences[0].employment_type).toEqual('full_time');
                });

                it('should not be featured', () => {
                    profile.addDummyWorkExperienceIfNecessary();
                    expect(profile.work_experiences[0].featured).toEqual(false);
                });
            });
        });
    });

    describe('before save callbacks', () => {
        it('should store last_calculated_complete_percentage', () => {
            jest.spyOn(EditCareerProfileHelper, 'isCareerProfile').mockReturnValue(true);
            jest.spyOn(profile, 'percentComplete').mockReturnValue(42);
            expect(profile.last_calculated_complete_percentage).not.toBe(42);
            CareerProfile.expect('save');
            profile.save();
            expect(profile.last_calculated_complete_percentage).toBe(42);
        });

        it('should sanitizePeerRecommendations', () => {
            jest.spyOn(profile, 'sanitizePeerRecommendations').mockImplementation(() => {});
            CareerProfile.expect('save');
            profile.save();
            expect(profile.sanitizePeerRecommendations).toHaveBeenCalled();
        });
    });

    describe('percent completed', () => {
        it('should get the percent completed for the entire career profile', () => {
            expect(profile.percentComplete()).toBe(0);

            profile.name = 'John Doe';
            const valWithName = profile.percentComplete();
            expect(valWithName).toBeDefined();
            expect(valWithName).not.toBe(0);

            profile.personal_fact = 'as akljdalsj dalk jd';
            const valWithNameAndPersonalFact = profile.percentComplete();
            expect(valWithNameAndPersonalFact).toBeDefined();
            expect(valWithNameAndPersonalFact).not.toBe(0);
            expect(valWithNameAndPersonalFact).toBeGreaterThan(valWithName);
        });

        it('should get the percent completed for a section of the career profile', () => {
            expect(profile.percentComplete()).toBe(0);

            // update a field in the basic_info section
            profile.name = 'John Doe';
            const sectionPercentCompleteWithName = profile.percentComplete('basic_info');
            expect(sectionPercentCompleteWithName).toBeDefined();
            expect(sectionPercentCompleteWithName).not.toBe(0);

            // update another field in the basic_info section
            profile.place_id = 'test_id';
            const sectionPercentCompleteWithNameAndPlaceId = profile.percentComplete('basic_info');
            expect(sectionPercentCompleteWithNameAndPlaceId).toBeDefined();
            expect(sectionPercentCompleteWithNameAndPlaceId).not.toBe(0);
            expect(sectionPercentCompleteWithNameAndPlaceId).toBeGreaterThan(sectionPercentCompleteWithName);

            // compare section percent complete against profile percent complete
            const profilePercentComplete = profile.percentComplete();
            expect(sectionPercentCompleteWithNameAndPlaceId).toBeGreaterThan(profilePercentComplete);
        });
    });

    describe('hasAtLeastOneTestScore', () => {
        it('should work', () => {
            _.each(profile.TESTS, test => {
                profile[`score_on_${test}`] = null;
            });
            expect(profile.hasAtLeastOneTestScore).toBe(false);
            profile[`score_on_${profile.TESTS[1]}`] = 42;
            expect(profile.hasAtLeastOneTestScore).toBe(true);
        });
    });

    describe('interestedInEmploymentType', () => {
        it('should work', () => {
            profile.employment_types_of_interest = [];
            expect(profile.interestedInEmploymentType('a')).toBe(false);
            profile.employment_types_of_interest = ['b'];
            expect(profile.interestedInEmploymentType('a')).toBe(false);
            profile.employment_types_of_interest = ['b', 'a'];
            expect(profile.interestedInEmploymentType('a')).toBe(true);
        });
    });

    describe('hasScore', () => {
        it('should work', () => {
            profile.score_on_test = null;
            expect(profile.hasScore('test')).toBe(false);
            profile.score_on_test = 0;
            expect(profile.hasScore('test')).toBe(true);
            profile.score_on_test = 42;
            expect(profile.hasScore('test')).toBe(true);
        });
    });

    describe('getScore', () => {
        it('should work', () => {
            profile.score_on_test = null;
            expect(profile.getScore('test')).toBe(null);
            profile.score_on_test = 0;
            expect(profile.getScore('test')).toBe(0);
            profile.score_on_test = 42;
            expect(profile.getScore('test')).toBe(42);
        });
    });

    describe('mostRecentEducationText', () => {
        it('should look at degree programs only', () => {
            profile.education_experiences = [
                EducationExperience.new({
                    educational_organization: {
                        text: 'College of Foo',
                    },
                    degree: 'Certificate of Foo',
                    degree_program: false,
                }),
                EducationExperience.new({
                    educational_organization: {
                        text: 'University of Foo',
                    },
                    degree: 'Masters of Foo',
                    degree_program: true,
                }),
                EducationExperience.new({
                    educational_organization: {
                        text: 'University of Foo',
                    },
                    degree: 'Bachelor of Foo',
                    degree_program: true,
                }),
            ];
            expect(profile.mostRecentEducationText).toEqual('Masters of Foo, University of Foo');
        });
    });

    describe('deepLinkUrl', () => {
        it('should dynamically create URL based on window.ENDPOINT_ROOT', () => {
            const $window = $injector.get('$window');
            $window.ENDPOINT_ROOT = 'https://foobar';
            expect(profile.deepLinkUrl).toEqual(
                `https://foobar/hiring/browse-candidates?tab=featured&id=${profile.id}`,
            );
        });
    });

    describe('salaryRangeForEventPayload', () => {
        it('should return lower bound of range', () => {
            profile.salary_range = '40000_to_49999';
            expect(profile.salaryRangeForEventPayload).toEqual(40000);
        });

        it("should handle 'over_200000'", () => {
            profile.salary_range = 'over_200000';
            expect(profile.salaryRangeForEventPayload).toEqual(200000);
        });

        it("should respect 'prefer_not_to_disclose'", () => {
            profile.salary_range = 'prefer_not_to_disclose';
            expect(profile.salaryRangeForEventPayload).toEqual(0);
        });

        it('should return 0 if no salary_range', () => {
            profile.salary_range = undefined;
            expect(profile.salaryRangeForEventPayload).toEqual(0);
        });
    });

    describe('complete', () => {
        it('should be false if < 100', () => {
            jest.spyOn(ProfileCompletionHelper.prototype, 'getPercentComplete').mockReturnValue(99);
            expect(profile.complete).toBe(false);
            expect(ProfileCompletionHelper.prototype.getPercentComplete).toHaveBeenCalledWith(profile, undefined);
        });

        it('should be true if 100', () => {
            jest.spyOn(ProfileCompletionHelper.prototype, 'getPercentComplete').mockReturnValue(100);
            expect(profile.complete).toBe(true);
            expect(ProfileCompletionHelper.prototype.getPercentComplete).toHaveBeenCalledWith(profile, undefined);
        });
    });

    describe('sanitization', () => {
        it("should sanitize avatar and name fields if the current user's preference is set", () => {
            const $rootScope = $injector.get('$rootScope');
            SpecHelper.stubCurrentUser();

            profile.name = 'Alfred Bojangles Clemmingsworth';
            profile.avatar_src = 'http://alfreds.org/clemmingsworth.png';

            jest.spyOn(profile, 'userInitials', 'get').mockReturnValue('A. B. C.');

            $rootScope.currentUser.pref_show_photos_names = true;

            expect(profile.sanitizedName).toBe(profile.name);
            expect(profile.sanitizedAvatarSrc).toBeUndefined();

            $rootScope.currentUser.pref_show_photos_names = false;

            expect(profile.sanitizedName).toBe('A. B. C.');
            expect(profile.sanitizedAvatarSrc).toBe(null);
        });

        it('should remove all peer recommendations that do not have a valid email', () => {
            const validPeerRecommendation = {
                id: 'some_id',
                email: 'johnny.appleseed@foo.com',
            };
            const invalidPeerRecommendation = {
                email: '',
            };
            const anotherInvalidPeerRecommendation = {};
            profile.peer_recommendations = [
                validPeerRecommendation,
                invalidPeerRecommendation,
                anotherInvalidPeerRecommendation,
            ];

            profile.sanitizePeerRecommendations();

            expect(profile.peer_recommendations).toEqual([validPeerRecommendation]);
        });
    });

    describe('hasSharedStudentNetworkInterests', () => {
        it('should return true when the comparison profile has share interests', () => {
            profile.student_network_interests = [
                {
                    text: 'travel',
                },
            ];
            const comparison = {
                student_network_interests: [
                    {
                        text: 'travel',
                    },
                ],
            };

            expect(profile.hasSharedStudentNetworkInterests(comparison)).toBe(true);
        });

        it('should return false when the comparison profile has no share interests', () => {
            profile.student_network_interests = [
                {
                    text: 'travel',
                },
            ];
            const comparison = {
                student_network_interests: [
                    {
                        text: 'art',
                    },
                ],
            };

            expect(profile.hasSharedStudentNetworkInterests(comparison)).toBe(false);
        });
    });

    describe('hasSameCompany', () => {
        beforeEach(() => {
            profile.addDummyWorkExperienceIfNecessary();
        });

        it('should return true when both profiles work at the same company', () => {
            profile.work_experiences[0] = {
                professional_organization: {
                    id: 123,
                    text: 'Acme Co.',
                },
                featured: true,
            };
            const comparison = {
                featuredWorkExperience: {
                    professional_organization: {
                        id: 123,
                        text: 'Acme Co.',
                    },
                },
            };

            expect(profile.hasSameCompany(comparison)).toBe(true);
        });

        it('should return false when profiles do not work at same company', () => {
            profile.work_experiences[0] = {
                professional_organization: {
                    id: 123,
                    text: 'Acme Co.',
                },
                featured: true,
            };
            const comparison = {
                featuredWorkExperience: {
                    professional_organization: {
                        id: 456,
                        text: 'Other Co.',
                    },
                },
            };

            expect(profile.hasSameCompany(comparison)).toBe(false);
        });
    });

    describe('hasSameRole', () => {
        beforeEach(() => {
            profile.addDummyWorkExperienceIfNecessary();
        });

        it('should return true when both profiles have same role', () => {
            profile.work_experiences[0] = {
                role: 'general_management',
                featured: true,
            };
            const comparison = {
                featuredWorkExperience: {
                    role: 'general_management',
                },
            };

            expect(profile.hasSameRole(comparison)).toBe(true);
        });

        it('should return false when profiles do not have the same role', () => {
            profile.work_experiences[0] = {
                role: 'general_management',
                featured: true,
            };
            const comparison = {
                featuredWorkExperience: {
                    role: 'other',
                },
            };

            expect(profile.hasSameRole(comparison)).toBe(false);
        });
    });

    describe('hasSharedSchools', () => {
        beforeEach(() => {
            profile.education_experiences = [];
            profile.addDummyEducationExperienceIfNecessary();
        });

        it('should return true when both profiles have same school', () => {
            profile.education_experiences[0] = {
                educational_organization: {
                    id: 123,
                    text: 'Acme University',
                },
            };
            const comparison = {
                education_experiences: [
                    {
                        educational_organization: {
                            id: 123,
                            text: 'Acme University',
                        },
                    },
                ],
            };

            expect(profile.hasSharedSchools(comparison)).toBe(true);
        });

        it('should return false when profiles do not have the same school', () => {
            profile.education_experiences[0] = {
                educational_organization: {
                    id: 123,
                    text: 'Acme University',
                },
            };
            const comparison = {
                education_experiences: [
                    {
                        educational_organization: {
                            id: 456,
                            text: 'Other University',
                        },
                    },
                ],
            };

            expect(profile.hasSharedSchools(comparison)).toBe(false);
        });
    });

    describe('fullTimeWorkExperiences', () => {
        it('should work', () => {
            const guid = $injector.get('guid');

            const workExperienceAttrs = [
                // featured first
                {
                    id: guid.generate(),
                    start_date: new Date('2007/01/01').getTime() / 1000,
                    featured: true,
                    end_date: null,
                    employment_type: 'full_time',
                    professional_organization: {
                        text: 'Foo Inc.',
                    },
                },

                // most recently started next
                {
                    id: guid.generate(),
                    start_date: new Date('2018/01/01').getTime() / 1000,
                    featured: false,
                    end_date: null,
                    employment_type: 'full_time',
                    professional_organization: {
                        text: 'Foo Inc.',
                    },
                },

                // less recently started last
                {
                    id: guid.generate(),
                    start_date: new Date('2017/01/01').getTime() / 1000,
                    featured: false,
                    end_date: null,
                    employment_type: 'full_time',
                    professional_organization: {
                        text: 'Foo Inc.',
                    },
                },

                // part_time filtered out
                {
                    id: guid.generate(),
                    start_date: new Date('2017/01/01').getTime() / 1000,
                    featured: false,
                    end_date: null,
                    employment_type: 'part_time',
                    professional_organization: {
                        text: 'Foo Inc.',
                    },
                },
            ];

            const careerProfile = CareerProfile.fixtures.getInstance({
                work_experiences: _.clone(workExperienceAttrs).reverse(),
            });

            const expectedIds = _.pluck(workExperienceAttrs.slice(0, 3), 'id');
            const actualIds = _.pluck(careerProfile.fullTimeWorkExperiences, 'id');
            expect(actualIds).toEqual(expectedIds);
        });
    });

    describe('indicatesUserShouldUploadEnglishLanguageProficiencyDocuments', () => {
        it('should be false when native_english_speaker is true', () => {
            profile.native_english_speaker = true;
            expect(profile.indicatesUserShouldUploadEnglishLanguageProficiencyDocuments).toEqual(false);
        });

        it('should be false when native_english_speaker is unknown', () => {
            profile.native_english_speaker = null;
            expect(profile.indicatesUserShouldUploadEnglishLanguageProficiencyDocuments).toEqual(false);
        });

        describe('when native_english_speaker is false', () => {
            beforeEach(() => {
                profile.native_english_speaker = false;
            });

            it('should be false when earned_accredited_degree_in_english is true', () => {
                profile.earned_accredited_degree_in_english = true;
                expect(profile.indicatesUserShouldUploadEnglishLanguageProficiencyDocuments).toEqual(false);
            });

            it('should be false when earned_accredited_degree_in_english is unknown', () => {
                profile.earned_accredited_degree_in_english = null;
                expect(profile.indicatesUserShouldUploadEnglishLanguageProficiencyDocuments).toEqual(false);
            });

            describe('when earned_accredited_degree_in_english is false', () => {
                beforeEach(() => {
                    profile.earned_accredited_degree_in_english = false;
                });

                it('should be false when sufficient_english_work_experience is true', () => {
                    profile.sufficient_english_work_experience = true;
                    expect(profile.indicatesUserShouldUploadEnglishLanguageProficiencyDocuments).toEqual(false);
                });

                it('should be true when sufficient_english_work_experience is false', () => {
                    profile.sufficient_english_work_experience = false;
                    expect(profile.indicatesUserShouldUploadEnglishLanguageProficiencyDocuments).toEqual(true);
                });

                it('should be true when sufficient_english_work_experience is unknown', () => {
                    profile.sufficient_english_work_experience = null;
                    expect(profile.indicatesUserShouldUploadEnglishLanguageProficiencyDocuments).toEqual(true);
                });
            });
        });
    });

    describe('program_type', () => {
        beforeEach(() => {
            jest.spyOn(ErrorLogService, 'notifyInProd').mockImplementation(() => {});
        });

        it('should call ErrorLogService.notifyInProd', () => {
            jest.spyOn(profile, 'program_type', 'get').mockReturnValue('emba');
            jest.spyOn(ClientStorage, 'getItem').mockReturnValue(true);
            profile.program_type = 'foobar';
            expect(ErrorLogService.notifyInProd).toHaveBeenCalledWith(
                'Unexpected program_type change after converted_to_emba on career_profile',
                undefined,
                {
                    level: 'warn',
                    career_profile_id: profile.id,
                },
            );
        });

        it('should not call ErrorLogService.notifyInProd if not already EMBA', () => {
            jest.spyOn(profile, 'program_type', 'get').mockReturnValue('mba');
            profile.program_type = 'foobar';
            expect(ErrorLogService.notifyInProd).not.toHaveBeenCalled();
        });

        it('should not call ErrorLogService.notifyInProd if not converted_to_emba', () => {
            jest.spyOn(profile, 'program_type', 'get').mockReturnValue('emba');
            jest.spyOn(ClientStorage, 'getItem').mockReturnValue(false);
            profile.program_type = 'foobar';
            expect(ErrorLogService.notifyInProd).not.toHaveBeenCalled();
        });
    });

    describe('missingTranscripts', () => {
        let educationExperiences;

        beforeEach(() => {
            educationExperiences = [EducationExperience.new(), EducationExperience.new(), EducationExperience.new()];
        });

        it('should be true if not all education experiences requiring transcript have one or a waiver reason', () => {
            jest.spyOn(educationExperiences[0], 'transcriptUploadedOrWaived', 'get').mockReturnValue(true);
            jest.spyOn(educationExperiences[1], 'transcriptUploadedOrWaived', 'get').mockReturnValue(true);
            jest.spyOn(educationExperiences[2], 'transcriptUploadedOrWaived', 'get').mockReturnValue(false);
            jest.spyOn(profile, 'educationExperiencesIndicatingTranscriptRequired', 'get').mockReturnValue(
                educationExperiences,
            );
            expect(profile.missingTranscripts).toBe(true);
        });

        it('should be false if no education expriences require a transcript', () => {
            jest.spyOn(educationExperiences[0], 'transcriptUploadedOrWaived', 'get').mockReturnValue(true);
            jest.spyOn(educationExperiences[1], 'transcriptUploadedOrWaived', 'get').mockReturnValue(true);
            jest.spyOn(educationExperiences[2], 'transcriptUploadedOrWaived', 'get').mockReturnValue(true);
            jest.spyOn(profile, 'educationExperiencesIndicatingTranscriptRequired', 'get').mockReturnValue(
                educationExperiences,
            );
            expect(profile.missingTranscripts).toBe(false);
        });
    });

    describe('numRequiredTranscripts', () => {
        let educationExperiences;

        beforeEach(() => {
            educationExperiences = [EducationExperience.new(), EducationExperience.new(), EducationExperience.new()];
        });

        it('should return number of required transcripts for numRequiredTranscripts', () => {
            educationExperiences.forEach(e => {
                jest.spyOn(e, 'transcriptRequired', 'get').mockReturnValue(true);
            });
            jest.spyOn(profile, 'educationExperiencesIndicatingTranscriptRequired', 'get').mockReturnValue([
                EducationExperience.new(),
                EducationExperience.new(),
                EducationExperience.new(),
            ]);
            expect(profile.numRequiredTranscripts).toBe(3);
        });

        it('should return number of required transcripts for numRequiredTranscriptsUploaded', () => {
            jest.spyOn(educationExperiences[0], 'transcriptUploaded', 'get').mockReturnValue(true);
            jest.spyOn(educationExperiences[1], 'transcriptUploaded', 'get').mockReturnValue(false);
            jest.spyOn(educationExperiences[2], 'transcriptUploaded', 'get').mockReturnValue(false);
            jest.spyOn(profile, 'educationExperiencesIndicatingTranscriptRequired', 'get').mockReturnValue(
                educationExperiences,
            );
            expect(profile.numRequiredTranscriptsUploaded).toBe(1);
        });

        it('should return number of required transcripts for numRequiredTranscriptsApproved', () => {
            jest.spyOn(profile, 'educationExperiencesIndicatingTranscriptRequired', 'get').mockReturnValue(
                educationExperiences,
            );
            educationExperiences[0].transcript_approved = true;
            expect(profile.numRequiredTranscriptsApproved).toBe(1);
        });

        it('should return number of required transcripts for numRequiredTranscriptsWaived', () => {
            educationExperiences[0].transcript_waiver = 'test waiver';
            jest.spyOn(profile, 'educationExperiencesIndicatingTranscriptRequired', 'get').mockReturnValue(
                educationExperiences,
            );
            expect(profile.numRequiredTranscriptsWaived).toBe(1);
        });

        it('should return number of required transcripts for numRequiredTranscriptsNotWaived', () => {
            jest.spyOn(profile, 'educationExperiencesIndicatingTranscriptRequired', 'get').mockReturnValue(
                educationExperiences,
            );
            educationExperiences[0].transcript_waiver = null;
            educationExperiences[1].transcript_waiver = 'test waiver';
            educationExperiences[2].transcript_waiver = 'test waiver';

            jest.spyOn(profile, 'educationExperiencesIndicatingTranscriptRequired', 'get').mockReturnValue(
                educationExperiences,
            );
            expect(profile.numRequiredTranscriptsNotWaived).toBe(1);
        });
    });

    describe('educationExperiencesIndicatingTranscriptRequired', () => {
        it('should work', () => {
            profile.education_experiences = [
                EducationExperience.new({
                    id: 1,
                    graduation_year: 2010,
                    will_not_complete: false,
                    degree_program: true,
                }),
                EducationExperience.new({
                    id: 2,
                    graduation_year: 2010,
                    will_not_complete: true,
                }),
                EducationExperience.new({
                    id: 3,
                    graduation_year: new Date().getFullYear(),
                    will_not_complete: false,
                    degree_program: true,
                }),
                EducationExperience.new({
                    id: 4,
                    graduation_year: new Date().getFullYear() + 1,
                    will_not_complete: false,
                }),
            ];

            expect(profile.educationExperiencesIndicatingTranscriptRequired.map(e => e.id)).toEqual([1, 3]);
        });
    });

    describe('currentSalaryRange', () => {
        it('should return lower and upper bound of salary_range', () => {
            profile.salary_range = '90000_to_99999';
            expect(profile.currentSalaryRange.lower).toBe(90000);
            expect(profile.currentSalaryRange.upper).toBe(99999);
        });

        it('should set bounds to null if not parseable', () => {
            profile.salary_range = 'foo_to_bar';
            expect(profile.currentSalaryRange.lower).toBe(null);
            expect(profile.currentSalaryRange.upper).toBe(null);
        });

        it('should return null if prefer_not_to_disclose', () => {
            profile.salary_range = 'prefer_not_to_disclose';
            expect(profile.currentSalaryRange).toBe(null);
        });

        it('should return null if no salary_range', () => {
            profile.salary_range = undefined;
            expect(profile.currentSalaryRange).toBe(null);
        });
    });
});
