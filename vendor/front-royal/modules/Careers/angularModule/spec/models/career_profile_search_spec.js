import 'AngularSpecHelper';
import 'Careers/angularModule';

describe('FrontRoyal.Careers.CareerProfileSearch', () => {
    let $injector;
    let CareerProfileSearch;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                CareerProfileSearch = $injector.get('CareerProfileSearch');
            },
        ]);
    });

    describe('cloneFilters', () => {
        it('should work', () => {
            const filters = {
                places: ['a'],
                only_local: true,
                preferred_primary_areas_of_interest: ['a'],
                industries: ['a'],
                years_experience: ['a'],
                keyword_search: 'a',
                skills: ['a', 'b', 'c'],
                employment_types_of_interest: ['a'],
                levels_of_interest: ['a', 'b'],
                company_name: 'a',
                school_name: 'a',
                in_school: true,
            };
            const search = CareerProfileSearch.new(filters);

            const cloned = search.cloneFilters();
            expect(cloned).toEqual(filters);

            // ensure I can change the clone without messing up the instance
            cloned.places.push('b');
            cloned.only_local = false;
            cloned.preferred_primary_areas_of_interest.push('b');
            cloned.industries.push('b');
            cloned.years_experience.push('b');
            cloned.keyword_search = 'b';
            cloned.skills = ['d'];
            cloned.employment_types_of_interest.push('b');
            cloned.levels_of_interest.push('c');
            cloned.company_name = 'b';
            cloned.school_name = 'b';
            cloned.in_school = false;

            expect(
                _.pick(
                    search.asJson(),
                    'places',
                    'only_local',
                    'preferred_primary_areas_of_interest',
                    'industries',
                    'years_experience',
                    'keyword_search',
                    'skills',
                    'employment_types_of_interest',
                    'levels_of_interest',
                    'company_name',
                    'school_name',
                    'in_school',
                ),
            ).toEqual(filters);
        });
    });
});
