import 'AngularSpecHelper';
import 'Careers/angularModule';
import studentDashboardEnrollmentLocales from 'Lessons/locales/lessons/stream/student_dashboard_enrollment-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(studentDashboardEnrollmentLocales);

describe('FrontRoyal.Careers.EducationExperience', () => {
    let $injector;
    let EducationExperience;
    let S3TranscriptAsset;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                $injector.get('MockIguana');
                EducationExperience = $injector.get('EducationExperience');
                S3TranscriptAsset = $injector.get('S3TranscriptAsset');
            },
        ]);
    });

    it('should set $$transcript_waived if transcript_waived', () => {
        const experience = EducationExperience.new({
            transcript_waiver: 'test waiver',
        });
        expect(experience.$$transcript_waived).toBe(true);
    });

    describe('transcriptRequired', () => {
        it('should work', () => {
            const experience = EducationExperience.new({
                degree_program: false,
                will_not_complete: true,
                graduation_year: new Date().getFullYear() + 1,
            });
            expect(experience.transcriptRequired).toBe(false);

            experience.degree_program = true;
            expect(experience.transcriptRequired).toBe(false);

            experience.will_not_complete = false;
            expect(experience.transcriptRequired).toBe(false);

            experience.graduation_year = new Date().getFullYear();
            expect(experience.transcriptRequired).toBe(true);

            experience.graduation_year = new Date().getFullYear() - 1;
            expect(experience.transcriptRequired).toBe(true);
        });
    });

    describe('transcriptUploaded', () => {
        it('should be true if there is an embedded transcript', () => {
            const experience = EducationExperience.new({
                transcripts: [S3TranscriptAsset.new()],
            });
            expect(experience.transcriptUploaded).toBe(true);
        });

        it('should be false when no transcripts', () => {
            const experience = EducationExperience.new({
                transcripts: null,
            });
            expect(experience.transcriptUploaded).toBe(false);
        });
    });

    describe('transcriptInReview', () => {
        it('should be true if transcript is uploaded but not approved', () => {
            const experience = EducationExperience.new({
                transcript_approved: false,
            });
            jest.spyOn(experience, 'transcriptUploaded', 'get').mockReturnValue(true);
            expect(experience.transcriptInReview).toBe(true);
        });

        it('should be false if transcript is approved approved', () => {
            const experience = EducationExperience.new({
                transcript_approved: true,
            });
            jest.spyOn(experience, 'transcriptUploaded', 'get').mockReturnValue(true);
            expect(experience.transcriptInReview).toBe(false);
        });
    });

    describe('degreeAndOrgNameWithTranscriptRequirementString', () => {
        let experience;

        beforeEach(() => {
            experience = EducationExperience.new();
            jest.spyOn(experience, 'degreeAndOrgNameString', 'get').mockReturnValue('Some degree and org');
        });

        it('should reflect that an official transcript is required', () => {
            experience.official_transcript_required = true;
            expect(experience.degreeAndOrgNameWithTranscriptRequirementString).toEqual(
                'Some degree and org (Official)',
            );
        });

        it('should reflect that an official transcript is not required', () => {
            expect(experience.degreeAndOrgNameWithTranscriptRequirementString).toEqual(
                'Some degree and org (Unofficial)',
            );
        });
    });
});
