import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';
import positionFormLocales from 'Careers/locales/careers/position_form-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import chooseARoleLocales from 'Careers/locales/careers/choose_a_role-en.json';
import selectSkillsFormLocales from 'Careers/locales/careers/edit_career_profile/select_skills_form-en.json';
import positionCardLocales from 'Careers/locales/careers/position_card-en.json';
import writeTextAboutLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/write_text_about-en.json';
import salaryLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/salary-en.json';
import selectedPillsLocales from 'FrontRoyalForm/locales/front_royal_form/selected_pills-en.json';
import inputConstAutosuggestLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/input_const_autosuggest-en.json';
import setSpecLocales from 'Translation/setSpecLocales';
import moment from 'moment-timezone';

setSpecLocales(
    positionFormLocales,
    fieldOptionsLocales,
    chooseARoleLocales,
    selectSkillsFormLocales,
    positionCardLocales,
    writeTextAboutLocales,
    salaryLocales,
    selectedPillsLocales,
    inputConstAutosuggestLocales,
);

describe('FrontRoyal.Careers.PositionForm.AddPositionForm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let OpenPosition;
    let hiringManager;
    let position;
    let HiringTeamCheckoutHelper;
    let HiringTeam;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.module($provide => {
            HiringTeamCheckoutHelper = function () {};
            $provide.value('Payments.HiringTeamCheckoutHelper', HiringTeamCheckoutHelper);
        });

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                OpenPosition = $injector.get('OpenPosition');
                HiringTeam = $injector.get('HiringTeam');
                $injector.get('HiringTeamFixtures');
            },
        ]);

        hiringManager = SpecHelper.stubCurrentUser();
        hiringManager.hiring_team = HiringTeam.fixtures.getInstance();
        position = OpenPosition.new({
            id: '1',
            hiring_manager_id: hiringManager.id,
            title: 'open position title',
            desired_years_experience: {},
            role: 'marketing',
            location: 'new_york',
            position_descriptors: ['permanent'],
        });

        SpecHelper.stubDirective('locationAutocomplete');
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.position = position;
        renderer.scope.hiringManager = hiringManager;
        renderer.scope.steps = [
            {
                stepName: 'add_position',
            },
        ];
        renderer.render(
            '<position-form position="position" steps="steps" hiring-manager="hiringManager"></position-form>',
        );
        elem = renderer.elem;
        scope = elem.find('add-position-form').scope();
        scope.$digest();
    }

    function assertYearsMax(min) {
        // select a new minimum
        SpecHelper.updateSelect(elem, '[name=years_min]', min);

        // create an array starting with an empty value and concat an array of numbers
        const options = [''].concat(_.range(min + 1, 11).map(year => `number:${year}`));
        SpecHelper.assertSelectOptions(elem, '[name=years_max]', options);
    }

    it('should adjust years_max when years_min changes', () => {
        render();

        assertYearsMax(0); // starts out with 1-10
        assertYearsMax(5); // will change to 6-10
        assertYearsMax(1); // will add back removed values, so 2-10
        assertYearsMax(10); // will empty out years max to just No maximum/''
    });

    describe('Will Sponsor Visa field', () => {
        it('should have a required radio field', () => {
            render();
            SpecHelper.expectHasClass(elem, '.will-sponsor-visa label:eq(0)', 'required');
            SpecHelper.expectElementText(elem, '.will-sponsor-visa label', 'Will Sponsor Visa', 0);
            SpecHelper.toggleRadio(elem, '[name="will_sponsor_visa"]', 1);
            expect(position.will_sponsor_visa).toEqual(false);
            SpecHelper.toggleRadio(elem, '[name="will_sponsor_visa"]', 0);
            expect(position.will_sponsor_visa).toEqual(true);
        });
    });

    describe('Short Description field', () => {
        it('should have a required write-text-about field with detailed instructions and a placeholder', () => {
            render();
            SpecHelper.expectHasClass(elem, '.short_description label', 'required');
            SpecHelper.expectElementText(
                elem,
                '.short_description p[translate-once="careers.position_form.short_description_caption"]',
                'We recommend one to two sentences that succinctly describe the opportunity.',
            );
            SpecHelper.expectElementPlaceholder(
                elem,
                '.short_description write-text-about[ng-model="position.short_description"]',
                "'careers.position_form.short_description_placeholder' | translate",
            );
        });
    });

    describe('Full Description field', () => {
        it('should have a write-text-about field with detailed instructions and a placeholder', () => {
            render();
            SpecHelper.expectElementText(
                elem,
                '.full_description p[translate-once="careers.position_form.full_description_caption"]',
                'This optional description is shown if a candidate chooses to view more detail.',
            );
            SpecHelper.expectElementPlaceholder(
                elem,
                '.full_description write-text-about[ng-model="position.description"]',
                "'careers.position_form.full_description_placeholder' | translate",
            );
        });
    });

    describe('Auto-Expiration Date field', () => {
        it('should preset auto_expiration_date to 60 days in the future if no auto_expiration_date is present', () => {
            position.auto_expiration_date = null;
            render();

            expect(position.auto_expiration_date).toEqual(moment().add(60, 'days').format('YYYY-MM-DD'));
        });

        it('should not preset auto_expiration_date for archived position', () => {
            position.auto_expiration_date = null;
            position.archived = true;
            render();
            expect(position.auto_expiration_date).toBe(null);
        });

        it('should not preset auto_expiration_date for pay-per-post users', () => {
            jest.spyOn(hiringManager, 'usingPayPerPostHiringPlan', 'get').mockReturnValue(true);
            position.auto_expiration_date = null;
            render();

            expect(position.auto_expiration_date).toBe(null);
        });

        it('should show formatted preview of auto-expiration date', () => {
            position.auto_expiration_date = '2018-10-02';
            render();

            SpecHelper.expectElementText(elem, '.expiration-preview', 'October 2, 2018');

            // when invalid
            scope.autoExpirationDateViewValue = moment().format('YYYYMMDD');
            scope.$digest();
            SpecHelper.expectElementText(elem, '.expiration-preview', moment().format('MMMM D, YYYY'));
        });

        it('should show validation error message if autoExpirationDateViewValue is not between 1 and 120 days from today', () => {
            render();

            // today (invalid)
            scope.autoExpirationDateViewValue = moment().format('YYYYMMDD');
            scope.$digest();
            expect(scope.showInvalidAutoExpirationDate).toBe(true);
            SpecHelper.expectElementText(
                elem,
                '.expiration-date-validation-error',
                'Date must be between 1 and 120 days from today.',
            );

            // tomorrow (valid)
            scope.autoExpirationDateViewValue = moment().add(1, 'day').format('YYYYMMDD');
            scope.$digest();
            expect(scope.showInvalidAutoExpirationDate).toBe(false);
            SpecHelper.expectNoElement(elem, '.expiration-date-validation-error');

            // 121 days from today (invalid)
            scope.autoExpirationDateViewValue = moment().add(121, 'days').format('YYYYMMDD');
            scope.$digest();
            expect(scope.showInvalidAutoExpirationDate).toBe(true);
            SpecHelper.expectElementText(
                elem,
                '.expiration-date-validation-error',
                'Date must be between 1 and 120 days from today.',
            );

            // 120 days from today (valid)
            scope.autoExpirationDateViewValue = moment().add(120, 'days').format('YYYYMMDD');
            scope.$digest();
            expect(scope.showInvalidAutoExpirationDate).toBe(false);
            SpecHelper.expectNoElement(elem, '.expiration-date-validation-error');
        });
    });
});
