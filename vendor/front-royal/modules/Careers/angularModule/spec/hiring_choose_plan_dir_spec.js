import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';
import setSpecLocales from 'Translation/setSpecLocales';
import hiringChoosePlanLocales from 'Careers/locales/careers/hiring_choose_plan-en.json';

setSpecLocales(hiringChoosePlanLocales);

describe('FrontRoyal.Careers.hiringChoosePlan', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let $rootScope;
    let HiringTeam;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');

                $rootScope = $injector.get('$rootScope');
                HiringTeam = $injector.get('HiringTeam');
                $injector.get('HiringTeamFixtures');
            },
        ]);

        const currentUser = SpecHelper.stubCurrentUser();
        currentUser.hiring_team = HiringTeam.fixtures.getInstance({
            hiring_plan: null,
        });
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<hiring-choose-plan></hiring-choose-plan>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('selectPlan', () => {
        it('should forward to positions page when selecting pay-per-post', () => {
            render();
            jest.spyOn(scope, 'loadRoute');
            SpecHelper.click(elem, 'button', 0);
            expect(scope.loadRoute).toHaveBeenCalledWith('hiring/positions/?action=create');
        });

        it('save the the hiring plan on the hiring team before forwarding the user back home', () => {
            render();
            const hiringTeam = ($rootScope.currentUser.hiring_team = HiringTeam.fixtures.getInstance());
            jest.spyOn(scope, 'loadRoute');
            HiringTeam.expect('update');
            jest.spyOn(hiringTeam, 'stripePlanForHiringPlan').mockReturnValue({
                amount: 1337,
            });

            HiringTeam.expect('update').toBeCalledWith(
                Object.assign(hiringTeam, {
                    hiring_plan: HiringTeam.HIRING_PLAN_UNLIMITED_WITH_SOURCING,
                }),
            );
            expect(scope.showPlans).toBe(true);
            SpecHelper.click(elem, 'button', 1);

            SpecHelper.expectElementDisabled(elem, 'button:eq(1)');
            SpecHelper.expectElement(elem, 'button:eq(1) > front-royal-spinner');
            jest.spyOn($rootScope, 'goHome').mockImplementation();
            HiringTeam.flush('update');

            expect(scope.showPlans).toBe(false);
            expect($rootScope.currentUser.hiring_team.hiring_plan).toEqual(
                HiringTeam.HIRING_PLAN_UNLIMITED_WITH_SOURCING,
            );
            SpecHelper.expectElement(elem, '.program-applied');
            SpecHelper.expectNoElement(elem, '.program-choice-featured-container');

            // Note that we'll still be at /hiring/plan unless the hiring team
            // was accepted before choosing a plan
            expect($rootScope.goHome).toHaveBeenCalled();
            SpecHelper.expectElementText(elem, '.program-subtitle span:eq(1)', '$13.37/month');
        });
    });
});
