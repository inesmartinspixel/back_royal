import 'AngularSpecHelper';
import 'Careers/angularModule';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(editCareerProfileLocales);

describe('FrontRoyal.Careers.ProgressNav', () => {
    let $injector;
    let renderer;
    let elem;
    let SpecHelper;
    let steps;
    let onClick;
    let currentStepName;
    let stepsProgressMap;
    let submitStep;
    let percentComplete;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);

        steps = [
            {
                stepName: 'education',
            },
            {
                stepName: 'work',
            },
            {
                stepName: 'basic_info',
            },
        ];

        onClick = jest.fn();

        currentStepName = 'education';

        stepsProgressMap = {
            education: 'incomplete',
            work: 'incomplete',
            basic_info: 'incomplete',
        };

        submitStep = 'basic_info';

        percentComplete = 99;
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.steps = steps;
        renderer.scope.onClick = onClick;
        renderer.scope.currentStepName = currentStepName;
        renderer.scope.stepsProgressMap = stepsProgressMap;
        renderer.scope.percentComplete = angular.isDefined(opts.percentComplete)
            ? opts.percentComplete
            : percentComplete;
        renderer.render(
            `<progress-nav steps="steps" on-click="onClick" current-step-name="currentStepName" steps-progress-map="stepsProgressMap" submit-step="${submitStep}" percent-complete="percentComplete" locale-file="edit_career_profile"></progress-nav>`,
        );

        elem = renderer.elem;
    }

    it('should render a dot for every step exept for the submitStep', () => {
        render();
        // the submit step should be rendered twice: once as a hidden step, and once as the submit step
        SpecHelper.expectElements(elem, 'li:not(.percentage):not(.hide)', 2);
    });

    it('should add selected class to current step', () => {
        render();
        SpecHelper.expectElementHasClass(elem.find('li:eq(0)'), 'selected');
    });

    it('should render all steps with the incomplete class', () => {
        render();
        SpecHelper.expectElements(elem, 'li.incomplete', steps.length + 1); // + 1 because the submit step renders twice
    });

    it('should render submit step with percentage inside', () => {
        render();
        SpecHelper.expectElementText(elem, '.percentage .circle', '99%');
    });

    it('should say 0% in submit step if no percentComplete', () => {
        render({
            percentComplete: null,
        });
        SpecHelper.expectElementText(elem, '.percentage .circle', '0%');
    });

    it('should fire onClick function', () => {
        render();
        SpecHelper.click(elem, 'li:eq(0)');
        expect(onClick).toHaveBeenCalledWith(0);
    });
});
