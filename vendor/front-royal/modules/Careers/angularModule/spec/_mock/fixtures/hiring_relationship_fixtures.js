import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_application_fixtures';

angular.module('FrontRoyal.Careers').factory('HiringRelationshipFixtures', [
    '$injector',
    $injector => {
        const HiringRelationship = $injector.get('HiringRelationship');
        const guid = $injector.get('guid');
        const CareerProfile = $injector.get('CareerProfile');
        $injector.get('CareerProfileFixtures');
        const HiringApplication = $injector.get('HiringApplication');
        $injector.get('HiringApplicationFixtures');

        HiringRelationship.fixtures = {
            sampleAttrs(overrides = {}) {
                const attrs = angular.extend(
                    {
                        id: guid.generate(),
                        created_at: 123,
                        updated_at: 234,
                        candidate_id: guid.generate(),
                        hiring_manager_id: guid.generate(),
                        candidate_status: 'pending',
                        hiring_manager_status: 'accepted',
                        candidate_display_name: 'candidate@pedago.com',
                        hiring_manager_display_name: 'Johnny Appleseed',
                        hiring_manager_email: 'hiringmanager@pedago.com',
                        hiring_manager_priority: 100,
                        hiring_manager_closed: false,
                        hiring_manager_closed_info: {},
                        candidate_closed: false,
                    },
                    overrides,
                );

                // FIXME? now that this stuff is availabel in the career profile,
                // should we remove it from hiring_relationship json?
                attrs.career_profile = CareerProfile.fixtures.sampleAttrs(
                    angular.extend(
                        {
                            name: attrs.candidate_display_name,
                            user_id: attrs.candidate_id,
                            email: attrs.candidate_email,
                        },
                        overrides.career_profile,
                    ),
                );

                if (attrs.candidate_display_name !== attrs.career_profile.name) {
                    throw new Error('candidate_display_name does not match career_profile.name');
                }

                if (attrs.candidate_email !== attrs.career_profile.email) {
                    throw new Error('candidate_email does not match career_profile.email');
                }

                attrs.hiring_application = HiringApplication.fixtures.sampleAttrs(
                    angular.extend(
                        {
                            name: attrs.hiring_manager_display_name,
                            user_id: attrs.hiring_manager_id,
                            email: attrs.hiring_manager_email,
                        },
                        overrides.hiring_application,
                    ),
                );

                if (attrs.hiring_manager_display_name !== attrs.hiring_application.name) {
                    throw new Error('hiring_manager_display_name does not match hiring_application.name');
                }
                if (attrs.hiring_manager_email !== attrs.hiring_application.email) {
                    throw new Error('hiring_manager_email does not match hiring_application.email');
                }

                return attrs;
            },

            getInstance(overrides) {
                return HiringRelationship.new(this.sampleAttrs(overrides));
            },
        };

        return {};
    },
]);
