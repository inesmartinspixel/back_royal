import 'Careers/angularModule';

angular.module('FrontRoyal.Careers').factory('CareerProfileFixtures', [
    '$injector',
    $injector => {
        const CareerProfile = $injector.get('CareerProfile');
        const WorkExperience = $injector.get('WorkExperience');
        const EducationExperience = $injector.get('EducationExperience');
        const guid = $injector.get('guid');

        CareerProfile.fixtures = {
            sampleAttrs(overrides = {}) {
                overrides.id = overrides.id || String(Math.random());

                const now = new Date().getTime();
                const mockWorks = [
                    WorkExperience.new({
                        professional_organization: {
                            text: 'Foo Inc.',
                        },
                        industry: 'education',
                        role: 'graphic_design',
                        job_title: 'Foo Handler',
                        start_date: now + 1000, // rails will send us back a timestamp in seconds
                        end_date: now + 1000, // rails will send us back a timestamp in seconds
                        featured: true,
                        responsibilities: ['foo', 'bar', 'baz'],
                        employment_type: 'full_time',
                    }),
                    WorkExperience.new({
                        professional_organization: {
                            text: 'Bar LLC',
                        },
                        industry: 'consumer_products',
                        role: 'devops',
                        job_title: 'Bar Maintainer',
                        start_date: now, // rails will send us back a timestamp in seconds
                        end_date: now, // rails will send us back a timestamp in seconds
                        featured: false,
                        responsibilities: ['res1', 'res2'],
                        employment_type: 'part_time',
                    }),
                ];

                const mockEducations = [
                    EducationExperience.new({
                        id: guid.generate(),
                        educational_organization: {
                            text: 'Bar State University',
                        },
                        graduation_year: 2010,
                        degree: 'Doctorate in Bar',
                        major: 'Bar Science',
                        minor: 'Handling Bar',
                        gpa: '4.0',
                        degree_program: true,
                        will_not_complete: false,
                    }),
                    EducationExperience.new({
                        id: guid.generate(),
                        educational_organization: {
                            text: 'University of Foo',
                        },
                        graduation_year: 1999,
                        degree: 'Bachelor of Foo',
                        major: 'Foo Science',
                        minor: 'Bar Engineering',
                        gpa: '3.5',
                        degree_program: true,
                        will_not_complete: false,
                    }),
                ];

                return angular.extend(
                    {
                        user_id: guid.generate(),

                        // basic info
                        name: `User ${guid.generate()}`,
                        do_not_create_relationships: false,
                        last_calculated_complete_percentage: 100,
                        address_line_1: '100 Timbuktoo Lane',
                        address_line_2: 'Apt 1337',
                        city: 'Jackson',
                        state: 'Mississippi',
                        country: 'US',
                        zip: '12345',
                        place_id: 'ChIJOwg_06VPwokRYv534QaPC8g',
                        place_details: {
                            formatted_address: 'Seattle, WA',
                        },

                        // work and education experience
                        work_experiences: mockWorks,
                        education_experiences: mockEducations,
                        has_no_formal_education: false,
                        industry: 'consulting',

                        // test scores
                        score_on_sat: null,
                        sat_max_score: '1600',
                        score_on_gmat: null,
                        score_on_act: null,
                        short_answers: {},

                        // job preferences
                        willing_to_relocate: false,
                        open_to_remote_work: true,
                        authorized_to_work_in_us: 'f1visa',
                        job_sectors_of_interest: ['consulting', 'financial', 'technology'],
                        employment_types_of_interest: ['internship', 'part_time', 'contract', 'permanent'],
                        company_sizes_of_interest: ['25-99', '100-499', '500_plus', '500-4999', '5000_plus'],
                        interested_in_joining_new_company: 'very_interested', // marks the user as having an active profile
                        locations_of_interest: ['none'],
                        salary_range: 'prefer_not_to_disclose',
                        preferred_company_culture_descriptors: ['results_oriented', 'stable', 'merit_based'],
                        primary_areas_of_interest: ['finance', 'general_management', 'sales'],

                        // about your mba
                        short_answer_greatest_achievement: 'asdasd',
                        short_answer_why_pursuing: 'asdasd',
                        short_answer_use_skills_to_advance: 'asdasd',
                        short_answer_ask_professional_advice: 'asdasd',
                        primary_reason_for_applying: 'expand_business_knowledge',
                        top_mba_subjects: ['finance_and_accounting', 'strategy_and_innovation', 'entrepreneurship'],

                        // more about you
                        top_personal_descriptors: ['creative', 'driven', 'meticulous'],
                        top_motivations: ['personal_growth', 'career_advancement', 'helping_others'],
                        top_workplace_strengths: ['business_acumen', 'problem_solving', 'communication'],

                        // skills
                        skills: [
                            {
                                id: '472c76aa-6fe0-4dc3-aa2e-39250fdba0fa',
                                created_at: '2016-09-26T22:46:28.748Z',
                                updated_at: '2016-09-26T22:46:28.748Z',
                                type: 'skill',
                                text: 'Software Engineering',
                                locale: 'en',
                                suggest: true,
                            },
                            {
                                id: 'f66aa3fd-5b4b-497c-a34e-4017ae3527b0',
                                created_at: '2016-09-26T22:46:28.748Z',
                                updated_at: '2016-09-26T22:46:28.748Z',
                                type: 'skill',
                                text: 'Ingeniería de software',
                                locale: 'es',
                                suggest: true,
                            },
                            {
                                id: '54b63a35-4c91-4032-9d99-688a34e1bb37',
                                created_at: '2016-09-26T22:46:28.748Z',
                                updated_at: '2016-09-26T22:46:28.748Z',
                                type: 'skill',
                                text: '软件工程',
                                locale: 'zh',
                                suggest: true,
                            },
                            {
                                id: '67198e13-bcda-4b19-aaaa-737dd5717853',
                                created_at: '2016-09-26T22:46:28.748Z',
                                updated_at: '2016-09-26T22:46:28.748Z',
                                type: 'skill',
                                text: 'Management',
                                locale: 'en',
                                suggest: true,
                            },
                        ],
                        awards_and_interests: [
                            {
                                id: 'ec18b747-97cb-4d79-abb1-7b42e567d5cf',
                                created_at: '2016-09-30T22:35:48.818Z',
                                updated_at: '2016-09-30T22:35:48.818Z',
                                type: 'awards_and_interest',
                                text: 'Soccer',
                                locale: 'en',
                                suggest: false,
                            },
                            {
                                id: 'acd8871f-ae7e-4f24-acb6-47c2dfc36b86',
                                created_at: '2016-10-01T01:16:08.252Z',
                                updated_at: '2016-10-01T01:16:08.252Z',
                                type: 'awards_and_interest',
                                text: 'Crossfit',
                                locale: 'en',
                                suggest: false,
                            },
                            {
                                id: '5fcc1bcb-8b1d-4de3-92e5-db45a184fa2e',
                                created_at: '2016-10-01T01:16:08.269Z',
                                updated_at: '2016-10-01T01:16:08.269Z',
                                type: 'awards_and_interest',
                                text: 'Rugby',
                                locale: 'en',
                                suggest: false,
                            },
                            {
                                id: 'e046493a-8c14-4bd0-97bf-77cd2847642f',
                                created_at: '2016-10-01T01:16:08.277Z',
                                updated_at: '2016-10-01T01:16:08.277Z',
                                type: 'awards_and_interest',
                                text: 'Reading',
                                locale: 'en',
                                suggest: false,
                            },
                        ],

                        // Interests
                        student_network_interests: [
                            {
                                text: 'Travel',
                                locale: 'en',
                            },
                            {
                                text: 'AI',
                                locale: 'es',
                            },
                        ],

                        // links
                        personal_website_url: 'http://path/to/website.hot',
                        blog_url: 'http://path/to/blog.wow',
                        tw_profile_url: 'http://path/to/twitter.com',
                        fb_profile_url: 'http://path/to/fb.com',
                        li_profile_url: 'https://www.linkedin.com/in/ligatolorenzo',
                        resume_id: '369d4249-4094-4904-939b-18637b0e5c31',
                        resume: {
                            id: '369d4249-4094-4904-939b-18637b0e5c31',
                            file_file_name: 'Lorenzo_Ligato—Resume.pdf',
                            file_fingerprint: '94acd21ef9c04526466b5dbe5cbea24b',
                            formats: {
                                original: {
                                    url:
                                        'https://uploads.smart.ly/resumes/94acd21ef9c04526466b5dbe5cbea24b/original/94acd21ef9c04526466b5dbe5cbea24b.pdf',
                                },
                            },
                            dimensions: null,
                            file_updated_at: '2017-03-07T15:15:54.064Z',
                        },

                        // personal summary
                        bio:
                            "Hi. My name is Cool. I'm going to be the best Pokemon Trainer the world has ever seen!. Gotta catch 'em all!",
                        personal_fact: 'Can flip you for real',
                        peer_recommendations: [],

                        // user stuff
                        avatar_url: 'http://path/to/candidates_avatar.png',
                        target_graduation: new Date('2016/01/01').getTime() / 1000,
                        email: 'bobdoe@email.com',

                        candidate_cohort_name: 'MBA Foo',
                        race: [],

                        // tracking data
                        last_updated_at_by_student: new Date('2015/11/11').getTime() / 1000,
                        last_confirmed_at_by_student: new Date('2015/11/11').getTime() / 1000,
                        last_confirmed_at_by_internal: new Date('2015/11/12').getTime() / 1000,
                        last_confirmed_internally_by: 'Admin Foo',
                    },
                    overrides,
                );
            },

            getInstance(overrides) {
                return CareerProfile.new(this.sampleAttrs(overrides));
            },
        };

        return {};
    },
]);
