import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';

angular.module('FrontRoyal.Careers').factory('CareerProfileListFixtures', [
    '$injector',
    $injector => {
        const CareerProfileList = $injector.get('CareerProfileList');
        const CareerProfile = $injector.get('CareerProfile');
        const guid = $injector.get('guid');

        $injector.get('CareerProfileFixtures');

        CareerProfileList.fixtures = {
            sampleAttrs(overrides = {}) {
                overrides.id = overrides.id || String(Math.random());

                const careerProfiles = [
                    CareerProfile.fixtures.getInstance(),
                    CareerProfile.fixtures.getInstance(),
                    CareerProfile.fixtures.getInstance(),
                ];

                return angular.extend(
                    {
                        name: `CareerProfileList${guid.generate()}`,
                        career_profile_ids: _.pluck(careerProfiles, 'id'),
                        role_descriptors: [],
                    },
                    overrides,
                );
            },

            getInstance(overrides) {
                return CareerProfileList.new(this.sampleAttrs(overrides));
            },
        };

        return {};
    },
]);
