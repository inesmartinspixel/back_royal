import 'Careers/angularModule';

angular.module('FrontRoyal.Careers').factory('HiringApplicationFixtures', [
    '$injector',
    $injector => {
        const HiringApplication = $injector.get('HiringApplication');
        const guid = $injector.get('guid');

        HiringApplication.fixtures = {
            sampleAttrs(overrides = {}) {
                overrides.id = overrides.id || String(Math.random());

                return angular.extend(
                    {
                        applied_at: 123,
                        updated_at: 456,
                        user_id: guid.generate(),
                        name: 'Sarah Peterson',
                        email: 'hiring@pedago.com',
                        job_role: 'hiring_manager',
                        job_title: 'VP Operations',
                        role_descriptors: ['sales', 'marketing', 'general_management', 'finance'],
                        company_year: '2006',
                        funding: 'public',
                        company_annual_revenue: 'more_than_500_million',
                        company_employee_count: '5000_plus',
                        team_mission: 'Enable easy building of sophisticated, scalable web applications.',
                        place_details: {
                            formatted_address: 'Seattle, WA',
                        },
                        industry: 'technology',
                        team_name: 'Core Services Team',
                        fun_fact: 'My first job at Amazon was packing boxes in 2006',
                        where_descriptors: [],
                        position_descriptors: [],
                        salary_ranges: [],

                        company_name: 'ACME products',
                        status: 'pending',
                        website_url: 'https://example.com',
                        avatar_url: 'https://path/to/hiring_manager/avatar.png',
                        last_calculated_complete_percentage: 42,
                    },
                    overrides,
                );
            },

            getInstance(overrides) {
                return HiringApplication.new(this.sampleAttrs(overrides));
            },
        };

        return {};
    },
]);
