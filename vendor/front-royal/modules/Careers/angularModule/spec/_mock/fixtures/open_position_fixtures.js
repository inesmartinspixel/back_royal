import 'Careers/angularModule';

angular.module('FrontRoyal.Careers').factory('OpenPositionFixtures', [
    '$injector',
    $injector => {
        const OpenPosition = $injector.get('OpenPosition');
        const guid = $injector.get('guid');

        OpenPosition.fixtures = {
            sampleAttrs(overrides = {}) {
                const attrs = angular.extend(
                    {
                        id: guid.generate(),
                        created_at: 123,
                        updated_at: 234,
                        hiring_manager_id: guid.generate(),
                        title: 'Awesome position',
                        last_curated_at: new Date().getTime() / 1000,
                    },
                    overrides,
                );

                return attrs;
            },

            getInstance(overrides) {
                return OpenPosition.new(this.sampleAttrs(overrides));
            },
        };

        return {};
    },
]);
