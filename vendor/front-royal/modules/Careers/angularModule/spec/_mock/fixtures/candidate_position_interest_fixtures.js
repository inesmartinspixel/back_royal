import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';

angular.module('FrontRoyal.Careers').factory('CandidatePositionInterestFixtures', [
    '$injector',
    $injector => {
        const CandidatePositionInterest = $injector.get('CandidatePositionInterest');
        const guid = $injector.get('guid');
        const CareerProfile = $injector.get('CareerProfile');
        $injector.get('CareerProfileFixtures');

        CandidatePositionInterest.fixtures = {
            sampleAttrs(overrides = {}) {
                const attrs = angular.extend(
                    {
                        id: guid.generate(),
                        open_position_id: guid.generate(),
                        created_at: 123,
                        updated_at: 234,
                        candidate_id: guid.generate(),
                        candidate_status: 'accepted',
                        hiring_manager_status: 'hidden',
                        admin_status: 'unreviewed',
                        cover_letter: {},
                    },
                    overrides,
                );

                attrs.career_profile = CareerProfile.fixtures.getInstance();

                return attrs;
            },

            getInstance(overrides) {
                return CandidatePositionInterest.new(this.sampleAttrs(overrides));
            },
        };

        return {};
    },
]);
