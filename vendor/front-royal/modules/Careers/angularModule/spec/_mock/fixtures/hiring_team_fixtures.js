import 'Careers/angularModule';

angular.module('FrontRoyal.Careers').factory('HiringTeamFixtures', [
    '$injector',
    $injector => {
        const HiringTeam = $injector.get('HiringTeam');
        const guid = $injector.get('guid');

        HiringTeam.fixtures = {
            sampleAttrs(overrides = {}) {
                overrides.id = overrides.id || String(Math.random());

                const hiringManagerIds = [
                    guid.generate(),
                    guid.generate(),
                    guid.generate(),
                    guid.generate(),
                    guid.generate(),
                ];

                const attrs = angular.extend(
                    {
                        id: guid.generate(),
                        created_at: '1505354032',
                        updated_at: '1505354099',
                        title: 'Team Rocket',
                        domain: 'rocket.com',
                        hiring_manager_ids: hiringManagerIds,
                        owner_id: hiringManagerIds[0],
                        hiring_plan: HiringTeam.HIRING_PLAN_LEGACY,
                        subscriptions: [],
                        subscription_required: true,
                    },
                    overrides,
                );

                if (attrs.hiring_plan === HiringTeam.HIRING_PLAN_LEGACY && !attrs.stripe_plans) {
                    attrs.stripe_plans = [
                        {
                            nickname: 'Hiring Unlimited $420',
                            amount: 42000,
                            billing_scheme: 'per_unit',
                            currency: 'usd',
                            interval: 'month',
                            interval_count: 1,
                            usage_type: 'licensed',
                            id: 'hiring_unlimited_300',
                            product_id: 'hiring_unlimited',
                            product_name: 'Smartly Hiring (unlimited)',
                            product_metadata: {
                                available_for_hiring_plan: HiringTeam.HIRING_PLAN_LEGACY,
                            },
                        },
                        {
                            nickname: 'Hiring Per Match $42',
                            amount: 4200,
                            billing_scheme: 'per_unit',
                            currency: 'usd',
                            interval: 'month',
                            interval_count: 1,
                            usage_type: 'metered',
                            id: 'hiring_per_match_20',
                            product_id: 'hiring_per_match',
                            product_name: 'Smartly Hiring',
                            product_unit_label: 'match',
                            product_metadata: {
                                available_for_hiring_plan: HiringTeam.HIRING_PLAN_LEGACY,
                            },
                        },
                    ];
                } else if (!attrs.stripe_plans) {
                    attrs.stripe_plans = [
                        {
                            nickname: 'Hiring Per Post $20',
                            amount: 20 * 100,
                            billing_scheme: 'per_unit',
                            currency: 'usd',
                            interval: 'month',
                            interval_count: 1,
                            usage_type: 'licensed',
                            id: 'hiring_pay_per_post_89',
                            product_id: 'hiring_pay_per_post',
                            product_name: 'Smartly Talent (Job Post)',
                            product_metadata: {
                                available_for_hiring_plan: HiringTeam.HIRING_PLAN_PAY_PER_POST,
                            },
                        },
                        {
                            nickname: 'Hiring Unlimited w/ Sourcing',
                            amount: 249 * 100,
                            billing_scheme: 'per_unit',
                            currency: 'usd',
                            interval: 'month',
                            interval_count: 1,
                            usage_type: 'licensed',
                            id: 'hiring_unlimited_w_sourcing_249',
                            product_id: 'hiring_unlimited_w_sourcing',
                            product_name: 'Smartly Talent',
                            product_metadata: {
                                available_for_hiring_plan: HiringTeam.HIRING_PLAN_UNLIMITED_WITH_SOURCING,
                            },
                        },
                    ];
                }

                if (overrides.hiring_managers) {
                    attrs.hiring_manager_ids = _.pluck(overrides.hiring_managers, 'id');
                } else {
                    attrs.hiring_manager_ids = hiringManagerIds;
                }

                return attrs;
            },

            getInstance(overrides) {
                return HiringTeam.new(this.sampleAttrs(overrides));
            },
        };

        return {};
    },
]);
