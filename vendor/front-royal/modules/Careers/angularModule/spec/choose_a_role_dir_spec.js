import 'AngularSpecHelper';
import 'Careers/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import chooseARoleLocales from 'Careers/locales/careers/choose_a_role-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';

setSpecLocales(chooseARoleLocales, fieldOptionsLocales);

describe('FrontRoyal.Careers.ChooseARole', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);
    });

    function render(params = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = params.ngModel || null;
        renderer.scope.allOption = params.allOption;
        renderer.scope.shouldDisable = params.shouldDisable;
        const mode = params.mode || 'select';

        renderer.render(
            `<choose-a-role all-option="allOption" ng-model="ngModel" mode="${mode}" should-disable="shouldDisable"></choose-a-role>`,
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    // It sucks how fickle these tests are, but roles don't change that often and I didn't
    // want to spend more time figuring out how to mock the constant CAREERS_AREA_KEYS
    describe('alphabetizing with other option at the end', () => {
        let careersConstantCount;
        beforeEach(() => {
            const areaKeys = $injector.get('CAREERS_AREA_KEYS');
            careersConstantCount = Object.keys(areaKeys).length;
            Object.keys(areaKeys).forEach(key => {
                if (areaKeys[key]) {
                    careersConstantCount += areaKeys[key].length;
                }
            });
        });

        function assert(hasValueKey = false) {
            expect(scope.roleOptions.length).toEqual(careersConstantCount);

            if (hasValueKey) {
                expect(scope.roleOptions[0].value).toEqual('data_analysis');
                expect(scope.roleOptions[scope.roleOptions.length - 2].value).toEqual('teaching_education');
                expect(scope.roleOptions[scope.roleOptions.length - 1].value).toEqual('other');
                expect(scope.roleOptions[4].value).toEqual('art_creative');
            } else {
                expect(scope.roleOptions[0]).toEqual('data_analysis');
                expect(scope.roleOptions[scope.roleOptions.length - 2]).toEqual('teaching_education');
                expect(scope.roleOptions[scope.roleOptions.length - 1]).toEqual('other');
                expect(scope.roleOptions[4]).toEqual('art_creative');
            }
        }

        it('should work when select', () => {
            render({
                mode: 'select',
            });
            assert(true);
        });

        it('should work when multi', () => {
            render({
                mode: 'multi',
            });
            assert(false);
        });

        it('should work when selectize', () => {
            render({
                mode: 'selectize',
            });
            assert(true);
        });
    });

    // Again, kind of fickle, but roles don't change that often
    describe('getOptionDisabled', () => {
        it('should return true if top-level with nested values', () => {
            render();
            expect(scope.getOptionDisabled('design')).toBe(true);
        });

        it('should return false if top-level but isolated', () => {
            render();
            expect(scope.getOptionDisabled('data_analysis')).toBe(false);
        });

        it('should return false if nested value', () => {
            render();
            expect(scope.getOptionDisabled('ux_ui')).toBe(false);
        });
    });

    describe('select mode with all option', () => {
        it('should not disable the first option', () => {
            render({
                allOption: true,
                mode: 'select',
            });
            SpecHelper.expectElementAttr(elem, 'option:eq(0)', 'disabled');
        });
        it('should disable the first option', () => {
            render({
                allOption: false,
                mode: 'select',
            });
            SpecHelper.expectElementAttr(elem, 'option:eq(0)', 'disabled', 'disabled');
        });
    });

    describe('select mode', () => {
        it('should render a select box', () => {
            render({
                mode: 'select',
            });
            SpecHelper.expectNoElement(elem, '> selectize');
            SpecHelper.expectNoElement(elem, '> multi-select');

            const val = elem.find('option:eq(1)').val();

            SpecHelper.updateSelect(elem, '> select', val);

            const model = scope.ngModel;
            expect(model).toEqual(val);
        });

        it('should disable select if shouldDisable', () => {
            render({
                mode: 'select',
                shouldDisable: true,
            });
            SpecHelper.expectElementDisabled(elem, 'select');
        });

        // A legacy case where users might have selected a top-level role that is now disabled
        // See https://trello.com/c/FKBIfP0X
        it('should not disable option even if specified in the options map if the ngModel is set to that option', () => {
            render({
                mode: 'select',
                ngModel: 'design',
            });
            SpecHelper.expectElementText(elem, 'option:eq(4)', 'Design');
            SpecHelper.expectElementEnabled(elem, 'option:eq(4)');
        });
    });

    describe('selectize mode', () => {
        it('should render a selectize control', () => {
            render({
                mode: 'selectize',
            });
            SpecHelper.expectNoElement(elem, '> select');
            SpecHelper.expectNoElement(elem, '> multi-select');

            const val = elem.find('.option:eq(1)').attr('data-value');

            SpecHelper.updateSelectize(elem, '> selectize', val);
            const model = scope.ngModel;
            expect(model).toEqual([val]);
        });

        it('should disable select if shouldDisable', () => {
            render({
                mode: 'selectize',
                shouldDisable: true,
            });
            SpecHelper.expectElementDisabled(elem, 'selectize');
        });
    });

    describe('multi-select mode', () => {
        it('should render a multi-select control', () => {
            render({
                mode: 'multi',
            });
            SpecHelper.expectNoElement(elem, '> select');
            SpecHelper.expectNoElement(elem, '> selectize');

            const val = elem.find('option:eq(1)').val();

            SpecHelper.updateMultiSelect(elem, '> multi-select', val);
            const model = scope.ngModel;
            expect(model).toEqual([val]);
        });
    });
});
