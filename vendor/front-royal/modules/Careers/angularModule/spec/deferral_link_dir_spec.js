import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import setSpecLocales from 'Translation/setSpecLocales';
import deferralLinkLocales from 'Careers/locales/careers/deferral_link-en.json';

setSpecLocales(deferralLinkLocales);

describe('DeferralLinkDir', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let DeferralLink;
    let Cohort;
    let CohortApplication;
    let Subscription;
    let deferralLink;
    let upcomingCohorts;
    let toCohortApplication;
    let user;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                DeferralLink = $injector.get('DeferralLink');
                Cohort = $injector.get('Cohort');
                CohortApplication = $injector.get('CohortApplication');
                Subscription = $injector.get('Subscription');

                $injector.get('CohortFixtures');
                $injector.get('CohortApplicationFixtures');

                user = SpecHelper.stubCurrentUser();
                user.relevant_cohort = Cohort.fixtures.getInstance();
                jest.spyOn(user.relevant_cohort, 'supportsPayments', 'get').mockReturnValue(true);
            },
        ]);

        deferralLink = DeferralLink.new({
            from_cohort_application: CohortApplication.fixtures.getInstance({ program_type: 'mba' }),
            used: false,
            active: true,
        });

        upcomingCohorts = [Cohort.fixtures.getInstance(), Cohort.fixtures.getInstance()];
        toCohortApplication = CohortApplication.fixtures.getInstance();
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.id = opts.id || deferralLink.id;
        renderer.render('<deferral-link id="id"></deferral-link>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should redirect to home if not an active deferral link', () => {
        deferralLink.active = false;
        DeferralLink.expect('show').returns(deferralLink);
        render();
        jest.spyOn(scope, 'gotoHome');
        DeferralLink.flush('show');
        expect(scope.gotoHome).toHaveBeenCalled();
    });

    it('should redirect to home if no deferral link found', () => {
        DeferralLink.expect('show').returns({});
        render();
        jest.spyOn(scope, 'gotoHome');
        DeferralLink.flush('show');
        expect(scope.gotoHome).toHaveBeenCalled();
    });

    it('should work when an active link', () => {
        DeferralLink.expect('show').returns({
            result: deferralLink,
            meta: {
                upcoming_cohorts: upcomingCohorts,
            },
        });
        render();
        jest.spyOn(scope.currentUser, 'primarySubscription', 'get').mockReturnValue(Subscription.new());
        SpecHelper.expectElement(elem, 'front-royal-spinner');
        DeferralLink.flush('show');
        scope.$digest();
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');

        SpecHelper.expectElement(elem, '.from-messaging-wrapper');
        SpecHelper.expectNoElement(elem, '.to-messaging-wrapper');

        SpecHelper.expectElements(elem, 'option', upcomingCohorts.length + 1);
        DeferralLink.expect('save').returns({
            result: deferralLink,
            meta: {
                to_cohort_application: toCohortApplication,
            },
        });

        SpecHelper.expectNoElement(elem, '.billing-confirmation-wrapper');
        SpecHelper.selectOptionByLabel(elem, 'select', scope.cohortOptions[0].label);
        SpecHelper.expectElement(elem, '.billing-confirmation-wrapper');
        SpecHelper.expectElementText(elem, '.contact-us', 'Questions? Contact us at mba@quantic.edu');
        SpecHelper.expectElementDisabled(elem, '[name="use-deferral-link"]');

        SpecHelper.checkCheckbox(elem, '[name="billing-confirmation"]');

        SpecHelper.click(elem, '[name="use-deferral-link"]');
        SpecHelper.expectElement(elem, 'front-royal-spinner');
        DeferralLink.flush('save');
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');

        SpecHelper.expectElement(elem, '.to-messaging-wrapper');
        SpecHelper.expectElement(elem, 'add-to-calendar-widget');
    });
});
