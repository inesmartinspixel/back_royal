import 'AngularSpecHelper';
import 'Careers/angularModule';

describe('CareersNetworkViewModelInterceptor', () => {
    let SpecHelper;
    let OpenPosition;
    let careersNetworkViewModel;
    let CareersNetworkViewModelInterceptor;
    let $injector;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('SpecHelper', 'FrontRoyal.Users');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                OpenPosition = $injector.get('OpenPosition');
                CareersNetworkViewModelInterceptor = $injector.get('CareersNetworkViewModelInterceptor');
                SpecHelper.stubCurrentUser();
                careersNetworkViewModel = $injector.get('CareersNetworkViewModel').get('hiringManager');
            },
        ]);
    });

    afterAll(() => {
        SpecHelper.cleanup();
    });

    describe('response', () => {
        describe('handlePushedOpenPositions', () => {
            it('should commit open position', () => {
                jest.spyOn(careersNetworkViewModel, 'commitOpenPosition');

                CareersNetworkViewModelInterceptor.response({
                    data: {
                        meta: {
                            open_positions: [
                                {
                                    id: 1,
                                },
                            ],
                        },
                    },
                });
                expect(careersNetworkViewModel.commitOpenPosition).toHaveBeenCalled();
                const arg = careersNetworkViewModel.commitOpenPosition.mock.calls[0][0];
                expect(arg.id).toEqual(1);
                expect(arg.isA(OpenPosition)).toBe(true);
            });
        });
    });
});
