import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/candidate_position_interest_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Careers/angularModule/spec/_mock/fixtures/open_position_fixtures';
import 'Careers/angularModule/spec/careers_spec_helper';
import setSpecLocales from 'Translation/setSpecLocales';
import positionReviewLocales from 'Careers/locales/careers/position_review-en.json';
import positionCardLocales from 'Careers/locales/careers/position_card-en.json';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';
import inviteToInterviewLocales from 'Careers/locales/careers/invite_to_interview-en.json';
import salaryLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/salary-en.json';

setSpecLocales(
    positionReviewLocales,
    positionCardLocales,
    candidateListCardLocales,
    inviteToInterviewLocales,
    salaryLocales,
);

describe('FrontRoyal.Careers.PositionReviewDir', () => {
    let $injector;
    let SpecHelper;
    let CareersSpecHelper;
    let renderer;
    let elem;
    let scope;
    let OpenPosition;
    let CandidatePositionInterest;
    let $q;
    let $timeout;
    let currentUser;
    let CareerProfile;
    let HiringRelationship;
    let interests;
    let $location;
    let careersNetworkViewModel;
    let $rootScope;
    let DialogModal;
    let User;
    let PositionsWithInterestsHelper;
    let positionsWithInterestsHelper;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                OpenPosition = $injector.get('OpenPosition');
                CandidatePositionInterest = $injector.get('CandidatePositionInterest');
                CareerProfile = $injector.get('CareerProfile');
                $injector.get('CareerProfileFixtures');
                OpenPosition = $injector.get('OpenPosition');
                HiringRelationship = $injector.get('HiringRelationship');
                $location = $injector.get('$location');
                $q = $injector.get('$q');
                $timeout = $injector.get('$timeout');
                $rootScope = $injector.get('$rootScope');
                DialogModal = $injector.get('DialogModal');
                User = $injector.get('User');
                PositionsWithInterestsHelper = $injector.get('PositionsWithInterestsHelper');
                $injector.get('OpenPositionFixtures');

                $injector.get('CandidatePositionInterestFixtures');
            },
        ]);

        SpecHelper.stubDirective('positionExpirationMessage');
        careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
        currentUser = careersNetworkViewModel.user;
        SpecHelper.stubDirective('candidateListCard');
    });

    function render(useInterests, openPositions, position) {
        if (useInterests) {
            CandidatePositionInterest.expect('create');
            interests = [
                // new/unseen
                CandidatePositionInterest.new({
                    hiring_manager_id: currentUser.id,
                    open_position_id: 1,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'unseen',
                    career_profile: CareerProfile.fixtures.getInstance({
                        anonymized: false,
                    }),
                }),
                // pending/seen
                CandidatePositionInterest.new({
                    hiring_manager_id: currentUser.id,
                    open_position_id: 1,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'pending',
                    career_profile: CareerProfile.fixtures.getInstance(),
                }),
                // invited
                CandidatePositionInterest.new({
                    hiring_manager_id: currentUser.id,
                    open_position_id: 1,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'invited',
                    career_profile: CareerProfile.fixtures.getInstance(),
                }),
                // liked
                CandidatePositionInterest.new({
                    hiring_manager_id: currentUser.id,
                    open_position_id: 1,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'accepted',
                    career_profile: CareerProfile.fixtures.getInstance(),
                }),
                // saved_for_later
                CandidatePositionInterest.new({
                    hiring_manager_id: currentUser.id,
                    open_position_id: 1,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'saved_for_later',
                    career_profile: CareerProfile.fixtures.getInstance(),
                }),
                // passed
                CandidatePositionInterest.new({
                    hiring_manager_id: currentUser.id,
                    open_position_id: 1,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'rejected',
                    career_profile: CareerProfile.fixtures.getInstance(),
                }),
                // unseen, anonymized
                CandidatePositionInterest.new({
                    hiring_manager_id: currentUser.id,
                    open_position_id: 1,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'unseen',
                    career_profile: CareerProfile.fixtures.getInstance({
                        anonymized: true,
                    }),
                }),
            ];
        } else {
            interests = [];
        }

        renderer = SpecHelper.renderer();
        renderer.scope.position =
            position ||
            OpenPosition.fixtures.getInstance({
                id: 1,
                title: 'Marketing Leader Extraordinaire',
                hiring_manager_id: careersNetworkViewModel.user.id,
                role: 'marketing',
                archived: false,
                featured: true,
                auto_expiration_date: '2019-01-01',
            });

        positionsWithInterestsHelper = new PositionsWithInterestsHelper(careersNetworkViewModel);
        jest.spyOn(PositionsWithInterestsHelper, 'loadPositionsAndInterests').mockReturnValue(
            $q.when(positionsWithInterestsHelper),
        );
        jest.spyOn(positionsWithInterestsHelper, 'interests').mockReturnValue(interests);

        openPositions = openPositions || [
            renderer.scope.position,
            OpenPosition.fixtures.getInstance({
                id: 2,
                created_at: 1234,
                featured: true,
            }),
            OpenPosition.fixtures.getInstance({
                id: 3,
                created_at: 1235,
                featured: true,
            }),
        ];
        careersNetworkViewModel.openPositions = openPositions;

        renderer.render('<position-review position="position" ></position-review');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should hideRelatedCandidatePositionInterests on hiringRelationship:updateStatus', () => {
        render();
        const mockHiringRelationshipViewModel = {};
        jest.spyOn(positionsWithInterestsHelper, 'hideRelatedCandidatePositionInterests').mockImplementation(() => {});
        scope.$broadcast('hiringRelationship:updateStatus', mockHiringRelationshipViewModel);
        expect(positionsWithInterestsHelper.hideRelatedCandidatePositionInterests).toHaveBeenCalledWith(
            mockHiringRelationshipViewModel,
        );
    });

    describe('openPositions', () => {
        it('should be positions that are non-archived and featured', () => {
            const nonFeaturedPosition = OpenPosition.fixtures.getInstance({
                featured: false,
                hiring_manager_id: currentUser.id,
            });
            const archivedPosition = OpenPosition.fixtures.getInstance({
                featured: true,
                archived: true,
                hiring_manager_id: currentUser.id,
            });
            const featuredPosition = OpenPosition.fixtures.getInstance({
                featured: true,
                archived: false,
                hiring_manager_id: currentUser.id,
            });
            const reviewingPosition = OpenPosition.fixtures.getInstance({
                featured: true,
                archived: false,
                hiring_manager_id: currentUser.id,
            });

            // This would normally happen in hiring-positions.
            // See https://trello.com/c/TScf5z7m. Once we fix that,
            // we probably won't need this here
            // reviewingPosition.$interests = [];

            render(
                true,
                [nonFeaturedPosition, archivedPosition, featuredPosition, reviewingPosition],
                reviewingPosition,
            );

            expect(scope.openPositions).toEqual(expect.arrayContaining([featuredPosition, reviewingPosition]));
            expect(_.contains(scope.openPositions, nonFeaturedPosition)).toBe(false);
            expect(_.contains(scope.openPositions, archivedPosition)).toBe(false);
        });
    });

    it('should go back to the positions page', () => {
        render(false);
        jest.spyOn($location, 'path').mockImplementation(() => {});
        SpecHelper.click(elem, '.back');
        expect($location.path).toHaveBeenCalledWith('/hiring/positions');
    });

    // FIXME: this dialog modal can be gutted after we launch pay-per-post
    // https://trello.com/c/XqkS7esI
    describe('DialogModal', () => {
        describe('with legacy hiring plan', () => {
            beforeEach(() => {
                jest.spyOn(currentUser, 'usingLegacyHiringPlan', 'get').mockReturnValue(true);
                jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            });

            it('should show a modal the first time an unsubscribed user reviews a position', () => {
                currentUser.has_seen_first_position_review_modal = false;
                jest.spyOn(currentUser, 'hasFullHiringManagerAccess', 'get').mockReturnValue(false);
                jest.spyOn(PositionsWithInterestsHelper.prototype, 'numInterests').mockReturnValue(1);

                render(true);

                expect(DialogModal.alert).toHaveBeenCalled();
            });

            it('should not show a modal after the first time', () => {
                currentUser.has_seen_first_position_review_modal = true;
                jest.spyOn(currentUser, 'hasFullHiringManagerAccess', 'get').mockReturnValue(false);

                render(true);

                expect(DialogModal.alert).not.toHaveBeenCalled();
            });

            it('should save user attribute in certain conditions', () => {
                currentUser.has_seen_first_position_review_modal = false;
                jest.spyOn(currentUser, 'hasFullHiringManagerAccess', 'get').mockReturnValue(true);
                User.expect('save');

                render(true);

                expect(DialogModal.alert).not.toHaveBeenCalled();
                User.flush('save');
            });
        });

        describe('with modern hiring plan', () => {
            beforeEach(() => {
                jest.spyOn(currentUser, 'usingLegacyHiringPlan', 'get').mockReturnValue(false);
                jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            });

            it('should not show a modal', () => {
                currentUser.has_seen_first_position_review_modal = false;
                jest.spyOn(currentUser, 'hasFullHiringManagerAccess', 'get').mockReturnValue(false);

                render(true);

                expect(DialogModal.alert).not.toHaveBeenCalled();
            });
        });
    });

    describe('with candidates', () => {
        it('should group interests', () => {
            render(true);
            expect(scope.interestGroups.all_interested.length).toEqual(3); // two unseen + one pending
            expect(scope.interestGroups.connected.length).toEqual(2); // one invited + one liked
            expect(scope.interestGroups.saved.length).toEqual(1);
            expect(scope.interestGroups.passed.length).toEqual(1);
        });

        it('should move the career profile to the new group and update tab counts when saving', () => {
            HiringRelationship.expect('create');
            render(true);
            jest.spyOn(
                positionsWithInterestsHelper,
                'hideRelatedCandidatePositionInterests',
            ).mockImplementation(() => {});

            expect(scope.tabs[0].translate.values.num).toEqual(3); // all_interested tab count
            expect(scope.tabs[1].translate.values.num).toEqual(1); // saved tab count
            expect(scope.interestGroups.all_interested.length).toEqual(3); // two unseen + one pending
            expect(scope.interestGroups.saved.length).toEqual(1);

            SpecHelper.click(elem, 'candidate-list-actions:eq(0) .save');

            expect(scope.tabs[0].translate.values.num).toEqual(2); // all_interested tab count
            expect(scope.tabs[1].translate.values.num).toEqual(2); // saved tab count
            expect(scope.interestGroups.all_interested.length).toEqual(2);
            expect(scope.interestGroups.saved.length).toEqual(2);
        });

        it('should move the position interest to the new group and update tab counts when inviting', () => {
            HiringRelationship.expect('create');
            SpecHelper.stubDirective('tabs');
            render(true);
            jest.spyOn(
                positionsWithInterestsHelper,
                'hideRelatedCandidatePositionInterests',
            ).mockImplementation(() => {});

            expect(scope.tabs[0].translate.values.num).toEqual(3); // all_interested tab
            expect(scope.tabs[2].translate.values.num).toEqual(2); // connected tab
            expect(scope.interestGroups.all_interested.length).toEqual(3); // two unseen + one pending
            expect(scope.interestGroups.connected.length).toEqual(2);

            jest.spyOn(careersNetworkViewModel, 'ensureOpenPositions').mockReturnValue($q.when([scope.position]));
            SpecHelper.click(elem, 'candidate-list-actions:eq(0) .invite');

            const body = $('body');
            const interviewElement = body.find('invite-to-interview-form');

            SpecHelper.submitForm(interviewElement);

            HiringRelationship.flush('create');
            $timeout.flush();

            expect(scope.tabs[0].translate.values.num).toEqual(2); // all_interested tab
            expect(scope.tabs[2].translate.values.num).toEqual(3); // connected tab
            expect(scope.interestGroups.all_interested.length).toEqual(2);
            expect(scope.interestGroups.connected.length).toEqual(3);
        });

        it('should mark unseen, non-anonymized profiles as pending with a $new flag', () => {
            render(true);
            expect(interests[0].hiring_manager_status).toBe('pending');
            expect(interests[0].$new).toBe(true);

            // Not changed to new because it is anonymized
            expect(_.last(interests).hiring_manager_status).toBe('unseen');
            expect(_.last(interests).$new).toBeUndefined();
        });

        it('should update interest groups on candidatePositionInterestUpdate', () => {
            render(true);
            expect(scope.interestGroups.all_interested.length).toEqual(3); // two unseen + one pending
            interests.push(
                CandidatePositionInterest.fixtures.getInstance({
                    hiring_manager_id: currentUser.id,
                    open_position_id: 1,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'pending',
                    career_profile: CareerProfile.fixtures.getInstance(),
                }),
            );
            $rootScope.$broadcast('candidatePositionInterestUpdate');
            expect(scope.interestGroups.all_interested.length).toEqual(4);
        });
    });

    describe('no candidates', () => {
        it('should display long text on first tab', () => {
            render(false);
            SpecHelper.expectNoElement(elem, 'candidate-list');
            SpecHelper.expectElementText(
                elem,
                '.no-candidates p',
                'We’ll be in touch as soon as you have candidates to review! In the meantime, post another position or source talent from our candidate pool.',
            );
        });

        it('should display short text on other tabs', () => {
            render(false);
            SpecHelper.click(elem, 'button.positions-saved');
            SpecHelper.expectNoElement(elem, 'candidate-list');
            SpecHelper.expectElementText(elem, '.no-candidates h1', 'No candidates to review');
        });
    });

    describe('traversePositions', () => {
        it('should NOT show previous/next post links if there are no other positions to show', () => {
            render(null, [OpenPosition.new()]);
            SpecHelper.expectNoElement(elem, '.traverse');
        });

        it('should show previous/next post links', () => {
            render();
            SpecHelper.expectElements(elem, '.traverse', 2);
        });

        it('should change positions', () => {
            render();
            jest.spyOn(scope, 'traversePositions');
            SpecHelper.click(elem, '.traverse:eq(0)'); // this is the "previous" button
            expect(scope.traversePositions).toHaveBeenCalledWith(2); // 3 is the last openPositions id
            expect($location.search().positionId).toEqual(2);
        });
    });
});
