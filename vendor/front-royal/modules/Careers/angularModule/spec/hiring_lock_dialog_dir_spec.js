import 'AngularSpecHelper';
import 'Careers/angularModule';
import careersLocales from 'Careers/locales/careers/careers-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(careersLocales);

describe('FrontRoyal.Careers.HiringLockDialog', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let currentUser;
    let hasAcceptedHiringManagerAccess;
    let unlockWhenReviewing;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);

        currentUser = SpecHelper.stubCurrentUser();
        currentUser.hiring_team = {};
        hasAcceptedHiringManagerAccess = true;
        unlockWhenReviewing = false;
        jest.spyOn(currentUser, 'hasAcceptedHiringManagerAccess', 'get').mockImplementation(() => {
            return hasAcceptedHiringManagerAccess;
        });
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.unlockWhenReviewing = unlockWhenReviewing;
        renderer.render('<hiring-lock-dialog unlock-when-reviewing="unlockWhenReviewing"></hiring-lock-dialog>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('that-team-exists', () => {
        it('should be visible if no hiring team', () => {
            currentUser.hiring_team = null;
            currentUser.email = 'someone@myDomain.com';
            render();
            SpecHelper.expectElementText(
                elem,
                '.that-team-exists .title',
                'Your team may already be on Smartly Talent',
            );
            SpecHelper.expectElementText(
                elem,
                '.that-team-exists .body',
                'It appears that we already have a team for myDomain.com. Please contact hiring@smart.ly to complete account setup!',
            );
        });

        it('should not be visible if there is a hiring team', () => {
            render();
            SpecHelper.expectNoElement(elem, '.that-team-exists');
        });
    });

    describe('we-are-reviewing', () => {
        it('should be visible if !hasAcceptedHiringManagerAccess and there is a hiring team', () => {
            hasAcceptedHiringManagerAccess = false;
            render();
            SpecHelper.expectElementText(elem, '.we-are-reviewing .title', "We're reviewing your profile");
            SpecHelper.expectElementText(
                elem,
                '.we-are-reviewing .body',
                'Our team will be in touch soon. Thank you for joining Smartly Talent!',
            );
        });

        it('should not be visible if no hiring team', () => {
            hasAcceptedHiringManagerAccess = false;
            currentUser.hiring_team = null;
            render();
            SpecHelper.expectNoElement(elem, '.we-are-reviewing');
        });

        it('should not be visible if hasAcceptedHiringManagerAccess', () => {
            hasAcceptedHiringManagerAccess = true;
            render();
            SpecHelper.expectNoElement(elem, '.we-are-reviewing');
        });

        it('should not be visible if unlock-when-reviewing', () => {
            hasAcceptedHiringManagerAccess = false;
            unlockWhenReviewing = true;
            render();
            SpecHelper.expectNoElement(elem, '.we-are-reviewing');
        });
    });
});
