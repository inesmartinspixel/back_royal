import 'AngularSpecHelper';
import 'Careers/angularModule';

describe('FrontRoyal.Careers.CandidatePositionInterest', () => {
    let $injector;
    let CandidatePositionInterest;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                $injector.get('MockIguana');
                CandidatePositionInterest = $injector.get('CandidatePositionInterest');
            },
        ]);
    });

    describe('reviewedByHiringManager', () => {
        it('should work', () => {
            const interest = CandidatePositionInterest.new();
            interest.hiring_manager_status = 'invited';
            expect(interest.reviewedByHiringManager).toBe(true);
            interest.hiring_manager_status = 'hidden';
            expect(interest.reviewedByHiringManager).toBe(false);
        });
    });

    describe('hiddenOrReviewedByHiringManager', () => {
        it('should work', () => {
            const interest = CandidatePositionInterest.new();
            interest.hiring_manager_status = 'invited';
            expect(interest.hiddenOrReviewedByHiringManager).toBe(true);
            interest.hiring_manager_status = 'hidden';
            expect(interest.hiddenOrReviewedByHiringManager).toBe(true);
            interest.hiring_manager_status = 'unseen';
            expect(interest.hiddenOrReviewedByHiringManager).toBe(false);
        });
    });

    describe('adminStatusPriority', () => {
        it('should return 0 for outstanding', () => {
            const interest = CandidatePositionInterest.new({
                admin_status: 'outstanding',
            });
            expect(interest.adminStatusPriority).toBe(0);
        });

        it('should return 1 for reviewed', () => {
            const interest = CandidatePositionInterest.new({
                admin_status: 'reviewed',
            });
            expect(interest.adminStatusPriority).toBe(1);
        });
    });
});
