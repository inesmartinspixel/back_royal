import 'AngularSpecHelper';
import 'Careers/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import recommendedPositionsNotificationModalLocales from 'Careers/locales/careers/recommended_positions_notification_modal-en.json';

setSpecLocales(recommendedPositionsNotificationModalLocales);

describe('FrontRoyal.Careers.RecommendedPositionsNotificationModal', () => {
    let $injector;
    let renderer;
    let elem;
    let SpecHelper;
    let User;
    let onFinish;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                SpecHelper = $injector.get('SpecHelper');
                User = $injector.get('User');
            },
        ]);

        SpecHelper.stubCurrentUser();
        onFinish = jest.fn();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.onFinish = onFinish;
        renderer.render(
            '<recommended-positions-notification-modal on-finish="onFinish"></recommended-positions-notification-modal>',
        );
        elem = renderer.elem;
    }

    it('should save user and call onFinish', () => {
        render();
        User.expect('save');
        SpecHelper.toggleRadio(elem, '[id="bi_weekly"]'); // set scope.setting and enable button
        SpecHelper.click(elem, 'button');
        User.flush('save');
        expect(onFinish).toHaveBeenCalled();
    });
});
