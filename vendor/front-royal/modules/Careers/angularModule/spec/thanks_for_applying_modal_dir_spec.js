import 'AngularSpecHelper';
import 'Careers/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import thanksForApplyingModalLocales from 'Careers/locales/careers/thanks_for_applying_modal-en.json';

setSpecLocales(thanksForApplyingModalLocales);

describe('FrontRoyal.Careers.ThanksForApplyingModal', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let DialogModal;
    let $rootScope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                DialogModal = $injector.get('DialogModal');
                $rootScope = $injector.get('$rootScope');
            },
        ]);

        SpecHelper.stubConfig();
        SpecHelper.stubCurrentUser('learner');
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<thanks-for-applying-modal></thanks-for-applying-modal>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should dismiss and load home if action button is clicked', () => {
        render();
        jest.spyOn(DialogModal, 'hideAlerts').mockImplementation(() => {});
        SpecHelper.click(elem, 'button');
        expect(DialogModal.hideAlerts).toHaveBeenCalled();
    });

    it('should show appropriate text while in MBA or EMBA', () => {
        Object.defineProperty($rootScope.currentUser, 'isCareerNetworkOnly', {
            value: false,
            configurable: true,
        });
        render();
        SpecHelper.expectElementText(
            elem,
            '.message',
            'Get started in our introductory courses right away! Progress is considered as part of your application.',
        );
    });

    it('should show appropriate text while in MBA', () => {
        Object.defineProperty($rootScope.currentUser, 'isCareerNetworkOnly', {
            value: true,
            configurable: true,
        });
        render();
        SpecHelper.expectElementText(
            elem,
            '.message',
            'You’ll hear back from us soon. Earn a reward by sharing your referral link, then enjoy our free open courses on the Home tab!',
        );
    });
});
