import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_application_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/open_position_fixtures';
import 'Careers/angularModule/spec/careers_spec_helper';
import setSpecLocales from 'Translation/setSpecLocales';
import positionBarLocales from 'Careers/locales/careers/position_bar-en.json';

setSpecLocales(positionBarLocales);

describe('FrontRoyal.Careers.PositionBar', () => {
    let $injector;
    let SpecHelper;
    let CareersSpecHelper;
    let renderer;
    let elem;
    let scope;
    let OpenPosition;
    let currentUser;
    let position;
    let $location;
    let HiringApplication;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                OpenPosition = $injector.get('OpenPosition');
                $location = $injector.get('$location');
                HiringApplication = $injector.get('HiringApplication');

                $injector.get('CareerProfileFixtures');
                $injector.get('OpenPositionFixtures');
                $injector.get('HiringApplicationFixtures');
            },
        ]);

        SpecHelper.stubConfig();

        currentUser = SpecHelper.stubCurrentUser();
        expect(currentUser.pref_positions_candidate_list).toEqual(true); // sanity check

        CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
    });

    function render(position) {
        renderer = SpecHelper.renderer();
        renderer.scope.position = OpenPosition.new(position);
        renderer.render('<position-bar position="position"></position-bar>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should show basic info', () => {
        const updated_at = new Date();
        const position = OpenPosition.new({
            title: 'Marketing Leader Extraordinaire',
            role: 'marketing',
            updated_at: updated_at.getTime() / 1000,
            $numApplied: 15,
            $numReviewed: 5,
            $numUnreviewed: 10,
            $status: 'awaiting_review',
            hiring_application: HiringApplication.fixtures.getInstance({
                name: 'James Bond',
            }),
        });
        render(position);

        SpecHelper.expectElementText(elem, '.title span', position.title);
        SpecHelper.expectElementText(elem, '.title button', 'Edit'); // capitalized with css
        SpecHelper.expectElementText(elem, '.author', 'Added by: James Bond');

        // This element is hidden by default in list mode
        SpecHelper.expectNoElement(elem, 'button.reviewed');

        SpecHelper.expectNoElement(elem, '.drafted');
    });

    describe('new candidates', () => {
        const position = {
            title: 'Marketing Leader Extraordinaire',
            role: 'marketing',
            $numApplied: 15,
            $numReviewed: 5,
        };

        it('should NOT have new label', () => {
            render(position);
            SpecHelper.expectNoElement(elem, '.new');
        });
    });

    describe('actions', () => {
        beforeEach(() => {
            position = OpenPosition.new({
                id: '1212312312',
                title: 'Marketing Leader Extraordinaire',
                role: 'marketing',
                $numApplied: 15,
                $numReviewed: 5,
                archived: false,
                $status: 'open',
            });
        });

        it('should have archive and unarchive buttons when not pay-per-post', () => {
            jest.spyOn(currentUser, 'usingPayPerPostHiringPlan', 'get').mockReturnValue(false);
            jest.spyOn(position, 'toggleArchived').mockImplementation(() => {});
            render(position);
            position = scope.position;
            expect(position.archived).toBe(false);
            SpecHelper.expectElement(elem, 'button.archive');
            SpecHelper.expectNoElement(elem, 'button.unarchive');

            SpecHelper.click(elem, 'button.archive');
            expect(position.toggleArchived).toHaveBeenCalled();
            position.toggleArchived.mockClear();
            position.archived = true;
            scope.$digest();

            SpecHelper.expectNoElement(elem, 'button.archive');
            SpecHelper.expectElement(elem, 'button.unarchive');
            SpecHelper.click(elem, 'button.unarchive');
            expect(position.toggleArchived).toHaveBeenCalled();
        });

        it('should have manage or renew buttons when pay-per-post', () => {
            let subscription = {};
            jest.spyOn(currentUser, 'usingPayPerPostHiringPlan', 'get').mockReturnValue(true);
            render(position);
            jest.spyOn(scope.position, 'manage').mockImplementation(() => {});
            jest.spyOn(scope.position, 'renew').mockImplementation(() => {});
            jest.spyOn(currentUser, 'subscriptionForPosition').mockImplementation(() => subscription);
            position = scope.position;
            scope.$digest();

            // When there is a subscription, the manage button is visible
            expect(position.archived).toBe(false);
            SpecHelper.expectElement(elem, 'button.manage');
            SpecHelper.expectNoElement(elem, 'button.renew');

            SpecHelper.click(elem, 'button.manage');
            expect(scope.position.manage).toHaveBeenCalled();
            scope.position.manage.mockClear();

            // When the position is archived, there should be a
            // renew button
            subscription.cancel_at_period_end = true;
            position.archived = true;
            scope.$digest();

            SpecHelper.expectNoElement(elem, 'button.manage');
            SpecHelper.expectElement(elem, 'button.renew');
            SpecHelper.click(elem, 'button.renew');
            expect(scope.position.renew).toHaveBeenCalled();

            // When there is no subscription and the position is
            // not archived, there should not be a manage button
            // or a renew button
            subscription = null;
            position.archived = false;
            scope.$digest();

            SpecHelper.expectNoElement(elem, 'button.manage');
            SpecHelper.expectNoElement(elem, 'button.renew');
        });

        it('should allow editing the position', () => {
            jest.spyOn($location, 'search').mockImplementation(() => {});
            render(position);
            SpecHelper.click(elem, 'button.edit');
            expect($location.search.mock.calls[0][0]).toEqual('positionId', position.id);
            expect($location.search.mock.calls[1][0]).toEqual('action', 'edit');
        });

        describe('in list mode', () => {
            it('should allow reviewing when there are some to review and some reviewed', () => {
                position.$status = 'awaiting_review';
                render(position);
                jest.spyOn(scope, 'reviewCandidates').mockImplementation(() => {});
                SpecHelper.click(elem, '.position');
                expect(scope.reviewCandidates).toHaveBeenCalledWith();
                scope.reviewCandidates.mockClear();

                SpecHelper.expectNoElement(elem, 'button.reviewed');
            });
        });
    });

    describe('reviewCandidates', () => {
        it('should update location params', () => {
            render({
                title: 'Marketing Leader Extraordinaire',
                role: 'marketing',
                $numApplied: 4,
                $numReviewed: 1,
                archived: false,
                $status: 'awaiting_review',
            });
            jest.spyOn($location, 'search').mockImplementation(() => {});
            scope.reviewCandidates();
            expect($location.search.mock.calls[0][0]).toEqual('positionId', position.id);
            expect($location.search.mock.calls[1][0]).toEqual('action', 'review');
        });
    });
});
