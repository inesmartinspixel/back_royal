import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/careers_spec_helper';
import setSpecLocales from 'Translation/setSpecLocales';
import connectionBarButtonLocales from 'Careers/locales/careers/connection_bar_button-en.json';

setSpecLocales(connectionBarButtonLocales);

describe('FrontRoyal.Careers.ConnectionBarButton', () => {
    let $injector;
    let SpecHelper;
    let CareersSpecHelper;
    let renderer;
    let elem;
    let scope;
    let OpenPosition;
    let careersNetworkViewModel;
    let hiringRelationshipViewModel;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                OpenPosition = $injector.get('OpenPosition');
            },
        ]);

        SpecHelper.stubConfig();
        careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
        hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.hiringRelationshipViewModel = hiringRelationshipViewModel;
        renderer.render(
            '<connection-bar-button hiring-relationship-view-model="hiringRelationshipViewModel"></connection-bar-button>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should show basic info', () => {
        render();
        expect(hiringRelationshipViewModel.connectionAvatarSrc).not.toBeUndefined();
        expect(elem.find('careers-avatar').isolateScope().src).toEqual(hiringRelationshipViewModel.connectionAvatarSrc);

        SpecHelper.expectElementText(elem, '.name', hiringRelationshipViewModel.connectionName);
        SpecHelper.expectElementText(elem, '.company', hiringRelationshipViewModel.connectionCompany);
    });

    it('should have fancy bind-once that makes everything work with only one watcher', () => {
        // setup
        updateVm({
            connectionName: 'original',
            lastActivityAt: 1,
        });

        // render and check that the name is on the screen
        render();
        SpecHelper.expectElementText(elem, '.name', 'original');

        // changing the name should not change the display, because it is bind-once
        updateVm({
            connectionName: 'updated',
        });
        scope.$apply();
        SpecHelper.expectElementText(elem, '.name', 'original');

        // changing the lastActivityAt, however, should force a full
        // re-render, so now the name has updated in the display
        updateVm({
            lastActivityAt: 2,
        });
        scope.$apply();
        SpecHelper.expectElementText(elem, '.name', 'updated');
    });

    function updateVm(props) {
        _.each(props, (val, key) => {
            Object.defineProperty(hiringRelationshipViewModel, key, {
                value: val,
                configurable: true,
            });
        });
    }

    describe('new', () => {
        it('should NOT have new label', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate', {
                statusPairs: [['accepted', 'accepted']],
            });
            hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            render();
            SpecHelper.expectNoElement(elem, '.new');
        });

        it('should be new request', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate', {
                statusPairs: [['accepted', 'pending']],
            });
            hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            render();
            SpecHelper.expectElementText(elem, '.new', 'New Request');
            SpecHelper.expectHasClass(elem, '.connection', 'new-request');
        });

        it('should be new match', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate', {
                statusPairs: [['accepted', 'accepted']],
            });
            hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            jest.spyOn(hiringRelationshipViewModel, 'newInstantMatch', 'get').mockReturnValue(true);
            render();
            SpecHelper.expectElementText(elem, '.new', 'New Match');
            SpecHelper.expectHasClass(elem, '.connection', 'new-instant-match');
        });

        it('should be new message', () => {
            const Message = $injector.get('Mailbox.Message');
            $injector.get('MessageFixtures');
            hiringRelationshipViewModel = _.detect(
                careersNetworkViewModel.hiringRelationshipViewModels,
                'conversation',
            );
            hiringRelationshipViewModel.conversation.messages.push(
                Message.fixtures.getInstance({
                    sender_id: hiringRelationshipViewModel.connectionId,
                    receipts: [
                        {
                            receiver_id: careersNetworkViewModel.user.id,
                            is_read: false,
                        },
                    ],
                }),
            );
            render();
            SpecHelper.expectElementText(elem, '.new', 'New Message');
            SpecHelper.expectHasClass(elem, '.connection', 'new-message');
        });
    });

    describe('status', () => {
        it('should be invited to connect', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate', {
                statusPairs: [['accepted', 'pending']],
            });
            hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            jest.spyOn(hiringRelationshipViewModel, 'invitedPosition', 'get').mockReturnValue(
                OpenPosition.new({
                    id: '1',
                    title: 'open position title',
                }),
            );
            jest.spyOn(hiringRelationshipViewModel, 'hasOpenPositionId', 'get').mockReturnValue(true);
            render();
            SpecHelper.expectElementText(elem, '.status', 'Invited to Connect: open position title');
        });

        it('should be liked', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate', {
                statusPairs: [['accepted', 'pending']],
            });
            hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            render();
            SpecHelper.expectElementText(elem, '.status', 'Liked');
        });
    });

    describe('actions', () => {
        beforeEach(() => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            hiringRelationshipViewModel = _.detect(
                careersNetworkViewModel.hiringRelationshipViewModels,
                'conversation',
            );
        });

        it('should have chat button', () => {
            // mocking "connected" section layout
            render();
            SpecHelper.expectNoElement(elem, 'button.profile');
            SpecHelper.expectElement(elem, 'button.chat');
            SpecHelper.expectNoElement(elem, 'button.messages');
        });

        it('should have profile and messages buttons', () => {
            // mocking "closed" tab layout
            jest.spyOn(hiringRelationshipViewModel, 'closed', 'get').mockReturnValue(true);
            render();
            SpecHelper.expectElement(elem, 'button.profile');
            SpecHelper.expectNoElement(elem, 'button.chat');
            SpecHelper.expectElement(elem, 'button.messages');
        });

        it('should have profile button', () => {
            // mocking "closed" tab layout
            hiringRelationshipViewModel = _.reject(
                careersNetworkViewModel.hiringRelationshipViewModels,
                'conversation',
            )[0];
            jest.spyOn(hiringRelationshipViewModel, 'closed', 'get').mockReturnValue(true);
            render();
            SpecHelper.expectElement(elem, 'button.profile');
            SpecHelper.expectNoElement(elem, 'button.chat');
            SpecHelper.expectNoElement(elem, 'button.messages');
        });
    });
});
