import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/open_position_fixtures';
import setSpecLocales from 'Translation/setSpecLocales';
import positionFormLocales from 'Careers/locales/careers/position_form-en.json';
import positionsLocales from 'Careers/locales/careers/positions-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import chooseARoleLocales from 'Careers/locales/careers/choose_a_role-en.json';
import selectSkillsFormLocales from 'Careers/locales/careers/edit_career_profile/select_skills_form-en.json';
import writeTextAboutLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/write_text_about-en.json';
import salaryLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/salary-en.json';
import selectedPillsLocales from 'FrontRoyalForm/locales/front_royal_form/selected_pills-en.json';
import inputConstAutosuggestLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/input_const_autosuggest-en.json';

setSpecLocales(
    positionFormLocales,
    positionsLocales,
    fieldOptionsLocales,
    chooseARoleLocales,
    selectSkillsFormLocales,
    writeTextAboutLocales,
    salaryLocales,
    selectedPillsLocales,
    inputConstAutosuggestLocales,
);

describe('FrontRoyal.Careers.PositionForm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let currentUser;
    let DialogModal;
    let OpenPosition;
    let careersNetworkViewModel;
    let CareersNetworkViewModel;
    let position;
    let HiringTeamCheckoutHelper;
    let HiringTeam;
    let $location;
    let $rootScope;
    let $timeout;
    let $q;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.module($provide => {
            HiringTeamCheckoutHelper = function () {};
            $provide.value('Payments.HiringTeamCheckoutHelper', HiringTeamCheckoutHelper);
        });

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                OpenPosition = $injector.get('OpenPosition');
                $injector.get('OpenPositionFixtures');
                CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
                DialogModal = $injector.get('DialogModal');
                $location = $injector.get('$location');
                HiringTeam = $injector.get('HiringTeam');
                $rootScope = $injector.get('$rootScope');
                $q = $injector.get('$q');
                $timeout = $injector.get('$timeout');
                $injector.get('HiringTeamFixtures');

                SpecHelper.stubDirective('locationAutocomplete');
                SpecHelper.stubDirective('positionCard');

                $injector.get('OpenPositionFixtures');
                HiringTeamCheckoutHelper.prototype.subscribeToPayPerPostPlan = jest.fn().mockReturnValue($q.when());
            },
        ]);

        currentUser = SpecHelper.stubCurrentUser();
        currentUser.hiring_application = {};
        currentUser.hiring_team = HiringTeam.fixtures.getInstance({
            hiring_plan: HiringTeam.HIRING_PLAN_PAY_PER_POST, // setting this here will make sure the stripe plans get setup
        });
        careersNetworkViewModel = CareersNetworkViewModel.get('hiringManager');
        position = OpenPosition.fixtures.getInstance({
            hiring_manager_id: currentUser.id,
            hiring_application: currentUser.hiring_application,
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render(options = {}) {
        if (options.hasOwnProperty('canCreateFeaturedPositions')) {
            jest.spyOn(currentUser, 'canCreateFeaturedPositions', 'get').mockReturnValue(
                options.canCreateFeaturedPositions,
            );
        }

        renderer = SpecHelper.renderer();
        renderer.scope.position = options.position ? OpenPosition.fixtures.getInstance(options.position) : position;
        renderer.scope.hiringManager = currentUser;
        jest.spyOn(renderer.scope.position, 'locationString', 'get').mockReturnValue('Seattle, WA');

        if (options.saveCallback) {
            renderer.scope.saveCallback = options.saveCallback;
            renderer.render(
                '<position-form position="position" save-callback="saveCallback()" hiring-manager="hiringManager"></position-form>',
            );
        } else {
            renderer.render('<position-form position="position" hiring-manager="hiringManager"></position-form>');
        }

        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('welcome_to_smartly_talent alert', () => {
        it('should be shown on load if legacy, !canCreateFeaturedPositions && !has_drafted_open_position', () => {
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            currentUser.has_drafted_open_position = false;
            currentUser.hiring_team.hiring_plan = HiringTeam.HIRING_PLAN_LEGACY;

            render({
                canCreateFeaturedPositions: false,
            });

            expect(DialogModal.alert).toHaveBeenCalled();
        });

        it('should not be shown on load if not legacy hiring_plan', () => {
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            currentUser.has_drafted_open_position = false;
            currentUser.hiring_team.hiring_plan = 'another_plan';

            render({
                canCreateFeaturedPositions: false,
            });

            expect(DialogModal.alert).not.toHaveBeenCalled();
        });
    });

    describe('saveButtonText', () => {
        it('should be "Pay and Publish" on the pay_and_publish page', () => {
            currentUser.hiring_team.hiring_plan = null; // make sure we can get to this page
            $location.search('page', 2);
            render();
            SpecHelper.expectElementText(elem, 'button[name=submit]', 'Pay and Publish');
        });

        it('should be "Continue" on the success page', () => {
            currentUser.hiring_team.hiring_plan = HiringTeam.HIRING_PLAN_UNLIMITED_WITH_SOURCING; // make sure we can get to this page
            $location.search('page', 2);
            render();
            SpecHelper.expectElementText(elem, 'button[name=submit]', 'Continue');
        });

        describe('with hiringPlan = nil user', () => {
            beforeEach(() => {
                currentUser.hiring_team.hiring_plan = null;
            });

            it('should be "Continue" if creating a new position', () => {
                position.id = null;
                render();
                SpecHelper.expectElementText(elem, 'button[name=submit]', 'Continue');
            });

            it('should be "Continue" if updating an existing unpaid position', () => {
                position.id = 'positionId';
                jest.spyOn(currentUser, 'subscriptionForPosition').mockReturnValue(null);
                render();
                SpecHelper.expectElementText(elem, 'button[name=submit]', 'Continue');
            });
        });

        describe('with payPerPost user', () => {
            beforeEach(() => {
                currentUser.hiring_team.hiring_plan = HiringTeam.HIRING_PLAN_PAY_PER_POST;
            });

            it('should be "Continue" if creating a new position', () => {
                position.id = null;
                render();
                SpecHelper.expectElementText(elem, 'button[name=submit]', 'Continue');
            });

            it('should be "Update Job Post" if updating an existing paid position', () => {
                position.id = 'positionId';
                position.featured = true;
                jest.spyOn(currentUser, 'subscriptionForPosition').mockReturnValue({});
                render();
                SpecHelper.expectElementText(elem, 'button[name=submit]', 'Update Job Post');
            });

            it('should be "Continue" if updating an existing unpaid position', () => {
                position.id = 'positionId';
                jest.spyOn(currentUser, 'subscriptionForPosition').mockReturnValue(null);
                render();
                SpecHelper.expectElementText(elem, 'button[name=submit]', 'Continue');
            });
        });

        describe('with non-payPerPost, !canCreateFeaturedPositions user', () => {
            beforeEach(() => {
                currentUser.hiring_team.hiring_plan = 'not pay-per-post';
            });

            it('should be "Save" if creating a new position', () => {
                position.id = null;
                render({
                    canCreateFeaturedPositions: false,
                });
                SpecHelper.expectElementText(elem, 'button[name=submit]', 'Save');
            });

            it('should be "Save" if updating an existing position', () => {
                position.id = 'positionId';
                render({
                    canCreateFeaturedPositions: false,
                });
                SpecHelper.expectElementText(elem, 'button[name=submit]', 'Save');
            });
        });

        describe('with non-payPerPost, canCreateFeaturedPositions user', () => {
            beforeEach(() => {
                jest.spyOn(currentUser, 'canCreateFeaturedPositions', 'get').mockReturnValue(true);
                currentUser.hiring_team.hiring_plan = 'not pay-per-post';
            });

            // In this case, "Save as Draft" is visible on the other side
            it('should be "Publish" if creating a new position', () => {
                position.id = null;
                render({
                    canCreateFeaturedPositions: true,
                });
                SpecHelper.expectElementText(elem, 'button[name=submit]', 'Publish');
            });

            it('should be "Update Job Post" if updating an existing featured position', () => {
                position.id = 'positionId';
                position.featured = true;
                render({
                    canCreateFeaturedPositions: true,
                });
                SpecHelper.expectElementText(elem, 'button[name=submit]', 'Update Job Post');
            });

            it('should be "Publish" if updating an existing drafted position', () => {
                position.id = 'positionId';
                position.featured = false;
                render({
                    canCreateFeaturedPositions: true,
                });
                SpecHelper.expectElementText(elem, 'button[name=submit]', 'Publish');
            });
        });
    });

    describe('showSaveAsDraftButton', () => {
        beforeEach(() => {
            render();
        });

        it('should be false if position is featured', () => {
            position.featured = true;
            scope.$digest();
            expect(scope.showSaveAsDraftButton).toBe(false);
        });

        it('should be false if position is archived', () => {
            position.featured = false;
            position.archived = true;
            scope.$digest();
            expect(scope.showSaveAsDraftButton).toBe(false);
        });

        it('should be false if !hiringManager.canCreateFeaturedPositions', () => {
            position.featured = false;
            position.archived = false;
            jest.spyOn(currentUser, 'canCreateFeaturedPositions', 'get').mockReturnValue(false);
            scope.$digest();
            expect(scope.showSaveAsDraftButton).toBe(false);
        });

        it('should be false if hiringManager.postingJobsRequiresPayment', () => {
            position.featured = false;
            position.archived = false;
            jest.spyOn(currentUser, 'canCreateFeaturedPositions', 'get').mockReturnValue(true);
            jest.spyOn(currentUser, 'postingJobsRequiresPayment', 'get').mockReturnValue(true);
            scope.$digest();
            expect(scope.showSaveAsDraftButton).toBe(false);
        });

        it('should be true otherwise', () => {
            position.featured = false;
            position.archived = false;
            jest.spyOn(currentUser, 'canCreateFeaturedPositions', 'get').mockReturnValue(true);
            jest.spyOn(currentUser, 'postingJobsRequiresPayment', 'get').mockReturnValue(false);
            scope.$digest();
            expect(scope.showSaveAsDraftButton).toBe(true);
        });
    });

    describe('savePosition', () => {
        beforeEach(() => {
            render();
            position = scope.position;
            jest.spyOn(position, 'save').mockReturnValue($q.when());
            scope.hiringManager.has_drafted_open_position = true; // prevent user save by default

            jest.spyOn(careersNetworkViewModel, 'commitOpenPosition').mockImplementation();
        });

        describe('with saveAsDraft = true', () => {
            it('should set position.drafted_from_positions = true', () => {
                expect(position.drafted_from_positions).not.toBe(true);
                scope.savePosition(true);
                expect(position.drafted_from_positions).toBe(true);
                expect(position.save).toHaveBeenCalled();
            });

            describe('has_drafted_open_position', () => {
                it('should save user if changing has_drafted_open_position', () => {
                    scope.hiringManager.has_drafted_open_position = false;
                    jest.spyOn(scope.hiringManager, 'save').mockImplementation();
                    scope.savePosition(true);
                    expect(scope.hiringManager.has_drafted_open_position).toBe(true);
                    expect(scope.hiringManager.save).toHaveBeenCalled();
                });

                it('should not save user if currentUser is admin user', () => {
                    scope.hiringManager.has_drafted_open_position = false;
                    jest.spyOn($rootScope.currentUser, 'hasAdminAccess', 'get').mockReturnValue(true);
                    jest.spyOn(scope.hiringManager, 'save');
                    scope.savePosition(true);
                    expect(scope.hiringManager.has_drafted_open_position).toBe(false);
                    expect(scope.hiringManager.save).not.toHaveBeenCalled();
                });

                it('should not save user if not changing has_drafted_open_position', () => {
                    scope.hiringManager.has_drafted_open_position = false;
                    jest.spyOn($rootScope.currentUser, 'hasAdminAccess', 'get').mockReturnValue(true);
                    jest.spyOn(scope.hiringManager, 'save');
                    scope.savePosition(true);
                    expect(scope.hiringManager.has_drafted_open_position).toBe(false);
                    expect(scope.hiringManager.save).not.toHaveBeenCalled();
                });
            });

            describe('congrats_first_post modal', () => {
                beforeEach(() => {
                    jest.spyOn(DialogModal, 'alert');
                });

                it('should not be shown if has_drafted_open_position', () => {
                    scope.hiringManager.has_drafted_open_position = true;
                    jest.spyOn(scope.hiringManager, 'canCreateFeaturedPositions', 'get').mockReturnValue(false);
                    scope.savePosition(true);
                    $timeout.flush(); // flush save
                    expect(DialogModal.alert).not.toHaveBeenCalled();
                });

                it('should not be shown if canCreateFeaturedPositions', () => {
                    jest.spyOn(scope.hiringManager, 'canCreateFeaturedPositions', 'get').mockReturnValue(true);
                    scope.savePosition(true);
                    $timeout.flush(); // flush save
                    expect(DialogModal.alert).not.toHaveBeenCalled();
                });

                it('should navigate to review page when triggered', () => {
                    triggerModalAlert();

                    expect($location.search.mock.calls[0]).toEqual(['positionId', position.id]);
                    expect($location.search.mock.calls[1]).toEqual(['action', 'review']);
                    expect($location.search.mock.calls[2]).toEqual(['page', null]);
                });

                it('should handle addPosition callback', () => {
                    triggerModalAlert();
                    $location.search.mockClear();
                    DialogModal.alert.mock.calls[0][0].scope.addPosition();

                    expect(DialogModal.hideAlerts).toHaveBeenCalled();
                    expect($location.search.mock.calls[0]).toEqual(['action', 'create']);
                    expect($location.search.mock.calls[1]).toEqual(['positionId', null]);
                    expect($location.search.mock.calls[2]).toEqual(['list', null]);
                });

                it('should handle viewPosition callback', () => {
                    triggerModalAlert();
                    $location.search.mockClear();
                    DialogModal.alert.mock.calls[0][0].scope.viewPosition();

                    expect(DialogModal.hideAlerts).toHaveBeenCalled();
                    expect($location.search).not.toHaveBeenCalled();
                });

                function triggerModalAlert() {
                    scope.hiringManager.has_drafted_open_position = false;
                    jest.spyOn(scope.hiringManager, 'save').mockImplementation();
                    jest.spyOn(scope.hiringManager, 'canCreateFeaturedPositions', 'get').mockReturnValue(false);
                    jest.spyOn($location, 'search');
                    scope.savePosition(true);
                    $timeout.flush(); // flush save
                    expect(DialogModal.alert).toHaveBeenCalled();
                    jest.spyOn(DialogModal, 'hideAlerts').mockImplementation();
                }
            });
        });

        describe('with saveAsDraft = false', () => {
            it('should set featured to true', () => {
                expect(position.drafted_from_positions).not.toBe(true);
                scope.savePosition();
                expect(position.featured).toBe(true);
                expect(position.save).toHaveBeenCalled();
            });
            it('should not proactively set featured to true if archived', () => {
                position.featured = false;
                position.archived = true;
                scope.savePosition();
                expect(position.featured).toBe(false);
                expect(position.save).toHaveBeenCalled();
            });
        });

        it('should call commitOpenPosition after saving', () => {
            expect(position.drafted_from_positions).not.toBe(true);
            scope.savePosition();
            $timeout.flush(); // flush save
            expect(careersNetworkViewModel.commitOpenPosition).toHaveBeenCalled();
        });

        describe('with saveCallback', () => {
            it('should not call commitOpenPosition and should call the callback', () => {
                scope.saveCallback = jest.fn();
                scope.savePosition();
                $timeout.flush(); // flush save
                expect(careersNetworkViewModel.commitOpenPosition).not.toHaveBeenCalled();
                expect(scope.saveCallback).toHaveBeenCalled();
            });
        });
    });

    describe('submit', () => {
        let nextStep;

        beforeEach(() => {
            nextStep = jest.fn();
        });

        describe('with currentSection=add_position', () => {
            let postingJobsRequiresPayment;
            let subscriptionForPosition;
            let canCreateFeaturedPositions;

            beforeEach(() => {
                render();
                jest.spyOn(scope, 'savePosition').mockReturnValue($q.when());
                jest.spyOn(scope.hiringManager, 'postingJobsRequiresPayment', 'get').mockImplementation(
                    () => postingJobsRequiresPayment,
                );
                jest.spyOn(scope.hiringManager, 'subscriptionForPosition').mockImplementation(
                    () => subscriptionForPosition,
                );
                jest.spyOn(scope.hiringManager, 'canCreateFeaturedPositions', 'get').mockImplementation(
                    () => canCreateFeaturedPositions,
                );

                postingJobsRequiresPayment = false;
                subscriptionForPosition = null;
                canCreateFeaturedPositions = true;
            });

            it('should pass saveAsDraft through to savePosition', () => {
                scope.submit('add_position', nextStep, {
                    saveAsDraft: 'saveAsDraft',
                });
                expect(scope.savePosition).toHaveBeenCalledWith('saveAsDraft');
                expect(nextStep).not.toHaveBeenCalled();
                $timeout.flush(); // flush the save
                expect(nextStep).toHaveBeenCalled();
            });

            it('should default saveAsDraft to true if !canCreateFeaturedPositions', () => {
                render();
                canCreateFeaturedPositions = false;
                assertSavedWithSaveAsDraftValue(true);
            });

            it('should default saveAsDraft to true for pay-per-post user saving a position that does not yet have a subscription', () => {
                render();
                postingJobsRequiresPayment = true;
                subscriptionForPosition = null;
                assertSavedWithSaveAsDraftValue(true);
            });

            it('should not default saveAsDraft to true for pay-per-post user updating a position that has a subscription', () => {
                render();
                postingJobsRequiresPayment = true;
                subscriptionForPosition = {};
                assertSavedWithSaveAsDraftValue(false);
            });

            function assertSavedWithSaveAsDraftValue(saveAsDraft) {
                jest.spyOn(scope, 'savePosition').mockReturnValue($q.when());
                scope.submit('add_position', nextStep);
                expect(scope.savePosition).toHaveBeenCalledWith(saveAsDraft);
                expect(nextStep).not.toHaveBeenCalled();
                $timeout.flush(); // flush the save
                expect(nextStep).toHaveBeenCalled();
            }
        });

        describe('with currentSection=pay_and_publish', () => {
            it('should call subscribeToPayPerPostPlan', () => {
                render();
                scope.submit('pay_and_publish', nextStep);
                expect(HiringTeamCheckoutHelper.prototype.subscribeToPayPerPostPlan).toHaveBeenCalledWith(
                    scope.stripePlan.id,
                    scope.position.id,
                );
                expect(nextStep).not.toHaveBeenCalled();
                expect(scope.position.featured).toBe(false);

                $timeout.flush(); // flush the save
                expect(nextStep).toHaveBeenCalled();
                expect(scope.position.featured).toBe(true);
            });
        });

        describe('with currentSection=success', () => {
            it('should navigate to positions if no saveCallback', () => {
                render();
                jest.spyOn(scope, 'loadRoute');
                scope.submit('success', nextStep);
                expect(nextStep).not.toHaveBeenCalled();
                expect(scope.loadRoute).toHaveBeenCalledWith('/hiring/positions');
            });

            it('should do nothing if there is a saveCallback', () => {
                render();
                scope.saveCallback = jest.fn();
                jest.spyOn(scope, 'loadRoute');
                scope.submit('success', nextStep);
                expect(nextStep).not.toHaveBeenCalled();
                expect(scope.loadRoute).not.toHaveBeenCalled();
            });
        });
    });

    describe('save_as_draft button', () => {
        let postingJobsRequiresPayment;
        let canCreateFeaturedPositions;

        beforeEach(() => {
            render();
            postingJobsRequiresPayment = false;
            canCreateFeaturedPositions = true;
            scope.position.featured = false;
            jest.spyOn(scope.hiringManager, 'postingJobsRequiresPayment', 'get').mockImplementation(
                () => postingJobsRequiresPayment,
            );
            jest.spyOn(scope.hiringManager, 'canCreateFeaturedPositions', 'get').mockImplementation(
                () => canCreateFeaturedPositions,
            );
            scope.$digest();
            SpecHelper.expectElement(elem, '[name="draft"]');
        });

        it('should be hidden unless canCreateFeaturedPositions', () => {
            canCreateFeaturedPositions = false;
            scope.$digest();
            SpecHelper.expectNoElement(elem, '[name="draft"]');
        });

        it('should be hidden if position.featured', () => {
            scope.position.featured = true;

            // This is hackish, but in order to trigger the watcher, we need
            // to change something else as well
            canCreateFeaturedPositions = false;
            scope.$digest();
            canCreateFeaturedPositions = true;
            scope.$digest();
            SpecHelper.expectNoElement(elem, '[name="draft"]');
        });

        it('should be hidden if postingJobsRequiresPayment', () => {
            postingJobsRequiresPayment = true;
            scope.$digest();
            SpecHelper.expectNoElement(elem, '[name="draft"]');
        });

        it('should stay visible while publishing a position', () => {
            OpenPosition.expect('save');
            scope.savePosition(false);

            // even though saving updated featured to true, the draft button
            // should still be visible while saving
            expect(scope.position.featured).toBe(true);
            SpecHelper.expectElement(elem, '[name="draft"]');

            OpenPosition.flush('save');
            SpecHelper.expectNoElement(elem, '[name="draft"]');
        });
    });

    describe('stepsInfo', () => {
        it('should just be add_position for admins', () => {
            jest.spyOn(currentUser, 'hasAdminAccess', 'get').mockReturnValue(true);
            render();
            expect(_.pluck(scope.stepsInfo, 'key')).toEqual(['add_position']);
        });

        it('should also include the success screen when the user does not need to pay for positions', () => {
            jest.spyOn(currentUser, 'postingJobsRequiresPayment', 'get').mockReturnValue(false);
            render();
            expect(_.pluck(scope.stepsInfo, 'key')).toEqual(['add_position', 'success']);
        });

        it('should also include the success screen when the position already has a subscription', () => {
            jest.spyOn(currentUser, 'postingJobsRequiresPayment', 'get').mockReturnValue(true);
            jest.spyOn(currentUser, 'subscriptionForPosition').mockReturnValue({});
            render();
            expect(_.pluck(scope.stepsInfo, 'key')).toEqual(['add_position', 'success']);
            expect(currentUser.subscriptionForPosition).toHaveBeenCalledWith(position);
        });

        it('should also include pay_and_publish when the position requires payment', () => {
            jest.spyOn(currentUser, 'postingJobsRequiresPayment', 'get').mockReturnValue(true);
            jest.spyOn(currentUser, 'subscriptionForPosition').mockReturnValue(null);
            render();
            expect(_.pluck(scope.stepsInfo, 'key')).toEqual(['add_position', 'pay_and_publish', 'success']);
            expect(currentUser.subscriptionForPosition).toHaveBeenCalledWith(position);
        });
    });

    describe('startPage', () => {
        it('should allow for starting on a different page', () => {
            $location.search('startPage', 'pay_and_publish');
            jest.spyOn(currentUser, 'postingJobsRequiresPayment', 'get').mockReturnValue(true);
            jest.spyOn(currentUser, 'subscriptionForPosition').mockReturnValue(null);
            render();
            // sanity check
            expect(_.pluck(scope.stepsInfo, 'key')).toEqual(['add_position', 'pay_and_publish', 'success']);
            expect($location.search().startPage).toBeUndefined();
            expect($location.search().page).toEqual(2);
        });
    });

    it('should allow for starting on the success page, even if the posting is already paid for', () => {
        $location.search('page', '3');
        jest.spyOn(currentUser, 'postingJobsRequiresPayment', 'get').mockReturnValue(true);
        jest.spyOn(currentUser, 'subscriptionForPosition').mockReturnValue({});
        render();
        expect(_.pluck(scope.stepsInfo, 'key')).toEqual(['add_position', 'pay_and_publish', 'success']);
        SpecHelper.expectElement(elem, 'success-form');
    });

    describe('payment-source-section', () => {
        it('should show only on the pay and publish section', () => {
            $location.search('startPage', 'pay_and_publish');
            render();
            SpecHelper.expectElement(elem, '.payment-source-section');

            $location.search('startPage', 'add_position');
            render();
            SpecHelper.expectNoElement(elem, '.payment-source-section');
        });

        it('should show message only if hiringTeamSeemsToHavePaymentSource', () => {
            let hiringTeamSeemsToHavePaymentSource = false;
            jest.spyOn(currentUser, 'hiringTeamSeemsToHavePaymentSource', 'get').mockImplementation(
                () => hiringTeamSeemsToHavePaymentSource,
            );
            $location.search('startPage', 'pay_and_publish');
            render();
            SpecHelper.expectElementText(elem, '.payment-source-section', 'Secure Checkout');

            hiringTeamSeemsToHavePaymentSource = true;
            scope.$digest();

            // Modify is not here because this is not a hiring team owner
            SpecHelper.expectElementText(
                elem,
                '.payment-source-section',
                'Your saved payment method will be used. Secure Checkout',
            );
        });

        it('should have working modify button if user is hiring team owner', () => {
            jest.spyOn(currentUser, 'hiringTeamSeemsToHavePaymentSource', 'get').mockReturnValue(true);
            let isHiringTeamOwner = false;
            jest.spyOn(currentUser, 'isHiringTeamOwner', 'get').mockImplementation(() => isHiringTeamOwner);
            $location.search('startPage', 'pay_and_publish');
            render();

            // non owner does not see modify button
            SpecHelper.expectNoElement(elem, '[name="modify"]');

            isHiringTeamOwner = true;
            scope.$digest();
            scope.hiringTeamCheckoutHelper.modifyPaymentDetails = jest.fn();
            SpecHelper.click(elem, '[name="modify"]');
            expect(scope.hiringTeamCheckoutHelper.modifyPaymentDetails).toHaveBeenCalled();
        });
    });
});
