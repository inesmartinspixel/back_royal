import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import setSpecLocales from 'Translation/setSpecLocales';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import invalidFieldsLinksLocales from 'FrontRoyalForm/locales/front_royal_form/invalid_fields_links-en.json';
import writeTextAboutLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/write_text_about-en.json';
import selectedPillsLocales from 'FrontRoyalForm/locales/front_royal_form/selected_pills-en.json';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';
import careersLocales from 'Careers/locales/careers/careers-en.json';
import inputConstLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/input_const_autosuggest-en.json';
import previewCandidateCardLocales from 'Careers/locales/careers/preview_candidate_card-en.json';
import recommendedPositionsNotificationModalLocales from 'Careers/locales/careers/recommended_positions_notification_modal-en.json';
import frontRoyalFormLocales from 'FrontRoyalForm/locales/front_royal_form/front_royal_form-en.json';
import uploadAvatarLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/upload_avatar-en.json';
import locales1 from 'Careers/locales/careers/edit_career_profile/about_your_mba_form-en.json';
import locales2 from 'Careers/locales/careers/edit_career_profile/answers_too_short_modal-en.json';
import locales3 from 'Careers/locales/careers/edit_career_profile/basic_info_form-en.json';
import locales4 from 'Careers/locales/careers/edit_career_profile/cert_application_questions_form-en.json';
import locales5 from 'Careers/locales/careers/edit_career_profile/demographics_form_section-en.json';
import locales6 from 'Careers/locales/careers/edit_career_profile/education_experience_detail-en.json';
import locales7 from 'Careers/locales/careers/edit_career_profile/education_form-en.json';
import locales8 from 'Careers/locales/careers/edit_career_profile/emba_application_questions_form-en.json';
import locales9 from 'Careers/locales/careers/edit_career_profile/employer_preferences_form-en.json';
import locales10 from 'Careers/locales/careers/edit_career_profile/job_preferences_form-en.json';
import locales11 from 'Careers/locales/careers/edit_career_profile/mba_application_questions_form-en.json';
import locales12 from 'Careers/locales/careers/edit_career_profile/more_about_you_form-en.json';
import locales13 from 'Careers/locales/careers/edit_career_profile/personal_summary_form-en.json';
import locales14 from 'Careers/locales/careers/edit_career_profile/profile_photo_form_section-en.json';
import locales15 from 'Careers/locales/careers/edit_career_profile/program_choice_form-en.json';
import locales16 from 'Careers/locales/careers/edit_career_profile/program_lockdown_form-en.json';
import locales17 from 'Careers/locales/careers/edit_career_profile/resume_and_links_form-en.json';
import locales18 from 'Careers/locales/careers/edit_career_profile/salary_survey_form_section-en.json';
import locales19 from 'Careers/locales/careers/edit_career_profile/select_skills_form-en.json';
import locales20 from 'Careers/locales/careers/edit_career_profile/short_answer_scholarship_modal-en.json';
import locales21 from 'Careers/locales/careers/edit_career_profile/submit_application_form-en.json';
import locales22 from 'Careers/locales/careers/edit_career_profile/work_experience_detail-en.json';
import locales23 from 'Careers/locales/careers/edit_career_profile/work_form-en.json';

setSpecLocales(
    editCareerProfileLocales,
    fieldOptionsLocales,
    invalidFieldsLinksLocales,
    writeTextAboutLocales,
    selectedPillsLocales,
    candidateListCardLocales,
    careersLocales,
    inputConstLocales,
    previewCandidateCardLocales,
    recommendedPositionsNotificationModalLocales,
    frontRoyalFormLocales,
    uploadAvatarLocales,
    locales1,
    locales2,
    locales3,
    locales4,
    locales5,
    locales6,
    locales7,
    locales8,
    locales9,
    locales10,
    locales11,
    locales12,
    locales13,
    locales14,
    locales15,
    locales16,
    locales17,
    locales18,
    locales19,
    locales20,
    locales21,
    locales22,
    locales23,
);

describe('FrontRoyal.Careers.EditCareerProfile', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let $rootScope;
    let CareerProfile;
    let StudentDashboard;
    let Cohort;
    let user;
    let $timeout;
    let EditCareerProfileHelper;
    let $q;
    let ClientStorage;
    let $location;
    let DialogModal;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $rootScope = $injector.get('$rootScope');
                CareerProfile = $injector.get('CareerProfile');
                StudentDashboard = $injector.get('StudentDashboard');
                Cohort = $injector.get('Cohort');
                CareerProfile = $injector.get('CareerProfile');
                $timeout = $injector.get('$timeout');
                EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                $q = $injector.get('$q');
                ClientStorage = $injector.get('ClientStorage');
                $location = $injector.get('$location');
                DialogModal = $injector.get('DialogModal');

                $injector.get('CareerProfileFixtures');
                $injector.get('CohortFixtures');
            },
        ]);

        SpecHelper.stubCurrentUser();
        SpecHelper.stubConfig();
        SpecHelper.stubDirective('locationAutocomplete');
        SpecHelper.stubDirective('uploadAvatar');

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
        user.relevant_cohort = Cohort.fixtures.getInstance();
    });

    function render(
        steps = [
            {
                stepName: 'basic_info',
            },
            {
                stepName: 'personal_summary',
            },
        ],
        initialStep,
        stepsProgressMap,
    ) {
        StudentDashboard.expect('index').toBeCalledWith({
            get_has_available_incomplete_streams: true,
            get_has_available_incomplete_playlists: true,
            user_id: $rootScope.currentUser.id,
            filters: {},
        });

        renderer = SpecHelper.renderer();
        renderer.scope.steps = steps;
        renderer.scope.initialStep = initialStep;
        renderer.scope.stepsProgressMap = stepsProgressMap;
        renderer.render(
            '<edit-career-profile initial-step="initialStep" steps="steps" steps-progress-map="stepsProgressMap"></edit-career-profile>',
        );
        $timeout.flush(); // apply the scope.steps assignment

        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should set the page param if initialStep is provided', () => {
        jest.spyOn($location, 'search');
        render(
            [
                {
                    stepName: 'basic_info',
                },
                {
                    stepName: 'personal_summary',
                },
            ],
            2,
        );
        expect($location.search).toHaveBeenCalledWith({
            page: 2,
        });
    });

    it('should not show form header if not in the MBA Application context', () => {
        render();
        jest.spyOn(scope.helper, 'isMbaApplication').mockReturnValue(false);
        scope.$digest();
        SpecHelper.expectNoElement(elem, 'form-group.row.header');
    });

    describe('$on $locationChangeStart', () => {
        it('should display an alert if trying to leave a form with unsaved changes (dirty state)', () => {
            jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {});
            render([
                {
                    stepName: 'education',
                    requiredFields: {
                        education_experiences: {
                            min: 1,
                        },
                    },
                },
            ]);

            jest.spyOn(EditCareerProfileHelper, 'isApplicantEditor').mockReturnValue(true);
            scope.currentForm.$setDirty();
            const evt = scope.$broadcast('$locationChangeStart', '/path/to/form?page=2');
            expect(evt.defaultPrevented).toBe(true);
            expect(DialogModal.confirm).toHaveBeenCalledWith({
                text: 'You currently have unsaved changes. Are you sure you wish to navigate away?',
                confirmCallback: expect.any(Function),
            });
        });

        it('should not display an alert if trying to leave a form without unsaved changes (dirty state)', () => {
            jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {});
            render([
                {
                    stepName: 'education',
                    requiredFields: {
                        education_experiences: {
                            min: 1,
                        },
                    },
                },
            ]);

            jest.spyOn(EditCareerProfileHelper, 'isApplicantEditor').mockReturnValue(true);
            const evt = scope.$broadcast('$locationChangeStart', '/path/to/form?page=2');
            expect(evt.defaultPrevented).toBe(false);
            expect(DialogModal.confirm).not.toHaveBeenCalled();
        });

        it('should navigate to the new location on DialogModal confirm', () => {
            let confirmCallback;
            jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(true);
            SpecHelper.stubEventLogging();
            jest.spyOn(DialogModal, 'confirm').mockImplementation(opts => {
                confirmCallback = opts.confirmCallback;
            });
            render([
                {
                    stepName: 'education',
                    requiredFields: {
                        education_experiences: {
                            min: 1,
                        },
                    },
                },
            ]);

            const origCareerProfile = scope.careerProfile;
            scope.currentForm.$setDirty();
            origCareerProfile.changed = true;

            // $location.url will be called once with no args to
            // grab the intended url, and then again with args to set it
            jest.spyOn($location, 'url').mockImplementation(targetUrl => {
                if (!targetUrl) {
                    return '/path/to/new/page';
                }
                return targetUrl;
            });
            scope.$broadcast('$locationChangeStart');
            $location.url.mockClear();
            confirmCallback();
            expect($location.url).toHaveBeenCalledWith('/path/to/new/page');
            expect(scope.careerProfile).not.toBe(origCareerProfile);

            // the user's career profile should have been cloned onto the scope with the $leavingDirtyForm property
            expect(scope.careerProfile.changed).toBeUndefined();
            expect(scope.careerProfile.asJson()).toEqual(scope.currentUser.career_profile.asJson());
        });
    });

    describe('save', () => {
        beforeEach(() => {
            render();
        });

        it('should call saveForm', () => {
            jest.spyOn(scope, 'saveForm').mockImplementation(() => $q.when());
            scope.save();
            expect(scope.saveForm).toHaveBeenCalled();
        });

        it('should handle rejected promise from saveForm', () => {
            const deferred = $q.defer();
            jest.spyOn(scope, 'saveForm').mockImplementation(() => deferred.promise);
            jest.spyOn(angular, 'noop'); // angular.noop is passed in as the 'catch'
            scope.save();
            expect(angular.noop).not.toHaveBeenCalled();
            deferred.reject();
            scope.$digest(); // propagate rejected promise to 'catch'
            expect(angular.noop).toHaveBeenCalled();
        });

        it('should be called when save button is clicked', () => {
            jest.spyOn(scope, 'save').mockImplementation(angular.noop);
            SpecHelper.click(elem, 'button[name="save"]');
            expect(scope.save).toHaveBeenCalled();
        });
    });

    describe('saveForm', () => {
        it('should return rejected promise early if form is invalid', () => {
            render();
            scope.currentForm.$valid = false; // mock form validity
            const mockFn = jest.fn();
            scope.saveForm().catch(mockFn);
            expect(mockFn).not.toHaveBeenCalled();
            scope.$digest(); // propagate rejected promise to 'catch'
            expect(mockFn).toHaveBeenCalled();
        });

        it('should set ClientStorage item for saved step', () => {
            const stepName = 'resume_and_links';

            jest.spyOn(ClientStorage, 'setItem').mockImplementation(() => {});
            render([
                {
                    stepName,
                },
            ]);

            CareerProfile.expect('update');
            scope.saveForm();

            expect(ClientStorage.setItem).toHaveBeenCalledWith(`saved_${stepName}`, true);
        });

        it('should not display an alert if trying to navigate to the same step even while form is dirty', () => {
            jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {});
            render([
                {
                    stepName: 'select_skills',
                    requiredFields: {
                        skills: {
                            min: 4,
                        },
                    },
                },
            ]);

            SpecHelper.updateTextInput(elem, 'input-const-autosuggest input', 'some text');

            scope.$broadcast('gotoFormStep', $location.search().page - 1);
            expect(DialogModal.confirm).not.toHaveBeenCalled();
        });

        it('should log a completed-* event', () => {
            jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(true);
            jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});
            render([
                {
                    stepName: 'resume_and_links',
                },
            ]);

            SpecHelper.updateTextInput(elem, '[name=li-profile-url-input]', 'linkedin.com');

            const deferred = $q.defer();
            jest.spyOn(scope.careerProfile, 'save').mockReturnValue(deferred.promise);
            scope.saveForm();
            expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith('completed-resume_and_links');
        });

        it('should save the career profile', () => {
            render();
            scope.currentForm.$valid = true; // mock validity of formController
            jest.spyOn(scope, 'save');

            // mock out automatic changes that make it hard to assert
            jest.spyOn(scope.careerProfile, '_beforeSave').mockImplementation(() => {});
            const originalValue = scope.currentUser.hasActiveCareerProfile;
            Object.defineProperty(scope.currentUser, 'hasActiveCareerProfile', {
                get() {
                    return !originalValue;
                },
            });
            CareerProfile.expect('save').toBeCalledWith(scope.careerProfile).returns(scope.careerProfile);
            scope.saveForm();
            CareerProfile.flush('save');
            expect(scope.currentUser.hasActiveCareerProfile).toBe(!originalValue);
        });

        it('should set currentForm to pristine on successful save', () => {
            render();
            scope.currentForm.$valid = true; // mock validity of formController
            scope.$digest();

            jest.spyOn(scope.currentForm, '$setPristine').mockImplementation(() => {});
            CareerProfile.expect('save').toBeCalledWith(scope.careerProfile);
            scope.saveForm();
            CareerProfile.flush('save');
            expect(scope.currentForm.$setPristine).toHaveBeenCalled();
        });

        it("should set currentUser's career_profile to a copy of (not a reference to) the careerProfile proxy", () => {
            render();
            scope.currentForm.$valid = true; // mock validity of formController

            const careerProfile = CareerProfile.fixtures.getInstance();
            const careerProfileJson = scope.careerProfile.asJson();
            jest.spyOn(scope.careerProfile, 'asJson').mockReturnValue(careerProfileJson);
            jest.spyOn(CareerProfile, 'new').mockReturnValue(careerProfile);

            CareerProfile.expect('save').toBeCalledWith(scope.careerProfile);
            scope.saveForm();
            CareerProfile.flush('save');

            // references should be different since the careerProfile on the scope
            // is now a copy of the career_profile on the currentUser
            expect(CareerProfile.new).toHaveBeenCalledWith(careerProfileJson);
            expect(scope.currentUser.career_profile).toBe(careerProfile);
            expect(scope.currentUser.career_profile).not.toBe(scope.careerProfile);
        });

        it('should $broadcast savedCareerProfile event on successful save with program type value prior to save', () => {
            render();
            scope.currentForm.$valid = true; // mock validity of formController

            const broadcastSpy = jest.spyOn(scope, '$broadcast').mockImplementation(() => {});
            let programTypePriorToSave = scope.currentUser.career_profile.program_type;

            // Even though we get the programTypePriorToSave value from the career_profile on the currentUser, we change the program_type on
            // the careerProfile proxy rather than the career_profile on the currentuser because the careerProfile proxy is what gets saved,
            // so this assertion ensures that the program_type on the careerProfile proxy and programTypePriorToSave are the save value.
            expect(scope.careerProfile.program_type).toEqual(programTypePriorToSave);

            scope.careerProfile.program_type = 'foo';
            CareerProfile.expect('save').toBeCalledWith(scope.careerProfile);
            scope.saveForm();
            expect(broadcastSpy).not.toHaveBeenCalled();
            CareerProfile.flush('save');

            expect(broadcastSpy).toHaveBeenCalledWith('savedCareerProfile', programTypePriorToSave);

            // clear broadcastSpy in preparation for another save on the careerProfile to assert that the savedCareerProfile event
            // gets broadcast with the appropriate value if the user changes their program type multiple times
            broadcastSpy.mockClear();

            programTypePriorToSave = scope.currentUser.career_profile.program_type;
            // programTypePriorToSave should be 'foo' now because that's what was saved above and scope.currentUser.career_profile
            // should be set to a copy of the scope.careerProfile proxy by now.
            expect(programTypePriorToSave).toEqual('foo');

            scope.careerProfile.program_type = 'bar';
            CareerProfile.expect('save').toBeCalledWith(scope.careerProfile);
            scope.saveForm();
            expect(broadcastSpy).not.toHaveBeenCalled();
            CareerProfile.flush('save');

            expect(broadcastSpy).toHaveBeenCalledWith('savedCareerProfile', programTypePriorToSave);
        });
    });

    describe('saveAndNext', () => {
        it('should call saveForm and go to the next form when clicked', () => {
            render();
            const deferred = $q.defer();
            jest.spyOn(scope, 'saveForm').mockImplementation(() => deferred.promise);
            jest.spyOn(scope, 'saveAndNext');

            SpecHelper.click(elem, 'button[name="save-and-next"]');

            expect(scope.saveAndNext).toHaveBeenCalled();
            expect(scope.saveForm).toHaveBeenCalled();
            SpecHelper.expectElement(elem, `form[name="${scope.stepsInfo[0].key}"]`);
            SpecHelper.expectNoElement(elem, `form[name="${scope.stepsInfo[1].key}"]`);

            deferred.resolve();
            scope.$digest(); // propagate resolved promise to 'then' function

            SpecHelper.expectNoElement(elem, `form[name="${scope.stepsInfo[0].key}"]`);
            SpecHelper.expectElement(elem, `form[name="${scope.stepsInfo[1].key}"]`);
        });

        it('should handle rejected promise from saveForm', () => {
            render();
            jest.spyOn(scope, 'saveAndNext');
            const deferred = $q.defer();
            jest.spyOn(scope, 'saveForm').mockImplementation(() => deferred.promise);
            jest.spyOn(angular, 'noop'); // angular.noop is passed into the 'cattch'

            scope.saveAndNext();

            expect(angular.noop).not.toHaveBeenCalled();

            deferred.reject();
            scope.$digest();

            expect(angular.noop).toHaveBeenCalled();
        });

        it('should not show "save and next" button on last page when in the Edit Profile context', () => {
            jest.spyOn(EditCareerProfileHelper, 'isCareerProfile').mockReturnValue(true);
            SpecHelper.stubEventLogging();
            render([
                {
                    stepName: 'preview_profile',
                },
            ]);
            SpecHelper.expectNoElement(elem, 'button[name="save-and-next"]');
        });

        it('should not show "save and next" button on last page when in the Application context', () => {
            jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(true);
            SpecHelper.stubEventLogging();
            render([
                {
                    stepName: 'submit_application',
                },
            ]);
            SpecHelper.expectNoElement(elem, 'button[name="save-and-next"]');
        });

        it('should gotoFormStep for next incomplete form step when isCareerProfile, ignoring the nextStepCallback if there is an incomplete form step to go to', () => {
            const steps = [
                {
                    stepName: 'basic_info',
                },
                {
                    stepName: 'more_about_you',
                },
                {
                    stepName: 'career_preferences',
                },
            ];
            jest.spyOn(EditCareerProfileHelper, 'isCareerProfile').mockReturnValue(true);

            render(steps, 1);

            const deferred = $q.defer();
            jest.spyOn(scope, 'saveForm').mockReturnValue(deferred.promise);
            jest.spyOn(scope, 'currentFormIsIncomplete').mockImplementation(() => {});
            jest.spyOn(scope, 'getNextIncompleteFormStepIndex').mockReturnValue(2);
            jest.spyOn(scope, 'gotoFormStep').mockImplementation(() => {});
            const nextStepCallback = jest.fn();

            scope.saveAndNext(nextStepCallback);

            expect(scope.getNextIncompleteFormStepIndex).not.toHaveBeenCalled();
            expect(scope.gotoFormStep).not.toHaveBeenCalled();
            expect(nextStepCallback).not.toHaveBeenCalled();

            deferred.resolve();
            scope.$digest();

            expect(scope.getNextIncompleteFormStepIndex).toHaveBeenCalled();
            expect(scope.gotoFormStep).toHaveBeenCalledWith(2);
            expect(nextStepCallback).not.toHaveBeenCalled();
        });

        it('should go to the last form step when isCareerProfile, ignoring the nextStepCallback if the current form was the last incomplete form step', () => {
            const steps = [
                {
                    stepName: 'basic_info',
                },
                {
                    stepName: 'more_about_you',
                },
                {
                    stepName: 'career_preferences',
                },
            ];
            jest.spyOn(EditCareerProfileHelper, 'isCareerProfile').mockReturnValue(true);

            render(steps, 1);

            const deferred = $q.defer();
            jest.spyOn(scope, 'saveForm').mockReturnValue(deferred.promise);
            jest.spyOn(scope, 'currentFormIsIncomplete').mockReturnValue(true);
            jest.spyOn(scope, 'getNextIncompleteFormStepIndex').mockReturnValue(-1);
            jest.spyOn(scope, 'gotoFormStep').mockImplementation(() => {});
            const nextStepCallback = jest.fn();

            scope.saveAndNext(nextStepCallback);

            expect(scope.currentFormIsIncomplete).toHaveBeenCalled();
            expect(scope.getNextIncompleteFormStepIndex).not.toHaveBeenCalled();
            expect(scope.gotoFormStep).not.toHaveBeenCalled();
            expect(nextStepCallback).not.toHaveBeenCalled();

            deferred.resolve();
            scope.$digest();

            expect(scope.getNextIncompleteFormStepIndex).toHaveBeenCalled();
            expect(scope.gotoFormStep).toHaveBeenCalledWith(2);
            expect(nextStepCallback).not.toHaveBeenCalled();
        });

        it('should trigger nextStepCallback when isCareerProfile if there is no incomplete form step to go to and the current form already complete prior to save', () => {
            const steps = [
                {
                    stepName: 'basic_info',
                },
                {
                    stepName: 'more_about_you',
                },
                {
                    stepName: 'career_preferences',
                },
            ];
            jest.spyOn(EditCareerProfileHelper, 'isCareerProfile').mockReturnValue(true);

            render(steps, 1);

            const deferred = $q.defer();
            jest.spyOn(scope, 'saveForm').mockReturnValue(deferred.promise);
            jest.spyOn(scope, 'currentFormIsIncomplete').mockReturnValue(false);
            jest.spyOn(scope, 'getNextIncompleteFormStepIndex').mockReturnValue(-1);
            jest.spyOn(scope, 'gotoFormStep').mockImplementation(() => {});
            const nextStepCallback = jest.fn();

            scope.saveAndNext(nextStepCallback);

            expect(scope.currentFormIsIncomplete).toHaveBeenCalled();
            expect(scope.getNextIncompleteFormStepIndex).not.toHaveBeenCalled();
            expect(scope.gotoFormStep).not.toHaveBeenCalled();
            expect(nextStepCallback).not.toHaveBeenCalled();

            deferred.resolve();
            scope.$digest();

            expect(scope.getNextIncompleteFormStepIndex).toHaveBeenCalled();
            expect(scope.gotoFormStep).not.toHaveBeenCalled();
            expect(nextStepCallback).toHaveBeenCalled();
        });
    });

    describe('getNextIncompleteFormStepIndex', () => {
        it('should throw an error if no stepsProgressMap', () => {
            render();
            expect(scope.stepsProgressMap).toBeUndefined();
            expect(() => {
                scope.getNextIncompleteFormStepIndex();
            }).toThrowError(
                new Error('scope.stepsProgressMap must be defined to determine the next incomplete form step.'),
            );
        });

        it('should return the index of the first incomplete form step after the current form page', () => {
            const steps = [
                {
                    stepName: 'basic_info',
                },
                {
                    stepName: 'career_preferences',
                },
                {
                    stepName: 'more_about_you',
                },
                {
                    stepName: 'personal_summary',
                },
                {
                    stepName: 'select_skills',
                },
            ];
            const stepsProgressMap = {
                basic_info: 'incomplete',
                career_preferences: 'complete',
                more_about_you: 'complete',
                personal_summary: 'incomplete',
                select_skills: 'incomplete',
            };
            render(steps, 2, stepsProgressMap);

            expect($location.search().page).toEqual(2); // sanity check
            const value = scope.getNextIncompleteFormStepIndex();
            expect(value).toEqual(3);
            expect(scope.steps[value].stepName).toEqual('personal_summary');
        });

        it('should return the index of the first incomplete form step before the current form page if no incomplete form steps exist after the current form page', () => {
            const steps = [
                {
                    stepName: 'basic_info',
                },
                {
                    stepName: 'more_about_you',
                },
                {
                    stepName: 'select_skills',
                },
                {
                    stepName: 'career_preferences',
                },
                {
                    stepName: 'personal_summary',
                },
            ];
            const stepsProgressMap = {
                basic_info: 'complete',
                more_about_you: 'incomplete',
                select_skills: 'incomplete',
                career_preferences: 'complete',
                personal_summary: 'complete',
            };
            render(steps, 4, stepsProgressMap);

            expect($location.search().page).toEqual(4); // sanity check
            const value = scope.getNextIncompleteFormStepIndex();
            expect(value).toEqual(1);
            expect(scope.steps[value].stepName).toEqual('more_about_you');
        });

        it('shoud return -1 if no incomplete form steps are found', () => {
            const steps = [
                {
                    stepName: 'basic_info',
                },
                {
                    stepName: 'more_about_you',
                },
                {
                    stepName: 'career_preferences',
                },
                {
                    stepName: 'personal_summary',
                },
            ];
            const stepsProgressMap = {
                basic_info: 'complete',
                more_about_you: 'complete',
                career_preferences: 'complete',
                personal_summary: 'complete',
            };
            render(steps, 1, stepsProgressMap);

            expect($location.search().page).toEqual(1); // sanity check
            expect(scope.getNextIncompleteFormStepIndex()).toEqual(-1);
        });
    });

    describe('cancel', () => {
        it('should show cancel button if user is reapplyingOrEditingApplication', () => {
            jest.spyOn(user, 'reapplyingOrEditingApplication', 'get').mockReturnValue(true);
            render();
            SpecHelper.expectElement(elem, '[name="cancel"]');
        });

        it('should not show cancel button if user is not showPaidCertSelection', () => {
            jest.spyOn(user, 'showPaidCertSelection', 'get').mockReturnValue(false);
            render();
            SpecHelper.expectNoElement(elem, '[name="cancel"]');
        });

        it('should set reapplyingOrEditingApplication to false and navigate user to application_status page', () => {
            const reapplyingOrEditingApplicationSetterSpy = jest.spyOn(user, 'reapplyingOrEditingApplication', 'set');
            jest.spyOn($location, 'path');
            render();
            scope.cancel();
            expect(reapplyingOrEditingApplicationSetterSpy).toHaveBeenCalledWith(false);
            expect($location.path).toHaveBeenCalledWith('/settings/application_status');
        });
    });

    describe('when no currentUser', () => {
        it('should only show spinner', () => {
            render();
            SpecHelper.expectNoElement(elem, 'front-royal-spinner');
            SpecHelper.expectElement(elem, '[multi-step-container]');

            SpecHelper.stubNoCurrentUser();
            scope.$digest();

            SpecHelper.expectElement(elem, 'front-royal-spinner');
            SpecHelper.expectNoElement(elem, '[multi-step-container]');
        });
    });

    describe('$on gotoEditProfileSection', () => {
        it('should go to the form step for the specified section', () => {
            render();
            jest.spyOn(scope, 'gotoFormStep').mockImplementation(() => {});
            scope.$broadcast('gotoEditProfileSection', scope.steps[0].stepName);
            expect(scope.gotoFormStep).toHaveBeenCalledWith(0);
        });
    });
});
