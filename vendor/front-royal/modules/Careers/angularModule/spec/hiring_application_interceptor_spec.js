import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_application_fixtures';

describe('HiringApplicationInterceptor', () => {
    let SpecHelper;
    let $rootScope;
    let User;
    let HiringApplicationInterceptor;
    let HiringApplication;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('SpecHelper', 'FrontRoyal.Careers');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                $rootScope = $injector.get('$rootScope');
                HiringApplicationInterceptor = $injector.get('HiringApplicationInterceptor');
                User = $injector.get('User');
                HiringApplication = $injector.get('HiringApplication');

                $injector.get('HiringApplicationFixtures');
            },
        ]);
    });

    afterAll(() => {
        SpecHelper.cleanup();
    });

    describe('response', () => {
        it("should copy vanilla properties to user's hiring_application", () => {
            $rootScope.currentUser = User.new({
                a: 1,
                b: 2,
                hiring_application: {
                    status: 'pending',
                },
            });

            HiringApplicationInterceptor.response({
                data: {
                    meta: {
                        push_messages: {
                            hiring_application: {
                                status: 'accepted',
                            },
                        },
                    },
                },
            });
            expect($rootScope.currentUser.a).toEqual(1);
            expect($rootScope.currentUser.hiring_application.status).toEqual('accepted');
        });

        it('should not mess things up when there are other push messages', () => {
            $rootScope.currentUser = User.new({
                hiring_application: HiringApplication.new({
                    status: 'foo',
                }),
            });
            $rootScope.currentUser.hiring_application.status = 'changed';

            HiringApplicationInterceptor.response({
                data: {
                    meta: {
                        push_messages: {
                            something_else: {},
                        },
                    },
                },
            });

            // we had an issue where copyAttrs called with undefined would
            // blow this away
            expect($rootScope.currentUser.hiring_application.status).toEqual('changed');
        });
    });
});
