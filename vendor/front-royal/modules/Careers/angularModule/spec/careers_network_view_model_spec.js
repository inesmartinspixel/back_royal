import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/candidate_position_interest_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_relationship_fixtures';
import 'Careers/angularModule/spec/careers_spec_helper';
import setSpecLocales from 'Translation/setSpecLocales';
import careersNetworkViewModelLocales from 'Careers/locales/careers/careers_network_view_model-en.json';

setSpecLocales(careersNetworkViewModelLocales);

describe('FrontRoyal.Careers.CareersNetworkViewModel', () => {
    let $injector;
    let SpecHelper;
    let CareersSpecHelper;
    let $timeout;
    let HiringRelationship;
    let CareersNetworkViewModel;
    let $rootScope;
    let DialogModal;
    let ClientStorage;
    let $q;
    let CareerProfile;
    let $location;
    let CandidatePositionInterest;
    let guid;
    let HiringTeam;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper', 'NoUnhandledRejectionExceptions');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                $q = $injector.get('$q');
                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                $injector.get('MockIguana');
                $timeout = $injector.get('$timeout');
                HiringRelationship = $injector.get('HiringRelationship');
                $injector.get('HiringRelationshipFixtures');
                CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
                $rootScope = $injector.get('$rootScope');
                DialogModal = $injector.get('DialogModal');
                ClientStorage = $injector.get('ClientStorage');
                $injector.get('MessageFixtures');
                CareerProfile = $injector.get('CareerProfile');
                $injector.get('CareerProfileFixtures');
                $location = $injector.get('$location');
                CandidatePositionInterest = $injector.get('CandidatePositionInterest');
                guid = $injector.get('guid');
                HiringTeam = $injector.get('HiringTeam');

                const statusPoller = $injector.get('FrontRoyal.StatusPoller');
                jest.spyOn(statusPoller, 'send').mockImplementation(() => {}); // unexpected pings can cause errors when we flush $interval, so mock it out

                $injector.get('CandidatePositionInterestFixtures');
                SpecHelper.stubCurrentUser();
            },
        ]);
    });

    describe('initialize', () => {
        describe('_preloadHiringRelationships', () => {
            beforeEach(() => {
                jest.spyOn(CareersNetworkViewModel.prototype, 'ensureHiringRelationshipsLoaded').mockReturnValue(
                    $q.when(),
                );
                jest.spyOn(CareersNetworkViewModel.prototype, '_setupIntervals').mockImplementation(() => {});
            });

            it('should load myCandidates for candidates', () => {
                jest.spyOn($rootScope.currentUser, 'hasCareersNetworkAccess', 'get').mockReturnValue(true);
                const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate', {
                    preload: true,
                });
                $timeout.flush(); // preloading only happens after a delay
                expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded).toHaveBeenCalledWith('myCandidates');
                expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded).not.toHaveBeenCalledWith(
                    'teamCandidates',
                );
            });

            it('should load myCandidates and teamCandidates for hiring managers', () => {
                const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                    preload: true,
                });
                $timeout.flush(); // preloading only happens after a delay
                expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded).toHaveBeenCalledWith('teamCandidates');
            });

            it('should not be called if !hasCareersNetworkAccess', () => {
                jest.spyOn($rootScope.currentUser, 'hasCareersNetworkAccess', 'get').mockReturnValue(false);
                const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate', {
                    preload: true,
                });
                expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded).not.toHaveBeenCalled();
            });
        });

        describe('_setupIntervals', () => {
            it('should reset the cache every hour', () => {
                const $interval = $injector.get('$interval');
                const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate', {
                    preload: true,
                });
                jest.spyOn(CareersNetworkViewModel.prototype, 'resetCache').mockImplementation(() => {});
                $interval.flush(60 * 60 * 1000 - 1);
                expect(careersNetworkViewModel.resetCache).not.toHaveBeenCalled();
                $interval.flush(1);
                expect(careersNetworkViewModel.resetCache).toHaveBeenCalled();
            });
        });
    });

    describe('ensureHiringRelationshipsLoaded', () => {
        let careersNetworkViewModel;

        beforeEach(() => {
            careersNetworkViewModel = null;
        });

        describe('hiringManager', () => {
            beforeEach(() => {
                setupCareersNetworkViewModel('hiringManager');
                careersNetworkViewModel.user.hiring_team_id = guid.generate();
            });

            it('should load up featured view models', () => {
                setupCareersNetworkViewModel('hiringManager');

                ensureHiringRelationshipsFetched('featured', {
                    hiring_manager_status: 'pending',
                });
                ensureHiringRelationshipsPulledFromCache('featured');
            });

            it('should load up saved view models', () => {
                setupCareersNetworkViewModel('hiringManager');

                ensureHiringRelationshipsFetched('saved', {
                    hiring_manager_status: 'saved_for_later',
                    open_position_id: null,
                });
                ensureHiringRelationshipsPulledFromCache('saved');
            });

            it('should load up rejected view models', () => {
                setupCareersNetworkViewModel('hiringManager');

                ensureHiringRelationshipsFetched('rejected', {
                    hiring_manager_id: careersNetworkViewModel.user.id,
                    hiring_manager_status: 'rejected',
                    candidate_status: 'hidden',
                    closed: false,
                });
                ensureHiringRelationshipsPulledFromCache('rejected');
            });

            it('should load up closed view models', () => {
                setupCareersNetworkViewModel('hiringManager');

                ensureHiringRelationshipsFetched('closed', {
                    closed: true,
                });
                ensureHiringRelationshipsPulledFromCache('closed');
            });

            it('should load up teamCandidates view models', () => {
                SpecHelper.stubCurrentUser().hiring_team_id = 'theBrowns';
                setupCareersNetworkViewModel('hiringManager');

                ensureHiringRelationshipsFetched('teamCandidates', {
                    OUR_STATUS: ['accepted', 'saved_for_later'],
                    CONNECTION_STATUS: ['accepted', 'pending', 'hidden'],
                });
                ensureHiringRelationshipsPulledFromCache('teamCandidates');
            });

            it('should not load up candidates if hiringManager is without a team', () => {
                setupCareersNetworkViewModel('hiringManager');
                careersNetworkViewModel.user.hiring_team_id = undefined;

                careersNetworkViewModel._loadHiringRelationships.mockClear();
                careersNetworkViewModel.ensureHiringRelationshipsLoaded('teamCandidates');
                $timeout.flush();
                expect(careersNetworkViewModel._loadHiringRelationships).not.toHaveBeenCalled();
            });

            // FIXME
            it('should not make a request when team view models are requested fro a hiringManger with no teams', () => {
                const callback = jest.fn();
                setupCareersNetworkViewModel('hiringManager');
                careersNetworkViewModel.user.hiring_team_id = null;

                careersNetworkViewModel._loadHiringRelationships.mockClear();
                careersNetworkViewModel.ensureHiringRelationshipsLoaded('teamCandidates').then(callback);
                expect(careersNetworkViewModel._loadHiringRelationships).not.toHaveBeenCalled();
                $timeout.flush();
                expect(callback).toHaveBeenCalled();
            });

            describe('independent arg', () => {
                describe('when true', () => {
                    it('should add promise to _independentHiringRelationshipListLoadPromises', () => {
                        setupCareersNetworkViewModel('hiringManager');

                        expect(
                            careersNetworkViewModel._dependentHiringRelationshipListLoadPromises.saved,
                        ).toBeUndefined();
                        expect(
                            careersNetworkViewModel._independentHiringRelationshipListLoadPromises.saved,
                        ).toBeUndefined();

                        const independent = true;
                        const promise = ensureHiringRelationshipsFetched(
                            'saved',
                            {
                                hiring_manager_status: 'saved_for_later',
                                open_position_id: null,
                            },
                            independent,
                        );

                        expect(
                            careersNetworkViewModel._dependentHiringRelationshipListLoadPromises.saved,
                        ).toBeUndefined();
                        expect(
                            careersNetworkViewModel._independentHiringRelationshipListLoadPromises.saved,
                        ).toBeDefined();
                        expect(careersNetworkViewModel._independentHiringRelationshipListLoadPromises.saved).toEqual(
                            promise,
                        );

                        const newPromise = ensureHiringRelationshipsPulledFromCache('saved', independent);
                        expect(newPromise).toEqual(promise);
                    });
                });

                describe('when false', () => {
                    it('should add promise to _dependentHiringRelationshipListLoadPromises', () => {
                        setupCareersNetworkViewModel('hiringManager');

                        expect(
                            careersNetworkViewModel._dependentHiringRelationshipListLoadPromises.saved,
                        ).toBeUndefined();
                        expect(
                            careersNetworkViewModel._independentHiringRelationshipListLoadPromises.saved,
                        ).toBeUndefined();

                        const independent = false;
                        const promise = ensureHiringRelationshipsFetched(
                            'saved',
                            {
                                hiring_manager_status: 'saved_for_later',
                                open_position_id: null,
                            },
                            independent,
                        );

                        expect(
                            careersNetworkViewModel._independentHiringRelationshipListLoadPromises.saved,
                        ).toBeUndefined();
                        expect(
                            careersNetworkViewModel._dependentHiringRelationshipListLoadPromises.saved,
                        ).toBeDefined();
                        expect(careersNetworkViewModel._dependentHiringRelationshipListLoadPromises.saved).toEqual(
                            promise,
                        );

                        const newPromise = ensureHiringRelationshipsPulledFromCache('saved', independent);
                        expect(newPromise).toEqual(promise);
                    });
                });
            });
        });

        describe('candidate', () => {
            it('should load up closed candidate models', () => {
                setupCareersNetworkViewModel('candidate');

                ensureHiringRelationshipsFetched('closed', {
                    rejected_by_candidate_or_closed: true,
                });
                ensureHiringRelationshipsPulledFromCache('closed');
            });

            it('should load up myCandidates view models', () => {
                setupCareersNetworkViewModel('candidate');

                ensureHiringRelationshipsFetched('myCandidates', {
                    hiring_manager_status: ['accepted'],
                    candidate_status: ['accepted', 'pending', 'hidden'],
                    closed: false,
                });
                ensureHiringRelationshipsPulledFromCache('myCandidates');
            });
        });

        function setupCareersNetworkViewModel(role) {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel(role, {
                addHiringRelationships: false,
            });
            jest.spyOn(careersNetworkViewModel, '_loadHiringRelationships').mockReturnValue($q.when());

            // empty out the cache so we can start from scratch
            careersNetworkViewModel._dependentHiringRelationshipListLoadPromises = {};
            careersNetworkViewModel._independentHiringRelationshipListLoadPromises = {};
            careersNetworkViewModel.hiringRelationships = undefined;
            careersNetworkViewModel.hiringRelationshipViewModels = undefined;
        }

        function ensureHiringRelationshipsPulledFromCache(prop, independent) {
            careersNetworkViewModel._loadHiringRelationships.mockClear();
            const promise = careersNetworkViewModel.ensureHiringRelationshipsLoaded(prop, independent);
            $timeout.flush();
            // Since the results for this request are cached, it should not
            // be necessary to make another request
            expect(careersNetworkViewModel._loadHiringRelationships).not.toHaveBeenCalled();
            return promise;
        }

        function ensureHiringRelationshipsFetched(prop, filters, independent) {
            careersNetworkViewModel._loadHiringRelationships.mockClear();
            const promise = careersNetworkViewModel.ensureHiringRelationshipsLoaded(prop, independent);
            $timeout.flush();
            expect(careersNetworkViewModel._loadHiringRelationships).toHaveBeenCalledWith(filters);
            return promise;
        }
    });

    describe('loading hiring relationships', () => {
        it('should set hasNotYetLikedFirstCandidate', () => {
            SpecHelper.stubCurrentUser();
            $rootScope.currentUser.hiring_team_id = guid.generate();
            HiringRelationship.expect('index').returns({
                result: [],
                meta: {
                    has_liked_candidate: true,
                },
            });
            const vm = new CareersNetworkViewModel('hiringManager');
            vm.ensureHiringRelationshipsLoaded('teamCandidates');
            HiringRelationship.flush('index');
            expect(vm.hasNotYetLikedFirstCandidate).toBe(false);
        });
    });

    describe('bumpLastRelationshipUpdatedAt', () => {
        it('should update lastRelationshipUpdatedAt if updated_at on passed in hiringRelationship is greater than self.lastRelationshipUpdatedAt', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const hiringRelationship = careersNetworkViewModel.hiringRelationshipViewModels[0].hiringRelationship;

            const time = Date.now();
            careersNetworkViewModel.lastRelationshipUpdatedAt = time - 1000;
            hiringRelationship.updated_at = time; // updated_at on hiring relationship is now greater than lastRelationshipUpdatedAt

            expect(careersNetworkViewModel.lastRelationshipUpdatedAt).not.toEqual(time);
            careersNetworkViewModel.bumpLastRelationshipUpdatedAt(hiringRelationship);
            expect(careersNetworkViewModel.lastRelationshipUpdatedAt).toEqual(time);
        });

        it('should not update lastRelationshipUpdatedAt if updated_at on passed in hiringRelationship is less than self.lastRelationshipUpdatedAt', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const hiringRelationship = careersNetworkViewModel.hiringRelationshipViewModels[0].hiringRelationship;

            const time = Date.now();
            careersNetworkViewModel.lastRelationshipUpdatedAt = time;
            hiringRelationship.updated_at = time - 1000; // updated_at on hiring relationship is less than lastRelationshipUpdatedAt

            expect(careersNetworkViewModel.lastRelationshipUpdatedAt).toEqual(time);
            careersNetworkViewModel.bumpLastRelationshipUpdatedAt(hiringRelationship);
            expect(careersNetworkViewModel.lastRelationshipUpdatedAt).not.toEqual(time - 1000);
            expect(careersNetworkViewModel.lastRelationshipUpdatedAt).toEqual(time);
        });
    });

    describe('onUnloadedHiringRelationshipChange', () => {
        it('should create new hiring relationship view models as needed', () => {
            SpecHelper.stubCurrentUser();
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const hiringRelationship = HiringRelationship.fixtures.getInstance({
                hiring_manager_id: $rootScope.currentUser.id,
                updated_at: 1,
            });
            jest.spyOn(careersNetworkViewModel, '_loadHiddenRelationships').mockReturnValue($q.when());
            HiringRelationship.expect('index').returns({
                result: [hiringRelationship],
                meta: {},
            });
            careersNetworkViewModel.onUnloadedHiringRelationshipChange(42);
            HiringRelationship.flush('index');
            const hiringRelationshipViewModel = _.detect(
                careersNetworkViewModel.hiringRelationshipViewModels,
                vm => vm.hiringRelationship.id === hiringRelationship.id,
            );
            expect(hiringRelationshipViewModel).not.toBe(null);
        });

        it('should update existing hiring relationship view models as needed', () => {
            SpecHelper.stubCurrentUser();
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const origHiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            const updatedHiringRelationshipAttrs = _.extend(
                origHiringRelationshipViewModel.hiringRelationship.asJson(),
                {
                    update: 'received',
                    updated_at: origHiringRelationshipViewModel.hiringRelationship.updated_at + 1,
                },
            );
            jest.spyOn(careersNetworkViewModel, '_loadHiddenRelationships').mockReturnValue($q.when());
            HiringRelationship.expect('index').returns({
                result: [updatedHiringRelationshipAttrs],
                meta: {},
            });
            careersNetworkViewModel.onUnloadedHiringRelationshipChange(42);
            HiringRelationship.flush('index');

            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            // the same view model should still be attached
            expect(hiringRelationshipViewModel).toBe(origHiringRelationshipViewModel);

            // but it should have the new hiringRelationship
            expect(hiringRelationshipViewModel.hiringRelationship.update).toEqual('received');
        });

        it('should load up deletions and remove them', () => {
            SpecHelper.stubCurrentUser();
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const origHiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            const updatedHiringRelationshipAttrs = _.extend(
                origHiringRelationshipViewModel.hiringRelationship.asJson(),
                {
                    hiring_manager_status: 'hidden', // since this is now hidden, it should be removed
                    updated_at: origHiringRelationshipViewModel.hiringRelationship.updated_at + 1,
                },
            );
            jest.spyOn(careersNetworkViewModel, '_loadUpdatedRelationships').mockReturnValue($q.when());
            HiringRelationship.expect('index').returns({
                result: [updatedHiringRelationshipAttrs],
                meta: {},
            });
            careersNetworkViewModel.onUnloadedHiringRelationshipChange(42);
            $timeout.flush(); // flush _loadUpdatedRelationships
            HiringRelationship.flush('index');

            const hiringRelationshipViewModel = _.detect(
                careersNetworkViewModel.hiringRelationshipViewModels,
                vm => vm.hiringRelationship.id === origHiringRelationshipViewModel.hiringRelationship.id,
            );
            expect(hiringRelationshipViewModel).toBeUndefined();
        });

        it('should show a modal to a hiring manager if there is a new match', () => {
            SpecHelper.stubCurrentUser();
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            jest.spyOn(careersNetworkViewModel, '_showModalForHiringRelationship').mockImplementation(() => {});
            const hiringRelationship = careersNetworkViewModel.testHelper.getRelationshipsByStatus()
                .waiting_on_candidate;
            reloadRelationship(
                careersNetworkViewModel,
                _.extend(hiringRelationship.asJson(), {
                    candidate_status: 'accepted',
                }),
            );
            expect(careersNetworkViewModel._showModalForHiringRelationship).toHaveBeenCalled();
            expect(careersNetworkViewModel._showModalForHiringRelationship.mock.calls[0][0].id).toEqual(
                hiringRelationship.id,
            );
        });

        it('should not show a modal to a hiring manager if a teammate has a new match', () => {
            SpecHelper.stubCurrentUser();
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                teammateId: 'teammate',
            });
            jest.spyOn(careersNetworkViewModel, '_showModalForHiringRelationship').mockImplementation(() => {});
            const hiringRelationship = careersNetworkViewModel.testHelper.getRelationshipsByStatus()
                .waiting_on_candidate;
            reloadRelationship(
                careersNetworkViewModel,
                _.extend(hiringRelationship.asJson(), {
                    candidate_status: 'accepted',
                }),
            );
            expect(careersNetworkViewModel._showModalForHiringRelationship).not.toHaveBeenCalled();
        });

        it('should not show a modal to a hiring manager if there is no new match', () => {
            SpecHelper.stubCurrentUser();
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            jest.spyOn(careersNetworkViewModel, '_showModalForHiringRelationship').mockImplementation(() => {});
            const hiringRelationship = careersNetworkViewModel.testHelper.getRelationshipsByStatus()
                .waiting_on_candidate;

            // if we just reload the relationship and it has not changed, then there
            // should be no popup
            reloadRelationship(careersNetworkViewModel, hiringRelationship);
            expect(careersNetworkViewModel._showModalForHiringRelationship).not.toHaveBeenCalled();
        });

        it('should show a modal to a candidate if there is a new request', () => {
            SpecHelper.stubCurrentUser();
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            jest.spyOn(careersNetworkViewModel, '_showModalForHiringRelationship').mockImplementation(() => {});
            const hiringRelationship = HiringRelationship.fixtures.getInstance({
                candidate_id: $rootScope.currentUser.id,
                hiring_manager_status: 'accepted',
                candidate_status: 'pending',
            });
            reloadRelationship(careersNetworkViewModel, hiringRelationship);
            expect(careersNetworkViewModel._showModalForHiringRelationship).toHaveBeenCalled();
            expect(careersNetworkViewModel._showModalForHiringRelationship.mock.calls[0][0].id).toEqual(
                hiringRelationship.id,
            );
        });

        it('should not show a modal to a candidate if there is no new request', () => {
            SpecHelper.stubCurrentUser();
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            jest.spyOn(careersNetworkViewModel, '_showModalForHiringRelationship').mockImplementation(() => {});
            const hiringRelationship = careersNetworkViewModel.testHelper.getRelationshipsByStatus()
                .waiting_on_candidate;

            // if we just reload the relationship and it has not changed, then there
            // should be no popup
            reloadRelationship(careersNetworkViewModel, hiringRelationship);
            expect(careersNetworkViewModel._showModalForHiringRelationship).not.toHaveBeenCalled();
        });

        function reloadRelationship(careersNetworkViewModel, hiringRelationship) {
            const expectedParams = careersNetworkViewModel._getHiringRelationshipIndexParams({
                updated_since: 42,
            });
            jest.spyOn(careersNetworkViewModel, '_loadHiddenRelationships').mockReturnValue($q.when());
            HiringRelationship.expect('index')
                .toBeCalledWith(expectedParams)
                .returns({
                    result: [hiringRelationship],
                });
            careersNetworkViewModel.onUnloadedHiringRelationshipChange(42);
            HiringRelationship.flush('index');
        }
    });

    describe('hideRelatedCandidatePositionInterests', () => {
        let careersNetworkViewModel;

        beforeEach(() => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
        });

        it("should short-circuit and return a promise with an empty array if ourStatus on the hiring relationship view-model isn't accepted", () => {
            const hiringRelationshipViewModel = _.first(careersNetworkViewModel.hiringRelationshipViewModels);
            jest.spyOn(hiringRelationshipViewModel, 'ourStatus', 'get').mockReturnValue('saved_for_later');
            jest.spyOn(careersNetworkViewModel, 'ensureCandidatePositionInterests');
            jest.spyOn(careersNetworkViewModel, 'ensureOpenPositions');
            const originalCandidatePositionInterests = angular.copy(careersNetworkViewModel.candidatePositionInterests);
            let relatedInterests;

            careersNetworkViewModel
                .hideRelatedCandidatePositionInterests(hiringRelationshipViewModel)
                .then(_relatedInterests => {
                    relatedInterests = _relatedInterests;
                });
            $timeout.flush(); // propogate promise returned by hideRelatedCandidatePositionInterests to `then` callback

            expect(relatedInterests).toEqual([]);
            expect(careersNetworkViewModel.ensureCandidatePositionInterests).not.toHaveBeenCalled();
            expect(careersNetworkViewModel.ensureOpenPositions).not.toHaveBeenCalled();
            expect(careersNetworkViewModel.candidatePositionInterests).toEqual(originalCandidatePositionInterests);
        });

        it('should ensureCandidatePositionInterests, ensureOpenPositions, and then remove related interests from candidatePositionInterests when ourStatus is accepted', () => {
            const hiringRelationshipViewModel = _.first(careersNetworkViewModel.hiringRelationshipViewModels);
            const relatedInterest = CandidatePositionInterest.fixtures.getInstance({
                id: 1,
                candidate_id: hiringRelationshipViewModel.connectionId,
            });
            const firstUnrelatedInterest = CandidatePositionInterest.fixtures.getInstance({
                id: 2,
            });
            const secondUnrelatedInterest = CandidatePositionInterest.fixtures.getInstance({
                id: 3,
                candidate_id: hiringRelationshipViewModel.connectionId,
            });
            const thirdUnrelatedInterest = CandidatePositionInterest.fixtures.getInstance({
                id: 4,
            });
            careersNetworkViewModel.candidatePositionInterests = [
                relatedInterest,
                firstUnrelatedInterest,
                secondUnrelatedInterest,
                thirdUnrelatedInterest,
            ];
            jest.spyOn(relatedInterest, 'hiddenOrReviewedByHiringManager', 'get').mockReturnValue(false);
            jest.spyOn(secondUnrelatedInterest, 'hiddenOrReviewedByHiringManager', 'get').mockReturnValue(true);
            jest.spyOn(hiringRelationshipViewModel, 'ourStatus', 'get').mockReturnValue('accepted');
            jest.spyOn(careersNetworkViewModel, 'ensureCandidatePositionInterests').mockReturnValue($q.when());
            jest.spyOn(careersNetworkViewModel, 'ensureOpenPositions').mockReturnValue($q.when());
            let relatedInterests;

            careersNetworkViewModel
                .hideRelatedCandidatePositionInterests(hiringRelationshipViewModel)
                .then(_relatedInterests => {
                    relatedInterests = _relatedInterests;
                });
            $timeout.flush(); // propogate promise returned by hideRelatedCandidatePositionInterests to `then` callback

            expect(relatedInterests).toEqual([relatedInterest]);
            expect(careersNetworkViewModel.ensureCandidatePositionInterests).toHaveBeenCalled();
            expect(careersNetworkViewModel.ensureOpenPositions).toHaveBeenCalled();
            expect(careersNetworkViewModel.candidatePositionInterests).toEqual([
                firstUnrelatedInterest,
                secondUnrelatedInterest,
                thirdUnrelatedInterest,
            ]);
        });
    });

    describe('_showModalForHiringRelationship', () => {
        it('should work', () => {
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationship = careersNetworkViewModel.hiringRelationships[0];
            careersNetworkViewModel._showModalForHiringRelationship(hiringRelationship);
            expect(DialogModal.alert).toHaveBeenCalled();
            // make sure we alerted with info about the correct relationship
            expect(DialogModal.alert.mock.calls[0][0].scope.connectionId).toEqual(hiringRelationship.hiring_manager_id);
        });
    });

    describe('nagIfNecessary', () => {
        let careersNetworkViewModel;

        beforeEach(() => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            jest.spyOn(careersNetworkViewModel, '_nag').mockImplementation(() => {});
        });

        it('should show a modal if never nagged', () => {
            careersNetworkViewModel.connectionsNumNotifications = 42;
            ClientStorage.removeItem('careersLastNaggedAt');
            careersNetworkViewModel.nagIfNecessary();
            expect(careersNetworkViewModel._nag).toHaveBeenCalled();
        });

        it('should show a modal if there are notifications and the last nag was more than an hour ago', () => {
            careersNetworkViewModel.connectionsNumNotifications = 42;
            ClientStorage.setItem('careersLastNaggedAt', Date.now() - 3600001);
            careersNetworkViewModel.nagIfNecessary();
            expect(careersNetworkViewModel._nag).toHaveBeenCalled();
        });

        it('should not show a modal if recently nagged', () => {
            careersNetworkViewModel.connectionsNumNotifications = 42;
            ClientStorage.setItem('careersLastNaggedAt', Date.now() - 1000000);
            careersNetworkViewModel.nagIfNecessary();
            expect(careersNetworkViewModel._nag).not.toHaveBeenCalled();
        });

        it('should not show a modal if no notifications', () => {
            careersNetworkViewModel.connectionsNumNotifications = 0;
            ClientStorage.setItem('careersLastNaggedAt', 1);
            careersNetworkViewModel.nagIfNecessary();
            expect(careersNetworkViewModel._nag).not.toHaveBeenCalled();
        });
    });

    describe('_nag', () => {
        it('should show a modal and set careersLastNaggedAt', () => {
            jest.spyOn(Date, 'now').mockReturnValue(42);
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            careersNetworkViewModel._nag();
            expect(DialogModal.alert).toHaveBeenCalled();
            expect(ClientStorage.getItem('careersLastNaggedAt')).toEqual('42');
        });
    });

    describe('notifications', () => {
        describe('connectionsNumNotifications', () => {
            it('should set the correct notification number for a learner', () => {
                const vm = CareersSpecHelper.stubCareersNetworkViewModel('candidate', {
                    hiringRelationshipCount: 6,
                    statusPairs: [
                        ['accepted', 'pending'], // (1st notification, new request)
                        ['accepted', 'accepted'], // (2nd notification, see conversationOverrides)
                        ['accepted', 'accepted'], // (3rd notification, see conversationOverrides)
                        ['accepted', 'accepted'], // (4th notification, see conversationOverrides)
                        ['accepted', 'accepted'], // (5th notification, see conversationOverrides)
                        ['accepted', 'accepted'], // (no notification, see conversationOverrides)
                    ],
                    conversationOverrides: [
                        // (2nd notification) match with back-and-forth conversation and an unread message for candidate (2nd notification)
                        {
                            messages: [
                                {
                                    from: 'me',
                                },
                                {
                                    from: 'them',
                                    isRead: false,
                                },
                            ],
                        },
                        // (3rd notification) match with all read messages sent from hiring manager, but no response ever sent from candidate
                        {
                            messages: [
                                {
                                    from: 'them',
                                    isRead: true,
                                },
                            ],
                        },
                        // (4th notification) match with only messages sent from hiring manager and an unread message for candidate
                        {
                            messages: [
                                {
                                    from: 'them',
                                    isRead: true,
                                },
                                {
                                    from: 'them',
                                    isRead: false,
                                },
                            ],
                        },
                        // (5th notification) no conversation
                        null,

                        // (no notification) match with last message sent from user
                        {
                            messages: [
                                {
                                    from: 'them',
                                    isRead: true,
                                },
                                {
                                    from: 'me',
                                    isRead: true,
                                },
                            ],
                        },
                    ],
                });

                vm.calculateNumNotifications();
                expect(vm.connectionsNumNotifications).toBe(5);
            });

            it('should set the correct notification number for a hiring manager', () => {
                const vm = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                    hiringRelationshipCount: 3,
                    statusPairs: [['accepted', 'accepted']],
                    conversationOverrides: [
                        // newMatch, since there is no conversation
                        null,

                        // is unread
                        {
                            messages: [
                                {
                                    from: 'me',
                                },
                                {
                                    from: 'them',
                                    isRead: false,
                                },
                            ],
                        },

                        // is read
                        {
                            messages: [
                                {
                                    from: 'them',
                                    isRead: true,
                                },
                            ],
                        },
                    ],
                });

                // 1 new match
                vm.calculateNumNotifications();
                expect(vm.connectionsNumNotifications).toBe(2);
            });

            it('should set the correct notification number for someone with no notifications', () => {
                SpecHelper.stubCurrentUser();
                const vm = CareersSpecHelper.stubCareersNetworkViewModel('candidate', {
                    hiringRelationshipCount: 2,
                    statusPairs: [['pending', 'pending']],
                });
                vm.calculateNumNotifications();
                expect(vm.connectionsNumNotifications).toBe(0);
            });
        });

        describe('featuredPositionsNumNotifications', () => {
            it('should be 1 when hasSeenFeaturedPositionsPage===false', () => {
                SpecHelper.stubCurrentUser();
                $rootScope.currentUser.hasSeenFeaturedPositionsPage = false;
                const vm = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
                vm.setFeaturedPositionsNumNotifications();
                expect(vm.featuredPositionsNumNotifications).toEqual(1);
            });

            it('should be 0 when hasSeenFeaturedPositionsPage===true and on the featured positions page', () => {
                SpecHelper.stubCurrentUser();
                $rootScope.currentUser.hasSeenFeaturedPositionsPage = true;
                jest.spyOn($location, 'path').mockReturnValue('/hiring/positions');
                const vm = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
                vm.setFeaturedPositionsNumNotifications();
                expect(vm.featuredPositionsNumNotifications).toEqual(0);
            });

            it('should call setFeaturedPositionsNumNotifications when user.hasSeenFeaturedPositionsPage changes', () => {
                SpecHelper.stubCurrentUser();
                $rootScope.currentUser.hiring_team_id = guid.generate();
                const vm = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');

                $rootScope.currentUser.hasSeenFeaturedPositionsPage = false;
                $rootScope.$digest();
                jest.spyOn(vm, 'setFeaturedPositionsNumNotifications').mockImplementation(() => {});
                $rootScope.currentUser.hasSeenFeaturedPositionsPage = true;
                $rootScope.$digest();
                expect(vm.setFeaturedPositionsNumNotifications).toHaveBeenCalled();
            });

            it('should call setFeaturedPositionsNumNotifications when the location changes', () => {
                SpecHelper.stubCurrentUser();
                const vm = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');

                let path = 1;
                jest.spyOn($location, 'path').mockImplementation(() => path);
                $rootScope.$digest();
                jest.spyOn(vm, 'setFeaturedPositionsNumNotifications').mockImplementation(() => {});
                path = 2;
                $rootScope.$digest();
                expect(vm.setFeaturedPositionsNumNotifications).toHaveBeenCalled();
            });

            it('should be the # of unseen candidate_positions_interests', () => {
                SpecHelper.stubCurrentUser();
                $rootScope.currentUser.hasSeenFeaturedPositionsPage = true;

                const vm = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');

                CandidatePositionInterest.expect('index').returns([
                    CandidatePositionInterest.new({
                        hiring_manager_id: $rootScope.currentUser.id,
                        open_position_id: guid.generate(),
                        candidate_status: 'accepted',
                        hiring_manager_status: 'unseen',
                    }),
                    CandidatePositionInterest.new({
                        hiring_manager_id: $rootScope.currentUser.id,
                        open_position_id: guid.generate(),
                        candidate_status: 'accepted',
                        hiring_manager_status: 'pending',
                    }),
                    CandidatePositionInterest.new({
                        hiring_manager_id: $rootScope.currentUser.id,
                        open_position_id: guid.generate(),
                        candidate_status: 'accepted',
                        hiring_manager_status: 'reviewed',
                    }),
                ]);

                vm.setFeaturedPositionsNumNotifications();
                CandidatePositionInterest.flush('index');
                expect(vm.featuredPositionsNumNotifications).toEqual(1);
            });

            it('should be undefined for candidates', () => {
                SpecHelper.stubCurrentUser();
                const vm = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
                vm.setFeaturedPositionsNumNotifications();
                expect(vm.featuredPositionsNumNotifications).toBe(undefined);
            });
        });

        it('should set the correct notification number for a hiring teammate', () => {
            const promise = $q(() => {});
            jest.spyOn(CareersNetworkViewModel.prototype, 'ensureHiringRelationshipsLoaded').mockReturnValue(promise);

            SpecHelper.stubCurrentUser().hiring_team_id = 'theBrowns';
            const vm = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                teammateId: 'teammate',
                hiringTeamId: 'theBrowns',
                preload: true,
                hiringRelationshipCount: 3,
                statusPairs: [['accepted', 'accepted']],
                conversationOverrides: [
                    // newMatch, since there is no conversation.
                    // NOTE: should create notification for teammate.
                    null,

                    // is unread
                    // NOTE: 'me' in the first message is actually the teammate, see
                    //  spec_helper.stubCareersNetworkViewModel
                    // NOTE: our current user drops into the conversation "before"
                    //  the second message, from 'them' has been sent. This should
                    //  result in a notificaiton for the currentUser
                    {
                        messages: [
                            {
                                from: 'me', // note this is actually the teammate
                            },
                            {
                                from: 'them',
                                isRead: false,
                                droppedIn: true, // this is when our user
                                droppedInUserId: $rootScope.currentUser.id,
                            },
                        ],
                    },

                    // is unread
                    // NOTE: 'me' in the first message is actually the teammate, see
                    //  spec_helper.stubCareersNetworkViewModel
                    // NOTE: since we have not dropped into the conversation, this
                    //  should result in a notificaiton for the currentUser because
                    //  it comes from the teammate's newMatch
                    {
                        messages: [
                            {
                                from: 'me', // note this is actually the teammate
                            },
                            {
                                from: 'them',
                                isRead: false,
                            },
                        ],
                    },

                    // is read
                    // NOTE: obviously should not create notification for teammate.
                    {
                        messages: [
                            {
                                from: 'them',
                                isRead: true,
                            },
                        ],
                    },
                ],
            });
            $timeout.flush(); // preloading only happens after a delay
            expect(vm.ensureHiringRelationshipsLoaded).toHaveBeenCalledWith('teamCandidates');
            expect(vm.connectionsNumNotifications).toBe(3);
        });
    });

    describe('getMyHiringRelationshipViewModelForConnectionId', () => {
        it('should work if stuff is already loaded', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            let hiringRelationshipViewModel;
            const connectionId =
                careersNetworkViewModel.hiringRelationshipViewModels[0].hiringRelationship.hiring_manager_id;
            careersNetworkViewModel.getMyHiringRelationshipViewModelForConnectionId(connectionId).then(result => {
                hiringRelationshipViewModel = result;
            });
            $timeout.flush();
            expect(hiringRelationshipViewModel.hiringRelationship.hiring_manager_id).toEqual(connectionId);
        });

        it('should work if stuff is not already loaded', () => {
            let hiringRelationship;

            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate', {
                addHiringRelationships: false,
            });
            const expectedHiringRelationship = HiringRelationship.fixtures.getInstance({
                candidate_id: careersNetworkViewModel.user.id,
            });

            // send out a request
            HiringRelationship.expect('index').returns([expectedHiringRelationship]);
            careersNetworkViewModel.ensureHiringRelationshipsLoaded('myCandidates');
            careersNetworkViewModel
                .getMyHiringRelationshipViewModelForConnectionId(expectedHiringRelationship.hiring_manager_id)
                .then(result => {
                    hiringRelationship = result.hiringRelationship;
                });

            $timeout.flush();
            // Since the request has not returned, getHiringRelationshipViewModelForConnectionId should
            // not yet resolve
            expect(hiringRelationship).toBeUndefined();

            HiringRelationship.flush('index');
            expect(hiringRelationship.id).toEqual(expectedHiringRelationship.id);
        });

        it('should not try loading the profile if checkApiIfNotFound=false', () => {
            const callback = jest.fn();
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            jest.spyOn(careersNetworkViewModel, '_loadHiringRelationships').mockReturnValue($q.when());
            careersNetworkViewModel
                .getMyHiringRelationshipViewModelForConnectionId('unfoundConnectionId', false)
                .then(callback);
            $timeout.flush();
            expect(careersNetworkViewModel._loadHiringRelationships).not.toHaveBeenCalled();
            expect(callback).toHaveBeenCalled();
        });

        it('should try loading the profile if checkApiIfNotFound=true', () => {
            const callback = jest.fn();
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            jest.spyOn(careersNetworkViewModel, '_loadHiringRelationships').mockReturnValue($q.when());
            careersNetworkViewModel
                .getMyHiringRelationshipViewModelForConnectionId('unfoundConnectionId', true)
                .then(callback);
            $timeout.flush();
            expect(careersNetworkViewModel._loadHiringRelationships).toHaveBeenCalledWith({
                CONNECTION_ID: 'unfoundConnectionId',
            });
            expect(callback).toHaveBeenCalled();
        });

        it('should not return a relationship for a teammate', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                teammateId: 'teammate',
            });
            let hiringRelationshipViewModel;
            const connectionId =
                careersNetworkViewModel.hiringRelationshipViewModels[0].hiringRelationship.candidate_id;
            careersNetworkViewModel.getMyHiringRelationshipViewModelForConnectionId(connectionId).then(result => {
                hiringRelationshipViewModel = result;
            });
            $timeout.flush();
            expect(hiringRelationshipViewModel).toBeUndefined();
        });
    });

    describe('getHiringRelationshipViewModelAcceptedByTeam', () => {
        it('should work if stuff is already loaded', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            let hiringRelationshipViewModel;
            const connectionId = careersNetworkViewModel.testHelper.getRelationshipsByStatus().matched
                .hiring_manager_id;
            careersNetworkViewModel.getHiringRelationshipViewModelAcceptedByTeam(connectionId).then(result => {
                hiringRelationshipViewModel = result;
            });
            $timeout.flush();
            expect(hiringRelationshipViewModel.hiringRelationship.hiring_manager_id).toEqual(connectionId);
        });

        it('should work if stuff is not already loaded', () => {
            let hiringRelationship;

            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate', {
                addHiringRelationships: false,
            });
            const expectedHiringRelationship = HiringRelationship.fixtures.getInstance({
                candidate_id: careersNetworkViewModel.user.id,
                hiring_manager_status: 'accepted',
                candidate_status: 'accepted',
            });

            // send out a request
            HiringRelationship.expect('index').returns([expectedHiringRelationship]);
            careersNetworkViewModel.ensureHiringRelationshipsLoaded('myCandidates');
            careersNetworkViewModel
                .getHiringRelationshipViewModelAcceptedByTeam(expectedHiringRelationship.hiring_manager_id)
                .then(result => {
                    hiringRelationship = result.hiringRelationship;
                });

            $timeout.flush();
            // Since the request has not returned, getHiringRelationshipViewModelForConnectionId should
            // not yet resolve
            expect(hiringRelationship).toBeUndefined();

            HiringRelationship.flush('index');
            expect(hiringRelationship.id).toEqual(expectedHiringRelationship.id);
        });

        it('should not try loading the profile if checkApiIfNotFound=false', () => {
            const callback = jest.fn();
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            jest.spyOn(careersNetworkViewModel, '_loadHiringRelationships').mockReturnValue($q.when());
            careersNetworkViewModel
                .getHiringRelationshipViewModelAcceptedByTeam('unfoundConnectionId', false)
                .then(callback);
            $timeout.flush();
            expect(careersNetworkViewModel._loadHiringRelationships).not.toHaveBeenCalled();
            expect(callback).toHaveBeenCalled();
        });

        it('should try loading the profile if checkApiIfNotFound=true', () => {
            const callback = jest.fn();
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            jest.spyOn(careersNetworkViewModel, '_loadHiringRelationships').mockReturnValue($q.when());
            careersNetworkViewModel
                .getHiringRelationshipViewModelAcceptedByTeam('unfoundConnectionId', true)
                .then(callback);
            $timeout.flush();
            expect(careersNetworkViewModel._loadHiringRelationships).toHaveBeenCalledWith({
                CONNECTION_ID: 'unfoundConnectionId',
            });
            expect(callback).toHaveBeenCalled();
        });

        it('should not return an unmatched relationship', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            let hiringRelationshipViewModel;
            const connectionId = careersNetworkViewModel.testHelper.getRelationshipsByStatus().waiting_on_candidate
                .hiring_manager_id;
            careersNetworkViewModel.getMyHiringRelationshipViewModelForConnectionId(connectionId).then(result => {
                hiringRelationshipViewModel = result;
            });
            $timeout.flush();
            expect(hiringRelationshipViewModel).toBeUndefined();
        });
    });

    describe('acceptance messaging', () => {
        beforeEach(() => {
            SpecHelper.stubCurrentUser();
            $rootScope.currentUser.hiring_application = {
                status: 'pending',
            };
        });

        function setupAcceptance() {
            CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                hiringRelationshipCount: 1, // a single relationship, with ['pending', 'hidden'] (waiting_on_hiring_manager) status
            });
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            $rootScope.currentUser.hiring_application.status = 'accepted';
            $rootScope.$digest();
        }

        it('should take an unlimited-with-sourcing user to their home', () => {
            jest.spyOn($rootScope.currentUser, 'hiringPlan', 'get').mockReturnValue(
                HiringTeam.HIRING_PLAN_UNLIMITED_WITH_SOURCING,
            );
            jest.spyOn($location, 'url');
            setupAcceptance();
            expect($location.url).toHaveBeenCalledWith('/home');
        });

        it('should display an alert if the hiring_application status is changed to accepted when legacy', () => {
            jest.spyOn($rootScope.currentUser, 'hiringPlan', 'get').mockReturnValue(HiringTeam.HIRING_PLAN_LEGACY);
            setupAcceptance();
            expect(DialogModal.alert).toHaveBeenCalledWith({
                content:
                    '<hiring-manager-accepted-modal message="message" button-text="buttonText"></hiring-manager-accepted-modal>',
                title: "You're In!",
                scope: {
                    message: "You've been approved. Start reviewing candidates now.",
                    buttonText: 'Review Candidates',
                },
                size: 'small',
                classes: ['blue'],
                blurTargetSelector: 'div[ng-controller]',
            });
        });

        it('should not display an alert if the hiring_application status is changed to accepted but they already have swiped connections', () => {
            CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                hiringRelationshipCount: 2, // one of these relationships is rejected_by_hiring_manager
            });

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            $rootScope.currentUser.hiring_application.status = 'accepted';
            Object.defineProperty($rootScope.currentUser, 'hasCompleteHiringApplication', {
                value: true,
            });
            $rootScope.$digest();

            expect(DialogModal.alert).not.toHaveBeenCalled();
        });
    });

    describe('existing data refresh', () => {
        it("should be triggered on the user's avatar_url update", () => {
            const currentUser = SpecHelper.stubCurrentUser();

            currentUser.hiring_application = {
                avatar_url: currentUser.avatar_url,
            };
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                conversationOverrides: [
                    {
                        messages: [
                            {
                                from: 'me',
                            },
                        ],
                    },
                ],
            });
            $rootScope.$digest();

            jest.spyOn(CareersNetworkViewModel.prototype, '_refreshExistingData');

            // setup
            const hiringRelationshipViewModel = _.detect(
                careersNetworkViewModel.hiringRelationshipViewModels,
                'conversation',
            );
            const conversation = hiringRelationshipViewModel.conversation;
            const sentMessage = conversation.messages[0];

            // sanity checks
            expect(sentMessage).not.toBeUndefined();
            expect(sentMessage.sender.id).toBe(currentUser.id);
            expect(sentMessage.sender.avatar_url).toBe(currentUser.avatar_url);
            expect(hiringRelationshipViewModel).not.toBeUndefined();
            expect(hiringRelationshipViewModel.ourAvatarSrc).toBe(currentUser.avatar_url);
            expect(currentUser.hiring_application.avatar_url).toBe(currentUser.avatar_url);

            // update avatar
            currentUser.avatar_url = 'new_url';
            $rootScope.$digest();

            // verify
            expect(sentMessage.sender.avatar_url).toBe('new_url');
            expect(hiringRelationshipViewModel.ourAvatarSrc).toBe('new_url');
            expect(currentUser.hiring_application.avatar_url).toBe('new_url');
        });
    });

    describe('getOrInitializeHiringRelationshipViewModelForCareerProfile', () => {
        it('should return existing view model', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const expectedViewModel = _.first(careersNetworkViewModel.hiringRelationshipViewModels);

            let viewModel;
            careersNetworkViewModel
                .getOrInitializeHiringRelationshipViewModelForCareerProfile(expectedViewModel.careerProfile)
                .then(_viewModel => {
                    viewModel = _viewModel;
                });
            $timeout.flush(); // flush getHiringRelationshipViewModelForConnectionId
            expect(viewModel).toBe(expectedViewModel);
        });

        it('should initialize a new relationship and view model if none exist', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');

            const careerProfile = CareerProfile.fixtures.getInstance();
            let viewModel;
            careersNetworkViewModel
                .getOrInitializeHiringRelationshipViewModelForCareerProfile(careerProfile)
                .then(_viewModel => {
                    viewModel = _viewModel;
                });
            $timeout.flush(); // flush getHiringRelationshipViewModelForConnectionId

            expect(viewModel.connectionId).toEqual(careerProfile.user_id);
            expect(viewModel.hiringRelationship.isNew()).toBe(true);

            // before the relationship is saved, it should not be included in the
            // careersNetworkViewModel
            expect(_.contains(careersNetworkViewModel.hiringRelationships, viewModel.hiringRelationship)).toBe(false);
            expect(_.contains(careersNetworkViewModel.hiringRelationshipViewModels, viewModel)).toBe(false);
            expect(viewModel.careersNetworkViewModel).toBe(null);

            HiringRelationship.expect('save');
            viewModel.save();
            HiringRelationship.flush('save');

            expect(_.contains(careersNetworkViewModel.hiringRelationships, viewModel.hiringRelationship)).toBe(true);
            expect(
                _.chain(careersNetworkViewModel.hiringRelationshipViewModels)
                    .pluck('hiringRelationshipId')
                    .contains(viewModel.hiringRelationshipId)
                    .value(),
            ).toBe(true);
            expect(viewModel.careersNetworkViewModel).toBe(careersNetworkViewModel);
        });

        it('should pass foundWithSearchId and checkApiIfNotFound through', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const careerProfile = CareerProfile.fixtures.getInstance();

            jest.spyOn(careersNetworkViewModel, 'getMyHiringRelationshipViewModelForConnectionId');
            let viewModel;
            careersNetworkViewModel
                .getOrInitializeHiringRelationshipViewModelForCareerProfile(careerProfile, {
                    foundWithSearchId: 'searchId',
                })
                .then(_viewModel => {
                    viewModel = _viewModel;
                });
            $timeout.flush(); // flush getHiringRelationshipViewModelForConnectionId
            expect(careersNetworkViewModel.getMyHiringRelationshipViewModelForConnectionId).toHaveBeenCalledWith(
                careerProfile.user_id,
                false,
            );

            jest.spyOn(viewModel.hiringRelationship, 'save').mockReturnValue($q.when());
            viewModel.save();
            expect(viewModel.hiringRelationship.save).toHaveBeenCalled();
            expect(viewModel.hiringRelationship.save.mock.calls[0][0].found_with_search_id).toEqual('searchId');
        });
    });

    describe('showShareModal', () => {
        it('should open a dialog modal', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            const careerProfile = CareerProfile.fixtures.getInstance();
            careersNetworkViewModel.showShareModal(careerProfile);
            expect(DialogModal.alert).toHaveBeenCalled();
            const args = DialogModal.alert.mock.calls[0];
            expect(args[0].scope.careerProfile).toEqual(careerProfile);

            const onCloseCallback = args[0].scope.onCloseCallback;
            jest.spyOn(DialogModal, 'removeAlerts').mockImplementation(() => {});
            onCloseCallback();
            expect(DialogModal.removeAlerts).toHaveBeenCalled();
        });

        describe('onCloseCallback', () => {
            describe('when openHiringTeamInviteModal', () => {
                it('should open HiringTeamInviteModal', () => {
                    const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
                    jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});

                    const viewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
                    const HiringTeamInviteModal = $injector.get('HiringTeamInviteModal');
                    jest.spyOn(HiringTeamInviteModal, 'open').mockImplementation(() => {});
                    careersNetworkViewModel.showShareModal(viewModel.connectionProfile);
                    expect(DialogModal.alert).toHaveBeenCalled();

                    const args = DialogModal.alert.mock.calls[0];
                    const onCloseCallback = args[0].scope.onCloseCallback;
                    jest.spyOn(DialogModal, 'removeAlerts').mockImplementation(() => {});
                    onCloseCallback({
                        openHiringTeamInviteModal: true,
                    });
                    $timeout.flush(); // flush getHiringRelationshipViewModelForConnectionId
                    expect(DialogModal.removeAlerts).toHaveBeenCalled();
                    expect(HiringTeamInviteModal.open).toHaveBeenCalled();
                });
            });

            describe('when !openHiringTeamInviteModal', () => {
                it('should execute onFinish if present', () => {
                    const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
                    jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});

                    const viewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
                    const onFinish = jest.fn();
                    const careerProfile = viewModel.connectionProfile;
                    careersNetworkViewModel.showShareModal(careerProfile, onFinish);
                    expect(DialogModal.alert).toHaveBeenCalled();
                    const args = DialogModal.alert.mock.calls[0];
                    expect(args[0].scope.careerProfile).toEqual(careerProfile);

                    const onCloseCallback = args[0].scope.onCloseCallback;
                    jest.spyOn(DialogModal, 'removeAlerts').mockImplementation(() => {});
                    onCloseCallback();
                    $timeout.flush(); // flush getHiringRelationshipViewModelForConnectionId
                    expect(DialogModal.removeAlerts).toHaveBeenCalled();
                    expect(onFinish).toHaveBeenCalled();
                });
            });
        });
    });

    describe('ensureOpenPositions', () => {
        let OpenPosition;
        let careersNetworkViewModel;
        let openPositions;

        beforeEach(() => {
            OpenPosition = $injector.get('OpenPosition');
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            openPositions = [
                OpenPosition.new({
                    title: '1',
                    updated_at: 1,
                }),
            ];
        });

        describe('filters', () => {
            it('should pass the proper filters when hiringManager with a team', () => {
                const currentUser = SpecHelper.stubCurrentUser();
                currentUser.hiring_team_id = 'theBrowns';
                careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');

                const args = {
                    filters: {
                        hiring_team_id: careersNetworkViewModel.user.hiring_team_id,
                    },
                };
                OpenPosition.expect('index').toBeCalledWith(args);
                careersNetworkViewModel.ensureOpenPositions();
                OpenPosition.flush('index');
            });

            it('should pass the proper filters when solo hiringManager', () => {
                const currentUser = SpecHelper.stubCurrentUser();
                currentUser.hiring_team_id = undefined;
                careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');

                const args = {
                    filters: {
                        hiring_manager_id: careersNetworkViewModel.user.id,
                    },
                };
                OpenPosition.expect('index').toBeCalledWith(args);
                careersNetworkViewModel.ensureOpenPositions();
                OpenPosition.flush('index');
            });

            it('should throw error when candidate', () => {
                const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
                expect(() => {
                    careersNetworkViewModel.ensureOpenPositions();
                }).toThrow('Only hiring managers use this method now');
            });
        });

        it('should return an existing promise if there is one', () => {
            assertEnsureOpenPositions(true);
            const originalPromise = careersNetworkViewModel._openPositionsPromise;
            jest.spyOn(OpenPosition, 'index').mockImplementation(() => {});
            assertEnsureOpenPositions(false);
            expect(OpenPosition.index).not.toHaveBeenCalled();
            expect(careersNetworkViewModel._openPositionsPromise).toEqual(originalPromise);
        });

        function expectIndex() {
            OpenPosition.expect('index')
                .toBeCalledWith({
                    filters: {
                        hiring_manager_id: careersNetworkViewModel.user.id,
                    },
                })
                .returns(openPositions);
            return openPositions;
        }

        function assertEnsureOpenPositions(expectToReload) {
            if (expectToReload) {
                expectIndex();
            }
            let result;
            careersNetworkViewModel.ensureOpenPositions().then(_openPositions => {
                result = _openPositions;
            });
            if (expectToReload) {
                OpenPosition.flush('index');
            } else {
                $timeout.flush();
            }
            expect(result).toEqual(openPositions);
            return openPositions;
        }
    });

    describe('loadRecommendedPositions', () => {
        it('should throw error when not candidate', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            expect(() => {
                careersNetworkViewModel.loadRecommendedPositions();
            }).toThrow('Only candidates use this method');
        });

        it('should pass the proper arguments', () => {
            let OpenPosition;
            let careersNetworkViewModel;
            let openPositions;

            OpenPosition = $injector.get('OpenPosition');
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            openPositions = [
                OpenPosition.new({
                    id: 1337,
                    title: '1',
                    updated_at: 1,
                }),
            ];

            const otherArgs = {
                limit: 2,
                foo: 'bar',
            };
            OpenPosition.expect('index')
                .toBeCalledWith({
                    filters: {
                        recommended_for: careersNetworkViewModel.user.id,
                        featured: true,
                        archived: false,
                        hiring_manager_might_be_interested_in: careersNetworkViewModel.user.id,
                        candidate_has_acted_on: false,
                    },
                    candidate_id: careersNetworkViewModel.user.id,
                    sort: 'RECOMMENDED_FOR_CANDIDATE',
                    limit: 2,
                    foo: 'bar',
                })
                .returns({
                    result: openPositions,
                });
            careersNetworkViewModel.loadRecommendedPositions(otherArgs).then(recommendedPositions => {
                expect(_.pluck(recommendedPositions, 'id')).toEqual([1337]);
            });
            OpenPosition.flush('index');
        });
    });

    describe('commitOpenPosition', () => {
        let careersNetworkViewModel;

        beforeEach(() => {
            const OpenPosition = $injector.get('OpenPosition');
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            careersNetworkViewModel.openPositions = [
                OpenPosition.new({
                    id: 1,
                    title: '1',
                    updated_at: 1,
                }),
            ];
        });

        it('should do nothing if there are no open positions', () => {
            careersNetworkViewModel.openPositions = null;
            const newOpenPosition = {
                id: 1,
            };
            careersNetworkViewModel.commitOpenPosition(newOpenPosition);

            expect(careersNetworkViewModel.openPositions).toEqual(null);
        });

        it('should replace an existing position', () => {
            const newOpenPosition = {
                id: 1,
            };
            careersNetworkViewModel.commitOpenPosition(newOpenPosition);

            expect(careersNetworkViewModel.openPositions).toEqual([newOpenPosition]);
        });

        it('should add a new position', () => {
            const existingPosition = careersNetworkViewModel.openPositions[0];
            const newOpenPosition = {
                id: 2,
            };
            careersNetworkViewModel.commitOpenPosition(newOpenPosition);

            expect(careersNetworkViewModel.openPositions).toEqual([newOpenPosition, existingPosition]);
        });
    });

    describe('_onHiringRelationshipsLoaded', () => {
        it('should add the hiring_application from the current user to loaded relationships', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                addHiringRelationships: false,
            });
            const hiringRelationship = HiringRelationship.fixtures.getInstance({
                hiring_manager_id: careersNetworkViewModel.user.id,
                hiring_application: null,
            });

            careersNetworkViewModel._onHiringRelationshipsLoaded([hiringRelationship.asJson()]);
            expect(careersNetworkViewModel.hiringRelationships[0].hiring_application).toBe(
                careersNetworkViewModel.user.hiring_application,
            );
        });

        it('should add the career_profile from the current user to loaded relationships', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate', {
                addHiringRelationships: false,
            });
            const hiringRelationship = HiringRelationship.fixtures.getInstance({
                candidate_id: careersNetworkViewModel.user.id,
                career_profile: null,
            });

            careersNetworkViewModel._onHiringRelationshipsLoaded([hiringRelationship.asJson()]);
            expect(careersNetworkViewModel.hiringRelationships[0].career_profile).toBe(
                careersNetworkViewModel.user.career_profile,
            );
        });

        it('should add the hiring_application from the meta to loaded teammate relationships', () => {
            const HiringApplication = $injector.get('HiringApplication');
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                addHiringRelationships: false,
            });
            const hiringRelationship = HiringRelationship.fixtures.getInstance({
                hiring_manager_id: 'teammate',
                hiring_application: null,
            });

            careersNetworkViewModel._onHiringRelationshipsLoaded([hiringRelationship.asJson()], {
                hiring_applications: [
                    {
                        user_id: 'teammate',
                        a: 'b',
                    },
                ],
            });
            expect(careersNetworkViewModel.hiringRelationships[0].hiring_application.a).toEqual('b');
            expect(careersNetworkViewModel.hiringRelationships[0].hiring_application.isA(HiringApplication)).toBe(true);
        });
    });

    describe('resetCache', () => {
        it('should be called and work when the hiring team changes', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            jest.spyOn(careersNetworkViewModel, 'resetCache').mockImplementation(() => {});
            $rootScope.currentUser.hiring_team_id = 'changed';
            $rootScope.$apply();
            expect(careersNetworkViewModel.resetCache).toHaveBeenCalled();
        });

        it('should be called and work when has_full_access changes', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            $rootScope.currentUser.hiring_team = {
                has_full_access: false,
            };
            jest.spyOn(careersNetworkViewModel, 'resetCache').mockImplementation(() => {});
            $rootScope.currentUser.hiring_team.has_full_access = true;
            $rootScope.$apply();
            expect(careersNetworkViewModel.resetCache).toHaveBeenCalled();
        });
    });

    describe('_loadCandidatePositionInterests', () => {
        let careersNetworkViewModel;

        beforeEach(() => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
        });

        describe('filters', () => {
            it('should pass the proper filters when hiringManager with a team', () => {
                const currentUser = SpecHelper.stubCurrentUser();
                currentUser.hiring_team_id = 'theBrowns';
                careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');

                const args = {
                    filters: {
                        hiring_team_id: careersNetworkViewModel.user.hiring_team_id,
                        candidate_status: 'accepted',
                        hiring_manager_status_not: 'hidden',
                    },
                    'fields[]': ['HIRING_MANAGER_REVIEWER_FIELDS'],
                };
                CandidatePositionInterest.expect('index').toBeCalledWith(args);
                careersNetworkViewModel._loadCandidatePositionInterests();
                CandidatePositionInterest.flush('index');
            });

            it('should pass the proper filters when solo hiringManager', () => {
                const currentUser = SpecHelper.stubCurrentUser();
                currentUser.hiring_team_id = undefined;
                careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');

                const args = {
                    filters: {
                        hiring_manager_id: careersNetworkViewModel.user.id,
                        candidate_status: 'accepted',
                        hiring_manager_status_not: 'hidden',
                    },
                    'fields[]': ['HIRING_MANAGER_REVIEWER_FIELDS'],
                };
                CandidatePositionInterest.expect('index').toBeCalledWith(args);
                careersNetworkViewModel._loadCandidatePositionInterests();
                CandidatePositionInterest.flush('index');
            });

            it('should pass the proper filters when candidate', () => {
                const currentUser = SpecHelper.stubCurrentUser();
                const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
                const args = {
                    filters: {
                        candidate_id: currentUser.id,
                    },
                    'except[]': ['hiring_manager_status', 'admin_status'],
                };
                CandidatePositionInterest.expect('index').toBeCalledWith(args);
                careersNetworkViewModel._loadCandidatePositionInterests();
                CandidatePositionInterest.flush('index');
            });
        });
    });

    describe('saveCandidatePositionInterest', () => {
        it('should handle a conflict', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const interest = CandidatePositionInterest.fixtures.getInstance();

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            jest.spyOn(careersNetworkViewModel, 'onCandidatePositionInterestConflict').mockImplementation(() => {});
            CandidatePositionInterest.expect('save').fails({
                status: 409,
                data: {
                    meta: {
                        error_type: 'interest_conflict',
                        candidate_position_interest: interest.asJson(),
                    },
                },
            });

            careersNetworkViewModel.saveCandidatePositionInterest(interest);
            CandidatePositionInterest.flush('save');
            expect(DialogModal.alert).toHaveBeenCalledWith({
                content: 'This applicant has already been been reviewed by another teammate.',
            });
            expect(careersNetworkViewModel.onCandidatePositionInterestConflict).toHaveBeenCalled();
        });
    });

    describe('removeHiddenRelationships', () => {
        it('should work', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            const hiringRelationshipId = hiringRelationshipViewModel.hiringRelationship.id;

            // mock out the status change getting pushed down
            hiringRelationshipViewModel.hiringRelationship.hiring_manager_status = 'hidden';
            $rootScope.$broadcast('hiringRelationshipUpdate');
            $timeout.flush();

            expect(
                _.select(
                    careersNetworkViewModel.hiringRelationshipViewModels,
                    vm => vm.hiringRelationship.id === hiringRelationshipId,
                ).length,
            ).toBe(0);
            expect(
                _.select(
                    careersNetworkViewModel.hiringRelationships,
                    hiringRelationship => hiringRelationship.id === hiringRelationshipId,
                ).length,
            ).toBe(0);
            expect(
                _.has(
                    careersNetworkViewModel._hiringRelationshipViewModelsByConnectionId,
                    hiringRelationshipViewModel.connectionId,
                ),
            ).toBe(false);
        });

        it("should leave a teammate's relationship with the same connection", () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            careersNetworkViewModel._onHiringRelationshipsLoaded([
                HiringRelationship.fixtures.getInstance({
                    hiring_manager_id: 'teammate',
                    candidate_id: hiringRelationshipViewModel.connectionId,
                }),
            ]);
            const connectionId = hiringRelationshipViewModel.connectionId;

            // mock out the status change getting pushed down
            hiringRelationshipViewModel.hiringRelationship.hiring_manager_status = 'hidden';
            $rootScope.$broadcast('hiringRelationshipUpdate');
            $timeout.flush();

            let hiringRelationshipViewModels = _.select(
                careersNetworkViewModel.hiringRelationshipViewModels,
                vm => vm.connectionId === connectionId,
            );
            expect(hiringRelationshipViewModels.length).toBe(1);
            expect(hiringRelationshipViewModels[0].hiringRelationship.hiring_manager_id).toEqual('teammate');

            const hiringRelationships = _.select(
                careersNetworkViewModel.hiringRelationships,
                hiringRelationship => hiringRelationship.candidate_id === connectionId,
            );
            expect(hiringRelationships.length).toBe(1);
            expect(hiringRelationships[0].hiring_manager_id).toEqual('teammate');

            hiringRelationshipViewModels =
                careersNetworkViewModel._hiringRelationshipViewModelsByConnectionId[
                    hiringRelationshipViewModel.connectionId
                ];
            expect(hiringRelationshipViewModels.length).toBe(1);
            expect(hiringRelationshipViewModels[0].hiringRelationship.hiring_manager_id).toEqual('teammate');
        });
    });

    describe('_getHiringRelationshipIndexParams', () => {
        it('should work for a candidate', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            let result;

            // defaults
            result = careersNetworkViewModel._getHiringRelationshipIndexParams({});
            expect(result['except[]']).toEqual(['career_profile']);
            expect(result.filters.candidate_status_not).toEqual('hidden');

            // role
            expect(result.filters.candidate_id).toEqual(careersNetworkViewModel.user.id);
            expect(result.filters.hiring_team_id).toBeUndefined();

            // should add in extra filters and params
            result = careersNetworkViewModel._getHiringRelationshipIndexParams(
                {
                    a: 'b',
                },
                {
                    c: 'd',
                },
            );
            expect(result.c).toEqual('d');
            expect(result.filters.a).toEqual('b');

            // CONNECTION_ID filter
            result = careersNetworkViewModel._getHiringRelationshipIndexParams({
                CONNECTION_ID: 'connection_id',
            });
            expect(result.filters.hiring_manager_id).toEqual('connection_id');
            expect(_.has(result.filters.CONNECTION_ID)).toEqual(false);

            // CONNECTION_STATUS filter
            result = careersNetworkViewModel._getHiringRelationshipIndexParams({
                CONNECTION_STATUS: 'connection_status',
            });
            expect(result.filters.hiring_manager_status).toEqual('connection_status');
            expect(_.has(result.filters.CONNECTION_STATUS)).toEqual(false);

            // OUR_STATUS
            result = careersNetworkViewModel._getHiringRelationshipIndexParams({
                OUR_STATUS: 'our_status',
            });
            expect(result.filters.candidate_status).toEqual('our_status');
            expect(_.has(result.filters.candidate_status_not)).toEqual(false);
            expect(_.has(result.filters.OUR_STATUS)).toEqual(false);
        });

        it('should work for a hiring manager', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            let result;

            // should set the except value
            expect(careersNetworkViewModel._getHiringRelationshipIndexParams({})['except[]']).toEqual([
                'hiring_application',
            ]);

            // should add in extra filters and params
            result = careersNetworkViewModel._getHiringRelationshipIndexParams(
                {
                    a: 'b',
                },
                {
                    c: 'd',
                },
            );
            expect(result.c).toEqual('d');
            expect(result.filters.a).toEqual('b');

            // role
            expect(result.filters.candidate_id).toBeUndefined();
            expect(result.filters.hiring_team_id).toEqual(careersNetworkViewModel.user.hiring_team_id);

            // should default candidate_status_not
            result = careersNetworkViewModel._getHiringRelationshipIndexParams({});
            expect(result.filters.hiring_manager_status_not).toEqual('hidden');

            // CONNECTION_ID filter
            result = careersNetworkViewModel._getHiringRelationshipIndexParams({
                CONNECTION_ID: 'connection_id',
            });
            expect(result.filters.candidate_id).toEqual('connection_id');
            expect(_.has(result.filters.CONNECTION_ID)).toEqual(false);

            // CONNECTION_STATUS filter
            result = careersNetworkViewModel._getHiringRelationshipIndexParams({
                CONNECTION_STATUS: 'connection_status',
            });
            expect(result.filters.candidate_status).toEqual('connection_status');
            expect(_.has(result.filters.CONNECTION_STATUS)).toEqual(false);

            // OUR_STATUS
            result = careersNetworkViewModel._getHiringRelationshipIndexParams({
                OUR_STATUS: 'our_status',
            });
            expect(result.filters.hiring_manager_status).toEqual('our_status');
            expect(_.has(result.filters.hiring_manager_status_not)).toEqual(false);
            expect(_.has(result.filters.OUR_STATUS)).toEqual(false);
        });
    });

    describe('_getAvailableActionsForProfile', () => {
        let careersNetworkViewModel;
        let hiringRelationshipViewModel;

        it('should set like to false when oneClickReachOutEnabled is false', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            $rootScope.currentUser.pref_one_click_reach_out = false;
            hiringRelationshipViewModel.hiringRelationship.hiring_manager_status = 'pending';
            expect(
                careersNetworkViewModel._getAvailableActionsForProfile(hiringRelationshipViewModel.connectionProfile),
            ).toEqual({
                pass: true,
                save: true,
                like: false,
                invite: true,
            });
        });

        describe('with role=hiringManager', () => {
            beforeEach(() => {
                careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
                hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            });

            it('should work when no hiringRelationshipViewModel', () => {
                expect(careersNetworkViewModel._getAvailableActionsForProfile()).toEqual({
                    pass: true,
                    save: true,
                    like: true,
                    invite: true,
                });
            });

            it('should work when ourStatus is pending', () => {
                jest.spyOn(hiringRelationshipViewModel, 'myStatus', 'get').mockReturnValue('pending');
                expect(careersNetworkViewModel._getAvailableActionsForProfile(hiringRelationshipViewModel)).toEqual({
                    pass: true,
                    save: true,
                    like: true,
                    invite: true,
                });
            });

            it('should work when ourStatus is rejected and teammate has not accepted', () => {
                jest.spyOn(hiringRelationshipViewModel, 'myStatus', 'get').mockReturnValue('rejected');
                const teammateHasAccepted = false;
                expect(
                    careersNetworkViewModel._getAvailableActionsForProfile(
                        hiringRelationshipViewModel,
                        teammateHasAccepted,
                    ),
                ).toEqual({
                    pass: false,
                    save: true,
                    like: true,
                    invite: true,
                });
            });

            it('should work when ourStatus is accepted', () => {
                jest.spyOn(hiringRelationshipViewModel, 'myStatus', 'get').mockReturnValue('accepted');
                expect(careersNetworkViewModel._getAvailableActionsForProfile(hiringRelationshipViewModel)).toEqual({
                    pass: false,
                    save: false,
                    like: false,
                    invite: false,
                });
            });

            it('should work when teammateHasAccepted is true', () => {
                jest.spyOn(hiringRelationshipViewModel, 'myStatus', 'get').mockReturnValue('pending');
                const teammateHasAccepted = true;
                expect(
                    careersNetworkViewModel._getAvailableActionsForProfile(
                        hiringRelationshipViewModel,
                        teammateHasAccepted,
                    ),
                ).toEqual({
                    pass: false,
                    save: false,
                    like: false,
                    invite: false,
                });
            });

            it('should work when ourStatus is saved_for_later', () => {
                jest.spyOn(hiringRelationshipViewModel, 'myStatus', 'get').mockReturnValue('saved_for_later');
                expect(careersNetworkViewModel._getAvailableActionsForProfile(hiringRelationshipViewModel)).toEqual({
                    pass: true,
                    save: false,
                    like: false,
                    invite: true,
                });
            });
        });

        describe('with viewingTeammateRelationship=true', () => {
            describe('with teammateHasAccepted=true', () => {
                it('should hide all buttons', () => {
                    jest.spyOn(hiringRelationshipViewModel, 'myStatus', 'get').mockReturnValue('pending');
                    expect(
                        careersNetworkViewModel._getAvailableActionsForProfile(hiringRelationshipViewModel, true, true),
                    ).toEqual({
                        pass: false,
                        save: false,
                        like: false,
                        invite: false,
                    });
                });
            });

            describe('with teammateHasAccepted not true', () => {
                it('should hide pass and otherwise delegate to the rules for myStatus', () => {
                    jest.spyOn(hiringRelationshipViewModel, 'myStatus', 'get').mockReturnValue('pending');

                    expect(
                        careersNetworkViewModel._getAvailableActionsForProfile(hiringRelationshipViewModel, null, true),
                    ).toEqual({
                        pass: false,
                        save: true,
                        like: true,
                        invite: true,
                    });
                });
            });
        });
    });

    describe('getAvailableActionsWithEverythingPreloaded', () => {
        let careersNetworkViewModel;

        describe('with role=hiringManager', () => {
            beforeEach(() => {
                careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            });

            it('should set teammateHasAccepted and pass it along', () => {
                const careerProfile = CareerProfile.fixtures.getInstance();

                const deferred = $q.defer();
                jest.spyOn(careersNetworkViewModel, 'getMyHiringRelationshipViewModelForConnectionId').mockReturnValue(
                    deferred.promise,
                );
                jest.spyOn(careersNetworkViewModel, '_getAvailableActionsForProfile').mockImplementation(() => {});

                careersNetworkViewModel.getAvailableActionsWithEverythingPreloaded(careerProfile);

                const myHiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[1];
                deferred.resolve(myHiringRelationshipViewModel);
                $timeout.flush();

                expect(careersNetworkViewModel._getAvailableActionsForProfile).toHaveBeenCalledWith(
                    myHiringRelationshipViewModel,
                    false,
                    undefined,
                );
            });
        });
    });

    describe('getAvailableActionsForReviewedPosition', () => {
        let careersNetworkViewModel;

        beforeEach(() => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
        });

        it('should not load relationship when my relationship already loaded', () => {
            const deferredAcceptedHiringRelationshipViewModel = $q.defer();
            jest.spyOn(careersNetworkViewModel, 'getHiringRelationshipViewModelAcceptedByTeam').mockReturnValue(
                deferredAcceptedHiringRelationshipViewModel.promise,
            );

            jest.spyOn(
                careersNetworkViewModel,
                'getMyHiringRelationshipViewModelForConnectionId',
            ).mockImplementation(() => {});

            const deferredActions = $q.defer();
            jest.spyOn(careersNetworkViewModel, '_getAvailableActionsForProfile').mockReturnValue(
                deferredActions.promise,
            );

            const careerProfile = CareerProfile.fixtures.getInstance();
            careersNetworkViewModel.getAvailableActionsForReviewedPosition(careerProfile);

            const acceptedHiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            jest.spyOn(acceptedHiringRelationshipViewModel, 'ourStatus', 'get').mockReturnValue('accepted');
            jest.spyOn(acceptedHiringRelationshipViewModel, 'isMyRelationship', 'get').mockReturnValue(true);
            deferredAcceptedHiringRelationshipViewModel.resolve(acceptedHiringRelationshipViewModel);
            $timeout.flush();

            expect(careersNetworkViewModel.getHiringRelationshipViewModelAcceptedByTeam).toHaveBeenCalledWith(
                careerProfile.user_id,
                true,
            );
            expect(careersNetworkViewModel.getMyHiringRelationshipViewModelForConnectionId).not.toHaveBeenCalled();
            expect(careersNetworkViewModel._getAvailableActionsForProfile).toHaveBeenCalledWith(
                acceptedHiringRelationshipViewModel,
                undefined,
                undefined,
            );
        });

        it('should load relationship when my relationship not already loaded', () => {
            const deferredAcceptedHiringRelationshipViewModel = $q.defer();
            jest.spyOn(careersNetworkViewModel, 'getHiringRelationshipViewModelAcceptedByTeam').mockReturnValue(
                deferredAcceptedHiringRelationshipViewModel.promise,
            );

            const deferredMyHiringRelationshipViewModel = $q.defer();
            jest.spyOn(careersNetworkViewModel, 'getMyHiringRelationshipViewModelForConnectionId').mockReturnValue(
                deferredMyHiringRelationshipViewModel.promise,
            );

            const deferredActions = $q.defer();
            jest.spyOn(careersNetworkViewModel, '_getAvailableActionsForProfile').mockReturnValue(
                deferredActions.promise,
            );

            const careerProfile = CareerProfile.fixtures.getInstance();
            careersNetworkViewModel.getAvailableActionsForReviewedPosition(careerProfile);

            deferredAcceptedHiringRelationshipViewModel.resolve(undefined);
            $timeout.flush();

            const acceptedHiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            jest.spyOn(acceptedHiringRelationshipViewModel, 'ourStatus', 'get').mockReturnValue('accepted');
            jest.spyOn(acceptedHiringRelationshipViewModel, 'isMyRelationship', 'get').mockReturnValue(true);
            deferredMyHiringRelationshipViewModel.resolve(acceptedHiringRelationshipViewModel);
            $timeout.flush();

            expect(careersNetworkViewModel.getHiringRelationshipViewModelAcceptedByTeam).toHaveBeenCalledWith(
                careerProfile.user_id,
                true,
            );
            expect(careersNetworkViewModel.getMyHiringRelationshipViewModelForConnectionId).toHaveBeenCalledWith(
                careerProfile.user_id,
                true,
            );
            expect(careersNetworkViewModel._getAvailableActionsForProfile).toHaveBeenCalledWith(
                acceptedHiringRelationshipViewModel,
                undefined,
                undefined,
            );
        });

        it('should set teammateHasAccepted and pass it along', () => {
            const deferredAcceptedHiringRelationshipViewModel = $q.defer();
            jest.spyOn(careersNetworkViewModel, 'getHiringRelationshipViewModelAcceptedByTeam').mockReturnValue(
                deferredAcceptedHiringRelationshipViewModel.promise,
            );

            const deferredMyHiringRelationshipViewModel = $q.defer();
            jest.spyOn(careersNetworkViewModel, 'getMyHiringRelationshipViewModelForConnectionId').mockReturnValue(
                deferredMyHiringRelationshipViewModel.promise,
            );

            const deferredActions = $q.defer();
            jest.spyOn(careersNetworkViewModel, '_getAvailableActionsForProfile').mockReturnValue(
                deferredActions.promise,
            );

            const careerProfile = CareerProfile.fixtures.getInstance();
            careersNetworkViewModel.getAvailableActionsForReviewedPosition(careerProfile);

            const acceptedHiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            jest.spyOn(acceptedHiringRelationshipViewModel, 'ourStatus', 'get').mockReturnValue('accepted');
            jest.spyOn(acceptedHiringRelationshipViewModel, 'isMyRelationship', 'get').mockReturnValue(false);
            deferredAcceptedHiringRelationshipViewModel.resolve(acceptedHiringRelationshipViewModel);
            $timeout.flush();

            const myHiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[1];
            jest.spyOn(myHiringRelationshipViewModel, 'ourStatus', 'get').mockReturnValue('pending');
            jest.spyOn(myHiringRelationshipViewModel, 'isMyRelationship', 'get').mockReturnValue(true);
            deferredMyHiringRelationshipViewModel.resolve(myHiringRelationshipViewModel);
            $timeout.flush();
            expect(careersNetworkViewModel._getAvailableActionsForProfile).toHaveBeenCalledWith(
                myHiringRelationshipViewModel,
                true,
                undefined,
            );
        });

        it('should pass along viewingTeammateRelationship', () => {
            const deferredAcceptedHiringRelationshipViewModel = $q.defer();
            jest.spyOn(careersNetworkViewModel, 'getHiringRelationshipViewModelAcceptedByTeam').mockReturnValue(
                deferredAcceptedHiringRelationshipViewModel.promise,
            );

            jest.spyOn(
                careersNetworkViewModel,
                'getMyHiringRelationshipViewModelForConnectionId',
            ).mockImplementation(() => {});

            const deferredActions = $q.defer();
            jest.spyOn(careersNetworkViewModel, '_getAvailableActionsForProfile').mockReturnValue(
                deferredActions.promise,
            );

            const careerProfile = CareerProfile.fixtures.getInstance();
            careersNetworkViewModel.getAvailableActionsForReviewedPosition(careerProfile, {
                viewingTeammateRelationship: true,
            });

            const acceptedHiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            jest.spyOn(acceptedHiringRelationshipViewModel, 'ourStatus', 'get').mockReturnValue('accepted');
            jest.spyOn(acceptedHiringRelationshipViewModel, 'isMyRelationship', 'get').mockReturnValue(true);
            deferredAcceptedHiringRelationshipViewModel.resolve(acceptedHiringRelationshipViewModel);
            $timeout.flush();

            expect(careersNetworkViewModel._getAvailableActionsForProfile).toHaveBeenCalledWith(
                acceptedHiringRelationshipViewModel,
                undefined,
                true,
            );
        });
    });
});
