import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/candidate_position_interest_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_relationship_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/open_position_fixtures';
import 'Careers/angularModule/spec/careers_spec_helper';
import setSpecLocales from 'Translation/setSpecLocales';
import featuredPositionsLocales from 'Careers/locales/careers/featured_positions-en.json';
import positionCardLocales from 'Careers/locales/careers/position_card-en.json';
import candidateActionButtonsLocales from 'Careers/locales/careers/candidate_action_buttons-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';

setSpecLocales(featuredPositionsLocales, positionCardLocales, candidateActionButtonsLocales, fieldOptionsLocales);

describe('FrontRoyal.Careers.FeaturedPositions', () => {
    let $injector;
    let SpecHelper;
    let CareersSpecHelper;
    let renderer;
    let elem;
    let scope;
    let careersNetworkViewModel;
    let CareersNetworkViewModel;
    let CandidatePositionInterest;
    let OpenPosition;
    let $q;
    let $location;
    let DialogModal;
    let $rootScope;
    let HiringRelationship;
    let HiringRelationshipViewModel;
    let OpenPositionFilterSet;
    let filterSetLoadingDeferred;
    let recommendedPositions;
    let clientPaginatedPositions;
    let serverPaginatedPositions;
    let locationPlaceDetails;
    let currentUser;
    let mockgetZipRecruiterJobsReturnValue;
    let EventLogger;
    let $window;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                OpenPosition = $injector.get('OpenPosition');
                CandidatePositionInterest = $injector.get('CandidatePositionInterest');
                $q = $injector.get('$q');
                $location = $injector.get('$location');
                DialogModal = $injector.get('DialogModal');
                $rootScope = $injector.get('$rootScope');
                HiringRelationship = $injector.get('HiringRelationship');
                HiringRelationshipViewModel = $injector.get('HiringRelationshipViewModel');
                CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
                OpenPositionFilterSet = $injector.get('OpenPositionFilterSet');
                locationPlaceDetails = $injector.get('LOCATION_PLACE_DETAILS');
                EventLogger = $injector.get('EventLogger');
                $window = $injector.get('$window');

                $injector.get('HiringRelationshipFixtures');
                $injector.get('OpenPositionFixtures');
                $injector.get('CandidatePositionInterestFixtures');

                SpecHelper.stubConfig();
                SpecHelper.stubDirective('locationAutocomplete');
            },
        ]);

        currentUser = SpecHelper.stubCurrentUser('learner');
        careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
        filterSetLoadingDeferred = $q.defer();

        const positionWithIndustryAndLocation = OpenPosition.new({
            id: 1,
            role: 'ux_ui',
            hiring_application: {
                industry: 'consulting',
            },
            position_descriptors: ['part_time'],
            desired_years_experience: {
                min: 1,
                max: 2,
            },
        });

        jest.spyOn(positionWithIndustryAndLocation, 'locationString', 'get').mockReturnValue('New York, NY');

        recommendedPositions = [
            OpenPosition.fixtures.getInstance({
                id: 'recommended-1',
                recommended: true,
            }),
            OpenPosition.fixtures.getInstance({
                id: 'recommended-2',
                recommended: true,
            }),
            OpenPosition.fixtures.getInstance({
                id: 'recommended-3',
                recommended: true,
            }),
        ];

        serverPaginatedPositions = [
            // one that is open and has industry and location
            positionWithIndustryAndLocation,
            // one that is open and has NO industry or location
            OpenPosition.new({
                id: 2,
                role: 'marketing', // if excluded, throws a missing translation error
                position_descriptors: ['permanent'],
                desired_years_experience: {
                    min: 5,
                    max: 8,
                },
            }),
        ];

        clientPaginatedPositions = [
            // one that is accepted
            OpenPosition.new({
                id: 3,
                role: 'marketing',
                position_descriptors: ['permanent'],
                desired_years_experience: {
                    min: 1,
                    max: 2,
                },
                $interest: {
                    hasCoverLetter: true,
                    cover_letter: {
                        content: 'foo',
                        created_at: new Date(),
                    },
                },
            }),
            // another that is accepted
            OpenPosition.new({
                id: 32,
                role: 'ux_ui',
                hiring_application: {
                    industry: 'consulting',
                },
                desired_years_experience: {
                    min: 5,
                    max: 8,
                },
                position_descriptors: ['part_time'],
            }),
            // one that is rejected
            OpenPosition.new({
                id: 4,
                role: 'marketing',
                position_descriptors: ['part_time'],
            }),
            // one that is connected
            OpenPosition.new({
                id: 5,
                role: 'marketing',
                position_descriptors: ['part_time'],
                hiring_manager_id: 'abc',
            }),
            // one that is hidden
            OpenPosition.new({
                id: 'hidden-id',
                role: 'marketing',
                position_descriptors: ['part_time'],
                hiring_manager_id: 'abc',
            }),
            // one that is closed by me
            OpenPosition.new({
                id: 6,
                role: 'marketing',
                position_descriptors: ['part_time'],
                hiring_manager_id: 'def',
            }),
            // one that is closed by employer
            OpenPosition.new({
                id: 7,
                role: 'marketing',
                position_descriptors: ['part_time'],
                hiring_manager_id: 'ghi',
            }),
        ];

        SpecHelper.stubConfig();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render(params = {}) {
        params.list = params.list || 'open';

        // Mock setup of the client paginated tabs
        let interests = [];
        let myCandidatesHiringRelationships = [];
        let closedHiringRelationships = [undefined, undefined];

        interests = [
            // accept position where id === 3
            CandidatePositionInterest.new({
                candidate_id: careersNetworkViewModel.user.id,
                open_position_id: 3,
                candidate_status: 'accepted',
                hiring_manager_rejected: false,
            }),
            // accept position where id === 32
            CandidatePositionInterest.new({
                candidate_id: careersNetworkViewModel.user.id,
                open_position_id: 32,
                candidate_status: 'accepted',
                hiring_manager_rejected: false,
            }),
            // reject position where id === 4
            CandidatePositionInterest.new({
                candidate_id: careersNetworkViewModel.user.id,
                open_position_id: 4,
                candidate_status: 'rejected',
                hiring_manager_rejected: false,
            }),
            // accept position where id === 5
            CandidatePositionInterest.new({
                candidate_id: careersNetworkViewModel.user.id,
                open_position_id: 5,
                candidate_status: 'accepted',
                hiring_manager_rejected: false,
            }),
            // hide position where id === 'hidden-id'
            CandidatePositionInterest.new({
                candidate_id: careersNetworkViewModel.user.id,
                open_position_id: 'hidden-id',
                candidate_status: 'accepted',
                hiring_manager_rejected: false,
            }),
            // accept position where id === 6
            CandidatePositionInterest.new({
                candidate_id: careersNetworkViewModel.user.id,
                open_position_id: 6,
                candidate_status: 'accepted',
                hiring_manager_rejected: false,
            }),
            // accept position where id === 7
            CandidatePositionInterest.new({
                candidate_id: careersNetworkViewModel.user.id,
                open_position_id: 7,
                candidate_status: 'accepted',
                hiring_manager_rejected: false,
            }),
        ];

        myCandidatesHiringRelationships = [
            new HiringRelationshipViewModel(
                HiringRelationship.fixtures.getInstance({
                    open_position_id: 5,
                    candidate_id: careersNetworkViewModel.user.id,
                    hiring_manager_id: 'abc',
                    candidate_status: 'accepted',
                    hiring_manager_status: 'accepted',
                }),
                careersNetworkViewModel,
                'candidate',
            ),
        ];

        closedHiringRelationships = [
            new HiringRelationshipViewModel(
                HiringRelationship.fixtures.getInstance({
                    open_position_id: 6,
                    candidate_id: careersNetworkViewModel.user.id,
                    hiring_manager_id: 'def',
                    candidate_closed: true,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'accepted',
                }),
                careersNetworkViewModel,
                'candidate',
            ),
            new HiringRelationshipViewModel(
                HiringRelationship.fixtures.getInstance({
                    open_position_id: 7,
                    candidate_id: careersNetworkViewModel.user.id,
                    hiring_manager_id: 'ghi',
                    candidate_closed: false,
                    hiring_manager_closed: true,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'accepted',
                }),
                careersNetworkViewModel,
                'candidate',
            ),
        ];

        jest.spyOn(CareersNetworkViewModel.prototype, 'loadRecommendedPositions').mockReturnValue(
            $q.when(params.recommendedPositions || recommendedPositions),
        );
        jest.spyOn(CareersNetworkViewModel.prototype, 'ensureCandidatePositionInterests').mockReturnValue(
            $q.when(interests),
        );
        jest.spyOn(CareersNetworkViewModel.prototype, 'ensureHiringRelationshipsLoaded')
            .mockReturnValueOnce($q.when(myCandidatesHiringRelationships))
            .mockReturnValueOnce($q.when(closedHiringRelationships));
        jest.spyOn(CareersNetworkViewModel.prototype, 'getMyHiringRelationshipViewModelForConnectionId')
            .mockReturnValueOnce($q.when(myCandidatesHiringRelationships[0]))
            .mockReturnValueOnce($q.when(closedHiringRelationships[0]))
            .mockReturnValueOnce($q.when(closedHiringRelationships[1]));

        OpenPosition.expect('index')
            .toBeCalledWith({
                filters: {
                    featured: true,
                    archived: false,
                    hiring_manager_might_be_interested_in: currentUser.id,
                    candidate_has_acted_on: true,
                    id: _.chain(interests.concat(myCandidatesHiringRelationships).concat(closedHiringRelationships))
                        .pluck('open_position_id')
                        .compact()
                        .value(),
                },
            })
            .returns(params.clientPaginatedPositions || clientPaginatedPositions);

        // Mock setup of the openPositionFilterSet that is used for the server paginated tab
        jest.spyOn(OpenPositionFilterSet.prototype, 'ensureResultsPreloaded').mockImplementation(function () {
            const self = this;

            self._loadingPromise = filterSetLoadingDeferred.promise;
            self._loadingPromise.then(() => {
                self._loadingPromise = null;
                self._initialResultsLoaded = true;
                self.initialTotalCount = params.serverPaginatedPositions ? params.serverPaginatedPositions.length : 0;
                self.results = params.serverPaginatedPositions || serverPaginatedPositions;
            });

            return self._loadingPromise;
        });

        jest.spyOn($rootScope.currentUser, 'hasActiveCareerProfile', 'get').mockReturnValue(true);

        $location.search('list', params.list);
        renderer = SpecHelper.renderer();
        renderer.render('<featured-positions></featured-positions>');
        elem = renderer.elem;
        scope = elem.isolateScope();

        mockgetZipRecruiterJobsReturnValue = [];
        jest.spyOn(scope, '_getZipRecruiterJobs').mockImplementation(() => {
            $q.when(mockgetZipRecruiterJobsReturnValue);
        });

        // Flush the call for the server paginated positions
        filterSetLoadingDeferred.resolve();
        scope.$digest();

        // Flush the call for the client paginated positions
        OpenPosition.flush('index');
    }

    it('should default to recommended if no acceptable list param', () => {
        render({
            list: 'foo',
        });
        expect(scope.currentTab).toBe('recommended');
    });

    it('should remove list search param from the URL when scope gets destroyed', () => {
        render();
        jest.spyOn($location, 'search').mockImplementation(() => {});
        scope.$emit('$destroy');
        expect($location.search).toHaveBeenCalledWith('list', null);
    });

    it('should reset the positions for the open tab config when openPositionFilterSet gets new results', () => {
        render();
        const newPositions = [OpenPosition.fixtures.getInstance(), OpenPosition.fixtures.getInstance()];
        const openTabConfig = scope.tabConfigs.open;

        scope.openPositionFilterSet.results = newPositions;
        scope.$digest();

        expect(openTabConfig.initialPositions).toEqual(newPositions);
        expect(openTabConfig.filteredPositions).toEqual(newPositions);
    });

    it('should properly handle any likes or rejects while client paginated positions are still loading', () => {
        render({
            list: 'accepted',
        });
        const testPositions = [
            OpenPosition.fixtures.getInstance({
                title: 'test position 1',
            }),
            OpenPosition.fixtures.getInstance({
                title: 'test position 2',
            }),
        ];

        const expectedPositions = scope.tabConfigs.accepted.initialPositions.concat(testPositions);

        // This method is called once the client paginated positions and other necessary information is loaded.
        scope.initializeClientPaginatedList(testPositions, 'accepted');

        // We expect the existing accepted positions to be added to the test positions that were passed to
        // onClientPaginatedListsCreated
        expect(scope.tabConfigs.accepted.initialPositions).toEqual(expectedPositions);
        expect(scope.tabConfigs.accepted.filteredPositions).toEqual(expectedPositions);
    });

    it('should call ensureResultsPreloaded when the length of the filter set results changes', () => {
        render();

        // Called once from the initial setup
        expect(scope.openPositionFilterSet.ensureResultsPreloaded).toHaveBeenCalled();
        scope.openPositionFilterSet.results.splice(0, 1);
        scope.$digest();

        // Called again when the length changed
        expect(scope.openPositionFilterSet.ensureResultsPreloaded).toHaveBeenCalled();
    });

    it('should decrement the offset is on the last page with no results', () => {
        render();
        scope.offset = 3;
        scope.listLimit = 10;
        jest.spyOn(scope, 'activePositions', 'get').mockReturnValue([
            OpenPosition.fixtures.getInstance(),
            OpenPosition.fixtures.getInstance(),
            OpenPosition.fixtures.getInstance(),
        ]);
        jest.spyOn(scope, 'getOpenPositionsRelativeToOffset').mockImplementation(() => {});
        scope.$digest();
        expect(scope.getOpenPositionsRelativeToOffset).toHaveBeenCalledWith(3, 'previous', 10);
    });

    describe('recommended tab', () => {
        it('should remove recommended from tabs and redirect to open if there are no recommended positions on initial load', () => {
            render({
                recommendedPositions: [],
            });
            scope.positionTabs.forEach(tab => {
                expect(tab.name).not.toBe('recommended');
            });
            expect(scope.currentTab).toBe('open');
        });

        it('should remove recommended from tabs and redirect to open if there are no recommended positions after load', () => {
            render({
                list: 'recommended',
            });
            expect(scope.currentTab).toBe('recommended');
            expect(scope.positionTabs[0].name).toBe('recommended');

            // Simulate the user processing all of the recommended positions
            scope.tabConfigs.recommended.initialPositions = [];
            scope.$digest();

            scope.positionTabs.forEach(tab => {
                expect(tab.name).not.toBe('recommended');
            });
            expect(scope.currentTab).toBe('open');
        });

        it('should show recommended tab when recommended positions', () => {
            render({
                list: 'recommended',
            });
            expect(scope.currentTab).toBe('recommended');
            expect(scope.positionTabs[0].name).toBe('recommended');
        });
    });

    describe('watch currentTab', () => {
        it('should normally update offset, reset filters, and reset the old tab positions', () => {
            render({
                list: 'accepted',
            });

            const oldTabConfig = scope.tabConfigs.accepted;

            expect(scope.currentTab).toBe('accepted'); // sanity check
            jest.spyOn(scope, 'hasFilters', 'get').mockReturnValue(true);
            scope.currentTabConfig.filteredPositions = [];
            scope.$digest();

            expect(oldTabConfig.filteredPositions).toEqual([]); // sanity check
            scope.currentTab = 'foo';
            jest.spyOn(scope, 'setOffset').mockImplementation(() => {});
            jest.spyOn(scope, 'resetFilters').mockImplementation(() => {});
            jest.spyOn(scope, 'updateTabNum').mockImplementation(() => {});
            scope.$digest();

            expect(scope.setOffset).toHaveBeenCalled();
            expect(scope.resetFilters).toHaveBeenCalled();
            expect(scope.updateTabNum).toHaveBeenCalled();
            expect(oldTabConfig.filteredPositions).toEqual(oldTabConfig.initialPositions);
        });

        it('should update offset and reset filters but not reset positions if old tab is still loading', () => {
            render({
                list: 'accepted',
            });

            const oldTabConfig = scope.tabConfigs.accepted;

            expect(scope.currentTab).toBe('accepted'); // sanity check
            jest.spyOn(scope, 'hasFilters', 'get').mockReturnValue(true);
            scope.currentTabConfig.filteredPositions = [];
            scope.$digest();

            expect(oldTabConfig.filteredPositions).toEqual([]); // sanity check
            scope.currentTab = 'foo';
            jest.spyOn(scope, 'setOffset').mockImplementation(() => {});
            jest.spyOn(scope, 'resetFilters').mockImplementation(() => {});
            jest.spyOn(scope, 'updateTabNum').mockImplementation(() => {});
            oldTabConfig.loading = true;
            scope.$digest();

            expect(scope.setOffset).toHaveBeenCalled();
            expect(scope.resetFilters).toHaveBeenCalled();
            expect(scope.updateTabNum).not.toHaveBeenCalled();
            expect(oldTabConfig.filteredPositions).not.toEqual(oldTabConfig.initialPositions);
        });

        it('should update offset only if no filters', () => {
            render({
                list: 'accepted',
            });

            const oldTabConfig = scope.tabConfigs.accepted;

            expect(scope.currentTab).toBe('accepted'); // sanity check
            jest.spyOn(scope, 'hasFilters', 'get').mockReturnValue(false);
            scope.currentTabConfig.filteredPositions = [];
            scope.$digest();

            expect(oldTabConfig.filteredPositions).toEqual([]); // sanity check
            scope.currentTab = 'foo';
            jest.spyOn(scope, 'setOffset').mockImplementation(() => {});
            jest.spyOn(scope, 'resetFilters').mockImplementation(() => {});
            jest.spyOn(scope, 'updateTabNum').mockImplementation(() => {});
            scope.$digest();

            expect(scope.setOffset).toHaveBeenCalled();
            expect(scope.resetFilters).not.toHaveBeenCalled();
            expect(scope.updateTabNum).not.toHaveBeenCalled();
            expect(oldTabConfig.filteredPositions).not.toEqual(oldTabConfig.initialPositions);
        });

        it('should do nothing if user does not have an active career profile', () => {
            jest.spyOn($rootScope.currentUser, 'hasActiveCareerProfile', 'get').mockReturnValue(false);

            $location.search('list', 'accepted');
            renderer = SpecHelper.renderer();
            renderer.render('<featured-positions></featured-positions>');
            elem = renderer.elem;
            scope = elem.isolateScope();

            const oldTabConfig = scope.tabConfigs.accepted;

            expect(scope.currentTab).toBe('accepted'); // sanity check
            jest.spyOn(scope, 'hasFilters', 'get').mockReturnValue(false);

            const filteredPositions = [OpenPosition.fixtures.getInstance(), OpenPosition.fixtures.getInstance()];
            scope.currentTabConfig.filteredPositions = filteredPositions;
            scope.$digest();

            expect(oldTabConfig.filteredPositions).toEqual(filteredPositions); // sanity check
            scope.currentTab = 'foo';
            jest.spyOn(scope, 'setOffset').mockImplementation(() => {});
            jest.spyOn(scope, 'resetFilters').mockImplementation(() => {});
            jest.spyOn(scope, 'updateTabNum').mockImplementation(() => {});
            scope.$digest();

            expect(scope.setOffset).not.toHaveBeenCalled();
            expect(scope.resetFilters).not.toHaveBeenCalled();
            expect(scope.updateTabNum).not.toHaveBeenCalled();
            expect(oldTabConfig.filteredPositions).not.toEqual(oldTabConfig.initialPositions);
        });
    });

    // describe('recommended_positions_seen_ids', () => {
    //     it('should update when switching to the recommended tab if there are new ones', () => {
    //         jest.spyOn(currentUser, 'save');
    //         render({
    //             list: 'open'
    //         });
    //         scope.offset = 0;
    //         scope.listLimit = 2;
    //         scope.currentTab = 'recommended';
    //         scope.$digest();
    //         expect(currentUser.save).toHaveBeenCalled();
    //         expect(currentUser.recommended_positions_seen_ids).toEqual(['recommended-1', 'recommended-2']);
    //     });

    //     it('should update when cycling through the positions if there are new ones', () => {
    //         jest.spyOn(currentUser, 'save');
    //         render({
    //             list: 'open'
    //         });
    //         scope.offset = 0;
    //         scope.listLimit = 2;
    //         scope.currentTab = 'recommended';
    //         scope.$digest();
    //         scope.offset = 2;
    //         scope.$digest();
    //         expect(currentUser.save).toHaveBeenCalledTimes(2);
    //         expect(currentUser.recommended_positions_seen_ids).toEqual(recommendedPositions.map(position => position.id));
    //     });

    //     it('should not update when in ghostMode', () => {
    //         currentUser.ghostMode = true;
    //         jest.spyOn(currentUser, 'save');
    //         render({
    //             list: 'recommended'
    //         });
    //         expect(scope.currentTab).toBe('recommended'); // sanity check
    //         expect(currentUser.save).not.toHaveBeenCalled();
    //         expect(currentUser.recommended_positions_seen_ids).toBeUndefined();
    //     });

    //     it('should not update when there are no unseen recommended positions', () => {
    //         currentUser.recommended_positions_seen_ids = recommendedPositions.map(position => position.id);
    //         jest.spyOn(currentUser, 'save');
    //         render({
    //             list: 'recommended'
    //         });
    //         expect(scope.currentTab).toBe('recommended'); // sanity check
    //         expect(currentUser.save).not.toHaveBeenCalled();
    //     });
    // });

    describe('when !hasActiveCareerProfile', () => {
        beforeEach(() => {
            jest.spyOn($rootScope.currentUser, 'hasActiveCareerProfile', 'get').mockReturnValue(false);

            jest.spyOn(
                CareersNetworkViewModel.prototype,
                'ensureCandidatePositionInterests',
            ).mockImplementation(() => {});
            jest.spyOn(
                CareersNetworkViewModel.prototype,
                'ensureHiringRelationshipsLoaded',
            ).mockImplementation(() => {});
            jest.spyOn(
                CareersNetworkViewModel.prototype,
                'getMyHiringRelationshipViewModelForConnectionId',
            ).mockImplementation(() => {});

            $location.search('list', 'open');
            renderer = SpecHelper.renderer();
            renderer.render('<featured-positions></featured-positions>');
            elem = renderer.elem;
            scope = elem.isolateScope();

            expect(CareersNetworkViewModel.prototype.ensureCandidatePositionInterests).not.toHaveBeenCalled();
            expect(CareersNetworkViewModel.prototype.ensureHiringRelationshipsLoaded).not.toHaveBeenCalled();
            expect(
                CareersNetworkViewModel.prototype.getMyHiringRelationshipViewModelForConnectionId,
            ).not.toHaveBeenCalled();
        });

        it('should disable filters', () => {
            expect(scope.disableFilters).toBe(true);
        });

        it('should display message explaining their profile is inactive with working link to /careers/preview to easily activate their profile', () => {
            SpecHelper.expectNoElement(elem, '.card');
            SpecHelper.expectElement(elem, '.no-positions');
            SpecHelper.expectElementText(
                elem,
                '.no-positions p',
                'Your career profile is inactive. To view and express interest in featured positions, update your profile status.',
            );
        });
    });

    describe('when noPositions', () => {
        it('should display no open positions', () => {
            renderEmptyListAndAssertFiltersDisabled('open');
        });

        it('should display no accepted positions', () => {
            renderEmptyListAndAssertFiltersDisabled('accepted');
        });

        it('should display no connected positions', () => {
            renderEmptyListAndAssertFiltersDisabled('connected');
        });

        it('should display no rejected positions', () => {
            renderEmptyListAndAssertFiltersDisabled('rejected');
        });

        it('should display message about filters when no positions with filters being used', () => {
            render({
                serverPaginatedPositions: [],
                clientPaginatedPositions: [],
                list: 'accepted',
            });
            expect(scope.noPositions).toBe(true);
            SpecHelper.expectNoElement(elem, '.card');
            SpecHelper.expectElement(elem, '.no-positions');
            SpecHelper.expectElementText(elem, '.no-positions p', 'No positions found.');
            SpecHelper.updateSelect(elem, '.industry select', 'military');
            SpecHelper.expectElementText(elem, '.no-positions p', 'No positions found. Please adjust your filters.');

            // and remove it if you select no filters
            SpecHelper.click(elem, '.clear-all');
            SpecHelper.expectElementText(elem, '.no-positions p', 'No positions found.');
        });

        function renderEmptyListAndAssertFiltersDisabled(list) {
            render({
                serverPaginatedPositions: [],
                clientPaginatedPositions: [],
                list,
            });
            expect(scope.noPositions).toBe(true);
            SpecHelper.expectNoElement(elem, '.card');
            SpecHelper.expectElement(elem, '.no-positions');
            assertFiltersDisabled();
        }
    });

    describe('display cards', () => {
        it('should display open positions', () => {
            render();
            SpecHelper.expectElements(elem, '.card', 2);
        });

        it('should display accepted position', () => {
            render({
                list: 'accepted',
            });
            SpecHelper.expectElements(elem, '.card', 2);
        });

        it('should display connected position', () => {
            render({
                list: 'connected',
            });
            SpecHelper.expectElement(elem, '.card');
        });

        it('should display rejected positions', () => {
            render({
                list: 'rejected',
            });
            SpecHelper.expectElements(elem, '.card', 3);
            SpecHelper.expectElementText(elem, '.card[data-id="4"] .status', 'Hiddenby Me'); // missing space because the local string has a <br>
            SpecHelper.expectElementText(elem, '.card[data-id="6"] .status', 'Closedby Me');
            SpecHelper.expectElementText(elem, '.card[data-id="7"] .status', 'Closed byEmployer');
        });
    });

    describe('removeCard', () => {
        beforeEach(() => {
            jest.spyOn(careersNetworkViewModel, 'saveCandidatePositionInterest').mockImplementation(() => {});
        });

        it('should remove appropriate position from active positions list based on offset', () => {
            renderWithExtraPositionsAndUpdateOffset('open');

            const position = scope.activePositions[1 + scope.offset];
            scope.removeCard(1, 'accepted'); // remove the card at index 1
            expect(_.indexOf(scope.activePositions, position)).toEqual(-1);
        });

        it('should remove appropriate position from unfiltered positions list', () => {
            renderWithExtraPositionsAndUpdateOffset('open');

            const openTabConfig = scope.tabConfigs.open;
            const positionsLength = scope.activePositions.length;
            const originalLength = openTabConfig.initialPositions.length;
            const position = scope.activePositions[1 + scope.offset];

            expect(_.indexOf(openTabConfig.initialPositions, position)).not.toEqual(-1);

            scope.removeCard(1, 'accepted'); // remove the card at index 1

            expect(_.indexOf(openTabConfig.initialPositions, position)).toEqual(-1);
            expect(scope.activePositions.length).toEqual(positionsLength - 1);
            expect(openTabConfig.initialPositions.length).toEqual(originalLength - 1);
        });

        it('should remove a position from the open tab when removing from recommended', () => {
            const openPositions = recommendedPositions.concat(serverPaginatedPositions);
            const originalOpenLength = openPositions.length;
            const originalRecommendedLength = recommendedPositions.length;

            render({
                list: 'recommended',
                serverPaginatedPositions: openPositions,
                recommendedPositions,
            });

            const openTabConfig = scope.tabConfigs.open;
            const recommendedTabConfig = scope.tabConfigs.recommended;

            expect(openTabConfig.initialPositions[0].id).toEqual(recommendedTabConfig.initialPositions[0].id);
            scope.removeCard(0, 'accepted');
            expect(recommendedTabConfig.initialPositions.length).toBe(originalRecommendedLength - 1);
            expect(openTabConfig.initialPositions.length).toBe(originalOpenLength - 1);
        });

        it('should remove a recommended position from the recommended tab when removing from open', () => {
            recommendedPositions = [recommendedPositions[0], recommendedPositions[1]];

            const openPositions = recommendedPositions.concat(serverPaginatedPositions);
            const originalOpenLength = openPositions.length;

            render({
                list: 'open',
                serverPaginatedPositions: openPositions,
                recommendedPositions,
            });

            const openTabConfig = scope.tabConfigs.open;
            const recommendedTabConfig = scope.tabConfigs.recommended;

            expect(openTabConfig.initialPositions[0].id).toEqual(recommendedTabConfig.initialPositions[0].id);
            expect(recommendedTabConfig.translate.values.num).toBe(2);

            scope.removeCard(0, 'accepted');

            expect(recommendedTabConfig.initialPositions.length).toBe(1);
            expect(recommendedTabConfig.translate.values.num).toBe(1);
            expect(openTabConfig.initialPositions.length).toBe(originalOpenLength - 1);

            // This should not be a recommended position being removed now
            // since we originally had two then removed one
            scope.removeCard(1, 'accepted');
            expect(recommendedTabConfig.initialPositions.length).toBe(1);
            expect(recommendedTabConfig.translate.values.num).toBe(1);
            expect(openTabConfig.initialPositions.length).toBe(originalOpenLength - 2);
        });

        describe('cover letter', () => {
            it('should add the coverLetter to a new interest', () => {
                render();

                jest.spyOn(careersNetworkViewModel, 'saveCandidatePositionInterest');

                const coverLetter = {
                    content: 'foo',
                    created_at: new Date(),
                };

                const positionId = scope.tabConfigs.open.initialPositions[0].id;

                scope.removeCard(0, 'accepted', coverLetter);

                const acceptedPosition = scope.tabConfigs.accepted.initialPositions.find(
                    position => position.id === positionId,
                );

                expect(acceptedPosition.$interest.cover_letter).toEqual(coverLetter);
                expect(careersNetworkViewModel.saveCandidatePositionInterest).toHaveBeenCalledWith(
                    acceptedPosition.$interest,
                );
            });

            it('should overwrite coverLetter when new one is provided', () => {
                render();
                jest.spyOn(careersNetworkViewModel, 'saveCandidatePositionInterest');

                const oldCoverLetter = {
                    content: 'foo',
                    created_at: new Date(),
                };

                const newCoverLetter = {
                    content: 'bar',
                    created_at: new Date(),
                };

                const position = scope.tabConfigs.open.initialPositions[0];
                const interest = CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: position.id,
                    cover_letter: oldCoverLetter,
                });

                // mock out interest with cover letter
                position.$interest = interest;

                scope.removeCard(0, 'accepted', newCoverLetter);

                expect(position.$interest.cover_letter).toEqual(newCoverLetter);
                expect(careersNetworkViewModel.saveCandidatePositionInterest).toHaveBeenCalledWith(position.$interest);
            });
        });

        describe('tab num', () => {
            describe('new tab', () => {
                it('should increment and use the totalCount of the filter set if not initialTotalCountIsMin', () => {
                    render({
                        list: 'accepted',
                    });
                    scope.openPositionFilterSet.initialTotalCountIsMin = false;
                    scope.openPositionFilterSet.totalCount = 50;
                    jest.spyOn(scope, 'updateTabNum').mockImplementation(() => {});
                    scope.removeCard(0, 'open');
                    expect(scope.openPositionFilterSet.totalCount).toBe(51);
                    expect(scope.updateTabNum).toHaveBeenCalledWith('open', 51);
                });

                it('should not change if initialTotalCountIsMin', () => {
                    render({
                        list: 'accepted',
                    });
                    scope.openPositionFilterSet.initialTotalCountIsMin = true;
                    scope.tabConfigs.open.translate.values.num = '1337+';
                    scope.removeCard(0, 'open');
                    expect(scope.tabConfigs.open.translate.values.num).toEqual('1337+');
                });

                it('should be the length of the initial positions of the tab config if not using the filter set', () => {
                    render({
                        list: 'accepted',
                    });
                    jest.spyOn(scope, 'updateTabNum').mockImplementation(() => {});
                    scope.removeCard(0, 'rejected');
                    expect(scope.tabConfigs.accepted.initialPositions.length).toBeGreaterThan(0); // sanity check
                    expect(scope.updateTabNum).toHaveBeenCalledWith(
                        'accepted',
                        scope.tabConfigs.accepted.initialPositions.length,
                    );
                });
            });

            describe('old tab', () => {
                it('should increment and use the totalCount of the filter set if not initialTotalCountIsMin', () => {
                    render({
                        list: 'open',
                    });
                    scope.openPositionFilterSet.initialTotalCountIsMin = false;
                    scope.openPositionFilterSet.totalCount = 50;
                    jest.spyOn(scope, 'updateTabNum').mockImplementation(() => {});
                    scope.removeCard(0, 'accepted');
                    expect(scope.openPositionFilterSet.totalCount).toBe(49);
                    expect(scope.updateTabNum).toHaveBeenCalledWith('open', 49);
                });

                it('should not change if initialTotalCountIsMin', () => {
                    render({
                        list: 'open',
                    });
                    scope.openPositionFilterSet.initialTotalCountIsMin = true;
                    scope.tabConfigs.open.translate.values.num = '1337+';
                    scope.removeCard(0, 'accepted');
                    expect(scope.tabConfigs.open.translate.values.num).toEqual('1337+');
                });

                it('should be the length of the initial positions of the tab config if not using the filter set', () => {
                    render({
                        list: 'accepted',
                    });
                    jest.spyOn(scope, 'updateTabNum').mockImplementation(() => {});
                    scope.removeCard(0, 'rejected');
                    expect(scope.tabConfigs.rejected.initialPositions.length).toBeGreaterThan(0); // sanity check
                    expect(scope.updateTabNum).toHaveBeenCalledWith(
                        'rejected',
                        scope.tabConfigs.rejected.initialPositions.length,
                    );
                });
            });
        });

        function renderWithExtraPositionsAndUpdateOffset(list) {
            const positions = list === 'open' ? serverPaginatedPositions : clientPaginatedPositions;

            // create more positions so the pagination controls are enabled
            for (let i = positions.length + 1; i < 20; i++) {
                positions.push(
                    OpenPosition.new({
                        id: i,
                        role: 'marketing',
                        position_descriptors: ['permanent'],
                    }),
                );
            }
            render({
                list,
            });

            // update the offset to the listLimit
            scope.offset = scope.listLimit;
            scope.$digest();
        }
    });

    describe('actions', () => {
        describe('hide', () => {
            const actions = [
                {
                    currentStatus: 'open',
                    newStatus: 'rejected',
                    button: 'hide',
                    remainingCards: 1,
                },
                {
                    currentStatus: 'accepted',
                    newStatus: 'rejected',
                    button: 'hide',
                    remainingCards: 1,
                },
            ];

            actions.forEach(action => {
                it(`should ${action.button} the position and remove it from the ${action.currentStatus} list`, () => {
                    render({
                        list: action.currentStatus,
                    });

                    CandidatePositionInterest.expect('create');
                    jest.spyOn(scope, 'removeCard');

                    SpecHelper.click(elem, `.card:eq(0) [name=${action.button}]`);
                    CandidatePositionInterest.flush('create');

                    expect(scope.removeCard).toHaveBeenCalledWith(0, action.newStatus);
                    SpecHelper.expectElements(elem, '.card', action.remainingCards);
                });
            });
        });

        describe('apply', () => {
            it('should open a modal for internal positions', () => {
                render();

                jest.spyOn(DialogModal, 'alert');
                scope.apply(0, scope.activePositions[0]);
                expect(DialogModal.alert).toHaveBeenCalled();
            });
            it('should open external position URL and fire event', () => {
                render();

                const externalPosition = OpenPosition.new({
                    external: true,
                    id: 'zip_id',
                    url: 'https://foobar.com',
                    title: 'zip recruiter position',
                    industry_name: 'technology',
                    location: 'Some City, USA',
                    hiring_application: {
                        company_name: 'Foo Bar Inc.',
                    },
                });

                const internalPositions = [];
                scope.openPositionFilterSet.results = internalPositions;
                scope.externalPositions = [externalPosition];
                scope.mergeInternalAndExternalPositions();

                jest.spyOn(EventLogger.prototype, 'log');
                jest.spyOn($window, 'open');

                scope.apply(0, scope.activePositions[0]);
                expect(EventLogger.prototype.log).toHaveBeenCalledWith('candidate:clicked_external_position_url', {
                    external_position_id: externalPosition.id,
                    external_position_url: externalPosition.url,
                    external_position_title: externalPosition.title,
                    external_position_industry: externalPosition.industry_name,
                    external_position_location: externalPosition.location,
                    external_position_company_name: externalPosition.hiring_application.company_name,
                });
                expect($window.open).toHaveBeenCalledWith(externalPosition.url);
            });
        });
    });

    describe('viewCoverLetter', () => {
        it('should open a modal', () => {
            render({
                list: 'accepted',
            });

            jest.spyOn(DialogModal, 'alert');

            scope.viewCoverLetter(clientPaginatedPositions[0]);

            expect(DialogModal.alert).toHaveBeenCalled();
        });
    });

    describe('filters', () => {
        describe('when changed', () => {
            it('should normally call setSelectedFilterOptions and applyFilters', () => {
                render();
                jest.spyOn(scope, 'setSelectedFilterOptions').mockImplementation(() => {});
                jest.spyOn(scope, 'applyFilters').mockImplementation(() => {});
                scope.filters = _.extend(scope.filters, {
                    foo: true,
                });
                scope.$digest();
                expect(scope.setSelectedFilterOptions).toHaveBeenCalled();
                expect(scope.applyFilters).toHaveBeenCalled();
            });

            it('should not call setSelectedFilterOptions nor applyFilters if user does not have an active profile', () => {
                jest.spyOn($rootScope.currentUser, 'hasActiveCareerProfile', 'get').mockReturnValue(false);

                $location.search('list', 'accepted');
                renderer = SpecHelper.renderer();
                renderer.render('<featured-positions></featured-positions>');
                elem = renderer.elem;
                scope = elem.isolateScope();

                jest.spyOn(scope, 'setSelectedFilterOptions').mockImplementation(() => {});
                jest.spyOn(scope, 'applyFilters').mockImplementation(() => {});
                scope.filters = _.extend(scope.filters, {
                    foo: true,
                });
                scope.$digest();
                expect(scope.setSelectedFilterOptions).not.toHaveBeenCalled();
                expect(scope.applyFilters).not.toHaveBeenCalled();
            });
        });

        describe('client pagination', () => {
            it('should filter based on role', () => {
                render({
                    list: 'accepted',
                });
                SpecHelper.expectElements(elem, 'position-card', 2);
                SpecHelper.updateSelect(elem, 'choose-a-role select', 'ux_ui');

                // expect only one card to be returned
                const resultingCard = SpecHelper.expectElement(elem, 'position-card');
                expect(resultingCard.isolateScope().position.role).toBe('ux_ui');
            });

            it('should filter based on nested role', () => {
                render({
                    list: 'accepted',
                });
                SpecHelper.expectElements(elem, 'position-card', 2);
                SpecHelper.updateSelect(elem, 'choose-a-role select', 'design'); // ux_ui should return true since it is a child of design

                // expect only one card to be returned
                const resultingCard = SpecHelper.expectElement(elem, 'position-card');
                expect(resultingCard.isolateScope().position.role).toBe('ux_ui');
            });

            it('should filter based on location', () => {
                render({
                    list: 'accepted',
                });
                SpecHelper.expectElements(elem, 'position-card', 2);

                jest.spyOn(scope.activePositions[0], 'distanceTo').mockReturnValue(100);
                jest.spyOn(scope.activePositions[1], 'distanceTo').mockReturnValue(101);

                scope.filters.places = [locationPlaceDetails.boston];
                scope.$digest();

                // expect only one card to be returned
                SpecHelper.expectElement(elem, 'position-card');
                assertFiltersEnabled();
            });

            it('should filter based on position_descriptors', () => {
                render({
                    list: 'accepted',
                });
                SpecHelper.expectElements(elem, 'position-card', 2);
                SpecHelper.updateSelect(elem, '.job-type select', 'permanent');

                // expect only one card to be returned
                const resultingCard = SpecHelper.expectElement(elem, 'position-card');
                expect(resultingCard.isolateScope().position.position_descriptors).toContain('permanent');
                assertFiltersEnabled();
            });

            it('should filter based on industry', () => {
                render({
                    list: 'accepted',
                });
                SpecHelper.expectElements(elem, 'position-card', 2);
                SpecHelper.updateSelect(elem, '.industry select', 'consulting');

                // expect only one card to be returned
                const resultingCard = SpecHelper.expectElement(elem, 'position-card');
                expect(resultingCard.isolateScope().position.hiring_application.industry).toBe('consulting');
                assertFiltersEnabled();
            });

            it('should filter based on years_experience', () => {
                render({
                    list: 'accepted',
                });
                SpecHelper.expectElements(elem, 'position-card', 2);
                SpecHelper.updateSelect(elem, '.years-experience select', '0_1_years');

                // expect only one card to be returned
                const resultingCard = SpecHelper.expectElement(elem, 'position-card');
                expect(resultingCard.isolateScope().position.desired_years_experience.min).toBe(1);
                expect(resultingCard.isolateScope().position.desired_years_experience.max).toBe(2);
                assertFiltersEnabled();
            });

            it('should display no open positions when you filter an unused industry', () => {
                render({
                    list: 'accepted',
                });
                SpecHelper.expectElements(elem, 'position-card', 2);
                SpecHelper.updateSelect(elem, '.industry select', 'military');
                SpecHelper.expectNoElement(elem, 'position-card');
                SpecHelper.expectElement(elem, '.no-positions');
                assertFiltersEnabled();
            });
        });

        describe('server pagination', () => {
            beforeEach(() => {
                render();
                jest.spyOn(scope, 'createNewOpenPositionFilterSet').mockImplementation(() => $q.when({}));
            });

            it('should filter based on role', () => {
                SpecHelper.updateSelect(elem, 'choose-a-role select', 'ux_ui');
                expect(scope.createNewOpenPositionFilterSet).toHaveBeenCalled();
            });

            it('should filter based on nested role', () => {
                SpecHelper.updateSelect(elem, 'choose-a-role select', 'design'); // ux_ui should return true since it is a child of design
                expect(scope.createNewOpenPositionFilterSet).toHaveBeenCalled();
            });

            it('should filter based on location', () => {
                scope.filters.places = [locationPlaceDetails.boston];
                scope.$digest();
                expect(scope.createNewOpenPositionFilterSet).toHaveBeenCalled();
            });

            it('should filter based on position_descriptors', () => {
                SpecHelper.updateSelect(elem, '.job-type select', 'permanent');
                expect(scope.createNewOpenPositionFilterSet).toHaveBeenCalled();
            });

            it('should filter based on industry', () => {
                SpecHelper.updateSelect(elem, '.industry select', 'consulting');
                expect(scope.createNewOpenPositionFilterSet).toHaveBeenCalled();
            });

            it('should filter based on years_experience', () => {
                SpecHelper.updateSelect(elem, '.years-experience select', '0_1_years');
                expect(scope.createNewOpenPositionFilterSet).toHaveBeenCalled();
            });
        });
    });

    describe('createNewOpenPositionFilterSet', () => {
        it('should initialize a new OpenPositionFilterSet on the scope', () => {
            render();
            const origOpenPositionFilterSet = scope.openPositionFilterSet;

            scope.createNewOpenPositionFilterSet();
            expect(scope.openPositionFilterSet).not.toEqual(origOpenPositionFilterSet);
        });

        it('should call ensureResultsPreloaded on newly initialized OpenPositionFilterSet', () => {
            render();

            scope.openPositionFilterSet.ensureResultsPreloaded.mockReset(); // it's called on render
            jest.spyOn(scope.openPositionFilterSet, 'ensureResultsPreloaded').mockImplementation(
                () => $q.defer().promise,
            );
            expect(scope.openPositionFilterSet.ensureResultsPreloaded).not.toHaveBeenCalled();

            scope.createNewOpenPositionFilterSet();
            expect(scope.openPositionFilterSet.ensureResultsPreloaded).toHaveBeenCalled();
        });
    });

    describe('mergeInternalAndExternalPositions', () => {
        let internalPositions;
        let externalPositions;

        beforeEach(() => {
            internalPositions = [
                OpenPosition.fixtures.getInstance({
                    title: 'foo',
                }),
                OpenPosition.fixtures.getInstance({
                    title: 'bar',
                }),
            ];
            internalPositions[0].recommended = true;
            externalPositions = [
                OpenPosition.new({
                    id: 'zip_id',
                    title: 'zip recruiter position',
                }),
            ];
        });

        it('should prioritize external positions over non-recommended internal positions when no selected roles', () => {
            render();

            scope.openPositionFilterSet.results = internalPositions;
            scope.externalPositions = externalPositions;

            scope.mergeInternalAndExternalPositions();

            const openTabConfig = scope.tabConfigs.open;
            expect(openTabConfig.initialPositions).toEqual([
                internalPositions[0],
                ...scope.externalPositions,
                internalPositions[1],
            ]);
            expect(openTabConfig.filteredPositions).toEqual([
                internalPositions[0],
                ...scope.externalPositions,
                internalPositions[1],
            ]);
        });

        it('should prioritize all internal positions over external positions when selected roles', () => {
            render();

            scope.openPositionFilterSet.results = internalPositions;
            scope.externalPositions = externalPositions;

            scope.filters.role = ['foo', 'bar'];
            scope.mergeInternalAndExternalPositions();

            const openTabConfig = scope.tabConfigs.open;
            expect(openTabConfig.initialPositions).toEqual([...internalPositions, ...scope.externalPositions]);
            expect(openTabConfig.filteredPositions).toEqual([...internalPositions, ...scope.externalPositions]);
        });

        it('should work with with no externalPositions', () => {
            render();

            scope.openPositionFilterSet.results = internalPositions;
            scope.externalPositions = undefined;

            scope.mergeInternalAndExternalPositions();

            const openTabConfig = scope.tabConfigs.open;
            expect(openTabConfig.initialPositions).toEqual([...internalPositions]);
            expect(openTabConfig.filteredPositions).toEqual([...internalPositions]);
        });

        it('should work with no openPositionFilterSet results', () => {
            render();

            scope.openPositionFilterSet.results = undefined;
            scope.externalPositions = externalPositions;

            scope.mergeInternalAndExternalPositions();

            const openTabConfig = scope.tabConfigs.open;
            expect(openTabConfig.initialPositions).toEqual([...scope.externalPositions]);
            expect(openTabConfig.filteredPositions).toEqual([...scope.externalPositions]);
        });
    });

    describe('fetchInternalAndExternalPositions', () => {
        it('should set open tab state after ensureResultsLoaded is called', () => {
            const positions = [
                OpenPosition.fixtures.getInstance({
                    title: 'foo',
                }),
                OpenPosition.fixtures.getInstance({
                    title: 'bar',
                }),
            ];

            // Note: render() handles the resolving of the ensureResultsPreloaded call
            render({
                serverPaginatedPositions: positions,
            });

            const openTabConfig = scope.tabConfigs.open;
            expect(openTabConfig.initialPositions).toEqual(positions);
            expect(openTabConfig.filteredPositions).toEqual(positions);
            expect(openTabConfig.translate.values.num).toBe(0);
            expect(openTabConfig.loading).toBe(false);
            expect(scope.activePositions).toEqual(positions);
        });

        it('should reset open tab state', () => {
            render();

            scope.fetchInternalAndExternalPositions();

            const openTabConfig = scope.tabConfigs.open;
            expect(openTabConfig.loading).toBe(true);
            expect(openTabConfig.initialPositions).toBeNull();
            expect(openTabConfig.filteredPositions).toBeNull();
            expect(openTabConfig.translate.values.num).toBe('…');

            expect(scope.openPositionFilterSet.filters).toEqual(scope.filters);
        });
    });

    describe('applyFilters', () => {
        it('should fetchInternalAndExternalPositions for the open tab', () => {
            render();
            jest.spyOn(scope, 'fetchInternalAndExternalPositions').mockImplementation(() => $q.when({}));
            scope.applyFilters();
            expect(scope.fetchInternalAndExternalPositions).toHaveBeenCalled();
        });

        it('should filter client paginated positions when not the open tab', () => {
            render({
                list: 'accepted',
            });
            jest.spyOn(scope, 'filterClientPaginatedPositions').mockImplementation(() => {});
            scope.applyFilters();
            expect(scope.filterClientPaginatedPositions).toHaveBeenCalled();
        });
    });

    describe('setSelectedFilterOptions', () => {
        it('should set filter options in the UI', () => {
            render();
            scope.filters.role = ['ux_ui', 'marketing'];
            scope.$digest();
            SpecHelper.expectElementText(elem, '.filter-options', 'UX / UI Marketing Clear all');
        });
    });

    describe('removeSelectedFilterOption', () => {
        it('should remove filter option in the UI', () => {
            render();
            scope.filters.role = ['ux_ui', 'marketing'];
            scope.$digest();
            SpecHelper.expectElementText(elem, '.filter-options', 'UX / UI Marketing Clear all');
            SpecHelper.click(elem, '.filter-options i:eq(0)');
            SpecHelper.expectElementText(elem, '.filter-options', 'Marketing Clear all');
        });
    });

    describe('openConnection', () => {
        it('should work', () => {
            render({
                list: 'connected',
            });
            jest.spyOn($location, 'url').mockImplementation(() => {});
            SpecHelper.click(elem, '.open:eq(0)');
            expect($location.url).toHaveBeenCalledWith(
                `/careers/connections?connectionId=${scope.activePositions[0].hiring_manager_id}`,
            );
        });
    });

    describe('totalCount', () => {
        it('should be the activePositions length if not using the filter set', () => {
            render({
                list: 'accepted',
            });
            jest.spyOn(scope, 'activePositions', 'get').mockReturnValue([
                OpenPosition.fixtures.getInstance(),
                OpenPosition.fixtures.getInstance(),
            ]);
            expect(scope.totalCount).toBe(2);
        });

        it('should be the totalCount of the filter set if not initialTotalCountIsMin', () => {
            render({
                list: 'open',
            });
            jest.spyOn(scope, 'activePositions', 'get').mockReturnValue([
                OpenPosition.fixtures.getInstance(),
                OpenPosition.fixtures.getInstance(),
            ]);
            scope.openPositionFilterSet.initialTotalCountIsMin = false;
            scope.openPositionFilterSet.totalCount = 1337;
            expect(scope.totalCount).toBe(1337);
        });

        it('should be the initialTotalCount with a plus sign if initialTotalCountIsMin', () => {
            render({
                list: 'open',
            });
            jest.spyOn(scope, 'activePositions', 'get').mockReturnValue([
                OpenPosition.fixtures.getInstance(),
                OpenPosition.fixtures.getInstance(),
            ]);
            scope.openPositionFilterSet.initialTotalCountIsMin = true;
            scope.openPositionFilterSet.totalCount = 1337;
            scope.openPositionFilterSet.initialTotalCount = 1338;
            expect(scope.totalCount).toEqual('1338+');
        });
    });

    describe('shouldShowRecommendedBadge', () => {
        it('should be true if recommended and on recommended or open tab', () => {
            const position = OpenPosition.fixtures.getInstance({
                recommended: true,
            });
            render({
                list: 'open',
            });
            expect(scope.shouldShowRecommendedBadge(position)).toBe(true);

            scope.currentTab = 'recommended';
            expect(scope.shouldShowRecommendedBadge(position)).toBe(true);

            position.recommended = false;
            expect(scope.shouldShowRecommendedBadge(position)).toBe(false);

            position.recommended = true;
            scope.currentTab = 'accepted';
            expect(scope.shouldShowRecommendedBadge(position)).toBe(false);
        });
    });

    describe('watchGroup openPositionFilterSet.results and externalPositions', () => {
        beforeEach(() => {
            render();
            jest.spyOn(scope, 'mergeInternalAndExternalPositions');
            expect(scope.mergeInternalAndExternalPositions).not.toHaveBeenCalled();
        });

        it('should call mergeInternalAndExternalPositions if openPositionFilterSet.results changes', () => {
            scope.openPositionFilterSet.results = [];
            scope.$digest();
            expect(scope.mergeInternalAndExternalPositions).toHaveBeenCalled();
        });

        it('should call mergeInternalAndExternalPositions if externalPositions changes', () => {
            scope.externalPositions = [];
            scope.$digest();
            expect(scope.mergeInternalAndExternalPositions).toHaveBeenCalled();
        });
    });

    describe('watchGroup tabConfigs.open.loading, openPositionFilterSet.totalCount, externalPositions.length', () => {
        beforeEach(() => {
            render();
            scope.openPositionFilterSet.totalCount = 2;
            scope.externalPositions = ['baz'];
            jest.spyOn(scope, 'mergeInternalAndExternalPositions').mockImplementation(() => {});
            jest.spyOn(scope, 'updateTabNum').mockImplementation(() => {});
            scope.updateTabNum.mockReset();
        });

        it('should call updateTabNum for open tab with correct number when tabConfigs.open.loading changes', () => {
            scope.tabConfigs.loading = false;
            scope.$digest();
            expect(scope.updateTabNum).toHaveBeenCalledWith('open', 3);
        });

        it('should call updateTabNum for open tab with correct number when openPositionFilterSet.totalCount changes', () => {
            scope.openPositionFilterSet.totalCount = 1;
            scope.$digest();
            expect(scope.updateTabNum).toHaveBeenCalledWith('open', 2);
        });

        it('should call updateTabNum for open tab with correct number when tabConfigs.open.loading changes', () => {
            scope.externalPositions = [];
            scope.$digest();
            expect(scope.updateTabNum).toHaveBeenCalledWith('open', 2);
        });

        it('should not updateTabNum if open tab is loading', () => {
            scope.tabConfigs.open.loading = true;
            scope.$digest();
            expect(scope.updateTabNum).not.toHaveBeenCalled();
        });
    });

    // make sure filters don't disable just because there are no positions
    function assertFiltersEnabled() {
        expect(scope.disableFilters).toBe(false);
    }

    function assertFiltersDisabled() {
        expect(scope.disableFilters).toBe(true);
    }
});
