import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/candidate_position_interest_fixtures';
import 'Careers/angularModule/spec/careers_spec_helper';
import inviteToInterviewLocales from 'Careers/locales/careers/invite_to_interview-en.json';
import salaryLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/salary-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(inviteToInterviewLocales, salaryLocales);

describe('FrontRoyal.Careers.InviteToInterviewFormDir', () => {
    let $injector;
    let SpecHelper;
    let CareersSpecHelper;
    let OpenPosition;
    let renderer;
    let elem;
    let scope;
    let careersNetworkViewModel;
    let $timeout;
    let $q;
    let onFinish;
    let hiringRelationshipViewModel;
    let CandidatePositionInterest;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                $timeout = $injector.get('$timeout');
                $q = $injector.get('$q');
                OpenPosition = $injector.get('OpenPosition');
                CandidatePositionInterest = $injector.get('CandidatePositionInterest');

                $injector.get('CandidatePositionInterestFixtures');
            },
        ]);

        onFinish = jest.fn();

        careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
        hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
    });

    function render(
        initialOpenPositions = [
            OpenPosition.new({
                id: '1',
                hiring_manager_id: careersNetworkViewModel.user.id,
                title: 'open position title',
                salary: 42000,
                bonus: false,
                equity: false,
                available_interview_times: 'midnight!!!!',
            }),
        ],
        candidatePositionInterest,
        showInviteSentCheckmark = true,
    ) {
        jest.spyOn(careersNetworkViewModel, 'ensureOpenPositions').mockReturnValue($q.when(initialOpenPositions));
        renderer = SpecHelper.renderer();
        renderer.scope.onFinish = onFinish;
        renderer.scope.hiringRelationshipViewModel = hiringRelationshipViewModel;
        renderer.scope.candidatePositionInterest = candidatePositionInterest || null;
        renderer.scope.showInviteSentCheckmark = showInviteSentCheckmark;
        renderer.render(
            '<invite-to-interview-form on-finish="onFinish" hiring-relationship-view-model="hiringRelationshipViewModel" candidate-position-interest="candidatePositionInterest" show-invite-sent-checkmark="showInviteSentCheckmark"></invite-to-interview-form',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('invite', () => {
        it('should save with an open position that is NOT featured', () => {
            let resolveSave;
            jest.spyOn(hiringRelationshipViewModel, 'addMessage').mockImplementation(() => {});
            jest.spyOn(careersNetworkViewModel, 'commitOpenPosition').mockImplementation(() => {});
            jest.spyOn(hiringRelationshipViewModel, 'save').mockReturnValue(
                $q(resolve => {
                    resolveSave = resolve;
                }),
            );

            render();

            // position isn't featured, so save checkbox shouldn't appear
            SpecHelper.expectNoElement(elem, '[name=save]');

            SpecHelper.submitForm(elem);
            expect(hiringRelationshipViewModel.addMessage).toHaveBeenCalledWith(
                scope.proxy.messageBody,
                {
                    senderCompanyName: hiringRelationshipViewModel.hiringManagerCompany,
                    openPosition: _.pick(scope.openPosition, [
                        'id',
                        'available_interview_times',
                        'company',
                        'title',
                        'salary',
                        'bonus',
                        'equity',
                        'job_post_url',
                        'description',
                        'hiring_manager_id',
                    ]),
                },
                {
                    candidate_position_interest_id: undefined, // undefined because there's no candidatePositionInterest on the scope
                },
            );
            expect(careersNetworkViewModel.commitOpenPosition).toHaveBeenCalledWith(scope.openPosition);
            expect(hiringRelationshipViewModel.save).toHaveBeenCalledWith({
                openPosition: scope.openPosition,
            });

            resolveSave();
            assertTimeoutAndFinish();
        });

        it('should save a featured position with checkbox is checked', () => {
            let resolveSave;
            jest.spyOn(hiringRelationshipViewModel, 'addMessage').mockImplementation(() => {});
            jest.spyOn(careersNetworkViewModel, 'commitOpenPosition').mockImplementation(() => {});
            jest.spyOn(hiringRelationshipViewModel, 'save').mockReturnValue(
                $q(resolve => {
                    resolveSave = resolve;
                }),
            );

            render([
                OpenPosition.new({
                    id: '1',
                    hiring_manager_id: careersNetworkViewModel.user.id,
                    title: 'open position title',
                    salary: 42000,
                    bonus: false,
                    equity: false,
                    available_interview_times: 'midnight!!!!',
                    featured: true,
                }),
            ]);

            // elect to save our changes
            SpecHelper.toggleCheckbox(elem, '[name=save]');
            SpecHelper.submitForm(elem);

            // should send message
            expect(hiringRelationshipViewModel.addMessage).toHaveBeenCalled();
            const args = hiringRelationshipViewModel.addMessage.mock.calls[0];
            expect(args[0]).toEqual(scope.proxy.messageBody);
            expect(args[1].senderCompanyName).toEqual(hiringRelationshipViewModel.hiringManagerCompany);
            expect(args[1].openPosition.id).toEqual(scope.openPosition.id);

            // should commit position
            expect(careersNetworkViewModel.commitOpenPosition).toHaveBeenCalledWith(scope.openPosition);
            // should save position
            expect(hiringRelationshipViewModel.save).toHaveBeenCalledWith({
                openPosition: scope.openPosition,
            });

            resolveSave();
            assertTimeoutAndFinish();
        });

        it('should NOT save a featured position with checkbox is NOT checked', () => {
            let resolveSave;
            jest.spyOn(hiringRelationshipViewModel, 'addMessage').mockImplementation(() => {});
            jest.spyOn(careersNetworkViewModel, 'commitOpenPosition').mockImplementation(() => {});
            jest.spyOn(hiringRelationshipViewModel, 'save').mockReturnValue(
                $q(resolve => {
                    resolveSave = resolve;
                }),
            );

            render([
                OpenPosition.new({
                    id: '1',
                    hiring_manager_id: careersNetworkViewModel.user.id,
                    title: 'open position title',
                    salary: 42000,
                    bonus: false,
                    equity: false,
                    available_interview_times: 'midnight!!!!',
                    featured: true,
                }),
            ]);

            // don't elect to save our changes
            SpecHelper.submitForm(elem);

            // should send message
            expect(hiringRelationshipViewModel.addMessage).toHaveBeenCalled();
            const args = hiringRelationshipViewModel.addMessage.mock.calls[0];
            expect(args[0]).toEqual(scope.proxy.messageBody);
            expect(args[1].senderCompanyName).toEqual(hiringRelationshipViewModel.hiringManagerCompany);
            expect(args[1].openPosition.id).toEqual(scope.openPosition.id);

            // should NOT commit position
            expect(careersNetworkViewModel.commitOpenPosition).not.toHaveBeenCalled();
            // should save WITHOUT changing position
            expect(hiringRelationshipViewModel.save).toHaveBeenCalledWith({});

            resolveSave();
            assertTimeoutAndFinish();
        });

        it('should save a candidate position interest if provided', () => {
            let resolveSave;
            let candidatePositionInterest;
            jest.spyOn(hiringRelationshipViewModel, 'addMessage').mockImplementation(() => {
                // see note in code about why this has to be set to reviewed before we
                // call addMessage
                expect(candidatePositionInterest.hiring_manager_status).toEqual('invited');
            });
            jest.spyOn(careersNetworkViewModel, 'commitOpenPosition').mockImplementation(() => {});
            jest.spyOn(hiringRelationshipViewModel, 'save').mockReturnValue(
                $q(resolve => {
                    resolveSave = resolve;
                }),
            );

            const openPosition = OpenPosition.new({
                id: '1',
                hiring_manager_id: careersNetworkViewModel.user.id,
                title: 'open position title',
                salary: 42000,
                bonus: false,
                equity: false,
                available_interview_times: 'midnight!!!!',
                featured: true,
            });

            candidatePositionInterest = CandidatePositionInterest.fixtures.getInstance({
                hiring_manager_id: careersNetworkViewModel.user.id,
                open_position_id: openPosition.id,
                candidate_status: 'accepted',
                hiring_manager_status: 'unseen',
            });

            render([openPosition]);

            scope.openPosition = openPosition;
            scope.candidatePositionInterest = candidatePositionInterest;

            // don't elect to save our openPosition changes
            SpecHelper.submitForm(elem);

            // should send message
            expect(hiringRelationshipViewModel.addMessage).toHaveBeenCalled();
            const args = hiringRelationshipViewModel.addMessage.mock.calls[0];
            expect(args[0]).toEqual(scope.proxy.messageBody);
            expect(args[1].senderCompanyName).toEqual(hiringRelationshipViewModel.hiringManagerCompany);
            expect(args[1].openPosition.id).toEqual(scope.openPosition.id);

            // should NOT commit position
            expect(careersNetworkViewModel.commitOpenPosition).not.toHaveBeenCalled();
            // should save WITHOUT changing position
            expect(hiringRelationshipViewModel.save).toHaveBeenCalledWith({
                candidatePositionInterest,
            });

            expect(candidatePositionInterest.hiring_manager_status).toBe('invited');

            resolveSave();
            assertTimeoutAndFinish();
        });

        it('should not pass along candidate_position_interest_id if candidate position interest has no cover letter', () => {
            let resolveSave;
            let candidatePositionInterest;
            jest.spyOn(hiringRelationshipViewModel, 'addMessage').mockImplementation(() => {});
            jest.spyOn(careersNetworkViewModel, 'commitOpenPosition').mockImplementation(() => {});
            jest.spyOn(hiringRelationshipViewModel, 'save').mockReturnValue(
                $q(resolve => {
                    resolveSave = resolve;
                }),
            );

            const openPosition = OpenPosition.new({
                id: '1',
                hiring_manager_id: careersNetworkViewModel.user.id,
                title: 'open position title',
                salary: 42000,
                bonus: false,
                equity: false,
                available_interview_times: 'midnight!!!!',
                featured: true,
            });

            candidatePositionInterest = CandidatePositionInterest.fixtures.getInstance({
                hiring_manager_id: careersNetworkViewModel.user.id,
                open_position_id: openPosition.id,
                candidate_status: 'accepted',
                hiring_manager_status: 'unseen',
            });
            const hasCoverLetterSpy = jest
                .spyOn(candidatePositionInterest, 'hasCoverLetter', 'get')
                .mockReturnValue(false);

            render([openPosition]);

            scope.openPosition = openPosition;
            scope.candidatePositionInterest = candidatePositionInterest;

            scope.invite();

            expect(hasCoverLetterSpy).toHaveBeenCalled();

            // should send message
            expect(hiringRelationshipViewModel.addMessage).toHaveBeenCalled();
            const args = hiringRelationshipViewModel.addMessage.mock.calls[0];
            expect(args[2].candidate_position_interest_id).toBeUndefined();

            resolveSave();
            assertTimeoutAndFinish();
        });

        it('should pass along candidate_position_interest_id if candidate position interest hasCoverLetter', () => {
            let resolveSave;
            let candidatePositionInterest;
            jest.spyOn(hiringRelationshipViewModel, 'addMessage').mockImplementation(() => {});
            jest.spyOn(careersNetworkViewModel, 'commitOpenPosition').mockImplementation(() => {});
            jest.spyOn(hiringRelationshipViewModel, 'save').mockReturnValue(
                $q(resolve => {
                    resolveSave = resolve;
                }),
            );

            const openPosition = OpenPosition.new({
                id: '1',
                hiring_manager_id: careersNetworkViewModel.user.id,
                title: 'open position title',
                salary: 42000,
                bonus: false,
                equity: false,
                available_interview_times: 'midnight!!!!',
                featured: true,
            });

            candidatePositionInterest = CandidatePositionInterest.fixtures.getInstance({
                hiring_manager_id: careersNetworkViewModel.user.id,
                open_position_id: openPosition.id,
                candidate_status: 'accepted',
                hiring_manager_status: 'unseen',
            });
            const hasCoverLetterSpy = jest
                .spyOn(candidatePositionInterest, 'hasCoverLetter', 'get')
                .mockReturnValue(true);

            render([openPosition]);

            scope.openPosition = openPosition;
            scope.candidatePositionInterest = candidatePositionInterest;

            scope.invite();

            expect(hasCoverLetterSpy).toHaveBeenCalled();

            // should send message
            expect(hiringRelationshipViewModel.addMessage).toHaveBeenCalled();
            const args = hiringRelationshipViewModel.addMessage.mock.calls[0];
            expect(args[2].candidate_position_interest_id).toEqual(candidatePositionInterest.id);

            resolveSave();
            assertTimeoutAndFinish();
        });

        it('should resolve immediately without a timeout, not set inviteSent, and not show the big checkmark when !showInviteSentCheckmark', () => {
            let resolveSave;
            jest.spyOn(hiringRelationshipViewModel, 'addMessage').mockImplementation(() => {});
            jest.spyOn(careersNetworkViewModel, 'commitOpenPosition').mockImplementation(() => {});
            jest.spyOn(hiringRelationshipViewModel, 'save').mockReturnValue(
                $q(resolve => {
                    resolveSave = resolve;
                }),
            );

            render(
                [
                    OpenPosition.new({
                        id: '1',
                        hiring_manager_id: careersNetworkViewModel.user.id,
                        title: 'open position title',
                        salary: 42000,
                        bonus: false,
                        equity: false,
                        available_interview_times: 'midnight!!!!',
                        featured: true,
                    }),
                ],
                null,
                false,
            );
            expect(scope.inviteSent).not.toBe(true);

            // elect to save our changes
            SpecHelper.toggleCheckbox(elem, '[name=save]');
            SpecHelper.submitForm(elem);

            // should send message
            expect(hiringRelationshipViewModel.addMessage).toHaveBeenCalled();
            const args = hiringRelationshipViewModel.addMessage.mock.calls[0];
            expect(args[0]).toEqual(scope.proxy.messageBody);
            expect(args[1].senderCompanyName).toEqual(hiringRelationshipViewModel.hiringManagerCompany);
            expect(args[1].openPosition.id).toEqual(scope.openPosition.id);

            // should commit position
            expect(careersNetworkViewModel.commitOpenPosition).toHaveBeenCalledWith(scope.openPosition);
            // should save position
            expect(hiringRelationshipViewModel.save).toHaveBeenCalledWith({
                openPosition: scope.openPosition,
            });

            resolveSave();
            expect(scope.inviteSent).not.toBe(true);
            expect(scope.onFinish).not.toHaveBeenCalled();
            SpecHelper.expectElement(elem, '.front-royal-form-container');
            SpecHelper.expectNoElement(elem, '.sent-message');
            scope.$digest(); // propogate the promise to the `.then()` callback so that onFinish is called
            expect(scope.inviteSent).not.toBe(true);
            expect(scope.onFinish).toHaveBeenCalled();
            SpecHelper.expectElement(elem, '.front-royal-form-container');
            SpecHelper.expectNoElement(elem, '.sent-message');
        });

        function assertTimeoutAndFinish() {
            expect(scope.inviteSent).not.toBe(true);
            $timeout.flush(0);
            expect(scope.inviteSent).toBe(true);

            SpecHelper.expectNoElement(elem, '.front-royal-form-container');
            SpecHelper.expectElement(elem, '.sent-message');

            $timeout.flush(1749);
            expect(scope.onFinish).not.toHaveBeenCalled();
            $timeout.flush(1);
            expect(scope.onFinish).toHaveBeenCalled();
        }
    });

    describe('error handling', () => {
        it('should pass result through to onfinish', () => {
            let rejectSave;
            jest.spyOn(hiringRelationshipViewModel, 'save').mockReturnValue(
                $q((ignore, reject) => {
                    rejectSave = reject;
                }),
            );

            render();

            SpecHelper.submitForm(elem);

            rejectSave();
            $timeout.flush(0);
            SpecHelper.expectNoElement(elem, '.sent-message');
            expect(scope.onFinish).toHaveBeenCalled();
        });
    });

    describe('selectize special handling', () => {
        it('should work', () => {
            render();

            // an initial open position is selected
            const openPosition = scope.openPosition;
            const selectize = elem.find('[name="title"]')[0].selectize;
            // SpecHelper.expectTextInputVal(elem, '[name="title"]+.selectize-control input', scope.openPosition.title);
            SpecHelper.assertSelectizeValue(elem, '[name="title"]', scope.openPosition.id);

            // When the user clicks to focus on the selectize, the selectize
            // input should be deselected (see comment at onTitleSelectizeFocus
            // for an explanation as to why.)
            selectize.settings.onFocus.apply(selectize);
            SpecHelper.assertSelectizeValue(elem, '[name="title"]', undefined);

            // Even though the selectize input as been deselected, the openPosition
            // should remain the same and the rest of the inputs should remain filled
            expect(scope.openPosition).toBe(openPosition);
            SpecHelper.expectTextInputVal(elem, '[name="salary"]', openPosition.salary.toString());

            // When the user blurs the selectize, the selectize should
            // once again show the selected item
            selectize.settings.onBlur.apply(selectize);
            SpecHelper.assertSelectizeValue(elem, '[name="title"]', scope.openPosition.id);
        });

        it('should work when we are initialized with no options', () => {
            render([]);

            // an initial open position is selected
            const openPositionPlaceholder = scope.openPosition;
            expect(openPositionPlaceholder.id).toBeUndefined();

            // set the salary so we can check it is still there later
            SpecHelper.updateInput(elem, '[name="available_interview_times"]', 'whenev');

            const selectize = elem.find('[name="title"]')[0].selectize;
            SpecHelper.assertSelectizeValue(elem, '[name="title"]', undefined);

            // When the user clicks to focus on the selectize, the selectize
            // input should be deselected (see comment at onTitleSelectizeFocus
            // for an explanation as to why.)
            selectize.settings.onFocus.apply(selectize);
            SpecHelper.assertSelectizeValue(elem, '[name="title"]', undefined);

            // Even though the selectize input as been deselected, the openPosition
            // should remain the same and the rest of the inputs should remain filled
            expect(scope.openPosition).toBe(openPositionPlaceholder);

            // When the user blurs the selectize, the selectize should
            // once again show the selected item
            selectize.settings.onBlur.apply(selectize);
            SpecHelper.assertSelectizeValue(elem, '[name="title"]', undefined);

            assertUpdateSelectizeCreatesOpenPosition();
        });
    });

    it('should sort by last_invitation_at', () => {
        const openPositions = [
            OpenPosition.new({
                hiring_manager_id: careersNetworkViewModel.user.id,
                id: '1',
                last_invitation_at: 1,
            }),
            OpenPosition.new({
                hiring_manager_id: careersNetworkViewModel.user.id,
                id: '3',
                last_invitation_at: 3,
            }),
            OpenPosition.new({
                hiring_manager_id: careersNetworkViewModel.user.id,
                id: '2',
                last_invitation_at: 2,
            }),
        ];
        render(openPositions);
        expect(scope.openPosition.id).toEqual(openPositions[1].id);
    });

    it('should work if we start off with no open positions', () => {
        render([]);

        SpecHelper.expectElementDisabled(elem, 'button[type=submit]');
        SpecHelper.updateInput(elem, '[name=available_interview_times]', 'anytime');
        const newOpenPosition = assertUpdateSelectizeCreatesOpenPosition();

        expect(newOpenPosition.hiring_manager_id).toBe(
            hiringRelationshipViewModel.hiringRelationship.hiring_manager_id,
        );
        expect(newOpenPosition.title).toBe('new title');
        expect(newOpenPosition.available_interview_times).toBe('anytime');

        SpecHelper.expectElementEnabled(elem, 'button[type=submit]');
    });

    describe('salary input', () => {
        it('should be set when openPosition is selected', () => {
            render();
            SpecHelper.updateSelectize(elem, '[name=title]', scope.openPositions[0].id);
            SpecHelper.expectTextInputVal(elem, '[name=salary]', '42000');
        });
    });

    describe('messaging', () => {
        it('should have normal messageBody', () => {
            Object.defineProperty(hiringRelationshipViewModel.currentUser, 'nickname', {
                value: 'Hiring Manager',
            });
            render();
            expect(scope.proxy.messageBody).toEqual(
                'Hello Connection 1,\n\nWould love to connect with you and hear about your experience and share more information about ACME products. What is your availability this or next week?\n\nLook forward to connecting soon,\nHiring Manager',
            );
        });

        it('should have messageBody when inviting from a position', () => {
            Object.defineProperty(hiringRelationshipViewModel.currentUser, 'nickname', {
                value: 'Hiring Manager',
            });
            render(undefined, {
                foo: 'bar',
                bar: 'foo',
            });
            expect(scope.proxy.messageBody).toEqual(
                'Hello Connection 1,\n\nThanks for your interest in ACME products! It would be great to connect with you, hear about your experience, and share more information about our company. What is your availability this or next week?\n\nLook forward to connecting soon,\nHiring Manager',
            );
        });

        it("should use nickname in messageBody regardless of hiring manager's _showPhotosAndNames setting", () => {
            Object.defineProperty(hiringRelationshipViewModel.currentUser, 'nickname', {
                value: 'Hiring Manager',
            });
            Object.defineProperty(hiringRelationshipViewModel.connectionProfile, 'nickname', {
                value: 'Junior',
            });
            Object.defineProperty(hiringRelationshipViewModel, '_showPhotosAndNames', {
                value: false,
            });
            render();

            // expect initials due to _showPhotosAndNames settings
            SpecHelper.expectElementText(elem, 'header h2', 'C. 1.');
            // expect nickname in message regardless of _showPhotosAndNames setting
            expect(scope.proxy.messageBody).toEqual(
                'Hello Junior,\n\nWould love to connect with you and hear about your experience and share more information about ACME products. What is your availability this or next week?\n\nLook forward to connecting soon,\nHiring Manager',
            );
        });

        it("should use fullname when there's no nickname in messageBody regardless of hiring manager's _showPhotosAndNames setting", () => {
            Object.defineProperty(hiringRelationshipViewModel.currentUser, 'nickname', {
                value: 'Hiring Manager',
            });
            Object.defineProperty(hiringRelationshipViewModel, '_showPhotosAndNames', {
                value: false,
            });
            render();

            // expect initials due to _showPhotosAndNames settings
            SpecHelper.expectElementText(elem, 'header h2', 'C. 1.');
            // expect full name in message regardless of _showPhotosAndNames setting
            expect(scope.proxy.messageBody).toEqual(
                'Hello Connection 1,\n\nWould love to connect with you and hear about your experience and share more information about ACME products. What is your availability this or next week?\n\nLook forward to connecting soon,\nHiring Manager',
            );
        });

        it('submit button should display Invite to Connect without scope.candidatePositionInterest', () => {
            render();
            SpecHelper.expectElementText(elem, ':submit', 'Invite to Connect');
        });

        it('submit button should display Connect with <nickanme> with scope.candidatePositionInterest', () => {
            render(undefined, {
                foo: 'bar',
                bar: 'foo',
            });
            SpecHelper.expectElementText(elem, ':submit', 'Connect with Connection 1');
        });
    });

    function assertUpdateSelectizeCreatesOpenPosition() {
        SpecHelper.selectizeCreateItem(elem, '[name=title]', 'new title');
        const newOpenPosition = scope.openPosition;

        expect(newOpenPosition.isA(OpenPosition)).toBe(true);
        expect(newOpenPosition.id).not.toBeUndefined();
        return newOpenPosition;
    }
});
