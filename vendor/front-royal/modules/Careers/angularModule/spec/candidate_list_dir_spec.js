import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/candidate_position_interest_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Careers/angularModule/spec/careers_spec_helper';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import setSpecLocales from 'Translation/setSpecLocales';
import candidateListLocales from 'Careers/locales/careers/candidate_list-en.json';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';
import hiringBrowseCandidatesLocales from 'Careers/locales/careers/hiring_browse_candidates-en.json';

setSpecLocales(candidateListLocales, candidateListCardLocales, hiringBrowseCandidatesLocales);

describe('FrontRoyal.Careers.CandidateListDir', () => {
    let $injector;
    let CareerProfile;
    let ngModel;
    let SpecHelper;
    let CareersSpecHelper;
    let renderer;
    let elem;
    let scope;
    let getProfilesSpy;
    let CandidatePositionInterest;
    let afterItemRemoval;
    let Cohort;
    let currentUser;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                CareerProfile = $injector.get('CareerProfile');
                $injector.get('CareerProfileFixtures');
                Cohort = $injector.get('Cohort');
                CandidatePositionInterest = $injector.get('CandidatePositionInterest');

                $injector.get('CandidatePositionInterestFixtures');
                $injector.get('CohortFixtures');
            },
        ]);

        currentUser = SpecHelper.stubCurrentUser(); // need to stub currentUser for candidateListCard directives
        currentUser.relevant_cohort = Cohort.fixtures.getInstance();
        SpecHelper.stubDirective('candidateListActions');
    });

    function render(opts = {}) {
        if (opts.stubCard !== false) {
            SpecHelper.stubDirective('candidateListCard');
        }

        ngModel = opts.ngModel || null;
        if (!ngModel) {
            ngModel = [];
            for (let i = 0; i < (opts.numProfiles || 5); i++) {
                ngModel.push(CareerProfile.fixtures.getInstance());
            }
        }
        if (opts.withDeepLink) {
            _.last(ngModel)._deepLinked = true;
        }
        getProfilesSpy = jest.fn();
        afterItemRemoval = jest.fn();

        renderer = SpecHelper.renderer();
        renderer.scope.getProfiles = getProfilesSpy;
        renderer.scope.ngModel = ngModel;
        renderer.scope.listLimit = opts.listLimit || 4;
        renderer.scope.offset = angular.isDefined(opts.offset) ? opts.offset : 0;
        renderer.scope.noMoreAvailable = opts.noMoreAvailable || false;
        renderer.scope.unsupportedDeepLink = opts.unsupportedDeepLink || false;
        renderer.scope.afterItemRemoval = afterItemRemoval;
        renderer.scope.showFreemiumLimiting = opts.showFreemiumLimiting;
        renderer.render(
            '<candidate-list ng-model="ngModel" get-profiles="getProfiles(action, listLimit, offset)" list-limit="listLimit" ' +
                'unsupported-deep-link="unsupportedDeepLink" unsupported-deep-link-key="\'careers.hiring_browse_candidates.inactive_deep_link_error_message\'"' +
                'offset="offset" no-more-available="noMoreAvailable" after-item-removal="afterItemRemoval()"' +
                'show-freemium-limiting="showFreemiumLimiting"' +
                '></candidate-list>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should be hidden if ngModel.length is not greater than 0', () => {
        render({
            ngModel: [],
        });
        SpecHelper.expectNoElement(elem, 'candidate-list-card');
    });

    it('should limit visible profiles to listLimit', () => {
        const limit = 5;
        render({
            numProfiles: 8, // just needs to be greater than or equal to limit
            listLimit: limit,
        });
        expect(scope.ngModel.length).toBeGreaterThan(limit);
        SpecHelper.expectElements(elem, 'candidate-list-card', limit);
    });

    it('should begin visible profiles at offset', () => {
        const offset = 2;
        render({
            offset,
            stubCard: false,
        });
        SpecHelper.expectElements(elem, 'candidate-list-card', ngModel.length - offset);

        // the first 2 profiles should have been skipped over because of the offset,
        // so the first candidateListCard should be the profile at the offset index
        const firstCandidateListCardScope = SpecHelper.expectElement(elem, 'candidate-list-card:eq(0)').isolateScope();
        expect(firstCandidateListCardScope.careerProfile.id).toEqual(ngModel[offset].id);
    });

    it('should orderBy properly if ngModel is a collection of CandidatePositionInterest', () => {
        render({
            ngModel: [CandidatePositionInterest.fixtures.getInstance()],
        });
        expect(scope.orderBy).toEqual([
            'anonymizedPriority',
            'adminStatusPriority',
            'hiring_manager_priority',
            '-created_at',
        ]);
    });

    it('should show message explaining the profile is no longer in our system if unsupportedDeepLink', () => {
        render({
            unsupportedDeepLink: true,
        });
        SpecHelper.expectElementText(
            elem,
            '.unsupported-deep-link',
            'Sorry, this profile is no longer active in our system.',
        );
        SpecHelper.expectElementText(elem, '.more-candidates', 'MORE CANDIDATES');
    });

    it('should show a divider below the place where the deeply linked career profile would be if it was in our system', () => {
        render({
            unsupportedDeepLink: true,
        });
        SpecHelper.expectElementText(
            elem,
            '.candidate-list-card-placeholder + .deep-link-divider .more-candidates',
            'MORE CANDIDATES',
        );
    });

    it('should show the _deepLinked hiring relationship view-model first', () => {
        const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
        render({
            ngModel: careersNetworkViewModel.hiringRelationshipViewModels,
            withDeepLink: true,
        });
        expect(scope.orderBy).toEqual('_deepLinked');
        SpecHelper.expectElements(elem, '.card-list-item-container[ng-repeat="item in limitedModels"]');
    });

    it('should show a divider below the deeply linked career profile with a short message', () => {
        const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
        render({
            ngModel: careersNetworkViewModel.hiringRelationshipViewModels,
            withDeepLink: true,
        });
        SpecHelper.expectElementText(
            elem,
            '.card-list-item-container:first-of-type .deep-link-divider .more-candidates',
            'MORE CANDIDATES',
        );
    });

    describe('removeItem', () => {
        it('should remove item from ngModel', () => {
            const numProfiles = 3;
            render({
                numProfiles,
            });
            SpecHelper.expectElements(elem, 'candidate-list-card', numProfiles);
            scope.removeItem(ngModel[0]);
            scope.$apply();
            SpecHelper.expectElements(elem, 'candidate-list-card', numProfiles - 1);
        });

        it('should call afterItemRemoval if present', () => {
            const numProfiles = 3;
            render({
                numProfiles,
            });
            scope.removeItem(ngModel[0]);
            expect(afterItemRemoval).toHaveBeenCalled();
        });
    });

    describe('freemium limiting', () => {
        it('should blur candidates after a given index and show a provided element', () => {
            const numProfiles = 5;
            render({
                numProfiles,
                showFreemiumLimiting: true,
                listLimit: 10,
            });
            SpecHelper.expectDoesNotHaveClass(elem, '.card-list-item:eq(2)', 'blurred');
            SpecHelper.expectHasClass(elem, '.card-list-item:eq(3)', 'blurred');
            SpecHelper.expectHasClass(elem, '.card-list-item:eq(4)', 'blurred');

            SpecHelper.expectNoElement(elem, '.card-list-item:eq(2) .choose-a-plan-wrapper');
            SpecHelper.expectNoElement(elem, '.card-list-item:eq(4) .choose-a-plan-wrapper');

            SpecHelper.expectElementText(
                elem,
                '.card-list-item-container:eq(3) .choose-a-plan-wrapper > span',
                'More Smartly candidates are interested in connecting with you!',
            );
            SpecHelper.expectElementText(
                elem,
                '.card-list-item-container:eq(3) .choose-a-plan-wrapper > button',
                'Upgrade to Review',
            );

            jest.spyOn(scope.careersNetworkViewModel, 'showHiringBillingModal').mockImplementation(() => {});
            SpecHelper.click(elem, '.choose-a-plan-wrapper button');
            expect(scope.careersNetworkViewModel.showHiringBillingModal).toHaveBeenCalledWith(
                null,
                'candidate-list#choosePlan',
            );
        });
    });

    describe('showingProfiles', () => {
        it('should work', () => {
            render();
            expect(scope.showingProfiles).toBe(true);
            scope.offset = 999999;
            scope.$digest();
            expect(scope.showingProfiles).toBe(false);
        });
    });

    describe('pagination', () => {
        it('should be disabled when not showing any profiles', () => {
            render();
            SpecHelper.expectElementEnabled(elem, 'list-pagination [name="next-page"]');
            scope.offset = 999999;
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, 'list-pagination [name="next-page"]');
        });
    });
});
