import 'AngularSpecHelper';
import 'Careers/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import positionCardLocales from 'Careers/locales/careers/position_card-en.json';

setSpecLocales(positionCardLocales);

describe('FrontRoyal.Careers.PositionCard', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let position;
    let OpenPosition;
    let scope;
    let $sce;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                OpenPosition = $injector.get('OpenPosition');
                $sce = $injector.get('$sce');
            },
        ]);

        position = {
            id: 3,
            title: 'Marketing Leader Extraordinaire',
            role: 'trading',
            industry: 'legal',
            position_descriptors: ['permanent', 'part_time'],
            salary: 250000,
            perks: ['bonus', 'equity', 'remote', 'relocation'],
            job_post_url: 'http://example.com',
            desired_years_experience: {
                min: 2,
                max: 5,
            },
            skills: [
                {
                    text: 'AngularJS',
                },
                {
                    text: 'Adobe Photoshop',
                },
                {
                    text: 'Adobe Illustrator',
                },
                {
                    text: 'Microsoft Word',
                },
                {
                    text: 'Good Spelr',
                },
            ],
            short_description: 'This is a short description',
            description:
                'Product Managers architect the future of our products by bridging engineering and business as you manage a product’s full lifecycle, from strategic planning to development and launch.',
            newCandidates: false,
            numApplied: 0,
            numReviewed: 0,
            careerProfiles: [],
            archived: false,
            hiring_application: {
                website_url: 'http://amazon.com',
                company_logo_url:
                    'https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Amazon.com-Logo.svg/799px-Amazon.com-Logo.svg.png',
                company_name: 'Amazon',
            },
        };

        jest.spyOn($sce, 'trustAsHtml').mockImplementation(() => `trusted value`);
    });

    function render(options = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.position = OpenPosition.new(position);
        jest.spyOn(renderer.scope.position, 'locationString', 'get').mockReturnValue('Seattle, WA');
        renderer.render(
            `<position-card position="position" show-recommended-badge=${options.showRecommendedBadge}></position-card>`,
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should display position details', () => {
        render();

        // links
        SpecHelper.expectElementAttr(elem, '.company a', 'href', position.hiring_application.website_url);
        SpecHelper.expectElementAttr(elem, '.title a', 'href', position.job_post_url);

        // company and title
        SpecHelper.expectElementText(
            elem,
            '.company',
            `${position.hiring_application.company_name} Seattle, WA Website`,
        );
        SpecHelper.expectElementText(elem, '.title', `${position.title} Job Post`);

        // compensation and details
        SpecHelper.expectElementText(elem, '.compensation', 'Compensation $250,000 Bonus Equity');
        SpecHelper.expectElementText(
            elem,
            '.details',
            'Position Details Permanent or Part-Time 2–5 years experience Remote Relocation',
        );

        // short description
        SpecHelper.expectElementText(elem, '.short-description', position.short_description);

        // skills
        SpecHelper.expectElements(elem, '.pill', 5);

        // no badge though
        SpecHelper.expectNoElement(elem, '.badges > .recommended');
    });

    it('should render required areas only', () => {
        position.job_post_url = undefined;
        position.salary = undefined;
        position.desired_years_experience = undefined;
        position.perks = undefined;
        position.description = undefined;
        position.skills = undefined;
        render();

        // minimum required elements
        SpecHelper.expectElementText(
            elem,
            '.company',
            `${position.hiring_application.company_name} Seattle, WA Website`,
        );
        SpecHelper.expectElementText(elem, '.title', position.title);
        SpecHelper.expectElementText(elem, '.compensation', 'Compensation $ Open to Discussion');
        SpecHelper.expectElementText(elem, '.details', 'Position Details Permanent or Part-Time');

        // not required elements
        SpecHelper.expectNoElement(elem, '.description');
        SpecHelper.expectNoElement(elem, '.skills');
    });

    it('should show badge when showRecommendedBadge is true', () => {
        render({
            showRecommendedBadge: true,
        });
        SpecHelper.expectElement(elem, '.badges > .recommended');
    });

    describe('when full description is not present', () => {
        beforeEach(() => {
            position.description = undefined;
        });

        it('should hide full description visibility toggle and full description', () => {
            render();
            SpecHelper.expectNoElement(elem, '.show-hide-description');
            SpecHelper.expectNoElement(elem, '.description');
        });
    });

    describe('when full description is present', () => {
        it('should support toggling visibility of full description', () => {
            render();
            SpecHelper.expectNoElement(elem, '.description');
            SpecHelper.click(elem, '.show-hide-description');
            SpecHelper.expectElement(elem, '.description');
            SpecHelper.click(elem, '.show-hide-description');
            SpecHelper.expectNoElement(elem, '.description');
        });
    });

    describe('shortDescriptionReadable', () => {
        it('should be true for internal position with a short_description', () => {
            render();
            expect(scope.shortDescriptionReadable).toBe(true);
        });

        it('should be false for position without short_description', () => {
            position.short_description = undefined;
            render();
            expect(scope.shortDescriptionReadable).toBe(false);
        });

        it('should be true for external position if short_description does not start with ellipsis', () => {
            position.external = true;
            render();
            expect(scope.shortDescriptionReadable).toBe(true);
        });

        it('should be false for external position if short_description does start with ellipsis', () => {
            position.external = true;
            position.short_description = '... this is a short description';
            render();
            expect(scope.shortDescriptionReadable).toBe(false);
        });
    });

    describe('getReadableShortDescription', () => {
        beforeEach(() => {
            render();
        });

        describe('when shortDescriptionReadable', () => {
            beforeEach(() => {
                scope.shortDescriptionReadable = true;
            });

            it('should return short_description for internal positions', () => {
                scope.position.external = false;
                scope.position.short_description = 'foobar';
                expect(scope.getReadableShortDescription()).toEqual(scope.position.short_description);
            });

            it('should return trusted short_description for external positions with potentially readable short_description', () => {
                scope.position.external = true;
                scope.position.short_description = '<b>foobar</b>';
                expect(scope.getReadableShortDescription()).toEqual('trusted value');
                expect($sce.trustAsHtml).toHaveBeenCalledWith(scope.position.short_description);
            });
        });

        it('should return fallback_short_description when !shortDescriptionReadable', () => {
            scope.shortDescriptionReadable = false;
            expect(scope.getReadableShortDescription()).toEqual(
                'For more information about this position, click Learn More.',
            );
        });
    });
});
