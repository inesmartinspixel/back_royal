import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/careers_spec_helper';
import careersNetworkViewModelLocales from 'Careers/locales/careers/careers_network_view_model-en.json';
import hiringRelationshipViewModelLocales from 'Careers/locales/careers/hiring_relationship_view_model-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(careersNetworkViewModelLocales, hiringRelationshipViewModelLocales);

describe('FrontRoyal.Careers.HiringRelationshipViewModel', () => {
    let $injector;
    let CareersSpecHelper;
    let hiringRelationshipViewModel;
    let hiringRelationship;
    let $location;
    let DialogModal;
    let HiringRelationship;
    let $timeout;
    let $window;
    let Conversation;
    let $rootScope;
    let HiringRelationshipViewModel;
    let OpenPosition;
    let CandidatePositionInterest;
    let HttpQueue;
    let candidatePositionInterest;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper', 'NoUnhandledRejectionExceptions');

        angular.mock.inject([
            '$injector',
            _$injector => {
                $injector = _$injector;
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                $location = $injector.get('$location');
                DialogModal = $injector.get('DialogModal');
                HiringRelationship = $injector.get('HiringRelationship');
                HiringRelationshipViewModel = $injector.get('HiringRelationshipViewModel');
                $timeout = $injector.get('$timeout');
                $window = $injector.get('$window');
                Conversation = $injector.get('Mailbox.Conversation');
                $rootScope = $injector.get('$rootScope');
                OpenPosition = $injector.get('OpenPosition');
                CandidatePositionInterest = $injector.get('CandidatePositionInterest');
                HttpQueue = $injector.get('HttpQueue');
            },
        ]);
    });

    afterEach(() => {
        DialogModal.hideAlerts();
    });

    describe('initialize', () => {
        it('should default role to hiringManager', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                hiringRelationshipCount: 0,
            });
            const hiringRelationship = HiringRelationship.fixtures.getInstance({
                hiring_manager_id: careersNetworkViewModel.user.id,
            });
            const hiringRelationshipViewModel = new HiringRelationshipViewModel(
                hiringRelationship,
                careersNetworkViewModel,
            );
            expect(hiringRelationshipViewModel.role).toBe('hiringManager');
        });

        it('should default role to candidate', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate', {
                hiringRelationshipCount: 0,
            });
            const hiringRelationship = HiringRelationship.fixtures.getInstance({
                candidate_id: careersNetworkViewModel.user.id,
            });
            const hiringRelationshipViewModel = new HiringRelationshipViewModel(
                hiringRelationship,
                careersNetworkViewModel,
            );
            expect(hiringRelationshipViewModel.role).toBe('candidate');
        });

        it('should default role to hiringTeammate the hiring_manager is a teammate of the current user', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                hiringRelationshipCount: 0,
            });
            careersNetworkViewModel.user.hiring_team_id = 'the Browns';
            const hiringRelationship = HiringRelationship.fixtures.getInstance({
                hiring_manager_id: 'teammate',
                hiring_application: {
                    hiring_team_id: 'the Browns',
                },
            });
            const hiringRelationshipViewModel = new HiringRelationshipViewModel(
                hiringRelationship,
                careersNetworkViewModel,
            );
            expect(hiringRelationshipViewModel.role).toBe('hiringTeammate');
        });
    });

    describe('closedByMe', () => {
        it('should work', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];

            hiringRelationshipViewModel.hiringRelationship.hiring_manager_closed = null;
            hiringRelationshipViewModel.hiringRelationship.candidate_closed = null;
            expect(hiringRelationshipViewModel.closedByMe).toBe(false);

            hiringRelationshipViewModel.hiringRelationship.hiring_manager_closed = true;
            hiringRelationshipViewModel.hiringRelationship.candidate_closed = null;
            expect(hiringRelationshipViewModel.closedByMe).toBe(false);

            hiringRelationshipViewModel.hiringRelationship.hiring_manager_closed = null;
            hiringRelationshipViewModel.hiringRelationship.candidate_closed = true;
            expect(hiringRelationshipViewModel.closedByMe).toBe(true);

            hiringRelationshipViewModel.hiringRelationship.hiring_manager_closed = true;
            hiringRelationshipViewModel.hiringRelationship.candidate_closed = true;
            expect(hiringRelationshipViewModel.closedByMe).toBe(true);
        });
    });

    describe('close', () => {
        it('should do nothing if conversation is already closed', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            jest.spyOn(hiringRelationshipViewModel, 'closedByMe', 'get').mockReturnValue(true);
            jest.spyOn(hiringRelationshipViewModel, 'save').mockImplementation(() => {});
            hiringRelationshipViewModel.close();
            expect(hiringRelationshipViewModel.save).not.toHaveBeenCalled();
            $timeout.flush();
        });

        it('should trash messages if there is a conversation', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = _.detect(
                careersNetworkViewModel.hiringRelationshipViewModels,
                'conversation',
            );
            const conversation = hiringRelationshipViewModel.conversation;
            const conversationViewModel = hiringRelationshipViewModel.conversationViewModel;
            jest.spyOn(hiringRelationshipViewModel, 'save').mockImplementation(() => {});

            let message = conversation.messages[0];
            expect(conversationViewModel._myReceipt(message).trashed).toBe(false);

            hiringRelationshipViewModel.close();
            message = conversation.messages[0];
            expect(conversationViewModel._myReceipt(message).trashed).toBe(true);
        });

        it('should work if there is no conversation', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = _.reject(
                careersNetworkViewModel.hiringRelationshipViewModels,
                'conversation',
            )[0];
            jest.spyOn(hiringRelationshipViewModel, 'save').mockImplementation(() => {});

            expect(() => {
                hiringRelationshipViewModel.close();
            }).not.toThrow();
        });

        it('should update closed property but not save', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            jest.spyOn(careersNetworkViewModel, 'calculateNumNotifications').mockImplementation(() => {});
            jest.spyOn(hiringRelationshipViewModel, 'save').mockImplementation(() => {});

            hiringRelationshipViewModel.close();
            expect(hiringRelationshipViewModel.save).not.toHaveBeenCalled();
        });
    });

    describe('connectionAvatarSrc', () => {
        it('should work for hiring manager', () => {
            getFirstHiringRelationship('hiringManager');
            expect(hiringRelationship.career_profile.avatar_url).not.toBeUndefined();
            expect(hiringRelationshipViewModel.connectionAvatarSrc).toEqual(
                hiringRelationship.career_profile.avatar_url,
            );
        });

        it('should work for candidate', () => {
            getFirstHiringRelationship('candidate');
            expect(hiringRelationship.hiring_application.avatar_url).not.toBeUndefined();
            expect(hiringRelationshipViewModel.connectionAvatarSrc).toEqual(
                hiringRelationship.hiring_application.avatar_url,
            );
        });
    });

    describe('connectionName', () => {
        describe('as a hiring manager', () => {
            it("should be the candidate's full name if the hiring relationship status is 'matched' even if pref_show_photos_names is false", () => {
                const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
                const hiringRelationshipViewModelsByStatus = careersNetworkViewModel.testHelper.getRelationshipViewModelsByStatus();
                hiringRelationshipViewModel = hiringRelationshipViewModelsByStatus.matched;
                hiringRelationshipViewModel.currentUser.pref_show_photos_names = false;
                hiringRelationship = hiringRelationshipViewModel.hiringRelationship;
                expect(hiringRelationship.matched).toBe(true);
                expect(hiringRelationshipViewModel.connectionName).toEqual(
                    hiringRelationshipViewModel.connectionProfile.name,
                );
            });

            it("should be the candidate's initials when hiring relationship is not matched and pref_show_photos_names is false", () => {
                const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
                const hiringRelationshipViewModelsByStatus = careersNetworkViewModel.testHelper.getRelationshipViewModelsByStatus();

                // make sure that each view model's connectionName value returns the user's initials if the status is NOT 'matched'
                for (const status in hiringRelationshipViewModelsByStatus) {
                    if (status !== 'matched') {
                        hiringRelationshipViewModel = hiringRelationshipViewModelsByStatus[status];
                        hiringRelationshipViewModel.currentUser.pref_show_photos_names = false;
                        expect(hiringRelationshipViewModel.connectionName).toEqual(
                            hiringRelationshipViewModel.connectionProfile.userInitials,
                        );
                    }
                }
            });

            it("should be the candidate's full name when viewed as a hiring manager with pref_show_photos_names set to true", () => {
                getFirstHiringRelationship('hiringManager');
                expect(hiringRelationshipViewModel.currentUser.pref_show_photos_names).toBe(true);
                expect(hiringRelationship.candidate_display_name).not.toBeUndefined();
                expect(hiringRelationshipViewModel.connectionName).toEqual(hiringRelationship.candidate_display_name);
            });

            it("should be the candidate's initials when viewed as a hiring manager with pref_show_photos_names set to false", () => {
                getFirstHiringRelationship('hiringManager');
                hiringRelationshipViewModel.currentUser.pref_show_photos_names = false;
                expect(hiringRelationship.career_profile.name).not.toBeUndefined();
                expect(hiringRelationshipViewModel.connectionName).toEqual(
                    hiringRelationship.career_profile.userInitials,
                );
            });
        });

        describe('as a candidate', () => {
            it("should always show the hiring manager's full name", () => {
                getFirstHiringRelationship('candidate');
                expect(hiringRelationshipViewModel.currentUser.pref_show_photos_names).toBe(true);
                expect(hiringRelationship.hiring_manager_display_name).not.toBeUndefined();
                expect(hiringRelationshipViewModel.connectionName).toEqual(
                    hiringRelationship.hiring_manager_display_name,
                );
            });
        });
    });

    describe('connectionFormattedAddress', () => {
        it('should work for hiring manager', () => {
            getFirstHiringRelationship('hiringManager');
            expect(hiringRelationship.career_profile.locationString).not.toBeUndefined();
            expect(hiringRelationshipViewModel.connectionFormattedAddress).toEqual(
                hiringRelationship.career_profile.locationString,
            );
        });

        it('should work for candidate', () => {
            getFirstHiringRelationship('candidate');
            expect(hiringRelationship.hiring_application.locationString).not.toBeUndefined();
            expect(hiringRelationshipViewModel.connectionFormattedAddress).toEqual(
                hiringRelationship.hiring_application.locationString,
            );
        });
    });

    describe('msSinceLastActivity', () => {
        it('should be based on the updated_at on the hiringRelationship if there is no conversation', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const viewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithNoConversation();
            const now = Date.now();
            jest.spyOn(Date, 'now').mockReturnValue(now);
            expect(viewModel.hiringRelationship.updated_at).not.toBeUndefined();
            expect(viewModel.msSinceLastActivity).toBe(now - 1000 * viewModel.hiringRelationship.updated_at);
        });

        it('should be based on the last message in the conversation if there is one', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const viewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            expect(viewModel).not.toBeUndefined();
            const now = Date.now();
            jest.spyOn(Date, 'now').mockReturnValue(now);
            const lastMessageAt = viewModel.conversation.messages[0].updated_at;
            expect(lastMessageAt).not.toBeUndefined();
            expect(viewModel.msSinceLastActivity).toBe(now - 1000 * lastMessageAt);
        });
    });

    describe('openConnection', () => {
        it('should go to the conversation for a candidate', () => {
            jest.spyOn($location, 'path').mockReturnValue('');
            jest.spyOn($location, 'search').mockImplementation(() => {});
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const viewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            viewModel.openConnection();
            expect($location.path).toHaveBeenCalledWith('/careers/connections');
            expect($location.search).toHaveBeenCalledWith('connectionId', viewModel.connectionId);
        });

        it('should go to the conversation for a hiring manager', () => {
            jest.spyOn($location, 'path').mockReturnValue('');
            jest.spyOn($location, 'search').mockImplementation(() => {});
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const viewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            viewModel.openConnection();
            expect($location.path).toHaveBeenCalledWith('/hiring/tracker');
            expect($location.search).toHaveBeenCalledWith('connectionId', viewModel.connectionId);
        });
    });

    describe('decline', () => {
        it('should throw if role is not candidate or hiring_manager', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            hiringRelationshipViewModel.role = 'foo';
            expect(hiringRelationshipViewModel.decline.bind(hiringRelationshipViewModel)).toThrow(
                new Error('Only for participants in relationship.'),
            );
        });

        it('should allow for closing when hiring manager', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();

            jest.spyOn(hiringRelationshipViewModel, 'decline');
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            jest.spyOn($window, 'confirm').mockReturnValue(true);
            jest.spyOn(hiringRelationshipViewModel, 'save').mockImplementation(() => {});

            Conversation.expect('update').returns({
                result: {}, // we don't care about the result, so just return an empty response
            });

            expect(hiringRelationshipViewModel.closed).toBe(false);
            hiringRelationshipViewModel.decline();
            expect(DialogModal.alert).toHaveBeenCalledWith({
                content:
                    '<hiring-close-connection hiring-relationship-view-model="hiringRelationshipViewModel"></hiring-close-connection>',
                scope: {
                    hiringRelationshipViewModel,
                },
                size: 'large',
                hideCloseButton: false,
                classes: ['blue', 'hiring-welcome-modal', 'no-title'],
                blurTargetSelector: 'div[ng-controller]',
            });
        });

        it('should reject if this is a candidate and the status is pending', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationship = careersNetworkViewModel.testHelper.getRelationshipsByStatus()
                .waiting_on_candidate;
            const hiringRelationshipViewModel =
                careersNetworkViewModel._hiringRelationshipViewModelsByConnectionId[
                    hiringRelationship.hiring_manager_id
                ][0];
            expect(hiringRelationshipViewModel).not.toBeUndefined();

            jest.spyOn(hiringRelationshipViewModel, 'decline');
            jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {
                hiringRelationshipViewModel._rejectOrCloseViewModel();
            });
            jest.spyOn(hiringRelationshipViewModel, 'save').mockImplementation(() => {});
            jest.spyOn(hiringRelationshipViewModel, 'navigateToTracker').mockImplementation(() => {});

            Conversation.expect('update').returns({
                result: {}, // we don't care about the result, so just return an empty response
            });

            expect(hiringRelationshipViewModel.closed).toBe(false);

            hiringRelationshipViewModel.decline();
            expect(DialogModal.confirm).toHaveBeenCalledWith({
                text: `Are you sure? If you continue, you will no longer be able to exchange messages with ${hiringRelationshipViewModel.connectionNickname}.`,
                confirmCallback: expect.any(Function),
            });
            expect(hiringRelationshipViewModel.closed).toBe(false);
            expect(hiringRelationshipViewModel.ourStatus).toBe('rejected');
            expect(hiringRelationshipViewModel.save).toHaveBeenCalled();
            expect(hiringRelationshipViewModel.navigateToTracker).toHaveBeenCalled();
        });

        it('should close and not reject if this is a candidate and the status is not pending', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationship = careersNetworkViewModel.testHelper.getRelationshipsByStatus()
                .waiting_on_candidate;
            const hiringRelationshipViewModel =
                careersNetworkViewModel._hiringRelationshipViewModelsByConnectionId[
                    hiringRelationship.hiring_manager_id
                ][0];
            hiringRelationship.candidate_status = 'accepted';

            expect(hiringRelationshipViewModel).not.toBeUndefined();

            jest.spyOn(hiringRelationshipViewModel, 'decline');
            jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {
                hiringRelationshipViewModel._rejectOrCloseViewModel();
            });
            jest.spyOn(hiringRelationshipViewModel, 'save').mockImplementation(() => {});
            jest.spyOn(hiringRelationshipViewModel, 'navigateToTracker').mockImplementation(() => {});

            Conversation.expect('update').returns({
                result: {}, // we don't care about the result, so just return an empty response
            });

            expect(hiringRelationshipViewModel.closed).toBe(false);

            hiringRelationshipViewModel.decline();
            expect(DialogModal.confirm).toHaveBeenCalledWith({
                text: `Are you sure? If you continue, you will no longer be able to exchange messages with ${hiringRelationshipViewModel.connectionNickname}.`,
                confirmCallback: expect.any(Function),
            });
            expect(hiringRelationshipViewModel.closed).toBe(true);
            expect(hiringRelationshipViewModel.ourStatus).not.toBe('rejected');
            expect(hiringRelationshipViewModel.save).toHaveBeenCalled();
            expect(hiringRelationshipViewModel.navigateToTracker).toHaveBeenCalled();
        });
    });

    describe('navigateToTracker', () => {
        it('should navigate back to tracker', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();

            jest.spyOn($location, 'search').mockImplementation(() => {});
            jest.spyOn(hiringRelationshipViewModel, 'navigateToTracker');

            hiringRelationshipViewModel.navigateToTracker();
            expect($location.search).toHaveBeenCalledWith('connectionId', null);
        });
    });

    describe('save', () => {
        it('should broadcast an event', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            hiringRelationshipViewModel = _.detect(
                careersNetworkViewModel.hiringRelationshipViewModels,
                'conversation',
            );
            jest.spyOn($rootScope, '$broadcast').mockImplementation(() => {});
            HiringRelationship.expect('update');
            hiringRelationshipViewModel.save();
            HiringRelationship.flush('update');
            expect($rootScope.$broadcast).toHaveBeenCalledWith('hiringRelationshipUpdate');
        });

        it('should save open position', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            hiringRelationship = hiringRelationshipViewModel.hiringRelationship;
            jest.spyOn(hiringRelationship, '_beforeSave').mockImplementation(() => {});

            const openPosition = OpenPosition.new({
                id: 'id',
                title: 'ok',
            });

            HiringRelationship.expect('save')
                .toBeCalledWith(
                    _.extend(hiringRelationship.asJson()),
                    {
                        open_position: openPosition.asJson(),
                    },
                    {
                        'FrontRoyal.ApiErrorHandler': {
                            skip: true,
                        },
                    },
                )
                .returnsMeta({
                    open_position: {
                        updated_at: 12345,
                    },
                });
            hiringRelationshipViewModel.save({
                openPosition,
            });
            HiringRelationship.flush('save');
            expect(openPosition.updated_at).toEqual(12345);
        });

        describe('candidate position interest', () => {
            beforeEach(() => {
                const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
                hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
                hiringRelationship = hiringRelationshipViewModel.hiringRelationship;
                jest.spyOn(hiringRelationship, '_beforeSave').mockImplementation(() => {});

                const openPosition = OpenPosition.new({
                    id: 'id',
                    title: 'ok',
                });

                candidatePositionInterest = CandidatePositionInterest.new({
                    hiring_manager_id: careersNetworkViewModel.user.id,
                    open_position_id: openPosition.id,
                    candidate_status: 'accepted',
                    hiring_manager_status: 'reviewed',
                });
            });

            it('should save candidate position interest', () => {
                HiringRelationship.expect('save')
                    .toBeCalledWith(
                        _.extend(hiringRelationship.asJson()),
                        {
                            candidate_position_interest: candidatePositionInterest.asJson(),
                        },
                        {
                            'FrontRoyal.ApiErrorHandler': {
                                skip: true,
                            },
                        },
                    )
                    .returnsMeta({
                        candidate_position_interest: {
                            updated_at: 12345,
                        },
                    });
                hiringRelationshipViewModel.save({
                    candidatePositionInterest,
                });
                HiringRelationship.flush('save');
                expect(candidatePositionInterest.updated_at).toEqual(12345);
            });

            it('should add open_position_id to relationship if saving for later with candidate position interest', () => {
                expect(hiringRelationshipViewModel.hiringRelationship.open_position_id).toBe(undefined);

                Object.defineProperty(hiringRelationship, 'hiring_manager_status', {
                    value: 'saved_for_later',
                });

                HiringRelationship.expect('save');
                hiringRelationshipViewModel.save({
                    candidatePositionInterest,
                });
                HiringRelationship.flush('save');
                expect(hiringRelationshipViewModel.hiringRelationship.open_position_id).toEqual(
                    candidatePositionInterest.open_position_id,
                );
            });
        });

        it('should handle pushed down changes to other hiring relationships', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            hiringRelationship = hiringRelationshipViewModel.hiringRelationship;
            const anotherHiringRelationship = careersNetworkViewModel.hiringRelationships[1];
            jest.spyOn(hiringRelationship, '_beforeSave').mockImplementation(() => {});

            HiringRelationship.expect('save').returnsMeta({
                hiring_relationships: [
                    {
                        id: anotherHiringRelationship.id,
                        changed: true,
                    },
                ],
            });
            hiringRelationshipViewModel.save();
            HiringRelationship.flush('save');

            const reloadedHiringRelationship = _.findWhere(careersNetworkViewModel.hiringRelationships, {
                id: anotherHiringRelationship.id,
            });
            expect(reloadedHiringRelationship.changed).toEqual(true);
        });

        it('should handle pushed down changes to other hiring relationships on an error', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            hiringRelationship = hiringRelationshipViewModel.hiringRelationship;
            const anotherHiringRelationship = careersNetworkViewModel.hiringRelationships[1];
            jest.spyOn(hiringRelationship, '_beforeSave').mockImplementation(() => {});

            HiringRelationship.expect('save').fails({
                data: {
                    meta: {
                        hiring_relationships: [
                            {
                                id: anotherHiringRelationship.id,
                                changed: true,
                            },
                        ],
                    },
                },
            });
            hiringRelationshipViewModel.save();
            HiringRelationship.flush('save');

            const reloadedHiringRelationship = _.findWhere(careersNetworkViewModel.hiringRelationships, {
                id: anotherHiringRelationship.id,
            });
            expect(reloadedHiringRelationship.changed).toEqual(true);
        });

        it('should handle pushed down changes to candidate position interests', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            hiringRelationship = hiringRelationshipViewModel.hiringRelationship;

            jest.spyOn(careersNetworkViewModel, 'setFeaturedPositionsNumNotifications').mockImplementation(() => {});
            jest.spyOn(hiringRelationship, '_beforeSave').mockImplementation(() => {});

            HiringRelationship.expect('save').returnsMeta({
                candidate_position_interests: [{}, {}],
            });
            hiringRelationshipViewModel.save();
            HiringRelationship.flush('save');

            expect(careersNetworkViewModel.setFeaturedPositionsNumNotifications).toHaveBeenCalled();
        });

        it('should bumpLastRelationshipUpdatedAt', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            hiringRelationship = hiringRelationshipViewModel.hiringRelationship;
            hiringRelationship.hiring_manager_status = 'accepted';

            jest.spyOn(careersNetworkViewModel, 'bumpLastRelationshipUpdatedAt').mockImplementation(() => {});
            HiringRelationship.expect('update');
            hiringRelationshipViewModel.save();
            HiringRelationship.flush('update');

            expect(careersNetworkViewModel.bumpLastRelationshipUpdatedAt).toHaveBeenCalledWith(hiringRelationship);
        });

        describe('error handling', () => {
            it('should handle interest conflict', () => {
                const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
                const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];

                jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
                jest.spyOn(careersNetworkViewModel, 'onCandidatePositionInterestConflict').mockImplementation(() => {});
                jest.spyOn(HttpQueue.instance, 'unfreezeAfterError').mockImplementation(() => {});
                HiringRelationship.expect('save').fails({
                    status: 409,
                    data: {
                        meta: {
                            error_type: 'interest_conflict',
                            candidate_position_interest: CandidatePositionInterest.new(),
                        },
                    },
                });

                hiringRelationshipViewModel.save();
                HiringRelationship.flush('save');
                expect(DialogModal.alert).toHaveBeenCalledWith({
                    content: 'This applicant has already been been reviewed by another teammate.',
                });
                expect(HttpQueue.instance.unfreezeAfterError).toHaveBeenCalled();
                expect(careersNetworkViewModel.onCandidatePositionInterestConflict).toHaveBeenCalled();
            });

            describe('hiringRelationship', () => {
                let onModalClosed;
                let success;
                let fail;
                let careersNetworkViewModel;

                beforeEach(() => {
                    careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
                    hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
                    hiringRelationship = hiringRelationshipViewModel.hiringRelationship;
                    jest.spyOn(DialogModal, 'alert').mockImplementation(opts => {
                        onModalClosed = opts.close;
                    });
                    success = jest.fn();
                    fail = jest.fn();
                });

                // see also: NoUnhandledRejectionExceptions module use above
                it('should special-handle multiple_team_connections_to_candidate error', () => {
                    hiringRelationshipViewModel.hiringRelationship.hiring_manager_status = 'accepted';
                    saveRequestFails(hiringRelationship);

                    expect(DialogModal.alert).toHaveBeenCalled();
                    const opts = DialogModal.alert.mock.calls[0][0];
                    expect(_.pick(opts, ['content', 'classes', 'title'])).toEqual({
                        content: `That asshole in accounting has already connected with ${hiringRelationshipViewModel.connectionName}. You can find this connection in the Team section of the Tracker.`,
                        classes: ['server-error-modal', 'small'],
                        title: 'Candidate Already Connected',
                    });
                    expect(HttpQueue.instance.unfreezeAfterError).toHaveBeenCalled();
                    expect($rootScope.$broadcast).toHaveBeenCalledWith('hiringRelationshipUpdate');

                    // The promise should not reject until the modal is dismissed
                    expect(success).not.toHaveBeenCalled();
                    expect(fail).not.toHaveBeenCalled();
                    onModalClosed();
                    $timeout.flush();
                    expect(fail).toHaveBeenCalled();
                });

                it('should open the connection page when getting an error while creating a pending relationship', () => {
                    hiringRelationship.hiring_manager_status = 'pending';
                    jest.spyOn(hiringRelationshipViewModel, 'openConnection').mockImplementation(() => {});
                    saveRequestFailsWithoutModal();
                    expect(hiringRelationshipViewModel.openConnection).toHaveBeenCalled();
                });

                it('should not show a modal when getting an error on rejecting a candidate', () => {
                    hiringRelationship.hiring_manager_status = 'rejected';
                    saveRequestFailsWithoutModal();
                });

                it('should not special handle other errors', () => {
                    const ApiErrorHandler = $injector.get('ApiErrorHandler');
                    jest.spyOn(HttpQueue.instance, 'unfreezeAfterError').mockImplementation(() => {});
                    jest.spyOn(ApiErrorHandler, 'onResponseError').mockImplementation(() => {});
                    const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
                    hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
                    const config = {
                        'FrontRoyal.ApiErrorHandler': {
                            skip: true,
                        },
                        url: '/api/hiring_relationships',
                    };

                    // see also: NoUnhandledRejectionExceptions module use above
                    HiringRelationship.expect('save').fails({
                        status: 500,
                        config,
                    });

                    hiringRelationshipViewModel.save();
                    HiringRelationship.flush('save');
                    expect(DialogModal.alert).not.toHaveBeenCalled();
                    expect(HttpQueue.instance.unfreezeAfterError).not.toHaveBeenCalled();
                    expect(ApiErrorHandler.onResponseError).toHaveBeenCalled();
                    expect(
                        ApiErrorHandler.onResponseError.mock.calls[0][0].config['FrontRoyal.ApiErrorHandler'].skip,
                    ).toBe(false);
                });

                function saveRequestFails() {
                    jest.spyOn(HttpQueue.instance, 'unfreezeAfterError').mockImplementation(() => {});
                    jest.spyOn($rootScope, '$broadcast');

                    const config = {
                        'FrontRoyal.ApiErrorHandler': {
                            skip: true,
                        },
                        url: '/api/hiring_relationships',
                    };
                    HiringRelationship.expect('save').fails({
                        status: 406,
                        data: {
                            meta: {
                                error_type: 'multiple_team_connections_to_candidate',
                                teammate_name: 'That asshole in accounting',
                                hiring_relationship: _.extend(hiringRelationship.asJson(), {
                                    hiring_manager_status: 'hidden',
                                }),
                            },
                        },
                        config,
                    });

                    hiringRelationshipViewModel.save().then(success, fail);
                    HiringRelationship.flush('save');
                    expect(hiringRelationship.hiring_manager_status).toEqual('hidden');
                }

                function saveRequestFailsWithoutModal() {
                    saveRequestFails(hiringRelationship);

                    expect(DialogModal.alert).not.toHaveBeenCalled();
                    expect(HttpQueue.instance.unfreezeAfterError).toHaveBeenCalled();
                    expect($rootScope.$broadcast).toHaveBeenCalledWith('hiringRelationshipUpdate');

                    // The promise should reject immediately, since there is no modal
                    expect(success).not.toHaveBeenCalled();
                    expect(fail).toHaveBeenCalled();
                }
            });
        });

        describe('you liked your first candidate modal handling', () => {
            let careersNetworkViewModel;

            beforeEach(() => {
                careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
                hiringRelationshipViewModel = _.detect(
                    careersNetworkViewModel.hiringRelationshipViewModels,
                    vm => vm.hiringRelationship.hiring_manager_status === 'pending',
                );
                HiringRelationship.expect('update');
                jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            });

            it('should work when hasNotYetLikedFirstCandidate is false and status="rejected"', () => {
                careersNetworkViewModel.hasNotYetLikedFirstCandidate = true;
                hiringRelationshipViewModel.updateStatus('rejected');
                hiringRelationshipViewModel.save();
                HiringRelationship.flush('update');
                expect(DialogModal.alert).not.toHaveBeenCalled();
                expect(careersNetworkViewModel.hasNotYetLikedFirstCandidate).toBe(true);
            });
            it('should work when hasNotYetLikedFirstCandidate is false and status="accepted"', () => {
                careersNetworkViewModel.hasNotYetLikedFirstCandidate = true;
                hiringRelationshipViewModel.updateStatus('accepted');
                hiringRelationshipViewModel.save();
                HiringRelationship.flush('update');
                $injector.get('$timeout').flush();
                expect(DialogModal.alert).toHaveBeenCalled();
                expect(hiringRelationshipViewModel.hiringRelationship.hiring_manager_status).toEqual('accepted');
                expect(careersNetworkViewModel.hasNotYetLikedFirstCandidate).toBe(false);
            });
            it('should work when hasNotYetLikedFirstCandidate is false and status="rejected"', () => {
                careersNetworkViewModel.hasNotYetLikedFirstCandidate = false;
                hiringRelationshipViewModel.updateStatus('rejected');
                hiringRelationshipViewModel.save();
                HiringRelationship.flush('update');
                expect(DialogModal.alert).not.toHaveBeenCalled();
                expect(hiringRelationshipViewModel.hiringRelationship.hiring_manager_status).toEqual('rejected');
                expect(careersNetworkViewModel.hasNotYetLikedFirstCandidate).toBe(false);
            });

            it('should work when hasNotYetLikedFirstCandidate is false and status="accepted"', () => {
                careersNetworkViewModel.hasNotYetLikedFirstCandidate = false;
                hiringRelationshipViewModel.updateStatus('accepted');
                hiringRelationshipViewModel.save();
                HiringRelationship.flush('update');
                expect(DialogModal.alert).not.toHaveBeenCalled();
                expect(hiringRelationshipViewModel.hiringRelationship.hiring_manager_status).toEqual('accepted');
                expect(careersNetworkViewModel.hasNotYetLikedFirstCandidate).toBe(false);
            });
        });
    });

    describe('updateStatus', () => {
        let careersNetworkViewModel;
        let hiringRelationshipViewModel;
        let hiringRelationship;
        it('should work for candidate', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            hiringRelationship = hiringRelationshipViewModel.hiringRelationship;
            hiringRelationship.candidate_status = 'pending';
            jest.spyOn($rootScope, '$broadcast').mockImplementation(() => {});
            hiringRelationshipViewModel.updateStatus('newStatus');
            expect($rootScope.$broadcast).toHaveBeenCalledWith(
                'hiringRelationship:updateStatus',
                hiringRelationshipViewModel,
            );
            expect(hiringRelationshipViewModel.hiringRelationship.candidate_status).toEqual('newStatus');
        });

        it('should work for candidate', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            hiringRelationship = hiringRelationshipViewModel.hiringRelationship;
            hiringRelationship.hiring_manager_status = 'pending';
            jest.spyOn($rootScope, '$broadcast').mockImplementation(() => {});
            hiringRelationshipViewModel.updateStatus('rejected');
            expect(hiringRelationshipViewModel.hiringRelationship.hiring_manager_status).toEqual('rejected');
            expect($rootScope.$broadcast).toHaveBeenCalledWith(
                'hiringRelationship:updateStatus',
                hiringRelationshipViewModel,
            );
        });
    });

    describe('addMessage', () => {
        let careersNetworkViewModel;
        let ConversationViewModel;

        beforeEach(() => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            ConversationViewModel = $injector.get('ConversationViewModel');
        });

        it('should accept a pending relationship', () => {
            hiringRelationshipViewModel = _.findWhere(careersNetworkViewModel.hiringRelationshipViewModels, {
                ourStatus: 'pending',
            });
            hiringRelationshipViewModel.addMessage('body');
            expect(hiringRelationshipViewModel.hiringRelationship.hiring_manager_status).toEqual('accepted');
        });

        it('should work when there already is a conversation', () => {
            hiringRelationshipViewModel = _.detect(
                careersNetworkViewModel.hiringRelationshipViewModels,
                'conversationViewModel',
            );
            jest.spyOn(hiringRelationshipViewModel.conversationViewModel, 'addMessage').mockImplementation(() => {});
            hiringRelationshipViewModel.addMessage('body', 'metadata');
            expect(hiringRelationshipViewModel.conversationViewModel.addMessage).toHaveBeenCalledWith(
                'body',
                'metadata',
            );
        });

        it('should work when there is not already a conversation', () => {
            hiringRelationshipViewModel = _.reject(
                careersNetworkViewModel.hiringRelationshipViewModels,
                'conversationViewModel',
            )[0];
            jest.spyOn(hiringRelationshipViewModel, 'ourStatus', 'get').mockReturnValue('accepted');
            jest.spyOn(ConversationViewModel.prototype, 'addMessage').mockImplementation(function () {
                expect(hiringRelationshipViewModel.conversationViewModel).toBe(this);
            });
            hiringRelationshipViewModel.addMessage('body', 'metadata');
            expect(ConversationViewModel.prototype.addMessage).toHaveBeenCalledWith('body', 'metadata');
            expect(hiringRelationshipViewModel.conversationViewModel).not.toBeUndefined();
            expect(hiringRelationshipViewModel.conversationViewModel.conversation.recipients[0].id).toEqual(
                hiringRelationshipViewModel.currentUser.id,
            );
            expect(hiringRelationshipViewModel.conversationViewModel.conversation.recipients[1].id).toEqual(
                hiringRelationshipViewModel.connectionId,
            );
        });
    });

    describe('sendMessage', () => {
        it('should add the message and save', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            hiringRelationshipViewModel = _.detect(
                careersNetworkViewModel.hiringRelationshipViewModels,
                'conversationViewModel',
            );
            jest.spyOn(hiringRelationshipViewModel, 'save').mockImplementation(() => {});
            jest.spyOn(hiringRelationshipViewModel, 'addMessage').mockImplementation(() => {});
            hiringRelationshipViewModel.sendMessage('body');
            expect(hiringRelationshipViewModel.addMessage).toHaveBeenCalledWith('body');
            expect(hiringRelationshipViewModel.save).toHaveBeenCalled();
        });
    });

    describe('markAllAsRead', () => {
        it('should mark all as read', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            hiringRelationshipViewModel = _.detect(
                careersNetworkViewModel.hiringRelationshipViewModels,
                'conversationViewModel',
            );
            const conversationViewModel = hiringRelationshipViewModel.conversationViewModel;
            const message = conversationViewModel.messagesFromOthers[0];
            hiringRelationshipViewModel.conversationViewModel._myReceipt(message).is_read = false;
            expect(hiringRelationshipViewModel.conversationViewModel.hasUnread).toBe(true);
            jest.spyOn(hiringRelationshipViewModel, 'save').mockImplementation(() => {});
            hiringRelationshipViewModel.markAllAsRead();
            expect(hiringRelationshipViewModel.conversationViewModel.hasUnread).not.toBe(true);
            expect(hiringRelationshipViewModel.save).toHaveBeenCalled();
        });

        it('should mark not save if in ghostMode', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            hiringRelationshipViewModel = _.detect(
                careersNetworkViewModel.hiringRelationshipViewModels,
                'conversationViewModel',
            );

            // set ghostMode
            hiringRelationshipViewModel.currentUser.ghostMode = true;

            const conversationViewModel = hiringRelationshipViewModel.conversationViewModel;
            const message = conversationViewModel.messagesFromOthers[0];
            hiringRelationshipViewModel.conversationViewModel._myReceipt(message).is_read = false;
            expect(hiringRelationshipViewModel.conversationViewModel.hasUnread).toBe(true);
            jest.spyOn(hiringRelationshipViewModel, 'save').mockImplementation(() => {});
            hiringRelationshipViewModel.markAllAsRead();

            // nothing should change, should not save
            expect(hiringRelationshipViewModel.conversationViewModel.hasUnread).toBe(true);
            expect(hiringRelationshipViewModel.save).not.toHaveBeenCalled();
        });

        it('should not save if nothing changes', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                conversationOverrides: [
                    {
                        messages: [
                            {
                                from: 'them',
                                isRead: true,
                            },
                        ],
                    },
                ],
            });
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();
            jest.spyOn(hiringRelationshipViewModel, 'save').mockImplementation(() => {});
            jest.spyOn(careersNetworkViewModel, 'calculateNumNotifications').mockImplementation(() => {});
            hiringRelationshipViewModel.markAllAsRead();
            expect(hiringRelationshipViewModel.conversationViewModel.hasUnread).not.toBe(true);
            expect(hiringRelationshipViewModel.save).not.toHaveBeenCalled();
            expect(careersNetworkViewModel.calculateNumNotifications).not.toHaveBeenCalled();
        });
    });

    describe('ourParticipantId', () => {
        it('should work for hiring manager', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            const hiringRelationship = hiringRelationshipViewModel.hiringRelationship;
            expect(hiringRelationshipViewModel.ourParticipantId).toEqual(hiringRelationship.hiring_manager_id);
        });

        it('should work for candidate', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            const hiringRelationship = hiringRelationshipViewModel.hiringRelationship;
            expect(hiringRelationshipViewModel.ourParticipantId).toEqual(hiringRelationship.candidate_id);
        });

        it('should work for hiring teammate', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                teammateId: 'teammate',
            });
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            expect(hiringRelationshipViewModel.ourParticipantId).toEqual('teammate');
        });
    });

    describe('theirParticipantId', () => {
        it('should work for hiring manager', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            const hiringRelationship = hiringRelationshipViewModel.hiringRelationship;
            expect(hiringRelationshipViewModel.theirParticipantId).toEqual(hiringRelationship.candidate_id);
        });

        it('should work for candidate', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            const hiringRelationship = hiringRelationshipViewModel.hiringRelationship;
            expect(hiringRelationshipViewModel.theirParticipantId).toEqual(hiringRelationship.hiring_manager_id);
        });

        it('should work for hiring teammate', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                teammateId: 'teammate',
            });
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            const hiringRelationship = hiringRelationshipViewModel.hiringRelationship;
            expect(hiringRelationshipViewModel.theirParticipantId).toEqual(hiringRelationship.candidate_id);
        });
    });

    describe('messageIsSentByUs', () => {
        it('should work for hiring manager', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];

            expect(
                hiringRelationshipViewModel.messageIsSentByUs({
                    sender_id: careersNetworkViewModel.user.id,
                }),
            ).toBe(true);

            expect(
                hiringRelationshipViewModel.messageIsSentByUs({
                    sender_id: 'assume_this_is_a_teammate',
                }),
            ).toBe(true);

            expect(
                hiringRelationshipViewModel.messageIsSentByUs({
                    sender_id: hiringRelationshipViewModel.hiringRelationship.candidate_id,
                }),
            ).toBe(false);
        });

        it('should work for candidate', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];

            expect(
                hiringRelationshipViewModel.messageIsSentByUs({
                    sender_id: careersNetworkViewModel.user.id,
                }),
            ).toBe(true);

            expect(
                hiringRelationshipViewModel.messageIsSentByUs({
                    sender_id: 'assume_this_is_a_teammate',
                }),
            ).toBe(false);

            expect(
                hiringRelationshipViewModel.messageIsSentByUs({
                    sender_id: hiringRelationshipViewModel.hiringRelationship.hiring_manager_id,
                }),
            ).toBe(false);
        });

        it('should work for hiring teammate', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                teammateId: 'teammate',
            });
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];

            expect(
                hiringRelationshipViewModel.messageIsSentByUs({
                    sender_id: careersNetworkViewModel.user.id,
                }),
            ).toBe(true);

            expect(
                hiringRelationshipViewModel.messageIsSentByUs({
                    sender_id: 'assume_this_is_another_teammate',
                }),
            ).toBe(true);

            expect(
                hiringRelationshipViewModel.messageIsSentByUs({
                    sender_id: hiringRelationshipViewModel.hiringRelationship.hiring_manager_id,
                }),
            ).toBe(true);

            expect(
                hiringRelationshipViewModel.messageIsSentByUs({
                    sender_id: hiringRelationshipViewModel.hiringRelationship.candidate_id,
                }),
            ).toBe(false);
        });
    });

    describe('messageIsUnreadByAllOfThem', () => {
        it('should be true if message is unread', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            jest.spyOn(hiringRelationshipViewModel, 'messageIsSentByUs').mockReturnValue(true);

            const message = $injector.get('Mailbox.Message').new({
                receipts: [
                    {
                        receiver_id: careersNetworkViewModel.user.id,
                        is_read: true,
                    },
                    {
                        receiver_id: hiringRelationshipViewModel.theirParticipantId,
                        is_read: false,
                    },
                    {
                        receiver_id: 'teammate',
                        is_read: false,
                    },
                ],
            });
            expect(hiringRelationshipViewModel.messageIsUnreadByAllOfThem(message)).toBe(true);
        });

        it('should be false if message is read', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            jest.spyOn(hiringRelationshipViewModel, 'messageIsSentByUs').mockReturnValue(true);

            const message = $injector.get('Mailbox.Message').new({
                receipts: [
                    {
                        receiver_id: careersNetworkViewModel.user.id,
                        is_read: true,
                    },
                    {
                        receiver_id: hiringRelationshipViewModel.theirParticipantId,
                        is_read: true,
                    },
                    {
                        receiver_id: 'teammate',
                        is_read: false,
                    },
                ],
            });
            expect(hiringRelationshipViewModel.messageIsUnreadByAllOfThem(message)).toBe(false);
        });

        it('should be false if we did not send the message', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            jest.spyOn(hiringRelationshipViewModel, 'messageIsSentByUs').mockReturnValue(false);
            expect(hiringRelationshipViewModel.messageIsUnreadByAllOfThem()).toBe(false);
        });
    });

    describe('newMatch', () => {
        let careersNetworkViewModel;
        let hiringRelationshipViewModel;

        beforeEach(() => {
            $injector.get('MessageFixtures');
            careersNetworkViewModel = null;
            hiringRelationshipViewModel = null;
        });

        it('should be false for candidates', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            _.each(careersNetworkViewModel.hiringRelationshipViewModels, hiringRelationshipViewModel => {
                expect(hiringRelationshipViewModel.newMatch).toBe(false);
            });
        });

        it('should be false if not matched', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            hiringRelationshipViewModel = _.detect(
                careersNetworkViewModel.hiringRelationshipViewModels,
                vm => !vm.matched,
            );
            expect(hiringRelationshipViewModel.newMatch).toBe(false);
        });

        it('should be true if no conversation', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            hiringRelationshipViewModel = _.detect(
                careersNetworkViewModel.hiringRelationshipViewModels,
                vm => vm.matched,
            );
            jest.spyOn(hiringRelationshipViewModel, 'conversationViewModel', 'get').mockReturnValue(null);
            expect(hiringRelationshipViewModel.newMatch).toBe(true);
        });

        it('should be true if hiring manager has sent one message and not matchedByHiringManager', () => {
            const opts = {
                conversationOverrides: [
                    {
                        messages: [
                            {
                                from: 'me',
                            },
                        ],
                    },
                ],
            };
            assertNewMatchForHiringManagerAndTeammate(true, true, opts);
        });

        it('should be false if matchedByHiringManager', () => {
            const opts = {
                conversationOverrides: [
                    {
                        messages: [
                            {
                                from: 'me',
                            },
                        ],
                    },
                ],
            };
            assertNewMatchForHiringManagerAndTeammate(false, false, opts, true);
        });

        it('should be false if hiring manager has sent more than one message', () => {
            const opts = {
                conversationOverrides: [
                    {
                        messages: [
                            {
                                from: 'me',
                            },
                            {
                                from: 'me',
                            },
                        ],
                    },
                ],
            };
            assertNewMatchForHiringManagerAndTeammate(false, false, opts);
        });

        it('should be false if hiring manager has read a message from the other', () => {
            const opts = {
                conversationOverrides: [
                    {
                        messages: [
                            {
                                from: 'them',
                                isRead: true,
                            },
                            {
                                from: 'them',
                                isRead: false,
                            },
                        ],
                    },
                ],
            };
            assertNewMatchForHiringManagerAndTeammate(false, false, opts);
        });

        // See comment in allUnreadFrom for the edge case which makes this necessary
        it('should be false if there are no receipts for the hiring manager', () => {
            const opts = {
                conversationOverrides: [
                    {
                        messages: [
                            {
                                from: 'them',
                                hasOurReceipt: false,
                            },
                        ],
                    },
                ],
            };
            assertNewMatchForHiringManagerAndTeammate(false, false, opts);
        });

        it('should be true if hiring manager has unread messages from the other', () => {
            const opts = {
                conversationOverrides: [
                    {
                        messages: [
                            {
                                from: 'me',
                            },
                            {
                                from: 'them',
                                isRead: false,
                            },
                            {
                                from: 'them',
                                isRead: false,
                            },
                        ],
                    },
                ],
            };
            assertNewMatchForHiringManagerAndTeammate(true, true, opts);
        });

        function assertNewMatchForHiringManagerAndTeammate(
            expectedValue,
            expectedTeammateValue,
            opts,
            matchedByHiringManager,
        ) {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', opts);
            hiringRelationshipViewModel = _.detect(careersNetworkViewModel.hiringRelationshipViewModels, vm => {
                if (matchedByHiringManager) {
                    Object.defineProperty(vm, 'matchedByHiringManager', {
                        value: true,
                    });
                }
                return vm.matched;
            });

            expect(hiringRelationshipViewModel.newMatch).toBe(expectedValue);

            // sometimes true for teammate
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel(
                'hiringManager',
                _.extend(opts, {
                    teammateId: 'teammate',
                }),
            );
            hiringRelationshipViewModel = _.detect(careersNetworkViewModel.hiringRelationshipViewModels, vm => {
                if (matchedByHiringManager) {
                    Object.defineProperty(vm, 'matchedByHiringManager', {
                        value: true,
                    });
                }
                return vm.matched;
            });
            expect(hiringRelationshipViewModel.newMatch).toBe(expectedTeammateValue);
        }
    });

    describe('hasMultipleHiringTeammatesParticipatingInConversation', () => {
        it('should work', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                teammateId: 'teammate',
            });
            const hiringRelationshipViewModel = careersNetworkViewModel.testHelper.getHiringRelationshipViewModelWithConversation();

            // sanity checks.  Only the teammate has sent a message so far
            const senderIds = _.chain(hiringRelationshipViewModel.conversation.messages)
                .pluck('sender_id')
                .uniq()
                .value();
            expect(_.contains(senderIds, careersNetworkViewModel.user.id)).not.toBe(true);
            expect(_.contains(senderIds, 'teammate')).toBe(true);

            expect(hiringRelationshipViewModel.hasMultipleHiringTeammatesParticipatingInConversation).toBe(false);

            // when this hiring manager sends a message, there are now multiple participants
            hiringRelationshipViewModel.addMessage('ok!');
            expect(hiringRelationshipViewModel.hasMultipleHiringTeammatesParticipatingInConversation).toBe(true);
        });
    });

    describe('theirDisplayNameForConversation', () => {
        it('should work for candidate', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            let hasMultipleHiringTeammatesParticipatingInConversation = false;
            jest.spyOn(
                hiringRelationshipViewModel,
                'hasMultipleHiringTeammatesParticipatingInConversation',
                'get',
            ).mockImplementation(() => hasMultipleHiringTeammatesParticipatingInConversation);

            expect(hiringRelationshipViewModel.theirDisplayNameForConversation).toEqual(
                hiringRelationshipViewModel.connectionNickname,
            );

            hasMultipleHiringTeammatesParticipatingInConversation = true;
            expect(hiringRelationshipViewModel.theirDisplayNameForConversation).toEqual(
                hiringRelationshipViewModel.connectionCompany,
            );
        });

        it('should work for hiring manager', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            expect(hiringRelationshipViewModel.theirDisplayNameForConversation).toEqual(
                hiringRelationshipViewModel.connectionNickname,
            );
        });
    });

    describe('showUpdateModal', () => {
        describe('as hiring manager', () => {
            it('should call showFullProfile', () => {
                const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
                const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
                jest.spyOn(careersNetworkViewModel, 'showFullProfile').mockImplementation(() => {});
                hiringRelationshipViewModel.showUpdateModal();
                expect(careersNetworkViewModel.showFullProfile).toHaveBeenCalled();
                const options = careersNetworkViewModel.showFullProfile.mock.calls[0][0];
                expect(options.careerProfile).toEqual(hiringRelationshipViewModel.connectionProfile);
            });
        });

        describe('as teammate', () => {
            it('should call showFullProfile', () => {
                const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                    teammateId: 'teammate',
                });
                const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
                jest.spyOn(careersNetworkViewModel, 'showFullProfile').mockImplementation(() => {});
                hiringRelationshipViewModel.showUpdateModal();
                expect(careersNetworkViewModel.showFullProfile).toHaveBeenCalled();
                const options = careersNetworkViewModel.showFullProfile.mock.calls[0][0];
                expect(options.careerProfile).toEqual(hiringRelationshipViewModel.connectionProfile);
            });
        });

        describe('as candidate', () => {
            it('should show modal', () => {
                const DialogModal = $injector.get('DialogModal');
                const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
                const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
                jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
                hiringRelationshipViewModel.showUpdateModal();
                expect(DialogModal.alert).toHaveBeenCalled();
                const opts = DialogModal.alert.mock.calls[0][0];
                expect(opts.scope.hiringApplication.id).toEqual(hiringRelationshipViewModel.hiringApplication.id);
            });
        });
    });

    function getFirstHiringRelationship(role) {
        const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel(role);
        hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
        hiringRelationship = hiringRelationshipViewModel.hiringRelationship;
    }
});
