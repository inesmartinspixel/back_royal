import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import setSpecLocales from 'Translation/setSpecLocales';
import salarySurveyFormSectionLocales from 'Careers/locales/careers/edit_career_profile/salary_survey_form_section-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';

setSpecLocales(editCareerProfileLocales);
setSpecLocales(salarySurveyFormSectionLocales);

describe('FrontRoyal.Careers.EditCareerProfile.SalarySurveyFormSection', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let CareerProfile;
    let careerProfile;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');

                $injector.get('CareerProfileFixtures');
            },
        ]);

        careerProfile = CareerProfile.fixtures.getInstance();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.careerProfile = careerProfile;
        renderer.render('<salary-survey-form-section career-profile="careerProfile"></salary-survey-form-section>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it("should include 'prefer_not_to_disclose' as a valid option", () => {
        render();
        const preferNotToDiscloseOption = _.findWhere(scope.salaryRangeOptions, {
            value: 'prefer_not_to_disclose',
        });
        expect(preferNotToDiscloseOption).toBeTruthy();
    });

    it("should default the salary_range on the careerProfile to 'less_than_40000'", () => {
        careerProfile.salary_range = null;
        render();
        expect(scope.careerProfile.salary_range).toEqual('less_than_40000');
    });

    it("should not set the salary_range on the careerProfile to 'less_than_40000' if the salary_range has already been set", () => {
        careerProfile.salaray_range = 'prefer_not_to_disclose';
        render();
        expect(scope.careerProfile.salary_range).toEqual('prefer_not_to_disclose');
    });

    it('should show caption text', () => {
        render();
        SpecHelper.expectElementText(
            elem,
            '.sub-text',
            'This information is confidential and will not be shown publicly. It helps us match you with potential employers if you join the career network.',
        );
    });

    it('should support updating the salary_range on the careerProfile', () => {
        const newValue = '80000_to_89999';
        careerProfile.salaray_range = 'prefer_not_to_disclose';
        render();
        expect(scope.careerProfile.salary_range).not.toEqual(newValue);
        SpecHelper.updateSelect(elem, 'select[name="salary_range"]', newValue);
        expect(scope.careerProfile.salary_range).toEqual(newValue);
    });
});
