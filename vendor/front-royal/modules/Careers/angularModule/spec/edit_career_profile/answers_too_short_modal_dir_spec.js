import 'AngularSpecHelper';
import 'Careers/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import answersTooShortModalLocales from 'Careers/locales/careers/edit_career_profile/answers_too_short_modal-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';

setSpecLocales(editCareerProfileLocales);
setSpecLocales(answersTooShortModalLocales);

describe('FrontRoyal.Careers.EditCareerProfile.AnswersTooShortModal', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let user;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);

        user = SpecHelper.stubCurrentUser();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.onFinish = jest.fn();
        renderer.render('<answers-too-short-modal on-finish="onFinish"></answers-too-short-modal>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('closeModal', () => {
        it('should save user and call onFinish', () => {
            jest.spyOn(user, 'save').mockImplementation(() => {});
            user.ghostMode = false;
            render();
            scope.closeModal();
            expect(user.save).toHaveBeenCalled();
            expect(scope.onFinish).toHaveBeenCalled();
        });

        it('should not save user when in ghostMode but should still call onFinish', () => {
            jest.spyOn(user, 'save').mockImplementation(() => {});
            user.ghostMode = true;
            render();
            scope.closeModal();
            expect(user.save).not.toHaveBeenCalled();
            expect(scope.onFinish).toHaveBeenCalled();
        });
    });
});
