import 'AngularSpecHelper';
import 'Careers/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import answersTooShortLocales from 'Careers/locales/careers/edit_career_profile/answers_too_short_modal-en.json';

setSpecLocales(editCareerProfileLocales, answersTooShortLocales);

describe('Settings.answersTooShortHelper', () => {
    let $injector;
    let SpecHelper;
    let answersTooShortHelper;
    let user;
    let DialogModal;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                answersTooShortHelper = $injector.get('answersTooShortHelper');
                DialogModal = $injector.get('DialogModal');
            },
        ]);

        user = SpecHelper.stubCurrentUser();
        user.has_seen_short_answer_warning = false;
    });

    describe('maybeShowModal', () => {
        const object = {};

        beforeEach(() => {
            object.answer1 = 'too short';
            object.answer2 = 'too short';
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
        });

        it('should show modal if length of object fields is below minLength', () => {
            answersTooShortHelper.maybeShowModal(object, ['answer1', 'answer2'], 100);
            expect(DialogModal.alert).toHaveBeenCalled();
        });

        it('should NOT show modal if length of object fields is above minLength', () => {
            answersTooShortHelper.maybeShowModal(object, ['answer1', 'answer2'], 10);
            expect(DialogModal.alert).not.toHaveBeenCalled();
        });

        it('should handle undefined values', () => {
            answersTooShortHelper.maybeShowModal(object, ['answer1', 'answer2', 'foo'], 100);
            expect(DialogModal.alert).toHaveBeenCalled();
        });

        it('should handle null and undefined values', () => {
            object.bar = null;
            answersTooShortHelper.maybeShowModal(object, ['answer1', 'answer2', 'foo', 'bar'], 100);
            expect(DialogModal.alert).toHaveBeenCalled();
        });

        it('should NOT show modal if user has_seen_short_answer_warning', () => {
            user.has_seen_short_answer_warning = true;
            answersTooShortHelper.maybeShowModal(object, ['answer1', 'answer2'], 100);
            expect(DialogModal.alert).not.toHaveBeenCalled();
        });
    });
});
