import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import setSpecLocales from 'Translation/setSpecLocales';
import workExperienceDetailLocales from 'Careers/locales/careers/edit_career_profile/work_experience_detail-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import chooseAnItemLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/choose_an_item-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import chooseARoleLocales from 'Careers/locales/careers/choose_a_role-en.json';
import chooseADateLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/choose_a_date-en.json';
import addAnItemLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/add_an_item-en.json';

setSpecLocales(
    workExperienceDetailLocales,
    editCareerProfileLocales,
    chooseAnItemLocales,
    fieldOptionsLocales,
    chooseARoleLocales,
    chooseADateLocales,
    addAnItemLocales,
);

describe('FrontRoyal.Careers.EditCareerProfile.WorkExperienceDetail', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let user;
    let CareerProfile;
    let WorkExperience;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');
                WorkExperience = $injector.get('WorkExperience');
                $injector.get('CareerProfileFixtures');
                SpecHelper.stubConfig();
            },
        ]);

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = user.career_profile.work_experiences;
        renderer.scope.employmentType = opts.employmentType;
        renderer.render(
            '<work-experience-detail ng-model="ngModel" employment-type="{{employmentType}}"></work-experience-detail>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should have correct button key according to how many experiences there are', () => {
        user.career_profile.work_experiences = [
            WorkExperience.new({
                employment_type: 'full_time',
            }),
        ];
        render({
            employmentType: 'full_time',
        });
        SpecHelper.expectElementText(elem, 'button[name="add-work-experience"]', 'Add Another Work Experience');
        scope.ngModel = [];
        scope.$digest();
        SpecHelper.expectElementText(elem, 'button[name="add-work-experience"]', 'Add a Work Experience');
    });

    it('should display the experiences in ngModel filtered by employmentType', () => {
        user.career_profile.work_experiences = [
            WorkExperience.new(),
            WorkExperience.new({
                employment_type: 'part_time',
            }),
            WorkExperience.new(),
        ];
        render({
            employmentType: 'full_time',
        });
        SpecHelper.expectElements(elem, '.experience-detail', 2); // for the two full-time work experiences
    });

    it('should delete the work experience when the icon is pressed', () => {
        user.career_profile.work_experiences = [
            WorkExperience.new(),
            WorkExperience.new({
                employment_type: 'part_time',
            }),
        ];
        render({
            employmentType: 'part_time',
        });
        const workExperience = user.career_profile.work_experiences[1];
        const originalNum = scope.ngModel.length;
        expect(scope.ngModel.includes(workExperience)).toBe(true);
        SpecHelper.click(elem, '[name="remove-experience"]');
        expect(scope.ngModel.length).toBe(originalNum - 1);
        expect(scope.ngModel.includes(workExperience)).toBe(false);
    });

    it('should end pad the date, but not the start date', () => {
        render({
            employmentType: 'full_time',
        });
        const numFullTimeWorkExperiences = _.where(scope.ngModel, {
            employment_type: 'full_time',
        }).length;
        SpecHelper.expectElements(
            elem,
            'choose-a-date[name="end_date"][allow-future="true"]',
            numFullTimeWorkExperiences,
        );
        SpecHelper.expectElements(
            elem,
            'choose-a-date[name="start_date"]:not([allow-future="false"])',
            numFullTimeWorkExperiences,
        );
    });

    it('should show the role dropdown', () => {
        user.career_profile.work_experiences = [
            WorkExperience.new({
                role: 'some_role',
            }),
        ];
        render({
            employmentType: 'full_time',
        });
        SpecHelper.expectElements(elem, 'choose-a-role', 2); // mobile + non-mobile
    });

    it('should show the industry dropdown', () => {
        user.career_profile.work_experiences = [
            WorkExperience.new({
                industry: 'some_industry',
            }),
        ];
        render({
            employmentType: 'full_time',
        });
        SpecHelper.expectElements(elem, 'choose-an-item[item-key="industry"]', 2); // mobile + non-mobile
    });

    describe('when adding new work experience', () => {
        it('should push experience onto ngModel', () => {
            render({
                employmentType: 'full_time',
            });
            const newWorkExperience = {};
            const originalNumWorkExperiences = scope.ngModel.length;

            jest.spyOn(WorkExperience, 'new').mockReturnValue(newWorkExperience);
            jest.spyOn(scope.ngModel, 'push');

            SpecHelper.click(elem, '[name="add-work-experience"]');

            expect(scope.ngModel.push).toHaveBeenCalledWith(newWorkExperience);
            expect(scope.ngModel.length).toEqual(originalNumWorkExperiences + 1);
        });

        it('should set work experience employment_type to value of employmentType', () => {
            render({
                employmentType: 'foo',
            });
            expect(scope.employmentType).toEqual('foo'); // sanity check

            SpecHelper.click(elem, '[name="add-work-experience"]');

            const newWorkExperience = _.last(scope.ngModel);
            expect(newWorkExperience.employment_type).toEqual('foo');
        });

        it('should show new, blank experience detail form', () => {
            render({
                employmentType: 'full_time',
            });
            SpecHelper.expectElement(elem, '.experience-detail');
            SpecHelper.click(elem, '[name="add-work-experience"]');
            SpecHelper.expectElements(elem, '.experience-detail', 2);
        });
    });
});
