import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import * as userAgentHelper from 'userAgentHelper';
import setSpecLocales from 'Translation/setSpecLocales';
import stubSpecLocale from 'Translation/stubSpecLocale';

import uploadLocales from 'FrontRoyalUpload/locales/front_royal_upload/front_royal_upload-en.json';
import resumeAndLinksFormLocales from 'Careers/locales/careers/edit_career_profile/resume_and_links_form-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';

setSpecLocales(editCareerProfileLocales, resumeAndLinksFormLocales, uploadLocales);

stubSpecLocale('front_royal_form.invalid_fields_links.please_complete_fields');

describe('FrontRoyal.Careers.EditCareerProfile.ResumeAndLinksForm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let $window;
    let StudentDashboard;
    let user;
    let CareerProfile;
    let EditCareerProfileHelper;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $window = $injector.get('$window');
                StudentDashboard = $injector.get('StudentDashboard');
                CareerProfile = $injector.get('CareerProfile');
                EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                $timeout = $injector.get('$timeout');

                $injector.get('CareerProfileFixtures');
                SpecHelper.stubConfig();
            },
        ]);

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
    });

    function render() {
        StudentDashboard.expect('index').toBeCalledWith({
            get_has_available_incomplete_streams: true,
            get_has_available_incomplete_playlists: true,
            user_id: user.id,
            filters: {},
        });

        renderer = SpecHelper.renderer();
        renderer.scope.steps = [
            {
                stepName: 'resume_and_links',
            },
        ];
        renderer.render('<edit-career-profile steps="steps"></edit-career-profile>');
        $timeout.flush(); // apply the scope.steps assignment
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('required resume', () => {
        beforeEach(() => {
            jest.spyOn(EditCareerProfileHelper, 'supportsRequiredResume');
        });

        it('should be required for biz certs', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('the_business_certificate');
            render();
            SpecHelper.expectElementAttr(elem, '.upload-button', 'required', 'required');
        });

        it('should be required for career network', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('career_network_only');
            render();
            SpecHelper.expectElementAttr(elem, '.upload-button', 'required', 'required');
        });

        it('should NOT be required for MBA', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
            render();
            SpecHelper.expectElementAttr(elem, '.upload-button', 'required', undefined);
        });

        it('should NOT be required for EMBA', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('emba');
            render();
            SpecHelper.expectElementAttr(elem, '.upload-button', 'required', undefined);
        });

        it('should NOT be required when in the applicant editor', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('the_business_certificate');
            jest.spyOn(EditCareerProfileHelper, 'isApplicantEditor').mockReturnValue(true);
            render();
            SpecHelper.expectElementAttr(elem, '.upload-button', 'required', undefined);
        });
    });

    describe('required linked in profile', () => {
        beforeEach(() => {
            jest.spyOn(EditCareerProfileHelper, 'supportsRequiredLinkedInProfile');
        });

        it('should be required for career network', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('career_network_only');
            render();
            SpecHelper.expectHasClass(elem, '[name="li-profile-url-label"]', 'required');
            SpecHelper.expectElementAttr(elem, '[name="li-profile-url-input"]', 'required', 'required');
        });

        it('should be required for biz certs', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('the_business_certificate');
            render();
            SpecHelper.expectHasClass(elem, '[name="li-profile-url-label"]', 'required');
            SpecHelper.expectElementAttr(elem, '[name="li-profile-url-input"]', 'required', 'required');
        });

        it('should NOT be required for MBA', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
            render();
            SpecHelper.expectDoesNotHaveClass(elem, '[name="li-profile-url-label"]', 'required');
            SpecHelper.expectElementAttr(elem, '[name="li-profile-url-input"]', 'required', undefined);
        });

        it('should NOT be required for EMBA', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('emba');
            render();
            SpecHelper.expectDoesNotHaveClass(elem, '[name="li-profile-url-label"]', 'required');
            SpecHelper.expectElementAttr(elem, '[name="li-profile-url-input"]', 'required', undefined);
        });
    });

    it('should not show resume upload on iOS tablet device', () => {
        // Mock out Cordova properties
        Object.defineProperty($window, 'CORDOVA', {
            value: true,
            configurable: true,
        });

        jest.spyOn(userAgentHelper, 'isiOSDevice').mockReturnValue(true);
        jest.spyOn(userAgentHelper, 'isMobileDevice').mockReturnValue(true);

        render();
        SpecHelper.expectNoElement(elem, '[name="resume-upload"]');

        delete $window.CORDOVA;
        delete $window.device;
    });

    it('should be wired to careerProfile', () => {
        render();
        expect(scope.careerProfile).toBeDefined();
    });

    it('should call logEventForApplication when initialized', () => {
        jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});
        render();
        expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith('viewed-resume_and_links');
    });

    it('should return max file size error message', () => {
        render();
        const childScope = $(elem).find('resume-and-links-form').scope();
        childScope.onFileSelect(null, [
            {
                $error: 'maxSize',
                $errorParam: '3MB',
            },
        ]);
        scope.$digest();
        expect(childScope.errMessage).toBe('Sorry, your upload exceeds the maximum file size: 3MB.');
    });

    it('should show correct url inputs', () => {
        render();
        SpecHelper.expectElement(elem, '[name="personal-website-input"]');
        SpecHelper.expectElement(elem, '[name="blog-input"]');
        SpecHelper.expectElement(elem, '[name="twitter-input"]');
        SpecHelper.expectElement(elem, '[name="facebook-input"]');
        SpecHelper.expectElement(elem, '[name="github-input"]');
    });

    // invalidFieldsLinks dir
    it('should render labels/labelText for missing required fields when blank form is submitted', () => {
        jest.spyOn(EditCareerProfileHelper, 'supportsRequiredResume').mockReturnValue(true);
        jest.spyOn(user, 'programType', 'get').mockReturnValue('career_network_only');
        user.career_profile.li_profile_url = null;
        user.career_profile.resume = null;

        render();
        scope.save();
        scope.$digest();

        expect(scope.invalidFields.length).toBe(2);

        // items don't order as expected because in headless Chrome they have an offset = {top: 0, left: 0}
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(0)', 'Public LinkedIn URL');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(1)', 'Resume / CV');
    });
});
