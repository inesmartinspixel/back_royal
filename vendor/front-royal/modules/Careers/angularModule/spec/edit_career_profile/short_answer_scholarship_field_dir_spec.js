import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import setSpecLocales from 'Translation/setSpecLocales';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import writeTextAboutLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/write_text_about-en.json';

setSpecLocales(editCareerProfileLocales, writeTextAboutLocales);

describe('FrontRoyal.Careers.EditCareerProfile.ShortAnswerScholarshipField', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let CareerProfile;
    let careerProfile;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');

                $injector.get('CareerProfileFixtures');
            },
        ]);

        careerProfile = CareerProfile.fixtures.getInstance();
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.careerProfile = careerProfile;
        const isRequiredHtml = opts.isRequired === undefined ? '' : `is-required="${opts.isRequired}"`;
        renderer.render(
            `<short-answer-scholarship-field career-profile="careerProfile" ${isRequiredHtml}></short-answer-scholarship-field>`,
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('isRequired', () => {
        it('should default to false', () => {
            render();
            expect(elem.attr('is-required')).toBeUndefined();
            expect(scope.isRequired).toBe(false);
        });

        it('should respect is-required attr when true', () => {
            render({
                isRequired: true,
            });
            expect(elem.attr('is-required')).toEqual('true');
            expect(scope.isRequired).toBe(true);
        });

        it('should respect is-required attr when false', () => {
            render({
                isRequired: false,
            });
            expect(elem.attr('is-required')).toEqual('false');
            expect(scope.isRequired).toBe(false);
        });

        it('should be wired to ng-required on write-text-about', () => {
            render();
            SpecHelper.expectElement(elem, 'write-text-about[ng-required="isRequired"]');
        });
    });

    describe('write-text-about', () => {
        it('should be wired to short_answer_scholarship attr on careerProfile', () => {
            render();
            SpecHelper.expectElement(elem, 'write-text-about[ng-model="careerProfile.short_answer_scholarship"]');
        });
    });
});
