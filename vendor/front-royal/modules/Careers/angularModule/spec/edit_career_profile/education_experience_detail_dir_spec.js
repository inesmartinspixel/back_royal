import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import setSpecLocales from 'Translation/setSpecLocales';
import educationExperienceDetailLocales from 'Careers/locales/careers/edit_career_profile/education_experience_detail-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';

setSpecLocales(editCareerProfileLocales);
setSpecLocales(educationExperienceDetailLocales);

describe('FrontRoyal.Careers.EditCareerProfile.EducationExperienceDetail', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let CareerProfile;
    let careerProfile;
    let EducationExperience;
    let user;
    let CohortApplication;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');
                CohortApplication = $injector.get('CohortApplication');
                EducationExperience = $injector.get('EducationExperience');

                $injector.get('CareerProfileFixtures');
                $injector.get('CohortApplicationFixtures');
                SpecHelper.stubConfig();
            },
        ]);

        user = SpecHelper.stubCurrentUser('learner');
        user.cohort_applications = [CohortApplication.fixtures.getInstance()];
        user.career_profile = CareerProfile.fixtures.getInstance();

        careerProfile = user.career_profile;
    });

    function render(degreeProgram) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = careerProfile.education_experiences;
        renderer.render(
            `<education-experience-detail ng-model="ngModel" degree-program="${degreeProgram}"></education-experience-detail>`,
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('degree programs', () => {
        it('should have correct button key according to how many experiences there are', () => {
            render(true);
            SpecHelper.expectElementText(elem, 'button[name="add-education-experience"]', 'Add Another Degree Program');
            scope.ngModel = [];
            scope.$digest();
            SpecHelper.expectElementText(elem, 'button[name="add-education-experience"]', 'Add a Degree Program');
        });

        it('should allow adding a blank education experience', () => {
            render(true);
            const originalNum = scope.ngModel.length;

            SpecHelper.click(elem, '[name="add-education-experience"]');
            SpecHelper.expectElements(elem, '.experience-detail', originalNum + 1);
        });

        it('should delete the education experience when the icon is pressed', () => {
            render(true);
            const originalNum = scope.ngModel.length;
            SpecHelper.click(elem, '[name="remove-experience"]', 0);
            expect(scope.ngModel.length).toBe(originalNum - 1);
            SpecHelper.click(elem, '[name="remove-experience"]', 0);
            SpecHelper.expectNoElement(elem, '.experience-detail');
        });

        it('should have correct text in year field if program is or will be graduated', () => {
            render(true);
            SpecHelper.updateSelect(elem, '.year-container select:eq(0)', false);
            SpecHelper.expectElementText(elem, '.year-container select:eq(1) option[disabled]', 'Grad Year');
        });

        it('should show tooltip when degree selectize is focused', () => {
            render(true);
            SpecHelper.expectDoesNotHaveClass(elem, '.degree-tooltip:eq(0)', 'show');
            SpecHelper.selectizeOnFocus(elem, 'selectize.detail-input:eq(0)');
            SpecHelper.expectHasClass(elem, '.degree-tooltip:eq(0)', 'show');
            SpecHelper.selectizeOnBlur(elem, 'selectize.detail-input:eq(0)');
            SpecHelper.expectDoesNotHaveClass(elem, '.degree-tooltip:eq(0)', 'show');
        });

        describe('$$disableEdit', () => {
            beforeEach(() => {
                user.transcripts_verified = false;
                user.lastCohortApplication.status = 'pre_accepted';
                jest.spyOn(user.career_profile, 'indicatesUserShouldProvideTranscripts', 'get').mockReturnValue(true);
            });

            it('should be true if criteria met', () => {
                render(true);
                scope.educationExperiences.forEach(experience => {
                    expect(experience.$$disableEdit).toBe(true);
                });
            });

            it('should be false if transcripts_verified', () => {
                user.transcripts_verified = true;
                render(true);
                scope.educationExperiences.forEach(experience => {
                    expect(experience.$$disableEdit).toBeFalsy();
                });
            });

            it('should be false if pending', () => {
                user.lastCohortApplication.status = 'pending';
                render(true);
                scope.educationExperiences.forEach(experience => {
                    expect(experience.$$disableEdit).toBeFalsy();
                });
            });

            it('should be false if added experience', () => {
                render(true);
                SpecHelper.click(elem, '[name="add-education-experience"]');
                expect(scope.educationExperiences[scope.educationExperiences.length - 1].$$disableEdit).toBe(false);
            });

            it('should be falsy if not degree programs', () => {
                render(false);
                scope.educationExperiences.forEach(experience => {
                    expect(experience.$$disableEdit).toBeFalsy();
                });
            });

            it('should disable UI when true', () => {
                render(true);
                // organization-autocomplete
                // selectize
                SpecHelper.expectElementDisabled(elem, '.performance-container input', true, 0);
                SpecHelper.expectElementDisabled(elem, '.performance-container input', true, 1);
                SpecHelper.expectElementDisabled(elem, '.performance-container input', true, 2);
                SpecHelper.expectElementDisabled(elem, 'input.detail-input-full', true);
                SpecHelper.expectElementDisabled(elem, 'select.detail-input', true, 0);
                SpecHelper.expectElementDisabled(elem, 'select.detail-input', true, 1);
                SpecHelper.expectNoElement(elem, '.img-icon-container');
            });
        });
    });

    describe('non-degree programs', () => {
        it('should have correct button key according to how many experiences there are', () => {
            render(false);
            scope.ngModel.push(
                EducationExperience.new({
                    educational_organization: {
                        text: 'University of Foo',
                    },
                    graduation_year: 1999,
                    degree: 'Bachelor of Foo',
                    major: 'Foo Science',
                    minor: 'Bar Engineering',
                    gpa: '3.5',
                    degree_program: false,
                }),
            );
            scope.$digest();
            SpecHelper.expectElementText(elem, 'button[name="add-education-experience"]', 'Add Another Program');
            scope.ngModel = [];
            scope.$digest();
            SpecHelper.expectElementText(elem, 'button[name="add-education-experience"]', 'Add a Program');
        });

        it('should allow adding a blank education experience', () => {
            render(false);
            SpecHelper.click(elem, '[name="add-education-experience"]');
            SpecHelper.expectElements(elem, '.experience-detail', 1);
        });

        it('should delete the education experience when the icon is pressed', () => {
            render(false);

            // add a non-degree program to the model so we can remove it
            scope.ngModel.push(
                EducationExperience.new({
                    educational_organization: {
                        text: 'University of Foo',
                    },
                    graduation_year: 1999,
                    degree: 'Bachelor of Foo',
                    major: 'Foo Science',
                    minor: 'Bar Engineering',
                    gpa: '3.5',
                    degree_program: false,
                }),
            );
            scope.$digest();

            const originalNum = scope.ngModel.length;
            SpecHelper.click(elem, '[name="remove-experience"]', 0);
            expect(scope.ngModel.length).toBe(originalNum - 1);
            SpecHelper.expectNoElement(elem, '.experience-detail');
        });

        it('should have correct text in year field if program is or will be completed', () => {
            render(false);
            SpecHelper.click(elem, '[name="add-education-experience"]');
            SpecHelper.updateSelect(elem, '.year-container select:eq(0)', false);
            SpecHelper.expectElementText(elem, '.year-container select:eq(1) option[disabled]', 'Year Completed');
        });
    });
});
