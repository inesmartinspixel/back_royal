import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import setSpecLocales from 'Translation/setSpecLocales';
import personalSummaryFormLocales from 'Careers/locales/careers/edit_career_profile/personal_summary_form-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import demographicsFormSectionLocales from 'Careers/locales/careers/edit_career_profile/demographics_form_section-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import writeTextAboutLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/write_text_about-en.json';
import invalidFieldsLinksLocales from 'FrontRoyalForm/locales/front_royal_form/invalid_fields_links-en.json';

setSpecLocales(editCareerProfileLocales);
setSpecLocales(
    personalSummaryFormLocales,
    editCareerProfileLocales,
    demographicsFormSectionLocales,
    fieldOptionsLocales,
    writeTextAboutLocales,
    invalidFieldsLinksLocales,
);

describe('FrontRoyal.Careers.EditCareerProfile.PersonalSummaryForm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let StudentDashboard;
    let user;
    let CareerProfile;
    let EditCareerProfileHelper;
    let $timeout;
    let Cohort;
    let relevantCohort;
    let $rootScope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                $rootScope = $injector.get('$rootScope');
                SpecHelper = $injector.get('SpecHelper');
                StudentDashboard = $injector.get('StudentDashboard');
                CareerProfile = $injector.get('CareerProfile');
                EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                $timeout = $injector.get('$timeout');
                Cohort = $injector.get('Cohort');
                $injector.get('CohortFixtures');
                $injector.get('CareerProfileFixtures');
                SpecHelper.stubConfig();
            },
        ]);

        SpecHelper.stubEventLogging();

        user = SpecHelper.stubCurrentUser();
        relevantCohort = Cohort.fixtures.getInstance();
        user.career_profile = CareerProfile.fixtures.getInstance();
        user.relevant_cohort = relevantCohort;
    });

    function render() {
        StudentDashboard.expect('index').toBeCalledWith({
            get_has_available_incomplete_streams: true,
            get_has_available_incomplete_playlists: true,
            user_id: user.id,
            filters: {},
        });

        renderer = SpecHelper.renderer();
        renderer.scope.steps = [
            {
                stepName: 'personal_summary',
            },
        ];
        renderer.render('<edit-career-profile steps="steps"></edit-career-profile>');
        $timeout.flush(); // apply the scope.steps assignment
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should be wired to careerProfile', () => {
        render();
        expect(scope.careerProfile).toBeDefined();
    });

    it('should call logEventForApplication when initialized', () => {
        jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});
        render();
        expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith('viewed-personal_summary');
    });

    it('should have Long Term Goal when Career Network and not legacy', () => {
        Object.defineProperty($rootScope.currentUser, 'programType', {
            value: 'career_network_only',
        });
        $rootScope.currentUser.career_profile.created_at = new Date(2017, 9, 26).getTime() / 1000;
        render();
        const goalInput = SpecHelper.expectElement(elem, 'write-text-about:eq(1) > textarea');
        expect(goalInput.attr('placeholder')).toEqual(
            'I hope to combine my long-standing interest in marketing with a steady climb up the executive ranks in a Fortune 500 company, ultimately running an international division...',
        );
    });

    it('should have field asking the user how they heard about us', () => {
        // when this is true, the 'How did you hear about Quantic?' question becomes the third form field
        jest.spyOn(EditCareerProfileHelper, 'supportsLongTermGoalField').mockReturnValue(true);
        render();
        SpecHelper.expectElementText(elem, '.form-group:eq(2) label', 'How did you hear about Quantic?');
    });

    it('should contain correct howHearOptionKeys', () => {
        render();
        const childScope = $(elem).find('personal-summary-form').scope();
        expect(childScope.howHearOptionKeys).toEqual([
            'financial_times',
            'economist',
            'tv_cnbc',
            'tv_bloomberg',
            'facebook',
            'instagram',
            'linkedin',
            'friend',
            'ivyexec',
            'search_engine',
            'other', // included by select-with-other
        ]);
    });

    it('should show the demographics-form-section', () => {
        render();
        SpecHelper.expectElement(elem, 'demographics-form-section');
    });

    describe('peer recommendations section', () => {
        it('should be hidden if program type !supportsPeerRecommendations()', () => {
            jest.spyOn(EditCareerProfileHelper, 'supportsPeerRecommendations').mockReturnValue(false);
            render();
            SpecHelper.expectNoElement(elem, '.peer-recommendations');
        });
        it('should be visible if isApplication', () => {
            jest.spyOn(EditCareerProfileHelper, 'supportsPeerRecommendations').mockReturnValue(true);
            jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(true);
            jest.spyOn(EditCareerProfileHelper, 'isApplicantEditor').mockReturnValue(false);
            render();
            SpecHelper.expectElement(elem, '.peer-recommendations');
        });
        it('should be visible in admin application editor', () => {
            jest.spyOn(EditCareerProfileHelper, 'supportsPeerRecommendations').mockReturnValue(true);
            jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(false);
            jest.spyOn(EditCareerProfileHelper, 'isApplicantEditor').mockReturnValue(true);
            render();
            SpecHelper.expectElement(elem, '.peer-recommendations');
        });
        describe('when visible', () => {
            beforeEach(() => {
                jest.spyOn(EditCareerProfileHelper, 'supportsPeerRecommendations').mockReturnValue(true);
                jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(true);
            });

            it("should be clearly labeled that it's optional", () => {
                render();
                SpecHelper.expectElementText(elem, '.peer-recommendations h3', 'Peer Recommendations (optional)');
            });

            it('should provide instructions', () => {
                render();
                SpecHelper.expectElementText(
                    elem,
                    '.peer-recommendations p.sub-text.dark',
                    'Provide up to 3 colleagues/managers/friends who can provide a positive recommendation to help your application. They will receive a brief 5-10 minute multiple choice survey with the accompanying message below.',
                );
            });

            it('should use peerRecommendations directive', () => {
                render();
                SpecHelper.expectElement(elem, 'peer-recommendations');
            });

            describe('sample message', () => {
                it('should be personalized for MBA user', () => {
                    assertSampleMessageText('mba');
                });

                it('should be personalized for EMBA user', () => {
                    assertSampleMessageText('emba');
                });

                function assertSampleMessageText(programType) {
                    Object.defineProperty($rootScope.currentUser, 'programType', {
                        value: programType,
                    });
                    render();
                    const programTypeVal = programType === 'emba' ? 'Executive MBA' : 'MBA';
                    SpecHelper.expectElementText(
                        elem,
                        '.peer-recommendations .sample-message > p',
                        `${$rootScope.currentUser.name} has submitted an application to the Quantic ${programTypeVal}, a selective degree program for high-achieving professionals, with the following request:`,
                    );
                    SpecHelper.expectElementText(
                        elem,
                        '.peer-recommendations .sample-message blockquote p',
                        "Hey! I've chosen you as a peer reference to help strengthen my application. Please submit the following 5-10 minute survey with honest, thoughtful answers, and let me know if you have any questions!",
                    );
                }
            });
        });
    });

    // invalidFieldsLinks dir
    it('should render labels/labelText for missing required fields when blank form is submitted', () => {
        user.career_profile.personal_fact = null;
        user.career_profile.how_did_you_hear_about_us = null;

        render();
        scope.save();
        scope.$digest();

        expect(scope.invalidFields.length).toBe(3);

        // items don't order as expected because in headless Chrome they have an offset = {top: 0, left: 0}
        // (This may no longer be true.  Started failing after modularization and I switched the order, so
        // maybe now it's the expected order?)

        // Also, do not know why a Jest update is now causing Fun Fact to be rendered twice in JSDOM for this
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(0)', 'Fun Fact');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(1)', 'Fun Fact');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(2)', 'How did you hear about Quantic?');
    });
});
