import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import setSpecLocales from 'Translation/setSpecLocales';
import programChoiceFormLocales from 'Careers/locales/careers/edit_career_profile/program_choice_form-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import invalidFieldLinksLocales from 'FrontRoyalForm/locales/front_royal_form/invalid_fields_links-en.json';

setSpecLocales(programChoiceFormLocales, editCareerProfileLocales, invalidFieldLinksLocales);

describe('FrontRoyal.Careers.EditCareerProfile.ProgramChoiceForm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let StudentDashboard;
    let user;
    let CareerProfile;
    let EditCareerProfileHelper;
    let $timeout;
    let DialogModal;
    let CohortApplication;
    let parentScope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'FrontRoyal.Form', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                StudentDashboard = $injector.get('StudentDashboard');
                CareerProfile = $injector.get('CareerProfile');
                EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                $timeout = $injector.get('$timeout');
                DialogModal = $injector.get('DialogModal');
                CohortApplication = $injector.get('CohortApplication');

                $injector.get('CohortApplicationFixtures');
                $injector.get('CareerProfileFixtures');
                SpecHelper.stubConfig();
            },
        ]);

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
    });

    function render() {
        StudentDashboard.expect('index').toBeCalledWith({
            get_has_available_incomplete_streams: true,
            get_has_available_incomplete_playlists: true,
            user_id: user.id,
            filters: {},
        });

        renderer = SpecHelper.renderer();
        renderer.scope.steps = [
            {
                stepName: 'program_choice',
            },
        ];
        renderer.render('<edit-career-profile steps="steps"></edit-career-profile>');
        $timeout.flush(); // apply the scope.steps assignment
        elem = renderer.elem;
        parentScope = elem.isolateScope();
        scope = elem.find('program-choice-form').scope();
        scope.$apply();
    }

    it('should be wired to careerProfile', () => {
        render();
        expect(scope.careerProfile).toBeDefined();
    });

    it('should call logEventForApplication when initialized', () => {
        jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});
        render();
        expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith('viewed-program_choice');
    });

    it('should show standard options by default', () => {
        render();
        SpecHelper.expectElements(elem, '.program-control.featured', 2);
        SpecHelper.expectElements(elem, '.program-control:not(.featured)', 2);

        SpecHelper.expectElementText(elem, '.program-control.featured:eq(0) .program-title', 'The Free MBA');
        SpecHelper.expectElementText(elem, '.program-control.featured:eq(0) li:eq(1)', 'Global class: 20+ countries');
        SpecHelper.expectElementText(elem, '.program-control.featured:eq(1) .program-title', 'The Executive MBA');
        SpecHelper.expectElementText(elem, '.program-control.featured:eq(1) li:eq(1)', 'Additional group projects');
        SpecHelper.expectElementText(elem, '.program-control:eq(2) .program-title', 'The Free Certificates');
        SpecHelper.expectElementText(elem, '.program-control:eq(3) .program-title', 'Career Network Only');

        // nothing should be selected
        SpecHelper.expectNoElement(elem, '.program-control.selected');

        // nothing should be coming soon
        SpecHelper.expectNoElement(elem, '.program-coming-soon-box');
    });

    describe('by program_type', () => {
        beforeEach(() => {
            render();
        });

        it('should have correct text/options for MBA', () => {
            SpecHelper.expectElementText(elem, '.program-control.featured:eq(0) .program-title', 'The Free MBA');
            SpecHelper.expectElementText(
                elem,
                '.program-control.featured:eq(0) .program-subtitle',
                'For high potential early-career learners.',
            );
            SpecHelper.expectElementText(elem, '.program-control.featured:eq(0) li:eq(0)', 'Complete core curriculum');
            SpecHelper.expectElementText(
                elem,
                '.program-control.featured:eq(0) li:eq(1)',
                'Global class: 20+ countries',
            );
            SpecHelper.expectElementText(elem, '.program-control.featured:eq(0) li:eq(2)', 'Career network access');
        });

        it('should have correct text/options for EMBA', () => {
            SpecHelper.expectElementText(elem, '.program-control.featured:eq(1) .program-title', 'The Executive MBA');
            SpecHelper.expectElementText(
                elem,
                '.program-control.featured:eq(1) .program-subtitle',
                'For high achieving mid-career professionals.',
            );
            SpecHelper.expectElementText(
                elem,
                '.program-control.featured:eq(1) li:eq(0)',
                'Adds specialized electives',
            );
            SpecHelper.expectElementText(elem, '.program-control.featured:eq(1) li:eq(1)', 'Additional group projects');
            SpecHelper.expectElementText(elem, '.program-control.featured:eq(1) li:eq(2)', 'Optional on-site weekends');
            SpecHelper.expectElementText(
                elem,
                '.program-control.featured:eq(1) li:eq(3)',
                'Global class: 20+ countries',
            );
            SpecHelper.expectElementText(elem, '.program-control.featured:eq(1) li:eq(4)', 'Career network access');
        });

        it('should have correct text/options for free certs', () => {
            SpecHelper.expectElementText(elem, '.program-control:eq(2) .program-title', 'The Free Certificates');
            SpecHelper.expectElementText(
                elem,
                '.program-control:eq(2) .program-subtitle:eq(0)',
                'Fundamentals of Business: A 6-week foundational program for students of all ages.',
            );
            SpecHelper.expectElementText(elem, '.program-control:eq(2) .program-subtitle:eq(1)', 'Free If Admitted');
        });

        it('should have correct text/options for career network', () => {
            SpecHelper.expectElementText(elem, '.program-control:eq(3) .program-title', 'Career Network Only');
            SpecHelper.expectElementText(
                elem,
                '.program-control:eq(3) .program-subtitle:eq(0)',
                'For students and professionals only interested in new job opportunities.',
            );
            SpecHelper.expectElementText(elem, '.program-control:eq(3) .program-subtitle:eq(1)', 'Free If Admitted');
        });
    });

    describe('selection', () => {
        it('should pre-select a program option if available on career profile', () => {
            user.career_profile.program_type = 'mba';
            render();

            SpecHelper.expectElementText(elem, '.program-control.selected .program-title', 'The Free MBA');
        });

        it('should work to select a program choice via select buttons', () => {
            render();

            const currentUser = $injector.get('$rootScope').currentUser;
            expect(currentUser.$$pendingProgramTypeSelected).toBeUndefined();

            SpecHelper.expectNoElement(elem, '.program-control.selected');

            // select the EMBA option
            SpecHelper.click(elem, '.select-button:eq(1)');

            // it should now be selected
            SpecHelper.expectElement(elem, '.program-control:eq(1).selected');
            expect(scope.careerProfile.program_type).toEqual('emba');
            expect(currentUser.$$pendingProgramTypeSelected).toBe('emba');
        });

        it('should work to select a program choice via radio buttons', () => {
            render();

            const currentUser = $injector.get('$rootScope').currentUser;
            expect(currentUser.$$pendingProgramTypeSelected).toBeUndefined();

            SpecHelper.expectNoElement(elem, '.program-control.selected');

            // select the EMBA option
            SpecHelper.toggleRadio(elem, '.program-control input', 1);

            // it should now be selected
            SpecHelper.expectElement(elem, '.program-control:eq(1).selected');
            expect(scope.careerProfile.program_type).toEqual('emba');
            expect(currentUser.$$pendingProgramTypeSelected).toBe('emba');
        });
    });

    it('should hide the career network choice if user has career access', () => {
        Object.defineProperty(user, 'hasCareersNetworkAccess', {
            value: true,
        });
        render();
        SpecHelper.expectElements(elem, '.program-control.featured', 2);
        SpecHelper.expectElements(elem, '.program-control:not(.featured)', 1);

        SpecHelper.expectElementText(elem, '.program-control:eq(0) .program-title', 'The Free MBA');
        SpecHelper.expectElementText(elem, '.program-control:eq(1) .program-title', 'The Executive MBA');
        SpecHelper.expectElementText(elem, '.program-control:eq(2) .program-title', 'The Free Certificates');
    });

    it('should hide non-mba options if in the h experiment', () => {
        user.experiment_ids = ['h'];
        render();
        SpecHelper.expectElements(elem, '.program-control.featured', 2);

        SpecHelper.expectElementText(elem, '.program-control:eq(0) .program-title', 'The Free MBA');
        SpecHelper.expectElementText(elem, '.program-control:eq(1) .program-title', 'The Executive MBA');

        SpecHelper.expectNoElement(elem, '.program-control:not(.featured)');
        SpecHelper.expectNoElement(elem, '[name="options-header-additional"]');
        SpecHelper.expectNoElement(elem, '.non-featured-options');
    });

    it('should show coming soon for EMBA if enabled in user config', () => {
        SpecHelper.stubConfig({
            emba_coming_soon: 'true',
        });
        render();

        SpecHelper.expectElement(elem, '.program-coming-soon-box');
    });

    describe('on program type change', () => {
        beforeEach(() => {
            jest.spyOn(user, 'lastCohortApplication', 'get').mockReturnValue(CohortApplication.fixtures.getInstance());
            user.lastCohortApplication.warn_if_switching_from_mba_emba = true;
        });

        it('should show modal if user is changing from mba type to a non-degree type', () => {
            user.career_profile.program_type = 'mba';
            render();
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            scope.careerProfile.program_type = 'career_network_only';
            scope.$digest();
            expect(DialogModal.alert).toHaveBeenCalled();

            DialogModal.alert.mockClear();
            scope.careerProfile.program_type = 'the_business_certificate';
            scope.$digest();
            expect(DialogModal.alert).toHaveBeenCalled();

            DialogModal.alert.mockClear();
            scope.careerProfile.program_type = 'emba';
            scope.$digest();
            expect(DialogModal.alert).not.toHaveBeenCalled();
        });

        it('should show modal if user is changing from emba type to a non-degree type', () => {
            user.career_profile.program_type = 'emba';
            render();
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            scope.careerProfile.program_type = 'career_network_only';
            scope.$digest();
            expect(DialogModal.alert).toHaveBeenCalled();

            DialogModal.alert.mockClear();
            scope.careerProfile.program_type = 'the_business_certificate';
            scope.$digest();
            expect(DialogModal.alert).toHaveBeenCalled();

            DialogModal.alert.mockClear();
            scope.careerProfile.program_type = 'mba';
            scope.$digest();
            expect(DialogModal.alert).not.toHaveBeenCalled();
        });
    });

    // invalidFieldsLinks dir
    it('should render labels/labelText for missing required fields when blank form is submitted', () => {
        user.career_profile.program_type = null;

        render();
        parentScope.save();
        parentScope.$digest();

        // can't test for text because it uses the page's application-header-text which is not visible in this elem
        expect(parentScope.invalidFields.length).toBe(1);
    });
});
