import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import setSpecLocales from 'Translation/setSpecLocales';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import embaApplicationQuestionsFormLocales from 'Careers/locales/careers/edit_career_profile/emba_application_questions_form-en.json';
import personalSummaryFormLocales from 'Careers/locales/careers/edit_career_profile/personal_summary_form-en.json';
import writeTextAboutLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/write_text_about-en.json';

setSpecLocales(
    editCareerProfileLocales,
    embaApplicationQuestionsFormLocales,
    personalSummaryFormLocales,
    writeTextAboutLocales,
);

describe('FrontRoyal.Careers.EditCareerProfile.shortAnswerFieldsSection', () => {
    let $injector;
    let SpecHelper;
    let scope;
    let elem;
    let CareerProfile;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');

                $injector.get('CareerProfileFixtures');
            },
        ]);
    });

    function render(opts = {}) {
        const renderer = SpecHelper.renderer();
        renderer.scope.step = {
            stepName: 'emba_application_questions',
            requiredFields: {
                short_answer_why_pursuing: true,
                short_answer_greatest_achievement: true,
                short_answer_ask_professional_advice: true,
            },
        };
        renderer.scope.modelProps = [
            'short_answer_why_pursuing',
            'short_answer_greatest_achievement',
            'short_answer_ask_professional_advice',
            'anything_else_to_tell_us',
        ];
        renderer.scope.careerProfile = CareerProfile.fixtures.getInstance();
        renderer.scope.fieldOverrides = opts.fieldOverrides || {
            anything_else_to_tell_us: {
                maxCharLength: 500,
                localeKey: 'careers.edit_career_profile.personal_summary_form.anything_else_to_tell_us',
            },
        };
        renderer.render(
            '<short-answer-fields-section step="step" model-props="modelProps" career-profile="careerProfile" field-overrides="fieldOverrides"></short-answer-fields-section>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$digest();
    }

    it('should render a working write-text-about directive for each model prop', () => {
        render();
        SpecHelper.expectElements(elem, 'write-text-about', scope.modelProps.length);

        expect(scope.careerProfile.short_answer_why_pursuing).not.toEqual('foo');
        SpecHelper.updateTextArea(elem, 'write-text-about:eq(0) textarea', 'foo');
        expect(scope.careerProfile.short_answer_why_pursuing).toEqual('foo');

        expect(scope.careerProfile.short_answer_greatest_achievement).not.toEqual('bar');
        SpecHelper.updateTextArea(elem, 'write-text-about:eq(1) textarea', 'bar');
        expect(scope.careerProfile.short_answer_greatest_achievement).toEqual('bar');

        expect(scope.careerProfile.short_answer_ask_professional_advice).not.toEqual('baz');
        SpecHelper.updateTextArea(elem, 'write-text-about:eq(2) textarea', 'baz');
        expect(scope.careerProfile.short_answer_ask_professional_advice).toEqual('baz');

        expect(scope.careerProfile.anything_else_to_tell_us).not.toEqual('qux');
        SpecHelper.updateTextArea(elem, 'write-text-about:eq(3) textarea', 'qux');
        expect(scope.careerProfile.anything_else_to_tell_us).toEqual('qux');
    });

    it('should respect fieldOverrides (except modelProp and isRequired)', () => {
        render({
            fieldOverrides: {
                anything_else_to_tell_us: {
                    modelProp: 'foo', // should NOT be respected
                    isRequired: true, // should NOT be respected
                    maxCharLength: 600, // should be respected
                    localeKey:
                        'careers.edit_career_profile.emba_application_questions_form.short_answer_greatest_achievement', // should be respected
                },
            },
        });
        expect(scope.shortAnswerFieldConfigs).toEqual([
            {
                $$hashKey: expect.anything(),
                modelProp: 'short_answer_why_pursuing',
                isRequired: true,
                maxCharLength: 350,
                localeKey: 'careers.edit_career_profile.emba_application_questions_form.short_answer_why_pursuing',
            },
            {
                $$hashKey: expect.anything(),
                modelProp: 'short_answer_greatest_achievement',
                isRequired: true,
                maxCharLength: 350,
                localeKey:
                    'careers.edit_career_profile.emba_application_questions_form.short_answer_greatest_achievement',
            },
            {
                $$hashKey: expect.anything(),
                modelProp: 'short_answer_ask_professional_advice',
                isRequired: true,
                maxCharLength: 350,
                localeKey:
                    'careers.edit_career_profile.emba_application_questions_form.short_answer_ask_professional_advice',
            },
            {
                $$hashKey: expect.anything(),
                modelProp: 'anything_else_to_tell_us',
                isRequired: false, // isRequired is derived from the requiredFields on the step, so trying to override it should fail
                maxCharLength: 600,
                localeKey:
                    'careers.edit_career_profile.emba_application_questions_form.short_answer_greatest_achievement',
            },
        ]);
    });
});
