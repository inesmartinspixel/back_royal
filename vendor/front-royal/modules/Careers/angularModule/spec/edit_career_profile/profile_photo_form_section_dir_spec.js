import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Users/angularModule/spec/_mock/fixtures/users';
import setSpecLocales from 'Translation/setSpecLocales';
import profilePhotoFormSectionLocales from 'Careers/locales/careers/edit_career_profile/profile_photo_form_section-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import uploadAvatarLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/upload_avatar-en.json';

setSpecLocales(editCareerProfileLocales);
setSpecLocales(profilePhotoFormSectionLocales);
setSpecLocales(uploadAvatarLocales);

describe('FrontRoyal.Careers.EditCareerProfile.ProfilePhotoFormSection', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let User;
    let user;
    let CareerProfile;
    let careerProfile;
    let TranslationHelper;
    let translationHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                User = $injector.get('User');
                CareerProfile = $injector.get('CareerProfile');
                TranslationHelper = $injector.get('TranslationHelper');

                $injector.get('UserFixtures');
                $injector.get('CareerProfileFixtures');
            },
        ]);

        user = User.fixtures.getInstance();
        careerProfile = CareerProfile.fixtures.getInstance();
        translationHelper = new TranslationHelper('careers.edit_career_profile.profile_photo_form_section');
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.user = user;
        renderer.scope.careerProfile = careerProfile;
        renderer.render(
            '<profile-photo-form-section user="user" career-profile="careerProfile"></profile-photo-form-section>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('when CNO', () => {
        beforeEach(() => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('career_network_only');
        });

        it('should show the appropriate upload instructions', () => {
            render();
            SpecHelper.expectElementText(
                elem,
                '.form-group label + .sub-text',
                'Smile! Your photo adds personality to your career network profile.',
            );
        });

        it('should set the avatarUploadSubTextKey properly', () => {
            const localeKey = 'upload_avatar_note_alt';
            render();
            expect(scope.avatarUploadSubTextKey).toEqual(
                `careers.edit_career_profile.profile_photo_form_section.${localeKey}`,
            );
            expect(translationHelper.get(localeKey)).toEqual(
                'Your profile picture will be publicly visible to employers in our career network.',
            );
        });
    });

    describe('when !CNO', () => {
        beforeEach(() => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('not_career_network_only');
        });

        it('should show the appropriate upload instructions', () => {
            render();
            SpecHelper.expectElementText(
                elem,
                '.form-group label + .sub-text',
                'Smile! Your photo adds personality to your student and career network profile.',
            );
        });

        it('should set the avatarUploadSubTextKey properly', () => {
            const localeKey = 'upload_avatar_note';
            render();
            expect(scope.avatarUploadSubTextKey).toEqual(
                `careers.edit_career_profile.profile_photo_form_section.${localeKey}`,
            );
            expect(translationHelper.get(localeKey)).toEqual(
                'Your profile picture will be visible to classmates and employers, should you join the career network.',
            );
        });
    });
});
