import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';

import setSpecLocales from 'Translation/setSpecLocales';
import stubSpecLocale from 'Translation/stubSpecLocale';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import studentNetworkLocales from 'StudentNetwork/locales/student_network/student_network-en.json';
import fieldOptionsLocales from 'StudentNetwork/locales/student_network/field_options-en.json';
import selectedPillsLocales from 'FrontRoyalForm/locales/front_royal_form/selected_pills-en.json';
import inputConstLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/input_const_autosuggest-en.json';

stubSpecLocale('email_input.email_validation.please_use_non_relay_email');

setSpecLocales(
    editCareerProfileLocales,
    studentNetworkLocales,
    fieldOptionsLocales,
    selectedPillsLocales,
    inputConstLocales,
);

describe('FrontRoyal.Careers.EditCareerProfile.StudentNetworkSettingsForm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let user;
    let CareerProfile;
    let EditCareerProfileHelper;
    let $timeout;
    let studentNetworkSettingsForm;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'FrontRoyal.Form', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');
                EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                $timeout = $injector.get('$timeout');

                $injector.get('CareerProfileFixtures');

                SpecHelper.stubConfig();
            },
        ]);

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.steps = [
            {
                stepName: 'student_network_settings',
            },
        ];
        renderer.render('<edit-career-profile steps="steps"></edit-career-profile>');
        $timeout.flush(); // apply the scope.steps assignment
        elem = renderer.elem;
        studentNetworkSettingsForm = SpecHelper.expectElement(elem, 'student-network-settings-form');
        scope = studentNetworkSettingsForm.scope();
    }

    it('should be wired to careerProfile', () => {
        render();
        expect(scope.careerProfile).toBeDefined();
    });

    it('should call logEventForApplication when initialized', () => {
        jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});
        render();
        expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith('viewed-student_network_settings');
    });

    it('should show inputs', () => {
        render();
        SpecHelper.expectElements(
            elem,
            'input[type="checkbox"][name="student_network_looking_for"]',
            scope.lookingForOptions.length,
        );
        SpecHelper.expectElement(elem, 'pills-select[ng-model="careerProfile.student_network_interests"]');
        SpecHelper.expectElement(elem, 'select[name="pref_student_network_privacy"]');
    });

    describe('pref_student_network_privacy select', () => {
        it('should be hooked up to the careerProfile', () => {
            render();
            SpecHelper.updateSelect(elem, 'select[name="pref_student_network_privacy"]', 'hidden');
            expect(scope.careerProfile.pref_student_network_privacy).toEqual('hidden');
            expect(scope.currentUser.pref_student_network_privacy).not.toEqual('hidden');
        });
    });

    describe('student-profile-list-card', () => {
        it('should disappear when privacy is set to hidden', () => {
            render();
            SpecHelper.expectElement(elem, 'student-profile-list-card');
            scope.careerProfile.pref_student_network_privacy = 'hidden';
            scope.$apply();
            SpecHelper.expectNoElement(elem, 'student-profile-list-card');
        });
    });

    describe('student_network_email input', () => {
        beforeEach(() => {
            jest.spyOn(user, 'hasStudentNetworkAccess', 'get').mockReturnValue(true);
            jest.spyOn(user, 'isEmailValidForStudentNetwork', 'get').mockReturnValue(false);
            user.career_profile.pref_student_network_privacy = 'full';
        });

        it('should present itself when the user is network-enabled with a non-hidden profile and without a valid primary email', () => {
            render();
            SpecHelper.expectElement(elem, 'input[name="student_network_email"]');
        });

        it('should be hidden when there is a valid primary email', () => {
            jest.spyOn(user, 'isEmailValidForStudentNetwork', 'get').mockReturnValue(true);
            render();
            SpecHelper.expectNoElement(elem, 'input[name="student_network_email"]');
        });

        it('should be hidden when not network-enabled', () => {
            jest.spyOn(user, 'hasStudentNetworkAccess', 'get').mockReturnValue(false);
            render();
            SpecHelper.expectNoElement(elem, 'input[name="student_network_email"]');
        });

        it('should be hidden when the profile is hidden or anonymous', () => {
            user.career_profile.pref_student_network_privacy = 'hidden';
            render();
            SpecHelper.expectNoElement(elem, 'input[name="student_network_email"]');
            user.career_profile.pref_student_network_privacy = 'anonymous';
            scope.$digest();
            SpecHelper.expectNoElement(elem, 'input[name="student_network_email"]');
        });

        it('should present when it is not explicitly required, but user has an existing value', () => {
            jest.spyOn(user, 'isEmailValidForStudentNetwork', 'get').mockReturnValue(true);
            user.student_network_email = 'already-populated@valid-domain.com';
            render();
            SpecHelper.expectElement(elem, 'input[name="student_network_email"]');
        });

        it('should be hooked up to the careerProfile', () => {
            jest.spyOn(user, 'hasStudentNetworkAccess', 'get').mockReturnValue(true);
            jest.spyOn(user, 'isEmailValidForStudentNetwork', 'get').mockReturnValue(false);
            user.career_profile.pref_student_network_privacy = 'full';
            render();
            SpecHelper.updateInput(elem, 'input[name="student_network_email"]', 'non-anon-email@valid-domain.com');
            expect(scope.careerProfile.student_network_email).toEqual('non-anon-email@valid-domain.com');
            expect(scope.currentUser.student_network_email).not.toEqual('non-anon-email@valid-domain.com');
        });

        it('should be be invalid if providing a known blacklist email address', () => {
            jest.spyOn(user, 'hasStudentNetworkAccess', 'get').mockReturnValue(true);
            jest.spyOn(user, 'isEmailValidForStudentNetwork', 'get').mockReturnValue(false);
            user.career_profile.pref_student_network_privacy = 'full';
            render();

            SpecHelper.expectNoElement(elem, 'input[name="student_network_email"].ng-dirty.ng-invalid');
            SpecHelper.updateInput(elem, 'input[name="student_network_email"]', 'anon-email@privaterelay.appleid.com');
            SpecHelper.expectElement(elem, 'input[name="student_network_email"].ng-dirty.ng-invalid');
        });
    });

    it('should display message if user hasStudentNetworkAccess and pref_student_network_privacy is set to full', () => {
        jest.spyOn(user, 'hasStudentNetworkAccess', 'get').mockReturnValue(true);
        user.career_profile.pref_student_network_privacy = 'full';
        render();
        SpecHelper.expectElement(elem, '#full-profile-visible-message');
        scope.careerProfile.pref_student_network_privacy = 'anonymous';
        scope.$apply();
        SpecHelper.expectNoElement(elem, '#full-profile-visible-message');
    });

    describe('$setDirty', () => {
        it('should dirty the form when changing interests', () => {
            render();
            jest.spyOn(scope.student_network_settings, '$setDirty').mockImplementation(() => {});
            scope.careerProfile.student_network_interests.push({
                text: 'foo',
            });
            scope.$digest();
            expect(scope.student_network_settings.$setDirty).toHaveBeenCalledWith(true);
        });

        it('should not dirty the form is interests are the same as initialInterests', () => {
            render();
            jest.spyOn(scope.student_network_settings, '$setDirty').mockImplementation(() => {});
            scope.careerProfile.student_network_interests.push({
                text: 'foo',
            });
            scope.$digest();
            scope.careerProfile.student_network_interests.pop();
            scope.$digest();
            expect(scope.student_network_settings.$setDirty).toHaveBeenCalledTimes(1); // the second time we manipulate interests, it shouldn't call it
        });
    });
});
