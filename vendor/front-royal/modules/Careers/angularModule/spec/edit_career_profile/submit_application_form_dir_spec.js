import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import setSpecLocales from 'Translation/setSpecLocales';
import submitApplicationFormLocales from 'Careers/locales/careers/edit_career_profile/submit_application_form-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import jobPreferencesFormLocales from 'Careers/locales/careers/edit_career_profile/job_preferences_form-en.json';
import thanksForApplyingLocales from 'Careers/locales/careers/thanks_for_applying_modal-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';

setSpecLocales(
    submitApplicationFormLocales,
    jobPreferencesFormLocales,
    editCareerProfileLocales,
    thanksForApplyingLocales,
    fieldOptionsLocales,
    candidateListCardLocales,
);

describe('FrontRoyal.Careers.EditCareerProfile.SubmitApplicationForm', () => {
    let $injector;
    let $q;
    let SpecHelper;
    let renderer;
    let elem;
    let editCareerProfileElem;
    let scope;
    let StudentDashboard;
    let Cohort;
    let CareerProfile;
    let user;
    let DialogModal;
    let ClientStorage;
    let EditCareerProfileHelper;
    let CohortApplication;
    let ngToast;
    let $timeout;
    let User;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                $q = $injector.get('$q');
                SpecHelper = $injector.get('SpecHelper');
                StudentDashboard = $injector.get('StudentDashboard');
                Cohort = $injector.get('Cohort');
                CareerProfile = $injector.get('CareerProfile');
                User = $injector.get('User');
                DialogModal = $injector.get('DialogModal');
                ClientStorage = $injector.get('ClientStorage');
                CohortApplication = $injector.get('CohortApplication');
                EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                ngToast = $injector.get('ngToast');
                $timeout = $injector.get('$timeout');

                $injector.get('CohortFixtures');
                $injector.get('CohortApplicationFixtures');
                $injector.get('CareerProfileFixtures');

                SpecHelper.stubConfig();
                SpecHelper.stubEventLogging();
            },
        ]);

        user = SpecHelper.stubCurrentUser();
        user.relevant_cohort = Cohort.fixtures.getInstance();
        user.career_profile = CareerProfile.fixtures.getInstance();
    });

    function render() {
        StudentDashboard.expect('index').toBeCalledWith({
            get_has_available_incomplete_streams: true,
            get_has_available_incomplete_playlists: true,
            user_id: user.id,
            filters: {},
        });

        renderer = SpecHelper.renderer();
        renderer.scope.steps = [
            {
                stepName: 'submit_application',
            },
        ];
        renderer.scope.stepsProgressMap = {
            basic_info: 'incomplete',
            submit_application: 'incomplete',
        };
        renderer.scope.goToStep = jest.fn();
        renderer.render(
            '<edit-career-profile steps="steps" steps-progress-map="stepsProgressMap" go-to-step="gotToStep($$index)"></edit-career-profile>',
        );
        $timeout.flush(); // apply the scope.steps assignment
        editCareerProfileElem = renderer.elem;
        // To get access to the functions defined on the submitApplicationForm directive scope (for spies etc.)
        // you have to get the scope on the submit-application-form element; not edit-career-profile.
        elem = SpecHelper.expectElement(editCareerProfileElem, 'submit-application-form');
        scope = elem.scope();
        scope.$apply();
    }

    it('should be wired to careerProfile', () => {
        render();
        expect(scope.careerProfile).toBeDefined();
    });

    it('should call logEventForApplication when initialized', () => {
        jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});
        render();
        expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith('viewed-submit_application');
    });

    it('should just show toast if pending application already exists', () => {
        const save = jest.fn();
        Object.defineProperty(user, 'cohort_applications', {
            value: [
                {
                    cohort_id: 9999,
                    status: 'pending',
                    save,
                },
            ],
        });

        render();

        CareerProfile.expect('update');
        jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
        jest.spyOn(ngToast, 'create').mockImplementation(() => {});
        scope.applicationComplete = true;
        scope.$digest();

        SpecHelper.click(elem, '[name="submit-application"]');
        CareerProfile.flush('update');

        expect(save).not.toHaveBeenCalled();
        $timeout.flush();
        expect(ngToast.create).toHaveBeenCalledWith({
            content: 'Application Updated!',
            className: 'success',
        });
    });

    it('should show message if application is not complete', () => {
        render();
        jest.spyOn(scope, 'applicationCompleteWithoutConsiderEarlyDecision', 'get').mockReturnValue(false);
        scope.$digest();
        SpecHelper.expectElement(elem, '.not-complete-message');
    });

    it('should list incomplete steps and navigate to step when clicked', () => {
        render();
        jest.spyOn(scope, 'applicationCompleteWithoutConsiderEarlyDecision', 'get').mockReturnValue(false);
        scope.$digest();

        jest.spyOn(scope, 'goToStep').mockImplementation(() => {});

        SpecHelper.expectElement(elem, '.incomplete-steps');
        SpecHelper.expectElements(elem, '.incomplete-steps li', 1); // don't expect submit_application step
        SpecHelper.click(elem, '.incomplete-steps li');
        expect(scope.goToStep).toHaveBeenCalledWith({
            $$index: 0,
        });
    });

    it("should show the user's candidate card if applicationCompleteWithoutConsiderEarlyDecision and user hasEditCareerProfileAccess", () => {
        jest.spyOn(user, 'hasEditCareerProfileAccess', 'get').mockReturnValue(true);
        render();
        jest.spyOn(scope, 'applicationCompleteWithoutConsiderEarlyDecision', 'get').mockReturnValue(true);
        scope.$digest();
        SpecHelper.expectElement(elem, 'candidate-list-card');
    });

    it("should not show the user's candidate card if !applicationCompleteWithoutConsiderEarlyDecision", () => {
        render();
        jest.spyOn(scope, 'applicationCompleteWithoutConsiderEarlyDecision', 'get').mockReturnValue(false);
        scope.$digest();
        SpecHelper.expectNoElement(elem, 'candidate-list-card');
    });

    it("should not show the user's candidate card if profile complete but can_edit_career_profile is false", () => {
        Object.defineProperty(user, 'can_edit_career_profile', {
            value: false,
        });
        render();
        scope.applicationComplete = true;
        scope.$digest();
        SpecHelper.expectNoElement(elem, 'candidate-list-card');
    });

    it('should set reapplyingOrEditingApplication on the user to false when submitted', () => {
        render();
        const reapplyingOrEditingApplicationSetterSpy = jest.spyOn(
            scope.currentUser,
            'reapplyingOrEditingApplication',
            'set',
        );

        scope.applicationComplete = true;
        scope.$digest();

        const deferred = $q.defer();
        jest.spyOn(scope.currentUser, 'applyToCohort').mockReturnValue(deferred.promise);
        jest.spyOn(ClientStorage, 'setItem').mockImplementation(() => {});
        CareerProfile.expect('update');

        SpecHelper.click(elem, '[name="submit-application"]');
        CareerProfile.flush('update');

        expect(scope.currentUser.applyToCohort).toHaveBeenCalled();

        deferred.resolve();
        scope.$digest();

        expect(reapplyingOrEditingApplicationSetterSpy).toHaveBeenCalledWith(false);
    });

    it('should show the correct blurb when not career_network_only', () => {
        Object.defineProperty(user, 'programType', {
            value: 'mba',
        });
        render();

        jest.spyOn(scope, 'applicationCompleteWithoutConsiderEarlyDecision', 'get').mockReturnValue(true);
        scope.$digest();

        SpecHelper.expectElementText(
            elem,
            '[name="opening-blurb"]',
            "Your profile card is only visible to our Admissions team. If you're accepted, it may be seen by classmates, alumni and potential employers if you opt in to our career network.",
        );
    });

    it('should setPristine on currentForm after the career profile has been saved but before route changes', () => {
        CohortApplication.expect('save');
        render();
        jest.spyOn(scope.currentForm, '$setPristine').mockImplementation(() => {});
        jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
        CareerProfile.expect('save');
        scope.submitApplication();
        expect(scope.currentForm.$setPristine).not.toHaveBeenCalled();
        expect(scope.loadRoute).not.toHaveBeenCalled();
        CareerProfile.flush('save');
        expect(scope.currentForm.$setPristine).toHaveBeenCalled();
        expect(scope.loadRoute).not.toHaveBeenCalled();
        CohortApplication.flush('save');
        expect(scope.loadRoute).toHaveBeenCalled();
    });

    _.each(['mba', 'career_network_only', 'the_business_certificate'], programType => {
        it(`should not set consider_early_decision to false upon application submission for ${programType}`, () => {
            Object.defineProperty(user, 'programType', {
                value: programType,
            });
            render();

            jest.spyOn(scope, 'lastAdmissionRoundDeadlineHasPassed', 'get').mockReturnValue(true);
            jest.spyOn(scope.currentUser, 'applyToCohort').mockReturnValue($q.when());
            scope.$digest();

            CareerProfile.expect('save').toBeCalledWith(scope.careerProfile);
            expect(scope.careerProfile.consider_early_decision).toBe(false);
            scope.submitApplication();
            CareerProfile.flush('save');
        });
    });

    describe('MBA Application', () => {
        beforeEach(() => {
            Object.defineProperty(user, 'programType', {
                value: 'mba',
            });
        });

        it('should show should appropriate messages for MBA applications', () => {
            jest.spyOn(user.relevant_cohort, 'getApplicationDeadlineMessage').mockReturnValue(
                'A test deadline message.',
            );
            render();
            scope.applicationComplete = true;
            scope.$digest();
            SpecHelper.expectElementText(
                elem,
                '#main-message-1',
                "That's it! Just use the button below to submit your application for review.",
            );
            SpecHelper.expectElementText(elem, '#application-deadline-message', 'A test deadline message.');
            SpecHelper.expectElementText(elem, '#main-message-2', 'We hope to see you as part of the next MBA class!');
        });

        it('should submit application and disable button when pressed, then show modal and navigate to mba section', () => {
            render();

            scope.applicationComplete = true;
            scope.$digest();

            const deferred = $q.defer();
            jest.spyOn(scope, 'submitApplication');
            jest.spyOn(scope.currentUser, 'applyToCohort').mockReturnValue(deferred.promise);
            expect(scope.submitting).toBeFalsy();
            CareerProfile.expect('update');

            SpecHelper.click(elem, '[name="submit-application"]');
            CareerProfile.flush('update');

            expect(scope.submitApplication).toHaveBeenCalled();
            expect(scope.submitting).toBe(true);
            SpecHelper.expectElementDisabled(elem, '[name="submit-application"]');
            expect(scope.currentUser.applyToCohort).toHaveBeenCalled();

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});

            deferred.resolve();
            scope.$digest();

            expect(scope.submitting).toBe(false);
            expect(DialogModal.alert).toHaveBeenCalledWith({
                content: '<thanks-for-applying-modal></thanks-for-applying-modal>',
                title: 'Thank you for applying to Quantic',
                size: 'small',
                scope: {},
            });

            // FIXME: The spy gets destroyed on the scope.$digest that is required for deferred.resolve
            // so I'm not sure what to do here.
            // expect(scope.loadRoute).toHaveBeenCalledWith('/dashboard');
        });
    });

    describe('EMBA Application', () => {
        beforeEach(() => {
            Object.defineProperty(user, 'programType', {
                value: 'emba',
            });
        });

        it('should show should appropriate messages for EMBA applications', () => {
            jest.spyOn(user.relevant_cohort, 'getApplicationDeadlineMessage').mockReturnValue(
                'A test deadline message.',
            );
            render();
            scope.applicationComplete = true;
            scope.$digest();
            SpecHelper.expectElementText(
                elem,
                '#main-message-1',
                "That's it! Just use the button below to submit your application for review.",
            );
            SpecHelper.expectElementText(elem, '#application-deadline-message', 'A test deadline message.');
            SpecHelper.expectElementText(
                elem,
                '#main-message-2',
                'We hope to see you as part of the next Executive MBA class!',
            );
        });

        it('should submit application when button is pressed and then show a modal', () => {
            render();

            scope.applicationComplete = true;
            scope.$digest();

            const deferred = $q.defer();
            jest.spyOn(scope.currentUser, 'applyToCohort').mockReturnValue(deferred.promise);
            CareerProfile.expect('update');

            SpecHelper.click(elem, '[name="submit-application"]');
            CareerProfile.flush('update');

            expect(scope.currentUser.applyToCohort).toHaveBeenCalled();

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});

            deferred.resolve();
            scope.$digest();

            expect(DialogModal.alert).toHaveBeenCalledWith({
                content: '<thanks-for-applying-modal></thanks-for-applying-modal>',
                title: 'Thank you for applying to Quantic',
                size: 'small',
                scope: {},
            });
        });

        describe('consider_early_decision', () => {
            beforeEach(() => {
                jest.spyOn(Cohort.prototype, 'supportsEarlyDecisionConsideration', 'get').mockReturnValue(true);
            });

            it('should be set to false by default upon application submission if not already set', () => {
                render();

                jest.spyOn(scope, 'lastAdmissionRoundDeadlineHasPassed', 'get').mockReturnValue(true);
                jest.spyOn(scope.currentUser, 'applyToCohort').mockReturnValue($q.when());
                scope.$digest();

                const expected = _.extend(user.career_profile, {
                    consider_early_decision: false,
                });
                CareerProfile.expect('save').toBeCalledWith(expected);
                expect(scope.careerProfile.consider_early_decision).toBeUndefined();
                scope.submitApplication();
                CareerProfile.flush('save');
            });

            it('should not be set to false upon application submission if already set', () => {
                user.career_profile.consider_early_decision = true; // ensure consider_early_decision is already set
                render();

                jest.spyOn(scope, 'lastAdmissionRoundDeadlineHasPassed', 'get').mockReturnValue(true);
                jest.spyOn(scope.currentUser, 'applyToCohort').mockReturnValue($q.when());
                scope.$digest();

                CareerProfile.expect('save').toBeCalledWith(scope.careerProfile);
                expect(scope.careerProfile.consider_early_decision).toBe(true);
                scope.submitApplication();
                CareerProfile.flush('save');
            });

            describe('form field', () => {
                it('should be visible if application is complete', () => {
                    render();
                    jest.spyOn(scope, 'lastAdmissionRoundDeadlineHasPassed', 'get').mockReturnValue(false);
                    jest.spyOn(scope, 'applicationCompleteWithoutConsiderEarlyDecision', 'get').mockReturnValue(true);
                    scope.$digest();
                    SpecHelper.expectElement(elem, '[name="early-decision-wrapper"]');
                });

                it('should be hidden if lastAdmissionRoundDeadlineHasPassed', () => {
                    render();
                    jest.spyOn(scope, 'applicationCompleteWithoutConsiderEarlyDecision', 'get').mockReturnValue(true);
                    jest.spyOn(scope, 'lastAdmissionRoundDeadlineHasPassed', 'get').mockReturnValue(true);
                    scope.$digest();
                    SpecHelper.expectNoElement(elem, '[name="early-decision-wrapper"]');
                });
            });
        });
    });

    describe('Career Network Only Application', () => {
        beforeEach(() => {
            Object.defineProperty(user, 'programType', {
                value: 'career_network_only',
            });
        });

        it('should submit application when button is pressed, and DO NOT show a modal', () => {
            render();

            scope.applicationComplete = true;
            scope.$digest();
            CareerProfile.expect('update');

            const deferred = $q.defer();
            jest.spyOn(scope.currentUser, 'applyToCohort').mockReturnValue(deferred.promise);
            SpecHelper.click(elem, '[name="submit-application"]');
            CareerProfile.flush('update');

            expect(scope.currentUser.applyToCohort).toHaveBeenCalled();

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});

            deferred.resolve();
            scope.$digest();

            // this program type supports submitting to the status page, so we don't want the modal
            expect(DialogModal.alert).not.toHaveBeenCalled();
        });

        it('should update a previously rejected application on submit', () => {
            render();

            scope.applicationComplete = true;

            const deferred = $q.defer();
            const save = jest.fn().mockReturnValue(deferred.promise);
            const priorCohortApplication = {
                cohort_id: user.relevant_cohort.id,
                status: 'rejected',
                save,
            };
            Object.defineProperty(user, 'cohort_applications', {
                value: [priorCohortApplication],
            });

            CareerProfile.expect('update');
            scope.$digest();
            SpecHelper.click(elem, '[name="submit-application"]');
            CareerProfile.flush('update');

            expect(save).toHaveBeenCalled();

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});

            deferred.resolve();
            scope.$digest();

            expect(DialogModal.alert).toHaveBeenCalled();
        });

        it('should show the correct blurb', () => {
            render();
            jest.spyOn(scope, 'applicationCompleteWithoutConsiderEarlyDecision', 'get').mockReturnValue(true);
            scope.$digest();
            SpecHelper.expectElementText(
                elem,
                '[name="opening-blurb"]',
                'Your profile card is only visible to our Admissions team. If you’re accepted, it will be visible to potential employers recruiting in our career network.',
            );
        });
    });

    describe('Fundamentals of Business Certificate Application', () => {
        beforeEach(() => {
            Object.defineProperty(user, 'programType', {
                value: 'the_business_certificate',
            });
        });

        it('should show should appropriate messages for Fundamentals of Business Certificate applications', () => {
            jest.spyOn(user.relevant_cohort, 'getApplicationDeadlineMessage').mockReturnValue(
                'A test deadline message.',
            );
            render();
            scope.applicationComplete = true;
            scope.$digest();
            SpecHelper.expectElementText(
                elem,
                '#main-message-1',
                "That's it! Just use the button below to submit your application for review.",
            );
            SpecHelper.expectElementText(elem, '#application-deadline-message', 'A test deadline message.');
            SpecHelper.expectNoElement(elem, '#main-message-2');
        });

        it('should submit application when button is pressed, and show a modal', () => {
            render();

            scope.applicationComplete = true;
            scope.$digest();
            CareerProfile.expect('update');

            const deferred = $q.defer();
            jest.spyOn(scope.currentUser, 'applyToCohort').mockReturnValue(deferred.promise);
            SpecHelper.click(elem, '[name="submit-application"]');
            CareerProfile.flush('update');

            expect(scope.currentUser.applyToCohort).toHaveBeenCalled();

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});

            deferred.resolve();
            scope.$digest();

            expect(DialogModal.alert).toHaveBeenCalled();
        });

        it('should update a previously rejected application on submit', () => {
            render();

            scope.applicationComplete = true;

            const deferred = $q.defer();
            const save = jest.fn().mockReturnValue(deferred.promise);
            const priorCohortApplication = {
                cohort_id: user.relevant_cohort.id,
                status: 'rejected',
                save,
            };
            Object.defineProperty(user, 'cohort_applications', {
                value: [priorCohortApplication],
            });

            CareerProfile.expect('update');
            scope.$digest();
            SpecHelper.click(elem, '[name="submit-application"]');
            CareerProfile.flush('update');

            expect(save).toHaveBeenCalled();

            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});

            deferred.resolve();
            scope.$digest();

            expect(DialogModal.alert).toHaveBeenCalled();
        });
    });

    describe('when isApplicantEditor', () => {
        beforeEach(() => {
            jest.spyOn(EditCareerProfileHelper, 'isApplicantEditor').mockReturnValue(true);
        });

        // applicant editor in Manage Careers already shows preview of candidate card,
        // so showing it in this directive is unnecessary
        it("should not show preview of user's candidate card", () => {
            render();
            SpecHelper.expectNoElement(elem, '.preview-row');
            SpecHelper.expectNoElement(elem, 'candidate-card-preview');
        });

        it('should not show main-message even if applicationComplete', () => {
            const ProfileCompletionHelper = $injector.get('ProfileCompletionHelper');
            jest.spyOn(ProfileCompletionHelper.prototype, 'getPercentComplete').mockReturnValue(100); // determines if applicationComplete
            render();
            expect(ProfileCompletionHelper.prototype.getPercentComplete).toHaveBeenCalledWith(
                scope.currentUser.career_profile,
            );
            expect(scope.applicationComplete).toBe(true);
            SpecHelper.expectNoElement(elem, '.main-message');
        });

        it('should show consider_early_decision field if supportsEarlyDecisionConsideration', () => {
            jest.spyOn(EditCareerProfileHelper, 'supportsEarlyDecisionConsideration').mockReturnValue(true);
            render();
            expect(EditCareerProfileHelper.supportsEarlyDecisionConsideration).toHaveBeenCalledWith(scope.currentUser);
            SpecHelper.expectElements(elem, 'input[ng-model="careerProfile.consider_early_decision"]', 2);
            SpecHelper.expectElementText(
                elem,
                'input[ng-model="careerProfile.consider_early_decision"]:eq(0) + span',
                'Yes',
            );
            SpecHelper.expectElementText(
                elem,
                'input[ng-model="careerProfile.consider_early_decision"]:eq(1) + span',
                'No',
            );
        });
    });

    describe('submitApplication', () => {
        describe('before submitting MBA application modal', () => {
            let hasPendingOrPreAcceptedCohortApplicationSpy;
            let hasAppliedToRelevantCohortSpy;

            beforeEach(() => {
                render();
                scope.careerProfile.program_type = 'mba';
                scope.careerProfile.birthdate = '1980-12-01'; // older than 32
                scope.currentUser.has_seen_mba_submit_popup = false;
                hasPendingOrPreAcceptedCohortApplicationSpy = jest
                    .spyOn(scope.currentUser, 'hasPendingOrPreAcceptedCohortApplication', 'get')
                    .mockReturnValue(false);
                hasAppliedToRelevantCohortSpy = jest
                    .spyOn(scope, 'hasAppliedToRelevantCohort', 'get')
                    .mockReturnValue(false);
            });

            it('should display under certain conditions', () => {
                // assert that it interrupts sumitting with a dialog modal
                jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
                scope.submitApplication();
                expect(DialogModal.alert).toHaveBeenCalled();

                DialogModal.alert.mockClear();
                // the rest of this spec makes sure that the modal doesn't interrupt the submission
                scope.careerProfile.program_type = 'emba';
                assertApplicationSubmittedWithoutModal();

                scope.careerProfile.program_type = 'mba';
                scope.careerProfile.birthdate = '2000-12-01'; // younger than 32
                assertApplicationSubmittedWithoutModal();

                scope.careerProfile.birthdate = '1980-12-01';
                scope.currentUser.has_seen_mba_submit_popup = true;
                assertApplicationSubmittedWithoutModal();

                scope.currentUser.has_seen_mba_submit_popup = false;
                hasPendingOrPreAcceptedCohortApplicationSpy.mockReturnValue(true);
                assertApplicationSubmittedWithoutModal();

                hasPendingOrPreAcceptedCohortApplicationSpy.mockReturnValue(false);
                hasAppliedToRelevantCohortSpy.mockReturnValue(true);
                jest.spyOn(scope, 'previousApplicationToRelevantCohort', 'get').mockReturnValue(
                    CohortApplication.fixtures.getInstance(),
                );
                CohortApplication.expect('update');
                assertApplicationSubmittedWithoutModal();
                CohortApplication.flush('update');
            });

            it('handle EMBA selection by setting the flag, logging an event, and dispatching an event for form redirection', () => {
                scope.submitApplication();
                $timeout.flush();
                const modalElem = $('dialog-modal-alert');

                jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});

                CareerProfile.expect('update');
                SpecHelper.click(modalElem, 'button.flat', 0);
                expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith(
                    'emba_upsell_modal_selection',
                    true,
                    {
                        selection_details: 'emba',
                    },
                );

                expect(scope.careerProfile.program_type).toEqual('emba');

                jest.spyOn(scope, '$emit').mockImplementation(() => {});
                jest.spyOn(scope, 'submitApplication').mockImplementation(() => {});
                User.expect('update');
                CareerProfile.flush('update');

                expect(scope.currentUser.has_seen_mba_submit_popup).toBe(true);
                expect(scope.$emit).toHaveBeenCalledWith(
                    'savedCareerProfileWithStepChange',
                    'emba_application_questions',
                );

                User.flush('update');
                expect(scope.submitApplication).not.toHaveBeenCalled();
            });

            it('should handle MBA selection by setting the flag, logging an event, and submitting the application', () => {
                scope.submitApplication();
                $timeout.flush();
                const modalElem = $('dialog-modal-alert');
                jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});

                jest.spyOn(scope, '$emit').mockImplementation(() => {});
                jest.spyOn(scope, 'submitApplication').mockImplementation(() => {});
                User.expect('update');

                SpecHelper.click(modalElem, 'button.flat', 1);
                expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith(
                    'emba_upsell_modal_selection',
                    true,
                    {
                        selection_details: 'mba',
                    },
                );

                expect(scope.careerProfile.program_type).toEqual('mba');
                expect(scope.currentUser.has_seen_mba_submit_popup).toBe(true);
                expect(scope.$emit).not.toHaveBeenCalled();

                User.flush('update');
                expect(scope.submitApplication).toHaveBeenCalled();
            });

            function assertApplicationSubmittedWithoutModal() {
                CareerProfile.expect('update');
                CohortApplication.expect('create');
                scope.submitApplication();
                CareerProfile.flush('update');
                expect(DialogModal.alert).not.toHaveBeenCalled();
                DialogModal.alert.mockClear();
            }
        });
    });

    describe('changeProgramToEmba', () => {
        it('should change programs and save user', () => {
            render();
            CareerProfile.expect('update');
            User.expect('update');
            jest.spyOn(DialogModal, 'hideAlerts').mockImplementation(() => {});
            jest.spyOn(scope, '$emit').mockImplementation(() => {});

            scope.changeProgramToEmba();

            CareerProfile.flush('update');
            User.flush('update');
            expect(scope.$emit).toHaveBeenCalledWith('savedCareerProfileWithStepChange', 'emba_application_questions');
            expect(DialogModal.hideAlerts).toHaveBeenCalled();
        });
    });

    describe('keepProgramSubmitApplication', () => {
        it('should save user and submit application', () => {
            render();
            User.expect('update');
            jest.spyOn(DialogModal, 'hideAlerts').mockImplementation(() => {});
            jest.spyOn(scope, 'submitApplication').mockImplementation(() => {});

            scope.keepProgramSubmitApplication();

            User.flush('update');
            expect(DialogModal.hideAlerts).toHaveBeenCalled();
            expect(scope.submitApplication).toHaveBeenCalled();
        });
    });
});
