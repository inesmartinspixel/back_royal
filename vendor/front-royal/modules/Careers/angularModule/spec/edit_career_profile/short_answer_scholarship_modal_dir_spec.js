import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import setSpecLocales from 'Translation/setSpecLocales';
import shortAnswerScholarshipModalLocales from 'Careers/locales/careers/edit_career_profile/short_answer_scholarship_modal-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import writeTextAboutLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/write_text_about-en.json';

setSpecLocales(editCareerProfileLocales, shortAnswerScholarshipModalLocales, writeTextAboutLocales);

describe('FrontRoyal.Careers.EditCareerProfile.ShortAnswerScholarshipModal', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let CareerProfile;
    let careerProfile;
    let onSave;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');

                $injector.get('CareerProfileFixtures');
            },
        ]);

        careerProfile = CareerProfile.fixtures.getInstance();
        onSave = jest.fn();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.careerProfile = careerProfile;
        renderer.scope.onSave = onSave;
        renderer.render(
            '<short-answer-scholarship-modal career-profile="careerProfile" on-save="onSave"></short-answer-scholarship-modal>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('shortAnswerScholarship form', () => {
        it('should have headline with message', () => {
            render();
            SpecHelper.expectElementText(
                elem,
                'form[name="shortAnswerScholarship"] .headline',
                'Thank You For Updating Your Application',
            );
            SpecHelper.expectElementText(
                elem,
                'form[name="shortAnswerScholarship"] .message:eq(0)',
                'Executive MBA applicants are eligible for scholarships based on merit and need. Please share any circumstances that you would like us to consider.',
            );
            SpecHelper.expectElementText(
                elem,
                'form[name="shortAnswerScholarship"] .message:eq(1)',
                'Scholarship decisions are given at the same time as your admission decision.',
            );
        });

        it('should have required short-answer-scholarship-field wired to careerProfile', () => {
            render();
            SpecHelper.expectElement(
                elem,
                'form[name="shortAnswerScholarship"] short-answer-scholarship-field[career-profile="careerProfile"][is-required="true"]',
            );
        });

        it('should handle disabling/enabling of save button if form is $invalid or not', () => {
            render();
            scope.shortAnswerScholarship.$invalid = true;
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, 'form[name="shortAnswerScholarship"] button[name="save"]');

            scope.shortAnswerScholarship.$invalid = false;
            scope.$digest();
            SpecHelper.expectElementEnabled(elem, 'form[name="shortAnswerScholarship"] button[name="save"]');
        });

        describe('when valid', () => {
            beforeEach(() => {
                render();
                scope.shortAnswerScholarship.$invalid = false; // make the form valid so we can click the save button
                scope.$digest();
            });

            it('should call scope.save handler when save button is clicked', () => {
                const saveSpy = jest.spyOn(scope, 'save').mockImplementation(() => {});
                SpecHelper.click(elem, 'form[name="shortAnswerScholarship"] button[name="save"]');
                expect(saveSpy).toHaveBeenCalled();
            });
        });
    });

    describe('save', () => {
        it('should do nothing if shortAnswerScholarship form is $invalid', () => {
            render();
            scope.shortAnswerScholarship.$invalid = true;
            scope.save();
            expect(scope.savedShortAnswerScholarship).toBe(false);
            expect(onSave).not.toHaveBeenCalled();
        });

        it('should set savedShortAnswerScholarship flag and call onSave when shortAnswerScholarship is valid', () => {
            render();
            scope.shortAnswerScholarship.$invalid = false;
            scope.save();
            expect(scope.savedShortAnswerScholarship).toBe(true);
            expect(onSave).toHaveBeenCalled();
        });
    });

    describe('$on destroy', () => {
        it('should unset careerProfile.short_answer_scholarship if user has not savedShortAnswerScholarship', () => {
            render();
            scope.careerProfile.short_answer_scholarship = 'foo';
            scope.savedShortAnswerScholarship = false;
            scope.$destroy();
            expect(scope.careerProfile.short_answer_scholarship).toBeNull();
        });

        it('should NOT unset careerProfile.short_answer_scholarship if user has savedShortAnswerScholarship', () => {
            render();
            scope.careerProfile.short_answer_scholarship = 'foo';
            scope.savedShortAnswerScholarship = true;
            scope.$destroy();
            expect(scope.careerProfile.short_answer_scholarship).toEqual('foo');
        });
    });
});
