import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import setSpecLocales from 'Translation/setSpecLocales';
import workFormLocales from 'Careers/locales/careers/edit_career_profile/work_form-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import workExperienceDetailLocales from 'Careers/locales/careers/edit_career_profile/work_experience_detail-en.json';
import salarySurveyFormSectionLocales from 'Careers/locales/careers/edit_career_profile/salary_survey_form_section-en.json';
import chooseAnItemLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/choose_an_item-en.json';
import chooseARoleLocales from 'Careers/locales/careers/choose_a_role-en.json';
import chooseADateLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/choose_a_date-en.json';
import addAnItemLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/add_an_item-en.json';
import invalidFieldsLinksLocales from 'FrontRoyalForm/locales/front_royal_form/invalid_fields_links-en.json';
import moment from 'moment-timezone';

setSpecLocales(
    editCareerProfileLocales,
    workFormLocales,
    fieldOptionsLocales,
    workExperienceDetailLocales,
    salarySurveyFormSectionLocales,
    chooseAnItemLocales,
    chooseARoleLocales,
    chooseADateLocales,
    addAnItemLocales,
    invalidFieldsLinksLocales,
);

describe('FrontRoyal.Careers.EditCareerProfile.WorkForm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let StudentDashboard;
    let user;
    let CareerProfile;
    let EditCareerProfileHelper;
    let WorkExperience;
    let $timeout;
    let parentScope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                StudentDashboard = $injector.get('StudentDashboard');
                CareerProfile = $injector.get('CareerProfile');
                EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                WorkExperience = $injector.get('WorkExperience');
                $timeout = $injector.get('$timeout');

                $injector.get('CareerProfileFixtures');
                SpecHelper.stubConfig();
            },
        ]);

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();

        SpecHelper.stubEventLogging();
    });

    function render() {
        Object.defineProperty(user, 'programType', {
            value: 'mba',
        });

        StudentDashboard.expect('index').toBeCalledWith({
            get_has_available_incomplete_streams: true,
            get_has_available_incomplete_playlists: true,
            user_id: user.id,
            filters: {},
        });

        renderer = SpecHelper.renderer();
        renderer.scope.steps = [
            {
                stepName: 'work',
                requiredFields: {
                    fullTimeWorkExperiences: {
                        min: 1,
                        requiredFields: {},
                    },
                },
            },
            {
                stepName: 'resume_and_links',
            },
        ]; // Needed so the $isLast() doesn't hide the button
        renderer.render('<edit-career-profile steps="steps"></edit-career-profile>');
        $timeout.flush(); // apply the scope.steps assignment
        elem = renderer.elem;
        parentScope = elem.isolateScope();
        const formScope = SpecHelper.expectElement(elem, 'work-form');
        scope = formScope.scope(); // get the workForm directive scope; not the editCareerProfile directive scope
        scope.$apply();
    }

    it('should be wired to careerProfile', () => {
        render();
        expect(scope.careerProfile).toBeDefined();
    });

    it('should call logEventForApplication when initialized', () => {
        jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});
        render();
        expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith('viewed-work');
    });

    it('should set hasWorkExperience requirement as $invalid if no fullTimeWorkExperiences', () => {
        render();

        const fullTimeWorkExperiencesSpy = jest
            .spyOn(scope.careerProfile, 'fullTimeWorkExperiences', 'get')
            .mockReturnValue([{}]);
        scope.$digest();
        expect(scope.work.hasWorkExperience.$invalid).toBe(false);

        fullTimeWorkExperiencesSpy.mockReturnValue([]);
        scope.$digest();
        expect(scope.work.hasWorkExperience.$invalid).toBe(true);
    });

    it('should addDummyWorkExperienceIfNecessary on initial load', () => {
        user.career_profile.work_experiences = [];
        jest.spyOn(CareerProfile.prototype, 'addDummyWorkExperienceIfNecessary');
        render();
        expect(scope.careerProfile.addDummyWorkExperienceIfNecessary).toHaveBeenCalled();
        SpecHelper.expectElements(elem, 'work-experience-detail .experience-detail', 1);
    });

    describe('Work Summary section', () => {
        it('should have a select wired to careerProfile.survey_years_full_time_experience', () => {
            render();
            const value = _.first($injector.get('CAREERS_FULL_TIME_EXPERIENCE_KEYS'));
            expect(scope.careerProfile.survey_years_full_time_experience).not.toEqual(value);
            SpecHelper.updateSelect(elem, 'select[name="years-experience"]', value);
            expect(scope.careerProfile.survey_years_full_time_experience).toEqual(value);
        });

        it('should have a select wired to careerProfile.survey_most_recent_role_description', () => {
            render();
            const value = _.first($injector.get('CAREERS_RECENT_ROLE_KEYS'));
            expect(scope.careerProfile.survey_most_recent_role_description).not.toEqual(value);
            SpecHelper.updateSelect(elem, 'select[name="recent-role"]', value);
            expect(scope.careerProfile.survey_most_recent_role_description).toEqual(value);
        });
    });

    describe('fullTimeWorkExperiences section', () => {
        it('should be marked as required', () => {
            render();
            SpecHelper.expectHasClass(elem, '[name="work-experience-title"]', 'required');
        });

        it('should apply invalid-warning class to instructions if hasWorkExperience requirement is invalid', () => {
            render();
            scope.work.hasWorkExperience.$setValidity('required', false);
            scope.$digest();
            SpecHelper.expectHasClass(elem, '[name="full_time_work_instructions"]', 'invalid-warning');
        });

        describe('workFormFullTimeWorkHistoryKey', () => {
            describe('when provide_full_time_work', () => {
                beforeEach(() => {
                    jest.spyOn(EditCareerProfileHelper, 'workFormFullTimeWorkHistoryKey').mockReturnValue(
                        'provide_full_time_work',
                    );
                });

                it('should display proper instructions', () => {
                    render();
                    SpecHelper.expectElementText(
                        elem,
                        '[name="full_time_work_instructions"]',
                        'Provide at least 6 years of work history, starting with the earliest full-time position first.',
                    );
                });
            });

            describe('when provide_full_time_work_after_university', () => {
                beforeEach(() => {
                    jest.spyOn(EditCareerProfileHelper, 'workFormFullTimeWorkHistoryKey').mockReturnValue(
                        'provide_full_time_work_after_university',
                    );
                });

                it('should display proper instructions', () => {
                    render();
                    SpecHelper.expectElementText(
                        elem,
                        '[name="full_time_work_instructions"]',
                        'Provide your full-time work history starting with your first position after university. Less experienced applicants may include full-time internships here.',
                    );
                });
            });
        });
    });

    describe('partTimeWorkExperiences section', () => {
        it('should not be marked as required', () => {
            render();
            SpecHelper.expectDoesNotHaveClass(elem, '[name="other-experience-title"]', 'required');
        });

        it('should not apply invalid-warning class to instructions if hasWorkExperience requirement is invalid', () => {
            render();
            scope.work.hasWorkExperience.$setValidity('required', false);
            scope.$digest();
            SpecHelper.expectDoesNotHaveClass(elem, '[name="part_time_work_instructions"]', 'invalid-warning');
        });

        describe('workFormPartTimeWorkHistoryKey', () => {
            describe('when list_part_time_work', () => {
                beforeEach(() => {
                    jest.spyOn(EditCareerProfileHelper, 'workFormPartTimeWorkHistoryKey').mockReturnValue(
                        'list_part_time_work',
                    );
                });

                it('should display proper instructions', () => {
                    render();
                    SpecHelper.expectElementText(
                        elem,
                        '[name="part_time_work_instructions"]',
                        'List significant non-full time experiences, like board positions or volunteer activities.',
                    );
                });
            });

            describe('when list_part_time_work_students_and_graduates', () => {
                beforeEach(() => {
                    jest.spyOn(EditCareerProfileHelper, 'workFormPartTimeWorkHistoryKey').mockReturnValue(
                        'list_part_time_work_students_and_graduates',
                    );
                });

                it('should display proper instructions', () => {
                    render();
                    SpecHelper.expectElementText(
                        elem,
                        '[name="part_time_work_instructions"]',
                        'List significant non-full time experiences, like board positions or volunteer activities. For students and recent graduates, list your most significant campus activities here.',
                    );
                });
            });
        });
    });

    describe('featuredWorkExperience', () => {
        it('should be visible if the user has multiple currentFullTimeWorkExperiences', () => {
            render();
            const currentFullTimeWorkExperiencesSpy = jest
                .spyOn(scope.careerProfile, 'currentFullTimeWorkExperiences', 'get')
                .mockReturnValue([]);
            scope.$digest();
            SpecHelper.expectNoElement(elem, '[name="featured-work-experience"]');

            // mock currentFullTimeWorkExperiences to return an array of 1 current full-time work experience
            currentFullTimeWorkExperiencesSpy.mockReturnValue([{}]);
            scope.$digest();
            SpecHelper.expectNoElement(elem, '[name="featured-work-experience"]');

            // mock currentFullTimeWorkExperiences to return an array multiple current full-time work experiences
            currentFullTimeWorkExperiencesSpy.mockReturnValue([{}, {}]);
            scope.$digest();
            SpecHelper.expectElement(elem, '[name="featured-work-experience"]');
        });

        it('should not be visible if the user has less than 2 currentFullTimeWorkExperiences', () => {
            render();
            // mock currentFullTimeWorkExperiences to return an array of multiple current full-time work experiences
            const currentFullTimeWorkExperiencesSpy = jest
                .spyOn(scope.careerProfile, 'currentFullTimeWorkExperiences', 'get')
                .mockReturnValue([{}, {}]);
            scope.$digest();
            SpecHelper.expectElement(elem, '[name="featured-work-experience"]');

            // mock currentFullTimeWorkExperiences to return an array of just 1 current full-time work experience
            currentFullTimeWorkExperiencesSpy.mockReturnValue([{}]);
            scope.$digest();
            SpecHelper.expectNoElement(elem, '[name="featured-work-experience"]');
        });

        it('should be set in the background is user has less than 2 currentFullTimeWorkExperiences', () => {
            const workExperiences = user.career_profile.work_experiences;
            _.extend(workExperiences[0], {
                end_date: null, // make this experience current
            });
            assertSettingFeaturedWorkExperienceInBackground(workExperiences);
        });

        it('should not be set in the background if the user can manually set it themselves', () => {
            const workExperiences = user.career_profile.work_experiences;
            _.extend(workExperiences[0], {
                end_date: null, // make this experience current
            });
            let newMostRecentWorkExperience = WorkExperience.new({
                professional_organization: {
                    text: 'Baz Inc.',
                },
                job_title: 'Something different',
                start_date: workExperiences[0] + 1000, // rails will send us back a timestamp in seconds
                end_date: null, // set it to be current
                featured: false,
                responsibilities: ['foo', 'bar', 'baz'],
            });
            addWorkExperiencesUntilSelectIsVisible(workExperiences, newMostRecentWorkExperience);
            const expectedFeaturedExperience = _.extend(newMostRecentWorkExperience, {
                featured: true,
            });

            // add another work experience
            SpecHelper.click(elem, '[name="add-work-experience"]:eq(0)');
            expect(scope.careerProfile.work_experiences.length).toBeGreaterThan(3);
            expect(scope.careerProfile.featuredWorkExperience).toEqual(expectedFeaturedExperience); // should not have changed
            newMostRecentWorkExperience = WorkExperience.new({
                professional_organization: {
                    text: 'Baz Inc.',
                },
                job_title: 'Something different',
                start_date: newMostRecentWorkExperience + 1000, // rails will send us back a timestamp in seconds
                end_date: null,
                featured: false,
                responsibilities: ['foo', 'bar', 'baz'],
            });
            scope.careerProfile.work_experiences[3] = newMostRecentWorkExperience;
            scope.$digest();
            expect(scope.careerProfile.featuredWorkExperience).toEqual(expectedFeaturedExperience); // should not have changed
            SpecHelper.expectElement(elem, '[name="featured-work-experience"]'); // should be visible

            // select a different featured work experience from the UI
            SpecHelper.updateSelect(elem, '[name="featured-work-experience"]', newMostRecentWorkExperience);
            expect(scope.careerProfile.featuredWorkExperience).toEqual(newMostRecentWorkExperience); // should have changed
        });

        it('should be set to undefined if the user can manually set it themselves and the currently featured experience gets removed', () => {
            user.career_profile.work_experiences = [
                WorkExperience.new({
                    professional_organization: {
                        text: 'Baz Inc.',
                    },
                    job_title: 'Job #1',
                    start_date: moment().subtract(3, 'years').toDate(),
                    end_date: null,
                    featured: false,
                    responsibilities: ['foo', 'bar', 'baz'],
                }),
                WorkExperience.new({
                    professional_organization: {
                        text: 'Baz Inc.',
                    },
                    job_title: 'Job #2',
                    start_date: moment().subtract(2, 'years').toDate(),
                    end_date: null,
                    featured: false,
                    responsibilities: ['foo', 'bar', 'baz'],
                }),
                WorkExperience.new({
                    professional_organization: {
                        text: 'Baz Inc.',
                    },
                    job_title: 'Job #3',
                    start_date: moment().subtract(1, 'years').toDate(),
                    end_date: null,
                    featured: true, // this marks this as the currently featured work experience
                    responsibilities: ['foo', 'bar', 'baz'],
                }),
            ];
            render();
            scope.careerProfile.work_experiences.splice(2, 1); // remove the featuredWorkExperience
            scope.$digest();
            expect(scope.proxy.featuredWorkExperience).toBeUndefined();
        });

        it('should be set to undefined if the user can manually set it themselves and the currently featured experience becomes no longer current', () => {
            user.career_profile.work_experiences = [
                WorkExperience.new({
                    professional_organization: {
                        text: 'Baz Inc.',
                    },
                    job_title: 'Job #3',
                    start_date: moment().subtract(1, 'years').toDate() / 1000,
                    end_date: null,
                    featured: true, // this marks this as the currently featured work experience
                    responsibilities: ['foo', 'bar', 'baz'],
                }),
                WorkExperience.new({
                    professional_organization: {
                        text: 'Baz Inc.',
                    },
                    job_title: 'Job #2',
                    start_date: moment().subtract(2, 'years').toDate() / 1000,
                    end_date: null,
                    featured: false,
                    responsibilities: ['foo', 'bar', 'baz'],
                }),
                WorkExperience.new({
                    professional_organization: {
                        text: 'Baz Inc.',
                    },
                    job_title: 'Job #1',
                    start_date: moment().subtract(3, 'years').toDate() / 1000,
                    end_date: null,
                    featured: false,
                    responsibilities: ['foo', 'bar', 'baz'],
                }),
            ];
            render();
            // make the featured work experience no longer current
            scope.careerProfile.work_experiences[0].end_date = 1454284800000; // Feb 2016
            // since we're setting an end_date value on the featuredWorkExperience, the work experience is no longer
            // contained in the currentFullTimeWorkExperiences array, so this gets set to undefined as a side-effect
            scope.proxy.featuredWorkExperience = undefined;
            scope.$digest();
            expect(scope.proxy.featuredWorkExperience).toBeUndefined();
        });

        function assertSettingFeaturedWorkExperienceInBackground(workExperiences) {
            const firstWorkExperience = workExperiences[1];
            const mostRecentWorkExperience = workExperiences[0];
            user.career_profile.work_experiences = [];
            render();
            SpecHelper.expectNoElement(elem, '[name="featured-work-experience"]');

            // the profile is populated with an empty dummy experience with featured set to false,
            // and then then logic in the directive sets it to featured
            expect(scope.careerProfile.featuredWorkExperience).toBeDefined();

            // add a work experience with an end date
            scope.careerProfile.work_experiences[0] = firstWorkExperience;
            scope.$digest();
            SpecHelper.expectNoElement(elem, '[name="featured-work-experience"]');
            let expectedFeaturedExperience = _.extend(firstWorkExperience, {
                featured: true, // since this is the only experience, it should be set as the featured work experience
            });
            expect(scope.careerProfile.featuredWorkExperience).toEqual(expectedFeaturedExperience);
            expect(scope.careerProfile.currentFullTimeWorkExperiences.length).toEqual(0);

            // add another work experience
            SpecHelper.click(elem, '[name="add-work-experience"]:eq(0)');
            expect(scope.careerProfile.work_experiences.length).toBeGreaterThan(1);
            expect(scope.careerProfile.featuredWorkExperience).toEqual(expectedFeaturedExperience); // should not have changed
            scope.careerProfile.work_experiences[1] = mostRecentWorkExperience;
            scope.$digest();
            SpecHelper.expectNoElement(elem, '[name="featured-work-experience"]');
            expectedFeaturedExperience = _.extend(mostRecentWorkExperience, {
                featured: true, // this should be the featured experience since it has a more recent end_date
            });
            expect(scope.careerProfile.featuredWorkExperience).toEqual(expectedFeaturedExperience);
            expect(scope.careerProfile.currentFullTimeWorkExperiences.length).toEqual(1);
            SpecHelper.expectNoElement(elem, '[name="featured-work-experience"]');
        }

        function addWorkExperiencesUntilSelectIsVisible(workExperiences, newMostRecentWorkExperience) {
            assertSettingFeaturedWorkExperienceInBackground(workExperiences);

            // add another work experience
            SpecHelper.click(elem, '[name="add-work-experience"]:eq(0)');
            expect(scope.careerProfile.work_experiences.length).toBeGreaterThan(2);
            let expectedFeaturedExperience = _.extend(workExperiences[0], {
                featured: true,
            });
            expect(scope.careerProfile.featuredWorkExperience).toEqual(expectedFeaturedExperience); // should not have changed yet
            scope.careerProfile.work_experiences[2] = newMostRecentWorkExperience;
            scope.$digest();
            expectedFeaturedExperience = _.extend(newMostRecentWorkExperience, {
                featured: true,
            });
            expect(scope.careerProfile.featuredWorkExperience).toEqual(expectedFeaturedExperience); // should be this experience since it has the most recent end_date
            SpecHelper.expectElement(elem, '[name="featured-work-experience"]'); // should be visible now that we have 2 current experiences
        }
    });

    it('should show the salary-survey-form-section', () => {
        render();
        SpecHelper.expectElement(elem, 'salary-survey-form-section');
    });

    // invalidFieldsLinks dir
    it('should render labels/labelText for missing required fields when blank form is submitted', () => {
        jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(true);
        user.career_profile.survey_years_full_time_experience = null;
        user.career_profile.survey_most_recent_role_description = null;
        user.career_profile.work_experiences = [];

        render();
        parentScope.save();
        parentScope.$digest();

        expect(parentScope.invalidFields.length).toBe(14);

        SpecHelper.expectElementText(
            elem,
            'invalid-fields-links .field:eq(0)',
            'How many years of full time work experience do you have?',
        );
        SpecHelper.expectElementText(
            elem,
            'invalid-fields-links .field:eq(1)',
            'My most recent role can best be described as…',
        );

        SpecHelper.expectElementText(
            elem,
            'invalid-fields-links .field:eq(2)',
            'Provide your full-time work history starting with your first position after university. Less experienced applicants may include full-time internships here.',
        );

        // items don't order as expected because in headless Chrome they have an offset = {top: 0, left: 0}
        // a few of these duplicate because they aren't being picked up as display: none for mobile vs desktop
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(3)', 'Job Title');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(4)', 'Job Title');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(5)', 'Organization');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(6)', 'Industry');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(7)', 'Role');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(8)', 'Month');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(9)', 'Year');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(10)', 'Industry');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(11)', 'Role');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(12)', 'Month');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(13)', 'Year');
    });
});
