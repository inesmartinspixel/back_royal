import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import setSpecLocales from 'Translation/setSpecLocales';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import employerPreferencesFormLocales from 'Careers/locales/careers/edit_career_profile/employer_preferences_form-en.json';
import jobPreferencesFormLocales from 'Careers/locales/careers/edit_career_profile/job_preferences_form-en.json';
import frontRoyalFormLocales from 'FrontRoyalForm/locales/front_royal_form/front_royal_form-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import profilePhotoFormSectionLocales from 'Careers/locales/careers/edit_career_profile/profile_photo_form_section-en.json';
import uploadAvatarLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/upload_avatar-en.json';
import invalidFieldsLinksLocales from 'FrontRoyalForm/locales/front_royal_form/invalid_fields_links-en.json';

setSpecLocales(
    editCareerProfileLocales,
    employerPreferencesFormLocales,
    jobPreferencesFormLocales,
    frontRoyalFormLocales,
    fieldOptionsLocales,
    profilePhotoFormSectionLocales,
    uploadAvatarLocales,
    invalidFieldsLinksLocales,
);

describe('FrontRoyal.Careers.EditCareerProfile.CareerPreferencesForm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let StudentDashboard;
    let user;
    let CareerProfile;
    let EditCareerProfileHelper;
    let careerPreferencesFormElem;
    let $timeout;
    let parentScope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                StudentDashboard = $injector.get('StudentDashboard');
                CareerProfile = $injector.get('CareerProfile');
                EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                $timeout = $injector.get('$timeout');

                $injector.get('CareerProfileFixtures');
                SpecHelper.stubConfig();
            },
        ]);

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance({
            locations_of_interest: ['none'],
        });
        SpecHelper.stubEventLogging();
    });

    function render() {
        StudentDashboard.expect('index').toBeCalledWith({
            get_has_available_incomplete_streams: true,
            get_has_available_incomplete_playlists: true,
            user_id: user.id,
            filters: {},
        });

        renderer = SpecHelper.renderer();
        renderer.scope.steps = [
            {
                stepName: 'career_preferences',
                requiredFields: {
                    primary_areas_of_interest: {
                        min: 1,
                    },
                    job_sectors_of_interest: {
                        min: 1,
                    },
                    preferred_company_culture_descriptors: {
                        min: 3,
                    },
                    employment_types_of_interest: {
                        min: 1,
                    },
                    company_sizes_of_interest: {
                        min: 1,
                    },
                    interested_in_joining_new_company: true,
                    locations_of_interest: {
                        min: 1,
                    },
                    authorized_to_work_in_us: true,
                    salary_range: true,
                },
            },
        ];
        renderer.render('<edit-career-profile steps="steps"></edit-career-profile>');
        $timeout.flush();
        elem = renderer.elem;
        parentScope = elem.isolateScope();
        careerPreferencesFormElem = SpecHelper.expectElement(elem, 'career-preferences-form');
        scope = careerPreferencesFormElem.scope();
        parentScope.$apply();
    }

    it('should be wired to careerProfile', () => {
        render();
        expect(scope.careerProfile).toBeDefined();
    });

    it('should call logEventForApplication when initialized', () => {
        jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});
        render();
        expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith('viewed-career_preferences');
    });

    it('should show the appropriate text for desired job information in an EMBA or MBA context', () => {
        Object.defineProperty(user, 'programType', {
            value: 'mba',
        });
        render();
        SpecHelper.expectElementText(
            elem,
            '#desired_employment_group > span',
            'If you enter the career network, what type of employment would you be interested in?',
        );
    });

    it('should show the appropriate text for desired job information in an Careers Only context', () => {
        Object.defineProperty(user, 'programType', {
            value: 'career_network_only',
        });
        render();
        SpecHelper.expectElementText(
            elem,
            '#desired_employment_group > span',
            'What type of employment are you interested in?',
        );
    });

    describe('profile-photo-form-section', () => {
        it('should be visible is isApplication', () => {
            jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(true);
            render();
            SpecHelper.expectElement(elem, 'profile-photo-form-section');

            EditCareerProfileHelper.isApplication.mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectNoElement(elem, 'profile-photo-form-section');
        });
    });

    describe('interested_in_joining_new_company', () => {
        it("should support having a default value of 'neutral' if interested_in_joining_new_company is not present", () => {
            // When interested_in_joining_new_company is not already set
            user.career_profile.interested_in_joining_new_company = null;
            render();
            expect(scope.careerProfile.interested_in_joining_new_company).toEqual('neutral');

            // When interested_in_joining_new_company is already set
            user.career_profile.interested_in_joining_new_company = 'something_other_than_neutral';
            render();
            expect(scope.careerProfile.interested_in_joining_new_company).toEqual('something_other_than_neutral');
        });
    });

    describe('willing_to_relocate', () => {
        it('should not overwrite locations_of_interest when initializing to true', () => {
            user.career_profile.willing_to_relocate = true;
            user.career_profile.locations_of_interest = ['washington_dc'];
            render();
            expect(scope.careerProfile.willing_to_relocate).toBe(true);
            expect(scope.careerProfile.locations_of_interest).toEqual(['washington_dc']);
        });

        it('should set locations_of_interest to empty array if initializing to true and no locations_of_interest present', () => {
            user.career_profile.willing_to_relocate = true;
            user.career_profile.locations_of_interest = ['none'];
            render();
            expect(scope.careerProfile.willing_to_relocate).toBe(true);
            expect(scope.careerProfile.locations_of_interest).toEqual([]);
        });

        it('should set locations_of_interest to none if initializing to false', () => {
            user.career_profile.willing_to_relocate = false;
            user.career_profile.locations_of_interest = ['washington_dc'];
            render();
            expect(scope.careerProfile.willing_to_relocate).toBe(false);
            expect(scope.careerProfile.locations_of_interest).toEqual(['none']);
        });

        it('should set locations_of_interest to none if changing to false', () => {
            user.career_profile.willing_to_relocate = true;
            user.career_profile.locations_of_interest = ['washington_dc'];
            render();
            SpecHelper.uncheckCheckbox(elem, '[name="willing_to_relocate"]');
            expect(scope.careerProfile.willing_to_relocate).toBe(false);
            expect(scope.careerProfile.locations_of_interest).toEqual(['none']);
        });

        it('should set locations_of_interest to flexible if changing to true', () => {
            user.career_profile.willing_to_relocate = false;
            user.career_profile.locations_of_interest = ['washington_dc'];
            render();
            jest.spyOn(scope, 'toggleCheckbox');
            SpecHelper.checkCheckbox(elem, '[name="willing_to_relocate"]');
            expect(scope.careerProfile.willing_to_relocate).toBe(true);
            expect(scope.toggleCheckbox).toHaveBeenCalledWith('locations_of_interest', 'flexible');
            expect(scope.careerProfile.locations_of_interest).toEqual(['flexible']);
        });

        it('should do nothing if not being changed', () => {
            user.career_profile.willing_to_relocate = true;
            user.career_profile.locations_of_interest = ['flexible', 'washington_dc'];
            render();
            expect(user.career_profile.willing_to_relocate).toBe(true);
            expect(user.career_profile.locations_of_interest).toEqual(['flexible', 'washington_dc']);
        });
    });

    describe('locations_of_interest', () => {
        it('should be hidden if willing_to_relocate is false and vice versa', () => {
            render();
            assertLocationsOfInterestIsHiddenThenVisible();
        });

        it("should support having a default value of 'flexible'", () => {
            render();
            assertLocationsOfInterestIsHiddenThenVisible();

            // should default to flexible
            expect(scope.careerProfile.locations_of_interest).toEqual(['flexible']);
            SpecHelper.expectCheckboxChecked(elem, '[name="location_of_interest"]:eq(0)');
        });

        it("should support having a dominant value of 'flexible'", () => {
            render();
            assertLocationsOfInterestIsHiddenThenVisible();
            SpecHelper.expectElements(elem, '[name="location_of_interest"]');

            // should default to flexible, which should be the first checkbox option
            expect(scope.careerProfile.locations_of_interest).toEqual(['flexible']);
            SpecHelper.expectCheckboxChecked(elem, '[name="location_of_interest"]:eq(0)');

            // check a different checkbox in the group
            SpecHelper.checkCheckbox(elem, '[name="location_of_interest"]', 1);
            SpecHelper.expectCheckboxUnchecked(elem, '[name="location_of_interest"]:eq(0)');
            SpecHelper.expectCheckboxChecked(elem, '[name="location_of_interest"]:eq(1)');
            expect(scope.careerProfile.locations_of_interest).not.toEqual(['flexible']);
            expect(scope.careerProfile.locations_of_interest).toEqual([scope.locationsOfInterest[1]]);

            // check another checkbox in the group that's not the default value
            SpecHelper.checkCheckbox(elem, '[name="location_of_interest"]', 2);
            SpecHelper.expectCheckboxUnchecked(elem, '[name="location_of_interest"]:eq(0)');
            SpecHelper.expectCheckboxChecked(elem, '[name="location_of_interest"]:eq(1)');
            SpecHelper.expectCheckboxChecked(elem, '[name="location_of_interest"]:eq(2)');
            expect(scope.careerProfile.locations_of_interest).not.toEqual(['flexible']);
            expect(scope.careerProfile.locations_of_interest).toEqual([
                scope.locationsOfInterest[1],
                scope.locationsOfInterest[2],
            ]);

            // Now check the checkbox for the default value. Every other checkbox should be unchecked.
            SpecHelper.checkCheckbox(elem, '[name="location_of_interest"]', 0);
            SpecHelper.expectCheckboxUnchecked(elem, '[name="location_of_interest"]:eq(1)');
            SpecHelper.expectCheckboxUnchecked(elem, '[name="location_of_interest"]:eq(2)');
            SpecHelper.expectCheckboxChecked(elem, '[name="location_of_interest"]:eq(0)');
            expect(scope.careerProfile.locations_of_interest).not.toEqual([
                scope.locationsOfInterest[1],
                scope.locationsOfInterest[2],
            ]);
            expect(scope.careerProfile.locations_of_interest).toEqual(['flexible']);
        });

        describe('on mobile', () => {
            it('should show only first 10 location options and a button to show all locations', () => {
                render();
                scope.isMobile = true;
                assertLocationsOfInterestIsHiddenThenVisible();
                SpecHelper.expectElements(elem, '[name="location_of_interest"]', 10); // should only have 10 visible locations
            });

            it("should show all available location options when 'Show All Locations' button is clicked", () => {
                render();
                scope.isMobile = true;
                assertLocationsOfInterestIsHiddenThenVisible();
                SpecHelper.expectElements(elem, '[name="location_of_interest"]', 10); // should only have 10 visible locations

                // click the button to show all locations
                SpecHelper.click(elem, '[name="show_all_locations"]');
                const locationsOfInterestElems = SpecHelper.expectElements(elem, '[name="location_of_interest"]');
                expect(locationsOfInterestElems.length).toBeGreaterThan(10); // should now have more than 10 visible locations
            });
        });

        describe('on desktop', () => {
            it('should show all locations split into two columns', () => {
                render();
                scope.isMobile = false;
                scope.$digest();
                assertLocationsOfInterestIsHiddenThenVisible();

                const locationsOfInterestElems = SpecHelper.expectElements(elem, '[name="location_of_interest"]');
                expect(locationsOfInterestElems.length).toBeGreaterThan(10); // should now have more than 10 visible locations

                // assert that each element has the expected classes applied to it
                for (let i = 0; i < locationsOfInterestElems.length; i++) {
                    const labelElem = {
                        0: locationsOfInterestElems[i].parentElement,
                    };
                    SpecHelper.expectElementHasClass(labelElem, 'split-group');
                    SpecHelper.expectElementHasClass(labelElem, i % 2 === 0 ? 'left' : 'right');
                }
            });
        });

        function assertLocationsOfInterestIsHiddenThenVisible() {
            // should be hidden
            expect(scope.careerProfile.willing_to_relocate).toBe(false);
            SpecHelper.expectNoElement(elem, '[name="location_of_interest"]');
            SpecHelper.expectNoElement(elem, '[name="show_all_locations"]');

            scope.careerProfile.willing_to_relocate = true;
            scope.$digest();

            // should be visible
            expect(scope.careerProfile.willing_to_relocate).toBe(true);
            SpecHelper.expectElements(elem, '[name="location_of_interest"]');
            if (scope.isMobile) {
                SpecHelper.expectElement(elem, '[name="show_all_locations"]');
            } else {
                SpecHelper.expectNoElement(elem, '[name="show_all_locations"]');
            }
        }
    });

    // invalidFieldsLinks dir
    it('should render labels/labelText for missing required fields when blank form is submitted', () => {
        jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(true);
        jest.spyOn(EditCareerProfileHelper, 'isCareerProfile').mockReturnValue(false);
        user.career_profile.job_sectors_of_interest = null;
        user.career_profile.primary_areas_of_interest = null;
        user.career_profile.preferred_company_culture_descriptors = null;
        user.career_profile.company_sizes_of_interest = [];
        user.career_profile.employment_types_of_interest = [];
        user.career_profile.authorized_to_work_in_us = null;

        render();
        parentScope.save();
        parentScope.$digest();

        expect(parentScope.invalidFields.length).toBe(6);

        // items don't order as expected because in headless Chrome they have an offset = {top: 0, left: 0}
        SpecHelper.expectElementText(
            elem,
            'invalid-fields-links .field:eq(0)',
            'Select your citizenship/immigration status',
        );
        SpecHelper.expectElementText(
            elem,
            'invalid-fields-links .field:eq(1)',
            'What size companies do you prefer working in?',
        );
        SpecHelper.expectElementText(
            elem,
            'invalid-fields-links .field:eq(2)',
            'If you enter the career network, what type of employment would you be interested in?',
        );
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(3)', 'Industries');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(4)', 'Company Culture');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(5)', 'Roles');
    });
});
