import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import setSpecLocales from 'Translation/setSpecLocales';
import basicInfoFormLocales from 'Careers/locales/careers/edit_career_profile/basic_info_form-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import invalidFieldsLinksLocales from 'FrontRoyalForm/locales/front_royal_form/invalid_fields_links-en.json';

setSpecLocales(basicInfoFormLocales, editCareerProfileLocales, invalidFieldsLinksLocales);

describe('FrontRoyal.Careers.EditCareerProfile.BasicInfoForm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let StudentDashboard;
    let user;
    let CareerProfile;
    let EditCareerProfileHelper;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'FrontRoyal.Form', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                StudentDashboard = $injector.get('StudentDashboard');
                CareerProfile = $injector.get('CareerProfile');
                EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                $timeout = $injector.get('$timeout');

                $injector.get('CareerProfileFixtures');
                SpecHelper.stubConfig();
                SpecHelper.stubEventLogging();
            },
        ]);

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();

        SpecHelper.stubDirective('locationAutocomplete');
    });

    function render() {
        StudentDashboard.expect('index').toBeCalledWith({
            get_has_available_incomplete_streams: true,
            get_has_available_incomplete_playlists: true,
            user_id: user.id,
            filters: {},
        });

        renderer = SpecHelper.renderer();
        renderer.scope.steps = [
            {
                stepName: 'basic_info',
            },
        ];
        renderer.render('<edit-career-profile steps="steps"></edit-career-profile>');
        $timeout.flush(); // apply the scope.steps assignment
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should be wired to careerProfile', () => {
        render();
        expect(scope.careerProfile).toBeDefined();
    });

    it('should call logEventForApplication when initialized', () => {
        jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});
        render();
        expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith('viewed-basic_info');
    });

    it('should autofill an empty city, state, and / or country when using the location autocomplete', () => {
        user.career_profile.city = null;
        user.career_profile.state = null;
        user.career_profile.country = null;
        user.career_profile.address_line_1 = null;

        // Mock details that would have been built in location_autocomplete_dir
        user.career_profile.place_details = {};

        render();

        scope.careerProfile.place_details = {
            locality: {
                long: 'Foo',
            },
            administrative_area_level_1: {
                short: 'Ba',
            },
            country: {
                short: 'US',
            },
        };

        scope.$digest();

        expect(scope.careerProfile.city).toBe('Foo');
        expect(scope.careerProfile.state).toBe('Ba');
        expect(scope.careerProfile.country).toBe('US');

        // If city, state, and country are already set they should not be overriden
        scope.careerProfile.place_details = {
            country: {
                short: 'New Country',
            },
            locality: {
                long: 'New City',
            },
            administrative_area_level_1: {
                long: 'New State',
            },
        };

        expect(scope.careerProfile.city).toBe('Foo');
        expect(scope.careerProfile.state).toBe('Ba');
        expect(scope.careerProfile.country).toBe('US');
    });

    it('should show invalid age text if invalid age is provided', () => {
        render();

        // date below min
        SpecHelper.updateTextInput(elem, 'date-with-fallback input', '1818-12-31');
        SpecHelper.expectElement(elem, '.birthdate-preview.sub-text.red');

        // valid date within range
        SpecHelper.updateTextInput(elem, 'date-with-fallback input', '1983-05-07');
        SpecHelper.expectElementText(elem, '.birthdate-preview.sub-text', 'May 7, 1983');
        SpecHelper.expectNoElement(elem, '.birthdate-preview.sub-text.red');

        // date above max
        SpecHelper.updateTextInput(elem, 'date-with-fallback input', '2115-12-31');
        SpecHelper.expectElement(elem, '.birthdate-preview.sub-text.red');
    });

    // invalidFieldsLinks dir
    it('should render labels/labelText for missing required fields when blank form is submitted', () => {
        user.career_profile.nickname = null;
        user.career_profile.name = null;
        user.career_profile.birthdate = null;
        user.career_profile.place_id = null;

        render();
        scope.save();
        scope.$digest();

        expect(scope.invalidFields.length).toBe(4);

        // items don't order as expected because in headless Chrome they have an offset = {top: 0, left: 0}
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(0)', 'First Name or Nickname');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(1)', 'Full Name');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(2)', 'Location');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(3)', 'Birthdate');
    });
});
