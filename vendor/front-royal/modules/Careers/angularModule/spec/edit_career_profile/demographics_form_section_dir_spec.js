import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import setSpecLocales from 'Translation/setSpecLocales';
import demographicsFormSectionLocales from 'Careers/locales/careers/edit_career_profile/demographics_form_section-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';

setSpecLocales(demographicsFormSectionLocales, editCareerProfileLocales, fieldOptionsLocales);

describe('FrontRoyal.Careers.EditCareerProfile.DemographicsFormSection', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let CareerProfile;
    let careerProfile;
    let FormHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');
                FormHelper = $injector.get('FormHelper');

                $injector.get('CareerProfileFixtures');
            },
        ]);

        careerProfile = CareerProfile.fixtures.getInstance();
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.careerProfile = careerProfile;
        renderer.scope.gdprAppliesToUser = opts.gdprAppliesToUser || false;
        renderer.render(
            '<demographics-form-section career-profile="careerProfile" gdpr-applies-to-user="gdprAppliesToUser"></demographics-form-section>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should support updating the sex on the careerProfile', () => {
        careerProfile.sex = null;
        render();
        expect(scope.careerProfile.sex).toBeNull();
        SpecHelper.updateSelect(elem, 'select[name="sex"]', 'male');
        expect(scope.careerProfile.sex).toEqual('male');
    });

    it('should supportCheckboxGroups for the race field', () => {
        jest.spyOn(FormHelper, 'supportCheckboxGroups').mockImplementation(() => {});
        render();
        expect(FormHelper.supportCheckboxGroups).toHaveBeenCalledWith(scope, scope.careerProfile, {
            race: [
                'american_indian_or_alaska',
                'asian',
                'african_american',
                'latin_american',
                'arab',
                'native_hawaiian_or_other',
                'white',
            ],
        });
    });

    describe('when !gdprAppliesToUser', () => {
        beforeEach(() => {
            careerProfile.race = [];
            render({
                gdprAppliesToUser: false,
            });
        });

        it('should show racial-identity section', () => {
            SpecHelper.expectElement(elem, '.racial-identity');
        });

        it('should support updating the race on the careerProfile', () => {
            expect(scope.careerProfile.race).toEqual([]);
            SpecHelper.checkCheckbox(elem, 'input[name="race"][type="checkbox"]:eq(0)');
            expect(scope.careerProfile.race).toEqual([scope.raceKeys[0]]);
            SpecHelper.uncheckCheckbox(elem, 'input[name="race"][type="checkbox"]:eq(0)');
            expect(scope.careerProfile.race).toEqual([]);
        });
    });

    describe('when gdprAppliesToUser', () => {
        beforeEach(() => {
            render({
                gdprAppliesToUser: true,
            });
        });

        it('should not show the racial-identity section', () => {
            SpecHelper.expectNoElement(elem, '.racial-identity');
        });
    });
});
