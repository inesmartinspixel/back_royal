import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import setSpecLocales from 'Translation/setSpecLocales';
import embaApplicationQuestionsFormLocales from 'Careers/locales/careers/edit_career_profile/emba_application_questions_form-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import writeTextAboutLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/write_text_about-en.json';
import personalSummaryFormLocales from 'Careers/locales/careers/edit_career_profile/personal_summary_form-en.json';
import invalidFieldsLinksLocales from 'FrontRoyalForm/locales/front_royal_form/invalid_fields_links-en.json';

setSpecLocales(
    embaApplicationQuestionsFormLocales,
    writeTextAboutLocales,
    editCareerProfileLocales,
    personalSummaryFormLocales,
    invalidFieldsLinksLocales,
);

describe('FrontRoyal.Careers.EditCareerProfile.EmbaApplicationQuestionsForm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let user;
    let CareerProfile;
    let EditCareerProfileHelper;
    let $timeout;
    let answersTooShortHelper;
    let parentScope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');
                EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                $timeout = $injector.get('$timeout');
                answersTooShortHelper = $injector.get('answersTooShortHelper');

                $injector.get('CareerProfileFixtures');
                SpecHelper.stubConfig();
            },
        ]);

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
    });

    function render(requiredFields) {
        renderer = SpecHelper.renderer();
        renderer.scope.steps = [
            {
                stepName: 'emba_application_questions',
                requiredFields: requiredFields || {},
            },
        ];
        renderer.render('<edit-career-profile steps="steps"></edit-career-profile>');
        $timeout.flush(); // apply the scope.steps assignment
        elem = renderer.elem;
        parentScope = elem.isolateScope();
        scope = elem.find('emba-application-questions-form').scope();
        scope.$digest();
    }

    it('should be wired to careerProfile', () => {
        render();
        expect(scope.careerProfile).toBeDefined();
    });

    it('should call logEventForApplication when initialized', () => {
        jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});
        render();
        expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith(
            'viewed-emba_application_questions',
        );
    });

    it('should maybeShowModal', () => {
        jest.spyOn(answersTooShortHelper, 'maybeShowModal').mockImplementation(() => {});
        render();
        scope.$broadcast('savedCareerProfile');
        expect(answersTooShortHelper.maybeShowModal).toHaveBeenCalled();
    });

    it('should have short-answer-fields-section and support short answer model with override values', () => {
        render();
        SpecHelper.expectElement(
            elem,
            'short-answer-fields-section[model-props="shortAnswerModelProps"][field-overrides="shortAnswerFieldOverrides"]',
        );
        expect(scope.shortAnswerModelProps).toEqual([
            'short_answer_why_pursuing',
            'short_answer_greatest_achievement',
            'short_answer_ask_professional_advice',
            'anything_else_to_tell_us',
        ]);
        expect(scope.shortAnswerFieldOverrides).toEqual({
            short_answer_why_pursuing: {
                localeValues: "{brandName: 'Quantic School of Business and Technology'}",
            },
            anything_else_to_tell_us: {
                maxCharLength: 500,
                localeKey: 'careers.edit_career_profile.personal_summary_form.anything_else_to_tell_us',
            },
        });
    });

    it('should have optional short-answer-scholarship-field', () => {
        render();
        SpecHelper.expectDoesNotHaveClass(
            elem,
            '[name="short-answer-scholarship-wrapper"] label[translate-once="careers.edit_career_profile.emba_application_questions_form.merit_reasons"]',
            'required',
        );
        SpecHelper.expectElement(
            elem,
            'short-answer-scholarship-field[career-profile="careerProfile"][is-required="false"]',
        );
    });

    // invalidFieldsLinks dir
    it('should render labels/labelText for missing required fields when blank form is submitted', () => {
        user.career_profile.short_answer_why_pursuing = null;
        user.career_profile.short_answer_greatest_achievement = null;
        user.career_profile.short_answer_ask_professional_advice = null;

        render({
            short_answer_why_pursuing: true,
            short_answer_greatest_achievement: true,
            short_answer_ask_professional_advice: true,
        });
        parentScope.save();
        parentScope.$digest();

        expect(parentScope.invalidFields.length).toBe(3);

        // items don't order as expected because in headless Chrome they have an offset = {top: 0, left: 0}
        SpecHelper.expectElementText(
            elem,
            'invalid-fields-links .field:eq(0)',
            'Why are you pursuing your Executive MBA at this point in your career? And why are you considering Quantic School of Business and Technology in particular?',
        );
        SpecHelper.expectElementText(
            elem,
            'invalid-fields-links .field:eq(1)',
            'What do you consider to be your greatest career achievement so far? Why?',
        );
        SpecHelper.expectElementText(
            elem,
            'invalid-fields-links .field:eq(2)',
            'If you could ask anyone for professional advice, who would you choose and what would you ask? Why?',
        );
    });
});
