import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import setSpecLocales from 'Translation/setSpecLocales';
import programLockdownFormLocales from 'Careers/locales/careers/edit_career_profile/program_lockdown_form-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import programChoiceFormLocales from 'Careers/locales/careers/edit_career_profile/program_choice_form-en.json';

setSpecLocales(editCareerProfileLocales, programLockdownFormLocales, programChoiceFormLocales);

describe('FrontRoyal.Careers.EditCareerProfile.ProgramLockdownForm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let user;
    let CareerProfile;
    let EditCareerProfileHelper;
    let $timeout;
    let DialogModal;
    let CohortApplication;
    let Cohort;
    let ClientStorage;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'FrontRoyal.Form', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');
                EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                $timeout = $injector.get('$timeout');
                DialogModal = $injector.get('DialogModal');
                CohortApplication = $injector.get('CohortApplication');
                Cohort = $injector.get('Cohort');
                ClientStorage = $injector.get('ClientStorage');

                $injector.get('CohortApplicationFixtures');
                $injector.get('CareerProfileFixtures');
                $injector.get('CohortFixtures');
                SpecHelper.stubConfig();
            },
        ]);

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
        user.relevant_cohort = Cohort.fixtures.getInstance();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.steps = [
            {
                stepName: 'program_lockdown',
            },
        ];
        renderer.render('<edit-career-profile steps="steps"></edit-career-profile>');
        $timeout.flush(); // apply the scope.steps assignment
        elem = renderer.elem;
        scope = elem.find('program-lockdown-form').scope();
        scope.$apply();
    }

    it('should be wired to careerProfile', () => {
        render();
        expect(scope.careerProfile).toBeDefined();
    });

    it('should call logEventForApplication when initialized', () => {
        jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});
        render();
        expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith('viewed-program_lockdown');
    });

    describe('!canConvertToEMBA', () => {
        beforeEach(() => {
            user.cohort_applications = [CohortApplication.fixtures.getInstance()];
        });

        it('should show standard lockdown elements with admission decision', () => {
            user.career_profile.program_type = 'mba';
            render();
            SpecHelper.expectElement(elem, '.thankyou-text');

            SpecHelper.expectElementText(elem, '.program-title', 'The Free MBA');
            SpecHelper.expectElementText(elem, '.program-subtitle', 'For high potential early-career learners.');

            SpecHelper.expectElement(elem, '.admission-decision');
            SpecHelper.expectElement(elem, '.application-actions');
        });

        describe('editApplication', () => {
            it('should work', () => {
                render();
                jest.spyOn(scope, '$emit').mockImplementation(() => {});

                scope.editApplication();

                expect(scope.$emit).toHaveBeenCalledWith('gotoFormStep', 1);
            });
        });

        describe('decisionDate', () => {
            it('should be set when appliedAt is before admission round', () => {
                jest.spyOn(user.lastCohortApplication, 'appliedAt', 'get').mockReturnValue(new Date(2017, 2, 7));
                user.relevant_cohort.admission_rounds = [
                    {
                        decisionDate: new Date(2018, 2, 7),
                    },
                ];
                render();
                expect(scope.decisionDate).toEqual('March 6');
            });

            it('should be set when appliedAt is after admission round', () => {
                jest.spyOn(user.lastCohortApplication, 'appliedAt', 'get').mockReturnValue(new Date(2019, 2, 7));
                user.relevant_cohort.admission_rounds = [
                    {
                        decisionDate: new Date(2018, 2, 7),
                    },
                ];
                render();
                expect(scope.decisionDate).toEqual('March 6');
            });
        });
    });

    describe('canConvertToEMBA', () => {
        beforeEach(() => {
            user.cohort_applications = [
                CohortApplication.fixtures.getInstance({
                    can_convert_to_emba: true,
                }),
            ];
            user.career_profile.program_type = 'mba';
        });

        it('should show standard switch to EMBA elements', () => {
            render();
            SpecHelper.expectElement(elem, '.thankyou-text');

            SpecHelper.expectElementText(elem, '.program-title', 'The Executive MBA');
            SpecHelper.expectElementText(elem, '.program-subtitle', 'For high achieving mid-career professionals.');

            SpecHelper.expectElement(elem, '.sub-text');
        });

        it('should show thankyou text for program_type', () => {
            render();
            SpecHelper.expectElementText(
                elem,
                '.thankyou-text',
                `Thank you for applying to Quantic. After careful review, our team concluded that you are not a fit for the Quantic early-career MBA. However, your application was chosen from the thousands we receive as a strong match for our Executive MBA program.`,
            );

            user.career_profile.program_type = 'career_network_only';
            render();
            SpecHelper.expectElementText(
                elem,
                '.thankyou-text',
                `Thank you for applying to Quantic. After careful review, our team concluded that you are not a fit for Career Network Only. However, your application was chosen from the thousands we receive as a strong match for our Executive MBA program.`,
            );
        });

        describe('updateApplication', () => {
            it('should work', () => {
                render();
                CareerProfile.expect('update');
                jest.spyOn(ClientStorage, 'setItem').mockImplementation(() => {});
                jest.spyOn(scope, 'showShortAnswerScholarshipModal');
                jest.spyOn(scope, '$emit').mockImplementation(() => {});

                scope.updateApplication('emba');
                CareerProfile.flush('update');

                expect(ClientStorage.setItem).toHaveBeenCalledWith('converted_to_emba', true);
                expect(scope.showShortAnswerScholarshipModal).toHaveBeenCalled();
                expect(scope.$emit).toHaveBeenCalledWith('savedCareerProfile');
            });
        });

        describe('onShortAnswerScholarshipSave', () => {
            it('should save and then remove DialogModal alerts', () => {
                render();
                const $q = $injector.get('$q');
                const deferred = $q.defer();
                const saveSpy = jest.spyOn(scope, 'save').mockReturnValue(deferred.promise);
                const dialogModalRemoveAlertsSpy = jest.spyOn(DialogModal, 'removeAlerts').mockImplementation(() => {});

                scope.onShortAnswerScholarshipSave();

                expect(saveSpy).toHaveBeenCalled();
                expect(dialogModalRemoveAlertsSpy).not.toHaveBeenCalled();

                deferred.resolve();
                $timeout.flush(); // flush the $q promise to trigger the .then() callback

                expect(dialogModalRemoveAlertsSpy).toHaveBeenCalled();
            });
        });

        describe('showShortAnswerScholarshipModal', () => {
            it("should show modal if user changed program type to emba, hasn't filled out the short_answer_scholarship question yet, and converted_pending_application_to_emba_upon_invitation", () => {
                render();
                jest.spyOn(scope.currentUser, 'lastCohortApplication', 'get').mockReturnValue({
                    converted_pending_application_to_emba_upon_invitation: true,
                });
                jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
                scope.showShortAnswerScholarshipModal();
                expect(DialogModal.alert).toHaveBeenCalledWith({
                    content:
                        '<short-answer-scholarship-modal career-profile="careerProfile" on-save="onSave"></short-answer-scholarship-modal>',
                    scope: {
                        careerProfile: scope.careerProfile,
                        onSave: scope.onShortAnswerScholarshipSave,
                    },
                });
            });
        });
    });
});
