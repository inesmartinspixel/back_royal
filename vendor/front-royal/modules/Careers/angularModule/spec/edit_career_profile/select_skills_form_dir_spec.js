import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import setSpecLocales from 'Translation/setSpecLocales';
import selectSkillsFormLocales from 'Careers/locales/careers/edit_career_profile/select_skills_form-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import selectedPillsLocales from 'FrontRoyalForm/locales/front_royal_form/selected_pills-en.json';
import inputConstLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/input_const_autosuggest-en.json';
import invalidFieldsLinksLocales from 'FrontRoyalForm/locales/front_royal_form/invalid_fields_links-en.json';

setSpecLocales(
    editCareerProfileLocales,
    selectSkillsFormLocales,
    selectedPillsLocales,
    inputConstLocales,
    invalidFieldsLinksLocales,
);

describe('FrontRoyal.Careers.EditCareerProfile.SelectSkillsForm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let StudentDashboard;
    let user;
    let CareerProfile;
    let EditCareerProfileHelper;
    let $timeout;
    let formScope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                StudentDashboard = $injector.get('StudentDashboard');
                CareerProfile = $injector.get('CareerProfile');
                EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                $timeout = $injector.get('$timeout');

                $injector.get('CareerProfileFixtures');
                SpecHelper.stubConfig();
            },
        ]);

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
        user.career_profile.skills = undefined;
    });

    function render(numRequiredSkills) {
        StudentDashboard.expect('index').toBeCalledWith({
            get_has_available_incomplete_streams: true,
            get_has_available_incomplete_playlists: true,
            user_id: user.id,
            filters: {},
        });

        renderer = SpecHelper.renderer();
        renderer.scope.steps = [
            {
                stepName: 'select_skills',
                requiredFields: {
                    skills: {
                        min: numRequiredSkills || 4,
                    },
                },
            },
        ];
        renderer.render('<edit-career-profile steps="steps"></edit-career-profile>');
        $timeout.flush(); // apply the scope.steps assignment
        elem = renderer.elem;
        scope = elem.isolateScope();
        formScope = elem.find('select-skills-form').scope();
        scope.$apply();
    }

    function populateSkills(numSkills) {
        scope.careerProfile.skills = [];

        for (let i = 0; i <= numSkills; i++) {
            scope.careerProfile.skills.push({
                locale: 'en',
                text: 'skill',
                type: 'skill',
            });
        }
        scope.$apply();
    }

    it('should be wired to careerProfile', () => {
        render();
        expect(scope.careerProfile).toBeDefined();
    });

    it('should call logEventForApplication when initialized', () => {
        jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});
        render();
        expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith('viewed-select_skills');
    });

    it('should validate/invalidate the form when appropriate', () => {
        render();
        expect(formScope.select_skills.$valid).toBe(false);
        populateSkills(1);
        expect(formScope.select_skills.$valid).toBe(false);
        populateSkills(4);
        expect(formScope.select_skills.$valid).toBe(true);
    });

    it('should dirty the form when appropriate', () => {
        render();
        expect(formScope.select_skills.$dirty).toBe(false);
        populateSkills(1);
        expect(formScope.select_skills.$dirty).toBe(true);
        populateSkills(4);
        expect(formScope.select_skills.$dirty).toBe(true);
    });

    describe('$setDirty', () => {
        it('should dirty the form when changing skills', () => {
            render();
            jest.spyOn(formScope.select_skills, '$setDirty').mockImplementation(() => {});
            populateSkills(1);
            expect(formScope.select_skills.$setDirty).toHaveBeenCalledWith(true);
        });

        it('should not dirty the form is skills are the same as initialSkills', () => {
            user.career_profile.skills = [
                {
                    text: 'foo',
                },
            ];
            render();
            jest.spyOn(formScope.select_skills, '$setDirty').mockImplementation(() => {});
            scope.careerProfile.skills.push({
                text: 'bar',
            });
            scope.$digest();
            scope.careerProfile.skills.pop();
            scope.$digest();
            expect(formScope.select_skills.$setDirty).toHaveBeenCalledTimes(1); // the second time we manipulate interests, it shouldn't call it
        });
    });

    // invalidFieldsLinks dir
    it('should render labels/labelText for missing required fields when blank form is submitted', () => {
        user.career_profile.skills = [];

        render();
        scope.save();
        scope.$digest();

        // can't test for text because it uses the page's application-header-text which is not visible in this elem
        expect(scope.invalidFields.length).toBe(1);
    });
});
