import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import setSpecLocales from 'Translation/setSpecLocales';
import educationFormLocales from 'Careers/locales/careers/edit_career_profile/education_form-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import educationExperienceDetailLocales from 'Careers/locales/careers/edit_career_profile/education_experience_detail-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import invalidFieldsLinksLocales from 'FrontRoyalForm/locales/front_royal_form/invalid_fields_links-en.json';

setSpecLocales(
    editCareerProfileLocales,
    educationFormLocales,
    educationExperienceDetailLocales,
    fieldOptionsLocales,
    invalidFieldsLinksLocales,
);

describe('FrontRoyal.Careers.EditCareerProfile.educationForm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let StudentDashboard;
    let user;
    let CareerProfile;
    let EditCareerProfileHelper;
    let $timeout;
    let parentScope;
    let Cohort;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                StudentDashboard = $injector.get('StudentDashboard');
                CareerProfile = $injector.get('CareerProfile');
                EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                $timeout = $injector.get('$timeout');
                Cohort = $injector.get('Cohort');

                $injector.get('CareerProfileFixtures');
                SpecHelper.stubConfig();
            },
        ]);

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();

        SpecHelper.stubEventLogging();
    });

    function render() {
        StudentDashboard.expect('index').toBeCalledWith({
            get_has_available_incomplete_streams: true,
            get_has_available_incomplete_playlists: true,
            user_id: user.id,
            filters: {},
        });

        renderer = SpecHelper.renderer();
        renderer.scope.steps = [
            {
                stepName: 'education',
                requiredFields: {
                    education_experiences: {
                        min: 1,
                        requiredFields: {
                            educational_organization: true,
                            degree: true,
                            graduation_year: true,
                            major: true,
                        },
                    },
                },
            },
            {
                stepName: 'resume_and_links',
            },
        ]; // Needed so the $isLast() doesn't hide the button
        renderer.render('<edit-career-profile steps="steps"></edit-career-profile>');
        $timeout.flush(); // apply the scope.steps assignment
        elem = renderer.elem;
        parentScope = elem.isolateScope();
        scope = elem.find('education-form').scope();
        scope.$apply();
        scope.proxy.mobileNavMenu = false;
        scope.$digest();
    }

    it('should be wired to careerProfile', () => {
        render();
        expect(scope.careerProfile).toBeDefined();
    });

    it('should call logEventForApplication when initialized', () => {
        jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});
        render();
        expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith('viewed-education');
    });

    it('should show red required placeholder message when education validation fails in MBA Application', () => {
        jest.spyOn(EditCareerProfileHelper, 'isMbaApplication').mockReturnValue(true);
        Object.defineProperty(user, 'programType', {
            value: 'mba',
        });
        render();

        // there's a $watch on careerProfile.education_experiences in the directive that will add a dummy work experience
        // if it seems that there are no education experiences on the career profile, which triggers if the reference to
        // scope.careerProfile.education_experiences changes, so we need to remove the elements from this array in place
        // to prevent this $watch from triggering. Otherwise, we won't get the expected messages.
        scope.careerProfile.education_experiences.splice(0, scope.careerProfile.education_experiences.length);
        scope.$digest();

        SpecHelper.expectElementText(
            elem,
            '[name="education-instructions-without-checkbox"]',
            'Please list the formal degrees you have earned or enrolled in, beginning with your undergraduate degree. Please do not include certificates, non-degree programs, or your high school diploma; you can add certificates and non-degree programs in the next step.',
        );
        SpecHelper.expectElementText(
            elem,
            '[name="education-blank-required"]',
            'Please add at least one education entry.',
        );

        SpecHelper.click(elem, '[name="save"]');

        expect(scope.currentForm.$valid).toBe(false);
        scope.$digest();
        SpecHelper.expectElement(elem, 'invalid-fields-links');
    });

    describe('Education Summary section', () => {
        it('should have a select wired to careerProfile.survey_highest_level_completed_education_description', () => {
            render();
            const value = _.first($injector.get('CAREERS_HIGHEST_EDUCATION_KEYS'));
            expect(scope.careerProfile.survey_highest_level_completed_education_description).not.toEqual(value);
            SpecHelper.updateSelect(elem, 'select[name="completed-education-description"]', value);
            expect(scope.careerProfile.survey_highest_level_completed_education_description).toEqual(value);
        });
    });

    describe('when formalEducationIsRequired', () => {
        beforeEach(() => {
            jest.spyOn(EditCareerProfileHelper, 'formalEducationIsRequired').mockReturnValue(true);
        });

        it('should addDummyEducationExperienceIfNecessary at initial load', () => {
            user.career_profile.education_experiences = [];
            jest.spyOn(CareerProfile.prototype, 'addDummyEducationExperienceIfNecessary');
            render();
            expect(EditCareerProfileHelper.formalEducationIsRequired).toHaveBeenCalledWith(
                scope.currentUser,
                scope.careerProfile,
            );
            expect(scope.careerProfile.addDummyEducationExperienceIfNecessary).toHaveBeenCalled();
            SpecHelper.expectElements(elem, 'education-experience-detail .experience-detail', 1);
        });
    });

    describe('when !formalEducationIsRequired', () => {
        beforeEach(() => {
            jest.spyOn(EditCareerProfileHelper, 'formalEducationIsRequired').mockReturnValue(false);
        });

        it('should not addDummyEducationExperienceIfNecessary at initial load', () => {
            user.career_profile.education_experiences = [];
            jest.spyOn(CareerProfile.prototype, 'addDummyEducationExperienceIfNecessary').mockImplementation(() => {});
            render();
            expect(EditCareerProfileHelper.formalEducationIsRequired).toHaveBeenCalledWith(
                scope.currentUser,
                scope.careerProfile,
            );
            expect(scope.careerProfile.addDummyEducationExperienceIfNecessary).not.toHaveBeenCalled();
            expect(scope.careerProfile.education_experiences).toEqual([]);
            SpecHelper.expectElements(elem, 'education-experience-detail .experience-detail', 0);
        });

        describe('when careerProfile hasLoneEmptyFormalEducationExperience', () => {
            it('should remove empty formal education experience', () => {
                user.career_profile.education_experiences = [
                    {
                        degreeProgram: true,
                    },
                ];
                const hasLoneEmptyFormalEducationExperienceSpy = jest
                    .spyOn(CareerProfile.prototype, 'hasLoneEmptyFormalEducationExperience', 'get')
                    .mockReturnValue(true);
                render();
                expect(scope.careerProfile.education_experiences).toEqual([]);
                expect(hasLoneEmptyFormalEducationExperienceSpy).toHaveBeenCalled();
            });
        });
    });

    describe('showHasNoFormalEducationCheckbox', () => {
        it('should be be false if no careerProfile', () => {
            render();
            scope.careerProfile = undefined;
            expect(scope.showHasNoFormalEducationCheckbox).toBe(false);
        });

        describe('when careerProfile is present', () => {
            it('should be false if formalEducationIsRequired', () => {
                render();
                jest.spyOn(EditCareerProfileHelper, 'formalEducationIsRequired').mockReturnValue(true);
                expect(scope.showHasNoFormalEducationCheckbox).toBe(false);
            });

            describe('when !formalEducationIsRequired', () => {
                it('should be false if there are any formalEducationExperiences', () => {
                    render();
                    jest.spyOn(scope.careerProfile, 'formalEducationExperiences', 'get').mockReturnValue([{}, {}, {}]);
                    expect(scope.showHasNoFormalEducationCheckbox).toBe(false);
                });

                describe('when careerProfile has formalEducationExperiences', () => {
                    beforeEach(() => {
                        jest.spyOn(CareerProfile.prototype, 'formalEducationExperiences', 'get').mockReturnValue([]);
                    });

                    it('should be false if highestLevelEducationRequiresFormalEducation', () => {
                        render();
                        jest.spyOn(
                            EditCareerProfileHelper,
                            'highestLevelEducationRequiresFormalEducation',
                        ).mockReturnValue(true);
                        expect(scope.showHasNoFormalEducationCheckbox).toBe(false);
                    });

                    describe('when !highestLevelEducationRequiresFormalEducation', () => {
                        it('should be true', () => {
                            render();
                            jest.spyOn(
                                EditCareerProfileHelper,
                                'highestLevelEducationRequiresFormalEducation',
                            ).mockReturnValue(false);
                            expect(scope.showHasNoFormalEducationCheckbox).toBe(true);
                        });
                    });
                });
            });
        });
    });

    describe('Formal Degree Programs section', () => {
        describe('sub-text', () => {
            it('should not mention message about transcripts if not supportsDocumentUpload program_type', () => {
                user.career_profile.program_type = 'foo';
                render();
                SpecHelper.expectNoElement(elem, '[name="if-accepted-provide-copies"]');
            });

            it('should not mention message about transcripts if not in the application context', () => {
                user.career_profile.program_type = 'mba';
                jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(false);
                render();
                SpecHelper.expectNoElement(elem, '[name="if-accepted-provide-copies"]');
            });

            it('should mention message about transcripts if a supportsDocumentUpload program_type and in the application context', () => {
                user.career_profile.program_type = 'mba';
                Object.defineProperty(user, 'programType', {
                    value: 'mba',
                });
                jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(true);
                render();
                SpecHelper.expectElementText(
                    elem,
                    '[name="if-accepted-provide-copies"]',
                    'Upon acceptance, we will require copies of official academic transcripts.',
                );
            });

            describe('when showHasNoFormalEducationCheckbox', () => {
                beforeEach(() => {
                    render();
                    jest.spyOn(scope, 'showHasNoFormalEducationCheckbox', 'get').mockReturnValue(true);
                    scope.$digest();
                });

                it('should have instructions to add formal degrees or check the has_no_formal_education checkbox', () => {
                    SpecHelper.expectNoElement(elem, '[name="education-instructions-without-checkbox"]');
                    SpecHelper.expectElementText(
                        elem,
                        '[name="education-instructions-when-checkbox-visible"]',
                        'Please list the formal degrees you have earned or enrolled in, beginning with your undergraduate degree or check the box below to indicate you have not enrolled in a degree program. Please do not include certificates, non-degree programs, or your high school diploma; you can add those in the next step.',
                    );
                });
            });

            describe('when !showHasNoFormalEducationCheckbox', () => {
                beforeEach(() => {
                    render();
                    jest.spyOn(scope, 'showHasNoFormalEducationCheckbox', 'get').mockReturnValue(false);
                    scope.$digest();
                });

                it('should have instructions to add formal degrees', () => {
                    SpecHelper.expectNoElement(elem, '[name="education-instructions-when-checkbox-visible"]');
                    SpecHelper.expectElementText(
                        elem,
                        '[name="education-instructions-without-checkbox"]',
                        'Please list the formal degrees you have earned or enrolled in, beginning with your undergraduate degree. Please do not include certificates, non-degree programs, or your high school diploma; you can add certificates and non-degree programs in the next step.',
                    );
                });
            });
        });

        describe('has_no_formal_education', () => {
            it('should be set to false if !showHasNoFormalEducationCheckbox when $watchCollection on education_experiences is triggered', () => {
                render();
                scope.careerProfile.has_no_formal_education = true;
                jest.spyOn(scope, 'showHasNoFormalEducationCheckbox', 'get').mockReturnValue(true);
                scope.careerProfile.education_experiences.push({});
                scope.$digest();
                expect(scope.careerProfile.has_no_formal_education).toBe(true);

                jest.spyOn(scope, 'showHasNoFormalEducationCheckbox', 'get').mockReturnValue(false);
                scope.careerProfile.education_experiences.push({});
                scope.$digest();
                expect(scope.careerProfile.has_no_formal_education).toBe(false);
            });

            describe('checkbox', () => {
                it('should be visible when showHasNoFormalEducationCheckbox', () => {
                    render();
                    const showHasNoFormalEducationCheckboxSpy = jest
                        .spyOn(scope, 'showHasNoFormalEducationCheckbox', 'get')
                        .mockReturnValue(true);
                    scope.$digest();
                    SpecHelper.expectElement(elem, 'input[type="checkbox"][name="has_no_formal_education"]');

                    showHasNoFormalEducationCheckboxSpy.mockReturnValue(false);
                    scope.$digest();
                    SpecHelper.expectNoElement(elem, 'input[type="checkbox"][name="has_no_formal_education"]');
                });

                it('should be wired to careerProfile.has_no_formal_education', () => {
                    user.career_profile.has_no_formal_education = false;
                    render();
                    jest.spyOn(scope, 'showHasNoFormalEducationCheckbox', 'get').mockReturnValue(true);
                    scope.$digest();
                    SpecHelper.expectElement(
                        elem,
                        'input[name="has_no_formal_education"][ng-model="careerProfile.has_no_formal_education"]',
                    );
                    SpecHelper.expectCheckboxUnchecked(
                        elem,
                        'input[name="has_no_formal_education"][ng-model="careerProfile.has_no_formal_education"]',
                    );
                    SpecHelper.checkCheckbox(
                        elem,
                        'input[name="has_no_formal_education"][ng-model="careerProfile.has_no_formal_education"]',
                    );
                    expect(scope.careerProfile.has_no_formal_education).toBe(true);
                });
            });
        });
    });

    describe('English Language Proficiency section', () => {
        it('should be visible if requiresEnglishLanguageProficiency, and !isCareerProfile', () => {
            jest.spyOn(Cohort, 'requiresEnglishLanguageProficiency').mockReturnValue(false);
            jest.spyOn(EditCareerProfileHelper, 'isCareerProfile').mockReturnValue(true);
            render();
            SpecHelper.expectNoElement(elem, '.english_language_proficiency');

            Cohort.requiresEnglishLanguageProficiency.mockReturnValue(true);
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.english_language_proficiency');

            Cohort.requiresEnglishLanguageProficiency.mockReturnValue(false);
            EditCareerProfileHelper.isCareerProfile.mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.english_language_proficiency');

            Cohort.requiresEnglishLanguageProficiency.mockReturnValue(true);
            EditCareerProfileHelper.isCareerProfile.mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectElement(elem, '.english_language_proficiency');
            SpecHelper.expectElementText(
                elem,
                '.english_language_proficiency .form-group:eq(0)',
                'English Language Proficiency',
            );
        });

        describe('when visible', () => {
            beforeEach(() => {
                jest.spyOn(Cohort, 'requiresEnglishLanguageProficiency').mockReturnValue(true);
                jest.spyOn(EditCareerProfileHelper, 'isCareerProfile').mockReturnValue(false);
            });

            describe('native_english_speaker field', () => {
                it('should work', () => {
                    render();
                    const fieldConfig = _.findWhere(scope.englishLangueProficiencyFieldConfigs, {
                        modelProp: 'native_english_speaker',
                    });
                    expect(scope.careerProfile.native_english_speaker).toBeUndefined();
                    expect(fieldConfig.fieldIsVisible()).toBe(true);
                    SpecHelper.expectElementText(
                        elem,
                        '.english_language_proficiency .form-group:eq(1) .control-title',
                        'I am a native English speaker.',
                    );
                    SpecHelper.expectElementText(
                        elem,
                        '.english_language_proficiency .form-group:eq(1) input[type="radio"]:eq(0) + .checkbox-group-label-wrapper span',
                        'Yes',
                    );
                    SpecHelper.expectElementText(
                        elem,
                        '.english_language_proficiency .form-group:eq(1) input[type="radio"]:eq(1) + .checkbox-group-label-wrapper span',
                        'No',
                    );
                    SpecHelper.toggleRadio(
                        elem,
                        '.english_language_proficiency .form-group:eq(1) input[type="radio"]:eq(0)',
                    );
                    expect(scope.careerProfile.native_english_speaker).toBe(true);
                    SpecHelper.toggleRadio(
                        elem,
                        '.english_language_proficiency .form-group:eq(1) input[type="radio"]:eq(1)',
                    );
                    expect(scope.careerProfile.native_english_speaker).toBe(false);
                });

                it('should add no-border style class if another field in this section is visible', () => {
                    render();
                    const anotherFieldConfig = _.findWhere(scope.englishLangueProficiencyFieldConfigs, {
                        modelProp: 'earned_accredited_degree_in_english',
                    });
                    jest.spyOn(anotherFieldConfig, 'fieldIsVisible').mockReturnValue(false);
                    scope.$digest();

                    const nativeEnglishSpeakerFormGroupElem = SpecHelper.expectElement(
                        elem,
                        '.english_language_proficiency .form-group.checkbox-group',
                    );
                    SpecHelper.expectElementDoesNotHaveClass(nativeEnglishSpeakerFormGroupElem, 'no-border');

                    anotherFieldConfig.fieldIsVisible.mockReturnValue(true);
                    scope.$digest();

                    SpecHelper.expectElements(elem, '.english_language_proficiency .form-group.checkbox-group', 2);
                    SpecHelper.expectElementHasClass(nativeEnglishSpeakerFormGroupElem, 'no-border');
                });
            });

            describe('earned_accredited_degree_in_english field', () => {
                it('should be visible if native_english_speaker is false', () => {
                    render();
                    const fieldConfig = _.findWhere(scope.englishLangueProficiencyFieldConfigs, {
                        modelProp: 'earned_accredited_degree_in_english',
                    });
                    expect(scope.careerProfile.native_english_speaker).toBeUndefined();
                    expect(fieldConfig.fieldIsVisible()).toBe(false);
                    SpecHelper.expectNoElement(elem, '.english_language_proficiency .form-group:eq(2)');
                    SpecHelper.toggleRadio(
                        elem,
                        '.english_language_proficiency .form-group:eq(1) input[type="radio"]:eq(1)',
                    );
                    expect(scope.careerProfile.native_english_speaker).toBe(false);
                    expect(fieldConfig.fieldIsVisible()).toBe(true);
                    SpecHelper.expectElement(elem, '.english_language_proficiency .form-group:eq(2)');
                    SpecHelper.expectElementText(
                        elem,
                        '.english_language_proficiency .form-group:eq(2) .control-title',
                        'I have earned a degree from an accredited university taught in English.',
                    );
                    SpecHelper.expectElementText(
                        elem,
                        '.english_language_proficiency .form-group:eq(2) input[type="radio"]:eq(0) + .checkbox-group-label-wrapper span',
                        'Yes',
                    );
                    SpecHelper.expectElementText(
                        elem,
                        '.english_language_proficiency .form-group:eq(2) input[type="radio"]:eq(1) + .checkbox-group-label-wrapper span',
                        'No',
                    );
                });

                it('should toggle modelProp', () => {
                    user.career_profile.native_english_speaker = false;
                    render();
                    expect(scope.careerProfile.earned_accredited_degree_in_english).toBeUndefined();
                    SpecHelper.toggleRadio(
                        elem,
                        '.english_language_proficiency .form-group:eq(2) input[type="radio"]:eq(0)',
                    );
                    expect(scope.careerProfile.earned_accredited_degree_in_english).toBe(true);
                    SpecHelper.expectNoElement(elem, '.english_language_proficiency .form-group:eq(2) .sub-text');
                    SpecHelper.toggleRadio(
                        elem,
                        '.english_language_proficiency .form-group:eq(2) input[type="radio"]:eq(1)',
                    );
                    expect(scope.careerProfile.earned_accredited_degree_in_english).toBe(false);
                });

                it('should unset english_work_experience_description and sufficient_english_work_experience when changing to true from false', () => {
                    user.career_profile.native_english_speaker = false;
                    user.career_profile.earned_accredited_degree_in_english = false;
                    user.career_profile.sufficient_english_work_experience = true;
                    user.career_profile.english_work_experience_description = 'Pedago LLC';
                    render();
                    SpecHelper.toggleRadio(
                        elem,
                        '.english_language_proficiency .form-group:eq(2) input[type="radio"]:eq(0)',
                    );
                    scope.$digest();
                    expect(scope.careerProfile.earned_accredited_degree_in_english).toBe(true);
                    expect(scope.careerProfile.sufficient_english_work_experience).toEqual(null);
                    expect(scope.careerProfile.english_work_experience_description).toEqual(null);
                });
            });

            describe('sufficient_english_work_experience', () => {
                it('should be visible if earned_accredited_degree_in_english is false', () => {
                    render();
                    const fieldConfig = _.findWhere(scope.englishLangueProficiencyFieldConfigs, {
                        modelProp: 'sufficient_english_work_experience',
                    });
                    expect(scope.careerProfile.native_english_speaker).toBeUndefined();
                    expect(scope.careerProfile.earned_accredited_degree_in_english).toBeUndefined();
                    expect(fieldConfig.fieldIsVisible()).toBe(false);
                    SpecHelper.expectNoElement(elem, '.english_language_proficiency .form-group:eq(3)');
                    SpecHelper.toggleRadio(
                        elem,
                        '.english_language_proficiency .form-group:eq(1) input[type="radio"]:eq(1)',
                    );
                    expect(scope.careerProfile.native_english_speaker).toBe(false);
                    expect(fieldConfig.fieldIsVisible()).toBe(false);
                    SpecHelper.toggleRadio(
                        elem,
                        '.english_language_proficiency .form-group:eq(2) input[type="radio"]:eq(1)',
                    );
                    expect(scope.careerProfile.earned_accredited_degree_in_english).toBe(false);
                    expect(fieldConfig.fieldIsVisible()).toBe(true);
                    SpecHelper.expectElement(elem, '.english_language_proficiency .form-group:eq(3)');
                    SpecHelper.expectElementText(
                        elem,
                        '.english_language_proficiency .form-group:eq(3) .control-title',
                        'Have you worked in a predominantly English-speaking workplace for five or more years?',
                    );
                    SpecHelper.expectElementText(
                        elem,
                        '.english_language_proficiency .form-group:eq(3) input[type="radio"]:eq(0) + .checkbox-group-label-wrapper span',
                        'Yes',
                    );
                    SpecHelper.expectElementText(
                        elem,
                        '.english_language_proficiency .form-group:eq(3) input[type="radio"]:eq(1) + .checkbox-group-label-wrapper span',
                        'No',
                    );
                });

                it('should show subText when sufficient_english_work_experience is false', () => {
                    user.career_profile.native_english_speaker = false;
                    user.career_profile.earned_accredited_degree_in_english = false;
                    user.career_profile.sufficient_english_work_experience = false;
                    jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
                    render();
                    SpecHelper.expectElementText(
                        elem,
                        '.english_language_proficiency .form-group:eq(3) .sub-text',
                        'Upon acceptance, we will require you to provide English language proficiency scores from the TOEFL or similarly recognized test.',
                    );
                });

                it('should show certificate subText when sufficient_english_work_experience is false and program type is the_business_certificate', () => {
                    user.career_profile.native_english_speaker = false;
                    user.career_profile.earned_accredited_degree_in_english = false;
                    user.career_profile.sufficient_english_work_experience = false;
                    jest.spyOn(user, 'programType', 'get').mockReturnValue('the_business_certificate');
                    render();
                    SpecHelper.expectElementText(
                        elem,
                        '.english_language_proficiency .form-group:eq(3) .sub-text',
                        'The certificate is taught exclusively in English. Candidates with strong English language proficiency are more likely to succeed.',
                    );
                });

                it('should toggle modelProp', () => {
                    user.career_profile.native_english_speaker = false;
                    user.career_profile.earned_accredited_degree_in_english = false;
                    jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
                    render();
                    expect(scope.careerProfile.sufficient_english_work_experience).toBeUndefined();
                    SpecHelper.toggleRadio(
                        elem,
                        '.english_language_proficiency .form-group:eq(3) input[type="radio"]:eq(0)',
                    );
                    expect(scope.careerProfile.sufficient_english_work_experience).toBe(true);
                    SpecHelper.toggleRadio(
                        elem,
                        '.english_language_proficiency .form-group:eq(3) input[type="radio"]:eq(1)',
                    );
                    expect(scope.careerProfile.sufficient_english_work_experience).toBe(false);
                });
            });

            describe('english_work_experience_description', () => {
                it('should be visible if sufficient_english_work_experience is true', () => {
                    render();
                    const fieldConfig = _.findWhere(scope.englishLangueProficiencyFieldConfigs, {
                        modelProp: 'english_work_experience_description',
                    });
                    expect(scope.careerProfile.native_english_speaker).toBeUndefined();
                    expect(scope.careerProfile.earned_accredited_degree_in_english).toBeUndefined();
                    expect(scope.sufficient_english_work_experience).toBeUndefined();
                    expect(fieldConfig.fieldIsVisible()).toBe(false);
                    SpecHelper.expectNoElement(elem, '.english_language_proficiency .form-group:eq(4)');
                    SpecHelper.toggleRadio(
                        elem,
                        '.english_language_proficiency .form-group:eq(1) input[type="radio"]:eq(1)',
                    );
                    expect(scope.careerProfile.native_english_speaker).toBe(false);
                    expect(fieldConfig.fieldIsVisible()).toBe(false);
                    SpecHelper.toggleRadio(
                        elem,
                        '.english_language_proficiency .form-group:eq(2) input[type="radio"]:eq(1)',
                    );
                    expect(scope.careerProfile.earned_accredited_degree_in_english).toBe(false);
                    expect(fieldConfig.fieldIsVisible()).toBe(false);
                    SpecHelper.toggleRadio(
                        elem,
                        '.english_language_proficiency .form-group:eq(3) input[type="radio"]:eq(0)',
                    );
                    expect(scope.careerProfile.sufficient_english_work_experience).toBe(true);
                    expect(fieldConfig.fieldIsVisible()).toBe(true);
                    SpecHelper.expectElement(elem, '.english_language_proficiency .form-group:eq(4)');
                    SpecHelper.expectElementText(
                        elem,
                        '.english_language_proficiency .form-group:eq(4) .control-title',
                        'What company or companies?',
                    );
                    SpecHelper.expectElement(
                        elem,
                        '.english_language_proficiency .form-group:eq(4) input[type="text"]',
                    );
                });

                it('should allow editing modelProp', () => {
                    user.career_profile.native_english_speaker = false;
                    user.career_profile.earned_accredited_degree_in_english = false;
                    user.career_profile.sufficient_english_work_experience = true;
                    render();
                    expect(scope.careerProfile.english_work_experience_description).toBeUndefined();
                    SpecHelper.updateTextInput(
                        elem,
                        '.english_language_proficiency .form-group:eq(4) input[type="text"]',
                        'Pedago, LLC',
                    );
                    expect(scope.careerProfile.english_work_experience_description).toEqual('Pedago, LLC');
                });
            });
        });
    });

    describe('Test Scores', () => {
        it('should only be visible if earlyCareerApplicant', () => {
            render();
            scope.earlyCareerApplicant = true;
            scope.$digest();
            SpecHelper.expectElement(elem, '.test-scores');

            scope.earlyCareerApplicant = false;
            scope.$digest();
            SpecHelper.expectNoElement(elem, '.test-scores');
        });

        it('should have international-friendly wording', () => {
            render();
            scope.earlyCareerApplicant = true;
            scope.$digest();
            SpecHelper.expectElementText(
                elem,
                '.test-scores .sub-text:eq(0)',
                'Standardized tests can vary from country to country. This section is optional. Please note that GMAT and GRE scores should be no more than 5 years old.',
            );
        });

        describe('SAT fields', () => {
            beforeEach(() => {
                render();
                scope.earlyCareerApplicant = true;
                scope.$digest();
            });

            describe('sat_max_score', () => {
                it('should be valid if scoreOnSAT is null', () => {
                    expect(scope.careerProfile.scoreOnSAT).toBeNull();
                    const satMaxScoreElem = SpecHelper.expectElement(elem, '[name="sat-max-score"]');
                    expect(scope.careerProfile.sat_max_score).toEqual('1600');
                    SpecHelper.expectElementHasClass(satMaxScoreElem, 'ng-valid-sat-max-score');
                });

                it('should be valid if scoreOnSAT is greater than the highest possible SAT score (2400)', () => {
                    expect(scope.careerProfile.scoreOnSAT).toBeNull();
                    expect(scope.careerProfile.sat_max_score).toEqual('1600');
                    const scoreOnSatElem = SpecHelper.expectElement(elem, '[name="score-on-sat"]');
                    const satMaxScoreElem = SpecHelper.expectElement(elem, '[name="sat-max-score"]');
                    SpecHelper.expectElementHasClass(scoreOnSatElem, 'ng-valid-number');

                    // Set scoreOnSAT to be greater than the current sat_max_score
                    // value and assert scoreOnSAT field is valid
                    SpecHelper.updateTextInput(elem, '[name="score-on-sat"]', 2500);
                    expect(scope.careerProfile.scoreOnSAT).toEqual(2500);
                    SpecHelper.expectElementHasClass(scoreOnSatElem, 'ng-invalid-number');
                    SpecHelper.expectElementHasClass(satMaxScoreElem, 'ng-valid-sat-max-score');
                });

                it('should be invalid if scoreOnSAT is less than or equal to the highest possible SAT score (2400) and greater than the sat_max_score', () => {
                    expect(scope.careerProfile.scoreOnSAT).toBeNull();
                    expect(scope.careerProfile.sat_max_score).toEqual('1600');
                    const scoreOnSatElem = SpecHelper.expectElement(elem, '[name="score-on-sat"]');
                    const satMaxScoreElem = SpecHelper.expectElement(elem, '[name="sat-max-score"]');
                    SpecHelper.expectElementHasClass(scoreOnSatElem, 'ng-valid-number');

                    // Set scoreOnSAT to be greater than the current sat_max_score
                    // value and assert scoreOnSAT field is valid
                    SpecHelper.updateTextInput(elem, '[name="score-on-sat"]', 2000);
                    expect(scope.careerProfile.scoreOnSAT).toEqual(2000);
                    SpecHelper.expectElementHasClass(scoreOnSatElem, 'ng-valid-number');
                    SpecHelper.expectElementHasClass(satMaxScoreElem, 'ng-invalid-sat-max-score');
                });
            });
        });
    });

    // invalidFieldsLinks dir
    it('should render labels/labelText for missing required fields when blank form is submitted', () => {
        jest.spyOn(Cohort, 'requiresEnglishLanguageProficiency').mockReturnValue(true);
        jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(true);
        jest.spyOn(EditCareerProfileHelper, 'isCareerProfile').mockReturnValue(false);
        jest.spyOn(EditCareerProfileHelper, 'requiresEnglishLanguageProficiency').mockReturnValue(true);
        jest.spyOn(EditCareerProfileHelper, 'supportsRequiredEducation').mockReturnValue(true);
        user.career_profile.survey_highest_level_completed_education_description = null;
        user.career_profile.education_experiences = [];
        user.career_profile.has_no_formal_education = null;
        user.career_profile.native_english_speaker = null;

        render();
        jest.spyOn(scope, 'showHasNoFormalEducationCheckbox', 'get').mockReturnValue(true);
        parentScope.$digest();
        parentScope.save();
        parentScope.$digest();

        expect(parentScope.invalidFields.length).toBe(7);

        // items don't order as expected because in headless Chrome they have an offset = {top: 0, left: 0}
        SpecHelper.expectElementText(
            elem,
            'invalid-fields-links .field:eq(0)',
            'My highest level of COMPLETED education is best described as…',
        );
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(1)', 'I am a native English speaker.');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(2)', 'Major or Specialization');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(3)', 'Degree');
        SpecHelper.expectElementText(
            elem,
            'invalid-fields-links .field:eq(4)',
            'I have graduated or expect to graduate with this degree:',
        );
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(5)', 'University/College');
        SpecHelper.expectElementText(
            elem,
            'invalid-fields-links .field:eq(6)',
            'I have never enrolled in a formal degree program.',
        );

        scope.careerProfile.native_english_speaker = false;
        scope.careerProfile.earned_accredited_degree_in_english = null;
        parentScope.$digest();
        parentScope.save();
        parentScope.$digest();

        SpecHelper.expectElementText(
            elem,
            'invalid-fields-links .field:eq(6)',
            'I have earned a degree from an accredited university taught in English.',
        );

        scope.careerProfile.earned_accredited_degree_in_english = false;
        scope.careerProfile.sufficient_english_work_experience = null;
        parentScope.$digest();
        parentScope.save();
        parentScope.$digest();

        SpecHelper.expectElementText(
            elem,
            'invalid-fields-links .field:eq(6)',
            'Have you worked in a predominantly English-speaking workplace for five or more years?',
        );

        scope.careerProfile.sufficient_english_work_experience = true;
        scope.careerProfile.english_work_experience_description = null;
        parentScope.$digest();
        parentScope.save();
        parentScope.$digest();

        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(6)', 'What company or companies?');
    });
});
