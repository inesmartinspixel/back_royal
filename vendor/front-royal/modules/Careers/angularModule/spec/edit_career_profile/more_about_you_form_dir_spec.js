import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import setSpecLocales from 'Translation/setSpecLocales';
import moreAboutYouFormLocales from 'Careers/locales/careers/edit_career_profile/more_about_you_form-en.json';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import profilePhotoFormSectionLocales from 'Careers/locales/careers/edit_career_profile/profile_photo_form_section-en.json';
import uploadAvatarLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/upload_avatar-en.json';
import invalidFieldsLinksLocales from 'FrontRoyalForm/locales/front_royal_form/invalid_fields_links-en.json';
import frontRoyalFormLocales from 'FrontRoyalForm/locales/front_royal_form/front_royal_form-en.json';
import addAnItemLocales from 'FrontRoyalForm/locales/front_royal_form/inputs/add_an_item-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';

setSpecLocales(
    frontRoyalFormLocales,
    editCareerProfileLocales,
    moreAboutYouFormLocales,
    profilePhotoFormSectionLocales,
    uploadAvatarLocales,
    invalidFieldsLinksLocales,
    addAnItemLocales,
    fieldOptionsLocales,
);

describe('FrontRoyal.Careers.EditCareerProfile.MoreAboutYouForm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let StudentDashboard;
    let user;
    let CareerProfile;
    let EditCareerProfileHelper;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                StudentDashboard = $injector.get('StudentDashboard');
                CareerProfile = $injector.get('CareerProfile');
                EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                $timeout = $injector.get('$timeout');

                $injector.get('CareerProfileFixtures');
                SpecHelper.stubConfig();
            },
        ]);

        SpecHelper.stubEventLogging();

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
    });

    function render() {
        StudentDashboard.expect('index').toBeCalledWith({
            get_has_available_incomplete_streams: true,
            get_has_available_incomplete_playlists: true,
            user_id: user.id,
            filters: {},
        });

        renderer = SpecHelper.renderer();
        renderer.scope.steps = _.filter(
            EditCareerProfileHelper.getStepsForCareersForm(user),
            step => step.stepName === 'more_about_you',
        );
        renderer.render('<edit-career-profile steps="steps"></edit-career-profile>');
        $timeout.flush(); // apply the scope.steps assignment
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should be wired to careerProfile', () => {
        render();
        expect(scope.careerProfile).toBeDefined();
    });

    it('should call logEventForApplication when initialized', () => {
        jest.spyOn(EditCareerProfileHelper, 'logEventForApplication').mockImplementation(() => {});
        render();
        expect(EditCareerProfileHelper.logEventForApplication).toHaveBeenCalledWith('viewed-more_about_you');
    });

    // invalidFieldsLinks dir
    it('should render labels/labelText for missing required fields when blank form is submitted', () => {
        jest.spyOn(user, 'isEMBA', 'get').mockReturnValue(true);
        jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(true);
        jest.spyOn(EditCareerProfileHelper, 'isCareerProfile').mockReturnValue(true);
        user.career_profile.avatar_url = null;
        user.career_profile.primary_reason_for_applying = null;
        user.career_profile.top_mba_subjects = [];
        user.career_profile.top_personal_descriptors = [];
        user.career_profile.top_motivations = [];
        user.career_profile.top_workplace_strengths = [];
        user.career_profile.awards_and_interests = [];

        render();
        scope.save();
        scope.$digest();

        expect(scope.invalidFields.length).toBe(7);

        // items don't order as expected because in headless Chrome they have an offset = {top: 0, left: 0}
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(0)', 'Profile Photo');
        SpecHelper.expectElementText(
            elem,
            'invalid-fields-links .field:eq(1)',
            'What is the primary reason you applied to the Quantic EMBA?',
        );
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(2)', 'My Attributes');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(3)', 'My Motivations');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(4)', 'My Strengths');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(5)', 'Awards & Interests');
        SpecHelper.expectElementText(elem, 'invalid-fields-links .field:eq(6)', 'Subject Areas');
    });

    describe('profile-photo-form-section', () => {
        it('should be visible if isCareerProfile', () => {
            jest.spyOn(EditCareerProfileHelper, 'isCareerProfile').mockReturnValue(true);
            render();
            SpecHelper.expectElement(elem, 'profile-photo-form-section');

            EditCareerProfileHelper.isCareerProfile.mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectNoElement(elem, 'profile-photo-form-section');
        });

        it('should be visible if isApplicantEditor', () => {
            jest.spyOn(EditCareerProfileHelper, 'isApplicantEditor').mockReturnValue(true);
            render();
            SpecHelper.expectElement(elem, 'profile-photo-form-section');

            EditCareerProfileHelper.isApplicantEditor.mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectNoElement(elem, 'profile-photo-form-section');
        });
    });

    describe('primary_reason_for_applying field', () => {
        it('should be visible if isMBA', () => {
            jest.spyOn(user, 'isEMBA', 'get').mockReturnValue(false); // prevent false positive
            const spy = jest.spyOn(user, 'isMBA', 'get').mockReturnValue(true);
            render();
            SpecHelper.expectElement(elem, '#primary-reason-for-applying');

            spy.mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectNoElement(elem, '#primary-reason-for-applying');
        });

        it('should be visible if isMBA', () => {
            jest.spyOn(user, 'isMBA', 'get').mockReturnValue(false); // prevent false positive
            const spy = jest.spyOn(user, 'isEMBA', 'get').mockReturnValue(true);
            render();
            SpecHelper.expectElement(elem, '#primary-reason-for-applying');

            spy.mockReturnValue(false);
            scope.$digest();
            SpecHelper.expectNoElement(elem, '#primary-reason-for-applying');
        });

        describe('when isMBA', () => {
            beforeEach(() => {
                jest.spyOn(user, 'isMBA', 'get').mockReturnValue(true);
                jest.spyOn(user, 'isEMBA', 'get').mockReturnValue(false);
            });

            it('should have contextual messaging', () => {
                render();
                SpecHelper.expectElementText(
                    elem,
                    '#primary-reason-for-applying label',
                    'What is the primary reason you applied to the Quantic MBA?',
                );
            });
        });

        describe('when isEMBA', () => {
            beforeEach(() => {
                jest.spyOn(user, 'isMBA', 'get').mockReturnValue(false);
                jest.spyOn(user, 'isEMBA', 'get').mockReturnValue(true);
            });

            it('should have contextual messaging', () => {
                render();
                SpecHelper.expectElementText(
                    elem,
                    '#primary-reason-for-applying label',
                    'What is the primary reason you applied to the Quantic EMBA?',
                );
            });
        });
    });
});
