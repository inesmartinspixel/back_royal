import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import setSpecLocales from 'Translation/setSpecLocales';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import personalSummaryFormLocales from 'Careers/locales/careers/edit_career_profile/personal_summary_form-en.json';

setSpecLocales(editCareerProfileLocales, personalSummaryFormLocales);

describe('FrontRoyal.Careers.EditCareerProfile.PeerRecommendations', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let CareerProfile;
    let careerProfile;
    let maxNumPeerRecommendations;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');

                $injector.get('CareerProfileFixtures');
            },
        ]);

        careerProfile = CareerProfile.fixtures.getInstance();
        maxNumPeerRecommendations = 3;
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.careerProfile = careerProfile;
        renderer.render('<peer-recommendations career-profile="careerProfile"></peer-recommendations>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should default peer_recommendations to array with blank objects if not set', () => {
        careerProfile.peer_recommendations = null;
        render();
        // the peer_recommendations array should be set upon initialization of the directive
        expect(scope.careerProfile.peer_recommendations).toBeTruthy();
        // each of the objects in the newly created peer_recommendations array should be blank
        for (let i = 1; i <= maxNumPeerRecommendations; i++) {
            expect(scope.careerProfile.peer_recommendations[i - 1]).toEqual({
                $$hashKey: expect.anything(), // this is added by angular, but other than this the object should be empty
            });
        }
    });

    it('should have an input element for each peer recommendation up to the max num allowed', () => {
        const existingPeerRecommendation = {
            id: 'some id',
            email: 'johnny.appleseed@foo.com',
            contacted: true,
        };
        careerProfile.peer_recommendations = [existingPeerRecommendation];
        render();

        SpecHelper.expectElements(elem, 'input[type="email"]', maxNumPeerRecommendations);

        // the first one should be filled in
        SpecHelper.expectTextInputVal(elem, 'input[type="email"]:eq(0)', 'johnny.appleseed@foo.com');

        // all others up to maxNumPeerRecommendations should be blank
        for (let i = 2; i <= maxNumPeerRecommendations; i++) {
            SpecHelper.expectTextInputVal(elem, `input[type="email"]:eq(${i - 1})`, '');
        }
    });

    it('should populate empty indexes in peer_recommendations array with blank objects upon initialization', () => {
        const existingPeerRecommendation = {
            id: 'some id',
            email: 'johnny.appleseed@foo.com',
            contacted: true,
        };
        careerProfile.peer_recommendations = [existingPeerRecommendation];
        render();

        // the first index should contain existingPeerRecommendation
        expect(scope.careerProfile.peer_recommendations[0]).toEqual(existingPeerRecommendation);

        // all others up to maxNumPeerRecommendations should be blank peer recommendations
        for (let i = 2; i <= maxNumPeerRecommendations; i++) {
            expect(scope.careerProfile.peer_recommendations[i - 1]).toEqual({
                $$hashKey: expect.anything(), // this is added by angular, but other than this the object should be empty
            });
        }
    });

    it('should sanitizePeerRecommendations on $destroy', () => {
        const existingPeerRecommendation = {
            id: 'some id',
            email: 'johnny.appleseed@foo.com',
            contacted: true,
        };
        careerProfile.peer_recommendations = [existingPeerRecommendation];
        render();

        jest.spyOn(scope.careerProfile, 'sanitizePeerRecommendations').mockImplementation(() => {});
        scope.$destroy();
        expect(scope.careerProfile.sanitizePeerRecommendations).toHaveBeenCalled();
    });

    it('should repopulate the peer_recommendations array if the length does not equal the max num allowed', () => {
        render();
        expect(scope.careerProfile.peer_recommendations.length).toEqual(maxNumPeerRecommendations);

        jest.spyOn(scope, 'addEmptyPeerRecommendations').mockImplementation(() => {});
        scope.careerProfile.peer_recommendations = [];
        scope.$digest();
        expect(scope.addEmptyPeerRecommendations).toHaveBeenCalled();
    });

    describe('addEmptyPeerRecommendations', () => {
        it('should populate empty indexes in peer_recommendations array with empty objects', () => {
            render();
            expect(scope.careerProfile.peer_recommendations.length).toEqual(maxNumPeerRecommendations);

            jest.spyOn(scope, 'addEmptyPeerRecommendations');

            // remove all peer recommendations in preparation to assert if addEmptyPeerRecommendations works as expected
            scope.careerProfile.peer_recommendations = [];
            scope.$digest();

            expect(scope.addEmptyPeerRecommendations).toHaveBeenCalled();

            // all indexes in peer_recommendations array up to maxNumPeerRecommendations should be blank peer recommendations
            for (let i = 1; i <= maxNumPeerRecommendations; i++) {
                expect(scope.careerProfile.peer_recommendations[i - 1]).toEqual({
                    $$hashKey: expect.anything(), // this is added by angular, but other than this the object should be empty
                });
            }
        });
    });
});
