import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import setSpecLocales from 'Translation/setSpecLocales';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';
import jobPreferencesFormLocales from 'Careers/locales/careers/edit_career_profile/job_preferences_form-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';

setSpecLocales(candidateListCardLocales, jobPreferencesFormLocales, fieldOptionsLocales);

describe('FrontRoyal.Careers.CandidateListCard', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let careerProfile;
    let userOverrides;
    let currentUser;
    let Cohort;
    let CareersNetworkViewModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                const CareerProfile = $injector.get('CareerProfile');
                $injector.get('CareerProfileFixtures');
                Cohort = $injector.get('Cohort');
                $injector.get('CohortFixtures');
                CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');

                careerProfile = CareerProfile.fixtures.getInstance();
            },
        ]);

        currentUser = SpecHelper.stubCurrentUser(undefined, undefined, undefined, userOverrides);
        currentUser.relevant_cohort = Cohort.fixtures.getInstance();
    });

    function render(_careerProfile) {
        renderer = SpecHelper.renderer();
        renderer.scope.careerProfile = _careerProfile || careerProfile;
        renderer.render('<candidate-list-card career-profile="careerProfile"></candidate-list-card>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should have avatar_url if set', () => {
        render();
        expect(elem.find('careers-avatar').isolateScope().fullSrcUrl).toEqual(
            `url(http://path/to/candidates_avatar.png), url(${$injector.get('DEFAULT_PROFILE_PHOTO')})`,
        );
    });

    describe('status', () => {
        const statuses = [
            {
                key: 'very_interested',
                text: 'Actively seeking a new position (Permanent, Part-Time, Contract or Internship)',
            },
            {
                key: 'interested',
                text: 'Passively seeking a new position (Permanent, Part-Time, Contract or Internship)',
            },
            {
                key: 'neutral',
                text: 'Open to connections (Permanent, Part-Time, Contract or Internship)',
            },
        ];

        statuses.forEach(status => {
            it(`should show ${status.key}text`, () => {
                careerProfile.interested_in_joining_new_company = status.key;
                render(careerProfile);
                SpecHelper.expectElementText(elem, '.status .tooltip', status.text);
            });
        });

        it('should not show tooltip text when no interested_in_joining_new_company', () => {
            careerProfile.interested_in_joining_new_company = null;
            render(careerProfile);
            SpecHelper.expectNoElement(elem, '.status .tooltip');
        });
    });

    describe('name and avatar', () => {
        beforeEach(() => {
            userOverrides = {
                pref_show_photos_names: false,
            };
            render();
        });

        it("should show the candidate's full name if pref_show_photos_names is true", () => {
            expect(scope.userInfo.displayName).toEqual(scope.careerProfile.name);
        });

        it("should show the candidate's initials if pref_show_photos_names is false", () => {
            render();
            expect(scope.userInfo.displayName).toEqual(scope.careerProfile.userInitials);
        });

        it("should show the candidate's avatar if pref_show_photos_names is true", () => {
            // I do not understnad why this is only needed here
            jest.spyOn(CareersNetworkViewModel.prototype, '_preloadHiringRelationships').mockImplementation(() => {});
            currentUser = SpecHelper.stubCurrentUser();
            currentUser.relevant_cohort = Cohort.fixtures.getInstance();
            render();
            expect(scope.userInfo.avatarUrl).toBe(scope.careerProfile.avatar_url);
        });

        it('should show default profile avatar if pref_show_photos_names is false', () => {
            SpecHelper.stubCurrentUser(undefined, undefined, undefined, userOverrides);
            scope.careerProfile.avatar_url = null;
            scope.$digest();
            expect(elem.find('careers-avatar').isolateScope().fullSrcUrl).toEqual(
                `url(${$injector.get('DEFAULT_PROFILE_PHOTO')})`,
            );
        });
    });

    describe('links', () => {
        function renderAndSpy() {
            render();
            jest.spyOn(CareersNetworkViewModel.prototype, 'showHiringBillingModal').mockImplementation(() => {});
            jest.spyOn(scope, 'loadUrl').mockImplementation(() => {});
        }

        it('should NOT have resume button if resume DOES NOT exist', () => {
            jest.spyOn(careerProfile, 'resumeUrl', 'get').mockReturnValue(undefined);
            renderAndSpy();
            SpecHelper.expectNoElement(elem, '.name-links button[name=resume]');
        });

        it('should have resume button if resume exists', () => {
            jest.spyOn(careerProfile, 'resumeUrl', 'get').mockReturnValue('http://example.com');
            renderAndSpy();
            SpecHelper.click(elem, '.name-links button[name=resume]');
            expect(CareersNetworkViewModel.prototype.showHiringBillingModal).not.toHaveBeenCalled();
            expect(scope.loadUrl).toHaveBeenCalledWith('http://example.com', '_blank');
        });

        it('should NOT have GitHub button if GitHub url DOES NOT exist', () => {
            jest.spyOn(careerProfile, 'githubUrl', 'get').mockReturnValue(undefined);
            renderAndSpy();
            SpecHelper.expectNoElement(elem, '.name-links button[name=github]');
        });

        it('should have GitHub button if GitHub url exists', () => {
            jest.spyOn(careerProfile, 'githubUrl', 'get').mockReturnValue('http://example.com');
            renderAndSpy();
            SpecHelper.click(elem, '.name-links button[name=github]');
            expect(CareersNetworkViewModel.prototype.showHiringBillingModal).not.toHaveBeenCalled();
            expect(scope.loadUrl).toHaveBeenCalledWith('http://example.com', '_blank');
        });

        it('should have a resume button that opens a billing modal if anonymized', () => {
            careerProfile.anonymized = true;
            jest.spyOn(careerProfile, 'resumeUrl', 'get').mockReturnValue(undefined);
            renderAndSpy();
            SpecHelper.click(elem, '[name="resume"]');
            expect(CareersNetworkViewModel.prototype.showHiringBillingModal).toHaveBeenCalledWith(
                scope.careerProfile,
                'openExternalLink',
            );
            expect(scope.loadUrl).not.toHaveBeenCalled();
        });
    });

    describe('expanded view', () => {
        describe('seeking', () => {
            const seekingRoles = 'side-kick or mad-scientist';
            const seekingIndustries = 'crime-fighting or biotechnology';
            const expectedSeekingText =
                'side-kick or mad-scientist roles in the crime-fighting or biotechnology industries';

            beforeEach(() => {
                currentUser = SpecHelper.stubCurrentUser('admin');
                currentUser.relevant_cohort = Cohort.fixtures.getInstance();
            });

            it('should display the seeking section if data is available', () => {
                renderWithSeekingInfo(seekingRoles, seekingIndustries);
                SpecHelper.expectElementText(elem, '.full-profile .seeking .experience', expectedSeekingText);
            });

            it('should hide the seeking section if data is unavailable', () => {
                renderWithSeekingInfo();
                SpecHelper.expectNoElement(elem, '.full-profile .seeking');
            });

            it('should only be visible to hiring managers or admins', () => {
                currentUser = SpecHelper.stubCurrentUser('learner');
                renderWithSeekingInfo(seekingRoles, seekingIndustries);
                SpecHelper.expectNoElement(elem, '.full-profile .seeking');

                currentUser = SpecHelper.stubCurrentUser('editor');
                renderWithSeekingInfo(seekingRoles, seekingIndustries);
                SpecHelper.expectNoElement(elem, '.full-profile .seeking');

                currentUser = SpecHelper.stubCurrentUser('admin');
                renderWithSeekingInfo(seekingRoles, seekingIndustries);
                SpecHelper.expectElementText(elem, '.full-profile .seeking .experience', expectedSeekingText);

                currentUser = SpecHelper.stubCurrentUser('learner');
                jest.spyOn(currentUser, 'hasHiringManagerAccess', 'get').mockReturnValue(true);
                renderWithSeekingInfo(seekingRoles, seekingIndustries);
                SpecHelper.expectElementText(elem, '.full-profile .seeking .experience', expectedSeekingText);
            });

            function renderWithSeekingInfo(roles, industries) {
                render();
                scope.showFullProfile = true;
                scope.userInfo.seekingRoles = roles;
                scope.userInfo.seekingIndustries = industries;
                scope.$digest();
            }
        });
    });
});
