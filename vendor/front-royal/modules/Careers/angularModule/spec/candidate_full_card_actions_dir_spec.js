import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/candidate_position_interest_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Careers/angularModule/spec/careers_spec_helper';
import setSpecLocales from 'Translation/setSpecLocales';
import fullCardActionsLocales from 'Careers/locales/careers/candidate_list_card-en.json';
import hideCandidateConfirmLocales from 'Careers/locales/careers/hide_candidate_confirm-en.json';

setSpecLocales(fullCardActionsLocales);
setSpecLocales(hideCandidateConfirmLocales);

describe('FrontRoyal.Careers.CandidateFullCardActions', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let careerProfile;
    let userOverrides;
    let DialogModal;
    let CandidatePositionInterest;
    let CareerProfile;
    let careersNetworkViewModel;
    let CareersSpecHelper;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper', 'CareersSpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;

                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                DialogModal = $injector.get('DialogModal');
                CandidatePositionInterest = $injector.get('CandidatePositionInterest');
                CareerProfile = $injector.get('CareerProfile');

                $injector.get('CareerProfileFixtures');
                $injector.get('CandidatePositionInterestFixtures');

                careerProfile = CareerProfile.fixtures.getInstance();
            },
        ]);

        SpecHelper.stubCurrentUser(undefined, undefined, undefined, userOverrides);
        careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.ngModel = opts.ngModel || careerProfile;
        renderer.scope.onFinish = jest.fn();
        renderer.scope.showHideButton = angular.isDefined(opts.showHideButton) ? opts.showHideButton : true;
        renderer.scope.candidatePositionInterest = opts.candidatePositionInterest;
        renderer.scope.adminMode = opts.adminMode;
        renderer.render(
            '<candidate-full-card-actions ng-model="ngModel" on-finish="onFinish" candidate-position-interest="candidatePositionInterest" show-hide-button="showHideButton"></candidate-full-card-actions>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('careerProfile', () => {
        it('should set career profile to the ngModel if it is a CareerProfile', () => {
            render();
            expect(scope.careerProfile).toBe(scope.ngModel);
        });

        it('should get career profile from the hiring relationship if ngModel is a HiringRelationshipViewModel', () => {
            const hrvm = careersNetworkViewModel.hiringRelationshipViewModels[0];
            render({
                ngModel: hrvm,
            });
            expect(scope.careerProfile).toBe(hrvm.careerProfile);
        });
    });

    describe('actions', () => {
        ['accepted', 'rejected', 'save_for_later'].forEach(status => {
            it('should update status and remove card', () => {
                render();
                scope.currentUser.has_seen_hide_candidate_confirm = true;
                scope.updateStatus(status);
                expect(scope.onFinish).toHaveBeenCalled();
            });
        });

        ['unseen', 'pending', 'saved_for_later'].forEach(status => {
            it('should show hide action', () => {
                render({
                    candidatePositionInterest: CandidatePositionInterest.fixtures.getInstance({
                        hiring_manager_status: status,
                    }),
                    showHideButton: true,
                });
                scope.$digest();

                SpecHelper.expectElement(elem, '.pass');
            });
        });

        ['unseen', 'pending', 'saved_for_later', 'rejected'].forEach(status => {
            it('should show share action', () => {
                render({
                    candidatePositionInterest: CandidatePositionInterest.fixtures.getInstance({
                        hiring_manager_status: status,
                    }),
                });
                scope.$digest();

                SpecHelper.expectElement(elem, '.share');
            });
        });

        it('should ask user to confirm before hiding', () => {
            render();
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
            scope.updateStatus('rejected');
            expect(DialogModal.alert).toHaveBeenCalled();
        });

        it('should call share method', () => {
            render();
            jest.spyOn(careersNetworkViewModel, 'showShareModal').mockImplementation(() => {});
            scope.share();
            expect(careersNetworkViewModel.showShareModal).toHaveBeenCalledWith(careerProfile);
        });
    });

    describe('when anonymized', () => {
        beforeEach(() => {
            render();
            scope.careerProfile.anonymized = true;
            jest.spyOn(scope.careersNetworkViewModel, 'showHiringBillingModal').mockImplementation(() => {});
        });

        it('should show billing modal when calling updateStatus', () => {
            SpecHelper.click(elem, '.save');
            expect(scope.onFinish).not.toHaveBeenCalled();
            expect(scope.careersNetworkViewModel.showHiringBillingModal).toHaveBeenCalledWith(
                scope.careerProfile,
                'updateStatus:saved_for_later',
            );
        });

        it('should show billing modal when sharing', () => {
            scope.showShare = true;
            scope.$digest();
            SpecHelper.click(elem, '.share');
            expect(scope.onFinish).not.toHaveBeenCalled();
            expect(scope.careersNetworkViewModel.showHiringBillingModal).toHaveBeenCalledWith(
                scope.careerProfile,
                'share',
            );
        });
    });
});
