import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';
import 'Careers/angularModule/spec/careers_spec_helper';
import setSpecLocales from 'Translation/setSpecLocales';
import editCareerProfileLocales from 'Careers/locales/careers/edit_career_profile-en.json';
import hiringBrowseCandidatesLocales from 'Careers/locales/careers/hiring_browse_candidates-en.json';
import careersLocales from 'Careers/locales/careers/careers-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import selectSkillsFormLocales from 'Careers/locales/careers/edit_career_profile/select_skills_form-en.json';

setSpecLocales(hiringBrowseCandidatesLocales, careersLocales, fieldOptionsLocales, selectSkillsFormLocales);
setSpecLocales(editCareerProfileLocales);

describe('FrontRoyal.Careers.HiringBrowseCandidatesDir', () => {
    let $injector;
    let SpecHelper;
    let CareersSpecHelper;
    let renderer;
    let elem;
    let scope;
    let currentUser;
    let CareerProfileFilterSet;
    let $timeout;
    let CareerProfileSearch;
    let HiringApplication;
    let $rootScope;
    let DialogModal;
    let CareerProfileList;
    let CareerProfile;
    let careerProfiles;
    let HiringRelationship;
    let CareersNetworkViewModel;
    let $q;
    let $location;
    let EventLogger;
    let listOffsetHelper;
    let HiringTeam;
    let hasAcceptedHiringManagerAccess;
    let hasCompleteHiringApplication;
    let hasFullHiringManagerAccess;
    let guid;
    let locationPlaceDetails;
    let $window;
    let hasSourcingAccess;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper');

        angular.mock.module($provide => {
            $window = {
                document: window.document,
                google: {
                    maps: {
                        places: {
                            Autocomplete() {},
                        },
                        event: {
                            addListener() {},
                        },
                    },
                },
            };

            $provide.value('$window', $window);
        });

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                CareerProfileFilterSet = $injector.get('CareerProfileFilterSet');
                CareerProfileSearch = $injector.get('CareerProfileSearch');
                HiringApplication = $injector.get('HiringApplication');
                HiringRelationship = $injector.get('HiringRelationship');
                CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
                $q = $injector.get('$q');
                $timeout = $injector.get('$timeout');
                $rootScope = $injector.get('$rootScope');
                $location = $injector.get('$location');
                DialogModal = $injector.get('DialogModal');
                CareerProfileList = $injector.get('CareerProfileList');
                CareerProfile = $injector.get('CareerProfile');
                EventLogger = $injector.get('EventLogger');
                listOffsetHelper = $injector.get('listOffsetHelper');
                HiringTeam = $injector.get('HiringTeam');
                $injector.get('HiringTeamFixtures');
                guid = $injector.get('guid');
                locationPlaceDetails = $injector.get('LOCATION_PLACE_DETAILS');
                $window = $injector.get('$window');

                $injector.get('CareerProfileFixtures');

                SpecHelper.stubDirective('locationAutocomplete');
            },
        ]);

        currentUser = SpecHelper.stubCurrentUser();
        currentUser.hiring_application = HiringApplication.new();
        hasSourcingAccess = true;
        jest.spyOn(currentUser, 'hasSourcingAccess', 'get').mockImplementation(() => hasSourcingAccess);
        mockFullAccess();

        SpecHelper.stubDirective('candidateList');
        SpecHelper.stubDirective('candidateListCard');
        SpecHelper.stubDirective('sourcingUpsell');
        SpecHelper.stubEventLoggingHarder();
        CareerProfile.expect('index');
    });

    function render() {
        HiringRelationship.expect('index');
        renderer = SpecHelper.renderer();
        renderer.render('<hiring-browse-candidates></hiring-browse-candidates>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should set has_seen_welcome to true if hiring application complete and first time', () => {
        $rootScope.currentUser.hiring_application.has_seen_welcome = false;
        Object.defineProperty($rootScope.currentUser.hiring_application, 'complete', {
            value: true,
        });

        jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
        HiringApplication.expect('save');

        render();

        expect(DialogModal.alert).not.toHaveBeenCalled();

        expect($rootScope.currentUser.hiring_application.has_seen_welcome).toBe(true);
    });

    it('should only show the local candidates checkbox if there is a location', () => {
        const search = CareerProfileSearch.new();
        jest.spyOn(search, 'cloneFilters').mockReturnValue({
            places: [],
        });
        CareerProfileList.expect('index');
        render();
        SpecHelper.expectNoElement(elem, '[ng-model="filters.only_local"]');
        scope.filters.places = [locationPlaceDetails.boston];
        scope.$digest();
        SpecHelper.expectElement(elem, '[ng-model="filters.only_local"]');
    });

    describe('on $destroy', () => {
        it('should remove all query params', () => {
            CareerProfileList.expect('index');
            render();
            jest.spyOn($location, 'search').mockImplementation(() => {});
            scope.$destroy();
            expect($location.search).toHaveBeenCalledWith('tab', null);
            expect($location.search).toHaveBeenCalledWith('id', null);
        });

        it("should set hiringRelationshipViewModelForDeepLinkedCareerProfile and it's _deepLinked property to undefined", () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            jest.spyOn(careersNetworkViewModel, 'ensureHiringRelationshipsLoaded').mockReturnValue($q.when());
            const hiringRelationshipViewModel = _.first(careersNetworkViewModel.hiringRelationshipViewModels);
            jest.spyOn(careersNetworkViewModel, 'getMyHiringRelationshipViewModelForConnectionId').mockReturnValue(
                $q.when(hiringRelationshipViewModel),
            );
            CareerProfile.expect('show')
                .toBeCalledWith(hiringRelationshipViewModel.careerProfile.id, expect.anything(), {
                    'FrontRoyal.ApiErrorHandler': {
                        skip: true,
                    },
                })
                .returns(hiringRelationshipViewModel.careerProfile);
            jest.spyOn($location, 'search').mockReturnValue({
                tab: 'featured', // the featured tab supportsDeepLinking
                id: hiringRelationshipViewModel.careerProfile.id,
            });

            render();
            CareerProfile.flush('show');

            expect(scope.hiringRelationshipViewModelForDeepLinkedCareerProfile._deepLinked).toBe(true);
            expect(scope.hiringRelationshipViewModelForDeepLinkedCareerProfile).toEqual(hiringRelationshipViewModel);
            scope.$destroy();
            expect(hiringRelationshipViewModel._deepLinked).toBeUndefined();
            expect(scope.hiringRelationshipViewModelForDeepLinkedCareerProfile).toBeUndefined();
        });
    });

    describe('currentTab', () => {
        beforeEach(() => {
            // This gets called because the candidateStackDemo directive gets rendered.
            // It's not important for the purposes of these specs though
            CareerProfileList.expect('index');
        });

        describe('when changed', () => {
            describe('when currentUser.hasAcceptedHiringManagerAccess', () => {
                it('should reset the offset to 0 so the user is sent back to the first page of results', () => {
                    render();
                    jest.spyOn(scope, 'setOffset');
                    jest.spyOn(scope.careerProfileFilterSet, 'setOffset');
                    scope.offset = 20;
                    scope.currentTab = 'featured';
                    scope.$digest();
                    expect(scope.setOffset).toHaveBeenCalledWith(20, 'first', scope.listLimit);
                    expect(scope.careerProfileFilterSet.setOffset).toHaveBeenCalledWith(0, 'first', scope.listLimit); // 0 because the careerProfileFilterSet has it's own offset counter
                    expect(scope.offset).toEqual(0);
                });

                describe('when newTab usesCareerProfileFilterSet', () => {
                    it('should extendFiltersWithMostRecentCareerProfileSearch', () => {
                        const mostRecentSearch = CareerProfileSearch.new();
                        jest.spyOn(mostRecentSearch, 'cloneFilters').mockReturnValue({
                            places: [locationPlaceDetails.boston],
                            industries: ['foo', 'bar'], // advanced filter
                        });
                        const anotherSearch = CareerProfileSearch.new();
                        jest.spyOn(anotherSearch, 'cloneFilters').mockReturnValue({
                            places: [locationPlaceDetails.washington_dc],
                        });
                        currentUser.hiring_application.recent_career_profile_searches = [
                            mostRecentSearch,
                            anotherSearch,
                        ];

                        render();

                        // switch to a different tab that doesn't use the most recent career profile search
                        // so the filters get reset
                        scope.currentTab = 'saved';
                        scope.$digest();
                        expect(scope.currentTabConfig.usesCareerProfileFilterSet).toBe(false);
                        expect(scope.filters.places).toEqual([]); // shouldn't use most recent career profile search for this tab
                        expect(scope.proxy.showAdvancedFilters).toBe(false);

                        // switch to a tab that does use the most recent career profile search
                        // and assert that the filters match the most recent career profile search
                        scope.currentTab = 'all';
                        scope.$digest();
                        expect(scope.currentTabConfig.usesCareerProfileFilterSet).toBe(true);
                        expect(scope.filters.places).toEqual([locationPlaceDetails.boston]);
                        expect(scope.filters.industries).toEqual(['foo', 'bar']);
                        expect(scope.proxy.showAdvancedFilters).toBe(true); // the most recent career profile search uses an advanced filter, so showAdvancedFilters should be ture
                    });
                });
            });
        });

        describe('getter', () => {
            it('should be derived from $location.search tab query param', () => {
                render();
                expect(scope.currentTab).toEqual($location.search().tab);

                $location.search('tab', 'featured');
                expect(scope.currentTab).toEqual('featured');
            });

            it("should default to 'all'", () => {
                render();
                expect($location.search().tab).toEqual('all');
                expect(scope.currentTab).toEqual('all');
            });

            it("should fallback to 'all' if invalid tab query param", () => {
                render();
                // currentTab is already 'all', so change it to something else as an additional
                // check to validate that currentTab does indeed fallback to 'all'
                $location.search('tab', 'featured');
                expect(scope.currentTab).toEqual('featured');

                $location.search('tab', 'invalid_tab');
                expect(scope.currentTab).toEqual('all');
            });
        });

        describe('setter', () => {
            it('should set tab query param', () => {
                render();
                expect($location.search().tab).not.toEqual('featured');
                scope.currentTab = 'featured';
                expect($location.search().tab).toEqual('featured');
            });
        });
    });

    describe('currentTabConfig', () => {
        beforeEach(() => {
            // This gets called because the candidateStackDemo directive gets rendered.
            // It's not important for the purposes of these specs though
            CareerProfileList.expect('index');
        });

        it('should be the config for the currentTab', () => {
            render();
            // it should initially be the tab config for the 'all' tab
            // because the 'all' tab is shown by default if no tab query
            // param is specified
            expect(scope.currentTabConfig).toEqual(scope.tabConfigs.all);
            scope.currentTab = 'featured';
            expect(scope.currentTabConfig).toEqual(scope.tabConfigs.featured);
        });

        describe('usesCareerProfileFilterSet', () => {
            describe('when true', () => {
                it('should show front-royal-spinner if careerProfileFilterSet.searchingForMoreWhenEmpty and currentUser.hasAcceptedHiringManagerAccess', () => {
                    jest.spyOn(CareerProfileFilterSet.prototype, 'searchingForMoreWhenEmpty', 'get').mockReturnValue(
                        true,
                    );
                    render();
                    expect(scope.currentTabConfig.usesCareerProfileFilterSet).toEqual(true);

                    SpecHelper.expectElement(elem, 'front-royal-spinner');
                });

                it('should show candidate-list careerProfileFilterSet.showDisplayElement', () => {
                    jest.spyOn(CareerProfileFilterSet.prototype, 'showDisplayElement', 'get').mockReturnValue(true);

                    render();
                    expect(scope.currentTabConfig.usesCareerProfileFilterSet).toEqual(true);

                    SpecHelper.expectElement(elem, 'candidate-list');
                });

                it('should show candidate-list-demo if no careerProfileFilterSet', () => {
                    hasAcceptedHiringManagerAccess = false; // need to mock this ensure careerProfileFilterSet gets set to null

                    render();
                    expect(scope.currentTabConfig.usesCareerProfileFilterSet).toEqual(true);

                    expect(scope.careerProfileFilterSet).toBeNull();
                    SpecHelper.expectElement(elem, 'candidate-list-demo');
                });

                it("should show 'No additional candidates' message with working link to 'featured' tab if careerProfileFilterSet.noAdditionalCandidates and currentUser.hasAcceptedHiringManagerAccess", () => {
                    jest.spyOn(CareerProfileFilterSet.prototype, 'noAdditionalCandidates', 'get').mockReturnValue(true);

                    render();
                    expect(scope.currentTabConfig.usesCareerProfileFilterSet).toEqual(true);

                    SpecHelper.expectElement(elem, '[name="no-candidates"]');
                    SpecHelper.expectElementText(
                        elem,
                        '[name="no-candidates"] h1',
                        'No additional candidates found to review',
                    );
                    // NOTE: 'orcheck' in the expected element text is not a typo. There's a <br> element with no spaces
                    // around it between 'or' and 'check' in the locale string, so 'or<br>check' gets squashed to 'orcheck'
                    SpecHelper.expectElementText(
                        elem,
                        '[name="no-candidates"] h2',
                        'You may wish to adjust your preferences, orcheck out the Featured Candidates tab.',
                    );

                    // test the link in the locale string
                    jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
                    SpecHelper.click(elem, '[name="no-candidates"] h2 a');
                    expect(scope.loadRoute).toHaveBeenCalledWith('/hiring/browse-candidates?tab=featured');
                });
            });

            describe('when false', () => {
                it('should show front-royal-spinner if loading and hasAcceptedHiringManagerAccess', () => {
                    render();
                    scope.currentTabConfig.usesCareerProfileFilterSet = false;
                    scope.currentTabConfig.loading = true;
                    scope.$digest();

                    SpecHelper.expectElement(elem, 'front-royal-spinner');
                });

                it('should only show preferred_primary_areas_of_interest and keyword_search filters', () => {
                    render();
                    scope.currentTabConfig.usesCareerProfileFilterSet = false;
                    scope.$digest();

                    SpecHelper.expectElement(elem, '[ng-model="filters.preferred_primary_areas_of_interest"]');
                    SpecHelper.expectElement(elem, '[ng-model="filters.keyword_search"]');
                    SpecHelper.expectNoElement(elem, '[ng-model="filters.places"]');
                    SpecHelper.expectNoElement(elem, '[ng-model="filters.only_local"]');
                    SpecHelper.expectNoElement(elem, '[ng-model="filters.years_experience"]');
                    SpecHelper.expectNoElement(elem, '[ng-model="filters.employment_types_of_interest"]');
                    SpecHelper.expectNoElement(elem, '[ng-model="proxy.showAdvancedFilters"]');
                    SpecHelper.expectNoElement(elem, '.advanced');
                });

                describe('when showAllReviewedMessage', () => {
                    it("should show 'All candidates reviewed' messages", () => {
                        hasAcceptedHiringManagerAccess = false;

                        render();
                        scope.currentTabConfig.usesCareerProfileFilterSet = false;
                        jest.spyOn(scope, 'showAllReviewedMessage', 'get').mockReturnValue(true);
                        scope.$digest();

                        SpecHelper.expectElementText(elem, 'h1', 'No featured profiles to share');
                        SpecHelper.expectElementText(
                            elem,
                            'h2',
                            "Your account manager can help you find qualified candidates. Contact hiring@smart.ly to let us know what you're looking for!",
                        );
                    });

                    it('should have working button to review activity if hasAcceptedHiringManagerAccess', () => {
                        render();
                        scope.currentTabConfig.usesCareerProfileFilterSet = false;
                        jest.spyOn(scope, 'showAllReviewedMessage', 'get').mockReturnValue(true);
                        scope.$digest();

                        SpecHelper.expectElement(elem, 'button[name="review-activity"]');
                        SpecHelper.expectElementText(elem, 'button[name="review-activity"]', 'Review Your Activity');
                    });
                });

                describe('when showNoCandidatesForActionMessage', () => {
                    it("should show 'You haven't hidden any candidates' message when on Hidden tab", () => {
                        jest.spyOn($location, 'search').mockReturnValue({
                            tab: 'hidden',
                            replace() {},
                        });
                        render();
                        scope.currentTabConfig.usesCareerProfileFilterSet = false;
                        jest.spyOn(scope, 'showNoCandidatesForActionMessage', 'get').mockReturnValue(true);
                        scope.$digest();

                        SpecHelper.expectElementText(elem, 'h1', "You haven't hidden any candidates");
                    });

                    it("should show 'You haven't saved any candidates' message when on Saved tab", () => {
                        jest.spyOn($location, 'search').mockReturnValue({
                            tab: 'saved',
                            replace() {},
                        });
                        render();
                        scope.currentTabConfig.usesCareerProfileFilterSet = false;
                        jest.spyOn(scope, 'showNoCandidatesForActionMessage', 'get').mockReturnValue(true);
                        scope.$digest();

                        SpecHelper.expectElementText(elem, 'h1', "You haven't saved any candidates");
                    });
                });

                describe('when !showAllReviewedMessage, !showNoCandidatesForActionMessage, and careerProfilesOrHiringRelationshipViewModels', () => {
                    it('should show candidate-list if !showAllReviewedMessage, !showNoCandidatesForActionMessage, and careerProfilesOrHiringRelationshipViewModels.length > 0', () => {
                        render();
                        scope.currentTabConfig.usesCareerProfileFilterSet = false;
                        jest.spyOn(scope, 'showAllReviewedMessage', 'get').mockReturnValue(false);
                        jest.spyOn(scope, 'showNoCandidatesForActionMessage', 'get').mockReturnValue(false);
                        jest.spyOn(scope, 'careerProfilesOrHiringRelationshipViewModels', 'get').mockReturnValue([
                            CareerProfile.fixtures.getInstance(),
                        ]);
                        scope.$digest();

                        SpecHelper.expectElement(elem, 'candidate-list');
                    });
                });

                describe('when !currentUser.hasAcceptedHiringManagerAccess', () => {
                    beforeEach(() => {
                        hasAcceptedHiringManagerAccess = false;
                    });

                    it('should show candidate-list if !showAllReviewedMessage, !showNoCandidatesForActionMessage, and careerProfilesOrHiringRelationshipViewModels.length > 0', () => {
                        render();
                        scope.currentTabConfig.usesCareerProfileFilterSet = false;
                        jest.spyOn(scope, 'showAllReviewedMessage', 'get').mockReturnValue(false);
                        jest.spyOn(scope, 'showNoCandidatesForActionMessage', 'get').mockReturnValue(false);
                        jest.spyOn(scope, 'careerProfilesOrHiringRelationshipViewModels', 'get').mockReturnValue([
                            CareerProfile.fixtures.getInstance(),
                        ]);
                        scope.$digest();

                        SpecHelper.expectElement(elem, 'candidate-list');
                    });

                    it('should show candidate-list-demo if no careerProfilesOrHiringRelationshipViewModels', () => {
                        render();
                        scope.currentTabConfig.usesCareerProfileFilterSet = false;
                        jest.spyOn(scope, 'careerProfilesOrHiringRelationshipViewModels', 'get').mockReturnValue(null);
                        scope.$digest();

                        SpecHelper.expectElement(elem, 'candidate-list-demo');
                    });
                });
            });
        });
    });

    describe('afterCandidateReviewed', () => {
        describe('when currentTab supportsDeepLinking and removed candidate is the _deepLinked candidate', () => {
            it('should remove id query param and _deepLinked flag', () => {
                const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
                const hiringRelationshipViewModel = _.first(careersNetworkViewModel.hiringRelationshipViewModels);
                const careerProfile = hiringRelationshipViewModel.careerProfile;
                jest.spyOn(careersNetworkViewModel, 'ensureHiringRelationshipsLoaded').mockReturnValue($q.when());
                jest.spyOn(careersNetworkViewModel, 'getMyHiringRelationshipViewModelForConnectionId').mockReturnValue(
                    $q.when(hiringRelationshipViewModel),
                );

                CareerProfile.expect('show')
                    .toBeCalledWith(careerProfile.id, expect.anything(), {
                        'FrontRoyal.ApiErrorHandler': {
                            skip: true,
                        },
                    })
                    .returns(careerProfile);

                jest.spyOn($location, 'search').mockReturnValue({
                    tab: 'featured', // the featured tab supportsDeepLinking
                    id: careerProfile.id,
                    replace() {},
                });

                render();
                CareerProfile.flush('show');
                expect(scope.currentTabConfig.supportsDeepLinking).toBe(true);
                expect(scope.hiringRelationshipViewModelForDeepLinkedCareerProfile).toEqual(
                    hiringRelationshipViewModel,
                );
                expect(hiringRelationshipViewModel._deepLinked).toBe(true);

                scope.afterCandidateReviewed(hiringRelationshipViewModel);

                expect($location.search).toHaveBeenCalledWith('id', null);
                expect(hiringRelationshipViewModel._deepLinked).toBeUndefined();
            });
        });

        describe('when currentTab supportsAllCandidatesReviewedEvent', () => {
            it('should log hiring_manager:all_candidates_reviewed even if last candidate has been reviewed', () => {
                render();
                expect(scope.currentTabConfig.supportsAllCandidatesReviewedEvent).toBe(false);

                // change the currentTab to one that supportsAllCandidatesReviewedEvent
                scope.currentTab = 'saved';
                scope.$digest();
                expect(scope.currentTabConfig.supportsAllCandidatesReviewedEvent).toBe(true);

                // ensure that there is at least one element in the unfiltered list of career profiles for the currentTab
                scope.careerProfilesMap.saved = ['foo'];
                scope.afterCandidateReviewed();
                expect(EventLogger.prototype.log).not.toHaveBeenCalled();

                // the hiring_manager:all_candidates_reviewed event should only be triggered if the UNFILTERED
                // list of profiles is empty, not the filtered list (i.e. scope.careerProfilesOrHiringRelationshipViewModels)
                jest.spyOn(scope, 'careerProfilesOrHiringRelationshipViewModels', 'get').mockReturnValue([]);
                scope.afterCandidateReviewed();
                expect(EventLogger.prototype.log).not.toHaveBeenCalled();

                // set the list of unfiltered career profiles for the currentTab to be empty, indicating the last candidate has been reviewed
                scope.careerProfilesMap.saved = [];
                scope.afterCandidateReviewed();
                expect(EventLogger.prototype.log).toHaveBeenCalledWith('hiring_manager:all_candidates_reviewed');
            });
        });
    });

    describe('when !hasAcceptedHiringManagerAccess', () => {
        beforeEach(() => {
            hasAcceptedHiringManagerAccess = false;
            careerProfiles = [CareerProfile.fixtures.getInstance(), CareerProfile.fixtures.getInstance()];

            CareerProfileList.expect('index').returns({
                result: [
                    {
                        career_profiles: [careerProfiles[0].asJson(), careerProfiles[1].asJson()],
                    },
                ],
            });
        });

        it('should show a modal and default candidate stack for all tab', () => {
            render();

            CareerProfileList.flush('index');
            SpecHelper.expectElement(elem, 'candidate-list-demo');
            SpecHelper.expectElementText(elem, 'partial-screen-dialog-modal .title', "We're reviewing your profile");
            SpecHelper.expectElementText(
                elem,
                'partial-screen-dialog-modal .body span',
                'Our team will be in touch soon. Thank you for joining Smartly Talent!',
            );
            SpecHelper.expectNoElement(elem, 'partial-screen-dialog-modal button');
        });

        it('should not ensureHiringRelationshipsLoaded for any tab', () => {
            jest.spyOn(
                CareersNetworkViewModel.prototype,
                'ensureHiringRelationshipsLoaded',
            ).mockImplementation(() => {});
            render();
            expect(CareersNetworkViewModel.prototype.ensureHiringRelationshipsLoaded).not.toHaveBeenCalled();
        });
    });

    describe('when hasFullHiringManagerAccess', () => {
        beforeEach(() => {
            CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            jest.spyOn(CareerProfileFilterSet.prototype, 'ensureCareerProfilesPreloaded').mockImplementation(
                function () {
                    const self = this;

                    // mock a request that sets the stack once timeouts are flushed
                    self._loadingPromise = $timeout();
                    self._loadingPromise.then(() => {
                        self._loadingPromise = null;
                        scope.careerProfileFilterSet._initialResultsLoaded = true;
                        scope.careerProfileFilterSet.careerProfiles = [];
                    });
                },
            );
        });

        it('should extendFiltersWithMostRecentCareerProfileSearch', () => {
            const search = CareerProfileSearch.new();
            jest.spyOn(search, 'cloneFilters').mockReturnValue({
                places: [locationPlaceDetails.boston],
            });

            currentUser.hiring_application.recent_career_profile_searches = [search];
            CareerProfileFilterSet.prototype.ensureCareerProfilesPreloaded.mockImplementation(function () {
                expect(this.filters.places).toEqual([locationPlaceDetails.boston]);
            });
            render();
            expect(scope.filters.places).toEqual([locationPlaceDetails.boston]);
        });

        it("should ensureHiringRelationshipsLoaded for 'saved' tab", () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            jest.spyOn(careersNetworkViewModel, 'ensureHiringRelationshipsLoaded').mockReturnValue($q.when());
            render();
            // since we don't initially land on the 'saved' tab, the call to ensure that the hiring relationships
            // are loaded for the 'saved' tab should be independent, so we expect true to be the second argument
            // signifying that this API call is independent
            expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded).toHaveBeenCalledWith('saved', true);
        });

        it("should ensureHiringRelationshipsLoaded for 'saved' tab after more important API calls", () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            jest.spyOn(careersNetworkViewModel, 'ensureHiringRelationshipsLoaded').mockReturnValue($q.when());
            jest.spyOn($location, 'search').mockReturnValue({
                tab: 'featured',
                replace() {},
            });
            render();
            // We initially land on the 'featured' tab, so we depend on its API call to finish in order to determine
            // what actions should be made available, which is why false is passed in as the second argument when
            // ensuring hiring relationships are loaded for the 'featured' tab. The call to ensure the hiring relationships
            // are loaded for the 'saved' tab doesn't affect what actions are available for the connections in the 'featured'
            // tab, so it's API call is independent, which is why we expect true to passed in as the second param for its
            // call to ensureHiringRelationshipsLoaded
            expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded.mock.calls[0]).toEqual(['featured', false]);
            expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded.mock.calls[1]).toEqual(['saved', true]);
        });

        it("should set 'saved' tab count to number of hiring relationships that have been 'saved_for_later' without an open_position_id", () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            render();
            const savedHiringRelationshipViewModels = _.filter(
                careersNetworkViewModel.hiringRelationshipViewModels,
                vm =>
                    vm.hiringRelationship.hiring_manager_status === 'saved_for_later' &&
                    !vm.hiringRelationship.open_position_id,
            );
            const savedTab = _.findWhere(scope.candidatesTabs, {
                name: 'saved',
            });
            expect(savedTab.translate.values.count).toEqual(savedHiringRelationshipViewModels.length);
            SpecHelper.expectElementText(
                elem,
                'tabs .saved-candidates',
                `Saved (${savedHiringRelationshipViewModels.length})`,
            );
        });

        it('should ensureHiringRelationshipsLoaded on currentTab change if new currentTab !usesCareerProfileFilterSet', () => {
            render();
            jest.spyOn(CareersNetworkViewModel.prototype, 'ensureHiringRelationshipsLoaded').mockReturnValue($q.when());
            scope.currentTab = 'featured';
            expect(scope.currentTabConfig.usesCareerProfileFilterSet).toBe(false);
            scope.$digest();
            // we depend on the API call that gets triggered inside ensureHiringRelationshipsLoaded to finish in order to
            // determine what actions should be made available for the candidates on the 'featured' tab, which is why we
            // expect false to be passed in as the second arg when ensureHiringRelationshipsLoaded gets called
            expect(CareersNetworkViewModel.prototype.ensureHiringRelationshipsLoaded).toHaveBeenCalledWith(
                'featured',
                false,
            );
        });

        it('should not ensureHiringRelationshipsLoaded on currentTab change if new currentTab usesCareerProfileFilterSet', () => {
            render();
            // we're already on the 'all' tab so change to a different tab
            scope.currentTab = 'featured';
            scope.$digest();

            jest.spyOn(
                CareersNetworkViewModel.prototype,
                'ensureHiringRelationshipsLoaded',
            ).mockImplementation(() => {});
            scope.currentTab = 'all'; // the 'all' tab doesn't use the careerProfileFilterSet, so it shouldn't ensureHiringRelationshipsLoaded
            expect(scope.currentTabConfig.usesCareerProfileFilterSet).toBe(true);
            scope.$digest();

            expect(CareersNetworkViewModel.prototype.ensureHiringRelationshipsLoaded).not.toHaveBeenCalled();
        });

        it('should show deeply linked career profile if currentTab supportsDeepLinking', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            jest.spyOn(careersNetworkViewModel, 'ensureHiringRelationshipsLoaded').mockReturnValue($q.when());
            jest.spyOn(careersNetworkViewModel, 'getMyHiringRelationshipViewModelForConnectionId').mockReturnValue(
                $q.when(),
            );

            const careerProfile = CareerProfile.fixtures.getInstance();
            CareerProfile.expect('show')
                .toBeCalledWith(careerProfile.id, expect.anything(), {
                    'FrontRoyal.ApiErrorHandler': {
                        skip: true,
                    },
                })
                .returns(careerProfile);

            jest.spyOn($location, 'search').mockReturnValue({
                tab: 'featured', // the featured tab supportsDeepLinking
                id: careerProfile.id,
                replace() {},
            });

            render();
            expect(scope.currentTabConfig.supportsDeepLinking).toBe(true);

            CareerProfile.flush('show');
            expect(careersNetworkViewModel.getMyHiringRelationshipViewModelForConnectionId).toHaveBeenCalledWith(
                careerProfile.user_id,
                true,
            ); // true so we checkApiIfNotFound
        });

        it("should search teammate hiring relationship view-models for connection with deep linked candidate if hiring manager doesn't have one", () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            jest.spyOn(careersNetworkViewModel, 'ensureHiringRelationshipsLoaded').mockReturnValue($q.when());
            jest.spyOn(careersNetworkViewModel, 'getMyHiringRelationshipViewModelForConnectionId').mockReturnValue(
                $q.when(),
            );
            const hiringRelationshipViewModel = _.first(careersNetworkViewModel.hiringRelationshipViewModels);
            jest.spyOn(careersNetworkViewModel, 'getHiringRelationshipViewModelAcceptedByTeam').mockReturnValue(
                $q.when(hiringRelationshipViewModel),
            );

            const careerProfile = CareerProfile.fixtures.getInstance();
            CareerProfile.expect('show')
                .toBeCalledWith(careerProfile.id, expect.anything(), {
                    'FrontRoyal.ApiErrorHandler': {
                        skip: true,
                    },
                })
                .returns(careerProfile);

            jest.spyOn($location, 'search').mockReturnValue({
                tab: 'featured', // the featured tab supportsDeepLinking
                id: careerProfile.id,
                replace() {},
            });

            render();
            expect(scope.currentTabConfig.supportsDeepLinking).toBe(true);

            CareerProfile.flush('show');
            expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded).toHaveBeenCalledWith('teamCandidates');
            expect(careersNetworkViewModel.getHiringRelationshipViewModelAcceptedByTeam).toHaveBeenCalledWith(
                careerProfile.user_id,
            );
        });

        it('should set unsupportedDeepLink to true if deeply linked career profile is not found', () => {
            const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            jest.spyOn(careersNetworkViewModel, 'ensureHiringRelationshipsLoaded').mockReturnValue($q.when());
            jest.spyOn(
                careersNetworkViewModel,
                'getOrInitializeHiringRelationshipViewModelForCareerProfile',
            ).mockImplementation(() => {});
            const HttpQueue = $injector.get('HttpQueue');
            CareerProfile.expect('show').fails({
                status: 404,
            });
            jest.spyOn($location, 'search').mockReturnValue({
                tab: 'featured', // the featured tab supportsDeepLinking
                id: 'some_id',
            });
            jest.spyOn(HttpQueue.prototype, 'unfreezeAfterError').mockImplementation(angular.noop);

            render();
            CareerProfile.flush('show');
            expect(HttpQueue.prototype.unfreezeAfterError).toHaveBeenCalled();
            expect(scope.currentTabConfig.supportsDeepLinking).toBe(true);
            expect(
                careersNetworkViewModel.getOrInitializeHiringRelationshipViewModelForCareerProfile,
            ).not.toHaveBeenCalled();
            expect(scope.unsupportedDeepLink).toEqual(true);
        });

        it('should not preload career profiles for careerProfileFilterSet on init if !usesCareerProfileFilterSet', () => {
            jest.spyOn($location, 'search').mockReturnValue({
                tab: 'featured',
                replace() {},
            });
            render();
            expect(scope.currentTabConfig.usesCareerProfileFilterSet).toBe(false);
            expect(CareerProfileFilterSet.prototype.ensureCareerProfilesPreloaded).not.toHaveBeenCalled();
        });

        describe('applyFilters', () => {
            it('should be triggered by clicking the button', () => {
                render();
                jest.spyOn(scope, 'applyFilters').mockImplementation(() => {});
                SpecHelper.click(elem, '[name="apply-filters"]');
                expect(scope.applyFilters).toHaveBeenCalled();
            });

            it('should be triggered by <enter> within keyword_search', () => {
                render();
                jest.spyOn(scope, 'applyFilters').mockImplementation(() => {});
                const e = $.Event('keypress');
                e.which = 13;
                elem.find('[name="keyword_search"]').trigger(e);
                expect(scope.applyFilters).toHaveBeenCalled();
            });

            describe('when currentTabConfig.usesCareerProfileFilterSet', () => {
                it('should create a new careerProfileFilterSet and then ensureCareerProfilesPreloaded', () => {
                    render();
                    const origCareerProfileFilterSet = scope.careerProfileFilterSet;
                    scope.applyFilters();
                    expect(scope.careerProfileFilterSet).not.toBe(origCareerProfileFilterSet);
                    expect(scope.careerProfileFilterSet.ensureCareerProfilesPreloaded).toHaveBeenCalled();
                });
            });

            describe('when !currentTabConfig.usesCareerProfileFilterSet', () => {
                it('should reset the offset to 0 so the user is sent back to the first page of results', () => {
                    render();

                    // update the currentTab to a tab where usesCareerProfileFilterSet is false
                    scope.currentTab = 'featured';
                    scope.$digest();
                    expect(scope.currentTabConfig.usesCareerProfileFilterSet).toBe(false);

                    // change the offset so we can verify that it does indeed get changed back to 0
                    // when filters get applied
                    scope.offset = 20;
                    scope.$digest();

                    // set a filter and apply it
                    SpecHelper.updateMultiSelect(elem, 'choose-a-role multi-select', 'research');
                    jest.spyOn(scope, 'setOffset');
                    jest.spyOn(listOffsetHelper, 'setOffset').mockReturnValue(0);

                    scope.applyFilters();

                    expect(scope.setOffset).toHaveBeenCalledWith(20, 'first', scope.listLimit);
                    expect(listOffsetHelper.setOffset).toHaveBeenCalledWith(20, 'first', scope.listLimit);
                    expect(scope.offset).toEqual(0);
                });

                it('should filter by preferred_primary_areas_of_interest and updateTabCount', () => {
                    const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
                    const savedHiringRelationshipViewModels = _.filter(
                        careersNetworkViewModel.hiringRelationshipViewModels,
                        vm =>
                            vm.hiringRelationship.hiring_manager_status === 'saved_for_later' &&
                            !vm.hiringRelationship.open_position_id,
                    );
                    savedHiringRelationshipViewModels[0].careerProfile.primary_areas_of_interest = ['finance'];
                    render();

                    // update the currentTab to a tab where usesCareerProfileFilterSet is false
                    $location.search('tab', 'saved');
                    expect(scope.currentTabConfig.usesCareerProfileFilterSet).toBe(false);

                    // set a filter and apply it
                    SpecHelper.updateMultiSelect(elem, 'choose-a-role multi-select', 'research');
                    scope.applyFilters();

                    const savedTab = _.findWhere(scope.candidatesTabs, {
                        name: 'saved',
                    });
                    expect(savedTab.translate.values.count).toEqual(0);
                    expect(scope.careerProfilesOrHiringRelationshipViewModels.length).toEqual(0);

                    // change the filter and apply it
                    SpecHelper.updateMultiSelect(elem, 'choose-a-role multi-select', 'finance');
                    scope.applyFilters();

                    expect(savedTab.translate.values.count).toEqual(1);
                    expect(scope.careerProfilesOrHiringRelationshipViewModels.length).toEqual(1);
                    expect(scope.careerProfilesOrHiringRelationshipViewModels[0].hiringRelationship.id).toEqual(
                        savedHiringRelationshipViewModels[0].hiringRelationship.id,
                    );
                });

                it('should filter by keyword_search (case insensitive) and updateTabCount', () => {
                    SpecHelper.stubDirective('tabs');
                    const careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
                    const savedHiringRelationshipViewModels = _.filter(
                        careersNetworkViewModel.hiringRelationshipViewModels,
                        vm =>
                            vm.hiringRelationship.hiring_manager_status === 'saved_for_later' &&
                            !vm.hiringRelationship.open_position_id,
                    );
                    // savedHiringRelationshipViewModels[0].careerProfile.keyword_text = 'finance';
                    render();

                    // update the currentTab to a tab where usesCareerProfileFilterSet is false
                    $location.search('tab', 'saved');
                    scope.$digest();
                    expect(scope.currentTabConfig.usesCareerProfileFilterSet).toBe(false);

                    // set a filter and apply it
                    SpecHelper.updateTextInput(elem, '[name="keyword_search"]', 'foobar');
                    scope.applyFilters();

                    const savedTab = _.findWhere(scope.candidatesTabs, {
                        name: 'saved',
                    });
                    expect(savedTab.translate.values.count).toEqual(0);
                    expect(scope.careerProfilesOrHiringRelationshipViewModels.length).toEqual(0);

                    // change the filter and apply it
                    SpecHelper.updateTextInput(elem, '[name="keyword_search"]', 'cAN FlIp YOu foR reAL'); // case insensitive
                    scope.applyFilters();

                    expect(savedTab.translate.values.count).toEqual(1);
                    expect(scope.careerProfilesOrHiringRelationshipViewModels.length).toEqual(1);
                    expect(scope.careerProfilesOrHiringRelationshipViewModels[0].hiringRelationship.id).toEqual(
                        savedHiringRelationshipViewModels[0].hiringRelationship.id,
                    );
                });
            });
        });

        describe('resetFilters', () => {
            beforeEach(() => {
                // just for a small performance boost, don't even bother with the stack
                render();
            });

            function someFilterSelected() {
                return _.chain(scope.filters)
                    .map(list => _.any(list))
                    .any()
                    .value();
            }

            it('should be visible when a list filter is selected and reset on click', () => {
                expect(someFilterSelected()).toBe(false); // sanity check
                SpecHelper.expectNoElement(elem, '[name="reset-filters"]');
                scope.filters.key = ['option'];
                scope.$digest();
                expect(someFilterSelected()).toBe(true); // sanity check
                SpecHelper.click(elem, '[name="reset-filters"]');
                expect(someFilterSelected()).toBe(false); // check that they have been reset
            });

            it('should be visible when only_local is checked and reset when clicked', () => {
                expect(scope.filters.only_local).toBe(false); // sanity check
                SpecHelper.expectNoElement(elem, '[name="reset-filters"]');
                scope.filters.only_local = true;
                scope.$digest();
                SpecHelper.click(elem, '[name="reset-filters"]');
                expect(scope.filters.only_local).toBe(false); // check that they have been reset
            });
        });

        describe('mobile header', () => {
            beforeEach(() => {
                render();
            });

            it('should describe selected preferences', () => {
                SpecHelper.expectElementText(elem, '.navigation-header-span', 'No Preferences Selected');

                // places
                scope.filters.places = [locationPlaceDetails.washington_dc];
                scope.applyFilters();
                scope.$digest();
                SpecHelper.expectElementText(elem, '.navigation-header-span', scope.selectedFiltersSummary);

                scope.filters.places = [locationPlaceDetails.washington_dc, locationPlaceDetails.tokyo];
                scope.applyFilters();

                // A place that is not from the list of location keys should work.  This is not
                // a realistic situation now, since you can currently only select from the list, but
                // it likely will be in the future, so it seemed best to add the test now while refactoring
                // from locations to places
                scope.filters.places = [
                    {
                        formatted_address: 'a formatted address',
                        locality: {
                            long: 'Foo',
                        },
                        administrative_area_level_1: {
                            short: 'Ba',
                            long: 'Bar',
                        },
                        country: {
                            short: 'US',
                        },
                    },
                ];
                scope.applyFilters();
                expect(scope.selectedFiltersSummary).toEqual('Foo, Ba');

                scope.filters.places = [
                    locationPlaceDetails.washington_dc,
                    locationPlaceDetails.tokyo,
                    locationPlaceDetails.beijing,
                ];
                scope.applyFilters();
                expect(scope.selectedFiltersSummary).toEqual('Washington, DC (+2 locations)');

                // preferred_primary_areas_of_interest
                scope.filters.preferred_primary_areas_of_interest = ['marketing'];
                scope.applyFilters();
                expect(scope.selectedFiltersSummary).toEqual('Washington, DC (+2 locations), Marketing');

                scope.filters.places = [];
                scope.filters.preferred_primary_areas_of_interest = ['marketing', 'legal'];
                scope.applyFilters();
                expect(scope.selectedFiltersSummary).toEqual('Marketing (+1 role)');

                scope.filters.preferred_primary_areas_of_interest = ['marketing', 'legal', 'software'];
                scope.applyFilters();
                expect(scope.selectedFiltersSummary).toEqual('Marketing (+2 roles)');

                // years_experience
                scope.filters.years_experience = ['0_1_years'];
                scope.applyFilters();
                expect(scope.selectedFiltersSummary).toEqual('Marketing (+2 roles), 0-1 Years');

                scope.filters.preferred_primary_areas_of_interest = [];
                scope.filters.years_experience = ['0_1_years', '1_2_years'];
                scope.applyFilters();
                expect(scope.selectedFiltersSummary).toEqual('0-1 Years (+1 option)');

                scope.filters.years_experience = ['0_1_years', '1_2_years', '2_4_years'];
                scope.applyFilters();
                expect(scope.selectedFiltersSummary).toEqual('0-1 Years (+2 options)');

                // keyword_search
                scope.filters.keyword_search = 'blah';
                scope.applyFilters();
                expect(scope.selectedFiltersSummary).toEqual('0-1 Years (+2 options), "blah"');

                // skills
                scope.filters.skills = ['JavaScript'];
                scope.applyFilters();
                expect(scope.selectedFiltersSummary).toEqual('0-1 Years (+2 options), JavaScript, "blah"');

                scope.filters.skills = ['JavaScript', 'Ruby'];
                scope.applyFilters();
                expect(scope.selectedFiltersSummary).toEqual('0-1 Years (+2 options), JavaScript (+1 skill), "blah"');

                // employment_types_of_interest
                scope.filters.employment_types_of_interest = ['permanent'];
                scope.applyFilters();
                expect(scope.selectedFiltersSummary).toEqual(
                    '0-1 Years (+2 options), JavaScript (+1 skill), "blah", Permanent',
                );

                scope.filters.employment_types_of_interest = ['permanent', 'part_time'];
                scope.applyFilters();
                expect(scope.selectedFiltersSummary).toEqual(
                    '0-1 Years (+2 options), JavaScript (+1 skill), "blah", Permanent (+1 job type)',
                );

                // company_name
                scope.proxy.professionalOrganization = {
                    text: 'Pedago',
                };
                scope.applyFilters();
                expect(scope.selectedFiltersSummary).toEqual(
                    '0-1 Years (+2 options), JavaScript (+1 skill), "blah", "Pedago", Permanent (+1 job type)',
                );

                // school_name
                scope.proxy.professionalOrganization = {
                    text: 'Texas',
                };
                scope.applyFilters();
                expect(scope.selectedFiltersSummary).toEqual(
                    '0-1 Years (+2 options), JavaScript (+1 skill), "blah", "Texas", Permanent (+1 job type)',
                );
            });

            it('should reset filters back to the currently active ones if closed without resetting', () => {
                // set some filters and apply them
                scope.filters.places = [locationPlaceDetails.new_york];
                scope.applyFilters();

                // expand the mobile menu, change filters,
                // and then close it
                scope.mobileState.expanded = true;
                scope.$digest();
                scope.filters.places = [locationPlaceDetails.new_york, locationPlaceDetails.washington_dc];
                scope.$digest();
                scope.mobileState.expanded = false;
                scope.$digest();

                // filters should go back
                expect(scope.filters.places).toEqual([locationPlaceDetails.new_york]);
            });

            it('should say editing preferences while open', () => {
                scope.mobileState.expanded = true;
                scope.$digest();
                SpecHelper.expectElementText(elem, '.navigation-header-span', 'Editing Preferences');
            });
        });

        describe('showAdvancedFilters', () => {
            it('should NOT show advanced filters on load if they do NOT have values saved', () => {
                render();
                expect(scope.proxy.showAdvancedFilters).toEqual(false);
                SpecHelper.expectNoElement(elem, '.advanced');
            });

            it('should show advanced filters on load if they have values saved', () => {
                const search = CareerProfileSearch.new();
                jest.spyOn(search, 'cloneFilters').mockReturnValue({
                    skills: ['html'],
                });

                currentUser.hiring_application.recent_career_profile_searches = [search];
                CareerProfileFilterSet.prototype.ensureCareerProfilesPreloaded.mockImplementation(function () {
                    expect(this.filters.skills).toEqual(['html']);
                });
                render();
                expect(scope.proxy.showAdvancedFilters).toEqual(true);
                SpecHelper.expectElement(elem, '.advanced');
                expect(scope.filters.skills).toEqual(['html']);
            });

            it('should reset advanced filters if you uncheck the box', () => {
                const search = CareerProfileSearch.new();
                jest.spyOn(search, 'cloneFilters').mockReturnValue({
                    skills: ['html'],
                });

                currentUser.hiring_application.recent_career_profile_searches = [search];
                CareerProfileFilterSet.prototype.ensureCareerProfilesPreloaded.mockImplementation(function () {
                    expect(this.filters.skills).toEqual(['html']);
                });
                render();
                jest.spyOn(scope, 'resetFilters');

                expect(scope.proxy.showAdvancedFilters).toEqual(true); // checkbox is checked
                SpecHelper.expectElement(elem, '.advanced');
                expect(scope.filters.skills).toEqual(['html']);

                // uncheck the box, setting showAdvancedFilters to false
                SpecHelper.toggleCheckbox(elem, '.show-advanced-group input');

                expect(scope.resetFilters).toHaveBeenCalled(); // resets the advanced filters so they are now empty
                expect(scope.proxy.showAdvancedFilters).toEqual(false);
                SpecHelper.expectNoElement(elem, '.advanced');
                expect(scope.filters.skills).toEqual([]); // empty filter, voila!
            });
        });

        describe('organization-autocomplete', () => {
            it('should fill in professionalOrganization if company_name is in saved search', () => {
                const search = CareerProfileSearch.new();
                jest.spyOn(search, 'cloneFilters').mockReturnValue({
                    company_name: 'Pedago',
                });

                currentUser.hiring_application.recent_career_profile_searches = [search];
                CareerProfileFilterSet.prototype.ensureCareerProfilesPreloaded.mockImplementation(function () {
                    expect(this.filters.company_name).toEqual('Pedago');
                });
                render();
                expect(scope.filters.company_name).toEqual('Pedago');
                expect(scope.proxy.professionalOrganization.text).toEqual('Pedago');
            });

            it('should fill in educationalOrganization if school_name is in saved search', () => {
                const search = CareerProfileSearch.new();
                jest.spyOn(search, 'cloneFilters').mockReturnValue({
                    school_name: 'Texas',
                });

                currentUser.hiring_application.recent_career_profile_searches = [search];
                CareerProfileFilterSet.prototype.ensureCareerProfilesPreloaded.mockImplementation(function () {
                    expect(this.filters.school_name).toEqual('Texas');
                });
                render();
                expect(scope.filters.school_name).toEqual('Texas');
                expect(scope.proxy.educationalOrganization.text).toEqual('Texas');
            });
        });
    });

    describe('when !hasFullHiringManagerAccess', () => {
        it('should show choose-a-plan stuff and hide pagination when cards are limited because of lack of access', () => {
            currentUser.hiring_team_id = guid.generate();
            CareerProfileList.expect('index');

            render();
            jest.spyOn(scope, 'showAllReviewedMessage', 'get').mockReturnValue(false);
            jest.spyOn(scope, 'showNoCandidatesForActionMessage', 'get').mockReturnValue(false);
            jest.spyOn(scope.careersNetworkViewModel, 'showHiringBillingModal').mockImplementation(() => {});
            let careerProfilesOrHiringRelationshipViewModels;
            jest.spyOn(scope, 'careerProfilesOrHiringRelationshipViewModels', 'get').mockImplementation(
                () => careerProfilesOrHiringRelationshipViewModels,
            );

            _.each(['all', 'featured'], tab => {
                // In all cases, when the user has full access we show
                // pagination and hide the choose-a-plan stuff
                hasFullHiringManagerAccess = true;
                scope.currentTab = tab;
                scope.$digest();
                scope.currentTabConfig.loading = false;
                scope.$digest();
                SpecHelper.expectNoElement(elem, '.choose-a-plan-wrapper');
                expect(showingPagination()).toBe(true);

                hasFullHiringManagerAccess = false;

                // Testing that the loading state resets on hasFullHiringManagerAccess change
                if (tab !== 'all') {
                    scope.$digest();
                    expect(scope.tabConfigs[tab].loading).toBe(true);
                    scope.currentTabConfig.loading = false;
                    scope.$digest();
                }

                // No profiles loaded yet
                careerProfilesOrHiringRelationshipViewModels = [];
                scope.$digest();

                // While loading profiles, the choose plan stuff is hidden
                SpecHelper.expectNoElement(elem, '.choose-a-plan-wrapper');
                expect(showingPagination()).toBe(false);

                // Once loaded, we hide pagination and show the choose plan stuff
                careerProfilesOrHiringRelationshipViewModels = [{}];
                scope.$digest();
                SpecHelper.expectElement(elem, '.choose-a-plan-wrapper');
                expect(showingPagination()).toBe(false);

                SpecHelper.click(elem, '.choose-a-plan-wrapper button');
                expect(scope.careersNetworkViewModel.showHiringBillingModal).toHaveBeenCalledWith(
                    null,
                    'hiring-browse-candidates#choosePlan',
                );
            });
        });

        it('should disable saved and hidden tabs', () => {
            hasFullHiringManagerAccess = false;
            CareerProfileList.expect('index');
            render();
            SpecHelper.expectElementEnabled(elem, 'tabs button[name="all"]');
            SpecHelper.expectElementEnabled(elem, 'tabs button[name="featured"]');
            SpecHelper.expectElementDisabled(elem, 'tabs button[name="saved"]');
            SpecHelper.expectElementDisabled(elem, 'tabs button[name="hidden"]');

            // if the location bar or something changes the currentTab to a disabled
            // one, we should forward to all
            scope.currentTab = 'saved';
            scope.$digest();
            expect(scope.currentTab).toEqual('all');
        });

        function showingPagination() {
            const candidateList = SpecHelper.expectElement(elem, 'candidate-list');
            const showPaginationAttr = candidateList.attr('show-pagination');
            return scope.$eval(showPaginationAttr);
        }
    });

    describe('when !hasAcceptedHiringManagerAccess', () => {
        it('should disable all tabs except for "all"', () => {
            hasFullHiringManagerAccess = false;
            hasAcceptedHiringManagerAccess = false;
            CareerProfileList.expect('index');

            render();
            SpecHelper.expectElementEnabled(elem, 'tabs button[name="all"]');
            SpecHelper.expectElementDisabled(elem, 'tabs button[name="featured"]');
            SpecHelper.expectElementDisabled(elem, 'tabs button[name="saved"]');
            SpecHelper.expectElementDisabled(elem, 'tabs button[name="hidden"]');

            // if the location bar or something changes the currentTab to a disabled
            // one, we should forward to all
            scope.currentTab = 'featured';
            scope.$digest();
            expect(scope.currentTab).toEqual('all');
        });
    });

    describe('sourcing-upsell', () => {
        it('should be shown if !hasSourcingAccess', () => {
            hasSourcingAccess = false;
            render();

            SpecHelper.expectElement(elem, 'sourcing-upsell');
            SpecHelper.expectNoElement(elem, '.page-with-left-nav-and-main-box');

            CareerProfileList.expect('index');
            hasSourcingAccess = true;
            scope.$digest();

            SpecHelper.expectNoElement(elem, 'sourcing-upsell');
            SpecHelper.expectElement(elem, '.page-with-left-nav-and-main-box');
        });
    });

    function mockAccepted() {
        hasAcceptedHiringManagerAccess = true;
        jest.spyOn(currentUser, 'hasAcceptedHiringManagerAccess', 'get').mockImplementation(
            () => hasAcceptedHiringManagerAccess,
        );
        currentUser.hiring_team = HiringTeam.new();
    }

    function mockFullAccess() {
        mockAccepted();
        hasCompleteHiringApplication = true;
        hasFullHiringManagerAccess = true;
        jest.spyOn(currentUser, 'hasCompleteHiringApplication', 'get').mockImplementation(
            () => hasCompleteHiringApplication,
        );
        jest.spyOn(currentUser, 'hasFullHiringManagerAccess', 'get').mockImplementation(
            () => hasFullHiringManagerAccess,
        );
    }
});
