import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';

describe('FrontRoyal.Careers.CandidateListDemoDir', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let CareerProfileList;
    let CareerProfile;
    let ngModel;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfileList = $injector.get('CareerProfileList');
                CareerProfile = $injector.get('CareerProfile');
                $injector.get('CareerProfileFixtures');
            },
        ]);

        SpecHelper.stubCurrentUser();
        SpecHelper.stubDirective('candidateListCard');

        ngModel = [CareerProfile.fixtures.getInstance(), CareerProfile.fixtures.getInstance()];
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<candidate-list-demo></candidate-list-demo>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should render cards', () => {
        CareerProfileList.expect('index').returns({
            result: [
                {
                    career_profiles: [ngModel[0].asJson(), ngModel[1].asJson()],
                },
            ],
        });

        render();

        CareerProfileList.flush('index');

        SpecHelper.expectElement(elem, '.card-list-item:eq(0)');
        SpecHelper.expectElement(elem, '.card-list-item:eq(1)');
        SpecHelper.expectNoElement(elem, '.card-list-item:eq(2)');
    });

    it('should render nothing if no results', () => {
        CareerProfileList.expect('index').returns({
            result: [
                {
                    career_profiles: [],
                },
            ],
        });

        render();

        CareerProfileList.flush('index');

        SpecHelper.expectNoElement(elem, '.card-list-item');
    });
});
