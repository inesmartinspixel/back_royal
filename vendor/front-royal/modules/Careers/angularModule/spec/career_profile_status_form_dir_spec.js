import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import setSpecLocales from 'Translation/setSpecLocales';
import previewCandidateCardLocales from 'Careers/locales/careers/preview_candidate_card-en.json';
import recommendedPositionsNotificationModalLocales from 'Careers/locales/careers/recommended_positions_notification_modal-en.json';

setSpecLocales(previewCandidateCardLocales, recommendedPositionsNotificationModalLocales);

describe('FrontRoyal.Careers.CareerProfileStatusForm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let CareerProfile;
    let Cohort;
    let user;
    let newCompanyInterestLevels;
    let translationHelper;
    let DialogModal;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareerProfile = $injector.get('CareerProfile');
                Cohort = $injector.get('Cohort');
                DialogModal = $injector.get('DialogModal');
                const TranslationHelper = $injector.get('TranslationHelper');
                translationHelper = new TranslationHelper('careers.preview_candidate_card');

                $injector.get('CareerProfileFixtures');
                $injector.get('CohortFixtures');
            },
        ]);

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
        user.relevant_cohort = Cohort.fixtures.getInstance({
            program_type: 'mba',
        });
        newCompanyInterestLevels = $injector.get('NEW_COMPANY_INTEREST_LEVELS');
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<career-profile-status-form></career-profile-status-form>');

        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should clean form on save', () => {
        user.can_edit_career_profile = true;
        jest.spyOn(user.career_profile, 'complete', 'get').mockReturnValue(true);
        jest.spyOn(CareerProfile.prototype, '_beforeSave').mockImplementation(angular.noop);

        render();
        jest.spyOn(scope.status_form, '$setPristine').mockImplementation(() => {});

        CareerProfile.expect('save');
        scope.save();
        CareerProfile.flush('save');

        expect(scope.status_form.$setPristine).toHaveBeenCalled();
    });

    it('should add confirmed_by_student to meta', () => {
        user.can_edit_career_profile = true;
        jest.spyOn(user.career_profile, 'complete', 'get').mockReturnValue(true);
        jest.spyOn(CareerProfile.prototype, '_beforeSave').mockImplementation(angular.noop);

        render();

        CareerProfile.expect('save').toBeCalledWith(user.career_profile, {
            confirmed_by_student: true,
        });
        SpecHelper.click(elem, '[name="save"]');
        CareerProfile.flush('save');
    });

    it('should have a select input that supports updating the value of interested_in_joining_new_company on the career profile', () => {
        user.can_edit_career_profile = true;
        jest.spyOn(user.career_profile, 'complete', 'get').mockReturnValue(true);
        jest.spyOn(CareerProfile.prototype, '_beforeSave').mockImplementation(angular.noop);

        render();
        SpecHelper.expectElementEnabled(elem, '[name="profile_status"]');

        const expectedValue = _.findWhere(scope.careerProfileActiveInterestLevels, {
            value: user.career_profile.interested_in_joining_new_company,
        });
        expect(expectedValue).not.toBeUndefined();
        SpecHelper.assertSelectValue(elem, '[name="profile_status"]', expectedValue);

        const expectedCareerProfile = CareerProfile.new(user.career_profile.asJson());
        // select a different option from the select
        const newValue = _.findWhere(scope.careerProfileActiveInterestLevels, {
            value: newCompanyInterestLevels[1],
        });
        SpecHelper.updateSelect(elem, '[name="profile_status"]', newValue);

        // it should update the value of interested_in_joining_new_company on the user's career profile
        CareerProfile.expect('save').toBeCalledWith(
            _.extend(expectedCareerProfile, {
                interested_in_joining_new_company: newCompanyInterestLevels[1],
            }),
            {
                confirmed_by_student: true,
            },
        );
        SpecHelper.click(elem, '[name="save"]');
        CareerProfile.flush('save');
    });

    describe('recommended-positions-notification-modal', () => {
        beforeEach(() => {
            user.can_edit_career_profile = true;
            jest.spyOn(user.career_profile, 'complete', 'get').mockReturnValue(true);
            jest.spyOn(CareerProfile.prototype, '_beforeSave').mockImplementation(angular.noop);
            user.career_profile.interested_in_joining_new_company = 'very_interested';
        });

        it('should show modal if not subscribed to recommended positions notification and setting to anything other than inactive', () => {
            user.notify_candidate_positions_recommended = 'never';
            jest.spyOn(DialogModal, 'alert');
            render();
            const newValue = _.findWhere(scope.careerProfileActiveInterestLevels, {
                value: newCompanyInterestLevels[1], // interested
            });
            SpecHelper.updateSelect(elem, '[name="profile_status"]', newValue);
            CareerProfile.expect('save');
            SpecHelper.click(elem, '[name="save"]');
            CareerProfile.flush('save');
            expect(DialogModal.alert).toHaveBeenCalled();
        });

        it('should not show modal if user is subscribed to recommended positions notifications already', () => {
            user.notify_candidate_positions_recommended = 'weekly';
            jest.spyOn(DialogModal, 'alert');
            render();
            const newValue = _.findWhere(scope.careerProfileActiveInterestLevels, {
                value: newCompanyInterestLevels[1], // interested
            });
            SpecHelper.updateSelect(elem, '[name="profile_status"]', newValue);
            CareerProfile.expect('save');
            SpecHelper.click(elem, '[name="save"]');
            CareerProfile.flush('save');
            expect(DialogModal.alert).not.toHaveBeenCalled();
        });

        it('should not show modal if changing interst level to not interested', () => {
            user.notify_candidate_positions_recommended = 'never';
            jest.spyOn(DialogModal, 'alert');
            render();
            const newValue = _.findWhere(scope.careerProfileActiveInterestLevels, {
                value: newCompanyInterestLevels[3], // not_interested
            });
            SpecHelper.updateSelect(elem, '[name="profile_status"]', newValue);
            CareerProfile.expect('save');
            SpecHelper.click(elem, '[name="save"]');
            CareerProfile.flush('save');
            expect(DialogModal.alert).not.toHaveBeenCalled();
        });
    });

    it('should have a caption for the selectedActiveInterestLevel', () => {
        user.can_edit_career_profile = true;
        jest.spyOn(user.career_profile, 'complete', 'get').mockReturnValue(true);
        render();
        SpecHelper.expectElementText(
            elem,
            '.profile_status_caption',
            translationHelper.get(`${scope.currentUser.career_profile.interested_in_joining_new_company}_caption`),
        );
    });

    it('should hide the select if user can_edit_career_profile is false', () => {
        // mock this out
        user.can_edit_career_profile = false;

        render();
        SpecHelper.expectHasClass(elem, '[name="status_form"]', 'ng-hide');
    });

    it('should hide the select if profile is not complete', () => {
        // mock this out
        jest.spyOn(user.career_profile, 'complete', 'get').mockReturnValue(false);
        render();
        SpecHelper.expectHasClass(elem, '[name="status_form"]', 'ng-hide');
    });
});
