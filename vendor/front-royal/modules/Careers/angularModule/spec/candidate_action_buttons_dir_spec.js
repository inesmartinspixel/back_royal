import 'AngularSpecHelper';
import 'Careers/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import candidateActionButtonsLocales from 'Careers/locales/careers/candidate_action_buttons-en.json';

setSpecLocales(candidateActionButtonsLocales);

describe('FrontRoyal.Careers.CandidateActionButtonsDirSpec', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.showHide = opts.showHide;
        renderer.scope.showApply = opts.showApply;
        renderer.scope.showView = opts.showView;
        renderer.scope.hasInstantApply = opts.hasInstantApply || false;
        renderer.scope.hide = jest.fn();
        renderer.scope.apply = jest.fn();
        renderer.scope.viewCoverLetter = jest.fn();
        renderer.render(
            '<candidate-action-buttons show-hide="showHide" show-apply="showApply" show-view="showView" has-instant-apply="hasInstantApply" apply="apply()" hide="hide()" view-cover-letter="viewCoverLetter()"></candidate-action-buttons>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should hide button if show* is false', () => {
        render({
            showHide: false,
            showApply: false,
            showView: false,
        });
        SpecHelper.expectNoElement(elem, '[name="hide"]');
        SpecHelper.expectNoElement(elem, '[name="apply"]');
        SpecHelper.expectNoElement(elem, '[name="view"]');
    });

    it('should fire actions when buttons are clicked', () => {
        render({
            showHide: true,
            showApply: true,
            showView: true,
        });

        SpecHelper.click(elem, '[name="hide"]');
        expect(renderer.scope.hide).toHaveBeenCalled();

        SpecHelper.click(elem, '[name="apply"]');
        expect(renderer.scope.apply).toHaveBeenCalled();

        SpecHelper.click(elem, '[name="view"]');
        expect(renderer.scope.viewCoverLetter).toHaveBeenCalled();
    });

    describe('instant apply UI', () => {
        it('should show if scope.showApply and scope.hasInstantApply', () => {
            render({
                showApply: true,
                hasInstantApply: true,
            });
            SpecHelper.expectElement(elem, '.instant-apply');
        });

        it('should not show if not scope.showApply ', () => {
            render({
                showApply: false,
                hasInstantApply: true,
            });
            SpecHelper.expectNoElement(elem, '.instant-apply');
        });

        it('should not show if not scope.hasInstantApply ', () => {
            render({
                showApply: true,
                hasInstantApply: false,
            });
            SpecHelper.expectNoElement(elem, '.instant-apply');
        });
    });

    describe('applyLocale', () => {
        it("should be 'apply_now' if hasInstantApply", () => {
            render({
                hasInstantApply: true,
            });
            expect(scope.applyLocale).toBe('apply_now');
        });

        it("should be 'learn_more' otherwise", () => {
            render();
            expect(scope.applyLocale).toBe('learn_more');
        });
    });
});
