import 'AngularSpecHelper';
import 'Careers/angularModule';

describe('FrontRoyal.Careers.HiringManagerAcceptedModal', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);

        SpecHelper.stubCurrentUser();

        render();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.message = 'test message';
        renderer.scope.buttonText = 'button text';
        renderer.render(
            '<hiring-manager-accepted-modal message="message" button-text="buttonText"></hiring-manager-card>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('action', () => {
        it('should show button to review candidates if profile is complete', () => {
            jest.spyOn(scope, 'loadRoute').mockImplementation(() => {});
            scope.action();
            expect(scope.loadRoute).toHaveBeenCalledWith('/hiring/browse-candidates');
        });
    });
});
