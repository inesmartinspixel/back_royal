import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/careers_spec_helper';
import setSpecLocales from 'Translation/setSpecLocales';
import connectionsListLocales from 'Careers/locales/careers/connections_list-en.json';
import connectionsLocales from 'Careers/locales/careers/connections-en.json';
import connectionBarButtonLocales from 'Careers/locales/careers/connection_bar_button-en.json';
import candidateListLocales from 'Careers/locales/careers/candidate_list-en.json';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';
import inviteTeammateLocales from 'Careers/locales/careers/invite_teammate-en.json';

setSpecLocales(
    connectionsListLocales,
    connectionsLocales,
    connectionBarButtonLocales,
    candidateListLocales,
    candidateListCardLocales,
    inviteTeammateLocales,
);

describe('FrontRoyal.Careers.ConnectionsList', () => {
    let $injector;
    let $timeout;
    let SpecHelper;
    let CareersSpecHelper;
    let renderer;
    let elem;
    let scope;
    let careersNetworkViewModel;
    let $q;
    let OpenPosition;
    let openPositions;
    let $rootScope;

    beforeEach(() => {
        angular.mock.module('CareersSpecHelper', 'FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                CareersSpecHelper = $injector.get('CareersSpecHelper');
                $timeout = $injector.get('$timeout');
                $q = $injector.get('$q');
                OpenPosition = $injector.get('OpenPosition');
                $rootScope = $injector.get('$rootScope');
            },
        ]);

        SpecHelper.stubDirective('candidateListCard');

        careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
        const CareersNetworkViewModel = $injector.get('CareersNetworkViewModel');
        openPositions = [
            OpenPosition.new({
                id: '1',
                hiring_manager_id: careersNetworkViewModel.user.id,
                title: 'open position title',
                salary: 42000,
                bonus: false,
                equity: false,
                available_interview_times: 'midnight!!!!',
            }),
        ];
        jest.spyOn(CareersNetworkViewModel.prototype, 'ensureOpenPositions').mockReturnValue($q.when(openPositions));

        // Except for the one spec where we test it directly, make all relationships
        // matches so they will be visible
        _.each(careersNetworkViewModel.hiringRelationshipViewModels, vm => {
            vm.hiringRelationship.hiring_manager_status = 'accepted';
            vm.hiringRelationship.candidate_status = 'accepted';
        });
    });

    function render(closed, role) {
        jest.spyOn(careersNetworkViewModel, 'ensureHiringRelationshipsLoaded').mockReturnValue($q.when());
        renderer = SpecHelper.renderer();
        renderer.scope.closed = closed;
        renderer.scope.role = role || careersNetworkViewModel.role;
        renderer.render('<connections-list closed="closed" role="role"></connections-list>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    describe('empty states', () => {
        it('should render expected message when empty and closed === true', () => {
            careersNetworkViewModel.hiringRelationshipViewModels = [];
            render(false);
            SpecHelper.expectElementText(elem, '.no-connections', 'Your matched connections will appear here.');
        });

        it('should render expected message when empty and closed !== true', () => {
            careersNetworkViewModel.hiringRelationshipViewModels = [];
            render(true);
            SpecHelper.expectElementText(elem, '.no-connections', 'Your closed connections will appear here.');
        });

        it('should render expected message when empty and in team tab and you have no team', () => {
            careersNetworkViewModel.hiringRelationshipViewModels = [];
            careersNetworkViewModel.user.hiring_team_id = null;
            render(false, 'hiringTeammate');
            SpecHelper.expectElementText(
                elem,
                '.no-connections p',
                'Teams are a great way to collaborate in your search for the right candidates.',
            );
            SpecHelper.expectElementText(elem, 'button.flat.blue', 'Invite Coworker');
        });

        it('should render expected message when empty and in team tab but you have a team', () => {
            careersNetworkViewModel.hiringRelationshipViewModels = [];
            careersNetworkViewModel.user.hiring_team_id = 'teamid';
            render(false, 'hiringTeammate');
            SpecHelper.expectElementText(elem, '.no-connections', "Your team's connections will appear here.");
        });
    });

    describe('connection status filtering', () => {
        describe('for hiringManager', () => {
            it('should group matched, saved for later, and liked connections for a hiring manager', () => {
                careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
                const relationshipsByStatus = careersNetworkViewModel.testHelper.getRelationshipsByStatus();

                render(false, 'hiringManager');

                expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded).toHaveBeenCalledWith('teamCandidates');

                expect(scope.viewModelLists.connected[0].hiringRelationshipId).toBe(relationshipsByStatus.matched.id);
                expect(scope.viewModelLists.liked[0].hiringRelationshipId).toBe(
                    relationshipsByStatus.waiting_on_candidate.id,
                );
                expect(scope.viewModelLists.saved_for_later[0].hiringRelationshipId).toBe(
                    relationshipsByStatus.saved_for_later.id,
                );
            });

            it('should group matched, saved for later, and liked teammate connections for a hiring manager', () => {
                careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                    teammateId: 'teammate',
                });
                const relationshipsByStatus = careersNetworkViewModel.testHelper.getRelationshipsByStatus();

                render(false, 'hiringTeammate');

                expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded).toHaveBeenCalledWith('teamCandidates');

                expect(scope.viewModelLists.connected[0].hiringRelationshipId).toBe(relationshipsByStatus.matched.id);
                expect(scope.viewModelLists.liked[0].hiringRelationshipId).toBe(
                    relationshipsByStatus.waiting_on_candidate.id,
                );
                expect(scope.viewModelLists.saved_for_later[0].hiringRelationshipId).toBe(
                    relationshipsByStatus.saved_for_later.id,
                );
            });
        });

        describe('for candidate', () => {
            it('should only render matched and pending connections for a candidate', () => {
                careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
                const relationshipsByStatus = careersNetworkViewModel.testHelper.getRelationshipsByStatus();

                render(false);

                expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded.mock.calls.length).toBe(1);
                expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded).toHaveBeenCalledWith('myCandidates');

                SpecHelper.expectElement(
                    elem,
                    `[section-name="'new'"] connection-bar-button [data-id="${relationshipsByStatus.waiting_on_candidate.id}"]`,
                );
                SpecHelper.expectNoElement(
                    elem,
                    `connection-bar-button [data-id="${relationshipsByStatus.rejected_by_candidate.id}"]`,
                );
                // this user is matched and in "Needs Response"/new section because the most recent message is not from the candidate
                SpecHelper.expectElement(
                    elem,
                    `[section-name="'new'"] connection-bar-button [data-id="${relationshipsByStatus.matched.id}"]`,
                );
            });

            it('should group "needs response" and "responded" connections', () => {
                careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
                const viewModelsByStatus = careersNetworkViewModel.testHelper.getRelationshipViewModelsByStatus();

                jest.spyOn(viewModelsByStatus.matched, 'newInstantMatch', 'get').mockReturnValue(true);

                jest.spyOn(viewModelsByStatus.waiting_on_candidate.hiringRelationship, 'status', 'get').mockReturnValue(
                    'matched',
                );
                jest.spyOn(viewModelsByStatus.waiting_on_candidate, 'matched', 'get').mockReturnValue(true);
                jest.spyOn(viewModelsByStatus.waiting_on_candidate, 'needsResponseFromMe', 'get').mockReturnValue(
                    false,
                );

                render(false);

                // newInstantMatch should be in "Needs Response"/new group
                SpecHelper.expectElement(
                    elem,
                    `[section-name="'new'"] connection-bar-button [data-id="${viewModelsByStatus.matched.hiringRelationship.id}"]`,
                );
                // needsResponseFromMe === false should be in "Responded"/connected group
                SpecHelper.expectElement(
                    elem,
                    `[section-name="'connected'"] connection-bar-button [data-id="${viewModelsByStatus.waiting_on_candidate.hiringRelationship.id}"]`,
                );
            });
        });
    });

    describe('duplicate connections', () => {
        it('should de-dupe and consolidate connections when multiple teammates have saved for later the same candidate', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                teammateId: 'teammate',
                duplicateCandidate: true, // duplicates the saved for later candidate
            });

            render(false, 'hiringTeammate');

            expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded).toHaveBeenCalledWith('teamCandidates');
            expect(scope.role).toEqual('hiringTeammate');

            SpecHelper.click(elem, '.candidates-saved');

            // test only 1 connection bar and has the consolidated information
            const candidateListActions = SpecHelper.expectElement(elem, 'candidate-list candidate-list-actions:eq(0)');
            SpecHelper.expectElementText(candidateListActions, '.badges', 'teammate teammate 2');

            // test that the information from duplicates gets added to the original
            expect(scope.viewModelLists.saved_for_later[0].teammateAvatarSrcs.length > 0).toBe(true);
            expect(scope.viewModelLists.saved_for_later[0].teammateNames.length > 0).toBe(true);
            expect(scope.viewModelLists.saved_for_later[0].teammateHiringManagerIds.length > 0).toBe(true);
        });
    });

    describe('closed', () => {
        it('should show closed conversations only if closed===true', () => {
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            const hiringRelationshipId = hiringRelationshipViewModel.hiringRelationship.id;
            Object.defineProperty(hiringRelationshipViewModel, 'closed', {
                value: true,
            });

            render(true);
            expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded).toHaveBeenCalledWith('closed');
            expect(scope.viewModelLists.closed[0].hiringRelationshipId).toBe(hiringRelationshipId);
        });

        it('should show declined connections for hiring relationship view-models where rejectedByCandidate is true', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            const hiringRelationshipId = hiringRelationshipViewModel.hiringRelationship.id;
            jest.spyOn(hiringRelationshipViewModel, 'rejectedByCandidate', 'get').mockReturnValue(true);

            render(true);
            expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded.mock.calls.length).toBe(1); // closed
            expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded).toHaveBeenCalledWith('closed');
            expect(scope.viewModelLists.declined[0].hiringRelationshipId).toBe(hiringRelationshipId);
        });

        it('should show closed team relationships for a hiring manager', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                teammateId: 'teammate',
            });

            const closedHiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            const closedHiringRelationshipId = closedHiringRelationshipViewModel.hiringRelationship.id;
            Object.defineProperty(closedHiringRelationshipViewModel, 'closed', {
                value: true,
            });
            Object.defineProperty(closedHiringRelationshipViewModel, 'role', {
                value: 'hiringManager',
            });

            const closedTeamHiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[1];
            const closedTeamHiringRelationshipId = closedTeamHiringRelationshipViewModel.hiringRelationship.id;
            Object.defineProperty(closedTeamHiringRelationshipViewModel, 'closed', {
                value: true,
            });
            Object.defineProperty(closedTeamHiringRelationshipViewModel, 'role', {
                value: 'hiringTeammate',
            });

            render(true, 'hiringManager');

            expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded).toHaveBeenCalledWith('closed');
            expect(scope.viewModelLists.closed[0].hiringRelationshipId).toBe(closedHiringRelationshipId);
            expect(scope.viewModelLists.closed_team[0].hiringRelationshipId).toBe(closedTeamHiringRelationshipId);
        });

        it('should show non-closed conversations only if closed !==true', () => {
            const hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            const hiringRelationshipId = hiringRelationshipViewModel.hiringRelationship.id;
            Object.defineProperty(hiringRelationshipViewModel, 'closed', {
                value: false,
            });

            render(false);
            expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded).not.toHaveBeenCalledWith('closed');
            expect(
                _.contains(
                    _.chain(scope.viewModelLists.connected).pluck('hiringRelationshipId').value(),
                    hiringRelationshipId,
                ),
            ).toBe(true);
        });
    });

    describe('connection-bar-button click (candidate)', () => {
        let hiringRelationshipViewModel;
        let hiringRelationship;

        beforeEach(() => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('candidate');
            hiringRelationshipViewModel = careersNetworkViewModel.hiringRelationshipViewModels[0];
            hiringRelationship = hiringRelationshipViewModel.hiringRelationship;
        });

        it('should open the connection when a new request', () => {
            hiringRelationship.hiring_manager_status = 'accepted';
            hiringRelationship.candidate_status = 'pending';
            render(false);
            jest.spyOn(hiringRelationshipViewModel, 'openConnection').mockImplementation(() => {});
            SpecHelper.click(elem, `connection-bar-button [data-id="${hiringRelationship.id}"] button.chat`);
            expect(hiringRelationshipViewModel.openConnection).toHaveBeenCalled();
        });

        it('should open the connection when accepted', () => {
            hiringRelationship.hiring_manager_status = hiringRelationship.candidate_status = 'accepted';
            render(false);
            jest.spyOn(hiringRelationshipViewModel, 'openConnection').mockImplementation(() => {});
            SpecHelper.click(elem, `connection-bar-button [data-id="${hiringRelationship.id}"] button.chat`);
            expect(hiringRelationshipViewModel.openConnection).toHaveBeenCalled();
        });
    });

    describe('open positions filter', () => {
        it('should include open positions from current list of connections', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager');
            const relationshipsByStatus = careersNetworkViewModel.testHelper.getRelationshipsByStatus();
            const hiringRelationship = relationshipsByStatus.waiting_on_candidate;
            hiringRelationship.open_position_id = openPositions[0].id;

            render(false);
            SpecHelper.click(elem, '.candidates-pending');

            expect(_.pluck(scope.openPositionOptions, 'id')).toEqual(['all', openPositions[0].id]);
            expect(_.pluck(scope.openPositionOptions, 'title')).toEqual(['All Positions', openPositions[0].title]);
        });
    });

    describe('teammates filter', () => {
        it('should include names current list of teammates', () => {
            careersNetworkViewModel = CareersSpecHelper.stubCareersNetworkViewModel('hiringManager', {
                teammateId: 'teammate',
            });
            const relationshipsByStatus = careersNetworkViewModel.testHelper.getRelationshipsByStatus();
            const hiringRelationship = relationshipsByStatus.waiting_on_candidate;

            render(false, 'hiringTeammate');

            expect(_.pluck(scope.teamMemberOptions, 'id')).toEqual(['all', 'teammate']);
            expect(_.pluck(scope.teamMemberOptions, 'name')).toEqual([
                'All Team Members',
                hiringRelationship.hiring_manager_display_name,
            ]);
        });
    });

    describe('search text', () => {
        it('should show everything with blank text filter', () => {
            render(false);
            scope.search.text = '';
            scope.$apply();

            SpecHelper.expectElements(
                elem,
                'candidate-list-card',
                careersNetworkViewModel.hiringRelationshipViewModels.length,
            );
        });

        it('should filter by skills properly', () => {
            careersNetworkViewModel.hiringRelationshipViewModels[0].connectionProfile.skills = [
                {
                    text: 'nunchucks',
                },
            ];
            render(false);
            scope.search.text = 'nunchucks';
            scope.$apply();

            SpecHelper.expectElements(elem, 'candidate-list-card', 1);
        });

        it('should filter by name properly', () => {
            careersNetworkViewModel.hiringRelationshipViewModels[0].connectionProfile.name = 'Caspian';
            render(false);
            scope.search.text = 'caspian';
            scope.$apply();

            SpecHelper.expectElements(elem, 'candidate-list-card', 1);
        });
    });

    describe('selectedOpenPositionId', () => {
        beforeEach(() => {
            careersNetworkViewModel.hiringRelationshipViewModels[0].hiringRelationship.open_position_id = '1';
        });

        it('should show all connections without open positions', () => {
            render(false);
            scope.proxy.selectedOpenPositionId = 'all';
            scope.$apply();

            SpecHelper.expectElements(
                elem,
                'candidate-list-card',
                careersNetworkViewModel.hiringRelationshipViewModels.length,
            );
        });

        it('should show connections with specific open position', () => {
            render(false);
            scope.proxy.selectedOpenPositionId = '1';
            scope.$apply();

            SpecHelper.expectElements(
                elem,
                'candidate-list-card',
                _.chain(careersNetworkViewModel.hiringRelationshipViewModels)
                    .map('hiringRelationship')
                    .where({
                        open_position_id: '1',
                    })
                    .size()
                    .value(),
            );
        });
    });

    describe('hiringManagerId', () => {
        it('should show all connections', () => {
            render(false);
            scope.proxy.selectedTeamMemberId = 'all';
            scope.$apply();

            SpecHelper.expectElements(
                elem,
                'candidate-list-card',
                careersNetworkViewModel.hiringRelationshipViewModels.length,
            );
        });

        it('should show connections with specific hiring manager', () => {
            careersNetworkViewModel.hiringRelationshipViewModels[0].hiringRelationship.hiring_manager_id = '1';
            render(false);
            scope.proxy.selectedTeamMemberId = '1';
            scope.$apply();

            SpecHelper.expectElements(elem, 'candidate-list-card', 1);
        });

        it('should show connections with specific hiring manager if multiple relationships', () => {
            careersNetworkViewModel.hiringRelationshipViewModels[0].hiringRelationship.hiring_manager_id = '1';
            careersNetworkViewModel.hiringRelationshipViewModels[0].teammateHiringManagerIds = ['1', '2'];
            render(false);
            scope.proxy.selectedTeamMemberId = '2';
            scope.$apply();

            SpecHelper.expectElements(elem, 'candidate-list-card', 1);
        });
    });

    describe('hiringRelationshipUpdate', () => {
        it('should ensureHiringRelationshipsLoaded on hiringRelationshipUpdate', () => {
            SpecHelper.stubDirective('tabs');
            render(false);
            $rootScope.$broadcast('hiringRelationshipUpdate');
            $timeout.flush();
            expect(careersNetworkViewModel.ensureHiringRelationshipsLoaded).toHaveBeenCalled();
        });
    });
});
