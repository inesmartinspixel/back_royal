import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohort_applications';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';

describe('FrontRoyal.Careers.EditCareerProfileHelper', () => {
    let $injector;
    let $location;
    let EditCareerProfileHelper;
    let $rootScope;
    let SpecHelper;
    let CohortApplication;
    let CareerProfile;
    let Cohort;
    let EventLogger;
    let user;
    let steps;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                $location = $injector.get('$location');
                EditCareerProfileHelper = $injector.get('EditCareerProfileHelper');
                $rootScope = $injector.get('$rootScope');
                CohortApplication = $injector.get('CohortApplication');
                $injector.get('CohortApplicationFixtures');
                CareerProfile = $injector.get('CareerProfile');
                Cohort = $injector.get('Cohort');
                EventLogger = $injector.get('EventLogger');
                $injector.get('CareerProfileFixtures');
                $injector.get('CohortFixtures');
            },
        ]);
        steps = angular.copy($injector.get('CAREER_PROFILE_FORM_STEPS'));

        user = SpecHelper.stubCurrentUser('learner');
        user.career_profile = CareerProfile.fixtures.getInstance();
    });

    describe('getStepsForApplicationForm()', () => {
        beforeEach(() => {
            $rootScope.currentUser.relevant_cohort = Cohort.fixtures.getInstance();
        });

        it('should add additional to CAREER_PROFILE_FORM_STEPS', () => {
            jest.spyOn(user, 'programType', 'get').mockReturnValue('mba');
            expect(EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser).length).toBe(9);
        });

        it('should mark everything as unrequired if retargeted_from_program_type', () => {
            $rootScope.currentUser.cohort_applications = [
                CohortApplication.fixtures.getInstance({
                    status: 'pending',
                    retargeted_from_program_type: 'program_type',
                }),
            ];
            const result = EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser);
            expect(result[0].requiredFields).toEqual({});
        });

        _.each(['mba', 'emba'], programType => {
            describe(`when user's programType is ${programType}`, () => {
                let programTypeSpy;

                beforeEach(() => {
                    programTypeSpy = jest.spyOn(user, 'programType', 'get').mockReturnValue(programType);
                });

                _.each(['more_about_you', 'select_skills', 'career_preferences'], stepName => {
                    it(`should remove the ${stepName} step`, () => {
                        let steps = EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser);
                        let step = _.findWhere(steps, {
                            stepName,
                        });
                        expect(step).toBeUndefined();

                        programTypeSpy.mockReturnValue(`not_${programType}`);
                        steps = EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser);
                        step = _.findWhere(steps, {
                            stepName,
                        });
                        expect(step).toBeDefined();
                    });
                });
            });
        });

        describe('program step', () => {
            it('should add program lockdown step', () => {
                jest.spyOn($rootScope.currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                    CohortApplication.fixtures.getInstance(),
                );
                jest.spyOn($rootScope.currentUser, 'reapplyingOrEditingApplication', 'get').mockReturnValue(true);
                jest.spyOn($rootScope.currentUser, 'isRejected', 'get').mockReturnValue(false);
                jest.spyOn($rootScope.currentUser, 'isExpelled', 'get').mockReturnValue(false);
                expect(EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser)[0].stepName).toBe(
                    'program_lockdown',
                );
            });

            it('should add program choice step when no lastCohortApplication', () => {
                jest.spyOn($rootScope.currentUser, 'lastCohortApplication', 'get').mockReturnValue(undefined);
                jest.spyOn($rootScope.currentUser, 'reapplyingOrEditingApplication', 'get').mockReturnValue(true);
                jest.spyOn($rootScope.currentUser, 'isRejected', 'get').mockReturnValue(false);
                jest.spyOn($rootScope.currentUser, 'isExpelled', 'get').mockReturnValue(false);
                expect(EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser)[0].stepName).toBe(
                    'program_choice',
                );
            });

            it('should add program choice step when not reapplyingOrEditingApplication', () => {
                jest.spyOn($rootScope.currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                    CohortApplication.fixtures.getInstance(),
                );
                jest.spyOn($rootScope.currentUser, 'reapplyingOrEditingApplication', 'get').mockReturnValue(false);
                jest.spyOn($rootScope.currentUser, 'isRejected', 'get').mockReturnValue(false);
                jest.spyOn($rootScope.currentUser, 'isExpelled', 'get').mockReturnValue(false);
                expect(EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser)[0].stepName).toBe(
                    'program_choice',
                );
            });

            it('should add program choice step when not user isRejected', () => {
                jest.spyOn($rootScope.currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                    CohortApplication.fixtures.getInstance(),
                );
                jest.spyOn($rootScope.currentUser, 'reapplyingOrEditingApplication', 'get').mockReturnValue(true);
                jest.spyOn($rootScope.currentUser, 'isRejected', 'get').mockReturnValue(true);
                jest.spyOn($rootScope.currentUser, 'isExpelled', 'get').mockReturnValue(false);
                expect(EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser)[0].stepName).toBe(
                    'program_choice',
                );
            });

            it('should add program choice step when not user isExpelled', () => {
                jest.spyOn($rootScope.currentUser, 'lastCohortApplication', 'get').mockReturnValue(
                    CohortApplication.fixtures.getInstance(),
                );
                jest.spyOn($rootScope.currentUser, 'reapplyingOrEditingApplication', 'get').mockReturnValue(true);
                jest.spyOn($rootScope.currentUser, 'isRejected', 'get').mockReturnValue(false);
                jest.spyOn($rootScope.currentUser, 'isExpelled', 'get').mockReturnValue(true);
                expect(EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser)[0].stepName).toBe(
                    'program_choice',
                );
            });
        });

        it('should addWorkToRequiredFields', () => {
            jest.spyOn(EditCareerProfileHelper, 'addWorkToRequiredFields').mockReturnValue(steps);
            EditCareerProfileHelper.getStepsForApplicationForm(user);
            expect(EditCareerProfileHelper.addWorkToRequiredFields).toHaveBeenCalledWith(expect.anything());
        });

        it('should addEducationToRequiredFields', () => {
            jest.spyOn(EditCareerProfileHelper, 'addWorkToRequiredFields').mockReturnValue(steps);
            jest.spyOn(EditCareerProfileHelper, 'addEducationToRequiredFields').mockReturnValue(steps);
            EditCareerProfileHelper.getStepsForApplicationForm(user);
            expect(EditCareerProfileHelper.addEducationToRequiredFields).toHaveBeenCalledWith(steps, user);
        });

        describe('with career network only selected', () => {
            it('should hide MBA application questions', () => {
                Object.defineProperty($rootScope.currentUser, 'programType', {
                    value: 'career_network_only',
                });
                expect(EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser).length).toBe(10);
            });

            it('should move the avatar_url requirement from more_about_you to career_preferences', () => {
                // First check that the avatar_url is required in the more_about_you form
                // and not in the career_preferences form for the base CAREER_PROFILE_FORM_STEPS
                const careerProfileMoreAboutYouFormStep = _.find(steps, step => step.stepName === 'more_about_you');
                const careerProfileCareerPreferencesFormStep = _.find(
                    steps,
                    step => step.stepName === 'career_preferences',
                );
                expect(careerProfileMoreAboutYouFormStep.requiredFields.avatar_url).toBe(true);
                expect(careerProfileCareerPreferencesFormStep.requiredFields.avatar_url).toBeUndefined();

                // Now mock the user's programType and get the steps for the application form
                // and verify that the avatar_url requirement has been moved from the more_about_you
                // form to the career_preferences form.
                jest.spyOn($rootScope.currentUser, 'programType', 'get').mockReturnValue('career_network_only');
                const applicationFormSteps = EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser);
                const applicationMoreAboutYouFormStep = _.find(
                    applicationFormSteps,
                    step => step.stepName === 'more_about_you',
                );
                const applicationCareerPreferencesFormStep = _.find(
                    applicationFormSteps,
                    step => step.stepName === 'career_preferences',
                );
                expect(applicationMoreAboutYouFormStep.requiredFields.avatar_url).toBeUndefined();
                expect(applicationCareerPreferencesFormStep.requiredFields.avatar_url).toBe(true);
            });
        });

        describe('with the business certificate selected', () => {
            beforeEach(() => {
                jest.spyOn($rootScope.currentUser, 'programType', 'get').mockReturnValue('the_business_certificate');
            });

            it('should include cert application questions', () => {
                const steps = EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser);
                expect(steps.length).toBe(11);
                const step = _.findWhere(steps, {
                    stepName: 'cert_application_questions',
                });
                expect(step).toEqual({
                    stepName: 'cert_application_questions',
                    requiredFields: {
                        short_answer_ask_professional_advice: true,
                        short_answer_greatest_achievement: true,
                        short_answer_why_pursuing: true,
                    },
                });
            });

            it('should require cert_application_questions if career profile was created on or after 02/22/2019', () => {
                $rootScope.currentUser.career_profile.created_at = new Date(2019, 1, 23).getTime() / 1000;

                const steps = EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser);
                const certApplicationQuestionsStep = _.findWhere(steps, {
                    stepName: 'cert_application_questions',
                });
                expect(certApplicationQuestionsStep.requiredFields).toEqual({
                    short_answer_why_pursuing: true,
                    short_answer_greatest_achievement: true,
                    short_answer_ask_professional_advice: true,
                });
            });

            it('should not require cert_application_questions if career profile was created before 02/22/2019', () => {
                $rootScope.currentUser.career_profile.created_at = new Date(2018, 1, 22).getTime() / 1000;

                const steps = EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser);
                const certApplicationQuestionsStep = _.findWhere(steps, {
                    stepName: 'cert_application_questions',
                });
                expect(certApplicationQuestionsStep.requiredFields).toEqual({});
            });

            it('should move the avatar_url requirement from more_about_you to career_preferences', () => {
                // First check that the avatar_url is required in the more_about_you form
                // and not in the career_preferences form for the base CAREER_PROFILE_FORM_STEPS
                const careerProfileMoreAboutYouFormStep = _.find(steps, step => step.stepName === 'more_about_you');
                const careerProfileCareerPreferencesFormStep = _.find(
                    steps,
                    step => step.stepName === 'career_preferences',
                );
                expect(careerProfileMoreAboutYouFormStep.requiredFields.avatar_url).toBe(true);
                expect(careerProfileCareerPreferencesFormStep.requiredFields.avatar_url).toBeUndefined();

                // Now mock the user's programType and get the steps for the application form
                // and verify that the avatar_url requirement has been moved from the more_about_you
                // form to the career_preferences form.
                jest.spyOn($rootScope.currentUser, 'programType', 'get').mockReturnValue('the_business_certificate');
                const applicationFormSteps = EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser);
                const applicationMoreAboutYouFormStep = _.find(
                    applicationFormSteps,
                    step => step.stepName === 'more_about_you',
                );
                const applicationCareerPreferencesFormStep = _.find(
                    applicationFormSteps,
                    step => step.stepName === 'career_preferences',
                );
                expect(applicationMoreAboutYouFormStep.requiredFields.avatar_url).toBeUndefined();
                expect(applicationCareerPreferencesFormStep.requiredFields.avatar_url).toBe(true);
            });
        });

        // FIXME: This entire describe block can be removed once we force users to upgrade their
        // clients to support these new certificate program types. See https://trello.com/c/0C1D0SaE.
        describe('with new certificate program type', () => {
            it('should not include program_choice step', () => {
                assertStepsAreNotPresentForProgramType('marketing_basics_certificate');
                assertStepsAreNotPresentForProgramType('accounting_basics_certificate');
                assertStepsAreNotPresentForProgramType('finance_basics_certificate');
                assertStepsAreNotPresentForProgramType('microeconomics_basics_certificate');
                assertStepsAreNotPresentForProgramType('statistics_basics_certificate');
            });

            function assertStepsAreNotPresentForProgramType(programType) {
                Object.defineProperty($rootScope.currentUser, 'programType', {
                    value: programType,
                    configurable: true,
                });
                const steps = EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser);
                const programChoiceStep = _.findWhere(steps, {
                    stepName: 'program_choice',
                });
                expect(programChoiceStep).toBeUndefined();
            }
        });

        describe('long term goals', () => {
            beforeEach(() => {
                Object.defineProperty($rootScope.currentUser, 'programType', {
                    value: 'career_network_only',
                });
                $rootScope.currentUser.cohort_applications = [
                    CohortApplication.fixtures.getInstance({
                        status: 'pending',
                    }),
                ];
            });

            it('should require long term goals', () => {
                const steps = EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser);
                const personalSummaryStep = _.findWhere(steps, {
                    stepName: 'personal_summary',
                });
                expect(personalSummaryStep.requiredFields.long_term_goal).toEqual(true);
                expect(EditCareerProfileHelper.supportsLongTermGoalField($rootScope.currentUser)).toBe(true);
            });
        });

        describe('application questions step', () => {
            it('should be added when programType is emba', () => {
                jest.spyOn($rootScope.currentUser, 'programType', 'get').mockReturnValue('emba');
                const steps = EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser);
                const step = _.findWhere(steps, {
                    stepName: 'emba_application_questions',
                });
                expect(step).toEqual({
                    stepName: 'emba_application_questions',
                    requiredFields: {
                        short_answer_why_pursuing: true,
                        short_answer_greatest_achievement: true,
                        short_answer_ask_professional_advice: true,
                    },
                });
            });

            it('should be added when programType is mba', () => {
                jest.spyOn($rootScope.currentUser, 'programType', 'get').mockReturnValue('mba');
                const steps = EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser);
                const step = _.findWhere(steps, {
                    stepName: 'mba_application_questions',
                });
                expect(step).toEqual({
                    stepName: 'mba_application_questions',
                    requiredFields: {
                        short_answer_why_pursuing: true,
                        short_answer_use_skills_to_advance: true,
                        short_answer_greatest_achievement: true,
                        short_answer_ask_professional_advice: true,
                    },
                });
            });
        });

        describe('consider_early_decision', () => {
            _.each(['mba', 'career_network_only', 'the_business_certificate'], programType => {
                it(`should not requre consider_early_decision when ${programType}`, () => {
                    jest.spyOn($rootScope.currentUser, 'programType', 'get').mockReturnValue(programType);

                    const steps = EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser);
                    const step = _.findWhere(steps, {
                        stepName: 'submit_application',
                    });
                    expect(step.requiredFields).toBe(undefined);
                });
            });

            describe('for EMBA application', () => {
                it('should require consider_early_decision when emba and last admission round deadline has not passed', () => {
                    jest.spyOn(
                        $rootScope.currentUser.relevant_cohort,
                        'lastAdmissionRoundDeadlineHasPassed',
                    ).mockReturnValue(false);
                    jest.spyOn(
                        $rootScope.currentUser.relevant_cohort,
                        'supportsEarlyDecisionConsideration',
                        'get',
                    ).mockReturnValue(true);
                    jest.spyOn($rootScope.currentUser, 'programType', 'get').mockReturnValue('emba');

                    const steps = EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser);
                    const required = _.findWhere(steps, {
                        stepName: 'submit_application',
                    }).requiredFields.consider_early_decision;
                    expect(
                        $rootScope.currentUser.relevant_cohort.lastAdmissionRoundDeadlineHasPassed,
                    ).toHaveBeenCalled();
                    expect(required).toBe(true);
                });

                it('should not require consider_early_decision when emba and last admission round deadline has passed', () => {
                    jest.spyOn(
                        $rootScope.currentUser.relevant_cohort,
                        'lastAdmissionRoundDeadlineHasPassed',
                    ).mockReturnValue(true);
                    jest.spyOn(
                        $rootScope.currentUser.relevant_cohort,
                        'supportsEarlyDecisionConsideration',
                        'get',
                    ).mockReturnValue(true);
                    jest.spyOn($rootScope.currentUser, 'programType', 'get').mockReturnValue('emba');

                    const steps = EditCareerProfileHelper.getStepsForApplicationForm($rootScope.currentUser);
                    const step = _.findWhere(steps, {
                        stepName: 'submit_application',
                    });
                    expect(
                        $rootScope.currentUser.relevant_cohort.lastAdmissionRoundDeadlineHasPassed,
                    ).toHaveBeenCalled();
                    expect(step.requiredFields).toBe(undefined);
                });
            });
        });
    });

    describe('getStepsForCareersForm()', () => {
        it('should not require city, state, country on the Career Profile if user had filled out legacy Google Place', () => {
            $rootScope.currentUser.career_profile = {
                place_id: 'foobar',
            };
            const basicInfoStep = _.findWhere(EditCareerProfileHelper.getStepsForCareersForm($rootScope.currentUser), {
                stepName: 'basic_info',
            });
            expect(basicInfoStep.requiredFields.city).toBeUndefined();
            expect(basicInfoStep.requiredFields.state).toBeUndefined();
            expect(basicInfoStep.requiredFields.country).toBeUndefined();
        });

        it('should addWorkToRequiredFields', () => {
            jest.spyOn(EditCareerProfileHelper, 'addWorkToRequiredFields').mockImplementation(() => {});
            jest.spyOn(EditCareerProfileHelper, 'addEducationToRequiredFields').mockImplementation(() => {});
            EditCareerProfileHelper.getStepsForCareersForm(user);
            expect(EditCareerProfileHelper.addWorkToRequiredFields).toHaveBeenCalledWith(steps);
        });

        it('should addEducationToRequiredFields', () => {
            jest.spyOn(EditCareerProfileHelper, 'addWorkToRequiredFields').mockReturnValue(steps);
            jest.spyOn(EditCareerProfileHelper, 'addEducationToRequiredFields').mockImplementation(() => {});
            EditCareerProfileHelper.getStepsForCareersForm(user);
            expect(EditCareerProfileHelper.addEducationToRequiredFields).toHaveBeenCalledWith(steps, user);
        });

        it('should set min number of required awards/interests to 1 if career profile was created before 06/01/2017', () => {
            $rootScope.currentUser.career_profile.created_at = new Date(2017, 5, 1).getTime() / 1000;

            const steps = EditCareerProfileHelper.getStepsForCareersForm($rootScope.currentUser);
            const moreAboutYouStep = _.findWhere(steps, {
                stepName: 'more_about_you',
            });
            expect(moreAboutYouStep.requiredFields.awards_and_interests.min).toEqual(1);

            const selectSkillsStep = _.findWhere(steps, {
                stepName: 'select_skills',
            });
            expect(selectSkillsStep.requiredFields.skills.min).toEqual(1);
        });
    });

    describe('getStepsForApplicantEditorForm()', () => {
        it('should match getStepsForCareersForm when not in MBA/EMBA except for birthdate, preview profile, and cert application questions', () => {
            Object.defineProperty($rootScope.currentUser, 'programType', {
                value: 'the_business_certificate',
            });
            const steps1 = EditCareerProfileHelper.getStepsForCareersForm($rootScope.currentUser);
            // remove birthday
            delete steps1[0].requiredFields.birthdate;
            // remove preview_profile
            steps1.splice(8, 1);
            const steps2 = EditCareerProfileHelper.getStepsForApplicantEditorForm($rootScope.currentUser);
            // remove cert application questions
            steps2.splice(steps2.length - 1, 1)[0];
            expect(steps1).toEqual(steps2);
        });

        it('should match getStepsForCareersForm, but include MBA questions step and exclude preview profile and network settings steps', () => {
            Object.defineProperty($rootScope.currentUser, 'programType', {
                value: 'mba',
            });
            const steps1 = EditCareerProfileHelper.getStepsForCareersForm($rootScope.currentUser);
            const steps2 = EditCareerProfileHelper.getStepsForApplicantEditorForm($rootScope.currentUser);
            const applicationQuestionsStep = steps2.splice(steps2.length - 1, 1)[0];

            // remove preview profile step and network settings step
            steps1.splice(8, 2);

            expect(steps1).toEqual(steps2);
            expect(applicationQuestionsStep).toEqual({
                stepName: 'mba_application_questions',
                requiredFields: {
                    short_answer_why_pursuing: true,
                    short_answer_use_skills_to_advance: true,
                    short_answer_greatest_achievement: true,
                    short_answer_ask_professional_advice: true,
                },
            });
        });

        it('should addWorkToRequiredFields', () => {
            jest.spyOn(EditCareerProfileHelper, 'addWorkToRequiredFields').mockReturnValue(steps);
            jest.spyOn(EditCareerProfileHelper, 'addEducationToRequiredFields').mockReturnValue(steps);
            EditCareerProfileHelper.getStepsForApplicantEditorForm(user);
            expect(EditCareerProfileHelper.addWorkToRequiredFields).toHaveBeenCalledWith(steps);
        });

        it('should addEducationToRequiredFields', () => {
            jest.spyOn(EditCareerProfileHelper, 'addWorkToRequiredFields').mockReturnValue(steps);
            jest.spyOn(EditCareerProfileHelper, 'addEducationToRequiredFields').mockReturnValue(steps);
            EditCareerProfileHelper.getStepsForApplicantEditorForm(user);
            expect(EditCareerProfileHelper.addEducationToRequiredFields).toHaveBeenCalledWith(steps, user);
        });
    });

    describe('getStepsForCareerProfile()', () => {
        it('should match getStepsForCareersForm when not in MBA/EMBA, with some tweaks', () => {
            Object.defineProperty($rootScope.currentUser, 'programType', {
                value: 'the_business_certificate',
            });
            const stepsForCareersForm = EditCareerProfileHelper.getStepsForCareersForm($rootScope.currentUser);

            // stepsForCareersForm includes some requiredFields that we expect to not be included in
            // stepsForCareerProfile, so we need to delete those fields from stepsForCareersForm to
            // assert that they're not included in stepsForCareersForm
            delete stepsForCareersForm[0].requiredFields.birthdate;
            delete stepsForCareersForm[2].requiredFields.education_experiences;
            delete stepsForCareersForm[2].requiredFields.has_no_formal_education;
            delete stepsForCareersForm[2].requiredFields.survey_highest_level_completed_education_description;
            delete stepsForCareersForm[3].requiredFields.fullTimeWorkExperiences;
            delete stepsForCareersForm[3].requiredFields.survey_most_recent_role_description;
            delete stepsForCareersForm[3].requiredFields.survey_years_full_time_experience;
            delete stepsForCareersForm[3].requiredFields.featuredWorkExperience;
            delete stepsForCareersForm[6].requiredFields.how_did_you_hear_about_us;

            stepsForCareersForm.splice(8, 1); // preview_profile

            const stepsForCareerProfile = EditCareerProfileHelper.getStepsForCareerProfile(
                $rootScope.currentUser.career_profile,
            );
            expect(stepsForCareersForm).toEqual(stepsForCareerProfile);
        });
    });

    describe('logEventForApplication()', () => {
        it('should log an event with empty label when Application context', () => {
            jest.spyOn(EventLogger.prototype, 'allowEmptyLabel').mockImplementation(() => {});
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(true);
            EditCareerProfileHelper.logEventForApplication('foo');

            expect(EventLogger.prototype.allowEmptyLabel).toHaveBeenCalledWith(['application-form:foo']);
            expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                'application-form:foo',
                {
                    program_type: user.programType,
                },
                {
                    cordovaFacebook: false,
                },
            );
        });

        it('should not log an event with empty label when not in the MBA or EMBA Application context', () => {
            jest.spyOn(EventLogger.prototype, 'allowEmptyLabel').mockImplementation(() => {});
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(false);
            EditCareerProfileHelper.logEventForApplication('foo');

            expect(EventLogger.prototype.allowEmptyLabel).not.toHaveBeenCalled();
            expect(EventLogger.prototype.log).not.toHaveBeenCalled();
        });

        it('should log to cordovaFacebook if submit-application event', () => {
            jest.spyOn(EventLogger.prototype, 'allowEmptyLabel').mockImplementation(() => {});
            jest.spyOn(EventLogger.prototype, 'log').mockImplementation(() => {});
            jest.spyOn(EditCareerProfileHelper, 'isApplication').mockReturnValue(true);
            EditCareerProfileHelper.logEventForApplication('submit-application');

            expect(EventLogger.prototype.allowEmptyLabel).toHaveBeenCalledWith(['application-form:submit-application']);
            expect(EventLogger.prototype.log).toHaveBeenCalledWith(
                'application-form:submit-application',
                {
                    program_type: user.programType,
                },
                {
                    cordovaFacebook: true,
                },
            );
        });
    });

    describe('addSubmitApplicationStep', () => {
        it('should add submit_application step', () => {
            steps = EditCareerProfileHelper.addSubmitApplicationStep(steps, user);
            const submitApplicationStep = _.last(steps);
            expect(submitApplicationStep.stepName).toEqual('submit_application');
        });

        describe("when isApplicantEditor and programType is 'emba'", () => {
            it('should add consider_early_decision to requiredFields', () => {
                jest.spyOn(user, 'programType', 'get').mockReturnValue('emba');
                user.relevant_cohort = {
                    supportsEarlyDecisionConsideration: true,
                };
                jest.spyOn(EditCareerProfileHelper, 'isApplicantEditor').mockReturnValue(true);
                steps = EditCareerProfileHelper.addSubmitApplicationStep(steps, user);
                const submitApplicationStep = _.last(steps);
                expect(submitApplicationStep.requiredFields.consider_early_decision).toBe(true);
            });
        });

        describe("when programType is 'emba' and lastAdmissionRoundDeadline has not passed", () => {
            it('should add consider_early_decision to requiredFields', () => {
                user.relevant_cohort = Cohort.fixtures.getInstance();
                jest.spyOn(user, 'programType', 'get').mockReturnValue('emba');
                jest.spyOn(user.relevant_cohort, 'lastAdmissionRoundDeadlineHasPassed').mockReturnValue(false);
                jest.spyOn(user.relevant_cohort, 'supportsEarlyDecisionConsideration', 'get').mockReturnValue(true);
                steps = EditCareerProfileHelper.addSubmitApplicationStep(steps, user);
                const submitApplicationStep = _.last(steps);
                expect(submitApplicationStep.requiredFields.consider_early_decision).toBe(true);
            });
        });
    });

    describe('addWorkToRequiredFields', () => {
        it('should add fullTimeWorkExperiences to work step', () => {
            const workStep = _.findWhere(steps, {
                stepName: 'work',
            });
            expect(workStep.requiredFields.fullTimeWorkExperiences).toBeUndefined();
            EditCareerProfileHelper.addWorkToRequiredFields(steps);
            expect(workStep.requiredFields.fullTimeWorkExperiences).toEqual({
                min: 1,
                requiredFields: {
                    professional_organization: true,
                    job_title: true,
                    start_date: true,
                    responsibilities: {
                        min: 1,
                    },
                },
            });
        });

        it('should add featuredWorkExperience to work step', () => {
            const workStep = _.findWhere(steps, {
                stepName: 'work',
            });
            expect(workStep.requiredFields.featuredWorkExperience).toBeUndefined();
            EditCareerProfileHelper.addWorkToRequiredFields(steps);
            expect(workStep.requiredFields.featuredWorkExperience).toBe(true);
        });

        it('should set min number of required responsibilities to 1', () => {
            steps = EditCareerProfileHelper.addWorkToRequiredFields(steps);
            const workStep = _.findWhere(steps, {
                stepName: 'work',
            });
            expect(workStep.requiredFields.fullTimeWorkExperiences.requiredFields.responsibilities.min).toEqual(1);
        });

        it('should set survey_years_full_time_experience and survey_most_recent_role_description as required', () => {
            steps = EditCareerProfileHelper.addWorkToRequiredFields(steps);
            const workStep = _.findWhere(steps, {
                stepName: 'work',
            });
            expect(workStep.requiredFields.survey_years_full_time_experience).toBe(true);
            expect(workStep.requiredFields.survey_most_recent_role_description).toBe(true);
        });
    });

    describe('addEducationToRequiredFields', () => {
        beforeEach(() => {
            jest.spyOn(EditCareerProfileHelper, 'supportsRequiredEducation').mockImplementation(() => {});
        });

        it("should check if user's career profile supportsRequiredEducation", () => {
            EditCareerProfileHelper.addEducationToRequiredFields(steps, user);
            expect(EditCareerProfileHelper.supportsRequiredEducation).toHaveBeenCalledWith(user);
        });

        it('should require native_english_speaker if isDegreeProgram and !isCareerProfile', () => {
            jest.spyOn(EditCareerProfileHelper, 'isDegreeProgram').mockReturnValue(false);
            jest.spyOn(EditCareerProfileHelper, 'isCareerProfile').mockReturnValue(true);
            steps = EditCareerProfileHelper.addEducationToRequiredFields(steps, user);
            const educationStep = _.findWhere(steps, {
                stepName: 'education',
            });
            expect(educationStep.requiredFields.native_english_speaker).toBeFalsy();
            EditCareerProfileHelper.isDegreeProgram.mockReturnValue(true);
            EditCareerProfileHelper.addEducationToRequiredFields(steps, user);
            expect(educationStep.requiredFields.native_english_speaker).toBeFalsy();
            EditCareerProfileHelper.isDegreeProgram.mockReturnValue(false);
            EditCareerProfileHelper.isCareerProfile.mockReturnValue(false);
            EditCareerProfileHelper.addEducationToRequiredFields(steps, user);
            expect(educationStep.requiredFields.native_english_speaker).toBeFalsy();
            EditCareerProfileHelper.isDegreeProgram.mockReturnValue(true);
            EditCareerProfileHelper.isCareerProfile.mockReturnValue(false);
            EditCareerProfileHelper.addEducationToRequiredFields(steps, user);
            expect(educationStep.requiredFields.native_english_speaker).toBe(true);
        });

        describe('when !supportsRequiredEducation', () => {
            beforeEach(() => {
                EditCareerProfileHelper.supportsRequiredEducation.mockReturnValue(false);
                jest.spyOn(EditCareerProfileHelper, 'highestLevelEducationRequiresFormalEducation');
            });

            describe('when highestLevelEducationRequiresFormalEducation', () => {
                beforeEach(() => {
                    EditCareerProfileHelper.highestLevelEducationRequiresFormalEducation.mockReturnValue(true);
                });

                it('should add education experience requirements to steps', () => {
                    steps = EditCareerProfileHelper.addEducationToRequiredFields(steps, user);
                    const educationStep = _.findWhere(steps, {
                        stepName: 'education',
                    });
                    expect(EditCareerProfileHelper.highestLevelEducationRequiresFormalEducation).toHaveBeenCalledWith(
                        user.career_profile,
                    );
                    expect(educationStep.requiredFields.education_experiences).toEqual({
                        min: 1,
                        requiredFields: {
                            educational_organization: true,
                            degree: true,
                            graduation_year: true,
                            major: true,
                        },
                    });
                });
            });

            describe('when !highestLevelEducationRequiresFormalEducation', () => {
                let educationStep;

                beforeEach(() => {
                    EditCareerProfileHelper.highestLevelEducationRequiresFormalEducation.mockReturnValue(false);
                    steps = EditCareerProfileHelper.addEducationToRequiredFields(steps, user);
                    educationStep = _.findWhere(steps, {
                        stepName: 'education',
                    });
                });

                it('should not add education experience requirements to steps', () => {
                    expect(EditCareerProfileHelper.highestLevelEducationRequiresFormalEducation).toHaveBeenCalledWith(
                        user.career_profile,
                    );
                    expect(educationStep.requiredFields.education_experiences).toBeUndefined();
                });

                it('should add has_no_formal_education as a required field', () => {
                    expect(educationStep.requiredFields.has_no_formal_education).toBe(true);
                });
            });
        });

        describe('when supportsRequiredEducation', () => {
            beforeEach(() => {
                EditCareerProfileHelper.supportsRequiredEducation.mockReturnValue(true);
            });

            it('should add education experience requirements to steps', () => {
                steps = EditCareerProfileHelper.addEducationToRequiredFields(steps, user);
                const educationStep = _.findWhere(steps, {
                    stepName: 'education',
                });
                expect(educationStep.requiredFields.education_experiences).toEqual({
                    min: 1,
                    requiredFields: {
                        educational_organization: true,
                        degree: true,
                        graduation_year: true,
                        major: true,
                    },
                });
            });
        });

        it('should set survey_highest_level_completed_education_description as required', () => {
            steps = EditCareerProfileHelper.addEducationToRequiredFields(steps, user);
            const educationStep = _.findWhere(steps, {
                stepName: 'education',
            });
            expect(educationStep.requiredFields.survey_highest_level_completed_education_description).toBe(true);
        });
    });

    describe('Edit Career Profile context', () => {
        beforeEach(() => {
            jest.spyOn($location, 'path').mockReturnValue('/settings/my-profile');
        });

        it('should return true from isCareerProfile', () => {
            expect(EditCareerProfileHelper.isCareerProfile()).toBe(true);
        });

        it('should return correct rootUrl', () => {
            expect(EditCareerProfileHelper.getRootUrl()).toBe('/settings/my-profile');
        });
    });

    describe('MBA Application context', () => {
        beforeEach(() => {
            Object.defineProperty($rootScope.currentUser, 'programType', {
                value: 'mba',
            });
            jest.spyOn($location, 'path').mockReturnValue('/settings/application');
        });

        it('should return true from isApplication', () => {
            expect(EditCareerProfileHelper.isMbaApplication()).toBe(true);
        });

        it('should return true from isMbaApplication', () => {
            expect(EditCareerProfileHelper.isMbaApplication()).toBe(true);
        });

        it('should return true false isEmbaApplication', () => {
            expect(EditCareerProfileHelper.isEmbaApplication()).toBe(false);
        });

        it('should return true false supportsEarlyCareerApplicants', () => {
            expect(EditCareerProfileHelper.supportsEarlyCareerApplicants($rootScope.currentUser)).toBe(false);
        });

        it('should return correct rootUrl', () => {
            expect(EditCareerProfileHelper.getRootUrl()).toBe('/settings/application');
        });
    });

    describe('EMBA Application context', () => {
        beforeEach(() => {
            Object.defineProperty($rootScope.currentUser, 'programType', {
                value: 'emba',
            });
            jest.spyOn($location, 'path').mockReturnValue('/settings/application');
        });

        it('should return true from isApplication', () => {
            expect(EditCareerProfileHelper.isApplication()).toBe(true);
        });

        it('should return true false isMbaApplication', () => {
            expect(EditCareerProfileHelper.isMbaApplication()).toBe(false);
        });

        it('should return true from isEmbaApplication', () => {
            expect(EditCareerProfileHelper.isEmbaApplication()).toBe(true);
        });

        it('should return true false supportsEarlyCareerApplicants', () => {
            expect(EditCareerProfileHelper.supportsEarlyCareerApplicants($rootScope.currentUser)).toBe(false);
        });

        it('should return correct rootUrl', () => {
            expect(EditCareerProfileHelper.getRootUrl()).toBe('/settings/application');
        });
    });

    describe('Biz Cert Application context', () => {
        beforeEach(() => {
            Object.defineProperty($rootScope.currentUser, 'programType', {
                value: 'the_business_certificate',
            });
            jest.spyOn($location, 'path').mockReturnValue('/settings/application');
        });

        it('should return true from isApplication', () => {
            expect(EditCareerProfileHelper.isApplication()).toBe(true);
        });

        it('should return true false isMbaApplication', () => {
            expect(EditCareerProfileHelper.isMbaApplication()).toBe(false);
        });

        it('should return true from isEmbaApplication', () => {
            expect(EditCareerProfileHelper.isEmbaApplication()).toBe(false);
        });

        it('should return true false supportsEarlyCareerApplicants', () => {
            expect(EditCareerProfileHelper.supportsEarlyCareerApplicants($rootScope.currentUser)).toBe(true);
        });

        it('should return correct rootUrl', () => {
            expect(EditCareerProfileHelper.getRootUrl()).toBe('/settings/application');
        });
    });
});
