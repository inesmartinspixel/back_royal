import 'AngularSpecHelper';
import 'Careers/angularModule';
import setSpecLocales from 'Translation/setSpecLocales';
import hideCandidateConfirmLocales from 'Careers/locales/careers/hide_candidate_confirm-en.json';

setSpecLocales(hideCandidateConfirmLocales);

describe('FrontRoyal.Careers.HideCandidateConfirm', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let currentUser;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);

        currentUser = SpecHelper.stubCurrentUser();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.currentUser = currentUser;
        renderer.render('<hide-candidate-confirm current-user="currentUser"></hide-candidate-confirm>');
        elem = renderer.elem;
        scope = elem.isolateScope();
        scope.$apply();
    }

    it('should save user on load', () => {
        jest.spyOn(currentUser, 'save').mockImplementation(() => {});
        render();
        expect(currentUser.save).toHaveBeenCalled();
    });
});
