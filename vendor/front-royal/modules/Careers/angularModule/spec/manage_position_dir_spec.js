import 'AngularSpecHelper';
import 'Careers/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/open_position_fixtures';
import setSpecLocales from 'Translation/setSpecLocales';
import managePositionLocales from 'Careers/locales/careers/manage_position-en.json';
import positionCardLocales from 'Careers/locales/careers/position_card-en.json';

setSpecLocales(managePositionLocales, positionCardLocales);

describe('FrontRoyal.Careers.ManagePosition', () => {
    let $injector;
    let SpecHelper;
    let renderer;
    let elem;
    let currentUser;
    let position;
    let subscription;
    let scope;
    let closeModal;
    let $q;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Careers', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            _$injector_ => {
                $injector = _$injector_;
                SpecHelper = $injector.get('SpecHelper');
                const HiringTeam = $injector.get('HiringTeam');
                $injector.get('HiringTeamFixtures');
                const OpenPosition = $injector.get('OpenPosition');
                $injector.get('OpenPositionFixtures');
                const Subscription = $injector.get('Subscription');
                $q = $injector.get('$q');

                currentUser = SpecHelper.stubCurrentUser();
                currentUser.hiring_team = HiringTeam.fixtures.getInstance();
                subscription = Subscription.new({
                    id: 'subscriptionId',
                });
                currentUser.hiring_team.subscriptions = [subscription];
                position = OpenPosition.fixtures.getInstance({
                    subscription_id: subscription.id,
                });
                closeModal = jest.fn();
            },
        ]);
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.position = position;
        renderer.scope.closeModal = closeModal;

        renderer.render('<manage-position position="position" close-modal="closeModal"></manage-position>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('available buttons', () => {
        it('should include expected buttons when hasActiveAutoRenewal', () => {
            setupHasActiveAutoRenewal(true);
            render();
            SpecHelper.expectElements(elem, 'button', 2);
            SpecHelper.expectElement(elem, 'button[name="cancel_renewal"]');
            SpecHelper.expectElement(elem, 'button[name="cancel_and_close"]');
        });

        it('should include expected buttons when !hasActiveAutoRenewal', () => {
            setupHasActiveAutoRenewal(false);
            render();
            SpecHelper.expectElements(elem, 'button', 2);
            SpecHelper.expectElement(elem, 'button[name="renew"]');
            SpecHelper.expectElement(elem, 'button[name="close"]');
        });

        it('should include expected buttons when hasActiveAutoRenewal and manually archived', () => {
            setupHasActiveAutoRenewal(false);
            position.manually_archived = true;
            render();
            SpecHelper.expectElements(elem, 'button', 2);
            SpecHelper.expectElement(elem, 'button[name="reopen_and_renew"]');
            SpecHelper.expectElement(elem, 'button[name="leave_closed"]');
        });
    });

    describe('buttons', () => {
        beforeEach(() => {
            jest.spyOn(position, 'save').mockReturnValue($q.when());
        });

        it('should have a working cancel button', () => {
            setupHasActiveAutoRenewal(true, '2019-06-08 12:00');
            render();
            const buttonIndex = 0;
            SpecHelper.expectElementText(elem, 'button', 'Cancel Renewal', buttonIndex);
            SpecHelper.expectElementText(elem, '.button-caption p', 'Cancel renewal and close on Jun 8', buttonIndex);
            SpecHelper.click(elem, 'button', buttonIndex);
            expect(scope.position.save).toHaveBeenCalledWith({
                new_value_for_cancel_at_period_end: true,
            });
            expect(scope.position.archived).toBe(false);
            expect(closeModal).toHaveBeenCalled();
        });

        it('should have a working cancelClose button', () => {
            setupHasActiveAutoRenewal(true);
            render();
            const buttonIndex = 1;
            SpecHelper.expectElementText(elem, 'button', 'Cancel and Close', buttonIndex);
            SpecHelper.expectElementText(
                elem,
                '.button-caption p',
                'Cancel renewal and close immediately',
                buttonIndex,
            );
            SpecHelper.click(elem, 'button', buttonIndex);
            expect(scope.position.save).toHaveBeenCalledWith({
                new_value_for_cancel_at_period_end: true,
            });
            expect(scope.position.archived).toBe(true);
            expect(closeModal).toHaveBeenCalled();
        });

        it('should have a working renew button', () => {
            setupHasActiveAutoRenewal(false, '2019-06-08 12:00');
            render();
            const buttonIndex = 0;
            SpecHelper.expectElementText(elem, 'button', 'Renew', buttonIndex);
            SpecHelper.expectElementText(elem, '.button-caption p', 'Enable renewal starting on Jun 8', buttonIndex);
            SpecHelper.click(elem, 'button', buttonIndex);
            expect(scope.position.save).toHaveBeenCalledWith({
                new_value_for_cancel_at_period_end: false,
            });
            expect(closeModal).toHaveBeenCalled();
        });

        it('should have a repoen and renew button that works to unarchive a position', () => {
            setupHasActiveAutoRenewal(false, '2019-06-08 12:00');
            position.archived = true;
            position.manually_archived = true;
            render();
            const buttonIndex = 0;
            SpecHelper.expectElementText(elem, 'button', 'Reopen and Renew', buttonIndex);
            SpecHelper.expectElementText(elem, '.button-caption p', 'Enable renewal starting on Jun 8', buttonIndex);
            SpecHelper.click(elem, 'button', buttonIndex);
            expect(scope.position.archived).toBe(false);
            expect(scope.position.manually_archived).toBe(false);
            expect(scope.position.save).toHaveBeenCalledWith({
                new_value_for_cancel_at_period_end: false,
            });
            expect(closeModal).toHaveBeenCalled();
        });

        it('should have a working close button', () => {
            setupHasActiveAutoRenewal(false);
            render();
            const buttonIndex = 1;
            SpecHelper.expectElementText(elem, 'button', 'Close', buttonIndex);
            SpecHelper.expectElementText(elem, '.button-caption p', 'Close job post immediately', buttonIndex);
            SpecHelper.click(elem, 'button', buttonIndex);
            expect(scope.position.save).toHaveBeenCalledWith(undefined); // assert called with no arguments
            expect(closeModal).toHaveBeenCalled();
        });

        it('should have a working leave_closed button', () => {
            setupHasActiveAutoRenewal(false);
            position.manually_archived = true;
            render();
            const buttonIndex = 1;
            SpecHelper.expectElementText(elem, 'button', 'Leave Closed', buttonIndex);
            SpecHelper.expectElementText(elem, '.button-caption p', 'Allow position to remain closed', buttonIndex);
            SpecHelper.click(elem, 'button', buttonIndex);
            expect(scope.position.save).not.toHaveBeenCalled();
            expect(closeModal).toHaveBeenCalled();
        });
    });

    function setupHasActiveAutoRenewal(val, formattedDate) {
        subscription.cancel_at_period_end = !val;
        subscription.current_period_end =
            (formattedDate ? new Date(formattedDate).getTime() : new Date().getTime()) / 1000;
    }
});
