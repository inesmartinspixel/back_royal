import 'AngularSpecHelper';
import 'CaptchaHelper/angularModule';

describe('captchaHelper', () => {
    let $injector;
    let $window;
    let captchaHelper;

    beforeEach(() => {
        angular.mock.module('SpecHelper', 'captchaHelper');

        angular.mock.inject(_$injector_ => {
            $injector = _$injector_;
            $window = $injector.get('$window');
            captchaHelper = $injector.get('captchaHelper');
        });

        jest.spyOn(captchaHelper, 'resetCaptcha');
        $window.grecaptcha = {
            reset() {
                // noop
            },
        };
        jest.spyOn($window.grecaptcha, 'reset');
        $window.captchaComplete = true;
    });

    describe('captcha resetting', () => {
        it('should reset captcha', () => {
            $window.CORDOVA = false;
            captchaHelper.resetCaptcha();
            expect($window.captchaComplete).toBe(false);
            expect($window.grecaptcha.reset).toHaveBeenCalled();
        });

        it('should not reset recaptcha if Cordova', () => {
            $window.CORDOVA = true;
            captchaHelper.resetCaptcha();
            expect($window.captchaComplete).toBe(true);
            expect($window.grecaptcha.reset).not.toHaveBeenCalled();
        });

        afterEach(() => {
            window.CORDOVA = undefined;
        });
    });
});
