import 'AngularSpecHelper';
import 'AngularHttpQueueAndRetry/angularModule';
import { generateGuid } from 'guid';

jest.mock('guid', () => ({
    generateGuid: jest.fn(),
}));

describe('HttpQueueAndRetry.HttpQueue', () => {
    let SpecHelper;
    let HttpQueue;
    let $timeout;
    let sendRequest;
    let $rootScope;
    let $httpBackend;
    let handleError;
    let EventLogger;

    beforeEach(() => {
        angular.mock.module('HttpQueueAndRetry', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                SpecHelper = $injector.get('SpecHelper');
                HttpQueue = $injector.get('HttpQueue');
                $timeout = $injector.get('$timeout');
                $rootScope = $injector.get('$rootScope');
                $httpBackend = $injector.get('$httpBackend');
                EventLogger = $injector.get('EventLogger');

                SpecHelper.stubEventLogging();

                // Giving these spies some useful names so hopefully the tests
                // will be easier to understand.

                // This callback is called sendRequest because, when it is resolved,
                // the request will be sent
                sendRequest = jest.fn();

                // This callback is called handleError because, when it is rejected,
                // the error will be sent back to whoever made the original request
                handleError = jest.fn();

                let i = 0;
                generateGuid.mockReset();
                generateGuid.mockImplementation(() => {
                    const id = `queueId_${i}`;
                    i += 1;
                    return id;
                });
            },
        ]);
    });

    describe('queue', () => {
        it('should immediately send request if nothing is in the queue', () => {
            HttpQueue.queue({}).then(sendRequest);

            // even though the promise is already resolved, the callback
            // is not called until after a timeout
            $timeout.flush();
            expect(sendRequest).toHaveBeenCalled();
        });

        it('should move a retried request to the front of the queue', () => {
            // put something else in the queue.  the retried request
            // should move in front of it
            HttpQueue.instance._queue = ['queuedConfig'];

            // since retry is true, this one gets pushed to the front
            // of the queue
            const retriedConfig = {
                httpQueue: {
                    retry: true,
                },
            };
            HttpQueue.queue(retriedConfig).then(sendRequest);

            // even though the promise is already resolved, the callback
            // is not called until after a timeout
            $timeout.flush();
            expect(sendRequest).toHaveBeenCalledWith(retriedConfig);
        });

        it('should handle it if something outside of HttpQueue initiates a retry', () => {
            const config = {
                url: '/test',
            };

            // The first time the request goes through $http,
            // the request interceptor here triggers HttpQueue.queue
            HttpQueue.queue(config);

            const callback = jest.fn();

            // Suppose the request errors, but there is a responseInterceptor
            // somewhere else that decides to retry the request, short-circuiting
            // the responseError interceptor in angularHttpQueueAndRetry.
            // In that case, the request hits HttpQueue.queue again, when it is
            // still the pending request.  HttpQueue should recognize that someone
            // else is retrying the pending request and let it through.
            HttpQueue.queue(config).then(callback);
            $timeout.flush();
            expect(callback).toHaveBeenCalled();
        });

        it('should send a high-priority request before a low-priority request', () => {
            // send out one request first so the next two
            // get queued up
            const pendingRequestConfig = {
                url: '/pending',
            };
            const lowPriorityConfig = {
                url: '/lowPriority',
                httpQueueOptions: {
                    priority: -10,
                },
            };
            const highPriorityConfig = {
                url: '/highPriority',
            };

            const callbacks = {
                pending: jest.fn(),
                low: jest.fn(),
                high: jest.fn(),
            };
            HttpQueue.queue(pendingRequestConfig).then(callbacks.pending);
            HttpQueue.queue(lowPriorityConfig).then(callbacks.low);
            HttpQueue.queue(highPriorityConfig).then(callbacks.high);

            $timeout.flush();
            expect(callbacks.pending).toHaveBeenCalled(); // sanity check
            HttpQueue.onResponseSuccess({ config: pendingRequestConfig });
            $timeout.flush();

            // the high-priority request should be sent next
            expect(callbacks.low).not.toHaveBeenCalled();
            expect(callbacks.high).toHaveBeenCalled();
        });

        // Fixing a bug where we were not handling it correctly when findIndex did not find anything
        it('should work when there are multiple higher and no lower-priority items in the queue', () => {
            // setup 3 high priority requests
            const highPriorityConfigs = [];
            const callbacks = {};
            for (let i = 0; i < 3; i++) {
                highPriorityConfigs.push({
                    url: `/highPriority/${i}`,
                });
                callbacks[i] = jest.fn();
            }

            // and then 1 low priority request
            const lowPriorityConfig = {
                url: '/lowPriority',
                httpQueueOptions: {
                    priority: -10,
                },
            };
            callbacks.low = jest.fn();
            HttpQueue.queue(highPriorityConfigs[0]).then(callbacks[0]);
            HttpQueue.queue(highPriorityConfigs[1]).then(callbacks[1]);
            HttpQueue.queue(highPriorityConfigs[2]).then(callbacks[2]);
            HttpQueue.queue(lowPriorityConfig).then(callbacks.low);

            for (let i = 0; i < 3; i++) {
                expect(callbacks[i]).not.toHaveBeenCalled();
                $timeout.flush();
                HttpQueue.onResponseSuccess({ config: highPriorityConfigs[i] });
                expect(callbacks[i]).toHaveBeenCalled();
                expect(callbacks.low).not.toHaveBeenCalled();
            }

            // the low-priority request should be sent next
            $timeout.flush();
            expect(callbacks.low).toHaveBeenCalled();
        });
    });

    describe('event logging', () => {
        it('should log events at useful times if logging is enabled', () => {
            HttpQueue.instance.loggingVerbosity = 'verbose';
            failAndRetry();

            const loggedEvents = EventLogger.instance.buffer;
            expect(loggedEvents.map(event => event.event_type)).toEqual([
                'http_queue:queue_request',
                'http_queue:send_request',
                'http_queue:request_error',
                'http_queue:retry',
                'http_queue:queue_request',
                'http_queue:send_request',
                'http_queue:request_success',
            ]);
        });

        it('should not log events if logging is disabled (default)', () => {
            HttpQueue.instance.loggingVerbosity = 'silent';
            const config = {};
            HttpQueue.queue(config).then(sendRequest);
            $timeout.flush();
            expect(sendRequest).toHaveBeenCalled();
            HttpQueue.onResponseSuccess({
                config,
            });

            const loggedEvents = EventLogger.instance.buffer;
            expect(loggedEvents.map(event => event.event_type)).toEqual([]);
        });

        it('should log some events if just set to log errors', () => {
            HttpQueue.instance.loggingVerbosity = 'errors';
            failAndRetry();

            // queue another request.  This one should not be logged
            HttpQueue.queue({
                url: '/test',
            });

            const loggedEvents = EventLogger.instance.buffer;
            expect(loggedEvents.map(event => event.event_type)).toEqual([
                'http_queue:request_error',
                'http_queue:retry',
                'http_queue:queue_request',
                'http_queue:send_request',
                'http_queue:request_success',
            ]);
        });
    });

    describe('reset', () => {
        it('should clear the queue and cancel any pending entries', () => {
            const config = {};
            HttpQueue.queue(config);
            const requestCanceler = config.timeout;
            expect(requestCanceler).not.toBeUndefined();
            const canceled = jest.fn();
            requestCanceler.then(canceled);

            HttpQueue.reset();
            $timeout.flush();

            expect(canceled).toHaveBeenCalled();
            expect(HttpQueue.instance._queue).toEqual([]);
            expect(HttpQueue.instance._pendingRequest).toBeUndefined();
            expect(HttpQueue.instance._lastFailedQueueId).toBeUndefined();
            expect(HttpQueue.instance._storedEvents).toEqual({});
        });
    });

    describe('retry', () => {
        it('should throw if request is not the last failed one', () => {
            expect(() => {
                HttpQueue.retry({});
            }).toThrow(new Error('Can only retry the last request that failed.'));
        });

        it('should retry the request if it is the last failed one', () => {
            failAndRetry();
        });
    });

    function failAndRetry() {
        const config = {
            url: '/test',
        };
        HttpQueue.queue(config);
        HttpQueue.onResponseError({
            status: 999,
            config,
        }).then(sendRequest, handleError);

        $httpBackend.verifyNoOutstandingExpectation();
        expect(handleError).toHaveBeenCalled();

        const callback = jest.fn();
        $httpBackend.expectGET('/test').respond(200, '');
        HttpQueue.retry(config).then(callback);
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.flush();
        expect(callback).toHaveBeenCalled();
    }

    // we stopped doing this when we added timeouts to requests.  Some
    // chance this can come back,
    // so commenting out for now.
    // describe('offline handling', function() {
    //     it('should abort an in flight request', function() {
    //         var config = {};
    //         HttpQueue.queue(config);
    //         var requestCanceler = config.timeout;
    //         expect(requestCanceler).not.toBeUndefined();
    //         var canceled = jest.fn();
    //         requestCanceler.then(canceled);

    //         $rootScope.$emit('offline');
    //         // honestly, I don't realluy know why we have to flush here
    //         $timeout.flush();
    //         expect(canceled).toHaveBeenCalled();
    //     });
    // });

    describe('_shiftIfNoRequestPending', () => {
        it('should add http_queue info to the config data', () => {
            jest.spyOn(HttpQueue.prototype, '_msSince').mockReturnValue(42);

            HttpQueue.queue({
                data: {},
            }).then(sendRequest);

            // even though the promise is already resolved, the callback
            // is not called until after a timeout
            $timeout.flush();
            expect(HttpQueue.prototype._msSince).toHaveBeenCalled();
            expect(sendRequest).toHaveBeenCalled();
            expect(sendRequest.mock.calls[0][0].data.http_queue).toEqual({
                queued_for_seconds: 42 / 1000,
                retry: false,
                attempt_count: 1,
            });
        });

        // This is not functionality we necessarily want.  Eventually GET requests, which
        // have no data, should also support queued_for_seconds, but we don't need this
        // now.
        it('should not add queued_for_seconds if there is no config data', () => {
            jest.spyOn(HttpQueue.prototype, '_msSince').mockReturnValue(42);

            HttpQueue.queue({}).then(sendRequest);

            // even though the promise is already resolved, the callback
            // is not called until after a timeout
            $timeout.flush();
            expect(HttpQueue.prototype._msSince).not.toHaveBeenCalled();
        });
    });

    describe('onResponseSuccess', () => {
        it('should send the next queued request if this is the pending request', () => {
            HttpQueue.queue({}).then(sendRequest);
            HttpQueue.queue({}).then(sendRequest);

            $timeout.flush();
            expect(sendRequest.mock.calls.length).toBe(1);

            HttpQueue.onResponseSuccess({
                config: {
                    httpQueue: {
                        queueId: 'queueId_0',
                    },
                },
            });

            $timeout.flush();
            expect(sendRequest.mock.calls.length).toBe(2);
        });

        it('should do nothing if this is not the pending request', () => {
            HttpQueue.queue({}).then(sendRequest);
            HttpQueue.queue({}).then(sendRequest);

            $timeout.flush();
            expect(sendRequest.mock.calls.length).toBe(1);

            HttpQueue.onResponseSuccess({
                config: {
                    // this is some other request that was never queued
                },
            });
            expect(sendRequest.mock.calls.length).toBe(1);
        });
    });

    describe('onResponseError', () => {
        it('should set the retry to true and resend the request', () => {
            const config = {
                url: '/test',
            };
            HttpQueue.queue(config).then(sendRequest);
            $timeout.flush();
            expect(sendRequest.mock.calls.length).toBe(1);

            $httpBackend.expectGET('/test').respond(0, '');
            HttpQueue.onResponseError({
                status: 0,
                config,
            }).then(sendRequest, handleError);

            $httpBackend.verifyNoOutstandingExpectation();
            expect(sendRequest).toHaveBeenCalled();
            expect(handleError).not.toHaveBeenCalled();
        });

        it('should return a rejected promise if not a retryable status code', () => {
            const config = {
                url: '/test',
            };
            HttpQueue.queue(config).then(sendRequest);
            $timeout.flush();
            expect(sendRequest.mock.calls.length).toBe(1);

            sendRequest.mockClear();
            HttpQueue.onResponseError({
                status: 'do_not_retry',
                config,
            }).then(sendRequest, handleError);

            $rootScope.$digest();
            $httpBackend.verifyNoOutstandingRequest();
            expect(sendRequest).not.toHaveBeenCalled();
            expect(handleError).toHaveBeenCalled();
        });

        it('should return a rejected promise if this is not the pending request', () => {
            HttpQueue.queue({}).then(sendRequest);

            $timeout.flush();
            expect(sendRequest.mock.calls.length).toBe(1);

            sendRequest.mockClear();
            HttpQueue.onResponseError({
                config: {
                    // this is some other request that was never queued
                },
            }).then(sendRequest, handleError);

            $rootScope.$digest();
            $httpBackend.verifyNoOutstandingRequest();
            expect(sendRequest).not.toHaveBeenCalled();
            expect(handleError).toHaveBeenCalled();
        });

        it('should return a rejected promise if we have reached the maxAttemptCount', () => {
            const config = {
                url: '/test',
            };
            HttpQueue.queue(config).then(sendRequest);
            $timeout.flush();
            expect(sendRequest.mock.calls.length).toBe(1);

            config.httpQueue.attemptCount = 42;
            sendRequest.mockClear();
            HttpQueue.onResponseError({
                status: 0,
                config,
            }).then(sendRequest, handleError);

            $rootScope.$digest();
            $httpBackend.verifyNoOutstandingRequest();
            expect(sendRequest).not.toHaveBeenCalled();
            expect(handleError).toHaveBeenCalled();
        });

        it('should not retry if we have disabled retry', () => {
            const config = {
                url: '/test',
                httpQueueOptions: {
                    disable_retry: true, // the magic words
                },
            };
            HttpQueue.queue(config).then(sendRequest);
            $timeout.flush();
            expect(sendRequest.mock.calls.length).toBe(1);

            $httpBackend.expectGET('/test').respond(0, '');
            sendRequest.mockClear();
            HttpQueue.onResponseError({
                status: 0,
                config,
            }).then(sendRequest, handleError);

            $httpBackend.verifyNoOutstandingRequest();
            expect(sendRequest).not.toHaveBeenCalled();
            expect(handleError).toHaveBeenCalled();
        });

        it('should not continue to send requests after an error', () => {
            assertFrozenAfterError();
        });
    });

    describe('unfreezeAfterError', () => {
        it('should allow more requests to be sent after an error', () => {
            const config = assertFrozenAfterError();
            HttpQueue.unfreezeAfterError(config);
            $timeout.flush();
            expect(sendRequest.mock.calls.length).toBe(1);
        });

        it('should throw if the last failed request config is not passed in', () => {
            assertFrozenAfterError();
            expect(() => {
                HttpQueue.unfreezeAfterError();
            }).toThrow(new Error('Unexpected config.'));
        });
    });

    function assertFrozenAfterError() {
        const config = {
            url: '/test',
        };
        HttpQueue.queue(config).then(sendRequest);
        $timeout.flush();
        expect(sendRequest.mock.calls.length).toBe(1);

        sendRequest.mockClear();
        HttpQueue.onResponseError({
            status: 'do_not_retry',
            config,
        }).then(sendRequest, handleError);

        $rootScope.$digest();
        $httpBackend.verifyNoOutstandingRequest();

        const config2 = {
            url: '/test',
        };
        HttpQueue.queue(config2).then(sendRequest);
        $timeout.flush();
        expect(sendRequest.mock.calls.length).toBe(0);
        return config;
    }
});
