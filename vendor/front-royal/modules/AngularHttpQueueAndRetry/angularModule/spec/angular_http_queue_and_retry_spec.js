import 'AngularSpecHelper';
import 'AngularHttpQueueAndRetry/angularModule';

describe('HttpQueueAndRetry', () => {
    let HttpQueueAndRetry;

    beforeEach(() => {
        angular.mock.module('HttpQueueAndRetry', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                HttpQueueAndRetry = $injector.get('HttpQueueAndRetry');
            },
        ]);
    });

    describe('shouldQueue', () => {
        it('should be true if there are no filters', () => {
            expect(HttpQueueAndRetry.shouldQueue()).toBe(true);
        });
        it('should be true if there is a matching filter', () => {
            HttpQueueAndRetry.addFilter(() => false);
            HttpQueueAndRetry.addFilter(() => true);
            expect(HttpQueueAndRetry.shouldQueue()).toBe(true);
        });
        it('should be false if there are no matching filters', () => {
            HttpQueueAndRetry.addFilter(() => false);
            HttpQueueAndRetry.addFilter(() => false);
            expect(HttpQueueAndRetry.shouldQueue()).toBe(false);
        });
        it('should be true if true is passed in', () => {
            HttpQueueAndRetry.addFilter(() => false);
            expect(HttpQueueAndRetry.shouldQueue({ httpQueueOptions: { shouldQueue: true } })).toBe(true);
        });
        it('should be false if false is passed in', () => {
            expect(HttpQueueAndRetry.shouldQueue({ httpQueueOptions: { shouldQueue: false } })).toBe(false);
        });
    });
});
