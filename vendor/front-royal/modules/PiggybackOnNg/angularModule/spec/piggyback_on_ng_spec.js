import 'AngularSpecHelper';
import 'PiggybackOnNg/angularModule';

describe('PiggybackOnNg', () => {
    let PiggybackOnNg;
    let SpecHelper;
    let renderer;

    beforeEach(() => {
        angular.mock.module('PiggybackOnNg', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                PiggybackOnNg = $injector.get('PiggybackOnNg');
                SpecHelper = $injector.get('SpecHelper');
            },
        ]);
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for adding global callback to ng- events', () => {
        const piggyBackedCallback = jest.fn();
        PiggybackOnNg.on('ngClick', piggyBackedCallback);
        render();
        renderer.scope.callback = jest.fn();
        renderer.elem.click();

        expect(renderer.scope.callback).toHaveBeenCalled();
        expect(piggyBackedCallback).toHaveBeenCalled();

        SpecHelper.expectEqual(
            renderer.scope,
            piggyBackedCallback.mock.calls[0][0],
            'scope passed to piggyBackedCallback',
        );
        SpecHelper.expectEqual(
            renderer.elem[0],
            piggyBackedCallback.mock.calls[0][1][0],
            'element passed to piggyBackedCallback',
        );

        SpecHelper.expectEqual(
            renderer.elem[0],
            piggyBackedCallback.mock.calls[0][2].$$element[0],
            'attr passed to piggyBackedCallback',
        );

        SpecHelper.expectEqual(
            'click',
            piggyBackedCallback.mock.calls[0][3].type,
            'event passed to piggyBackedCallback',
        );
    });

    it('should also add callback to noApply events', () => {
        const piggyBackedCallback = jest.fn();
        PiggybackOnNg.on('ngClick', piggyBackedCallback);
        renderNoApplyClick();
        renderer.scope.callback = jest.fn();
        renderer.elem.click();
        expect(renderer.scope.callback).toHaveBeenCalled();
        expect(piggyBackedCallback).toHaveBeenCalled();

        SpecHelper.expectEqual(
            renderer.scope,
            piggyBackedCallback.mock.calls[0][0],
            'scope passed to piggyBackedCallback',
        );
        SpecHelper.expectEqual(
            renderer.elem[0],
            piggyBackedCallback.mock.calls[0][1][0],
            'element passed to piggyBackedCallback',
        );

        SpecHelper.expectEqual(
            renderer.elem[0],
            piggyBackedCallback.mock.calls[0][2].$$element[0],
            'attr passed to piggyBackedCallback',
        );

        SpecHelper.expectEqual(
            'click',
            piggyBackedCallback.mock.calls[0][3].type,
            'event passed to piggyBackedCallback',
        );
    });

    function render() {
        renderer = SpecHelper.render('<div ng-click="callback()"></div>');
        renderer.scope.callback = null;
    }

    function renderNoApplyClick() {
        renderer = SpecHelper.render('<div no-apply-click="callback()"></div>');
        renderer.scope.callback = null;
    }
});
