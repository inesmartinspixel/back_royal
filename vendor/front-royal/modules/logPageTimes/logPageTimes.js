// this stuff is tested in spec/puppeteer/dynamic_landing_page_screenshot_spec.js
import { logTinyEvent, BROWSER_AND_BUILD_INFO } from 'TinyEventLogger';
import ServerTime, { ensureServerClientTimeOffsetOnWindow } from 'ServerTime';

function getQueryVariable(variable) {
    const url = new URL(window.location.href);
    return url.searchParams.get(variable);
}

function logUtmParams(serverTime) {
    const logProperties = {};
    ['source', 'content', 'campaign', 'medium'].forEach(suffix => {
        const key = `utm_${suffix}`;
        const val = getQueryVariable(key);
        if (val) {
            logProperties[key] = val;
        }
    });

    if (Object.keys(logProperties).length > 0) {
        logTinyEvent('user:set_utm_params', logProperties, serverTime);
    }
}

export default function logPageTimes() {
    const serverTime = new ServerTime(ensureServerClientTimeOffsetOnWindow());

    logTinyEvent('loading_page', { value: window.performance.now(), ...BROWSER_AND_BUILD_INFO }, serverTime);
    logUtmParams(serverTime);

    let hasScrolled = false;

    document.addEventListener('DOMContentLoaded', () => {
        const timeAtDomContentLoaded = window.performance.now();
        logTinyEvent('dom_content_loaded', { value: timeAtDomContentLoaded, has_scrolled: hasScrolled }, serverTime);
    });

    [5 * 1000, 10 * 1000, 30 * 1000].forEach(delay => {
        setTimeout(() => {
            logTinyEvent('still_on_page', { value: delay, has_scrolled: hasScrolled }, serverTime);
        }, delay);
    });

    // Since we're passing this to removeEventListener, it makes
    // theoretical sense to define it locally inside of logPageTimes,
    // but in practice it could be defined above and it wouldn't matter.
    function onScroll() {
        hasScrolled = true;
        window.removeEventListener('scroll', onScroll);
        logTinyEvent('scrolled_first_time', { value: window.performance.now() }, serverTime);
    }
    window.addEventListener('scroll', onScroll);
}
