import {
    TweenLite,
    TweenMax,
    TimelineMax,
    Power1,
    Power2,
    Bounce,
    CSSPlugin,
    AttrPlugin,
    ScrollToPlugin,
} from 'gsap/all';
import * as jQueryPlugin from 'gsap/src/uncompressed/jquery.gsap';

// Prevent tree-shaking the plugins since they aren't referenced in
// any of our code that imports this module.
// See also: https://greensock.com/docs/v2/NPMUsage
// eslint-disable-next-line no-unused-vars
const plugins = [CSSPlugin, AttrPlugin, ScrollToPlugin, jQueryPlugin];

export { TweenLite, TweenMax, TimelineMax, Power1, Power2, Bounce };
