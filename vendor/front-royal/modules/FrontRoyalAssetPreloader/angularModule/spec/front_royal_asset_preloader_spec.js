import 'AngularSpecHelper';
import 'FrontRoyalAssetPreloader/angularModule';

_.each([false, true], cordovaMode => {
    describe('FrontRoyal.AssetPreloader run', () => {
        let $rootScope;

        beforeEach(() => {
            window.CORDOVA = cordovaMode;

            window.cordova = {
                file: {
                    applicationDirectory: '/',
                },
            };

            angular.mock.module('FrontRoyal.AssetPreloader', $provide => {
                $rootScope = {
                    $on(eventName) {
                        if (eventName === '$viewContentLoaded' && window.CORDOVA) {
                            throw 'Improper call to $rootScope.$on() while in window.CORDOVA';
                        }
                    },
                };
                $provide.value('$rootScope', $rootScope);
            });

            angular.mock.inject(_$rootScope_ => {
                $rootScope = _$rootScope_;
            });
        });

        afterEach(() => {
            window.CORDOVA = undefined;
        });

        it(`should handle window.CORDOVA as ${window.CORDOVA}`, () => {
            // no-op (see $provide block)
        });
    });
});
