import { some, identity, map, reject } from 'lodash/fp';

async function flushBatch($injector) {
    const $http = $injector.get('$http');
    const $window = $injector.get('$window');
    const frontRoyalStore = $injector.get('frontRoyalStore');

    // Pull the oldest 100 events from the db
    let events = await frontRoyalStore.retryAfterHandledError(db =>
        db.events.orderBy('client_utc_timestamp').limit(100).toArray(),
    );

    if (!some(identity)(events)) {
        return false;
    }

    // The server is only able to handle bundles of events from a single
    // page load.  Maybe we should fix this on the server, but for now
    // at least trying to keep changes isolated to the FrontRoyalStore
    // as much as possible
    events = reject(event => event.page_load_id !== events[0].page_load_id)(events);

    // Eventually, we should remove the buffering from EventLogger.  The FrontRoyalStore
    // makes it unnecessary.  But, leaving that as-is for now to try to restrict
    // changes to store layer as much as possible.
    events.forEach(event => {
        event.buffered_time += new Date().getTime() / 1000 - event.saved_to_front_royal_store_at;
        delete event.saved_to_front_royal_store_at;
    });

    const formData = new FormData();
    formData.append(
        'record',
        JSON.stringify({
            events,
        }),
    );
    formData.append('flushingFrontRoyalStore', 'true');

    const request = {
        method: 'POST',
        url: `${$window.ENDPOINT_ROOT}/api/event_bundles.json`,
        data: formData,
        headers: {
            Accept: 'application/json',

            // We have to set Content-type to undefined here.  When we
            // do that, the content-type ends up getting set to
            // multipart form with an appropriate boundary.  Otherwise
            // it ends up being application/json.  I guess maybe
            // $http is doing that
            'Content-type': undefined,
        },
    };

    await $http(request);

    await frontRoyalStore.retryAfterHandledError(db => db.events.where('id').anyOf(map('id')(events)).delete());

    return true;
}

export default async function flushStoredEvents($injector) {
    let hasMoreEvents = true;
    while (hasMoreEvents) {
        // NOTE: it would PROBABLY be fine to save all the batches
        // in parallel. Would also fix eslint nagging. Reconsider later.
        // eslint-disable-next-line no-await-in-loop
        hasMoreEvents = await flushBatch($injector);
    }
}
