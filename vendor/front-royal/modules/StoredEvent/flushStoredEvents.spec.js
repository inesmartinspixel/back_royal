import openTestDb from 'FrontRoyalStore/openTestDb';
import { map } from 'lodash/fp';
import flushStoredEvents from './flushStoredEvents';

describe('flushStoredEvents', () => {
    let $injector;
    let $http;
    const db = openTestDb();

    beforeEach(async () => {
        $http = jest.fn();
        $injector = {
            get: key =>
                ({
                    $http,
                    $window: {
                        ENDPOINT_ROOT: 'http://endpoint/root',
                    },
                    frontRoyalStore: { retryAfterHandledError: fn => fn(db) },
                }[key]),
        };
    });

    it('should save 100 events in each batch', async () => {
        const events = [];
        for (let i = 0; i < 101; i++) {
            events.push({
                id: `event-${i}`,
                client_utc_timestamp: i,
                page_load_id: 'my-page-load',
            });
        }
        await db.events.bulkPut(events);
        await flushStoredEvents($injector);
        expect($http.mock.calls.length).toBe(2);
        assertCall(0, events.slice(0, 100));
        assertCall(1, events.slice(100, 101));
    });

    it('should separate events from different page loads into different batches', async () => {
        const events = [
            {
                id: 'id-1',
                client_utc_timestamp: 1,
                page_load_id: 'page-load-1',
            },
            {
                id: 'id-2',
                client_utc_timestamp: 2,
                page_load_id: 'page-load-2',
            },
        ];
        await db.events.bulkPut(events);
        await flushStoredEvents($injector);
        expect($http.mock.calls.length).toBe(2);
        assertCall(0, events.slice(0, 1));
        assertCall(1, events.slice(1, 2));
    });

    it('should set the buffered_time', async () => {
        const bufferedInEventLoggerSeconds = 1;
        const bufferedInFrontRoyalStoreTime = 5;
        const totalBufferedTime = bufferedInEventLoggerSeconds + bufferedInFrontRoyalStoreTime;
        const events = [
            {
                id: 'id-1',
                client_utc_timestamp: 1,
                page_load_id: 'page-load-1',
                buffered_time: bufferedInEventLoggerSeconds,
                saved_to_front_royal_store_at: new Date().getTime() / 1000 - bufferedInFrontRoyalStoreTime, // saved to front royal 5 seconds ago
            },
        ];
        await db.events.bulkPut(events);
        await flushStoredEvents($injector);
        expect($http.mock.calls.length).toBe(1);
        const savedEvents = assertCall(0, events.slice(0, 1));
        expect(Math.abs(savedEvents[0].buffered_time - totalBufferedTime) < 0.1).toBe(true);
    });

    function assertCall(i, expectedEvents) {
        const arg = $http.mock.calls[i][0];
        expect(arg.headers).toEqual({
            Accept: 'application/json',
            'Content-type': undefined,
        });
        expect(arg.method).toEqual('POST');
        expect(arg.url).toEqual('http://endpoint/root/api/event_bundles.json');

        const savedEvents = JSON.parse(arg.data.get('record')).events;
        const mapById = map('id');
        expect(mapById(savedEvents)).toEqual(mapById(expectedEvents));
        return savedEvents;
    }
});
