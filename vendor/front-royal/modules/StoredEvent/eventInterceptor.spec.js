import openTestDb from 'FrontRoyalStore/openTestDb';
import eventInterceptor from './eventInterceptor';

describe('eventInterceptor', () => {
    const db = openTestDb();
    const flush = jest.fn();
    const $injector = {
        get: key => ({ frontRoyalStore: { retryAfterHandledError: fn => fn(db), flush } }[key]),
    };
    const event = {
        id: 'id',
        client_utc_timestamp: 42,
    };
    const eventBundle = {
        events: [event],
    };

    beforeEach(() => {
        flush.mockReset();
    });

    it('should save events included in the meta of a request', async () => {
        const data = {
            meta: {
                event_bundle: eventBundle,
            },
        };
        eventInterceptor(
            {
                method: 'post',
                url: 'http://path/to/api/lesson_progress.json',
                data,
            },
            $injector,
        );

        // the events should be removed from the metadata
        expect(data.meta.event_bundle).toBeUndefined();
        return waitForEvent(db, event);
    });

    it('should handle event save requests', async () => {
        const handleRequest = eventInterceptor(
            {
                method: 'post',
                url: 'http://path/to/api/event_bundles.json',
                data: {
                    record: eventBundle,
                },
            },
            $injector,
        );

        handleRequest();
        await waitForEvent(db, event);
        expect(flush).toHaveBeenCalled();
    });

    it('should not handle a save request when we are flushing', async () => {
        const data = {
            record: eventBundle,
            get: key =>
                ({
                    flushingFrontRoyalStore: 'true',
                }[key]),
        };

        const returnValue = eventInterceptor(
            {
                method: 'post',
                url: 'http://path/to/api/event_bundles.json',
                data,
            },
            $injector,
        );

        expect(returnValue).toBeNull();
    });
});

async function waitForEvent(db, expectedEvent) {
    let evt;
    let i = 0;
    while (!evt) {
        i += 1;
        if (i > 5) {
            throw new Error('Expected to find an event within 5 tries');
        }
        // eslint-disable-next-line no-await-in-loop
        evt = await db.events
            .where({
                id: expectedEvent.id,
            })
            .first();
    }
    expect(evt.client_utc_timestamp).toEqual(expectedEvent.client_utc_timestamp);
}
