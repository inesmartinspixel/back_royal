import 'AngularSpecHelper';
import 'FormatsText/angularModule';

describe('LineBreakPrevention', () => {
    let LineBreakPrevention;

    beforeEach(() => {
        angular.mock.module('FormatsText');

        angular.mock.inject(_LineBreakPrevention_ => {
            LineBreakPrevention = _LineBreakPrevention_;
        });
    });

    it('should trigger when a special character follows a blank', () => {
        expect(
            LineBreakPrevention.preventLineBreaks(
                '<p>test <cf-challenge-blank id="blank_0" inline view-model="viewModel.challengesComponentViewModel.challengesViewModels[0]" ng-style="{zIndex: 100-0}">test</cf-challenge-blank>%</p>',
            ),
        ).toBe(
            '<p>test <span style="white-space: nowrap"><cf-challenge-blank id="blank_0" inline view-model="viewModel.challengesComponentViewModel.challengesViewModels[0]" ng-style="{zIndex: 100-0}">test</cf-challenge-blank>%</span></p>',
        );
    });

    it('should not trigger when mathjax follows a blank', () => {
        expect(
            LineBreakPrevention.preventLineBreaks(
                '<p>test <cf-challenge-blank id="blank_0" inline view-model="viewModel.challengesComponentViewModel.challengesViewModels[0]" ng-style="{zIndex: 100-0}">test</cf-challenge-blank>%%</p>',
            ),
        ).toBe(
            '<p>test <cf-challenge-blank id="blank_0" inline view-model="viewModel.challengesComponentViewModel.challengesViewModels[0]" ng-style="{zIndex: 100-0}">test</cf-challenge-blank>%%</p>',
        );
    });
});
