import 'AngularSpecHelper';
import 'FormatsText/angularModule';
import { markdown } from 'Markdown';

describe('FormatsText', () => {
    let FormatsText;
    let $window;
    let $sanitizeSpy;
    let $sanitize;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('FormatsText');

        angular.mock.module($provide => {
            $sanitizeSpy = jest.fn();
            $sanitize = html => {
                $sanitizeSpy(html);
                return html;
            };

            $provide.value('$sanitize', $sanitize);
        });

        angular.mock.inject((_FormatsText_, _$window_) => {
            FormatsText = _FormatsText_;
            $window = _$window_;
        });
    });

    describe('stripMarkdown', () => {
        it('should strip out everything except text', () => {
            expect(FormatsText.stripFormatting('some text')).toBe('some text');
            expect(FormatsText.stripFormatting('{pink:key term}')).toBe('key term');
            expect(FormatsText.stripFormatting('%%math%%')).toBe('math');
            expect(FormatsText.stripFormatting('**stuff**')).toBe('stuff');
            expect(FormatsText.stripFormatting('[[modals]]')).toBe('modals');
        });
    });

    describe('withMarkdown', () => {
        let scope;
        let formattedText;

        beforeEach(() => {
            formattedText = '<p>formatted</p>';
            jest.spyOn(markdown, 'toHTML').mockImplementation(() => formattedText);
            scope = {};
            FormatsText.withMarkdown(scope);
        });

        it('should format text', () => {
            expect(scope.formatText('some text')).toBe(formattedText);
            expect($window.markdown.toHTML).toHaveBeenCalled();
        });

        it('should sanitize by default', () => {
            scope.formatText('some text');
            expect($sanitizeSpy).toHaveBeenCalled();
        });

        it('should explicitly un-sanitize newlines', () => {
            formattedText = '<p>some&#10;text</p>';
            scope = {};
            FormatsText.withMarkdown(scope);
            expect(scope.formatText('some text')).toBe('<p>some\ntext</p>');
        });

        it('should rewrite links', () => {
            formattedText = '<a href="https://quantic.edu">Quantic</a>';
            expect(scope.formatText('some text')).toBe(
                '<a class="external-link" ng-click="openExternalLink(\'https://quantic.edu\')">Quantic</a>',
            );
            expect($window.markdown.toHTML.mock.calls.length).toBe(1);
        });

        it('should cache formatted strings', () => {
            // once it's called a single time, there should be no need
            // to process with markdown again
            expect(scope.formatText('some text')).toBe('<p>formatted</p>');
            expect(scope.formatText('some text')).toBe('<p>formatted</p>');
            expect($window.markdown.toHTML.mock.calls.length).toBe(1);
        });
    });
});
