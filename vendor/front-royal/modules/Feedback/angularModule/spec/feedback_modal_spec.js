import 'AngularSpecHelper';
import 'Feedback/angularModule';
import feedbackModalLocales from 'Feedback/locales/feedback/feedback_modal-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(feedbackModalLocales);

describe('FeedbackModal', () => {
    let FeedbackModal;
    let DialogModal;

    angular.mock.module.sharedInjector();

    beforeAll(() => {
        angular.mock.module('FrontRoyal.Feedback', 'SpecHelper');

        angular.mock.inject($injector => {
            FeedbackModal = $injector.get('FeedbackModal');
            DialogModal = $injector.get('DialogModal');
        });
    });

    it('should create a new dialog window', () => {
        jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
        FeedbackModal.launchFeedback();
        expect(DialogModal.alert).toHaveBeenCalled();
    });
});
