import angularModule from 'Feedback/angularModule/scripts/feedback_module';
import template from 'Feedback/angularModule/views/feedback_sidebar.html';
import cacheAngularTemplate from 'cacheAngularTemplate';

const templateUrl = cacheAngularTemplate(angularModule, template);

angularModule.directive('feedbackSidebar', [
    '$injector',

    function factory($injector) {
        const FeedbackModal = $injector.get('FeedbackModal');
        const $rootScope = $injector.get('$rootScope');
        const offlineModeManager = $injector.get('offlineModeManager');

        return {
            scope: {},
            restrict: 'E',
            templateUrl,
            controllerAs: 'controller',

            link(scope) {
                scope.launchFeedback = () => {
                    FeedbackModal.launchFeedback();
                };

                $rootScope.$watch('currentUser', currentUser => {
                    scope.hideSidebar =
                        !currentUser ||
                        currentUser.isExpelled ||
                        currentUser.isRejected ||
                        currentUser.isMiyaMiya ||
                        offlineModeManager.inOfflineMode;
                });
            },
        };
    },
]);
