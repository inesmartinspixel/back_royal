import './scripts/feedback_module';

import './scripts/feedback_modal';
import './scripts/feedback_sidebar_dir';
import './scripts/feedback_form_dir';
import 'Feedback/locales/feedback/feedback_form-en.json';
import 'Feedback/locales/feedback/feedback_modal-en.json';
import 'Feedback/locales/feedback/feedback_sidebar-en.json';
