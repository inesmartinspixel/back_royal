import setSpecLocales from './setSpecLocales';

export default function stubSpecLocale(path, locale = 'en') {
    const fullObj = {};
    fullObj[locale] = {};
    let obj = fullObj[locale];
    const parts = path.split('.');
    const key = parts.pop();
    parts.forEach(part => {
        obj[part] = {};
        obj = obj[part];
    });
    obj[key] = 'Mocked out translation';
    setSpecLocales(fullObj);
}
