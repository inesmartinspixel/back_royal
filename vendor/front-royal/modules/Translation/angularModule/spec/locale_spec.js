import 'AngularSpecHelper';
import 'Translation/angularModule';

describe('Locale', () => {
    let Locale;
    let translationErrorLoggerUtility;
    let $translate;
    let $q;
    let $route;
    let $timeout;

    beforeEach(() => {
        angular.mock.module('Translation', 'SpecHelper');

        angular.mock.inject([
            '$injector',
            $injector => {
                Locale = $injector.get('Locale');
                translationErrorLoggerUtility = $injector.get('translationErrorLoggerUtility');
                $translate = $injector.get('$translate');
                $q = $injector.get('$q');
                $route = $injector.get('$route');
                $timeout = $injector.get('$timeout');
            },
        ]);
    });

    describe('preferredCode', () => {
        it('should call attemptToSetPreferredLanguage when setting', () => {
            jest.spyOn(Locale, 'attemptToSetPreferredLanguage');
            Locale.preferredCode = 'foobar';
            expect(Locale.attemptToSetPreferredLanguage).toHaveBeenCalled();
        });
    });

    describe('attemptToSetPreferredLanguage', () => {
        beforeEach(() => {
            jest.spyOn(Locale, 'attemptToSetPreferredLanguage');
            jest.spyOn($translate, 'use').mockImplementation(val => (val === false ? 'foobar' : $q.resolve()));
            jest.spyOn($route, 'reload');
        });

        it('should not call $translate.use with a locale code we are already using', () => {
            Locale.preferredCode = 'foobar';
            expect($translate.use).not.toHaveBeenCalledWith('foobar');
        });

        it('should call $translate.use with a locale code we are not using', () => {
            Locale.preferredCode = 'es';
            expect(Locale.attemptToSetPreferredLanguage).toHaveBeenCalled();
            expect($translate.use).toHaveBeenCalledWith('es');
        });

        it('should reload route by default', () => {
            Locale.preferredCode = 'es';
            $timeout.flush();
            expect($route.reload).toHaveBeenCalled();
        });

        it('should catch errors in the promise chain', () => {
            jest.spyOn($translate, 'use').mockImplementation(val =>
                val === false
                    ? 'foobar'
                    : $q.reject({
                          message: 'foobar',
                      }),
            );
            jest.spyOn(translationErrorLoggerUtility, 'logTranslationError');
            Locale.preferredCode = 'es';
            $timeout.flush();
            expect(translationErrorLoggerUtility.logTranslationError).toHaveBeenCalledWith('Error setting es locale', {
                response: 'foobar',
            });
        });
    });
});
