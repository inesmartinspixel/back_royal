import angularModule from 'Translation/angularModule/scripts/translation_module';
import moment from 'moment-timezone';
import { DefaultLocale } from 'Translation';

/* eslint-disable no-use-before-define */
angularModule.factory('Locale', [
    '$injector',
    $injector => {
        const SuperModel = $injector.get('SuperModel');
        const $window = $injector.get('$window');
        const $q = $injector.get('$q');
        const $route = $injector.get('$route');
        const SupportedLocales = $injector.get('Translation.SupportedLocales');
        const UseServerDeterminedLocale = $injector.get('Translation.UseServerDeterminedLocale');
        const $translate = $injector.get('$translate');
        const $rootScope = $injector.get('$rootScope');
        const $location = $injector.get('$location');

        $rootScope.$watch('currentUser.pref_locale', prefLocale => {
            if (prefLocale) {
                Locale.preferredCode = prefLocale;
            } else {
                // reset the private variable so that next time it will read again
                // from serverDeterminedLocale (see getter)
                Locale._preferredCode = null;
            }
        });

        $rootScope.$on('$translateChangeEnd', (event, lang) => {
            if (!lang || !SupportedLocales.includes(lang)) {
                return;
            }
            Locale.refreshLangAndDir(lang.language);
        });

        const Locale = SuperModel.subclass(function () {
            this.extend({
                // Note: if you support Chinese for regular users,
                // see user.rb ensure_valid_pref_locale and validate_pref_locale
                availablePreferenceLocales() {
                    return [
                        Locale.english,
                        Locale.spanish,
                        Locale.italian,
                        Locale.chinese,
                        Locale.arabic,
                        Locale.swahili,
                    ];
                },

                getPreferredOrDefaultLocale() {
                    // see comment near SupportedLocales in translation_module.js for why we have
                    // to be careful not to set this to an unsupported value.
                    return SupportedLocales.includes(this.preferredCode) ? this.preferredCode : DefaultLocale;
                },

                attemptToSetLanguage(locale, reloadRoute, rethrowOnError = false) {
                    locale = SupportedLocales.includes(locale) ? locale : this.getPreferredOrDefaultLocale();
                    this.activeCode = locale;
                    if ($translate.use(false) !== locale) {
                        return $translate
                            .use(locale)
                            .then(() => {
                                if (locale === 'ar') {
                                    const RouteAssetLoader = $injector.get('Navigation.RouteAssetLoader');
                                    return RouteAssetLoader.loadArabicFontDependencies();
                                }
                                return $q.when();
                            })
                            .then(() => {
                                // This will only refresh directives within the ui-view.
                                if (reloadRoute) {
                                    $route.reload();
                                }
                                return $q.when();
                            })
                            .then(() => {
                                return $q.when().then(() => {
                                    this.refreshLangAndDir(locale);
                                });
                            })
                            .catch(response => {
                                $injector
                                    .get('translationErrorLoggerUtility')
                                    .logTranslationError(`Error setting ${locale} locale`, {
                                        response: response && response.message,
                                    });
                                if (rethrowOnError) {
                                    throw response;
                                }
                            });
                    }

                    // even if we're currently set to that language, refresh the language and dir in the HTML
                    // (When using $translate.use, our listener on `translateChangeEnd`
                    this.refreshLangAndDir(locale);
                    return $q.when();
                },

                attemptToSetPreferredLanguage(reloadRoute = true) {
                    return this.attemptToSetLanguage(this.getPreferredOrDefaultLocale(), reloadRoute);
                },

                // Note: see translation_module.js for where we call this method whenever a locale *changes*
                // This covers a case where the locale could have been changed outside of the setLanguage method above
                refreshLangAndDir(locale) {
                    locale = locale.toLowerCase();
                    const localeObject = Locale[locale];

                    const overridesLookup = {
                        ar: {
                            relativeTime: {
                                m: '%d دقيقة',
                                h: '%d ساعة',
                                M: '%d شهر',
                                y: '%d سنة',
                            },
                        },
                        en: {
                            relativeTime: {
                                m: '%d minute',
                                h: '%d hour',
                                M: '%d month',
                                y: '%d year',
                            },
                        },
                        es: {
                            relativeTime: {
                                m: '%d minuto',
                                h: '%d hora',
                                M: '%d mes',
                                y: '%d año',
                            },
                        },
                        it: {
                            relativeTime: {
                                m: '%d minuto',
                                h: '%d ora',
                                M: '%d mese',
                                y: '%d anno',
                            },
                        },
                        sw: {
                            relativeTime: {
                                m: 'dakika %d',
                                h: 'saa %d',
                                M: 'miezi %d',
                                y: 'mwaka %d',
                            },
                        },
                        zh: {
                            relativeTime: {
                                m: '%d 分钟',
                                h: '%d 小时',
                                M: '%d 个月',
                                y: '%d 年',
                            },
                        },
                    };

                    // moment is giving us deprecation warnings here and telling us to use updateLocale,
                    // but if we do then the datetimepicker used in report-date-range blows up. Try changing
                    // this to updateLocale and the run the spec for report-date-range and you'll see.
                    moment.locale(locale, overridesLookup[locale]);
                    $('html')
                        .attr('lang', locale)
                        .attr('dir', localeObject && localeObject.rtl ? 'rtl' : 'ltr');
                },

                defaultLocaleForSignUpCode(signUpCode) {
                    // For all FREEMBA signups, we want to force the 'en' locale, the
                    // only exception being when the 'force_locale' query string param
                    // is present.
                    return signUpCode === 'FREEMBA' && !$location.search().force_locale
                        ? DefaultLocale
                        : this.preferredCode;
                },
            });

            Object.defineProperty(this, 'preferredCode', {
                get() {
                    if (!this._preferredCode) {
                        this._preferredCode = UseServerDeterminedLocale
                            ? $window.serverDeterminedLocale
                            : DefaultLocale;
                    }
                    return this._preferredCode;
                },
                set(languageCode) {
                    this._preferredCode = languageCode;
                    this.attemptToSetPreferredLanguage();
                },
                configurable: true,
            });

            return {
                initialize(code, title, opts) {
                    this.code = code;
                    this.value = code; // easy usage in selectize components
                    this.title = title;

                    opts = opts || {};
                    _.extend(this, opts);
                    Locale[code] = this;
                },

                spacesNecessaryForInput(text) {
                    let count = 0;
                    const self = this;
                    if (self.supportsIME) {
                        _.each(text, char => {
                            count += self.imeCharMultiplier(char);
                        });
                    } else {
                        count = text.length;
                    }

                    return count;
                },
            };
        });

        // set the initial activeCode
        Locale.attemptToSetPreferredLanguage();

        // Define available languages
        Locale.english = new Locale('en', 'English');
        Locale.spanish = new Locale('es', 'Español');
        Locale.italian = new Locale('it', 'Italiano');
        Locale.chinese = new Locale('zh', '中文', {
            supportsIME: true,
            imeCharMultiplier(char) {
                return char.charCodeAt(0) > 255 ? 4 : 1;
            },
        });
        Locale.arabic = new Locale('ar', 'العربية', {
            rtl: true,
        });
        Locale.swahili = new Locale('sw', 'Kiswahili');

        // Learner language options
        Locale.availablePrefLocales = [
            Locale.english,
            Locale.spanish,
            Locale.italian,
            Locale.chinese,
            Locale.arabic,
            Locale.swahili,
        ];

        // Editor localization options
        Locale.editorLocales = [
            Locale.english,
            Locale.spanish,
            Locale.italian,
            Locale.chinese,
            Locale.arabic,
            Locale.swahili,
        ];

        Locale.editorLocalesWithoutEnglish = _.without(Locale.editorLocales, Locale.english);
        Locale.editorLocaleCodeWithoutEnglish = _.pluck(Locale.editorLocalesWithoutEnglish, 'code');
        Locale.editorLocale = DefaultLocale;

        return Locale;
    },
]);
