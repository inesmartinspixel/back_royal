import angularModule from 'Translation/angularModule/scripts/translation_module';
import { DefaultLocale } from 'Translation';

angularModule.factory('translationLoader', [
    '$injector',

    $injector => {
        const $q = $injector.get('$q');
        const $http = $injector.get('$http');
        const HttpQueue = $injector.get('HttpQueue');

        const asyncLoadedLanguagesPromises = {};

        return options => {
            if (options.key === DefaultLocale) {
                // If options.key corresponds to the DefaultLocale, i.e. the locale
                // that was bundled in the JS via localeLoader, we can assume the data
                // is on the window.
                return $q.resolve(window.Smartly.locales.modules[options.key]);
            }

            if (!asyncLoadedLanguagesPromises[options.key]) {
                const url = window.webpackManifest[`/assets/locales/modules/locales-${options.key}.json`];
                const requestConfig = {
                    method: 'GET',
                    httpQueueOptions: {
                        shouldQueue: true,
                    },
                    url,
                };

                asyncLoadedLanguagesPromises[options.key] = $http(requestConfig)
                    .then(response => {
                        return response.data;
                    })
                    .catch(response => {
                        HttpQueue.unfreezeAfterError(response.config);

                        $injector
                            .get('translationErrorLoggerUtility')
                            .logTranslationError(`Error fetching translation tables`, {
                                response: response && response.message,
                                attemptingToLoadLocale: options.key,
                            });

                        throw response; // re-throw to support Locale handling
                    });
            }

            return asyncLoadedLanguagesPromises[options.key];
        };
    },
]);
