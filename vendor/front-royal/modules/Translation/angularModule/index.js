import './scripts/translation_module';

import './scripts/locale';
import './scripts/missing_translation_handler';
import './scripts/translation_error_logger_utility';
import './scripts/translation_helper';
import './scripts/translation_loader';
