// DefaultLocale is not defined in our code base. The critical locale is determined
// at compile time, and any references to DefaultLocale are dynamically replaced.
const DefaultLocale = window.bundledLocale;

// eslint-disable-next-line import/prefer-default-export
export { DefaultLocale };
