import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'ContentItem/angularModule/spec/_mock/mock_s3_asset';

describe('Admin::AdminEditOrganizationOption', () => {
    let availableOrganizations;
    let elem;
    let file;
    let guid;
    let $httpBackend;
    let MockS3Asset;
    let $q;
    let renderer;
    let scope;
    let SpecHelper;
    let Upload;
    let $window;
    let ngToast;
    let $rootScope;
    let DialogModal;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            SpecHelper.stubCurrentUser('admin');
            guid = $injector.get('guid');
            $httpBackend = $injector.get('$httpBackend');
            MockS3Asset = $injector.get('MockS3Asset');
            $q = $injector.get('$q');
            Upload = $injector.get('Upload');
            $window = $injector.get('$window');
            ngToast = $injector.get('ngToast');
            DialogModal = $injector.get('DialogModal');
            $rootScope = $injector.get('$rootScope');
        });

        availableOrganizations = [
            {
                id: guid.generate(),
                created_at: new Date().getTime(),
                updated_at: new Date().getTime(),
                type: 'professional_organization',
                text: 'Foobar',
                locale: 'en',
                suggest: true,
                source_id: guid.generate(),
                image_url: 'https://www.crunchbase.com/organization/foobar/primary-image/raw',
            },
        ];

        file = {
            name: 'organization_icon.jpg',
        };
    });

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
        SpecHelper.cleanup();
    });

    function render(options = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.organization = options.organization || availableOrganizations[0];
        renderer.scope.availableOrganizations = availableOrganizations;
        renderer.scope.createMode = options.createMode;
        renderer.render(
            '<admin-edit-organization-option organization="organization" available-organizations="availableOrganizations" create-mode="createMode"></admin-edit-organization-option>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should support updating a particular organization', () => {
        render();

        const organization = _.first(availableOrganizations);
        const newText = 'Foobar, Inc.';

        SpecHelper.updateTextInput(elem, '[name="organization_name"]', newText);
        expect(scope.organization.text).toEqual(organization.text);
        expect(scope.organizationProxy.text).toEqual(newText);

        const updatedOrganization = angular.copy(organization);
        updatedOrganization.text = newText;

        SpecHelper.disableHttpQueue();
        $httpBackend
            .expectPUT(`${$window.ENDPOINT_ROOT}/api/auto_suggest_options.json`, {
                record: updatedOrganization,
            })
            .respond(200, {
                contents: {
                    professional_organization_options: [updatedOrganization],
                },
            });

        SpecHelper.click(elem, '[name="update"]');
        $httpBackend.flush();
        expect(scope.organization.text).toEqual(newText);
    });

    it('should support creating an organization', () => {
        render({
            organization: {
                type: 'professional_organization',
                locale: 'en',
            },
            createMode: true,
        });

        const newText = 'new org';
        const newOrganization = {
            type: 'professional_organization',
            locale: 'en',
            text: newText,
            suggest: false,
        };

        jest.spyOn(ngToast, 'create').mockImplementation(() => {});
        jest.spyOn(DialogModal, 'hideAlerts').mockImplementation(() => {});
        jest.spyOn($rootScope, '$broadcast').mockReturnValue({
            preventDefault: true,
        });

        SpecHelper.updateTextInput(elem, '[name="organization_name"]', newText);
        SpecHelper.toggleRadio(elem, '[name="suggest"]', 1);

        SpecHelper.disableHttpQueue();
        $httpBackend
            .expectPOST(`${$window.ENDPOINT_ROOT}/api/auto_suggest_options.json`, {
                record: newOrganization,
            })
            .respond(200, {
                contents: {
                    professional_organization_options: [newOrganization],
                },
            });

        SpecHelper.click(elem, '[name="create"]');
        $httpBackend.flush();
        expect(ngToast.create).toHaveBeenCalledWith({
            content: 'new org successfully created',
            className: 'success',
        });
        expect(DialogModal.hideAlerts).toHaveBeenCalled();
        expect($rootScope.$broadcast).toHaveBeenCalledWith('admin-organization-options:search');
    });

    it('should support uploading an icon', () => {
        render();
        const deferred = $q.defer();

        jest.spyOn(Upload, 'upload').mockReturnValue(deferred.promise);

        // Mock call to onFileSelect after a file is selected
        scope.onFileSelect(file, []);
        const id = guid.generate();
        const record = availableOrganizations[0];

        expect(scope.uploading).toBe(true);
        expect(Upload.upload).toHaveBeenCalledWith({
            url: `${window.ENDPOINT_ROOT}/api/auto_suggest_options/upload_icon.json`,
            data: {
                organization_icon: file,
                record,
            },
        });

        // mock completion of an image upload
        const data = {
            data: {
                contents: {
                    organization_icon: MockS3Asset.get({
                        id,
                        file_file_name: 'organization_icon.jpg',
                    }),
                },
            },
        };
        deferred.resolve(data);

        scope.$digest();

        expect(scope.organizationProxy.source_id).toEqual(id);
        expect(scope.organizationProxy.image_url).toEqual('original_url.jpg');
        expect(scope.uploading).toBe(false);
    });

    it('should remove the upload button and disable the update button on image upload', () => {
        render();
        SpecHelper.expectElement(elem, 'input[name="organizationImageUpload"]');
        SpecHelper.expectElementEnabled(elem, 'button[name="update"]');
        const deferred = $q.defer();

        jest.spyOn(Upload, 'upload').mockReturnValue(deferred.promise);

        // Mock call to onFileSelect after a file is selected
        scope.onFileSelect(file, []);
        scope.$digest();
        SpecHelper.expectNoElement(elem, 'input[name="organizationImageUpload"]');
        SpecHelper.expectElementDisabled(elem, 'button[name="update"]');
        SpecHelper.expectElement(elem, 'front-royal-spinner');

        const id = guid.generate();
        // mock completion of an image upload
        const data = {
            data: {
                contents: {
                    organization_icon: MockS3Asset.get({
                        id,
                        file_file_name: 'organization_icon.jpg',
                    }),
                },
            },
        };
        deferred.resolve(data);
        scope.$digest();

        SpecHelper.expectElementEnabled(elem, 'input[name="organizationImageUpload"]');
        SpecHelper.expectElementEnabled(elem, 'button[name="update"]');
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
    });
});
