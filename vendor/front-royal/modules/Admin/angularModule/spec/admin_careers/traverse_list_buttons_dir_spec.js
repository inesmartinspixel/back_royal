import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/open_position_fixtures';

describe('Admin::TraverseListButtons', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let OpenPosition;
    let openPositions;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            SpecHelper.stubCurrentUser('admin');

            OpenPosition = $injector.get('OpenPosition');

            $injector.get('OpenPositionFixtures');

            openPositions = [
                OpenPosition.fixtures.getInstance(),
                OpenPosition.fixtures.getInstance(),
                OpenPosition.fixtures.getInstance(),
            ];
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render(opts = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.currentThing = opts.currentThing || openPositions[0];
        renderer.scope.sortedThings = opts.sortedThings || openPositions;
        renderer.render(
            '<traverse-list-buttons current-thing="currentThing" sorted-things="sortedThings"></traverse-list-buttons>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should disable the previous button if on the first element of the list', () => {
        render();
        SpecHelper.expectElementDisabled(elem, '[name="previous"]');
    });

    it('should disable the next button if on the second element of the list', () => {
        render({
            currentThing: _.last(openPositions),
        });
        SpecHelper.expectElementDisabled(elem, '[name="next"]');
    });

    it('should go to the detail page for the next thing in the list', () => {
        render();
        scope.$parent.gotoThing = jest.fn();
        expect(scope.currentIndex).toEqual(0);
        SpecHelper.click(elem, '[name="next"]');
        expect(scope.currentIndex).toEqual(1);
        expect(scope.$parent.gotoThing).toHaveBeenCalledWith({
            $thing: openPositions[1],
        });
    });
});
