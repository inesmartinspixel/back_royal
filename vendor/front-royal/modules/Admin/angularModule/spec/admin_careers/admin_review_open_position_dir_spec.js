import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/candidate_position_interest_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';
import 'Careers/angularModule/spec/_mock/fixtures/open_position_fixtures';
import candidateListLocales from 'Careers/locales/careers/candidate_list-en.json';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';
import apiErrorHandlerLocales from 'FrontRoyalApiErrorHandler/locales/front_royal_api_error_handler/api_error_handler-en.json';
import setSpecLocales from 'Translation/setSpecLocales';
import MockDate from 'mockdate';

setSpecLocales(candidateListLocales, candidateListCardLocales, apiErrorHandlerLocales);

describe('Admin::AdminReviewOpenPosition', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let OpenPosition;
    let CandidatePositionInterest;
    let LogInAs;
    let ngToast;
    let $httpBackend;
    let $window;
    let $rootScope;
    let openPosition;
    let interests;
    let User;
    let currentUser;
    let DialogModal;
    let HiringTeam;
    let guid;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper', 'NoUnhandledRejectionExceptions');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            OpenPosition = $injector.get('OpenPosition');
            CandidatePositionInterest = $injector.get('CandidatePositionInterest');
            LogInAs = $injector.get('LogInAs');
            ngToast = $injector.get('ngToast');
            $httpBackend = $injector.get('$httpBackend');
            $window = $injector.get('$window');
            $rootScope = $injector.get('$rootScope');
            User = $injector.get('User');
            HiringTeam = $injector.get('HiringTeam');
            guid = $injector.get('guid');
            DialogModal = $injector.get('DialogModal');

            $injector.get('OpenPositionFixtures');
            $injector.get('CandidatePositionInterestFixtures');
            $injector.get('HiringTeamFixtures');

            openPosition = OpenPosition.fixtures.getInstance({
                hiring_manager_id: guid.generate(),
                hiring_manager_email: 'test@pedago.com',
                hiring_manager_name: 'Mr. Awesome',
            });

            interests = [
                CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: openPosition.id,
                    admin_status: 'outstanding',
                    hiring_manager_status: 'hidden',
                }),
                CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: openPosition.id,
                    admin_status: 'reviewed',
                    hiring_manager_status: 'unseen',
                }),
                CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: openPosition.id,
                    admin_status: 'unreviewed',
                    hiring_manager_status: 'hidden',
                }),
            ];

            currentUser = SpecHelper.stubCurrentUser('admin');
            SpecHelper.stubDirective('positionForm');
            SpecHelper.stubDirective('traverseListButtons');
            SpecHelper.stubConfig();
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
        MockDate.reset();
    });

    function render(options = {}) {
        CandidatePositionInterest.expect('index').returns(options.interests || interests);

        renderer = SpecHelper.renderer();
        renderer.scope.goBack = jest.fn();
        renderer.scope.openPosition = openPosition;
        renderer.render(
            '<admin-review-open-position thing="openPosition" go-back="goBack()"></admin-review-open-position>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();

        CandidatePositionInterest.flush('index');
    }

    it('should set mode to review and create toast onSavePositionFormCallback', () => {
        render();
        jest.spyOn(ngToast, 'create').mockImplementation(() => {});
        scope.onSavePositionFormCallback();
        expect(scope.proxy.mode).toBe('review');
        expect(ngToast.create).toHaveBeenCalledWith({
            content: 'Position saved',
            className: 'success',
        });
    });

    it('should support logging in as user', () => {
        render();
        jest.spyOn(scope, 'logInAsUser');
        jest.spyOn(LogInAs, 'logInAsUser').mockImplementation(() => {});

        SpecHelper.click(elem, '[name="log-in-as"]');

        expect(scope.logInAsUser).toHaveBeenCalledWith(scope.openPosition);
        expect(LogInAs.logInAsUser).toHaveBeenCalledWith({
            id: scope.openPosition.hiring_manager_id,
            email: scope.openPosition.hiring_manager_email,
        });
    });

    it('should toggle position archived status, save, and toast when toggleArchived is called', () => {
        render();
        jest.spyOn(ngToast, 'create').mockImplementation(() => {});
        OpenPosition.expect('save');
        scope.openPosition.archived = false;
        scope.toggleArchived(scope.openPosition);
        expect(scope.openPosition.archived).toBe(true);
        OpenPosition.flush('save');
        expect(ngToast.create).toHaveBeenCalledWith({
            content: 'Position archived',
            className: 'success',
        });
    });

    it('should delete position, toast, and trigger callbacks when deleteOpenPosition is called', () => {
        render();

        jest.spyOn($window, 'confirm').mockReturnValue(true);
        jest.spyOn(ngToast, 'create').mockImplementation(() => {});

        const goBackSpy = jest.fn();
        const destroyedSpy = jest.fn();
        scope.goBack = goBackSpy;
        scope.destroyed = destroyedSpy;

        OpenPosition.expect('destroy');

        scope.deleteOpenPosition(scope.openPosition);

        expect($window.confirm).toHaveBeenCalledWith('Are you sure you want to delete this position?');
        OpenPosition.flush('destroy');
        expect(ngToast.create).toHaveBeenCalledWith({
            content: `Successfully deleted ${scope.openPosition.title}`,
            className: 'success',
        });
        expect(goBackSpy).toHaveBeenCalled();
        expect(destroyedSpy).toHaveBeenCalled();
    });

    describe('moveAllToApproved', () => {
        it('should batch update candidates when moveAllToApproved is called', () => {
            interests.push(
                CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: openPosition.id,
                    admin_status: 'unreviewed',
                    hiring_manager_status: 'pending',
                }),
            );
            render({
                interests,
            });
            scope.proxy.currentTab = 'unreviewed';
            scope.$digest();

            jest.spyOn($window, 'confirm').mockReturnValue(true);
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});

            const mockDate = new Date(2013, 9, 23);
            MockDate.set(mockDate);

            // FIXME: Brent and Matt are unsure why passing a data object to $httpBackend breaks things. We figured out
            // that we hit the `responseError` code in ng-token-auth.js, which throws an error when trying to reference `config.url`;
            // but we weren't sure why and didn't want to keep spending time on it.
            // $httpBackend.expectPUT($window.ENDPOINT_ROOT + '/api/candidate_position_interests/batch_update.json', {
            //         records: [interests[1], interests[2]],
            //         meta: {
            //             last_curated_at: mockDate.getTime() / 1000
            //         }
            //     })
            //     .respond(200, {
            //         contents: {
            //             candidate_position_interests: [interests[2]]
            //         }
            //     });
            $httpBackend
                .expectPUT(`${$window.ENDPOINT_ROOT}/api/candidate_position_interests/batch_update.json`)
                .respond(200, {
                    contents: {
                        candidate_position_interests: [interests[2]],
                    },
                });

            SpecHelper.click(elem, '[name="move-all-to-approved"]');
            expect($window.confirm).toHaveBeenCalledWith('Move all candidates in unreviewed to approved?');

            SpecHelper.expectElementDisabled(elem, '[name="move-all-to-approved"]');

            $httpBackend.flush();

            expect(scope.interests[2].admin_status).toBe('reviewed'); // should be admin_status reviewed now
            expect(scope.interests[2].hiring_manager_status).toBe('unseen'); // should be hiring_manager_status unseen now
            expect(scope.interests[3].admin_status).toBe('reviewed'); // should be admin_status reviewed now
            expect(scope.interests[3].hiring_manager_status).toBe('pending'); // should still be pending
            expect(scope.interests[1].hiring_manager_priority).toBe(0); // priority recalculated
            expect(scope.interests[2].hiring_manager_priority).toBe(1); // priority recalculated
            expect(scope.openPosition.lastCuratedAt).toEqual(mockDate); // position lastCuratedAt updated
            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'Moved all unreviewed to approved',
                className: 'success',
            });

            scope.interestGroups.unreviewed = [CandidatePositionInterest.fixtures.getInstance()];
            scope.$digest();
            SpecHelper.expectElementEnabled(elem, '[name="move-all-to-approved"]');
        });

        it('should disable when no unreviewed interests', () => {
            render();
            scope.interestGroups.unreviewed = null;
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, '[name="move-all-to-approved"]');
        });

        it('should call handlePositionConflict on 409', () => {
            render({
                interests,
            });
            scope.proxy.currentTab = 'unreviewed';
            scope.$digest();

            jest.spyOn($window, 'confirm').mockReturnValue(true);
            jest.spyOn(scope, 'handlePositionConflict');

            const mockDate = new Date(2013, 9, 23);
            MockDate.set(mockDate);

            $rootScope.site = () => 'public';

            $httpBackend
                .expectPUT(`${$window.ENDPOINT_ROOT}/api/candidate_position_interests/batch_update.json`)
                .respond(409, {
                    meta: {
                        candidate_position_interest: interests[0],
                    },
                });

            SpecHelper.click(elem, '[name="move-all-to-approved"]');
            expect($window.confirm).toHaveBeenCalledWith('Move all candidates in unreviewed to approved?');

            SpecHelper.expectElementDisabled(elem, '[name="move-all-to-approved"]');

            $httpBackend.flush();

            expect(scope.handlePositionConflict).toHaveBeenCalled();

            expect(scope.interests[0].admin_status).toBe('outstanding'); // should still be outstanding
            expect(scope.interests[0].hiring_manager_status).toBe('hidden'); // should still be hidden

            scope.interestGroups.unreviewed = [CandidatePositionInterest.fixtures.getInstance()];
        });
    });

    describe('handlePositionConflict', () => {
        beforeEach(() => {
            interests.unshift(
                CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: openPosition.id,
                    admin_status: 'unreviewed',
                    hiring_manager_status: 'pending',
                    candidate_status: 'accepted',
                }),
            );
            render({
                interests,
            });
        });

        it('should show DialogModal', () => {
            jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});

            scope.handlePositionConflict({
                data: {
                    meta: {
                        candidate_position_interest: interests[0],
                    },
                },
            });

            expect(DialogModal.alert).toHaveBeenCalledWith({
                content: 'Candidate moved to unreviewed because a hiring manager or candidate has already taken action',
            });
        });

        it('should replace the interest with the updated one', () => {
            scope.handlePositionConflict({
                data: {
                    meta: {
                        candidate_position_interest: {
                            id: interests[0].id,
                            hiring_manager_status: 'hidden',
                        },
                    },
                },
            });

            scope.$digest();
            expect(scope.interests[0].hiring_manager_status).toBe('hidden');
        });

        it('should refresh interest groups', () => {
            scope.$digest();
            expect(scope.interestGroups.unreviewed.length).toEqual(2);

            scope.handlePositionConflict({
                data: {
                    meta: {
                        candidate_position_interest: {
                            id: interests[0].id,
                            hiring_manager_status: 'pending',
                            admin_status: 'outstanding',
                        },
                    },
                },
            });

            scope.$digest();
            expect(scope.interestGroups.unreviewed.length).toEqual(1);
            expect(scope.interestGroups.outstanding.length).toEqual(1);
        });

        it('should handle candidate rejection', () => {
            scope.$digest();
            expect(scope.interestGroups.unreviewed.length).toEqual(2);

            scope.handlePositionConflict({
                data: {
                    meta: {
                        candidate_position_interest: {
                            id: interests[0].id,
                            admin_status: 'unreviewed',
                            hiring_manager_status: 'pending',
                            candidate_status: 'rejected',
                        },
                    },
                },
            });

            scope.$digest();
            expect(scope.interestGroups.unreviewed.length).toEqual(1);
            expect(scope.interestGroups.rejected.length).toEqual(1);
        });
    });

    describe('curation email', () => {
        it('should trigger curation email', () => {
            const expectedCurationEmailTimestamp = new Date().getTime();
            render();
            jest.spyOn(scope, 'sendCurationEmail');
            jest.spyOn($window, 'confirm').mockReturnValue(true);
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});

            $httpBackend
                .expectGET(
                    `${$window.ENDPOINT_ROOT}/api/open_positions/send_curation_email/${openPosition.id}.json?hiring_manager_id=${openPosition.hiring_manager_id}`,
                )
                .respond(200, {
                    contents: {
                        event_type: 'open_position:send_curation_email',
                        created_at: expectedCurationEmailTimestamp / 1000,
                    },
                });

            SpecHelper.click(elem, '[name="send-curation-email"]');
            expect(scope.sendCurationEmail).toHaveBeenCalled();
            expect($window.confirm).toHaveBeenCalledWith(
                'Trigger email to Mr. Awesome detailing interest for this position?',
            );

            expect(scope.disableSendCurationEmail).toBe(true);

            $httpBackend.flush();

            expect(scope.disableSendCurationEmail).toBe(false);
            expect(scope.openPosition.curationEmailLastTriggeredAt).toEqual(new Date(expectedCurationEmailTimestamp));
            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'Triggered curation email',
                className: 'success',
            });
        });

        it('should allow for selecting a team member to send to', () => {
            openPosition.hiring_manager_hiring_team_id = 'fooId';
            const openPositionOwner = User.fixtures.getInstance({
                id: openPosition.hiring_manager_id,
                email: openPosition.hiring_manager_email,
                name: openPosition.hiring_manager_name,
                hiring_team_id: openPosition.hiring_manager_hiring_team_id,
            });
            const teamMembers = [User.fixtures.getInstance(), openPositionOwner, User.fixtures.getInstance()];
            const hiringTeam = HiringTeam.fixtures.getInstance({
                hiring_managers: teamMembers,
            });
            HiringTeam.expect('show').returns(hiringTeam);

            render();
            HiringTeam.flush('show');
            SpecHelper.updateSelect(elem, '[name="hiring-manager-for-curation-email"]', teamMembers[0].id);
            scope.$digest();
            expect(scope.proxy.hiringManagerIdForCurationEmail).toBe(teamMembers[0].id);
        });
    });

    it('should load interests', () => {
        CandidatePositionInterest.expect('index')
            .toBeCalledWith({
                filters: {
                    open_position_id: openPosition.id,
                    candidate_status_not: ['rejected'],
                },
                'fields[]': ['ADMIN_FIELDS'],
            })
            .returns(interests);

        renderer = SpecHelper.renderer();
        renderer.scope.openPosition = openPosition;
        renderer.render(
            '<admin-review-open-position thing="openPosition" go-back="goBack()"></admin-review-open-position>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();

        SpecHelper.expectElement(elem, 'front-royal-spinner');
        CandidatePositionInterest.flush('index');
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
        expect(_.pluck(scope.interests, 'id')).toEqual(_.pluck(interests, 'id'));
    });

    it('should sort interests in the correct way to maintain integrity of the priority changing', () => {
        interests = [
            CandidatePositionInterest.fixtures.getInstance({
                open_position_id: openPosition.id,
                admin_status: 'reviewed',
                hiring_manager_status: 'unseen',
                hiring_manager_priority: 1,
            }),
            CandidatePositionInterest.fixtures.getInstance({
                open_position_id: openPosition.id,
                admin_status: 'reviewed',
                hiring_manager_status: 'unseen',
                hiring_manager_priority: 0,
            }),
            CandidatePositionInterest.fixtures.getInstance({
                open_position_id: openPosition.id,
                admin_status: 'reviewed',
                hiring_manager_status: 'unseen',
                hiring_manager_priority: 2,
                created_at: 1,
            }),
            CandidatePositionInterest.fixtures.getInstance({
                open_position_id: openPosition.id,
                admin_status: 'reviewed',
                hiring_manager_status: 'unseen',
                hiring_manager_priority: 2,
                created_at: 2,
            }),
        ];

        CandidatePositionInterest.expect('index')
            .toBeCalledWith({
                filters: {
                    open_position_id: openPosition.id,
                    candidate_status_not: ['rejected'],
                },
                'fields[]': ['ADMIN_FIELDS'],
            })
            .returns(interests);

        renderer = SpecHelper.renderer();
        renderer.scope.openPosition = openPosition;
        renderer.render(
            '<admin-review-open-position thing="openPosition" go-back="goBack()"></admin-review-open-position>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();

        CandidatePositionInterest.flush('index');
        expect(_.pluck(scope.interestGroups.approved, 'id')).toEqual([
            interests[1].id,
            interests[0].id,
            interests[3].id,
            interests[2].id,
        ]);
    });

    it('should load the hiring team', () => {
        openPosition.hiring_manager_hiring_team_id = 'fooId';
        const openPositionOwner = User.fixtures.getInstance({
            id: openPosition.hiring_manager_id,
            email: openPosition.hiring_manager_email,
            name: openPosition.hiring_manager_name,
            hiring_team_id: openPosition.hiring_manager_hiring_team_id,
        });
        const teamMembers = [User.fixtures.getInstance(), openPositionOwner, User.fixtures.getInstance()];
        const hiringTeam = HiringTeam.fixtures.getInstance({
            hiring_managers: teamMembers,
        });
        HiringTeam.expect('show').returns(hiringTeam);

        render();
        expect(scope.disableSendCurationEmail).toBe(true);
        HiringTeam.flush('show');
        expect(scope.disableSendCurationEmail).toBe(false);
        expect(scope.hiringTeam).toEqual(hiringTeam);
        expect(scope.hiringManagerOptions.length).toBe(3);
    });

    it('should set up default candidate list pagination values and reset them on tab switch', () => {
        render();
        expect(scope.listLimit).toBe(10);
        expect(scope.offset).toBe(0);

        scope.listLimit = 100;
        scope.offset = 50;
        scope.proxy.currentTab = 'foo';
        scope.$digest();
        expect(scope.listLimit).toBe(10);
        expect(scope.offset).toBe(0);
    });

    describe('mode', () => {
        describe('review', () => {
            it('should allow editing position when changing to edit mode', () => {
                render();
                User.expect('show').returns(currentUser);
                SpecHelper.click(elem, '[name="edit-position"]');
                SpecHelper.expectElement(elem, 'front-royal-spinner');
                User.flush('show');
                expect(scope.proxy.mode).toBe('edit');
                expect(scope.hiringManager).toEqual(currentUser);
                SpecHelper.expectNoElement(elem, 'front-royal-spinner');
                SpecHelper.expectElement(elem, 'position-form');
                SpecHelper.expectElement(elem, '[name="back-to-review"]');
            });
        });

        describe('edit', () => {
            it('should switch back to review when clicking the back arrow', () => {
                render();
                scope.proxy.mode = 'edit';
                scope.$digest();
                SpecHelper.click(elem, '[name="back-to-review"]');
                expect(scope.proxy.mode).toBe('review');
                SpecHelper.expectElement(elem, '[name="back-to-list"]');
                SpecHelper.expectElement(elem, 'traverse-list-buttons');
                SpecHelper.expectElement(elem, '[name="log-in-as"]');
            });
        });
    });

    describe('tabs', () => {
        beforeEach(() => {
            interests = [
                CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: openPosition.id,
                    hiring_manager_status: 'hidden',
                    admin_status: 'outstanding',
                }),
                CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: openPosition.id,
                    hiring_manager_status: 'unseen',
                    admin_status: 'reviewed',
                }),
                CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: openPosition.id,
                    hiring_manager_status: 'hidden',
                    admin_status: 'unreviewed',
                }),
                CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: openPosition.id,
                    hiring_manager_status: 'saved_for_later',
                    admin_status: 'outstanding',
                }),
                CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: openPosition.id,
                    hiring_manager_status: 'invited',
                    admin_status: 'reviewed',
                }),
                CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: openPosition.id,
                    hiring_manager_status: 'accepted',
                    admin_status: 'outstanding',
                }),
                CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: openPosition.id,
                    hiring_manager_status: 'rejected',
                    admin_status: 'outstanding',
                }),
                CandidatePositionInterest.fixtures.getInstance({
                    open_position_id: openPosition.id,
                    hiring_manager_status: 'unseen',
                    admin_status: 'outstanding',
                }),
            ];

            render({
                interests,
            });
        });

        it('should switch tabs', () => {
            render();
            expect(scope.proxy.currentTab).toBe('unreviewed');
            SpecHelper.click(elem, '[name="outstanding-tab"]');
            expect(scope.proxy.currentTab).toBe('outstanding');
        });

        it('should set up groups properly', () => {
            expect(scope.interestGroups.unreviewed.length).toBe(1);
            expect(scope.interestGroups.outstanding.length).toBe(1);
            expect(scope.interestGroups.approved.length).toBe(1);
            expect(scope.interestGroups.saved.length).toBe(1);
            expect(scope.interestGroups.connected.length).toBe(2);
            expect(scope.interestGroups.passed.length).toBe(1);
            expect(scope.interestGroups.hidden.length).toBe(1);
        });

        it('should initialize tabs', () => {
            SpecHelper.expectElements(elem, '.nav-tabs > li', 7);
        });

        it('should show count in tabs', () => {
            SpecHelper.expectElementText(elem, '.nav-tabs > li:eq(0) > .text-capitalize', 'unreviewed (1)');
            SpecHelper.expectElementText(elem, '.nav-tabs > li:eq(1) > .text-capitalize', 'outstanding (1)');
            SpecHelper.expectElementText(elem, '.nav-tabs > li:eq(2) > .text-capitalize', 'approved (1)');
            SpecHelper.expectElementText(elem, '.nav-tabs > li:eq(3) > .text-capitalize', 'saved (1)');
            SpecHelper.expectElementText(elem, '.nav-tabs > li:eq(4) > .text-capitalize', 'connected (2)');
            SpecHelper.expectElementText(elem, '.nav-tabs > li:eq(5) > .text-capitalize', 'passed (1)');
            SpecHelper.expectElementText(elem, '.nav-tabs > li:eq(6) > .text-capitalize', 'hidden (1)');
        });
    });

    describe('admin:interestAdminAction', () => {
        describe('moveInterest', () => {
            it('should reset hiring_manager_priority, update openPosition.last_curated_at, update the groups, and save the interest', () => {
                interests = [
                    CandidatePositionInterest.fixtures.getInstance({
                        open_position_id: openPosition.id,
                        admin_status: 'unreviewed',
                    }),
                    CandidatePositionInterest.fixtures.getInstance({
                        open_position_id: openPosition.id,
                        hiring_manager_status: 'hidden',
                        admin_status: 'reviewed',
                    }),
                ];
                render({
                    interests,
                });

                const mockDate = new Date(2013, 9, 23);
                MockDate.set(mockDate);

                CandidatePositionInterest.expect('save').toBeCalledWith(
                    _.extend(interests[0], {
                        hiring_manager_priority: null,
                    }),
                    {
                        last_curated_at: mockDate.getTime() / 1000,
                    },
                );

                scope.moveInterest(interests[0], 'unreviewed', 'hidden');
                CandidatePositionInterest.flush('save');

                expect(scope.interestGroups.unreviewed.length).toBe(0);
                expect(scope.interestGroups.hidden.length).toBe(2);
                expect(scope.openPosition.lastCuratedAt).toEqual(mockDate);
            });

            it('should detect the interest being hidden due to hiring relationship conflict, repair, and let the admin user know', () => {
                interests = [
                    CandidatePositionInterest.fixtures.getInstance({
                        open_position_id: openPosition.id,
                        hiring_manager_status: 'hidden',
                        admin_status: 'unreviewed',
                    }),
                ];
                render({
                    interests,
                });

                const returnedInterest = _.clone(scope.interests[0]);
                returnedInterest.hiring_manager_status = 'hidden';
                CandidatePositionInterest.expect('save').returns(returnedInterest);

                // Simulate move to outstanding bucket
                scope.interests[0].hiring_manager_status = 'unseen';
                scope.interests[0].admin_status = 'outstanding';
                scope.moveInterest(scope.interests[0], 'unreviewed', 'outstanding');

                jest.spyOn(ngToast, 'create').mockImplementation(() => {});
                CandidatePositionInterest.flush('save');

                expect(scope.interestGroups.unreviewed.length).toBe(0);
                expect(scope.interestGroups.outstanding.length).toBe(0);
                expect(scope.interestGroups.hidden.length).toBe(1);
                expect(ngToast.create).toHaveBeenCalledWith({
                    content: 'Interest hidden by server due to hiring relationship conflict',
                    className: 'warning',
                });
            });

            // see also: NoUnhandledRejectionExceptions module use above
            it('should call handlePositionConflict on 409', () => {
                interests = [
                    CandidatePositionInterest.fixtures.getInstance({
                        id: 'foo-id',
                        open_position_id: openPosition.id,
                        hiring_manager_status: 'pending',
                        admin_status: 'outstanding',
                    }),
                    CandidatePositionInterest.fixtures.getInstance({
                        open_position_id: openPosition.id,
                        hiring_manager_status: 'pending',
                        admin_status: 'reviewed',
                    }),
                    CandidatePositionInterest.fixtures.getInstance({
                        open_position_id: openPosition.id,
                        hiring_manager_status: 'pending',
                        admin_status: 'reviewed',
                    }),
                    CandidatePositionInterest.fixtures.getInstance({
                        open_position_id: openPosition.id,
                        hiring_manager_status: 'hidden',
                        admin_status: 'reviewed',
                    }),
                ];
                render({
                    interests,
                });

                expect(scope.interestGroups.approved.length).toBe(2);
                expect(scope.interestGroups.outstanding.length).toBe(1);
                expect(scope.interestGroups.hidden.length).toBe(1);

                CandidatePositionInterest.expect('save').fails({
                    status: 409,
                    data: {
                        meta: {
                            candidate_position_interest: CandidatePositionInterest.fixtures.getInstance({
                                id: 'foo-id',
                                open_position_id: openPosition.id,
                                hiring_manager_status: 'saved_for_later',
                                admin_status: 'outstanding',
                            }),
                        },
                    },
                });
                jest.spyOn(scope, 'handlePositionConflict');

                // We are emulating the admin trying to move the first interest, which is in outstanding, to
                // the approved bucket, but after a hiring manager has reviewed it. Thus a conflict should occur
                // that we gracefully handle.
                scope.moveInterest(interests[0], 'outstanding', 'hidden');

                CandidatePositionInterest.flush('save');

                expect(scope.handlePositionConflict).toHaveBeenCalled();

                expect(scope.interestGroups.approved.length).toBe(2);
                expect(scope.interestGroups.hidden.length).toBe(1);

                // So after the conflict has handle we should show that outstanding interest that was
                // in conflict in the saved_for_later bucket.
                expect(scope.interestGroups.saved.length).toBe(1);
            });

            it('should call handlePositionConflict on 409 conflict from candidate rejection', () => {
                interests = [
                    CandidatePositionInterest.fixtures.getInstance({
                        id: 'foo-id',
                        open_position_id: openPosition.id,
                        hiring_manager_status: 'hidden',
                        admin_status: 'unreviewed',
                    }),
                ];
                render({
                    interests,
                });

                expect(scope.interestGroups.unreviewed.length).toBe(1);

                CandidatePositionInterest.expect('save').fails({
                    status: 409,
                    data: {
                        meta: {
                            candidate_position_interest: CandidatePositionInterest.fixtures.getInstance({
                                id: 'foo-id',
                                open_position_id: openPosition.id,
                                candidate_status: 'rejected',
                                admin_status: 'unreviewed',
                            }),
                        },
                    },
                });
                jest.spyOn(scope, 'handlePositionConflict');

                // We are emulating the admin trying to move the first interest, which is in outstanding, to
                // the approved bucket, but after a hiring manager has reviewed it. Thus a conflict should occur
                // that we gracefully handle.
                scope.moveInterest(interests[0], 'unreviewed', 'outstanding');

                CandidatePositionInterest.flush('save');

                expect(scope.handlePositionConflict).toHaveBeenCalled();
            });

            describe('priority', () => {
                it('should recalculate priority and save for both outstanding and approved if moving from one to the other', () => {
                    interests = [
                        CandidatePositionInterest.fixtures.getInstance({
                            open_position_id: openPosition.id,
                            admin_status: 'outstanding',
                            hiring_manager_status: 'unseen',
                        }),
                        CandidatePositionInterest.fixtures.getInstance({
                            open_position_id: openPosition.id,
                            admin_status: 'outstanding',
                            hiring_manager_status: 'unseen',
                        }),
                        CandidatePositionInterest.fixtures.getInstance({
                            open_position_id: openPosition.id,
                            admin_status: 'reviewed',
                            hiring_manager_status: 'unseen',
                        }),
                        CandidatePositionInterest.fixtures.getInstance({
                            open_position_id: openPosition.id,
                            admin_status: 'reviewed',
                            hiring_manager_status: 'unseen',
                        }),
                    ];
                    render({
                        interests,
                    });

                    CandidatePositionInterest.expect('save');
                    scope.moveInterest(scope.interests[0], 'outstanding', 'approved');

                    // FIXME: See comment above. We should ideally pass the data as an expectation as well.
                    $httpBackend
                        .expectPUT(`${$window.ENDPOINT_ROOT}/api/candidate_position_interests/batch_update.json`)
                        .respond(200, {
                            contents: {
                                candidate_position_interests: interests,
                            },
                        });

                    CandidatePositionInterest.flush('save');
                    $httpBackend.flush();

                    expect(scope.interests[0].hiring_manager_priority).toBe(2);
                    expect(scope.interests[2].hiring_manager_priority).toBe(0);
                    expect(scope.interests[3].hiring_manager_priority).toBe(1);
                });

                it('should recalculate priority and save for curated if moving to or from curated', () => {
                    interests = [
                        CandidatePositionInterest.fixtures.getInstance({
                            open_position_id: openPosition.id,
                            admin_status: 'outstanding',
                            hiring_manager_status: 'unseen',
                        }),
                        CandidatePositionInterest.fixtures.getInstance({
                            open_position_id: openPosition.id,
                            admin_status: 'outstanding',
                            hiring_manager_status: 'unseen',
                        }),
                        CandidatePositionInterest.fixtures.getInstance({
                            open_position_id: openPosition.id,
                            admin_status: 'reviewed',
                            hiring_manager_status: 'hidden',
                        }),
                    ];
                    render({
                        interests,
                    });

                    CandidatePositionInterest.expect('save');
                    scope.moveInterest(scope.interests[2], 'hidden', 'outstanding');

                    // FIXME: See comment above. We should ideally pass the data as an expectation as well.
                    $httpBackend
                        .expectPUT(`${$window.ENDPOINT_ROOT}/api/candidate_position_interests/batch_update.json`)
                        .respond(200, {
                            contents: {
                                candidate_position_interests: interests,
                            },
                        });

                    CandidatePositionInterest.flush('save');
                    $httpBackend.flush();

                    expect(scope.interests[0].hiring_manager_priority).toBe(0);
                    expect(scope.interests[1].hiring_manager_priority).toBe(1);
                    expect(scope.interests[2].hiring_manager_priority).toBe(2);
                });

                it('should recalculate priority and save for approved if moving to or from approved', () => {
                    interests = [
                        CandidatePositionInterest.fixtures.getInstance({
                            open_position_id: openPosition.id,
                            admin_status: 'reviewed',
                            hiring_manager_status: 'unseen',
                        }),
                        CandidatePositionInterest.fixtures.getInstance({
                            open_position_id: openPosition.id,
                            admin_status: 'reviewed',
                            hiring_manager_status: 'unseen',
                        }),
                        CandidatePositionInterest.fixtures.getInstance({
                            open_position_id: openPosition.id,
                            admin_status: 'unreviewed',
                            hiring_manager_status: 'hidden',
                        }),
                    ];
                    render({
                        interests,
                    });

                    CandidatePositionInterest.expect('save');
                    scope.moveInterest(scope.interests[0], 'approved', 'hidden');

                    // FIXME: See comment above. We should ideally pass the data as an expectation as well.
                    $httpBackend
                        .expectPUT(`${$window.ENDPOINT_ROOT}/api/candidate_position_interests/batch_update.json`)
                        .respond(200, {
                            contents: {
                                candidate_position_interests: interests,
                            },
                        });

                    CandidatePositionInterest.flush('save');
                    $httpBackend.flush();

                    expect(scope.interests[1].hiring_manager_priority).toBe(0);
                });
            });
        });

        describe('setInterestPriority', () => {
            beforeEach(() => {
                interests = [
                    CandidatePositionInterest.fixtures.getInstance({
                        open_position_id: openPosition.id,
                        admin_status: 'outstanding',
                        hiring_manager_status: 'unseen',
                    }),
                    CandidatePositionInterest.fixtures.getInstance({
                        open_position_id: openPosition.id,
                        admin_status: 'outstanding',
                        hiring_manager_status: 'pending',
                    }),
                    CandidatePositionInterest.fixtures.getInstance({
                        open_position_id: openPosition.id,
                        admin_status: 'outstanding',
                        hiring_manager_status: 'unseen',
                    }),
                    CandidatePositionInterest.fixtures.getInstance({
                        open_position_id: openPosition.id,
                        admin_status: 'unreviewed',
                    }),
                ];
            });

            it('should set the interest to the priority if isInteger', () => {
                render();
                scope.setInterestPriority(scope.interests[2], 'outstanding', 0);
                expect(scope.interestGroups.outstanding[0]).toBe(scope.interests[2]);
                _.each(scope.interestGroups.outstanding, (interest, index) => {
                    expect(interest.hiring_manager_priority).toBe(index);
                });

                // FIXME: See comment above. We should ideally pass the data as an expectation as well.
                $httpBackend
                    .expectPUT(`${$window.ENDPOINT_ROOT}/api/candidate_position_interests/batch_update.json`)
                    .respond(200, {
                        contents: {
                            candidate_position_interests: scope.interestGroups.outstanding,
                        },
                    });
                $httpBackend.flush();
            });

            it('should move the interest up if higherPriority', () => {
                render();
                scope.setInterestPriority(scope.interests[2], 'outstanding', 'higherPriority');
                expect(scope.interestGroups.outstanding[1]).toBe(scope.interests[2]);
                _.each(scope.interestGroups.outstanding, (interest, index) => {
                    expect(interest.hiring_manager_priority).toBe(index);
                });

                // FIXME: See comment above. We should ideally pass the data as an expectation as well.
                $httpBackend
                    .expectPUT(`${$window.ENDPOINT_ROOT}/api/candidate_position_interests/batch_update.json`)
                    .respond(200, {
                        contents: {
                            candidate_position_interests: scope.interestGroups.outstanding,
                        },
                    });
                $httpBackend.flush();
            });

            it('should move the interest down if lowerPriority', () => {
                render();
                scope.setInterestPriority(scope.interests[1], 'outstanding', 'lowerPriority');
                expect(scope.interestGroups.outstanding[2]).toBe(scope.interests[1]);
                _.each(scope.interestGroups.outstanding, (interest, index) => {
                    expect(interest.hiring_manager_priority).toBe(index);
                });

                // FIXME: See comment above. We should ideally pass the data as an expectation as well.
                $httpBackend
                    .expectPUT(`${$window.ENDPOINT_ROOT}/api/candidate_position_interests/batch_update.json`)
                    .respond(200, {
                        contents: {
                            candidate_position_interests: scope.interestGroups.outstanding,
                        },
                    });
                $httpBackend.flush();
            });
        });

        it('should handle sendToOutstanding', () => {
            interests[0].admin_status = 'unreviewed';
            render();

            jest.spyOn(scope, 'moveInterest').mockImplementation(() => {});

            scope.$emit('admin:interestAdminAction', {
                action: 'sendToOutstanding',
                interest: interests[0],
                group: 'unreviewed',
                priority: undefined,
            });

            expect(interests[0].admin_status).toBe('outstanding');
            expect(interests[0].hiring_manager_status).toBe('unseen');
            expect(scope.moveInterest).toHaveBeenCalledWith(interests[0], 'unreviewed', 'outstanding');
        });

        it('should not mark an interest as hiring_manager_status unseen if they have already seen it and we sendToOutstanding', () => {
            interests[0].admin_status = 'reviewed';
            interests[0].hiring_manager_status = 'pending';
            render();

            jest.spyOn(scope, 'moveInterest').mockImplementation(() => {});

            scope.$emit('admin:interestAdminAction', {
                action: 'sendToOutstanding',
                interest: interests[0],
                group: 'approved',
                priority: undefined,
            });

            expect(interests[0].admin_status).toBe('outstanding');
            expect(interests[0].hiring_manager_status).toBe('pending');
        });

        it('should handle sendToApproved', () => {
            interests[0].candidate_status = 'unreviewed';
            render();

            jest.spyOn(scope, 'moveInterest').mockImplementation(() => {});

            scope.$emit('admin:interestAdminAction', {
                action: 'sendToApproved',
                interest: interests[0],
                group: 'unreviewed',
                priority: undefined,
            });

            expect(interests[0].admin_status).toBe('reviewed');
            expect(interests[0].hiring_manager_status).toBe('unseen');
            expect(scope.moveInterest).toHaveBeenCalledWith(interests[0], 'unreviewed', 'approved');
        });

        it('should not mark an interest as hiring_manager_status unseen if they have already seen it and we sendToApproved', () => {
            interests[0].admin_status = 'outstanding';
            interests[0].hiring_manager_status = 'pending';
            render();

            jest.spyOn(scope, 'moveInterest').mockImplementation(() => {});

            scope.$emit('admin:interestAdminAction', {
                action: 'sendToApproved',
                interest: interests[0],
                group: 'outstanding',
                priority: undefined,
            });

            expect(interests[0].admin_status).toBe('reviewed');
            expect(interests[0].hiring_manager_status).toBe('pending');
        });

        it('should handle sendToHidden', () => {
            interests[0].candidate_status = 'unreviewed';
            render();

            jest.spyOn(scope, 'moveInterest').mockImplementation(() => {});

            scope.$emit('admin:interestAdminAction', {
                action: 'sendToHidden',
                interest: interests[0],
                group: 'unreviewed',
                priority: undefined,
            });

            expect(interests[0].admin_status).toBe('reviewed');
            expect(interests[0].hiring_manager_status).toBe('hidden');
            expect(scope.moveInterest).toHaveBeenCalledWith(interests[0], 'unreviewed', 'hidden');
        });

        it('should handle sendToUnreviewed', () => {
            interests[0].admin_status = 'reviewed';
            interests[0].hiring_manager_status = 'hidden';
            render();

            jest.spyOn(scope, 'moveInterest').mockImplementation(() => {});

            scope.$emit('admin:interestAdminAction', {
                action: 'sendToUnreviewed',
                interest: interests[0],
                group: 'hidden',
                priority: undefined,
            });

            expect(interests[0].admin_status).toBe('unreviewed');
            expect(interests[0].hiring_manager_status).toBe('hidden');
            expect(scope.moveInterest).toHaveBeenCalledWith(interests[0], 'hidden', 'unreviewed');
        });

        it('should handle setInterestPriority', () => {
            interests[0].candidate_status = 'curated';
            render();

            jest.spyOn(scope, 'setInterestPriority').mockImplementation(() => {});

            scope.$emit('admin:interestAdminAction', {
                action: 'setInterestPriority',
                interest: interests[0],
                group: 'curated',
                priority: 5,
            });

            expect(scope.setInterestPriority).toHaveBeenCalledWith(interests[0], 'curated', 5);
        });
    });
});
