import 'AngularSpecHelper';
import 'Admin/angularModule';

describe('Admin::AdminOrganizationOptions', () => {
    let amDateFormat;
    let availableOrganizations;
    let DialogModal;
    let elem;
    let guid;
    let $q;
    let renderer;
    let scope;
    let SpecHelper;
    let $rootScope;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            SpecHelper.stubCurrentUser('admin');
            amDateFormat = $injector.get('$filter')('amDateFormat');
            DialogModal = $injector.get('DialogModal');
            guid = $injector.get('guid');
            $q = $injector.get('$q');
            $rootScope = $injector.get('$rootScope');
        });

        availableOrganizations = [
            {
                id: guid.generate(),
                created_at: new Date().getTime(),
                updated_at: new Date().getTime(),
                type: 'professional_organization',
                text: 'Foobar',
                locale: 'en',
                suggest: true,
                source_id: guid.generate(),
                image_url: 'https://www.crunchbase.com/organization/foobar/primary-image/raw',
            },
            {
                id: guid.generate(),
                created_at: new Date().getTime(),
                updated_at: new Date().getTime(),
                type: 'professional_organization',
                text: 'Bazquux',
                locale: 'en',
                suggest: true,
                source_id: guid.generate(),
                image_url: 'https://www.crunchbase.com/organization/bazquux/primary-image/raw',
            },
        ];
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should disable searching unless the organization type and locale are selected', () => {
        render();
        // NOTE: locale select defaults to 'EN' on render
        SpecHelper.expectElementDisabled(elem, 'button[name="search"]');
        SpecHelper.updateSelect(elem, '[name="organizationType"]', scope.organizationTypeOptions[0]);
        SpecHelper.expectElementEnabled(elem, 'button[name="search"]');
    });

    it('should disable creating unless the organization type and locale are selected', () => {
        render();
        // NOTE: locale select defaults to 'EN' on render
        SpecHelper.expectElementDisabled(elem, 'button[name="create"]');
        SpecHelper.updateSelect(elem, '[name="organizationType"]', scope.organizationTypeOptions[0]);
        SpecHelper.expectElementEnabled(elem, 'button[name="create"]');
    });

    it('should show how many organizations were found', () => {
        render();
        loadOrganizations();
        SpecHelper.expectElementEnabled(elem, '[name="search"]');
        SpecHelper.expectElementText(
            elem,
            'div.pagination-container span',
            `${scope.availableOrganizations.length} organizations found.`,
        );
    });

    it('should show a list of organizations', () => {
        render();
        loadOrganizations();
        SpecHelper.expectElements(elem, 'tr.organization', scope.availableOrganizations.length);
    });

    it('should populate each row in the table with values from organization', () => {
        availableOrganizations.splice(0, 1);
        const organization = availableOrganizations[0];

        // set the suggest and image_url so we can assert the checkmark is there
        organization.suggest = true;
        organization.image_url = true;
        render();
        loadOrganizations();
        const colIndexMap = SpecHelper.assertRowValues(elem, 'tr.organization:eq(0) td', {
            created_at: amDateFormat(organization.created_at, 'lll'),
            updated_at: amDateFormat(organization.updated_at, 'lll'),
            text: organization.text,
            locale: 'EN',
            suggest: '',
            image_url: '',
        });

        // assert that the suggest and image_url columns have a check mark in it
        const suggestCell = elem.find('tr.organization:eq(0) td').eq(colIndexMap.suggest);
        const imageUrlCell = elem.find('tr.organization:eq(0) td').eq(colIndexMap.suggest);
        SpecHelper.expectElement(suggestCell, 'i.fa-check');
        SpecHelper.expectElement(imageUrlCell, 'i.fa-check');
    });

    it('should allow for editing a particular organization', () => {
        availableOrganizations.splice(0, 1);
        render();
        loadOrganizations();
        jest.spyOn(scope, 'editOrganization');
        jest.spyOn(DialogModal, 'alert').mockImplementation(() => {});
        SpecHelper.click(elem, 'button[name="edit"]');
        expect(scope.editOrganization).toHaveBeenCalledWith(availableOrganizations[0]);
        expect(DialogModal.alert).toHaveBeenCalled();
    });

    it('should search on admin-organization-options:search event', () => {
        render();
        jest.spyOn(scope, 'search').mockImplementation(() => {});
        $rootScope.$broadcast('admin-organization-options:search');
        expect(scope.search).toHaveBeenCalled();
    });

    function loadOrganizations() {
        const deferred = $q.defer();
        const response = availableOrganizations;
        jest.spyOn(scope, 'getOptionsForType').mockReturnValue(deferred.promise);
        SpecHelper.updateSelect(elem, '[name="organizationType"]', scope.organizationTypeOptions[0]);
        SpecHelper.updateSelect(elem, '[name="locale"]', scope.localeOptions[0]);
        expect(scope.params.organization_type.identifier).toEqual('professional_organization');
        expect(scope.params.freetext_search_param).toBeUndefined();
        expect(scope.params.locale.label).toEqual('en');
        // SpecHelper.click(elem, '[name="search"]');
        SpecHelper.submitForm(elem);
        deferred.resolve(response);
        scope.$digest();
    }

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.containerViewModel = {
            header: 'last page',
        };
        renderer.render(
            '<admin-organization-options container-view-model="containerViewModel"></admin-organization-options>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }
});
