import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/open_position_fixtures';

describe('Admin::ListOpenPositions', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let OpenPosition;
    let $window;
    let ngToast;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            SpecHelper.stubCurrentUser('admin');

            OpenPosition = $injector.get('OpenPosition');
            $window = $injector.get('$window');
            ngToast = $injector.get('ngToast');

            $injector.get('OpenPositionFixtures');

            SpecHelper.stubDirective('editableThingsList');
        });
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.render('<list-open-positions></list-open-positions>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should load editable things for open positions', () => {
        render();
        expect(scope.indexParamsForOpenPositions).toEqual({
            limit: 50,
            fields: ['ADMIN_FIELDS'],
        });
        SpecHelper.expectElement(elem, '[name="open-position-editable-things"]');
    });

    describe('mode', () => {
        it('should initialize to list mode', () => {
            render();
            expect(scope.proxy.mode).toBe('list');
        });
    });

    describe('editor abilities', () => {
        it('should toggle position archived status, save, and toast when toggleArchived is called', () => {
            render();
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            const openPosition = OpenPosition.fixtures.getInstance({
                archive: false,
            });
            const callbacks = _.findWhere(scope.displayColumns, {
                prop: 'editorAbilities',
            }).callbacks;

            OpenPosition.expect('save');
            callbacks.toggleArchived(openPosition);
            expect(openPosition.archived).toBe(true);
            OpenPosition.flush('save');
            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'Position archived',
                className: 'success',
            });
        });

        it('should delete position, toast, and trigger callbacks when deleteOpenPosition is called', () => {
            render();
            jest.spyOn($window, 'confirm').mockReturnValue(true);
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            jest.spyOn(scope, '$broadcast').mockImplementation(() => {});
            const openPosition = OpenPosition.fixtures.getInstance({
                archive: false,
            });
            const callbacks = _.findWhere(scope.displayColumns, {
                prop: 'editorAbilities',
            }).callbacks;

            OpenPosition.expect('destroy');
            callbacks.deleteOpenPosition(openPosition);

            expect($window.confirm).toHaveBeenCalledWith('Are you sure you want to delete this position?');
            OpenPosition.flush('destroy');
            expect(ngToast.create).toHaveBeenCalledWith({
                content: `Successfully deleted ${openPosition.title}`,
                className: 'success',
            });
            expect(scope.$broadcast).toHaveBeenCalledWith('admin:OpenPositionDestroyed', openPosition);
        });
    });
});
