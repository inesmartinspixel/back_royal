import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';

describe('Admin::AdminEditHiringTeam', () => {
    let renderer;
    let SpecHelper;
    let elem;
    let scope;
    let HiringTeam;
    let hiringTeam;
    let User;
    let goBack;
    let destroyed;
    let created;
    let $location;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            SpecHelper.stubCurrentUser('admin');
            HiringTeam = $injector.get('HiringTeam');
            User = $injector.get('User');
            $location = $injector.get('$location');

            $injector.get('HiringTeamFixtures');
        });

        hiringTeam = HiringTeam.fixtures.getInstance();

        goBack = jest.fn();
        destroyed = jest.fn();
        created = jest.fn();
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.goBack = goBack;
        renderer.scope.destroyed = destroyed;
        renderer.scope.created = created;
        renderer.scope.hiringTeam = hiringTeam;
        renderer.render(
            '<admin-edit-hiring-team thing="hiringTeam" go-back="goBack()" created="created($thing)" destroyed="destroyed($thing)"></admin-edit-hiring-team>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it('should have working tabs', () => {
        User.expect('index'); // Default tab is 'edit', which indexes users immediately
        expect($location.search().tab).toBeUndefined();
        render();

        // Should default to edit
        expect($location.search().tab).toEqual('edit');
        SpecHelper.expectElement(elem, 'admin-edit-hiring-team-details');

        // Mimic clicking member-assignments
        SpecHelper.click(elem, '[name="member-assignments-tab"]');
        expect($location.search().tab).toEqual('member-assignments');
        SpecHelper.expectElement(elem, 'admin-edit-hiring-team-members');

        // Mimic clicking edit
        SpecHelper.click(elem, '[name="edit-tab"]');
        expect($location.search().tab).toEqual('edit');
        SpecHelper.expectElement(elem, 'admin-edit-hiring-team-details');

        // Mimic clicking payments
        SpecHelper.click(elem, '[name="payments-tab"]');
        expect($location.search().tab).toEqual('payments');
        SpecHelper.expectElement(elem, 'admin-owner-payments');

        User.flush('index');
    });

    it('should only show member-assignments tab when hiringTeam.id', () => {
        const originalValue = hiringTeam.id;

        hiringTeam.id = null;
        render();
        SpecHelper.expectNoElement(elem, '[name="member-assignments-tab"]');

        User.expect('index');
        hiringTeam.id = originalValue;
        scope.$digest();
        SpecHelper.expectElement(elem, '[name="member-assignments-tab"]');
        User.flush('index');
    });

    it('should have a working goBack button', () => {
        User.expect('index'); // Default tab is 'edit', which indexes users immediately
        render();
        User.flush('index');
        SpecHelper.click(elem, '[name="back-to-list"]');
        expect(goBack).toHaveBeenCalled();
    });

    it('should remove tab search param on destroy', () => {
        User.expect('index'); // Default tab is 'edit', which indexes users immediately
        render();
        User.flush('index');
        expect($location.search().tab).toEqual('edit');
        scope.$destroy();
        expect($location.search().tab).toBeUndefined();
    });
});
