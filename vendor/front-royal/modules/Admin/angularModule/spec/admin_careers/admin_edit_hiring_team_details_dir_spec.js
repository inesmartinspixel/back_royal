import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';

describe('Admin::AdminEditHiringTeamDetails', () => {
    let renderer;
    let SpecHelper;
    let elem;
    let scope;
    let HiringTeam;
    let hiringTeam;
    let User;
    let hiringManagers;
    let $window;
    let goBack;
    let ngToast;
    let destroyed;
    let created;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            SpecHelper.stubCurrentUser('admin');
            HiringTeam = $injector.get('HiringTeam');
            User = $injector.get('User');
            ngToast = $injector.get('ngToast');
            $window = $injector.get('$window');

            $injector.get('HiringTeamFixtures');
        });

        hiringTeam = HiringTeam.fixtures.getInstance();
        hiringManagers = [];
        _.each(hiringTeam.hiring_manager_ids, hiringManagerId => {
            hiringManagers.push(
                User.fixtures.getInstance({
                    id: hiringManagerId,
                }),
            );
        });
        hiringManagers.push(User.fixtures.getInstance());
        hiringManagers.push(User.fixtures.getInstance());
        hiringManagers.push(User.fixtures.getInstance());
        hiringManagers.push(User.fixtures.getInstance());
        hiringManagers.push(User.fixtures.getInstance());

        goBack = jest.fn();
        destroyed = jest.fn();
        created = jest.fn();
    });

    function render() {
        User.expect('index').returns(hiringManagers);
        renderer = SpecHelper.renderer();
        renderer.scope.goBack = goBack;
        renderer.scope.destroyed = destroyed;
        renderer.scope.created = created;
        renderer.scope.hiringTeam = hiringTeam;
        renderer.render(
            '<admin-edit-hiring-team-details hiring-team="hiringTeam" created="created({$thing: hiringTeam})" destroyed="destroyed({$thing: hiringTeam})" go-back="goBack({$thing: hiringTeam})"></admin-edit-hiring-team-details>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        User.flush('index');
    }

    function renderCreate() {
        renderer = SpecHelper.renderer();
        renderer.scope.goBack = goBack;
        renderer.scope.destroyed = destroyed;
        renderer.scope.created = created;
        hiringTeam = HiringTeam.new();
        renderer.scope.hiringTeam = hiringTeam;
        renderer.render(
            '<admin-edit-hiring-team-details hiring-team="hiringTeam" created="created({$thing: hiringTeam})" destroyed="destroyed({$thing: hiringTeam})" go-back="goBack({$thing: hiringTeam})"></admin-edit-hiring-team-details>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('editing', () => {
        it('should allow editing and saving', () => {
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            render();
            SpecHelper.updateInput(elem, '[name="title"]', 'New Team');
            expect(scope.proxy.title).toEqual('New Team');

            const ownerId = scope.proxy.hiring_manager_ids[3];
            SpecHelper.updateSelect(elem, '[name="owner"]', ownerId);
            expect(scope.proxy.owner_id).toEqual(ownerId);

            HiringTeam.expect('save');
            SpecHelper.click(elem, '[name="save"]');
            SpecHelper.expectElementDisabled(elem, '[name="save"]');
            SpecHelper.expectElementDisabled(elem, '[name="destroy"]');
            HiringTeam.flush('save');
            SpecHelper.expectElementEnabled(elem, '[name="save"]');
            SpecHelper.expectElementEnabled(elem, '[name="destroy"]');
            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'Team saved',
                className: 'success',
            });
        });

        it('should allow for setting subscription_required to false when current plan is HIRING_PLAN_PAY_PER_POST', () => {
            hiringTeam.hiring_plan = HiringTeam.HIRING_PLAN_PAY_PER_POST;
            render();
            SpecHelper.expectNoElement(elem, '.row.subscription-required .sub-text');
            SpecHelper.uncheckCheckbox(elem, '[name="subscription_required"]');
            SpecHelper.expectElementText(
                elem,
                '.row.subscription-required .sub-text',
                'Plan will be changed to unlimited_w_sourcing and all existing subscriptions will be canceled and given a prorated refund.',
            );
            HiringTeam.expect('save');
            SpecHelper.click(elem, '[name="save"]');
            expect(scope.proxy.subscription_required).toBe(false); // because it wasn't set, it should get saved as false
            expect(scope.proxy.hiring_plan).toEqual(HiringTeam.HIRING_PLAN_UNLIMITED_WITH_SOURCING);
        });

        it('should allow for setting subscription_required to false when current plan is HIRING_PLAN_UNLIMITED_WITH_SOURCING', () => {
            hiringTeam.hiring_plan = HiringTeam.HIRING_PLAN_UNLIMITED_WITH_SOURCING;
            hiringTeam.subscriptions = [{}];
            render();
            SpecHelper.expectNoElement(elem, '.row.subscription-required .sub-text');
            SpecHelper.uncheckCheckbox(elem, '[name="subscription_required"]');
            SpecHelper.expectElementText(
                elem,
                '.row.subscription-required .sub-text',
                'Subscription will be canceled with no refund.',
            );
            HiringTeam.expect('save');
            SpecHelper.click(elem, '[name="save"]');
            expect(scope.proxy.subscription_required).toBe(false); // because it wasn't set, it should get saved as false
            expect(scope.proxy.hiring_plan).toEqual(HiringTeam.HIRING_PLAN_UNLIMITED_WITH_SOURCING);
        });

        it('should disable save button when invalid', () => {
            render();
            SpecHelper.expectElementEnabled(elem, '[name="save"]');
            SpecHelper.updateInput(elem, '[name="title"]', '');
            SpecHelper.expectElementDisabled(elem, '[name="save"]');
        });

        it('should disable save and destroy buttons when there is no proxy', () => {
            render();
            SpecHelper.expectElementEnabled(elem, '[name="save"]');
            SpecHelper.expectElementEnabled(elem, '[name="destroy"]');
            scope.proxy = undefined;
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, '[name="save"]');
            SpecHelper.expectElementDisabled(elem, '[name="destroy"]');
        });

        it('should enable member select and require an owner if there are any hiring managers', () => {
            render();
            SpecHelper.expectElementEnabled(elem, '[name="save"]');
            SpecHelper.expectElementEnabled(elem, 'select[name="owner"]');
            scope.proxy.owner_id = null;
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, '[name="save"]');
            scope.proxy.hiring_manager_ids = [];
            scope.$digest();
            SpecHelper.expectElementEnabled(elem, '[name="save"]');
        });

        it('should disable member select and show message if no team members have been added', () => {
            hiringTeam.hiring_manager_ids = [];
            render();
            SpecHelper.expectElementDisabled(elem, 'select[name="owner"]');
            SpecHelper.expectElementText(
                elem,
                '.sub-text:eq(0)',
                'You must add at least one team member before selecting an owner.',
            );
        });

        it('should disable destroy button when there are accepted users', () => {
            render();
            SpecHelper.expectElementEnabled(elem, '[name="destroy"]');

            hiringManagers[0].hiring_application = {
                status: 'accepted',
            };
            render();
            SpecHelper.expectElementDisabled(elem, '[name="destroy"]');
        });
    });

    describe('creating', () => {
        it('should call created callback only when creating a new item', () => {
            renderCreate();
            SpecHelper.updateInput(elem, '[name="title"]', 'New Team');
            SpecHelper.updateInput(elem, '[name="domain"]', 'team.com');

            // When the new item is created, the callback should be triggered
            HiringTeam.expect('create');
            SpecHelper.click(elem, '[name="save"]');
            HiringTeam.flush('create');

            User.expect('index');
            scope.proxy.id = hiringTeam.id || 'saved';
            expect(created).toHaveBeenCalledWith({
                $thing: scope.hiringTeam,
            });
            User.expect('index');
            created.mockClear();

            // When the existing item is saved again, the callback should not be triggered
            SpecHelper.updateInput(elem, '[name="title"]', 'Team');
            HiringTeam.expect('save');
            SpecHelper.click(elem, '[name="save"]');
            HiringTeam.flush('save');
            expect(created).not.toHaveBeenCalled();
        });

        it('should hide member select when creating a new team', () => {
            renderCreate();
            SpecHelper.expectNoElement(elem, 'select[name="owner"]');
        });

        it('should disable destroy button when creating a new team', () => {
            hiringTeam = HiringTeam.new();
            renderCreate();
            SpecHelper.expectElementDisabled(elem, '[name="destroy"]');
        });
    });

    describe('deleting', () => {
        it('should allow for deleting', () => {
            jest.spyOn($window, 'confirm').mockReturnValue(true);
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            render();
            HiringTeam.expect('destroy');
            SpecHelper.click(elem, '[name="destroy"]');
            SpecHelper.expectElementDisabled(elem, '[name="destroy"]');
            SpecHelper.expectElementDisabled(elem, '[name="save"]');
            HiringTeam.flush('destroy');
            SpecHelper.expectElementEnabled(elem, '[name="destroy"]');
            SpecHelper.expectElementEnabled(elem, '[name="save"]');
            expect(destroyed).toHaveBeenCalledWith({
                $thing: scope.hiringTeam,
            });
            expect(goBack).toHaveBeenCalled();
            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'Team deleted',
                className: 'success',
            });
        });
    });
});
