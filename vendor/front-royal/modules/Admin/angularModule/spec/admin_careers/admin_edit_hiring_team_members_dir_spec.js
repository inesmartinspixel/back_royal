import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_team_fixtures';

describe('Admin::AdminEditHiringTeamMembers', () => {
    let renderer;
    let SpecHelper;
    let elem;
    let scope;
    let HiringTeam;
    let hiringTeam;
    let anotherHiringTeam;
    let User;
    let hiringManagers;
    let hiringManager;
    let editContentItemListMixin;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            SpecHelper.stubCurrentUser('admin');
            HiringTeam = $injector.get('HiringTeam');
            User = $injector.get('User');
            editContentItemListMixin = $injector.get('editContentItemListMixin');

            $injector.get('HiringTeamFixtures');
        });

        hiringTeam = HiringTeam.fixtures.getInstance();
        anotherHiringTeam = HiringTeam.fixtures.getInstance();
        hiringManagers = [User.fixtures.getInstance()];
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.hiringTeam = hiringTeam;
        renderer.render('<admin-edit-hiring-team-members hiring-team="hiringTeam""></admin-edit-hiring-team-members>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('checkbox callbacks', () => {
        beforeEach(() => {
            hiringManager = hiringManagers[0];
        });

        describe('onClick', () => {
            beforeEach(() => {
                User.expect('update');
            });

            it('should add user to team if not already a team member', () => {
                hiringManager.hiring_team_id = null;
                render();
                getColumnConfig('is-member').callbacks.onClick(hiringManager);
                User.flush('update');
                expect(hiringManager.hiring_team_id).toEqual(hiringTeam.id);
                expect(hiringTeam.hiring_manager_ids).toContain(hiringManager.id);
            });

            it('should remove user from team if already a team member', () => {
                hiringManager.hiring_team_id = hiringTeam.id;
                render();
                getColumnConfig('is-member').callbacks.onClick(hiringManager);
                User.flush('update');
                expect(hiringManager.hiring_team_id).toBe(null);
                expect(hiringTeam.hiring_manager_ids).not.toContain(hiringManager.id);
            });
        });

        describe('isChecked', () => {
            it('should be isChecked if user is already a team member', () => {
                hiringManager.hiring_team_id = hiringTeam.id;
                render();
                expect(getColumnConfig('is-member').callbacks.isChecked(hiringManager)).toBe(true);
            });

            it('should be !isChecked if user is not already a team member', () => {
                hiringManager.hiring_team_id = undefined;
                render();
                expect(getColumnConfig('is-member').callbacks.isChecked(hiringManager)).toBe(false);
            });
        });

        describe('isDisabled', () => {
            it('should have correct disabled state based on user.$$saving', () => {
                render();

                // Should be this when we first load the page
                expect(hiringManager.$$saving).toBeUndefined();
                expect(getColumnConfig('is-member').callbacks.isDisabled(hiringManager)).toBe(false);

                // Mimic a save
                hiringManager.$$saving = true;
                expect(getColumnConfig('is-member').callbacks.isDisabled(hiringManager)).toBe(true);

                // Mimic the above save completing
                hiringManager.$$saving = false;
                expect(getColumnConfig('is-member').callbacks.isDisabled(hiringManager)).toBe(false);
            });
        });
    });

    it('should link to editContentItemListMixin', () => {
        jest.spyOn(editContentItemListMixin, 'onLink').mockImplementation(() => {});
        render();
        expect(editContentItemListMixin.onLink).toHaveBeenCalledWith(
            scope,
            'adminEditHiringTeamMembers',
            [
                {
                    server: true,
                    default: true,
                    value: {
                        has_hiring_application: true,
                    },
                },
                {
                    server: true,
                    default: true,
                    value: {
                        hiring_application_status: ['pending', 'accepted'],
                    },
                },
            ],
            [],
            null,
            true,
        );
    });

    it('should set scope.indexParams and link to editContentItemListMixin when scope.hiringTeam.id changes', () => {
        jest.spyOn(editContentItemListMixin, 'onLink').mockImplementation(() => {});
        render();
        expect(scope.indexParams.available_for_hiring_team).toEqual(hiringTeam.id);
        expect(editContentItemListMixin.onLink).toHaveBeenCalled();
        scope.hiringTeam = anotherHiringTeam;
        scope.$digest();
        expect(scope.indexParams.available_for_hiring_team).toEqual(anotherHiringTeam.id);
    });

    function getColumnConfig(columnId) {
        return _.find(scope.displayColumns, column => column.id === columnId);
    }
});
