import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Users/angularModule/spec/_mock/fixtures/users';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import chooseARoleLocales from 'Careers/locales/careers/choose_a_role-en.json';
import jobPreferencesFormLocales from 'Careers/locales/careers/edit_career_profile/job_preferences_form-en.json';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';
import candidateListLocales from 'Careers/locales/careers/candidate_list-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(
    fieldOptionsLocales,
    chooseARoleLocales,
    jobPreferencesFormLocales,
    candidateListCardLocales,
    candidateListLocales,
);

describe('Admin::AdminEditCareerProfileList', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let ngToast;
    let CareerProfile;
    let CareerProfileList;
    let users;
    let careerProfileList;
    let goBack;
    let destroyed;
    let created;
    let $window;
    let User;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            ngToast = $injector.get('ngToast');
            User = $injector.get('User');
            $injector.get('UserFixtures');
            CareerProfile = $injector.get('CareerProfile');
            CareerProfileList = $injector.get('CareerProfileList');
            $injector.get('CareerProfileFixtures');
            $window = $injector.get('$window');
        });

        users = [
            _.extend(User.fixtures.getInstance(), {
                career_profile: CareerProfile.fixtures.getInstance(),
            }),
            _.extend(User.fixtures.getInstance(), {
                career_profile: CareerProfile.fixtures.getInstance(),
            }),
            _.extend(User.fixtures.getInstance(), {
                career_profile: CareerProfile.fixtures.getInstance(),
            }),
        ];
        const careerProfiles = _.pluck(users.slice(0, 2), 'career_profile');
        careerProfileList = CareerProfileList.new({
            id: 'id',
            name: 'name',
            career_profiles: careerProfiles,
            career_profile_ids: _.pluck(careerProfiles, 'id'),
        });
        jest.spyOn(ngToast, 'create').mockImplementation(() => {});

        SpecHelper.stubCurrentUser();
    });

    afterEach(() => {
        SpecHelper.cleanup();
    });

    it('should allow for editing and saving', () => {
        render();
        SpecHelper.updateInput(elem, '[name="name"]', 'newName');
        expect(careerProfileList.name).toEqual('newName');

        const careerProfile = _.last(users).career_profile;
        expect(_.contains(careerProfileList.career_profile_ids, careerProfile.id)).toBe(false);
        CareerProfile.expect('show').returns(careerProfile);
        SpecHelper.updateMultiSelect(elem, '[name="career_profiles"]', careerProfile.id);
        CareerProfile.flush('show');
        expect(_.contains(careerProfileList.career_profile_ids, careerProfile.id)).toBe(true);

        SpecHelper.updateMultiSelect(elem, '[name="role_descriptors"]', 'general_management');
        expect(careerProfileList.role_descriptors).toEqual(['general_management']);

        CareerProfileList.expect('save');
        SpecHelper.click(elem, '[name="save"]');
        SpecHelper.expectElementDisabled(elem, '[name="save"]');
        SpecHelper.expectElementDisabled(elem, '[name="destroy"]');
        CareerProfileList.flush('save');
        SpecHelper.expectElementEnabled(elem, '[name="save"]');
        SpecHelper.expectElementEnabled(elem, '[name="destroy"]');
        expect(ngToast.create).toHaveBeenCalledWith({
            content: 'List saved',
            className: 'success',
        });
    });

    it('should disable save button when invalid', () => {
        render();
        SpecHelper.expectElementEnabled(elem, '[name="save"]');
        SpecHelper.updateInput(elem, '[name="name"]', '');
        SpecHelper.expectElementDisabled(elem, '[name="save"]');
    });

    it('should call created callback only when creating a new item', () => {
        careerProfileList = CareerProfileList.new();
        render();
        SpecHelper.updateInput(elem, '[name="name"]', 'newName');

        // When the new item is created, the callback should be triggered
        saveAndFlush();
        expect(created).toHaveBeenCalledWith(careerProfileList);
        created.mockClear();

        // When the existing item is saved again, the callback should not be triggered
        saveAndFlush();
        expect(created).not.toHaveBeenCalled();
    });

    it('should allow for deleting', () => {
        jest.spyOn($window, 'confirm').mockReturnValue(true);
        render();
        CareerProfileList.expect('destroy');
        SpecHelper.click(elem, '[name="destroy"]');
        SpecHelper.expectElementDisabled(elem, '[name="destroy"]');
        SpecHelper.expectElementDisabled(elem, '[name="save"]');
        CareerProfileList.flush('destroy');
        SpecHelper.expectElementEnabled(elem, '[name="destroy"]');
        SpecHelper.expectElementEnabled(elem, '[name="save"]');
        expect(destroyed).toHaveBeenCalledWith(careerProfileList);
        expect(goBack).toHaveBeenCalled();
        expect(ngToast.create).toHaveBeenCalledWith({
            content: 'List deleted',
            className: 'success',
        });
    });

    it('should have a working goBack button', () => {
        render();
        SpecHelper.click(elem, '[name="back-to-list"]');
        expect(goBack).toHaveBeenCalled();
    });

    it('should have a button to export career profile list as a CSV', () => {
        render();
        SpecHelper.expectElement(elem, '.export-link');
    });

    it('should have a button to toggle the visibility of the candidate list', () => {
        render();
        SpecHelper.expectElement(elem, '[name="toggle_candidate_list"]');
        expect(scope.showCandidateList).toBe(false);
        expect(scope.toggleCandidateListText).toEqual('Show Candidate List');
        SpecHelper.expectNoElement(elem, 'candidate-list');

        SpecHelper.click(elem, '[name="toggle_candidate_list"]');
        expect(scope.showCandidateList).toBe(true);
        expect(scope.toggleCandidateListText).toEqual('Hide Candidate List');
        SpecHelper.expectElement(elem, 'candidate-list');
    });

    it('should change the ordering of the candidate cards in the candidate list if the ordering changes in the form', () => {
        render();

        // make the candidate candidateList visible
        makeCandidateListVisible();

        // change the ordering
        const originalOrder = scope.careerProfiles;
        const temp = scope.careerProfiles[0];
        scope.careerProfiles[0] = scope.careerProfiles[1];
        scope.careerProfiles[1] = temp;

        // check the candidateList's profiles
        const candidateListEl = SpecHelper.expectElement(elem, 'candidate-list');
        const candidateListProfiles = candidateListEl.isolateScope().ngModel;
        expect(scope.listProfiles).not.toEqual(originalOrder);
        expect(candidateListProfiles).toEqual(scope.listProfiles);
    });

    it("should add the candidate's card to the candidate list if selected from the dropdown list of available candidates", () => {
        render();

        // make the candidate candidateList visible
        makeCandidateListVisible();

        const originalProfilesLength = scope.listProfiles.length;

        const careerProfile = _.last(users).career_profile;
        expect(_.contains(careerProfileList.career_profile_ids, careerProfile.id)).toBe(false);
        CareerProfile.expect('show').returns(careerProfile);
        SpecHelper.updateMultiSelect(elem, '[name="career_profiles"]', careerProfile.id);
        CareerProfile.flush('show');
        expect(_.contains(careerProfileList.career_profile_ids, careerProfile.id)).toBe(true);

        expect(scope.listProfiles.length).not.toEqual(originalProfilesLength);

        // check the candidateList's profiles
        const candidateListEl = SpecHelper.expectElement(elem, 'candidate-list');
        const candidateListProfiles = candidateListEl.isolateScope().ngModel;
        expect(candidateListProfiles).toEqual(scope.listProfiles);
    });

    it("shoud remove the candidate's card from the candidate list if they are removed from the list", () => {
        render();

        // make the candidate candidateList visible
        makeCandidateListVisible();

        const originalProfilesLength = scope.listProfiles.length;
        careerProfileList.career_profile_ids.splice(0, 1); // remove all but one career profile id from career profile list
        scope.$digest();
        expect(scope.listProfiles.length).not.toEqual(originalProfilesLength);

        // check the candidateList's profiles
        const candidateListEl = SpecHelper.expectElement(elem, 'candidate-list');
        const candidateListProfiles = candidateListEl.isolateScope().ngModel;
        expect(candidateListProfiles).toEqual(scope.listProfiles);
    });

    function makeCandidateListVisible() {
        // make the candidate candidateList visible
        SpecHelper.click(elem, '[name="toggle_candidate_list"]');
        expect(scope.showCandidateList).toBe(true);
        expect(scope.toggleCandidateListText).toEqual('Hide Candidate List');
        SpecHelper.expectElement(elem, 'candidate-list');
    }

    function saveAndFlush() {
        CareerProfileList.expect('save');
        SpecHelper.click(elem, '[name="save"]');
        CareerProfileList.flush('save');
        careerProfileList.id = careerProfileList.id || 'saved';
    }

    function render() {
        CareerProfileList.expect('index').returns(careerProfileList);
        User.expect('index').returns(users);
        renderer = SpecHelper.renderer();
        renderer.scope.careerProfileList = careerProfileList;
        goBack = renderer.scope.goBack = jest.fn();
        destroyed = renderer.scope.destroyed = jest.fn();
        created = renderer.scope.created = jest.fn();
        renderer.render(
            '<admin-edit-career-profile-list thing="careerProfileList" go-back="goBack()" created="created($thing)" destroyed="destroyed($thing)"></admin-edit-career-profile-list>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
        SpecHelper.expectElement(elem, 'front-royal-spinner');
        SpecHelper.expectNoElement(elem, '[name="career_profiles"]');
        CareerProfileList.flush('index');
        User.flush('index');
        SpecHelper.expectNoElement(elem, 'front-royal-spinner');
        SpecHelper.expectElement(elem, '[name="career_profiles"]');
    }
});
