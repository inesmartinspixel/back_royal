import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/hiring_relationship_fixtures';
import jobPreferencesFormLocales from 'Careers/locales/careers/edit_career_profile/job_preferences_form-en.json';
import fieldOptionsLocales from 'Careers/locales/careers/field_options-en.json';
import candidateListCardLocales from 'Careers/locales/careers/candidate_list_card-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(jobPreferencesFormLocales, fieldOptionsLocales, candidateListCardLocales);

describe('Admin::AdminEditHiringRelationship', () => {
    let SpecHelper;
    let renderer;
    let elem;
    let scope;
    let HiringRelationship;
    let hiringRelationship;
    let existingHiringRelationshipsByCandidateId;
    let LogInAs;
    let $window;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            HiringRelationship = $injector.get('HiringRelationship');
            LogInAs = $injector.get('LogInAs');
            $window = $injector.get('$window');

            $injector.get('HiringRelationshipFixtures');
        });

        const hiringRelationships = [
            HiringRelationship.fixtures.getInstance(),
            HiringRelationship.fixtures.getInstance(),
            HiringRelationship.fixtures.getInstance(),
            HiringRelationship.fixtures.getInstance(),
            HiringRelationship.fixtures.getInstance(),
        ];
        hiringRelationship = hiringRelationships[0];
        existingHiringRelationshipsByCandidateId = _.indexBy(hiringRelationships, 'candidate_id');

        SpecHelper.stubCurrentUser();
    });

    function render(options = {}) {
        renderer = SpecHelper.renderer();
        renderer.scope.hiringRelationship = hiringRelationship;
        renderer.scope.existingHiringRelationships = existingHiringRelationshipsByCandidateId;
        renderer.scope.allowEdit = angular.isDefined(options.allowEdit) ? options.allowEdit : true;
        renderer.render(
            '<admin-edit-hiring-relationship hiring-relationship="hiringRelationship" existing-hiring-relationships="existingHiringRelationships" allow-edit="allowEdit"></admin-edit-hiring-relationship>',
        );
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    it("should show the candidate's card", () => {
        render();

        SpecHelper.expectElement(elem, 'candidate-list-card');
    });

    it('should show applicant links', () => {
        render();

        SpecHelper.expectElement(elem, 'applicant-links');
    });

    it('should support logging in as user', () => {
        render();

        jest.spyOn(scope, 'logInAsUser');
        jest.spyOn(LogInAs, 'logInAsUser').mockImplementation(() => {});

        SpecHelper.click(elem, '[name="logInAs"]');

        expect(scope.logInAsUser).toHaveBeenCalledWith(scope.hiringRelationship);
        expect(LogInAs.logInAsUser).toHaveBeenCalledWith({
            id: scope.hiringRelationship.candidate_id,
            email: scope.hiringRelationship.candidateEmail,
        });
    });

    it("should support copying deep link to candidate's career profile", () => {
        render();
        jest.spyOn(scope, 'copyCareerProfileDeepLinkUrl').mockImplementation(() => {});
        SpecHelper.expectElementText(elem, '[name="copyLink"]', 'Copy Link');
        SpecHelper.click(elem, '[name="copyLink"]');
        expect(scope.copyCareerProfileDeepLinkUrl).toHaveBeenCalled();
    });

    describe('form', () => {
        it('should be hidden if allowEdit is false', () => {
            const opts = {
                allowEdit: false,
            };
            render(opts);

            SpecHelper.expectNoElement(elem, '[name="update"]');
            SpecHelper.expectNoElement(elem, 'form[name="updateHiringRelationshipForm"]');
        });

        it('should display the hiring relationship status', () => {
            render();

            SpecHelper.expectElementText(elem, '.relationship-status', 'Waiting on Candidate');
        });

        it('should support updating the hiring manager and candidate closed stautses', () => {
            render();

            SpecHelper.expectElement(elem, 'input[name="hiringManagerClosed"]');
            SpecHelper.expectElement(elem, 'input[name="candidateClosed"]');

            SpecHelper.checkCheckbox(elem, 'input[name="hiringManagerClosed"]');
            expect(scope.hiringRelationshipProxy.hiring_manager_closed).toBe(true);
            SpecHelper.checkCheckbox(elem, 'input[name="candidateClosed"]');
            expect(scope.hiringRelationshipProxy.candidate_closed).toBe(true);

            HiringRelationship.expect('update');
            SpecHelper.click(elem, '[name="update"]');
            HiringRelationship.flush('update');
        });

        it('should show the reasons and feedback for a closed connection', () => {
            hiringRelationship.hiring_manager_closed = true;
            hiringRelationship.hiring_manager_closed_info = {
                reasons: ['foo', 'bar'],
                feedback: 'sample feedback',
            };
            render();

            SpecHelper.expectElementText(elem, '[name="reasons"] li:eq(0)', 'foo');
            SpecHelper.expectElementText(elem, '[name="reasons"] li:eq(1)', 'bar');
            SpecHelper.expectElementText(elem, '[name="feedback"]', 'sample feedback');
        });
    });
});
