import 'AngularSpecHelper';
import 'Admin/angularModule';
import 'Careers/angularModule/spec/_mock/fixtures/career_profiles';
import 'Cohorts/angularModule/spec/_mock/fixtures/cohorts';
import editDocumentsLocales from 'Settings/locales/settings/edit_documents-en.json';
import applicationStatusLocales from 'Settings/locales/settings/application_status-en.json';
import setSpecLocales from 'Translation/setSpecLocales';

setSpecLocales(editDocumentsLocales, applicationStatusLocales);

describe('adminStudentRecords', () => {
    let renderer;
    let elem;
    let scope;
    let SpecHelper;
    let user;
    let $q;
    let $http;
    let User;
    let ngToast;
    let Cohort;
    let CareerProfile;
    let S3IdentificationAsset;
    let S3TranscriptAsset;
    let EducationExperience;
    let DialogModal;

    beforeEach(() => {
        angular.mock.module('FrontRoyal.Admin', 'SpecHelper');

        angular.mock.inject($injector => {
            SpecHelper = $injector.get('SpecHelper');
            $q = $injector.get('$q');
            $http = $injector.get('$http');
            User = $injector.get('User');
            ngToast = $injector.get('ngToast');
            Cohort = $injector.get('Cohort');
            CareerProfile = $injector.get('CareerProfile');
            S3IdentificationAsset = $injector.get('S3IdentificationAsset');
            S3TranscriptAsset = $injector.get('S3TranscriptAsset');
            EducationExperience = $injector.get('EducationExperience');
            DialogModal = $injector.get('DialogModal');

            $injector.get('CohortFixtures');
            $injector.get('CareerProfileFixtures');

            SpecHelper.stubDirective('editEnglishDocuments');
        });

        user = SpecHelper.stubCurrentUser();
        user.career_profile = CareerProfile.fixtures.getInstance();
        user.relevant_cohort = Cohort.fixtures.getInstance();
        user.s3_english_language_proficiency_documents = [];

        const requiringTranscriptAttrs = {
            will_not_complete: false,
            degree_program: true,
            graduation_year: 2010,
        };
        user.career_profile.education_experiences = [
            EducationExperience.new({
                ...requiringTranscriptAttrs,
                id: 1,
                transcript_waiver: 'foo waiver',
                transcripts: [S3TranscriptAsset.new()],
            }),
            EducationExperience.new({
                ...requiringTranscriptAttrs,
                id: 2,
                transcript_waiver: 'bar waiver',
                transcripts: [S3TranscriptAsset.new()],
            }),
            EducationExperience.new({ ...requiringTranscriptAttrs, id: 3, transcript_waiver: null, transcripts: [] }),
            EducationExperience.new({
                ...requiringTranscriptAttrs,
                id: 4,
                transcript_waiver: null,
                transcripts: [S3TranscriptAsset.new()],
            }),
        ];
        user.s3_transcript_assets = [];
    });

    function render() {
        renderer = SpecHelper.renderer();
        renderer.scope.user = user;
        renderer.render('<admin-student-records user="user"></admin-student-records>');
        elem = renderer.elem;
        scope = elem.isolateScope();
    }

    describe('Identification section', () => {
        it('should show progress_badge_complete_turquoise box if identity_verified', () => {
            jest.spyOn(user, 'recordsIndicateUserShouldUploadIdentificationDocument', 'get').mockReturnValue(true);
            user.identity_verified = true;
            render();
            SpecHelper.expectElement(elem, '[id="identification-section"] .box img.progress-complete-turqoise');
            SpecHelper.expectElementText(
                elem,
                '[id="identification-section"] .box img + .sub-text',
                'Identification Verified',
            );
        });

        it('should show document upload if !identity_verified', () => {
            user.identity_verified = false;
            render();
            SpecHelper.expectElement(elem, '[id="identification-section"] input[name="identification-upload"]');
        });

        it('should show approval checkbox wired to identity_verified flag', () => {
            user.identity_verified = false;
            render();
            SpecHelper.toggleCheckbox(elem, '[id="identification-section"] input[type="checkbox"][name="approve"]');
            expect(scope.userProxy.identity_verified).toBe(true);
        });

        it('should allow viewing document', () => {
            user.s3_identification_asset = S3IdentificationAsset.new();
            render();
            jest.spyOn(scope, 'viewDocument');
            jest.spyOn($http, 'get').mockReturnValue(
                $q.when({
                    data: {},
                }),
            );
            SpecHelper.click(elem, '[id="identification-section"] .file-name');
            expect(scope.viewDocument).toHaveBeenCalledWith(scope.userProxy.s3_identification_asset);
            expect($http.get).toHaveBeenCalled();
        });

        it('should support deleting document', () => {
            user.s3_identification_asset = S3IdentificationAsset.new();
            render();
            jest.spyOn(scope, 'deleteIdentification').mockImplementation(() => {});
            SpecHelper.click(elem, '[id="identification-section"] .trashcan');
            expect(scope.deleteIdentification).toHaveBeenCalled();
        });

        it('should apply disabled class to trashcan when deletingIdentification and prevent destroy call', () => {
            jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {});
            user.s3_identification_asset = S3IdentificationAsset.new();
            render();
            SpecHelper.expectDoesNotHaveClass(elem, '[id="identification-section"] .trashcan', 'disabled');
            scope.deletingIdentification = true;
            scope.$digest();
            SpecHelper.expectHasClass(elem, '[id="identification-section"] .trashcan', 'disabled');
            jest.spyOn(user.s3_identification_asset, 'destroy').mockImplementation(() => {});
            SpecHelper.click(elem, '[id="identification-section"] .trashcan');
            expect(user.s3_identification_asset.destroy).not.toHaveBeenCalled();
        });

        it('should show a spinner when deletingIdentification', () => {
            render();
            SpecHelper.expectNoElement(elem, '[id="identification-section"] front-royal-spinner');
            scope.deletingIdentification = true;
            scope.$digest();
            SpecHelper.expectElement(elem, '[id="identification-section"] front-royal-spinner');
        });

        it('should show a spinner when uploadingIdentification', () => {
            render();
            SpecHelper.expectNoElement(elem, '[id="identification-section"] front-royal-spinner');
            scope.uploadingIdentification = true;
            scope.$digest();
            SpecHelper.expectElement(elem, '[id="identification-section"] front-royal-spinner');
        });

        it('should show message if there is an error', () => {
            render();
            SpecHelper.expectNoElement(elem, '.file-error-message');
            scope.idErrMessage = 'foo message';
            scope.$digest();
            SpecHelper.expectElementText(elem, '.file-error-message', 'foo message');
        });
    });

    describe('Transcripts section', () => {
        it('should allow viewing document', () => {
            render();
            jest.spyOn(scope, 'viewDocument');
            jest.spyOn($http, 'get').mockReturnValue(
                $q.when({
                    data: {},
                }),
            );
            SpecHelper.click(elem, '[id="transcripts-section"] .file-name:eq(0)');
            expect(scope.viewDocument).toHaveBeenCalledWith(
                scope.userProxy.career_profile.education_experiences[0].transcripts[0],
            );
            expect($http.get).toHaveBeenCalled();
        });

        it('should support deleting document', () => {
            render();
            jest.spyOn(scope, 'deleteTranscript').mockImplementation(() => {});
            SpecHelper.click(elem, '[id="transcripts-section"] .trashcan:eq(0)');
            expect(scope.deleteTranscript).toHaveBeenCalledWith(
                scope.userProxy.career_profile.education_experiences[0],
                0,
            );
        });

        it('should apply disabled class to trashcan when deletingTranscript and prevent destroy call', () => {
            jest.spyOn(DialogModal, 'confirm').mockImplementation(() => {});
            const transcript = S3TranscriptAsset.new();

            user.s3_transcript_assets = [];
            user.career_profile.education_experiences = [
                EducationExperience.new({
                    transcripts: [transcript],
                    graduation_year: 1900,
                    will_not_complete: false,
                    degree_program: true,
                }),
                EducationExperience.new({
                    transcripts: [transcript],
                    graduation_year: 1900,
                    will_not_complete: false,
                    degree_program: true,
                }),
            ];

            render();

            SpecHelper.expectNoElement(elem, '[name="legacy-transcripts"]');
            SpecHelper.expectDoesNotHaveClass(elem, '[id="transcripts-section"] .trashcan:eq(0)', 'disabled');
            SpecHelper.expectDoesNotHaveClass(elem, '[id="transcripts-section"] .trashcan:eq(1)', 'disabled');
            scope.userProxy.career_profile.education_experiences.forEach(experience => {
                experience.$$deletingTranscript = true;
            });
            scope.$digest();
            SpecHelper.expectHasClass(elem, '[id="transcripts-section"] .trashcan:eq(0)', 'disabled');
            SpecHelper.expectHasClass(elem, '[id="transcripts-section"] .trashcan:eq(1)', 'disabled');
            jest.spyOn(transcript, 'destroy').mockImplementation(() => {});
            SpecHelper.click(elem, '[id="transcripts-section"] .trashcan:eq(0)');
            expect(transcript.destroy).not.toHaveBeenCalled();
        });

        it('should show a spinner when deletingTranscript', () => {
            render();
            SpecHelper.expectNoElement(elem, '[id="transcripts-section"] front-royal-spinner');
            scope.userProxy.career_profile.educationExperiencesIndicatingTranscriptRequired[0].$$deletingTranscript = true;
            scope.$digest();
            SpecHelper.expectElements(elem, '[id="transcripts-section"] front-royal-spinner', 1);
        });

        it('should show a spinner when uploadingTranscript', () => {
            render();
            SpecHelper.expectNoElement(elem, '[id="transcripts-section"] front-royal-spinner');
            scope.userProxy.career_profile.educationExperiencesIndicatingTranscriptRequired[0].$$uploadingTranscript = true;
            scope.$digest();
            SpecHelper.expectElements(elem, '[id="transcripts-section"] front-royal-spinner', 1);
        });

        it('should show message if there is an error', () => {
            render();
            SpecHelper.expectNoElement(elem, '[id="transcripts-section"] .file-error-message');
            scope.userProxy.career_profile.educationExperiencesIndicatingTranscriptRequired[0].$$transcriptErrMessage =
                'foo message';
            scope.$digest();
            SpecHelper.expectElementText(elem, '[id="transcripts-section"] .file-error-message', 'foo message');
        });

        it('should show legacy transcripts', () => {
            user.s3_transcript_assets = [S3TranscriptAsset.new(), S3TranscriptAsset.new()];
            user.career_profile.education_experiences = [];
            render();
            SpecHelper.expectElements(elem, '[name="legacy-transcript-container"]', 2);
        });

        it('should show waiver reason input when waiving', () => {
            const educationExperience = EducationExperience.new({
                transcripts: undefined,
                waiver_reason: undefined,
                graduation_year: 1900,
                will_not_complete: false,
                degree_program: true,
            });
            user.career_profile.education_experiences = [educationExperience];

            render();
            SpecHelper.click(elem, '[name="waive-checkbox"]');
            SpecHelper.expectElement(elem, '.waiver-input');
            SpecHelper.expectElementDisabled(elem, '[name="save-documents"]');
            SpecHelper.updateTextInput(elem, '.waiver-input', 'test waiver');
            SpecHelper.click(elem, '[name="waive-checkbox"]');
            SpecHelper.expectNoElement(elem, '.waiver-input');
        });

        it('should show approve checkbox when transcript uploaded', () => {
            user.career_profile.education_experiences = [
                EducationExperience.new({
                    transcripts: [S3TranscriptAsset.new()],
                    waiver_reason: undefined,
                    graduation_year: 1900,
                    will_not_complete: false,
                    degree_program: true,
                }),
                EducationExperience.new({
                    transcripts: [S3TranscriptAsset.new()],
                    waiver_reason: undefined,
                    graduation_year: 1900,
                    will_not_complete: false,
                    degree_program: true,
                }),
            ];
            render();
            SpecHelper.expectElements(elem, '#transcripts-section [name="approve"]', 2);
        });

        describe('official_transcript_required', () => {
            beforeEach(() => {
                user.career_profile.education_experiences = [
                    EducationExperience.new({
                        id: 1,
                        transcripts: [S3TranscriptAsset.new()],
                        waiver_reason: undefined,
                        graduation_year: 1902,
                        will_not_complete: false,
                        degree_program: true,
                    }),
                    EducationExperience.new({
                        id: 2,
                        transcripts: [S3TranscriptAsset.new()],
                        waiver_reason: undefined,
                        graduation_year: 1901,
                        will_not_complete: false,
                        degree_program: true,
                    }),
                    EducationExperience.new({
                        id: 'some-uuid',
                        transcripts: [S3TranscriptAsset.new()],
                        waiver_reason: undefined,
                        graduation_year: 1900,
                        will_not_complete: false,
                        degree_program: true,
                    }),
                ];
            });

            it('should show correct text for official or unofficial transcript required', () => {
                user.career_profile.education_experiences[2].official_transcript_required = true;
                render();
                SpecHelper.expectElementText(elem, '.transcript-required-text:eq(0)', '(Unofficial required)');
                SpecHelper.expectElementText(elem, '.transcript-required-text:eq(2)', '(Official required)');
            });

            describe('override', () => {
                beforeEach(() => {
                    jest.spyOn(
                        user.career_profile.education_experiences[0],
                        'degreeAndOrgNameString',
                        'get',
                    ).mockReturnValue('Foo');
                    jest.spyOn(
                        user.career_profile.education_experiences[0],
                        'degreeAndOrgNameString',
                        'get',
                    ).mockReturnValue('Bar');
                    jest.spyOn(
                        user.career_profile.education_experiences[0],
                        'degreeAndOrgNameString',
                        'get',
                    ).mockReturnValue('Baz');
                });

                it('should initialize override options', () => {
                    render();
                    SpecHelper.assertSelectOptions(elem, '[name="transcript-override"]', [
                        'number:-1',
                        'number:1',
                        'number:2',
                        'string:some-uuid',
                    ]);
                });

                it('should default transcriptOverride to -1 for automatically chosen on server', () => {
                    render();
                    expect(scope.transcriptOverride).toEqual(-1);
                });

                it('should default transcriptOverride to value based on the override flag if present', () => {
                    user.career_profile.education_experiences[2].official_transcript_required_override = true;
                    render();
                    expect(scope.transcriptOverride).toEqual(user.career_profile.education_experiences[2].id);
                });

                it('should unset all override flags if admin selects the automatic choose option', () => {
                    render();
                    scope.userProxy.career_profile.education_experiences[2].official_transcript_required_override = true;
                    scope.$digest();

                    scope.onTranscriptOverrideChange(-1);

                    expect(
                        scope.userProxy.career_profile.education_experiences[2].official_transcript_required_override,
                    ).toBe(false);
                });

                it('should set override flag if admin chooses an education', () => {
                    render();
                    scope.onTranscriptOverrideChange(scope.userProxy.career_profile.education_experiences[1].id);
                    expect(
                        scope.userProxy.career_profile.education_experiences[1].official_transcript_required_override,
                    ).toBe(true);
                });
            });
        });

        describe('transcripts_verified badge', () => {
            it('should be visible if transcripts_verified', () => {
                user.transcripts_verified = true;
                render();
                SpecHelper.expectElement(elem, '[name="transcripts-verified-badge"]');
                SpecHelper.expectElementText(
                    elem,
                    '[name="transcripts-verified-badge"] .sub-text',
                    'Transcripts Verified',
                );
            });

            it('should not be visible if not transcripts_verified', () => {
                user.transcripts_verified = false;
                render();
                SpecHelper.expectNoElement(elem, '[name="transcripts-verified-badge"]');
            });
        });

        describe('instructions', () => {
            it('should show message explaining why user does not need to upload transcripts', () => {
                render();
                jest.spyOn(
                    scope.userProxy,
                    'careerProfileIndicatesUserShouldProvideTranscripts',
                    'get',
                ).mockReturnValue(false);
                scope.$digest();
                SpecHelper.expectElementText(
                    elem,
                    '[id="transcripts-section"] .admin-message',
                    'Learner did not complete a formal degree program and therefore does not require transcript approval.',
                );
            });
        });
    });

    describe('English Language Proficiency Documents section', () => {
        let cpIndicatesUserShouldUploadEnglishLanguageProficiencyDocsSpy;

        beforeEach(() => {
            cpIndicatesUserShouldUploadEnglishLanguageProficiencyDocsSpy = jest
                .spyOn(user, 'careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments', 'get')
                .mockReturnValue(true);
        });

        it('should check approval if approved', () => {
            user.english_language_proficiency_documents_approved = true;
            render();
            SpecHelper.expectCheckboxChecked(
                elem,
                '#english-language-proficiency-documentation-section [name="approve"]',
            );
        });

        it('should check approval but disable if not needed', () => {
            user.english_language_proficiency_documents_approved = false;
            jest.spyOn(
                user,
                'recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments',
                'get',
            ).mockReturnValue(false);
            render();
            SpecHelper.expectCheckboxChecked(
                elem,
                '#english-language-proficiency-documentation-section [name="approve"]',
            );
            SpecHelper.expectElementDisabled(
                elem,
                '#english-language-proficiency-documentation-section [name="approve"]',
            );
        });

        describe('left column', () => {
            it('should be disabled if !recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments', () => {
                render();
                const spy = jest
                    .spyOn(scope.userProxy, 'recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments', 'get')
                    .mockReturnValue(true);
                scope.$digest();
                SpecHelper.expectDoesNotHaveClass(
                    elem,
                    '[id="english-language-proficiency-documentation-section"] .col-sm-4',
                    'disabled',
                );

                spy.mockReturnValue(false);
                scope.$digest();
                SpecHelper.expectHasClass(
                    elem,
                    '[id="english-language-proficiency-documentation-section"] .col-sm-4',
                    'disabled',
                );
            });

            describe('approval checkbox', () => {
                it('should be visible if allowApproval', () => {
                    render({
                        allowApproval: true,
                    });
                    SpecHelper.expectElement(
                        elem,
                        '[id="english-language-proficiency-documentation-section"] input[type="checkbox"][name="approve"]',
                    );
                });

                describe('when visible', () => {
                    it('should be checked and disabled if !user.recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments and !user.careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments', () => {
                        jest.spyOn(
                            user,
                            'recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments',
                            'get',
                        ).mockReturnValue(false);
                        cpIndicatesUserShouldUploadEnglishLanguageProficiencyDocsSpy.mockReturnValue(false);
                        render({
                            allowApproval: true, // makes the English Language Proficiency Documentation section visible
                        });
                        SpecHelper.expectElementDisabled(
                            elem,
                            '[id="english-language-proficiency-documentation-section"] input[type="checkbox"][name="approve"]',
                        );
                        SpecHelper.expectCheckboxChecked(
                            elem,
                            '[id="english-language-proficiency-documentation-section"] input[type="checkbox"][name="approve"]',
                        );
                    });

                    it('should be wired to english_language_proficiency_documents_approved', () => {
                        jest.spyOn(
                            user,
                            'recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments',
                            'get',
                        ).mockReturnValue(true); // ensures checkbox is enabled
                        user.english_language_proficiency_documents_approved = false;
                        render({
                            allowApproval: true,
                        });
                        SpecHelper.toggleCheckbox(
                            elem,
                            '[id="english-language-proficiency-documentation-section"] input[type="checkbox"][name="approve"]',
                        );
                        expect(scope.userProxy.english_language_proficiency_documents_approved).toBe(true);
                    });
                });
            });

            describe('instructions', () => {
                describe('when showEnglishLanguageProficiencySection, !showUploadInstructions, allowApproval, and !careerProfileIndicatesUserShouldUploadEnglishLanguageProficiencyDocuments', () => {
                    it("should show message explaining that the user doesn't need to upload documents if the user is a native_english_speaker", () => {
                        user.career_profile.native_english_speaker = true;
                        render();
                        jest.spyOn(
                            scope.userProxy,
                            'recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments',
                            'get',
                        ).mockReturnValue(false);
                        scope.$digest();
                        expect(scope.englishLanguageProficiencySectionAdminMessageLocale).toEqual(
                            'english_language_proficiency_section_admin_message_for_native_english_speaker',
                        );
                        SpecHelper.expectElementText(
                            elem,
                            '[id="english-language-proficiency-documentation-section"] .col-sm-4 .sub-text.admin-message',
                            'Learner is a native English speaker and is therefore not required to upload English language proficiency documents and does not require document approval.',
                        );
                    });

                    it("should show message explaining that the user doesn't need to upload documents if the user is not a native_english_speaker but has earned_accredited_degree_in_english", () => {
                        user.career_profile.native_english_speaker = false;
                        user.career_profile.earned_accredited_degree_in_english = true;
                        render();
                        jest.spyOn(
                            scope.userProxy,
                            'recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments',
                            'get',
                        ).mockReturnValue(false);
                        scope.$digest();
                        expect(scope.englishLanguageProficiencySectionAdminMessageLocale).toEqual(
                            'english_language_proficiency_section_admin_message_for_accredited_degree_holder',
                        );
                        SpecHelper.expectElementText(
                            elem,
                            '[id="english-language-proficiency-documentation-section"] .col-sm-4 .sub-text.admin-message',
                            'Learner has earned an accredited degree from an institution where English was the principal language of instruction and is therefore not required to upload English language proficiency documents and does not require document approval.',
                        );
                    });

                    it("should show message explaining that the user doesn't need to upload documents if legacy user", () => {
                        user.career_profile.native_english_speaker = null;
                        user.career_profile.earned_accredited_degree_in_english = null;
                        render();
                        jest.spyOn(
                            scope.userProxy,
                            'recordsIndicateUserShouldUploadEnglishLanguageProficiencyDocuments',
                            'get',
                        ).mockReturnValue(false);
                        scope.$digest();
                        expect(scope.englishLanguageProficiencySectionAdminMessageLocale).toEqual(
                            'english_language_proficiency_section_admin_message_for_exempt_user',
                        );
                        SpecHelper.expectElementText(
                            elem,
                            '[id="english-language-proficiency-documentation-section"] .col-sm-4 .sub-text.admin-message',
                            "Learner was not required to specify on their cohort application if they're a native English speaker or if they've earned an accredited degree from an institution where the principal language of instruction was English and is therefore not required to upload English language proficiency documents and does not require document approval.",
                        );
                    });
                });
            });
        });
    });

    describe('toggleApproveCheckbox', () => {
        ['identity_verified', 'transcripts_verified', 'english_language_proficiency_documents_approved'].forEach(
            docsVerifiedProp => {
                it(`should appropriately set user.${docsVerifiedProp}`, () => {
                    scope.user[docsVerifiedProp] = undefined;
                    render();

                    scope.toggleApproveCheckbox(docsVerifiedProp);
                    expect(scope.userProxy[docsVerifiedProp]).toBe(true);

                    scope.toggleApproveCheckbox(docsVerifiedProp);
                    expect(scope.userProxy[docsVerifiedProp]).toBeUndefined(); // should set it back to its original value

                    scope.user[docsVerifiedProp] = false;
                    scope.userProxy[docsVerifiedProp] = false;
                    scope.$digest();

                    scope.toggleApproveCheckbox(docsVerifiedProp);
                    expect(scope.userProxy[docsVerifiedProp]).toBe(true);

                    scope.toggleApproveCheckbox(docsVerifiedProp);
                    expect(scope.userProxy[docsVerifiedProp]).toBe(false); // should set it back to its original value
                });
            },
        );
    });

    describe('Save Student Records button', () => {
        beforeEach(() => {
            render();
            scope.saving = false;
            scope.editDocuments.$invalid = false;
            scope.editDocuments.$dirty = false;
        });

        it('should be disabled if saving', () => {
            scope.saving = true;
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, '[name="save-documents"]');
        });

        it('should be disabled if editDocuments form is $invalid', () => {
            scope.editDocuments.$invalid = true;
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, '[name="save-documents"]');
        });

        it('should be disabled if form is clean', () => {
            scope.$digest();
            SpecHelper.expectElementDisabled(elem, '[name="save-documents"]');
        });

        it('should call save', () => {
            scope.editDocuments.$dirty = true;
            scope.$digest();
            jest.spyOn(scope, 'save').mockImplementation(() => {});
            SpecHelper.click(elem, '[name="save-documents"]');
            expect(scope.save).toHaveBeenCalled();
        });
    });

    describe('editDocuments $dirty', () => {
        it('should dirty form on transcript approved change', () => {
            render();
            expect(scope.editDocuments.$dirty).toBeFalsy();
            SpecHelper.toggleCheckbox(elem, '.approve-transcript:eq(0) input');
            expect(scope.editDocuments.$dirty).toBe(true);
        });

        it('should dirty on transcript waived change', () => {
            render();
            expect(scope.editDocuments.$dirty).toBeFalsy();
            SpecHelper.toggleCheckbox(elem, '[name="waive-checkbox"]:eq(0)');
            expect(scope.editDocuments.$dirty).toBe(true);
        });

        it('should dirty on transcript waiver change', () => {
            render();
            expect(scope.editDocuments.$dirty).toBeFalsy();
            SpecHelper.updateTextInput(elem, '.waiver-input:eq(0)', 'foo waiver updated');
            expect(scope.editDocuments.$dirty).toBe(true);
        });

        it('should dirty on transcript type change', () => {
            render();
            expect(scope.editDocuments.$dirty).toBeFalsy();
            SpecHelper.selectOptionByValue(elem, '.transcript-type-select:eq(0)', 'official');
            expect(scope.editDocuments.$dirty).toBe(true);
        });
    });

    describe('save', () => {
        it('should create ngToast on success', () => {
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            render();
            User.expect('save');
            scope.save();
            expect(ngToast.create).not.toHaveBeenCalled();
            User.flush('save');
            expect(ngToast.create).toHaveBeenCalledWith({
                content: 'Student Records Saved',
                className: 'success',
            });
        });

        it('should send a meta update object for any transcript_waiver changes', () => {
            render();
            User.expect('save').toBeCalledWith(user, {
                transcript_waiver_updates: {
                    '1': 'foo waiver',
                    '2': 'bar waiver',
                },
            });
            scope.save();
        });

        it('should reset saving and proxy after save request', () => {
            jest.spyOn(ngToast, 'create').mockImplementation(() => {});
            render();
            jest.spyOn(scope, 'resetProxy');
            scope.saving = true;
            scope.editDocuments.$dirty = true;
            scope.$digest();

            User.expect('save');
            scope.save();
            User.flush('save');
            expect(scope.saving).toBe(false);
            scope.editDocuments.$dirty = false;
            expect(scope.resetProxy).toHaveBeenCalled();
        });
    });
});
